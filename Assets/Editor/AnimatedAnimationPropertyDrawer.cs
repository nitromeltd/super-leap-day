using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

[CustomPropertyDrawer(typeof(Animated.Animation))]
public class AnimatedAnimationPropertyDrawer : PropertyDrawer
{
    private Sprite currentSpriteAnimating;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        var fps     = property.FindPropertyRelative("fps");
        var sprites = property.FindPropertyRelative("sprites");
        if (sprites.isExpanded == true)
        {
            return 30 +
                EditorGUI.GetPropertyHeight(fps, new GUIContent("FPS")) +
                EditorGUI.GetPropertyHeight(sprites, new GUIContent("Sprites"));
        }
        else
        {
            return 70;
        }
    }

    public override void OnGUI(
        Rect position, SerializedProperty property, GUIContent label
    )
    {
        var fpsProperty = property.FindPropertyRelative("fps");
        var spritesProperty = property.FindPropertyRelative("sprites");

        // background / border
        {
            GUI.Box(position, "", EditorStyles.helpBox);
        }

        // sprite on left
        {
            Sprite sprite = null;
            if (this.currentSpriteAnimating != null)
            {
                sprite = this.currentSpriteAnimating;
            }
            else if (spritesProperty.arraySize > 0)
            {
                var first = spritesProperty.GetArrayElementAtIndex(0);
                sprite = (Sprite)first.objectReferenceValue;
            }

            var previewStyle = new GUIStyle(EditorStyles.helpBox);
            previewStyle.alignment = TextAnchor.MiddleCenter;
            GUI.Box(
                new Rect(
                    position.x,
                    position.y,
                    spritesProperty.isExpanded ? 40 : 60,
                    spritesProperty.isExpanded ? 40 : 70
                ),
                sprite?.texture,
                previewStyle
            );
        }

        // animation name label
        {
            var labelStyle = new GUIStyle(EditorStyles.wordWrappedLabel);
            labelStyle.alignment = TextAnchor.MiddleLeft;
            var showText = label.text;
            EditorGUI.LabelField(
                new Rect(
                    position.x + (spritesProperty.isExpanded == true ? 50 : 70),
                    position.y + 5,
                    position.width - (spritesProperty.isExpanded == true ? 50 : 70) - 94,
                    40
                ),
                showText,
                labelStyle
            );
        }

        // fps box
        Rect fpsRect;
        {
            var fpsHeight =
                EditorGUI.GetPropertyHeight(fpsProperty, new GUIContent("FPS"));
            fpsRect =
                new Rect(position.x + position.width - 90, position.y + 15, 50, 20);
            var fpsLabelRect =
                new Rect(position.x + position.width - 65, position.y + 15, 20, 20);

            var fpsStyle = new GUIStyle(EditorStyles.textField);
            fpsStyle.alignment = TextAnchor.MiddleLeft;

            int newValue = EditorGUI.IntField(fpsRect, fpsProperty.intValue, fpsStyle);
            if (fpsProperty.intValue != newValue)
                fpsProperty.intValue = newValue;
            EditorGUI.LabelField(fpsLabelRect, "fps", EditorStyles.miniLabel);
        }

        // play button
        {
            var buttonRect = new Rect(
                position.x + position.width - 35, position.y + 10, 30, 30
            );
            if (GUI.Button(buttonRect, "▶") == true)
                PlayPreview(property);
        }

        // sprite array editor
        Rect spriteArrayRect;
        {
            var sHeight =
                EditorGUI.GetPropertyHeight(spritesProperty, new GUIContent("Sprites"));

            if (spritesProperty.isExpanded == true)
            {
                spriteArrayRect = new Rect(
                    position.x + 25, position.y + 43, position.width - 35, sHeight
                );
            }
            else
            {
                spriteArrayRect = new Rect(
                    // position.x + 80, position.y + 43, position.width - 85, sHeight
                    position.x + 70, position.y + 43, position.width - 75, sHeight
                );
            }

            GUI.backgroundColor = new Color(1.14f, 1.14f, 1.14f, 1);
            if (spritesProperty.isExpanded == true)
            {
                EditorGUI.PropertyField(spriteArrayRect, spritesProperty, true);
            }
            else
            {
                // TODO: fix the fact that this stops you from dragging
                // sprites into the field
                var style = new GUIStyle(EditorStyles.foldoutHeader);
                style.richText = true;
                var text =
                    $"<b>Sprites</b> <color=grey>({spritesProperty.arraySize})</color>";
                if (GUI.Button(spriteArrayRect, text, style) == true)
                {
                    spritesProperty.isExpanded = true;
                }
            }
            GUI.backgroundColor = Color.white;
        }

        if (position.Contains(Event.current.mousePosition) == true &&
            spriteArrayRect.Contains(Event.current.mousePosition) == false &&
            fpsRect.Contains(Event.current.mousePosition) == false)
        {
            switch (Event.current.type)
            {
                case EventType.DragUpdated:
                    NotifyDragUpdated();
                    break;

                case EventType.DragPerform:
                    NotifyDragPerformed(spritesProperty);
                    break;
            }
        }
    }

    private void PlayPreview(SerializedProperty property)
    {
        Sprite[] sprites;
        int fps;

        {
            var propertySprites = property.FindPropertyRelative("sprites");
            
            sprites = new Sprite[propertySprites.arraySize];
            for (int n = 0; n < propertySprites.arraySize; n += 1)
            {
                var item = propertySprites.GetArrayElementAtIndex(n);
                sprites[n] = item.objectReferenceValue as Sprite;
            }
            fps = property.FindPropertyRelative("fps").intValue;
        }

        if (sprites.Length == 0) return;

        var startTime = Time.realtimeSinceStartup;
        var frameDuration = 1.0f / fps;

        var editor = ActiveEditorTracker.sharedTracker.activeEditors.Where(
            e => e.serializedObject == property.serializedObject
        ).FirstOrDefault();

        void OnUpdate()
        {
            var oldSprite = this.currentSpriteAnimating;
            int f = Mathf.FloorToInt(
                (Time.realtimeSinceStartup - startTime) / frameDuration
            );
            this.currentSpriteAnimating = sprites[f % sprites.Length];
            if (this.currentSpriteAnimating == oldSprite) return;

            editor?.Repaint();

            if (f >= sprites.Length * 3)
            {
                this.currentSpriteAnimating = null;
                EditorApplication.update -= OnUpdate;
            }
        }

        EditorApplication.update += OnUpdate;
    }

    private bool IsDragDropValid()
    {
        foreach (Object draggedObject in DragAndDrop.objectReferences)
        {
            if (!(draggedObject is Texture2D))
                return false;
        }
        return true;
    }

    private void NotifyDragUpdated()
    {
        if (IsDragDropValid() == true)
            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
        else
            DragAndDrop.visualMode = DragAndDropVisualMode.Rejected;
    }

    private void NotifyDragPerformed(SerializedProperty spritesProperty)
    {
        if (IsDragDropValid() == false) return;

        var spriteAssetPaths = new List<string>();

        foreach (Object draggedObject in DragAndDrop.objectReferences)
        {
            var texture2d = draggedObject as Texture2D;
            if (texture2d == null) continue;
            spriteAssetPaths.Add(AssetDatabase.GetAssetPath(texture2d));
        }

        spriteAssetPaths.Sort();

        foreach (var assetPath in spriteAssetPaths)
        {
            var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(assetPath);
            if (sprite == null) continue;
            int index = spritesProperty.arraySize;
            spritesProperty.InsertArrayElementAtIndex(index);
            spritesProperty.GetArrayElementAtIndex(index)
                .objectReferenceValue = sprite;
        }
    }
}
