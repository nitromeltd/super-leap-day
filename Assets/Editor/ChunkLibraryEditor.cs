using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[CustomEditor(typeof(ChunkLibrary))]
public class ChunkLibraryEditor : Editor
{
    private StatGroups statGroups;

    private class StatGroups
    {
        public Stats all;
        public Stats alvaro;
        public Stats kenny;
        public Stats kiavik;
        public Stats mat;
        public Stats markus;
        public Stats owen;
        public Stats gabriel;
        public Stats marcin;
    };
    private struct Stats
    {
        public bool open;
        public int total;
        public int tutorial;
        public int easy;
        public int medium;
        public int hard;
        public int generic;
        public int rainyRuins;
        public int capitalHighway;
        public int conflictCanyon;
        public int hotNColdSprings;
        public int gravityGalaxy;
        public int sunkenIsland;
        public int treasureMines;
        public int windySkies;
        public int tombstoneHill;
        public int moltenFortress;
        public Dictionary<string, int> chunksWithSpecificItem;
        public int totalFruit;
    }

    public override void OnInspectorGUI()
    {
        var boxStyle = GUI.skin.GetStyle("HelpBox");

        EditorGUILayout.Space(5);
        EditorGUI.indentLevel += 1;

        if (GUILayout.Button("Copy basic stats to clipboard") == true)
        {
            var simpleTable = SimpleTable();
            EditorGUIUtility.systemCopyBuffer = simpleTable;
            Debug.Log(simpleTable);
        }

        if (GUILayout.Button("Detailed stats") == true)
        {
            this.statGroups = this.statGroups == null ? GetCounts() : null;
        }

        if (GUILayout.Button("Find old zero-g Gravity Galaxy chunks") == true)
        {
            FindOldZeroGChunks();
        }

        if (this.statGroups != null)
        {
            var items = this.statGroups.all.chunksWithSpecificItem.Keys.ToArray();
            Array.Sort(items);

            void DrawStats2(string header, ref Stats stats)
            {
                stats.open = EditorGUILayout.Foldout(stats.open, header);
                if (stats.open)
                {
                    EditorGUILayout.BeginVertical(boxStyle);
                    EditorGUILayout.LabelField("Total chunks", stats.total.ToString());
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(boxStyle);
                    EditorGUILayout.LabelField("Tutorial chunks", stats.tutorial.ToString());
                    EditorGUILayout.LabelField("Easy chunks", stats.easy.ToString());
                    EditorGUILayout.LabelField("Medium chunks", stats.medium.ToString());
                    EditorGUILayout.LabelField("Hard chunks", stats.hard.ToString());
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(boxStyle);
                    EditorGUILayout.LabelField("Generic", stats.generic.ToString());
                    EditorGUILayout.LabelField("Rainy Ruins", stats.rainyRuins.ToString());
                    EditorGUILayout.LabelField("Capital Highway", stats.capitalHighway.ToString());
                    EditorGUILayout.LabelField("Conflict Canyon", stats.conflictCanyon.ToString());
                    EditorGUILayout.LabelField("Hot 'n Cold Springs", stats.hotNColdSprings.ToString());
                    EditorGUILayout.LabelField("Gravity Galaxy", stats.gravityGalaxy.ToString());
                    EditorGUILayout.LabelField("Sunken Island", stats.sunkenIsland.ToString());
                    EditorGUILayout.LabelField("Treasure Mines", stats.treasureMines.ToString());
                    EditorGUILayout.LabelField("Windy Skies", stats.windySkies.ToString());
                    EditorGUILayout.LabelField("Tombstone Hill", stats.tombstoneHill.ToString());
                    EditorGUILayout.LabelField("Molten Fortress", stats.moltenFortress.ToString());
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(boxStyle);
                    EditorGUILayout.LabelField("Average fruit per chunk", ((float)stats.totalFruit / (float)stats.total).ToString("0.0"));
                    EditorGUILayout.EndVertical();
                    EditorGUILayout.BeginVertical(boxStyle);
                    if (stats.chunksWithSpecificItem != null)
                    {
                        foreach (var itemName in items)
                        {
                            int count = stats.chunksWithSpecificItem.TryGetValue(
                                itemName, out var value
                            ) ? value : 0;
                            EditorGUILayout.LabelField($"With {itemName}", count.ToString());
                        }
                    }
                    EditorGUILayout.EndVertical();
                }
            }
            DrawStats2("All chunks", ref this.statGroups.all);
            DrawStats2("Chunks by Alvaro", ref this.statGroups.alvaro);
            DrawStats2("Chunks by Kenny", ref this.statGroups.kenny);
            DrawStats2("Chunks by Kiavik", ref this.statGroups.kiavik);
            DrawStats2("Chunks by Mat", ref this.statGroups.mat);
            DrawStats2("Chunks by Markus", ref this.statGroups.markus);
            DrawStats2("Chunks by Owen", ref this.statGroups.owen);
            DrawStats2("Chunks by Gabriel", ref this.statGroups.gabriel);
            DrawStats2("Chunks by Marcin", ref this.statGroups.marcin);

            if (GUILayout.Button("Copy table to clipboard") == true)
            {
                var table = Table();
                EditorGUIUtility.systemCopyBuffer = table;
                Debug.Log(table);
            }
        }

        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space(5);

        base.DrawDefaultInspector();
    }

    private string SimpleTable()
    {
        var result = new StringBuilder();

        void Write<T>(string left, T tutorial, T easy, T medium, T hard)
        {
            result.AppendFormat(
                "│ {0,-25}│ {1,-10}{2,-10}{3,-10}{4,-10}│\n",
                left, tutorial, easy, medium, hard
            );
        }
        void WriteTheme(string prefix, string themeName) => Write(
            themeName,
            this.serializedObject.FindProperty(prefix + "Tutorial").arraySize,
            this.serializedObject.FindProperty(prefix + "Easy").arraySize,
            this.serializedObject.FindProperty(prefix + "Medium").arraySize,
            this.serializedObject.FindProperty(prefix + "Hard").arraySize
        );
        result.AppendLine("┌" + new string('─', 26) + "┬" + new string('─', 41) + "┐");
        Write("", "tutorial", "easy", "medium", "hard");
        result.AppendLine("├" + new string('─', 26) + "┼" + new string('─', 41) + "┤");
        WriteTheme("generic",         "generic");
        WriteTheme("rainyRuins",      "rainy ruins");
        WriteTheme("capitalHighway",  "capital highway");
        WriteTheme("conflictCanyon",  "conflict canyon");
        WriteTheme("hotNColdSprings", "hot 'n cold springs");
        WriteTheme("gravityGalaxy",   "gravity galaxy");
        WriteTheme("sunkenIsland",    "sunken island");
        WriteTheme("treasureMines",   "treasure mines");
        WriteTheme("windySkies",      "windy skies");
        WriteTheme("tombstoneHill",   "tombstone hill");
        WriteTheme("moltenFortress",  "molten fortress");
        result.AppendLine("└" + new string('─', 26) + "┴" + new string('─', 41) + "┘");

        return result.ToString();
    }

    private string Table()
    {
        var result = new StringBuilder();

        void Write<T>(string text, T all, T alvaro, T kenny, T kiavik, T owen, T gabriel, T marcin)
        {
            result.AppendFormat(
                "{0,-40}{1,-10}{2,-10}{3,-10}{4,-10}{5,-10}{6,-10}{7,-10}\n",
                text, all, alvaro, kenny, kiavik, owen, gabriel, marcin
            );
        }
        void WriteStat<T>(string text, Func<Stats, T> getter) => Write(
            text,
            getter(this.statGroups.all),
            getter(this.statGroups.alvaro),
            getter(this.statGroups.kenny),
            getter(this.statGroups.kiavik),
            getter(this.statGroups.owen),
            getter(this.statGroups.gabriel),
            getter(this.statGroups.marcin)
        );

        Write("", "All", "Alvaro", "Kenny", "Kiavik", "Owen", "Gabriel", "Marcin");
        result.AppendLine("------------------------------------------------------------------------------------------");
        WriteStat("Total chunks", s => s.total);
        result.AppendLine("------------------------------------------------------------------------------------------");
        WriteStat("Tutorial chunks", s => s.tutorial);
        WriteStat("Easy chunks", s => s.easy);
        WriteStat("Medium chunks", s => s.medium);
        WriteStat("Hard chunks", s => s.hard);
        result.AppendLine("------------------------------------------------------------------------------------------");
        WriteStat("Generic", s => s.generic);
        WriteStat("Rainy Ruins", s => s.rainyRuins);
        WriteStat("Capital Highway", s => s.capitalHighway);
        WriteStat("Conflict Canyon", s => s.conflictCanyon);
        WriteStat("Hot 'n Cold Springs", s => s.hotNColdSprings);
        WriteStat("Gravity Galaxy", s => s.gravityGalaxy);
        WriteStat("Sunken Island", s => s.sunkenIsland);
        WriteStat("Treasure Mines", s => s.treasureMines);
        WriteStat("Windy Skies", s => s.windySkies);
        WriteStat("Tombstone Hill", s => s.tombstoneHill);
        WriteStat("Molten Fortress", s => s.moltenFortress);
        result.AppendLine("------------------------------------------------------------------------------------------");
        WriteStat("Average fruit per chunk", s => ((float)s.totalFruit / (float)s.total).ToString("0.0"));

        result.AppendLine("------------------------------------------------------------------------------------------");
        var items = this.statGroups.all.chunksWithSpecificItem.Keys.ToArray();
        Array.Sort(items);
        foreach (var itemName in items)
        {
            WriteStat($"With {itemName}", s => {
                int count = s.chunksWithSpecificItem.TryGetValue(
                    itemName, out var value
                ) ? value : 0;
                return count;
            });
        }

        return result.ToString();
    }

    private StatGroups GetCounts()
    {
        var chunkArrays = new []
        {
            this.serializedObject.FindProperty("genericTutorial"),
            this.serializedObject.FindProperty("genericEasy"),
            this.serializedObject.FindProperty("genericMedium"),
            this.serializedObject.FindProperty("genericHard"),
            this.serializedObject.FindProperty("rainyRuinsTutorial"),
            this.serializedObject.FindProperty("rainyRuinsEasy"),
            this.serializedObject.FindProperty("rainyRuinsMedium"),
            this.serializedObject.FindProperty("rainyRuinsHard"),
            this.serializedObject.FindProperty("capitalHighwayTutorial"),
            this.serializedObject.FindProperty("capitalHighwayEasy"),
            this.serializedObject.FindProperty("capitalHighwayMedium"),
            this.serializedObject.FindProperty("capitalHighwayHard"),
            this.serializedObject.FindProperty("conflictCanyonTutorial"),
            this.serializedObject.FindProperty("conflictCanyonEasy"),
            this.serializedObject.FindProperty("conflictCanyonMedium"),
            this.serializedObject.FindProperty("conflictCanyonHard"),
            this.serializedObject.FindProperty("hotNColdSpringsTutorial"),
            this.serializedObject.FindProperty("hotNColdSpringsEasy"),
            this.serializedObject.FindProperty("hotNColdSpringsMedium"),
            this.serializedObject.FindProperty("hotNColdSpringsHard"),
            this.serializedObject.FindProperty("gravityGalaxyTutorial"),
            this.serializedObject.FindProperty("gravityGalaxyEasy"),
            this.serializedObject.FindProperty("gravityGalaxyMedium"),
            this.serializedObject.FindProperty("gravityGalaxyHard"),
            this.serializedObject.FindProperty("sunkenIslandTutorial"),
            this.serializedObject.FindProperty("sunkenIslandEasy"),
            this.serializedObject.FindProperty("sunkenIslandMedium"),
            this.serializedObject.FindProperty("sunkenIslandHard"),
            this.serializedObject.FindProperty("treasureMinesTutorial"),
            this.serializedObject.FindProperty("treasureMinesEasy"),
            this.serializedObject.FindProperty("treasureMinesMedium"),
            this.serializedObject.FindProperty("treasureMinesHard"),
            this.serializedObject.FindProperty("windySkiesTutorial"),
            this.serializedObject.FindProperty("windySkiesEasy"),
            this.serializedObject.FindProperty("windySkiesMedium"),
            this.serializedObject.FindProperty("windySkiesHard"),
            this.serializedObject.FindProperty("tombstoneHillTutorial"),
            this.serializedObject.FindProperty("tombstoneHillEasy"),
            this.serializedObject.FindProperty("tombstoneHillMedium"),
            this.serializedObject.FindProperty("tombstoneHillHard"),
            this.serializedObject.FindProperty("moltenFortressTutorial"),
            this.serializedObject.FindProperty("moltenFortressEasy"),
            this.serializedObject.FindProperty("moltenFortressMedium"),
            this.serializedObject.FindProperty("moltenFortressHard"),
        };

        string ItemNameFromTileName(string s)
        {
            // it's more helpful to see stats for (e.g.) "pipe" than the
            // individual pipe segment types. wherever it makes sense,
            // we consolidate variations and related things.

            var o = StringComparison.Ordinal;
            if (s.StartsWith("ab_block_",         o))  return "ab_block";
            if (s.StartsWith("ab_coin_",          o))  return "ab_coin";
            if (s.StartsWith("ab_silver_coin_",   o))  return "ab_silver_coin";
            if (s.StartsWith("ab_silver_coin_",   o))  return "ab_silver_coin";
            if (s.StartsWith("air_current_",      o))  return "air_current";
            if (s.StartsWith("apple_boulder_entrance_big_",   o))  return "apple_boulder_entrance_big";
            if (s.StartsWith("boost_gate_",       o))  return "boost_gate";
            if (s.StartsWith("breakable_block_",  o))  return "breakable_block";
            if (s.StartsWith("cctv_gate_block_",  o))  return "cctv_gate_block";
            if (s.StartsWith("chest_",            o))  return "chest";
            if (s.StartsWith("conveyor_",         o))  return "conveyor";
            if (s.StartsWith("crash_barrier_",    o))  return "crash_barrier";
            if (s.StartsWith("enemy_blocker_",    o))  return "enemy_blocker";
            if (s.StartsWith("gate_block_",       o))  return "gate_block";
            if (s.StartsWith("gravity_button_",   o))  return "gravity_button";
            if (s.StartsWith("grip_ground_",      o))  return "grip_ground";
            if (s.StartsWith("hotcold_platform_", o))  return "hotcold_platform";
            if (s.StartsWith("jellyfish_",        o))  return "jellyfish";
            if (s.StartsWith("locked_chest_",     o))  return "locked_chest";
            if (s.StartsWith("metal_block_",      o))  return "metal_block";
            if (s.StartsWith("no_grab_block_",    o))  return "no_grab_block";
            if (s.StartsWith("pipe_",             o))  return "pipe";
            if (s.StartsWith("player_blocker_",   o))  return "player_blocker";
            if (s.StartsWith("raft_motor_",       o))  return "raft_motor";
            if (s.StartsWith("raft_",             o))  return "raft";
            if (s.StartsWith("random_box_",       o))  return "random_box";
            if (s.StartsWith("sewer_pipe_",       o))  return "sewer_pipe";
            if (s.StartsWith("spike_",            o))  return "spike";
            if (s.StartsWith("spin_trap_",        o))  return "spin_trap";
            if (s.StartsWith("spring_",           o))  return "spring";
            if (s.StartsWith("timer_block_",      o))  return "timer_block";
            if (s.StartsWith("timer_button_",     o))  return "timer_button";
            if (s.StartsWith("timer_spike_",      o))  return "timer_spike";
            if (s.StartsWith("tntblock_",         o))  return "tntblock";

            switch (s)
            {
                case "random_fruit":
                case "apple":
                case "banana":
                case "cherry":
                case "lemon":
                case "melon":
                case "kiwi":
                case "orange":
                case "pear":
                case "pineapple":
                case "strawberry":
                    return "fruit";

                case "giant_apple":
                case "giant_cherry":
                case "giant_kiwi":
                case "giant_strawberry":
                    return "giant_fruit";

                case "apple_fish":
                case "banana_fish":
                case "melon_fish":
                case "orangee_fish":
                case "pear_fish":
                    return "fruit_fish";

                default:
                    return s;
            }
        }

        Dictionary<string, int> ItemCountsInChunk(string filename)
        {
            var resource = Resources.Load<TextAsset>("Levels/" + filename);
            var bytes = resource.bytes;
            var level = NitromeEditor.Level.UnserializeFromBytes(bytes);

            var result = new Dictionary<string, int>();

            foreach (var layerName in new [] { "objects", "objects2" })
            {
                var layer = level.TileLayerByName(layerName);
                if (layer == null) continue;

                foreach (var tile in layer.tiles)
                {
                    string t = tile.tile;
                    if (t == null || t == "")
                        continue;

                    string itemName = ItemNameFromTileName(t);

                    if (result.ContainsKey(itemName))
                        result[itemName] += 1;
                    else
                        result[itemName] = 1;
                }
            }

            return result;
        }

        var result = new StatGroups();

        foreach (var array in chunkArrays)
        {
            for (var n = 0; n < array.arraySize; n += 1)
            {
                var filename =
                    array.GetArrayElementAtIndex(n)
                    .FindPropertyRelative("filename").stringValue;
                var itemCountsInChunk = ItemCountsInChunk(filename);

                void Count(ref Stats stats)
                {
                    stats.total += 1;
                    if (filename.Contains("/Tutorial/"))           stats.tutorial += 1;
                    if (filename.Contains("/Easy/"))               stats.easy += 1;
                    if (filename.Contains("/Medium/"))             stats.medium += 1;
                    if (filename.Contains("/Hard/"))               stats.hard += 1;
                    if (filename.Contains("Generic/"))             stats.generic += 1;
                    if (filename.Contains("Rainy Ruins/"))         stats.rainyRuins += 1;
                    if (filename.Contains("Capital Highway/"))     stats.capitalHighway += 1;
                    if (filename.Contains("Conflict Canyon/"))     stats.conflictCanyon += 1;
                    if (filename.Contains("Hot N Cold Springs/"))  stats.hotNColdSprings += 1;
                    if (filename.Contains("Gravity Galaxy/"))      stats.gravityGalaxy += 1;
                    if (filename.Contains("Sunken Island/"))       stats.sunkenIsland += 1;
                    if (filename.Contains("Treasure Mines/"))      stats.treasureMines += 1;
                    if (filename.Contains("Windy Skies/"))         stats.windySkies += 1;
                    if (filename.Contains("Tombstone Hill/"))      stats.tombstoneHill += 1;
                    if (filename.Contains("Molten Fortress/"))     stats.moltenFortress += 1;

                    foreach (var kv in itemCountsInChunk)
                    {
                        if (stats.chunksWithSpecificItem == null)
                            stats.chunksWithSpecificItem = new Dictionary<string, int>();

                        if (stats.chunksWithSpecificItem.ContainsKey(kv.Key) == true)
                            stats.chunksWithSpecificItem[kv.Key] += 1;
                        else
                            stats.chunksWithSpecificItem[kv.Key] = 1;
                    }

                    int fruitInChunk =
                        itemCountsInChunk.TryGetValue("fruit", out var value) ? value : 0;

                    stats.totalFruit += fruitInChunk;
                }

                Count(ref result.all);
                if (filename.Contains("/Alvaro/")) Count(ref result.alvaro);
                if (filename.Contains("/Kenny/"))  Count(ref result.kenny);
                if (filename.Contains("/Kiavik/")) Count(ref result.kiavik);
                if (filename.Contains("/Mat/"))    Count(ref result.mat);
                if (filename.Contains("/Markus/")) Count(ref result.markus);
                if (filename.Contains("/Owen/"))   Count(ref result.owen);
                if (filename.Contains("/Gabriel/")) Count(ref result.gabriel);
                if (filename.Contains("/Marcin/")) Count(ref result.marcin);
            }
        }

        return result;
    }

    private void FindOldZeroGChunks()
    {
        var list = new List<ChunkLibrary.ChunkData>();

        foreach (var propEntries in new [] {
            this.serializedObject.FindProperty("gravityGalaxyTutorial"),
            this.serializedObject.FindProperty("gravityGalaxyEasy"),
            this.serializedObject.FindProperty("gravityGalaxyMedium"),
            this.serializedObject.FindProperty("gravityGalaxyHard")
        })
        {
            for (int n = 0; n < propEntries.arraySize; n += 1)
            {
                var entry = propEntries.GetArrayElementAtIndex(n);
                var filename = entry.FindPropertyRelative("filename").stringValue;
                if (filename.StartsWith("Set 7")) continue;

                var bytes = Resources.Load<TextAsset>("Levels/" + filename).bytes;
                var level = NitromeEditor.Level.UnserializeFromBytes(bytes);

                var objectsLayer = level.TileLayerByName("objects");
                var objects2Layer = level.TileLayerByName("objects2");
                var zeroG =
                    objectsLayer.tiles.Any(t => t.tile == "gravity_none_on_start") ||
                    objects2Layer.tiles.Any(t => t.tile == "gravity_none_on_start");

                if (zeroG == true)
                {
                    /* Right now this deletes the files locally, which isn't right.
                       Really, we would need to remove these levels from dropbox
                       (probably put them in some kind of 'deprecated' folder. */
                    Debug.Log(filename);
                    propEntries.DeleteArrayElementAtIndex(n);
                    n -= 1;
                }
            }
        }

        this.serializedObject.ApplyModifiedProperties();
    }
}
