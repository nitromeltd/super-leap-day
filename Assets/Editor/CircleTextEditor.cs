using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CircleText))]
public class CircleTextEditor : Editor
{
    public override void OnInspectorGUI()
    {
        var txt = (target as CircleText).GetComponent<TMPro.TMP_Text>();
        if(txt != null && txt.enableWordWrapping == true)
            EditorGUILayout.HelpBox("You must disable \"Wrapping\" in the Text Component.\n\nWhen the text is localized / changed it may overflow and break the calculations used to position the text.", MessageType.Warning);

        DrawDefaultInspector();
    }
}
