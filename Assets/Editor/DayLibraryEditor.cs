using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

[CustomEditor(typeof(DayLibrary))]
public class DayLibraryEditor : Editor
{
    public (int day, int month, int year) firstDate = (1, 1, DateTime.Now.Year);
    public (int day, int month, int year) lastDate = (1, 1, DateTime.Now.Year);
    public (int day, int month, int year) pinnedDate = (1, 1, DateTime.Now.Year);
    public Theme pinnedTheme = Theme.RainyRuins;
    public string version = "";

    private SerializedProperty orderOfThemesProperty;
    private int selectedTab;
    private int selectedYear;
    private HashSet<int> openDaysInDayLibrary = new HashSet<int>();
    private Regex dayNameRegex = new Regex("([\\w\\d ]+) \\((.*)\\)", RegexOptions.Compiled);
    private DateTime lastUpdateTime = new DateTime(1970, 1, 1);

    private void OnEnable()
    {
        this.orderOfThemesProperty = this.serializedObject.FindProperty("orderOfThemes");
    }

    public override void OnInspectorGUI()
    {
        if (DateTime.Now > this.lastUpdateTime.AddSeconds(1))
        {
            // calling this every single OnInspectorGUI() call is really slow. but we do
            // need to call it, to support Undo and potentially other weird situations.
            // i'm limiting the frequency of the calls so that the inspector isn't
            // quite so horrible to use.
            this.serializedObject.Update();
            this.lastUpdateTime = DateTime.Now;
        }

        // don't edit this. it's just here because reading this stuff
        // from the serialized object is really quite slow.
        DayLibrary library = (DayLibrary)this.serializedObject.targetObject;

        GUILayout.Space(10);
        this.selectedTab = GUILayout.Toolbar(
            this.selectedTab, new [] { "Order of Themes", "Days" },
            GUILayout.Height(30)
        );
        GUILayout.Space(10);

        if (this.selectedTab == 0)
        {
            // "ORDER OF THEMES" tab

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUI.indentLevel += 1;
            {
                int Count(Theme theme)
                {
                    int result = 0;
                    foreach (var t in library.orderOfThemes)
                    {
                        if (t == theme)
                            result += 1;
                    }
                    return result;
                }

                void Row(string name, Theme theme)
                {
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Space(10);
                    GUILayout.Label(name, GUILayout.Width(140));
                    GUILayout.Label(Count(theme).ToString());
                    EditorGUILayout.EndHorizontal();
                }

                Row("Rainy Ruins",         Theme.RainyRuins);
                Row("Capital Highway",     Theme.CapitalHighway);
                Row("Conflict Canyon",     Theme.ConflictCanyon);
                Row("Hot 'n Cold Springs", Theme.HotNColdSprings);
                Row("Gravity Galaxy",      Theme.GravityGalaxy);
                Row("Sunken Island",       Theme.SunkenIsland);
                Row("Treasure Mines",      Theme.TreasureMines);
                Row("Windy Skies",         Theme.WindySkies);
                Row("Tombstone Hill",      Theme.TombstoneHill);
                Row("Molten Fortress",     Theme.MoltenFortress);

                EditorGUILayout.Space(10);

                this.pinnedDate = DateField("Pin this date", this.pinnedDate);
                this.pinnedTheme = (Theme)EditorGUILayout.EnumPopup(
                    "...to this theme", (Enum)this.pinnedTheme
                );

                if (GUILayout.Button("Randomize order of themes") == true)
                    RandomizeOrderOfThemes();
            }
            EditorGUI.indentLevel -= 1;
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space(10);

            // EditorGUILayout.PropertyField(this.orderOfThemesProperty);
            
            var labelStyle = new GUIStyle(GUI.skin.label);
            labelStyle.richText = true;
            var themes = library.orderOfThemes;
            GUILayout.Label(
                new GUIContent(
                    "<size=20>Order of Themes</size> " +
                    $"<size=12>({themes.Length})</size>"
                ),
                labelStyle
            );

            for (int n = 0; n < themes.Length; n += 1)
            {
                GUILayout.Label(
                    $"[{n}]  <b>{RichTextForTheme(themes[n])}</b>",
                    labelStyle
                );
            }
        }
        else
        {
            // "DAYS" tab

            EditorGUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUI.indentLevel += 1;
            this.firstDate = DateField("First date", this.firstDate);
            this.lastDate = DateField("Last date", this.lastDate);

            DateTime first, last;
            bool isValid = false;
            try
            {
                first = new DateTime(firstDate.year, firstDate.month, firstDate.day);
                last = new DateTime(lastDate.year, lastDate.month, lastDate.day);
                if (last >= first && last < first.AddDays(1000))
                    isValid = true;
            }
            catch (Exception)
            {
                first = last = DateTime.Now.Date;
            }

            if (isValid == true)
            {
                var days = (last - first).TotalDays + 1;
                EditorGUILayout.LabelField($"Total days in range: {days}");
                this.version = EditorGUILayout.TextField("Game version", this.version);

                if (GUILayout.Button("Bake selected days", GUILayout.Height(25)) == true)
                    Bake(first, last);
                // if (GUILayout.Button("Rebake old Gravity Galaxy days for 1.9") == true)
                //     RebakeGravityGalaxy1_9();
            }
            else
            {
                EditorGUILayout.LabelField("Invalid date range");
                GUI.enabled = false;
                GUILayout.Button("Bake selected days");
                GUI.enabled = true;
            }

            EditorGUI.indentLevel -= 1;
            EditorGUILayout.EndVertical();
            EditorGUILayout.Space(10);

            // if (GUILayout.Button("Fix early chest bug"))
            // {
            //     FixEarlyChestBug();
            // }
            // EditorGUILayout.Space(10);

            var problematicDate = CheckDaysAreContiguous(library);
            if (problematicDate != null)
            {
                EditorGUILayout.HelpBox(
                    "Warning!\n" +
                    "The days array should be a contiguous list of days in order.\n" +
                    "After " + problematicDate.Value.ToString("d MMMM yyyy") +
                    ", the array should have " +
                    problematicDate.Value.AddDays(1).ToString("d MMMM yyyy") + ".",
                    MessageType.Warning
                );
            }

            {
                var years = library.days.Select(d => d.year).Distinct().ToArray();
                var yearStrings = years.Select(y => y.ToString()).ToArray();
                int newSelection = GUILayout.Toolbar(
                    Array.IndexOf(years, this.selectedYear), yearStrings, GUILayout.Height(30)
                );
                this.selectedYear = newSelection >= 0 ? years[newSelection] : 0;
            }

            EditorGUI.indentLevel += 2;

            var mostRecentMonth = (month: 0, year: 0);
            var mostRecentBrackets = "";
            var monthIndex = -1;
            var headerStyle = new GUIStyle(GUI.skin.label);
            headerStyle.fontSize = 20;
            var subheaderStyle = new GUIStyle(GUI.skin.label);
            subheaderStyle.fontSize = 14;
            var labelStyle = new GUIStyle(EditorStyles.foldout);
            labelStyle.richText = true;

            for (int n = 0; n < library.days.Length; n += 1)
            {
                var day = library.days[n];
                if (day.year != this.selectedYear) continue;

                if ((day.month, day.year) != mostRecentMonth)
                {
                    EditorGUI.indentLevel -= 1;

                    mostRecentMonth = (day.month, day.year);
                    monthIndex += 1;

                    string monthName;
                    try
                    {
                        monthName =
                            new DateTime(day.year, day.month, 1).ToString("MMMM yyyy");
                    }
                    catch (ArgumentOutOfRangeException)
                    {
                        monthName = "Invalid date";
                    }
                    EditorGUILayout.Space(10);
                    EditorGUILayout.LabelField(
                        monthName, headerStyle, GUILayout.Height(24)
                    );

                    EditorGUI.indentLevel += 1;
                    mostRecentBrackets = "";
                }

                string text = day.name;
                {
                    var match = this.dayNameRegex.Match(day.name);
                    if (match.Success == true)
                    {
                        if (match.Groups[2].Value != mostRecentBrackets)
                        {
                            mostRecentBrackets = match.Groups[2].Value;
                            EditorGUILayout.LabelField(mostRecentBrackets, subheaderStyle);
                        }
                        text = match.Groups[1].Value;
                    }
                }
                text = "<b>" + text + "</b>   " + RichTextForTheme(day.theme);
                var dayFoldedOut = EditorGUILayout.Foldout(
                    this.openDaysInDayLibrary.Contains(n), text, labelStyle
                );
                if (dayFoldedOut == true)
                    this.openDaysInDayLibrary.Add(n);
                else
                    this.openDaysInDayLibrary.Remove(n);

                if (dayFoldedOut == true)
                {
                    var property =
                        this.serializedObject.FindProperty("days")
                            .GetArrayElementAtIndex(n);

                    EditorGUILayout.BeginVertical(EditorStyles.helpBox);
                    foreach (var child in GetSerializedPropertyChildren(property))
                    {
                        EditorGUILayout.PropertyField(child, true);
                    }
                    EditorGUILayout.EndVertical();

                    this.serializedObject.ApplyModifiedProperties();
                }
            }

            EditorGUI.indentLevel -= 2;
            EditorGUILayout.Space(20);
        }
    }

    private (int, int, int) DateField(string label, (int day, int month, int year) date)
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label, GUILayout.Width(100));

        int indentLevel = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        bool wide = (EditorGUIUtility.currentViewWidth >= 340);
        GUILayout.Label(wide ? "day" : "d", GUILayout.ExpandWidth(false));
        date.day = EditorGUILayout.IntField(date.day, GUILayout.MinWidth(25));
        GUILayout.Label(wide ? "month" : "m", GUILayout.ExpandWidth(false));
        date.month = EditorGUILayout.IntField(date.month, GUILayout.MinWidth(25));
        GUILayout.Label(wide ? "year" : "y", GUILayout.ExpandWidth(false));
        date.year = EditorGUILayout.IntField(date.year, GUILayout.MinWidth(40));
        EditorGUILayout.EndHorizontal();

        EditorGUI.indentLevel = indentLevel;
        return date;
    }

    public static IEnumerable<SerializedProperty> GetSerializedPropertyChildren(
        SerializedProperty serializedProperty
    )
    {
        SerializedProperty currentProperty = serializedProperty.Copy();
        SerializedProperty nextSiblingProperty = serializedProperty.Copy();
        {
            nextSiblingProperty.Next(false);
        }
    
        if (currentProperty.Next(true))
        {
            do
            {
                if (SerializedProperty.EqualContents(currentProperty, nextSiblingProperty))
                    break;
    
                yield return currentProperty;
            }
            while (currentProperty.Next(false));
        }
    }

    private string RichTextForTheme(Theme theme)
    {
        string Dot(string colour) => $"<color={colour}><size=12>●</size></color> ";

        return theme switch
        {
            Theme.RainyRuins      => Dot("#8a946e") + "Rainy Ruins",
            Theme.CapitalHighway  => Dot("#bf1a5e") + "Capital Highway",
            Theme.ConflictCanyon  => Dot("#de9a39") + "Conflict Canyon",
            Theme.HotNColdSprings => Dot("#001479") + "Hot 'n Cold Springs",
            Theme.GravityGalaxy   => Dot("#000000") + "Gravity Galaxy",
            Theme.SunkenIsland    => Dot("#16ccb9") + "Sunken Island",
            Theme.TreasureMines   => Dot("#440d26") + "Treasure Mines",
            Theme.WindySkies      => Dot("#9effe8") + "Windy Skies",
            Theme.TombstoneHill   => Dot("#521c8f") + "Tombstone Hill",
            Theme.MoltenFortress  => Dot("#ff0000") + "Molten Fortress",
            _ => ObjectNames.NicifyVariableName(theme.ToString())
        };
    }

    private DateTime? CheckDaysAreContiguous(DayLibrary dayLibrary)
    {
        var before = new DateTime(
            dayLibrary.days[0].year,
            dayLibrary.days[0].month,
            dayLibrary.days[0].day
        );

        for (int n = 1; n < dayLibrary.days.Length; n += 1)
        {
            var day = dayLibrary.days[n];
            DateTime now;
            try
            {
                now = new DateTime(day.year, day.month, day.day);
            }
            catch (ArgumentOutOfRangeException)
            {
                return before;
            }

            if (now.Date != before.AddDays(1).Date)
                return before;
            before = now;
        }

        return null;
    }

    private void Bake(DateTime firstDay, DateTime lastDay)
    {
        var days = this.serializedObject.FindProperty("days");

        SerializedProperty FindDay(int day, int month, int year)
        {
            for (int n = 0; n < days.arraySize; n += 1)
            {
                var entry = days.GetArrayElementAtIndex(n);
                if (
                    entry.FindPropertyRelative("day").intValue == day &&
                    entry.FindPropertyRelative("month").intValue == month &&
                    entry.FindPropertyRelative("year").intValue == year
                )
                {
                    return entry;
                }
            }
            return null;
        }

        for (var date = firstDay; date <= lastDay; date = date.AddDays(1))
        {
            float completionAmount = 
                (float)(date.Ticks - firstDay.Ticks) /
                (float)(lastDay.Ticks - firstDay.Ticks);
            int percentage = Mathf.FloorToInt(completionAmount * 100);
            var dateAsString = date.ToString("d MMMM yyyy");
            EditorUtility.DisplayProgressBar(
                "Baking levels", $"{dateAsString} ({percentage}%)", completionAmount
            );

            var propDay = FindDay(date.Day, date.Month, date.Year);
            if (propDay == null)
            {
                days.InsertArrayElementAtIndex(days.arraySize);
                propDay = days.GetArrayElementAtIndex(days.arraySize - 1);
            }

            BakeDay(date, propDay, null);
        }

        EditorUtility.ClearProgressBar();
        this.serializedObject.ApplyModifiedProperties();
        AssetDatabase.SaveAssets();
    }

    private void BakeDay(
        DateTime date, SerializedProperty propDay, Theme? forcedTheme = null
    )
    {
        var dateAsString = date.ToString("d MMMM yyyy");

        var (theme, rows) =
            LevelGeneration.GenerateWithoutConnectors(date, forcedTheme);

        propDay.FindPropertyRelative("name").stringValue =
            $"{dateAsString} (version {this.version})";
        propDay.FindPropertyRelative("day").intValue = date.Day;
        propDay.FindPropertyRelative("month").intValue = date.Month;
        propDay.FindPropertyRelative("year").intValue = date.Year;
        propDay.FindPropertyRelative("theme").enumValueIndex = (int)theme;

        var propRowFilenames = propDay.FindPropertyRelative("rows");
        propRowFilenames.arraySize = 0;

        foreach (var row in rows)
        {
            propRowFilenames.InsertArrayElementAtIndex(propRowFilenames.arraySize);
            var propRow = propRowFilenames.GetArrayElementAtIndex(
                propRowFilenames.arraySize - 1
            );

            propRow.FindPropertyRelative("filename").stringValue = row.chunk.filename;
            propRow.FindPropertyRelative("type").enumValueIndex = Array.IndexOf(
                Enum.GetValues(typeof(LevelGeneration.RowType)), row.type
            );
            propRow.FindPropertyRelative(
                "fillAreaOfInterestWithCheckpoint"
            ).boolValue = row.fillAreaOfInterestWithCheckpoint;
            propRow.FindPropertyRelative(
                "fillAreaOfInterestWithBonusLift"
            ).boolValue = row.fillAreaOfInterestWithBonusLift;
        }
    }

    private void RebakeGravityGalaxy1_9()
    {
        var firstDay = new DateTime(2021, 8, 9);
        var lastDay = new DateTime(2024, 6, 30);

        var days = this.serializedObject.FindProperty("days");

        SerializedProperty FindDay(int day, int month, int year)
        {
            for (int n = 0; n < days.arraySize; n += 1)
            {
                var entry = days.GetArrayElementAtIndex(n);
                if (
                    entry.FindPropertyRelative("day").intValue == day &&
                    entry.FindPropertyRelative("month").intValue == month &&
                    entry.FindPropertyRelative("year").intValue == year
                )
                {
                    return entry;
                }
            }
            return null;
        }

        for (var date = firstDay; date <= lastDay; date = date.AddDays(1))
        {
            float completionAmount = 
                (float)(date.Ticks - firstDay.Ticks) /
                (float)(lastDay.Ticks - firstDay.Ticks);
            int percentage = Mathf.FloorToInt(completionAmount * 100);
            var dateAsString = date.ToString("d MMMM yyyy");
            EditorUtility.DisplayProgressBar(
                "Baking levels", $"{dateAsString} ({percentage}%)", completionAmount
            );

            var propDay = FindDay(date.Day, date.Month, date.Year);
            if (propDay == null) continue;

            Theme theme = (Theme)propDay.FindPropertyRelative("theme").enumValueIndex;
            if (theme != Theme.GravityGalaxy) continue;

            BakeDay(date, propDay, Theme.GravityGalaxy);
        }

        EditorUtility.ClearProgressBar();
        this.serializedObject.ApplyModifiedProperties();
        AssetDatabase.SaveAssets();
    }

    private void RandomizeOrderOfThemes()
    {
        var orderOfThemesProperty =
            this.serializedObject.FindProperty("orderOfThemes");

        var candidates = new [] {
            Theme.RainyRuins,
            Theme.CapitalHighway,
            Theme.ConflictCanyon,
            Theme.HotNColdSprings,
            Theme.GravityGalaxy,
            Theme.SunkenIsland,
            Theme.TreasureMines,
            Theme.WindySkies,
            Theme.TombstoneHill,
            Theme.MoltenFortress
        };
        int disallowRepeatWithinDays = 2;

        Theme[] Choices()
        {
            var result = new List<Theme>();

            int TimeSinceTheme(Theme theme)
            {
                for (int n = result.Count - 1; n >= 0; n -= 1)
                {
                    if (result[n] != theme) continue;
                    int time = result.Count - n;
                    return time;
                }
                return Mathf.Max(result.Count, 5);
            }

            for (int n = 0; n < 100; n += 1)
            {
                var candidatesThisTime = new List<Theme>();
                foreach (var theme in candidates)
                {
                    int count = TimeSinceTheme(theme);
                    if (count <= disallowRepeatWithinDays)
                        continue;
                    for (int i = 0; i < count; i += 1)
                        candidatesThisTime.Add(theme);
                }
                if (candidatesThisTime.Count == 0)
                    candidatesThisTime.AddRange(candidates);
                result.Add(Util.RandomChoice(candidatesThisTime));
            }

            return result.ToArray();
        }

        Theme[] ChoicesChecked()
        {
            for (int n = 0; n < 50000; n += 1)
            {
                var result = Choices();
                if (result[result.Length - 1] == result[0]) continue;
                if (result[result.Length - 2] == result[0]) continue;
                if (result[result.Length - 1] == result[1]) continue;
                if (result[result.Length - 2] == result[1]) continue;

                Func<Theme, int> FrequencyOfTheme =
                    theme => result.Count(c => c == theme);
                var frequencies = candidates.Select(FrequencyOfTheme).ToArray();
                if (frequencies.Max() > frequencies.Min() + 1) continue;

                return result;
            }
            Debug.Log("Didn't manage to find a valid arrangement!");
            return new Theme[0];
        }

        void PinSelectedDateToTheme(Theme[] result)
        {
            var pinned = new DateTime(
                this.pinnedDate.year, this.pinnedDate.month, this.pinnedDate.day
            );
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var indexOfPinnedDate = (int)((pinned - epoch).TotalDays);
            indexOfPinnedDate %= result.Length;

            var offset =
                Array.IndexOf(candidates, this.pinnedTheme) -
                Array.IndexOf(candidates, result[indexOfPinnedDate]);
            offset %= candidates.Length;
            if (offset < 0)
                offset += candidates.Length;

            for (int n = 0; n < result.Length; n += 1)
            {
                var index = Array.IndexOf(candidates, result[n]);
                index += offset;
                index %= candidates.Length;
                result[n] = candidates[index];
            }
        }

        {
            var result = ChoicesChecked();

            PinSelectedDateToTheme(result);

            while (orderOfThemesProperty.arraySize < result.Length)
                orderOfThemesProperty.InsertArrayElementAtIndex(0);

            while (orderOfThemesProperty.arraySize > result.Length)
                orderOfThemesProperty.DeleteArrayElementAtIndex(0);

            var enumValues = (Theme[])Enum.GetValues(typeof(Theme));

            for (int n = 0; n < result.Length; n += 1)
            {
                var enumValueIndex = Array.IndexOf(enumValues, result[n]);
                var propertyAtIndex = orderOfThemesProperty.GetArrayElementAtIndex(n);
                propertyAtIndex.enumValueIndex = enumValueIndex;
            }

            this.serializedObject.ApplyModifiedProperties();
            AssetDatabase.SaveAssets();
        }
    }

    private void FixEarlyChestBug()
    {
        var days = this.serializedObject.FindProperty("days");

        for (int d = 0; d < days.arraySize; d += 1)
        {
            var day = days.GetArrayElementAtIndex(d);
            var dayRows = day.FindPropertyRelative("rows");
            var isDayBugged =
                dayRows.GetArrayElementAtIndex(0).FindPropertyRelative("filename")
                    .stringValue == "start" &&
                dayRows.GetArrayElementAtIndex(1).FindPropertyRelative("filename")
                    .stringValue == "Technical/chest" &&
                dayRows.GetArrayElementAtIndex(2).FindPropertyRelative("filename")
                    .stringValue == "Technical/multiplayer_side_room";
            if (isDayBugged == false) continue;

            dayRows.MoveArrayElement(1, 2);
            Debug.Log("Fixed " + day.FindPropertyRelative("name").stringValue);
        }

        this.serializedObject.ApplyModifiedProperties();
        AssetDatabase.SaveAssets();
    }
}
