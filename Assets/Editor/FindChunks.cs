using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using UnityEditor;

public class FindChunks : EditorWindow
{
    private Vector2 scrollPosition;
    private string filenameSearchPattern;
    private string itemSearchPattern;
    private bool isMissingEntryExitMarker;
    [NonSerialized] private Search search;

    [MenuItem ("Super Leap Day/Find Chunks", priority = 50)]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(FindChunks));
    }

    public void OnGUI()
    {
        this.titleContent.text = "Find Chunks";
        this.scrollPosition = EditorGUILayout.BeginScrollView(this.scrollPosition);

        GUILayout.Space(10);
        this.filenameSearchPattern = EditorGUILayout.TextField(
            "Filename must contain:", this.filenameSearchPattern
        );
        this.itemSearchPattern = EditorGUILayout.TextField(
            "Must contain item:", this.itemSearchPattern
        );
        this.isMissingEntryExitMarker = EditorGUILayout.Toggle(
            "Has missing entry/exit marker", this.isMissingEntryExitMarker
        );
        if (this.search?.IsDone() == false)
        {
            if (GUILayout.Button("Cancel search", GUILayout.Height(30)) == true)
            {
                this.search.Cancel();
            }
        }
        else
        {
            if (GUILayout.Button("Search", GUILayout.Height(30)) == true)
            {
                this.search = new Search(
                    this.filenameSearchPattern,
                    this.itemSearchPattern,
                    this.isMissingEntryExitMarker
                );
            }
        }
        GUILayout.Space(10);

        var style = new GUIStyle(GUI.skin.label);
        style.richText = true;
        style.wordWrap = true;
        string nbsp = "\u00a0";

        if (this.search != null)
        {
            string title = $"Found {this.search.results.Count} matching chunk";
            if (this.search.results.Count != 1)
                title += "s";
            if (this.search.IsDone() == false)
                title += "...";
            GUILayout.Label($"<b><size=20>{title}</size></b>", style);

            if (this.search.IsDone() == false)
            {
                GUILayout.Label(
                    "    ... and searching " +
                    $"{this.search.Remaining()} chunks ...",
                    style
                );
            }
            foreach (var result in this.search.results)
            {
                GUILayout.Space(5);

                string filename = result.filename;
                if (search.filenameSearchPattern != "")
                {
                    filename = filename.Replace(
                        search.filenameSearchPattern,
                        $"<color=#ffc000>{search.filenameSearchPattern}</color>"
                    );
                }

                string HighlightItem(string foundItem) => foundItem.Replace(
                    search.itemSearchPattern,
                    $"<color=#20ffc0>{search.itemSearchPattern}</color>"
                );

                string itemInfo = string.Join(
                    ", ", result.countOfMatchingObjectNames.Select(
                        kv =>
                            $"<b><color=#20cfff>{HighlightItem(kv.Key)}</color></b>" +
                            $"{nbsp}×{nbsp}{kv.Value}"
                    )
                );
                itemInfo = $" ({itemInfo})";
                if (result.countOfMatchingObjectNames.Count == 0)
                    itemInfo = "";

                GUILayout.Label(
                    $"<b><color=white>{filename}</color></b>{itemInfo}", style
                );
            }

            if (this.search.IsDone() == false && this.search.results.Count > 0)
            {
                GUILayout.Label("    ...", style);
            }
        }

        GUILayout.Space(200);
        GUILayout.EndScrollView();

        if (Event.current.type == EventType.Repaint && this.search?.IsDone() == false)
        {
            this.search.DoWork();
            if (this.search.IsDone() == true)
            {
                Debug.Log(string.Join("\n", this.search.results.Select(r => r.filename)));
            }
            Repaint();
        }
    }

    public class Search
    {
        public string filenameSearchPattern;
        public string itemSearchPattern;
        public bool isMissingEntryExitMarker;
        public List<Result> results;
        [NonSerialized] private List<ChunkLibrary.ChunkData> remainingChunksToSearch;

        public class Result
        {
            public string filename;
            public Dictionary<string, int> countOfMatchingObjectNames;
        }

        public Search(
            string filenameSearchPattern,
            string itemSearchPattern,
            bool isMissingEntryExitMarker
        )
        {
            this.filenameSearchPattern = filenameSearchPattern ?? "";
            this.itemSearchPattern = itemSearchPattern ?? "";
            this.isMissingEntryExitMarker = isMissingEntryExitMarker;

            var chunkLibrary = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
                "Assets/Resources/Chunk Library.asset"
            );

            this.remainingChunksToSearch = new List<ChunkLibrary.ChunkData>();

            var bindingFlags = BindingFlags.Public | BindingFlags.Instance;
            foreach (var member in chunkLibrary.GetType().GetFields(bindingFlags))
            {
                if (member.FieldType == typeof(ChunkLibrary.ChunkData[]))
                {
                    var array = (ChunkLibrary.ChunkData[])member.GetValue(chunkLibrary);
                    this.remainingChunksToSearch.AddRange(array);
                }
            }

            if (this.filenameSearchPattern != "")
            {
                this.remainingChunksToSearch = this.remainingChunksToSearch.Where(
                    data => data.filename.Contains(this.filenameSearchPattern)
                ).ToList();
            }

            this.results = new List<Result>();
        }

        public bool IsDone() => this.remainingChunksToSearch.Count == 0;

        public int Remaining() => this.remainingChunksToSearch.Count;

        public void Cancel() => this.remainingChunksToSearch.Clear();

        public void DoWork()
        {
            for (int n = 0; n < 7; n += 1)
            {
                if (IsDone()) break;

                var next = this.remainingChunksToSearch[0];
                this.remainingChunksToSearch.RemoveAt(0);

                SearchData(next);
            }
        }

        private void SearchData(ChunkLibrary.ChunkData data)
        {
            var level = data.level;

            if (level == null)
            {
                var levelResource = Resources.Load<TextAsset>("Levels/" + data.filename);
                level = NitromeEditor.Level.UnserializeFromBytes(levelResource.bytes);
            }

            var result = new Result
            {
                filename = data.filename,
                countOfMatchingObjectNames = new Dictionary<string, int>()
            };

            if (this.itemSearchPattern != "")
            {
                foreach (var objectLayerName in new [] { "objects", "objects2" })
                {
                    var objectLayer = level.TileLayerByName(objectLayerName);
                    if (objectLayer == null) continue;

                    foreach (var tile in objectLayer.tiles)
                    {
                        if (tile.tile == null || tile.tile == "") continue;
                        if (tile.tile.Contains(this.itemSearchPattern))
                        {
                            if (result.countOfMatchingObjectNames.ContainsKey(tile.tile))
                                result.countOfMatchingObjectNames[tile.tile] += 1;
                            else
                                result.countOfMatchingObjectNames[tile.tile] = 1;
                        }
                    }
                }

                if (result.countOfMatchingObjectNames.Any() == false)
                    return; // disqualify this chunk
            }

            if (this.isMissingEntryExitMarker == true)
            {
                var markerLayer = level.TileLayerByName("markers");
                var markerCount = 0;

                if (markerLayer == null)
                    return;

                foreach (var tile in markerLayer.tiles)
                {
                    if (tile.tile == null || tile.tile == "") continue;
                    if (tile.tile.Contains("entry_exit_marker") == true)
                        markerCount += 1;
                }

                // if (markerCount != 1)
                if (markerCount == 2)
                    return; // disqualify this chunk
            }

            this.results.Add(result);
        }
    }
}
