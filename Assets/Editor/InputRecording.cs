using UnityEngine;
using UnityEditor;

public class InputRecording : EditorWindow
{
    private bool shouldPlayRecording = true;
    private bool autoStopEnabled;
    private int autoStopFrameNumber;
    private bool wasAtStopPoint;

    enum Symbol { Record, Square, Play }

    [MenuItem ("Super Leap Day/Input Recording", priority = 70)]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow(typeof(InputRecording));
    }

    public void OnEnable()
    {
        EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
    }

    public void OnDisable()
    {
        EditorApplication.playModeStateChanged -= OnPlayModeStateChanged;
    }

    public void OnInspectorUpdate()
    {
        if (GameInput.recordingInput.Length > 0)
            Repaint();
    }

    public void Update()
    {
        var isAtStopPoint =
            EditorApplication.isPlaying == true &&
            Game.instance != null &&
            Game.instance.map1 != null &&
            Game.instance.map1.frameNumber == this.autoStopFrameNumber;

        if (this.autoStopEnabled == true)
        {
            if (isAtStopPoint == true && this.wasAtStopPoint == false)
                EditorApplication.isPaused = true;
        }
        this.wasAtStopPoint = isAtStopPoint;
    }

    public void OnGUI()
    {
        this.titleContent.text = "Input Recording";

        var style = new GUIStyle(GUI.skin.label);
        style.richText = true;
        style.alignment = TextAnchor.MiddleLeft;

        var toggleStyle = new GUIStyle(GUI.skin.toggle);
        toggleStyle.richText = true;
        toggleStyle.alignment = TextAnchor.MiddleLeft;

        {
            GUILayout.Space(15);
            GUILayout.BeginVertical("HelpBox");
            GUILayout.Space(5);
            GUILayout.Label("<size=15>Recorded this session</size>", style);
            GUILayout.Space(5);

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            DrawSymbolInLayout(Symbol.Record, GameInput.recordingInput);
            GUILayout.Space(15);
            {
                var frames = GameInput.recordingInput.Length;
                string text1 = "<size=20>" + FramesAsTime(frames) + "</size>";
                string text2 = $"<size=12><color=grey>{frames} frames</color></size>";
                if (frames == 0) text2 = "";
                GUILayout.Label(text1, style, GUILayout.Width(110), GUILayout.Height(30));
                GUILayout.Label(text2, style, GUILayout.Height(30));
            }
            if (GUILayout.Button("Save", GUILayout.Width(80), GUILayout.Height(30)))
            {
                KeepRecording();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(14);
            GUILayout.EndVertical();
            DrawPreview(GUILayoutUtility.GetLastRect(), GameInput.recordingInput);
        }

        {
            GUILayout.Space(6);
            GUILayout.BeginVertical("HelpBox");
            GUILayout.Space(5);
            GUILayout.Label(
                "<size=15>Saved recording <color=grey>(in PlayerPrefs)</color></size>",
                style
            );
            GUILayout.Space(5);

            var recordedInput = PlayerPrefs.GetString("RecordedInput", "");

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            DrawSymbolInLayout(Symbol.Square, null);
            GUILayout.Space(15);
            {
                int frames = recordedInput.Length;
                string text1 = "<size=20>" + FramesAsTime(frames) + "</size>";
                string text2 = $"<size=12><color=grey>{frames} frames</color></size>";
                if (frames == 0) text2 = "";
                GUILayout.Label(text1, style, GUILayout.Width(110), GUILayout.Height(30));
                GUILayout.Label(text2, style, GUILayout.Height(30));
            }
            if (GUILayout.Button("Clear", GUILayout.Width(80), GUILayout.Height(30)))
                ClearRecording();
            GUILayout.EndHorizontal();

            GUILayout.Space(14);
            GUILayout.EndVertical();
            DrawPreview(GUILayoutUtility.GetLastRect(), recordedInput);

            GUILayout.Space(10);
        }

        {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            GUILayout.Label("<size=20>↓</size>", style);
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }

        {
            GUILayout.Space(5);
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            string text =
                " <color=#00ff00><b>PLAY</b></color> saved recording when game starts";
            this.shouldPlayRecording = GUILayout.Toggle(
                this.shouldPlayRecording, text, toggleStyle, GUILayout.Height(30)
            );
            if (GUILayout.Button("Play", GUILayout.Width(80), GUILayout.Height(30)))
                ReplayRecording();
            GUILayout.Space(10);
            GUILayout.EndHorizontal();
        }

        {
            var currentFrameNumber = 0;
            if (Game.instance != null && Game.instance.map1 != null)
                currentFrameNumber = Game.instance.map1.frameNumber;

            int replayingFrameNumber = GameInput.replayingInputFrameNumber;

            GUILayout.Space(10);
            GUILayout.BeginVertical("HelpBox");
            GUILayout.Space(5);

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            var recentData = GameInput.inputToReplay?.Substring(
                0, Mathf.Min(currentFrameNumber, GameInput.inputToReplay.Length)
            );
            DrawSymbolInLayout(Symbol.Play, recentData);
            GUILayout.Space(15);
            {
                var frames = Mathf.Min(
                    GameInput.inputToReplay?.Length ?? 0, currentFrameNumber
                );
                string text1 = "<size=20>" + FramesAsTime(frames) + "</size>";
                string text2 =
                    "<size=15>frame number <color=white><b>" +
                    $"#{currentFrameNumber} (#{replayingFrameNumber})</b></color></size>";
                GUILayout.BeginVertical();
                GUILayout.Label(text1, style, GUILayout.Width(110), GUILayout.Height(30));
                GUILayout.Label(text2, style);
                GUILayout.Space(4);
                GUILayout.EndVertical();
            }
            GUILayout.EndHorizontal();

            GUILayout.Space(5);
            GUILayout.EndVertical();
            GUILayout.Space(30);
        }

        {
            var textStyle = new GUIStyle(EditorStyles.textField);
            textStyle.fontSize = 15;
            textStyle.alignment = TextAnchor.MiddleLeft;

            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            var text = " <color=white><b>PAUSE</b></color> on frame number   #";
            this.autoStopEnabled = GUILayout.Toggle(
                this.autoStopEnabled, text, toggleStyle, GUILayout.Height(28)
            );
            this.autoStopFrameNumber = EditorGUILayout.IntField(
                this.autoStopFrameNumber, textStyle,
                GUILayout.Width(60), GUILayout.Height(28)
            );
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
    }

    private void DrawSymbolInLayout(Symbol symbol, string recentData)
    {
        GUILayout.Label("", GUILayout.Width(30), GUILayout.Height(30));
        var rect = GUILayoutUtility.GetLastRect();

        var color = symbol switch
        {
            Symbol.Record => Color.red,
            Symbol.Square => Color.yellow,
            Symbol.Play   => Color.green,
            _             => Color.white
        };

        if (recentData != null)
        {
            var highlight = 0.0f;
            for (int n = 0; n < 40; n += 1)
            {
                if (recentData.Length - 1 - n < 0) continue;
                if (recentData[recentData.Length - 1 - n] != ' ')
                {
                    highlight = 1 - (n / 40f);
                    break;
                }
            }
            color = Color.Lerp(color, Color.white, highlight * 0.6f);
        }
        var colorString = "#" + ColorUtility.ToHtmlStringRGB(color);

        var symbolStyle = new GUIStyle(GUI.skin.label);
        symbolStyle.richText = true;
        symbolStyle.alignment = TextAnchor.MiddleCenter;

        if (symbol == Symbol.Record)
        {
            rect.yMin -= 7;
            rect.xMax += 3;
            string text = $"<size=45><color={colorString}>•</color></size>";
            GUI.Label(rect, text, symbolStyle);
        }
        else if (symbol == Symbol.Square)
        {
            rect.yMin -= 7;
            rect.xMax += 1;
            string text = $"<size=28><color={colorString}>■</color></size>";
            GUI.Label(rect, text, symbolStyle);
        }
        else
        {
            rect.yMin -= 2;
            string text = $"<size=20><color={colorString}>▶</color></size>";
            GUI.Label(rect, text, symbolStyle);
        }
    }

    private void DrawPreview(Rect rect, string inputData)
    {
        if (Event.current.type != EventType.Repaint) return;

        var colorForJump = new Color(1, 0.5f, 0, 1);

        rect.xMin += 1;
        rect.xMax -= 1;
        rect.yMin = rect.yMax - 8;
        rect.yMax -= 1;
        EditorGUI.DrawRect(rect, Color.black);

        for (int n = 0; n < inputData.Length; n += 1)
        {
            if (inputData[n] == ' ') continue;

            var r = rect;
            r.xMin = Mathf.Lerp(
                rect.xMin, rect.xMax - 1, (float)n / inputData.Length
            );
            r.xMax = Mathf.Lerp(
                rect.xMin, rect.xMax - 1, ((float)(n + 1) / inputData.Length)
            ) + 1;
            EditorGUI.DrawRect(r, colorForJump);
        }
    }

    private void OnPlayModeStateChanged(PlayModeStateChange change)
    {
        if (change == PlayModeStateChange.EnteredPlayMode &&
            this.shouldPlayRecording == true)
        {
            ReplayRecording();
        }
    }

    private string FramesAsTime(int frames)
    {
        var seconds = frames / 60;
        var minutes = seconds / 60;
        seconds %= 60;
        frames %= 60;
        return
            "<b>" + minutes.ToString("00") + "</b>:" +
            "<b>" + seconds.ToString("00") + "</b>." +
            "<size=15>" + (frames * 100 / 60f).ToString("00") + "</size>";
    }

    private void KeepRecording()
    {
        PlayerPrefs.SetString("RecordedInput", GameInput.recordingInput);
    }

    private void ClearRecording()
    {
        PlayerPrefs.DeleteKey("RecordedInput");
        GameInput.inputToReplay = null;
    }

    private void ReplayRecording()
    {
        GameInput.inputToReplay = PlayerPrefs.GetString("RecordedInput");
    }
}
