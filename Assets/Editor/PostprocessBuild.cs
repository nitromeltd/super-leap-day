#if UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX

using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.iOS.Xcode;
using System.IO;

public class PostprocessBuild : IPostprocessBuildWithReport
{
    public int callbackOrder => 0;

    public void OnPostprocessBuild(BuildReport report)
    {
        string pathOfXcodeproj =
            Directory.GetDirectories(report.summary.outputPath, "*.xcodeproj")[0];
        string pathOfPbxProject = Path.Combine(pathOfXcodeproj, "project.pbxproj");

        if (report.summary.platform == BuildTarget.iOS)
        {
            string pathOfInfoPlist =
                Path.Combine(report.summary.outputPath, "Info.plist");

            UpdateInfoPlist(pathOfInfoPlist, report.summary.platform);
            AddEntitlements(
                pathOfPbxProject, report.summary.outputPath, report.summary.platform
            );
        }
        else if (report.summary.platform == BuildTarget.tvOS)
        {
            string pathOfInfoPlist =
                Path.Combine(report.summary.outputPath, "Info.plist");

            UpdateInfoPlist(pathOfInfoPlist, report.summary.platform);
            AddEntitlements(
                pathOfPbxProject, report.summary.outputPath, report.summary.platform
            );
            AddGameControllerFramework(pathOfPbxProject);
        }
        else if (report.summary.platform == BuildTarget.StandaloneOSX)
        {
            string contentsFolder =
                Directory.GetDirectories(report.summary.outputPath)[0];
            string pathOfInfoPlist = Path.Combine(contentsFolder, "Info.plist");

            UpdateInfoPlist(pathOfInfoPlist, report.summary.platform);
            AddEntitlements(
                pathOfPbxProject, report.summary.outputPath, report.summary.platform
            );
            SetOSXDeploymentTarget10_5(pathOfPbxProject);
        }
    }

    private void UpdateInfoPlist(string pathOfInfoPlist, BuildTarget platform)
    {
        PlistDocument plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(pathOfInfoPlist));

        PlistElementDict rootDict = plist.root;
        rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
        rootDict.SetBoolean("NSApplicationRequiresArcade", true);
        rootDict.SetBoolean("GCSupportsControllerUserInteraction", true);
        
        var controllers = rootDict.CreateArray("GCSupportedGameControllers");
        var extendedGamepad = controllers.AddDict();
        extendedGamepad.SetString("ProfileName", "ExtendedGamepad");

        if (platform == BuildTarget.tvOS)
        {
            var microGamepad = controllers.AddDict();
            microGamepad.SetString("ProfileName", "MicroGamepad");
        }

        if (platform == BuildTarget.StandaloneOSX)
        {
            rootDict.SetString("LSMinimumSystemVersion", "11.0");
        }

        File.WriteAllText(pathOfInfoPlist, plist.WriteToString());
    }

    private void AddEntitlements(
        string pathOfPbxProject, string parentPathOfBuild, BuildTarget platform
    )
    {
        var project = new PBXProject();
        project.ReadFromFile(pathOfPbxProject);

        string entitlementsProjectPath;
        string targetName;
        if (platform == BuildTarget.StandaloneOSX)
        {
            entitlementsProjectPath = $"Super Leap Day/Super Leap Day.entitlements";
            targetName = "Super Leap Day";
        }
        else
        {
            entitlementsProjectPath = "SuperLeapDay.entitlements";
            targetName = "Unity-iPhone";
        }

        var manager = new ProjectCapabilityManager(
            pathOfPbxProject, entitlementsProjectPath, targetName: targetName
        );
        manager.AddiCloud(true, false, false, false, null);
        manager.WriteToFile();

        var entitlementsAbsolutePath =
            Path.Combine(parentPathOfBuild, entitlementsProjectPath);

        var plist = new PlistDocument();
        plist.ReadFromFile(entitlementsAbsolutePath);

        plist.root.SetBoolean("com.apple.developer.game-center", true);

        if (platform == BuildTarget.iOS)
        {
            plist.root.SetString(
                "com.apple.developer.ubiquity-kvstore-identifier",
                "82C8U9W329.com.nitrome.leapday2"
            );
        }
        else if (platform == BuildTarget.tvOS)
        {
            var array = plist.root.CreateArray("com.apple.developer.user-management");
            array.AddString("runs-as-current-user");
        }
        else if (platform == BuildTarget.StandaloneOSX)
        {
            plist.root.SetBoolean("com.apple.security.app-sandbox", true);
            plist.root.SetBoolean("com.apple.developer.arcade-operations", true);
            plist.root.SetBoolean("com.apple.security.device.bluetooth", true);
            plist.root.SetBoolean("com.apple.security.device.usb", true);
        }

        plist.WriteToFile(entitlementsAbsolutePath);
    }

    private void AddGameControllerFramework(string pathOfPbxProject)
    {
        var project = new PBXProject();
        project.ReadFromFile(pathOfPbxProject);

        string targetGuid = project.GetUnityMainTargetGuid();
        project.AddFrameworkToProject(targetGuid, "GameController.framework", false);
        project.WriteToFile(pathOfPbxProject);
    }

    private void SetOSXDeploymentTarget10_5(string pathOfPbxProject)
    {
        var project = new PBXProject();
        project.ReadFromFile(pathOfPbxProject);

        string targetGuid = project.TargetGuidByName(PlayerSettings.productName);
        project.SetBuildProperty(targetGuid, "MACOSX_DEPLOYMENT_TARGET", "10.15");
        project.WriteToFile(pathOfPbxProject);
    }
}

#endif
