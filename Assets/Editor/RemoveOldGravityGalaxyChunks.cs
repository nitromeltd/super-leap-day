using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public static class RemoveOldGravityGalaxyChunks
{
    private class LogForDay
    {
        public Dictionary<int, (string from, string to)> rows;
    };
    private class LogForLibrary
    {
        public Dictionary<DayLibrary.Day, LogForDay> days;
    };

    [MenuItem("Super Leap Day/Remove and Replace old Gravity Galaxy chunks", priority = 200)]
    public static void RemoveAndReplaceOldGravityGalaxyChunks()
    {
        var chunkLibrary = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
            "Assets/Resources/Chunk Library.asset"
        );
        var dayLibrary = AssetDatabase.LoadAssetAtPath<DayLibrary>(
            "Assets/Resources/Day Library.asset"
        );

        Undo.RegisterCompleteObjectUndo(chunkLibrary, "Fix Old Gravity Galaxy Levels");
        Undo.RegisterCompleteObjectUndo(dayLibrary, "Fix Old Gravity Galaxy Levels");

        try
        {
            LogForLibrary log = new();
            System.Random rnd = new(08_12_1986);

            Debug.Log("[[ TUTORIAL ]]");
            var (valid, invalid) = PruneLevels(chunkLibrary.gravityGalaxyTutorial);
            chunkLibrary.gravityGalaxyTutorial = valid;
            EnsureDayLibraryIsValid(dayLibrary, log, rnd, valid, invalid, true);

            Debug.Log("[[ EASY ]]");
            (valid, invalid) = PruneLevels(chunkLibrary.gravityGalaxyEasy);
            chunkLibrary.gravityGalaxyEasy = valid;
            EnsureDayLibraryIsValid(dayLibrary, log, rnd, valid, invalid, false);

            Debug.Log("[[ MEDIUM ]]");
            (valid, invalid) = PruneLevels(chunkLibrary.gravityGalaxyMedium);
            chunkLibrary.gravityGalaxyMedium = valid;
            EnsureDayLibraryIsValid(dayLibrary, log, rnd, valid, invalid, false);

            Debug.Log("[[ HARD ]]");
            (valid, invalid) = PruneLevels(chunkLibrary.gravityGalaxyHard);
            chunkLibrary.gravityGalaxyHard = valid;
            EnsureDayLibraryIsValid(dayLibrary, log, rnd, valid, invalid, false);

            StringBuilder sb = new();
            foreach (var day in log.days.Keys.OrderBy(k => k.day + (k.month * 100) + (k.year * 10000)))
            {
                sb.AppendLine($"## {day.name}");
                foreach (var r in log.days[day].rows.Keys.OrderBy(k => k))
                {
                    var row = log.days[day].rows[r];
                    sb.AppendLine($"    [{r}] {row.from} -> {row.to}");
                }
                sb.AppendLine();
            }
            Debug.Log(sb);
        }
        finally
        {
            EditorUtility.SetDirty(chunkLibrary);
            EditorUtility.SetDirty(dayLibrary);
            AssetDatabase.SaveAssets();
        }
    }

    private static (
        ChunkLibrary.ChunkData[] valid, ChunkLibrary.ChunkData[] invalid
    ) PruneLevels(ChunkLibrary.ChunkData[] list)
    {
        var valid = new List<ChunkLibrary.ChunkData>();
        var invalid = new List<ChunkLibrary.ChunkData>();

        for (int n = 0; n < list.Length; n += 1)
        {
            var entry = list[n];

            var bytes = Resources.Load<TextAsset>("Levels/" + entry.filename).bytes;
            var level = NitromeEditor.Level.UnserializeFromBytes(bytes);

            var objectsLayer = level.TileLayerByName("objects");
            var objects2Layer = level.TileLayerByName("objects2");
            var zeroG =
                objectsLayer.tiles.Any(t => t.tile == "gravity_none_on_start") ||
                objects2Layer.tiles.Any(t => t.tile == "gravity_none_on_start");

            if (entry.filename.StartsWith("Set 7") || zeroG == false)
                valid.Add(entry);
            else
                invalid.Add(entry);
        }

        Debug.Log($"{list.Length} -> {valid.Count} valid, {invalid.Count} invalid");

        return (valid.ToArray(), invalid.ToArray());
    }

    private static void EnsureDayLibraryIsValid(
        DayLibrary dayLibrary,
        LogForLibrary log,
        System.Random rnd,
        ChunkLibrary.ChunkData[] validChunks,
        ChunkLibrary.ChunkData[] invalidChunks,
        bool isTutorial
    )
    {
        if (invalidChunks.Length == 0) return;

        foreach (var day in dayLibrary.days)
        {
            if (day.theme != Theme.GravityGalaxy)
                continue;

            // var str = new StringBuilder($"<color=white><b>{day.name}</b></color>\n");

            for (int r = 0; r < day.rows.Length; r += 1)
            {
                var row = day.rows[r];

                if (invalidChunks.Any(chunk => chunk.filename == row.filename) == false)
                    continue;

                var oldChunk = invalidChunks.First(chunk => chunk.filename == row.filename);

                // try and pick a chunk that matches in both direction (horz/vert) and
                // is a zero-g chunk. (we want zero-g chunks because those are the ones
                // being replaced.) we're trying to keep the level as close as possible
                // to how it was when it was originally baked/released.
                var candidates = validChunks.Where(
                    c =>
                        c.isHorizontal == oldChunk.isHorizontal &&
                        c.isZeroGravity == true &&
                        day.rows.Any(r => r.filename == c.filename) == false
                ).ToArray();

                if (candidates.Length == 0)
                {
                    if (isTutorial == true)
                    {
                        // for tutorial chunks, fallback to non-zero-g chunks.
                        // falling back to horizontal chunks here might be bad,
                        // as turning right too soon can cause the level structure
                        // to crash into the multiplayer room.
                        candidates = validChunks.Where(
                            c =>
                                c.isHorizontal == oldChunk.isHorizontal &&
                                // c.isZeroGravity == true &&
                                day.rows.Any(r => r.filename == c.filename) == false
                        ).ToArray();
                    }
                    else
                    {
                        // fallback to chunks with a different horz/vert direction.
                        // in most cases this is right, because we want to keep the
                        // distribution of zero-g and non-zero-g chunks the same
                        // as is was before.
                        candidates = validChunks.Where(
                            c =>
                                //c.isHorizontal == oldChunk.isHorizontal &&
                                c.isZeroGravity == true &&
                                day.rows.Any(r => r.filename == c.filename) == false
                        ).ToArray();
                    }
                }

                if (candidates.Length == 0)
                {
                    var s2 = new StringBuilder();
                    s2.AppendLine("No candidates :(");
                    candidates = validChunks;
                    s2.AppendLine($"Valid chunks = {candidates.Length}");
                    candidates = candidates.Where(c => c.isHorizontal == oldChunk.isHorizontal).ToArray();
                    s2.AppendLine($"With matching horizontal = {candidates.Length}");
                    candidates = candidates.Where(c => c.isZeroGravity == true).ToArray();
                    s2.AppendLine($"With zero-g true = {candidates.Length}");
                    candidates = candidates.Where(c => (day.rows.Any(r => r.filename == c.filename) == false)).ToArray();
                    s2.AppendLine($"With no duplicates = {candidates.Length}");
                    s2.AppendLine("---");
                    s2.AppendLine("Valid chunks:");
                    foreach (var c in validChunks) s2.AppendLine(c.filename);
                    s2.AppendLine("Invalid chunks:");
                    foreach (var c in invalidChunks) s2.AppendLine(c.filename);
                    Debug.LogError(s2);
                }

                var oldFilename = row.filename;
                var replacement = candidates[rnd.Next() % candidates.Length];
                row.filename = replacement.filename;

                log.days ??= new();
                if (log.days.ContainsKey(day) == false)
                    log.days[day] = new();
                log.days[day].rows ??= new();
                log.days[day].rows[r] = (oldFilename, replacement.filename);
                // str.AppendLine($"[{r}] {oldFilename} -> {row.filename}");
            }

            // Debug.Log(str);
        }
    }
}
