using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Linq;

public class RemovePngMetadata
{
    /* Remove Photshop's XMP data from a .png
     * 
     * Photoshop stuffs a PNG's iTXT chunk with XMP data
     * This can imflate an PNG's size by 5000% (I wish I was exaggerating)
     */
    [MenuItem("Super Leap Day/Remove Metadata From PNGs")]
    public static void RemovePNGMetadata()
    {
        var assetQueue = new Queue<string>();
        if(Selection.objects.Length > 0)
        {
            foreach(var o in Selection.objects)
            {
                if(o.GetType() == typeof(Texture2D))
                {
                    var s = AssetDatabase.GetAssetPath(o);
                    if(s.ToLower().EndsWith(".png"))
                        assetQueue.Enqueue(s);
                }
            }
            if(assetQueue.Count == 0)
                Debug.LogError("No pngs selected");
            else
            {
                while(assetQueue.Count > 0)
                    LoadAndSavePng(assetQueue.Dequeue());
            }
        }

        void LoadAndSavePng(string path)
        {
            Debug.Log($"Re-encoding {path}");

            var bytes = File.ReadAllBytes(path);
            //Debug.Log($"Bytes loaded {bytes.Length}");

            // Texture2D.LoadImage will silently fail when dealing with large XMP chunks
            // The iTXt chunk must be spliced manually
            bytes = RemovePNGChunk("iTXt", bytes);

            var tex = new Texture2D(2, 2, TextureFormat.RGBA32, 0, false);
            tex.LoadImage(bytes);
            
            //Debug.Log($"size {tex.width} {tex.height}");
            if((tex.width == 8 && tex.height == 8) || (tex.width == 2 && tex.height == 2))
            {
                // Unity silently fails LoadImage with an 8x8 png
                Debug.LogWarning($"failed to import: {path}");
                return;
            }

            bytes = tex.EncodeToPNG();
            //Debug.Log($"Bytes saved {bytes.Length}");
            File.WriteAllBytes(path, bytes);
            Texture2D.DestroyImmediate(tex);
        }
    }

    public static byte[] RemovePNGChunk(string name, byte[] bytes)
    {
        var encoding = new System.Text.ASCIIEncoding();
        byte[] nameBytes = encoding.GetBytes(name);
        uint idUint = BitConverter.ToUInt32(nameBytes, 0);

        for(int i = 0; i < bytes.Length - 4; i++)
        {
            if(BitConverter.ToUInt32(bytes, i) == idUint)
            {
                // BitConverter is reading little endian, so -
                var length =
                    bytes[i - 4] << 24 |
                    bytes[i - 3] << 16 |
                    bytes[i - 2] << 8 |
                    bytes[i - 1] << 0;
                Debug.Log($"removing: {name}, length: {length}");
                var byteList = bytes.ToList();
                // length + chunk id + CRC
                byteList.RemoveRange(i - 4, length + 12);
                return byteList.ToArray();
            }
        }
        return bytes;
    }
}
