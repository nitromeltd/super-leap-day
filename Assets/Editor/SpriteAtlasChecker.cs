using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.U2D;
using UnityEditor;

public class SpriteAtlasChecker
{
    [MenuItem("Super Leap Day/Check Sprite Atlases", priority = 30)]
    public static void CheckSpriteAtlases()
    {
        var spriteGuids = AssetDatabase.FindAssets("t:Sprite", null);
        var spritePaths = spriteGuids.Select(AssetDatabase.GUIDToAssetPath).ToList();
        spritePaths = spritePaths.Where(path => path.StartsWith("Assets/")).ToList();
        spritePaths.Sort();
        spriteGuids = spritePaths.Select(AssetDatabase.AssetPathToGUID).ToArray();

        var spriteAtlasGuids = AssetDatabase.FindAssets("t:SpriteAtlas", null);
        var spriteAtlasPaths =
            spriteAtlasGuids.Select(AssetDatabase.GUIDToAssetPath).ToArray();
        var spriteAtlases = spriteAtlasPaths.Select(
            path => AssetDatabase.LoadAssetAtPath<SpriteAtlas>(path)
        ).ToArray();

        var spritesForErrorMessage = new Dictionary<string, List<string>>();
        void AddError(string spritePath, string error)
        {
            if (spritesForErrorMessage.ContainsKey(error))
                spritesForErrorMessage[error].Add(spritePath);
            else
                spritesForErrorMessage[error] = new List<string> { spritePath };
        }

        for (int spriteIndex = 0; spriteIndex < spritePaths.Count; spriteIndex += 1)
        {
            EditorUtility.DisplayProgressBar(
                "Checking sprites",
                $"{spriteIndex + 1}/{spritePaths.Count}",
                (float)(spriteIndex) / spritePaths.Count
            );

            var spritePath = spritePaths[spriteIndex];
            var sprite = AssetDatabase.LoadAssetAtPath<Sprite>(spritePath);
            var matchingAtlases = new List<string>();

            for (int atlasIndex = 0; atlasIndex < spriteAtlases.Length; atlasIndex += 1)
            {
                if (spriteAtlases[atlasIndex].CanBindTo(sprite) == true)
                    matchingAtlases.Add(spriteAtlasPaths[atlasIndex]);
            }

            if (matchingAtlases.Count == 1)
                continue;

            if (matchingAtlases.Count > 1)
            {
                string Cyan(string s) => $"<b><color=cyan>{s}</color></b>";
                string White(string s) => $"<b><color=white>{s}</color></b>";
                var error =
                    "sprites in " + Cyan("multiple") + " SpriteAtlases [" +
                    string.Join(", ", matchingAtlases.Select(White)) + "]";
                AddError(spritePath, error);
                continue;
            }

            // sometimes sprites really don't want to be in an atlas. but they
            // usually do. record any exceptions to the rule here. (incomplete)
            if (spritePath.StartsWith("Assets/Editor/font")) continue;
            if (spritePath.StartsWith("Assets/Icon")) continue;

            AddError(
                spritePath, "sprites <b><color=yellow>not in any SpriteAtlas</color></b>"
            );
        }

        EditorUtility.ClearProgressBar();

        foreach (var kv in spritesForErrorMessage)
        {
            // we have to split up sprite names into groups of 100 because
            // error strings that are too long or complex can crash the unity console.
            for (int group = 0; group < kv.Value.Count; group += 100)
            {
                var spritePathsInGroup = kv.Value.Skip(group).Take(100).ToArray();

                var str =
                    "<b><color=white>" + spritePathsInGroup.Length +
                    "</color></b> " + kv.Key + "\n";
                foreach (var groupPath in spritePathsInGroup)
                {
                    str += "    - " + groupPath + "\n";
                }
                Debug.Log(str);
            }
        }
    }
}
