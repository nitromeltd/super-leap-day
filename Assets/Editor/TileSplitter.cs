using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using System.Collections.Generic;

public class TileSplitter
{
    private static Dictionary<string, SpriteAlignment> pivotsToSet;

    [MenuItem("Super Leap Day/Rebuild Tilesets/Capital Highway", priority = 10)]
    public static void BuildCapitalHighway() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/capital highway.png",
        targetAssetPath:  "Ingame/Tiles/City",
        themeName:        "City",
        sizeOfInteriorForNormalTiles:     new XY(8, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(7, 7)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Conflict Canyon", priority = 10)]
    public static void BuildConflictCanyon() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/conflict canyon.png",
        targetAssetPath:  "Ingame/Tiles/Desert",
        themeName:        "Desert",
        sizeOfInteriorForNormalTiles:     new XY(6, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(6, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Abseiling Day", priority = 10)]
    public static void BuildRollingDay() => ProcessTileset(
        assetPath: "Minigame/Textures/abseiling/Abseiling Tiles 1x.png",
        targetAssetPath: "Ingame/Tiles/Abseiling",
        themeName: "Abseiling",
        sizeOfInteriorForNormalTiles: new XY(6, 6),
        sizeOfInteriorForSecondaryTiles: new XY(6, 6),
        sizeOfInteriorForBackTiles: new XY(6, 6),
        sizeOfInteriorForOutsideTiles: new XY(6, 6)
    );

    // [MenuItem("Super Leap Day/Rebuild Tilesets/Hot N Cold Springs", priority = 10)]
    // public static void BuildHotNColdSprings() => ProcessTileset(
    //     assetPath:        "Ingame/Tiles/Templates/hot n cold springs.png",
    //     targetAssetPath:  "Ingame/Tiles/HotNCold",
    //     themeName:        "HotNCold",
    //     sizeOfInteriorForNormalTiles:     new XY(4, 4),
    //     sizeOfInteriorForSecondaryTiles:  new XY(4, 4),
    //     sizeOfInteriorForBackTiles:       new XY(8, 6),
    //     sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    // );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Sunken Island", priority = 10)]
    public static void BuildSunkenIsland() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/sunken island.png",
        targetAssetPath:  "Ingame/Tiles/Water",
        themeName:        "Water",
        sizeOfInteriorForNormalTiles:     new XY(6, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Sunken Island Dark Back Tiles", priority = 10)]
    public static void BuildSunkenIslandDarkBackTiles() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/sunken island dark inside.png",
        targetAssetPath:  "Ingame/Tiles/Water",
        themeName:        "WaterDark",
        sizeOfInteriorForNormalTiles:     new XY(6, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Treasure Mines", priority = 10)]
    public static void BuildTreasureMines() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/treasure mines.png",
        targetAssetPath:  "Ingame/Tiles/Mines",
        themeName:        "Mines",
        sizeOfInteriorForNormalTiles:     new XY(6, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Windy Skies", priority = 10)]
    public static void BuildWindySkies() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/windy skies.png",
        targetAssetPath:  "Ingame/Tiles/Windy Skies",
        themeName:        "Windy",
        sizeOfInteriorForNormalTiles:     new XY(8, 6),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Tombstone Hill", priority = 10)]
    public static void BuildTombstoneHill() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/tombstone hill.png",
        targetAssetPath:  "Ingame/Tiles/Tombstone Hill",
        themeName:        "Tombstone",
        sizeOfInteriorForNormalTiles:     new XY(8, 7),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Tombstone Hill Back For Tunnel", priority = 10)]
    public static void BuildTombstoneHillBackForTunnel() => ProcessTileset(
        assetPath:        "Ingame/Tiles/Templates/tombstone hill back for tunnel.png",
        targetAssetPath:  "Ingame/Tiles/Tombstone Hill Back for Tunnel",
        themeName:        "Tombstone",
        sizeOfInteriorForNormalTiles:     new XY(8, 7),
        sizeOfInteriorForSecondaryTiles:  new XY(5, 5),
        sizeOfInteriorForBackTiles:       new XY(4, 4),
        sizeOfInteriorForOutsideTiles:    new XY(4, 4)
    );

    [MenuItem("Super Leap Day/Rebuild Tilesets/Tombstone Hill Statue Trigger", priority = 10)]
    public static void BuildTombstoneHillExtraTilesets()
    {

        var themeName = "Tombstone";
        var assetPath = "Ingame/Tiles/Templates/tombstone hill statue trigger ground.png";
        var targetAssetPath = "Ingame/Tiles/Tombstone Hill Statue Trigger";
        XY sizeOfInteriorForInactiveTiles = new XY(5, 5);
        XY sizeOfInteriorForActiveTiles = new XY(4, 4);
        pivotsToSet = new Dictionary<string, SpriteAlignment>();

        var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(
            "Assets/" + assetPath
        );
        if (texture == null)
        {
            Debug.LogError("Texture does not exist.");
            return;
        }
        if (texture.isReadable == false)
        {
            Debug.LogError("Input Sprite must have 'Read/Write Enabled'");
            return;
        }
        if (texture.width <= 2048)
        {
            Debug.LogError("Input Sprite must have 'Texture Size' >= 4096");
            return;
        }

        var sprite = Sprite.Create(
            texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero
        );

        {
            var tileFolder = Path.Combine(Application.dataPath, targetAssetPath);
            Directory.CreateDirectory(tileFolder);
            Directory.CreateDirectory(Path.Combine(tileFolder, "Inactive"));
            Directory.CreateDirectory(Path.Combine(tileFolder, "Active"));
        }

        var path = targetAssetPath;

        // inactive (secondary)

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Inactive", 0.2f
        );

        GetSquare(texture, 24, 2, path, "Inactive");

        Extract(texture, 2 + 23, 9, path, "Inactive/row_left.png");
        Extract(texture, 3 + 23, 9, path, "Inactive/row_center_1.png");
        Extract(texture, 4 + 23, 9, path, "Inactive/row_center_2.png");
        Extract(texture, 5 + 23, 9, path, "Inactive/row_center_3.png");
        Extract(texture, 6 + 23, 9, path, "Inactive/row_right.png");

        Extract(texture, 6 + 23, 11, path, "Inactive/pillar_top.png");
        Extract(texture, 6 + 23, 12, path, "Inactive/pillar_middle_1.png");
        Extract(texture, 6 + 23, 13, path, "Inactive/pillar_middle_2.png");
        Extract(texture, 6 + 23, 14, path, "Inactive/pillar_middle_3.png");
        Extract(texture, 6 + 23, 15, path, "Inactive/pillar_bottom.png");

        Extract(texture, 4 + 23, 11, path, "Inactive/square_1.png");
        Extract(texture, 4 + 23, 13, path, "Inactive/square_2.png");
        Extract(texture, 4 + 23, 15, path, "Inactive/square_3.png");

        GetInterior(
            texture, 24, 17,
            sizeOfInteriorForInactiveTiles.x,
            sizeOfInteriorForInactiveTiles.y,
            path,
            "Inactive/interior_"
        );

        // active (back)

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Active", 0.4f
        );

        GetSquare(texture, 31, 2, path, "Active");

        Extract(texture, 1 + 30, 9, path, "Active/row_left.png");
        Extract(texture, 2 + 30, 9, path, "Active/row_center_1.png");
        Extract(texture, 3 + 30, 9, path, "Active/row_center_2.png");
        Extract(texture, 4 + 30, 9, path, "Active/row_center_3.png");
        Extract(texture, 5 + 30, 9, path, "Active/row_right.png");

        Extract(texture, 5 + 30, 11, path, "Active/pillar_top.png");
        Extract(texture, 5 + 30, 12, path, "Active/pillar_middle_1.png");
        Extract(texture, 5 + 30, 13, path, "Active/pillar_middle_2.png");
        Extract(texture, 5 + 30, 14, path, "Active/pillar_middle_3.png");
        Extract(texture, 5 + 30, 15, path, "Active/pillar_bottom.png");

        Extract(texture, 3 + 30, 11, path, "Active/square_1.png");
        Extract(texture, 3 + 30, 13, path, "Active/square_2.png");
        Extract(texture, 3 + 30, 15, path, "Active/square_3.png");

        GetInterior(
            texture, 31, 17,
            sizeOfInteriorForActiveTiles.x,
            sizeOfInteriorForActiveTiles.y,
            path,
            "Active/interior_"
        );

        // apply accumulated pivot information

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + " (pivots)", 0.8f
        );

        ApplyPivots(path);

        // if you're seeing phantom compiler errors below, go into
        // Assembly-CSharp-Editor.csproj and change the reference to
        // Assembly-CSharp.csproj so that <ReferenceOutputAssembly> is true, not false.
        // https://forum.unity.com/threads/2019-3-12f1-build-errors.880312/

        // create tilesets

        CreateTileset(
            targetAssetPath,
            themeName,
            "Inactive",
            Tileset.TilesetType.Dirt,
            sizeOfInteriorForInactiveTiles.x,
            sizeOfInteriorForInactiveTiles.y
        );
        CreateTileset(
            targetAssetPath,
            themeName,
            "Active",
            Tileset.TilesetType.Back,
            sizeOfInteriorForActiveTiles.x,
            sizeOfInteriorForActiveTiles.y
        );


        AssetDatabase.Refresh();

        ApplyPivots(path);
        AssetDatabase.Refresh();
        pivotsToSet = null;

        EditorUtility.ClearProgressBar();

    }

    private static void ProcessTileset(
        string assetPath,
        string targetAssetPath,
        string themeName,
        XY sizeOfInteriorForNormalTiles,
        XY sizeOfInteriorForSecondaryTiles,
        XY sizeOfInteriorForBackTiles,
        XY sizeOfInteriorForOutsideTiles
    )
    {
        pivotsToSet = new Dictionary<string, SpriteAlignment>();

        var texture = AssetDatabase.LoadAssetAtPath<Texture2D>(
            "Assets/" + assetPath
        );
        if (texture == null)
        {
            Debug.LogError("Texture does not exist.");
            return;
        }
        if (texture.isReadable == false)
        {
            Debug.LogError("Input Sprite must have 'Read/Write Enabled'");
            return;
        }
        if (texture.width <= 2048)
        {
            Debug.LogError("Input Sprite must have 'Texture Size' >= 4096");
            return;
        }

        var sprite = Sprite.Create(
            texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero
        );

        {
            var tileFolder = Path.Combine(Application.dataPath, targetAssetPath);
            Directory.CreateDirectory(tileFolder);
            Directory.CreateDirectory(Path.Combine(tileFolder, "Normal"));
            Directory.CreateDirectory(Path.Combine(tileFolder, "Secondary"));
            Directory.CreateDirectory(Path.Combine(tileFolder, "Back"));
            Directory.CreateDirectory(Path.Combine(tileFolder, "Outside"));
        }

        var path = targetAssetPath;

        // normal tiles

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Normal", 0
        );

        GetSquare(texture, 1, 2, path, "Normal");

        Extract(texture, 2, 9, path, "Normal/row_left.png");
        Extract(texture, 3, 9, path, "Normal/row_center_1.png");
        Extract(texture, 4, 9, path, "Normal/row_center_2.png");
        Extract(texture, 5, 9, path, "Normal/row_center_3.png");
        Extract(texture, 6, 9, path, "Normal/row_right.png");

        Extract(texture, 6, 11, path, "Normal/pillar_top.png");
        Extract(texture, 6, 12, path, "Normal/pillar_middle_1.png");
        Extract(texture, 6, 13, path, "Normal/pillar_middle_2.png");
        Extract(texture, 6, 14, path, "Normal/pillar_middle_3.png");
        Extract(texture, 6, 15, path, "Normal/pillar_bottom.png");

        Extract(texture, 4, 11, path, "Normal/square_1.png");
        Extract(texture, 4, 13, path, "Normal/square_2.png");
        Extract(texture, 4, 15, path, "Normal/square_3.png");

        Extract(texture, 2, 17, path, "Normal/45br.png");
        Extract(texture, 4, 17, path, "Normal/45bl.png");
        Extract(texture, 2, 19, path, "Normal/45tr.png");
        Extract(texture, 4, 19, path, "Normal/45tl.png");
        Extract(texture, 2, 3, 21, 21, path, "Normal/22br.png");
        Extract(texture, 5, 6, 21, 21, path, "Normal/22bl.png");
        Extract(texture, 2, 3, 23, 23, path, "Normal/22tr.png");
        Extract(texture, 5, 6, 23, 23, path, "Normal/22tl.png");

        Extract(texture, 2, 26, path, "Normal/cloudleft.png");
        Extract(texture, 3, 26, path, "Normal/cloud_1.png");
        Extract(texture, 4, 26, path, "Normal/cloud_2.png");
        Extract(texture, 5, 26, path, "Normal/cloud_3.png");
        Extract(texture, 6, 26, path, "Normal/cloudright.png");

        Extract(texture,  9, 11,  2,  4, path, "Normal/curveouttl3.png");
        Extract(texture, 12, 14,  2,  4, path, "Normal/curveouttr3.png");
        Extract(texture,  9, 11,  5,  7, path, "Normal/curveoutbl3.png");
        Extract(texture, 12, 14,  5,  7, path, "Normal/curveoutbr3.png");
        Extract(texture,  8, 11,  9, 12, path, "Normal/curvetl3plus1.png");
        Extract(texture, 12, 15,  9, 12, path, "Normal/curvetr3plus1.png");
        Extract(texture,  8, 11, 13, 16, path, "Normal/curvebl3plus1.png");
        Extract(texture, 12, 15, 13, 16, path, "Normal/curvebr3plus1.png");
        Extract(texture,  8, 11, 18, 21, path, "Normal/curvetl3plus1_outside.png");
        Extract(texture, 12, 15, 18, 21, path, "Normal/curvetr3plus1_outside.png");
        Extract(texture,  8, 11, 22, 25, path, "Normal/curvebl3plus1_outside.png");
        Extract(texture, 12, 15, 22, 25, path, "Normal/curvebr3plus1_outside.png");

        Extract(texture, 17, 18, 2, 3, path, "Normal/22br_joined.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 21, 2, 3, path, "Normal/22bl_joined.png", SpriteAlignment.LeftCenter);
        Extract(texture, 17, 18, 4, 5, path, "Normal/22tr_joined.png");
        Extract(texture, 20, 21, 4, 5, path, "Normal/22tl_joined.png");
        Extract(texture, 18, 18, 7, 8, path, "Normal/45br_joinedleft.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 20, 7, 8, path, "Normal/45bl_joinedright.png", SpriteAlignment.LeftCenter);
        Extract(texture, 18, 18, 9, 10, path, "Normal/45tr_joinedleft.png");
        Extract(texture, 20, 20, 9, 10, path, "Normal/45tl_joinedright.png");
        Extract(texture, 18, 18, 12, 13, path, "Normal/45br_joined.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 20, 12, 13, path, "Normal/45bl_joined.png", SpriteAlignment.LeftCenter);
        Extract(texture, 18, 18, 14, 15, path, "Normal/45tr_joined.png");
        Extract(texture, 20, 20, 14, 15, path, "Normal/45tl_joined.png");
        Extract(texture, 18, 18, 17, 18, path, "Normal/45bl_joinedleft.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 20, 17, 18, path, "Normal/45br_joinedright.png", SpriteAlignment.LeftCenter);
        Extract(texture, 18, 18, 19, 20, path, "Normal/45tl_joinedleft.png");
        Extract(texture, 20, 20, 19, 20, path, "Normal/45tr_joinedright.png");
        Extract(texture, 17, 18, 22, 23, path, "Normal/22bl_joinedleft.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 21, 22, 23, path, "Normal/22br_joinedright.png", SpriteAlignment.LeftCenter);
        Extract(texture, 17, 18, 24, 25, path, "Normal/22tl_joinedleft.png");
        Extract(texture, 20, 21, 24, 25, path, "Normal/22tr_joinedright.png");
        Extract(texture, 17, 18, 27, 28, path, "Normal/22br_joinedleft.png", SpriteAlignment.LeftCenter);
        Extract(texture, 20, 21, 27, 28, path, "Normal/22bl_joinedright.png", SpriteAlignment.LeftCenter);
        Extract(texture, 17, 18, 29, 30, path, "Normal/22tr_joinedleft.png");
        Extract(texture, 20, 21, 29, 30, path, "Normal/22tl_joinedright.png");

        GetInterior(
            texture, 8, 27,
            sizeOfInteriorForNormalTiles.x,
            sizeOfInteriorForNormalTiles.y,
            path,
            "Normal/interior_"
        );

        // secondary

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Secondary", 0.2f
        );

        GetSquare(texture, 24, 2, path, "Secondary");

        Extract(texture, 2 + 23, 9, path, "Secondary/row_left.png");
        Extract(texture, 3 + 23, 9, path, "Secondary/row_center_1.png");
        Extract(texture, 4 + 23, 9, path, "Secondary/row_center_2.png");
        Extract(texture, 5 + 23, 9, path, "Secondary/row_center_3.png");
        Extract(texture, 6 + 23, 9, path, "Secondary/row_right.png");

        Extract(texture, 6 + 23, 11, path, "Secondary/pillar_top.png");
        Extract(texture, 6 + 23, 12, path, "Secondary/pillar_middle_1.png");
        Extract(texture, 6 + 23, 13, path, "Secondary/pillar_middle_2.png");
        Extract(texture, 6 + 23, 14, path, "Secondary/pillar_middle_3.png");
        Extract(texture, 6 + 23, 15, path, "Secondary/pillar_bottom.png");

        Extract(texture, 4 + 23, 11, path, "Secondary/square_1.png");
        Extract(texture, 4 + 23, 13, path, "Secondary/square_2.png");
        Extract(texture, 4 + 23, 15, path, "Secondary/square_3.png");

        GetInterior(
            texture, 24, 17,
            sizeOfInteriorForSecondaryTiles.x,
            sizeOfInteriorForSecondaryTiles.y,
            path,
            "Secondary/interior_"
        );

        // back

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Back", 0.4f
        );

        GetSquare(texture, 31, 2, path, "Back");

        Extract(texture, 1 + 30, 9, path, "Back/row_left.png");
        Extract(texture, 2 + 30, 9, path, "Back/row_center_1.png");
        Extract(texture, 3 + 30, 9, path, "Back/row_center_2.png");
        Extract(texture, 4 + 30, 9, path, "Back/row_center_3.png");
        Extract(texture, 5 + 30, 9, path, "Back/row_right.png");

        Extract(texture, 5 + 30, 11, path, "Back/pillar_top.png");
        Extract(texture, 5 + 30, 12, path, "Back/pillar_middle_1.png");
        Extract(texture, 5 + 30, 13, path, "Back/pillar_middle_2.png");
        Extract(texture, 5 + 30, 14, path, "Back/pillar_middle_3.png");
        Extract(texture, 5 + 30, 15, path, "Back/pillar_bottom.png");

        Extract(texture, 3 + 30, 11, path, "Back/square_1.png");
        Extract(texture, 3 + 30, 13, path, "Back/square_2.png");
        Extract(texture, 3 + 30, 15, path, "Back/square_3.png");

        GetInterior(
            texture, 31, 17,
            sizeOfInteriorForBackTiles.x,
            sizeOfInteriorForBackTiles.y,
            path,
            "Back/interior_"
        );

        // outside

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + "/Outside", 0.6f
        );

        GetSquare(texture, 38, 2, path, "Outside");
        Extract(
            texture,
            38, 38 + sizeOfInteriorForOutsideTiles.x - 1,
            9,  9 + sizeOfInteriorForOutsideTiles.y - 1,
            path,
            "Outside/interior.png"
        );
        GetInterior(
            texture, 38, 9,
            sizeOfInteriorForBackTiles.x,
            sizeOfInteriorForBackTiles.y,
            path,
            "Outside/interior_"
        );

        {
            var outsideTexture1x4 = new [] {
                ExtractColors(texture, 38, 38, 9, 9),
                ExtractColors(texture, 38, 38, 10, 10),
                ExtractColors(texture, 38, 38, 11, 11),
                ExtractColors(texture, 38, 38, 12, 12)
            };
            OutsideFix(path, "Normal/curvetl3plus1_outside.png", outsideTexture1x4, false);
            OutsideFix(path, "Normal/curvetr3plus1_outside.png", outsideTexture1x4, true);
            OutsideFix(path, "Normal/curvebl3plus1_outside.png", outsideTexture1x4, false);
            OutsideFix(path, "Normal/curvebr3plus1_outside.png", outsideTexture1x4, true);
        }

        // apply accumulated pivot information

        EditorUtility.DisplayProgressBar(
            "Creating tilesets", path + " (pivots)", 0.8f
        );

        ApplyPivots(path);

        // if you're seeing phantom compiler errors below, go into
        // Assembly-CSharp-Editor.csproj and change the reference to
        // Assembly-CSharp.csproj so that <ReferenceOutputAssembly> is true, not false.
        // https://forum.unity.com/threads/2019-3-12f1-build-errors.880312/

        // create tilesets

        CreateTileset(
            targetAssetPath,
            themeName,
            "Normal",
            Tileset.TilesetType.Normal,
            sizeOfInteriorForNormalTiles.x,
            sizeOfInteriorForNormalTiles.y
        );
        CreateTileset(
            targetAssetPath,
            themeName,
            "Secondary",
            Tileset.TilesetType.Dirt,
            sizeOfInteriorForSecondaryTiles.x,
            sizeOfInteriorForSecondaryTiles.y
        );
        CreateTileset(
            targetAssetPath,
            themeName,
            "Back",
            Tileset.TilesetType.Back,
            sizeOfInteriorForBackTiles.x,
            sizeOfInteriorForBackTiles.y
        );
        CreateTileset(
            targetAssetPath,
            themeName,
            "Outside",
            Tileset.TilesetType.Outside,
            sizeOfInteriorForOutsideTiles.x,
            sizeOfInteriorForOutsideTiles.y
        );

        AssetDatabase.Refresh();

        ApplyPivots(path);
        AssetDatabase.Refresh();
        pivotsToSet = null;

        EditorUtility.ClearProgressBar();
    }

    private static void GetSquare(
        Texture2D texture, int tx, int ty, string tileAssetPath, string subfolder
    )
    {
        Extract(texture, tx + 0, ty + 0, tileAssetPath, subfolder + "/square_tl.png");
        Extract(texture, tx + 1, ty + 0, tileAssetPath, subfolder + "/square_t1.png");
        Extract(texture, tx + 2, ty + 0, tileAssetPath, subfolder + "/square_t2.png");
        Extract(texture, tx + 3, ty + 0, tileAssetPath, subfolder + "/square_t3.png");
        Extract(texture, tx + 4, ty + 0, tileAssetPath, subfolder + "/square_t4.png");
        Extract(texture, tx + 5, ty + 0, tileAssetPath, subfolder + "/square_tr.png");
        Extract(texture, tx + 5, ty + 1, tileAssetPath, subfolder + "/square_r1.png");
        Extract(texture, tx + 5, ty + 2, tileAssetPath, subfolder + "/square_r2.png");
        Extract(texture, tx + 5, ty + 3, tileAssetPath, subfolder + "/square_r3.png");
        Extract(texture, tx + 5, ty + 4, tileAssetPath, subfolder + "/square_r4.png");
        Extract(texture, tx + 5, ty + 5, tileAssetPath, subfolder + "/square_br.png");
        Extract(texture, tx + 4, ty + 5, tileAssetPath, subfolder + "/square_b4.png");
        Extract(texture, tx + 3, ty + 5, tileAssetPath, subfolder + "/square_b3.png");
        Extract(texture, tx + 2, ty + 5, tileAssetPath, subfolder + "/square_b2.png");
        Extract(texture, tx + 1, ty + 5, tileAssetPath, subfolder + "/square_b1.png");
        Extract(texture, tx + 0, ty + 5, tileAssetPath, subfolder + "/square_bl.png");
        Extract(texture, tx + 0, ty + 4, tileAssetPath, subfolder + "/square_l4.png");
        Extract(texture, tx + 0, ty + 3, tileAssetPath, subfolder + "/square_l3.png");
        Extract(texture, tx + 0, ty + 2, tileAssetPath, subfolder + "/square_l2.png");
        Extract(texture, tx + 0, ty + 1, tileAssetPath, subfolder + "/square_l1.png");
        Extract(texture, tx + 2, ty + 2, tileAssetPath, subfolder + "/square_in_br.png");
        Extract(texture, tx + 3, ty + 2, tileAssetPath, subfolder + "/square_in_bl.png");
        Extract(texture, tx + 2, ty + 3, tileAssetPath, subfolder + "/square_in_tr.png");
        Extract(texture, tx + 3, ty + 3, tileAssetPath, subfolder + "/square_in_tl.png");
    }

    private static void GetInterior(
        Texture2D texture,
        int tx,
        int ty,
        int w,
        int h,
        string tileAssetPath,
        string filenamePrefix
    )
    {
        for (int x = 0; x < w; x += 1)
        {
            for (int y = 0; y < h; y += 1)
            {
                int n = 1 + x + (y * w);
                Extract(
                    texture, tx + x, ty + y, tileAssetPath, $"{filenamePrefix}{n}.png"
                );
            }
        }
    }

    private static void Extract(
        Texture2D texture,
        int tx,
        int ty,
        string tileAssetPath,
        string pathRelativeToTileFolder,
        SpriteAlignment alignment = SpriteAlignment.BottomLeft
    )
    {
        Extract(
            texture, tx, tx, ty, ty, tileAssetPath, pathRelativeToTileFolder, alignment
        );
    }

    private static void Extract(
        Texture2D texture,
        int txLeft,
        int txRight,
        int tyBottom,
        int tyTop,
        string tileAssetPath,
        string pathRelativeToTileFolder,
        SpriteAlignment alignment = SpriteAlignment.BottomLeft
    )
    {
        tyBottom = (texture.height / 70) - 1 - tyBottom;
        tyTop =    (texture.height / 70) - 1 - tyTop;
        int width  = 70 * (1 + txRight - txLeft);
        int height = 70 * (1 + tyBottom - tyTop);
        var pixels = texture.GetPixels(70 * txLeft, 70 * tyTop, width, height);
        var result = new Texture2D(width, height, texture.format, 0, false);
        result.SetPixels(0, 0, width, height, pixels);

        byte[] bytes = result.EncodeToPNG();
        string filename = Path.Combine(
            Application.dataPath, tileAssetPath, pathRelativeToTileFolder
        );
        File.WriteAllBytes(filename, bytes);

        pivotsToSet[Path.Combine("Assets", tileAssetPath, pathRelativeToTileFolder)] =
            alignment;
    }

    private static Color[] ExtractColors(
        Texture2D texture, 
        int txLeft,
        int txRight,
        int tyBottom,
        int tyTop
    )
    {
        tyBottom = (texture.height / 70) - 1 - tyBottom;
        tyTop =    (texture.height / 70) - 1 - tyTop;
        int width  = 70 * (1 + txRight - txLeft);
        int height = 70 * (1 + tyBottom - tyTop);
        var pixels = texture.GetPixels(70 * txLeft, 70 * tyTop, width, height);
        return pixels;
    }

    private static void OutsideFix(
        string tileFolder,
        string pathRelativeToTileFolder,
        Color[][] texture1x4FromOutsideTiles,
        bool rhs
    )
    {
        string absoluteFilename = Path.Combine(
            Application.dataPath, tileFolder, pathRelativeToTileFolder
        );
        var bytes = File.ReadAllBytes(absoluteFilename);
        var texture = new Texture2D(2, 2);
        texture.LoadImage(bytes);

        var outsideTiles = new List<Texture2D>();
        for (int n = 1; n <= 4; n += 1)
        {
            string outsideRelativeFilename = $"Outside/square_{(rhs ? "l" : "r")}{n}.png";
            var outsideBytes = File.ReadAllBytes(Path.Combine(
                Application.dataPath, tileFolder, outsideRelativeFilename
            ));
            var outsideTexture = new Texture2D(2, 2);
            outsideTexture.LoadImage(outsideBytes);
            outsideTiles.Add(outsideTexture);
        }

        Color[] MergeTileArt(Color[] top, Color[] btm)
        {
            var result = new Color[top.Length];
            for (int n = 0; n < top.Length; n += 1)
            {
                result[n] = Color.Lerp(
                    btm[n], new Color(top[n].r, top[n].g, top[n].b, 1.0f), top[n].a
                );
            }
            return result;
        }

        int px = (rhs ? 210 : 0);
        for (int ty = 0; ty < 4; ty += 1)
        {
            var outsideTexture = texture1x4FromOutsideTiles[3 - ty];
            var outsideTile = outsideTiles[ty].GetPixels(0, 0, 70, 70);
            var merged = MergeTileArt(outsideTile, outsideTexture);
            texture.SetPixels(px, ty * 70, 70, 70, merged);
        }
        
        bytes = texture.EncodeToPNG();
        File.WriteAllBytes(absoluteFilename, bytes);
    }

    private static void ApplyPivots(string tileFolder)
    {
        AssetDatabase.Refresh();

        foreach (var kv in pivotsToSet)
        {
            string path = kv.Key;
            TextureImporter ti = (TextureImporter)AssetImporter.GetAtPath(path);
            if (ti == null)
            {
                Debug.Log("error texture import " + path);
                continue;
            }

            var texSettings = new TextureImporterSettings();
            ti.ReadTextureSettings(texSettings);
            bool changed = false;

            if (texSettings.spritePivot != Vector2.zero)
            {
                texSettings.spritePivot = Vector2.zero;
                changed = true;
            }
            if (texSettings.spriteAlignment != (int)kv.Value)
            {
                texSettings.spriteAlignment = (int)kv.Value;
                changed = true;
            }
            if (texSettings.spritePixelsPerUnit != 70)
            {
                texSettings.spritePixelsPerUnit = 70;
                changed = true;
            }

            if (changed == true || changed == false)
            {
                ti.SetTextureSettings(texSettings);
                // AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            }
        }
        AssetDatabase.ImportAsset(
            Path.Combine("Assets", tileFolder),
            ImportAssetOptions.ForceUpdate |
                ImportAssetOptions.ForceSynchronousImport |
                ImportAssetOptions.ImportRecursive
        );
    }

    private static void CreateTileset(
        string targetAssetPath,
        string themeName,
        string tilesetName,
        Tileset.TilesetType type,
        int interiorWidth,
        int interiorHeight
    )
    {
        var prefix = $"Assets/{targetAssetPath}/{tilesetName}/";

        Tileset tileset = (Tileset)ScriptableObject.CreateInstance("Tileset");
        tileset.type = type;

        Sprite[] LoadMultiple(string searchPrefix)
        {
            var result = new List<Sprite>();
            for (int n = 1; n <= 5; n += 1)
            {
                var sprite =
                    AssetDatabase.LoadAssetAtPath<Sprite>(searchPrefix + $"{n}.png");
                if (sprite != null)
                    result.Add(sprite);
            }
            return result.ToArray();
        }

        tileset.singleSquare = LoadMultiple(prefix + "square_");

        var interiorSprites = new List<Sprite>();
        for (int n = 1; n <= interiorWidth * interiorHeight; n += 1)
        {
            interiorSprites.Add(
                AssetDatabase.LoadAssetAtPath<Sprite>(prefix + $"interior_{n}.png")
            );
        }
        if (interiorSprites.Count > 0)
        {
            tileset.interior = interiorSprites.ToArray();
            tileset.interiorStyle = Tileset.InteriorStyle.Grid;
            tileset.interiorGridColumns = interiorWidth;
            tileset.interiorGridRows    = interiorHeight;
        }
        else
        {
            tileset.interior = interiorSprites.ToArray();
            tileset.interiorStyle = Tileset.InteriorStyle.None;
            tileset.interiorGridColumns = 0;
            tileset.interiorGridRows    = 0;
        }

        tileset.left = new Sprite[] {
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_l1.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_l2.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_l3.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_l4.png")
        };
        tileset.right = new Sprite[] {
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_r1.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_r2.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_r3.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_r4.png")
        };
        tileset.bottom = new Sprite[] {
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_b1.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_b2.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_b3.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_b4.png")
        };
        tileset.top = new Sprite[] {
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_t1.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_t2.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_t3.png"),
            AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_t4.png")
        };
        tileset.bottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_bl.png");
        tileset.bottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_br.png");
        tileset.topLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_tl.png");
        tileset.topRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_tr.png");
        tileset.insideBottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_in_bl.png");
        tileset.insideBottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_in_br.png");
        tileset.insideTopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_in_tl.png");
        tileset.insideTopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "square_in_tr.png");

        tileset.slope22BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22bl.png");
        tileset.slope22BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22br.png");
        tileset.slope22TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tl.png");
        tileset.slope22TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tr.png");
        tileset.slope45BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45bl.png");
        tileset.slope45BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45br.png");
        tileset.slope45TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tl.png");
        tileset.slope45TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tr.png");
        tileset.slopeJoined22BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22bl_joined.png");
        tileset.slopeJoined22BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22br_joined.png");
        tileset.slopeJoined22TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tl_joined.png");
        tileset.slopeJoined22TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tr_joined.png");
        tileset.slopeJoined45BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45bl_joined.png");
        tileset.slopeJoined45BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45br_joined.png");
        tileset.slopeJoined45TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tl_joined.png");
        tileset.slopeJoined45TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tr_joined.png");
        tileset.slopeJoinedLeft22BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22bl_joinedleft.png");
        tileset.slopeJoinedLeft22BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22br_joinedleft.png");
        tileset.slopeJoinedLeft22TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tl_joinedleft.png");
        tileset.slopeJoinedLeft22TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tr_joinedleft.png");
        tileset.slopeJoinedLeft45BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45bl_joinedleft.png");
        tileset.slopeJoinedLeft45BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45br_joinedleft.png");
        tileset.slopeJoinedLeft45TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tl_joinedleft.png");
        tileset.slopeJoinedLeft45TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tr_joinedleft.png");
        tileset.slopeJoinedRight22BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22bl_joinedright.png");
        tileset.slopeJoinedRight22BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22br_joinedright.png");
        tileset.slopeJoinedRight22TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tl_joinedright.png");
        tileset.slopeJoinedRight22TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "22tr_joinedright.png");
        tileset.slopeJoinedRight45BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45bl_joinedright.png");
        tileset.slopeJoinedRight45BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45br_joinedright.png");
        tileset.slopeJoinedRight45TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tl_joinedright.png");
        tileset.slopeJoinedRight45TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "45tr_joinedright.png");

        tileset.curve3Plus1BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvebl3plus1.png");
        tileset.curve3Plus1BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvebr3plus1.png");
        tileset.curve3Plus1TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvetl3plus1.png");
        tileset.curve3Plus1TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvetr3plus1.png");
        tileset.curve3Plus1BottomLeftWithOutside  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvebl3plus1_outside.png");
        tileset.curve3Plus1BottomRightWithOutside = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvebr3plus1_outside.png");
        tileset.curve3Plus1TopLeftWithOutside     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvetl3plus1_outside.png");
        tileset.curve3Plus1TopRightWithOutside    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curvetr3plus1_outside.png");
        tileset.curveOut3Plus1BottomLeft  = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curveoutbl3.png");
        tileset.curveOut3Plus1BottomRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curveoutbr3.png");
        tileset.curveOut3Plus1TopLeft     = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curveouttl3.png");
        tileset.curveOut3Plus1TopRight    = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "curveouttr3.png");

        tileset.singleRowLeft = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "row_left.png");
        tileset.singleRowMiddle = LoadMultiple(prefix + "row_center_");
        tileset.singleRowRight = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "row_right.png");

        tileset.pillar1WideTop = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "pillar_top.png");
        tileset.pillar1WideMiddle = LoadMultiple(prefix + "pillar_middle_");
        tileset.pillar1WideBottom = AssetDatabase.LoadAssetAtPath<Sprite>(prefix + "pillar_bottom.png");

        AssetDatabase.CreateAsset(
            tileset, $"Assets/{targetAssetPath}/Tileset{themeName}{tilesetName}.asset"
        );
    }
}
