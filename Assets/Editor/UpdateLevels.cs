using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

public class UpdateLevels
{
    private static (string nameOnDropbox, string nameInProject)[] Sets = new [] {
        (nameOnDropbox:"Chunks set 1",   nameInProject:"Set 1"),
        (nameOnDropbox:"Chunks set 2",   nameInProject:"Set 2"),
        (nameOnDropbox:"Chunks set 3",   nameInProject:"Set 3"),
        (nameOnDropbox:"Chunks set 4",   nameInProject:"Set 4"),
        (nameOnDropbox:"Chunks set 5",   nameInProject:"Set 5"),
        (nameOnDropbox:"Chunks set 6",   nameInProject:"Set 6"),
        (nameOnDropbox:"Chunks set 7",   nameInProject:"Set 7"),
    };
    private static (string nameOnDropbox, string nameInProject)[] Themes = new [] {
        (nameOnDropbox: "Generic",              nameInProject: "Generic"),
        (nameOnDropbox: "Rainy Ruins",          nameInProject: "Rainy Ruins"),
        (nameOnDropbox: "Capital Highway",      nameInProject: "Capital Highway"),
        (nameOnDropbox: "Conflict Canyon",      nameInProject: "Conflict Canyon"),
        (nameOnDropbox: "Hot n Cold Springs",   nameInProject: "Hot N Cold Springs"),
        (nameOnDropbox: "Gravity Galaxy",       nameInProject: "Gravity Galaxy"),
        (nameOnDropbox: "Sunken Island",        nameInProject: "Sunken Island"),
        (nameOnDropbox: "Treasure Mines",       nameInProject: "Treasure Mines"),
        (nameOnDropbox: "Windy Skies",          nameInProject: "Windy Skies"),
        (nameOnDropbox: "Tombstone Hill",       nameInProject: "Tombstone Hill"),
        (nameOnDropbox: "Molten Fortress",      nameInProject: "Molten Fortress"),
    };
    private static string[] Difficulties = new [] {
        "Tutorial", "Easy", "Medium", "Hard"
    };
    private static string[] Creators = new [] {
        "Alvaro", "Kenny", "Kiavik", "Markus", "Mat", "Owen", "Gabriel", "Marcin"
    };

    [MenuItem("Super Leap Day/Update Levels")]
    public static void UpdateLevelsFromDropox()
    {
        string dropboxRoot = EditorUtility.OpenFolderPanel(
            "Navigate to Dropbox 'Levels' folder", "", ""
        );

        if (dropboxRoot == null || dropboxRoot == "")
            return;

        if (Directory.Exists(dropboxRoot) == false)
        {
            Debug.LogError("Aborting: main folder doesn't exist");
            return;
        }

        foreach (var set in Sets)
        {
            if (Directory.Exists(Path.Combine(dropboxRoot, set.nameOnDropbox)) == false)
            {
                Debug.LogError(
                    "Aborting: selected folder should contain " +
                    $"'{set.nameOnDropbox}' folder"
                );
                return;
            }
        }

        string projectRoot = Path.Combine(Application.dataPath, "Resources", "Levels");

        var library = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
            "Assets/Resources/Chunk Library.asset"
        );

        CopyFilesIn(dropboxRoot, projectRoot, library);
        UpdateLibraryMainLists(projectRoot, library);

        foreach (var pack in library.reviewPacks)
        {
            bool DoesChunkExist(ChunkLibrary.ReviewPack.ChunkEntry chunk) =>
                File.Exists(Path.Combine(projectRoot, chunk.filename) + ".bytes");

            pack.chunks = pack.chunks.Where(DoesChunkExist).ToArray();
        }

        EditorUtility.SetDirty(library);
        AssetDatabase.SaveAssets();
    }

    [MenuItem("Super Leap Day/Create Level Review Packs")]
    public static void CreateLevelReviewPacksFromDropbox()
    {
        string projectRoot = Path.Combine(Application.dataPath, "Resources", "Levels");

        var library = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
            "Assets/Resources/Chunk Library.asset"
        );
        UpdateLibraryReviewPacks(projectRoot, library);
        EditorUtility.SetDirty(library);
        AssetDatabase.SaveAssets();
    }

    [MenuItem("Super Leap Day/Prune Level Review Packs")]
    public static void PruneLevelReviewPacks()
    {
        string projectRoot = Path.Combine(Application.dataPath, "Resources", "Levels");
        var library = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
            "Assets/Resources/Chunk Library.asset"
        );

        foreach (var pack in library.reviewPacks)
        {
            bool DoesChunkExist(ChunkLibrary.ReviewPack.ChunkEntry chunk) =>
                File.Exists(Path.Combine(projectRoot, chunk.filename) + ".bytes");

            pack.chunks = pack.chunks.Where(DoesChunkExist).ToArray();
        }

        EditorUtility.SetDirty(library);
        AssetDatabase.SaveAssets();
    }

    [MenuItem("Super Leap Day/Mark Packs Reviewed")]
    public static void MarkPacksReviewed()
    {
        var library = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
            "Assets/Resources/Chunk Library.asset"
        );

        var reviewed = library.chunksReviewed.ToList();
        var packs = library.reviewPacks.ToList();

        for (int n = 0; n < packs.Count; n += 1)
        {
            var reviewPack = library.reviewPacks[n];
            reviewed.AddRange(reviewPack.chunks);
        }
        packs.Clear();

        library.chunksReviewed = reviewed.ToArray();
        library.reviewPacks = packs.ToArray();

        EditorUtility.SetDirty(library);
        AssetDatabase.SaveAssets();
    }

    private static void CopyFilesIn(
        string dropboxRoot, string projectRoot, ChunkLibrary library
    )
    {
        var folders = new List<(string dropboxPath, string projectPath)>();

        foreach (var creator in Creators)
        {
            folders.Add((
                dropboxPath: Path.Combine(dropboxRoot, "Checkpoint", creator),
                projectPath: Path.Combine(projectRoot, "Checkpoint", creator)
            ));
        }

        folders.Add((
            dropboxPath: Path.Combine(dropboxRoot, "Chest"),
            projectPath: Path.Combine(projectRoot, "Chest")
        ));

        foreach (var set in Sets)
        {
            foreach (var theme in Themes)
            {
                foreach (var creator in Creators)
                {
                    foreach (var difficulty in Difficulties)
                    {
                        var dropboxPath = Path.Combine(dropboxRoot, set.nameOnDropbox, theme.nameOnDropbox, creator, difficulty);
                        var projectPath = Path.Combine(projectRoot, set.nameInProject, theme.nameInProject, creator, difficulty);
                        folders.Add((dropboxPath: dropboxPath, projectPath: projectPath));
                    }
                }
            }
        }

        var filesNeedingDeprecationOrDeletion = new List<string>();

        for (int n = 0; n < folders.Count; n += 1)
        {
            var folder = folders[n];

            {
                string from = RelativePath(dropboxRoot, folder.dropboxPath);
                string to   = RelativePath(projectRoot, folder.projectPath);
                EditorUtility.DisplayProgressBar(
                    "Updating levels",
                    $"{from} -> {to}",
                    (float)n / (float)folders.Count
                );
            }

            if (Directory.Exists(folder.dropboxPath) == false)
            {
                if (Directory.Exists(folder.projectPath) == true)
                {
                    Debug.Log("Deprecated folder: " + folder.projectPath);
            //         Directory.Delete(folder.projectPath, true);
            //         File.Delete(folder.projectPath + ".meta");
                }
                continue;
            }

            if (Directory.Exists(folder.projectPath) == false)
            {
                Directory.CreateDirectory(folder.projectPath);
            }

            foreach (var file in Directory.GetFiles(folder.projectPath, "*.bytes"))
            {
                var dropboxFilename = Path.Combine(
                    folder.dropboxPath,
                    Path.GetFileName(file)
                );
                if (File.Exists(dropboxFilename) == false)
                {
                    var relative = RelativePath(projectRoot, file);
                    if (relative.EndsWith(".bytes"))
                        relative = relative.Replace(".bytes", "");
                    relative = relative.Replace('\\', '/');

                    if (library.deprecatedFiles.Contains(relative) == false)
                        filesNeedingDeprecationOrDeletion.Add(relative);
                }
            }

            foreach (var file in Directory.GetFiles(folder.dropboxPath, "*.bytes"))
            {
                var projectFilename = Path.Combine(
                    folder.projectPath, Path.GetFileName(file)
                );
                File.Delete(projectFilename);
                File.Copy(file, projectFilename);

                var relative = RelativePath(dropboxRoot, file);
                if (relative.EndsWith(".bytes"))
                    relative = relative.Replace(".bytes", "");
                relative = relative.Replace('\\', '/');
                if (library.deprecatedFiles.Contains(relative))
                {
                    MarkFileNotDeprecated(relative, library);
                    Debug.LogWarning(
                        $"<b><color=yellow>File that was previously marked deprecated " +
                            "now exists again.</color></b> This means that any " +
                            "old days that had this chunk baked in, " +
                            $"will now use the new chunk.\n\nFilename = {relative}\n"
                    );
                }
            }

            if (Directory.Exists(folder.projectPath) == true &&
                Directory.GetFiles(folder.projectPath, "*.bytes").Length == 0)
            {
                Debug.Log("Deprecated folder: " + folder.projectPath);
                // Directory.Delete(folder.projectPath);
                // File.Delete(folder.projectPath + ".meta");
            }
        }

        if (filesNeedingDeprecationOrDeletion.Count > 0)
        {
            var result = EditorUtility.DisplayDialogComplex(
                "What to do with newly missing chunks?",
                $"{filesNeedingDeprecationOrDeletion.Count} chunk(s) are in the " +
                    "project folder but no longer on dropbox.\n\n" +
                    "If they are baked into previous days, deprecate them. " +
                    "This means that they will still be in the project so previous " +
                    "days will still work, but they won't be randomly selected " +
                    "by the algorithm anymore.\n\n" +
                    "If the chunks have never been baked into a day, delete them.\n\n" +
                    new string('-', 120) + "\n\n-   " +
                    string.Join("\n-   ", filesNeedingDeprecationOrDeletion),
                "Deprecate",
                "Cancel",
                "Delete in project"
            );
            if (result == 0 /* deprecate */)
            {
                foreach (var file in filesNeedingDeprecationOrDeletion)
                {
                    MarkFileDeprecated(file, library);
                }

                Debug.LogWarning(
                    $"<b><color=yellow>{filesNeedingDeprecationOrDeletion.Count} " +
                        "chunk(s) marked deprecated.</color></b> " +
                        "They won't be included in the algorithm anymore.\n\n-   " +
                        string.Join("\n-   ", filesNeedingDeprecationOrDeletion) + "\n"
                );
            }
            else if (result == 2 /* delete in project */)
            {
                foreach (var file in filesNeedingDeprecationOrDeletion)
                {
                    var absolutePath = Path.Combine(projectRoot, file + ".bytes");
                    File.Delete(absolutePath);
                    File.Delete(absolutePath + ".meta");
                }

                Debug.LogWarning(
                    $"<b><color=#ff8000>{filesNeedingDeprecationOrDeletion.Count} " +
                        "chunks(s) deleted to match what's on dropbox.</color></b> " +
                        "As long as they aren't baked into previous days, " +
                        "this is fine.\n\n-   " +
                        string.Join("\n-   ", filesNeedingDeprecationOrDeletion) + "\n"
                );
            }
        }

        EditorUtility.ClearProgressBar();
        EditorUtility.SetDirty(library);
        AssetDatabase.Refresh();
    }

    private static void MarkFileDeprecated(string relativeFilename, ChunkLibrary library)
    {
        if (library.deprecatedFiles.Contains(relativeFilename) == false)
        {
            var deprecated = library.deprecatedFiles.ToList();
            deprecated.Add(relativeFilename);
            library.deprecatedFiles = deprecated.ToArray();
        }
    }

    private static void MarkFileNotDeprecated(
        string relativeFilename, ChunkLibrary library
    )
    {
        if (library.deprecatedFiles.Contains(relativeFilename) == true)
        {
            var deprecated = library.deprecatedFiles.ToList();
            deprecated.Remove(relativeFilename);
            library.deprecatedFiles = deprecated.ToArray();
        }
    }

    private static void UpdateLibraryMainLists(string projectRoot, ChunkLibrary library)
    {
        library.checkpoint              = FindCheckpointChunks();
        library.chest                   = FindChestChunks();
        library.genericTutorial         = FindChunks("Generic", "Tutorial");
        library.genericEasy             = FindChunks("Generic", "Easy");
        library.genericMedium           = FindChunks("Generic", "Medium");
        library.genericHard             = FindChunks("Generic", "Hard");
        library.rainyRuinsTutorial      = FindChunks("Rainy Ruins", "Tutorial");
        library.rainyRuinsEasy          = FindChunks("Rainy Ruins", "Easy");
        library.rainyRuinsMedium        = FindChunks("Rainy Ruins", "Medium");
        library.rainyRuinsHard          = FindChunks("Rainy Ruins", "Hard");
        library.capitalHighwayTutorial  = FindChunks("Capital Highway", "Tutorial");
        library.capitalHighwayEasy      = FindChunks("Capital Highway", "Easy");
        library.capitalHighwayMedium    = FindChunks("Capital Highway", "Medium");
        library.capitalHighwayHard      = FindChunks("Capital Highway", "Hard");
        library.conflictCanyonTutorial  = FindChunks("Conflict Canyon", "Tutorial");
        library.conflictCanyonEasy      = FindChunks("Conflict Canyon", "Easy");
        library.conflictCanyonMedium    = FindChunks("Conflict Canyon", "Medium");
        library.conflictCanyonHard      = FindChunks("Conflict Canyon", "Hard");
        library.hotNColdSpringsTutorial = FindChunks("Hot N Cold Springs", "Tutorial");
        library.hotNColdSpringsEasy     = FindChunks("Hot N Cold Springs", "Easy");
        library.hotNColdSpringsMedium   = FindChunks("Hot N Cold Springs", "Medium");
        library.hotNColdSpringsHard     = FindChunks("Hot N Cold Springs", "Hard");
        library.gravityGalaxyTutorial   = FindChunks("Gravity Galaxy", "Tutorial");
        library.gravityGalaxyEasy       = FindChunks("Gravity Galaxy", "Easy");
        library.gravityGalaxyMedium     = FindChunks("Gravity Galaxy", "Medium");
        library.gravityGalaxyHard       = FindChunks("Gravity Galaxy", "Hard");
        library.sunkenIslandTutorial    = FindChunks("Sunken Island", "Tutorial");
        library.sunkenIslandEasy        = FindChunks("Sunken Island", "Easy");
        library.sunkenIslandMedium      = FindChunks("Sunken Island", "Medium");
        library.sunkenIslandHard        = FindChunks("Sunken Island", "Hard");
        library.treasureMinesTutorial   = FindChunks("Treasure Mines", "Tutorial");
        library.treasureMinesEasy       = FindChunks("Treasure Mines", "Easy");
        library.treasureMinesMedium     = FindChunks("Treasure Mines", "Medium");
        library.treasureMinesHard       = FindChunks("Treasure Mines", "Hard");
        library.windySkiesTutorial      = FindChunks("Windy Skies", "Tutorial");
        library.windySkiesEasy          = FindChunks("Windy Skies", "Easy");
        library.windySkiesMedium        = FindChunks("Windy Skies", "Medium");
        library.windySkiesHard          = FindChunks("Windy Skies", "Hard");
        library.tombstoneHillTutorial   = FindChunks("Tombstone Hill", "Tutorial");
        library.tombstoneHillEasy       = FindChunks("Tombstone Hill", "Easy");
        library.tombstoneHillMedium     = FindChunks("Tombstone Hill", "Medium");
        library.tombstoneHillHard       = FindChunks("Tombstone Hill", "Hard");
        library.moltenFortressTutorial  = FindChunks("Molten Fortress", "Tutorial");
        library.moltenFortressEasy      = FindChunks("Molten Fortress", "Easy");
        library.moltenFortressMedium    = FindChunks("Molten Fortress", "Medium");
        library.moltenFortressHard      = FindChunks("Molten Fortress", "Hard");

        ChunkLibrary.ChunkData DataForChunk(string relativePath)
        {
            var assetPath = "Assets/Resources/Levels/" + relativePath + ".bytes";
            var content = AssetDatabase.LoadAssetAtPath<TextAsset>(assetPath).bytes;
            var level = NitromeEditor.Level.UnserializeFromBytes(content);
            var data = new ChunkLibrary.ChunkData { filename = relativePath };
            ChunkLibrary.FillChunkData(data, level);
            return data;
        }

        ChunkLibrary.ChunkData[] FindCheckpointChunks()
        {
            var chunks = new List<ChunkLibrary.ChunkData>();
            foreach (var creator in Creators)
            {
                var folder = Path.Combine(projectRoot, "Checkpoint", creator);
                if (Directory.Exists(folder) == false) continue;
                foreach (var file in Directory.GetFiles(folder, "*.bytes"))
                {
                    var relative = RelativePath(projectRoot, file);
                    if (relative.EndsWith(".bytes"))
                        relative = relative.Replace(".bytes", "");
                    relative = relative.Replace('\\', '/');

                    if (library.deprecatedFiles.Contains(relative))
                        continue;

                    chunks.Add(DataForChunk(relative));
                }
            }
            return chunks.OrderBy(c => c.filename).ToArray();
        }

        ChunkLibrary.ChunkData[] FindChestChunks()
        {
            var chunks = new List<ChunkLibrary.ChunkData>();
            var folder = Path.Combine(projectRoot, "Chest");
            if (Directory.Exists(folder) == false) return chunks.ToArray();
            
            foreach (var file in Directory.GetFiles(folder, "*.bytes"))
            {
                var relative = RelativePath(projectRoot, file);
                if (relative.EndsWith(".bytes"))
                    relative = relative.Replace(".bytes", "");
                relative = relative.Replace('\\', '/');

                if (library.deprecatedFiles.Contains(relative))
                    continue;

                chunks.Add(DataForChunk(relative));
            }
            return chunks.OrderBy(c => c.filename).ToArray();
        }

        ChunkLibrary.ChunkData[] FindChunks(string theme, string difficulty)
        {
            var chunks = new List<ChunkLibrary.ChunkData>();
            foreach (var set in Sets)
            {
                foreach (var creator in Creators)
                {
                    var folder = Path.Combine(
                        projectRoot, set.nameInProject, theme, creator, difficulty
                    );
                    if (Directory.Exists(folder) == false) continue;
                    foreach (var file in Directory.GetFiles(folder, "*.bytes"))
                    {
                        var relative = RelativePath(projectRoot, file);
                        if (relative.EndsWith(".bytes"))
                            relative = relative.Replace(".bytes", "");
                        relative = relative.Replace('\\', '/');

                        if (library.deprecatedFiles.Contains(relative))
                            continue;

                        chunks.Add(DataForChunk(relative));
                    }
                }
            }
            return chunks.OrderBy(c => c.filename).ToArray();
        }
    }

    private static void UpdateLibraryReviewPacks(string projectRoot, ChunkLibrary library)
    {
        EditorUtility.DisplayProgressBar(
            "Updating Review Packs", "Collecting files", 0.1f
        );
        var allChunksWithTheme = new List<(Theme? theme, string filename)>();
        var allChunksSet = new HashSet<string>();
        void AddChunks(Theme? theme, ChunkLibrary.ChunkData[] chunks)
        {
            foreach (var c in chunks)
            {
                allChunksWithTheme.Add((theme, c.filename));
                allChunksSet.Add(c.filename);
            }
        }
        AddChunks(null, library.genericTutorial);
        AddChunks(null, library.genericEasy);
        AddChunks(null, library.genericMedium);
        AddChunks(null, library.genericHard);
        AddChunks(Theme.RainyRuins, library.rainyRuinsTutorial);
        AddChunks(Theme.RainyRuins, library.rainyRuinsEasy);
        AddChunks(Theme.RainyRuins, library.rainyRuinsMedium);
        AddChunks(Theme.RainyRuins, library.rainyRuinsHard);
        AddChunks(Theme.CapitalHighway, library.capitalHighwayTutorial);
        AddChunks(Theme.CapitalHighway, library.capitalHighwayEasy);
        AddChunks(Theme.CapitalHighway, library.capitalHighwayMedium);
        AddChunks(Theme.CapitalHighway, library.capitalHighwayHard);
        AddChunks(Theme.ConflictCanyon, library.conflictCanyonTutorial);
        AddChunks(Theme.ConflictCanyon, library.conflictCanyonEasy);
        AddChunks(Theme.ConflictCanyon, library.conflictCanyonMedium);
        AddChunks(Theme.ConflictCanyon, library.conflictCanyonHard);
        AddChunks(Theme.HotNColdSprings, library.hotNColdSpringsTutorial);
        AddChunks(Theme.HotNColdSprings, library.hotNColdSpringsEasy);
        AddChunks(Theme.HotNColdSprings, library.hotNColdSpringsMedium);
        AddChunks(Theme.HotNColdSprings, library.hotNColdSpringsHard);
        AddChunks(Theme.GravityGalaxy, library.gravityGalaxyTutorial);
        AddChunks(Theme.GravityGalaxy, library.gravityGalaxyEasy);
        AddChunks(Theme.GravityGalaxy, library.gravityGalaxyMedium);
        AddChunks(Theme.GravityGalaxy, library.gravityGalaxyHard);
        AddChunks(Theme.SunkenIsland, library.sunkenIslandTutorial);
        AddChunks(Theme.SunkenIsland, library.sunkenIslandEasy);
        AddChunks(Theme.SunkenIsland, library.sunkenIslandMedium);
        AddChunks(Theme.SunkenIsland, library.sunkenIslandHard);
        AddChunks(Theme.TreasureMines, library.treasureMinesTutorial);
        AddChunks(Theme.TreasureMines, library.treasureMinesEasy);
        AddChunks(Theme.TreasureMines, library.treasureMinesMedium);
        AddChunks(Theme.TreasureMines, library.treasureMinesHard);
        AddChunks(Theme.WindySkies, library.windySkiesTutorial);
        AddChunks(Theme.WindySkies, library.windySkiesEasy);
        AddChunks(Theme.WindySkies, library.windySkiesMedium);
        AddChunks(Theme.WindySkies, library.windySkiesHard);
        AddChunks(Theme.TombstoneHill, library.tombstoneHillTutorial);
        AddChunks(Theme.TombstoneHill, library.tombstoneHillEasy);
        AddChunks(Theme.TombstoneHill, library.tombstoneHillMedium);
        AddChunks(Theme.TombstoneHill, library.tombstoneHillHard);
        AddChunks(Theme.MoltenFortress, library.moltenFortressTutorial);
        AddChunks(Theme.MoltenFortress, library.moltenFortressEasy);
        AddChunks(Theme.MoltenFortress, library.moltenFortressMedium);
        AddChunks(Theme.MoltenFortress, library.moltenFortressHard);
        
        // create md5 hashes of all files
        EditorUtility.DisplayProgressBar(
            "Updating Review Packs", "Hashing files", 0.3f
        );

        var previousMD5ForFile = new Dictionary<string, string>();
        foreach (var chunk in library.chunksReviewed)
        {
            previousMD5ForFile[chunk.filename] = chunk.md5;
        }
        foreach (var pack in library.reviewPacks)
        {
            foreach (var chunk in pack.chunks)
                previousMD5ForFile[chunk.filename] = chunk.md5;
        }

        var md5ForFile = new Dictionary<string, string>();
        var md5 = MD5.Create();
        foreach (var (theme, chunkName) in allChunksWithTheme)
        {
            var fullPath = Path.Combine(projectRoot, chunkName + ".bytes");
            var content = File.ReadAllBytes(fullPath);
            md5ForFile[chunkName] = StringifyHash(md5.ComputeHash(content));
        }

        // remove altered or removed chunks from old review packs
        EditorUtility.DisplayProgressBar(
            "Updating Review Packs", "Checking old packs for changed/deleted chunks", 0.5f
        );

        {
            var list = library.chunksReviewed.ToList();
            for (int n = library.chunksReviewed.Length - 1; n >= 0; n -= 1)
            {
                var chunk = library.chunksReviewed[n];
                if (allChunksSet.Contains(chunk.filename) == false ||
                    md5ForFile[chunk.filename] != chunk.md5)
                {
                    list.RemoveAt(n);
                }
            }
            library.chunksReviewed = list.ToArray();
        }
        foreach (var oldPack in library.reviewPacks)
        {
            var list = oldPack.chunks.ToList();
            for (int n = list.Count - 1; n >= 0; n -= 1)
            {
                var chunk = oldPack.chunks[n];
                if (allChunksSet.Contains(chunk.filename) == false ||
                    md5ForFile[chunk.filename] != chunk.md5)
                {
                    list.RemoveAt(n);
                }
            }
            oldPack.chunks = list.ToArray();
        }

        // create new packs
        var reviewPacks = library.reviewPacks.ToList();
        var updatedChunksCategorized =
            new List<(Theme? theme, string packType, string sortKey, string filename)>();

        foreach (var file in allChunksWithTheme)
        {
            string creator =
                Creators.Where(c => file.filename.Contains(c)).FirstOrDefault();
            string packType = (file.theme?.ToString() ?? "Generic") + " " + creator;
            string sort =
                (file.theme != null ? (int)file.theme.Value + 1 : 0).ToString()
                + " " + creator;

            string hash = md5ForFile[file.filename];
            if (previousMD5ForFile.TryGetValue(file.filename, out var value))
            {
                if (value != hash)
                {
                    updatedChunksCategorized.Add(
                        (file.theme, packType, sort, file.filename)
                    );
                }
            }
            else
            {
                updatedChunksCategorized.Add(
                    (file.theme, packType, sort, file.filename)
                );
            }
        }

        updatedChunksCategorized =
            updatedChunksCategorized.OrderBy(item => item.sortKey).ToList();

        while (updatedChunksCategorized.Count > 0)
        {
            var packType = updatedChunksCategorized[0].packType;
            var theme = updatedChunksCategorized[0].theme;
            var chunks =
                updatedChunksCategorized
                    .Where(c => c.packType == packType).Take(10).ToArray();
            foreach (var chunk in chunks)
                updatedChunksCategorized.Remove(chunk);

            var pack = new ChunkLibrary.ReviewPack();
            pack.isGeneric = (theme == null);
            pack.specificToTheme = theme ?? (Theme)(-1);
            pack.chunks = chunks.Select(
                c => new ChunkLibrary.ReviewPack.ChunkEntry {
                    filename = c.filename, md5 = md5ForFile[c.filename]
                }
            ).ToArray();
            pack.name = $"pack #{library.nextReviewPackNumber++}: {packType}";
            reviewPacks.Add(pack);
        }

        library.reviewPacks = reviewPacks.ToArray();

        EditorUtility.ClearProgressBar();
    }
    
    private static string StringifyHash(byte[] hash)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var hashBytes in hash)
        {
            sb.Append(hashBytes.ToString("x2"));
        }
        return sb.ToString();
    }

    private static string RelativePath(string relativeTo, string path)
    {
        if (path.Substring(0, relativeTo.Length) != relativeTo)
            return path;

        string result = path.Substring(relativeTo.Length);
        if (result[0] == '\\' || result[0] == '/')
            result = result.Substring(1);
        return result;
    }
}
