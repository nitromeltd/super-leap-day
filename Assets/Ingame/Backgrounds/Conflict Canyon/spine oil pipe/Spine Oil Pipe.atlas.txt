
Spine Oil Pipe.png
size: 1083,595
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: true
  xy: 2, 2
  size: 591, 807
  orig: 598, 926
  offset: 7, 118
  index: -1
Layer 2
  rotate: true
  xy: 811, 40
  size: 328, 128
  orig: 328, 128
  offset: 0, 0
  index: -1
Layer 3
  rotate: false
  xy: 941, 198
  size: 135, 170
  orig: 135, 170
  offset: 0, 0
  index: -1
Layer 4
  rotate: true
  xy: 941, 14
  size: 182, 57
  orig: 182, 57
  offset: 0, 0
  index: -1
Layer 5
  rotate: false
  xy: 811, 17
  size: 57, 21
  orig: 57, 21
  offset: 0, 0
  index: -1
Layer 6
  rotate: false
  xy: 811, 370
  size: 270, 223
  orig: 270, 223
  offset: 0, 0
  index: -1
