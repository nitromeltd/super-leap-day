
Spine Space Miner.png
size: 665,265
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: true
  xy: 2, 2
  size: 261, 283
  orig: 261, 296
  offset: 0, 0
  index: -1
Layer 2
  rotate: true
  xy: 501, 37
  size: 107, 88
  orig: 107, 88
  offset: 0, 0
  index: -1
Layer 3
  rotate: false
  xy: 501, 146
  size: 124, 117
  orig: 124, 117
  offset: 0, 0
  index: -1
Layer 4
  rotate: false
  xy: 591, 56
  size: 72, 88
  orig: 72, 89
  offset: 0, 1
  index: -1
Layer 5
  rotate: false
  xy: 287, 23
  size: 212, 240
  orig: 212, 240
  offset: 0, 0
  index: -1
