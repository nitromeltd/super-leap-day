
Hippo Set 6.png
size: 85,76
format: RGBA8888
filter: Linear,Linear
repeat: none
Layer 1
  rotate: false
  xy: 2, 2
  size: 71, 11
  orig: 73, 12
  offset: 2, 0
  index: -1
Layer 2
  rotate: true
  xy: 2, 15
  size: 59, 49
  orig: 61, 49
  offset: 0, 0
  index: -1
Layer 3
  rotate: true
  xy: 53, 18
  size: 56, 19
  orig: 56, 19
  offset: 0, 0
  index: -1
Layer 4
  rotate: true
  xy: 74, 36
  size: 17, 9
  orig: 17, 9
  offset: 0, 0
  index: -1
Layer 5
  rotate: true
  xy: 74, 55
  size: 19, 9
  orig: 19, 9
  offset: 0, 0
  index: -1
