
/* Built off of "SpriteScissor" and contains all of that shader's functionality too. */

Shader "Custom/Conflict Canyon Pipe"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_MinimumX ("MinimumX", Float) = 0
		_MaximumX ("MaximumX", Float) = 0
		_MinimumY ("MinimumY", Float) = 0
		_MaximumY ("MaximumY", Float) = 0
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha
		//Blend Zero One

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 vertexWorld : TEXCOORD1;
			};
			
			float _MinimumX;
			float _MaximumX;
			float _MinimumY;
			float _MaximumY;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif
				OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				if (IN.vertexWorld.x < _MinimumX) discard;
				if (IN.vertexWorld.x > _MaximumX) discard;
				if (IN.vertexWorld.y < _MinimumY) discard;
				if (IN.vertexWorld.y > _MaximumY) discard;

                float2 uv = IN.texcoord;

                float curveStrength = abs(IN.texcoord.y - 0.5) * 1;
                curveStrength *= curveStrength * 0.07;
                float distanceFromCenter = IN.vertexWorld.x - _WorldSpaceCameraPos.x;
                uv.x += curveStrength * -distanceFromCenter;
                uv.x = frac(uv.x);

				fixed4 c = tex2D(_MainTex, uv) * IN.color;
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
}
