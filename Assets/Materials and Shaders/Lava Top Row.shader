
Shader "Custom/Lava Top Row"
{
    Properties
    {
        _LavaTexture ("Lava Texture", 2D) = "white" {}
        _RockTexture ("Rock Texture", 2D) = "white" {}
        _SeamsTexture ("Seams Texture", 2D) = "white" {}
        _BlurTexture ("Blur Texture", 2D) = "white" {}
        _TransitionTime ("Transition Time", Float) = 0
        [MaterialToggle] _IsTransitioningToRock ("Is Transitioning To Rock", Float) = 0
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        //Blend Zero One

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex      : SV_POSITION;
                float4 vertexWorld : TEXCOORD1;
                fixed4 color       : COLOR;
                half2 texcoord     : TEXCOORD0;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _LavaTexture;
            sampler2D _SeamsTexture;
            sampler2D _RockTexture;
            sampler2D _BlurTexture;
            float _TransitionTime;
            float _IsTransitioningToRock;

            fixed brightness(fixed4 rgb)
            {
                return (rgb.r + rgb.g + rgb.b) * 0.33333;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                fixed4 cr = tex2D(_RockTexture, IN.texcoord) * IN.color;
                fixed4 cs = tex2D(_SeamsTexture, IN.texcoord) * IN.color;
                fixed4 cb = tex2D(_BlurTexture, IN.texcoord * 0.5);

                fixed4 c = cr;

                float amount = clamp(_TransitionTime * 0.05, 0, 1);
                if (_IsTransitioningToRock == 1)
                    amount = 1 - amount;
                else
                    amount = clamp((_TransitionTime - 40) * 0.05, 0, 1);

                float offsetY = amount;
                if (amount * 2 > IN.texcoord.y + cb.r)
                    c.a = 0;

                if (_IsTransitioningToRock == 1 && _TransitionTime < 40)
                {
                    float alpha = 1.0 - ((_TransitionTime - 25) * 0.08);
                    alpha = clamp(alpha, 0.0, 1.0);
                    c = lerp(c, cs, cs.a * alpha);
                }

                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
