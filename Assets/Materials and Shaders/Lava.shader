
Shader "Custom/Lava"
{
    Properties
    {
        _LavaTexture  ("Lava Texture", 2D) = "white" {}
        _RockTexture  ("Rock Texture", 2D) = "white" {}
        _SeamsTexture ("Seams Texture", 2D) = "white" {}
        _BlurTexture  ("Blur Texture", 2D) = "white" {}
        _LavaCenterX  ("Lava Center X", Float) = 0
        _LavaTopY     ("Lava Top Y", Float) = 0
        _TransitionTime ("Transition Time", Float) = 0
        [MaterialToggle] _IsTransitioningToRock ("Is Transitioning To Rock", Float) = 0
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha
        //Blend Zero One

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex      : SV_POSITION;
                float4 vertexWorld : TEXCOORD1;
                fixed4 color       : COLOR;
                half2 texcoord     : TEXCOORD0;
            };

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif

                return OUT;
            }

            sampler2D _LavaTexture;
            sampler2D _SeamsTexture;
            sampler2D _RockTexture;
            sampler2D _BlurTexture;
            float _LavaCenterX;
            float _LavaTopY;
            float _TransitionTime;
            float _IsTransitioningToRock;

            fixed brightness(fixed4 rgb)
            {
                return (rgb.r + rgb.g + rgb.b) * 0.33333;
            }

            float square(float s)
            {
                return s * s;
            }

            float ease_for_transition(float s)
            {
                if (s < 0) return 0;
                if (s > 1) return 1;

                s = 1 - s;
                s *= s;
                s *= s;
                s = 1 - s;

                if (s < 0.5)
                    s = square(s * 2) * 0.5;
                else
                    s = 1 - (square((1 - s) * 2) * 0.5);

                return s;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                float2 texcoordWorld = float2(
                    IN.vertexWorld.x / 8.0,
                    IN.vertexWorld.y / 6.0
                );

                fixed4 cr = tex2D(_RockTexture, texcoordWorld) * IN.color;
                fixed4 cl = tex2D(_LavaTexture, IN.texcoord);
                fixed4 cs = tex2D(_SeamsTexture, texcoordWorld);
                fixed4 cb = tex2D(_BlurTexture, texcoordWorld);

                float transitionHeight;
                {
                    float t = _TransitionTime;
                    if (_IsTransitioningToRock == 0)
                        t = clamp(60 - t, 0, 80);

                    transitionHeight = _LavaTopY - (t * 0.2) - (t * t * 0.02);
                    if (t < 10)
                        transitionHeight += (10 - t) * 0.1;
                }
                float offsetY = IN.vertexWorld.y - transitionHeight;
                float offsetX = IN.vertexWorld.x - _LavaCenterX;
                offsetY += -abs(offsetX * offsetX * 0.02);
                offsetY += cs.a * -20;
                float range = 50 * square(square(1 - cb.r));
                float through = ease_for_transition(offsetY / range);

                float blurColour = cb.r;
                blurColour = 1 - blurColour;

                float s = (through * 20) - (blurColour * (20 - 1));
                s = clamp(s, 0.0, 1.0);
                fixed4 c = lerp(cl, cr, s);

                if (s < 0)
                    c = max(c, cl);

                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
