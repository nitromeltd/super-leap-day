Shader "Custom/SpriteHSV"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_Color("Alpha", Color) = (1,1,1,1)
		_HueValue("Hue Value", Float) = 0
		_SaturationValue("Saturation Value", Float) = 0
		_LightnessValue("Lightness Value", Float) = 0
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
		[HideInInspector] _RendererColor("RendererColor", Color) = (1,1,1,1)
		[HideInInspector] _Flip("Flip", Vector) = (1,1,1,1)
		[PerRendererData] _AlphaTex("External Alpha", 2D) = "white" {}
		[PerRendererData] _EnableExternalAlpha("Enable External Alpha", Float) = 0
	}

		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
			CGPROGRAM
				#pragma vertex SpriteVert
				#pragma fragment frag
				#pragma target 2.0
				#pragma multi_compile_instancing
				#pragma multi_compile_local _ PIXELSNAP_ON
				#pragma multi_compile _ ETC1_EXTERNAL_ALPHA
				#include "UnitySprites.cginc"

				float _HueValue;
				float _SaturationValue;
				float _LightnessValue;

				float3 rgb2hsv(float3 c)
				{
				  float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				  float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				  float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

				  float d = q.x - min(q.w, q.y);
				  float e = 1.0e-10;
				  return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
				}

				float3 hsv2rgb(float3 c)
				{
				  c = float3(c.x, clamp(c.yz, 0.0, 1.0));
				  float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
				  float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
				  return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
				}

				float4 frag(v2f IN) : COLOR
				{
					float4 o = float4(1, 0, 0, 0.2);
					float4 hsvaValue = float4(_HueValue, _SaturationValue, _LightnessValue, 0.0);

					float4 color = tex2D(_MainTex, IN.texcoord);
					color.rgb *= color.a;
					float3 hsv = rgb2hsv(color.rgb);
					float affectMult = step(0, hsv.r) * step(hsv.r, 1);
					float3 rgb = hsv2rgb(hsv + hsvaValue.xyz * affectMult);

					float4 final = float4(rgb, color.a + hsvaValue.a);
					return final *= IN.color.a;
				}
			ENDCG
			}
		}
}

