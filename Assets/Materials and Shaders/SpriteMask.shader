﻿Shader "Custom/SpriteMask"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        [IntRange] _StencilRef ("Stencil reference value", Range(0,255)) = 0
    }

    SubShader
    {

        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

         
        Pass
        {
            Cull Off
            Lighting Off
            ZWrite Off
            Blend One OneMinusSrcAlpha    
            ColorMask 0

            Stencil
            {
                Ref [_StencilRef]
                Comp Always
                Pass Replace
            }           

            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc"


            float4 frag(v2f IN) : COLOR
            {
                float4 color = tex2D (_MainTex, IN.texcoord);
                return color;
            }
            ENDCG
        }
    }
}
