﻿Shader "Custom/SpriteMasked"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        [IntRange] _StencilRef ("Stencil reference value", Range(0,255)) = 0
    }

    SubShader
    {

        Tags
        {
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "Queue" = "Transparent-1"
        }

        

        Pass
        {

            Cull off
            Lighting On
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha
            ColorMask 0

            Stencil
            {
                Ref [_StencilRef]
                Comp Equal
            }
            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc" 

            float4 frag(v2f IN) : COLOR
            {
                float4 color = tex2D (_MainTex, IN.texcoord);
                return color;
            }
            ENDCG
        }

        Pass
        {
            Cull off
            Lighting On
            ZWrite Off
            Fog { Mode Off }
            Blend SrcAlpha OneMinusSrcAlpha
            
            Stencil
            {
                Ref [_StencilRef]
                Comp NotEqual
            }
            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc" 

            float4 frag(v2f IN) : COLOR
            {
                float4 s = float4(0.02f, 0.54f, 0.98f, 1);
                float4 color = tex2D (_MainTex, IN.texcoord);
                clip(color.a - 0.01);
                return color;
            }
            ENDCG
 
             
        }
    }
}
