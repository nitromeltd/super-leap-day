
Shader "Custom/Temple Lightning"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		_LitTex ("Lit Texture", 2D) = "white" {}
		_FillAmount ("Fill Amount", Float) = 1
		_FillColour ("Fill Colour", Color) = (1,1,1,1)
		_LitAmount ("Lit Amount", Float) = 1
		_LitColour ("Lit Colour", Color) = (1,1,1,1)
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha
		//Blend Zero One

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
			};
			
			fixed4 _Color;
			float _Intensity;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _FillTex;
			sampler2D _LitTex;
			float4 _FillColour;
			float _FillAmount;
			float4 _LitColour;
			float _LitAmount;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
				fixed4 fill = tex2D(_FillTex, IN.texcoord) * IN.color;
				// c.rgb += ((fill.rgb * _FillColour) - c.rgb) * _FillAmount;				
				c.rgb += ((c.a * _FillColour) - c.rgb) * _FillAmount;				
				fixed4 lit = tex2D(_LitTex, IN.texcoord) * IN.color;
				c.rgb += ((lit.rgb * _LitColour) - c.rgb) * _LitAmount * lit.a;
				c.rgb *= c.a;
				return c;
			}
		ENDCG
		}
	}
}
