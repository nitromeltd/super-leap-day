﻿Shader "Custom/TestPlayerSilhouette"
{
    Properties
    {
        [IntRange] _StencilRef ("Stencil reference value", Range(0,255)) = 0
    }

    SubShader
    {
        

        Tags
        {
            "Queue"="Geometry"
        }
        
        Cull off
        ZWrite Off
        // ColorMask 0

        Pass
        {
            Stencil
            {
                Ref [_StencilRef]
                Comp Always
                Pass Replace
            }
            CGPROGRAM
            #pragma vertex SpriteVert
            #pragma fragment frag
            #pragma target 2.0
            #pragma multi_compile_instancing
            #pragma multi_compile_local _ PIXELSNAP_ON
            #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
            #include "UnitySprites.cginc" 

            float4 frag(v2f IN) : COLOR
            {
                float4 color = tex2D (_MainTex, IN.texcoord);
                clip(color.a - 0.01);
                return color;
            }
            ENDCG
        }

    }
    FallBack "Diffuse" 
}
