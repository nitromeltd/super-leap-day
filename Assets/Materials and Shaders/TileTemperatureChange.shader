﻿Shader "Custom/TemperatureTileShader"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _YForCold ("YForCold", Float) = 0
        _YForHot  ("YForHot", Float) = 0
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha
        //Blend Zero One

        //
        //  We can use multiple different styles of HSL manipulation depending on
        //  what the type of tile is, but we need to distinguish them, so we pass
        //  the type of the ground through vertex colour.
        //     FFFFFF white = ice cover which disappears when in hot
        //     00FFFF cyan  = ice base which changes (+135° hue, -31 sat, -10 light)
        //

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 vertexWorld : TEXCOORD1;
            };
            
            float _YForCold;
            float _YForHot;

            float _ColourBalanceRed[256];
            float _ColourBalanceGreen[256];
            float _ColourBalanceBlue[256];

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif
                OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);

                return OUT;
            }

            sampler2D _MainTex;

            float3 rgb2hsv(float3 c)
            {
                float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
                float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

                float d = q.x - min(q.w, q.y);
                float e = 1.0e-10;
                return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
            }

            float3 hsv2rgb(float3 c)
            {
                c = float3(c.x, clamp(c.yz, 0.0, 1.0));
                float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
                float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
                return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
            }

            float3 colourBalanceForHot(float3 rgb)
            {
                fixed r = fixed(rgb.r * 255);
                fixed g = fixed(rgb.g * 255);
                fixed b = fixed(rgb.b * 255);
                r = _ColourBalanceRed[r];
                g = _ColourBalanceGreen[g];
                b = _ColourBalanceBlue[b];
                rgb = float3(r / 255.0, g / 255.0, b / 255.0);
                return rgb;
            }

            fixed4 hotVersionForNormalTiles(fixed4 value)
            {
                float3 hsv = rgb2hsv(value);
                hsv.x += 0.27;
                hsv.x %= 1.0;
                float3 rgb = hsv2rgb(hsv);
                rgb = colourBalanceForHot(rgb);
                return fixed4(rgb.r, rgb.g, rgb.b, value.a);
            }

            fixed4 hotVersionForSecondaryTiles(fixed4 value)
            {
                float3 hsv = rgb2hsv(value);
                hsv.x += 0.661; // -122° hue
                hsv.x %= 1.0;
                hsv.y *= 0.53; // -47 saturday
                hsv.z *= 0.93; // -7 lightness
                float3 rgb = hsv2rgb(hsv);
                rgb = colourBalanceForHot(rgb);
                return fixed4(rgb.r, rgb.g, rgb.b, value.a);
            }

            fixed4 hotVersionForIce(fixed4 value)
            {
                float3 hsv = rgb2hsv(value);
                hsv.x += 0.325; // +117° hue
                hsv.x %= 1.0;
                hsv.y *= 0.79; // -21 saturation
                hsv.z *= 0.94; // -6 lightness
                float3 rgb = hsv2rgb(hsv);
                return fixed4(rgb.r, rgb.g, rgb.b, value.a);
            }

            fixed4 hotVersionForBackWall(fixed4 value)
            {
                float3 hsv = rgb2hsv(value);
                hsv.x += 0.256; // +92° hue
                hsv.x %= 1.0;
                hsv.y *= 1.03; // +3 saturation
                hsv.z *= 0.70; // -30 lightness
                float3 rgb = hsv2rgb(hsv);
                return fixed4(rgb.r, rgb.g, rgb.b, value.a);
            }

            fixed4 hotVersionForCloudPlatform(fixed4 value)
            {
                float3 hsv = rgb2hsv(value);
                hsv.x += 0.661; // -122° hue
                hsv.x %= 1.0;
                hsv.y *= 0.88; // -12 saturation
                hsv.z *= 0.95; // -5 lightness
                float3 rgb = hsv2rgb(hsv);
                return fixed4(rgb.r, rgb.g, rgb.b, value.a);
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                float s =
                    (IN.vertexWorld.y - _YForCold) /
                    (_YForHot - _YForCold);
                if (s < 0) s = 0;
                if (s > 1) s = 1;

                fixed4 c1 = tex2D(_MainTex, IN.texcoord);
                fixed4 c2;
                
                if (IN.color.r < 0.1 && IN.color.g < 0.1) // black = ice base
                    c2 = hotVersionForIce(c1);
                else if (IN.color.g < 0.1 && IN.color.b < 0.1) // red = secondary
                    c2 = hotVersionForSecondaryTiles(c1);
                else if (IN.color.r < 0.9) // cyan = normal tiles
                    c2 = hotVersionForNormalTiles(c1);
                else if (IN.color.g < 0.9) // magenta = back wall
                    c2 = hotVersionForBackWall(c1);
                else if (IN.color.b < 0.9) // yellow = cloud platform
                    c2 = hotVersionForCloudPlatform(c1);
                else // white = ice cover
                    c2 = float4(c1.r, c1.g, c1.b, 0);

                fixed4 c = lerp(c1, c2, s);

                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
