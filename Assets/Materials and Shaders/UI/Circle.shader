
Shader "Custom/Circle"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _Size ("Size", Float) = 0
        _Radius ("Radius", Float) = 0
        _OutlineThickness ("OutlineThickness", Float) = 0
        _OutlineColour ("OutlineColour", Color) = (0, 0, 0, 0)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha
        //Blend Zero One

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 vertexWorld : TEXCOORD1;
            };
            
            float _Size;
            float _Radius;
            float _OutlineThickness;
            float4 _OutlineColour;

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif
                OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);

                return OUT;
            }

            sampler2D _MainTex;

            fixed4 frag(v2f IN) : SV_Target
            {
                float outlineMin = _Radius - (_OutlineThickness * 0.5f);
                float outlineMax = _Radius + (_OutlineThickness * 0.5f);
                outlineMin *= outlineMin;
                outlineMax *= outlineMax;

                float dx = (IN.texcoord.x - 0.5f) * 2;
                float dy = (IN.texcoord.y - 0.5f) * 2;
                float sqDist = dx * dx + dy * dy;

                fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;

                if (_Radius > 0.001f)
                {
                    if (sqDist < outlineMin)
                        c.a = 0;
                    else if (sqDist < outlineMax)
                        c = _OutlineColour;
                }

                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
