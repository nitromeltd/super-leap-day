﻿Shader "Custom/Water Background"
{
    Properties
    {
        [PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
        _YForNormal("YForNormal", Float) = 0
        _YForBlue  ("YForBlue", Float) = 0
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }

    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha
        //Blend Zero One

        //
        //  We can use multiple different styles of HSL manipulation depending on
        //  what the type of tile is, but we need to distinguish them, so we pass
        //  the type of the ground through vertex colour.
        //     FFFFFF white = ice cover which disappears when in hot
        //     00FFFF cyan  = ice base which changes (+135° hue, -31 sat, -10 light)
        //

        Pass
        {
        CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            
            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color    : COLOR;
                half2 texcoord  : TEXCOORD0;
                float4 vertexWorld : TEXCOORD1;
            };
            
            float _YForNormal;
            float _YForBlue;

            float _HeightmapXMin;
            float _HeightmapXMax;
            float _Heightmap[256];

            v2f vert(appdata_t IN)
            {
                v2f OUT;
                OUT.vertex = UnityObjectToClipPos(IN.vertex);
                OUT.texcoord = IN.texcoord;
                OUT.color = IN.color;
                #ifdef PIXELSNAP_ON
                OUT.vertex = UnityPixelSnap (OUT.vertex);
                #endif
                OUT.vertexWorld = mul(unity_ObjectToWorld, IN.vertex);

                return OUT;
            }

            sampler2D _MainTex;

            float3 rgb2hsv(float3 c)
            {
                float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
                float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

                float d = q.x - min(q.w, q.y);
                float e = 1.0e-10;
                return float3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
            }

            float3 hsv2rgb(float3 c)
            {
                c = float3(c.x, clamp(c.yz, 0.0, 1.0));
                float4 K = float4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
                float3 p = abs(frac(c.xxx + K.xyz) * 6.0 - K.www);
                return c.z * lerp(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
            }

            fixed4 blueify(fixed4 color, fixed4 closeness)
            {
                float3 hsv = rgb2hsv(color);
                hsv[0] = 0.5f;
                hsv[1] = lerp(0.7f, 0.9f, hsv[1]);
                hsv[1] = lerp(0.99f, hsv[1], closeness);
                hsv[2] = lerp(0.8f, hsv[2], closeness);
                color.xyz = hsv2rgb(hsv).xyz;
                return color;
            }

            fixed4 frag(v2f IN) : SV_Target
            {
                float through =
                    (IN.vertexWorld.x - _HeightmapXMin) /
                    (_HeightmapXMax - _HeightmapXMin);
                int index = (int)(through * 256.0);
                if (index < 0) index = 0;
                if (index > 255) index = 255;
                float waterHeight = _Heightmap[index];

                float s = (waterHeight - IN.vertexWorld.y) * 2;
                if (s < 0) s = 0;
                if (s > 1) s = 1;
                s = 1 - s;
                s *= s;
                s = 1 - s;

                float closeness = 1 - (IN.vertexWorld.z * 0.1f);
                if (closeness < 0) closeness = 0;
                if (closeness > 1) closeness = 1;

                fixed4 colorOriginal = tex2D(_MainTex, IN.texcoord);
                fixed4 colorBlue = blueify(colorOriginal, closeness);

                fixed4 c = lerp(colorOriginal, colorBlue, s);

                if (IN.color.r < 0.1 && IN.color.g > 0.9 && IN.color.b < 0.1)
                {
                    c.a *= 1 - s;
                }
                else if (IN.color.r < 0.1 && IN.color.g < 0.1 && IN.color.b > 0.9)
                {
                    c.a *= s;
                }

                c.rgb *= c.a;
                return c;
            }
        ENDCG
        }
    }
}
