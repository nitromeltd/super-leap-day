
intro puffer frames.png
size: 514,547
format: RGBA8888
filter: Linear,Linear
repeat: none
Puffer body
  rotate: false
  xy: 2, 2
  size: 111, 93
  orig: 111, 93
  offset: 0, 0
  index: -1
Puffer cheek
  rotate: true
  xy: 115, 2
  size: 93, 89
  orig: 93, 89
  offset: 0, 0
  index: -1
Puffer copy
  rotate: false
  xy: 2, 300
  size: 177, 245
  orig: 177, 245
  offset: 0, 0
  index: -1
Puffer flinch
  rotate: false
  xy: 181, 323
  size: 179, 222
  orig: 179, 222
  offset: 0, 0
  index: -1
Puffer front arm
  rotate: true
  xy: 444, 55
  size: 71, 68
  orig: 71, 68
  offset: 0, 0
  index: -1
Puffer front leg
  rotate: false
  xy: 206, 39
  size: 59, 57
  orig: 59, 60
  offset: 0, 0
  index: -1
Puffer hair front
  rotate: false
  xy: 362, 398
  size: 143, 147
  orig: 163, 147
  offset: 20, 0
  index: -1
Puffer hair inflated back
  rotate: false
  xy: 324, 128
  size: 153, 112
  orig: 153, 112
  offset: 0, 0
  index: -1
Puffer hair inflated front
  rotate: true
  xy: 164, 98
  size: 200, 158
  orig: 200, 168
  offset: 0, 0
  index: -1
Puffer head
  rotate: false
  xy: 324, 15
  size: 118, 111
  orig: 156, 144
  offset: 8, 13
  index: -1
Puffer head back
  rotate: true
  xy: 362, 242
  size: 154, 121
  orig: 154, 121
  offset: 0, 0
  index: -1
Puffer head inflated
  rotate: true
  xy: 2, 97
  size: 201, 160
  orig: 222, 190
  offset: 3, 0
  index: -1
