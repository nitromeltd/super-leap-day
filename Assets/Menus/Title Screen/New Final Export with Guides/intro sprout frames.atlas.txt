
intro sprout frames.png
size: 483,529
format: RGBA8888
filter: Linear,Linear
repeat: none
Sprout
  rotate: false
  xy: 183, 329
  size: 189, 198
  orig: 189, 198
  offset: 0, 0
  index: -1
smack effect
  rotate: false
  xy: 153, 30
  size: 155, 182
  orig: 155, 182
  offset: 0, 0
  index: -1
sprout jump down back leg
  rotate: false
  xy: 374, 259
  size: 62, 101
  orig: 62, 102
  offset: 0, 0
  index: -1
sprout jump down body
  rotate: false
  xy: 183, 214
  size: 189, 113
  orig: 190, 199
  offset: 0, 77
  index: -1
sprout jump down eye close
  rotate: false
  xy: 435, 228
  size: 46, 29
  orig: 46, 29
  offset: 0, 0
  index: -1
sprout jump down eye open
  rotate: false
  xy: 427, 91
  size: 47, 47
  orig: 48, 47
  offset: 1, 0
  index: -1
sprout jump down fan 1
  rotate: true
  xy: 310, 16
  size: 71, 77
  orig: 71, 77
  offset: 0, 0
  index: -1
sprout jump down fan 2
  rotate: false
  xy: 389, 10
  size: 71, 77
  orig: 71, 77
  offset: 0, 0
  index: -1
sprout jump down front leg
  rotate: true
  xy: 374, 214
  size: 43, 59
  orig: 43, 59
  offset: 0, 0
  index: -1
sprout jump down head
  rotate: true
  xy: 310, 89
  size: 123, 115
  orig: 123, 115
  offset: 0, 0
  index: -1
sprout jump down mouth
  rotate: false
  xy: 374, 362
  size: 72, 94
  orig: 75, 94
  offset: 3, 0
  index: -1
sprout jump down particle 1
  rotate: false
  xy: 153, 2
  size: 65, 26
  orig: 65, 26
  offset: 0, 0
  index: -1
sprout jump down particle 2
  rotate: true
  xy: 448, 352
  size: 46, 24
  orig: 46, 24
  offset: 0, 0
  index: -1
sprout jump down particle 3
  rotate: true
  xy: 448, 400
  size: 56, 23
  orig: 56, 23
  offset: 0, 0
  index: -1
sprout jump down particle 4
  rotate: false
  xy: 220, 7
  size: 59, 21
  orig: 59, 21
  offset: 0, 0
  index: -1
sprout jump down skirt bump 1
  rotate: true
  xy: 438, 309
  size: 41, 37
  orig: 41, 37
  offset: 0, 0
  index: -1
sprout jump up body
  rotate: false
  xy: 2, 4
  size: 149, 224
  orig: 184, 241
  offset: 34, 17
  index: -1
sprout jump up feet
  rotate: true
  xy: 427, 140
  size: 72, 53
  orig: 73, 53
  offset: 0, 0
  index: -1
sprout jump up flower
  rotate: true
  xy: 374, 458
  size: 69, 107
  orig: 73, 107
  offset: 0, 0
  index: -1
sprout standing
  rotate: false
  xy: 2, 230
  size: 179, 297
  orig: 179, 297
  offset: 0, 0
  index: -1
