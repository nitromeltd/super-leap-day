
Polished Background.png
size: 3795,2052
format: RGBA8888
filter: Linear,Linear
repeat: none
Background optimised
  rotate: true
  xy: 2, 593
  size: 1457, 1522
  orig: 1459, 1524
  offset: 1, 1
  index: -1
Logo curved
  rotate: false
  xy: 2, 5
  size: 1099, 586
  orig: 1099, 586
  offset: 0, 0
  index: -1
Shape 2
  rotate: true
  xy: 1526, 1019
  size: 1031, 1259
  orig: 1031, 1259
  offset: 0, 0
  index: -1
Sky optimised
  rotate: false
  xy: 1314, 174
  size: 173, 173
  orig: 175, 175
  offset: 1, 1
  index: -1
Sun optimised
  rotate: false
  xy: 2733, 134
  size: 751, 751
  orig: 751, 751
  offset: 0, 0
  index: -1
lense flare 1
  rotate: false
  xy: 1103, 138
  size: 209, 209
  orig: 209, 209
  offset: 0, 0
  index: -1
lense flare 10
  rotate: false
  xy: 1239, 80
  size: 56, 56
  orig: 56, 56
  offset: 0, 0
  index: -1
lense flare 11
  rotate: false
  xy: 1103, 349
  size: 242, 242
  orig: 242, 242
  offset: 0, 0
  index: -1
lense flare 12
  rotate: false
  xy: 1103, 2
  size: 134, 134
  orig: 134, 134
  offset: 0, 0
  index: -1
lense flare 2
  rotate: false
  xy: 1347, 456
  size: 135, 135
  orig: 135, 135
  offset: 0, 0
  index: -1
lense flare 3
  rotate: false
  xy: 1314, 36
  size: 136, 136
  orig: 136, 136
  offset: 0, 0
  index: -1
lense flare 4
  rotate: false
  xy: 1314, 36
  size: 136, 136
  orig: 136, 136
  offset: 0, 0
  index: -1
lense flare 5
  rotate: false
  xy: 3486, 752
  size: 133, 133
  orig: 133, 133
  offset: 0, 0
  index: -1
lense flare 6
  rotate: false
  xy: 3486, 752
  size: 133, 133
  orig: 133, 133
  offset: 0, 0
  index: -1
lense flare 7
  rotate: false
  xy: 3486, 622
  size: 128, 128
  orig: 128, 128
  offset: 0, 0
  index: -1
lense flare 8
  rotate: false
  xy: 2733, 976
  size: 41, 41
  orig: 41, 41
  offset: 0, 0
  index: -1
lense flare 9
  rotate: false
  xy: 2733, 976
  size: 41, 41
  orig: 41, 41
  offset: 0, 0
  index: -1
shine 1
  rotate: true
  xy: 2787, 887
  size: 1163, 1006
  orig: 1163, 1006
  offset: 0, 0
  index: -1
shine 2
  rotate: false
  xy: 1526, 55
  size: 1205, 962
  orig: 1205, 962
  offset: 0, 0
  index: -1
