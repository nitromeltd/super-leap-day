
intro puffer frames.png
size: 1005,1084
format: RGBA8888
filter: Linear,Linear
repeat: none
Puffer body
  rotate: false
  xy: 2, 2
  size: 221, 185
  orig: 221, 185
  offset: 0, 0
  index: -1
Puffer cheek
  rotate: true
  xy: 225, 2
  size: 185, 178
  orig: 185, 178
  offset: 0, 0
  index: -1
Puffer copy
  rotate: false
  xy: 2, 592
  size: 354, 490
  orig: 354, 490
  offset: 0, 0
  index: -1
Puffer flinch
  rotate: false
  xy: 358, 639
  size: 357, 443
  orig: 357, 443
  offset: 0, 0
  index: -1
Puffer front arm
  rotate: false
  xy: 405, 53
  size: 141, 136
  orig: 141, 136
  offset: 0, 0
  index: -1
Puffer front leg
  rotate: true
  xy: 877, 134
  size: 117, 113
  orig: 117, 119
  offset: 0, 0
  index: -1
Puffer hair front
  rotate: false
  xy: 717, 788
  size: 286, 294
  orig: 326, 294
  offset: 40, 0
  index: -1
Puffer hair inflated back
  rotate: false
  xy: 640, 253
  size: 306, 224
  orig: 306, 224
  offset: 0, 0
  index: -1
Puffer hair inflated front
  rotate: true
  xy: 323, 191
  size: 399, 315
  orig: 399, 335
  offset: 0, 0
  index: -1
Puffer head
  rotate: false
  xy: 640, 30
  size: 235, 221
  orig: 312, 287
  offset: 16, 26
  index: -1
Puffer head back
  rotate: true
  xy: 717, 479
  size: 307, 242
  orig: 307, 242
  offset: 0, 0
  index: -1
Puffer head inflated
  rotate: true
  xy: 2, 189
  size: 401, 319
  orig: 443, 379
  offset: 7, 0
  index: -1
