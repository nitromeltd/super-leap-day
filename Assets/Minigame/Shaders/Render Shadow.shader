﻿Shader "Custom/Render Shadow"
{
	Properties
	{
		_MainTex("Camera Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
	}
		SubShader
		{
			Tags
			{
				"Queue" = "Transparent"
				"IgnoreProjector" = "True"
				"RenderType" = "Transparent"
				"PreviewType" = "Plane"
				"CanUseSpriteAtlas" = "True"
			}

			Cull Off
			Lighting Off
			ZWrite Off
			Blend One OneMinusSrcAlpha

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile _ PIXELSNAP_ON

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float4 color    : COLOR;
					float2 uv : TEXCOORD0;
				};

				struct v2f
				{
					float4 vertex : SV_POSITION;
					float2 uv : TEXCOORD0;
					fixed4 color : COLOR;
				};

				fixed4 _Color;
				sampler2D _MainTex;

				v2f vert(appdata v)
				{
					v2f o;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.uv = v.uv;
					o.color = v.color;
					return o;
				}

				fixed4 frag(v2f i) : SV_Target
				{

					fixed4 c = tex2D(_MainTex, i.uv) * i.color;
					
					c.r = _Color.r;
					c.g = _Color.g;
					c.b = _Color.b;

					if (c.a > 0) c.a *= _Color.a;

					c.rgb *= c.a;


					return c;
				}
				ENDCG
			}
		}
}
