
// http://blog.onebyonedesign.com/unity/photoshop-blend-modes-for-unity3d/

Shader "Custom/Overlay"
{
	Properties
	{
		[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
		_BlendTex("Blend Texture", 2D) = "white" {}
		_Color("Tint", Color) = (1,1,1,1)
		_Intensity("Intensity", Float) = 1
		[MaterialToggle] PixelSnap("Pixel snap", Float) = 0
	}
	SubShader
	{
		Tags
		{
			"Queue" = "Transparent"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
			"PreviewType" = "Plane"
			"CanUseSpriteAtlas" = "True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		Blend One OneMinusSrcAlpha
		//Blend Zero One

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"

			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color : COLOR;
				half2 texcoord  : TEXCOORD0;
			};

			float overlay(float s, float d)
			{
				return (d > 0.5) ? 2.0 * s * d : 1.0 - 2.0 * (1.0 - s) * (1.0 - d);
			}

			fixed3 Overlay(fixed3 s, fixed3 d)
			{
				fixed3 c;
				c.r = overlay(s.r, d.r);
				c.g = overlay(s.g, d.g);
				c.b = overlay(s.b, d.b);
				return c;
			}

			fixed4 _Color;
			float _Intensity;
			sampler2D _BlendTex;
			uniform float4  _BlendTex_ST;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				OUT.vertex = UnityObjectToClipPos(IN.vertex);
				OUT.texcoord = IN.texcoord;
				OUT.color = IN.color * _Color;
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap(OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;

			fixed4 frag(v2f IN) : SV_Target
			{
				fixed4 c = tex2D(_MainTex, IN.texcoord) * IN.color;
				fixed4 blendColor = tex2D(_BlendTex, IN.texcoord * _BlendTex_ST.xy + _BlendTex_ST.zw);
				fixed3 blendedColor = Overlay(c.rgb, blendColor.rgb);
				c.rgb = c.rgb * (1 - blendColor.a) + blendedColor * blendColor.a * _Intensity;
				return c;
			}
			ENDCG
		}
	}
}