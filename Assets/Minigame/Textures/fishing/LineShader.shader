Shader "Custom/Line Scroll"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _ScrollOffset("Scroll Offset", Float) = 0
        _Color("Tint", Color) = (1,1,1,1)
        _Intensity("Intensity", Float) = 1
    }
        SubShader
        {

            Tags
            {
                "Queue" = "Transparent"
                "IgnoreProjector" = "True"
                "RenderType" = "Transparent"
                "PreviewType" = "Plane"
                "CanUseSpriteAtlas" = "True"
            }

            Lighting Off
            Cull Off
            ZTest Always
            ZWrite Off
            Blend SrcAlpha OneMinusSrcAlpha

            Pass
            {
                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float4 color    : COLOR;
                    float2 uv : TEXCOORD0;
                };
                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float4 vertex : SV_POSITION;
                };
                sampler2D _MainTex;
                float4 _MainTex_ST;
                float _ScrollOffset;
                fixed4 _Color;
                float _Intensity;
                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                    return o;
                }
                fixed4 frag(v2f i) : SV_Target
                {
                    fixed4 col = tex2D(_MainTex, i.uv + float2(_ScrollOffset, 0));
                    col.r = col.r + (_Color.r - col.r) * _Intensity;
                    col.g = col.g + (_Color.g - col.g) * _Intensity;
                    col.b = col.b + (_Color.b - col.b) * _Intensity;
                    col.rgb *= col.a;
                    return col;
                }
                ENDCG
            }
        }
}