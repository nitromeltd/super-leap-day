
using UnityEngine;

namespace NitromeEditor
{
    public class PointInsidePolygon
    {
        public static bool Calculate(Vector2[] polygon, Vector2 pt)
        {
            int wn = 0;

            for (int n = 0; n < polygon.Length; n += 1)
            {
                var a = polygon[n];
                var b = polygon[(n + 1) % polygon.Length];

                if (a.y <= pt.y)
                {
                    if (b.y > pt.y)
                    {
                        float l = IsLeft(a, b, pt);
                        if (l > 0)
                            wn += 1;
                        else if (l == 0)
                            return false;
                    }
                }
                else
                {
                    if (b.y <= pt.y)
                    {
                        float l = IsLeft(a, b, pt);
                        if (l < 0)
                            wn -= 1;
                        else if (l == 0)
                            return false;
                    }
                }
            }

            return wn > 0;
        }

        private static float IsLeft(Vector2 p0, Vector2 p1, Vector2 pt)
        {
            return
                (p1.x - p0.x) * (pt.y - p0.y) -
                (pt.x - p0.x) * (p1.y - p0.y);
        }
    }
}
