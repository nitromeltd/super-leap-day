
using UnityEngine;
using MessagePack;
using System;
using System.Runtime.Serialization;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public class Entity
    {
        #if NITROME_EDITOR
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public TileSpec spec;
        #endif

        // TODO: constraint - the entity and the shape must be in the same shape layer
        [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Shape shape;
        [Key(0)] public int     shapeIndex;

        [Key(1)] public string  spriteName;
        [Key(2)] public Vector2 basePosition;
        [Key(3)] public bool    behind;

        public Entity DeepCopy(Entity target = null)
        {
            target = target ?? new Entity();

            #if NITROME_EDITOR
                target.spec = this.spec;
            #endif

            target.spriteName   = this.spriteName;
            target.shape        = this.shape;
            target.shapeIndex   = this.shapeIndex;
            target.basePosition = this.basePosition;
            target.behind       = this.behind;

            return target;
        }
    }
}
