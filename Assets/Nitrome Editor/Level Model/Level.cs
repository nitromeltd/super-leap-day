
using UnityEngine;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;

/*
    If any serialized part of the model changes, we need to regenerate the
    MessagePackGenerated.cs file.

    https://github.com/neuecc/MessagePack-CSharp/
    As written in the "AOT Code Generation" section,
    run the following in the terminal:
        dotnet tool install --global MessagePack.Generator
    Then locally in the repo itself
        dotnet new tool-manifest
        dotnet tool install MessagePack.Generator
    then, to actually generate the new .cs file:
        dotnet mpc "(wherever)...\Assembly-CSharp.csproj" -o "MessagePackGenerated.cs"

    To temporarily remove this restriction, comment out the code in MessagePackNitrome,
    but note that won't work on iOS.
 */

namespace NitromeEditor
{
    [MessagePackObject]
    public partial class Level
    {
        [Key(0)] public int minX;
        [Key(1)] public int maxX;
        [Key(2)] public int minY;
        [Key(3)] public int maxY;
        [Key(4)] public TileLayer [] tileLayers;
        [Key(5)] public PathLayer [] pathLayers;
        [Key(6)] public ShapeLayer[] shapeLayers;
        [Key(7)] public Dictionary<string, PropertyValue> properties;

    #if !NITROME_EDITOR
        public Level()
        {
            this.minX = 0;
            this.maxX = 0;
            this.minY = 0;
            this.maxY = 0;
            this.tileLayers  = new TileLayer [0];
            this.pathLayers  = new PathLayer [0];
            this.shapeLayers = new ShapeLayer[0];
            this.properties  = new Dictionary<string, PropertyValue>();
        }
    #endif

        /*
            The following methods are in here for my convenience when making the editor.
            You don't have to use them, it's fine to just read the data directly from
            the data structures, and when reading from level files, you can assume that
            minX and minY are 0 unless you change them yourself.
                - chris
        */

        public TileLayer TileLayerByName(string name)
        {
            foreach (var layer in this.tileLayers)
            {
                if (layer.layerName == name)
                    return layer;
            }
            return null;
        }

        public PathLayer PathLayerByName(string name)
        {
            foreach (var layer in this.pathLayers)
            {
                if (layer.layerName == name)
                    return layer;
            }
            return null;
        }

        public ShapeLayer ShapeLayerByName(string name)
        {
            foreach (var layer in this.shapeLayers)
            {
                if (layer.layerName == name)
                    return layer;
            }
            return null;
        }

        public int Columns() { return maxX - minX + 1; }
        public int Rows   () { return maxY - minY + 1; }

        public bool IsValidLocation(int x, int y)
        {
            return
                x >= minX && x <= maxX &&
                y >= minY && y <= maxY;
        }

        public int GetLocationIndex(int x, int y)
        {
            // only valid if IsValidLocation() returns true
            return x - minX + ((y - minY) * (maxX - minX + 1));
        }

        public TileLayer.Tile? GetTile(int x, int y, int layer)
        {
            if (layer >= tileLayers.Length)
            {
                return null;
            }
            
            if (IsValidLocation(x, y))
            {
                var loc = GetLocationIndex(x, y);

                if (loc < 0 || loc >= tileLayers[layer].tiles.Length)
                {
                    return null;
                }

                return tileLayers[layer].tiles[loc];
            }
            else
            {
                return null;
            }
        }

        public ShapeLayer ShapeLayerContainingShape(Shape s)
        {
            foreach (var shapeLayer in this.shapeLayers)
            {
                if (Array.IndexOf(shapeLayer.shapes, s) != -1)
                    return shapeLayer;
            }
            return null;
        }

        public string SerializeToJson()
        {
            PrepareForSerialize();
            return TinyJson.JSONWriter.ToJson(this);
        }

        public byte[] SerializeToBytes()
        {
            MessagePackNitrome.EnsureReady();
            PrepareForSerialize();
            var options =
                new MessagePackSerializerOptions(MessagePackNitrome.resolver)
                    .WithCompression(MessagePackCompression.Lz4Block);
            return MessagePackSerializer.Serialize(this, options);
        }

        private void PrepareForSerialize()
        {
#if NITROME_EDITOR
            foreach (var tileLayer in this.tileLayers)
            {
                for (int n = 0; n < tileLayer.tiles.Length; n += 1)
                {
                    if (tileLayer.tiles[n].tileSpec != null)
                        tileLayer.tiles[n].tile = tileLayer.tiles[n].tileSpec.name;
                    else
                        tileLayer.tiles[n].tile = null;
                }
            }
#endif

            foreach (var pathLayer in this.pathLayers)
            {
                pathLayer.paths =
                    pathLayer.paths.Where(p => p.nodes.Length > 0).ToArray();

                foreach (var path in pathLayer.paths)
                {
                    path.ProcessBeforeSerialize();
                }
            }

            ShapeLayer.ProcessBeforeSerialize(this.shapeLayers);
        }

        public static Level UnserializeFromJson(
            string json,
            Dictionary<string, ShapeMaterial> availableShapeMaterials = null
        )
        {
            var result = TinyJson.JSONParser.FromJson<Level>(json);
            PostprocessAfterUnserialize(result, availableShapeMaterials);
            return result;
        }

        public static Level UnserializeFromBytes(
            byte[] binary,
            Dictionary<string, ShapeMaterial> availableShapeMaterials = null
        )
        {
            MessagePackNitrome.EnsureReady();
            var options =
                new MessagePackSerializerOptions(MessagePackNitrome.resolver)
                    .WithCompression(MessagePackCompression.Lz4Block);
            var result = MessagePackSerializer.Deserialize<Level>(binary, options);
            PostprocessAfterUnserialize(result, availableShapeMaterials);
            return result;
        }

        private static void PostprocessAfterUnserialize(
            Level level,
            Dictionary<string, ShapeMaterial> availableShapeMaterials
        )
        {
            if (level == null) return; // probably a failed paste

#if NITROME_EDITOR
            NormalizeLayersToConfig(level);

            foreach (var tileLayer in level.tileLayers)
            {
                for (int n = 0; n < tileLayer.tiles.Length; n += 1)
                {
                    tileLayer.tiles[n].tileSpec =
                        Config.TileByName(tileLayer.tiles[n].tile, true);
                }
            }
#endif

            for (var pathLayerIndex = 0;
                pathLayerIndex < level.pathLayers.Length;
                pathLayerIndex += 1)
            {
                var pathLayer = level.pathLayers[pathLayerIndex];

                foreach (var path in pathLayer.paths)
                {
                    path.ProcessAfterUnserialize();

#if NITROME_EDITOR
                    foreach (var n in path.nodes)
                    {
                        if (n.properties == null)
                            n.properties = new Dictionary<string, PropertyValue>();

                        PropertiesEditorSpecific.NormalizePropertiesToConfig(
                            n.properties,
                            p => p.targetLayer == Config.pathLayers[pathLayerIndex]
                        );
                    }
#endif
                }
            }

            foreach (var shapeLayer in level.shapeLayers)
            {
                foreach (var shape in shapeLayer.shapes)
                {
                    if (availableShapeMaterials != null)
                    {
                        availableShapeMaterials.TryGetValue(
                            shape.materialName,
                            out shape.material
                        );
                    }

                    shape.ProcessAfterUnserialize();
                }
            }

            ShapeLayer.ProcessAfterUnserialize(level.shapeLayers);

#if NITROME_EDITOR
            if (level.properties == null)
                level.properties = new Dictionary<string, PropertyValue>();

            NormalizePropertiesToConfig(level);
#endif
        }
    }
}
