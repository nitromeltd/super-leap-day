
using MessagePack.Resolvers;

namespace NitromeEditor
{
    public class MessagePackNitrome
    {
        private static bool ready = false;
        public static MessagePack.IFormatterResolver resolver = null;

        public static void EnsureReady()
        {
            if (ready == true) return;

            resolver = CompositeResolver.Create(
                GeneratedResolver.Instance,
                StandardResolver.Instance
            );
            ready = true;
        }
    }
}
