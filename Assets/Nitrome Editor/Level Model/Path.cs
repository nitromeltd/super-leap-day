
using UnityEngine;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public class Path
    {
        [MessagePackObject]
        [Serializable]
        public class Node
        {
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Path path;
            [Key(0)] public float x;
            [Key(1)] public float y;
            [Key(2)] public Dictionary<string, PropertyValue> properties;
            [Key(3)] public float delaySeconds;
            [Key(4)] public float arcLength;
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Arc arc;
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public float distanceAlongPath;
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public float timeOfArrivalSeconds;

            public Node() {}

            public Node(Path path, float x, float y)
            {
                this.path = path;
                this.x = x;
                this.y = y;
                this.properties = new Dictionary<string, PropertyValue>();
                this.arcLength = 0;
            }

            public Node(Path path, Vector2 pos)
            {
                this.path = path;
                this.x = pos.x;
                this.y = pos.y;
                this.properties = new Dictionary<string, PropertyValue>();
                this.arcLength = 0;
            }

            [IgnoreMember] [IgnoreDataMember] public Vector2 Position
            {
                get
                {
                    return new Vector2(x, y);
                }
                set
                {
                    x = value.x;
                    y = value.y;
                }
            }

            public Node Previous()
            {
                var thisIndex = Array.IndexOf(this.path.nodes, this);
                if (thisIndex > 0)
                    return this.path.nodes[thisIndex - 1];
                else
                    return null;
            }

            public Node Next()
            {
                var thisIndex = Array.IndexOf(this.path.nodes, this);
                if (thisIndex < this.path.nodes.Length - 1)
                    return this.path.nodes[thisIndex + 1];
                else
                    return null;
            }
        }

        [Key(0)] public Node[] nodes;
        [Key(1)] public bool closed;

        // the following stuff can be calculated on load,
        // so let's not bother serializing any of it

        public class Arc
        {
            public Vector2 circleCenter;
            public float circleRadius;

            public float startDistanceAlongStraightEdge;
            public float startDistanceAlongPath;
            public float startAngleRadians;
            public Vector2 startPosition;

            public float endDistanceAlongStraightEdge;
            public float endDistanceAlongPath;
            public float endAngleRadians;
            public Vector2 endPosition;

            public float centerAngleRadians;

            public bool clockwise;
            public float distanceAround;
            public float distanceWithoutArc;
        }

        [MessagePackObject]
        [Serializable]
        public class Edge
        {
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Path path;
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Node from;
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Node to;
            [Key(0)] public int fromIndex;
            [Key(1)] public int toIndex;
            [Key(2)] public float distance;
            [Key(3)] public Vector2 direction;
            [Key(4)] public float speedPixelsPerSecond;
            [Key(5)] public float easing;

            public float DistanceToPoint(Vector2 point)
            {
                return Mathf.Sqrt(SquareDistanceToPoint(point));
            }

            public float SquareDistanceToPoint(Vector2 point)
            {
                float edgeDeltaX = to.x - from.x;
                float edgeDeltaY = to.y - from.y;
                float toTargetX = point.x - from.x;
                float toTargetY = point.y - from.y;

                var along =
                    (toTargetX * edgeDeltaX + toTargetY * edgeDeltaY) /
                    (edgeDeltaX * edgeDeltaX + edgeDeltaY * edgeDeltaY);
                if (along < 0) along = 0;
                if (along > 1) along = 1;

                var nearestX = from.x + (edgeDeltaX * along);
                var nearestY = from.y + (edgeDeltaY * along);
                var toNearestX = point.x - nearestX;
                var toNearestY = point.y - nearestY;
                var sqDist = toNearestX * toNearestX + toNearestY * toNearestY;
                return sqDist;
            }
        }

        [Key(2)] public Edge[] edges;
        [Key(3)] public float length;
        [Key(4)] public float duration;

        public struct PointOnPathInfo
        {
            public Vector2 position;
            public Vector2 tangent;
            public int edgeIndex;
            public Edge edge;
            public float distanceAlongEdge;
            public float distanceAlongPath;
            public float timeSeconds;
        }


        public Path()
        {
            this.nodes = new Node[0];
            this.closed = false;
            this.edges = new Edge[0];
            this.length = 0;
            this.duration = 0;
        }

        public Path DeepCopy(Path target = null)
        {
            target = target ?? new Path();
            target.nodes  = new Node[this.nodes.Length];
            target.closed = this.closed;

            for (var n = 0; n < this.nodes.Length; n += 1)
            {
                target.nodes[n] = new Node(
                    target,
                    this.nodes[n].x,
                    this.nodes[n].y
                );
                if (this.nodes[n].properties != null)
                {
                    target.nodes[n].properties =
                        new Dictionary<string, PropertyValue>(this.nodes[n].properties);
                }
                target.nodes[n].delaySeconds = this.nodes[n].delaySeconds;
                target.nodes[n].arcLength = this.nodes[n].arcLength;
            }

            target.edges = new Edge[this.edges.Length];
            target.length = this.length;
            target.duration = this.duration;

            for (var n = 0; n < this.edges.Length; n += 1)
            {
                target.edges[n] = new Edge();
                target.edges[n].fromIndex            = this.edges[n].fromIndex;
                target.edges[n].toIndex              = this.edges[n].toIndex;
                target.edges[n].distance             = this.edges[n].distance;
                target.edges[n].direction            = this.edges[n].direction;
                target.edges[n].speedPixelsPerSecond = this.edges[n].speedPixelsPerSecond;
                target.edges[n].easing               = this.edges[n].easing;

                target.edges[n].path = target;
                target.edges[n].from = target.nodes[target.edges[n].fromIndex];
                target.edges[n].to   = target.nodes[target.edges[n].toIndex];
            }

            return target;
        }

        public void ProcessAfterUnserialize()
        {
            foreach (var node in this.nodes)
            {
                node.path = this;
            }

            // if (this.edges == null)
            {
                RecalculateEdgesAndLengths();
            }

            foreach (var edge in this.edges)
            {
                edge.path = this;
                edge.from = this.nodes[edge.fromIndex];
                edge.to   = this.nodes[edge.toIndex];
            }

            RecalculateNonSerializedStuff();
        }

        public void ProcessBeforeSerialize()
        {
            if (this.nodes.Length <= 2)
            {
                this.closed = false;
                Recalculate();
            }
        }

        public void Recalculate(float defaultEdgeSpeed = 60)
        {
            RecalculateEdgesAndLengths(defaultEdgeSpeed);
            RecalculateNonSerializedStuff();
        }

        public void RecalculateEdgesAndLengths(float defaultEdgeSpeed = 60)
        {
            if (this.nodes.Length < 2)
            {
                this.edges = new Edge[0];
                this.length = 0;
                return;
            }

            var edgesToTransfer = new Dictionary<int, Edge>();
            foreach (var edge in this.edges)
                edgesToTransfer[edge.fromIndex] = edge;

            {
                var edgeCount = this.closed ?
                    this.nodes.Length :
                    this.nodes.Length - 1;
                this.edges = new Edge[edgeCount];
                this.length = 0;
                this.duration = 0;

                for (var n = 0; n < edgeCount; n += 1)
                {
                    var edge = new Edge();
                    edge.path      = this;
                    edge.fromIndex = n;
                    edge.toIndex   = (n + 1) % this.nodes.Length;
                    edge.from      = this.nodes[edge.fromIndex];
                    edge.to        = this.nodes[edge.toIndex];

                    Vector2 delta = edge.to.Position - edge.from.Position;
                    edge.distance = delta.magnitude;
                    edge.direction = delta.normalized;

                    Edge old;
                    if (edgesToTransfer.TryGetValue(edge.fromIndex, out old) &&
                        old.toIndex == edge.toIndex)
                    {
                        edge.speedPixelsPerSecond = old.speedPixelsPerSecond;
                        edge.easing = old.easing;
                    }
                    else
                    {
                        edge.speedPixelsPerSecond = defaultEdgeSpeed;
                        edge.easing = 0;
                    }

                    this.edges[n] = edge;
                    this.length += edge.distance;
                    this.duration += edge.distance / edge.speedPixelsPerSecond;
                }

                for (var n = 0; n < this.nodes.Length; n += 1)
                {
                    this.duration += this.nodes[n].delaySeconds;
                }
            }
        }

        public void RecalculateNonSerializedStuff()
        {
            for (var n = 0; n < this.nodes.Length; n += 1)
            {
                var node = this.nodes[n];
                node.arc = null;

                if (this.nodes[n].arcLength <= 0) continue;

                var arc = new Arc();

                Edge inEdge, outEdge;

                if (n > 0)
                    inEdge = this.edges[n - 1];
                else if (this.closed)
                    inEdge = this.edges[this.edges.Length - 1];
                else
                    continue; // there's no previous edge

                if (n < this.edges.Length)
                    outEdge = this.edges[n];
                else
                    continue; // there's no next edge

                float arcLength = node.arcLength;
                // if (arcLength > inEdge.distance)  arcLength = inEdge.distance;
                // if (arcLength > outEdge.distance) arcLength = outEdge.distance;

                float angleBetween = Mathf.Acos(
                    Vector2.Dot(-inEdge.direction, outEdge.direction)
                );
                arc.circleRadius = arcLength / (Mathf.PI - angleBetween);

                Vector2 inDirection  = inEdge.direction;
                Vector2 outDirection = outEdge.direction;
                Vector2 inTangent    = new Vector2(-inDirection.y,  inDirection.x);
                Vector2 outTangent   = new Vector2(-outDirection.y, outDirection.x);

                float crossProduct = Vector2.Dot(inDirection, outTangent);
                if (crossProduct == 0) continue;
                arc.clockwise = (crossProduct > 0);
                float sign = arc.clockwise ? -1 : 1;

                var circleCenter = LineIntersection(
                    node.Position + (inTangent  * arc.circleRadius * sign),
                    node.Position + (inTangent  * arc.circleRadius * sign) + inDirection,
                    node.Position + (outTangent * arc.circleRadius * sign),
                    node.Position + (outTangent * arc.circleRadius * sign) + outDirection
                );
                if (circleCenter.HasValue == false) continue;
                arc.circleCenter = circleCenter.Value;

                arc.startDistanceAlongStraightEdge = Vector2.Dot(
                    inDirection, arc.circleCenter - inEdge.from.Position
                );
                arc.startPosition =
                    inEdge.from.Position + (inDirection * arc.startDistanceAlongStraightEdge);
                arc.startAngleRadians = Mathf.Atan2(
                    arc.startPosition.y - arc.circleCenter.y,
                    arc.startPosition.x - arc.circleCenter.x
                );

                arc.endDistanceAlongStraightEdge = Vector2.Dot(
                    outDirection, arc.circleCenter - outEdge.from.Position
                );
                arc.endPosition =
                    outEdge.from.Position + (outDirection * arc.endDistanceAlongStraightEdge);
                arc.endAngleRadians = Mathf.Atan2(
                    arc.endPosition.y - arc.circleCenter.y,
                    arc.endPosition.x - arc.circleCenter.x
                );

                if (arc.endAngleRadians < arc.startAngleRadians)
                    arc.endAngleRadians += Mathf.PI * 2;
                if (arc.clockwise == true)
                    arc.endAngleRadians -= Mathf.PI * 2;

                arc.centerAngleRadians =
                    (arc.startAngleRadians + arc.endAngleRadians) * 0.5f;

                arc.distanceAround = arc.circleRadius *
                    Mathf.Abs(arc.endAngleRadians - arc.startAngleRadians);
                arc.distanceWithoutArc = arc.endDistanceAlongStraightEdge * 2;

                node.arc = arc;
            }

            {
                float distance = 0;
                float timeSeconds = 0;

                for (int n = 0; n < nodes.Length; n += 1)
                {
                    nodes[n].distanceAlongPath = distance;
                    nodes[n].timeOfArrivalSeconds = timeSeconds;

                    timeSeconds += nodes[n].delaySeconds;

                    if (n < edges.Length)
                    {
                        distance += edges[n].distance;
                        timeSeconds += edges[n].distance / edges[n].speedPixelsPerSecond;
                    }
                }
            }

            for (int e = 0; e < edges.Length; e += 1)
            {
                var edge = edges[e];

                float realDistance = edge.distance;
                if (edge.from.arc != null)
                {
                    realDistance +=
                        edge.from.arc.distanceAround / 2 -
                        edge.from.arc.distanceWithoutArc / 2;
                }
                if (edge.to.arc != null)
                {
                    realDistance +=
                        edge.to.arc.distanceAround / 2 -
                        edge.to.arc.distanceWithoutArc / 2;
                }

                float scalingFactor = edge.distance / realDistance;

                if (edge.from.arc != null)
                {
                    edge.from.arc.endDistanceAlongPath =
                        edge.from.distanceAlongPath +
                        ((edge.from.arc.distanceAround / 2) * scalingFactor);
                }
                if (edge.to.arc != null)
                {
                    var startOfArcDistanceAroundEdge =
                        realDistance - (edge.to.arc.distanceAround / 2);
                    edge.to.arc.startDistanceAlongPath =
                        edge.from.distanceAlongPath +
                        (startOfArcDistanceAroundEdge * scalingFactor);
                }

            }
        }

        public int? NearestNodeIndexTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            int? result = null;

            for (var n = 0; n < this.nodes.Length; n += 1)
            {
                var dx = this.nodes[n].x - pt.x;
                var dy = this.nodes[n].y - pt.y;
                var sqDist = dx * dx + dy * dy;
                if (sqDist < lowestSqDist)
                {
                    lowestSqDist = sqDist;
                    result = n;
                }
            }

            return result;
        }

        public int? NearestEdgeIndexTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            int? result = null;

            for (var n = 0; n < this.edges.Length; n += 1)
            {
                var e = this.edges[n];
                var sqDist = e.SquareDistanceToPoint(pt);
                if (sqDist < lowestSqDist)
                {
                    lowestSqDist = sqDist;
                    result = n;
                }
            }

            return result;
        }

        public Path.Node NearestNodeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var index = NearestNodeIndexTo(pt, maxDistance);
            return index != null ? this.nodes[index.Value] : null;
        }

        public Path.Edge NearestEdgeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var index = NearestEdgeIndexTo(pt, maxDistance);
            return index != null ? this.edges[index.Value] : null;
        }

        public PointOnPathInfo? NearestPointTo(
            Vector2 pt, float maxDistance = Mathf.Infinity
        )
        {
            float? bestDistanceAroundPath = null;
            float lowestSqDist = maxDistance * maxDistance;

            for (int n = 0; n < this.nodes.Length; n += 1)
            {
                var node = this.nodes[n];
                if (node.arc == null)
                    continue;

                var nMinus1 = (n + this.nodes.Length - 1) % this.nodes.Length;
                var edgeBefore = this.edges[nMinus1];
                var edgeAfter = this.edges[n];

                if (Vector2.Dot(pt - node.arc.startPosition, edgeBefore.direction) < 0)
                    continue;
                if (Vector2.Dot(pt - node.arc.endPosition, edgeAfter.direction) > 0)
                    continue;

                var nearestPointRelativeToCircleCenter = 
                    ((pt - node.arc.circleCenter).normalized * node.arc.circleRadius);
                var nearestPoint = node.arc.circleCenter + nearestPointRelativeToCircleCenter;
                if ((nearestPoint - pt).sqrMagnitude >= lowestSqDist)
                    continue;

                var angle = Mathf.Atan2(
                    nearestPointRelativeToCircleCenter.y,
                    nearestPointRelativeToCircleCenter.x
                );
                while (angle <= node.arc.centerAngleRadians - Mathf.PI)
                    angle += Mathf.PI * 2;
                while (angle >  node.arc.centerAngleRadians + Mathf.PI)
                    angle -= Mathf.PI * 2;

                bool secondHalf =
                    node.arc.clockwise == true && angle < node.arc.centerAngleRadians ||
                    node.arc.clockwise == false && angle > node.arc.centerAngleRadians;

                if (secondHalf == true)
                {
                    var throughHalf =
                        (angle - node.arc.centerAngleRadians) /
                        (node.arc.endAngleRadians - node.arc.centerAngleRadians);

                    lowestSqDist = (nearestPoint - pt).sqrMagnitude;
                    bestDistanceAroundPath = Mathf.Lerp(
                        node.distanceAlongPath,
                        node.arc.endDistanceAlongPath,
                        throughHalf
                    );
                }
                else
                {
                    var throughHalf =
                        (angle - node.arc.startAngleRadians) /
                        (node.arc.centerAngleRadians - node.arc.startAngleRadians);

                    var nodeDistanceAlongPath = node.distanceAlongPath;
                    if (n == 0)
                        nodeDistanceAlongPath = this.length;

                    lowestSqDist = (nearestPoint - pt).sqrMagnitude;
                    bestDistanceAroundPath = Mathf.Lerp(
                        node.arc.startDistanceAlongPath,
                        nodeDistanceAlongPath,
                        throughHalf
                    );
                }
            }

            for (int e = 0; e < this.edges.Length; e += 1)
            {
                var edge = this.edges[e];

                Vector2 from, to;
                float fromAlongPath, toAlongPath;

                if (edge.from.arc != null)
                {
                    from          = edge.from.arc.endPosition;
                    fromAlongPath = edge.from.arc.endDistanceAlongPath;
                }
                else
                {
                    from          = edge.from.Position;
                    fromAlongPath = edge.from.distanceAlongPath;
                }

                if (edge.to.arc != null)
                {
                    to          = edge.to.arc.startPosition;
                    toAlongPath = edge.to.arc.startDistanceAlongPath;
                }
                else
                {
                    to          = edge.to.Position;
                    toAlongPath = edge.to.distanceAlongPath;

                    if (edge.toIndex == 0)
                        toAlongPath = this.length;
                }

                Vector2 delta   = to - from;
                Vector2 toPoint = pt - from;
                float along = Vector2.Dot(toPoint, delta) / delta.sqrMagnitude;
                along = Mathf.Clamp01(along);

                Vector2 nearest;
                nearest.x = from.x + (along * delta.x);
                nearest.y = from.y + (along * delta.y);
                float sqDist = (nearest - pt).sqrMagnitude;
                if (sqDist < lowestSqDist)
                {
                    lowestSqDist = sqDist;
                    bestDistanceAroundPath = Mathf.Lerp(
                        fromAlongPath, toAlongPath, along
                    );
                }
            }

            if (bestDistanceAroundPath != null)
                return PointAtDistanceAlongPath(bestDistanceAroundPath.Value);
            else
                return null;
        }

        public int NearestNodeIndexAtDistanceAlongPath(float distance)
        {
            for (var e = 0; e < this.edges.Length; e += 1)
            {
                if (distance > this.edges[e].distance)
                {
                    distance -= this.edges[e].distance;
                    continue;
                }

                if (distance < this.edges[e].distance * 0.5f)
                    return e;
                else
                    return e + 1;
            }
            return 0;
        }

        public float DistanceAtNodeIndex(int nodeIndex)
        {
            float result = 0;
            for (var n = 0; n < nodeIndex; n += 1)
                result += this.edges[n].distance;
            return result;
        }

        public float TimeAtNodeIndex(int nodeIndex)
        {
            float result = 0;
            for (var n = 0; n < nodeIndex; n += 1)
                result += this.edges[n].distance / this.edges[n].speedPixelsPerSecond;
            return result;
        }

        // Unlike node.position, if the node contains an arc then this function will
        // return the point halfway round that arc (always touching the resultant path)
        public PointOnPathInfo PointAtNodeIndex(int nodeIndex)
        {
            var node = this.nodes[nodeIndex];
            Vector2 nodeActualPoint = node.Position;

            if (node.arc != null)
            {
                var angle =
                    (node.arc.startAngleRadians + node.arc.endAngleRadians) * 0.5f;
                nodeActualPoint = new Vector2(
                    node.arc.circleCenter.x + (node.arc.circleRadius * Mathf.Cos(angle)),
                    node.arc.circleCenter.y + (node.arc.circleRadius * Mathf.Sin(angle))
                );
            }

            if (nodeIndex >= this.edges.Length)
            {
                var lastPoint = new PointOnPathInfo();
                lastPoint.edgeIndex = this.edges.Length - 1;
                lastPoint.edge = this.edges[lastPoint.edgeIndex];
                lastPoint.distanceAlongEdge = lastPoint.edge.distance;
                lastPoint.distanceAlongPath = this.length;
                lastPoint.position = nodeActualPoint;
                lastPoint.tangent = this.edges[lastPoint.edgeIndex].direction;
                lastPoint.timeSeconds = TimeAtNodeIndex(this.nodes.Length - 1);
                return lastPoint;
            }
            else
            {
                return new PointOnPathInfo
                {
                    position = nodeActualPoint,
                    edge = this.edges[nodeIndex],
                    edgeIndex = nodeIndex,
                    distanceAlongEdge = 0,
                    distanceAlongPath = DistanceAtNodeIndex(nodeIndex),
                    timeSeconds = TimeAtNodeIndex(nodeIndex)
                };
            }
        }

        public PointOnPathInfo PointThroughEdge(int edgeIndex, float through)
        {
            var edge = this.edges[edgeIndex];
            var toDistanceAlongPath = edge.to.distanceAlongPath;
            if (this.closed == true && edgeIndex == this.edges.Length - 1)
                toDistanceAlongPath = edge.from.distanceAlongPath + edge.distance;

            float distance = Mathf.Lerp(
                edge.from.distanceAlongPath, toDistanceAlongPath, through
            );
            return PointAtDistanceAlongPath(distance);
        }

        public PointOnPathInfo PointAtDistanceAlongPath(float distanceAlongPath)
        {
            if (this.closed)
            {
                distanceAlongPath %= this.length;
                if (distanceAlongPath < 0) distanceAlongPath += this.length;
            }
            else
            {
                distanceAlongPath = Mathf.Clamp(distanceAlongPath, 0, this.length);
            }

            int edgeIndex;
            Edge edge = null;

            for (edgeIndex = 0; edgeIndex < this.edges.Length; edgeIndex += 1)
            {
                edge = this.edges[edgeIndex];

                if (distanceAlongPath >= edge.from.distanceAlongPath &&
                    distanceAlongPath < edge.to.distanceAlongPath)
                {
                    break;
                }
            }
            if (edgeIndex >= this.edges.Length)
                edgeIndex = this.edges.Length - 1;

            float time;
            {
                float through =
                    (distanceAlongPath - edge.from.distanceAlongPath) /
                    (edge.to.distanceAlongPath - edge.from.distanceAlongPath);

                through = UnapplyEasing(through, edge.easing);

                time = Mathf.Lerp(
                    edge.from.timeOfArrivalSeconds + edge.from.delaySeconds,
                    edge.to.timeOfArrivalSeconds,
                    through
                );
            }

            float edgeEndDistanceAlongPath = edge.to.distanceAlongPath;
            if (this.closed == true && edge.to == this.nodes[0])
                edgeEndDistanceAlongPath = edge.from.distanceAlongPath + edge.distance;

            if (edge.from.arc != null &&
                distanceAlongPath < edge.from.arc.endDistanceAlongPath)
            {
                var arc = edge.from.arc;
                var throughHalf =
                    (distanceAlongPath - edge.from.distanceAlongPath) /
                    (arc.endDistanceAlongPath - edge.from.distanceAlongPath);

                var angleRadians = Mathf.Lerp(
                    arc.centerAngleRadians, arc.endAngleRadians, throughHalf
                );
                var cosAngle = Mathf.Cos(angleRadians);
                var sinAngle = Mathf.Sin(angleRadians);
                var position = new Vector2(
                    arc.circleCenter.x + (arc.circleRadius * cosAngle),
                    arc.circleCenter.y + (arc.circleRadius * sinAngle)
                );
                var tangent =
                    arc.clockwise ?
                    new Vector2(sinAngle, -cosAngle) :
                    new Vector2(-sinAngle, cosAngle);

                return new PointOnPathInfo
                {
                    position = position,
                    tangent = tangent,
                    distanceAlongEdge = distanceAlongPath - edge.from.distanceAlongPath,
                    distanceAlongPath = distanceAlongPath,
                    edge = this.edges[edgeIndex],
                    edgeIndex = edgeIndex,
                    timeSeconds = time
                };
            }
            else if (edge.to.arc != null &&
                distanceAlongPath > edge.to.arc.startDistanceAlongPath)
            {
                var arc = edge.to.arc;
                var throughHalf =
                    (distanceAlongPath - arc.startDistanceAlongPath) /
                    (edgeEndDistanceAlongPath - arc.startDistanceAlongPath);

                var angleRadians = Mathf.Lerp(
                    arc.startAngleRadians, arc.centerAngleRadians, throughHalf
                );
                var cosAngle = Mathf.Cos(angleRadians);
                var sinAngle = Mathf.Sin(angleRadians);
                var position = new Vector2(
                    arc.circleCenter.x + (arc.circleRadius * cosAngle),
                    arc.circleCenter.y + (arc.circleRadius * sinAngle)
                );
                var tangent =
                    arc.clockwise ?
                    new Vector2(sinAngle, -cosAngle) :
                    new Vector2(-sinAngle, cosAngle);

                return new PointOnPathInfo
                {
                    position = position,
                    tangent = tangent,
                    distanceAlongEdge = distanceAlongPath - edge.from.distanceAlongPath,
                    distanceAlongPath = distanceAlongPath,
                    edge = this.edges[edgeIndex],
                    edgeIndex = edgeIndex,
                    timeSeconds = time
                };
            }
            else
            {
                float startAlongPath, endAlongPath;
                Vector2 startPosition, endPosition;
                
                if (edge.from.arc != null)
                {
                    startPosition = edge.from.arc.endPosition;
                    startAlongPath = edge.from.arc.endDistanceAlongPath;
                }
                else
                {
                    startPosition = edge.from.Position;
                    startAlongPath = edge.from.distanceAlongPath;
                }

                if (edge.to.arc != null)
                {
                    endPosition = edge.to.arc.startPosition;
                    endAlongPath = edge.to.arc.startDistanceAlongPath;
                }
                else
                {
                    endPosition = edge.to.Position;
                    endAlongPath = edgeEndDistanceAlongPath;
                }

                float s =
                    (distanceAlongPath - startAlongPath) /
                    (endAlongPath - startAlongPath);
                var position = Vector2.Lerp(
                    startPosition, endPosition, s
                );
                return new PointOnPathInfo
                {
                    position = position,
                    tangent = edge.direction,
                    distanceAlongEdge = distanceAlongPath - edge.from.distanceAlongPath,
                    distanceAlongPath = distanceAlongPath,
                    edge = this.edges[edgeIndex],
                    edgeIndex = edgeIndex,
                    timeSeconds = time
                };
            }
        }

        public PointOnPathInfo PointAtTime(float timeSeconds)
        {
            if (this.closed)
            {
                timeSeconds %= this.duration;
                if (timeSeconds < 0) timeSeconds += this.duration;
            }
            else
            {
                timeSeconds = Mathf.Clamp(timeSeconds, 0, this.duration);
            }

            for (var n = 0; n < this.nodes.Length; n += 1)
            {
                // delay
                if (timeSeconds < this.nodes[n].delaySeconds)
                    return PointAtNodeIndex(n);

                timeSeconds -= this.nodes[n].delaySeconds;

                if (n >= this.edges.Length) continue;

                // edge
                var edgeTime =
                    this.edges[n].distance / this.edges[n].speedPixelsPerSecond;
                if (timeSeconds >= edgeTime)
                {
                    timeSeconds -= edgeTime;
                    continue;
                }

                var through = timeSeconds / edgeTime;
                through = ApplyEasing(through, this.edges[n].easing);

                return PointThroughEdge(n, through);
            }

            return new PointOnPathInfo
            {
                position = this.edges[this.edges.Length - 1].to.Position,
                tangent = this.edges[this.edges.Length - 1].direction,
                edge = this.edges[this.edges.Length - 1],
                edgeIndex = this.edges.Length - 1,
                distanceAlongEdge = this.edges[this.edges.Length - 1].distance,
                distanceAlongPath = this.length,
                timeSeconds = timeSeconds
            };
        }

        private float ApplyEasing(float valueBeforeEasing, float easingFactor)
        {
            if (easingFactor > 0.001f)
            {
                float value = valueBeforeEasing;
                value = 1 - value;
                value = Mathf.Lerp(value, value * value, easingFactor);
                value = 1 - value;
                return value;
            }
            else if (easingFactor < -0.001f)
            {
                float value = valueBeforeEasing;
                return Mathf.Lerp(value, value * value, -easingFactor);
            }
            else
            {
                return valueBeforeEasing;
            };
        }

        private float UnapplyEasing(float valueAfterEasing, float easingFactor)
        {
            // let s = original uneased value (we want this)
            //     v = value after easing     (we know this)
            //     e = easing factor          (property of edge)

            if (easingFactor < -0.001f)
            {
                // easing in (e < 0) is done by lerping a certain amount (e) to s²:
                //    v = s + (((s * s) - s) * e)
                // according to wolfram alpha, this rearranges to:
                //    s = (sqrt(e (4 v - 2) + e^2 + 1) + e - 1)/(2 e)

                float e = -easingFactor;
                float inner = (4 * e * valueAfterEasing) + (e * e) - (2 * e) + 1;
                return (Mathf.Sqrt(inner) + e - 1) / (2 * e);
            }
            else if (easingFactor > 0.001f)
            {
                // easing out (e > 0) is done by lerping to 1 - ((1-s)²)
                //    v = s + (((1 - ((1 - s) * (1 - s))) - s) * e)
                // which rearranges to
                //    s = (-sqrt(-4 e v + e^2 + 2 e + 1) + e + 1)/(2 e)

                float e = easingFactor;
                float inner = (-4 * e * valueAfterEasing) + (e * e) + (2 * e) + 1;
                return (-Mathf.Sqrt(inner) + e + 1) / (2 * e);
            }
            else
            {
                return valueAfterEasing;
            }
        }

        private static Vector2? LineIntersection(
            Vector2 a1,
            Vector2 a2,
            Vector2 b1,
            Vector2 b2
        )
        {
            var d = (a2.x - a1.x) * (b2.y - b1.y) - (a2.y - a1.y) * (b2.x - b1.x);
            if (d == 0.0f) return null;

            var u = ((b1.x - a1.x) * (b2.y - b1.y) - (b1.y - a1.y) * (b2.x - b1.x)) / d;
            return new Vector2(
                a1.x + u * (a2.x - a1.x),
                a1.y + u * (a2.y - a1.y)
            );
        }
    }
}
