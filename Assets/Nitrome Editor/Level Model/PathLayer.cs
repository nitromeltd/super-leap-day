
using UnityEngine;
using MessagePack;
using System;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public class PathLayer
    {
        [Key(0)] public string layerName;
        [Key(1)] public Path[] paths;

        public PathLayer()
        {
            this.paths = new Path[0];
        }

        public PathLayer DeepCopy(PathLayer target = null)
        {
            target = target ?? new PathLayer();
            target.layerName = this.layerName;
            target.paths     = new Path[this.paths.Length];

            for (var n = 0; n < this.paths.Length; n += 1)
            {
                target.paths[n] = this.paths[n].DeepCopy();
            }

            return target;
        }

        public Path.Node NearestNodeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Path.Node result = null;

            foreach (var p in this.paths)
            {
                foreach (var n in p.nodes)
                {
                    var dx = n.x - pt.x;
                    var dy = n.y - pt.y;
                    var sqDist = dx * dx + dy * dy;
                    if (sqDist < lowestSqDist)
                    {
                        lowestSqDist = sqDist;
                        result = n;
                    }
                }
            }

            return result;
        }

        public Path.Edge NearestEdgeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Path.Edge result = null;

            foreach (var p in this.paths)
            {
                foreach (var e in p.edges)
                {
                    var sqDist = e.SquareDistanceToPoint(pt);
                    if (sqDist < lowestSqDist)
                    {
                        lowestSqDist = sqDist;
                        result = e;
                    }
                }
            }

            return result;
        }

        public Path NearestPathTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Path result = null;

            foreach (var p in this.paths)
            {
                if (p.nodes.Length > 1)
                {
                    foreach (var e in p.edges)
                    {
                        var sqDist = e.SquareDistanceToPoint(pt);
                        if (sqDist < lowestSqDist)
                        {
                            lowestSqDist = sqDist;
                            result = p;
                        }
                    }
                }
                else if (p.nodes.Length == 1)
                {
                    var n = p.nodes[0];
                    var dx = n.x - pt.x;
                    var dy = n.y - pt.y;
                    var sqDist = dx * dx + dy * dy;
                    if (sqDist < lowestSqDist)
                    {
                        lowestSqDist = sqDist;
                        result = p;
                    }
                }
            }

            return result;
        }
    }
}
