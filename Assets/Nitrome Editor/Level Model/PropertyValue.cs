
using MessagePack;
using System;

[MessagePackObject]
[Serializable]
public struct PropertyValue
{
    [Key(0)] public int    i;
    [Key(1)] public float  f;
    [Key(2)] public bool   b;
    [Key(3)] public string s;
}
