
using UnityEngine;
using MessagePack;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public partial class Shape
    {
        public enum EdgeStyle
        {
            L, R, T, B, Cut
        }

        [MessagePackObject]
        [Serializable]
        public class EdgePiece
        {
            [Key(0)] public string spriteName;

            [Key(1)] public bool   flipVertically;
            [Key(2)] public int    edgeIndex;
            [Key(3)] public float  startPxAlongEdge;
            [Key(4)] public float  startTextureU;
            [Key(5)] public float  endPxAlongEdge;
            [Key(6)] public float  endTextureU;
        }

        [MessagePackObject]
        [Serializable]
        public class InsidePiece
        {
            [Key(0)] public int cellX; // there might be multiple pieces in the same cell
            [Key(1)] public int cellY;
            [Key(2)] public int interiorSpriteIndex;
            [Key(3)] public InsidePieceVertex[] vertices;
            [Key(4)] public int[] triangulationIndices;
        }

        [MessagePackObject]
        [Serializable]
        public struct InsidePieceVertex
        {
            [Key(0)] public float x;
            [Key(1)] public float y;
            [Key(2)] public float u;
            [Key(3)] public float v;
        }

        [MessagePackObject]
        [Serializable]
        public class Node
        {
            [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Shape shape;

            [Key(0)] public float x;
            [Key(1)] public float y;
            [Key(2)] public float xShrunk;
            [Key(3)] public float yShrunk;

            public Node() {}

            public Node(Shape shape, float x, float y)
            {
                this.shape = shape;
                this.x = x;
                this.y = y;
            }

            public Node(Shape shape, Vector2 pos)
            {
                this.shape = shape;
                this.x = pos.x;
                this.y = pos.y;
            }

            [IgnoreMember] [IgnoreDataMember] public Vector2 Position
            {
                get
                {
                    return new Vector2(x, y);
                }
                set
                {
                    x = value.x;
                    y = value.y;
                }
            }

            [IgnoreMember] [IgnoreDataMember] public Vector2 ShrunkPosition
            {
                get
                {
                    return new Vector2(xShrunk, yShrunk);
                }
                set
                {
                    xShrunk = value.x;
                    yShrunk = value.y;
                }
            }

            public Node Previous()
            {
                var thisIndex = Array.IndexOf(this.shape.nodes, this);
                if (thisIndex > 0)
                    return this.shape.nodes[thisIndex - 1];
                else
                    return this.shape.nodes[this.shape.nodes.Length - 1];
            }

            public Node Next()
            {
                var thisIndex = Array.IndexOf(this.shape.nodes, this);
                if (thisIndex < this.shape.nodes.Length - 1)
                    return this.shape.nodes[thisIndex + 1];
                else
                    return this.shape.nodes[0];
            }
        }

        // we don't actually store any edge-specific data right now;
        // this is just a helper struct so that what ShapeLayer.NearestEdgeTo
        // returns is easier to understand.
        public struct Edge
        {
            public Shape.Node a;
            public Shape.Node b;
        }

        [Key(0)] public Node        [] nodes;
        [Key(1)] public int         [] triangulationIndices;
        [Key(2)] public EdgePiece   [] edgePieces;
        [Key(3)] public InsidePiece [] insidePieces;
        [Key(4)] public int            randomSeed;

        [Key(5)] public string materialName;
        [IgnoreMember] [IgnoreDataMember] [NonSerialized] public ShapeMaterial material;

        [IgnoreMember] [IgnoreDataMember] [NonSerialized] public float     [] edgeLengths;
        [IgnoreMember] [IgnoreDataMember] [NonSerialized] public Vector2   [] edgeNormals;
        [IgnoreMember] [IgnoreDataMember] [NonSerialized] public EdgeStyle [] edgeStyles;

        public Shape()
        {
            this.nodes = new Shape.Node[0];
            this.triangulationIndices = new int[0];
            this.edgePieces = new EdgePiece[0];
            this.insidePieces = new InsidePiece[0];
            this.edgeLengths = new float[0];
            this.edgeNormals = new Vector2[0];
            this.edgeStyles = new EdgeStyle[0];
        }

        public Shape DeepCopy(Shape target = null)
        {
            target = target ?? new Shape();

            target.nodes = new Node[this.nodes.Length];
            for (int n = 0; n < this.nodes.Length; n += 1)
            {
                var node = new Node();
                node.shape = target;
                node.x = this.nodes[n].x;
                node.y = this.nodes[n].y;
                node.xShrunk = this.nodes[n].xShrunk;
                node.yShrunk = this.nodes[n].yShrunk;
                target.nodes[n] = node;
            }

            target.triangulationIndices = (int[])this.triangulationIndices.Clone();
            
            target.edgePieces = new EdgePiece[this.edgePieces.Length];
            for (int n = 0; n < this.edgePieces.Length; n += 1)
            {
                var piece = new EdgePiece();
                piece.spriteName       = this.edgePieces[n].spriteName;
                piece.flipVertically   = this.edgePieces[n].flipVertically;
                piece.edgeIndex        = this.edgePieces[n].edgeIndex;
                piece.startPxAlongEdge = this.edgePieces[n].startPxAlongEdge;
                piece.startTextureU    = this.edgePieces[n].startTextureU;
                piece.endPxAlongEdge   = this.edgePieces[n].endPxAlongEdge;
                piece.endTextureU      = this.edgePieces[n].endTextureU;
                target.edgePieces[n] = piece;
            }

            target.insidePieces = new InsidePiece[this.insidePieces.Length];
            for (int n = 0; n < this.insidePieces.Length; n += 1)
            {
                var piece = new InsidePiece();
                piece.interiorSpriteIndex =
                    this.insidePieces[n].interiorSpriteIndex;
                piece.vertices =
                    (InsidePieceVertex[])this.insidePieces[n].vertices.Clone();
                piece.triangulationIndices =
                    (int[])this.insidePieces[n].triangulationIndices.Clone();
                target.insidePieces[n] = piece;
            }

            target.randomSeed = this.randomSeed;
            target.materialName = this.materialName;
            target.material = this.material;

            target.edgeLengths = (float    [])this.edgeLengths.Clone();
            target.edgeNormals = (Vector2  [])this.edgeNormals.Clone();
            target.edgeStyles  = (EdgeStyle[])this.edgeStyles .Clone();

            return target;
        }

        public void ProcessAfterUnserialize()
        {
            foreach (var node in this.nodes)
            {
                node.shape = this;
            }

            this.edgeNormals = new Vector2  [this.nodes.Length];
            this.edgeLengths = new float    [this.nodes.Length];
            this.edgeStyles  = new EdgeStyle[this.nodes.Length];

            for (int n = 0; n < this.nodes.Length; n += 1)
            {
                var a = this.nodes[n];
                var b = this.nodes[(n + 1) % this.nodes.Length];
                var delta = b.Position - a.Position;
                var length = delta.magnitude;
                var along = delta / length;
                edgeNormals[n] = new Vector2(along.y, -along.x);
                edgeLengths[n] = length;
                edgeStyles [n] = StyleForEdgeNormal(edgeNormals[n]);
            }
        }

        public static EdgeStyle StyleForEdgeNormal(Vector2 edgeNormal)
        {
            if      (edgeNormal.x < -0.707f) return EdgeStyle.L;
            else if (edgeNormal.x >  0.707f) return EdgeStyle.R;
            else if (edgeNormal.y < -0.707f) return EdgeStyle.B;
            else                             return EdgeStyle.T;
        }

        public float Area()
        {
            float area = 0;

            for (int n0 = 0; n0 < nodes.Length; n0 += 1)
            {
                var n1 = (n0 + 1) % nodes.Length;
                var n2 = (n0 + 2) % nodes.Length;

                area +=
                    nodes[n1].x * (nodes[n2].y - nodes[n0].y) +
                    nodes[n1].y * (nodes[n0].x - nodes[n2].x);
            }

            return area / 2;
        }

        public Shape.Node NearestNodeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Shape.Node result = null;

            foreach (var n in this.nodes)
            {
                var dx = n.Position.x - pt.x;
                var dy = n.Position.y - pt.y;
                var sqDist = dx * dx + dy * dy;
                if (sqDist < lowestSqDist)
                {
                    lowestSqDist = sqDist;
                    result = n;
                }
            }

            return result;
        }

        public Shape.Edge? NearestEdgeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Shape.Edge? result = null;

            for (int n = 0; n < this.nodes.Length; n += 1)
            {
                var a = this.nodes[n];
                var b = this.nodes[(n + 1) % this.nodes.Length];
                var sqDist = EdgeSquareDistance(a.Position, b.Position, pt);
                if (sqDist < lowestSqDist)
                {
                    lowestSqDist = sqDist;
                    result = new Shape.Edge { a = a, b = b };
                }
            }

            return result;
        }

        public static float EdgeSquareDistance(Vector2 node1, Vector2 node2, Vector2 pt)
        {
            var edgeDeltaX = node2.x - node1.x;
            var edgeDeltaY = node2.y - node1.y;
            var toTargetX = pt.x - node1.x;
            var toTargetY = pt.y - node1.y;

            var along =
                (float)(toTargetX * edgeDeltaX + toTargetY * edgeDeltaY) /
                (float)(edgeDeltaX * edgeDeltaX + edgeDeltaY * edgeDeltaY);
            if (along < 0) along = 0;
            if (along > 1) along = 1;

            var nearestX = node1.x + (int)((float) edgeDeltaX * along);
            var nearestY = node1.y + (int)((float) edgeDeltaY * along);
            var toNearestX = pt.x - nearestX;
            var toNearestY = pt.y - nearestY;
            var sqDist = toNearestX * toNearestX + toNearestY * toNearestY;
            return sqDist;
        }

        public Vector2 Center()
        {
            var sum = Vector2.zero;

            foreach (var n in this.nodes)
            {
                sum.x += n.x;
                sum.y += n.y;
            }

            return sum / this.nodes.Length;
        }

        public Edge[] Edges()
        {
            var result = new Edge[nodes.Length];

            for (var n = 0; n < nodes.Length; n += 1)
            {
                result[n].a = nodes[n];
                result[n].b = nodes[(n + 1) % nodes.Length];
            }

            return result;
        }
    }
}
