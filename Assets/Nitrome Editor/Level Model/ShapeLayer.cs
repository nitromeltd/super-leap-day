
using UnityEngine;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public partial class ShapeLayer
    {
        [Key(0)] public string layerName;
        [Key(1)] public Shape [] shapes;
        [Key(2)] public Entity[] entities;

        public ShapeLayer()
        {
            this.shapes   = new Shape [0];
            this.entities = new Entity[0];
        }

        public ShapeLayer DeepCopy(ShapeLayer target = null)
        {
            target = target ?? new ShapeLayer();
            target.layerName = this.layerName;
            target.shapes    = new Shape [this.shapes.Length];
            target.entities  = new Entity[this.entities.Length];

            for (var n = 0; n < this.shapes.Length; n += 1)
            {
                target.shapes[n] = this.shapes[n].DeepCopy();
            }

            for (var n = 0; n < this.entities.Length; n += 1)
            {
                this.entities[n].shapeIndex =
                    Array.IndexOf(this.shapes, this.entities[n].shape);

                target.entities[n] = this.entities[n].DeepCopy();

                if (target.entities[n].shapeIndex >= 0)
                    target.entities[n].shape = target.shapes[target.entities[n].shapeIndex];
            }

            return target;
        }

        public static void ProcessAfterUnserialize(ShapeLayer[] shapeLayers)
        {
            // entity.spriteName -> entity->spec (EDITOR ONLY)
            // entity.shapeLayerName & entity.shapeIndex -> entity.shape

            foreach (var layer in shapeLayers)
            {
                if (layer.entities == null)
                    layer.entities = new Entity[0];
            }

            #if NITROME_EDITOR
                foreach (var shapeLayer in shapeLayers)
                {
                    for (int n = 0; n < shapeLayer.entities.Length; n += 1)
                    {
                        // we'll want to create error tiles eventually
                        // (true instead of false),
                        // but I don't want to turn it on until I've tested it properly
                        shapeLayer.entities[n].spec =
                            Config.TileByName(shapeLayer.entities[n].spriteName, false);
                    }
                }
            #endif

            foreach (var layer in shapeLayers)
            {
                foreach (var e in layer.entities)
                {
                    if (e.shapeIndex >= 0)
                        e.shape = layer.shapes[e.shapeIndex];
                    else
                        e.shape = null;
                }
            }
        }

        public static void ProcessBeforeSerialize(ShapeLayer[] shapeLayers)
        {
            // copy entity.shape -> entity.shapeLayerName & entity.shapeIndex

            var shapeIndex = new Dictionary<Shape, int>();

            for (var a = 0; a < shapeLayers.Length; a += 1)
            {
                for (var b = 0; b < shapeLayers[a].shapes.Length; b += 1)
                {
                    var shape = shapeLayers[a].shapes[b];
                    shapeIndex.Add(shape, b);
                }
            }

            for (var a = 0; a < shapeLayers.Length; a += 1)
            {
                foreach (var e in shapeLayers[a].entities)
                {
                    if (e.shape != null)
                        e.shapeIndex = shapeIndex[e.shape];
                    else
                        e.shapeIndex = -1;
                }
            }
        }

        public Shape.Node NearestNodeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Shape.Node result = null;

            foreach (var s in this.shapes)
            {
                foreach (var n in s.nodes)
                {
                    var dx = n.Position.x - pt.x;
                    var dy = n.Position.y - pt.y;
                    var sqDist = dx * dx + dy * dy;
                    if (sqDist < lowestSqDist)
                    {
                        lowestSqDist = sqDist;
                        result = n;
                    }
                }
            }

            return result;
        }

        public Shape.Edge? NearestEdgeTo(Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            Shape.Edge? result = null;

            foreach (var p in this.shapes)
            {
                for (int n = 0; n < p.nodes.Length; n += 1)
                {
                    var a = p.nodes[n];
                    var b = p.nodes[(n + 1) % p.nodes.Length];
                    var sqDist = Shape.EdgeSquareDistance(a.Position, b.Position, pt);
                    if (sqDist < lowestSqDist)
                    {
                        lowestSqDist = sqDist;
                        result = new Shape.Edge { a = a, b = b };
                    }
                }
            }

            return result;
        }

        public Shape ShapeAt(Vector2 pt)
        {
            foreach (var s in this.shapes)
            {
                var shapePoints = s.nodes.Select(n => n.Position).ToArray();
                if (PointInsidePolygon.Calculate(shapePoints, pt))
                {
                    return s;
                }
            }

            return null;
        }

        public Shape NearestShapeTo(
            Vector2 pt,
            float maxDistance = float.PositiveInfinity
        )
        {
            foreach (var s in this.shapes)
            {
                var shapePoints = s.nodes.Select(n => n.Position).ToArray();
                if (PointInsidePolygon.Calculate(shapePoints, pt))
                {
                    return s;
                }
            }

            Shape bestShape = null;
            float bestSqrDistance = maxDistance * maxDistance;

            foreach (var s in this.shapes)
            {
                for (var n = 0; n < s.nodes.Length; n += 1)
                {
                    var a = s.nodes[n].Position;
                    var b = s.nodes[(n + 1) % s.nodes.Length].Position;
                    var vec = b - a;
                    var along = Vector2.Dot(vec, pt - a) / vec.sqrMagnitude;
                    var nearest = Vector2.Lerp(a, b, along);
                    var sqDist = (nearest - pt).sqrMagnitude;

                    if (sqDist < bestSqrDistance)
                    {
                        bestSqrDistance = sqDist;
                        bestShape = s;
                    }
                }
            }

            return bestShape;
        }
    }
}
