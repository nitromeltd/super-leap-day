
using UnityEngine;
using MessagePack;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NitromeEditor
{
    [MessagePackObject]
    [Serializable]
    public class TileLayer
    {
        [MessagePackObject]
        [Serializable]
        public struct Tile
        {
            #if NITROME_EDITOR
                [IgnoreMember] [IgnoreDataMember] [NonSerialized] public TileSpec tileSpec;
            #endif

            [Key(0)] public string tile;
            [Key(1)] public float offsetX;
            [Key(2)] public float offsetY;
            [Key(3)] public bool flip;
            [Key(4)] public int rotation;
            [Key(5)] public Dictionary<string, PropertyValue> properties;
        }

        [Key(0)] public string layerName;
        [Key(1)] public Tile[] tiles;

        public TileLayer()
        {
            this.tiles = new Tile[1];
        }

        public void DeepCopy(TileLayer target = null)
        {
            target = target ?? new TileLayer();
            target.layerName = this.layerName;
            target.tiles     = (Tile[])this.tiles.Clone();
        }
    }
}
