
#import <Foundation/Foundation.h>


NSTimer* syncTimer = nil;
NSDictionary* lastCopyOfPlayerPrefs = nil;


@interface CloudSaving : NSObject
+ (void)start;
@end


void CloudSaving_Start(void)
{
    [CloudSaving start];
}


@implementation CloudSaving

+ (void)start
{
    if (NSClassFromString(@"NSUbiquitousKeyValueStore") == nil)
        return;

    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore defaultStore];
    if (store == nil)
        return;
    
    lastCopyOfPlayerPrefs = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];

    [self startSyncing];
    [store synchronize];
}

+ (void)startSyncing
{
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(iCloudChanged:)
                   name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                 object:[NSUbiquitousKeyValueStore defaultStore]];
    [center addObserver:self
               selector:@selector(playerPrefsChanged:)
                   name:NSUserDefaultsDidChangeNotification
                 object:nil];
}

+ (void)stopSyncing
{
    NSNotificationCenter* center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self
                      name:NSUbiquitousKeyValueStoreDidChangeExternallyNotification
                    object:nil];
    [center removeObserver:self
                      name:NSUserDefaultsDidChangeNotification
                    object:nil];
}

+ (void)dealloc
{
    [self stopSyncing];
}

NSString* BestRecordForDay(NSString* recordForDay1, NSString* recordForDay2)
{
    NSArray* parts1 = [recordForDay1 componentsSeparatedByString:@" "];
    NSArray* parts2 = [recordForDay2 componentsSeparatedByString:@" "];
    
    if ([parts1 count] != 3) return recordForDay2;
    if ([parts2 count] != 3) return recordForDay1;
    
    int trophy1 = [[parts1 objectAtIndex:0] intValue];
    int trophy2 = [[parts2 objectAtIndex:0] intValue];
    if (trophy1 > trophy2) return recordForDay1;
    if (trophy2 > trophy1) return recordForDay2;
    
    float time1 = [[parts1 objectAtIndex:1] floatValue];
    float time2 = [[parts2 objectAtIndex:1] floatValue];
    float time = (time1 < time2) ? time1 : time2;
    
    int deaths1 = [[parts1 objectAtIndex:2] intValue];
    int deaths2 = [[parts2 objectAtIndex:2] intValue];
    int deaths = (deaths1 < deaths2) ? deaths1 : deaths2;
    
    return [NSString stringWithFormat:@"%i %.02f %i", trophy1, time, deaths];
}

NSString* MergeRecords(NSString* records1, NSString* records2)
{
    NSArray* split1 = [records1 componentsSeparatedByString:@"|"];
    NSArray* split2 = [records2 componentsSeparatedByString:@"|"];
    int length = ([split1 count] > [split2 count]) ? (int)[split1 count] : (int)[split2 count];
    
    NSMutableArray* resultDays = [NSMutableArray array];

    for (int n = 0; n < length; n += 1)
    {
        if (n >= [split1 count])
            [resultDays addObject:[split2 objectAtIndex:n]];
        else if (n >= [split2 count])
            [resultDays addObject:[split1 objectAtIndex:n]];
        else
        {
            NSString* record1 = [split1 objectAtIndex:n];
            NSString* record2 = [split2 objectAtIndex:n];
            [resultDays addObject:BestRecordForDay(record1, record2)];
        }
    }

    return [resultDays componentsJoinedByString:@"|"];
}

+ (void)copyPlayerPrefsToICloud
{
    NSDictionary* playerPrefsDictionary =
        [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore defaultStore];
    
    [playerPrefsDictionary enumerateKeysAndObjectsUsingBlock:
        ^(NSString* key, id obj, BOOL* stop)
        {
            if ([key isEqualToString:@"levelPatch"])
            {
                // do not copy into cloud storage; just load on every device
            }
            else if ([lastCopyOfPlayerPrefs[key] isEqual:obj])
            {
                // entry has not changed since last read
            }
            else if (
                [key hasPrefix:@"slot1_recordFor"] ||
                [key hasPrefix:@"slot2_recordFor"] ||
                [key hasPrefix:@"slot3_recordFor"]
            )
            {
                NSString* merged = MergeRecords(obj, [store stringForKey:key]);
                [store setObject:merged forKey:key];
            }
            else
            {
                [store setObject:obj forKey:key];
            }
        }
    ];

    NSArray* keysInICloud = [[store dictionaryRepresentation] allKeys];
    NSArray* keysInPlayerPrefs = [playerPrefsDictionary allKeys];
    
    for (NSString* string in keysInICloud)
    {
        if (![keysInPlayerPrefs containsObject:string])
        {
            [store removeObjectForKey:string];
        }
    }
    
    [store synchronize];
    syncTimer = nil;
    lastCopyOfPlayerPrefs = playerPrefsDictionary;
}

+ (void)playerPrefsChanged:(NSNotification*)notification
{
    if (syncTimer != nil && [syncTimer isValid])
        [syncTimer invalidate];
    
    syncTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                 target:self
                                               selector:@selector(copyPlayerPrefsToICloud)
                                               userInfo:nil
                                                repeats:false];
}

+ (void)iCloudChanged:(NSNotification*)notification
{
    [self stopSyncing];

    NSUserDefaults* playerPrefs = [NSUserDefaults standardUserDefaults];
    NSUbiquitousKeyValueStore* store = [NSUbiquitousKeyValueStore defaultStore];
    NSDictionary* storeDictionary = [store dictionaryRepresentation];

    [storeDictionary enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL* stop)
    {
        if ([key hasPrefix:@"slot1_recordFor"] ||
            [key hasPrefix:@"slot2_recordFor"] ||
            [key hasPrefix:@"slot3_recordFor"])
        {
            NSString* merged = MergeRecords(obj, [playerPrefs stringForKey:key]);
            [playerPrefs setObject:merged forKey:key];
        }
        else
        {
            [playerPrefs setObject:obj forKey:key];
        }
    }];
    
    [playerPrefs synchronize];
    [self startSyncing];
}

@end
