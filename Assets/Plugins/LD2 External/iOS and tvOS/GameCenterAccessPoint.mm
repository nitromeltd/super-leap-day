#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#include <Availability.h>
#include <TargetConditionals.h>

@interface GameCenterAccessPoint : NSObject

+(void) showAccessPoint: (int) location;
+(CGRect) getAccessPointFrameInScreenCoord;
+(bool) isPresentingGameCenter;
+(void) trigger;
+(bool) isVisible;
+(bool) isFocused;
+(void) changeFocus: (bool)focused;
@end

@implementation GameCenterAccessPoint


id __delegate = nil;

+(void)showAccessPoint:(int) location{
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        GKAccessPoint.shared.location = (GKAccessPointLocation)location;
        GKAccessPoint.shared.active = true;
    }
}

+(void) hideAccessPoint{
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        GKAccessPoint.shared.active = false;
    }
}

+(CGRect)getAccessPointFrameInScreenCoord{
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        return GKAccessPoint.shared.frameInScreenCoordinates;
    }
    else {
        return CGRectMake(0,0,0,0);
    }
}

+(void) showHighlights: (bool)visible {
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        GKAccessPoint.shared.showHighlights = visible;
    }
}

+(void) GKTriggerHandler {

}

+(bool) isPresentingGameCenter {
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        return GKAccessPoint.shared.isPresentingGameCenter;
    }
    else {
        return false;
    }
}

+(bool) isVisible {
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        return GKAccessPoint.shared.isVisible;
    }
    else {
        return false;
    }
}

+(void) trigger {
    if(@available(iOS 14, tvOS 14.0, macOS 11.0, *)){
        [[GKAccessPoint shared] triggerAccessPointWithHandler:^{
            
        }];
    }
}

+(bool) isFocused {
#if TARGET_OS_TV
    return GKAccessPoint.shared.focused;
#else
    return false;
#endif
}

+(void) changeFocus: (bool)focused {
#if TARGET_OS_TV
    GKAccessPoint.shared.focused = focused;
#endif
}
@end

extern "C"
{

    void accessPoint_Show(int location)
    {
        [GameCenterAccessPoint showAccessPoint:location];
    }

    void accessPoint_Hide(bool visible, int location)
    {
        [GameCenterAccessPoint hideAccessPoint];
    }

    CGRect accessPoint_getAccessPointFrameInScreenCoord()
    {
        CGRect _frame = [GameCenterAccessPoint getAccessPointFrameInScreenCoord];
        return _frame;
    }

    void accessPoint_showHighlights(bool visible)
    {
        [GameCenterAccessPoint showHighlights:visible];
    }

    bool accessPoint_isPresentingGameCenter()
    {
        return [GameCenterAccessPoint isPresentingGameCenter];

    }
    
    void accessPoint_trigger() {
        [GameCenterAccessPoint trigger];
    }

    bool accessPoint_isVisible() {
        return [GameCenterAccessPoint isVisible];
    }

    bool accessPoint_isFocused() {
        return [GameCenterAccessPoint isFocused];
    }
    
    void accessPoint_changeFocus(bool focused) {
        [GameCenterAccessPoint changeFocus:focused];
    }
}
