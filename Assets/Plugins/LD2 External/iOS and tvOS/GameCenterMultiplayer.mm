#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>


typedef void (*CallbackMatchmakerDidFindMatch)(const char* thisPlayerId, const char* remotePlayerIds);
typedef void (*CallbackMatchmakerDidFailWithError)();
typedef void (*CallbackMatchmakerPopupCancelled)();
typedef void (*CallbackPlayerDidAcceptInvite)(const char* playerId);
typedef void (*CallbackPlayerDidChangeConnectionState)(const char* playerId, GKPlayerConnectionState state);
typedef void (*CallbackReceivedDataFromPlayer)(const char* playerId, const uint8_t* bytes, int length);


@protocol GKMatchmakerViewControllerDelegate;

@interface Multiplayer : NSObject
    <GKMatchmakerViewControllerDelegate, GKMatchDelegate, GKLocalPlayerListener>
{
    UIViewController* parentViewController;
    GKMatchmakerViewController* matchmakerViewController;
}
@property (nonatomic, assign) CallbackMatchmakerDidFindMatch callbackMatchmakerDidFindMatch;
@property (nonatomic, assign) CallbackMatchmakerDidFailWithError callbackMatchmakerDidFailWithError;
@property (nonatomic, assign) CallbackMatchmakerPopupCancelled callbackMatchmakerPopupCancelled;
@property (nonatomic, assign) CallbackPlayerDidAcceptInvite callbackPlayerDidAcceptInvite;
@property (nonatomic, assign) CallbackPlayerDidChangeConnectionState callbackPlayerDidChangeConnectionState;
@property (nonatomic, assign) CallbackReceivedDataFromPlayer callbackReceivedDataFromPlayer;
@property (nonatomic, retain) GKMatch* currentMatch;
- (void)presentMatchmaker;
- (void)end;
@end

Multiplayer* multiplayer = nil;

@implementation Multiplayer

- (id)init
{
    if (self = [super init])
    {
        [GKLocalPlayer.local registerListener:self];
        self->parentViewController = UnityGetGLViewController();
    }
    return self;
}

- (void)presentMatchmaker
{
    if (GKLocalPlayer.local.isAuthenticated == false) return;

    GKMatchRequest* request = [[GKMatchRequest alloc] init];
    request.minPlayers = 2;
    request.maxPlayers = 4;

    GKMatchmakerViewController* vc =
        [[GKMatchmakerViewController alloc] initWithMatchRequest:request];
    vc.matchmakerDelegate = self;
    self->matchmakerViewController = vc;
    [self->parentViewController presentViewController:vc animated:NO completion:nil];
}

- (void)end
{
    if (GKLocalPlayer.local.isAuthenticated == false) return;
    if (self.currentMatch == nil) return;

    [self.currentMatch disconnect];
    self.currentMatch = nil;
}

- (void)matchmakerViewController:(GKMatchmakerViewController *)viewController didFindMatch:(GKMatch *)match
{
    dispatch_async(dispatch_get_main_queue(), ^{
        match.delegate = self;
        self.currentMatch = match;
        [self->matchmakerViewController dismissViewControllerAnimated:YES completion:nil];

        NSString* thisPlayerId = GKLocalPlayer.local.gamePlayerID;

        NSMutableString* allPlayerIds = [NSMutableString string];
        bool first = true;
        for (GKPlayer* player in match.players)
        {
            if (first == false)
                [allPlayerIds appendString:@";"];

            first = false;
            [allPlayerIds appendString:(player.gamePlayerID)];
        }

        if (self.callbackMatchmakerDidFindMatch != nullptr)
            self.callbackMatchmakerDidFindMatch([thisPlayerId UTF8String], [allPlayerIds UTF8String]);
    });
}

- (void)matchmakerViewController:(nonnull GKMatchmakerViewController *)viewController didFailWithError:(nonnull NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->matchmakerViewController dismissViewControllerAnimated:YES completion:nil];

        if (self.callbackMatchmakerDidFailWithError != nullptr)
            self.callbackMatchmakerDidFailWithError();
    });
}

- (BOOL)match:(GKMatch *)match shouldReinviteDisconnectedPlayer:(GKPlayer *)player
{
    return YES;
}

- (void)matchmakerViewControllerWasCancelled:(nonnull GKMatchmakerViewController *)viewController
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->matchmakerViewController dismissViewControllerAnimated:YES completion:nil];

        if (self.callbackMatchmakerPopupCancelled != nullptr)
            self.callbackMatchmakerPopupCancelled();
    });
}

- (void)match:(GKMatch *)match player:(GKPlayer *)player didChangeConnectionState:(GKPlayerConnectionState)state
{
    dispatch_async(dispatch_get_main_queue(), ^{
        const char* playerId = [[player gamePlayerID] UTF8String];

        if (self.callbackPlayerDidChangeConnectionState != nullptr)
            self.callbackPlayerDidChangeConnectionState(playerId, state);
    });
}

- (void)match:(GKMatch *)match didReceiveData:(NSData *)data fromRemotePlayer:(GKPlayer *)player
{
    dispatch_async(dispatch_get_main_queue(), ^{
        const char* playerId = [[player gamePlayerID] UTF8String];
        const uint8_t* bytes = (const uint8_t*)[data bytes];
        int length = (int)[data length];

        if (self.callbackReceivedDataFromPlayer != nil)
            self.callbackReceivedDataFromPlayer(playerId, bytes, length);
    });
}

- (void)player:(GKPlayer *)player didAcceptInvite:(GKInvite *)invite
{
    dispatch_async(dispatch_get_main_queue(), ^{
        GKMatchmakerViewController* vc =
            [[GKMatchmakerViewController alloc] initWithInvite:invite];
        vc.matchmakerDelegate = self;
        self->matchmakerViewController = vc;
        [self->parentViewController presentViewController:vc animated:NO completion:nil];

        const char* playerId = [[player gamePlayerID] UTF8String];
        if (self.callbackPlayerDidAcceptInvite != nil)
            self.callbackPlayerDidAcceptInvite(playerId);
    });
}

@end

extern "C"
{
    void GCMultiplayer_Init(
        CallbackMatchmakerDidFindMatch         callbackMatchmakerDidFindMatch,
        CallbackMatchmakerDidFailWithError     callbackMatchmakerDidFailWithError,
        CallbackMatchmakerPopupCancelled       callbackMatchmakerPopupCancelled,
        CallbackPlayerDidAcceptInvite          callbackPlayerDidAcceptInvite,
        CallbackPlayerDidChangeConnectionState callbackPlayerDidChangeConnectionState,
        CallbackReceivedDataFromPlayer         callbackReceivedDataFromPlayer
    )
    {
        multiplayer = [[Multiplayer alloc] init];
        multiplayer.callbackMatchmakerDidFindMatch         = callbackMatchmakerDidFindMatch;
        multiplayer.callbackMatchmakerDidFailWithError     = callbackMatchmakerDidFailWithError;
        multiplayer.callbackMatchmakerPopupCancelled       = callbackMatchmakerPopupCancelled;
        multiplayer.callbackPlayerDidAcceptInvite          = callbackPlayerDidAcceptInvite;
        multiplayer.callbackPlayerDidChangeConnectionState = callbackPlayerDidChangeConnectionState;
        multiplayer.callbackReceivedDataFromPlayer         = callbackReceivedDataFromPlayer;
    }

    void GCMultiplayer_PresentMatchmaker()
    {
        [multiplayer presentMatchmaker];
    }

    void GCMultiplayer_SendData(uint8_t* bytes, int length, bool reliable)
    {
        if (multiplayer == nil) return;
        if (multiplayer.currentMatch == nil) return;
        
        NSData* nsdata = [NSData dataWithBytes:bytes length:length];
        auto dataMode = reliable ? GKMatchSendDataReliable : GKMatchSendDataUnreliable;

        [multiplayer.currentMatch sendDataToAllPlayers:nsdata
                                          withDataMode:dataMode
                                                 error:nil];
    }

    void GCMultiplayer_SendDataToPlayer(
        const char* toPlayerId, uint8_t* bytes, int length, bool reliable
    )
    {
        if (multiplayer == nil) return;
        if (multiplayer.currentMatch == nil) return;
        
        NSData* nsdata = [NSData dataWithBytes:bytes length:length];
        auto dataMode = reliable ? GKMatchSendDataReliable : GKMatchSendDataUnreliable;

        NSString* playerIdAsNSString = [NSString stringWithUTF8String:toPlayerId];
        GKPlayer* player = nil;
        for (GKPlayer* p in multiplayer.currentMatch.players)
        {
            if ([p.gamePlayerID isEqualToString:playerIdAsNSString])
            {
                player = p;
                break;
            }
        }

        [multiplayer.currentMatch sendData:nsdata
                                 toPlayers:@[ player ]
                                  dataMode:dataMode
                                    error:nil];
    }

    void GCMultiplayer_End()
    {
        [multiplayer end];
    }
}
