#import <GameKit/GameKit.h>

typedef void (*GameCenter_LoadLeaderboardCallback)(const char*);
GameCenter_LoadLeaderboardCallback _gameCenterLoadLeaderboardCallbackHandler = NULL;
void gameCenter_setLoadLeaderboardCallbackHandler(GameCenter_LoadLeaderboardCallback handler);
void gameCenter_loadLeaderboard(const char* leaderboardId);

typedef void (*GameCenter_SubmitScoreLeaderboardCallback)(const char*);
GameCenter_SubmitScoreLeaderboardCallback _gameCenterSubmitScoreLeaderboardCallbackHandler = NULL;
void gameCenter_setSubmitScoreLeaderboardCallbackHandler(GameCenter_SubmitScoreLeaderboardCallback handler);
void gameCenter_submitScoreLeaderboard(const int submittedScore, const char* leaderboardId);

enum GameCenterLoadLeaderboardError
{
    GCLoadLeaderboard_NoError = 0,
    GCLoadLeaderboard_CallbackHandlerNotSet,
    GCLoadLeaderboard_ServiceError,
    GCLoadLeaderboard_LeaderboardNotFound
};

NSString* makeResultString(NSMutableDictionary* dictionary)
{
    NSData* json = [NSJSONSerialization dataWithJSONObject:dictionary
                                                    options:NSJSONWritingSortedKeys
                                                        error:nil];
    
    return [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
}

void gameCenter_setLoadLeaderboardCallbackHandler(GameCenter_LoadLeaderboardCallback handler)
{
    _gameCenterLoadLeaderboardCallbackHandler = handler;
}

void gameCenter_setSubmitScoreLeaderboardCallbackHandler(GameCenter_SubmitScoreLeaderboardCallback handler)
{
    _gameCenterSubmitScoreLeaderboardCallbackHandler = handler;
}

void gameCenter_loadLeaderboard(const char* leaderboardId)
{
    if(_gameCenterLoadLeaderboardCallbackHandler == NULL)
    {
        NSMutableDictionary* result = [NSMutableDictionary dictionary];
        result[@"LeaderboardId"] = [NSString stringWithUTF8String:leaderboardId];
        result[@"Sucess"] = [NSNumber numberWithBool:false];
        result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_CallbackHandlerNotSet];
        _gameCenterLoadLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
        return;
    }

    if (@available(iOS 14.0, tvOS 14.0, macOS 11.0, *)) {
        [GKLeaderboard loadLeaderboardsWithIDs:@[[NSString stringWithUTF8String:leaderboardId]]
                             completionHandler:^(NSArray<GKLeaderboard *> *leaderboards, NSError *error) {
            if(error)
            {
                NSMutableDictionary* result = [NSMutableDictionary dictionary];
                result[@"LeaderboardId"] = [NSString stringWithUTF8String:leaderboardId];
                result[@"Success"] = [NSNumber numberWithBool:false];
                result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_ServiceError];
                result[@"ErrorMessage"] = [error localizedDescription];
                _gameCenterLoadLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
                return;
            }
            
            if(leaderboards.count > 0)
            {
                GKLeaderboard* leaderboard = [leaderboards firstObject];
                [leaderboard loadEntriesForPlayerScope:GKLeaderboardPlayerScopeGlobal
                                             timeScope:GKLeaderboardTimeScopeAllTime
                                                 range:NSMakeRange(1, 100)
                                     completionHandler:^(GKLeaderboardEntry * _Nullable localPlayerEntry, NSArray<GKLeaderboardEntry *> * _Nullable entries, NSInteger totalPlayerCount, NSError * _Nullable error)
                 {
                    NSMutableDictionary* result = [NSMutableDictionary dictionary];
                    result[@"LeaderboardId"] = leaderboard.baseLeaderboardID;
                    
                    if(error)
                    {
                        result[@"Success"] = [NSNumber numberWithBool:false];
                        result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_ServiceError];
                        result[@"ErrorMessage"] = [error localizedDescription];
                    }
                    else
                    {
                        result[@"Success"] = [NSNumber numberWithBool:true];
                        result[@"TotalPlayerCount"] = [NSNumber numberWithLong:totalPlayerCount];
                        
                        if(localPlayerEntry)
                        {
                            result[@"LocalPlayerScore"] = [NSNumber numberWithLong:localPlayerEntry.score];
                            result[@"LocalPlayerRank"] = [NSNumber numberWithLong:localPlayerEntry.rank];
                        }
                        
                        if(entries)
                        {
                            NSMutableArray* leaderboardEntries = [NSMutableArray array];
                            for(GKLeaderboardEntry* entry in entries)
                            {
                                NSMutableDictionary* entryDict = [NSMutableDictionary dictionary];
                                entryDict[@"PlayerIdentifier"] = entry.player.alias;
                                entryDict[@"Rank"] = [NSNumber numberWithLong:entry.rank];
                                entryDict[@"Score"] = [NSNumber numberWithLong:entry.score];
                                [leaderboardEntries addObject:entryDict];
                            }
                            result[@"Entries"] = leaderboardEntries;
                        }
                    }
                    
                    _gameCenterLoadLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
                }];
            }
            else
            {
                NSMutableDictionary* result = [NSMutableDictionary dictionary];
                result[@"LeaderboardId"] = [NSString stringWithUTF8String:leaderboardId];
                result[@"Success"] = [NSNumber numberWithBool:false];
                result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_LeaderboardNotFound];
                _gameCenterLoadLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
            }
        }];
    } else {
        // Fallback on earlier versions
    }
}

void gameCenter_submitScoreLeaderboard(const int submittedScore, const char* leaderboardId)
{
    if(_gameCenterSubmitScoreLeaderboardCallbackHandler == NULL)
    {
        NSMutableDictionary* result = [NSMutableDictionary dictionary];
        result[@"LeaderboardId"] = [NSString stringWithUTF8String:leaderboardId];
        result[@"Success"] = [NSNumber numberWithBool:false];
        result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_CallbackHandlerNotSet];
        _gameCenterSubmitScoreLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
        return;
    }
    
    if (@available(iOS 14.0, tvOS 14.0, macOS 11.0, *)) {
        [GKLeaderboard submitScore:submittedScore context:0 player:GKLocalPlayer.local leaderboardIDs:@[[NSString stringWithUTF8String:leaderboardId]]
                 completionHandler:^(NSError *error) {
            if(error)
            {
                NSMutableDictionary* result = [NSMutableDictionary dictionary];
                result[@"Success"] = [NSNumber numberWithBool:false];
                result[@"Error"] = [NSNumber numberWithInt:GCLoadLeaderboard_ServiceError];
                result[@"ErrorMessage"] = [error localizedDescription];
                _gameCenterSubmitScoreLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
                return;
            }
            else
            {
                NSMutableDictionary* result = [NSMutableDictionary dictionary];
                result[@"Success"] = [NSNumber numberWithBool:true];
                _gameCenterSubmitScoreLeaderboardCallbackHandler([makeResultString(result) UTF8String]);
                return;
            }
        }];
    } else {
        // Fallback on earlier versions
    }
}
