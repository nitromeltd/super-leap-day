using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSpriteMaskForWindySkies : MonoBehaviour
{
    public float alphaCutoff = 0.2f;
    void Start()
    {
        if (Map.instance.theme == Theme.WindySkies)
        {
            if (GetComponent<SpriteMask>() == null)
            {
                var newComponent = this.gameObject.AddComponent<SpriteMask>();
                newComponent.isCustomRangeActive = true;
                newComponent.backSortingLayerID = newComponent.frontSortingLayerID = SortingLayer.NameToID("Silhouette");
                newComponent.frontSortingOrder = 10;
                newComponent.backSortingOrder = 0;
                newComponent.alphaCutoff = this.alphaCutoff;
            }
        }
        else
        {
            this.enabled = false;
        }
    }

    void Update()
    {
        var spriteMask = GetComponent<SpriteMask>();
        var spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteMask != null && spriteRenderer != null)
        {
            if (spriteRenderer.enabled == true)
                spriteMask.sprite = spriteRenderer.sprite;
            else
                spriteMask.sprite = null;
        }
    }
}
