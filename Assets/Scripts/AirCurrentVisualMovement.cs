using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirCurrentVisualMovement : MonoBehaviour
{
    public Transform topParentTransform;
    public Transform headingParentTransform;
    public Transform movingTrailTransform;

    private Vector2 initialTrailPosition;
    private float timer;
    private Vector2 previousPosition;

    private const float Amplitude = 0.50f;
    private const float Period = 0.08f;

    private void Start()
    {
        this.initialTrailPosition = this.movingTrailTransform.localPosition;
    }

    public void Advance()
    {
        this.timer += Time.deltaTime;

        float theta = this.timer / Period;
        float distance = Amplitude * Mathf.Sin(theta);

        this.movingTrailTransform.localPosition =
            this.initialTrailPosition + Vector2.right * distance;

        Vector2 heading = (Vector2)this.topParentTransform.position - this.previousPosition;
        Vector2 direction = heading / heading.magnitude;
        this.headingParentTransform.up = direction;

        this.previousPosition = this.topParentTransform.position;
    }
}
