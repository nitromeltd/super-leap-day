using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

public class Animated : MonoBehaviour
{
    private static HashSet<Animated> allRunning = new HashSet<Animated>();

    [Serializable]
    public class Animation
    {
        public Sprite[] sprites;
        public int fps = 30;

        public bool IsEmpty() => sprites.Length == 0;

        public Animation Reversed() => new Animation
        {
            sprites = Enumerable.Reverse(this.sprites).ToArray(),
            fps = this.fps
        };
    }

    private class FrameAction
    {
        public int frameNumber;
        public Action action;
    }

    private SpriteRenderer spriteRenderer;
    private Image image;

    [NonSerialized] public Animation currentAnimation;
    [NonSerialized] public int frameNumber;
    [NonSerialized] public float playbackSpeed = 1;
    [NonSerialized] public bool loop;
    [NonSerialized] public bool holdLastFrame;
    [NonSerialized] public Action onComplete;

    private float timeThroughFrame = 0.0f;
    private bool animationJustStarted = false;
    private bool nextAnimationStarted = false;
    private List<FrameAction> frameActions = new List<FrameAction>();

    private void OnEnable()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.image = GetComponent<Image>();
        allRunning.Add(this);
    }
    private void OnDisable()
    {
        allRunning.Remove(this);
    }

    public void PlayAndLoop(Animation animation)
    {
        if (animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        if (animation != this.currentAnimation)
        {
            this.currentAnimation = animation;
            this.frameNumber = 0;
            this.playbackSpeed = 1;
            this.loop = true;
            this.holdLastFrame = false;
            this.onComplete = null;
            this.timeThroughFrame = 0;
            this.animationJustStarted = true;
            this.frameActions.Clear();

            if (this.spriteRenderer != null)
                this.spriteRenderer.sprite = animation.sprites[0];
            if (this.image != null)
                this.image.sprite = animation.sprites[0];

            this.nextAnimationStarted = true;
        }
    }

    public void PlayAndHoldLastFrame(Animation animation)
    {
        if (animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        if (animation != this.currentAnimation)
        {
            this.currentAnimation = animation;
            this.frameNumber = 0;
            this.playbackSpeed = 1;
            this.loop = false;
            this.holdLastFrame = true;
            this.onComplete = null;
            this.timeThroughFrame = 0;
            this.animationJustStarted = true;
            this.frameActions.Clear();

            if (this.spriteRenderer != null)
                this.spriteRenderer.sprite = animation.sprites[0];
            if (this.image != null)
                this.image.sprite = animation.sprites[0];

            this.nextAnimationStarted = true;
        }
    }

    public void PlayOnce(Animation animation, Action onComplete = null)
    {
        if (animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        this.currentAnimation = animation;
        this.frameNumber = 0;
        this.playbackSpeed = 1;
        this.loop = false;
        this.holdLastFrame = false;
        this.onComplete = onComplete;
        this.timeThroughFrame = 0;
        this.animationJustStarted = true;
        this.frameActions.Clear();

        if (this.spriteRenderer != null)
            this.spriteRenderer.sprite = animation.sprites[0];
        if (this.image != null)
            this.image.sprite = animation.sprites[0];

        this.nextAnimationStarted = true;
    }

    public void Stop()
    {
        this.currentAnimation = null;
        this.frameNumber = 0;
        this.playbackSpeed = 1;
        this.loop = false;
        this.holdLastFrame = false;
        this.onComplete = null;
        this.timeThroughFrame = 0;
        this.frameActions.Clear();
        this.nextAnimationStarted = true;
    }

    // this callback might be fired more than once, if the animation loops.
    public void OnFrame(int frameNumberZeroBased, Action action)
    {
        this.frameActions.Add(
            new FrameAction { frameNumber = frameNumberZeroBased, action = action }
        );
    }

    public void Synchronize(Map map, int offset = 0)
    {
        if (this.currentAnimation == null) return;

        offset *= Mathf.RoundToInt(60.0f / this.currentAnimation.fps);
        var sequenceDurationFrames =
            this.currentAnimation.sprites.Length * 60.0f / this.currentAnimation.fps;
        var timeThroughSequence =
            (map.frameNumber + offset) % sequenceDurationFrames;
        var throughSequence =
            timeThroughSequence *
            this.currentAnimation.sprites.Length /
            sequenceDurationFrames;

        this.frameNumber = Mathf.FloorToInt(throughSequence);
        this.timeThroughFrame = throughSequence - this.frameNumber;
    }

    private static List<Animated> allRunningCopy = new();
    public static void AdvanceAll(float deltaTime)
    {
        allRunningCopy.Clear();
        allRunningCopy.AddRange(allRunning);

        foreach (var a in allRunningCopy)
            a.Advance(deltaTime);
    }

    private void Advance(float deltaTime)
    {
        if (this.currentAnimation == null) return;
        if (this.spriteRenderer == null && this.image == null) return;

        if (this.animationJustStarted == true)
        {
            // since Animated stuff is the last thing to Advance,
            // we probably want to hold that timer one frame before starting off
            this.animationJustStarted = false;
        }
        else
        {
            float time = this.currentAnimation.fps * deltaTime;
            this.timeThroughFrame += time * this.playbackSpeed;
        }

        int framesToAdvance = Mathf.FloorToInt(this.timeThroughFrame);
        this.frameNumber += framesToAdvance;
        this.timeThroughFrame -= framesToAdvance;

        if (this.loop)
        {
            this.frameNumber %= this.currentAnimation.sprites.Length;
        }
        else if (this.holdLastFrame)
        {
            if (this.frameNumber > this.currentAnimation.sprites.Length - 1)
                this.frameNumber = this.currentAnimation.sprites.Length - 1;
        }
        else if (this.frameNumber >= this.currentAnimation.sprites.Length)
        {
            this.frameNumber = this.currentAnimation.sprites.Length - 1;
            this.nextAnimationStarted = false;

            if (this.onComplete != null)
            {
                // If calling onComplete() sets this.nextAnimationStarted,
                // it means they started another animation running,
                // so only clear it if that didn't happen
                this.onComplete();

                if (this.nextAnimationStarted == false)
                    this.onComplete = null;
            }

            if (this.nextAnimationStarted == false)
                this.currentAnimation = null;
        }

        if (this.frameActions.Count() > 0 && framesToAdvance > 0)
        {
            foreach (var frameAction in this.frameActions.ToArray())
            {
                if (this.frameNumber >= frameAction.frameNumber &&
                    this.frameNumber - framesToAdvance < frameAction.frameNumber)
                {
                    frameAction.action.Invoke();
                }
            }
        }

        if (this.currentAnimation != null)
        {
            var sprite = this.currentAnimation.sprites[this.frameNumber];

            if (this.spriteRenderer != null)
                this.spriteRenderer.sprite = sprite;
            if (this.image != null)
                this.image.sprite = sprite;
        }
    }
}
