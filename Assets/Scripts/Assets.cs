using UnityEngine;
using UnityEngine.AddressableAssets;
using System;

public class Assets : MonoBehaviour
{
    public static Assets instance;

    public Material spritesDefaultMaterial;
    public Material spritesFullColorMaterial;
    public Material spritesScissorMaterial;
    public Material railMaterial;
    public Material spritesWaterScissorMaterial;
    public Material spriteGoldTint;
    public Material spriteGoldTint2;
    public Material spriteWater;
    public Material spriteWaterSolid;
    public Material spriteLava;
    public Material spriteLavaSolid;
    public Material spritePlayerInvincibility;
    public Material spritePlayerHologram;
    public Material tileTemperatureChangeMaterial;
    public GameObject debugDrawing;
    public GameObject chunkNameDisplay;
    public GameObject visibleAreaDebugGuide;

    public AssetReference yolk;
    public AssetReference puffer;
    public AssetReference sprout;
    public AssetReference goop;
    public AssetReference king;
    [NonSerialized] public GameObject loadedPlayer;
    public Sprite[] allCharacterSpritesInMultiplayerHeadset;
    public GameObject playerOffscreenIndicator;

    [Header("Generic items")]
    public GameObject rosette;
    public GameObject rosetteBanner;
    public GameObject checkpoint;
    public GameObject checkpointArrow;
    public GameObject puzzleSign;
    public GameObject checkpointBlender;
    public GameObject checkpointLightBig;
    public GameObject checkpointLightSmall;
    public GameObject apple;
    public GameObject banana;
    public GameObject cherry;
    public GameObject lemon;
    public GameObject melon;
    public GameObject kiwi;
    public GameObject orange;
    public GameObject pear;
    public GameObject pineapple;
    public GameObject strawberry;
    public GameObject giantApple;
    public GameObject giantCherry;
    public GameObject giantKiwi;
    public GameObject giantStrawberry;
    public GameObject coin;
    public GameObject silverCoin;
    public GameObject metalBlock1x1;
    public GameObject metalBlock1x2;
    public GameObject metalBlock2x1;
    public GameObject metalBlock2x2;
    public GameObject noGrabBlockRight;
    public GameObject noGrabBlockLeft;
    public GameObject spikeTile;
    public GameObject springYellow1;
    public GameObject springYellow2;
    public GameObject springRed1;
    public GameObject springRed2;
    public GameObject pushPlatform;
    public GameObject tramplePlatform;
    public GameObject movementAnchor;
    public GameObject movementPathDot;
    public GameObject movingPlatformTest;
    public GameObject buttonTriggerPlatform;
    public GameObject buttonTriggerPlatform1x1;
    public GameObject button;
    public GameObject oneWayFlap2x2;
    public GameObject breakableBlock1x1;
    public GameObject breakableBlock1x2;
    public GameObject breakableBlock2x1;
    public GameObject breakableBlock2x2;
    public GameObject tntBlock1x1;
    public GameObject tntBlock2x2;
    public GameObject enemyBlockerHorizontal;
    public GameObject enemyBlockerVertical;
    public GameObject playerBlockerHorizontal;
    public GameObject playerBlockerVertical;
    public GameObject timerBlockPurple;
    public GameObject timerBlockGreen;
    public GameObject timerSpikePurple;
    public GameObject timerSpikeGreen;
    public GameObject timerButtonPurpleTop;
    public GameObject timerButtonPurpleFloor;
    public GameObject timerButtonGreenTop;
    public GameObject timerButtonGreenFloor;
    public GameObject timerCoinPiecePurple;
    public GameObject abBlockRed;
    public GameObject abBlockBlue;
    public GameObject abCoinRed;
    public GameObject abCoinBlue;
    public GameObject abSilverCoinRed;
    public GameObject abSilverCoinBlue;
    public GameObject abSwitch;
    public GameObject freeBlock;
    public GameObject pipeGreenCorner;
    public GameObject pipeGreenExit;
    public GameObject pipeGreenStraight;
    public GameObject pipeGreenStraightBroken;
    public GameObject pipeGreenLid;
    public GameObject pipeBlueCorner;
    public GameObject pipeBlueExit;
    public GameObject pipeBlueStraight;
    public GameObject pipeBlueStraightBroken;
    public GameObject pipeBlueLid;
    public GameObject peg;
    public GameObject grabHook;
    public GameObject grabHookFlying;
    public GameObject chestRandom;
    public GameObject chestOneCoin;
    public GameObject chestFiveCoins;
    public GameObject chestPowerup;
    public GameObject chestKey;
    public GameObject chestCheckpoint;
    public GameObject chestRandomFlying;
    public GameObject chestOneCoinFlying;
    public GameObject chestFiveCoinsFlying;
    public GameObject chestPowerupFlying;
    public GameObject chestKeyFlying;
    public GameObject chestCheckpointFlying;
    public GameObject lockedChestRandom;
    public GameObject lockedChestOneCoin;
    public GameObject lockedChestFiveCoins;
    public GameObject lockedChestPowerup;
    public GameObject lockedChestCheckpoint;
    public GameObject chestRevealLightBlock;
    public GameObject gateBlockLock;
    public GameObject gateBlockHorizontal;
    public GameObject gateBlockVertical;
    public GameObject gateBlockCorner;
    public GameObject gateBlockHorizontalButtonLeft;
    public GameObject gateBlockHorizontalButtonRight;
    public GameObject gateBlockVerticalButtonUp;
    public GameObject gateBlockVerticalButtonDown;
    public GameObject gateKey;
    public GameObject randomBox1x1;
    public GameObject randomBox2x2;
    public GameObject blenderVendingMachine;
    public GameObject trophyBronze;
    public GameObject trophySilver;
    public GameObject trophyGold;
    public GameObject trophyGoldLid;
    public GameObject trophyGoldLidMultiplayerWinner;
    public GameObject trophyGoldLidMultiplayerLoser;
    public GameObject flyMultiplayerLoser;
    public GameObject snowBlock;
    public GameObject enemyRespawner;
    public GameObject PowerupHeart;

    [Header("Generic enemies")]
    public GameObject trunky;
    public GameObject trunkyBullet;
    public GameObject blueTestWalker;
    public GameObject fireSkull;
    public GameObject spikey;
    public GameObject jabbajaw;
    public GameObject chestMimic;
    public GameObject blob;

    [Header("Theme Specific Assets")]
    public AssetReference assetsForRainyRuins;
    public AssetReference assetsForCapitalHighway;
    public AssetReference assetsForConflictCanyon;
    public AssetReference assetsForHotNColdSprings;
    public AssetReference assetsForGravityGalaxy;
    public AssetReference assetsForSunkenIsland;
    public AssetReference assetsForTreasureMines;
    public AssetReference assetsForWindySkiesTheme;
    public AssetReference assetsForTombstoneHillTheme;
    public AssetReference assetsForMoltenFortressTheme;
    public AssetReference assetsForVRTrainingRoom;
    public AssetReference assetsForOpeningTutorial;
    [NonSerialized] public AssetsForRainyRuins      rainyRuins;
    [NonSerialized] public AssetsForCapitalHighway  capitalHighway;
    [NonSerialized] public AssetsForConflictCanyon  conflictCanyon;
    [NonSerialized] public AssetsForHotNColdSprings hotNColdSprings;
    [NonSerialized] public AssetsForGravityGalaxy   gravityGalaxy;
    [NonSerialized] public AssetsForSunkenIsland    sunkenIsland;
    [NonSerialized] public AssetsForTreasureMines   treasureMines;
    [NonSerialized] public AssetsForWindySkiesTheme windySkies;
    [NonSerialized] public AssetsForTombstoneHillTheme tombstoneHill;
    [NonSerialized] public AssetsForMoltenFortress  moltenFortress;
    [NonSerialized] public AssetsForVRTrainingRoom  vrTrainingRoom;
    [NonSerialized] public AssetsForOpeningTutorial openingTutorial;

    [Header("Special/required stuff")]
    public GameObject startCrowd;
    public GameObject endCrowd;
    public GameObject startBarrierRed;
    public GameObject startBarrierYellow;
    public GameObject startBarrierBlue;
    public GameObject bonusLift;
    public GameObject entranceLift;
    public GameObject referee;
    public GameObject petYellow;
    public GameObject petSword;
    public GameObject petTornado;
    public GameObject petBaseball;
    public GameObject petMine;
    public GameObject petPainter;
    public GameObject paintSplatter;
    public GameObject bubble;
    public GameObject wings;
    public GameObject ComingSoonBillboard;
    public GameObject TomorrowOnSuperLeapDay;
    public GameObject multiplayerRoom;
    public GameObject multiplayerHeadset;
    public GameObject timerPickup;

    [Header("Unused items/enemies")]
    public GameObject cloudMovingPlatform;
    public GameObject contactTriggerPlatform;
    public GameObject hangPoint;
    public GameObject togglePlatform1x1;
    public GameObject togglePlatform3x1Top;
    public GameObject buzzsaw;
    public GameObject grabAndSwingEnemy;
    public GameObject rocket;
    public GameObject sequenceMover;
    public GameObject powerupStar;
    public GameObject coinPickup;
    public GameObject colourGroupBlock1;
    public GameObject colourGroupBlock2;
    public GameObject colourGroupBlock3;
    public GameObject fireAndIceBlock;

    [Header("Generic sprites")]
    public Sprite emptySprite;
    public Sprite playerDeathParticle;
    public Sprite[] wildTrunkyBoneParticles;
    public Sprite jabbajawHitWallParticle;
    public Sprite[] iceParticles;

    [Header("Generic animations")]
    public Animated.Animation genericDustParticleAnimation;
    public Animated.Animation enemyDeathAnimation;
    public Animated.Animation enemyDeath2Animation;
    public Animated.Animation enemyRespawnAnimation;
    public Animated.Animation enemyHitPowAnimation;
    public Animated.Animation doubleJumpAnimation;
    public Animated.Animation coinSparkleAnimation;
    public Animated.Animation trophySparkleAnimation;
    public Animated.Animation goldTrophyShotAnimation;
    public Animated.Animation playerInvincibleSparkleAnimation;
    public Animated.Animation chestMimicDizzyAnimation;
    public Animated.Animation chestMimicAlertAnimation;
    public Animated.Animation exclamationMarkAnimation;
    public Animated.Animation explosionAnimation;
    public Animated.Animation sparkleAnimation;
    public Animated.Animation flameAnimationA;
    public Animated.Animation flameAnimationB;
    public Animated.Animation chargingParticlesAnimation;
    public Animated.Animation electricShockParticlesAnimation;
    public Animated.Animation enemyRespawnerLightningBoltAnimation;
    public Animated.Animation enemyRespawnerActivateAnimation;
    public Animated.Animation enemyRespawnerRespawnOrbAnimation;
    public Animated.Animation enemyRespawnerRespawnOrbLightningBoltsAnimation;
    
    [Header("Tile animations")]
    public GameObject templeBigPlant1;
    public GameObject templeBigPlant2;

    [Header("Transitions")]
    public GameObject transitionRainyRuinsPortrait;
    public GameObject transitionRainyRuinsLandscape;

    [Header("Audio")]
    public AudioClip sfxPlayerFootstep;
    public AudioClip[] sfxPlayerJump;
    public AudioClip[] sfxPlayerDoubleJump;
    public AudioClip[] sfxPlayerWallJump;
    public AudioClip[] sfxYolkVoice;
    public AudioClip[] sfxYolkPanic;
    public AudioClip sfxYolkEggInit;
    public AudioClip sfxYolkEggGrow;
    public AudioClip sfxYolkEggBomb;
    public AudioClip sfxYolkEggRespawnSparkle;
    public AudioClip sfxYolkEggRespawnGrow;
    public AudioClip sfxYolkEggRespawnFlap;
    public AudioClip[] sfxPuffer;
    public AudioClip[] sfxPufferInflate;
    public AudioClip sfxPufferRespawn;
    public AudioClip sfxPufferDeath;
    public AudioClip[] sfxSprout;
    public AudioClip[] sfxSproutSuction;
    public AudioClip sfxSproutDeath;
    public AudioClip sfxSproutRespawn;
    public AudioClip[] sfxKing;
    public AudioClip sfxKingHatLose;
    public AudioClip sfxKingRise;
    public AudioClip sfxKingHatReturn;
    public AudioClip[] sfxKingPanic;
    public AudioClip sfxKingGroundslamDrop;
    public AudioClip sfxKingGroundslamLand;
    public AudioClip[] sfxKingGroundslamVoice;
    public AudioClip sfxKingDeath;
    public AudioClip sfxKingRespawn;
    public AudioClip[] sfxGoop;
    public AudioClip sfxGoopDeath;
    public AudioClip sfxGoopLand;
    public AudioClip sfxGoopRespawn;
    public AudioClip sfxGoopSpin;
    public AudioClip[] sfxGoopStep;
    public AudioClip sfxGoopWallslide;


    public AudioClip sfxFruit;
    public AudioClip sfxCoin;
    public AudioClip sfxPlayerGrab;
    public AudioClip sfxCheckpointActivate;
    public AudioClip sfxEnemyDeath;
    public AudioClip sfxEnemyRespawn;
    public AudioClip sfxPlayerDeath;
    public AudioClip sfxPlayerRespawn;
    public AudioClip sfxPlayerEggBreak;
    public AudioClip sfxPushPlatform;
    public AudioClip sfxSpringRed;
    public AudioClip sfxSpringYellow;
    public AudioClip sfxBoostPad;
    public AudioClip sfxChestOpen;
    public AudioClip sfxBreakBlock;
    public AudioClip sfxButtonPress;
    public AudioClip sfxButtonUnpress;
    public AudioClip sfxHelmutSpearThrust;
    public AudioClip sfxHelmutHelmetBounce;
    public AudioClip sfxJabbajawHitsWall;
    public AudioClip sfxJabbajawChomp;
    public AudioClip sfxRhinoBugHoofScrape;
    public AudioClip sfxRhinoBugSpikeOut;
    public AudioClip sfxScuttlebugElectrify;
    public AudioClip sfxTempleLightning;
    public AudioClip sfxTrophyAppear;
    public AudioClip sfxTrophyLand;
    public AudioClip sfxTrunkyFire;
    public AudioClip sfxTrunkyWindup;
    public AudioClip sfxTrunkyProjectileLand;
    public AudioClip sfxVineSlide;
    public AudioClip sfxWildTrunkySneeze;
    public AudioClip sfxWildTrunkyBoneBounce;
    public AudioClip sfxWildTrunkyBoneShatter;
    public AudioClip sfxStartGong;
    public AudioClip sfxBambooTurn;
    public AudioClip sfxGoldTrophyShot;
    public AudioClip sfxGoldTrophyLidDrop;
    public AudioClip sfxSparkleTrophyAppear;
    public AudioClip sfxSparkleBronzeTrophyAppear;
    public AudioClip sfxSparkleSilverTrophyAppear;
    public AudioClip sfxSparkleGoldTrophyAppear;
    public AudioClip sfxReachNormalTrophy; // Bronze & Silver
    public AudioClip sfxReachGoldTrophy;
    public AudioClip sfxWobbleBronzeTrophy;
    public AudioClip sfxWobbleSilverTrophy;
    public AudioClip sfxJumpBronzeTrophy;
    public AudioClip sfxJumpSilverTrophy;
    public AudioClip sfxReachCheckpoint;
    public AudioClip sfxHelmetDie;
    public AudioClip sfxPlayerBumpOnEnemy;
    public AudioClip sfxGateUnlock;
    public AudioClip sfxTNTExplosion;
    public AudioClip sfxSwingingAxe1;
    public AudioClip sfxSwingingAxe2;
    public AudioClip sfxPipeEnter;
    public AudioClip sfxPipeExit;
    public AudioClip sfxSwitchEngage;
    public AudioClip sfxSwitchDisengage;
    public AudioClip sfxPowerupCollect;
    public AudioClip sfxBlobJump;
    public AudioClip sfxFireSkullSpawn;
    public AudioClip sfxFireSkullCollide;
    public AudioClip sfxFireSkullFireOff;
    public AudioClip sfxFireSkullFireOn;
    public AudioClip sfxChestMimicUnlock;
    public AudioClip sfxChestMimicActivate;
    public AudioClip sfxChestMimicRewardCoin;
    public AudioClip sfxChestMimicRewardPowerup;
    public AudioClip sfxChestMimicJump;
    public AudioClip sfxLoopCrowd;
    public AudioClip sfxLoopRainyRuinsRain;
    public AudioClip sfxEnemyTurtlecannonCold;
    public AudioClip sfxEnemyTurtlecannonHot;
    public AudioClip sfxEnemyTurtlecannonHide;
    public AudioClip sfxEnemyHippoDeath;
    public AudioClip sfxEnemyHippoJump;
    public AudioClip sfxEnemyHippoStompJump;
    public AudioClip sfxEnemyHippoStompLand;
    public AudioClip sfxEnvPullswitch;
    public AudioClip sfxEnvPullswitchCold;
    public AudioClip sfxEnvPullswitchHot;
    public AudioClip sfxEnvIcicleBreak;
    public AudioClip sfxEnvIcicleFall;
    public AudioClip sfxEnvIcicleShake;
    public AudioClip sfxEnvIcicleSpawn;
    public AudioClip sfxEnvSpinwheelCold;
    public AudioClip sfxEnvSpinwheelHot;
    public AudioClip sfxEnvSpinwheelJump;
    public AudioClip sfxEnvSnowMelt;
    public AudioClip sfxEnvSnowSpawn;
    public AudioClip sfxEnvButtonNeutral;
    public AudioClip sfxEnvGateBlockBurst;
    public AudioClip sfxEnvMovingBlockImpactLarge;
    public AudioClip sfxEnvMovingBlockImpactMed;
    public AudioClip sfxEnvMovingBlockImpactSmall;
    public AudioClip sfxEnvNoGrabBlockIn;
    public AudioClip sfxEnvNoGrabBlockOut;
    public AudioClip sfxEnvSpearActivate;
    public AudioClip sfxEnvSpearImpact;
    public AudioClip sfxEnvSpringGreen;
    public AudioClip sfxEnemyScuttlebugDeath;
    public AudioClip sfxEnemyScuttlebugZap;
    public AudioClip sfxEnemyHelmutAttack;
    public AudioClip sfxEnemyHelmutDeath;
    public AudioClip sfxEnemyHelmutSkullCollide;
    public AudioClip sfxEnemyWildTrunkyAttack;
    public AudioClip sfxEnemyWildTrunkyBoneBounce;
    public AudioClip sfxEnemyWildTrunkyBoneBreak;
    public AudioClip sfxEnemyWildTrunkyDeath;
    public AudioClip sfxEnvRopeGrab;
    public AudioClip sfxEnvRopeSwing1;
    public AudioClip sfxEnvRopeSwing2;
    public AudioClip sfxEnvRopeSlide;
    public AudioClip sfxEnvButtonGravity;
    public AudioClip sfxEnvSpaceboosterOpen;
    public AudioClip sfxEnvSpaceboosterEnter;
    public AudioClip sfxEnvSpaceboosterShoot;
    public AudioClip sfxEnvBoxEnter;
    public AudioClip sfxEnvBoxExit;
    public AudioClip sfxEnvBoxJump;
    public AudioClip sfxEnvConveyorBeltNeutral;
    public AudioClip sfxEnvGateBlock;
    public AudioClip sfxEnvLaser;
    public AudioClip sfxEnvPortal;
    public AudioClip sfxEnvSpacedoorClose;
    public AudioClip sfxEnvSpacedoorOpen;
    public AudioClip sfxEnvSpawnerShoot;
    public AudioClip sfxEnvSpawnerLand;
    public AudioClip sfxEnemyAlienskullBlastoff;
    public AudioClip sfxEnemyAlienskullBounce;
    public AudioClip sfxEnemyAliensquid;
    public AudioClip sfxEnemyBeetlesoldierChase;
    public AudioClip sfxEnemyBeetlesoldierDeath;
    public AudioClip sfxEnemyBeetlesoldiershoot;
    public AudioClip sfxEnemyBeetlesoldiershootImpact;
    public AudioClip sfxEnemyCactusAssassinJump;
    public AudioClip sfxEnemyCactusAssassinShoot;
    public AudioClip sfxEnemyCactusBombDebrisExploded;
    public AudioClip sfxEnemyCactusBombExplode;
    public AudioClip sfxEnemyCactusBombJump;
    public AudioClip sfxEnemyAlert;
    public AudioClip sfxEnemyGravityGuardian;
    public AudioClip sfxEnemyGravityGuardianBounce;
    public AudioClip sfxEnemyManholeMonsterDeath;
    public AudioClip sfxEnemyManholeMonsterHide;
    public AudioClip sfxEnemyManholeMonsterShow;
    public AudioClip sfxEnemyManholeMonsterSpit;
    public AudioClip sfxEnemyTerrorgeonDeath;
    public AudioClip sfxEnemyTerrorgeonEgg;
    public AudioClip sfxEnemyTerrorgeoneEggBreak;
    public AudioClip sfxEnemyTerrorgeonFlap;
    public AudioClip sfxEnemyConeAlert;
    public AudioClip sfxEnemyConeChase;
    public AudioClip sfxEnemyConeDeath;
    public AudioClip sfxEnemyConeWallBump;
    public AudioClip sfxEnvConeBump;
    public AudioClip sfxEnemyGrubAlert;
    public AudioClip sfxEnemyGrubDig;
    public AudioClip sfxEnemyGrubPopout;
    public AudioClip sfxEnemyGrubWalk;
    public AudioClip sfxEnemyJellyfishFall;
    public AudioClip sfxEnemyJellyfishElectricity;
    public AudioClip sfxEnemyJellyfishElectricityOutside;
    public AudioClip sfxEnemyJellyfishDeath;
    public AudioClip sfxEnemySkullfishFlopLoop;
    public AudioClip sfxEnemySkullfishSwimLoop;
    public AudioClip sfxEnemySkullfishGround;
    public AudioClip sfxEnemySkullfishDeath;
    public AudioClip sfxEnemySkullfishJump;
    public AudioClip sfxEnemyPiranhaSwimLoop;
    public AudioClip sfxEnemyPiranhaAggro;
    public AudioClip sfxEnemyPiranhaWall;
    public AudioClip sfxEnemyPiranhaDeath;
    public AudioClip sfxEnemyQuackerDeath;
    public AudioClip sfxEnemyQuackerJump;
    public AudioClip sfxEnemyQuackerWaterIn;
    public AudioClip sfxEnemyQuackerWaterOut;
    public AudioClip sfxEnemyQuackerLand;
    public AudioClip sfxEnvCannonCityLaunch;
    public AudioClip sfxEnvCannonMove;
    public AudioClip sfxEnvBarrierBreak;
    public AudioClip sfxEnvGripGroundsBoost;
    public AudioClip sfxEnvGripGroundsLoop;
    public AudioClip sfxEnvHidezoneCactusEnter;
    public AudioClip sfxEnvHidezoneMetalEnter;
    public AudioClip sfxEnvExplosion;
    public AudioClip sfxEnvMine;
    public AudioClip sfxEnvPipeIn;
    public AudioClip sfxEnvPipeOut;
    public AudioClip sfxEnvPipesCannon;
    public AudioClip sfxEnvPipesLoop;
    public AudioClip sfxEnvSteam;
    public AudioClip sfxEnvSteamLoop;
    public AudioClip sfxEnvLightBlock;
    public AudioClip sfxEnvAnchorStart;
    public AudioClip sfxEnvAnchorLand;
    public AudioClip sfxEnvSeaMine;
    public AudioClip sfxEnvCurrentLoop;
    public AudioClip sfxEnvClamOpen;
    public AudioClip sfxEnvClamBite;
    public AudioClip sfxEnvClamJump;
    public AudioClip sfxEnvRaftOn;
    public AudioClip sfxEnvRaftOff;
    public AudioClip sfxEnvRaftLoop;
    public AudioClip sfxEnvRaftWall;
    public AudioClip sfxEnvBeachBallBounce;
    public AudioClip sfxEnvBeachBallPop;
    public AudioClip sfxEnvBeachBallNonPlayer;
    public AudioClip sfxEnvWaterRise;
    public AudioClip sfxEnvBarrierMinecartHit;
    public AudioClip sfxEnvBoostActivate;
    public AudioClip sfxEnvMinecartCrash;
    public AudioClip sfxEnvCrystalFruitShatter;
    public AudioClip sfxEnvCrystalWallShatter;
    public AudioClip sfxEnvMinecartDriveRail;
    public AudioClip sfxEnvMinecartDriveTiles;
    public AudioClip sfxEnvElectrosnakeDie;
    public AudioClip sfxEnvElectrosnakeElectrify;
    public AudioClip sfxEnvElectrosnakeShockMoving;
    public AudioClip sfxEnvEnterDoor;
    public AudioClip sfxEnvExplosiveBarrierExplosion;
    public AudioClip sfxEnvMinecartFlip;
    public AudioClip sfxEnvMinecartJump;
    public AudioClip sfxEnvMinecartLandRail;
    public AudioClip sfxEnvMinecartLandTiles;
    public AudioClip sfxEnvMullInMinecartDie;
    public AudioClip sfxEnvRailRebound;
    public AudioClip sfxEnvRubberReboundBounceHeavy;
    public AudioClip sfxEnvRubberReboundBounceLight;
    public AudioClip sfxEnvUnstableGround;
    public AudioClip sfxEnemyBatFlyPurple;
    public AudioClip sfxEnemyBatFlyRed;
    public AudioClip sfxEnemyBatWakeUp;
    public AudioClip sfxEnemyBatDie;
    public AudioClip sfxWindyTilePropellerOn;
    public AudioClip sfxWindyShootingSunInhales;
    public AudioClip sfxWindyShootingSunFires;
    public AudioClip sfxWindyShootingSunBurns;
    public AudioClip sfxWindyParachuteUnequip;
    public AudioClip sfxWindyParachuteOpen;
    public AudioClip sfxWindyParachuteEquip;
    public AudioClip sfxWindyParachuteClose;
    public AudioClip sfxWindyHomingCloudElectrify;
    public AudioClip sfxWindyHomingCloudBlowAway;
    public AudioClip sfxWindyFlyingRing;
    public AudioClip sfxWindyDandelionRise;
    public AudioClip sfxWindyDandelionFall;
    public AudioClip sfxWindyDandelionDeath;
    public AudioClip sfxWindyDandelionBump;
    public AudioClip sfxWindyCloudPopOut;
    public AudioClip sfxWindyCloudLand;
    public AudioClip sfxWindyCloudInside;
    public AudioClip sfxWindyCloudBirdSurprised;
    public AudioClip sfxWindyCloudBirdHit;
    public AudioClip sfxWindyCloudBirdAttacking;
    public AudioClip sfxWindyBlowerEnemyWind;
    public AudioClip sfxWindyAirPropellerBlowing;
    public AudioClip sfxWindyAirCurrentBlowing;

    public AudioClip sfxRefWhistle;

    public AudioClip sfxWaterIn;
    public AudioClip sfxWaterOut;
    public AudioClip sfxWaterRise;
    public AudioClip sfxWaterSwim;

    public AudioClip sfxPowerupInvincibility;
    public AudioClip sfxPowerupShieldLoop;
    public AudioClip sfxPowerupShieldBreak;
    public AudioClip sfxPowerupYellowpetChomp;
    public AudioClip sfxPowerupTornado;
    public AudioClip sfxPowerupSlot;
    public AudioClip sfxPowerupActivate;
    public AudioClip sfxPowerupSwordAttack;
    public AudioClip sfxPowerupMidasStart;
    public AudioClip sfxPowerupMidasEnd;
    public AudioClip sfxPowerupWings;

    public AudioClip sfxUIBack;
    public AudioClip sfxUICalendarDayswipe;
    public AudioClip sfxUICalendarMonthswipe;
    public AudioClip sfxUIConfirm;
    public AudioClip sfxUIElevator;
    public AudioClip sfxUIScreenwipeIn;
    public AudioClip sfxUIScreenwipeOut;
    public AudioClip sfxUIWinIn;
    public AudioClip sfxUIWinOut;
    public AudioClip sfxUIPauseBubblesIn;
    public AudioClip sfxUIPauseBubblesOut;
    public AudioClip sfxCapsuleClose;
    public AudioClip sfxCapsuleDig;
    public AudioClip sfxCapsuleOpen;

    public AudioClip sfxFanfareBronzeSliver;
    public AudioClip sfxFanfareGold;
    public AudioClip sfxFanfareFruit;

    public AudioClip sfxMpPowerupBatBallup;
    public AudioClip sfxMpPowerupBatFlap;
    public AudioClip sfxMpPowerupBatSwing;
    public AudioClip sfxMpPowerupBatTwirl;
    public AudioClip sfxMpPowerupMineAppear;
    public AudioClip sfxMpPowerupMineTrigger;
    public AudioClip sfxMpPowerupPaintDisappear;
    public AudioClip sfxMpPowerupPaintSneeze;
    public AudioClip sfxMpVRButtonsDown;
    public AudioClip sfxMpVRButtonsUp;
    public AudioClip sfxMpVRGearDown;
    public AudioClip sfxMpVRGearUp;
    public AudioClip sfxMpReadySetGo1;
    public AudioClip sfxMpReadySetGo2;
    public AudioClip sfxMpArcadeAmb;

    public AudioClip sfxPinballPortalIn;
    public AudioClip sfxPrizeWinShort;
    public AudioClip sfxUIShopPurchaseCoins;

    public AudioClip musicInvincible;
    public AudioClip musicInvincibleExtended;
    public AudioClip musicWinMusicLoop;
    public AudioClip musicTransitionToMain;
    public AudioClip musicTransitionToMini;

    public static void Init()
    {
        instance = FindObjectOfType<Assets>();
    }

    public void OnDestroy()
    {
        void Unload<T>(ref T obj)
        {
            if (obj != null)
            {
                Addressables.Release(obj);
                obj = default;
            }
        }

        Unload(ref this.loadedPlayer);
        Unload(ref this.rainyRuins);
        Unload(ref this.capitalHighway);
        Unload(ref this.conflictCanyon);
        Unload(ref this.hotNColdSprings);
        Unload(ref this.gravityGalaxy);
        Unload(ref this.sunkenIsland);
        Unload(ref this.treasureMines);
        Unload(ref this.windySkies);
        Unload(ref this.moltenFortress);
        Unload(ref this.vrTrainingRoom);
        Unload(ref this.openingTutorial);

        if (instance == this)
            instance = null;
    }

    public async void PreloadAssetsForMap(Theme theme, System.Action onComplete)
    {
        var playerReference = SaveData.GetSelectedCharacter() switch
        {
            Character.Puffer => this.puffer,
            Character.Sprout => this.sprout,
            Character.King   => this.king,
            Character.Goop   => this.goop,
            _                => this.yolk
        };
        var taskLoadPlayer =
            Addressables.LoadAssetAsync<GameObject>(playerReference).Task;

        switch (theme)
        {
            case Theme.RainyRuins:
                this.rainyRuins =
                    await Addressables.LoadAssetAsync<AssetsForRainyRuins>(
                        this.assetsForRainyRuins
                    ).Task;
                break;
            case Theme.CapitalHighway:
                this.capitalHighway =
                    await Addressables.LoadAssetAsync<AssetsForCapitalHighway>(
                        this.assetsForCapitalHighway
                    ).Task;
                break;
            case Theme.ConflictCanyon:
                this.conflictCanyon =
                    await Addressables.LoadAssetAsync<AssetsForConflictCanyon>(
                        this.assetsForConflictCanyon
                    ).Task;
                break;
            case Theme.HotNColdSprings:
                this.hotNColdSprings =
                    await Addressables.LoadAssetAsync<AssetsForHotNColdSprings>(
                        this.assetsForHotNColdSprings
                    ).Task;
                break;
            case Theme.GravityGalaxy:
                this.gravityGalaxy =
                    await Addressables.LoadAssetAsync<AssetsForGravityGalaxy>(
                        this.assetsForGravityGalaxy
                    ).Task;
                break;
            case Theme.SunkenIsland:
                this.sunkenIsland =
                    await Addressables.LoadAssetAsync<AssetsForSunkenIsland>(
                        this.assetsForSunkenIsland
                    ).Task;
                break;
            case Theme.TreasureMines:
                this.treasureMines =
                    await Addressables.LoadAssetAsync<AssetsForTreasureMines>(
                        this.assetsForTreasureMines
                    ).Task;
                break;
            case Theme.WindySkies:
                this.windySkies =
                    await Addressables.LoadAssetAsync<AssetsForWindySkiesTheme>(
                        this.assetsForWindySkiesTheme
                    ).Task;
                break;
            case Theme.MoltenFortress:
                this.moltenFortress =
                    await Addressables.LoadAssetAsync<AssetsForMoltenFortress>(
                        this.assetsForMoltenFortressTheme
                    ).Task;
                break;
            case Theme.TombstoneHill:
                this.tombstoneHill =
                    await Addressables.LoadAssetAsync<AssetsForTombstoneHillTheme>(
                        this.assetsForTombstoneHillTheme
                    ).Task;
                break;
            case Theme.VRTrainingRoom:
                this.vrTrainingRoom =
                    await Addressables.LoadAssetAsync<AssetsForVRTrainingRoom>(
                        this.assetsForVRTrainingRoom
                    ).Task;
                break;
            case Theme.OpeningTutorial:
                this.openingTutorial =
                    await Addressables.LoadAssetAsync<AssetsForOpeningTutorial>(
                        this.assetsForOpeningTutorial
                    ).Task;
                break;
        }

        this.loadedPlayer = await taskLoadPlayer;

        onComplete.Invoke();
    }

    public GameObject GetPrefabByTileName(string name) => name switch
    {
        "rosette"                       => rosette,
        "rosette_banner_end"            => rosetteBanner,
        "rosette_banner_middle"         => rosetteBanner,
        "rosette_banner_shadow"         => rosetteBanner,
        "checkpoint"                    => checkpoint,
        "checkpoint_arrow_to_right"     => checkpointArrow,
        "puzzle_sign"                   => puzzleSign,
        "checkpoint_blender"            => checkpointBlender,
        "checkpoint_light_big"          => checkpointLightBig,
        "checkpoint_light_small"        => checkpointLightSmall,
        "random_fruit"                  => RandomFruit(),
        "apple"                         => apple,
        "banana"                        => banana,
        "cherry"                        => cherry,
        "kiwi"                          => kiwi,
        "lemon"                         => lemon,
        "melon"                         => melon,
        "orange"                        => orange,
        "pear"                          => pear,
        "pineapple"                     => pineapple,
        "strawberry"                    => strawberry,
        "giant_apple"                   => giantApple,
        "giant_cherry"                  => giantCherry,
        "giant_kiwi"                    => giantKiwi,
        "giant_strawberry"              => giantStrawberry,
        "coin"                          => coin,
        "spike_right"                   => spikeTile,
        "spike_horz"                    => spikeTile,
        "spike_junct"                   => spikeTile,
        "spike_single"                  => spikeTile,
        "spring_yellow1"                => springYellow1,
        "spring_yellow2"                => springYellow2,
        "spring_red1"                   => springRed1,
        "spring_red2"                   => springRed2,
        "movement_anchor"               => movementAnchor,
        "moving_platform_test"          => movingPlatformTest,
        "cloud_moving_platform"         => cloudMovingPlatform,
        "contact_trigger_platform"      => contactTriggerPlatform,
        "button_trigger_platform"       => buttonTriggerPlatform,
        "button_trigger_platform_1x1"   => buttonTriggerPlatform1x1,
        "button"                        => button,
        "one_way_flap_2x2"              => oneWayFlap2x2,
        "breakable_block_1x1"           => breakableBlock1x1,
        "breakable_block_1x2"           => breakableBlock1x2,
        "breakable_block_2x1"           => breakableBlock2x1,
        "breakable_block_2x2"           => breakableBlock2x2,
        "enemy_blocker_horz"            => enemyBlockerHorizontal,
        "enemy_blocker_vert"            => enemyBlockerVertical,
        "player_blocker_horz"           => playerBlockerHorizontal,
        "player_blocker_vert"           => playerBlockerVertical,
        "player_blocker"                => playerBlockerHorizontal,
        "hang_point"                    => hangPoint,
        "toggle_platform_1x1_a"         => togglePlatform1x1,
        "toggle_platform_1x1_b"         => togglePlatform1x1,
        "toggle_platform_3x1top_a"      => togglePlatform3x1Top,
        "toggle_platform_3x1top_b"      => togglePlatform3x1Top,
        "timer_block_purple"            => timerBlockPurple,
        "timer_block_green"             => timerBlockGreen,
        "timer_spike_purple"            => timerSpikePurple,
        "timer_spike_green"             => timerSpikeGreen,
        "timer_button_purpletop"        => timerButtonPurpleTop,
        "timer_button_purplefloor"      => timerButtonPurpleFloor,
        "timer_button_greentop"         => timerButtonGreenTop,
        "timer_button_greenfloor"       => timerButtonGreenFloor,
        "timer_coin_piece_purple"       => timerCoinPiecePurple,
        "free_block"                    => freeBlock,
        "ab_block_red"                  => abBlockRed,
        "ab_block_blue"                 => abBlockBlue,
        "ab_coin_red"                   => abCoinRed,
        "ab_coin_blue"                  => abCoinBlue,
        "ab_silver_coin_red"            => abSilverCoinRed,
        "ab_silver_coin_blue"           => abSilverCoinBlue,
        "ab_switch"                     => abSwitch,
        "buzzsaw"                       => buzzsaw,
        "grab_and_swing_enemy"          => grabAndSwingEnemy,
        "pipe_green_corner"             => pipeGreenCorner,
        "pipe_green_exit"               => pipeGreenExit,
        "pipe_green_straight"           => pipeGreenStraight,
        "pipe_green_straight_broken"    => pipeGreenStraightBroken,
        "pipe_green_lid"                => pipeGreenLid,
        "pipe_blue_corner"              => pipeBlueCorner,
        "pipe_blue_exit"                => pipeBlueExit,
        "pipe_blue_straight"            => pipeBlueStraight,
        "pipe_blue_straight_broken"     => pipeBlueStraightBroken,
        "pipe_blue_lid"                 => pipeBlueLid,
        "sequence_mover"                => sequenceMover,
        "peg"                           => peg,
        "grab_hook"                     => grabHook,
        "grab_hook_flying"              => grabHookFlying,
        "start_crowd"                   => startCrowd,
        "end_crowd"                     => endCrowd,
        "coming_soon_billboard"         => ComingSoonBillboard,
        "tomorrow_on_super_leap_day"    => TomorrowOnSuperLeapDay,
        "multiplayer_room"              => multiplayerRoom,
        "multiplayer_headset"           => multiplayerHeadset,
        "trophy_bronze"                 => trophyBronze,
        "trophy_silver"                 => trophySilver,
        "trophy_gold"                   => trophyGold,
        "start_barrier"                 => startBarrierRed,
        "start_barrier_yellow"          => startBarrierYellow,
        "start_barrier_blue"            => startBarrierBlue,
        "chest_random"                  => chestRandom,
        "chest_onecoin"                 => chestOneCoin,
        "chest_fivecoins"               => chestFiveCoins,
        "chest_powerup"                 => chestPowerup,
        "chest_key"                     => chestKey,
        "chest_checkpoint"              => chestCheckpoint,
        "chest_random_flying"           => chestRandomFlying,
        "chest_onecoin_flying"          => chestOneCoinFlying,
        "chest_fivecoins_flying"        => chestFiveCoinsFlying,
        "chest_powerup_flying"          => chestPowerupFlying,
        "chest_key_flying"              => chestKeyFlying,
        "chest_checkpoint_flying"       => chestCheckpointFlying,
        "locked_chest_random"           => lockedChestRandom,
        "locked_chest_onecoin"          => lockedChestOneCoin,
        "locked_chest_fivecoins"        => lockedChestFiveCoins,
        "locked_chest_powerup"          => lockedChestPowerup,
        "locked_chest_checkpoint"       => lockedChestCheckpoint,
        "tntblock_1x1"                  => tntBlock1x1,
        "tntblock_2x2"                  => tntBlock2x2,
        "gate_block_lock"               => gateBlockLock,
        "gate_block_horizontal"         => gateBlockHorizontal,
        "gate_block_vertical"           => gateBlockVertical,
        "gate_block_corner"             => gateBlockCorner,
        "gate_block_h_btn_left"         => gateBlockHorizontalButtonLeft,
        "gate_block_h_btn_right"        => gateBlockHorizontalButtonRight,
        "gate_block_v_btn_up"           => gateBlockVerticalButtonUp,
        "gate_block_v_btn_down"         => gateBlockVerticalButtonDown,
        "gate_key"                      => gateKey,
        "random_box_1x1"                => randomBox1x1,
        "random_box"                    => randomBox2x2,
        "no_grab_block_right"           => noGrabBlockRight,
        "no_grab_block_left"            => noGrabBlockLeft,
        "metal_block_1x1"               => metalBlock1x1,
        "metal_block_1x2"               => metalBlock1x2,
        "metal_block_2x1"               => metalBlock2x1,
        "metal_block_2x2"               => metalBlock2x2,
        "colour_group_block_1"          => colourGroupBlock1,
        "colour_group_block_2"          => colourGroupBlock2,
        "colour_group_block_3"          => colourGroupBlock3,
        "push_platform"                 => pushPlatform,
        "trample_platform"              => tramplePlatform,
        "silver_coin"                   => silverCoin,
        "blender_vending_machine"       => blenderVendingMachine,
        "chest_reveal_light_block"      => chestRevealLightBlock,
        "snow_block"                    => snowBlock,
        "fire_and_ice_block"            => fireAndIceBlock,
        "enemy_respawner"               => enemyRespawner,
        "bonus_lift"                    => bonusLift,
        "bubble_powerup"                => bubble,

        "trunky"                        => trunky,
        "blue_test_walker"              => blueTestWalker,
        "fire_skull"                    => fireSkull,
        "spikey"                        => spikey,
        "jabbajaw"                      => jabbajaw,
        "chest_mimic"                   => chestMimic,
        "blob"                          => blob,

        "apple_boulder_big"             => rainyRuins?.appleBoulderBig,
        "apple_boulder_small"           => rainyRuins?.appleBoulderSmall,
        "apple_boulder_entrance_big"    => rainyRuins?.appleBoulderEntranceBig,
        "apple_boulder_entrance_big_left"       => rainyRuins?.appleBoulderEntranceBigLeft,
        "apple_boulder_entrance_big_right"      => rainyRuins?.appleBoulderEntranceBigRight,
        "apple_boulder_entrance_small"          => rainyRuins?.appleBoulderEntranceSmall,
        "apple_boulder_entrance_small_left"     => rainyRuins?.appleBoulderEntranceSmallLeft,
        "apple_boulder_entrance_small_right"    => rainyRuins?.appleBoulderEntranceSmallRight,
        "bamboo_with_twist"             => rainyRuins?.bamboo,
        "bamboo"                        => rainyRuins?.bamboo,
        "helmut"                        => rainyRuins?.helmut,
        "rhino_bug"                     => rainyRuins?.rhinoBug,
        "rope_fixed"                    => rainyRuins?.rope,
        "rope_free"                     => rainyRuins?.rope,
        "scuttlebug"                    => rainyRuins?.scuttlebug,
        "spear_spawner"                 => rainyRuins?.spearSpawner,
        "swinging_axe"                  => rainyRuins?.swingingAxe,
        "wild_trunky_with_rider"        => rainyRuins?.wildTrunky,
        "wild_trunky"                   => rainyRuins?.wildTrunky,

        "boost_gate_left"               => capitalHighway?.boostGateLeft,
        "boost_gate_right"              => capitalHighway?.boostGateRight,
        "city_cannon"                   => capitalHighway?.cityCannon,
        "crash_barrier_2x1"             => capitalHighway?.crashBarrier2x1,
        "crash_barrier_3x1"             => capitalHighway?.crashBarrier3x1,
        "crash_barrier_4x1"             => capitalHighway?.crashBarrier4x1,
        "crash_barrier_5x1"             => capitalHighway?.crashBarrier5x1,
        "floor_cop"                     => capitalHighway?.floorCop,
        "grip_ground_curve_3_plus_1"    => capitalHighway?.gripGroundCurve3Plus1,
        "grip_ground_curve_out_3"       => capitalHighway?.gripGroundCurveOut3,
        "grip_ground_square"            => capitalHighway?.gripGroundSquare,
        "manhole_monster"               => capitalHighway?.manholeMonster,
        "robot_cop_enemy"               => capitalHighway?.robotCopEnemy,
        "sewer_pipe_cannon_horiz"       => capitalHighway?.sewerPipeCannonHorizontal,
        "sewer_pipe_cannon_vert"        => capitalHighway?.sewerPipeCannonVertical,
        "sewer_pipe_corner"             => capitalHighway?.sewerPipeCorner,
        "sewer_pipe_exit"               => capitalHighway?.sewerPipeExit,
        "sewer_pipe_manhole"            => capitalHighway?.sewerPipeManhole,
        "sewer_pipe_select_all"         => capitalHighway?.sewerPipeSelectAllDirections,
        "sewer_pipe_select_ldr"         => capitalHighway?.sewerPipeSelectLeftDownRight,
        "sewer_pipe_select_ulr"         => capitalHighway?.sewerPipeSelectUpLeftRight,
        "sewer_pipe_select_urd"         => capitalHighway?.sewerPipeSelectUpRightDown,
        "sewer_pipe_stop_dl"            => capitalHighway?.sewerPipeStopDownLeft,
        "sewer_pipe_stop_horiz"         => capitalHighway?.sewerPipeStopHorizontal,
        "sewer_pipe_stop_ul"            => capitalHighway?.sewerPipeStopUpLeft,
        "sewer_pipe_stop_vert"          => capitalHighway?.sewerPipeStopVertical,
        "sewer_pipe_straight"           => capitalHighway?.sewerPipeStraight,
        "terrorgeon"                    => capitalHighway?.terrorgeon,
        "touch_platform_block"          => capitalHighway?.touchPlatformBlock,
        "touch_platform_cog"            => capitalHighway?.touchPlatformCog,
        "touch_platform_light"          => capitalHighway?.touchPlatformLight,
        "traffic_cone_enemy"            => capitalHighway?.trafficConeEnemy,
        "traffic_cone"                  => capitalHighway?.trafficCone,

        "barrel"                        => conflictCanyon?.barrel,
        "beetle_soldier"                => conflictCanyon?.beetleSoldier,
        "box"                           => conflictCanyon?.box,
        "cactus_assassin"               => conflictCanyon?.cactusAssassin,
        "cactus_box"                    => conflictCanyon?.cactus_box,
        "cactus"                        => conflictCanyon?.cactus,
        "cctv_camera_beetle_fixed"      => conflictCanyon?.cctvCameraBeetleFixed,
        "cctv_camera_beetle"            => conflictCanyon?.cctvCameraBeetle,
        "cctv_camera_fixed"             => conflictCanyon?.cctvCameraFixed,
        "cctv_camera_gun"               => conflictCanyon?.cctvCameraGun,
        "cctv_camera"                   => conflictCanyon?.cctvCamera,
        "cctv_gate_block_corner"        => conflictCanyon?.cctvGateBlockCorner,
        "cctv_gate_block_horizontal"    => conflictCanyon?.cctvGateBlockHorizontal,
        "cctv_gate_block_vertical"      => conflictCanyon?.cctvGateBlockVertical,
        "conveyor_left_fast"            => conflictCanyon?.conveyorBlockFast,
        "conveyor_left_slow"            => conflictCanyon?.conveyorBlockSlow,
        "conveyor_left"                 => conflictCanyon?.conveyorBlock,
        "conveyor_right_fast"           => conflictCanyon?.conveyorBlockFast,
        "conveyor_right_slow"           => conflictCanyon?.conveyorBlockSlow,
        "conveyor_right"                => conflictCanyon?.conveyorBlock,
        "grub"                          => conflictCanyon?.grub,
        "locker"                        => conflictCanyon?.locker,
        "mine"                          => conflictCanyon?.mine,

        "cold_gate_end"                 => hotNColdSprings?.coldGateEnd,
        "cold_switch"                   => hotNColdSprings?.coldSwitch,
        "geyser"                        => hotNColdSprings?.geyser,
        "hippo"                         => hotNColdSprings?.hippo,
        "hot_and_cold_gate"             => hotNColdSprings?.hotAndColdGate,
        "hot_gate_end"                  => hotNColdSprings?.hotGateEnd,
        "hot_switch"                    => hotNColdSprings?.hotSwitch,
        "hotcold_platform_block"        => hotNColdSprings?.hotColdPlatformBlock,
        "hotcold_platform_cog"          => hotNColdSprings?.hotColdPlatformCog,
        "icicle"                        => hotNColdSprings?.icicle,
        "spin_trap_centre"              => hotNColdSprings?.spinTrapCentre,
        "spin_trap_part"                => hotNColdSprings?.spinTrapPart,
        "turtle_cannon"                 => hotNColdSprings?.turtleCannon,

        "airlock"                           => gravityGalaxy?.airlock,
        "airlock_and_cannon"                => gravityGalaxy?.airlockAndCannon,
        "alien_skull"                       => gravityGalaxy?.alienSkull,
        "alien_squid"                       => gravityGalaxy?.alienSquid,
        "gravity_area_none"                 => gravityGalaxy?.gravityArea,
        "gravity_area_up"                   => gravityGalaxy?.gravityArea,
        "gravity_atmosphere_flat"           => gravityGalaxy?.gravityAtmosphereFlat,
        "gravity_atmosphere_curvebr3plus1"  => gravityGalaxy?.gravityAtmosphereCurveBR3Plus1,
        "gravity_atmosphere_curveoutbr3"    => gravityGalaxy?.gravityAtmosphereCurveOutBR3,
        "gravity_atmosphere_corner"         => gravityGalaxy?.gravityAtmosphereCorner,
        "gravity_button_down"               => gravityGalaxy?.gravityButton,
        "gravity_button_left"               => gravityGalaxy?.gravityButton,
        "gravity_button_right"              => gravityGalaxy?.gravityButton,
        "gravity_button_up"                 => gravityGalaxy?.gravityButton,
        "gravity_display"                   => gravityGalaxy?.gravityDisplay,
        "gravity_guardian"                  => gravityGalaxy?.gravityGuardian,
        "gravity_none_on_start"             => gravityGalaxy?.gravityOnStart,
        "gravity_up_on_start"               => gravityGalaxy?.gravityOnStart,
        "laser_cannon"                      => gravityGalaxy?.laserCannon,
        "portal"                            => gravityGalaxy?.portal,
        "space_booster"                     => gravityGalaxy?.spaceBooster,
        "rocket"                            => gravityGalaxy?.rocket,

        "water_raiser_row"                  => sunkenIsland?.horizontalWaterRaiser,
        "water_raiser_column"               => sunkenIsland?.verticalWaterRaiser,
        "water_raiser_trigger"              => sunkenIsland?.waterRaiserTrigger,
        "water_raiser_switch"               => sunkenIsland?.waterRaiserSwitch,
        "water_raiser_button"               => sunkenIsland?.waterRaiserButton,
        "water_marker"                      => sunkenIsland?.waterMarker,
        "water_marker_loop"                 => sunkenIsland?.waterMarkerLoop,
        "raft_3x1"                          => sunkenIsland?.raft3x1,
        "raft_4x1"                          => sunkenIsland?.raft4x1,
        "raft_5x1"                          => sunkenIsland?.raft5x1,
        "raft_6x1"                          => sunkenIsland?.raft6x1,
        "raft_7x1"                          => sunkenIsland?.raft7x1,
        "raft_8x1"                          => sunkenIsland?.raft8x1,
        "raft_motor_left"                   => sunkenIsland?.raftMotorLeft,
        "raft_motor_right"                  => sunkenIsland?.raftMotorRight,
        "raft_motor_both"                   => sunkenIsland?.raftMotorBoth,
        "raft_motor_rod"                    => sunkenIsland?.raftMotorRod,
        "raft_motor_extension"              => sunkenIsland?.raftMotorExtension,
        "jellyfish_alwayson"                => sunkenIsland?.jellyfishAlwaysOn,
        "jellyfish_toggle"                  => sunkenIsland?.jellyfishToggle,
        "fish_skull"                        => sunkenIsland?.fishSkull,
        "piranha"                           => sunkenIsland?.piranha,
        "sea_anchor"                        => sunkenIsland?.seaAnchor,
        "sea_mine"                          => sunkenIsland?.seaMine,
        "sea_mine_source"                   => sunkenIsland?.seaMineSource,
        "clam"                              => sunkenIsland?.clam,
        "floating_duck"                     => sunkenIsland?.floatingDuck,
        "water_current_straight"            => sunkenIsland?.waterCurrentStraight,
        "water_current_corner"              => sunkenIsland?.waterCurrentCorner,
        "fast_water_current_straight"       => sunkenIsland?.fastWaterCurrentStraight,
        "fast_water_current_corner"         => sunkenIsland?.fastWaterCurrentCorner,
        "beach_ball"                        => sunkenIsland?.beachBall,
        "apple_fish"                        => sunkenIsland?.appleFish,
        "banana_fish"                       => sunkenIsland?.bananaFish,
        "melon_fish"                        => sunkenIsland?.melonFish,
        "pear_fish"                         => sunkenIsland?.pearFish,
        "orangee_fish"                       => sunkenIsland?.orangeFish,

        "minecart"                  => treasureMines?.minecart,
        "minecart_enemy"            => treasureMines?.minecartWithEnemy,
        "minecart_trigger"          => treasureMines?.triggerMinecart,
        "minecart_enemy_trigger"    => treasureMines?.triggerMinecartWithEnemy,
        "rail"                      => treasureMines?.rail,
        "mine_cannon"               => treasureMines?.mineCannon,
        "bat_straight"              => treasureMines?.batStraight,
        "bat_follow"                => treasureMines?.batFollow,
        "unstable_ground"           => treasureMines?.unstableGround,
        "rebounder"                 => treasureMines?.rebounder,
        "rail_rebounder"            => treasureMines?.railRebounder,
        "rail_speed_booster"        => treasureMines?.railSpeedBooster,
        "rail_rod_cap"              => treasureMines?.railRodCap,
        "rail_rod_body"             => treasureMines?.railRodBody,
        "rail_rod_connector"        => treasureMines?.railRodConnector,
        "rail_barrier"              => treasureMines?.railBarrier,
        "rail_deadly_barrier"       => treasureMines?.railDeadlyBarrier,
        "rail_door"                 => treasureMines?.railDoor,
        "rail_door_enemies_only"    => treasureMines?.railDoorEnemiesOnly,
        "rail_electrosnake"         => treasureMines?.railElectrosnake,
        "random_crystal_fruit"      => RandomCrystalFruit(),
        "crystal_apple"             => treasureMines?.crystalApple,
        "crystal_banana"            => treasureMines?.crystalBanana,
        "crystal_cherry"            => treasureMines?.crystalCherry,
        "crystal_kiwi"              => treasureMines?.crystalKiwi,
        "crystal_lemon"             => treasureMines?.crystalLemon,
        "crystal_orange"            => treasureMines?.crystalOrange,
        "crystal_pear"              => treasureMines?.crystalPear,
        "crystal_pineapple"         => treasureMines?.crystalPineapple,
        "crystal_strawberry"        => treasureMines?.crystalStrawberry,
        "crystal_watermelon"        => treasureMines?.crystalWatermelon,
        "crystal_barrier_block"     => treasureMines?.crystalBarrierBlock,
        "crystal_barrier_leader"    => treasureMines?.crystalBarrierLeader,
        "minecart_stopper"          => treasureMines?.minecartStopper,
        "crystal_spike"             => RandomCrystalSpike(),

        "parachute_block_on"                => windySkies?.parachuteBlockOn,
        "parachute_block_off"               => windySkies?.parachuteBlockOff,
        "flying_ring"                       => windySkies?.flyingRing,
        "boost_flying_ring"                 => windySkies?.boostFlyingRing,
        "downwards_boost_flying_ring"       => windySkies?.downwardsBoostFlyingRing,
        "angled_boost_flying_ring"          => windySkies?.angledBoostFlyingRing,
        "big_propeller"                     => windySkies?.bigPropeller,
        "tile_propeller"                    => windySkies?.tilePropeller,
        "tile_propeller_leader"             => windySkies?.tilePropellerLeader,
        "air_propeller"                     => windySkies?.airPropeller,
        "parachute"                         => windySkies?.parachute,
        "air_current_straight"              => windySkies?.airCurrentStraight,
        "air_current_corner"                => windySkies?.airCurrentCorner,
        "blowing_enemy"                     => windySkies?.blowingEnemy,
        "dandelion"                         => windySkies?.dandelion,
        "shooting_sun"                      => windySkies?.shootingSun,
        "cloud_block"                       => windySkies?.cloudBlock,
        "cloud_bird"                        => windySkies?.cloudBird,
        "homing_cloud"                      => windySkies?.homingCloud,
        "solid_balloon"                     => windySkies?.solidBalloon,
        "solid_fan"                         => windySkies?.solidFan,
        "windy_skies_finish_side"           => windySkies?.finishSide,
        "windy_skies_finish_platform"       => windySkies?.finishPlatform,
        "windy_skies_start_side"            => windySkies?.startSide,
        "windy_skies_start_platform"        => windySkies?.startPlatform,
        "windy_skies_start_fan"             => windySkies?.startFan,
        "windy_skies_multiplayer_overlay"   => windySkies?.multiplayerOverlay,
        "parachute_dispenser"               => windySkies?.parachuteDispenser,
        "flying_ring_sequence"              => windySkies?.flyingRingSequence,

        "rumble_ground"                 => tombstoneHill?.rumbleGround,
        "rumble_ground_block"           => tombstoneHill?.rumbleGroundBlock,
        "statue_trigger_block"          => tombstoneHill?.statueTriggerBlock,
        "cursed_shrine"                 => tombstoneHill?.cursedShrine,
        "invisible_cursed_shrine"       => tombstoneHill?.invisibleCursedShrine,
        "haunted_statue"                => tombstoneHill?.hauntedStatue,
        "snake_ghost"                   => tombstoneHill?.snakeGhostGrave,
        "snake_ghost_trap"              => tombstoneHill?.snakeGhostTrap,
        "zombie"                        => tombstoneHill?.zombie,
        "guillotine"                    => tombstoneHill?.guillotine,
        "guillotine_without_frame"      => tombstoneHill?.guillotineWithoutFrame,
        "guillotine_frame_topleft"      => tombstoneHill?.guillotineFrameTopLeft,
        "guillotine_frame_topcenter"    => tombstoneHill?.guillotineFrameTopCenter,
        "guillotine_frame_topright"     => tombstoneHill?.guillotineFrameTopRight,
        "sticky_block"                  => tombstoneHill?.stickyBlock,
        "zombie_hand"                   => tombstoneHill?.zombieHand,

        "lava_raiser_row"                  => moltenFortress?.horizontalLavaRaiser,
        "lava_raiser_column"               => moltenFortress?.verticalLavaRaiser,
        "lava_raiser_trigger"              => moltenFortress?.lavaRaiserTrigger,
        "lava_raiser_switch"               => moltenFortress?.lavaRaiserSwitch,
        "lava_raiser_button"               => moltenFortress?.lavaRaiserButton,
        "lava_marker"                      => moltenFortress?.lavaMarker,
        "lava_marker_loop"                 => moltenFortress?.lavaMarkerLoop,
        "rock_button"                      => moltenFortress?.rockButton,
        "ash_block"                        => moltenFortress?.ashBlock,
        "ash_skull"                        => moltenFortress?.ashSkull,
        "brain_box_4x4"                    => moltenFortress?.brainbox4x4,
        "brain_box_2x2"                    => moltenFortress?.brainbox2x2,
        "bully"                            => moltenFortress?.bully,
        "spin_block"                       => moltenFortress?.spinBlock,
        "spin_block_1_cannon"              => moltenFortress?.spinBlock1Cannon,
        "spin_block_2_cannons"             => moltenFortress?.spinBlock2Cannons,
        "auto_cannon"                      => moltenFortress?.autoCannon,
        "chain_fixed"                      => moltenFortress?.chain,
        "chain_free"                       => moltenFortress?.chain,
        "swinging_platform_6x2"            => moltenFortress?.swingingPlatform6x2,
        "swinging_platform_10x2"           => moltenFortress?.swingingPlatform10x2,
        "swinging_platform_14x2"           => moltenFortress?.swingingPlatform14x2,
        "swinging_platform_2x6"            => moltenFortress?.swingingPlatform2x6,
        "swinging_platform_2x10"           => moltenFortress?.swingingPlatform2x10,
        "swinging_platform_2x14"           => moltenFortress?.swingingPlatform2x14,
        "swinging_platform_anchor"         => moltenFortress?.swingingPlatformAnchor,
        "swinging_spike_ball"              => moltenFortress?.swingingSpikeBall,
        "zipline"                          => moltenFortress?.zipline,
        "zipline_climber"                  => moltenFortress?.ziplineClimber,

        "opening_tutorial_sign_jump"        => openingTutorial?.signJump,
        "opening_tutorial_sign_double_jump" => openingTutorial?.signDoubleJump,
        "opening_tutorial_sign_wall_jump"   => openingTutorial?.signWallJump,
        "opening_tutorial_sign_checkpoint"  => openingTutorial?.signCheckpoint,
        "opening_tutorial_sign_enemy"       => openingTutorial?.signEnemy,
        "opening_tutorial_sign_chest"       => openingTutorial?.signChest,
        "opening_tutorial_sign_arrow"       => openingTutorial?.signArrow,
        "opening_tutorial_light1"           => openingTutorial?.light1,
        "opening_tutorial_light2"           => openingTutorial?.light2,
        "opening_tutorial_box1"             => openingTutorial?.box1,
        "opening_tutorial_box2"             => openingTutorial?.box2,
        "opening_tutorial_box3"             => openingTutorial?.box3,
        "opening_tutorial_box4"             => openingTutorial?.box4,
        "opening_tutorial_box5"             => openingTutorial?.box5,
        "opening_tutorial_lift"             => openingTutorial?.lift,
        "opening_tutorial_trophy"           => openingTutorial?.trophy,

        _                               => null
    };

    public GameObject RandomFruit() => Util.RandomChoice(
        apple, banana, cherry, kiwi, lemon, orange, pear, pineapple, strawberry
    );

    public GameObject RandomCrystalFruit() => Util.RandomChoice(
        treasureMines?.crystalApple, treasureMines?.crystalBanana, treasureMines?.crystalCherry, treasureMines?.crystalKiwi, treasureMines?.crystalLemon,
        treasureMines?.crystalOrange, treasureMines?.crystalPear, treasureMines?.crystalPineapple, treasureMines?.crystalStrawberry
    );

    public GameObject RandomCrystalSpike() => Util.RandomChoice(
        treasureMines?.crystalSpike1, treasureMines?.crystalSpike2, treasureMines?.crystalSpike3, treasureMines?.crystalSpike4
    );
}
