using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Capital Highway")]
public class AssetsForCapitalHighway : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundCity background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject trafficCone;
    public GameObject trafficConeEnemy;
    public GameObject boostGateRight;
    public GameObject boostGateLeft;
    public GameObject gripGroundSquare;
    public GameObject gripGroundCurve3Plus1;
    public GameObject gripGroundCurveOut3;
    public GameObject crashBarrier2x1;
    public GameObject crashBarrier3x1;
    public GameObject crashBarrier4x1;
    public GameObject crashBarrier5x1;
    public GameObject sewerPipeCorner;
    public GameObject sewerPipeExit;
    public GameObject sewerPipeStraight;
    public GameObject sewerPipeSelectAllDirections;
    public GameObject sewerPipeSelectLeftDownRight;
    public GameObject sewerPipeSelectUpLeftRight;
    public GameObject sewerPipeSelectUpRightDown;
    public GameObject sewerPipeStopDownLeft;
    public GameObject sewerPipeStopUpLeft;
    public GameObject sewerPipeStopHorizontal;
    public GameObject sewerPipeStopVertical;
    public GameObject sewerPipeManhole;
    public GameObject sewerPipeCannonHorizontal;
    public GameObject sewerPipeCannonVertical;
    public GameObject manholeMonster;
    public GameObject manholeMonsterBullet;
    public GameObject floorCop;
    public GameObject touchPlatformBlock;
    public GameObject touchPlatformCog;
    public GameObject touchPlatformLight;
    public GameObject touchPlatformPathLane;
    public GameObject touchPlatformPathLaneEnd;
    public GameObject touchPlatformPathLaneStop;
    public GameObject terrorgeon;
    public GameObject terrorgeonEgg;
    public GameObject robotCopEnemy;
    public GameObject cityCannon;
}
