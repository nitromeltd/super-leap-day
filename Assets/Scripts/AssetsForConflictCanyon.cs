using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Conflict Canyon")]
public class AssetsForConflictCanyon : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundDesert background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject cactus;
    public GameObject cactusSpike;
    public GameObject cactusAssassin;
    public GameObject cactusAssassinRocket;
    public GameObject beetleSoldier;
    public GameObject beetleSoldierRocket;
    public GameObject box;
    public GameObject barrel;
    public GameObject cactus_box;
    public GameObject locker;
    public GameObject cctvCamera;
    public GameObject cctvCameraFixed;
    public GameObject cctvCameraBeetle;
    public GameObject cctvCameraBeetleFixed;
    public GameObject cctvCameraGun;
    public GameObject cctvGateBlockHorizontal;
    public GameObject cctvGateBlockVertical;
    public GameObject cctvGateBlockCorner;
    public GameObject homingMissile;
    public GameObject conveyorBlock;
    public GameObject conveyorBlockFast;
    public GameObject conveyorBlockSlow;
    public GameObject grub;
    public GameObject mine;
}
