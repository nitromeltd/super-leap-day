using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Gravity Galaxy")]
public class AssetsForGravityGalaxy : ScriptableObject
{
    public TileTheme tileThemeExterior;
    public TileTheme tileThemeInterior;
    public BackgroundSpace background;
    public OutsideBoundaryForGravityGalaxy outsideBoundary;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject gravityOnStart;
    public GameObject gravityButton;
    public GameObject gravityArea;
    public GameObject gravityDisplay;
    public GameObject gravityAtmosphereFlat;
    public GameObject gravityAtmosphereCurveBR3Plus1;
    public GameObject gravityAtmosphereCurveOutBR3;
    public GameObject gravityAtmosphereCorner;
    public GameObject spaceBooster;
    public GameObject alienSquid;
    public GameObject alienSquidBullet;
    public GameObject alienSkull;
    public GameObject portal;
    public GameObject gravityGuardian;
    public GameObject laserCannon;
    public GameObject airlock;
    public GameObject airlockAndCannon;
    public GameObject airlockPeg;
    public GameObject rocket;

    [Header("Sound effects")]
    public AudioClip sfxRocketFlying;
}
