using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Hot 'n Cold Springs")]
public class AssetsForHotNColdSprings : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundHotNCold background;
    public AudioClip musicHot;
    public AudioClip musicCold;

    [Header("Items and Enemies")]
    public GameObject hotGateEnd;
    public GameObject hotSwitch;    
    public GameObject coldGateEnd;
    public GameObject hotAndColdGate;
    public GameObject coldSwitch;
    public GameObject turtleCannon;
    public GameObject turtleFireBlock;
    public GameObject hippo;
    public GameObject geyser;
    public GameObject spinTrapCentre;
    public GameObject spinTrapPart;
    public GameObject icicle;
    public GameObject hotColdPlatformBlock;
    public GameObject hotColdPlatformCog;
    public GameObject touchPlatformPathLane;
    public GameObject touchPlatformPathLaneEnd;
    public GameObject touchPlatformPathLaneStop;
}
