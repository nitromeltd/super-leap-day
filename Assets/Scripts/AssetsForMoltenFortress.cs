using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Molten Fortress Theme")]
public class AssetsForMoltenFortress : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundMoltenFortress background;
    public AudioClip music;
    
    [Header("Items and Enemies")]
    public GameObject lava;
    public GameObject horizontalLavaRaiser;
    public GameObject verticalLavaRaiser;
    public GameObject lavaRaiserTrigger;
    public GameObject lavaRaiserSwitch;
    public GameObject lavaRaiserButton;
    public GameObject lavaMarker;
    public GameObject lavaMarkerLoop;
    public GameObject rockButton;
    public GameObject ashBlock;
    public GameObject ashSkull;
    public GameObject brainbox4x4;
    public GameObject brainbox2x2;
    public GameObject bully;
    public GameObject spinBlock;
    public GameObject spinBlock1Cannon;
    public GameObject spinBlock2Cannons;
    public GameObject autoCannon;
    public GameObject autoCannonBullet;
    public GameObject chain;
    public GameObject swingingPlatform6x2;
    public GameObject swingingPlatform10x2;
    public GameObject swingingPlatform14x2;
    public GameObject swingingPlatform2x6;
    public GameObject swingingPlatform2x10;
    public GameObject swingingPlatform2x14;
    public GameObject swingingPlatformAnchor;
    public GameObject swingingSpikeBall;
    public GameObject zipline;
    public GameObject ziplineClimber;

    [Header("Lava sprites")]
    public Sprite spriteSmallBubbleSubmerged;
    public Sprite spriteSmallBubbleSurface;
    public Animated.Animation animationSmallBubblePop;
    public Sprite spriteBigBubbleSubmerged;
    public Sprite spriteBigBubbleSurface;
    public Animated.Animation animationBigBubblePop;

    [Header("Sound effects")]
    public AudioClip sfxBrainboxSlam;
    public AudioClip sfxBrainboxSlide;
    public AudioClip sfxBrainboxTurn;
    public AudioClip sfxBullyAggro;
    public AudioClip sfxBullyDeath;
    public AudioClip sfxBullyRun;
    public AudioClip sfxBullySlam;
    public AudioClip sfxBullyWobble;
    public AudioClip sfxChainGrab;
    public AudioClip sfxChainJump;
    public AudioClip sfxChainSlideLoop;
    public AudioClip sfxLavaAppear;
    public AudioClip sfxLavaDisappear;
    public AudioClip sfxLavaSpoutAsh;
    public AudioClip sfxLavaSpoutRise;
    public AudioClip sfxLavaSpoutSpawn;
    public AudioClip sfxSpinBlockExplode;
    public AudioClip sfxSpinBlockFire;
    public AudioClip sfxSpinBlockTurn;
}
