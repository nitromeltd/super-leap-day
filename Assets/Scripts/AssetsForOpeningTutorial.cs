using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Opening Tutorial")]
public class AssetsForOpeningTutorial : ScriptableObject
{
    public TileTheme tileTheme;
    public Background background;

    public GameObject lift;
    public GameObject trophy;

    public AudioClip music;

    [Header("Decorations")]
    public GameObject light1;
    public GameObject light2;
    public GameObject box1;
    public GameObject box2;
    public GameObject box3;
    public GameObject box4;
    public GameObject box5;

    [Header("Signs")]
    public GameObject signJump;
    public GameObject signDoubleJump;
    public GameObject signWallJump;
    public GameObject signCheckpoint;
    public GameObject signEnemy;
    public GameObject signChest;
    public GameObject signArrow;
}
