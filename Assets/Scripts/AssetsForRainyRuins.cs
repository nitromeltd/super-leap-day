using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Rainy Ruins")]
public class AssetsForRainyRuins : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundTemple background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject helmut;
    public GameObject helmutHelmet;
    public GameObject wildTrunky;
    public GameObject wildTrunkyBone;
    public GameObject scuttlebug;
    public GameObject rhinoBug;
    public GameObject spear;
    public GameObject spearSpawner;
    public GameObject bamboo;
    public GameObject rope;
    public GameObject swingingAxe;
    public GameObject appleBoulderBig;
    public GameObject appleBoulderSmall;
    public GameObject appleBoulderEntranceBig;
    public GameObject appleBoulderEntranceBigRight;
    public GameObject appleBoulderEntranceBigLeft;
    public GameObject appleBoulderEntranceSmall;
    public GameObject appleBoulderEntranceSmallRight;
    public GameObject appleBoulderEntranceSmallLeft;
}
