using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Sunken Island")]
public class AssetsForSunkenIsland : ScriptableObject
{
    public TileTheme tileTheme;
    public TileTheme tileThemeForInside;
    public BackgroundWater background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject water;
    public GameObject horizontalWaterRaiser;
    public GameObject verticalWaterRaiser;
    public GameObject waterRaiserTrigger;
    public GameObject waterRaiserSwitch;
    public GameObject waterRaiserButton;
    public GameObject waterMarker;
    public GameObject waterMarkerLoop;
    public GameObject raft3x1;
    public GameObject raft4x1;
    public GameObject raft5x1;
    public GameObject raft6x1;
    public GameObject raft7x1;
    public GameObject raft8x1;
    public GameObject raftMotorLeft;
    public GameObject raftMotorRight;
    public GameObject raftMotorBoth;
    public GameObject raftMotorRod;
    public GameObject raftMotorExtension;
    public GameObject jellyfishAlwaysOn;
    public GameObject jellyfishToggle;
    public GameObject fishSkull;
    public GameObject piranha;
    public GameObject seaAnchor;
    public GameObject seaMine;
    public GameObject seaMineSource;
    public GameObject clam;
    public GameObject floatingDuck;
    public GameObject waterCurrentStraight;
    public GameObject waterCurrentCorner;
    public GameObject fastWaterCurrentStraight;
    public GameObject fastWaterCurrentCorner;
    public GameObject beachBall;
    public GameObject appleFish;
    public GameObject bananaFish;
    public GameObject melonFish;
    public GameObject pearFish;
    public GameObject orangeFish;

    [Header("Particles and Effects")]
    public GameObject swimmingSplashEffect;
    public Sprite[] bubbleParticles;
    public Sprite[] waterParticles;
    public Animated.Animation waterSplash;
    public AnimationCurve scaleOutCurveForBubbleParticles;
    public AnimationCurve scaleOutCurveForWaterParticles;

    public Sprite RandomBubbleParticle => this.bubbleParticles[Random.Range(0, this.bubbleParticles.Length)];
    public Sprite RandomWaterParticle => this.waterParticles[Random.Range(0, this.waterParticles.Length)];
}
