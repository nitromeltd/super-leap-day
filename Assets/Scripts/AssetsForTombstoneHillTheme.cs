using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Tombstone Hill Theme")]
public class AssetsForTombstoneHillTheme : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundTombstoneHill background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject rumbleGround;
    public GameObject rumbleGroundBlock;
    public GameObject statueTriggerBlock;
    public GameObject cursedShrine;
    public GameObject invisibleCursedShrine;
    public GameObject hauntedStatue;
    public Sprite[] hauntedStatueStones;
    public GameObject snakeGhostGrave;
    public GameObject snakeGhost;
    public GameObject snakeGhostSection;
    public Sprite snakeGhostBodySpriteLine;
    public Sprite snakeGhostBodySpriteBack;
    public Sprite snakeGhostBodySpriteEdge;
    public Sprite snakeGhostMediumTailSprite;
    public Sprite snakeGhostHeadSpriteLine;
    public Sprite snakeGhostHeadSpriteBack;
    public Sprite snakeGhostHeadSpriteEdge;
    public Sprite[] snakeGhostTailSpritesLine;
    public Sprite[] snakeGhostTailSpritesBack;
    public Sprite[] snakeGhostTailSpritesEdge;
    public Sprite[] snakeGhostHeadBoneSprites;
    public Sprite snakeGhostBodyBoneSprite;
    public Sprite snakeGhostTailBoneSprite;
    public GameObject snakeGhostTail;
    public GameObject snakeGhostTrap;
    public GameObject zombie;
    public GameObject zombiePiece;
    public GameObject guillotine;
    public GameObject guillotineWithoutFrame;
    public GameObject guillotineFrameTopLeft;
    public GameObject guillotineFrameTopRight;
    public GameObject guillotineFrameTopCenter;
    public GameObject stickyBlock;
    public GameObject zombieHand;
    public Tileset activeHaunteStatueBlocks;
    public Tileset inactiveHaunteStatueBlocks;

    [Header("Sound effects")]
    public AudioClip sfxGargoyleLand;
    public AudioClip sfxGargoyleStone;
    public AudioClip sfxGargoyleWake;
    public AudioClip sfxGhostTombAppear;
    public AudioClip sfxGhostTombRise;
    public AudioClip sfxGhostTrapGhostSuck;
    public AudioClip sfxGhostTrapLoop;
    public AudioClip sfxGuillotineLand;
    public AudioClip sfxGuillotineDrop;
    public AudioClip sfxGuillotineRise;
    public AudioClip sfxMagicGround;
    public AudioClip sfxTombstoneTriggerOff;
    public AudioClip sfxTombstoneTriggerOn;
    public AudioClip sfxZombieResurrect;
    public AudioClip sfxZombieRise;
    public AudioClip sfxZombieShatter;
    public AudioClip sfxZombieHandAppear;
    public AudioClip sfxZombieHandBreak;
}
