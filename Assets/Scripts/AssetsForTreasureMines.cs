using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Treasure Mines")]
public class AssetsForTreasureMines : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundTreasureMines background;
    public AudioClip music;

    [Header("Items and Enemies")]
    public GameObject minecart;
    public GameObject minecartWithEnemy;
    public GameObject triggerMinecart;
    public GameObject triggerMinecartWithEnemy;
    public GameObject rail;
    public GameObject mineCannon;
    public GameObject unstableGround;
    public GameObject rebounder;
    public GameObject railRebounder;
    public GameObject railSpeedBooster;
    public GameObject railRodCap;
    public GameObject railRodBody;
    public GameObject railBarrier;
    public GameObject railRodConnector;
    public GameObject railDeadlyBarrier;
    public GameObject railDoor;
    public GameObject railDoorEnemiesOnly;
    public GameObject railElectrosnake;
    public GameObject railShock;
    public GameObject batStraight;
    public GameObject batFollow;
    public GameObject crystalApple;
    public GameObject crystalBanana;
    public GameObject crystalCherry;
    public GameObject crystalKiwi;
    public GameObject crystalLemon;
    public GameObject crystalOrange;
    public GameObject crystalPear;
    public GameObject crystalPineapple;
    public GameObject crystalStrawberry;
    public GameObject crystalWatermelon;
    public GameObject crystalBarrierBlock;
    public GameObject crystalBarrierLeader;
    public GameObject crystalSpike1;
    public GameObject crystalSpike2;
    public GameObject crystalSpike3;
    public GameObject crystalSpike4;
    public GameObject minecartStopper;
}
