using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for VR Training Room")]
public class AssetsForVRTrainingRoom : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundVRRoom background;
}
