using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Assets for Air Theme")]
public class AssetsForWindySkiesTheme : ScriptableObject
{
    public TileTheme tileTheme;
    public BackgroundWindySkies background;
    public AudioClip music;
    
    [Header("Items and Enemies")]
    public GameObject parachuteBlockOn;
    public GameObject parachuteBlockOff;
    public GameObject flyingRing;
    public GameObject boostFlyingRing;
    public GameObject downwardsBoostFlyingRing;
    public GameObject angledBoostFlyingRing;
    public GameObject shootingSun;
    public GameObject cloudBlock;
    public GameObject shootingSunRay;
    public GameObject homingCloud;
    public GameObject solidBalloon;
    public GameObject solidFan;
    public GameObject solidBalloonChain;
    public GameObject finishSide;
    public GameObject finishPlatform;
    public GameObject startSide;
    public GameObject startPlatform;
    public GameObject startFan;
    public GameObject multiplayerOverlay;
    public GameObject cloudBird;
    public GameObject bigPropeller;
    public GameObject tilePropeller;
    public GameObject tilePropellerLeader;
    public GameObject airPropeller;
    public GameObject parachute;
    public GameObject parachuteDispenser;
    public GameObject airCurrentStraight;
    public GameObject airCurrentCorner;
    public GameObject blowingEnemy;
    public GameObject blowingEnemyPathLane;
    public GameObject blowingEnemyPathLaneStop;
    public GameObject blowingEnemyPathLaneEnd;
    public GameObject dandelion;
    public GameObject flyingRingSequence;
    public GameObject tileDecorationBigFan;
    public GameObject tileDecorationSmallFan;
    public GameObject tileDecorationLadderTop;
    public GameObject[] tileDecorationLadderMiddle;
    public GameObject tileDecorationLadderBottom;
    public GameObject[] tileDecorationWindows;
    // public Sprite   tileDecorationPillarFan;
    public GameObject tileDecorationPillarFan;
    public GameObject tileDecorationLargeBackJoiner;
    public GameObject tileDecorationSmallBackJoiner;
    public GameObject tileDecorationLargeSideCannon;
    public GameObject tileDecorationSmallSideCannon;
    public GameObject tileDecorationMetalTileTop;
    public GameObject tileDecorationMetalTileBottom;
    public GameObject[] tileDecorationMetalTileMiddle;
    public Sprite tileDecorationPillarBase;
    public Sprite[] cloudParticlesOutline;
    public Sprite[] cloudParticlesBack;
    public Sprite[] cloudParticlesFront;
    public AnimationCurve cloudParticleScaleOutCurve;
}
