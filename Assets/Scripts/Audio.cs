using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

public class Audio : MonoBehaviour
{
    private static Audio _instance;

    public static Audio instance
    {
        get { return _instance; }
    }

    private GameObject audioChannelMusicParent;
    private GameObject audioChannelSfxParent;

    private List<AudioSource> pooledMusicAudioSources = new List<AudioSource>();
    private List<AudioSource> activeMusicAudioSources = new List<AudioSource>();

    private List<AudioSource> pooledSfxAudioSources = new List<AudioSource>();
    private List<AudioSource> activeSfxAudioSources = new List<AudioSource>();

    private Dictionary<AudioClip, ClipData> clipsData;
    private float musicVolume = 1;
    private float sfxVolume = 1;
    private int frameNumber = 0;

    private AudioSource currentlyPlayingMusicSource;

    public class Options
    {
        public float volume;
        public bool randomizePitch;
        public float pitchIncrement;

        public Options(
            float volume = 1f,
            bool randomizePitch = true,
            float pitchIncrement = 0f)
        {
            this.volume = volume;
            this.randomizePitch = randomizePitch;
            this.pitchIncrement = pitchIncrement;
        }
    }

    // This struct holds information of an AudioClip that
    // has been played at a certain point in the past
    public struct ClipData
    {
        public int timeOfLastPlay;
        public float currentPitch;
        public bool isLooping;

        public ClipData(int timeOfLastPlay, float currentPitch, bool isLooping)
        {
            this.timeOfLastPlay = timeOfLastPlay;
            this.currentPitch = currentPitch;
            this.isLooping = isLooping;
        }
    }

    private const int AudioSourcePoolStartingSize = 10;
    private const int FramesToResetPitchBackToDefault = 60 * 2;
    private const float PitchDefaultValue = 1f;
    private const float PitchMaxValue = 2f;
    private const float PitchVariation = 0.15f;
    private const float SoundMaxDistance = 60f;

    public void Start()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        _instance = this;

        DontDestroyOnLoad(this.gameObject);

        musicVolume = SaveData.GetMusicVolume();
        sfxVolume = SaveData.GetSfxVolume();
        
        audioChannelMusicParent = GameObject.Find("Audio Channels (music)");
        audioChannelSfxParent = GameObject.Find("Audio Channels (SFXs)");
        clipsData = new Dictionary<AudioClip, ClipData>();

        pooledSfxAudioSources.Clear();
        activeSfxAudioSources.Clear();

        pooledMusicAudioSources.Clear();
        activeMusicAudioSources.Clear();

        for (int i = 0; i < AudioSourcePoolStartingSize; i++)
        {
            AudioSource audio = GetNewSfxAudioSource();
            audio.gameObject.SetActive(false);
            pooledSfxAudioSources.Add(audio);
        }
    }

    public void Update()
    {
        frameNumber += 1;

        for (var i = activeSfxAudioSources.Count - 1; i >= 0; i -= 1)
        {
            if (activeSfxAudioSources[i] != null &&
                activeSfxAudioSources[i].isPlaying == false)
            {
                ReturnAudioSourceToPool(activeSfxAudioSources[i]);
            }
        }

        var anyMusicSourcesPlaying =
            activeMusicAudioSources.Any(source => source.isPlaying == true);

        if (anyMusicSourcesPlaying == true)
        {
            for (var i = activeMusicAudioSources.Count - 1; i >= 0; i -= 1)
            {
                if (activeMusicAudioSources[i] != null &&
                    activeMusicAudioSources[i].isPlaying == false)
                {
                    ReturnMusicAudioSourceToPool(activeMusicAudioSources[i]);
                }
            }
        }
    }

    private AudioSource GetSfxAudioSourceFromPool()
    {
        AudioSource currentAudioSource = null;

        if(pooledSfxAudioSources.Count > 0)
        {
            currentAudioSource = pooledSfxAudioSources[0];
            pooledSfxAudioSources.Remove(currentAudioSource);
        }

        if(currentAudioSource == null)
        {
            currentAudioSource = GetNewSfxAudioSource();
        }

        activeSfxAudioSources.Add(currentAudioSource);
        currentAudioSource.gameObject.SetActive(true);
        currentAudioSource.playOnAwake = false;
        currentAudioSource.loop = false;
        currentAudioSource.volume = 1f;
        currentAudioSource.pitch = PitchDefaultValue;

        return currentAudioSource;
    }

    private AudioSource GetMusicAudioSourceFromPool()
    {
        AudioSource currentAudioSource = null;

        if (pooledMusicAudioSources.Count > 0)
        {
            currentAudioSource = pooledMusicAudioSources[0];
            pooledMusicAudioSources.Remove(currentAudioSource);
        }

        if (currentAudioSource == null)
        {
            currentAudioSource = GetNewMusicAudioSource();
        }

        activeMusicAudioSources.Add(currentAudioSource);
        currentAudioSource.gameObject.SetActive(true);
        currentAudioSource.playOnAwake = false;
        currentAudioSource.loop = false;
        currentAudioSource.volume = 1f;
        currentAudioSource.pitch = 1;

        return currentAudioSource;
    }

    private AudioSource GetNewSfxAudioSource()
    {
        GameObject child = new GameObject("Audio Source Channel");
        child.transform.SetParent(audioChannelSfxParent.transform);
        return child.AddComponent<AudioSource>();
    }

    private AudioSource GetNewMusicAudioSource()
    {
        GameObject child = new GameObject("Audio Source Channel");
        child.transform.SetParent(audioChannelMusicParent.transform);
        return child.AddComponent<AudioSource>();
    }

    private void ReturnAudioSourceToPool(AudioSource audioSource)
    {
        audioSource.Stop();
        audioSource.clip = null;
        audioSource.gameObject.SetActive(false);
        pooledSfxAudioSources.Add(audioSource);
        activeSfxAudioSources.Remove(audioSource);
    }

    private void ReturnMusicAudioSourceToPool(AudioSource audioSource)
    {
        audioSource.Stop();
        audioSource.clip = null;
        audioSource.gameObject.SetActive(false);
        pooledMusicAudioSources.Add(audioSource);
        activeMusicAudioSources.Remove(audioSource);
    }
    private float VolumeOfClip(AudioClip audioClip)
    {
        if (Assets.instance == null) return 1.0f;

        //if (audioClip == Assets.instance.sfxSpring) return 0.4f;
        if (audioClip == Assets.instance.sfxFruit) return 2.0f;
        if (audioClip == Assets.instance.sfxTrophyLand) return 3.0f;
        if (audioClip == Assets.instance.sfxTrunkyFire) return 2.0f;
        if (audioClip == Assets.instance.sfxHelmutHelmetBounce) return 2.0f;
        if (audioClip == Assets.instance.sfxTempleLightning) return 0.3f;
        if (audioClip == Assets.instance.sfxVineSlide) return 0.5f;
        if (audioClip == Assets.instance.sfxLoopRainyRuinsRain) return 0.3f;
        if (audioClip == Assets.instance.sfxLoopCrowd) return 0.7f;
        return 1.0f;
    }

    public void NotifyVolumeChanged()
    {
        musicVolume = SaveData.GetMusicVolume();
        sfxVolume = SaveData.GetSfxVolume();

        for (var i = activeMusicAudioSources.Count - 1; i >= 0; i -= 1)
        {
            if (activeMusicAudioSources[i] != null &&
                activeMusicAudioSources[i].isPlaying == true &&
                activeMusicAudioSources[i] == this.currentlyPlayingMusicSource)
            {
                activeMusicAudioSources[i].volume = musicVolume;
            }
        }

        for (var i = activeSfxAudioSources.Count - 1; i >= 0; i -= 1)
        {
            if (activeSfxAudioSources[i] != null &&
                activeSfxAudioSources[i].isPlaying == true)
            {
                activeSfxAudioSources[i].volume = sfxVolume * VolumeOfClip(activeSfxAudioSources[i].clip);
            }
        }
    }

    public void PlayMusic(AudioClip audioClip, bool? loop = true, bool? stopOtherMusic = true, bool? stopAllSfx = true)
    {
        if (stopAllSfx == true)
            StopAllSfx(onlyLooped: true);

        if (stopOtherMusic == true)
        {
            AudioSource AudioSource = activeMusicAudioSources.Find(a => a.clip == audioClip);
            if (AudioSource != null)
            {
                return;
            }
            else
            {
                StopAllMusic();
            }
        }

        AudioSource chosenAudioSource = GetMusicAudioSourceFromPool();
        chosenAudioSource.clip = audioClip;
        chosenAudioSource.loop = loop.Value;
        chosenAudioSource.volume = musicVolume;
        chosenAudioSource.Play();

        this.currentlyPlayingMusicSource = chosenAudioSource;
    }

    public void PlayMusicWithVariations(AudioClip audioClip1, AudioClip audioClip2)
    {
        StopAllMusic();

        AudioSource chosenAudioSource1 = GetMusicAudioSourceFromPool();

        chosenAudioSource1.clip = audioClip1;
        chosenAudioSource1.loop = true;
        chosenAudioSource1.volume = musicVolume;
        chosenAudioSource1.Play();
        this.currentlyPlayingMusicSource = chosenAudioSource1;

        AudioSource chosenAudioSource2 = GetMusicAudioSourceFromPool();

        chosenAudioSource2.clip = audioClip2;
        chosenAudioSource2.loop = true;
        chosenAudioSource2.volume = 0;
        chosenAudioSource2.Play();
    }

    public void StopMusic(AudioClip audioClip)
    {
        AudioSource loopAudioSource = activeMusicAudioSources.Find(a => a.clip == audioClip);
        if (loopAudioSource == null) return;

        ReturnMusicAudioSourceToPool(loopAudioSource);
    }

    public void StopAllMusic()
    {
        for (var i = activeMusicAudioSources.Count - 1; i >= 0; i -= 1)
        {
            if (activeMusicAudioSources[i] != null &&
                activeMusicAudioSources[i].isPlaying == true
                )
            {
                ReturnMusicAudioSourceToPool(activeMusicAudioSources[i]);
            }
        }
    }

    public bool IsMusicPlaying(AudioClip audioClip)
    {
        AudioSource audioSource = activeMusicAudioSources.Find(a => a.clip == audioClip);
        if (audioSource != null) return true;
        
        return false;
    }
    public IEnumerator SwitchToVariationCoroutine(AudioClip audioClipFrom, AudioClip audioClipTo, float duration)
    {
        AudioSource AudioSourceFrom = activeMusicAudioSources.Find(a => a.clip == audioClipFrom);
        AudioSource AudioSourceTo = activeMusicAudioSources.Find(a => a.clip == audioClipTo);
        if (AudioSourceTo == null || AudioSourceFrom == null) yield break;

        float currentTime = 0;
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            AudioSourceFrom.volume = Mathf.Lerp(musicVolume, 0, currentTime / duration);
            AudioSourceTo.volume = Mathf.Lerp(0, musicVolume, currentTime / duration);
            yield return null;
        }

        this.currentlyPlayingMusicSource = AudioSourceTo;
        yield break;
    }

    public IEnumerator FadeMusic( float setTo, float duration, AudioClip audioClip, float? startFrom = null)
    {
        AudioSource audioSource = activeMusicAudioSources.Find(a => a.clip == audioClip);
        if(audioSource == null) yield break;

        float currentTime = 0;
        float currentVolume = audioSource.volume;
        float startValue;
        if (startFrom != null)
        {
            startValue = startFrom.Value;
        }
        else
        {
            startValue = audioSource.volume;
        }
        
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            if(audioSource != null)
            {
                audioSource.volume = Mathf.Lerp(startValue, setTo * musicVolume, currentTime / duration);
            }
            yield return null;
        }
        yield break;
    }

    public void PlaySfx(AudioClip audioClip, Options options = null, Vector3? position = null)
    {
        if (audioClip == null)
        {
            var name = Util.DescriptionOfFakeNull(audioClip);
            Debug.LogError($"Tried to play missing sound effect ({name}).");
            return;
        }

        if(options == null)
        {
            options = new Options();
        }

        ClipData clipData;
        if (clipsData.TryGetValue(audioClip, out clipData) &&
            clipData.timeOfLastPlay > frameNumber - 5) return;

        // if we're using incremental pitch...
        if(options.pitchIncrement != 0f)
        {
            // we reset the pitch to the default value if the time is due
            // or the current pitch hasn't been initialized
            if(clipData.timeOfLastPlay < frameNumber - FramesToResetPitchBackToDefault ||
                clipData.currentPitch == 0)
            {
                clipData.currentPitch = PitchDefaultValue;
            }
            // if not, we increment the current pitch value by the increment value
            else
            {
                clipData.currentPitch += options.pitchIncrement;
                clipData.currentPitch = Mathf.Clamp(clipData.currentPitch, 0f, PitchMaxValue);
            }
        }
        // if we're not using incremental pitch, we just randomize the pitch between the default variation range
        else if(options.randomizePitch == true)
        {
            clipData.currentPitch = PitchDefaultValue + Random.Range(-PitchVariation, PitchVariation);
        }
        else if (options.randomizePitch == false)
        {
            clipData.currentPitch = PitchDefaultValue;
        }

        AudioSource chosenAudioSource = GetSfxAudioSourceFromPool();

        if(position.HasValue == true)
        {
            chosenAudioSource.transform.position = position.Value;
            chosenAudioSource.spatialBlend = 1f;
            chosenAudioSource.rolloffMode = AudioRolloffMode.Linear;
            chosenAudioSource.maxDistance = SoundMaxDistance;
        }
        else
        {
            chosenAudioSource.spatialBlend = 0f;
            chosenAudioSource.rolloffMode = AudioRolloffMode.Logarithmic;
            chosenAudioSource.maxDistance = 10000f;
        }

        chosenAudioSource.pitch = clipData.currentPitch;
        chosenAudioSource.volume = sfxVolume * VolumeOfClip(audioClip);
        chosenAudioSource.time = 0f;
        chosenAudioSource.clip = audioClip;
        chosenAudioSource.Play();

        clipData.timeOfLastPlay = frameNumber;
        clipsData[audioClip] = clipData;
    }

    public void PlayRandomSfx(Options options = null, Vector2? position = null, params AudioClip[] audioClips)
    {
        AudioClip randomlyChosenClip = audioClips[Random.Range(0, audioClips.Length)];
        PlaySfx(randomlyChosenClip, options, position);
    }

    public void PlaySfxSometimes(float chance, Options options = null, Vector2? position = null, params AudioClip[] audioClips)
    {
        float rollChance = Random.value;
        if(rollChance > chance) return;

        PlayRandomSfx(options, position, audioClips);
    }

    public void StopSfx(AudioClip audioClip)
    {
        AudioSource loopAudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        if (loopAudioSource == null) return;

        ReturnAudioSourceToPool(loopAudioSource);
    }

    public void StopAllSfx(bool? onlyLooped = false)
    {
        for (var i = activeSfxAudioSources.Count - 1; i >= 0; i -= 1)
        {
            if (activeSfxAudioSources[i] != null &&
                activeSfxAudioSources[i].isPlaying == true &&
                activeSfxAudioSources[i].loop == true &&
                onlyLooped == true)
            {
                ReturnAudioSourceToPool(activeSfxAudioSources[i]);
            }
            else if (activeSfxAudioSources[i] != null &&
                activeSfxAudioSources[i].isPlaying == true &&
                onlyLooped == false)
            {
                ReturnAudioSourceToPool(activeSfxAudioSources[i]);
            }
        }
    }

    public void PlaySfxLoop(AudioClip audioClip, Vector3? position = null)
    {
        if (audioClip == null)
        {
            var name = Util.DescriptionOfFakeNull(audioClip);
            Debug.LogError($"Tried to play missing sound effect ({name}).");
            return;
        }

        AudioSource chosenAudioSource = GetSfxAudioSourceFromPool();

        ClipData clipData;
        if(clipsData.TryGetValue(audioClip, out clipData) && 
            clipData.isLooping == true) return;

        if(position.HasValue == true)
        {
            chosenAudioSource.transform.position = position.Value;
            chosenAudioSource.spatialBlend = 1f;
            chosenAudioSource.rolloffMode = AudioRolloffMode.Linear;
            chosenAudioSource.maxDistance = SoundMaxDistance;
        }
        else
        {
            chosenAudioSource.spatialBlend = 0f;
            chosenAudioSource.rolloffMode = AudioRolloffMode.Logarithmic;
            chosenAudioSource.maxDistance = 10000f;
        }

        chosenAudioSource.volume = sfxVolume * VolumeOfClip(audioClip);
        chosenAudioSource.time = 0f;
        chosenAudioSource.clip = audioClip;
        chosenAudioSource.loop = true;
        chosenAudioSource.Play();

        clipData.timeOfLastPlay = frameNumber;
        clipsData[audioClip] = clipData;
    }

    public void StopSfxLoop(AudioClip audioClip, Vector2? position = null)
    {
        AudioSource loopAudioSource;
        if(position == null)
        {
            loopAudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        }
        else
        {
            loopAudioSource = activeSfxAudioSources.Find(a => (a.clip == audioClip && a.transform.position == position));
        }
        if (loopAudioSource == null) return;

        loopAudioSource.loop = false;
        ReturnAudioSourceToPool(loopAudioSource);
    }

    public bool IsSfxPlaying(AudioClip audioClip)
    {
        AudioSource AudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        if (AudioSource == null) return false;

        return true;
    }

    public IEnumerator ChangeSfxPitch(AudioClip audioClip, float setTo, float? startFrom = null, float? duration = null)
    {
        AudioSource AudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        if (AudioSource == null) yield break;

        if (duration == null)
        {
            AudioSource.pitch = setTo;
            yield break;
        }
        float currentTime = 0;
        float startvalue = (startFrom == null) ? 0 : startFrom.Value;
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            if(AudioSource != null)
            {
                AudioSource.pitch = Mathf.Lerp(startvalue, setTo, currentTime / duration.Value);
            }
            yield return null;
        }
        yield break;
    }

    public IEnumerator ChangeSfxVolume(AudioClip audioClip, float setTo, float? startFrom = null, float? duration = null)
    {
        AudioSource AudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        if (AudioSource == null) yield break;

        if (duration == null)
        {
            AudioSource.volume = setTo * sfxVolume;
            yield break;
        }
        float currentTime = 0;
        float startvalue = (startFrom == null) ? 0 : startFrom.Value;
        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            if(AudioSource != null)
            {
                AudioSource.volume = Mathf.Lerp(startvalue * sfxVolume, setTo * sfxVolume, currentTime / duration.Value);
            }
            yield return null;
        }
        if (setTo == 0)
        {
            AudioSource.loop = false;
            ReturnAudioSourceToPool(AudioSource);
        }
        yield break;
    }

    public IEnumerator ChangeSfxPosition(AudioClip audioClip, Vector3 updatedPosition)
    {
        AudioSource AudioSource = activeSfxAudioSources.Find(a => a.clip == audioClip);
        if (AudioSource == null) yield break;

        AudioSource.transform.position = updatedPosition;
    }
}
