using UnityEngine;
using System;
using System.Collections.Generic;
using Spine.Unity;

public class Background : MonoBehaviour
{
    protected Map map;

    public virtual void Init(Map map)
    {
        this.map = map;
        this.gameObject.name = "Background";
    }

    public virtual void Advance()
    {
        this.transform.position = new Vector3(
            this.map.gameCamera.transform.position.x,
            this.map.gameCamera.transform.position.y,
            10
        );
    }

    protected float? BestXForCenterColumn()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);

        foreach (int offset in new [] { 0, 1, -1, 2, -2, 3, -3, 4, -4 })
        {
            int index = currentChunk.index + offset;
            if (index < 0) continue;
            if (index >= this.map.chunks.Count) continue;

            var chunk = this.map.chunks[index];
            if (chunk.isHorizontal == true) continue;
            if (chunk.isTransitionalTunnel == true) continue;

            if (cameraRect.xMin > chunk.xMax + 1) continue;
            if (cameraRect.xMax < chunk.xMin) continue;
            if (cameraRect.yMin > chunk.yMax + 1) continue;
            if (cameraRect.yMax < chunk.yMin) continue;

            if (chunk.entry.type == Chunk.MarkerType.Wall)
                return chunk.exit.mapLocation.x;
            else
                return chunk.entry.mapLocation.x;
        }

        return null;
    }

    protected class Copier
    {
        private Map map;
        public Transform copyContainer;
        public GameObject original;
        public Rect objectBounds;
        public List<GameObject> copies;
        public float? xRepeatDistance;
        public float? yRepeatDistance;
        public bool xLimitToCentralColumn;
        public bool synchronizeSpineAnimation;
        public List<Action<GameObject>> onInstantiateCallbacks;

        public Copier(Map map, Transform originalTransform, Rect objectBounds)
        {
            this.map = map;
            var container = new GameObject();
            container.name = $"Copies of {originalTransform.name}";
            container.transform.parent = originalTransform.parent;
            container.transform.localPosition =
                new Vector3(0, 0, originalTransform.localPosition.z);
            container.transform.SetSiblingIndex(originalTransform.GetSiblingIndex());
            this.copyContainer = container.transform;
            this.objectBounds = objectBounds;
            this.original = originalTransform.gameObject;
            this.original.transform.parent = this.copyContainer;
            this.copies = new List<GameObject> { this.original };
            onInstantiateCallbacks = new List<Action<GameObject>>();
        }

        public void SetNumberOfCopies(int amountRequired)
        {
            while (this.copies.Count < amountRequired)
            {
                var copy = Instantiate(this.original, this.copyContainer);
                Util.SetLayerRecursively(copy.transform, this.map.Layer(), true);
                this.copies.Add(copy);

                if (this.onInstantiateCallbacks != null)
                {
                    foreach (var callback in this.onInstantiateCallbacks)
                    {
                        callback(copy);
                    }
                }
            }

            for (int n = 0; n < this.copies.Count; n += 1)
            {
                var active = (n < amountRequired);

                if (this.copies[n].activeSelf != active)
                {
                    this.copies[n].SetActive(active);
                    
                    if (active == true && this.synchronizeSpineAnimation == true)
                    {
                        SynchronizeSpineAnimationInCopy(this.copies[n]);
                    }
                }
            }
        }

        private float[] XPositions(Vector2 position)
        {
            if (this.xRepeatDistance == null) return new [] { position.x };

            float cameraHalfwidth =
                this.xLimitToCentralColumn == true ?
                8 :
                this.map.gameCamera.HalfWidth();

            float xCameraMin = -cameraHalfwidth;
            float xCameraMax = +cameraHalfwidth;

            float stepX = this.xRepeatDistance.Value;
            int indexOfGiven = Mathf.FloorToInt(
                (position.x + this.objectBounds.xMax - xCameraMin) / stepX
            );
            float startX = position.x - (indexOfGiven * stepX);

            var xs = new List<float>();
            for (float x = startX; x < xCameraMax - this.objectBounds.xMin; x += stepX)
                xs.Add(x);
            return xs.ToArray();
        }

        private float[] YPositions(Vector2 position)
        {
            if (this.yRepeatDistance == null) return new [] { position.y };

            float yCameraMin = -this.map.gameCamera.HalfHeight();
            float yCameraMax = +this.map.gameCamera.HalfHeight();

            float stepY = this.yRepeatDistance.Value;
            int indexOfGiven = Mathf.FloorToInt(
                (position.y + this.objectBounds.yMax - yCameraMin) / stepY
            );
            float startY = position.y - (indexOfGiven * stepY);

            var ys = new List<float>();
            for (float y = startY; y < yCameraMax - this.objectBounds.yMin; y += stepY)
                ys.Add(y);
            return ys.ToArray();
        }

        public void Move(float? toX, float? toY)
        {
            var position = new Vector2(
                toX ?? this.original.transform.localPosition.x,
                toY ?? this.original.transform.localPosition.y
            );
            var positions = new List<Vector2>();
            var xs = XPositions(position);
            var ys = YPositions(position);

            foreach (var y in ys)
            {
                foreach (var x in xs)
                {
                    positions.Add(new Vector2(x, y));
                }
            }

            SetNumberOfCopies(positions.Count);
            for (int n = 0; n < positions.Count; n += 1)
            {
                this.copies[n].transform.localPosition = positions[n];
            }
        }

        private void SynchronizeSpineAnimationInCopy(GameObject copy)
        {
            var originalAnimation = 
                this.original.GetComponent<SkeletonAnimation>() ??
                this.original.GetComponentInChildren<SkeletonAnimation>();

            var copyAnimation = 
                copy.GetComponent<SkeletonAnimation>() ??
                copy.GetComponentInChildren<SkeletonAnimation>();

            if (originalAnimation != null && copyAnimation != null)
                copyAnimation.state = originalAnimation.state;
        }
    };
}
