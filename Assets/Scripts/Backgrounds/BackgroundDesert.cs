using System.Collections.Generic;
using UnityEngine;

public class BackgroundDesert : Background
{
    public Transform centerColumn;
    public Transform insideAreas;
    public Material insidePipeMaterial;

    public Transform backSun;
    public Transform backLayer;
    private Copier midLayer;
    private Copier aGroupObservatory;
    private Copier aGroupBridge;
    private Copier aGroupCactiAndPipe;
    private Copier bGroupBomb;
    private Copier bGroupCactusTank;
    private Copier bGroupLeakingPipe;

    private BackgroundInnerSingleRowTiledArt insideBg;
    private BackgroundInnerSingleRowTiledArt insideBottom1Barrels;
    private BackgroundInnerSingleRowTiledArt insideBottom2Pipe;
    private BackgroundInnerSingleRowTiledArt insideBottom3Pipe;
    private BackgroundInnerSingleRowTiledArt insideBottom4Wall;
    private BackgroundInnerSingleRowTiledArt insideMiddlePipes;
    private BackgroundInnerSingleRowTiledArt insideTop1Girders;
    private BackgroundInnerSingleRowTiledArt insideTop2Wall;

    public override void Init(Map map)
    {
        base.Init(map);
        
        this.backSun = this.centerColumn.transform.Find("back sun");
        this.backLayer = this.centerColumn.transform.Find("back layer");
        this.midLayer = new Copier(
            map,
            this.centerColumn.transform.Find("mid layer"),
            new Rect(-8, 0, 16, 48)
        );
        this.aGroupObservatory = new Copier(
            map,
            this.centerColumn.transform.Find("group-A-3-observatory"),
            new Rect(-8, 0, 16, 32)
        );
        this.aGroupBridge = new Copier(
            map,
            this.centerColumn.transform.Find("group-A-5-bridge"),
            new Rect(-8, 0, 16, 45)
        );
        this.aGroupCactiAndPipe = new Copier(
            map,
            this.centerColumn.transform.Find("group-A-6-cacti-and-pipe"),
            new Rect(-8, 0, 16, 42)
        );
        this.bGroupBomb = new Copier(
            map,
            this.centerColumn.transform.Find("group-B-3-bomb"),
            new Rect(-8, 0, 16, 24)
        );
        this.bGroupCactusTank = new Copier(
            map,
            this.centerColumn.transform.Find("group-B-4-cactus-tank-and-bridge"),
            new Rect(-8, 0, 16, 30)
        );
        this.bGroupLeakingPipe = new Copier(
            map,
            this.centerColumn.transform.Find("group-B-5-leaking-pipe"),
            new Rect(-6, 0, 12, 16)
        );

        this.midLayer.yRepeatDistance = 46.72f;
        this.aGroupObservatory.yRepeatDistance = 78;
        this.bGroupBomb.yRepeatDistance = 78;

        this.insideBg = new BackgroundInnerSingleRowTiledArt();
        this.insideBg.Init(
            this.map,
            this.transform.Find("Inside Areas/background"),
            2449 / 100f, 2555 / 100f, 0.5f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null,
            material: this.insidePipeMaterial
        );
        this.insideBottom1Barrels = new BackgroundInnerSingleRowTiledArt();
        this.insideBottom1Barrels.Init(
            this.map,
            this.transform.Find("Inside Areas/bottom layer 1 barrels"),
            1540 / 100f, 983 / 100f, 0.6f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideBottom2Pipe = new BackgroundInnerSingleRowTiledArt();
        this.insideBottom2Pipe.Init(
            this.map,
            this.transform.Find("Inside Areas/bottom layer 2 pipe"),
            2589 / 100f, 767 / 100f, 0.7f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideBottom3Pipe = new BackgroundInnerSingleRowTiledArt();
        this.insideBottom3Pipe.Init(
            this.map,
            this.transform.Find("Inside Areas/bottom layer 3 pipe"),
            2589 / 100f, 466 / 100f, 0.8f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideBottom4Wall = new BackgroundInnerSingleRowTiledArt();
        this.insideBottom4Wall.Init(
            this.map,
            this.transform.Find("Inside Areas/bottom layer 4 wall"),
            2449 / 100f, 539 / 100f, 0.9f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideMiddlePipes = new BackgroundInnerSingleRowTiledArt();
        this.insideMiddlePipes.Init(
            this.map,
            this.transform.Find("Inside Areas/middle pipe"),
            2446 / 100f, 487 / 100f, 0.5f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideTop1Girders = new BackgroundInnerSingleRowTiledArt();
        this.insideTop1Girders.Init(
            this.map,
            this.transform.Find("Inside Areas/top layer 1 girders"),
            2553 / 100f, 951 / 100f, 0.8f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
        this.insideTop2Wall = new BackgroundInnerSingleRowTiledArt();
        this.insideTop2Wall.Init(
            this.map,
            this.transform.Find("Inside Areas/top layer 2 wall"),
            2449 / 100f, 539 / 100f, 0.9f,
            BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksCenterline,
            (art) => {},
            null
        );
    }

    public override void Advance()
    {
        var columnCenterX = BestXForCenterColumn();
        if (columnCenterX.HasValue)
        {
            this.centerColumn.transform.position = new Vector3(
                columnCenterX.Value,
                this.map.gameCamera.transform.position.y,
                10
            );
            this.centerColumn.gameObject.SetActive(true);
        }
        else
        {
            this.centerColumn.gameObject.SetActive(false);
        }
        AdvanceCenterColumn();
        AdvanceInsideAreas();
    }

    private void AdvanceCenterColumn()
    {
        void Move(Copier copier, Vector2 origin, Vector2 speed, float xMinLimit, float xMaxLimit)
        {
            foreach (var c in copier.copies)
            {
                Transform childTransform = c.transform.GetChild(0);

                childTransform.localPosition += (Vector3)speed;

                if(childTransform.localPosition.x < xMinLimit ||
                    childTransform.localPosition.x > xMaxLimit)
                {
                    childTransform.localPosition = origin;
                }
            }
        }

        float cameraY = this.map.gameCamera.transform.position.y;
        this.midLayer.Move(null, cameraY * -0.1f);

        this.aGroupObservatory.Move(null, cameraY * -0.2f);
        this.bGroupBomb       .Move(null, (cameraY * -0.2f) + 44);

        this.aGroupCactiAndPipe.SetNumberOfCopies(this.aGroupObservatory.copies.Count);
        this.aGroupBridge      .SetNumberOfCopies(this.aGroupObservatory.copies.Count);

        for (int n = 0; n < this.aGroupObservatory.copies.Count; n += 1)
        {
            var observatoryPosition =
                this.aGroupObservatory.copies[n].transform.localPosition;

            Transform bridge = this.aGroupBridge.copies[n].transform;
            bridge.localPosition = new Vector2(
                bridge.localPosition.x, (observatoryPosition.y * 1.5f) + 6.5f
            );

            Transform cacti = this.aGroupCactiAndPipe.copies[n].transform;
            cacti.localPosition = new Vector2(
                cacti.localPosition.x, (observatoryPosition.y * 2.0f) - 1.5f
            );
        }

        this.bGroupCactusTank  .SetNumberOfCopies(this.bGroupBomb.copies.Count);
        this.bGroupLeakingPipe .SetNumberOfCopies(this.bGroupBomb.copies.Count);

        for (int n = 0; n < this.bGroupBomb.copies.Count; n += 1)
        {
            var bombPosition =
                this.bGroupBomb.copies[n].transform.localPosition;

            Transform tankBridge = this.bGroupCactusTank.copies[n].transform;
            tankBridge.localPosition = new Vector2(
                tankBridge.localPosition.x, (bombPosition.y * 1.5f) + 13
            );

            Transform pipe = this.bGroupLeakingPipe.copies[n].transform;
            pipe.localPosition = new Vector2(
                pipe.localPosition.x, (bombPosition.y * 2.0f) - 1
            );
        }
        
        //Move(this.bGroupCactusTank, new Vector2(10f, 27.15f), new Vector2(-0.05f, 0.0035f), -30f, 30f);
        Move(this.aGroupBridge, new Vector2(-27.5f, 46f), new Vector2(0.025f, 0.0035f), -27.5f, 6f);
    }

    private void AdvanceInsideAreas()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);
        var chunksToConsider = new List<Chunk>();

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.isHorizontal == false) continue;
            if (cameraRect.Overlaps(chunk.Rectangle()) == false) continue;
            chunksToConsider.Add(chunk);
        }

        this.insideBg.Redraw(chunksToConsider.ToArray());
        this.insideBottom1Barrels.Redraw(chunksToConsider.ToArray());
        this.insideBottom2Pipe.Redraw(chunksToConsider.ToArray());
        this.insideBottom3Pipe.Redraw(chunksToConsider.ToArray());
        this.insideBottom4Wall.Redraw(chunksToConsider.ToArray());
        this.insideMiddlePipes.Redraw(chunksToConsider.ToArray());
        this.insideTop1Girders.Redraw(chunksToConsider.ToArray());
        this.insideTop2Wall.Redraw(chunksToConsider.ToArray());

        float angleDegrees = this.map.frameNumber * -3f;
        foreach (var copy in this.insideMiddlePipes.copies)
        {
            copy.transform.Find("fan").localRotation = Quaternion.Euler(0, 0, angleDegrees);
        }
    }
}
