using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundElementAnimation : MonoBehaviour
{
    public Animated.Animation animationElement;

    private Animated animated;

    public void Init(Map map)
    {
        this.animated = GetComponent<Animated>();
        this.animated.PlayAndLoop(this.animationElement);
        this.animated.Synchronize(map, 0);
    }
}
