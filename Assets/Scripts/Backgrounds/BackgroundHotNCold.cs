using UnityEngine;
using System.Collections.Generic;

public class BackgroundHotNCold : Background
{
    public Animated[] animatedHippos;
    public Animated.Animation[] animationsBlink;

    public Animated animatedSkaterHippo;
    public Animated.Animation animationSkaterHippo;
    public Sprite[] snowflakeParticles;
    
    private class Variant
    {
        public GameObject container;
        public Copier topClouds;
        public Copier middleClouds;
        public Copier bottomClouds;
        public Copier pool1;
        public Copier pool2;
        public Copier pool3;
        public Copier pool4;
    }

    private Variant hot;
    private Variant cold;
    private bool isHotLastFrame;
    private SpriteRenderer white;

    private float skaterAngle;
    private Vector2 skaterTargetPosition;

    public List<BackgroundHotNColdHippoDanceAnim> backgroundHotNColdHippoDanceAnims =
        new List<BackgroundHotNColdHippoDanceAnim>();

    public List<BackgroundHotNColdHippoBlinkAnim> backgroundHotNColdHippoBlinkAnims =
        new List<BackgroundHotNColdHippoBlinkAnim>();

    public override void Init(Map map)
    {
        base.Init(map);

        this.hot = new Variant();
        this.cold = new Variant();
        this.hot.container = this.transform.Find("hot").gameObject;
        this.cold.container = this.transform.Find("cold").gameObject;
        this.white = this.transform.Find("white").GetComponent<SpriteRenderer>();
        this.white.gameObject.SetActive(false);

        void SetupVariant(Variant v)
        {
            var t = v.container.transform;
            v.topClouds    = new Copier(map, t.Find("top clouds"), new Rect(0, 0, 17, 0));
            v.middleClouds = new Copier(map, t.Find("middle clouds"), new Rect(0, 0, 19, 0));
            v.bottomClouds = new Copier(map, t.Find("bottom clouds"), new Rect(0, 0, 22, 0));
            v.pool1        = new Copier(map, t.Find("pool 1"), new Rect(-35, -7, 67, 16));
            v.pool2        = new Copier(map, t.Find("pool 2"), new Rect(-30, 0, 55, 24));
            v.pool3        = new Copier(map, t.Find("pool 3"), new Rect(-5, -6, 55, 18));
            v.pool4        = new Copier(map, t.Find("pool 4"), new Rect(-25, -7, 55, 24));

            v.pool1.onInstantiateCallbacks.Add(CheckAddHippoBlink);
            v.pool2.onInstantiateCallbacks.Add(CheckAddHippoBlink);
            v.pool3.onInstantiateCallbacks.Add(CheckAddHippoBlink);
            v.pool4.onInstantiateCallbacks.Add(CheckAddHippoBlink);
            v.pool4.onInstantiateCallbacks.Add(CheckAddBGHippoDance);

            v.topClouds.xRepeatDistance = 16.26f;
            v.middleClouds.xRepeatDistance = 18.9f;
            v.bottomClouds.xRepeatDistance = 21.67f;
            v.pool1.xRepeatDistance = 54.95f;
            v.pool1.yRepeatDistance = 44f;
            v.pool2.xRepeatDistance = 52.30f;
            v.pool2.yRepeatDistance = 48f;
            v.pool3.xRepeatDistance = 41.9f;
            v.pool3.yRepeatDistance = 48f;
            v.pool4.xRepeatDistance = 43.50f;
            v.pool4.yRepeatDistance = 44f;
        }
        SetupVariant(this.hot);
        SetupVariant(this.cold);
        
        CheckAddHippoBlink(this.cold.container);
        CheckAddBGHippoDance(this.cold.container);
    }

    public override void Advance()
    {
        base.Advance();

        bool isHot = (this.map.currentTemperature == Map.Temperature.Hot);
        this.hot.container.SetActive(isHot == true);
        this.cold.container.SetActive(isHot == false);

        if (isHot != this.isHotLastFrame)
        {
            this.white.gameObject.SetActive(true);
            this.white.color = Color.white;
            this.isHotLastFrame = isHot;
        }
        else if (this.white.gameObject.activeSelf == true)
        {
            this.white.color = new Color(
                1, 1, 1, Util.Slide(this.white.color.a, 0, 0.03f)
            );
            this.white.gameObject.SetActive(this.white.color.a > 0);
        }

        var variant = (isHot == true) ? this.hot : this.cold;

        float topX = this.map.frameNumber * -0.02f;
        float middleX = this.map.frameNumber * -0.04f;
        float bottomX = this.map.frameNumber * -0.06f;
        variant.topClouds.Move(topX, null);
        variant.middleClouds.Move(middleX, null);
        variant.bottomClouds.Move(bottomX, null);

        float cameraX = this.map.gameCamera.transform.position.x;
        float cameraY = this.map.gameCamera.transform.position.y;
        variant.pool1.Move(cameraX * -0.4f, cameraY * -0.4f);
        variant.pool2.Move(cameraX * -0.2f, cameraY * -0.2f);
        variant.pool3.Move(cameraX * -0.2f, (cameraY * -0.2f) + 30);
        variant.pool4.Move(cameraX * -0.4f, (cameraY * -0.4f) + 22);

        SpawnFallingSnowflakes();

        foreach (var anim in this.backgroundHotNColdHippoDanceAnims)
        {
            anim.Advance();
        }
    }
    
    private void SpawnFallingSnowflakes()
    {
        if (this.map.currentTemperature != Map.Temperature.Cold)
            return;

        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);

        if (Random.Range(0, 100) > 80)
        {
            float speedH = Random.Range(0.10f, 0.20f) * -1f;
            float speedV = Random.Range(0.05f, 0.10f) * -1f;

            Sprite snowflakesSprite =
                this.snowflakeParticles[Random.Range(0, this.snowflakeParticles.Length)];
            var p = Particle.CreateWithSprite(
                snowflakesSprite,
                60 * 3,
                Vector2.zero,
                this.transform
            );
            p.gameObject.name = "Falling Background Snowflakes";
            p.transform.position =
                ((Vector2)this.map.gameCamera.transform.position).Add(
                    12f, Random.Range(-10, 30)
                );
            p.velocity = new Vector2(speedH, speedV);
            p.spriteRenderer.sortingLayerName = "Tiles - Mid Layer";
            p.spriteRenderer.sortingOrder = Random.Range(0, 10) <= 7 ? 8 : 30;
            p.transform.SetParent(playerChunk.transform);
        }
    }

    private void CheckAddBGHippoDance(GameObject go)
    {
        BackgroundHotNColdHippoDanceAnim backgroundHotNColdHippoDanceAnim =
            go.GetComponentInChildren<BackgroundHotNColdHippoDanceAnim>();

        if(backgroundHotNColdHippoDanceAnim != null &&
            this.backgroundHotNColdHippoDanceAnims.Contains(backgroundHotNColdHippoDanceAnim) == false)
        {
            this.backgroundHotNColdHippoDanceAnims.Add(backgroundHotNColdHippoDanceAnim);
            backgroundHotNColdHippoDanceAnim.Init();
        }
    }

    private void CheckAddHippoBlink(GameObject go)
    {
        BackgroundHotNColdHippoBlinkAnim[] backgroundHotNColdHippoBlinkAnim =
            go.GetComponentsInChildren<BackgroundHotNColdHippoBlinkAnim>();

        foreach(var b in backgroundHotNColdHippoBlinkAnim)
        {
            if(this.backgroundHotNColdHippoBlinkAnims.Contains(b) == false)
            {
                this.backgroundHotNColdHippoBlinkAnims.Add(b);
                b.Init();
            }

        }

    }
}
