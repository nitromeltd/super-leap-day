using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundHotNColdHippoBlinkAnim : MonoBehaviour
{
    public Animated.Animation animationBlinking;

    private Animated animated;
    
    public void Init()
    {
        this.animated = this.GetComponent<Animated>();
        this.animated.PlayAndLoop(this.animationBlinking);
    }
}
