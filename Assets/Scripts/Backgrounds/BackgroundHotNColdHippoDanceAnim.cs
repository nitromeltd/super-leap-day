using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundHotNColdHippoDanceAnim : MonoBehaviour
{
    public Animated.Animation animationSkaterHippo;

    private Animated animated;
    private float skaterAngle;
    private Vector2 skaterTargetPosition;

    public void Init()
    {
        this.animated = this.GetComponent<Animated>();
        this.animated.PlayAndLoop(this.animationSkaterHippo);
    }
    
    public void Advance()
    {
        if(this.animated == null || this.animationSkaterHippo == null) return;

        // move skater on ellipsoid path
        float a = 1f;
        float speed = 0.025f;
        float e = 0.995f;

        float b = Mathf.Sqrt(Mathf.Pow(a, 2) * (1 - Mathf.Pow(e, 2)));
        this.skaterAngle += speed;
        this.skaterTargetPosition.x = Mathf.Cos(this.skaterAngle) * a;
        this.skaterTargetPosition.y = Mathf.Sin(this.skaterAngle) * b;
        this.animated.transform.localPosition =
            new Vector2(7.9f, 11.95f) + this.skaterTargetPosition;
    }
}
