
using UnityEngine;
using System;
using System.Collections.Generic;

public class BackgroundInnerSingleRowTiledArt
{
    public enum AnchorType
    {
        MultipleChunksLowestEdge,
        ChunkTopEdge,
        ChunkTopEdgeFillWidth,
        ChunkBottomEdge,
        MultipleChunksCenterline,
        MultipleChunksHighestEdge
    }

    public class Copy
    {
        public Transform transform;
        public SpriteRenderer[] spriteRenderers;
    };

    public Map map;
    public Action<Transform> onSetup;
    public Action<Transform> onAdvance;
    public Transform firstInstance;
    public List<Copy> copies;
    public float artWidthTiles;
    public float artHeightTiles;
    public float parallaxFactor;
    private MaterialPropertyBlock propertyBlock;
    private AnchorType anchorType;
    private bool verticalParallax;
    private float horizontalScrollingSpeed;
    private float horizontalScrollingOffset;

    public void Init(
        Map map,
        Transform firstInstance,
        float artWidthTiles,
        float artHeightTiles,
        float parallaxFactor,
        AnchorType anchoredTo,
        Action<Transform> onSetup,
        Action<Transform> onAdvance,
        Material material = null,
        bool verticalParallax = false,
        float horizontalScrollingSpeed = 0
    )
    {
        this.map = map;
        this.firstInstance = firstInstance;
        this.copies = new List<Copy>
        {
            new Copy
            {
                transform = firstInstance,
                spriteRenderers = firstInstance.GetComponentsInChildren<SpriteRenderer>()
            }
        };
        this.artWidthTiles = artWidthTiles;
        this.artHeightTiles = artHeightTiles;
        this.parallaxFactor = parallaxFactor;
        this.anchorType = anchoredTo;
        this.onSetup = onSetup;
        this.onAdvance = onAdvance;
        this.onSetup?.Invoke(this.firstInstance);
        this.propertyBlock = new MaterialPropertyBlock();
        this.verticalParallax = verticalParallax;
        this.horizontalScrollingSpeed = horizontalScrollingSpeed;
        foreach (var sr in this.copies[0].spriteRenderers)
        {
            sr.material = material == null ? Assets.instance.spritesScissorMaterial : material;
        }
    }

    public void Redraw(Chunk[] chunks)
    {
        if (chunks.Length == 0)
        {
            foreach (var copy in this.copies)
                copy.transform.gameObject.SetActive(false);
            return;
        }

        Vector2 cameraPosition = this.map.gameCamera.transform.position;

        int copiesUsed = 0;
        Vector2 parallaxOffsetBase =
            this.map.gameCamera.transform.position * (1 - parallaxFactor);


        foreach (var chunk in chunks)
        {
            Vector2 parallaxOffset = parallaxOffsetBase;
            // chunkBase is the chunk we're aligning everything to, which might
            // be an earlier horizontal chunk to keep them connecting ok
            var chunkBase = chunk;
            var multipleChunksYMin = chunk.yMin;
            var multipleChunksYMax = chunk.yMax;
            for (int n = chunkBase.index - 1; n >= 0; n -= 1)
            {
                if (this.map.chunks[n].isHorizontal == true)
                {
                    if(this.anchorType == AnchorType.MultipleChunksLowestEdge)
                    {
                        if (this.map.chunks[n].yMin < chunkBase.yMin)
                        {
                            chunkBase = this.map.chunks[n];
                        }

                    }
                    else if(this.anchorType == AnchorType.MultipleChunksHighestEdge)
                    {
                        if (this.map.chunks[n].yMax > chunkBase.yMax)
                        {
                            chunkBase = this.map.chunks[n];
                        }
                    }

                    multipleChunksYMin = Mathf.Min(multipleChunksYMin, this.map.chunks[n].yMin);
                    multipleChunksYMax = Mathf.Max(multipleChunksYMax, this.map.chunks[n].yMax);
                }
                else
                    break;
            }

            for (int n = chunkBase.index; n< this.map.chunks.Count; n += 1)
            {
                if(this.map.chunks[n] == chunk)
                    continue;

                if (this.map.chunks[n].isHorizontal == true)
                {
                    if (this.anchorType == AnchorType.MultipleChunksLowestEdge)
                    {
                        if (this.map.chunks[n].yMin < chunkBase.yMin)
                        {
                            chunkBase = this.map.chunks[n];
                        }
                    }
                    else if(this.anchorType == AnchorType.MultipleChunksHighestEdge)
                    {
                        if (this.map.chunks[n].yMax > chunkBase.yMax)
                        {
                            chunkBase = this.map.chunks[n];
                        }
                    }

                    multipleChunksYMin = Mathf.Min(multipleChunksYMin, this.map.chunks[n].yMin);
                    multipleChunksYMax = Mathf.Max(multipleChunksYMax, this.map.chunks[n].yMax);
                }
                else
                    break;
            }

            float firstY = chunkBase.yMin;

            if(this.anchorType == AnchorType.MultipleChunksLowestEdge)
            {
                firstY = chunkBase.yMin;
            }
            else if(this.anchorType == AnchorType.ChunkBottomEdge)
            {
                firstY = chunk.yMin;
            }
            else if(this.anchorType == AnchorType.MultipleChunksCenterline)
            {
                firstY = (multipleChunksYMin + multipleChunksYMax + 1) * 0.5f;
            }
            else if(this.anchorType == AnchorType.MultipleChunksHighestEdge)
            {
                firstY = chunkBase.yMax;
            }
            else if(this.anchorType == AnchorType.ChunkTopEdgeFillWidth)
            {
                firstY = chunk.yMax;
                float chunkWidth = chunk.xMax - chunk.xMin;
                 
                // calculate number of copies, give it some extra space (4 tiles at each end), take walls into account (2)
                int howMany = Mathf.FloorToInt((chunkWidth - 2 - 8) / artWidthTiles);
             
                Vector3 chunkCenter = new Vector3(
                    chunk.xMin + 0.5f * (chunk.xMax - chunk.xMin),
                    chunk.yMin + 0.5f * (chunk.yMax - chunk.yMin));
 
                float maxDelta = (chunkWidth - 2 - (howMany * this.artWidthTiles)) * 0.5f;                
                var distFromCenter = chunkCenter - this.map.gameCamera.transform.position;

                distFromCenter.x = Mathf.Clamp(distFromCenter.x, -chunkWidth * 0.5f, chunkWidth * 0.5f);
                parallaxOffset = -(distFromCenter / (0.5f * chunkWidth)) * (1 - parallaxFactor) * maxDelta;
                
                int j = 0;
                for (
                float tx = -(howMany - 1) / 2f;
                tx <= (howMany - 1) / 2f;
                tx += 1)
                {
                    float y = firstY;
                    {
                        if (copiesUsed >= this.copies.Count)
                        {
                            Transform newTransform = GameObject.Instantiate(
                                this.firstInstance,
                                this.firstInstance.parent
                            );
                            this.onSetup?.Invoke(newTransform);
                            this.copies.Add(new Copy
                            {
                                transform = newTransform,
                                spriteRenderers =
                                    newTransform.GetComponentsInChildren<SpriteRenderer>()
                            });
                        }

                        Copy copy = this.copies[copiesUsed];
                        copy.transform.position = new Vector3(
                            chunkCenter.x + parallaxOffset.x + (tx * this.artWidthTiles), y, this.firstInstance.position.z
                        );
                        copy.transform.gameObject.SetActive(true);
                        
                        foreach (var sr in copy.spriteRenderers)
                        {
                            var min = new Vector2(chunk.xMin, chunk.yMin);
                            var max = new Vector2(chunk.xMax + 1, chunk.yMax + 1);

                            sr.GetPropertyBlock(this.propertyBlock);
                            this.propertyBlock.SetFloat("_MinimumX", min.x);
                            this.propertyBlock.SetFloat("_MaximumX", max.x);
                            this.propertyBlock.SetFloat("_MinimumY", min.y);
                            this.propertyBlock.SetFloat("_MaximumY", max.y);
                            sr.SetPropertyBlock(this.propertyBlock);
                        }

                        this.onAdvance?.Invoke(copy.transform);

                        copiesUsed += 1;
                        j++;
                    }
                }
                continue;
            }

            if(this.verticalParallax == true)
            {
                float baselineCameraY = chunkBase.yMin;
                float offsetFromBaselineY =
                    (cameraPosition.y - this.map.gameCamera.HalfHeight() - baselineCameraY) * (1 - parallaxFactor);
                
                firstY = baselineCameraY + offsetFromBaselineY * 1f;
            }

            parallaxOffset.x += horizontalScrollingOffset;

            for (
                int tx = Mathf.FloorToInt((chunk.xMin - parallaxOffset.x) / this.artWidthTiles);
                tx <= Mathf.CeilToInt((chunk.xMax + 1 - parallaxOffset.x) / this.artWidthTiles);
                tx += 1)
            {
                float y = firstY;
                {
                    if (copiesUsed >= this.copies.Count)
                    {
                        Transform newTransform = GameObject.Instantiate(
                            this.firstInstance,
                            this.firstInstance.parent
                        );
                        this.onSetup?.Invoke(newTransform);
                        this.copies.Add(new Copy
                        {
                            transform = newTransform,
                            spriteRenderers =
                                newTransform.GetComponentsInChildren<SpriteRenderer>()
                        });
                    }

                    Copy copy = this.copies[copiesUsed];
                    copy.transform.position = new Vector3(
                        parallaxOffset.x + (tx * this.artWidthTiles), y, this.firstInstance.position.z
                    );
                    copy.transform.gameObject.SetActive(true);

                    foreach (var sr in copy.spriteRenderers)
                    {
                        var min = new Vector2(chunk.xMin, chunk.yMin);
                        var max = new Vector2(chunk.xMax + 1, chunk.yMax + 1);

                        sr.GetPropertyBlock(this.propertyBlock);
                        this.propertyBlock.SetFloat("_MinimumX", min.x);
                        this.propertyBlock.SetFloat("_MaximumX", max.x);
                        this.propertyBlock.SetFloat("_MinimumY", min.y);
                        this.propertyBlock.SetFloat("_MaximumY", max.y);
                        sr.SetPropertyBlock(this.propertyBlock);
                    }

                    this.onAdvance?.Invoke(copy.transform);

                    copiesUsed += 1;
                }
            }
        }

        for (int n = copiesUsed; n < this.copies.Count; n += 1)
            this.copies[n].transform.gameObject.SetActive(false);

        horizontalScrollingOffset += horizontalScrollingSpeed;
        if(horizontalScrollingOffset > this.artWidthTiles)
        {
            horizontalScrollingOffset -= this.artWidthTiles;
        }

        if(horizontalScrollingOffset < -this.artWidthTiles)
        {
            horizontalScrollingOffset += this.artWidthTiles;
        }
    }
}
