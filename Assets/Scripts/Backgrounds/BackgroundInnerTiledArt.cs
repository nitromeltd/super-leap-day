
using UnityEngine;
using System;
using System.Collections.Generic;

public class BackgroundInnerTiledArt
{
    public class Copy
    {
        public Transform transform;
        public SpriteRenderer[] spriteRenderers;
    };

    public Map map;
    public Action<Transform> onSetup;
    public Action<Transform> onAdvance;
    public Transform firstInstance;
    public List<Copy> copies;
    public float artWidthTiles;
    public float artHeightTiles;
    public float parallaxFactor;
    private MaterialPropertyBlock propertyBlock;

    public void Init(
        Map map,
        Transform firstInstance,
        float artWidthTiles,
        float artHeightTiles,
        float parallaxFactor,
        Action<Transform> onSetup,
        Action<Transform> onAdvance,
        Material material = null
    )
    {
        this.map = map;
        this.firstInstance = firstInstance;
        this.copies = new List<Copy>
        {
            new Copy
            {
                transform = firstInstance,
                spriteRenderers = firstInstance.GetComponentsInChildren<SpriteRenderer>()
            }
        };
        this.artWidthTiles = artWidthTiles;
        this.artHeightTiles = artHeightTiles;
        this.parallaxFactor = parallaxFactor;
        this.onSetup = onSetup;
        this.onAdvance = onAdvance;
        this.onSetup?.Invoke(this.firstInstance);
        this.propertyBlock = new MaterialPropertyBlock();

        foreach (var sr in this.copies[0].spriteRenderers)
        {
            sr.material = material == null ? Assets.instance.spritesScissorMaterial : material;
        }
    }

    public void Redraw(Chunk[] chunks)
    {
        if (chunks.Length == 0)
        {
            foreach (var copy in this.copies)
                copy.transform.gameObject.SetActive(false);
            return;
        }

        Vector2 cameraPosition = this.map.gameCamera.transform.position;

        int copiesUsed = 0;
        Vector2 offset =
            this.map.gameCamera.transform.position * (1 - parallaxFactor);

        foreach (var chunk in chunks)
        {
            // chunkBase is the chunk we're aligning everything to, which might
            // be an earlier horizontal chunk to keep them connecting ok
            var chunkBase = chunk;
            for (int n = chunkBase.index - 1; n >= 0; n -= 1)
            {
                if (this.map.chunks[n].isHorizontal == true)
                    chunkBase = this.map.chunks[n];
                else
                    break;
            }

            float baselineCameraY = Mathf.Min(
                (chunkBase.yMin + chunkBase.yMax + 1) * 0.5f,
                (chunkBase.yMin + this.map.gameCamera.HalfHeight())
            );
            float offsetFromBaselineY =
                (cameraPosition.y - baselineCameraY) * (1 - parallaxFactor);
            offsetFromBaselineY %= this.artHeightTiles;
            if (offsetFromBaselineY > 0)
                offsetFromBaselineY -= this.artHeightTiles;

            float firstY = Mathf.Floor(
                (chunk.yMin - offsetFromBaselineY) / this.artHeightTiles)
            * this.artHeightTiles + offsetFromBaselineY;

            for (
                int tx = Mathf.FloorToInt((chunk.xMin - offset.x) / this.artWidthTiles);
                tx <= Mathf.CeilToInt((chunk.xMax + 1 - offset.x) / this.artWidthTiles);
                tx += 1)
            {
                for (float y = firstY; y <= chunk.yMax + 1; y += this.artHeightTiles)
                {
                    if (copiesUsed >= this.copies.Count)
                    {
                        Transform newTransform = GameObject.Instantiate(
                            this.firstInstance,
                            this.firstInstance.parent
                        );
                        this.onSetup?.Invoke(newTransform);
                        this.copies.Add(new Copy
                        {
                            transform = newTransform,
                            spriteRenderers =
                                newTransform.GetComponentsInChildren<SpriteRenderer>()
                        });
                    }

                    Copy copy = this.copies[copiesUsed];
                    copy.transform.position = new Vector3(
                        offset.x + (tx * this.artWidthTiles), y, this.firstInstance.position.z
                    );
                    copy.transform.gameObject.SetActive(true);

                    foreach (var sr in copy.spriteRenderers)
                    {
                        var min = new Vector2(chunk.xMin, chunk.yMin);
                        var max = new Vector2(chunk.xMax + 1, chunk.yMax + 1);

                        sr.GetPropertyBlock(this.propertyBlock);
                        this.propertyBlock.SetFloat("_MinimumX", min.x);
                        this.propertyBlock.SetFloat("_MaximumX", max.x);
                        this.propertyBlock.SetFloat("_MinimumY", min.y);
                        this.propertyBlock.SetFloat("_MaximumY", max.y);
                        sr.SetPropertyBlock(this.propertyBlock);
                    }

                    this.onAdvance?.Invoke(copy.transform);

                    copiesUsed += 1;
                }
            }
        }

        for (int n = copiesUsed; n < this.copies.Count; n += 1)
            this.copies[n].transform.gameObject.SetActive(false);
    }
}
