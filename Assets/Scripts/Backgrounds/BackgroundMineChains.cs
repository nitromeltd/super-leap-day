using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMineChains: MonoBehaviour
{
    public Transform[] chains;
    public bool isGoingDown;
    void Update()
    {
        for (int i = 0; i < chains.Length; i++)
        {
            if (isGoingDown == true)
            {
                chains[i].transform.localPosition = new Vector3(chains[i].transform.localPosition.x, BackgroundTreasureMines.chain1Positions[i]);
            }
            else
            {
                chains[i].transform.localPosition = new Vector3(chains[i].transform.localPosition.x, BackgroundTreasureMines.chain2Positions[i]);
            }
        }
    }
}

