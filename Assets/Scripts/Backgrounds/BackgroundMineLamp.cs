using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundMineLamp : MonoBehaviour
{
    public int maxAngle;
    public bool invertedRotation;

    public GameObject glow;
    public float speed;

    void Update()
    {
        this.transform.rotation = Quaternion.Euler(0, 0, this.maxAngle * Mathf.Sin(speed * BackgroundTreasureMines.framesProgress / 60f * (this.invertedRotation ? -1 : 1)));

        float scx = 1 - 0.03f * Mathf.Cos(4 * (Time.timeSinceLevelLoad));
        float scy = 1 - 0.08f * Mathf.Sin(4 * (Time.timeSinceLevelLoad));
        this.glow.transform.localScale = new Vector3(scx, scy, 1);
    }
}

