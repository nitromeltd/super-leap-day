using UnityEngine;

public class BackgroundMoltenFortress : Background
{
    private Copier backTexture;
    private Copier farWall;
    private Copier lavafalls;
    private Copier pillars;
    public SpriteRenderer glowBehind;
    public SpriteRenderer glowInFront;

    public override void Init(Map map)
    {
        base.Init(map);

        this.backTexture = new Copier(
            map,
            this.transform.Find("back texture"),
            new Rect((429 * -0.5f / 70f), 0, (429 / 70f), (429 / 70f))
        );
        this.farWall = new Copier(
            map,
            this.transform.Find("far wall"),
            new Rect((1133 * -0.5f / 70f), 0, (1133 / 70f), (2205 / 70f))
        );
        this.lavafalls = new Copier(
            map,
            this.transform.Find("lavafalls"),
            new Rect((1738 * -0.5f / 70f), 0, (1738 / 70f), (963 / 70f))
        );
        this.pillars = new Copier(
            map,
            this.transform.Find("pillars"),
            new Rect((1312 * -0.5f / 70f), 0, (1312 / 70f), (2204 / 70f))
        );

        this.backTexture.xRepeatDistance = 429 / 70f;
        this.backTexture.yRepeatDistance = 429 / 70f;
        this.farWall.xRepeatDistance = 1133 / 70f;
        this.farWall.yRepeatDistance = 2205 / 70f;
        this.lavafalls.xRepeatDistance = 1738 / 70f;
        this.lavafalls.yRepeatDistance = 963 / 70f;
        this.pillars.xRepeatDistance = 1312 / 70f;
        this.pillars.yRepeatDistance = 2204 / 70f;
    }

    public override void Advance()
    {
        base.Advance();

        Vector2 pos = this.map.gameCamera.transform.position;
        var lavaOffset = (this.map.frameNumber * -0.2f);

        this.backTexture.Move((pos.x - 8) * -0.1f, pos.y * -0.1f);
        this.farWall    .Move((pos.x - 8) * -0.2f, pos.y * -0.2f);
        this.lavafalls  .Move((pos.x - 8) * -0.3f, (pos.y * -0.3f) + lavaOffset);
        this.pillars    .Move((pos.x - 8) * -0.4f, pos.y * -0.4f);

        var rect = this.map.gameCamera.VisibleRectangle();
        var glowPosition = new Vector2(
            rect.xMin, this.map.lavaline.currentLevel
        );

        this.glowBehind.transform.position = glowPosition.Add(0, -1);
        this.glowInFront.transform.position = glowPosition.Add(0, -2);
        this.glowBehind.transform.localScale = new Vector2(rect.width / 22f, 1);
        this.glowInFront.transform.localScale = new Vector2(rect.width / 22f, 1);

        {
            var glowAlpha = this.glowInFront.color.a;
            if (this.map.lavaline.IsSolid == true)
                glowAlpha = Util.Slide(glowAlpha, 0, 0.05f);
            else
                glowAlpha = Util.Slide(glowAlpha, 1, 0.02f);

            this.glowBehind.color = new Color(1, 1, 1, glowAlpha);
            this.glowInFront.color = new Color(1, 1, 1, glowAlpha);
        }
    }
}
