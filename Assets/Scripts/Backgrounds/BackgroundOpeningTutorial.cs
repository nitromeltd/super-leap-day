using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class BackgroundOpeningTutorial : Background
{
    public List<Transform> back;
    private Transform shadow;
    // public List<Transform> middle1;
    // public List<Transform> middle2;
    // public List<Transform> front1;
    // public List<Transform> front2;

    public override void Init(Map map)
    {
        base.Init(map);

        this.back = new List<Transform> { this.transform.Find("back") };
        this.shadow = this.transform.Find("shadow");
        // this.middle1 = new List<Transform> { this.transform.Find("middle 1") };
        // this.middle2 = new List<Transform> { this.transform.Find("middle 2") };
        // this.front1 = new List<Transform> { this.transform.Find("front 1") };
        // this.front2 = new List<Transform> { this.transform.Find("front 2") };

        CheckForBackgroundElementAnimation(this.gameObject);
    }

    public override void Advance()
    {
        base.Advance();

        var camera = this.map.gameCamera.Camera;
        float cameraHalfheight = camera.orthographicSize;
        float cameraHalfwidth = camera.aspect * camera.orthographicSize;

        void CloneToFill(List<Transform> list, int amountRequired)
        {
            while (list.Count < amountRequired)
            {
                var copy = Instantiate(list[0].gameObject, list[0].parent);
                list.Add(copy.transform);

                CheckForBackgroundElementAnimation(copy);
            }
            while (list.Count > amountRequired)
            {
                var last = list[list.Count - 1];
                list.RemoveAt(list.Count - 1);
                Destroy(last.gameObject);
            }
        }

        void Parallax(
            List<Transform> list, Vector2 target, float repeatX, float repeatY
        )
        {
            var xs = new List<float>();
            float x = target.x % repeatX;
            while (x <= -cameraHalfwidth - repeatX) x += repeatX;
            while (x > -cameraHalfwidth) x -= repeatX;
            while (x < cameraHalfwidth) { xs.Add(x); x += repeatX; }

            var ys = new List<float>();
            float y = target.y % repeatY;
            while (y > -cameraHalfheight) y -= repeatY;
            while (y < cameraHalfheight) { ys.Add(y); y += repeatY; }

            int count = xs.Count * ys.Count;
            if (count < 1)
            {
                CloneToFill(list, count);
                list[0].gameObject.SetActive(false);
                return;
            }

            CloneToFill(list, count);

            for (int yn = 0; yn < ys.Count; yn += 1)
            {
                for (int xn = 0; xn < xs.Count; xn += 1)
                {
                    var n = xn + (yn * xs.Count);
                    var copy = list[n];
                    copy.gameObject.SetActive(true);
                    copy.transform.localPosition = new Vector2(xs[xn], ys[yn]);
                }
            }
        }

        Vector2 cam = this.map.gameCamera.transform.position;
        Parallax(this.back, (cam * -0.55f).Add(-3.9f, 0), 16, 35.9f);
        if(GameCamera.IsLandscape() == true)
        {
            this.shadow.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1.26f, 20));

        }
        else
        {
            this.shadow.position = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1, 20));
        }
        // Parallax(this.middle1, cam * -0.5f, 34.67f * 0.777f, 56 * 0.777f);
        // Parallax(this.middle2, (cam * -0.5f).Add(0, 28), 34.72f * 0.777f, 56 * 0.777f);
        // Parallax(this.front1, cam * -0.7f, 59.53f * 0.777f, 56 * 0.777f);
        // Parallax(this.front2, (cam * -0.7f).Add(0, 28), 34.45f * 0.777f, 56 * 0.777f);
    }

    private void CheckForBackgroundElementAnimation(GameObject go)
    {
        BackgroundElementAnimation[] backgroundElementAnimations
            = GetComponentsInChildren<BackgroundElementAnimation>();

        foreach (var e in backgroundElementAnimations)
        {
            e.Init(this.map);
        }
    }
}
