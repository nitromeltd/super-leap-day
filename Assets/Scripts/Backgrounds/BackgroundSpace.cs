using UnityEngine;
using System.Collections.Generic;
using Spine.Unity;
public class BackgroundSpace : Background
{ 
    private Copier back;
    private Copier fishShip;
    private Copier spaceStation;
    private Copier smallPlanet;
    private Copier satellite;
    private Copier cubePlanet;
    private Copier cubePlanetBig;
    private Copier bar;
    private Copier minerAsteroids;     

    public override void Init(Map map)
    {
        base.Init(map);
        this.back           = new Copier(map, this.transform.Find("back"), new Rect(0, 0, 60, 60));
        this.fishShip       = new Copier(map, this.transform.Find("fish ship"), new Rect(0, 0, 10, 10));
        this.spaceStation   = new Copier(map, this.transform.Find("space station"), new Rect(0, 0, 10, 10));
        this.smallPlanet    = new Copier(map, this.transform.Find("small planet"), new Rect(0, 0, 10, 10));
        this.satellite      = new Copier(map, this.transform.Find("satellite"), new Rect(0, 0, 10, 10));
        this.cubePlanet     = new Copier(map, this.transform.Find("cube planet"), new Rect(0, 0, 10, 10));
        this.cubePlanetBig  = new Copier(map, this.transform.Find("cube planet (atmos)"), new Rect(0, 0, 16, 16));
        this.bar            = new Copier(map, this.transform.Find("bar"), new Rect(0, 0, 12, 18));
        this.minerAsteroids = new Copier(map, this.transform.Find("miner and asteroids"), new Rect(0, 0, 20, 20));
        this.back          .xRepeatDistance = 58.91f;
        this.back          .yRepeatDistance = 57.54f;
        this.fishShip      .yRepeatDistance = 90;
        this.spaceStation  .xRepeatDistance = 25;
        this.smallPlanet   .xRepeatDistance = 25;
        this.satellite     .xRepeatDistance = 25;
        this.cubePlanet    .xRepeatDistance = 25;
        this.cubePlanetBig .xRepeatDistance = 25;
        this.bar           .xRepeatDistance = 25;
        this.minerAsteroids.xRepeatDistance = 25;
        this.spaceStation  .yRepeatDistance = 90;
        this.smallPlanet   .yRepeatDistance = 90;
        this.satellite     .yRepeatDistance = 90;
        this.cubePlanet    .yRepeatDistance = 90;
        this.cubePlanetBig .yRepeatDistance = 90;
        this.bar           .yRepeatDistance = 90;
        this.minerAsteroids.yRepeatDistance = 90;
        foreach (var barCopy in this.bar.copies)
            barCopy.GetComponentInChildren<BackgroundSpaceBarAnims>().Init();
        
        this.bar.onInstantiateCallbacks.Add((copy) => {
            copy.GetComponentInChildren<BackgroundSpaceBarAnims>().Init();
        });

        void SetupSynchronize(Copier copier)
        {
            var originalAnimation = 
                copier.original.GetComponent<SkeletonAnimation>() ??
                copier.original.GetComponentInChildren<SkeletonAnimation>();

            foreach (var copy in copier.copies)
            {
                var copyAnimation = 
                    copy.GetComponent<SkeletonAnimation>() ??
                    copy.GetComponentInChildren<SkeletonAnimation>();

                if(copyAnimation != null && originalAnimation != null)
                    copyAnimation.state = originalAnimation.state;
            }

            copier.onInstantiateCallbacks.Add((copy) =>
            {
                var copyAnimation = 
                    copy.GetComponent<SkeletonAnimation>() ??
                    copy.GetComponentInChildren<SkeletonAnimation>();
                    
                if(copyAnimation != null && originalAnimation != null)
                    copyAnimation.state = originalAnimation.state;
            });
        }
        SetupSynchronize(this.spaceStation);
        SetupSynchronize(this.smallPlanet);
        SetupSynchronize(this.cubePlanet);
        SetupSynchronize(this.cubePlanetBig);
        SetupSynchronize(this.bar);
        SetupSynchronize(this.satellite);
        SetupSynchronize(this.minerAsteroids);
    }

    public override void Advance()
    {
        base.Advance();
        void Move(Copier copier, Vector2 origin, Vector2 speed, Vector2 limits, Vector2 size, Vector2 offset)
        {
            foreach (var l in copier.copies)
            {
                Transform childTransform = l.transform.GetChild(0);
                childTransform.localPosition += (Vector3)speed;
                if (childTransform.localPosition.x > limits.x || childTransform.localPosition.y > limits.y)
                {
                    Vector2 offsetPosition = (Vector2)childTransform.position + offset;
                    Vector2 originWorldPosition = (Vector2)l.transform.position + origin;
                    Rect cameraRect = this.map.gameCamera.VisibleRectangle();
                    Rect originRect = new Rect(originWorldPosition.x, originWorldPosition.y, size.x, size.y);
                    Rect currentRect = new Rect(childTransform.position.x, childTransform.position.y, size.x, size.y);
                    bool isOriginVisible = cameraRect.Overlaps(originRect) || cameraRect.Contains(origin);
                    bool isCurrentlyVisible = cameraRect.Overlaps(currentRect) || cameraRect.Contains(offsetPosition);
                    if (isOriginVisible == false && isCurrentlyVisible == false)
                    {
                        childTransform.localPosition = origin;
                    }
                }
            }
        }
        Vector2 cam = this.map.gameCamera.transform.position;
        this.back          .Move((cam.x * -0.1f) + 30, (cam.y * -0.1f));
        this.fishShip      .Move(null,                 (cam.y * -0.5f) + 45);
        this.spaceStation  .Move((cam.x * -0.5f) - 3,  (cam.y * -0.5f) +  1);
        this.smallPlanet   .Move((cam.x * -0.3f) - 6,  (cam.y * -0.3f) + 12);
        this.satellite     .Move((cam.x * -0.3f) + 5,  (cam.y * -0.3f) + 40);
        this.cubePlanet    .Move((cam.x * -0.3f) - 2,  (cam.y * -0.3f) + 58);
        this.cubePlanetBig .Move((cam.x * -0.5f) + 2,  (cam.y * -0.5f) + 55);
        this.bar           .Move((cam.x * -0.5f) + 0,  (cam.y * -0.5f) + 70);
        this.minerAsteroids.Move((cam.x * -0.5f) - 6,  (cam.y * -0.5f) + 20);

        Move(this.fishShip, new Vector2(-23.5f, -5f), new Vector2(0.025f, 0.015f),
            new Vector2(20, 12), new Vector2(7f, 7f), new Vector2(0.50f, -4.5f));
        foreach (var bar in this.bar.copies)
        {
            bar.GetComponentInChildren<BackgroundSpaceBarAnims>()?.Advance();
        }
    }
}
