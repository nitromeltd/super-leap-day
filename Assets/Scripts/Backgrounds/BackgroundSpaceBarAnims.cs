using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSpaceBarAnims : MonoBehaviour
{
    public Animated animatedBarSign;
    public Animated animatedAlien1;
    public Animated animatedAlien2;

    public Animated.Animation animationBarSign;
    public Animated.Animation animationIdleAlien1;
    public Animated.Animation animationIdleAlien2;
    public Animated.Animation animationTalkAlien1;
    public Animated.Animation animationTalkAlien2;

    private int aliensTalkTimer = 0;

    private const int AliensTalkTime = 60 * 3;
    
    public void Init()
    {
        this.animatedBarSign.PlayAndLoop(this.animationBarSign);
        this.animatedAlien1.PlayAndLoop(this.animationIdleAlien1);
        this.animatedAlien2.PlayAndLoop(this.animationIdleAlien2);
    }
    
    public void Advance()
    {
        this.aliensTalkTimer += 1;

        if(this.aliensTalkTimer >= AliensTalkTime)
        {
            this.aliensTalkTimer = 0;
            PlayAliensTalk();
        }
    }
    
    private void PlayAliensTalk()
    {
        this.animatedAlien2.PlayOnce(this.animationTalkAlien2, 
            () => { this.animatedAlien2.PlayAndLoop(this.animationIdleAlien2); }
        );

        this.animatedAlien2.OnFrame(18, 
            () => {
                this.animatedAlien1.PlayOnce(this.animationTalkAlien1, 
                    () => { this.animatedAlien1.PlayAndLoop(this.animationIdleAlien1); }
                );
            }
        );
    }
}
