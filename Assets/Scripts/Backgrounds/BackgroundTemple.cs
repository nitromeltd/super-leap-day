
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class BackgroundTemple : Background
{
    public Animated.Animation animationSmallFlame;
    public Animated.Animation animationMediumFlame;
    public Animated.Animation animationBigFlame;
    public Animated.Animation animationRaindropSplash;
    public Animated.Animation animationRaindropSideSplash;
    public Animated.Animation animationLanternFlame;
    public Sprite[] rainParticles;

    private Transform centerColumn;
    private BackgroundTempleAnim instance1;
    private BackgroundTempleAnim instance2;
    private BackgroundTempleAnim instance3;
    private Transform back1;
    private Transform back2;
    private Transform back3;
    private Transform pillars1;
    private Transform pillars2;
    private Transform pillars3;
    private Transform smallPlantLeft;
    private Transform mediumPlantLeft;
    private Transform largePlantLeft;
    private Transform smallPlantRight;
    private Transform mediumPlantRight;
    private Transform largePlantRight;

    private Transform insideAreas;
    private BackgroundInnerTiledArt insideAreasBackArt;
    private BackgroundInnerTiledArt insideAreasFrontArt;

    private int timeToRestartAnimation;

    public override void Init(Map map)
    {
        base.Init(map);

        void Animate(
            Transform parent,
            string childName,
            Animated.Animation animation,
            bool outOfSync
        )
        {
            var childTransform = parent.Find(childName);
            var animated = childTransform.gameObject.AddComponent<Animated>();
            animated.PlayAndLoop(animation);
            if (outOfSync) animated.frameNumber = animation.sprites.Length / 2;
        }

        this.centerColumn = this.transform.Find("Center Column");

        this.instance1 =
            this.centerColumn.transform.Find("Animation Instance").GetComponent<BackgroundTempleAnim>();
        this.instance2 =
            Instantiate(instance1.gameObject, this.centerColumn.transform).GetComponent<BackgroundTempleAnim>();
        this.instance3 =
            Instantiate(instance1.gameObject, this.centerColumn.transform).GetComponent<BackgroundTempleAnim>();

        this.back1   = instance1.transform.Find("back");
        this.back2   = instance2.transform.Find("back");
        this.back3   = instance3.transform.Find("back");
        this.pillars1 = instance1.transform.Find("pillars");
        this.pillars2 = instance2.transform.Find("pillars");
        this.pillars3 = instance3.transform.Find("pillars");
        this.pillars2.transform.localScale = new Vector3(-1, 1, 1);

        this.smallPlantLeft = this.centerColumn.transform.Find("small plant left");
        this.mediumPlantLeft = this.centerColumn.transform.Find("medium plant left");
        this.largePlantLeft = this.centerColumn.transform.Find("large plant left");
        this.smallPlantRight = this.centerColumn.transform.Find("small plant right");
        this.mediumPlantRight = this.centerColumn.transform.Find("medium plant right");
        this.largePlantRight = this.centerColumn.transform.Find("large plant right");

        Animate(this.back1, "small flame 1", this.animationSmallFlame, false);
        Animate(this.back1, "small flame 2", this.animationSmallFlame, true);
        Animate(this.back1, "medium flame 1", this.animationMediumFlame, false);
        Animate(this.back1, "medium flame 2", this.animationMediumFlame, true);
        Animate(this.back1, "big flame", this.animationBigFlame, false);
        Animate(this.back2, "small flame 1", this.animationSmallFlame, false);
        Animate(this.back2, "small flame 2", this.animationSmallFlame, true);
        Animate(this.back2, "medium flame 1", this.animationMediumFlame, false);
        Animate(this.back2, "medium flame 2", this.animationMediumFlame, true);
        Animate(this.back2, "big flame", this.animationBigFlame, false);
        Animate(this.back3, "small flame 1", this.animationSmallFlame, false);
        Animate(this.back3, "small flame 2", this.animationSmallFlame, true);
        Animate(this.back3, "medium flame 1", this.animationMediumFlame, false);
        Animate(this.back3, "medium flame 2", this.animationMediumFlame, true);
        Animate(this.back3, "big flame", this.animationBigFlame, false);

        this.insideAreas = this.transform.Find("Inside Areas");
        this.insideAreasBackArt = new BackgroundInnerTiledArt();
        this.insideAreasFrontArt = new BackgroundInnerTiledArt();
        this.insideAreasBackArt.Init(
            this.map, this.insideAreas.transform.Find("Back"), 20, 20, 0.5f, null, null
        );
        this.insideAreasFrontArt.Init(
            this.map, this.insideAreas.transform.Find("Front"), 33, 33, 0.7f,
            (art) =>
            {
                var flame1 = art.Find("bowl flame 1").GetComponent<Animated>();
                flame1.PlayAndLoop(this.animationMediumFlame);
                flame1.Synchronize(this.map, 0);

                var flame2 = art.Find("bowl flame 2").GetComponent<Animated>();
                flame2.PlayAndLoop(this.animationMediumFlame);
                flame2.Synchronize(this.map, 4);

                var flame3 = art.Find("bowl flame 3").GetComponent<Animated>();
                flame3.PlayAndLoop(this.animationMediumFlame);
                flame3.Synchronize(this.map, 8);

                var flameL1 = art.Find("lantern 1/flame").GetComponent<Animated>();
                flameL1.PlayAndLoop(this.animationLanternFlame);
                flameL1.Synchronize(this.map, 0);

                var flameL2 = art.Find("lantern 2/flame").GetComponent<Animated>();
                flameL2.PlayAndLoop(this.animationLanternFlame);
                flameL2.Synchronize(this.map, 6);
            },
            (art) => {
                var flame1 = art.Find("lantern 1");
                float angleDegrees = Mathf.Sin(this.map.frameNumber * 0.03f) * 3;
                flame1.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);

                var flame2 = art.Find("lantern 2");
                angleDegrees = Mathf.Sin(this.map.frameNumber * 0.035f) * 2.5f;
                flame2.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);

                var flame3 = art.Find("lantern 3");
                angleDegrees = Mathf.Sin(this.map.frameNumber * 0.033f) * 2.75f;
                flame3.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);
            }
        );

        Audio.instance.PlaySfxLoop(Assets.instance.sfxLoopRainyRuinsRain);

        SetTimeToRestart();
    }

    private void SetTimeToRestart()
    {
        this.timeToRestartAnimation = UnityEngine.Random.Range(300, 600);
    }

    public override void Advance()
    {
        var columnCenterX = BestXForCenterColumn();
        if (columnCenterX.HasValue)
        {
            this.centerColumn.transform.localPosition = new Vector3(
                columnCenterX.Value,
                this.map.gameCamera.transform.position.y,
                0
            );
            this.centerColumn.gameObject.SetActive(true);
        }
        else
        {
            this.centerColumn.gameObject.SetActive(false);
        }

        AdvanceCenterColumn();
        AdvanceInsideAreas();
        SpawnFallingRain();
        SpawnGroundRain();
    }

    private void AdvanceCenterColumn()
    {
        if (this.instance1.shouldPlayLightningSfx == true)
        {
            this.instance1.shouldPlayLightningSfx = false;
            Audio.instance.PlaySfx(Assets.instance.sfxTempleLightning);
        }

        {
            this.timeToRestartAnimation -= 1;
            if (this.timeToRestartAnimation < 1)
            {
                SetTimeToRestart();

                this.instance1.GetComponent<Animator>().Play("Flash", -1, 0);
                this.instance2.GetComponent<Animator>().Play("Flash", -1, 0);
                this.instance3.GetComponent<Animator>().Play("Flash", -1, 0);
            }
        }

        void Parallax(Transform thing, float minY, float maxY, float factor, float offset)
        {
            Vector2 pos = thing.localPosition;
            pos.y = this.map.gameCamera.transform.position.y * factor;
            pos.y += offset;
            pos.y -= minY;
            pos.y %= (maxY - minY);
            if (pos.y < 0) pos.y += maxY - minY;
            pos.y += minY;
            thing.localPosition = pos;
        }

        float B = 927.5f / 16;
        float P = 1000f / 16;
        Parallax(this.back1,     -B*2,    0,  -0.2f,  200/16f);
        Parallax(this.back2,       -B,    B,  -0.2f,  200/16f + B);
        Parallax(this.back3,        0,  B*2,  -0.2f,  200/16f);
        Parallax(this.pillars1,  -P*2,    0,  -0.4f,  350/16f);
        Parallax(this.pillars2,    -P,    P,  -0.4f,  350/16f + P);
        Parallax(this.pillars3,     0,  P*2,  -0.4f,  350/16f);

        Parallax(this.smallPlantLeft,   -375 / 16f, 375 / 16f, -0.4f, 0);
        Parallax(this.mediumPlantLeft,  -375 / 16f, 375 / 16f, -0.4f, 250);
        Parallax(this.largePlantLeft,   -375 / 16f, 375 / 16f, -0.4f, 500);
        Parallax(this.smallPlantRight,  -375 / 16f, 375 / 16f, -0.4f, 500);
        Parallax(this.mediumPlantRight, -375 / 16f, 375 / 16f, -0.4f, 0);
        Parallax(this.largePlantRight,  -375 / 16f, 375 / 16f, -0.4f, 250);

        {
            var anim = this.instance1.GetComponent<BackgroundTempleAnim>();
            var block = new MaterialPropertyBlock();
            block.SetColor("_FillColor", anim.plantFillColour);
            block.SetFloat("_FillPhase", anim.plantFillAmount);

            foreach (var plant in new [] {
                this.smallPlantLeft, this.mediumPlantLeft, this.largePlantLeft,
                this.smallPlantRight, this.mediumPlantRight, this.largePlantRight
            })
            {
                var mr = plant.Find("anim").GetComponent<MeshRenderer>();
                mr.SetPropertyBlock(block);
            }
        }
    }

    private void AdvanceInsideAreas()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);
        var chunksToConsider = new List<Chunk>();

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.isHorizontal == false) continue;
            if (cameraRect.Overlaps(chunk.Rectangle()) == false) continue;
            chunksToConsider.Add(chunk);
        }

        this.insideAreasBackArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasFrontArt.Redraw(chunksToConsider.ToArray());
    }

    private void SpawnFallingRain()
    {
        if (this.map.NearestChunkTo(this.map.player.position).isHorizontal)
            return;

        if (Random.Range(0, 100) > 70)
        {
            float s = Random.Range(0.0f, 1.0f);
            s *= s;
            float speed = Mathf.Lerp(0.75f, 1.5f, s);
            var p = Particle.CreateWithSprite(
                this.rainParticles[0],
                30,
                Vector2.zero,
                this.transform
            );
            p.gameObject.name = "Falling Background Rain";
            p.transform.localPosition =
                ((Vector2)this.map.gameCamera.transform.position).Add(
                    9.5f, Random.Range(-10, 30)
                );
            p.velocity = new Vector2(-speed, -speed);
            p.spriteRenderer.sortingLayerName = "Background";
            p.spriteRenderer.sortingOrder = Random.Range(0, 10) <= 7 ? 8 : 30;
        }
    }

    private void SpawnGroundRain()
    {
        var cameraPos = this.map.gameCamera.transform.position;
        var yh = 16 / this.map.gameCamera.Camera.aspect;
        var minTx = Mathf.FloorToInt((cameraPos.x - 16) / Tile.Size);
        var maxTx = Mathf.FloorToInt((cameraPos.x + 16) / Tile.Size);
        var minTy = Mathf.FloorToInt((cameraPos.y - yh) / Tile.Size);
        var maxTy = Mathf.FloorToInt((cameraPos.y + yh) / Tile.Size);

        for (var n = 0; n < 3; n += 1)
        {
            var tx = Random.Range(minTx, maxTx);
            var ty = Random.Range(minTy, maxTy);
            var chunk = this.map.ChunkAt(tx, ty);
            if (chunk == null || chunk.isHorizontal == true || ty == chunk.yMax)
                continue;

            var tile = chunk.TileAt(tx, ty, Layer.A);
            var tileAbove = chunk.TileAt(tx, ty + 1, Layer.A);
            if (tile != null &&
                (
                    tile.spec.shape == Tile.Shape.Square ||
                    tile.spec.shape == Tile.Shape.TopLine
                ) &&
                tileAbove == null)
            {
                var pos = new Vector2((tx + 0.5f) * Tile.Size, (ty + 1) * Tile.Size);
                var p = Particle.CreateAndPlayOnce(
                    this.animationRaindropSplash, pos, this.map.transform
                );
                p.gameObject.name = "Raindrop Particle";
                p.spriteRenderer.sortingLayerName = "Tiles - Solid";
                p.transform.parent = this.map.transform;
            }
        }

        for (var n = 0; n < 3; n += 1)
        {
            var tx = Random.Range(minTx, maxTx);
            var ty = Random.Range(minTy, maxTy);
            var chunk = this.map.ChunkAt(tx, ty);
            if (chunk == null || chunk.isHorizontal == true)
                continue;

            var tile = chunk.TileAt(tx, ty, Layer.A);
            var tileOnRight = chunk.TileAt(tx + 1, ty, Layer.A);
            var chunkOnRight = this.map.ChunkAt(tx + 1, ty);
            if (tile != null &&
                tile.spec.shape == Tile.Shape.Square &&
                tileOnRight == null &&
                chunkOnRight != null)
            {
                var pos = new Vector2((tx + 1) * Tile.Size, (ty + 0.5f) * Tile.Size);
                var p = Particle.CreateAndPlayOnce(
                    this.animationRaindropSideSplash, pos, this.map.transform
                );
                p.gameObject.name = "Raindrop Particle";
                p.spriteRenderer.sortingLayerName = "Tiles - Solid";
                p.transform.parent = this.map.transform;
            }
        }
    }
}
