
using UnityEngine;

public class BackgroundTempleAnim : MonoBehaviour
{
    public Color plantFillColour;
    public float plantFillAmount;
    public bool shouldPlayLightningSfx = false;

    public void PlayLightningSfx()
    {
        this.shouldPlayLightningSfx = true;
    }
}
