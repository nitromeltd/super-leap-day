using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class BackgroundTombstoneHill : Background
{
    private Transform insideAreas;
    private Transform centerColumn;

    private BackgroundInnerSingleRowTiledArt insideAreasSkyArt;
    private BackgroundInnerSingleRowTiledArt insideAreasFarGravesArt;
    private BackgroundInnerSingleRowTiledArt insideAreasFog1Art;
    private BackgroundInnerSingleRowTiledArt insideAreasFog2Art;
    private BackgroundInnerSingleRowTiledArt insideAreasFog3Art;
    private BackgroundInnerSingleRowTiledArt insideAreasTreesArt;
    private BackgroundInnerSingleRowTiledArt insideAreasTreesBArt;
    private BackgroundInnerSingleRowTiledArt insideAreasGraves;
    private BackgroundInnerSingleRowTiledArt insideAreasNewGraves;
    private BackgroundInnerSingleRowTiledArt insideAreasNewGravesB;
    private BackgroundInnerSingleRowTiledArt insideAreasFog4Art;
    private BackgroundWaterChandeliers insideAreasChandeliers;
    public Transform horizontalSky;

    private Copier back;
    private Copier arches;
    private Copier fog1;
    private Copier fog2;
    private Copier fog3;
    private Copier cobwebs1;
    private Copier cobwebs2;
    private Copier cobwebs3;

    private MaterialPropertyBlock propertyBlock;

    public override void Init(Map map)
    {
        base.Init(map);
        this.centerColumn = this.transform.Find("Center Column");

        this.back                 = new Copier(map, this.centerColumn.Find("Back"),       new Rect(-8,   -0.5f * 5967f/138,  16, 5967f/138));
        this.arches               = new Copier(map, this.centerColumn.Find("Arches"),     new Rect(-8,   -0.5f * 8630f/138,  16, 8630f/138));
        this.fog1                 = new Copier(map, this.centerColumn.Find("Fog1"),       new Rect(-0.5f * 2492f/138, -0.5f * 8630f/138,  2492f/138, 8630f/138));
        this.fog2                 = new Copier(map, this.centerColumn.Find("Fog2"),       new Rect(-0.5f * 2492f/138, -0.5f * 8630f/138,  2492f/138, 8630f/138));
        this.fog3                 = new Copier(map, this.centerColumn.Find("Fog3"),       new Rect(-0.5f * 2492f/138, -0.5f * 8630f/138,  2492f/138, 8630f/138));
        this.cobwebs1             = new Copier(map, this.centerColumn.Find("Cobwebs1"),   new Rect(-8,   -0.5f * 8630f/138,  16, 8630f/138));
        this.cobwebs2             = new Copier(map, this.centerColumn.Find("Cobwebs2"),   new Rect(-8,   -0.5f * 8630f/138,  16, 8630f/138));
        this.cobwebs3             = new Copier(map, this.centerColumn.Find("Cobwebs3"),   new Rect(-8,   -0.5f * 8630f/138,  16, 8630f/138));

        this.back                .yRepeatDistance = 5967f/138;
        this.arches              .yRepeatDistance = 8630f/138;
        this.fog1                .yRepeatDistance = 8630f/138;
        this.fog2                .yRepeatDistance = 8630f/138;
        this.fog3                .yRepeatDistance = 8630f/138;
        this.cobwebs1            .yRepeatDistance = 8630f/138;
        this.cobwebs2            .yRepeatDistance = 8630f/138;
        this.cobwebs3            .yRepeatDistance = 8630f/138;

        this.fog1                .xRepeatDistance = 2492f/138;
        this.fog2                .xRepeatDistance = 2492f/138;
        this.fog3                .xRepeatDistance = 2492f/138;

        this.back             .synchronizeSpineAnimation = false;
        this.arches           .synchronizeSpineAnimation = false;
        this.fog1             .synchronizeSpineAnimation = false;
        this.fog2             .synchronizeSpineAnimation = false;
        this.fog3             .synchronizeSpineAnimation = false;
        this.cobwebs1         .synchronizeSpineAnimation = false;
        this.cobwebs2         .synchronizeSpineAnimation = false;
        this.cobwebs3         .synchronizeSpineAnimation = false;

        HorizontalInit();

        this.propertyBlock = new MaterialPropertyBlock();
    }

    public override void Advance()
    {
        var columnCenterX = BestXForCenterColumn();
        if (columnCenterX.HasValue)
        {
            this.centerColumn.transform.localPosition = new Vector3(
                columnCenterX.Value,
                this.map.gameCamera.transform.position.y,
                10
            );
            this.centerColumn.gameObject.SetActive(true);
        }
        else
        {
            this.centerColumn.gameObject.SetActive(false);
        }

        AdvanceCenterColumn();
        AdvanceInsideAreas();
    }

    private void AdvanceCenterColumn()
    {
        Vector2 cam = this.map.gameCamera.transform.position;
        float toX = -Time.fixedTime * 0.5f;
        this.back             .Move( 0    , (cam.y * -0.25f) + 0);
        this.fog1             .Move( toX  , (cam.y * -0.35f) + 0);
        this.fog2             .Move( toX  , (cam.y * -0.35f) + 20);
        this.fog3             .Move( toX  , (cam.y * -0.35f) + 40);
        this.cobwebs1         .Move( 0    , (cam.y * -0.55f) + 0);
        this.cobwebs2         .Move( 0    , (cam.y * -0.55f) + 20);
        this.cobwebs3         .Move( 0    , (cam.y * -0.55f) + 40);
        this.arches           .Move( 0    , (cam.y * -0.7f) + 0);
    }

    private const float pixelsPerUnit = 110;
    private void HorizontalInit()
    {
        this.insideAreas = this.transform.Find("Inside Areas");

        this.insideAreasFarGravesArt = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasFarGravesArt.Init(
            this.map, this.insideAreas.transform.Find("FarGraves"), 3825f/pixelsPerUnit, 2354f/pixelsPerUnit, 0.1f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasFog1Art = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasFog1Art.Init(
            this.map, this.insideAreas.transform.Find("Fog1"), 3832f/pixelsPerUnit, 1083f/pixelsPerUnit, 0.2f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true, 0.0025f
        );

        this.insideAreasFog2Art = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasFog2Art.Init(
            this.map, this.insideAreas.transform.Find("Fog2"), 3823f/pixelsPerUnit, 1248f/pixelsPerUnit, 0.3f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true, 0.005f
        );

        this.insideAreasFog3Art = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasFog3Art.Init(
            this.map, this.insideAreas.transform.Find("Fog3"), 3832f/pixelsPerUnit, 684f/pixelsPerUnit, 0.4f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true, 0.0075f
        );

        this.insideAreasGraves = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasGraves.Init(
            this.map, this.insideAreas.transform.Find("Graves"), 3550f/pixelsPerUnit, 637f/pixelsPerUnit, 0.5f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasNewGravesB = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasNewGravesB.Init(
            this.map, this.insideAreas.transform.Find("NewGravesB"), 2774f/pixelsPerUnit, 979f/pixelsPerUnit, 0.54f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasNewGraves = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasNewGraves.Init(
            this.map, this.insideAreas.transform.Find("NewGraves"), 2774f/pixelsPerUnit, 979f/pixelsPerUnit, 0.56f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasTreesBArt = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasTreesBArt.Init(
            this.map, this.insideAreas.transform.Find("TreesB"), 3753f/pixelsPerUnit, 1657f/pixelsPerUnit, 0.58f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null,null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasTreesArt = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasTreesArt.Init(
            this.map, this.insideAreas.transform.Find("Trees"), 3753f/pixelsPerUnit, 1657f/pixelsPerUnit, 0.6f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null,null, Assets.instance.spritesWaterScissorMaterial, true
        );

        this.insideAreasFog4Art = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasFog4Art.Init(
            this.map, this.insideAreas.transform.Find("Fog4"), 3832f/pixelsPerUnit, 477f/pixelsPerUnit, 0.7f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial, true, 0.01f
        );
    }

    private void AdvanceInsideAreas()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);
        var chunksToConsider = new List<Chunk>();

        this.horizontalSky.position = new Vector3(cameraPosition.x, cameraRect.yMax,0);

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.isHorizontal == false) continue;
            if (cameraRect.Overlaps(chunk.Rectangle()) == false) continue;
            chunksToConsider.Add(chunk);
        }

        this.insideAreasFarGravesArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasFog1Art.Redraw(chunksToConsider.ToArray());
        this.insideAreasFog2Art.Redraw(chunksToConsider.ToArray());
        this.insideAreasFog3Art.Redraw(chunksToConsider.ToArray());
        this.insideAreasTreesBArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasTreesArt.Redraw(chunksToConsider.ToArray());        
        this.insideAreasGraves.Redraw(chunksToConsider.ToArray());
        this.insideAreasNewGraves.Redraw(chunksToConsider.ToArray());
        this.insideAreasNewGravesB.Redraw(chunksToConsider.ToArray());
        this.insideAreasFog4Art.Redraw(chunksToConsider.ToArray());
    }
}
