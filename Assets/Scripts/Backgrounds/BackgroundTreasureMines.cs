using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class BackgroundTreasureMines : Background
{
    private Transform insideAreas;
    private Transform centerColumn;

    private BackgroundInnerTiledArt insideAreasBackArt;
    private BackgroundInnerSingleRowTiledArt insideAreasLayer1;
    private BackgroundInnerSingleRowTiledArt insideAreasLayer2;
    private BackgroundInnerSingleRowTiledArt insideAreasLayer3;
    private BackgroundInnerSingleRowTiledArt insideAreasLayer4;
    private BackgroundInnerSingleRowTiledArt insideAreasLayer5;
    private BackgroundInnerTiledArt insideAreasFrontArt;

    private Copier centerColumnBack;
    private Copier centerColumnFront;
    private Copier centerColumnChains;

    public static float[] chain1Positions = new float[12];
    public static float[] chain2Positions = new float[12];

    private float chainTimer = 0;

    public static int framesProgress = 0;
    public override void Init(Map map)
    {
        base.Init(map);

        this.centerColumn = this.transform.Find("Center Column");

        Transform[] chains = this.centerColumn.Find("Chain parent").Find("Chain group 1").GetComponent<BackgroundMineChains>().chains;

        for (int i = 0; i<chains.Length; i++)
        {
            chain1Positions[i] = chains[i].localPosition.y;
        }

        chains = this.centerColumn.Find("Chain parent").Find("Chain group 2").GetComponent<BackgroundMineChains>().chains;

        for (int i = 0; i < chains.Length; i++)
        {
            chain2Positions[i] = chains[i].localPosition.y;
        }


        this.centerColumnBack  = new Copier(map, this.centerColumn.Find("Back"),  new Rect(0,-10, 10, 30));
        this.centerColumnFront = new Copier(map, this.centerColumn.Find("Front"), new Rect(0, -10, 10, 60));
        this.centerColumnChains = new Copier(map, this.centerColumn.Find("Chain parent"), new Rect(0f, -30, 10, 60));

        this.centerColumnBack.yRepeatDistance = 16.8f;
        this.centerColumnFront.yRepeatDistance = 29.096f;
        this.centerColumnChains.yRepeatDistance = 30.60125f;

        HorizontalInit();
    }

    public override void Advance()
    {
        var columnCenterX = BestXForCenterColumn();
        if (columnCenterX.HasValue)
        {
            this.centerColumn.transform.localPosition = new Vector3(
                columnCenterX.Value,
                this.map.gameCamera.transform.position.y,
                10
            );
            this.centerColumn.gameObject.SetActive(true);
        }
        else
        {
            this.centerColumn.gameObject.SetActive(false);
        }
        AdvanceCenterColumn();

        AdvanceInsideAreas();
        framesProgress++;
    }

    private void AdvanceCenterColumn()
    {
        Vector2 cam = this.map.gameCamera.transform.position;

        this.centerColumnBack.Move(0, (cam.y * -0.4f));
        this.centerColumnChains.Move(0, (cam.y * -0.5f));
        this.centerColumnFront.Move(0, (cam.y * -0.6f));

        chainTimer += Time.deltaTime;

        if (chainTimer > 4)
        {
            if (chainTimer < 4.13f)
            {
                if (chainTimer < 4.1f)
                {
                    for (int i = 0; i < chain1Positions.Length; i++)
                    {
                        chain1Positions[i] = chain1Positions[i] + 1 / 60f * 0.9f * 2;
                        chain2Positions[i] = chain2Positions[i] - 1 / 60f * 0.9f * 2;
                    }
                }
                else
                {
                    for (int i = 0; i < chain1Positions.Length; i++)
                    {
                        chain1Positions[i] = chain1Positions[i] - 1 / 60f * 0.9f * 1.5f;
                        chain2Positions[i] = chain2Positions[i] + 1 / 60f * 0.9f * 1.5f;
                    }
                }
            }
            if (chainTimer > 6)
            {
                chainTimer = 0;
            }
        }
        else
        {
            for (int i = 0; i < chain1Positions.Length; i++)
            {
                chain1Positions[i] = chain1Positions[i] - 1 / 60f * 0.9f;
                if (chain1Positions[i] <= -22.05f - 2.55f)
                {
                    chain1Positions[i] = chain1Positions[i] + 2.55f * (chain1Positions.Length);
                }

                chain2Positions[i] = chain2Positions[i] + 1 / 60f * 0.9f;
                if (chain2Positions[i] >= 8.55f + 2.55f)
                {
                    chain2Positions[i] = chain2Positions[i] - 2.55f * (chain2Positions.Length);
                }
            }
        }
    }

    private void HorizontalInit()
    {
        this.insideAreas = this.transform.Find("Inside Areas");

        this.insideAreasBackArt = new BackgroundInnerTiledArt();
        this.insideAreasBackArt.Init(
            this.map, this.insideAreas.transform.Find("Back"), 1730f / 100, 1740f / 100, 0.5f, null, null, Assets.instance.spritesScissorMaterial
        );

        this.insideAreasFrontArt = new BackgroundInnerTiledArt();
        this.insideAreasFrontArt.Init(
            this.map, this.insideAreas.transform.Find("Front"), 4763f / 100, 1890f / 100, 0.8f, null, null, Assets.instance.spritesScissorMaterial
        );

        this.insideAreasLayer1 = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasLayer1.Init(
            this.map, this.insideAreas.transform.Find("1"), 0.7f * 2445f / 100, 0.7f * 1891 / 100, 0.8f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesScissorMaterial, verticalParallax: true
        );
        this.insideAreasLayer2 = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasLayer2.Init(
            this.map, this.insideAreas.transform.Find("2"), 0.7f * 2445f / 100, 0.7f * 1891 / 100, 0.72f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesScissorMaterial, verticalParallax: true
        );
        this.insideAreasLayer3 = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasLayer3.Init(
            this.map, this.insideAreas.transform.Find("3"), 0.7f * 2445f / 100, 0.7f * 1891 / 100, 0.64f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesScissorMaterial, verticalParallax: true
        );

        this.insideAreasLayer4 = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasLayer4.Init(
            this.map, this.insideAreas.transform.Find("4"), 0.7f * 2445f / 100, 0.7f * 1891 / 100, 0.58f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesScissorMaterial, verticalParallax: true
        );

        this.insideAreasLayer5 = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasLayer5.Init(
            this.map, this.insideAreas.transform.Find("5"), 0.7f * 2445f / 100, 0.7f * 1891 / 100, 0.54f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesScissorMaterial, verticalParallax: true
        );
    }

    private void AdvanceInsideAreas()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);
        var chunksToConsider = new List<Chunk>();

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.isHorizontal == false) continue;
            if (cameraRect.Overlaps(chunk.Rectangle()) == false) continue;
            chunksToConsider.Add(chunk);
        }

        this.insideAreasBackArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasFrontArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasLayer1.Redraw(chunksToConsider.ToArray());
        this.insideAreasLayer2.Redraw(chunksToConsider.ToArray());
        this.insideAreasLayer3.Redraw(chunksToConsider.ToArray());
        this.insideAreasLayer4.Redraw(chunksToConsider.ToArray());
        this.insideAreasLayer5.Redraw(chunksToConsider.ToArray());
    }
}

