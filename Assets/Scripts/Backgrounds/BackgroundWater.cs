using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class BackgroundWater : Background
{
    private Transform insideAreas;
    private Transform centerColumn;

    private BackgroundInnerTiledArt insideAreasBackArt;
    private BackgroundInnerTiledArt insideAreasFrontArt;
    private BackgroundInnerSingleRowTiledArt insideAreasBarrels;
    private BackgroundWaterChandeliers insideAreasChandeliers;

    private Transform sky;
    private Transform bigCloud;
    private Transform pillars;
    private Transform water;
    private Transform waterUnderneath;
    private SpriteRenderer[] nonCopiedSpriteRenderers;
    //private Transform smallCloud1;
    //private Transform smallCloud2;
    //private Transform smallCloud3;

    private Copier mountain1;
    private Copier mountain2;
    private Copier mountain3;
    private Copier mountainSkull;
    private Copier ship;
    private Copier bridge;
    private Copier palmTree1;
    private Copier palmTree2;

    private Copier sideFoliageRight1;
    private Copier sideFoliageRight2;
    private Copier sideFoliageRight3;
    private Copier sideFoliageLeft1;
    private Copier sideFoliageLeft2;
    private Copier sideFoliageLeft3;

    private MaterialPropertyBlock propertyBlock;

    public override void Init(Map map)
    {
        base.Init(map);

        this.centerColumn = this.transform.Find("Center Column");

        this.sky = this.centerColumn.Find("Sky");
        this.bigCloud = this.centerColumn.Find("Big Cloud");
        this.pillars = this.centerColumn.Find("Pillars");
        this.water = this.centerColumn.Find("Water");
        this.waterUnderneath = this.centerColumn.Find("Water Underneath");

        var list = new List<SpriteRenderer>();
        list.Add(this.sky.GetComponent<SpriteRenderer>());
        list.Add(this.bigCloud.GetComponent<SpriteRenderer>());
        list.Add(this.pillars.GetComponent<SpriteRenderer>());
        list.AddRange(this.water.GetComponentsInChildren<SpriteRenderer>(true));
        list.AddRange(this.waterUnderneath.GetComponentsInChildren<SpriteRenderer>(true));
        list.Add(this.centerColumn.Find("Small Cloud 1").GetComponent<SpriteRenderer>());
        list.Add(this.centerColumn.Find("Small Cloud 2").GetComponent<SpriteRenderer>());
        list.Add(this.centerColumn.Find("Small Cloud 3").GetComponent<SpriteRenderer>());
        this.nonCopiedSpriteRenderers = list.ToArray();

        this.ship              = new Copier(map, this.centerColumn.Find("Ship"),                 new Rect(-8,   4,  8, 24));
        this.mountain1         = new Copier(map, this.centerColumn.Find("Mountain 1"),           new Rect(-8,  -8, 16, 13));
        this.mountain2         = new Copier(map, this.centerColumn.Find("Mountain 2"),           new Rect(-8,  -7, 16, 14));
        this.mountain3         = new Copier(map, this.centerColumn.Find("Mountain 3"),           new Rect(-8,  -7, 16, 14));
        this.mountainSkull     = new Copier(map, this.centerColumn.Find("Mountain Skull"),       new Rect(-8, -12, 16, 27));
        this.bridge            = new Copier(map, this.centerColumn.Find("Bridge"),               new Rect(-8,  15, 16, 20));
        this.palmTree1         = new Copier(map, this.centerColumn.Find("Palm Tree 1"),          new Rect(-8, -10, 16, 20));
        this.palmTree2         = new Copier(map, this.centerColumn.Find("Palm Tree 2"),          new Rect(-8,  -8, 16, 16));
        this.sideFoliageRight1 = new Copier(map, this.centerColumn.Find("Side Foliage 1 Right"), new Rect(-4, -10,  8, 10));
        this.sideFoliageRight2 = new Copier(map, this.centerColumn.Find("Side Foliage 2 Right"), new Rect(-4,  -8,  8, 10));
        this.sideFoliageRight3 = new Copier(map, this.centerColumn.Find("Side Foliage 3 Right"), new Rect(-4,  -5,  8,  5));
        this.sideFoliageLeft1  = new Copier(map, this.centerColumn.Find("Side Foliage 1 Left"),  new Rect(-4, -10,  8, 10));
        this.sideFoliageLeft2  = new Copier(map, this.centerColumn.Find("Side Foliage 2 Left"),  new Rect(-4,  -8,  8, 10));
        this.sideFoliageLeft3  = new Copier(map, this.centerColumn.Find("Side Foliage 3 Left"),  new Rect(-4,  -5,  8,  5));

        this.ship             .yRepeatDistance = 100;
        this.mountainSkull    .yRepeatDistance = 60f;
        this.mountain1        .yRepeatDistance = 60f;
        this.mountain2        .yRepeatDistance = 60f;
        this.mountain3        .yRepeatDistance = 60f;
        this.bridge           .yRepeatDistance = 100;
        this.palmTree1        .yRepeatDistance = 100;
        this.palmTree2        .yRepeatDistance = 100;
        this.sideFoliageRight1.yRepeatDistance = 30;
        this.sideFoliageRight2.yRepeatDistance = 30;
        this.sideFoliageRight3.yRepeatDistance = 30;
        this.sideFoliageLeft1 .yRepeatDistance = 30;
        this.sideFoliageLeft2 .yRepeatDistance = 30;
        this.sideFoliageLeft3 .yRepeatDistance = 30;

        this.ship             .synchronizeSpineAnimation = true;
        this.bridge           .synchronizeSpineAnimation = true;
        this.palmTree1        .synchronizeSpineAnimation = true;
        this.palmTree2        .synchronizeSpineAnimation = true;
        this.sideFoliageLeft1 .synchronizeSpineAnimation = true;
        this.sideFoliageLeft2 .synchronizeSpineAnimation = true;
        this.sideFoliageLeft3 .synchronizeSpineAnimation = true;
        this.sideFoliageRight1.synchronizeSpineAnimation = true;
        this.sideFoliageRight2.synchronizeSpineAnimation = true;
        this.sideFoliageRight3.synchronizeSpineAnimation = true;

        HorizontalInit();

        this.propertyBlock = new MaterialPropertyBlock();
    }

    public override void Advance()
    {
        var columnCenterX = BestXForCenterColumn();
        if (columnCenterX.HasValue)
        {
            this.centerColumn.transform.localPosition = new Vector3(
                columnCenterX.Value,
                this.map.gameCamera.transform.position.y,
                10
            );
            this.centerColumn.gameObject.SetActive(true);
        }
        else
        {
            this.centerColumn.gameObject.SetActive(false);
        }

        AdvanceCenterColumn();

        if (GameCamera.IsLandscape() == true)
        {
            var naturalHorizon = this.water.parent.position.y - 4.9f;
            var delta = (this.map.waterline.currentLevel - naturalHorizon) * 0.8f;
            if (delta < 0) delta = 0;

            this.sky.transform.localPosition = new Vector3(0, 0);
            this.bigCloud.transform.localPosition = new Vector3(0, 0);
            this.pillars.transform.localPosition = new Vector3(0, 0);
            this.water.transform.localPosition = new Vector3(0, -4.9f + delta);
            this.waterUnderneath.transform.localPosition = new Vector3(0, -4.9f + delta);
        }
        else
        {
            var naturalHorizon = this.water.parent.position.y - 10.6f;
            var delta = (this.map.waterline.currentLevel - naturalHorizon) * 0.8f;
            if (delta < 0) delta = 0;

            this.sky.transform.localPosition = new Vector3(0, 0);
            this.bigCloud.transform.localPosition = new Vector3(0, -5);
            this.pillars.transform.localPosition = new Vector3(0, -5);
            this.water.transform.localPosition = new Vector3(0, -10.6f + delta);
            this.waterUnderneath.transform.localPosition = new Vector3(0, -10.6f + delta);
        }

        {
            var cameraRectangle = this.map.gameCamera.VisibleRectangle();
            var waterlineLevel = Mathf.Clamp(
                this.map.waterline.currentLevel,
                cameraRectangle.yMin - 2,
                cameraRectangle.yMax + 2
            );

            var dy = this.water.position.y - (waterlineLevel - 1);
            var dz = 10;
            var distance = Mathf.Sqrt((dy * dy) + (dz * dz));
            this.water.localRotation = Quaternion.Euler(
                Mathf.Atan2(dz, dy) * 180 / Mathf.PI, 0, 0
            );
            this.water.localScale = new Vector3(
                this.water.localScale.x, distance / 3.76f, 1
            );
            this.water.gameObject.SetActive(dy > 0);

            dy = (waterlineLevel + 1) - this.water.position.y;
            distance = Mathf.Sqrt((dy * dy) + (dz * dz));
            this.waterUnderneath.localRotation = Quaternion.Euler(
                -Mathf.Atan2(dz, dy) * 180 / Mathf.PI, 0, 0
            );
            this.waterUnderneath.localScale = new Vector3(
                this.water.localScale.x, distance / 3.76f, 1
            );
            this.waterUnderneath.gameObject.SetActive(dy > 0);
        }

        AdvanceInsideAreas();
        AdvanceHeightmap();
    }

    private void AdvanceCenterColumn()
    {
        Vector2 cam = this.map.gameCamera.transform.position;

        this.mountain1        .Move( -5.5f, (cam.y * -0.2f) + 25.3f + 10);
        this.mountain2        .Move( -4.7f, (cam.y * -0.2f) + 10);
        this.mountain3        .Move( 5.15f, (cam.y * -0.2f) + 40 + 10);
        this.mountainSkull    .Move(  0.4f, (cam.y * -0.2f) + 14.48f + 10);
        this.bridge           .Move(-0.02f, (cam.y * -0.3f) + 55 + 10);
        this.ship             .Move(    0f, (cam.y * -0.3f) + 25 + 10);
        this.palmTree1        .Move(-1.49f, (cam.y * -0.3f) + 5 + 10);
        this.palmTree2        .Move(  5.4f, (cam.y * -0.3f) + 10 + 10);
        this.sideFoliageRight1.Move( 6.26f, (cam.y * -0.6f) + 54 + 10);
        this.sideFoliageRight2.Move( 5.57f, (cam.y * -0.6f) + 45 + 10);
        this.sideFoliageRight3.Move( 5.69f, (cam.y * -0.6f) + 33 + 10);
        this.sideFoliageLeft1 .Move(-4.24f, (cam.y * -0.6f) + 49 + 10);
        this.sideFoliageLeft2 .Move(-3.65f, (cam.y * -0.6f) + 40 + 10);
        this.sideFoliageLeft3 .Move(-3.79f, (cam.y * -0.6f) + 33 + 10);
    }

    private void HorizontalInit()
    {
        this.insideAreas = this.transform.Find("Inside Areas");

        this.insideAreasBackArt = new BackgroundInnerTiledArt();
        this.insideAreasBackArt.Init(
            this.map, this.insideAreas.transform.Find("Back"), 2007f/70, 1403f/70, 0.5f, null, null, Assets.instance.spritesWaterScissorMaterial
        );

        this.insideAreasFrontArt = new BackgroundInnerTiledArt();
        this.insideAreasFrontArt.Init(
            this.map, this.insideAreas.transform.Find("Front"), 2004f/70, 1523f/70, 0.7f, null, null, Assets.instance.spritesWaterScissorMaterial
        );

        this.insideAreasBarrels = new BackgroundInnerSingleRowTiledArt();
        this.insideAreasBarrels.Init(
            this.map, this.insideAreas.transform.Find("Barrels"), 2123f/70, 419f/70, 0.8f, BackgroundInnerSingleRowTiledArt.AnchorType.MultipleChunksLowestEdge, null, null, Assets.instance.spritesWaterScissorMaterial
        );

        this.insideAreasChandeliers = new BackgroundWaterChandeliers();
        this.insideAreasChandeliers.Init(
            this.map, this.insideAreas.transform.Find("Chandeliers"), 24, 500f/70, 0.5f,  null, null, Assets.instance.spritesWaterScissorMaterial
        );
    }

    private void AdvanceInsideAreas()
    {
        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        Chunk currentChunk = this.map.NearestChunkTo(cameraPosition);
        var chunksToConsider = new List<Chunk>();

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.isHorizontal == false) continue;
            if (cameraRect.Overlaps(chunk.Rectangle()) == false) continue;
            chunksToConsider.Add(chunk);
        }

        this.insideAreasBackArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasFrontArt.Redraw(chunksToConsider.ToArray());
        this.insideAreasBarrels.Redraw(chunksToConsider.ToArray());
        this.insideAreasChandeliers.Redraw(chunksToConsider.ToArray());
    }

    private void AdvanceHeightmap()
    {
        var yForNormal = this.map.waterline.currentLevel;
        var yForBlue = this.map.waterline.currentLevel - 1.5f;

        var waterline = this.map.waterline;
        var cameraRectangle = this.map.gameCamera.VisibleRectangle();
        var xMin = waterline.heightmap.xMin;
        var xMax = waterline.heightmap.xMax;

        var heightmap = new List<float>(256);
        for (int n = 0; n < 256; n += 1)
        {
            float x = Mathf.Lerp(xMin, xMax, n / 255.0f);
            float displacement = waterline.heightmap.DisplacementAtX(x);
            heightmap.Add(waterline.currentLevel + displacement);
        }

        SetWaterHeight(mountainSkull.copies, heightmap, xMin, xMax);
        SetWaterHeight(mountain1.copies, heightmap, xMin, xMax);
        SetWaterHeight(mountain2.copies, heightmap, xMin, xMax);
        SetWaterHeight(mountain3.copies, heightmap, xMin, xMax);
        SetWaterHeight(ship.copies, heightmap, xMin, xMax);
        SetWaterHeight(bridge.copies, heightmap, xMin, xMax);
        SetWaterHeight(palmTree1.copies, heightmap, xMin, xMax);
        SetWaterHeight(palmTree2.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageLeft1.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageLeft2.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageLeft3.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageRight1.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageRight2.copies, heightmap, xMin, xMax);
        SetWaterHeight(sideFoliageRight3.copies, heightmap, xMin, xMax);

        foreach(var copy in this.insideAreasBackArt.copies) SetWaterHeight(copy.spriteRenderers, heightmap, xMin, xMax);
        foreach(var copy in this.insideAreasFrontArt.copies) SetWaterHeight(copy.spriteRenderers, heightmap, xMin, xMax);
        foreach(var copy in this.insideAreasBarrels.copies) SetWaterHeight(copy.spriteRenderers, heightmap, xMin, xMax);
        foreach(var kv in this.insideAreasChandeliers.chunkCopies)
        {
            foreach(var copy in kv.Value)
            {
                SetWaterHeight(copy.spriteRenderers, heightmap, xMin, xMax);
            }
        }

        SetWaterHeight(this.nonCopiedSpriteRenderers, heightmap, xMin, xMax);
    }

    public void SetWaterHeight(
        List<GameObject> list, List<float> heightmap, float xMin, float xMax
    )
    {
        foreach (GameObject child in list)
        {
            if (child.transform.Find("anim") != null)
            {
                var childMR = child.transform.Find("anim").GetComponent<MeshRenderer>();
                childMR.GetPropertyBlock(this.propertyBlock);
                this.propertyBlock.SetFloatArray("_Heightmap", heightmap);
                this.propertyBlock.SetFloat("_HeightmapXMin", xMin);
                this.propertyBlock.SetFloat("_HeightmapXMax", xMax);
                childMR.SetPropertyBlock(this.propertyBlock);
            }
            else if (child.GetComponent<SpriteRenderer>() != null)
            {
                var childSR = child.GetComponent<SpriteRenderer>();
                childSR.material.SetFloatArray("_Heightmap", heightmap);
                childSR.material.SetFloat("_HeightmapXMin", xMin);
                childSR.material.SetFloat("_HeightmapXMax", xMax);
            }
        }        
    }

    public void SetWaterHeight(
        SpriteRenderer[] list, List<float> heightmap, float xMin, float xMax
    )
    {
        foreach (var childSR in list)
        {
            childSR.material.SetFloatArray("_Heightmap", heightmap);
            childSR.material.SetFloat("_HeightmapXMin", xMin);
            childSR.material.SetFloat("_HeightmapXMax", xMax);
        }        
    }
}
