using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundWaterChandelier : MonoBehaviour
{
    public Map map;
    public float maxAngleRotation;
    public float hangingHeightAsPercentageOfChunkHeightMeasuredFromTop;
    public Sprite chandelierLitSprite;
    public Sprite chandelierUnlitSprite;
    public SpriteRenderer chandelierSpriteRenderer;
    public Transform objectUsedToDetermineIfChandelierIsLit;
    public Vector2 chandelierInitialOffset;
    private SpriteRenderer chainRenderer;
    private Transform chandelierTransform;
    private Chunk chunk;
    private float timeProgress;
    private bool invertedRotation;

    public GameObject[] candles;
    public GameObject[] glows;
    public int[] reigniteCounters;
    private float[] candlesProgress;

    public void Init(Map map)
    {
        this.map = map;
        this.invertedRotation = Random.Range(0, 1f) > 0.5f;
        InitChandelier();
    }
 
    public void InitChandelier()
    {
        this.chunk = this.map.NearestChunkTo(this.transform.position);
        this.chandelierTransform = this.transform.Find("Chandelier").transform;        

        this.chainRenderer = GetComponent<SpriteRenderer>();        

        this.chandelierTransform.localPosition = (Vector3)chandelierInitialOffset + new Vector3(0, -(this.chunk.yMax - this.chunk.yMin) * hangingHeightAsPercentageOfChunkHeightMeasuredFromTop, 0);

        this.chainRenderer.size = new Vector2(0.7f, chunk.yMax - this.chandelierTransform.position.y);
        this.candlesProgress = new float[this.candles.Length];

        for (int i = 0; i < this.candlesProgress.Length; i++)
        {
            this.candlesProgress[i] = Random.Range(0f, 3f);
        }
    }

    void Update()
    {
        this.transform.localRotation = Quaternion.Euler(0, 0, this.maxAngleRotation * Mathf.Sin(timeProgress));
        this.timeProgress += this.invertedRotation ? -Time.deltaTime : Time.deltaTime;

        bool chandelierShouldBeLit = false;

        for (int i = 0; i < this.candles.Length; i++)
        {
            float scx = 1f + 0.1f * Mathf.Cos(4*(Time.timeSinceLevelLoad + this.candlesProgress[i]));
            float scy = 1f + 0.2f * Mathf.Sin(4*(Time.timeSinceLevelLoad + this.candlesProgress[i]));            
            this.candles[i].transform.localScale = new Vector3(scx, scy, 1);
            this.glows[i].transform.localScale = new Vector3(scx, scy, 1);


            if(this.map.waterline.currentLevel >= this.candles[i].transform.position.y)
            {
                if (this.candles[i].gameObject.activeInHierarchy)
                {
                    this.candles[i].SetActive(false);
                    this.glows[i].SetActive(false);
                }
                this.reigniteCounters[i] = 60 + Random.Range(0,20);
            }
            else 
            {
                bool shouldBeActive = true;
                if(this.reigniteCounters[i] > 0)
                {
                    shouldBeActive = false;
                    this.reigniteCounters[i] -= 1;
                }
                else
                {
                    chandelierShouldBeLit = true;
                }
                this.candles[i].gameObject.SetActive(shouldBeActive);
                this.glows[i].SetActive(shouldBeActive);
            }
        }
        
        if (this.map.waterline.currentLevel < objectUsedToDetermineIfChandelierIsLit.transform.position.y && chandelierShouldBeLit)
        {
            if (this.chandelierSpriteRenderer.sprite != this.chandelierLitSprite)
            {
                this.chandelierSpriteRenderer.sprite = this.chandelierLitSprite;
            }
        }
        else
        {
            if (this.chandelierSpriteRenderer.sprite != this.chandelierUnlitSprite)
            {
                this.chandelierSpriteRenderer.sprite = this.chandelierUnlitSprite;
            }
        }
    }

    private void OnEnable()
    {
        if (this.map != null)
            InitChandelier();
    }
}
