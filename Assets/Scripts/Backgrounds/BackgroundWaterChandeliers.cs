
using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;

public class BackgroundWaterChandeliers
{
    public class Copy
    {
        public Transform transform;
        public SpriteRenderer[] spriteRenderers;
    };

    public Map map;
    public Action<Transform> onSetup;
    public Action<Transform> onAdvance;
    public Transform firstInstance;
    // public List<Copy> copies;
    public float artWidthTiles;
    public float artHeightTiles;
    public float parallaxFactor;
    public Vector2 offset;
    private MaterialPropertyBlock propertyBlock;
    public Dictionary<Chunk, List<Copy>> chunkCopies = new Dictionary<Chunk,List<Copy>>();
    public Copy initialCopy;

    public List<Copy> pooledFreeCopies = new List<Copy>();


    public void Init(
        Map map,
        Transform firstInstance,
        float artWidthTiles,
        float artHeightTiles,
        float parallaxFactor,
        Action<Transform> onSetup,
        Action<Transform> onAdvance,
        Material material = null
    )
    {
        this.map = map;
        this.firstInstance = firstInstance;
        this.firstInstance.gameObject.SetActive(false);
        
        this.initialCopy = new Copy
        {
            transform = firstInstance,
            spriteRenderers = firstInstance.GetComponentsInChildren<SpriteRenderer>(),
        };

        foreach (
            var chandelier in this.initialCopy.transform.
            GetComponentsInChildren<BackgroundWaterChandelier>()
        ) {
            chandelier.Init(this.map);
        }

        this.artWidthTiles = artWidthTiles;
        this.artHeightTiles = artHeightTiles;
        this.parallaxFactor = parallaxFactor;
        this.onSetup = onSetup;
        this.onAdvance = onAdvance;
        this.onSetup?.Invoke(this.firstInstance);
        this.propertyBlock = new MaterialPropertyBlock();

        foreach (var sr in this.initialCopy.spriteRenderers)
        {
            sr.material = material == null ? Assets.instance.spritesScissorMaterial : material;
        }
    }

    public void Redraw(Chunk[] chunks)
    {
        if(chunks.Length == 0)
        {
            foreach(var kv in this.chunkCopies)
            {
                foreach(var copy in kv.Value)
                {
                    copy.transform.gameObject.SetActive(false);
                }
            }
            return;
        }

        Vector2 cameraPosition = this.map.gameCamera.transform.position;
        Vector2 parallaxOffset =
            this.map.gameCamera.transform.position * (1 - parallaxFactor);

        foreach (var chunk in chunks)
        {
            // chunkBase is the chunk we're aligning everything to, which might
            // be an earlier horizontal chunk to keep them connecting ok
            var chunkBase = chunk;
            int chunkCopiesUsed = 0;
            for (int n = chunkBase.index - 1; n >= 0; n -= 1)
            {
                if (this.map.chunks[n].isHorizontal == true)
                {
                    if (this.map.chunks[n].yMax > chunkBase.yMax)
                    {
                        chunkBase = this.map.chunks[n];
                    }            
                }
                else
                    break;
            }

            for (int n = chunkBase.index; n< this.map.chunks.Count; n += 1)
            {
                if(this.map.chunks[n] == chunk)
                    continue;

                if (this.map.chunks[n].isHorizontal == true)
                {
                    if (this.map.chunks[n].yMax > chunkBase.yMax)
                    {
                        chunkBase = this.map.chunks[n];
                    }
                }
                else
                    break;
            }

            float firstY = chunk.yMax;
            float chunkWidth = chunk.xMax - chunk.xMin;
                
            // calculate number of copies, give it some extra space (4 tiles at each end), take walls into account (2)
            int howMany = Mathf.FloorToInt((chunkWidth - 2 - 8) / artWidthTiles);
            
            Vector3 chunkCenter = new Vector3(
                chunk.xMin + 0.5f * (chunk.xMax - chunk.xMin),
                chunk.yMin + 0.5f * (chunk.yMax - chunk.yMin));

            float maxDelta = (chunkWidth - 2 - (howMany * this.artWidthTiles)) * 0.5f;                
            var distFromCenter = chunkCenter - this.map.gameCamera.transform.position;

            distFromCenter.x = Mathf.Clamp(distFromCenter.x, -chunkWidth * 0.5f, chunkWidth * 0.5f);
            parallaxOffset = -(distFromCenter / (0.5f * chunkWidth)) * (1 - parallaxFactor) * maxDelta;
            
            int j = 0;
            for (
            float tx = -(howMany - 1) / 2f;
            tx <= (howMany - 1) / 2f;
            tx += 1)
            {
                float y = firstY;
                {
                    if(!this.chunkCopies.ContainsKey(chunk))
                    {
                        this.chunkCopies.Add(chunk, new List<Copy>());
                    }

                    if(chunkCopiesUsed >= this.chunkCopies[chunk].Count)
                    {
                        if(this.pooledFreeCopies.Count > 0)
                        {
                            this.chunkCopies[chunk].Add(this.pooledFreeCopies[0]);
                            this.pooledFreeCopies.RemoveAt(0);
                        }
                        else
                        {
                            Transform newTransform = GameObject.Instantiate(
                                this.firstInstance,
                                this.firstInstance.parent
                            );
                            this.onSetup?.Invoke(newTransform);

                            foreach (
                                var chandelier in newTransform.
                                GetComponentsInChildren<BackgroundWaterChandelier>()
                            ) {
                                chandelier.Init(this.map);
                            }

                            this.chunkCopies[chunk].Add(new Copy
                            {
                                transform = newTransform,
                                spriteRenderers =
                                    newTransform.GetComponentsInChildren<SpriteRenderer>()
                            });
                        }
                    }

                    Copy copy = this.chunkCopies[chunk][chunkCopiesUsed];
                    copy.transform.position = new Vector3(
                        chunkCenter.x + parallaxOffset.x + (tx * this.artWidthTiles), y, 0
                    );
                    copy.transform.gameObject.SetActive(true);

                    foreach (var sr in copy.spriteRenderers)
                    {
                        var min = new Vector2(chunk.xMin, chunk.yMin);
                        var max = new Vector2(chunk.xMax + 1, chunk.yMax + 1);

                        sr.GetPropertyBlock(this.propertyBlock);
                        this.propertyBlock.SetFloat("_MinimumX", min.x);
                        this.propertyBlock.SetFloat("_MaximumX", max.x);
                        this.propertyBlock.SetFloat("_MinimumY", min.y);
                        this.propertyBlock.SetFloat("_MaximumY", max.y);
                        sr.SetPropertyBlock(this.propertyBlock);
                    }

                    this.onAdvance?.Invoke(copy.transform);
                    chunkCopiesUsed += 1;
                    j++;
                }
            }

            if (this.chunkCopies.ContainsKey(chunk))
            {
                for (int n = chunkCopiesUsed; n < this.chunkCopies[chunk].Count; n += 1)
                {
                    this.chunkCopies[chunk][n].transform.gameObject.SetActive(false);
                }
            }
        }

        foreach(var k in this.chunkCopies.Keys)
        {
            if(!chunks.Contains(k))
            {
                //remove copies from chunks which are not considered and assign them to the pool
                foreach(var copy in this.chunkCopies[k])
                {
                    copy.transform.gameObject.SetActive(false);
                    this.pooledFreeCopies.Add(copy);
                }
                this.chunkCopies[k].Clear();
            }
        }
    }
}
