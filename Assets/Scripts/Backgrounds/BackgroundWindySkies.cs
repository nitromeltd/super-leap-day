using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundWindySkies : Background
{
    private Copier back;
    private Copier front;
    private Copier balloon;
    private Copier balloon2;
    private Copier balloon3;
    private Copier cloud1;
    private Copier cloud2;
    private Copier cloud3;
    private Copier cloud4;
    private Transform shadow;

    public Animated.Animation wind1;
    public Animated.Animation wind2;
    public Animated.Animation wind3;
    public Animated.Animation wind4;

    public float timer;

    public override void Init(Map map)
    {
        float scaleFactor = 1.5f;
        base.Init(map);

        this.shadow = this.transform.Find("Gradient");

        this.back = new Copier(map, transform.Find("Back"), new Rect(0, 0, 60, 60));
        this.front = new Copier(map, transform.Find("Front"), new Rect(0, 0, 60, 60));
        this.balloon = new Copier(map, transform.Find("Balloon"), new Rect(-10, -10, 60, 60));
        this.balloon2 = new Copier(map, transform.Find("Balloon 2"), new Rect(-10, -10, 60, 60));
        this.balloon3 = new Copier(map, transform.Find("Balloon 3"), new Rect(-10, -10, 60, 60));
        this.cloud1 = new Copier(map, transform.Find("Cloud 1"), new Rect(-10, -10, 50, 50));
        this.cloud2 = new Copier(map, transform.Find("Cloud 2"), new Rect(-10, -10, 50, 50));
        this.cloud3 = new Copier(map, transform.Find("Cloud 3"), new Rect(-10, -10, 50, 50));
        this.cloud4 = new Copier(map, transform.Find("Cloud 4"), new Rect(-10, -10, 50, 50));

        this.back.yRepeatDistance = 35.96f* 0.525f * scaleFactor;
        this.back.xRepeatDistance = 35.96f* 0.525f * scaleFactor;
        this.front.yRepeatDistance = 36f * 0.525f * scaleFactor;
        this.front.xRepeatDistance = 36f * 0.525f * scaleFactor;
        this.balloon.yRepeatDistance = 25f * scaleFactor;
        this.balloon.xRepeatDistance = 25f * scaleFactor;
        this.balloon2.yRepeatDistance = 25f * scaleFactor;
        this.balloon2.xRepeatDistance = 25f * scaleFactor;
        this.balloon3.yRepeatDistance = 25f * scaleFactor;
        this.balloon3.xRepeatDistance = 25f * scaleFactor;
        this.cloud1.xRepeatDistance = 30f * scaleFactor;
        this.cloud1.yRepeatDistance = 25f * scaleFactor;
        this.cloud2.xRepeatDistance = 40f * scaleFactor;
        this.cloud2.yRepeatDistance = 35f * scaleFactor;
        this.cloud3.xRepeatDistance = 25f * scaleFactor;
        this.cloud3.yRepeatDistance = 50f * scaleFactor;
        this.cloud4.xRepeatDistance = 45f * scaleFactor;
        this.cloud4.yRepeatDistance = 35f * scaleFactor;

        this.timer = 0;
    }

    public override void Advance()
    {
        // base.Advance();
        timer += Time.deltaTime;
        Vector2 cam = this.map.gameCamera.transform.position;

        float cloudX = this.map.frameNumber * -0.007f;
        float cloudX2 = this.map.frameNumber * -0.003f;
        this.front.Move((cam.x * -0.1f - cloudX), (cam.y * -0.1f));
        this.back.Move((cam.x * -0.07f), (cam.y * -0.07f));
        this.balloon.Move((cam.x * -0.15f), (cam.y * -0.15f));
        this.balloon2.Move((cam.x * -0.15f), (cam.y * -0.15f));
        this.balloon3.Move((cam.x * -0.15f), (cam.y * -0.15f));
        this.cloud1.Move((cam.x * -0.125f - cloudX2), (cam.y * -0.125f));
        this.cloud2.Move((cam.x * -0.125f - cloudX2), (cam.y * -0.125f));
        this.cloud3.Move((cam.x * -0.125f - cloudX2), (cam.y * -0.125f));
        this.cloud4.Move((cam.x * -0.125f - cloudX2), (cam.y * -0.125f));

        if (GameCamera.IsLandscape() == true)
        {
            this.shadow.position = this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector3(0.5f, 1.26f, 20));
        }
        else
        {
            this.shadow.position = this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector3(0.5f, 1, 20));
        }

        if (Random.Range(0.00f, 5.00f) > 4.97f)
        {
            int pickAni = Random.Range(0, 4);
            Animated.Animation wind = wind1;
            switch (pickAni)
            {
                case 0: 
                    wind = wind1;
                    break;
                case 1:
                    wind = wind2;
                    break;
                case 2:
                    wind = wind3;
                    break;
                case 3:
                    wind = wind4;
                    break;
            }
            
            var p = Particle.CreateAndPlayOnce(
                wind,
                Vector2.zero,
                this.transform
            );
            p.gameObject.name = "Background Wind";
            p.transform.position =
                ((Vector2)this.map.gameCamera.transform.position).Add(
                    Random.Range(-5, 5), Random.Range(-5, 5)
                );
            p.transform.localScale *= -0.4f;
            p.spriteRenderer.sortingLayerName = "Tiles - Mid Layer";
            p.spriteRenderer.sortingOrder = 30;
            p.transform.SetParent(this.transform);
        }

        Vector3 offset = new Vector3(
            0.5f * Mathf.Sin((float)this.map.frameNumber /200),
            0.5f *Mathf.Cos((float)this.map.frameNumber /150)
            );
        this.transform.position = offset + new Vector3(
            this.map.gameCamera.transform.position.x,
            this.map.gameCamera.transform.position.y,
            10
        );
    }
}
