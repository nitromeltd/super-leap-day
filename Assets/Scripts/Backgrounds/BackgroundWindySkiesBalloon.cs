using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundWindySkiesBalloon : MonoBehaviour
{
    private const float BreathePeriod = .3f;
    private const float BreatheAmplitude = .15f;

    private Vector3 initialPos;
    public Animated propeller;
    public Animated.Animation propellerAnimation;

    public BackgroundWindySkies backgroundWindySkies;

    void Start()
    {
        initialPos = this.transform.localPosition;
        propeller.PlayAndLoop(propellerAnimation);
    }

    void Update()
    {
        float theta = backgroundWindySkies.timer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);

        this.transform.localPosition = initialPos + this.transform.up * distance;
    }
}
