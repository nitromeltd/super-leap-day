using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundWindySkiesPropeller : MonoBehaviour
{
    public float speed;
    private float timer;
    public float startRotation;

    public BackgroundWindySkies backgroundWindySkies;

    private void Start()
    {
        this.transform.rotation = Quaternion.Euler(0, 0, startRotation);
    }

    void Update()
    {
        this.transform.rotation = Quaternion.Euler(0, 0, this.transform.rotation.z + backgroundWindySkies.timer * speed);
    }
}
