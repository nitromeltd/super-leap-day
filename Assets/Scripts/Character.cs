using UnityEngine;

public enum Character
{
    Yolk, Goop, Sprout, Puffer, King
}
