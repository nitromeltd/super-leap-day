using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/*
    Everything Chunk exposes to the rest of the game code should be in map space
    i.e. to get the first tile of the chunk we call TileAt(xMin, yMin) not TileAt(0, 0).
    Since the chunk level file obviously starts at (0, 0), we need to do a conversion.
    - Paths are translated on init and then left public.
    - Groups are handled internally by GroupLayer class.
    - Everything else is offered as a FooAt(tx,ty) method which takes map tx, ty.
*/

public class Chunk : MonoBehaviour
{
    public Map map;
    public int index;
    public string chunkName;
    public int xMin;
    public int xMax; // inclusive
    public int yMin;
    public int yMax;
    public Marker entry;
    public Marker exit;
    public BoundaryMarkers boundaryMarkers;
    public bool isHorizontal;
    public bool isTransitionalTunnel;
    public bool isAirlock;
    public bool isAdditionalStepDownForHorizontal;
    public bool isMultiplayerSideRoom;
    public bool isTomorrowOnSuperLeapDay;
    public bool isFlippedHorizontally;
    public int? checkpointNumber;
    public Player.Orientation? gravityDirection = Player.Orientation.Normal;
    public Player.Orientation? initialGravityDirection = Player.Orientation.Normal;
    public ABBlock.Colour abBlockColour = ABBlock.Colour.BLUE;
    public ChunkDebugView chunkDebugView;
    public int totalFruitOnStart;

    public bool hasFinishedInitDetails = false;
    public bool hasFinishedInitItems = false;
    public bool hasFinishedInitAutotile = false;
    public bool hasFinishedInitDecoration = false;
    public bool HasLoaded => this.hasFinishedInitDecoration;

    public List<Item.Def> itemDefs;
    public List<Item> items;
    public Item[,] itemsByGridCell1;
    public Item[,] itemsByGridCell2;
    public Portal[,] portalsByGridCell;
    public GravityArea[,] gravityAreasByGridCell;
    public CloudBlock[,] cloudBlockByGridCell;

    public List<Item>              subsetOfItemsRaycastCanHit           = new();
    public List<Enemy>             subsetOfItemsOfTypeEnemy             = new();
    public List<WalkingEnemy>      subsetOfItemsOfTypeWalkingEnemy      = new();
    public List<AppleBoulder>      subsetOfItemsOfTypeAppleBoulder      = new();
    public List<AshBlock>          subsetOfItemsOfTypeAshBlock          = new();
    public List<BeachBall>         subsetOfItemsOfTypeBeachBall         = new();
    public List<BreakableBlock>    subsetOfItemsOfTypeBreakableBlock    = new();
    public List<CCTVGateBlock>     subsetOfItemsOfTypeCCTVGateBlock     = new();
    public List<Chest>             subsetOfItemsOfTypeChest             = new();
    public List<CloudBird>         subsetOfItemsOfTypeCloudBird         = new();
    public List<CloudBlock>        subsetOfItemsOfTypeCloudBlock        = new();
    public List<Dandelion>         subsetOfItemsOfTypeDandelion         = new();
    public List<GravityAtmosphere> subsetOfItemsOfTypeGravityAtmosphere = new();
    public List<HauntedStatue>     subsetOfItemsOfTypeHauntedStatue     = new();
    public List<LightUpBlock>      subsetOfItemsOfTypeLightUpBlock      = new();
    public List<Minecart>          subsetOfItemsOfTypeMinecart          = new();
    public List<Raft>              subsetOfItemsOfTypeRaft              = new();
    public List<RaftMotor>         subsetOfItemsOfTypeRaftMotor         = new();
    public List<RaftMotorBoth>     subsetOfItemsOfTypeRaftMotorBoth     = new();
    public List<RandomBox>         subsetOfItemsOfTypeRandomBox         = new();
    public List<Rail>              subsetOfItemsOfTypeRail              = new();
    public List<RailDoor>          subsetOfItemsOfTypeRailDoor          = new();
    public List<RumbleGround>      subsetOfItemsOfTypeRumbleGround      = new();
    public List<SeaMine>           subsetOfItemsOfTypeSeaMine           = new();
    public List<SnowBlock>         subsetOfItemsOfTypeSnowBlock         = new();
    public List<TrafficCone>       subsetOfItemsOfTypeTrafficCone       = new();
    public List<TNTBlock>          subsetOfItemsOfTypeTNTBlock          = new();

    public NitromeEditor.PathLayer pathLayer;
    public NitromeEditor.PathLayer connectionLayer;
    public GroupLayer groupLayer;

    public TileGrid backTileGrid1;
    public TileGrid backTileGrid2;
    public TileGrid[] solidTilesA;
    public TileGrid[] solidTilesB;
    public SpriteRenderer[] extraSpriteRenderersForTransitionalTunnel;

    public int timeToRemainActive; // to let enemy death animations play out

    public NitromeEditor.Level levelData;
    private NitromeEditor.TileLayer triggerLayer;
    private NitromeEditor.TileLayer markerLayer;

    public Pathfinding pathfinding;
    public HashSet<XY> xyOfAlreadySpawnedPickups = new HashSet<XY>();
    public enum MarkerType
    {
        Wide, Left, Center, Right, Wall
    }

    public class Marker
    {
        public XY mapLocation;
        public MarkerType type;
    }

    public class BoundaryMarkers
    {
        // expressed relative to original chunk origin (i.e. their location in the editor)
        public XY bottomLeft;
        public XY topRight;
    }

    public Chunk Previous() =>
        (this.index > 0) ?
        this.map.chunks[this.index - 1] :
        null;

    public Chunk Next() =>
        (this.index < this.map.chunks.Count - 1) ?
        this.map.chunks[this.index + 1] :
        null;

    private NitromeEditor.Level FlipHorizontally(NitromeEditor.Level levelData)
    {
        var w = levelData.Columns();
        var h = levelData.Rows();

        NitromeEditor.TileLayer FlipTileLayer(NitromeEditor.TileLayer source)
        {
            var target = new NitromeEditor.TileLayer();
            target.layerName = source.layerName;
            target.tiles = new NitromeEditor.TileLayer.Tile[w * h];

            for (int n = 0; n < source.tiles.Length; n += 1)
            {
                int tx = n % w;
                int ty = n / w;

                var sourceTile = source.tiles[(w - 1 - tx) + (ty * w)];
                target.tiles[n].tile = sourceTile.tile;
                target.tiles[n].offsetX = 16 - sourceTile.offsetX;
                target.tiles[n].offsetY = sourceTile.offsetY;
                target.tiles[n].flip = !sourceTile.flip;
                switch (sourceTile.rotation)
                {
                    case 0:    target.tiles[n].rotation =   0;  break;
                    case 90:   target.tiles[n].rotation = 270;  break;
                    case 180:  target.tiles[n].rotation = 180;  break;
                    case 270:  target.tiles[n].rotation =  90;  break;
                }
                target.tiles[n].properties = sourceTile.properties;
            }
            return target;
        }

        NitromeEditor.PathLayer FlipPathLayer(NitromeEditor.PathLayer sourcePathLayer)
        {
            var targetPathLayer = sourcePathLayer.DeepCopy();

            foreach (var path in targetPathLayer.paths)
            {
                foreach (var node in path.nodes)
                    node.x = (w * 16) - node.x;

                path.Recalculate();
            }

            return targetPathLayer;
        }

        var result = new NitromeEditor.Level();
        result.minX = 0;
        result.maxX = levelData.maxX;
        result.minY = 0;
        result.maxY = levelData.maxY;
        result.tileLayers = levelData.tileLayers.Select(FlipTileLayer).ToArray();
        result.pathLayers = levelData.pathLayers.Select(FlipPathLayer).ToArray();
        return result;
    }

    public void InitEssentials(
        Map map,
        int index,
        string chunkName,
        NitromeEditor.Level levelData,
        LevelGeneration.RowType rowType,
        bool flipHorizontally,
        bool fillAreaOfInterestWithCheckpoint,
        XY entry,
        int nextCheckpointNumber
    )
    {
        if (flipHorizontally == true)
            levelData = FlipHorizontally(levelData);

        this.gameObject.name = $"Chunk #{index + 1}: {chunkName}";

        this.map                   = map;
        this.index                 = index;
        this.chunkName             = chunkName;
        this.isFlippedHorizontally = flipHorizontally;
        this.levelData             = levelData;
        this.pathLayer             = levelData.PathLayerByName("paths");
        this.connectionLayer       = levelData.PathLayerByName("connections");
        this.triggerLayer          = levelData.TileLayerByName("triggers");
        this.markerLayer           =
            levelData.TileLayerByName("markers") ??
            levelData.TileLayerByName("chunk_separators");
        this.chunkDebugView =
            Instantiate(Assets.instance.chunkNameDisplay)
            .GetComponent<ChunkDebugView>();

        Marker entryLocal, exitLocal;
        ProcessMarkers(
            levelData,
            markerLayer,
            out entryLocal,
            out exitLocal,
            out this.boundaryMarkers
        );

        this.xMin = entry.x - entryLocal.mapLocation.x + boundaryMarkers.bottomLeft.x;
        this.yMin = entry.y - entryLocal.mapLocation.y + boundaryMarkers.bottomLeft.y;
        this.xMax = this.xMin + boundaryMarkers.topRight.x - boundaryMarkers.bottomLeft.x;
        this.yMax = this.yMin + boundaryMarkers.topRight.y - boundaryMarkers.bottomLeft.y;
        this.transform.position = new Vector3(
            this.xMin * Tile.Size,
            this.yMin * Tile.Size
        );
        this.entry = entryLocal;
        this.entry.mapLocation.x += this.xMin - boundaryMarkers.bottomLeft.x;
        this.entry.mapLocation.y += this.yMin - boundaryMarkers.bottomLeft.y;
        this.exit = exitLocal;
        this.exit.mapLocation.x += this.xMin - boundaryMarkers.bottomLeft.x;
        this.exit.mapLocation.y += this.yMin - boundaryMarkers.bottomLeft.y;
        this.isHorizontal =
            this.entry.type == MarkerType.Wall &&
            ((this.exit.type == MarkerType.Wall) || (this.xMax != this.xMin + 15));
        this.isTransitionalTunnel =
            chunkName == "side_tunnel" ||
            chunkName == "side_tunnel_for_windy_skies" ||
            chunkName == "side_tunnel_very_wide";
        this.isAdditionalStepDownForHorizontal =
            chunkName == "horizontal_step_down" ||
            chunkName == "horizontal_step_down_for_windy_skies" ||
            chunkName == "horizontal_breathing_point" ||
            chunkName == "horizontal_breathing_point_for_windy_skies";
        this.isAirlock =
            chunkName == "airlock" ||
            chunkName == "airlock_horizontal";
        this.isMultiplayerSideRoom =
            chunkName == "multiplayer_side_room" ||
            chunkName == "multiplayer_side_room_for_windy_skies";
        this.isTomorrowOnSuperLeapDay =
            chunkName == "tomorrow_on_super_leap_day";

        this.chunkDebugView.Init(this);
        RefreshDebugUIEnabled();

        if (rowType == LevelGeneration.RowType.DedicatedCheckpoint ||
            rowType == LevelGeneration.RowType.BronzeCup ||
            rowType == LevelGeneration.RowType.SilverCup ||
            rowType == LevelGeneration.RowType.End ||
            fillAreaOfInterestWithCheckpoint == true)
        {
            this.checkpointNumber = nextCheckpointNumber;
        }

        var sx = 1 + this.xMax - this.xMin;
        var sy = 1 + this.yMax - this.yMin;

        this.backTileGrid1 = new();
        this.backTileGrid1.InitBlank(sx, sy);
        this.backTileGrid2 = new();
        this.backTileGrid2.InitBlank(sx, sy);

        this.solidTilesA = new [] { new TileGrid() };
        this.solidTilesA[0].InitBlank(sx, sy);
        this.solidTilesB = new [] { new TileGrid() };
        this.solidTilesB[0].InitBlank(sx, sy);

        this.items = new();

        this.itemsByGridCell1       = new Item[sx, sy];
        this.itemsByGridCell2       = new Item[sx, sy];
        this.portalsByGridCell      = new Portal[sx, sy];
        this.gravityAreasByGridCell = new GravityArea[sx, sy];
        this.cloudBlockByGridCell   = new CloudBlock[sx, sy];

        if (this.map.theme == Theme.GravityGalaxy)
        {
            // OutsideBoundaryForGravityGalaxy needs to know which chunks are zero-g,
            // sometimes very far ahead of the chunk being init'ed, so that it can
            // shape itself around airlocks.

            var objectLayer = levelData.TileLayerByName("objects");
            foreach (var tile in objectLayer.tiles)
            {
                if (tile.tile == "gravity_none_on_start")
                {
                    this.initialGravityDirection = null;
                    break;
                }
            }
        }
    }

    public void InitDetails(
        bool fillAreaOfInterestWithCheckpoint,
        bool fillAreaOfInterestWithBonusLift
    )
    {
        var process = InitDetailsCoroutine(
            fillAreaOfInterestWithCheckpoint, fillAreaOfInterestWithBonusLift
        );
        while (process.MoveNext()) { /* just do all of the work synchronously */ }
    }

    public IEnumerator InitDetailsCoroutine(
        bool fillAreaOfInterestWithCheckpoint,
        bool fillAreaOfInterestWithBonusLift
    )
    {
        var sx = levelData.Columns();
        var sy = levelData.Rows();

        var tBack1    = levelData.TileLayerByName("tiles_back1");
        var tBack2    = levelData.TileLayerByName("tiles_back2");
        var tSolidB   = levelData.TileLayerByName("tiles_layer_b");
        var tSolid    = levelData.TileLayerByName("tiles");
        var tGroups   = levelData.TileLayerByName("groups");

        this.backTileGrid1    = new TileGrid();
        this.backTileGrid2    = new TileGrid();
        this.solidTilesA      = new TileGrid[] { new TileGrid() };
        this.solidTilesB      = new TileGrid[] { new TileGrid() };
        this.groupLayer       = new GroupLayer(this, tGroups, sx, sy);

        this.backTileGrid1 .Init(sx, sy, tBack1,  this);
        this.backTileGrid2 .Init(sx, sy, tBack2,  this);
        this.solidTilesA[0].Init(sx, sy, tSolid,  this);
        this.solidTilesB[0].Init(sx, sy, tSolidB, this);

        foreach (var group in this.groupLayer.allGroups)
        {
            TileGrid tileGrid;

            tileGrid = this.groupLayer.TakeTilesIntoNewGrid(this.solidTilesA[0], group);
            if (tileGrid != null)
                this.solidTilesA = this.solidTilesA.Append(tileGrid).ToArray();

            tileGrid = this.groupLayer.TakeTilesIntoNewGrid(this.solidTilesB[0], group);
            if (tileGrid != null)
                this.solidTilesB = this.solidTilesB.Append(tileGrid).ToArray();
        }

        this.solidTilesA[0].ShrinkToFit((this.xMin, this.xMax, this.yMin, this.yMax));
        this.solidTilesB[0].ShrinkToFit((this.xMin, this.xMax, this.yMin, this.yMax));

        this.pathLayer = this.pathLayer.DeepCopy();
        this.connectionLayer = this.connectionLayer.DeepCopy();
        foreach (var layer in new [] { this.pathLayer, this.connectionLayer })
        {
            foreach (var path in layer.paths)
            {
                foreach (var node in path.nodes)
                {
                    node.x /= 16;
                    node.y /= 16;
                    node.arcLength /= 16;
                    node.x += this.xMin - boundaryMarkers.bottomLeft.x;
                    node.y += this.yMin - boundaryMarkers.bottomLeft.y;
                }
                foreach (var edge in path.edges)
                {
                    edge.speedPixelsPerSecond /= 16;
                }
                path.Recalculate();
            }
        }

        this.items                  = new List<Item>();
        this.itemsByGridCell1       = new Item[sx, sy];
        this.itemsByGridCell2       = new Item[sx, sy];
        this.portalsByGridCell      = new Portal[sx, sy];
        this.gravityAreasByGridCell = new GravityArea[sx, sy];
        this.cloudBlockByGridCell   = new CloudBlock[sx, sy];

        this.itemDefs     = new List<Item.Def>();
        var objectsLayer  = levelData.TileLayerByName("objects");
        var objectsLayer2 = levelData.TileLayerByName("objects2");

        void ProcessObjectTile(
            int x,
            int y,
            NitromeEditor.TileLayer.Tile t,
            Item[,] itemGrid
        )
        {
            var mapTx = x + this.xMin - boundaryMarkers.bottomLeft.x;
            var mapTy = y + this.yMin - boundaryMarkers.bottomLeft.y;

            if (t.tile == "player")
            {
                if (this.map.player == null)
                {
                    var obj = Instantiate(
                        Assets.instance.loadedPlayer, this.map.transform
                    );
                    obj.name = obj.name.Replace("(Clone)", "");
                    this.map.player = obj.GetComponent<Player>();
                    this.map.player.Init(
                        this.map, mapTx * Tile.Size, mapTy * Tile.Size
                    );
                }
                else
                {
                    Debug.LogError("Tried to instantiate a second Player.");
                }
                return;
            }

            GameObject prefab;
            if (t.tile == "area_of_interest_6x7")
            {
                if (fillAreaOfInterestWithCheckpoint == true)
                    prefab = Assets.instance.checkpoint;
                else if (fillAreaOfInterestWithBonusLift == true)
                    prefab = Assets.instance.bonusLift;
                else
                {
                    FillAreaOfInterest(x, y);
                    return;
                }
            }
            else
            {
                prefab = Assets.instance.GetPrefabByTileName(t.tile);
                if (prefab == null)
                {
                    Debug.Log(
                        $"No prefab for tile name '{t.tile}' in chunk #{this.index + 1}."
                    );
                    return;
                }
            }

            if (prefab?.GetComponent<Item>())
            {
                GameObject obj = Instantiate(prefab, this.transform);
                obj.name = $"{t.tile} ({mapTx}, {mapTy})";
                var it = obj.GetComponent<Item>();

                // this is handled better within each item's Init() method, but this
                // still helps when items search for each other before they're all Initted.
                it.position.x = (mapTx * Tile.Size) + (t.offsetX / 16f);
                it.position.y = (mapTy * Tile.Size) + (t.offsetY / 16f);

                var def = new Item.Def(mapTx, mapTy, t, this);
                it.ItemDef = def;
                it.chunk = this;
                this.items.Add(it);
                this.itemDefs.Add(def);

                // most items only take up one cell in the grid and can be
                // handled here but some like pipes and grip-ground need more care
                // and are handled in InitItems() instead
                if (mapTx >= this.xMin && mapTx <= this.xMax &&
                    mapTy >= this.yMin && mapTy <= this.yMax &&
                    !(it is GripGround))
                {
                    itemGrid[mapTx - this.xMin, mapTy - this.yMin] = it;
                }
            }
            else
            {
                Debug.Log("Prefab has no Item or Player component.");
            }
        }

        for (int x = 0; x < sx; x += 1)
        {
            for (int y = 0; y < sy; y += 1)
            {
                var location = levelData.GetLocationIndex(x, y);

                var t = objectsLayer.tiles[location];
                if (t.tile != null && t.tile != "")
                {
                    ProcessObjectTile(x, y, t, itemsByGridCell1);
                    yield return null;
                }

                if (objectsLayer2 != null)
                {
                    t = objectsLayer2.tiles[location];
                    if (t.tile != null && t.tile != null)
                    {
                        ProcessObjectTile(x, y, t, itemsByGridCell2);
                        yield return null;
                    }
                }
            }
        }

        EnforceItemOrder();

        this.totalFruitOnStart =
            this.items.Where(i => i is Fruit or CrystalFruit or FruitFish).Count();

        this.pathfinding = new Pathfinding(this);
        // CheckSoundness();

        this.hasFinishedInitDetails = true;
    }

    private void FillAreaOfInterest(int x, int y)
    {
        // this is all intended to masquerade as the standard item creation
        // procedure in Init(), so if that changes this should change too.

        void MakeTopTileAt(int gridTx, int gridTy)
        {
            var grid = this.solidTilesA[0];
            var tile = new Tile();
            tile.Init(
                gridTx + this.xMin,
                gridTy + this.yMin,
                gridTx,
                gridTy,
                new NitromeEditor.TileLayer.Tile { tile = "BLANK:topline" }
            );
            tile.tileGrid = grid;
            grid.tiles[gridTx + (gridTy * grid.columns)] = tile;
        }

        Item MakeItemAt(GameObject prefab, Vector2 gridPosition)
        {
            var obj = Instantiate(prefab, this.transform);
            var item = obj.GetComponent<Item>();
            int itemTx = Mathf.FloorToInt(gridPosition.x);
            int itemTy = Mathf.FloorToInt(gridPosition.y);
            obj.name =
                $"{item.GetType().Name} | area of interest ({itemTx}, {itemTy})";
            item.ItemDef = new Item.Def(
                itemTx + this.xMin,
                itemTy + this.yMin,
                new NitromeEditor.TileLayer.Tile {
                    offsetX = Mathf.RoundToInt((gridPosition.x - itemTx) * 16),
                    offsetY = Mathf.RoundToInt((gridPosition.y - itemTy) * 16)
                },
                this
            );
            this.items.Add(item);
            this.itemDefs.Add(item.ItemDef);
            this.itemsByGridCell1[itemTx, itemTy] = item;
            return item;
        }

        void MakeCirclePath(Vector2 center, float radius)
        {
            float cx = center.x;
            float cy = center.y;

            var path = new NitromeEditor.Path();
            path.nodes = new NitromeEditor.Path.Node[] {
                new NitromeEditor.Path.Node(path, cx,          cy - radius),
                new NitromeEditor.Path.Node(path, cx - radius, cy - radius),
                new NitromeEditor.Path.Node(path, cx - radius, cy),
                new NitromeEditor.Path.Node(path, cx - radius, cy + radius),
                new NitromeEditor.Path.Node(path, cx,          cy + radius),
                new NitromeEditor.Path.Node(path, cx + radius, cy + radius),
                new NitromeEditor.Path.Node(path, cx + radius, cy),
                new NitromeEditor.Path.Node(path, cx + radius, cy - radius)
            };
            path.nodes[1].arcLength = radius * Mathf.PI / 2;
            path.nodes[3].arcLength = radius * Mathf.PI / 2;
            path.nodes[5].arcLength = radius * Mathf.PI / 2;
            path.nodes[7].arcLength = radius * Mathf.PI / 2;
            path.closed = true;
            path.RecalculateEdgesAndLengths();
            foreach (var edge in path.edges)
                edge.speedPixelsPerSecond = 4;
            path.Recalculate();

            var pathList = this.pathLayer.paths.ToList();
            pathList.Add(path);
            this.pathLayer.paths = pathList.ToArray();
        }

        int index = Random.Range(0, 4);
        switch (index)
        {
            case 0: {
                MakeTopTileAt(x - 1, y + 2);
                MakeTopTileAt(x,     y + 2);
                MakeItemAt(Assets.instance.chestRandom, new Vector2(x, y + 3));
                break;
            }

            case 1: {
                MakeItemAt(Assets.instance.coin, new Vector2(x - 1.5f, y + 2));
                MakeItemAt(Assets.instance.coin, new Vector2(x + 1.5f, y + 4));
                MakeCirclePath(new Vector2(
                    this.xMin + x,
                    this.yMin + y + 3
                ), 1.3f);
                break;
            }

            case 2: {
                var prefab1 = Assets.instance.RandomFruit();
                var prefab2 = Assets.instance.RandomFruit();
                MakeItemAt(prefab1, new Vector2(x, y + 2));
                MakeItemAt(prefab1, new Vector2(x, y + 5));
                MakeItemAt(prefab2, new Vector2(x - 1.5f, y + 3.5f));
                MakeItemAt(prefab2, new Vector2(x + 1.5f, y + 3.5f));
                MakeCirclePath(new Vector2(x, y + 3.5f), 1.5f);
                break;
            }

            case 3: {
                MakeItemAt(Assets.instance.chestRandomFlying, new Vector2(x, y + 3));
                break;
            }
        }
    }

    public TileTheme TileTheme()
    {
        return (this.map.theme) switch
        {
            Theme.RainyRuins      => Assets.instance.rainyRuins.tileTheme,
            Theme.CapitalHighway  => Assets.instance.capitalHighway.tileTheme,
            Theme.ConflictCanyon  => Assets.instance.conflictCanyon.tileTheme,
            Theme.HotNColdSprings => Assets.instance.hotNColdSprings.tileTheme,
            Theme.GravityGalaxy   => (
                this.gravityDirection != null ?
                    Assets.instance.gravityGalaxy.tileThemeInterior :
                    Assets.instance.gravityGalaxy.tileThemeExterior
            ),
            Theme.SunkenIsland    =>
                this.isHorizontal == true ?
                Assets.instance.sunkenIsland.tileThemeForInside :
                Assets.instance.sunkenIsland.tileTheme,
            Theme.TreasureMines   => Assets.instance.treasureMines.tileTheme,
            Theme.WindySkies      => Assets.instance.windySkies.tileTheme,
            Theme.TombstoneHill   => Assets.instance.tombstoneHill.tileTheme,
            Theme.MoltenFortress      => Assets.instance.moltenFortress.tileTheme,
            Theme.OpeningTutorial => Assets.instance.openingTutorial.tileTheme,
            Theme.VRTrainingRoom  => Assets.instance.vrTrainingRoom.tileTheme,
            _                     => Assets.instance.rainyRuins.tileTheme
        };
    }

    public void InitAutotile() => Util.CoroutineFinish(InitAutotileCoroutine());

    public IEnumerator InitAutotileCoroutine()
    {
        var tileTheme = TileTheme();

        yield return
            this.backTileGrid1.AutotileCoroutine(tileTheme, "Tiles - Back 1", true);
        yield return
            this.backTileGrid2.AutotileCoroutine(tileTheme, "Tiles - Back 2", true);

        foreach (var grid in this.solidTilesA)
            yield return grid.AutotileCoroutine(tileTheme, "Tiles - Solid", false);

        foreach (var grid in this.solidTilesB)
            yield return grid.AutotileCoroutine(tileTheme, "Tiles - Layer B", false);

        if (this.map.theme == Theme.GravityGalaxy)
        {
            var outsideBoundary = Instantiate(
                Assets.instance.gravityGalaxy.outsideBoundary, this.transform
            );
            outsideBoundary.Init(this);
        }

        if (this.isTransitionalTunnel == true)
        {
            var position = new Vector2(this.exit.mapLocation.x, this.exit.mapLocation.y);
            this.extraSpriteRenderersForTransitionalTunnel =
                this.solidTilesA[0].CreateBackWallForExit(tileTheme, position);
        }

        this.hasFinishedInitAutotile = true;
    }

    public void InitItems() => Util.CoroutineFinish(InitItemsCoroutine());

    public IEnumerator InitItemsCoroutine()
    {
        foreach (var portal in this.items.OfType<Portal>())
        {
            int chunkTx = portal.ItemDef.tx - this.xMin;
            int chunkTy = portal.ItemDef.ty - this.yMin;
            this.portalsByGridCell[chunkTx, chunkTy] = portal;
        }
        foreach (var gravityArea in this.items.OfType<GravityArea>())
        {
            int chunkTx = gravityArea.ItemDef.tx - this.xMin;
            int chunkTy = gravityArea.ItemDef.ty - this.yMin;
            this.gravityAreasByGridCell[chunkTx, chunkTy] = gravityArea;
        }
        foreach (var cloudBlock in this.items.OfType<CloudBlock>())
        {
            int chunkTx = cloudBlock.ItemDef.tx - this.xMin;
            int chunkTy = cloudBlock.ItemDef.ty - this.yMin;
            this.cloudBlockByGridCell[chunkTx, chunkTy] = cloudBlock;
        }

        foreach (var it in this.items.ToArray())
        {
            it.Init(it.ItemDef);
            it.RefreshHitboxes();
            yield return null;
        }

        /* The code for setting up items in InitDetailsCoroutine.ProcessObjectTile
           places the item in .itemsByGridCell1/2, but it only fills the single tile
           in which the object was placed in the neditor. Certain larger items benefit
           from taking ALL of the cells in that array that their hitboxes cover. */
        foreach (var item in this.items.Where(i => i is MetalBlock or Pipe or GripGround))
        {
            var hitbox = item.hitboxes[0];
            var rect = hitbox.InWorldSpace();
            var minTx = Mathf.FloorToInt(rect.xMin) - this.xMin;
            var maxTx = Mathf.CeilToInt(rect.xMax) - 1 - this.xMin;
            var minTy = Mathf.FloorToInt(rect.yMin) - this.yMin;
            var maxTy = Mathf.CeilToInt(rect.yMax) - 1 - this.yMin;
            minTx = Mathf.Clamp(minTx, 0, this.xMax - this.xMin);
            maxTx = Mathf.Clamp(maxTx, 0, this.xMax - this.xMin);
            minTy = Mathf.Clamp(minTy, 0, this.yMax - this.yMin);
            maxTy = Mathf.Clamp(maxTy, 0, this.yMax - this.yMin);

            for (int y = minTy; y <= maxTy; y += 1)
            {
                for (int x = minTx; x <= maxTx; x += 1)
                {
                    if (this.itemsByGridCell1[x, y] == item) continue;

                    if (hitbox.customShape != null)
                    {
                        if (hitbox.customShape.IsSolidAnywhereInCell(
                            x + this.xMin - (int)item.position.x,
                            y + this.yMin - (int)item.position.y
                        ) == false) continue;
                    }

                    if (this.itemsByGridCell1[x, y] == null)
                        this.itemsByGridCell1[x, y] = item;
                    else
                        this.itemsByGridCell2[x, y] = item;
                }
            }
        }

        foreach (var item in this.items.OfType<GripGround>())
        {
            item.Autotile();
        }

        RefreshItemSubsets();

        this.hasFinishedInitItems = true;
    }

    public void InitSurfaceDecoration() => Util.CoroutineFinish(InitSurfaceDecorationCoroutine());

    public IEnumerator InitSurfaceDecorationCoroutine()
    {
        var tileTheme = TileTheme();

        if (this.chunkName == "start" ||
            this.chunkName == "start_windy_skies" ||
            this.chunkName == "ending" ||
            this.chunkName == "ending_windy_skies" ||
            this.chunkName == "cup_silver" ||
            this.chunkName == "cup_bronze" ||
            this.chunkName == "tomorrow_on_super_leap_day" ||
            this.chunkName == "coming_soon_billboard")
        {
            if(this.map.theme == Theme.GravityGalaxy)
            {
                this.solidTilesA[0].AddSpaceInteriorBackWall(tileTheme);
            }
            this.hasFinishedInitDecoration = true;
            yield break;
        }

        int columns   = this.solidTilesA[0].columns;
        int rows      = this.solidTilesA[0].rows;
        var permitted = new bool[columns, rows];

        for (int x = 0; x < columns; x += 1)
        {
            for (var y = 0; y < rows; y += 1)
            {
                permitted[x, y] = true;
            }
        }

        foreach (var item in this.items)
        {
            if (DoesItemBlockDecoration(item) == false)
                continue;

            foreach (var h in item.hitboxes)
            {
                var txMin = Mathf.FloorToInt(h.worldXMin) - this.xMin;
                var txMax = Mathf.FloorToInt(h.worldXMax) - this.xMin;
                var tyMin = Mathf.FloorToInt(h.worldYMin) - this.yMin;
                var tyMax = Mathf.FloorToInt(h.worldYMax) - this.yMin;
                tyMin -= 1;

                for (var tx = txMin; tx <= txMax; tx += 1)
                {
                    if (tx < 0 || tx >= columns) continue;
                    for (var ty = tyMin; ty <= tyMax; ty += 1)
                    {
                        if (ty < 0 || ty >= rows) continue;
                        permitted[tx, ty] = false;
                    }
                }
            }
        }

        // here, the 'permitted' grid only contains information about
        // items in the way. AddFoliage needs more complex information about
        // the tiles than can be expressed in a boolean, so don't forbid
        // foliage in tiles that way.
        if(this.map.theme != Theme.SunkenIsland || this.isHorizontal == false)
        {
            foreach (var grid in this.solidTilesA)
                grid.AddFoliage(tileTheme, permitted);

            foreach (var grid in this.solidTilesB)
                grid.AddFoliage(tileTheme, permitted);
        }

        if (this.map.theme == Theme.ConflictCanyon)
        {
            this.solidTilesA[0].AddSand(tileTheme);
        }

        // the wall sprites (below) take a simpler view of things and are
        // forbidden in any space with any tile in it, so we'll just add them to
        // the grid now.
        var gridA = this.solidTilesA[0];
        for (int ty = 0; ty < rows; ty += 1)
        {
            for (int tx = 0; tx < columns; tx += 1)
            {
                if (gridA.GetTile(
                    tx + this.xMin - gridA.xMin,
                    ty + this.yMin - gridA.yMin
                ) != null)
                    permitted[tx, ty] = false;
            }
        }

        if (tileTheme == Assets.instance.sunkenIsland?.tileTheme)
        {
            this.solidTilesA[0].AddIvy(tileTheme, permitted);
            yield return null;
        }
        else
        {
            this.solidTilesA[0].AddPillars(tileTheme, permitted);
            yield return null;
        }

        if (tileTheme.decorationSpritesPass2?.Length > 0)
        {
            this.solidTilesA[0].AddDecorationSpritesToBack(
                tileTheme.decorationSpritesPass2,
                tileTheme,
                (bool[,])permitted.Clone()
            );
            yield return null;
        }

        if ((this.map.theme, this.isHorizontal) != (Theme.ConflictCanyon, true))
        {
            this.solidTilesA[0].AddDecorationSpritesToBack(
                tileTheme.decorationSprites, tileTheme, permitted
            );
            yield return null;
            this.solidTilesA[0].AddDecorationSpritesToSolid(tileTheme);
            yield return null;
        }

        if (tileTheme == Assets.instance.rainyRuins?.tileTheme)
        {
            this.solidTilesA[0].AddLargeFoliage();
            yield return null;
        }

        if (tileTheme == Assets.instance.gravityGalaxy?.tileThemeInterior)
        {
            this.solidTilesA[0].AddSpaceInteriorBackWall(tileTheme);
            yield return null;
        }

        if (tileTheme == Assets.instance.treasureMines?.tileTheme)
        {
            this.solidTilesA[0].AddDecorationSpritesToSecondary(tileTheme);
            yield return null;
        }

        if (tileTheme == Assets.instance.windySkies?.tileTheme)
        {
            this.solidTilesA[0].AddWindySkiesDecorations(tileTheme, permitted);
            yield return null;
        }

        this.hasFinishedInitDecoration = true;
    }

    private void ProcessMarkers(
        NitromeEditor.Level level,
        NitromeEditor.TileLayer tileLayer,
        out Marker entryMarker,
        out Marker exitMarker,
        out BoundaryMarkers reworkedBoundary
    )
    {
        var locations = new List<Marker>();
        XY? bottomLeftCorner = null, topRightCorner = null;

        for (var n = 0; n < tileLayer.tiles.Length; n += 1)
        {
            var xy = new XY(
                n % level.Columns(),
                n / level.Columns()
            );

            if (tileLayer.tiles[n].tile == "entry_exit_marker" ||
                tileLayer.tiles[n].tile == "entry_exit_marker_left" ||
                tileLayer.tiles[n].tile == "entry_exit_marker_center" ||
                tileLayer.tiles[n].tile == "entry_exit_marker_right")
            {
                if (tileLayer.tiles[n].offsetX == 16) xy.x += 1;

                MarkerType type;
                switch (tileLayer.tiles[n].tile)
                {
                    case "entry_exit_marker_left":   type = MarkerType.Left;   break;
                    case "entry_exit_marker_center": type = MarkerType.Center; break;
                    case "entry_exit_marker_right":  type = MarkerType.Right;  break;
                    default:                         type = MarkerType.Wide;   break;
                }

                locations.Add(new Marker { mapLocation = xy, type = type });
            }
            else if (tileLayer.tiles[n].tile == "entry_exit_marker_wall")
            {
                if (tileLayer.tiles[n].offsetX == 16) xy.x += 1;
                locations.Add(new Marker { mapLocation = xy, type = MarkerType.Wall });
            }
            else if (tileLayer.tiles[n].tile == "chunk_bottom_left_corner")
            {
                bottomLeftCorner = xy;
            }
            else if (tileLayer.tiles[n].tile == "chunk_top_right_corner")
            {
                topRightCorner = xy;
            }
        }

        // defaults
        entryMarker = new Marker
        {
            mapLocation = new XY(8, 0),
            type = MarkerType.Wide
        };
        exitMarker = new Marker
        {
            mapLocation = new XY(8, level.Rows() - 1),
            type = MarkerType.Wide
        };

        if (locations.Count >= 2)
        {
            if (locations[0].type == MarkerType.Wall &&
                locations[1].type == MarkerType.Wall &&
                locations[0].mapLocation.x > locations[1].mapLocation.x)
            {
                (locations[0], locations[1]) = (locations[1], locations[0]);
            }

            if (this.isFlippedHorizontally == true
                && locations[0].type == MarkerType.Wall
                && locations[1].type == MarkerType.Wall)
            {
                entryMarker = locations[1];
                exitMarker = locations[0];
            }
            else
            {
                entryMarker = locations[0];
                exitMarker = locations[1];
            }
        }
        else if (locations.Count == 1)
        {
            ReportError(null, "Missing entry/exit marker");
        }

        if (topRightCorner != null && bottomLeftCorner != null)
        {
            var bl = bottomLeftCorner.Value;
            var tr = topRightCorner.Value;

            if (this.isFlippedHorizontally == true)
                (tr.x, bl.x) = (bl.x, tr.x);

            reworkedBoundary = new BoundaryMarkers { bottomLeft = bl, topRight = tr };
        }
        else
        {
            if (bottomLeftCorner != null || topRightCorner != null)
                ReportError(null, "Only one boundary marker provided");

            reworkedBoundary = new BoundaryMarkers {
                bottomLeft = new XY(),
                topRight = new XY(level.Columns() - 1, level.Rows() - 1)
            };
        }
    }

    private void EnforceItemOrder()
    {
        bool Early(Item it) => it is
            MovementAnchor or
            RumbleGround   or
            Raft;

        bool Late(Item it) => it is
            MultiplayerRoom      or  // must be after multiplayer headset
            ZiplineClimber       or  // must be after zipline
            SpinBlock            or  // must be after spike
            SpinTrapCentre       or  // must be after spin-trap-part and fruit
            Scuttlebug           or  // must be after bamboo
            RhinoBug             or  // must be after bamboo
            CloudBird            or  // must be after cloudblock
            StickyBlock          or  // must be after RumbleGround and RumbleGroundBlock
            BeachBall            or  // must be after pickups
            RailDoor             or  // must be after minecarts
            CrystalBarrierLeader or  // must be after crystal barrier blocks
            TilePropellerLeader  or  // must be after tile propellers
            HauntedStatue        or  // must be after statue trigger block
            FlyingRingSequence;      // must be after chests and pickups

        var early = this.items.Where(Early).ToArray();
        var late = this.items.Where(Late).ToArray();
        this.items.RemoveAll(Early);
        this.items.RemoveAll(Late);
        this.items.InsertRange(0, early);
        this.items.AddRange(late);
    }

    public static bool CanItemPressButton(Item item) =>
        item is WalkingEnemy ||
        item is Spikey ||
        item is HelmutHelmet ||
        item is Spear ||
        item is ButtonTriggerPlatform ||
        item is WildTrunkyBone ||
        item is TrunkyBullet ||
        item is ManholeMonsterBullet ||
        item is Minecart ||
        item is HauntedStatue ||
        item is Raft ||
        item is BrainBox;

    public static bool DoesItemBlockDecoration(Item item) =>
        item is SpikeTile ||
        item is Spring ||
        item is Button ||
        item is GrabHook ||
        item is BreakableBlock ||
        item is Pipe ||
        item is CursedShrine ||
        item is StickyBlock;

    public void KillThingsPreparingForReset()
    {
        foreach (var item in this.items.ToArray())
        {
            if (item.whenChunkResets == Item.ChunkResetBehaviour.DestroyGameobject)
            {
                if (item is Enemy)
                {
                    ((Enemy)item).Kill(
                        new Enemy.KillInfo { isBecauseLevelIsResetting = true }
                    );
                }
                else
                {
                    Destroy(item.gameObject);
                    this.items.Remove(item);
                }
            }
        }
    }

    public void ResetAndRecreateItems()
    {
        var itemsToRecreate = new List<Item.Def>(this.itemDefs);

        foreach (var item in this.items)
        {
            if (item.whenChunkResets == Item.ChunkResetBehaviour.PersistAndCallReset)
            {
                itemsToRecreate.Remove(item.ItemDef);
            }
        }

        foreach (var def in itemsToRecreate)
        {
            var prefab = Assets.instance.GetPrefabByTileName(def.tile.tile);
            if (prefab == null || prefab.GetComponent<Item>() == null)
            {
                Debug.Log($"No valid item prefab for tile name '{def.tile.tile}'.");
                continue;
            }
            GameObject obj = Instantiate(prefab, this.transform);

            var item = obj.GetComponent<Item>();
            item.Init(def);
            if (item is Enemy)
                ((Enemy)item).TriggerRespawnAnimation();
            this.items.Add(item);
        }

        foreach (var item in this.items)
        {
            if (item.whenChunkResets == Item.ChunkResetBehaviour.PersistAndCallReset)
            {
                item.Reset();
            }
        }
        
        this.abBlockColour = ABBlock.Colour.BLUE;
        this.gravityDirection = this.initialGravityDirection;
    }

    public TileGrid[] TileGridsForLayer(Layer layer)
    {
        if (layer == Layer.A)
            return this.solidTilesA;
        else
            return this.solidTilesB;
    }

    public float CenterX() => (this.xMin + this.xMax + 1) * 0.5f;
    public float CenterY() => (this.yMin + this.yMax + 1) * 0.5f;

    public float Width() => this.xMax - this.xMin;
    public float Height() => this.yMax - this.yMin;

    public Rect Rectangle() => new Rect(
        this.xMin,
        this.yMin,
        this.xMax - this.xMin + 1,
        this.yMax - this.yMin + 1
    );

    public bool IsLocationValid(int mapTx, int mapTy) =>
        mapTx >= this.xMin &&
        mapTx <= this.xMax &&
        mapTy >= this.yMin &&
        mapTy <= this.yMax;

    public Tile TileAt(int mapTx, int mapTy, Layer layer)
    {
        var tileLayer = (layer == Layer.A) ? this.solidTilesA[0] : this.solidTilesB[0];
        return tileLayer.GetTile(mapTx - tileLayer.xMin, mapTy - tileLayer.yMin);
    }

    public NitromeEditor.TileLayer.Tile? TriggerAt(int mapTx, int mapTy) =>
        this.triggerLayer.tiles[
            this.levelData.GetLocationIndex(mapTx - this.xMin, mapTy - this.yMin)
        ];

    public Item ItemAt(int mapTx, int mapTy)
    {
        if (IsLocationValid(mapTx, mapTy) == false) return null;
        return
            this.itemsByGridCell1[mapTx - this.xMin, mapTy - this.yMin] ??
            this.itemsByGridCell2[mapTx - this.xMin, mapTy - this.yMin];
    }

    public T ItemAt<T>(int mapTx, int mapTy) where T : Item
    {
        if (IsLocationValid(mapTx, mapTy) == false) return null;
        return
            this.itemsByGridCell1[mapTx - this.xMin, mapTy - this.yMin] as T ??
            this.itemsByGridCell2[mapTx - this.xMin, mapTy - this.yMin] as T;
    }

    public Portal PortalAt(int mapTx, int mapTy)
    {
        if (IsLocationValid(mapTx, mapTy) == false) return null;
        return this.portalsByGridCell[mapTx - this.xMin, mapTy - this.yMin];
    }

    public GravityArea GravityAreaAt(int mapTx, int mapTy)
    {
        if (IsLocationValid(mapTx, mapTy) == false) return null;
        return this.gravityAreasByGridCell[mapTx - this.xMin, mapTy - this.yMin];
    }

    public CloudBlock CloudBlockAt(int mapTx, int mapTy)
    {
        if (IsLocationValid(mapTx, mapTy) == false) return null;
        return this.cloudBlockByGridCell[mapTx - this.xMin, mapTy - this.yMin];
    }

    public NitromeEditor.TileLayer.Tile? MarkerAt(int mapTx, int mapTy)
    {
        return this.markerLayer.tiles[
            this.levelData.GetLocationIndex(mapTx - this.xMin, mapTy - this.yMin)
        ];
    }

    public Tile TileAt(Vector2 mapPosition, Layer layer)
    {
        var tileGrids = (layer == Layer.A) ? this.solidTilesA : this.solidTilesB;

        foreach (var grid in tileGrids)
        {
            Vector2 point;
            point.x = mapPosition.x - grid.xMin - grid.offset.x;
            point.y = mapPosition.y - grid.yMin - grid.offset.y;

            var tx = Mathf.FloorToInt(point.x);
            var ty = Mathf.FloorToInt(point.y);
            Tile tile = grid.GetTile(tx, ty);
            if (tile == null) continue;
            if (tile.IsSolidAtPoint(point.x - tile.gridTx, point.y - tile.gridTy))
                return tile;
        }

        return null;
    }

    public T NearestItemTo<T>(
        Vector2 position,
        float maxDistance = float.PositiveInfinity
    ) where T : Item
    {
        T result = null;
        float maxSqDistance = maxDistance * maxDistance;

        foreach (var item in this.items)
        {
            if ((item is T) == false) continue;

            var dx = item.position.x - position.x;
            var dy = item.position.y - position.y;
            var sqDist = dx * dx + dy * dy;
            if (sqDist < maxSqDistance)
            {
                maxSqDistance = sqDist;
                result = (T)item;
            }
        }

        return result;
    }

    public void AdvanceItemsBySubsteps()
    {
        if (this.map.player.ShouldPerformSubstepping() == false)
            return;

        var anchors = this.items.OfType<MovementAnchor>().ToArray();
        var brainboxes = this.items.OfType<BrainBox>().ToArray();
        if (anchors.Length == 0 && brainboxes.Length == 0)
            return;

        for (var n = 1; n < 5; n += 1)
        {
            foreach (var a in anchors) a.AdvanceSubstep(n * 0.2f);
            foreach (var b in brainboxes) b.ApplyMovement(0.2f);
            this.map.player.CheckAllSensors();
        }
    }

    private bool ItemHasSolidHitbox(Item item)
    {
        foreach (var h in item.hitboxes)
        {
            if (h.solidXMin == true) return true;
            if (h.solidXMax == true) return true;
            if (h.solidYMin == true) return true;
            if (h.solidYMax == true) return true;
        }
        return false;
    }

    private void RefreshItemSubsets()
    {
        this.subsetOfItemsRaycastCanHit.Clear();
        this.subsetOfItemsOfTypeEnemy.Clear();
        this.subsetOfItemsOfTypeWalkingEnemy.Clear();

        this.subsetOfItemsOfTypeAppleBoulder.Clear();
        this.subsetOfItemsOfTypeAshBlock.Clear();
        this.subsetOfItemsOfTypeBeachBall.Clear();
        this.subsetOfItemsOfTypeBreakableBlock.Clear();
        this.subsetOfItemsOfTypeCCTVGateBlock.Clear();
        this.subsetOfItemsOfTypeChest.Clear();
        this.subsetOfItemsOfTypeCloudBird.Clear();
        this.subsetOfItemsOfTypeCloudBlock.Clear();
        this.subsetOfItemsOfTypeDandelion.Clear();
        this.subsetOfItemsOfTypeGravityAtmosphere.Clear();
        this.subsetOfItemsOfTypeHauntedStatue.Clear();
        this.subsetOfItemsOfTypeLightUpBlock.Clear();
        this.subsetOfItemsOfTypeMinecart.Clear();
        this.subsetOfItemsOfTypeRaft.Clear();
        this.subsetOfItemsOfTypeRaftMotor.Clear();
        this.subsetOfItemsOfTypeRaftMotorBoth.Clear();
        this.subsetOfItemsOfTypeRandomBox.Clear();
        this.subsetOfItemsOfTypeRail.Clear();
        this.subsetOfItemsOfTypeRailDoor.Clear();
        this.subsetOfItemsOfTypeRumbleGround.Clear();
        this.subsetOfItemsOfTypeSeaMine.Clear();
        this.subsetOfItemsOfTypeSnowBlock.Clear();
        this.subsetOfItemsOfTypeTrafficCone.Clear();
        this.subsetOfItemsOfTypeTNTBlock.Clear();

        for (int n = 0; n < this.items.Count; n += 1)
        {
            var item = this.items[n];
            if (ItemHasSolidHitbox(item))
                this.subsetOfItemsRaycastCanHit.Add(item);

            if (item is Enemy e)
            {
                this.subsetOfItemsOfTypeEnemy.Add(e);
                if (item is WalkingEnemy we)
                    this.subsetOfItemsOfTypeWalkingEnemy.Add(we);
            }

            switch (item)
            {
                case AppleBoulder appleBoulder:
                    this.subsetOfItemsOfTypeAppleBoulder.Add(appleBoulder);
                    break;
                case AshBlock ashBlock:
                    this.subsetOfItemsOfTypeAshBlock.Add(ashBlock);
                    break;
                case BeachBall beachBall:
                    this.subsetOfItemsOfTypeBeachBall.Add(beachBall);
                    break;
                case BreakableBlock breakableBlock:
                    this.subsetOfItemsOfTypeBreakableBlock.Add(breakableBlock);
                    break;
                case Chest chest:
                    this.subsetOfItemsOfTypeChest.Add(chest);
                    break;
                case CCTVGateBlock cctvGateBlock:
                    this.subsetOfItemsOfTypeCCTVGateBlock.Add(cctvGateBlock);
                    break;
                case CloudBird cloudBird:
                    this.subsetOfItemsOfTypeCloudBird.Add(cloudBird);
                    break;
                case CloudBlock cloudBlock:
                    this.subsetOfItemsOfTypeCloudBlock.Add(cloudBlock);
                    break;
                case Dandelion dandelion:
                    this.subsetOfItemsOfTypeDandelion.Add(dandelion);
                    break;
                case GravityAtmosphere gravityAtmosphere:
                    this.subsetOfItemsOfTypeGravityAtmosphere.Add(gravityAtmosphere);
                    break;
                case HauntedStatue hauntedStatue:
                    this.subsetOfItemsOfTypeHauntedStatue.Add(hauntedStatue);
                    break;
                case LightUpBlock lightUpBlock:
                    this.subsetOfItemsOfTypeLightUpBlock.Add(lightUpBlock);
                    break;
                case Minecart minecart:
                    this.subsetOfItemsOfTypeMinecart.Add(minecart);
                    break;
                case Raft raft:
                    this.subsetOfItemsOfTypeRaft.Add(raft);
                    break;
                case RaftMotor raftMotor:
                    this.subsetOfItemsOfTypeRaftMotor.Add(raftMotor);
                    break;
                case RaftMotorBoth raftMotorBoth:
                    this.subsetOfItemsOfTypeRaftMotorBoth.Add(raftMotorBoth);
                    break;
                case RandomBox randomBox:
                    this.subsetOfItemsOfTypeRandomBox.Add(randomBox);
                    break;
                case Rail rail:
                    this.subsetOfItemsOfTypeRail.Add(rail);
                    break;
                case RailDoor railDoor:
                    this.subsetOfItemsOfTypeRailDoor.Add(railDoor);
                    break;
                case RumbleGround rumbleGround:
                    this.subsetOfItemsOfTypeRumbleGround.Add(rumbleGround);
                    break;
                case SeaMine seaMine:
                    this.subsetOfItemsOfTypeSeaMine.Add(seaMine);
                    break;
                case SnowBlock snowBlock:
                    this.subsetOfItemsOfTypeSnowBlock.Add(snowBlock);
                    break;
                case TrafficCone trafficCone:
                    this.subsetOfItemsOfTypeTrafficCone.Add(trafficCone);
                    break;
                case TNTBlock tNTBlock:
                    this.subsetOfItemsOfTypeTNTBlock.Add(tNTBlock);
                    break;
            };
        }
    }

    public void AdvanceItems()
    {
        if (this.timeToRemainActive > 0)
            this.timeToRemainActive -= 1;

        RefreshItemSubsets();

        for (int n = 0; n < this.items.Count; n += 1)
        {
            var i = this.items[n];
            ProfilerMarkersForItems.Begin(i);
            i.Advance();
            ProfilerMarkersForItems.End(i);
            i.RefreshHitboxes();
        }

        if (this.map.theme == Theme.ConflictCanyon)
        {
            Sand.AdvanceSand(this);
        }

        foreach (var g in this.groupLayer.allGroups)
        {
            if (g.followerTileGrid == null) continue;
            if (g.leader == null) continue;

            var position = new Vector2(
                g.leader.position.x - g.leaderHome.x,
                g.leader.position.y - g.leaderHome.y
            );
            g.followerTileGrid.effectiveVelocity =
                position - g.followerTileGrid.offsetLastFrame;
            g.followerTileGrid.offset = position;
            g.followerTileGrid.offsetLastFrame = position;
            g.followerTileGrid.UpdateTilePositions();
        }
    }

    public void ReportError(Vector2? worldPosition, string message)
    {
        Debug.LogWarning($"Error on chunk #{this.index + 1}: {message}");
        this.chunkDebugView?.Error(worldPosition, message);
    }

    public void RefreshDebugUIEnabled()
    {
        this.chunkDebugView.gameObject.SetActive(DebugPanel.chunkNamesEnabled);
    }

    public T AnyItem<T>() where T : Item
    {
        foreach(var i in items)
        {
            if(i is T) return i as T;
        }

        return null;
    }
    public void UpdateEndingChunk(XY updatedEntry)
    {
        Marker entryLocal, exitLocal;

        ProcessMarkers(
            levelData,
            markerLayer,
            out entryLocal,
            out exitLocal,
            out this.boundaryMarkers
        );
        int previousY = yMin;
        this.xMin = updatedEntry.x - entryLocal.mapLocation.x + boundaryMarkers.bottomLeft.x;
        this.yMin = updatedEntry.y - entryLocal.mapLocation.y + boundaryMarkers.bottomLeft.y;
        this.xMax = this.xMin + boundaryMarkers.topRight.x - boundaryMarkers.bottomLeft.x;
        this.yMax = this.yMin + boundaryMarkers.topRight.y - boundaryMarkers.bottomLeft.y;
        this.transform.position = new Vector3(
            this.xMin * Tile.Size,
            this.yMin * Tile.Size
        );

        this.entry = entryLocal;
        this.entry.mapLocation.x += this.xMin - boundaryMarkers.bottomLeft.x;
        this.entry.mapLocation.y += this.yMin - boundaryMarkers.bottomLeft.y;
        this.exit = exitLocal;
        this.exit.mapLocation.x += this.xMin - boundaryMarkers.bottomLeft.x;
        this.exit.mapLocation.y += this.yMin - boundaryMarkers.bottomLeft.y;

        foreach (var item in this.items.ToArray())
        {
            Destroy(item.gameObject);
            this.items.Remove(item);
        }

        var itemsToRecreate = new List<Item.Def>(this.itemDefs);
        List<Item.Def> itemDefsUpdated = new List<Item.Def>();
        foreach (var def in itemsToRecreate)
        {
            var defNew = new Item.Def(def.tx, def.ty + (yMin - previousY), def.tile, this);
            itemDefsUpdated.Add(defNew);
        }

        foreach (var def in itemDefsUpdated)
        {
            var prefab = Assets.instance.GetPrefabByTileName(def.tile.tile);
            if (prefab == null || prefab.GetComponent<Item>() == null)
            {
                Debug.Log($"No valid item prefab for tile name '{def.tile.tile}'.");
                continue;
            }
            GameObject obj = Instantiate(prefab, this.transform);

            var item = obj.GetComponent<Item>();
            item.Init(def);
            this.items.Add(item);
        }
        itemDefs = itemDefsUpdated;
        solidTilesA[0].yMin = yMin;
        solidTilesB[0].yMin = yMin;
        solidTilesA[0].yMax = yMax;
        solidTilesB[0].yMax = yMax;
    }

    public bool IsPlayerInContactWithAnyAshBlock()
    {
        foreach (var item in this.items.OfType<AshBlock>().ToArray())
        {
            AshBlock ashBlock = item as AshBlock;
            if (ashBlock.IsBlockActive == false) continue;

            for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
            {
                /*var rectItem = item.hitboxes[hi].InWorldSpace();
                var rectPlayer = this.map.player.Rectangle().Inflate(0.20f);

                if (rectItem.Overlaps(rectPlayer))
                {
                    return true;
                }*/

                if(ashBlock.IsPlayerInContact == true)
                {
                    return true;
                }
            }
        }

        return false;
    }
}

public static class ProfilerMarkersForItems
{
    private static Dictionary<System.Type, Unity.Profiling.ProfilerMarker> markers = new();

    public static void Begin(Item i)
    {
        var type = i.GetType();
        if (markers.TryGetValue(type, out var marker) == false)
            markers[type] = marker = new Unity.Profiling.ProfilerMarker($"{type} Advance");
        marker.Begin();
    }

    public static void End(Item i)
    {
        markers[i.GetType()].End();
    }
}
