
using UnityEngine;
using System.Collections.Generic;

public class ChunkDebugView : MonoBehaviour
{
    private Chunk chunk;
    private bool initialized = false;
    private List<(Vector2?, string)> errorsBeforeInit;

    public void Init(Chunk chunk)
    {
        this.chunk = chunk;

        var chunkName = chunk.chunkName;
        chunkName = chunkName.Replace("Technical/", "");
        chunkName = chunkName.Replace(
            "level from editor", "<color=yellow><b>level from editor</b></color>"
        );

        var textField = this.transform.Find("Text").GetComponent<TMPro.TMP_Text>();
        var text = $"<color=#c0c0c0><b>#{chunk.index + 1}</b></color> {chunkName}";
        if (chunk.isFlippedHorizontally)
            text += " <color=#c0c0c0>(flipped)</color>";

        this.gameObject.name = $"Chunk Name Display";
        this.transform.SetParent(chunk.transform);
        this.transform.position = new Vector3(chunk.xMin, chunk.yMin, 0);
        textField.text = text;

        int columns = chunk.xMax - chunk.xMin + 1;
        this.transform.Find("Bg").transform.localScale = new Vector3(
            columns / 16.0f, 1, 1
        );
        var dots = this.transform.Find("Dots");
        for (int n = 1; n < columns; n += 1)
        {
            var copy = Instantiate(dots.gameObject, dots.parent);
            copy.transform.localPosition = new Vector3(n, 0, 0);
        }

        this.initialized = true;
        if (this.errorsBeforeInit != null)
        {
            foreach (var e in this.errorsBeforeInit)
                Error(e.Item1, e.Item2);
            this.errorsBeforeInit = null;
        }

        if (this.chunk.index > 0)
        {
            Chunk.MarkerType Flip(Chunk.MarkerType t)
            {
                switch (t)
                {
                    case Chunk.MarkerType.Left: return Chunk.MarkerType.Right;
                    case Chunk.MarkerType.Right: return Chunk.MarkerType.Left;
                    default: return t;
                }
            }

            var markerType =
                this.chunk.isFlippedHorizontally ?
                Flip(this.chunk.entry.type) :
                this.chunk.entry.type;

            if (markerType == Chunk.MarkerType.Wide)
            {
                var lastChunk = this.chunk.map.chunks[this.chunk.index - 1];
                markerType =
                    lastChunk.isFlippedHorizontally ?
                    Flip(lastChunk.exit.type) :
                    lastChunk.exit.type;
            }

            Vector2 position = new Vector2(
                this.chunk.entry.mapLocation.x,
                this.chunk.entry.mapLocation.y
            );
            switch (markerType)
            {
                case Chunk.MarkerType.Left: {
                    var marker = this.transform.Find("Entry Thin");
                    marker.gameObject.SetActive(true);
                    marker.position = position.Add(-4, -0.5f);
                    break;
                }
                case Chunk.MarkerType.Center: {
                    var marker = this.transform.Find("Entry Thin");
                    marker.gameObject.SetActive(true);
                    marker.position = position.Add(0, -0.5f);
                    break;
                }
                case Chunk.MarkerType.Right: {
                    var marker = this.transform.Find("Entry Thin");
                    marker.gameObject.SetActive(true);
                    marker.position = position.Add(4, -0.5f);
                    break;
                }
                case Chunk.MarkerType.Wide: {
                    var marker = this.transform.Find("Entry Wide");
                    marker.gameObject.SetActive(true);
                    marker.position = position.Add(0, -0.5f);
                    break;
                }
                case Chunk.MarkerType.Wall: {
                    var marker = this.transform.Find("Entry Wall");
                    marker.gameObject.SetActive(true);
                    if (this.chunk.isFlippedHorizontally == true)
                    {
                        marker.position = position.Add(0.5f, 0);
                        marker.localScale = new Vector3(-1, 1, 1);
                    }
                    else
                    {
                        marker.position = position.Add(-0.5f, 0);
                    }
                    break;
                }
            }
        }
    }

    public void Error(Vector2? worldPosition, string message)
    {
        if (this.initialized == false)
        {
            var e = (worldPosition, message);
            if (this.errorsBeforeInit == null)
                this.errorsBeforeInit = new List<(Vector2?, string)> { e };
            else
                this.errorsBeforeInit.Add(e);
            return;
        }

        var excl = this.transform.Find("Exclamation");
        if (excl.gameObject.activeSelf == true)
            excl = Instantiate(excl.gameObject, excl.transform.parent).transform;
        excl.gameObject.SetActive(true);
        excl.transform.position =
            worldPosition ?? new Vector2(this.chunk.CenterX(), this.chunk.yMin + 3);

        var textField = this.transform.Find("Text").GetComponent<TMPro.TMP_Text>();
        textField.text += $"\n<color=orange><b>{message}</b></color>";
        textField.ForceMeshUpdate();
        var height = textField.GetPreferredValues().y;

        var bg = this.transform.Find("Bg");
        var sy = bg.transform.localScale.y;
        bg.transform.localScale = new Vector3(1, sy + 0.63f, 1);
    }
}
