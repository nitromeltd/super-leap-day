using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Leap Day 2/Chunk Library")]
public class ChunkLibrary : ScriptableObject
{
    [Serializable]
    public class ChunkData
    {
        public string filename;
        [NonSerialized] public NitromeEditor.Level level;
        public Chunk.MarkerType entryType;
        public Chunk.MarkerType exitType;
        public bool isHorizontal;
        public bool isZeroGravity;
        public bool containsAreaOfInterest;
    }

    public ChunkData[] checkpoint;
    public ChunkData[] chest;
    public ChunkData[] blenderVendingMachine;
    public ChunkData[] genericTutorial;
    public ChunkData[] genericEasy;
    public ChunkData[] genericMedium;
    public ChunkData[] genericHard;
    public ChunkData[] rainyRuinsTutorial;
    public ChunkData[] rainyRuinsEasy;
    public ChunkData[] rainyRuinsMedium;
    public ChunkData[] rainyRuinsHard;
    public ChunkData[] capitalHighwayTutorial;
    public ChunkData[] capitalHighwayEasy;
    public ChunkData[] capitalHighwayMedium;
    public ChunkData[] capitalHighwayHard;
    public ChunkData[] conflictCanyonTutorial;
    public ChunkData[] conflictCanyonEasy;
    public ChunkData[] conflictCanyonMedium;
    public ChunkData[] conflictCanyonHard;
    public ChunkData[] hotNColdSpringsTutorial;
    public ChunkData[] hotNColdSpringsEasy;
    public ChunkData[] hotNColdSpringsMedium;
    public ChunkData[] hotNColdSpringsHard;
    public ChunkData[] gravityGalaxyTutorial;
    public ChunkData[] gravityGalaxyEasy;
    public ChunkData[] gravityGalaxyMedium;
    public ChunkData[] gravityGalaxyHard;
    public ChunkData[] sunkenIslandTutorial;
    public ChunkData[] sunkenIslandEasy;
    public ChunkData[] sunkenIslandMedium;
    public ChunkData[] sunkenIslandHard;
    public ChunkData[] treasureMinesTutorial;
    public ChunkData[] treasureMinesEasy;
    public ChunkData[] treasureMinesMedium;
    public ChunkData[] treasureMinesHard;
    public ChunkData[] windySkiesTutorial;
    public ChunkData[] windySkiesEasy;
    public ChunkData[] windySkiesMedium;
    public ChunkData[] windySkiesHard;
    public ChunkData[] tombstoneHillTutorial;
    public ChunkData[] tombstoneHillEasy;
    public ChunkData[] tombstoneHillMedium;
    public ChunkData[] tombstoneHillHard;
    public ChunkData[] moltenFortressTutorial;
    public ChunkData[] moltenFortressEasy;
    public ChunkData[] moltenFortressMedium;
    public ChunkData[] moltenFortressHard;
    public string[] deprecatedFiles;

    [Serializable]
    public class ReviewPack
    {
        [Serializable]
        public class ChunkEntry
        {
            public string filename;
            public string md5;
        }
        public string name;
        public bool isGeneric;
        public Theme specificToTheme;
        public ChunkEntry[] chunks;
    }
    public ReviewPack.ChunkEntry[] chunksReviewed;
    public ReviewPack[] reviewPacks;
    public int nextReviewPackNumber;

    public ChunkData[] GetChunks(
        Theme? theme, ChunkDifficulty difficulty
    ) => (theme, difficulty) switch
    {
        (null, ChunkDifficulty.Tutorial) => this.genericTutorial,
        (null, ChunkDifficulty.Easy)     => this.genericEasy,
        (null, ChunkDifficulty.Medium)   => this.genericMedium,
        (null, ChunkDifficulty.Hard)     => this.genericHard,

        (Theme.RainyRuins, ChunkDifficulty.Tutorial) => this.rainyRuinsTutorial,
        (Theme.RainyRuins, ChunkDifficulty.Easy)     => this.rainyRuinsEasy,
        (Theme.RainyRuins, ChunkDifficulty.Medium)   => this.rainyRuinsMedium,
        (Theme.RainyRuins, ChunkDifficulty.Hard)     => this.rainyRuinsHard,

        (Theme.CapitalHighway, ChunkDifficulty.Tutorial) => this.capitalHighwayTutorial,
        (Theme.CapitalHighway, ChunkDifficulty.Easy)     => this.capitalHighwayEasy,
        (Theme.CapitalHighway, ChunkDifficulty.Medium)   => this.capitalHighwayMedium,
        (Theme.CapitalHighway, ChunkDifficulty.Hard)     => this.capitalHighwayHard,

        (Theme.ConflictCanyon, ChunkDifficulty.Tutorial) => this.conflictCanyonTutorial,
        (Theme.ConflictCanyon, ChunkDifficulty.Easy)     => this.conflictCanyonEasy,
        (Theme.ConflictCanyon, ChunkDifficulty.Medium)   => this.conflictCanyonMedium,
        (Theme.ConflictCanyon, ChunkDifficulty.Hard)     => this.conflictCanyonHard,

        (Theme.HotNColdSprings, ChunkDifficulty.Tutorial) => this.hotNColdSpringsTutorial,
        (Theme.HotNColdSprings, ChunkDifficulty.Easy)     => this.hotNColdSpringsEasy,
        (Theme.HotNColdSprings, ChunkDifficulty.Medium)   => this.hotNColdSpringsMedium,
        (Theme.HotNColdSprings, ChunkDifficulty.Hard)     => this.hotNColdSpringsHard,

        (Theme.GravityGalaxy, ChunkDifficulty.Tutorial) => this.gravityGalaxyTutorial,
        (Theme.GravityGalaxy, ChunkDifficulty.Easy)     => this.gravityGalaxyEasy,
        (Theme.GravityGalaxy, ChunkDifficulty.Medium)   => this.gravityGalaxyMedium,
        (Theme.GravityGalaxy, ChunkDifficulty.Hard)     => this.gravityGalaxyHard,

        (Theme.SunkenIsland, ChunkDifficulty.Tutorial) => this.sunkenIslandTutorial,
        (Theme.SunkenIsland, ChunkDifficulty.Easy)     => this.sunkenIslandEasy,
        (Theme.SunkenIsland, ChunkDifficulty.Medium)   => this.sunkenIslandMedium,
        (Theme.SunkenIsland, ChunkDifficulty.Hard)     => this.sunkenIslandHard,

        (Theme.TreasureMines, ChunkDifficulty.Tutorial) => this.treasureMinesTutorial,
        (Theme.TreasureMines, ChunkDifficulty.Easy)     => this.treasureMinesEasy,
        (Theme.TreasureMines, ChunkDifficulty.Medium)   => this.treasureMinesMedium,
        (Theme.TreasureMines, ChunkDifficulty.Hard)     => this.treasureMinesHard,

        (Theme.WindySkies, ChunkDifficulty.Tutorial) => this.windySkiesTutorial,
        (Theme.WindySkies, ChunkDifficulty.Easy)     => this.windySkiesEasy,
        (Theme.WindySkies, ChunkDifficulty.Medium)   => this.windySkiesMedium,
        (Theme.WindySkies, ChunkDifficulty.Hard)     => this.windySkiesHard,

        (Theme.TombstoneHill, ChunkDifficulty.Tutorial) => this.tombstoneHillTutorial,
        (Theme.TombstoneHill, ChunkDifficulty.Easy)     => this.tombstoneHillEasy,
        (Theme.TombstoneHill, ChunkDifficulty.Medium)   => this.tombstoneHillMedium,
        (Theme.TombstoneHill, ChunkDifficulty.Hard)     => this.tombstoneHillHard,

        (Theme.MoltenFortress, ChunkDifficulty.Tutorial) => this.moltenFortressTutorial,
        (Theme.MoltenFortress, ChunkDifficulty.Easy)     => this.moltenFortressEasy,
        (Theme.MoltenFortress, ChunkDifficulty.Medium)   => this.moltenFortressMedium,
        (Theme.MoltenFortress, ChunkDifficulty.Hard)     => this.moltenFortressHard,

        _ => new ChunkData[0]
    };

    public static void FillChunkData(ChunkData data, NitromeEditor.Level level)
    {
        data.entryType = Chunk.MarkerType.Wide;
        data.exitType = Chunk.MarkerType.Wide;
        data.isHorizontal = false;
        data.isZeroGravity = false;
        data.containsAreaOfInterest = false;

        var objectLayers = new [] {
            level.TileLayerByName("objects"),
            level.TileLayerByName("objects2")
        };
        foreach (var layer in objectLayers)
        {
            foreach (var cell in layer.tiles)
            {
                if (cell.tile == null) continue;
                if (cell.tile.StartsWith("area_of_interest"))
                    data.containsAreaOfInterest = true;
                if (cell.tile == "gravity_none_on_start")
                    data.isZeroGravity = true;
            }
        }

        var markerLayer =
            level.TileLayerByName("markers") ??
            level.TileLayerByName("chunk_separators");
        for (int n = 0; n < markerLayer.tiles.Length; n += 1)
        {
            var cell = markerLayer.tiles[n];
            if (cell.tile == null) continue;
            if (cell.tile.StartsWith("entry_exit_marker") == false) continue;

            var xy = new XY(n % level.Columns(), n / level.Columns());
            if (cell.tile == "entry_exit_marker_wall")
            {
                if (xy.x >= level.Columns() / 2)
                    data.exitType = Chunk.MarkerType.Wall;
                else
                    data.entryType = Chunk.MarkerType.Wall;
            }
            else
            {
                bool isExit = (xy.y > 0);

                Chunk.MarkerType type = Chunk.MarkerType.Wide;
                if (cell.tile == "entry_exit_marker_left"  ) type = Chunk.MarkerType.Left;
                if (cell.tile == "entry_exit_marker_center") type = Chunk.MarkerType.Center;
                if (cell.tile == "entry_exit_marker_right" ) type = Chunk.MarkerType.Right;

                if (xy.y == 0)
                    data.entryType = type;
                else
                    data.exitType = type;
            }
        }

        int columns = 1 + level.maxX - level.minX;
        data.isHorizontal =
            (data.entryType == Chunk.MarkerType.Wall) &&
            (data.exitType == Chunk.MarkerType.Wall || columns != 16);
    }
}
