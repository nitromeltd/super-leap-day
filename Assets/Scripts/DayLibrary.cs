using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Leap Day 2/Day Library")]
public class DayLibrary : ScriptableObject
{
    [Serializable]
    public class Day
    {
        public string name;
        public int day;
        public int month;
        public int year;
        public Theme theme;
        public RowForDay[] rows;
    }

    [Serializable]
    public class RowForDay
    {
        public string filename;
        public LevelGeneration.RowType type;
        public bool fillAreaOfInterestWithCheckpoint;
        public bool fillAreaOfInterestWithBonusLift;
    }

    public Day[] days;
    public Theme[] orderOfThemes;
}
