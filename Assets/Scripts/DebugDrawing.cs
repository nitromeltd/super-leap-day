
using UnityEngine;
using System.Collections.Generic;

public class DebugDrawing : MonoBehaviour
{
    private static List<Raycast.Result> raycastsThisFrame;
    private static MeshFilter drawingMeshFilter;

    public static bool enableLoggingRaycasts = true;
    public static List<Raycast.Result> RaycastsThisFrame { get => raycastsThisFrame; }

    public static void Clear()
    {
        if (drawingMeshFilter != null)
        {
            Destroy(drawingMeshFilter.gameObject);
            drawingMeshFilter = null;
        }
    }

    public static void Draw(
        Map map,
        bool raycasts = false,
        bool hitboxes = false
    )
    {
        if (raycastsThisFrame == null || drawingMeshFilter == null)
        {
            if (Assets.instance.debugDrawing != null && map.player != null)
            {
                raycastsThisFrame = new List<Raycast.Result>();

                var obj = GameObject.Instantiate(
                    Assets.instance.debugDrawing,
                    map.player.transform.parent
                );
                obj.name = "Debug Drawing";
                obj.layer = map.Layer();
                drawingMeshFilter = obj.GetComponent<MeshFilter>();
                obj.GetComponent<MeshRenderer>().sortingLayerName = "Debug";
            }

            return;
        }

        var vertices = new List<Vector3>();
        var colours  = new List<Color>();
        var indices  = new List<int>();

        if (raycasts == true)
        {
            foreach (var result in raycastsThisFrame)
            {
                DrawLine(
                    vertices,
                    colours,
                    indices,
                    result.start,
                    result.start + (result.direction * result.distance),
                    new Color(1, 0.5f, 0, 0.5f),
                    0
                );
            }
        }

        if (hitboxes == true)
        {
            var chunk = map.NearestChunkTo(map.player.position);
            foreach (var item in chunk.items)
            {
                if (item.hitboxes == null) break;
                foreach (var hitbox in item.hitboxes)
                {
                    var rect = hitbox.InWorldSpace();
                    var colour = new Color(1, 1, 0, 0.5f);
                    var bl = new Vector2(rect.xMin, rect.yMin);
                    var br = new Vector2(rect.xMax, rect.yMin);
                    var tl = new Vector2(rect.xMin, rect.yMax);
                    var tr = new Vector2(rect.xMax, rect.yMax);
                    if (rect.width < 0 || rect.height < 0)
                        colour.g = 0;
                    DrawLine(vertices, colours, indices, bl, br, colour, -1);
                    DrawLine(vertices, colours, indices, br, tr, colour,  1);
                    DrawLine(vertices, colours, indices, tr, tl, colour, -1);
                    DrawLine(vertices, colours, indices, tl, bl, colour,  1);
                }
            }

            {
                var rect = map.player.Rectangle();
                var colour = new Color(0, 1, 1, 0.5f);
                var bl = new Vector2(rect.xMin, rect.yMin);
                var br = new Vector2(rect.xMax, rect.yMin);
                var tl = new Vector2(rect.xMin, rect.yMax);
                var tr = new Vector2(rect.xMax, rect.yMax);
                DrawLine(vertices, colours, indices, bl, br, colour, -1);
                DrawLine(vertices, colours, indices, br, tr, colour,  1);
                DrawLine(vertices, colours, indices, tr, tl, colour, -1);
                DrawLine(vertices, colours, indices, tl, bl, colour,  1);
            }
        }

        var mesh = drawingMeshFilter.mesh ?? new Mesh();
        mesh.Clear();
        mesh.SetVertices(vertices);
        mesh.SetColors  (colours);
        mesh.SetIndices (indices.ToArray(), MeshTopology.Triangles, 0);
        drawingMeshFilter.mesh = mesh;

        raycastsThisFrame.Clear();
    }

    private static void DrawLine(
        List<Vector3> outVertices,
        List<Color>   outColours,
        List<int>     outIndices,
        Vector2       a,
        Vector2       b,
        Color         colour,
        float         capAmount
    )
    {
        var forwards = (b - a).normalized;
        var sideways = new Vector2(forwards.y, -forwards.x) * 0.1f;
        var capOffset = forwards * (0.1f * capAmount);

        int firstVertex = outVertices.Count;
        outVertices.Add(a - sideways - capOffset);
        outVertices.Add(a + sideways - capOffset);
        outVertices.Add(b - sideways + capOffset);
        outVertices.Add(b + sideways + capOffset);
        outColours.Add(colour);
        outColours.Add(colour);
        outColours.Add(colour);
        outColours.Add(colour);
        outIndices.Add(firstVertex + 0);
        outIndices.Add(firstVertex + 1);
        outIndices.Add(firstVertex + 2);
        outIndices.Add(firstVertex + 2);
        outIndices.Add(firstVertex + 1);
        outIndices.Add(firstVertex + 3);
    }
}
