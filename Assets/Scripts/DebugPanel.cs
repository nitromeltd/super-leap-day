using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

public class DebugPanel : MonoBehaviour
{
    public enum Mode
    {
        EditorRainyRuins,
        EditorCapitalHighway,
        EditorConflictCanyon,
        EditorHotNColdSprings,
        EditorGravityGalaxy,
        EditorSunkenIsland,
        EditorTreasureMines,
        EditorWindySkies,
        EditorTombstoneHill,
        EditorMoltenFortress,
        EditorVRTrainingRoom,
        EditorOpeningTutorial,
        Algorithm,
        AlgorithmRegenerate,
        Algorithm2PSplitScreen,
        GenerateGravityGalaxyLevel,
        GenerateTombstoneHillLevel,
        GenerateMoltenFortressLevel,
        AppleDemo,
        TestChunks,
        MinigameGolf,
        MinigameRacing,
        MinigamePinball,
        MinigameFishing,
        MinigameRolling,
        MinigameAbseiling,
        WindySkiesChunkExamples,
        GravityAtmosphereExamples,
        TombstoneHillChunkExamples,
        MoltenFortressExamples,
        KiavikGravityChunks,
        ChunkReviewPack // must be last
    }
    public enum UIStyle
    {
        Normal, // <-- should always be this in production
        Clean,  // <-- for recording videos.
                //       no controller hints, no feedback button, hide pause on landscape
        Hidden, // <-- hide all ui
    }
    public static Mode selectedMode = Mode.Algorithm;
    public static ChunkLibrary.ReviewPack selectedReviewPack = null;
    public static int mapNumber = 1;

    public static bool fpsEnabled = false;
    public static bool shuffleEnabled = true;
    public static bool chunkNamesEnabled = false;
    public static bool hitboxesEnabled = false;
    public static bool visibleAreaGuideEnabled = false;
    public static UIStyle uiStyle = UIStyle.Normal;
    public static bool debugPanelEnabled = false;
    public static bool userInterfaceRemoved = false;
    public static bool debugInvincibilityCheat = false;

    [NonSerialized] public int currentTabIndex;
    private int futurePlayOffset = 0;
    private System.DateTime selectedDate;

    public bool respectSafeArea = false;
    [NonSerialized] public bool isExpanded = true;
    public SelectionCursor selectionCursor;
    public RectTransform containerPanelItself;
    public RectTransform containerToggleButton;
    public RectTransform containerToggleArrowUp;
    public RectTransform containerToggleArrowDown;
    public UnityEngine.UI.Button[] tabButtons;
    public RectTransform[] tabContainers;
    public Text mapNumberText;

    [Header("Page 1 \"Level\"")]
    public Dropdown levelDropdown;
    public Text dateText;
    public Dropdown characterSelectDropdown;
    public Dropdown futurePlayDropdown;

    [Header("Page 2 \"Cheat\"")]
    public Dropdown characterUpgradeLevelDropdown;
    public Toggle invincibilityToggle;
    public Dropdown spawnItemDropdown;

    [Header("Page 3 \"Debug\"")]
    public Toggle chunkNamesToggle;
    public Toggle hitboxesToggle;
    public Toggle visibleAreaGuideToggle;
    public Toggle fpsCounterToggle;
    public Toggle shuffleToggle;
    public Dropdown uiStyleDropdown;

    [Header("Page 4 \"Info\"")]
    public ScrollRect infoScrollRect;
    public TMPro.TextMeshProUGUI infoText;

    private (Map map, int index, int frameNumber) lastSnapToChunkIndex;

    public static void EnsureEditorMode()
    {
        if (selectedMode != Mode.EditorRainyRuins &&
            selectedMode != Mode.EditorCapitalHighway &&
            selectedMode != Mode.EditorConflictCanyon &&
            selectedMode != Mode.EditorHotNColdSprings &&
            selectedMode != Mode.EditorGravityGalaxy &&
            selectedMode != Mode.EditorSunkenIsland &&
            selectedMode != Mode.EditorTreasureMines &&
            selectedMode != Mode.EditorWindySkies &&
            selectedMode != Mode.EditorTombstoneHill &&
            selectedMode != Mode.EditorMoltenFortress &&
            selectedMode != Mode.EditorVRTrainingRoom &&
            selectedMode != Mode.EditorOpeningTutorial)
        {
            selectedMode = Mode.EditorRainyRuins;
        }
    }

    public void Init()
    {
        this.selectedDate = Game.selectedDate;

        Expand();
        this.gameObject.SetActive(false);
        this.selectionCursor.gameObject.SetActive(false);

        if (this.respectSafeArea == true)
        {
            var topAnchor = new Vector2(0.5f, Screen.safeArea.max.y / Screen.height);
            var rectTransform = this.GetComponent<RectTransform>();
            rectTransform.anchorMin = topAnchor;
            rectTransform.anchorMax = topAnchor;
        }

        if (this.shuffleToggle != null)
            this.shuffleToggle.isOn = shuffleEnabled;
        if (this.fpsCounterToggle != null)
            this.fpsCounterToggle.isOn = fpsEnabled;
        if (this.chunkNamesToggle != null)
            this.chunkNamesToggle.isOn = chunkNamesEnabled;
        if (this.hitboxesToggle != null)
            this.hitboxesToggle.isOn = hitboxesEnabled;
        if (this.visibleAreaGuideToggle != null)
            this.visibleAreaGuideToggle.isOn = visibleAreaGuideEnabled;

        if (this.characterSelectDropdown != null)
        {
            this.characterSelectDropdown.value = (int)(SaveData.GetSelectedCharacter());
        }

        if (this.futurePlayDropdown != null)
        {
            futurePlayOffset = SaveData.GetFuturePlayOffset();
            this.futurePlayDropdown.value =
                this.futurePlayOffset switch { 7 => 1, 28 => 2, _ => 0 };
        }

        string DropdownText(Mode mode) => mode switch
        {
            Mode.EditorRainyRuins           => "(editor) rainy ruins",
            Mode.EditorCapitalHighway       => "(editor) capital highway",
            Mode.EditorConflictCanyon       => "(editor) conflict canyon",
            Mode.EditorHotNColdSprings      => "(editor) hot 'n' cold springs",
            Mode.EditorGravityGalaxy        => "(editor) gravity galaxy",
            Mode.EditorSunkenIsland         => "(editor) sunken island",
            Mode.EditorTreasureMines        => "(editor) treasure mines",
            Mode.EditorWindySkies           => "(editor) windy skies",
            Mode.EditorTombstoneHill        => "(editor) tombstone hill",
            Mode.EditorMoltenFortress       => "(editor) molten fortress",
            Mode.EditorVRTrainingRoom       => "(editor) vr training room",
            Mode.EditorOpeningTutorial      => "(editor) opening tutorial",
            Mode.Algorithm                  => "algorithm",
            Mode.AlgorithmRegenerate        => "algorithm (force regenerate)",
            Mode.Algorithm2PSplitScreen     => "algorithm (2p splitscreen)",
            Mode.GenerateGravityGalaxyLevel  => "generate gravity galaxy level",
            Mode.GenerateTombstoneHillLevel  => "generate tombstone hill level",
            Mode.GenerateMoltenFortressLevel => "generate molten fortress level",
            Mode.AppleDemo                   => "apple demo",
            Mode.TestChunks                  => "test chunks",
            Mode.MinigameGolf                => "minigame: golf",
            Mode.MinigameRacing              => "minigame: racing",
            Mode.MinigamePinball             => "minigame: pinball",
            Mode.MinigameFishing             => "minigame: fishing",
            Mode.MinigameRolling             => "minigame: rolling",
            Mode.MinigameAbseiling           => "minigame: abseiling",
            Mode.WindySkiesChunkExamples     => "windy skies example chunks",
            Mode.GravityAtmosphereExamples   => "atmosphere example chunks",
            Mode.TombstoneHillChunkExamples  => "tombstone hill example chunks",
            Mode.MoltenFortressExamples      => "molten fortress chunks",
            Mode.KiavikGravityChunks         => "kiavik gravity chunks",
            _                                => "???"
        };

        LevelGeneration.Init();

        var options = new List<Dropdown.OptionData>();
        foreach (Mode mode in Enum.GetValues(typeof(Mode)))
        {
            if (mode != Mode.ChunkReviewPack)
                options.Add(new Dropdown.OptionData(DropdownText(mode)));
        }
        foreach (var pack in LevelGeneration.chunkLibrary.reviewPacks)
        {
            options.Add(new Dropdown.OptionData(pack.name));
        }
        this.levelDropdown.options = options;
        this.levelDropdown.value = (int)DebugPanel.selectedMode;
        if (DebugPanel.selectedMode == Mode.ChunkReviewPack)
        {
            int index = Array.IndexOf(
                LevelGeneration.chunkLibrary.reviewPacks, DebugPanel.selectedReviewPack
            );
            if (index >= 0)
                this.levelDropdown.value += index;
        }
        UpdateDateText();

        this.characterUpgradeLevelDropdown.onValueChanged.AddListener( delegate
        {
            int overwriteValue = this.characterUpgradeLevelDropdown.value;

            if(overwriteValue == 0)
            {
                CurrentMap().player.LoadSavedCharacterUpgradeLevel();
            }
            else
            {
                CurrentMap().player.ChangeCharacterUpgradeLevel(overwriteValue);
            }
        });

        if (this.spawnItemDropdown != null)
        {
            var pickupTypes =
                (PowerupPickup.Type[])Enum.GetValues(typeof(PowerupPickup.Type));
            this.spawnItemDropdown.options = pickupTypes
                .Where(t => t != PowerupPickup.Type.Length)
                .Select(t => new Dropdown.OptionData(t.ToString()))
                .ToList();

            this.spawnItemDropdown.options.Add(new Dropdown.OptionData("Coin"));
        }

        if (this.uiStyleDropdown != null)
        {
            this.uiStyleDropdown.value = (int)uiStyle;
            OnUIStyleChanged();
        }

        for (int n = 0; n < this.tabButtons.Length; n += 1)
        {
            int index = n;
            this.tabButtons[n].onClick.AddListener(() => {
                OnClickTab(index);
            });
        }
        OnClickTab(0);
    }

    public void SelectSensibleControl()
    {
        if (this.gameObject.activeSelf == false) return;

        if (this.tabContainers.Length > 0)
        {
            var tab = this.tabContainers[this.currentTabIndex];
            var control = tab.GetComponentsInChildren<Selectable>(false).FirstOrDefault();
            if (control != null)
                this.selectionCursor.Select(control, false);
        }
        else
        {
            this.selectionCursor.Select(this.fpsCounterToggle, false);
        }
    }

    public void OnClickMapNumberButton()
    {
        mapNumber += 1;
        if (mapNumber > Game.instance.maps.Length)
            mapNumber = 1;

        this.mapNumberText.text = mapNumber.ToString();
    }

    public Map CurrentMap()
    {
        if (mapNumber > Game.instance.maps.Length)
            OnClickMapNumberButton();

        return Game.instance.maps[mapNumber - 1];
    }

    public void OnClickTab(int index)
    {
        this.currentTabIndex = index;

        for (int n = 0; n < this.tabContainers.Length; n += 1)
            this.tabContainers[n].gameObject.SetActive(n == this.currentTabIndex);

        for (int n = 0; n < this.tabButtons.Length; n += 1)
            this.tabButtons[n].interactable = (n != this.currentTabIndex);

        SelectSensibleControl();

        if (this.infoText?.gameObject.activeInHierarchy == true)
            UpdateInfoText();
    }

    public void OnClickOpenClose()
    {
        if (this.isExpanded == true)
            Contract();
        else
            Expand();
    }

    public void Expand()
    {
        this.isExpanded = true;
        this.gameObject.SetActive(true);
        this.containerPanelItself.gameObject.SetActive(true);
        this.containerToggleButton.anchorMin = new Vector2(0.5f, 0);
        this.containerToggleButton.anchorMax = new Vector2(0.5f, 0);
        this.containerToggleArrowUp.gameObject.SetActive(true);
        this.containerToggleArrowDown.gameObject.SetActive(false);
        this.containerToggleButton.GetComponent<Image>().color = new Color(0, 0, 0, 0.7f);
        this.containerToggleArrowUp.GetComponent<Image>().color = new Color(1, 1, 1, 0.7f);
        this.containerToggleArrowDown.GetComponent<Image>().color = new Color(1, 1, 1, 0.7f);
        this.selectionCursor.gameObject.SetActive(true);

        UpdateInfoText();
        SelectSensibleControl();
    }

    public void Contract()
    {
        this.isExpanded = false;

        if (Application.isEditor == true && uiStyle == UIStyle.Normal)
        {
            this.containerPanelItself.gameObject.SetActive(false);
            this.containerToggleButton.anchorMin = new Vector2(0.5f, 1);
            this.containerToggleButton.anchorMax = new Vector2(0.5f, 1);
            this.containerToggleArrowUp.gameObject.SetActive(false);
            this.containerToggleArrowDown.gameObject.SetActive(true);
        }
        else if (
            IngameUI.instance.CurrentLayout() == IngameUI.instance.layoutPortrait &&
            uiStyle == UIStyle.Hidden
        )
        {
            this.containerPanelItself.gameObject.SetActive(false);
            this.containerToggleButton.anchorMin = new Vector2(0.5f, 1);
            this.containerToggleButton.anchorMax = new Vector2(0.5f, 1);
            this.containerToggleArrowUp.gameObject.SetActive(false);
            this.containerToggleArrowDown.gameObject.SetActive(true);
            this.containerToggleButton.GetComponent<Image>().color = Color.clear;
            this.containerToggleArrowUp.GetComponent<Image>().color = Color.clear;
            this.containerToggleArrowDown.GetComponent<Image>().color = Color.clear;
        }
        else
        {
            this.gameObject.SetActive(false);
        }

        this.selectionCursor.gameObject.SetActive(false);
    }

    public void Update()
    {
        if (this.shuffleToggle != null)
            shuffleEnabled = this.shuffleToggle.isOn;
        if (this.fpsCounterToggle != null)
            fpsEnabled = this.fpsCounterToggle.isOn;
        if (this.hitboxesToggle != null)
            hitboxesEnabled = this.hitboxesToggle.isOn;
        if (this.visibleAreaGuideToggle != null)
            visibleAreaGuideEnabled = this.visibleAreaGuideToggle.isOn;

        if(this.invincibilityToggle != null && this.invincibilityToggle.isOn != debugInvincibilityCheat)
        {
            if(this.invincibilityToggle.isOn)
            {
                CurrentMap()?.player?.invincibility.Activate();
            }
            else
            {
                CurrentMap()?.player?.invincibility.End();
            }
            debugInvincibilityCheat = this.invincibilityToggle.isOn;
        }

        if (this.chunkNamesToggle != null &&
            this.chunkNamesToggle.isOn != chunkNamesEnabled)
        {
            chunkNamesEnabled = this.chunkNamesToggle.isOn;
            foreach (var chunk in CurrentMap().chunks)
                chunk.RefreshDebugUIEnabled();
        }

        if (this.futurePlayDropdown != null)
        {
            int selectedValue = this.futurePlayDropdown.value switch
            {
                1 => 7,
                2 => 28,
                _ => 0
            };
            if (selectedValue != this.futurePlayOffset)
            {
                this.futurePlayOffset = selectedValue;
                SaveData.SetFuturePlayOffset(futurePlayOffset);
            }
        }
    }

    public void StartLevel()
    {
        DebugPanel.selectedMode = (Mode)this.levelDropdown.value;

        if (DebugPanel.selectedMode.ToString().StartsWith("Algorithm") ||
            DebugPanel.selectedMode.ToString().StartsWith("Generate"))
        {
            Game.selectedDate = this.selectedDate;
        }

        SaveData.SetSelectedCharacter((Character)this.characterSelectDropdown.value);

        if (DebugPanel.selectedMode >= Mode.ChunkReviewPack)
        {
            DebugPanel.selectedMode = Mode.ChunkReviewPack;
            DebugPanel.selectedReviewPack = LevelGeneration.chunkLibrary.reviewPacks[
                this.levelDropdown.value - (int)Mode.ChunkReviewPack
            ];
        }
        else
        {
            DebugPanel.selectedReviewPack = null;
        }

        switch (DebugPanel.selectedMode)
        {
            case Mode.MinigameGolf:       SceneManager.LoadScene("Golf");       break;
            case Mode.MinigameRacing:     SceneManager.LoadScene("Racing");     break;
            case Mode.MinigamePinball:    SceneManager.LoadScene("Pinball");    break;
            case Mode.MinigameFishing:    SceneManager.LoadScene("Fishing");    break;
            case Mode.MinigameRolling:    SceneManager.LoadScene("Rolling");    break;
            case Mode.MinigameAbseiling:  SceneManager.LoadScene("Abseiling");  break;

            case Mode.Algorithm:
            case Mode.Algorithm2PSplitScreen:
            case Mode.AlgorithmRegenerate:
            case Mode.GenerateGravityGalaxyLevel:
            case Mode.GenerateTombstoneHillLevel:
            case Mode.GenerateMoltenFortressLevel:
                Transition.GoToGame();
                break;

            default:
                SceneManager.LoadScene("Game");
                break;
        }

        if (Game.instance != null)
            Game.instance.isLeaving = true;

        Time.timeScale = 1;
    }

    public void ChunkUp()
    {
        var chunk = CurrentMap().NearestChunkTo(CurrentMap().player.position);
        var index = CurrentMap().chunks.IndexOf(chunk);

        if (chunk.map == lastSnapToChunkIndex.map &&
            chunk.map.frameNumber == lastSnapToChunkIndex.frameNumber)
        {
            index = lastSnapToChunkIndex.index;
        }

        index += 1;
        if (index >= CurrentMap().chunks.Count) return;

        SnapToChunkIndex(index);
    }

    public void ChunkDown()
    {
        var chunk = CurrentMap().NearestChunkTo(CurrentMap().player.position);
        var index = CurrentMap().chunks.IndexOf(chunk);

        if (chunk.map == lastSnapToChunkIndex.map &&
            chunk.map.frameNumber == lastSnapToChunkIndex.frameNumber)
        {
            index = lastSnapToChunkIndex.index;
        }

        index -= 1;
        if (index < 0) return;

        SnapToChunkIndex(index);
    }

    public void OnPressReset()
    {
        SnapToChunkIndex(0);
        SaveData.ClearLatestAttemptForDate(Game.selectedDate);
    }

    public void OnMultiplayer()
    {
        #if UNITY_IOS
        {
            GameCenterMultiplayer.PresentMatchmaker();
        }
        #else
        {
            Game.instance.ChangeNumberOfMaps(4);
        }
        #endif
    }

    public void OnSpawnButton()
    {
        var pickupType = (PowerupPickup.Type)this.spawnItemDropdown.value;
        if (pickupType == PowerupPickup.Type.None)
            return;

        var position = CurrentMap().player.position.Add(0, 2);
        var chunk = CurrentMap().NearestChunkTo(position);

        if (this.spawnItemDropdown.captionText.text.ToLower() == "coin")
        {
            Coin coin = Coin.CreateSilver(position, chunk);
            coin.movingFreely = false;
        }
        else if (pickupType.ToString().ToLower().Contains("bubble") == true)
            BubblePickup.Create(position, chunk, pickupType);
        else
            PowerupPickup.Create(position, chunk, pickupType);

        Contract();
    }

    public void Add10Coins()
    {
        if (CurrentMap() != null)
        {
            CurrentMap().coinsCollected += 10;
            CurrentMap().SaveCoins();
            IngameUI.instance.CurrentLayout().UpdateFruitAndCoinCounters(CurrentMap());
        }
    }

    public void Add100Fruit()
    {
        if (CurrentMap() != null)
        {
            CurrentMap().fruitCollected += 100;
            CurrentMap().fruitCurrent += 100;
            IngameUI.instance.CurrentLayout().UpdateFruitAndCoinCounters(CurrentMap());
        }
    }

    private void SnapToChunkIndex(int index)
    {
        var map = CurrentMap();
        var chunk = map.chunks[index];
        if (chunk == null) return;

        int factor = chunk.isFlippedHorizontally == true ? -1 : 1;
        Vector2 entry = new Vector2(chunk.entry.mapLocation.x, chunk.entry.mapLocation.y);
        Vector2 pos = chunk.entry.type switch
        {
            Chunk.MarkerType.Left   => entry.Add(-4 * factor, 1.5f),
            Chunk.MarkerType.Right  => entry.Add( 4 * factor, 1.5f),
            Chunk.MarkerType.Center => entry.Add(          0, 1.5f),
            Chunk.MarkerType.Wide   => entry.Add(          0, 1.5f),
            Chunk.MarkerType.Wall   => entry.Add( 1 * factor,    0),
            _                       => entry
        };

        map.player.position = pos;
        map.player.velocity = Vector2.zero;
        map.player.facingRight = true;
        map.player.state = Player.State.Normal;
        map.player.groundState =
            new Player.GroundState(true, Player.Orientation.Normal, 0, false, false);

        map.player.checkpointPosition = map.player.position;

        for (var n = 0; n < 100; n += 1)
            map.gameCamera.Advance(true);
        map.Advance();

        lastSnapToChunkIndex = (map, index, map.frameNumber);
    }

    public void PreviousDay()
    {
        this.selectedDate -= System.TimeSpan.FromDays(1);
        UpdateDateText();
    }

    public void NextDay()
    {
        this.selectedDate += System.TimeSpan.FromDays(1);
        UpdateDateText();
    }

    public void OnChangeLevelDropdown()
    {
        UpdateDateText();
    }

    private void UpdateDateText()
    {
        if (this.dateText != null)
        {
            var text = this.selectedDate.ToString(
                "d MMM yyyy", CultureInfo.InvariantCulture
            );
            text = $"<b>{text}</b>";

            Theme? theme = (Mode)this.levelDropdown.value switch
            {
                Mode.Algorithm =>
                    LevelGeneration.ThemeForDate(this.selectedDate, false),
                Mode.AlgorithmRegenerate =>
                    LevelGeneration.ThemeForDate(this.selectedDate, true),
                Mode.GenerateGravityGalaxyLevel => Theme.GravityGalaxy,
                Mode.GenerateTombstoneHillLevel => Theme.TombstoneHill,
                Mode.GenerateMoltenFortressLevel => Theme.MoltenFortress,
                _ => null
            };
            if (theme != null)
                text += $"\n<size=8>{theme.Value.CleanName()}</size>";

            this.dateText.text = text;
        }
    }

    public void BackToCalendar()
    {
        Transition.GoSimple("Calendar");
    }

    public void OnUIStyleChanged()
    {
        uiStyle = (UIStyle)this.uiStyleDropdown.value;

        IngameUI.instance.layoutPortrait.NotifyUIStyleChanged();
        IngameUI.instance.layoutLandscape.NotifyUIStyleChanged();
        IngameUI.instance.layoutLandscape2P.NotifyUIStyleChanged();
        IngameUI.instance.layoutLandscape3P.NotifyUIStyleChanged();
        IngameUI.instance.layoutLandscape4P.NotifyUIStyleChanged();
    }

    public void OnPressInfoUp()
    {
        this.infoScrollRect.content.localPosition += new Vector3(0, -100);
        this.infoScrollRect.verticalNormalizedPosition =
            Mathf.Clamp01(this.infoScrollRect.verticalNormalizedPosition);
    }

    public void OnPressInfoDown()
    {
        this.infoScrollRect.content.localPosition += new Vector3(0, 100);
        this.infoScrollRect.verticalNormalizedPosition =
            Mathf.Clamp01(this.infoScrollRect.verticalNormalizedPosition);
    }

    private void UpdateInfoText()
    {
        if (this.infoText == null) return;

        var str = new StringBuilder();
        str.AppendLine("<b>Controllers</b>");
        for (int n = 0; n < Rewired.ReInput.controllers.controllerCount; n += 1)
        {
            var c = Rewired.ReInput.controllers.Controllers[n];
            str.AppendLine($"{n}. {c.name} [{c.hardwareTypeGuid}]");
        }
        for (int pn = 0; pn < Rewired.ReInput.players.playerCount; pn += 1)
        {
            var player = Rewired.ReInput.players.Players[pn];
            var playing = player.isPlaying ? "playing" : "not playing";
            var controllers =
                string.Join(", ", player.controllers.Controllers.Select(c => c.name));
            if (controllers == "")
                controllers = "[none]";
            str.AppendLine($"<b>{player.name}</b> ({playing}): {controllers}");
        }
        this.infoText.text = str.ToString();
    }
}
