﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienSkull : Enemy
{
    public Animated.Animation animationCalm;
    public Animated.Animation animationDetect;
    public Animated.Animation animationAngry;
    public Animated.Animation animationSpin;
    public Animated.Animation animationScreech;
    public Animated.Animation animationScreechToIdle;
    public Animated.Animation animationBump;

    [Header("Trail")]
    public Sprite trailParticle;
    public AnimationCurve trailFadeCurve;
    public Sprite smackParticle;

    public enum State
    {
        Calm,
        Detect,
        Angry,
        Screech,
        CoolDown
    }

    private Vector2 velocity;
    private State state;
    private Vector2 spawnPosition;
    private Vector2 hitWallVelocity;
    private bool stopMovement;
    private Vector2 surfaceVelocity;
    private float currentAngleDegrees;
    private float targetAngleDegrees;
    private float currentSpeed;
    private int angryTimer;
    private int enterAngryTimer;
    private Vector2 movementDirection;

    private const float CalmSpeed = 0.04f;
    private const float AngrySpeed = 0.125f;
    private const float DistanceDetectPlayer = 5f;
    private const float TrailParticleLength = 0.60f;
    private const int TrailParticleLifetime = 30;
    private const float HitboxSize = 0.60f;
    private const int MinimumAngryTime = 60 * 4;
    private const int DelayEnterAngryAgain = 60 * 1;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(
                -HitboxSize, HitboxSize, -HitboxSize, HitboxSize,
                this.rotation, Hitbox.NonSolid
        )};

        this.spawnPosition = this.position;
        this.currentAngleDegrees = this.targetAngleDegrees = 315f;

        ChangeState(State.Calm);
    }

    public override void Advance()
    {
        switch (state)
        {
            case State.Calm:
                AdvanceCalm();
                break;

            case State.Detect:
                AdvanceDetect();
                break;

            case State.Angry:
                AdvanceAngry();
                break;

            case State.Screech:
                AdvanceScreech();
                break;

            case State.CoolDown:
                AdvanceCoolDown();
                break;
        }

        EnemyHitDetectionAgainstPlayer();
        Movement();

        this.position += this.velocity;
        this.transform.position = this.position;

        RefreshRotation();
    }

    private void AdvanceCalm()
    {
        if (this.enterAngryTimer > 0)
        {
            this.enterAngryTimer -= 1;
        }

        if (IsPlayerNearby() && this.enterAngryTimer == 0)
        {
            ChangeState(State.Detect);
        }
    }

    private void AdvanceDetect()
    {
        // nothing
    }

    private void AdvanceAngry()
    {
        if (IsPlayerNearby())
        {
            this.angryTimer = MinimumAngryTime;
        }

        if (this.angryTimer > 0)
        {
            this.angryTimer -= 1;

            if (this.angryTimer == 0)
            {
                ChangeState(State.Screech);
            }
        }

        CreateTrail();
    }

    private void CreateTrail()
    {
        Vector2 particlePosition = this.position;
        Particle p = Particle.CreateWithSprite(
            this.trailParticle,
            TrailParticleLifetime,
            particlePosition,
            this.transform.parent
        );

        p.spriteRenderer.sortingLayerID = this.spriteRenderer.sortingLayerID;
        p.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 2;
        p.FadeOut(this.trailFadeCurve);
        p.transform.localScale = new Vector3(1f, 1.1f, 1f);
        p.transform.localRotation =
                Quaternion.Euler(Vector3.forward * this.targetAngleDegrees);
    }

    private void AdvanceScreech()
    {
        this.currentSpeed = Util.Slide(this.currentSpeed, 0f, 0.005f);
        CreateDust();

        if (this.currentSpeed == 0)
        {
            ChangeState(State.CoolDown);
        }
    }

    private void CreateDust()
    {
        float x = this.position.x;
        float y = this.position.y;

        int lifetime = UnityEngine.Random.Range(25, 35);

        Particle p = Particle.CreatePlayAndHoldLastFrame(
            this.map.player.animationDustParticle,
            lifetime,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.75f, 1f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.3f, 0.3f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void AdvanceCoolDown()
    {
        // nothing
    }

    private void Movement()
    {
        void Bump()
        {
            this.animated.PlayOnce(this.animationBump, () =>
            {
                Animated.Animation nextAnimation = (this.state == State.Calm) ?
                    this.animationCalm : this.animationAngry;
                this.animated.PlayAndLoop(nextAnimation);
            });
        }

        bool hitWall = false;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0, 6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0, 6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), false, 0.75f)
        );

        Vector2 dustOffset = Vector2.zero;
        bool dustHorizontal = false;

        if (this.velocity.x > 0)
        {
            if (right.anything == true)
            {
                this.velocity.x = Mathf.Abs(this.velocity.x) * -1f;
                hitWall = true;

                dustOffset = Vector2.right;
                dustHorizontal = false;

                if (right.tiles.Length > 0 && right.tiles[0] != null)
                {
                    this.surfaceVelocity = right.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if (left.anything == true)
            {
                this.velocity.x = Mathf.Abs(this.velocity.x);
                hitWall = true;

                dustOffset = Vector2.left;
                dustHorizontal = false;

                if (left.tiles.Length > 0 && left.tiles[0] != null)
                {
                    surfaceVelocity = left.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if (this.velocity.y > 0)
        {
            if (up.anything == true)
            {
                this.velocity.y = Mathf.Abs(this.velocity.y) * -1f;
                hitWall = true;

                dustOffset = Vector2.up;
                dustHorizontal = true;

                if (up.tiles.Length > 0 && up.tiles[0] != null)
                {
                    surfaceVelocity = up.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if (down.anything == true)
            {
                this.velocity.y = Mathf.Abs(this.velocity.y);
                hitWall = true;

                dustOffset = Vector2.down;
                dustHorizontal = true;

                if (down.tiles.Length > 0 && down.tiles[0] != null)
                {
                    surfaceVelocity = down.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }


        if (hitWall == true && this.state != State.Calm)
        {
            CreateDustDrop(dustOffset, dustHorizontal);

            hitWall = false;

            Particle p = Particle.CreateWithSprite(
                this.smackParticle,
                TrailParticleLifetime,
                this.position,
                this.transform.parent
            );

            p.spriteRenderer.sortingLayerID = this.spriteRenderer.sortingLayerID;
            p.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;
            p.FadeOut(this.trailFadeCurve);
            p.transform.localScale = Vector3.one * 2f;

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyAlienskullBounce,
                position: this.position
            );
        }

        if (hitWall == true && this.stopMovement == false)
        {
            CreateDustDrop(dustOffset, dustHorizontal);
            this.hitWallVelocity = this.velocity;
            this.stopMovement = true;

            Bump();

            this.animated.OnFrame(4, () =>
            {
                this.stopMovement = false;
                this.surfaceVelocity = Vector2.zero;
                this.velocity = this.hitWallVelocity;
            });
        }

        this.movementDirection = this.velocity.normalized == Vector2.zero ?
            this.movementDirection : this.velocity.normalized;

        if (this.stopMovement == true)
        {
            this.velocity = surfaceVelocity;
        }
        else
        {
            this.velocity.x = this.currentSpeed * Mathf.Sign(this.movementDirection.x);
            this.velocity.y = this.currentSpeed * Mathf.Sign(this.movementDirection.y);
        }
    }

    private const float DustDropOffset = 0.50f;

    private void CreateDustDrop(Vector2 offset, bool horizontal)
    {
        if (this.state == State.Detect) return;

        float x = this.position.x + offset.x;
        float y = this.position.y + offset.y;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                originPosition,
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.65f, 0.65f, 1);

            bool isMovingFast = this.currentSpeed > CalmSpeed;
            float minVelocityX = isMovingFast ? 0.20f : 0.05f;
            float maxVelocityX = isMovingFast ? 0.30f : 0.10f;

            float accelerationValue = isMovingFast ? -0.05f : -0.02f;

            float localX =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(minVelocityX, maxVelocityX));
            float localY = UnityEngine.Random.Range(-0.05f, 0.05f);

            p.velocity.x = horizontal ? localX : localY;
            p.velocity.y = horizontal ? localY : localX;
            p.acceleration = p.velocity * accelerationValue;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
        }
    }

    private void RefreshRotation()
    {
        Vector3 eulerRotation =
            Quaternion.LookRotation(Vector3.forward, this.velocity.normalized).eulerAngles;
        this.targetAngleDegrees = eulerRotation == Vector3.zero ? this.targetAngleDegrees : eulerRotation.z;

        if (this.currentAngleDegrees > this.targetAngleDegrees + 180)
            this.currentAngleDegrees -= 360;
        if (this.currentAngleDegrees < this.targetAngleDegrees - 180)
            this.currentAngleDegrees += 360;

        float rotateSpeed = this.state != State.Calm ? 30f : 10f;

        this.currentAngleDegrees =
            Util.Slide(this.currentAngleDegrees, this.targetAngleDegrees, rotateSpeed);
        this.transform.rotation =
            Quaternion.Euler(Vector3.forward * this.currentAngleDegrees);
    }

    private bool IsPlayerNearby()
    {
        Raycast detectRaycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
        );

        Vector2 playerPosition = this.map.player.position;
        Vector2 heading = playerPosition - this.position;
        float distanceToPlayer = heading.magnitude;
        Vector2 directionToPlayer = heading / distanceToPlayer;

        Raycast.Result result = detectRaycast.Arbitrary(
            this.position, directionToPlayer, 20
        );

        float distanceToObstacle = result.distance;

        return distanceToObstacle > distanceToPlayer;
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.Calm:
                EnterCalm();
                break;

            case State.Detect:
                EnterDetect();
                break;

            case State.Angry:
                EnterAngry();
                break;

            case State.Screech:
                EnterScreech();
                break;

            case State.CoolDown:
                EnterCoolDown();
                break;
        }
    }

    private void EnterCalm()
    {
        this.animated.PlayAndLoop(this.animationCalm);
        this.currentSpeed = CalmSpeed;
        this.stopMovement = false;
    }

    private void EnterDetect()
    {
        this.animated.PlayOnce(this.animationDetect, () =>
        {
            ChangeState(State.Angry);
        });
        this.stopMovement = true;

        float animationDetectLifetimeInSeconds =
            (float)this.animationDetect.sprites.Length / (float)this.animationDetect.fps;
        int animationDetectLifetime =
            Mathf.RoundToInt(60f * (animationDetectLifetimeInSeconds));

        Particle p = Particle.CreatePlayAndLoop(
            Assets.instance.chargingParticlesAnimation,
            animationDetectLifetime,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerID = this.spriteRenderer.sortingLayerID;
        p.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyAlienskullBlastoff,
            position: this.position
        );
    }

    private void EnterAngry()
    {
        this.animated.PlayAndLoop(this.animationAngry);
        this.currentSpeed = AngrySpeed;
        this.stopMovement = false;
        this.angryTimer = MinimumAngryTime;
        this.enterAngryTimer = DelayEnterAngryAgain;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
    }

    private void EnterScreech()
    {
        this.animated.PlayOnce(this.animationSpin, () =>
        {
            this.animated.PlayAndLoop(this.animationScreech);
        });
    }

    private void EnterCoolDown()
    {
        this.animated.PlayOnce(this.animationScreechToIdle, () =>
        {
            ChangeState(State.Calm);
        });

        this.stopMovement = true;
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
    }


}
