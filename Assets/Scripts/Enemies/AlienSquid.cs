﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienSquid : Enemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationShoot;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;

    private Vector2 velocity; // when falling
    private float speed;
    private bool walkingRight;
    private float visibleAngleDegrees;
    private bool falling = false;
    private bool shooting = false;
    
    private const float DistanceToFeet = 1.05f;
    private const float Gravity = 0.03125f;
    private const float WalkSpeed = 0.66f;
    private const float DetectPlayerHorizontal = 1f;
    private const float BulletVelocity = 0.20f;

    public Vector2 Velocity => velocity;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.70f, 0.70f, -1f, 0.50f, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayAndLoop(this.animationWalk);
        this.walkingRight = !def.tile.flip;
        this.visibleAngleDegrees = this.rotation;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.speed = WalkSpeed;
    }

    private Vector2 Forward() => new Vector2(
        Mathf.Cos(this.rotation * Mathf.PI / 180),
        Mathf.Sin(this.rotation * Mathf.PI / 180)
    );

    private Vector2 Up()
    {
        var forward = Forward();
        return new Vector2(-forward.y, forward.x);
    }

    private Vector2 Down()
    {
        var forward = Forward();
        return new Vector2(forward.y, -forward.x);
    }

    public void BounceFromSpring(Vector2 velocity)
    {
        this.falling = true;
        this.velocity = velocity;
        this.rotation = 0;
        this.visibleAngleDegrees = 0;
    }
    

    public override void Advance()
    {
        if(this.deathAnimationPlaying == true) return;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
        );
        {
            // before even moving anywhere, check below for
            // moving ground and adjust as appropriate
            var below = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
            if (below.tile != null)
                this.position += below.tile.tileGrid.effectiveVelocity;
        }

        if (this.falling == true)
            Fall(raycast);
        else
            WalkForward(raycast);

        if(this.shooting == false)
        {
            DetectPlayer();
        }
        
        EnemyHitDetectionAgainstPlayer();
        SwitchToNearestChunk();

        this.transform.position = this.position;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
        this.transform.localScale = new Vector3(this.walkingRight ? 1 : -1, 1, 1);
    }

    private void Fall(Raycast raycast)
    {
        this.velocity.y -= Gravity;
        this.position += this.velocity;
        this.rotation = 0;

        if (this.velocity.y > 0)
        {
            this.animated.PlayAndLoop(this.animationRise);

            var r = raycast.Vertical(this.position, true, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Down() * DistanceToFeet);
                this.velocity.y = 0;
            }
        }
        else if (this.velocity.y < 0)
        {
            this.animated.PlayAndLoop(this.animationFall);

            var r = raycast.Vertical(this.position, false, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Up() * DistanceToFeet);
                this.falling = false;

                // dust particles
                for (var n = 0; n < 10; n += 1)
                {
                    var p = Particle.CreateAndPlayOnce(
                        this.map.player.animationDustLandParticle,
                        r.End(),
                        this.transform.parent
                    );
                    p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                    p.velocity.x =
                        (((n % 2 == 0) ? 1 : -1) *
                        UnityEngine.Random.Range(0.2f, 0.3f));
                    p.velocity.y = UnityEngine.Random.Range(-0.05f, 0.05f);
                    p.acceleration = p.velocity * -0.05f;
                    p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
                }
                
                this.animated.PlayAndLoop(animationWalk);
            }
        }
    }

    private void WalkForward(Raycast raycast)
    {
        void Turn()
        {
            this.walkingRight = !this.walkingRight;
            this.speed = 0f;

            if(this.shooting == false)
            {
                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.speed = WalkSpeed;
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            }
        }

        {
            var ahead = raycast.Arbitrary(
                this.position + (Up() * -0.625f),
                this.walkingRight ? Forward() : -Forward(),
                1
            );
            var aheadPeg = ahead.item as Peg;
            if (aheadPeg != null)
            {
                aheadPeg.NotifyEnemyBouncedOff(new Collision { otherItem = this });
                Turn();
            }
        }

        this.position += (this.walkingRight ? 1 : -1) * (Forward() / 16f) * this.speed;

        var forward = raycast.Arbitrary(
            this.position, this.walkingRight ? Forward() : -Forward(), 0.75f
        );

        if (forward.anything == true)
        {
            float angleDelta =
                Mathf.Abs(this.visibleAngleDegrees - forward.surfaceAngleDegrees);

            if(angleDelta >= 90f)
            {
                // detected solid
                Turn();
            }
            else
            {
                this.rotation = Mathf.RoundToInt(forward.surfaceAngleDegrees);
                this.position = forward.End() + (Up() * DistanceToFeet);
            }
        }
        else
        {
            var below = raycast.Arbitrary(this.position, -Up(), 1.875f);
            if (below.anything == true)
            {
                this.position += Up() * (DistanceToFeet - below.distance);
                this.rotation = Mathf.RoundToInt(below.surfaceAngleDegrees);

                var doublecheck = raycast.Arbitrary(this.position, -Up(), 1.875f);
                var groundThereAfterRotation =
                    doublecheck.anything == true &&
                    Mathf.Abs(
                        doublecheck.surfaceAngleDegrees - below.surfaceAngleDegrees
                    ) < 10.0f;

                if (this.rotation < this.visibleAngleDegrees - 180)
                    this.visibleAngleDegrees -= 360;
                if (this.rotation > this.visibleAngleDegrees + 180)
                    this.visibleAngleDegrees += 360;
                this.visibleAngleDegrees = Util.Slide(
                    this.visibleAngleDegrees, this.rotation, 7
                );

                if (groundThereAfterRotation == true)
                {
                    if (this.rotation < this.visibleAngleDegrees - 180)
                        this.visibleAngleDegrees -= 360;
                    if (this.rotation > this.visibleAngleDegrees + 180)
                        this.visibleAngleDegrees += 360;
                    this.visibleAngleDegrees = Util.Slide(
                        this.visibleAngleDegrees, this.rotation, 7
                    );
                }
            }

            var ahead = this.walkingRight ? Forward() : -Forward();
            var belowAhead = raycast.Arbitrary(
                this.position + (ahead * 0.625f),
                -Up(),
                1.875f
            );

            // detected gap
            if (belowAhead.anything == false)
            {                
                var up = Up();
                var rayStart = this.position + (ahead * 0.75f);
                var rayDelta = (up * -DistanceToFeet) + (ahead * -0.5625f);
                var belowBehind = raycast.Arbitrary(
                    rayStart,
                    rayDelta.normalized,
                    rayDelta.magnitude + 0.3125f
                );

                if (belowBehind.anything == true)
                {
                    Turn();
                }
                else
                {
                    // floor disappeared
                    this.falling = true;
                }
            }
        }

        this.velocity = Vector2.zero;
    }

    private void DetectPlayer()
    {
        if (this.map.NearestChunkTo(this.map.player.position) != this.chunk)
            return;

        var surfaceNormal = new Vector2(
            -Mathf.Sin(this.visibleAngleDegrees * Mathf.PI / 180f),
            Mathf.Cos(this.visibleAngleDegrees * Mathf.PI / 180f)
        );

        var detectRaycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
        );
        var maxDistance = detectRaycast.Arbitrary(
            this.position, surfaceNormal, 20
        ).distance;

        var furthestPoint = this.position + (surfaceNormal * maxDistance);
        var ray = surfaceNormal * maxDistance;

        var along = Vector2.Dot(
            this.map.player.position - this.position, ray
        ) / ray.sqrMagnitude;
        along = Mathf.Clamp01(along);

        var nearestPointAlongRayToPlayer =
            Vector2.Lerp(this.position, furthestPoint, along);

        if ((nearestPointAlongRayToPlayer - this.map.player.position).magnitude < 1)
        {
            StartShoot();
        }
    }

    private void StartShoot()
    {
        if(this.shooting == true) return;

        this.shooting = true;
        this.speed = 0f;
        this.animated.PlayOnce(this.animationShoot, EndShoot);
        this.animated.OnFrame(11, CreateBullet);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyAliensquid,
            position: this.position
        );
    }

    private void EndShoot()
    {
        this.shooting = false;
        this.speed = WalkSpeed;
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void CreateBullet()
    {
        Vector2 bulletSpawnPos = this.position + (Up() * 1.25f);
        var bullet = AlienSquidBullet.Create(
            bulletSpawnPos,
            this.chunk
        );
        bullet.velocity = Up() * BulletVelocity;
    }

}
