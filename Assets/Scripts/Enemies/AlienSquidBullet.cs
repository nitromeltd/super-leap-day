﻿using System;
using UnityEngine;

public class AlienSquidBullet : Enemy
{
    public Animated.Animation animationMove;
    public Animated.Animation animationBurst;

    [NonSerialized] public Vector2 velocity;
    private bool alive = true;
    
    public static AlienSquidBullet Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(
            Assets.instance.gravityGalaxy.alienSquidBullet, chunk.transform
        );
        obj.name = "Spawned Alien Squid Bullet";
        var item = obj.GetComponent<AlienSquidBullet>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndLoop(this.animationMove);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }
    
    public override void Advance()
    {
        if (this.alive == false) return;

        base.Advance();

        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByEnemy: true,
            ignoreTileTopOnlyFlag: true
        );

        MoveHorizontally(raycast);
        MoveVertically(raycast);
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        
        this.transform.position = this.position;
    }
    
    protected void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        float xOffset = Mathf.Sign(this.velocity.x) * -0.25f;

        var high    = this.position.Add(xOffset, rect.yMax - 0.50f);
        var mid     = this.position.Add(xOffset, (rect.yMin + rect.yMax) * 0.25f);
        var low     = this.position.Add(xOffset, rect.yMin + 0.50f);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid,  true, maxDistance),
                raycast.Horizontal(low,  true, maxDistance)
            );

            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid,  false, maxDistance),
                raycast.Horizontal(low,  false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }
    
    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        float yOffset = Mathf.Sign(this.velocity.y) * -0.25f;

        var high    = this.position.Add(rect.xMax - 0.50f, yOffset);
        var mid     = this.position.Add( (rect.xMin + rect.xMax) * 0.25f, yOffset);
        var low     = this.position.Add(rect.xMin + 0.50f, yOffset);

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(high, true, maxDistance),
                raycast.Vertical(mid,  true, maxDistance),
                raycast.Vertical(low,  true, maxDistance)
            );

            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = -Mathf.Abs(this.velocity.y);
                Collide();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(high, false, maxDistance),
                raycast.Vertical(mid,  false, maxDistance),
                raycast.Vertical(low,  false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = Mathf.Abs(this.velocity.y);
                Collide();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }
    
    private void Collide()
    {
        if(this.alive == false) return;
        this.alive = false;

        this.animated.PlayOnce(this.animationBurst, delegate
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
        });

        Audio.instance.PlaySfx(Assets.instance.sfxTrunkyProjectileLand, position: this.position);
    }
    
}
