using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy
{
    public bool followPlayer = false;
    public Animated.Animation animationSleep;
    public Animated.Animation animationWakeup;
    public Animated.Animation animationFly;
    public Animated.Animation animationDropStart;
    public Animated.Animation animationDrop;
    public Animated.Animation animationDropEnd;

    private bool facingRight = false;
    private enum State
    {
        Sleep,
        Wakeup,
        Drop,
        DropEnd,
        Fly,
    };

    private State state;
    Vector2 vel = Vector2.zero;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;

    private const float BreathePeriod = 0.3f;
    private const float BreatheAmplitude = 0.45f;

    private float flySpeed = 0.15f;
    private float DropSpeed = 0.175f;

    private float triggerRadius = 15;
    private Vector2 triggerPosition;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };

        ChangeState(State.Sleep);
        this.facingRight = false;

        if (def.tile.properties?.ContainsKey("trigger_radius") == true)
        {
            this.triggerRadius = def.tile.properties["trigger_radius"].f;
        }
        var connection = chunk.connectionLayer.NearestNodeTo(this.position, 2);
        if (connection != null)
        {
            var otherNode =
                connection.Previous() ??
                connection.Next();
            triggerPosition = otherNode.Position;
        }
        else
        {
            triggerPosition = this.position;
        }
    }

    public override void Advance()
    {
        
        EnemyHitDetectionAgainstPlayer();

        switch (state)
        {
            case State.Sleep:
                AdvanceSleep();
                break;

            case State.Drop:
                AdvanceDrop();
                break;

            case State.Fly:
                AdvanceFly();
                break;
        }

        if (this.map.player.alive)
            this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.position += this.vel;

        if(this.state == State.Fly)
        {
            // breathe movement
            this.breatheTimer += Time.deltaTime;
            float theta = this.breatheTimer / BreathePeriod;
            float distance = BreatheAmplitude * Mathf.Sin(theta);
            Vector2 breathePos = this.breatheMovement ? Vector2.up * distance : Vector2.zero;
            this.transform.position = this.position + breathePos;
        }
        else
        {
            this.transform.position = this.position;
        }

    }

    private void AdvanceSleep()
    {
        if (Vector2.Distance(this.map.player.position, triggerPosition) <= triggerRadius)
        {
            ChangeState(State.Wakeup);
        }
    }

    private void AdvanceDrop()
    {
        if (this.position.y <= this.map.player.position.y)
        {
            ChangeState(State.DropEnd);
        }
        else
        {
            float maxDistance = 1.00f;
            var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
            if (raycast.Vertical(this.position, false, maxDistance).anything == true)
            {
                ChangeState(State.DropEnd);
            }
        }
    }

    private void AdvanceFly()
    {
        if(this.map.player.alive== false)
        {
            this.vel = new Vector2(0, 0);
            return;
        }
        if (followPlayer == false)
        {
            float maxDistance = 1.00f;
            var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

            if (this.facingRight == true)
            {
                if (raycast.Horizontal(this.position, true, maxDistance).anything == true)
                {
                    this.vel = new Vector2(0, 0);
                    Die();
                }
            }
            else
            {
                if (raycast.Horizontal(this.position, false, maxDistance).anything == true)
                {
                    this.vel = new Vector2(0, 0);
                    Die();
                }
            }
        }
        else
        {
            if(this.map.NearestChunkTo(this.map.player.position) == this.chunk)
            {
                facingRight = this.map.player.position.x > this.position.x ? true : false;
                vel = (this.map.player.position - this.position).normalized / 10;
            }
            else
            {
                vel = Vector2.zero;
            }
        }
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.Sleep:
                EnterSleep();
                break;

            case State.Wakeup:
                EnterWakeup();
                break;

            case State.Drop:
                EnterDrop();
                break;

            case State.DropEnd:
                EnterDropEnd();
                break;

            case State.Fly:
                EnterFly();
                break;
        }
    }

    private void EnterSleep()
    {
        this.vel.x = 0f;
        this.animated.PlayAndLoop(this.animationSleep);
    }

    private void EnterWakeup()
    {
        this.vel.x = 0f;
        this.animated.PlayOnce(this.animationWakeup, () => 
        {
            ChangeState(State.Drop);
        });

        this.animated.OnFrame(2, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxEnemyBatWakeUp, position: this.position);
        });
    }

    private void EnterDrop()
    {
        this.vel.y = -DropSpeed;
        this.animated.PlayOnce(this.animationDropStart, () =>
        {
            this.animated.PlayAndLoop(this.animationDrop);
        });
    }

    private void EnterDropEnd()
    {
        this.vel.y = 0f;
        if (this.map.player.position.x > this.position.x)
        {
            this.facingRight = true;
        }
        else
        {
            this.facingRight = false;
        }
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.animated.PlayOnce(this.animationDropEnd, () =>
        {
            ChangeState(State.Fly);
        });

        if (this.followPlayer == true)
        {
            this.animated.OnFrame(3, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnemyBatFlyPurple, position: this.position);
            });
        }
        else
        {
            this.animated.OnFrame(3, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnemyBatFlyRed, position: this.position);
            });
        }
    }

    private void EnterFly()
    {
        this.animated.PlayAndLoop(this.animationFly);
        if(followPlayer == false)
        {
            this.vel = new Vector2(this.facingRight ? flySpeed : -flySpeed, 0);
        }
        if(this.followPlayer == true)
        {
            this.animated.OnFrame(3, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnemyBatFlyPurple, position: this.position);
            });
        }
        else
        {
            this.animated.OnFrame(3, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnemyBatFlyRed, position: this.position);
            });
        }
    }

    private void Die()
    {
        Kill(new KillInfo { itemDeliveringKill = this });
    }
}
