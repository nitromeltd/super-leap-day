using UnityEngine;
using System.Collections.Generic;

public class BeetleSoldier : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationWalkTurn;
    public Animated.Animation animationFly;
    public Animated.Animation animationFlyLookUp;
    public Animated.Animation animationFlyTurn;
    public Animated.Animation animationShoot;
    public Animated.Animation animationShootUp;
    public Animated.Animation animationAlert;
    public Animated.Animation animationBouncedUp;
    public Animated.Animation animationFallingDown;
    public Animated.Animation animationStandingIdle;

    Stopwatch timer = new();
    Stopwatch frameTimer = new();
    private enum State
    {
        Walk,
        AlertedWalk,
        TurnWalk,
        TurnAlertedWalk,
        FlyAround,
        TurnFly,
        Shoot,
        Surprised,
        WalkAirborne,
    };

    private enum ShootDirection
    {
        Left,
        Right,
        Up,
        Down,
    }

    private enum CameraOrientation
    {
        Horizontal,
        Vertical
    }
    private CameraOrientation cameraOrientation;

    public CameraCone cameraCone;
    public Transform raysRotationOrigin;    // beetle soldier does his own thing with rays (it rotates them)
    private State state;
    private int shootTimer;

    private const int ShootDelay = 60;
    private const float WalkSpeed = 0.7f / 16;
    private const float FlySpeed = 2f / 16;
    private int lastExclamationMarkCreatedOnFrame;

    private bool alerted;
    private int alertDelay;
    private Vector2 nextPoint;

    public override void Init(Def def)
    {
        base.Init(def);
        this.onTurnAround = EnterTurn;
        this.hitboxes = new[] {
            new Hitbox(-1.1f, 1.1f, -1.1f, 1.0f, this.rotation, Hitbox.NonSolid)
        };

        this.alerted = false;
        EnterWalk();
        this.cameraCone.Init(this.map);
        this.cameraCone.SetCallbacks(ShouldCameraActivate, onPlayerSpotted: PlayerSpottedBySoldier);
        this.cameraCone.insideNotchDistance = 1.2f;
        alertDelay = Random.Range(20, 60);
    }

    override public void Advance()
    {
        // DebugDrawing.Draw(false, true);
        this.cameraCone.Advance();

        if (this.state != State.TurnFly && this.state != State.TurnWalk && this.state != State.TurnAlertedWalk)
        {
            if (this.facingRight) this.transform.localScale = new Vector3(+1, 1, 1);
            if (!this.facingRight) this.transform.localScale = new Vector3(-1, 1, 1);
        }

        switch (this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.AlertedWalk:
                AdvanceAlertedWalk();
                break;

            case State.TurnFly:
                DetectPlayerPosition();
                break;

            case State.FlyAround:
                AdvanceFly();
                break;

            case State.WalkAirborne:
                AdvanceWalkAirborne();
                break;

            case State.Surprised:
                EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
                break;

            default:
                break;
        }

        this.transform.position = this.position;
        if (this.shootTimer > 0)
        {
            this.shootTimer--;
        }

        if (!this.alerted)
        {
            if (this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay)
            {
                this.alerted = true;
                EnterSurprised();
            }
            else if (this.map.desertAlertState.IsGloballyActive() && this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position))
            {
                this.alerted = true;
                PlayerSpottedBySoldier();
            }
        }
        else
        {
            if (!this.map.desertAlertState.IsGloballyActive())
            {
                if (!IsSoldierFlying())
                {
                    this.alerted = false;
                }
            }
        }
#if UNITY_EDITOR
        // if (Input.GetKeyDown(KeyCode.P))
        // {
        //     var chunk = this.map.NearestChunkTo(this.position);


        //     timer.elapsed = 0;
        //     timer.Start();
        //     chunk.pathfinding.Refresh();
        //     timer.Stop();
        //     frameTimer.Stop();
        //     Debug.Log("Timer:" + timer.elapsed + ", frameTimer:" + frameTimer.elapsed);

        //     // Debug.Break();
        //     PlayerSpottedBySoldier();
            

        // }
#endif     
        frameTimer.Reset();
        frameTimer.Start();

        if(this.state == State.FlyAround)
        {
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemyBeetlesoldierChase))
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyBeetlesoldierChase,
                    position: this.position
                );
            }
        }
        else
        {
            if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemyBeetlesoldierChase))
            {
                Audio.instance.StopSfx(Assets.instance.sfxEnemyBeetlesoldierChase);
            }
        }
    }

    private void DebugPath()
    {
#if UNITY_EDITOR        
        var tPos = this.position;
        for (int i = 0; i < 60; i++)
        {
            var nextPos = chunk.pathfinding.NextPointToPlayer(tPos);
            if (nextPos.HasValue)
            {
                var newPosition = new Vector2(chunk.xMin, chunk.yMin) + nextPos.Value;
                Debug.DrawLine(tPos, newPosition, Color.green, 0);
                tPos = newPosition;
                // Debug.Break();
            }
        }
#endif            
    }

    private bool ShouldCameraActivate()
    {
        if ((this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay) || this.alerted)
        {
            return true;
        }
        return false;
    }

    private void PlayerSpottedBySoldier()
    {
        if (this.state == State.Walk || this.state == State.AlertedWalk)
        {
            if (this.map.player.state == Player.State.InsideBox)
            {
                if (this.map.player.GetBox() != null)
                {
                    if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitBox();
                    }
                }

                if (this.map.player.GetHidingPlace() != null)
                {
                    if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitHidingPlace();
                    }
                }
            }

            EnterSurprised();
            this.map.desertAlertState.ActivateAlert(this.position);
            this.alerted = true;

        }
        else if (this.state == State.FlyAround)
        {
            PlayerSpottedMidAir();
        }
    }

    private void CreateExclamationMark()
    {
        if (this.map.frameNumber - this.lastExclamationMarkCreatedOnFrame < 1 * 60)
        {
            return;
        }

        this.lastExclamationMarkCreatedOnFrame = this.map.frameNumber;
        var exclamationMark = Particle.CreateAndPlayOnce(
                    Assets.instance.exclamationMarkAnimation,
                    this.position.Add(
                        0,
                        3f
                    ),
                    this.transform.parent
                );
        exclamationMark.spriteRenderer.sortingLayerName = "Items";
        exclamationMark.spriteRenderer.sortingOrder = -1;
    }

    private void EnterWalk()
    {
        lastOnGround = this.onGround;
        this.velocity = new Vector2(this.facingRight ? WalkSpeed : -WalkSpeed, 0.0f);
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        this.spriteRenderer.color = Color.white;
        this.state = State.Walk;
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterAlertedWalk()
    {
        lastOnGround = this.onGround;
        this.velocity = new Vector2(this.facingRight ? WalkSpeed * 2 : -WalkSpeed * 2, 0.0f);
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        this.spriteRenderer.color = Color.white;
        this.state = State.AlertedWalk;
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private float lastVerticalVelocity;

    private void EnterWalkAirborne()
    {
        this.state = State.WalkAirborne;
        this.lastVerticalVelocity = this.velocity.y;
        SetCorrectWalkAirborneAnimation();
    }

    private void AdvanceWalkAirborne()
    {
        this.velocity.y -= 0.5f / 16;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        if (this.lastVerticalVelocity * this.velocity.y <= 0)
        {
            SetCorrectWalkAirborneAnimation();
        }
        this.lastVerticalVelocity = this.velocity.y;

        if (this.onGround)
        {
            EnterWalk();
        }
    }

    private void SetCorrectWalkAirborneAnimation()
    {
        if (this.velocity.y > 0)
        {
            this.animated.PlayAndLoop(this.animationBouncedUp);
        }
        else if (this.velocity.y < 0)
        {
            this.animated.PlayAndLoop(this.animationFallingDown);
        }
    }

    private bool lastOnGround;
    private void AdvanceWalk()
    {
        this.velocity.y -= 0.5f / 16;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());

        if (!this.onGround && this.velocity.y != 0 && lastOnGround)
        {
            //we are falling or interacted with a spring
            EnterWalkAirborne();
        }
        lastOnGround = this.onGround;
        SlideCameraToAngle(0);

        if (this.trappedHorizontally == true)
        {
            this.animated.PlayAndLoop(animationStandingIdle);
        }
    }

    private void AdvanceAlertedWalk()
    {
        this.velocity.y -= 0.5f / 16;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());

        if (!this.onGround && this.velocity.y != 0 && lastOnGround)
        {
            //we are falling or interacted with a spring
            EnterWalkAirborne();
        }
        lastOnGround = this.onGround;

        if (!this.map.desertAlertState.IsGloballyActive())
        {
            EnterWalk();
        }
        else
        {
            if (IsPlayerInSameChunkAsUs())
            {
                EnterSurprised();
            }
        }
        SlideCameraToAngle(0);
    }

    private void EnterTurn(TurnInfo turnInfo)
    {
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);

        if (this.state == State.AlertedWalk)
        {
            this.animated.PlayOnce(this.animationWalkTurn, () =>
            {
                EnterAlertedWalk();
            });
            this.state = State.TurnAlertedWalk;
        }
        else
        {
            this.animated.PlayOnce(this.animationWalkTurn, () =>
            {
                EnterWalk();
            });
            this.state = State.TurnWalk;
        }
    }

    private void EnterTurnFly()
    {
        this.state = State.TurnFly;
        this.animated.PlayOnce(this.animationFlyTurn, () =>
        {
            this.facingRight = !this.facingRight;
            EnterFlyAround();
        });
    }

    private void EnterShoot(ShootDirection direction)
    {
        this.state = State.Shoot;
        this.shootTimer = ShootDelay;
        if (direction == ShootDirection.Up)
        {
            this.animated.PlayOnce(this.animationShootUp, () =>
            {
                LeaveShoot();
            });
            this.animated.OnFrame(3, () =>
            {
                ShootProjectile(direction);
            });
        }
        else
        {
            this.animated.PlayOnce(this.animationShoot, () =>
            {
                LeaveShoot();
            });
            this.animated.OnFrame(3, () =>
            {
                ShootProjectile(direction);
            });
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyBeetlesoldiershoot,
            position: this.position
        );
    }

    private void LeaveShoot()
    {
        EnterFlyAround();
    }

    private void EnterSurprised()
    {
        this.state = State.Surprised;
        this.animated.PlayOnce(this.animationAlert, () =>
        {
            FindNewPath();

        });
        CreateExclamationMark();
    }

    private void FindNewPath()
    {
        if (IsPlayerInSameChunkAsUs())
        {
            EnterFlyAround();
        }
        else
        {
            //in another chunk
            EnterAlertedWalk();
        }
    }

    private void CheckForFlyingTurn()
    {
        if (this.map.player.position.x > this.position.x && !this.facingRight)
        {
            EnterTurnFly();
        }
        else if (this.map.player.position.x < this.position.x && this.facingRight)
        {
            EnterTurnFly();
        }
    }

    private void EnterFlyAround()
    {
        nextPoint = this.position;
        this.state = State.FlyAround;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

        if (cameraOrientation == CameraOrientation.Horizontal)
        {
            this.animated.PlayAndLoop(this.animationFly);
        }
        else
        {
            this.animated.PlayAndLoop(this.animationFlyLookUp);
        }
    }

    private void AdvanceFly()
    {
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());

        float diffX = 0;
        if (IsPlayerInSameChunkAsUs() && this.map.player.alive && IsPlayerHiding() == false)
        {
            chunk.pathfinding.Refresh();
            var pos = chunk.pathfinding.NextPointToPlayer(this.position);

            if (pos.HasValue)
            {
                Vector2 nPos = new Vector2(
                    Util.Slide(this.position.x, chunk.xMin + pos.Value.x, 0.1f),
                    Util.Slide(this.position.y, chunk.yMin + pos.Value.y, 0.1f)
                    );
                diffX = (nPos - this.position).x;
                this.position = nPos;
                // DebugPath();

                //  rotate beetle while it is following the path
                if (diffX > 0)
                {
                    if (!this.facingRight)
                    {
                        EnterTurnFly();
                    }
                }
                else if (diffX < 0)
                {
                    if (this.facingRight)
                    {
                        EnterTurnFly();
                    }
                }
                else
                {
                    // going up or down - rotate towards player direction
                    CheckForFlyingTurn();
                }
            }
            else
            {
                FlyLeftAndRightTryingToLookSmart();
            }
        }
        else
        {
            FlyLeftAndRightTryingToLookSmart();
        }

        if (!this.map.desertAlertState.IsGloballyActive())
        {
            var raycast = new Raycast(this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);
            if (CheckForFloorUnderneath(raycast, 5))
            {
                EnterWalkAirborne();
                return;
            }
        }
        DetectPlayerPosition();
    }

    private void FlyLeftAndRightTryingToLookSmart()
    {
        //player is in another chunk, let's fly right and left
        velocity.x = this.facingRight ? WalkSpeed : -WalkSpeed;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        AdvanceFlyPhysics(raycast);
    }

    // update camera angle
    private void DetectPlayerPosition()
    {
        var playerRelative = this.map.player.position - this.position;

        float currentAngle = cameraCone.coneCenterlineAngle;
        if (playerRelative.x > playerRelative.y)
        {
            currentAngle = Util.Slide(currentAngle, 0, 5);
            if (cameraOrientation != CameraOrientation.Horizontal)
            {
                cameraOrientation = CameraOrientation.Horizontal;
                if (this.state == State.FlyAround)
                {
                    this.animated.PlayAndLoop(animationFly);
                    this.animated.Synchronize(this.map);
                }
            }
        }
        else
        {
            currentAngle = Util.Slide(currentAngle, 90, 5);
            if (cameraOrientation != CameraOrientation.Vertical)
            {
                cameraOrientation = CameraOrientation.Vertical;
                if (this.state == State.FlyAround)
                {
                    this.animated.PlayAndLoop(animationFlyLookUp);
                    this.animated.Synchronize(this.map);
                }
            }
        }
        //this.raysRotationOrigin.transform.localRotation = Quaternion.Euler(0, 0, currentAngle);
        cameraCone.coneCenterlineAngle = currentAngle;
    }

    private void SlideCameraToAngle(float targetAngle)
    {
        float currentAngle = this.raysRotationOrigin.transform.localRotation.eulerAngles.z;
        currentAngle = Util.Slide(currentAngle, targetAngle, 5);
        this.raysRotationOrigin.transform.localRotation = Quaternion.Euler(0, 0, currentAngle);
    }

    private void AdvanceFlyPhysics(Raycast raycast)
    {
        var right = Raycast.CombineClosest(
           raycast.Horizontal(this.position.Add(0, -1), true, 0.75f),
           raycast.Horizontal(this.position.Add(0, 1), true, 0.75f)
       );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -1), false, 0.75f),
            raycast.Horizontal(this.position.Add(0, 1), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), false, 0.75f)
        );

        if (this.velocity.x > 0)
        {
            if (right.anything == true)
            {
                EnterTurnFly();
            }
        }
        else
        {
            if (left.anything == true)
            {
                EnterTurnFly();
            }
        }

        this.position += this.velocity;
        this.transform.position = this.position;
    }

    private bool CheckForFloorUnderneath(Raycast raycast, float maxDistance)
    {
        var downLeft = raycast.Vertical(this.position.Add(-1.1f, 0), false, maxDistance);
        var down = raycast.Vertical(this.position.Add(0, 0), false, maxDistance);
        var downRight = raycast.Vertical(this.position.Add(1.1f, 0), false, maxDistance);

        if (downLeft.anything && downRight.anything && down.anything)
        {
            //check if floor is even
            if (Mathf.Abs(downLeft.distance - down.distance) < 1 && Mathf.Abs(downLeft.distance - downRight.distance) < 1)
            {
                return true;
            }
        }
        return false;
    }

    private void ShootProjectile(ShootDirection direction)
    {
        Vector2[] offsets = { new Vector2(0.19f, 1), new Vector2(0, -1), new Vector2(-1, 0), new Vector2(1, 0) };
        Vector2[] velocities = { new Vector2(0.00001f, 2f / 16), new Vector2(0.00001f, -2f / 16), new Vector2(-2f / 16, 0.00001f), new Vector2(2f / 16, 0.00001f) };

        int index = 2;  //left

        if (direction == ShootDirection.Down)
        {
            index = 1;
        }
        else if (direction == ShootDirection.Up)
        {
            if (!this.facingRight)
                offsets[0].x *= -1;
            index = 0;
        }
        else if (direction == ShootDirection.Right)
        {
            index = 3;
        }

        var rocket = BeetleSoldierRocket.Create(
                this.position + offsets[index],
                this.chunk,
                this.facingRight
            );
        rocket.velocity = velocities[index];

        if (direction == ShootDirection.Up || direction == ShootDirection.Down)
        {
            float angle = Vector3.SignedAngle(Vector3.right, velocities[index], Vector3.forward);
            angle = Util.WrapAngle0To360(angle) * (this.facingRight ? 1 : -1);
            rocket.transform.localRotation = Quaternion.Euler(0, 0, angle);
        }
    }

    private void PlayerSpottedMidAir()
    {
        if (this.shootTimer <= 0)
        {
            if (cameraOrientation == CameraOrientation.Vertical)
            {
                EnterShoot(ShootDirection.Up);
            }
            else
            {
                EnterShoot(this.facingRight ? ShootDirection.Right : ShootDirection.Left);
            }

            this.map.desertAlertState.ActivateAlert(this.position);
        }
    }

    private bool IsSoldierFlying()
    {
        return (this.state == State.FlyAround
                || this.state == State.TurnFly
                || this.state == State.Shoot
                );
    }

    private bool IsSoldierAlerted()
    {
        return (this.state == State.FlyAround
                || this.state == State.TurnFly
                || this.state == State.Shoot
                || this.state == State.Surprised
                || this.state == State.AlertedWalk
                || this.state == State.TurnAlertedWalk
                );
    }

    private bool IsPlayerInSameChunkAsUs()
    {
        return this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position);
    }

    private bool IsPlayerHiding()
    {
        if (this.map.player.state == Player.State.InsideBox)
        {
            if (this.map.player.GetBox() != null)
            {
                if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                {
                    return true;
                }
                else
                {
                    this.map.player.ExitBox();
                    return false;
                }
            }

            if (this.map.player.GetHidingPlace() != null)
            {
                if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                {
                    return true;
                }
                else
                {
                    this.map.player.ExitHidingPlace();
                    return false;
                }
            }
        }
        return false;
    }
}