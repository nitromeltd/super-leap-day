﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blob : WalkingEnemy
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationJumpPrepare;
    public Animated.Animation animationJumpUp;
    public Animated.Animation animationJumpDown;
    public Animated.Animation animationJumpLand;
    public Animated.Animation animationTurn;

    [Header("Particle")]
    public Animated.Animation animationBurst;

    private enum State
    {
        Idle,
        Prepare,
        Jump,
        Land,
        Turn
    };

    private State state;
    private int jumpTimer;
    private int turnTimer;
    private int alertTimer;
    private bool turningAnimationPlaying;
    private bool insideWater = false;

    private const int JumpTimePeriod = 90;
    private const int TurnNoticeTime = 25;
    private const int AlertTime = 10;
    private const float GravityForce = 0.03125f;
    private const float GravityForceUnderwater = 0.01f;
    
    private float CurrentGravityForce => this.insideWater == true ?
        GravityForceUnderwater : GravityForce;
    
    public override void Init(Def def)
    {
        base.Init(def);
        
        this.turnSensorForward = .3f;
        this.turnAtEdges = false;
        this.turningAnimationPlaying = false;
        
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

        this.jumpTimer = JumpTimePeriod;
        this.turnTimer = TurnNoticeTime;

        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };

        this.onTurnAround = OnTurnAround;

        EnterIdle();
    }

    public override void Advance()
    {
        AdvanceWater();

        this.velocity.y -= CurrentGravityForce;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer();

        switch(state)
        {
            case State.Idle:
                AdvanceIdle();
                break;

            case State.Prepare:
                AdvancePrepare();
                break;

            case State.Jump:
                AdvanceJump();
                break;

            case State.Land:
                AdvanceLand();
                break;

            case State.Turn:
                AdvanceTurn();
                break;
        }
        
        this.transform.position = this.position;
    }

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    private void AdvanceIdle()
    {
        if(IsPlayerInOppositeFacingDirection() == true)
        {
            if(this.turnTimer > 0)
            {
                this.turnTimer -= 1;
            }
            else
            {
                EnterTurn();
                return;
            }
        }
        else
        {
            this.turnTimer = TurnNoticeTime;
        }

        if(this.jumpTimer > 0)
        {
            this.jumpTimer -= 1;
        }
        else
        {
            EnterPrepare();
            return;
        }
    }

    private void AdvancePrepare()
    {
        // nothing
    }

    private void AdvanceJump()
    {
        Animated.Animation jumpAnimation = this.velocity.y > 0f ?
            this.animationJumpUp : this.animationJumpDown;

        this.animated.PlayAndLoop(jumpAnimation);

        if(this.onGround == true && this.velocity.y <= 0f)
        {
            EnterLand();
        }
    }

    private void AdvanceLand()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, .01f);
    }

    private void AdvanceTurn()
    {
        if(turningAnimationPlaying == true) return;

        if(alertTimer > 0)
        {
            alertTimer --;
        }
        else
        {
            this.facingRight = !this.facingRight;

            this.onTurnAround?.Invoke(new TurnInfo {
                raycastResults = null, turnReason = TurnReason.Player
            });
        }
    }

    private void EnterIdle()
    {
        this.state = State.Idle;
        this.velocity.x = 0f;
        this.animated.PlayAndLoop(this.animationIdle);
    }

    private void EnterPrepare()
    {
        this.state = State.Prepare;
        this.animated.PlayOnce(this.animationJumpPrepare, EnterJump);

        this.animated.OnFrame(6, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxBlobJump, position: this.position);
        });
    }

    private void EnterJump()
    {
        this.state = State.Jump;
        this.jumpTimer = JumpTimePeriod;
        this.velocity = new Vector2(this.facingRight ? 0.1f : -0.1f, 0.5f);
    }

    private void EnterLand()
    {
        this.state = State.Land;
        this.animated.PlayOnce(this.animationJumpLand, EnterIdle);
    }

    private void EnterTurn()
    {
        this.state = State.Turn;
        this.turnTimer = TurnNoticeTime;
        this.alertTimer = AlertTime;
        this.velocity = Vector2.up * 0.3f;
    }

    public override bool Kill(KillInfo info)
    {
        if(info.isBecauseLevelIsResetting == false)
        {
            Particle p = Particle.CreateAndPlayOnce(
                animationBurst, this.position, this.transform.parent);
        }

        return base.Kill(info);
    }
    
    private bool IsPlayerInOppositeFacingDirection()
    {
        float blobHorizontalPos = this.position.x + (1f * (this.facingRight ? -1f : 1f));

        bool isPlayerToTheRight =
            Mathf.Sign(blobHorizontalPos - this.map.player.position.x) < 0;

        return this.facingRight != isPlayerToTheRight;
    }
    
    private void OnTurnAround(TurnInfo turnInfo)
    {
        if(this.turningAnimationPlaying == true) return;
        
        switch(turnInfo.turnReason)
        {
            case TurnReason.Player:
            {
                this.turningAnimationPlaying = true;
                this.animated.PlayOnce(this.animationTurn, () =>
                {
                    this.turningAnimationPlaying = false;
                    EnterIdle();
                });
            }
            break;
        }

        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }
}
