using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlowingEnemy : Enemy
{
    public Animated spriteAnimated;
    public Animated.Animation animationFaceNeutral;
    public Animated.Animation animationFaceAngry;
    public Animated.Animation animationBlowingForward;
    public GameObject fanForwardGO;
    public Transform fanForwardPoint;
    public Animated.Animation animationBlowingUpwards;
    public GameObject fanUpwardsGO;
    public Transform fanUpwardsPoint;
    public Animated.Animation animationBlowingDownwards;
    public GameObject fanDownwardsGO;
    public Transform fanDownwardsPoint;

    [Header("Wind Effect")]
    public Animated.Animation animationWindEffect;
    public AnimationCurve particleFadeOutCurve;

    private enum State
    {
        Patrolling,
        ApproachPlayer,
        //PrepareBlow,
        Blowing
    }

    private State state = State.Patrolling;
    private NitromeEditor.Path path;
    private float currentPathTime;
    private float targetPathTime;
    private float offsetThroughPath;
    private bool moveForward = true;
    private float visibleAngleDegrees;
    private bool facingRight = false;

    private const float DistanceToApproachPlayer = 10f;
    private const float DistanceToGoBackPatrolling = 16f;
    private const float DistanceToBlow = 8f;
    private const float BlowForceWithoutParachute = 0.045f;
    private const float BlowForceWithParachute = 0.090f;

    private const float BlowMaxDistance = 6f;
    
    private static Vector2 HitboxSize = new Vector2(0.5f, 0.5f);
    private static Vector2 HitboxOffset = Vector2.zero;

    private float DistanceToPlayer => Vector2.Distance(this.position, this.map.player.position);

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(
                HitboxOffset.x - HitboxSize.x,
                HitboxOffset.x + HitboxSize.x,
                HitboxOffset.y - HitboxSize.y,
                HitboxOffset.y + HitboxSize.y,
                this.rotation, Hitbox.NonSolid)
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.facingRight = !def.tile.flip;
        
        this.path = chunk.pathLayer.NearestPathTo(this.position, 1.5f);
        if (this.path != null)
        {
            if (this.path.closed == false)
            {
                this.path = Util.MakeClosedPathByPingPonging(this.path);
            }
        }

        ChangeState(State.Patrolling);
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true) return;
        
        switch(this.state)
        {
            case State.Patrolling:
                AdvancePatrolling();
                break;

            case State.ApproachPlayer:
                AdvanceApproachPlayer();
                break;

            case State.Blowing:
                AdvanceBlowing();
                break;
        }
        
        EnemyHitDetectionAgainstPlayer();

        // position
        this.position = this.path.PointAtTime(this.currentPathTime).position;
        this.transform.position = this.position;

        // scale
        this.spriteAnimated.transform.localScale = new Vector3(
            this.facingRight ? -1f : 1f, 1f, 1f
        );

        // rotation
        this.rotation = Mathf.RoundToInt(this.visibleAngleDegrees);
        this.transform.localRotation = Quaternion.Euler(
            0, 0, this.rotation
        );
    }

    private void AdvancePatrolling()
    {
        // move along path
        float moveSpeed = 1f * (this.moveForward ? 1f : -1f);
        this.currentPathTime += (moveSpeed / 60f);
        this.currentPathTime %= this.path.duration;
        
        float timeNextFrame = this.currentPathTime + (moveSpeed / 60f);
        Vector2 posNextFrame = this.path.PointAtTime(timeNextFrame).position;

        Vector2 heading = posNextFrame - this.position;
        float distance = heading.magnitude;
        Vector2 directionNextFrame = heading / distance;

        RefreshPathFacingDirection(directionNextFrame);

        if(DistanceToPlayer < DistanceToApproachPlayer)
        {
            ChangeState(State.ApproachPlayer);
        }
    }

    private void AdvanceApproachPlayer()
    {
        Vector2 targetPosition = this.map.player.position;
        MoveToClosestPointAlongPath(targetPosition: targetPosition);

        if(DistanceToPlayer > DistanceToGoBackPatrolling)
        {
            ChangeState(State.Patrolling);
        }
        else if(DistanceToPlayer < DistanceToBlow)
        {
            ChangeState(State.Blowing);
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyBlowerEnemyWind,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
        }
    }

    private void RotationFacePlayer()
    {
        Vector2 targetPosition = this.map.player.position;
        Vector2 heading = targetPosition - this.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;

        float targetAngleRadians =
            Mathf.Atan2(direction.y, direction.x);

        float targetAngleDegrees = targetAngleRadians * Mathf.Rad2Deg;

        this.visibleAngleDegrees = Util.WrapAngleMinus180To180(
            visibleAngleDegrees - targetAngleDegrees
        ) + targetAngleDegrees;

        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, targetAngleDegrees, 15f
        );
    }

    private void MoveToClosestPointAlongPath(Vector2 targetPosition)
    {
        NitromeEditor.Path.PointOnPathInfo targetPointInfo =
            this.path.NearestPointTo(targetPosition).Value;

        targetPathTime = targetPointInfo.timeSeconds;

        float t1 = targetPathTime;
        float t2 = this.path.duration - targetPathTime;

        float d1 = Mathf.Abs(t1 - this.currentPathTime);
        float d2 = Mathf.Abs(t2 - this.currentPathTime);

        if(d1 > d2)
        {
            targetPathTime = t2;
        }

        this.currentPathTime = Util.Slide(this.currentPathTime, targetPathTime, 0.05f);

        // update sprite
        bool forward = targetPathTime >= this.currentPathTime;
        float moveSpeed = 1f * (forward ? 1f : -1f);
        float timeNextFrame = this.currentPathTime + (moveSpeed / 60f);
        Vector2 posNextFrame = this.path.PointAtTime(timeNextFrame).position;

        Vector2 heading = posNextFrame - this.position;
        float distance = heading.magnitude;
        Vector2 directionNextFrame = heading / distance;

        if(directionNextFrame.x == 0f)
        {
            if(this.facingRight == true && targetPosition.x < this.position.x)
            {
                this.facingRight = false;
            }

            if(this.facingRight == false && targetPosition.x > this.position.x)
            {
                this.facingRight = true;
            }
        }
        else
        {
            RefreshPathFacingDirection(directionNextFrame);
        }
    }

    private void RefreshPathFacingDirection(Vector2 direction)
    {
        if(this.facingRight == true && direction == Vector2.left)
        {
            this.facingRight = false;
        }

        if(this.facingRight == false && direction == Vector2.right)
        {
            this.facingRight = true;
        }
    }

    private void AdvanceBlowing()
    {
        var playerPosition = this.map.player.position;
        var headingToPlayer = playerPosition - this.position;
        float distanceToPlayer = headingToPlayer.magnitude;
        var directionToPlayer = headingToPlayer / distanceToPlayer;
        
        float angleRadians =
            Mathf.Atan2(directionToPlayer.y, directionToPlayer.x);
        float angleDegrees = angleRadians * Mathf.Rad2Deg;

        var detectRaycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
        );
        var distanceToSolid = detectRaycast.Arbitrary(
            this.position, directionToPlayer, BlowMaxDistance
        ).distance;

        // detect player
        var furthestPoint = this.position + (directionToPlayer * distanceToSolid);
        var ray = directionToPlayer * distanceToSolid;

        var along = Vector2.Dot(
            this.map.player.position - this.position, ray
        ) / ray.sqrMagnitude;
        along = Mathf.Clamp01(along);

        var nearestPointAlongRayToPlayer =
            Vector2.Lerp(this.position, furthestPoint, along);

        if ((nearestPointAlongRayToPlayer - this.map.player.position).magnitude < 1)
        {
            Vector2 targetPosition = this.map.player.position;
            Vector2 heading = targetPosition - this.position;
            float distance = heading.magnitude;
            Vector2 direction = heading / distance;

            Player player = this.map.player;
            float blowForce = (player.state == Player.State.UsingParachuteOpened) ?
                BlowForceWithParachute : BlowForceWithoutParachute;
            this.map.player.velocity += direction * blowForce;
        }

        if(this.facingRight == false && directionToPlayer.x > 0f)
        {
            this.facingRight = true;
        }

        if(this.facingRight == true && directionToPlayer.x < 0f)
        {
            this.facingRight = false;
        }

        Vector2 particlePosition = this.position;
        
        if(directionToPlayer.y > 0.50f)
        {
            this.spriteAnimated.PlayAndLoop(this.animationBlowingUpwards);
            this.fanUpwardsGO.SetActive(true);
            this.fanDownwardsGO.SetActive(false);
            this.fanForwardGO.SetActive(false);

            particlePosition = this.fanUpwardsPoint.position;
        }
        else if(directionToPlayer.y < -0.50f)
        {
            this.spriteAnimated.PlayAndLoop(this.animationBlowingDownwards);
            this.fanUpwardsGO.SetActive(false);
            this.fanDownwardsGO.SetActive(true);
            this.fanForwardGO.SetActive(false);
            
            particlePosition = this.fanDownwardsPoint.position;
        }
        else
        {
            this.spriteAnimated.PlayAndLoop(this.animationBlowingForward);
            this.fanUpwardsGO.SetActive(false);
            this.fanDownwardsGO.SetActive(false);
            this.fanForwardGO.SetActive(true);
            
            particlePosition = this.fanForwardPoint.position;
        }

        if(this.map.frameNumber % 20 == 0)
        {
            Particle windParticle = Particle.CreateAndPlayOnce(
                //this.animationWindEffect, 35, particlePosition, null
                this.animationWindEffect, particlePosition, null
            );

            windParticle.transform.rotation = Quaternion.Euler(
                0, 0, Mathf.RoundToInt(angleDegrees)
            );

            windParticle.velocity = directionToPlayer * 0.25f; //0.525f;
            windParticle.acceleration = directionToPlayer * -0.0075f; //-0.015f;
            //windParticle.FadeOut(this.particleFadeOutCurve);
        }

        if(DistanceToPlayer > DistanceToGoBackPatrolling)
        {
            ChangeState(State.Patrolling);
        }
    }

    private void ChangeState(State newState)
    {
        switch(this.state)
        {
            case State.Blowing:
                ExitBlowing();
                break;
        }

        this.state = newState;

        switch(this.state)
        {
            case State.Patrolling:
                EnterPatrolling();
                break;

            case State.ApproachPlayer:
                EnterApproachPlayer();
                break;

            case State.Blowing:
                EnterBlowing();
                break;
        }
    }

    private void EnterPatrolling()
    {
        this.spriteAnimated.PlayAndLoop(this.animationFaceNeutral);
        this.moveForward = this.targetPathTime >= this.currentPathTime;
    }

    private void EnterApproachPlayer()
    {
        // empty
    }

    private void EnterBlowing()
    {
        // empty
    }

    private void ExitBlowing()
    {
        this.fanUpwardsGO.SetActive(false);
        this.fanDownwardsGO.SetActive(false);
        this.fanForwardGO.SetActive(false);
    }
}
