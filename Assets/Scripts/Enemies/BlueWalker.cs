
using UnityEngine;

public class BlueWalker : WalkingEnemy
{
    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };
        this.velocity = new Vector2(1 / 16f, 0);
    }

    override public void Advance()
    {
        this.velocity.y -= 0.5f / 16;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());

        this.transform.position = this.position;

        if (this.velocity.x > 0) this.transform.localScale = new Vector3(+1, 1, 1);
        if (this.velocity.x < 0) this.transform.localScale = new Vector3(-1, 1, 1);
    }
}
