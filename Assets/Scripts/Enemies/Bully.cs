using UnityEngine;

public class Bully : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationAlert;
    public Animated.Animation animationCharge;
    public Animated.Animation animationImpact;
    public Animated.Animation animationWobble;

    private enum State
    {
        Walk,
        Turn,
        Alert,
        Charge,
        Wobble,
        Airborne,
        Knockback
    };
    
    private State state = State.Walk;
    private float visibleAngleDegrees = 0f;
    private bool isTurning = false;
    private bool snapToTargetRotation = false;
    private int chargeCooldownTime = 0;
    private bool isLastChargeDirectionRight = false;
    private bool isFirstCharge = true;
    private int chargeTime = 0;
    private int knockbackTime = 0;
    private bool shouldTurnAfterKnockback;

    private const int MaxPlayerVerticalDistance = 4;
    private const int MaxPlayerHorizontalDistance = 6;
    private const float WalkSpeed = 0.025f;
    private const float ChargeSpeed = 0.1f;
    private const int SameDirectionChargeCooldownTime = 60;
    private const int KnockbackCooldownTime = 40;
    private const float KnockbackForceX = 0.10f;
    private const float KnockbackForceY = 0.2f;
    private const float GravityForce = 0.03125f;
    private static Vector2 HitWallPushbackForce = new Vector2(-0.1f, 0.2f);
    
    private bool IsAirborne => this.onGround == false;
    private bool IsInsideLava => this.position.y < this.map.lavaline?.currentLevel;

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.turnSensorForward = .3f;
        this.edgeTurnSensorForward = -.45f;
        
        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = WalkSpeed * initialDirectionValue;

        this.hitboxes = new [] {
            new Hitbox(-1.1f, 1.1f, -1.35f, 1.35f, this.rotation, Hitbox.NonSolid)
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.onTurnAround = OnTurnAround;

        ChangeState(State.Walk);
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        switch(this.state)
        {
            case State.Walk:
            {
                ChangeState(State.Turn);
                break;
            }

            case State.Charge:
            {
                if (turnInfo.turnReason == TurnReason.Wall)
                {
                    Audio.instance.PlaySfx(
                        Assets.instance.moltenFortress.sfxBullySlam, null, this.position
                    );
                    this.shouldTurnAfterKnockback = true;
                    this.facingRight = turnInfo.facingRightBefore;
                    ChangeState(State.Knockback);
                    CauseDamageImpactingWall();
                }
                else if (turnInfo.turnReason == TurnReason.Ledge)
                {
                    ChangeState(State.Wobble);
                }
                break;
            }

            case State.Knockback:
            {
                this.velocity.x = turnInfo.velocityXBefore;
                this.facingRight = turnInfo.facingRightBefore;
                break;
            }
        }
    }
    
    private void CauseDamageImpactingWall()
    {
        var breakRect = this.hitboxes[0].InWorldSpace();
        if (this.facingRight == true)
        {
            breakRect.xMin = breakRect.xMax;
            breakRect.xMax += 0.5f;
        }
        else
        {
            breakRect.xMax = breakRect.xMin;
            breakRect.xMin -= 0.5f;
        }
        this.map.BreakBreakableItems(breakRect);
    }

    public override void Advance()
    {
        if (IsInsideLava == true)
        {
            Kill(new KillInfo());
        }

        if (this.deathAnimationPlaying == true)
            return;
        
        this.velocity.y -= GravityForce;
        
        if (IsAirborne == true
            && (state != State.Knockback && state != State.Airborne))
        {
            ChangeState(State.Airborne);
        }

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.Turn:
                AdvanceTurn();
                break;

            case State.Alert:
                AdvanceAlert();
                break;

            case State.Charge:
                AdvanceCharge();
                break;

            case State.Wobble:
                AdvanceWobble();
                break;

            case State.Airborne:
                AdvanceAirborne();
                break;

            case State.Knockback:
                AdvanceKnockback();
                break;
        }

        CheckCollisionWithPlayer();

        this.transform.position = this.position;
        RefreshRotation();
    }

    private void CheckCollisionWithPlayer()
    {
        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        var ownRectangle = this.hitboxes[0].InWorldSpace();
        
        if (player.ShouldCollideWithEnemies() == false) return;
        if (ownRectangle.Overlaps(playerRectangle) == false) return;
        
        if (player.invincibility.isActive == true)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else
        {
            var playerAbove =
                playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
                (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f ||
                (playerRectangle.yMin - player.VelocityLastFrame.y) > ownRectangle.yMax - 0.1f;
            
            var playerGoingDown =
                player.velocity.y < this.velocity.y ||
                player.VelocityLastFrame.y < this.velocity.y;

            Audio.instance.PlaySfx(
                Assets.instance.moltenFortress.sfxBullySlam, null, this.position
            );
            ChangeState(State.Knockback);

            var bottom = new Vector2(this.position.x, this.hitboxes[0].InWorldSpace().yMin);
            if (playerAbove == true && playerGoingDown == true)
                player.BounceOffHitEnemy(bottom);
            else
                player.Hit(bottom);
        }
    }
    
    private void ChangeState(State newState)
    {
        // Exit old state methods
        switch (this.state)
        {
            case State.Charge:
                ExitCharge();
                break;

            case State.Knockback:
                ExitKnockback();
            break; 
        }

        this.state = newState;

        // Enter new state methods
        switch (this.state)
        {
            case State.Walk:
                EnterWalk();
                break;

            case State.Turn:
                EnterTurn();
                break;

            case State.Alert:
                EnterAlert();
                break;

            case State.Charge:
                EnterCharge();
                break;

            case State.Wobble:
                EnterWobble();
                break;

            case State.Airborne:
                EnterAirborne();
                break;

            case State.Knockback:
                EnterKnockback();
                break; 
        }
    }

#region ADVANCE METHODS
    private void AdvanceWalk()
    {
        if (this.chargeCooldownTime > 0)
            this.chargeCooldownTime -= 1;
            
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        //base.Advance();

        if (IsDetectingPlayer())
        {
            if((this.facingRight && this.map.player.position.x < this.position.x) ||
                (!this.facingRight && this.map.player.position.x > this.position.x))
            {
                this.velocity.x *= -1;
                this.facingRight = (this.velocity.x > 0);
                this.onTurnAround?.Invoke(new TurnInfo {
                    raycastResults = null, turnReason = TurnReason.Player
                });

                // turn around before charging at the player
                return;
            }

            ChangeState(State.Alert);
        }

        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    private bool IsDetectingPlayer()
    {
        var playerRelative = this.map.player.position - this.position;

        if(!this.map.player.alive)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        if(this.isTurning)
        {
            return false;
        }

        if(IsChargingToSameDirectionAgain()
            && this.chargeCooldownTime > 0)
        {
            return false;
        }

        return true;
    }

    private bool IsChargingToSameDirectionAgain()
    {
        if(this.isFirstCharge)
        {
            return false;
        }

        bool isCurrentChargeDirectionRight = 
            this.position.x < this.map.player.position.x;

        return this.isLastChargeDirectionRight == isCurrentChargeDirectionRight;
    }

    private void AdvanceTurn()
    {
        this.velocity.x = 0;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast, stopHorizontally: true);
    }

    private void AdvanceAlert()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        
        Move(raycast, stopHorizontally: true);
    }

    private void AdvanceCharge()
    {
        this.chargeTime += 1;

        if(this.chargeTime > 60 * 10)
        {
            this.chargeTime = 0;
            ChangeState(State.Wobble);

            return;
        }

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        
        if (this.map.frameNumber % 2 == 0)
        {
            CreateDustParticles();
        }
    }

    private void CreateDustParticles()
    {
        float x = this.position.x;
        float y = this.position.y - 0.50f;

        int lifetime = UnityEngine.Random.Range(25, 35);

        Particle p = Particle.CreatePlayAndHoldLastFrame(
            this.map.player.animationDustParticle,
            lifetime,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.50f, 0.80f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.1f, 0.1f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void AdvanceWobble()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast, stopHorizontally: true);
    }

    private void AdvanceAirborne()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);

        if(IsAirborne == false)
        {
            ChangeState(State.Walk);
        }
    }

    private void AdvanceKnockback()
    {
        var facingRightBefore = this.facingRight;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);

        this.facingRight = facingRightBefore;
        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.006f);

        if (this.knockbackTime > 0)
        {
            this.knockbackTime -= 1;
        }
        else if (this.velocity.x == 0)
        {
            if (this.shouldTurnAfterKnockback == true)
            {
                this.shouldTurnAfterKnockback = false;
                this.facingRight = !this.facingRight;
                ChangeState(State.Turn);
            }
            else if (this.onGround == true)
                ChangeState(State.Walk);
            else
                ChangeState(State.Airborne);
        }
    }
#endregion

#region ENTER METHODS
    private void EnterWalk()
    {
        this.velocity.x = WalkSpeed * (this.facingRight ? 1f : -1f);
        this.snapToTargetRotation = false;

        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterTurn()
    {
        // this.facingRight = !this.facingRight;
        RefreshScale();

        this.isTurning = true;
        this.animated.PlayOnce(this.animationTurn, () =>
        {
            this.isTurning = false;
            ChangeState(State.Walk);
        });
    }

    private void EnterAlert()
    {
        Audio.instance.PlaySfx(
            Assets.instance.moltenFortress.sfxBullyAggro, null, this.position
        );

        this.animated.PlayOnce(this.animationAlert, () =>
        {
            ChangeState(State.Charge);
        });
    }

    private void EnterCharge()
    {
        Audio.instance.PlaySfx(
            Assets.instance.moltenFortress.sfxBullyRun, null, this.position
        );

        this.velocity.x = ChargeSpeed * Mathf.Sign(this.velocity.x);
        this.animated.PlayAndLoop(this.animationCharge);
        this.animated.OnFrame(2, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxJabbajawChomp, position: this.position);
        });
        this.chargeCooldownTime = SameDirectionChargeCooldownTime;
        this.isLastChargeDirectionRight = this.facingRight;
        this.isFirstCharge = false;
        this.chargeTime = 0;
    }

    private void EnterImpact()
    {
        float horizontalPushbackForce =
            Random.Range(-0.05f, 0.1f) + HitWallPushbackForce.x;
        float verticalPushbackForce =
            Random.Range(-0.035f, 0.035f) + HitWallPushbackForce.y;

        this.velocity.x = horizontalPushbackForce * Mathf.Sign(this.velocity.x);
        this.velocity.y = verticalPushbackForce;
        this.animated.PlayAndHoldLastFrame(this.animationImpact);
        
        var p = Particle.CreateWithSprite(
                Assets.instance.jabbajawHitWallParticle,
                8,
                this.position.Add(1 * Mathf.Sign(this.velocity.x), -.5f),
                this.map.transform
            );

        p.transform.localScale = new Vector3(0.7f, 1.3f, p.transform.localScale.z);
        p.scale = .05f;

        this.map.ScreenShakeAtPosition(this.position, new Vector2(.3f, 0.5f));
        Audio.instance.PlaySfx(Assets.instance.sfxJabbajawHitsWall,position: this.position);

        this.animated.PlayOnce(this.animationImpact, () =>
        {
            ChangeState(State.Walk);
        });
        
        this.snapToTargetRotation = true;
    }

    private void EnterWobble()
    {
        Audio.instance.PlaySfx(
            Assets.instance.moltenFortress.sfxBullyWobble, null, this.position
        );

        this.animated.PlayOnce(this.animationWobble, () =>
        {
            ChangeState(State.Walk);
        });
    }

    private void EnterAirborne()
    {
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterKnockback()
    {
        this.knockbackTime = KnockbackCooldownTime;
        this.turnAtEdges = false;

        this.animated.PlayAndHoldLastFrame(this.animationImpact);

        this.velocity.x = (this.facingRight ? -1 : 1) * KnockbackForceX;
        this.velocity.y = KnockbackForceY;
    }

    private void ExitCharge()
    {
        Audio.instance.StopSfx(Assets.instance.moltenFortress.sfxBullyRun);
    }

    private void ExitKnockback()
    {
        this.turnAtEdges = true;

        if (this.facingRight == true && this.velocity.x < 0.01f)
            this.velocity.x = 0.01f;
        if (this.facingRight == false && this.velocity.x > -0.01f)
            this.velocity.x = -0.01f;
    }
#endregion

    // private void Turn()
    // {
    //     RefreshScale();

    //     this.isTurning = true;
    //     this.animated.PlayOnce(this.animationTurn, () =>
    //     {
    //         this.isTurning = false;
    //         ChangeState(State.Walk);
    //     });
    // }

    protected void RefreshScale()
    {
        if (this.facingRight == true) this.transform.localScale = new Vector3(+1, 1, 1);
        if (this.facingRight == false) this.transform.localScale = new Vector3(-1, 1, 1);
    }

    private void RefreshRotation()
    {
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;

        this.visibleAngleDegrees = this.snapToTargetRotation ?
            this.groundAngleDegrees :
            Util.Slide(this.visibleAngleDegrees, this.groundAngleDegrees, 3);

        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }
}
