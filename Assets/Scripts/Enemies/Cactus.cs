﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cactus : WalkingEnemy
{
    public Animated.Animation animationIdle;    
    public Animated.Animation animationPrepareToJump;
    public Animated.Animation animationJumpUp;
    public Animated.Animation animationJumpDown;
    public Animated.Animation animationLand;
    public Animated.Animation animationTurn;
    public Animated.Animation animationWalk;
    public Animated.Animation animationShoot;
    public Animated.Animation animationExplode;

    private enum State
    {
        Idle,
        Jump,
        Land,
        Explode
    }
    public CameraCone cameraCone;

    private State state;
    private int jumpTimer;
    private int playerSeenCooldown;
    private int exclamationMarkCounter;
    private bool shouldCreateExclamationMark = false;
    private bool alerted;
    private int alertDelay;

    private const int MaxPlayerVerticalDistance = 2;
    private const int MaxPlayerHorizontalDistance = 16;
    private const float WalkVelocity = 0.2f / 16;    
    private const int JumpDelay = 30;
    private const int PlayerSeenDelay = 60*5;

    private Vector3 localOffsetPrepareToJump = new Vector3(0.5f, 0.2f);
    private Vector3 localOffsetLand = new Vector3(0.5f, 0.3f);
    private Vector3 localOffsetIdle = new Vector3(0.5f, 0.62f);

    public override void Init(Def def)
    {
        base.Init(def);
        this.turnSensorForward = .3f;
        this.onTurnAround = EnterTurn;
        this.facingRight = !def.tile.flip;
        this.velocity = Vector2.zero;
        this.turnAtEdges = false;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.hitboxes = new [] {
            new Hitbox(-1.0f, 1.0f, -1.0f, 1.0f, this.rotation, Hitbox.NonSolid)
        };
        this.shouldCreateExclamationMark = false;
        this.alerted = false;
        EnterIdle();
        
        this.cameraCone.Init(this.map);
        this.cameraCone.SetCallbacks(ShouldCameraActivate, onPlayerSpotted:PlayerSpotted);
        this.alertDelay = Random.Range(20, 60);
    }

    public override void Advance()
    {
        // DebugDrawing.Draw(false, true);
        this.velocity.y -= 0.5f / 16;       
        base.Advance();
        this.cameraCone.Advance();

        switch(this.state)
        {
            case State.Idle:
                ExplodeIfPlayerIsTooClose();
                AdvanceIdle();                
                break;

            case State.Jump:
                AdvanceJump();
                break;

            case State.Land:
                AdvanceLand();
                break;

            default:
                break;
        }

        if(this.playerSeenCooldown > 0)
        {
            this.playerSeenCooldown -= 1;
        }

        if(!this.alerted)
        {
            if(this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay)
            {
                this.alerted = true;
                PlayerSpotted();
            }
            else if(this.map.desertAlertState.IsGloballyActive() && this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position))
            {
                this.alerted = true;
                PlayerSpotted();
            }
        }
        else
        {
            if(!this.map.desertAlertState.IsGloballyActive())
            {
                this.alerted = false;
            }
        }

        if(animated.currentAnimation == animationPrepareToJump)
        {
            if (this.cameraCone.transform.localPosition != localOffsetPrepareToJump)
            {
                this.cameraCone.transform.localPosition = localOffsetPrepareToJump;
                this.cameraCone.RecalculateAndUpdateConeMesh();
            }
        }
        else if(animated.currentAnimation == animationLand)
        {
            if (this.cameraCone.transform.localPosition != localOffsetLand)
            {
                this.cameraCone.transform.localPosition = localOffsetLand;
                this.cameraCone.RecalculateAndUpdateConeMesh();
            }
        }
        else if(animated.currentAnimation == animationIdle)
        {
            if (this.cameraCone.transform.localPosition != localOffsetIdle)
            {
                this.cameraCone.transform.localPosition = localOffsetIdle;
                this.cameraCone.RecalculateAndUpdateConeMesh();
            }
        }   
    }

    private void ExplodeIfPlayerIsTooClose()
    {
        if(!this.onGround || this.state == State.Explode || this.playerSeenCooldown <= 0)
        {
            return;
        }
         
        if((this.map.player.position - this.position).sqrMagnitude < 5*5)
        {
            EnterExplode();
        }
    }

    private void ShootRockets()
    {
        float speed = 2f/16;
        Vector2[] offsets = { new Vector2(-1.21f, 1.21f), new Vector2(0,2), new Vector2(1.41f, 1.41f)};
        Vector2[] velocities = { new Vector2(-1, 1).normalized * speed, new Vector2(0, 1).normalized * speed, new Vector2(1, 1).normalized * speed};

        for (int i = 0; i < offsets.Length; i++)
        {
            var rocket = CactusRocket.Create(
                    this.position + offsets[i],
                    this.chunk
                );
                
            rocket.velocity = velocities[i];

            float angle = Vector3.SignedAngle(Vector3.right, offsets[i], Vector3.forward);
                angle = Util.WrapAngle0To360(angle);
                rocket.transform.localRotation = Quaternion.Euler(0, 0, angle);  
        }
    }   
    
    private void EnterTurn(TurnInfo turnInfo)
    {       
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        if(this.state != State.Idle)
        {
            return;
        }
    
        this.animated.PlayOnce(this.animationTurn, () => {
                EnterIdle();
            }); 
    }

    private void EnterLand()
    {
        this.state = State.Land;
        this.animated.PlayOnce(this.animationLand, () => { EnterIdle();});
    }

    private void EnterIdle(int delay = 0)
    {
        this.state = State.Idle;
        this.animated.PlayAndLoop(this.animationIdle);
        this.jumpTimer = (delay == 0) ? JumpDelay : delay;
    }

    private void EnterExplode()
    {
        this.velocity = Vector2.zero;
        this.state = State.Explode;
        this.animated.PlayOnce(this.animationExplode, delegate {
            DestroyAndCleanup();
        });
        this.animated.OnFrame(1, () =>
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyCactusBombExplode,
                position: this.position
            );
        });
        this.animated.OnFrame(10, () => 
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyCactusBombDebrisExploded,
                position: this.position
            );
        });
        this.animated.OnFrame(18,ShootRockets);
        
    }     

    private void AdvanceIdle()
    {
        if(this.shouldCreateExclamationMark)
        {
            this.shouldCreateExclamationMark = false;
            if (this.exclamationMarkCounter <= 0)
            {
                CreateExclamationMark();
            }
        }

        this.velocity.x = Util.Slide(this.velocity.x, 0f, .01f);
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        
        if(this.exclamationMarkCounter > 0)
        {
            this.exclamationMarkCounter -= 1;
            return;
        }

        if(this.jumpTimer > 0)
        {
            if(this.playerSeenCooldown > 0)
            {
                //kill jump timer - jump instantly
                this.jumpTimer = -1;
            }
            else
            {
                this.jumpTimer--;
            }

            if(this.jumpTimer <= 0)
            {
                if (this.facingRight == true)
                {
                    if (IsSafeToJump(raycast, 2, true, true) == false)
                    {
                        this.facingRight = false;
                        this.onTurnAround?.Invoke(new TurnInfo {
                            raycastResults = null, turnReason = TurnReason.Player
                        });
                        return;
                    }
                }
                else
                {
                    if (IsSafeToJump(raycast, 2, false, true) == false)
                    {
                        this.facingRight = true;
                        this.onTurnAround?.Invoke(new TurnInfo {
                            raycastResults = null, turnReason = TurnReason.Player
                        });
                        return;
                    }
                }

                this.animated.PlayOnce(this.animationPrepareToJump, delegate {
                    this.state = State.Jump;                   
                    this.velocity = new Vector2(this.facingRight ? 0.1f : -0.1f, 0.5f);
                });
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyCactusBombJump,
                    position: this.position
                );
            }
        }
    }    

    private void AdvanceJump()
    {
        Animated.Animation jumpAnimation = this.velocity.y > 0f ?
            this.animationJumpUp : this.animationJumpDown;

        this.animated.PlayAndHoldLastFrame(jumpAnimation);

        if(this.onGround == true && this.velocity.y <= 0f)
        {
            EnterLand();
        }
    }

    private void AdvanceLand()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, .01f);
    }

    private void DestroyAndCleanup()
    {
        Destroy(this.gameObject);
        this.chunk.items.Remove(this);
    }

    private bool ShouldCameraActivate()
    {
        if((this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay) || this.alerted)
        {
            return true;
        }
        return false;
    }

    public void PlayerSpotted()
    {
        if(this.map.player.state == Player.State.InsideBox)
        {
            if (this.map.player.GetBox() != null)
                {
                    if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitBox();
                    }
                }

                if (this.map.player.GetHidingPlace() != null)
                {
                    if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitHidingPlace();
                    }
                }
        }

        if(this.playerSeenCooldown > 0)
        {
            return;
        }
        
        this.shouldCreateExclamationMark = true;
        if (this.state != State.Jump && this.state != State.Explode && this.exclamationMarkCounter <= 0)
        {
            EnterIdle(30);
        }

        this.alerted = true;
        this.map.desertAlertState.ActivateAlert(this.position);        
        this.playerSeenCooldown = PlayerSeenDelay;
    }

    private int lastExclamationMarkCreatedOnFrame;
    private void CreateExclamationMark()
    {
        if(this.map.frameNumber - this.lastExclamationMarkCreatedOnFrame < 1*60)
        {
            return;
        }

        this.exclamationMarkCounter = 60;
        this.lastExclamationMarkCreatedOnFrame = this.map.frameNumber;
        var exclamationMark = Particle.CreateAndPlayOnce(
                    Assets.instance.exclamationMarkAnimation,
                    this.position.Add(
                        0,
                        3f
                    ),
                    this.transform.parent
                );
        exclamationMark.spriteRenderer.sortingLayerName = "Items";
        exclamationMark.spriteRenderer.sortingOrder = -1;
    }

    public override bool Kill(KillInfo info)
    {
        if (this.state != State.Explode)
            EnterExplode();
        return true;
    }
}
