using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;
public class CactusAssassin : WalkingEnemy
{
    public Animated.Animation animationTurn;
    public Animated.Animation animationWalk;
    public Animated.Animation animationReadyWeapon;
    public Animated.Animation animationLeaveAimWeapon;
    public Animated.Animation animationAimingTurn;
    public Animated.Animation animationAimingTurnOverHead;
    public CameraCone cameraCone;
    public GameObject cameraParentObject;
    public GameObject launcherRotationObject;
    public GameObject launcherObject;
    public Sprite aimSprite;
    public Sprite[] bodyRotationSprites;
    private enum State
    {
        Walk,
        Turn,
        ReadyWeapon,
        Aim,
        Shoot,
        LeaveAimWeapon,
    }
    private State state;
    private bool alerted;
     
    private int playerSeenCooldown;
    private const int PlayerSeenDelay = 60*5;
    private int alertDelay;
    private int lastExclamationMarkCreatedOnFrame;
    private int lastMissileFiredOnFrame;
    private const int ShootDelay = 60 * 2;
    private const float HorizontalSpeed = 0.02f;
    public override void Init(Def def)
    {
        base.Init(def);
        this.turnSensorForward = .3f;
        this.onTurnAround = EnterTurn;
        this.facingRight = !def.tile.flip;
        this.velocity.x = HorizontalSpeed * (this.facingRight? 1: -1);
        this.turnAtEdges = true;
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, -2.1f, 1.7f, this.rotation, Hitbox.NonSolid)
        };
         
        this.alerted = false;
        EnterWalk();
        
        this.cameraCone.Init(this.map);
        this.cameraCone.fixedCone = true;
        this.cameraCone.SetCallbacks(ShouldCameraActivate, onPlayerSpotted:PlayerSpotted);
        this.alertDelay = Random.Range(20, 60);
    }
    public override void Advance()
    {
        // DebugDrawing.Draw(false, true);
        this.velocity.y -= 0.5f / 16;       
        base.Advance();
        this.cameraCone.Advance();

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();             
                break;           
            
            case State.Aim:
                AdvanceAim();                
                break;  

            case State.Shoot:
                AdvanceShoot();                
                break; 

            default:
                break;
        }

        if(this.playerSeenCooldown > 0)
        {
            this.playerSeenCooldown -= 1;
        }

        if(!this.alerted)
        {
            if(this.map.desertAlertState.IsGloballyActive() &&  this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position) && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay)
            {
                this.alerted = true;
                PlayerSpotted();
            }
            // else 
            // if(this.map.desertAlertState.IsGloballyActive() && this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position))
            // {
            //     this.alerted = true;
            //     PlayerSpotted();
            // }
        }
        else
        {
            if(!this.map.desertAlertState.IsGloballyActive())
            {
                this.alerted = false;
                EnterLeaveAimWeapon();
            }
        }

#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.P))
        {
            EnterReadyWeapon();
        }
        if(Input.GetKeyDown(KeyCode.O))
        {
            EnterLeaveAimWeapon();
        }
        if(Input.GetKeyDown(KeyCode.L))
        {
            FireMissile();
        }
#endif

    }

    private void SlideConeLocalXPosition(float targetX)
    {
        Vector3 localPos = cameraParentObject.transform.localPosition;
        localPos.x = Util.Slide(localPos.x, targetX, 0.1f);
        cameraParentObject.transform.localPosition = localPos;
    }
    private void SlideConeLocalYPosition(float targetY)
    {
        Vector3 localPos = cameraParentObject.transform.localPosition;
        localPos.y = Util.Slide(localPos.y, targetY, 0.1f);
        cameraParentObject.transform.localPosition = localPos;
    }

    private void AdvanceWalk()
    {
        SlideConeLocalXPosition(0);
        SlideConeLocalYPosition(0);


        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    private Vector2 GetCenterPoint()
    {
        return (Vector2)this.launcherObject.transform.position;
    }

    private void AdvanceAim()
    {
        SlideConeLocalXPosition(1);
        // SlideConeLocalXPosition(0.6f);
        // SlideConeLocalYPosition(-0.9f);

        var playerRelative = this.map.player.position - (Vector2)this.transform.position;
#if UNITY_EDITOR        
        Debug.DrawLine(this.transform.position, this.transform.position + (Vector3)playerRelative.normalized * playerRelative.magnitude, Color.white, 0);
#endif        
        bool lastFacingRight = this.facingRight;
        this.facingRight = playerRelative.x > 0;

        if(lastFacingRight != this.facingRight)
        {
            if(this.map.player.position.y > this.transform.position.y)
            {
                this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
                launcherRotationObject.SetActive(false);
                this.animated.PlayOnce(animationAimingTurnOverHead, () =>
                {
                    this.animated.Stop();
                    launcherRotationObject.SetActive(true);
                    SetCorrectLauncherRotation();
                    SetCorrectBodySprite();
                });
                CreateGroundParticles(5);
            }
            else
            {
                this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
                launcherRotationObject.SetActive(false);
                this.animated.PlayOnce(animationAimingTurn, () =>
                {
                    this.animated.Stop();
                    launcherRotationObject.SetActive(true);
                    SetCorrectLauncherRotation();
                    SetCorrectBodySprite();
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnemyCactusAssassinJump,
                        position: this.position
                    );
                });
                CreateGroundParticles(5);
            }
        }

        if(this.animated.currentAnimation == animationAimingTurn || this.animated.currentAnimation == animationAimingTurnOverHead)
        {
            SetConeViewRotation();
            return;
        }
        SetCorrectLauncherRotation();
        SetCorrectBodySprite();
        SetConeViewRotation();
        ShootIfPossible();
    }

    private void AdvanceShoot()
    {
        int framesPassed = this.map.frameNumber - lastMissileFiredOnFrame;

        if(framesPassed < 5)
        {
            launcherObject.transform.localPosition = new Vector3(-0.5f, 0, 0);
        }
        else
        {
            launcherObject.transform.localPosition = Vector3.zero;
        }

        if( framesPassed > 10)
        {
            EnterAim();
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyCactusAssassinShoot,
            position: this.position
        );
    }

    private void SetConeViewRotation()
    {
        var playerRelative = this.map.player.position - GetCenterPoint();
        float angle = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));


        cameraCone.SetConeSearchMovement(false);
        float baseAngle;

        if(this.facingRight)
        {
            if(angle > 180 && angle < 300)
            {
                angle = 300;
            }

            baseAngle = Util.WrapAngleMinus180To180(angle);
        }
        else
        {
            if(angle > 240 && angle <360)
            {
                angle = 240;
            }
            baseAngle = Util.WrapAngleMinus180To180(180 - angle);
        }
 
        cameraCone.cameraSpriteObject.transform.localRotation = Quaternion.Euler(0, 0, baseAngle);
        cameraCone.coneCenterlineAngle = baseAngle;
        cameraCone.SetConeBoundaryRays();
    }

    private void SetCorrectLauncherRotation()
    {
        var playerRelative = this.map.player.position - GetCenterPoint();
        float angle = Util.WrapAngleMinus180To180(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));  

        if(this.facingRight)
        {
            // cap angle at the feet
            if(angle > -100 && angle <= -60)
            {
                angle = -60;
            }
        }
        else
        {
            if(angle > -120 && angle <= -80)
            {
                angle = -120;
            }
        }

        float newAngle = this.facingRight ? angle : 180 + angle;  
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        launcherRotationObject.transform.rotation = Quaternion.Euler(0, 0, newAngle);
    }

    private void SetCorrectBodySprite()
    {
        var playerRelative = this.map.player.position - GetCenterPoint();
        float bendAngle = Util.WrapAngleMinus180To180(Vector3.SignedAngle(Vector3.down, playerRelative, Vector3.forward));
        int frame = Mathf.FloorToInt(Mathf.Abs(bendAngle) / 180 * (bodyRotationSprites.Length - 1));
        spriteRenderer.sprite = bodyRotationSprites[frame];
    }

    private void EnterTurn(TurnInfo turnInfo)
    {       
        this.state = State.Turn;
        this.animated.PlayOnce(this.animationTurn, () => {
                EnterWalk();
            }); 
    }

    private void EnterWalk()
    {
        this.state = State.Walk;
        cameraCone.cameraSpriteObject.transform.rotation = Quaternion.Euler(0, 0, 0);
        cameraCone.coneCenterlineAngle = 0;
        
        cameraCone.SetConeBoundaryRays();
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterReadyWeapon()
    {
        this.state = State.ReadyWeapon;
        this.velocity.x =  0; 

        this.animated.PlayOnce(this.animationReadyWeapon, () => {
            
            EnterAim();
            CreateExclamationMark();
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyCactusAssassinJump,
                position: this.position
            );
        });
        this.animated.OnFrame(6, () => { CreateGroundParticles(); });
    }

    private void EnterAim()
    {
        this.state = State.Aim;
        spriteRenderer.sprite = aimSprite;
        launcherRotationObject.SetActive(true);
    }

    private void EnterShoot()
    {
        this.state = State.Shoot;
    }

    private void EnterLeaveAimWeapon()
    {
        CreateGroundParticles(5);
        launcherRotationObject.SetActive(false);
        this.state = State.LeaveAimWeapon;
        this.animated.PlayOnce(this.animationLeaveAimWeapon, () => {
            EnterWalk();
            this.velocity.x = this.facingRight ? HorizontalSpeed : -HorizontalSpeed;
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyCactusAssassinJump,
                position: this.position
            );
        }); 
    }

    private bool ShouldCameraActivate()
    {
        if((this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay) || this.alerted)
        {
            return true;
        }
        return false;
    }

    public void PlayerSpotted()
    {
        if(this.map.player.state == Player.State.InsideBox)
        {
            if (this.map.player.GetBox() != null)
                {
                    if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitBox();
                    }
                }

                if (this.map.player.GetHidingPlace() != null)
                {
                    if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitHidingPlace();
                    }
                }
        }

        if(this.playerSeenCooldown > 0)
        {
            return;
        }
        
        if (!IsAiming())
        {
            EnterReadyWeapon();
        }

        this.alerted = true;
        this.map.desertAlertState.ActivateAlert(this.position);        
        this.playerSeenCooldown = PlayerSeenDelay;
    }

    private void CreateExclamationMark()
    {
        if(this.map.frameNumber - this.lastExclamationMarkCreatedOnFrame < 1*60)
        {
            return;
        }

        this.lastExclamationMarkCreatedOnFrame = this.map.frameNumber;
        var exclamationMark = Particle.CreateAndPlayOnce(
                    Assets.instance.exclamationMarkAnimation,
                    this.position.Add(
                        0,
                        3f
                    ),
                    this.transform.parent
                );
        exclamationMark.spriteRenderer.sortingLayerName = "Items";
        exclamationMark.spriteRenderer.sortingOrder = -1;
    }

    private void ShootIfPossible()
    {
        var playerRelative = this.map.player.position - GetCenterPoint();
        float angle = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));

        if((angle > 180 + 30) && (angle < 360 - 30))
        {
            return;
        }

        if(this.map.frameNumber - lastMissileFiredOnFrame > ShootDelay)
        {
            lastMissileFiredOnFrame = this.map.frameNumber;
            FireMissile();
            EnterShoot();
        }
    }

    private void FireMissile()
    {
        var playerRelative = this.map.player.position - GetCenterPoint();
        float angle = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));
        Vector2 forward = new Vector2(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad));
        
        var missile = CactusAssassinRocket.Create(
                GetCenterPoint() + (forward * 2.0f),
                this.chunk,
                angle,
                true
                
            );
        missile.velocity = forward * 4f/16;
    }

    private bool IsAiming()
    {
        return state == State.ReadyWeapon || state == State.Aim || state == State.LeaveAimWeapon;
    }

    private void CreateGroundParticles(int howMany = 10)
    {
        var tangent = new Vector2(
            Mathf.Cos(0 * Mathf.PI / 180),
            Mathf.Sin(0 * Mathf.PI / 180)
        );
        var normal = new Vector2(-tangent.y, tangent.x);

        for (var n = 0; n < howMany; n += 1)
            {
                var p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position - (normal * 2.1f) + new Vector2(this.facingRight? 0.5f: -0.5f, 0),
                    this.transform.parent
                );
                p.velocity.x =
                    ((n % 2 == 0) ? 1 : -1) *
                    Rnd.Range(0.4f, 2.3f) / 16;
                p.velocity.y = Rnd.Range(-0.2f, 0.2f) / 16;
                p.acceleration = p.velocity * -0.05f / 16;
                p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
            }
    }
}
