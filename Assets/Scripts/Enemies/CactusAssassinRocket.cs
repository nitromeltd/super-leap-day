﻿using System;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class CactusAssassinRocket : Enemy
{
    public Animated.Animation animationMove;
    public Sprite particleSprite;
    public AnimationCurve scaleOutCurve;
    public Transform trailOrigin;
    [NonSerialized] public Vector2 velocity;
    public bool alive = true;
    private Vector2 startPosition;

    public static CactusAssassinRocket Create(Vector2 position, Chunk chunk, float angle, bool goingRight)
    {
        var obj = Instantiate(
            Assets.instance.conflictCanyon.cactusAssassinRocket, chunk.transform
        );
        obj.name = "Spawned Cactus Assassin Rocket";
        var item = obj.GetComponent<CactusAssassinRocket>();
        item.Init(position, chunk, angle);
        chunk.items.Add(item);
        if(!goingRight)
        {
            item.transform.localScale = new Vector3(-1, 1, 1);
        }
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, float angle)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = startPosition = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndLoop(this.animationMove);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.transform.rotation = Quaternion.Euler(0, 0, angle);
    }
    
    public override void Advance()
    {
        if (this.alive == false) return;

        base.Advance();
        Move();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        this.transform.position = this.position;

        AdvanceTrail();
        this.velocity = this.velocity * 1.03f;
    }

    private void AdvanceTrail()
    {
        // Debug.Log(this.map.player.position.x);
        if (((Vector2)this.trailOrigin.position - this.startPosition).magnitude > 0.25f)
        {
            // start creating particles at some distance from player
            CreateTrailParticle(new Vector3(
                    this.trailOrigin.position.x + Rnd.Range(-0.1f, 0.1f),
                    this.trailOrigin.position.y + Rnd.Range(-0.2f, 0.2f)
                    ),
                    new Vector2(
                        Rnd.Range(-0.1f, 0.1f),
                        Rnd.Range(0, 0.2f)
                        ) / 16,
                    140
                    );
        }
    }

    private void CreateExplosionParticles()
    {
        for (int i = 0; i < 30; i ++)
        {
            Vector3 particlePosition = new Vector3(
                this.transform.position.x + Rnd.Range(-0.2f, 0.2f),
                this.transform.position.y + Rnd.Range(-0.3f, 0.3f)
                );
            var particleVelocity = (particlePosition - this.transform.position).normalized * Rnd.Range(0.2f,0.3f);  
            particlePosition = this.transform.position + (particlePosition - this.transform.position).normalized * Rnd.Range(0.8f, 0.9f);              
            CreateTrailParticle(particlePosition, particleVelocity / 16, 140, 64 + i);
        }
    }

    private void CreateTrailParticle(Vector3 particlePosition, Vector2 particleVelocity, int lifeTime, int sortingOrderOffset = 0)
    {
        Particle trailParticle = Particle.CreateWithSprite(
                this.particleSprite,
                lifeTime,
                particlePosition,
                this.transform.parent
            );
        float s = Rnd.Range(0.8f, 1.4f);
        trailParticle.transform.localScale = new Vector3(s, s, 1);
        trailParticle.ScaleOut(this.scaleOutCurve);
        trailParticle.velocity = particleVelocity;
        trailParticle.spriteRenderer.sortingLayerName = "Player Trail (BL)";
        trailParticle.spriteRenderer.sortingOrder = -(1 + sortingOrderOffset + (this.map.frameNumber % 32));
    }

    private void Move()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), false, 0.75f)
        );

        bool hitWall = false;

        if (this.velocity.x > 0)
        {
            if(right.anything == true)
            {
                hitWall = true;
            }
        }
        else
        {
            if(left.anything == true)
            {
                hitWall = true;
            }
        }

        if (this.velocity.y > 0)
        {
            if(up.anything == true)
            {
                hitWall = true;
            }
        }
        else
        {
            if(down.anything == true)
            {
                hitWall = true;
            }
        }

        if(hitWall)
        {
            Collide();
        }

        this.position += this.velocity;
        this.transform.position = this.position;
    }    

    private void Collide()
    {
        this.alive = false;

        Particle.CreateAndPlayOnce(
            Assets.instance.explosionAnimation,
            this.position.Add(0, 0),
            this.transform.parent
        );
        CreateExplosionParticles();
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .5f);
        Audio.instance.PlaySfx(Assets.instance.sfxTrunkyProjectileLand, position: this.position);
        Destroy(this.gameObject);
        this.chunk.items.Remove(this);
    }
}
