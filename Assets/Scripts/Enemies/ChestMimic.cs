﻿using System.Collections;
using UnityEngine;
using System;

public class ChestMimic : WalkingEnemy
{
    public Animated.Animation animationIdleClose;
    public Animated.Animation animationUncover;
    public Animated.Animation animationIdleOpen;
    public Animated.Animation animationChomp;
    public Animated.Animation animationDizzy;
    public Animated.Animation animationTurn;
    public Animated.Animation animationWallTurn;
    public Animated.Animation animationAlert;
    public Animated.Animation animationHop;

    public enum State
    {
        IdleClose,
        Uncover,
        IdleOpen,
        Chomp,
        Dizzy,
        Turn
    };

    private Chest.Prize prize = Chest.Prize.NONE;
    [NonSerialized] public State state = State.IdleClose;
    private bool opening = false;
    private float smoothVelocityH = 0f;
    private float dizzyTimer = 0f;
    private bool hopping = false;
    private int hopTimer = 0;
    private bool insideWater = false;

    private const int HopTotalTime = 30;
    private const float ChompPeriod = 0.4f;
    private const float GravityForce = 0.02f;
    private const float GravityForceUnderwater = 0.01f;
    private const float HorizontalMomentum = 0.15f;
    private const float DizzyTotalTime = 1.5f;
    private const float DizzyParticleOffsetV = 2f;
    private const float AlertParticleOffsetV = 2.75f;

    private float CurrentGravityForce => this.insideWater == true ?
        GravityForceUnderwater : GravityForce;
        
    public override void Init(Def def)
    {
        base.Init(def);

        this.turnSensorForward = .3f;

        ToggleSolidHitbox(true);

        this.velocity = Vector2.zero;
        this.onCollisionFromBelow = OnCollisionFromBelow;
        this.onCollisionFromAbove = OnCollisionFromAbove;
        this.onCollisionFromLeft = OnCollisionFromLeft;
        this.onCollisionFromRight = OnCollisionFromRight;
        this.whenHitFromAbove = HitFromAboveBehaviour.Nothing;
        this.turnAtEdges = false;
        this.onTurnAround = EnterTurn;
        this.stopIfTrappedHorizontally = false;

        this.prize =
            Chest.PossiblePrizes[UnityEngine.Random.Range(0, Chest.PossiblePrizes.Length)];

        // maybe we need to flip the sprites in order to make it consistent with
        // all other enemies? this means we also need to flip the chest item
        this.facingRight = def.tile.flip;
        this.transform.localScale = new Vector3(this.facingRight ? -1f: 1f, 1f, 1f);

        EnterIdleClose();
    }

    private void ToggleSolidHitbox(bool activateSolid)
    {
        Hitbox.SolidData solidData = activateSolid ? Hitbox.Solid : Hitbox.NonSolid;

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.01f, 1.5f, this.rotation, solidData)
        };
    }
    
    public override void Advance()
    {
        RefreshPhysics();
        AdvanceWater();
        CheckHop();

        switch(state)
        {                
            case State.IdleOpen:
                AdvanceIdleOpen();
                break;

            case State.Dizzy:
                AdvanceDizzy();
                break;

            case State.Turn:
                AdvanceTurn();
                break;
        }

        this.transform.position = this.position;
    }

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    private void CheckHop()
    {
        var playerRelative = this.map.player.position - (this.position + Vector2.up * 0.50f);
        var playerForward = playerRelative.x * Mathf.Sign(this.velocity.x);
        float verticalDistance = 1f;
        float horizontalDistance = 1.50f;

        if(this.hopTimer > 0)
        {
            this.hopTimer -= 1;

            if(this.hopTimer == 0)
            {
                this.hopping = false;
                ToggleSolidHitbox(true);
            }
        }
        
        if (this.hopping == false &&
            this.map.player.alive == true &&
            this.map.player.state != Player.State.Respawning &&
            Mathf.Abs(playerRelative.y) < verticalDistance &&
            (playerForward > -horizontalDistance && playerForward < horizontalDistance) &&
            (this.hitboxes.Length > 0 && this.hitboxes[0].solidXMax == true) &&
            this.state == State.IdleClose)
        {
            this.hopping = true;
            this.hopTimer = HopTotalTime;
            ToggleSolidHitbox(false);
            this.velocity += Vector2.up * 0.25f;
            
            Particle.CreateDust(this.transform.parent, 5, 7,
                this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.left * .05f);

            Particle.CreateDust(this.transform.parent, 5, 7,
                this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.right * .05f);

            this.animated.PlayOnce(this.animationHop, () => {
                this.animated.PlayOnce(this.animationIdleClose);
            });
        }
    }

    private void AdvanceIdleOpen()
    {
        if(this.onGround == true &&
            this.map.player.alive == true)
        {
            // Change to Turn state
            if(IsPlayerInOppositeFacingDirection())
            {
                this.velocity.x *= -1;
                this.facingRight = !this.facingRight;
                this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);

                this.onTurnAround?.Invoke(new TurnInfo {
                    raycastResults = null, turnReason = TurnReason.Player
                });
            }
            else
            {
                EnterChomp();
            }
        }
    }

    private void AdvanceDizzy()
    {
        if(this.dizzyTimer < DizzyTotalTime)
        {
            this.dizzyTimer += Time.deltaTime;
        }
        else
        {
            this.dizzyTimer = 0f;
            EnterIdleOpen();
        }
    }

    private void AdvanceTurn()
    {
        // nothing
    }
    
    private void EnterIdleClose()
    {
        this.state = State.IdleClose;

        this.animated.PlayOnce(this.animationIdleClose);
    }

    private void EnterUncover()
    {
        this.state = State.Uncover;

        this.velocity.y = .25f;

        this.animated.PlayOnce(this.animationUncover, EnterChomp);
        this.animated.OnFrame(9, () => { CreatePrize(); });

        Audio.instance.PlaySfx(Assets.instance.sfxChestMimicUnlock, position: this.position);
        
        this.animated.OnFrame(5, () => {
            Audio.instance.PlaySfx(Assets.instance.sfxChestMimicActivate, position: this.position);
        });
    }
    
    private void EnterIdleOpen()
    {
        this.state = State.IdleOpen;
        
        this.opening = false;
        
        this.animated.PlayOnce(this.animationIdleOpen);
    }

    private bool IsPlayerInOppositeFacingDirection()
    {
        bool isPlayerToTheRight =
            Mathf.Sign(this.position.x - this.map.player.position.x) < 0;

        return this.facingRight != isPlayerToTheRight;
    }

    private void EnterChomp()
    {
        this.state = State.Chomp;

        this.animated.PlayOnce(this.animationChomp, EnterIdleOpen);

        this.animated.OnFrame(7, () => {
            this.velocity = new Vector2(.2f * (this.facingRight ? 1f : -1f), 0f);
        });

        this.animated.OnFrame(4, () => {
            Audio.instance.PlaySfx(Assets.instance.sfxChestMimicJump, position: this.position);
        });
    }

    private void EnterDizzy()
    {
        this.state = State.Dizzy;

        this.opening = false;
        this.dizzyTimer = 0f;

        this.animated.PlayAndLoop(this.animationDizzy);

        Particle.CreatePlayAndLoop(
            Assets.instance.chestMimicDizzyAnimation,
            (int)(DizzyTotalTime * 60),
            this.position + (Vector2.up * DizzyParticleOffsetV),
            this.transform
        );

        Audio.instance.PlaySfx(Assets.instance.sfxPlayerBumpOnEnemy, position: this.position);
    }

    private void EnterTurn(TurnInfo turnInfo)
    {
        this.state = State.Turn;
        
        if(turnInfo.turnReason == TurnReason.Player)
        {
            this.velocity.y = .1f;

            Particle.CreatePlayAndHoldLastFrame(
                Assets.instance.chestMimicAlertAnimation,
                30,
                this.position + (Vector2.up * AlertParticleOffsetV),
                this.transform
            );

            this.animated.PlayOnce(this.animationAlert, () => {
                this.animated.PlayOnce(this.animationTurn, () =>
                {
                    EnterIdleOpen();
                });
            }); 
        }
        else
        {
            this.velocity.y = .2f;
            this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);

            this.animated.PlayOnce(this.animationWallTurn, () => {
                EnterIdleOpen();
            }); 
        }  
    }
    
    private void RefreshPhysics()
    {
        this.velocity.y -= GravityForce;
        this.velocity.x = Mathf.SmoothDamp(
            this.velocity.x, 0f, ref smoothVelocityH, HorizontalMomentum
        );

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreItem1: this
        );

        Move(raycast);
        FollowPortals();
    }

    private void OnCollisionFromBelow(Collision collision)
    {
        this.velocity.y = .2f;

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.left * .05f);

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.right * .05f);

        Open();
    }

    private void OnCollisionFromAbove(Collision collision)
    {
        if (this.opening == false &&
            this.map.player.velocity.y <= -0.1f)
        {
            TriggerFromAbove();
        }
    }

    private void OnCollisionFromLeft(Collision collision)
    {
        OnCollisionFromSide(collision);
    }

    private void OnCollisionFromRight(Collision collision)
    {
        OnCollisionFromSide(collision);
    }

    public void OnCollisionFromSide(Collision collision)
    {
        // if player is invincible, player jumps and turn mimic dizzy
        if(this.map.player.invincibility.isActive &&
            this.state != State.IdleClose)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player
            });
        }
        // kill player if its in the facing direction
        else if(
            collision.otherPlayer != null &&
            !IsPlayerInOppositeFacingDirection() &&
            this.state == State.Chomp
        )
        {
            this.map.player.Hit((Vector2)this.transform.position);
        }
    }

    private void TriggerFromAbove()
    {
        // todo: change for proper player implementation
        this.map.player.BounceOffHitEnemy(this.transform.position, true);

        Particle.CreateDust(this.transform.parent, 8, 10,
            this.position + Vector2.up * 1f, .5f, .5f, Vector2.left * .1f);

        Particle.CreateDust(this.transform.parent, 8, 10,
            this.position + Vector2.up * 1f, .5f, .5f, Vector2.right * .1f);

        Open();
    }

    public void Open()
    {
        if(this.opening) return;
        this.opening = true;

        if(this.state == State.IdleClose)
        {
            EnterUncover();
        }
        else
        {
            EnterDizzy();
        }
    }
    
    public override void Hit(KillInfo info)
    {
        //this.map.player.TriggerBounce();
        EnterDizzy();
    }
    
    public override bool Kill(KillInfo info)
    {
        if (info.isBecauseLevelIsResetting == true)
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
            TriggerRespawnAnimation();
            return true;
        }
        else
        {
            Open();
            return false;
        }
    }

    
    private void CreatePrize()
    {
        if (this.chunk.xyOfAlreadySpawnedPickups.Contains(new XY(this.def.tx, this.def.ty)) == true)
            return;

        switch (prize)
        {
            case Chest.Prize.ONE_COIN:
            {
                Coin coin = Coin.Create(
                    this.position + Vector2.up * 2.5f, this.chunk);

                coin.movingFreely = true;
                coin.velocity = new Vector2(0.1f * (facingRight ? 1f : -1f), 0.3f);
                
                Audio.instance.PlaySfx(Assets.instance.sfxChestMimicRewardCoin, position: this.position);
            }
            break;

            case Chest.Prize.THREE_COINS:
            {
                StartCoroutine(_CreateThreeCoins());

                Audio.instance.PlaySfx(Assets.instance.sfxChestMimicRewardCoin, position: this.position);
            }
            break;

            case Chest.Prize.POWERUP:
            {
                // todo: choose random powerup
                PowerupPickup star = PowerupPickup.CreateRandomPowerup(
                    this.position + Vector2.up * 2.5f, this.chunk);
                    
                star.movingFreely = true;
                star.velocity = new Vector2(0.1f * (facingRight ? 1f : -1f), 0.3f);

                Audio.instance.PlaySfx(Assets.instance.sfxChestMimicRewardPowerup, position: this.position);
            }
            break;

            case Chest.Prize.KEY:
            {
                GateKey key = GateKey.Create(
                    this.position + Vector2.up * 2.5f, this.chunk);
                    
                key.movingFreely = true;
                key.velocity = new Vector2(0.1f * (facingRight ? 1f : -1f), 0.3f);
                key.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
            }
            break;
        }
        this.chunk.xyOfAlreadySpawnedPickups.Add(new XY(this.def.tx, this.def.ty));
    }

    private IEnumerator _CreateThreeCoins()
    {
        int coinsAmount = 3;
        
        for (int i = 0; i < coinsAmount; i++)
        {
            Coin coin = Coin.Create(this.position + Vector2.up * 2.5f, this.chunk);

            float hVelocity = UnityEngine.Random.Range(0.05f, 0.15f);
            float vVelocity = UnityEngine.Random.Range(0.25f, 0.35f);

            coin.movingFreely = true;
            coin.velocity = new Vector2(hVelocity * (i % 2 == 0 ? 1f : -1f), vVelocity);

            yield return new WaitForSeconds(.05f);
        }
    }
}
