using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using System.Linq;
using Rnd = UnityEngine.Random;

public class CloudBird : Enemy
{
    public enum State
    {
        Walk,
        Turn,
        PrepareToAttack,
        Attack,
        Stop,
        SurprisedBeforeFallDown,
        FallDown,
    }

    private State state;
    private bool facingRight = true;
    private bool isForwardSensorInClouds = false;
    private bool isBottomSensorInClouds = false;
    private bool isTopSensorInClouds = false;
    private bool isCoreSensorInClouds = false;
    private int prepareToAttackTime = 0;
    private int attackTime = 0;
    private bool isDead = false;
    private Vector2 velocity;
    private float groundAngleDegrees = 0;
    private float visibleAngleDegrees = 0;
    public Animated.Animation animationFall;
    public Animated.Animation animationAttack;
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationAlert;

    const int TopSensorIndex = 1;
    const int BottomSensorIndex = 2;
    const int LeftSensorIndex = 3;
    const int RightSensorIndex = 4;
    const int CoreSensorIndex = 5;
    const float MaximumVerticalWalkVelocity = 0.005f;
    
    public override void Init(Def def)
    {
        base.Init(def);
        this.facingRight = def.tile.flip;

        this.hitboxes = new [] {
                new Hitbox(-1.35f, 1.35f, -0.6f, 1.0f, this.rotation, Hitbox.NonSolid),
                new Hitbox(-0.8f, 0.8f, 1f, 1.05f, this.rotation, Hitbox.NonSolid),
                new Hitbox(-0.8f, 0.8f, -0.75f, -0.7f, this.rotation, Hitbox.NonSolid),
                new Hitbox(-1.38f, -1.33f, -0.3f, 0.5f, this.rotation, Hitbox.NonSolid),
                new Hitbox(1.33f, 1.38f, -0.3f, 0.5f, this.rotation, Hitbox.NonSolid),
                new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid),                
            };
 
        this.velocity = new Vector2(this.facingRight == true ? 0.05f : -0.05f, -MaximumVerticalWalkVelocity);
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.animated.PlayAndLoop(this.animationWalk);
    }

    public override void Advance()
    {
        this.transform.localScale = new Vector3(this.facingRight == true ? 1 : -1, 1, 1);

        if(this.isDead == true)
        {
            return;
        }       

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        MoveHorizontally(raycast);
        MoveVertically(raycast);
        this.transform.position = this.position;

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.PrepareToAttack:
                AdvancePrepareToAttack();
                break;    

            case State.Attack:
                AdvanceAttack();
                break;       

            case State.FallDown:
                AdvanceFallDown();
                break;


            case State.Turn:
            default:
                break;
        }

        CheckIfPlayerHitByBird();
    }

    // private void ProcessSensorsOld()
    // {
    //     var cloudBlocks = this.chunk.subsetOfItemsOfTypeCloudBlock;
    //     var thisRect = this.hitboxes[0].InWorldSpace();
    //     this.isForwardSensorInClouds = false;
    //     this.isBottomSensorInClouds = false;
    //     this.isTopSensorInClouds = false;
    //     this.isCoreSensorInClouds = false;

    //     var birdX = Mathf.FloorToInt(this.position.x);
    //     var birdY = Mathf.FloorToInt(this.position.y);

    //     foreach (var cloudBlock in cloudBlocks)
    //     {
    //         if(cloudBlock.position.x < birdX - 3
    //         || cloudBlock.position.x > birdX + 3
    //         || cloudBlock.position.y < birdY - 3
    //         || cloudBlock.position.y > birdY + 3
    //         )
    //             continue;

    //         if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[BottomSensorIndex].InWorldSpace()))
    //         {
    //             this.isBottomSensorInClouds = true;
    //         }
    //         if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[TopSensorIndex].InWorldSpace()))
    //         {
    //             this.isTopSensorInClouds = true;
    //         }
    //         if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[CoreSensorIndex].InWorldSpace()))
    //         {
    //             this.isCoreSensorInClouds = true;
    //         }
    //         int index = this.facingRight == false ? LeftSensorIndex : RightSensorIndex;
    //         if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[index].InWorldSpace()))
    //         {
    //             this.isForwardSensorInClouds = true;
    //         }
    //     }
    // }

    private void ProcessSensorsAndSpawnParticles()
    {
        this.isForwardSensorInClouds = false;
        this.isBottomSensorInClouds = false;
        this.isTopSensorInClouds = false;
        this.isCoreSensorInClouds = false;

        var txMin = Mathf.FloorToInt(this.position.x) - 3;
        var txMax = Mathf.FloorToInt(this.position.x) + 3;
        var tyMin = Mathf.FloorToInt(this.position.y) - 3;
        var tyMax = Mathf.FloorToInt(this.position.y) + 3;
         

        for (int tx = txMin; tx <= txMax; tx += 1)
        {
            if (tx < this.chunk.xMin || tx >= this.chunk.xMax) continue;

            for (int ty = tyMin; ty <= tyMax; ty += 1)
            {
                var cloudBlock = this.chunk.CloudBlockAt(tx, ty);
                if (cloudBlock != null)
                {
                    if (cloudBlock.insideBlock == false)
                    {
                        var bodyRect = this.hitboxes[0].InWorldSpace();
                        if (cloudBlock.bottomBlock == null && cloudBlock.bottomIgnoreTime == 0 && cloudBlock.IsOverlappingBottom(bodyRect) == true)
                        {
                            int mult = IsMovingFast() ? 3 : 1;
                            cloudBlock.SpawnParticlesOnBottomOfTheBlock(mult, mult);
                        }

                        if (cloudBlock.topBlock == null && cloudBlock.topIgnoreTime == 0 && cloudBlock.IsOverlappingTop(bodyRect) == true)
                        {
                            int mult = IsMovingFast() ? 3 : 1;
                            cloudBlock.SpawnParticlesOnTopOfTheBlock(mult, mult);
                        }

                        if (cloudBlock.leftBlock == null && cloudBlock.leftIgnoreTime == 0 && cloudBlock.IsOverlappingLeft(bodyRect) == true)
                        {
                            int mult = IsMovingFast() ? 3 : 1;
                            cloudBlock.SpawnParticlesOnLeftOfTheBlock(mult, mult);
                        }

                        if (cloudBlock.rightBlock == null && cloudBlock.rightIgnoreTime == 0 && cloudBlock.IsOverlappingRight(bodyRect) == true)
                        {
                            int mult = IsMovingFast() ? 3 : 1;
                            cloudBlock.SpawnParticlesOnRightOfTheBlock(mult, mult);
                        }
                    }

                    if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[BottomSensorIndex].InWorldSpace()))
                    {
                        this.isBottomSensorInClouds = true;
                    }
                    if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[TopSensorIndex].InWorldSpace()))
                    {
                        this.isTopSensorInClouds = true;
                    }
                    if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[CoreSensorIndex].InWorldSpace()))
                    {
                        this.isCoreSensorInClouds = true;
                    }
                    int index = this.facingRight == false ? LeftSensorIndex : RightSensorIndex;
                    if (cloudBlock.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[index].InWorldSpace()))
                    {
                        this.isForwardSensorInClouds = true;
                    }
                }
            }
        }
    }

    private bool ShouldChargePlayer()
    {
        var relativePlayer = this.map.player.position - this.position;

        if (CloudBlock.IsPlayerInsideClouds(this.map.player) == true)
        {
            if ((this.facingRight == true && relativePlayer.x > 0)
                || (this.facingRight == false && relativePlayer.x < 0)
            )
            {
                if(Mathf.Abs(relativePlayer.x) < 10 && Mathf.Abs(relativePlayer.y) < 2)
                {
                    return true;
                }
            }
        }

        return false;
    }

    private void AdvanceWalk()
    {
        ProcessSensorsAndSpawnParticles();

        if(this.isForwardSensorInClouds == false)
        {
            this.velocity.x = 0;
            this.state = State.Turn;
            this.animated.PlayOnce(this.animationTurn);
            this.animated.onComplete = () => {
                this.facingRight = !this.facingRight;
                this.velocity.x = this.facingRight == true ? 0.05f : -0.05f;
                this.state = State.Walk;
                this.animated.PlayAndLoop(this.animationWalk);
                this.transform.localScale = new Vector3(this.facingRight == true ? 1 : -1, 1, 1);
            };
            return;            
        }

        if(this.velocity.y < 0)
        {
            if(this.isBottomSensorInClouds == false)
            {
                this.velocity.y = -this.velocity.y;
            }
        }
        else if(this.velocity.y > 0)
        {
            if(this.isTopSensorInClouds == false)
            {
                this.velocity.y = -this.velocity.y;
            }
        }

        if(ShouldChargePlayer() == true)
        {
            this.state = State.PrepareToAttack;
            this.velocity = Vector2.zero;
            this.prepareToAttackTime = 0;

            this.animated.PlayOnce(this.animationAlert);
            this.attackTime = 0;
            this.animated.onComplete = () => {
                this.prepareToAttackTime = 60;
            };

            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyCloudBirdSurprised,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
        }

        this.velocity.y = Mathf.Clamp(this.velocity.y, -MaximumVerticalWalkVelocity, MaximumVerticalWalkVelocity);
    }

    private void AdvancePrepareToAttack()
    {
        if(this.prepareToAttackTime > 0)
        {
            this.prepareToAttackTime -= 1;
            if(this.prepareToAttackTime == 0)
            {
                this.state = State.Attack;
                this.attackTime = 120 * 1;
                this.velocity = new Vector2(this.facingRight == true ? 0.15f : -0.15f, 0f);
                this.animated.PlayAndLoop(this.animationAttack);
            }
        }
    }

    private void AdvanceAttack()
    {
        ProcessSensorsAndSpawnParticles();

        if(this.isBottomSensorInClouds == false)
        {
            this.state = State.FallDown;
        }
        else
        {
            if (this.attackTime > 0)
            {
                this.attackTime--;
                if (this.attackTime == 0)
                {
                    this.state = State.Walk;
                    this.animated.PlayAndLoop(this.animationWalk);
                    this.velocity = new Vector2(this.facingRight == true ? 0.05f : -0.05f, -0.005f);
                }
            }
        }
        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxWindyCloudBirdAttacking))
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyCloudBirdAttacking,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
        }
    }

    private void AdvanceFallDown()
    {
        ProcessSensorsAndSpawnParticles();

        if(this.isBottomSensorInClouds && this.isCoreSensorInClouds)
        {
            this.state = State.Walk;
            this.velocity = new Vector2(this.facingRight == true ? 0.05f : -0.05f, -0.005f);
            this.animated.PlayAndLoop(this.animationWalk);
        }
        this.velocity.y -= 0.02f;

        Audio.instance.StopSfx(Assets.instance.sfxWindyCloudBirdAttacking);

        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 3
        );
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    private void CheckIfPlayerHitByBird()
    {
        if (this.deathAnimationPlaying == true)
            return;

        var ownRectangle = this.hitboxes[0].InWorldSpace();

        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        if (player.ShouldCollideWithEnemies() == false)
            return;

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return;

        var playerAbove =
            playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.VelocityLastFrame.y) > ownRectangle.yMax - 0.1f;
        var enemyVelocityY = 0;
        var playerGoingDown =
            player.velocity.y < enemyVelocityY ||
            player.VelocityLastFrame.y < enemyVelocityY;

        if (player.invincibility.isActive == true)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Die)
        {
            player.BounceOffHitEnemy(this.transform.position);
            Hit(new KillInfo
            {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Nothing)
        {
            player.BounceOffHitEnemy((Vector2)this.transform.position);
        }
        else
        {
            player.Hit((Vector2)this.transform.position);
        }
    }

    private void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];        

        if (this.velocity.x > 0)
        {
            var high    = this.position.Add(0, rect.yMax - 0.250f);
            var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
            var low     = this.position.Add(0, rect.yMin + 0.250f);
            var maxDistanceHorizontal = this.velocity.x + rect.xMax;

            var rHigh = raycast.Horizontal(high, true, maxDistanceHorizontal);
            var rMid = raycast.Horizontal(mid, true, maxDistanceHorizontal);
            var rLow = raycast.Horizontal(low, true, maxDistanceHorizontal);

            if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees < 90) rMid.anything = false;
            if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;

            var resultHorizontal = Raycast.CombineClosest(
                rHigh,
                rMid,
                rLow
            );

            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= this.velocity.x + rect.xMax)
            {
                this.position.x += resultHorizontal.distance - rect.xMax;
                if (this.state == State.Walk)
                {
                    this.velocity.x = -this.velocity.x;
                    this.facingRight = false;
                }
                else if (this.state == State.Attack)
                {
                    this.facingRight = false;
                    this.state = State.Walk;
                    this.animated.PlayAndLoop(this.animationWalk);
                    this.velocity = new Vector2(this.facingRight == true ? 0.05f : -0.05f, -0.005f);
                }
                else
                {
                    Die();
                }
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var high    = this.position.Add(0, rect.yMax - 0.250f);
            var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
            var low     = this.position.Add(0, rect.yMin + 0.250f);

            var maxDistanceHorizontal = -(this.velocity.x + rect.xMin);

            var rHigh = raycast.Horizontal(high, false, maxDistanceHorizontal);
            var rMid = raycast.Horizontal(mid, false, maxDistanceHorizontal);
            var rLow = raycast.Horizontal(low, false, maxDistanceHorizontal);

            if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees > 270) rMid.anything = false;
            if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;

            var resultHorizontal = Raycast.CombineClosest(
                rHigh,
                rMid,
                rLow
            );

            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= -(this.velocity.x + rect.xMin))
            {
                this.position.x += -(resultHorizontal.distance + rect.xMin);
                if (this.state == State.Walk)
                {
                    this.velocity.x = -this.velocity.x;
                    this.facingRight = true;
                }
                else if (this.state == State.Attack)
                {
                    this.facingRight = true;
                    this.state = State.Walk;
                    this.animated.PlayAndLoop(this.animationWalk);
                    this.velocity = new Vector2(this.facingRight == true ? 0.05f : -0.05f, -0.005f);
                }
                else
                {
                    Die();
                }
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }

    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = -this.velocity.y;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var tolerance = 0.0f;
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin + tolerance;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin + tolerance)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = -this.velocity.y;

                if(this.state == State.FallDown)
                {
                    this.velocity.y = 0;
                    if (result.tiles.Length > 0)
                        this.groundAngleDegrees = result.surfaceAngleDegrees;
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private void Die()
    {
        foreach (var debris in this.deathAnimation.debris)
        {
            var p = Particle.CreateWithSprite(
                debris, 100, this.position, this.transform.parent
            );
            p.Throw(new Vector2(0, 0.5f), 0.3f, 0.3f, new Vector2(0, -0.02f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                p.spin *= -1;
        }
        this.isDead = true;
        this.animated.Stop();
        this.spriteRenderer.sprite = null;

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyCloudBirdHit,
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );
    }

    public Rect Rectangle()
    {
        if(this.isDead == true)
            return Rect.zero;
        
        return hitboxes[0].InWorldSpace();
    }

    public bool IsMovingFast()
    {
        return this.state == State.Attack || this.state == State.FallDown;
    }

    private void DebugAttackOnKeyA()
    {
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.A))
        {
            this.state = State.PrepareToAttack;
            this.velocity = Vector2.zero;
            this.prepareToAttackTime = 0;

            this.animated.PlayOnce(this.animationAlert);
            this.attackTime = 0;
            this.animated.onComplete = () => {
                this.prepareToAttackTime = 60;
            };
        }
#endif
    }
}
