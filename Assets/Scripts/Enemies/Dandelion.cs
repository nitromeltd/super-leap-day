using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Dandelion : WalkingEnemy
{
    public Animated.Animation animationLand;
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationFly;
    public Animated.Animation animationDropFast;
    public Animated.Animation animationDropSlow;
    public Animated.Animation animationInsideCurrent;

    private enum State
    {
        Patrolling,
        Flying,
        DroppingFast,
        DroppingSlow,
        Land
    }
    
    private State state = State.Patrolling;
    //private bool isTurning = false;
    private int landTime = 0;
    private int visibleAngleDegrees = 0;
    private Vector3 scale = Vector3.one;
    private bool[] propellerPushActive = new bool[TilePropeller.Directions.Length];
    [HideInInspector] public AirCurrent insideAirCurrent;
    [HideInInspector] public AirCurrent.TrackingData airCurrentTrackingData;
    
    private const float WalkSpeed = 0.03f;
    private const float GravityForce = 0.015f;
    private const float MaxVerticalVelocity = 0.20f;
    private const float PlayerDetectMaxDistance = 15f;
    private const float ReduceHorizontalSpeedAirborne = 0.0003f;
    private const int TotalLandTime = 30;
    private static Vector2 HitboxSize = new Vector2(0.5f, 0.75f);
    private static Vector2 HitboxOffset = new Vector2(0f, 0f);

    public bool IsDead => this.deathAnimationPlaying == true;
    private bool IsInsideAirCurrent => this.insideAirCurrent != null;

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.turnSensorForward = .3f;
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.onTurnAround = Turn;
        this.turnAtEdges = false;
        this.groundSensorSpacing = 0.60f;
        
        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = WalkSpeed * initialDirectionValue;

        this.hitboxes = new [] {
            new Hitbox(
                HitboxOffset.x - HitboxSize.x,
                HitboxOffset.x + HitboxSize.x,
                HitboxOffset.y - HitboxSize.y,
                HitboxOffset.y + HitboxSize.y,
                this.rotation, Hitbox.NonSolid)
        };

        ChangeState(State.Patrolling);

        foreach(var i in this.chunk.items)
        {
            if(i is AirCurrent)
            {
                AirCurrent airCurrent = i as AirCurrent;
                airCurrent.NotifyOfNewDandelion(this);
            }

            if(i is AirPropeller)
            {
                AirPropeller airPropeller = i as AirPropeller;
                airPropeller.NotifyOfNewDandelion(this);
            }

            if(i is TilePropeller)
            {
                TilePropeller tilePropeller = i as TilePropeller;
                tilePropeller.NotifyOfNewDandelion(this);
            }
        }
    }

    private void Turn(TurnInfo turnInfo)
    {
        void EndTurn()
        {
            //this.isTurning = false;
            this.transform.localScale = new Vector3(
                this.facingRight ? 1f : -1f, 1f, 1f
            );

            ChangeState(State.Patrolling);
        }

        //this.isTurning = true;
        this.velocity.x = 0f;

        if(this.state == State.Patrolling)
        {
            this.animated.PlayOnce(this.animationTurn, EndTurn);
        }
        else
        {
            EndTurn();
        }
    }

    private void AdvanceInsideAirCurrent()
    {
        this.insideAirCurrent.AdvanceDandelion(this);
        if(IsInsideAirCurrent == false) return;

        if(this.hitboxes[0].InWorldSpace().Overlaps(this.map.player.Rectangle()))
        {
            this.map.player.Hit(this.position);
        }
        
        this.visibleAngleDegrees += 10;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
        this.transform.position = this.position;
    }

    public override void Advance()
    {
        if(IsDead == true) return;

        if(IsInsideAirCurrent == true)
        {
            AdvanceInsideAirCurrent();
            return;
        }

        switch(this.state)
        {
            case State.Patrolling:
                AdvancePatrolling();
                break;

            case State.Flying:
                AdvanceFlying();
                break;

            case State.DroppingSlow:
                AdvanceDroppingSlow();
                break;

            case State.DroppingFast:
                AdvanceDroppingFast();
                break;

            case State.Land:
                AdvanceLand();
                break;
        }

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreItem1: this
        );
        Move(raycast);

        this.transform.position = this.position;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
        var localScale = this.scale;
        localScale.x = this.scale.x * (this.facingRight ? 1f : -1f);
        this.transform.localScale = localScale;

        EnemyHitDetectionAgainstPlayer();
    }

    private void AdvancePatrolling()
    {
        if(this.onGround == false)
        {
            ChangeState(this.velocity.y > 0 ? State.Flying : State.DroppingSlow);
            return;
        }
        
        this.velocity.x = WalkSpeed * (this.facingRight ? 1f : -1f);
        this.velocity.y -= GravityForce;

        ApplyPropellerPush();
        ApplyVerticalVelocityLimit(MaxVerticalVelocity);
    }

    private void AdvanceFlying()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, ReduceHorizontalSpeedAirborne);

        if(IsAnyPropellerActive() == false)
        {
            this.velocity.y -= GravityForce/4f;
        }
        
        ApplyPropellerPush();
        ApplyVerticalVelocityLimit(MaxVerticalVelocity);

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxWindyDandelionRise))
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyDandelionRise,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
        }

        if (this.onGround == true)
        {
            ChangeState(State.Land);
        }
        else if(this.velocity.y < 0f)
        {
            ChangeState(State.DroppingSlow);
        }
        else if(IsPlayerBelow())
        {
            ChangeState(State.DroppingFast);
        }
    }

    private void AdvanceDroppingFast()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, ReduceHorizontalSpeedAirborne);
        this.velocity.y -= GravityForce;

        ApplyVerticalVelocityLimit(MaxVerticalVelocity * 2.5f);

        if(this.onGround == true)
        {
            ChangeState(State.Land);
        }
    }

    private void AdvanceDroppingSlow()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, ReduceHorizontalSpeedAirborne);
        this.velocity.y -= GravityForce/4f;

        ApplyPropellerPush();
        ApplyVerticalVelocityLimit(MaxVerticalVelocity);

        if(this.onGround == true)
        {
            ChangeState(State.Land);
        }
        else if(this.velocity.y > 0f)
        {
            ChangeState(State.Flying);
        }
        else if(IsPlayerBelow())
        {
            ChangeState(State.DroppingFast);
        }
    }

    private void AdvanceLand()
    {
        this.velocity.x = 0f;
        this.velocity.y -= GravityForce;

        ApplyPropellerPush();
        ApplyVerticalVelocityLimit(MaxVerticalVelocity);

        this.scale.x = Util.Slide(this.scale.x, 1f, 0.02f);
        this.scale.y = Util.Slide(this.scale.y, 1f, 0.02f);

        if(this.landTime > 0)
        {
            this.landTime -= 1;

            if(this.landTime == 0)
            {
                ChangeState(State.Patrolling);
            }
        }
    }

    private void ApplyPropellerPush()
    {
        for (int i = 0; i < this.propellerPushActive.Length; i++)
        {
            if(this.propellerPushActive[i] == true)
            {
                Vector2 pushDirection = TilePropeller.Directions[i];

                if(pushDirection == Vector2.up && this.onGround == true)
                {
                    this.velocity.y = 0f;
                }
                
                this.velocity += pushDirection * 0.01f;
            }
        }
        this.propellerPushActive = new bool[TilePropeller.Directions.Length];
    }

    private void ApplyVerticalVelocityLimit(float maxVelocity)
    {
        this.velocity.y = Mathf.Clamp(this.velocity.y, -maxVelocity, maxVelocity);
    }

    private bool IsAnyPropellerActive()
    {
        foreach(var p in this.propellerPushActive)
        {
            if(p == true)
            {
                return true;
            }
        }

        return false;
    }

    private bool IsPlayerBelow()
    {
        Vector2 surfaceNormal = Vector2.down;

        var detectRaycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
        );
        var currentDistance = detectRaycast.Arbitrary(
            this.position, surfaceNormal, PlayerDetectMaxDistance
        ).distance;

        // detect player
        var furthestPoint = this.position + (surfaceNormal * currentDistance);
        var ray = surfaceNormal * currentDistance;

        var along = Vector2.Dot(
            this.map.player.position - this.position, ray
        ) / ray.sqrMagnitude;
        along = Mathf.Clamp01(along);

        var nearestPointAlongRayToPlayer =
            Vector2.Lerp(this.position, furthestPoint, along);

        return (nearestPointAlongRayToPlayer - this.map.player.position).magnitude < 1;
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch(this.state)
        {
            case State.Patrolling:
                EnterPatrolling();
                break;

            case State.Flying:
                EnterFlying();
                break;

            case State.DroppingFast:
                EnterDroppingFast();
                break;

            case State.DroppingSlow:
                EnterDroppingSlow();
                break;

            case State.Land:
                EnterLand();
                break;
        }
    }

    private void EnterPatrolling()
    {
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterFlying()
    {
        this.animated.PlayAndLoop(this.animationFly);
    }

    private void EnterDroppingFast()
    {
        this.animated.PlayAndLoop(this.animationDropFast);

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyDandelionFall,
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );
        Audio.instance.ChangeSfxVolume(Assets.instance.sfxWindyDandelionRise, setTo: 0, duration: .3f);
    }

    private void EnterDroppingSlow()
    {
        this.animated.PlayAndLoop(this.animationDropSlow);

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyDandelionFall,
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );
        Audio.instance.ChangeSfxVolume(Assets.instance.sfxWindyDandelionRise, setTo: 0, duration: .3f);
    }

    private void EnterLand()
    {
        this.animated.PlayOnce(this.animationLand);
        
        this.scale = new Vector3(1.2f, 0.8f, 1f);
        this.landTime = TotalLandTime;

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyDandelionBump,
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );
        Audio.instance.ChangeSfxVolume(Assets.instance.sfxWindyDandelionRise, setTo: 0, duration: .3f);
    }

    public void PushFromPropeller(Vector2 direction)
    {
        int index = Array.IndexOf(TilePropeller.Directions, direction);
        this.propellerPushActive[index] = true;
    }

    public void EnterAirCurrent(AirCurrent airCurrent)
    {
        if(IsDead == true) return;

        this.insideAirCurrent = airCurrent;
        this.animated.PlayAndHoldLastFrame(this.animationInsideCurrent);
        this.spriteRenderer.sortingLayerName = "Items (Front)";
    }

    public void NotifyExitAirCurrent()
    {
        this.insideAirCurrent = null;
        this.visibleAngleDegrees = 0;
        this.spriteRenderer.sortingLayerName = "Enemies";
        ChangeState(this.state);
    }

}
