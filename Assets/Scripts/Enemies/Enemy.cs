
using UnityEngine;
using System;
using System.Collections;

public class Enemy : Item
{
    public enum HitFromAboveBehaviour
    {
        Die,
        HurtPlayer,
        Nothing
    }
    [NonSerialized] public HitFromAboveBehaviour whenHitFromAbove;

    [Serializable]
    public class DeathAnimation
    {
        public Sprite frozenSprite;
        public Sprite[] debris;
        public AudioClip sfxDeath;

        public AudioClip GetDeathSFX()
        {
            return (sfxDeath != null) ? sfxDeath : Assets.instance.sfxEnemyDeath;
        }
    }
    public DeathAnimation deathAnimation;
    protected bool deathAnimationPlaying;

    Pickup.LuckyDrop insertedPickup;
    [NonSerialized] public bool becameGold;
    [NonSerialized] public BossExitGate exitGate;
    public struct KillInfo
    {
        public bool isBecauseLevelIsResetting;
        public Player playerDeliveringKill;
        public Item itemDeliveringKill;
        public bool freezeTime;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        string pickupName;
        if (def.tile.properties?.ContainsKey("contains_item") == true)
            pickupName = def.tile.properties["contains_item"].s;
        else
            pickupName = "none";

        insertedPickup = Pickup.GetPickupByPropertyName(pickupName);

        this.becameGold = false;

        var path =this.chunk.connectionLayer.NearestPathTo(this.position, 1);
        if (path != null)
        {
            var node = path.NearestNodeTo(this.position);
            if (node != null)
            {
                var otherNode =
                    node.Previous() ??
                    node.Next();
                var Gate = this.chunk.NearestItemTo<BossExitGate>(otherNode.Position);
                exitGate = Gate;
            }
        }
    }

    protected int GetUniqueSortingOrder()
    {
        // A sorting layer between [0..999], for this enemy to use.
        // It doesn't matter what it is, the point is we're just trying
        // to avoid enemies from z-fighting as they walk past each other.
        return (this.def.tx + (this.def.ty * 17)) % 1000;
    }

    public virtual bool Kill(KillInfo info)
    {
        if (info.isBecauseLevelIsResetting == true)
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
            TriggerRespawnAnimation();
        }
        else
        {
            TriggerNewDeathAnimation(info, delegate
            {
                Destroy(this.gameObject);
            });

            this.chunk.items.Remove(this);

            DropLuckyPickup();            
        }
        StopLoopedSfx();
        return true;
    }

    protected void DropLuckyPickup()
    {
        if(this.map.player.midasTouch.isActive != true || this.insertedPickup != Pickup.LuckyDrop.None)
        {
            if (this.insertedPickup != Pickup.LuckyDrop.None)
            {
                this.map.DropLuckyPickup(this.insertedPickup, this.position + Vector2.up * 1f, this.chunk);
            }
            else
            {
                bool isHelmet = this is HelmutHelmet;
                if (this.chunk.xyOfAlreadySpawnedPickups.Contains(new XY(this.def.tx, this.def.ty)) == false && isHelmet == false)
                {
                    this.chunk.xyOfAlreadySpawnedPickups.Add(new XY(this.def.tx, this.def.ty));

                    this.map.RandomlyDropLuckyPickup(
                        this.position + Vector2.up * 1f,
                        this.chunk
                        );
                }
            }
        }
    }

    public void TriggerDeathAnimation()
    {
        Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position.Add(0, 1),
            this.transform.parent
        );
    }

    protected void TriggerNewDeathAnimation(KillInfo killInfo, Action onComplete)
    {
        if (this.deathAnimationPlaying == true)
            return;

        StartCoroutine(DeathAnimationCoroutineForThis(killInfo, onComplete));
    }

    public IEnumerator DeathAnimationCoroutineForThis(
        KillInfo killInfo, Action onComplete
    )
    {
        if (this.map.player.midasTouch.isActive == true && 
            this.insertedPickup == Pickup.LuckyDrop.None &&
            this.chunk.xyOfAlreadySpawnedPickups.Contains(new XY(this.def.tx, this.def.ty)) == false)
        {
            this.chunk.xyOfAlreadySpawnedPickups.Add(new XY(this.def.tx, this.def.ty));
            this.animated?.Stop();
            this.becameGold = true;
            this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
            this.spriteRenderer.material.color = Color.white;

            yield return new WaitForSeconds(.1f);

            this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
            this.spriteRenderer.material.color = Color.black;

            yield return new WaitForSeconds(.1f);

            this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
            this.spriteRenderer.material.color = new Color(255, 165, 0);

            yield return new WaitForSeconds(.1f);

            this.spriteRenderer.material = Assets.instance.spriteGoldTint;

            yield return new WaitForSeconds(.5f);
            if (UnityEngine.Random.value > .5f)
            {
                this.map.DropLuckyPickup(Pickup.LuckyDrop.SilverCoin, this.position + Vector2.up * 1f, this.chunk);
            }
            else
            {
                this.map.DropLuckyPickup(Pickup.LuckyDrop.GoldCoin, this.position + Vector2.up * 1f, this.chunk);
            }
        }

        bool thrownDebris = false;
        void ThrowDebris()
        {
            if (thrownDebris == true) return;
            thrownDebris = true;

            this.spriteRenderer.sprite = null;

            foreach (var debris in this.deathAnimation.debris)
            {
                var p = Particle.CreateWithSprite(
                    debris, 100, this.position, this.transform.parent
                );
                p.Throw(new Vector2(0, 0.5f), 0.2f, 0.1f, new Vector2(0, -0.025f));

                p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
                if (UnityEngine.Random.Range(0, 2) == 0)
                    p.spin *= -1;
                if(this.map.player.midasTouch.isActive == true && this.insertedPickup == Pickup.LuckyDrop.None)
                {
                    p.spriteRenderer.material = Assets.instance.spriteGoldTint;
                }
            }
        }

        this.deathAnimationPlaying = true;
        if (this.deathAnimation.frozenSprite != null)
        {
            if (killInfo.playerDeliveringKill != null)
                this.spriteRenderer.sprite = this.deathAnimation.frozenSprite;
            else
                ThrowDebris();

            killInfo.playerDeliveringKill?.PlayStompFrame();
        }
        this.animated?.Stop();

        Audio.instance.PlaySfx(
            Assets.instance.sfxPlayerBumpOnEnemy,
            new Audio.Options(pitchIncrement: 0.20f),
            this.position
        );

        if (killInfo.freezeTime == true)
            this.map.timeFrozenByEnemyDeath = true;

        if (this.hitboxes.Length > 0)
            CreateSmackEffect(killInfo, this.hitboxes[0].InWorldSpace());

        yield return new WaitForSecondsRealtime(0.20f);

        Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        if (killInfo.freezeTime == true)
            this.map.timeFrozenByEnemyDeath = false;

        ThrowDebris();
        onComplete?.Invoke();
        this.deathAnimationPlaying = false;

        if (this.exitGate != null)
        {
            exitGate.DecreaseEnemy();
        }
    }

    public IEnumerator DeathAnimationCoroutineInPlace(
        DeathAnimation deathAnimation,
        Vector2 startLocation,
        Vector2 smackLocation,
        Action onComplete
    )
    {
        if (deathAnimation.frozenSprite != null)
        {
            Particle.CreateWithSprite(
                deathAnimation.frozenSprite,
                1,
                startLocation,
                this.transform.parent
            );
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxPlayerBumpOnEnemy,
            new Audio.Options(pitchIncrement: 0.20f),
            this.position
        );
        this.map.timeFrozenByEnemyDeath = true;

        {
            var p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyHitPowAnimation,
                smackLocation,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Enemies";
            p.spriteRenderer.sortingOrder = -1;
        }

        yield return new WaitForSecondsRealtime(0.10f);

        Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        this.map.timeFrozenByEnemyDeath = false;

        foreach (var debris in deathAnimation.debris)
        {
            var p = Particle.CreateWithSprite(
                debris, 100, this.position, this.transform.parent
            );
            p.Throw(new Vector2(0, 8), 3, 1.5f, new Vector2(0, -0.4f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                p.spin *= -1;
        }
        onComplete?.Invoke();
    }

    private void CreateSmackEffect(KillInfo killInfo, Rect rectOfThisEnemy)
    {
        Rect RectOfAttackingThing()
        {
            if (killInfo.itemDeliveringKill?.hitboxes?.Length > 0)
                return killInfo.itemDeliveringKill.hitboxes[0].InWorldSpace();
            else
                return this.map.player.Rectangle();
        }

        float CenterOfOverlapRange(float min1, float max1, float min2, float max2)
        {
            if (min2 > max1) return (min2 + max1) / 2;
            if (min1 > max2) return (min1 + max2) / 2;
            float min = Mathf.Max(min1, min2);
            float max = Mathf.Min(max1, max2);
            return (min + max) / 2;
        }

        Vector2 CenterOfOverlap(Rect r1, Rect r2) => new Vector2(
            CenterOfOverlapRange(r1.xMin, r1.xMax, r2.xMin, r2.xMax),
            CenterOfOverlapRange(r1.yMin, r1.yMax, r2.yMin, r2.yMax)
        );

        Rect rectOfAttacker = RectOfAttackingThing();
        Vector2 centerOfIntersection = CenterOfOverlap(rectOfAttacker, rectOfThisEnemy);

        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyHitPowAnimation,
            centerOfIntersection,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -1;
    }

    public void TriggerRespawnAnimation()
    {
        Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnAnimation,
            this.position,
            this.transform.parent
        );

        // we need to keep the chunk active for long enough for that particle
        // to destroy itself, otherwise they build up.
        this.chunk.timeToRemainActive = 30;
    }

    protected void EnemyHitDetectionAgainstPlayer()
    {
        if (this.deathAnimationPlaying == true)
            return;
            
        foreach (var hitbox in this.hitboxes)
        {
            EnemyHitDetectionAgainstPlayer(hitbox.InWorldSpace());
        }
    }

    protected void EnemyHitDetectionAgainstPlayer(Rect ownRectangle)
    {
        if (this.deathAnimationPlaying == true)
            return;

        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        if (player.ShouldCollideWithEnemies() == false)
            return;

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return;

        var playerAbove =
            playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.VelocityLastFrame.y) > ownRectangle.yMax - 0.1f;
        var enemyVelocityY = (this is WalkingEnemy) ? ((WalkingEnemy)this).velocity.y : 0;
        var playerGoingDown =
            player.velocity.y < enemyVelocityY ||
            player.VelocityLastFrame.y < enemyVelocityY;

        if (player.invincibility.isActive == true)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Die)
        {
            player.BounceOffHitEnemy(this.transform.position);
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Nothing)
        {
            player.BounceOffHitEnemy((Vector2)this.transform.position);
        }
        else
        {
            player.Hit((Vector2)this.transform.position);
        }
    }

    public virtual void StopLoopedSfx()
    {

    }

    public virtual void Hit(KillInfo killInfo)
    {
        Kill(killInfo);
    }
}
