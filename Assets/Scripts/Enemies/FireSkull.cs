
using UnityEngine;
using System;

public class FireSkull : Enemy
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationHitWall;
    public Animated.Animation animationTurn;
    public Animated.Animation animationFreeze;
    public Animated.Animation animationInsideWater;
    public Animated.Animation animationFizzleOut;
    public Animated.Animation animationIgnite;
    public Animated.Animation animationGroundedWater;
    public SpriteRenderer iceBlockSprite;
    public Sprite[] debris;

    [Header("Glow")]
    public Animated glowAnimated;
    public Animated.Animation animationGlow;
    public Animated.Animation animationGlowOff;
    public Animated.Animation animationGlowFizzleOut;
    public Animated.Animation animationGlowBackOn;

    [NonSerialized] public Vector2 velocity;

    private Vector2 hitWallVelocity;
    private bool stopMovement;
    private Vector2 surfaceVelocity;

    private Vector2 spawnPosition;
    private int notDeadlyTime;
    private bool frozen;
    private int freezeTimer;
    private Vector2 savedVelocity;
    private Vector2 alignedPosition;
    private float currentShakeAmount = 0f;
    private float scaleX;
    private float currentScale;
    private bool insideWater;
    private int exitWaterTimer;
    private bool grounded;
    private int timeInsideWater = 0;

    private const float SensorLength = 0.8f;
    private const float CrushDistance = 1.25f;
    private const float WaterSinkingAcceleration = 0.004f;
    private const int NotDeadlyResetTime = 60;
    private const int FreezeTime = 60 * 8;
    private const int ExitWaterTime = 30;

    private const float SkullShakeAmount = 0.025f;
    private const float EveythingShakeAmount = 0.075f;
    private const float ShakeSpeed = 1f;
    private const float ShakeSkullStartTime = 60 * 2.5f;
    private const float ShakeEveythingStartTime = 60 * 1.5f;
    private static Vector2 InitialVelocity = new Vector2(0.0625f, 0.0625f);

    public override void Init(Def def)
    {
        base.Init(def);

        this.animated.PlayAndLoop(this.animationIdle);
        this.glowAnimated.PlayAndLoop(this.animationGlow);

        this.spawnPosition = this.position;

        float initialDirectionValue = this.def.tile.flip ? -1f : 1f;
        this.velocity.x = InitialVelocity.x * initialDirectionValue;
        this.velocity.y = InitialVelocity.y;
        this.savedVelocity = this.velocity;

        this.hitWallVelocity = Vector2.zero;
        this.stopMovement = false;
        this.surfaceVelocity = Vector2.zero;
        this.scaleX = initialDirectionValue;
        this.currentScale = 1f;

        this.frozen = false;
        RefreshHitbox();
        this.iceBlockSprite.transform.SetParent(this.transform.parent);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        //this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;

        NotifyChangeTemperature(this.map.currentTemperature);
    }

    private void RefreshHitbox()
    {
        if(this.frozen == true)
        {
            this.hitboxes = new [] {
                new Hitbox(-1.25f, 1.25f, -1.25f, 1.25f, this.rotation, Hitbox.Solid, true, true)
            };
        }
        else if(this.insideWater == true)
        {
            this.hitboxes = new [] {
                new Hitbox(-0.65f, 0.65f, -1.4f, 0.10f, this.rotation, Hitbox.NonSolid)
            };

            this.spriteRenderer.sortingLayerName = "Items (Front)";
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
            };
            
            this.spriteRenderer.sortingLayerName = "Enemies";
        }
    }

    private Vector2 GetRandomPositionWithinTriangle(Vector2 vertA, Vector2 vertB, Vector2 vertC)
    {
        var r1 = Mathf.Sqrt(UnityEngine.Random.Range(0f, 1f));
        var r2 = UnityEngine.Random.Range(0f, 1f);
        var m1 = 1 - r1;
        var m2 = r1 * (1 - r2);
        var m3 = r2 * r1;

        return (m1 * vertA) + (m2 * vertB) + (m3 * vertC);
    }

    public override void Advance()
    {
        if(this.frozen == true)
        {
            AdvanceFrozen();
        }
        else
        {
            AdvanceWater();

            if(this.insideWater == true || this.exitWaterTimer > 0)
            {
                if(this.timeInsideWater > 45)
                {
                    this.velocity += Vector2.down * WaterSinkingAcceleration;
                }

                EnemyHitDetectionAgainstPlayer();
                Integrate();
            }
            else
            {
                AdvanceWarm();
                this.grounded = false;
            }
        }

        this.position += this.velocity;
        this.transform.position = this.position;
        
        if(this.stopMovement == false &&
            this.animated.currentAnimation != this.animationHitWall)
        {
            RefreshScale();
        }
    }

    private void AdvanceFrozen()
    {
        if(this.freezeTimer < ShakeSkullStartTime)
        {
            bool canShakeEverything = this.freezeTimer < ShakeEveythingStartTime;
            float targetShakeSpeed = canShakeEverything ? EveythingShakeAmount : SkullShakeAmount;
            this.currentShakeAmount = Util.Slide(this.currentShakeAmount, targetShakeSpeed, 0.005f);
            float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * this.currentShakeAmount;

            if(canShakeEverything == true)
            {
                this.iceBlockSprite.transform.position =
                    new Vector2(this.alignedPosition.x + shake,  this.alignedPosition.y + shake);
            }
            
            this.position.x = this.alignedPosition.x + shake;
            this.position.y = this.alignedPosition.y + shake;
        }
        else
        {
            this.position.x = Util.Slide(this.position.x, this.alignedPosition.x, 0.1f);
            this.position.y = Util.Slide(this.position.y, this.alignedPosition.y, 0.1f);

            this.iceBlockSprite.transform.position = this.position;
        }

        if(this.freezeTimer > 0f)
        {
            this.freezeTimer -= 1;

            if(this.freezeTimer <= 0)
            {
                Unfreeze();
            }
        }
    }

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.whenHitFromAbove = HitFromAboveBehaviour.Die;

            this.animated.PlayAndHoldLastFrame(this.animationFizzleOut);

            this.animated.OnFrame(5, () =>
            {
                this.glowAnimated.PlayAndHoldLastFrame(this.animationGlowFizzleOut);
            });

            this.velocity = Vector2.zero;
            this.exitWaterTimer = 0;
            this.currentShakeAmount = 0f;

            RefreshHitbox();

            Audio.instance.PlaySfx(
                Assets.instance.sfxFireSkullFireOff,
                position: this.position
            );
        }

        if(this.exitWaterTimer > 0)
        {
            this.exitWaterTimer -= 1;
            
            this.currentShakeAmount = Util.Slide(this.currentShakeAmount, SkullShakeAmount, 0.005f);
            float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * this.currentShakeAmount;
            
            this.position.x = this.alignedPosition.x + shake;
            this.position.y = this.alignedPosition.y + shake;

            if(this.exitWaterTimer == 0)
            {
                // recover fire
                this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
                
                this.velocity.x = InitialVelocity.x * scaleX;
                this.velocity.y = InitialVelocity.y;
                this.stopMovement = false;
                this.currentShakeAmount = 0f;
            }
        }
        else if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
            this.exitWaterTimer = ExitWaterTime;
            this.alignedPosition = this.position;
            RefreshHitbox();
            
            this.animated.PlayOnce(this.animationIgnite, () =>
            {
                this.animated.PlayAndLoop(this.animationIdle);
            });
            
            this.animated.OnFrame(8, () =>
            {
                this.glowAnimated.PlayOnce(this.animationGlowBackOn, () =>
                {
                    this.glowAnimated.PlayAndLoop(this.animationGlow);
                });
            });
            Audio.instance.PlaySfx(
                Assets.instance.sfxFireSkullFireOn,
                position: this.position
            );
        }

        if(this.insideWater == true)
        {
            if(this.grounded == true &&
                this.animated.currentAnimation == this.animationFizzleOut)
            {
                this.animated.PlayAndLoop(this.animationGroundedWater);
            }

            this.timeInsideWater += 1;
        }
        else
        {
            this.timeInsideWater = 0;
        }
    }

    private void Integrate()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -0.75f), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, 0.75f), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -0.75f), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, 0.75f), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {

                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var halfWidth = (this.hitboxes[0].xMax - this.hitboxes[0].xMin) / 2f;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-halfWidth, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(halfWidth, 0), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var halfWidth = (this.hitboxes[0].xMax - this.hitboxes[0].xMin) / 2f;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-halfWidth, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(halfWidth, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {

                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.grounded = true;
            }
            else
            {
                this.position.y += this.velocity.y;
                this.grounded = false;
            }
        }
    }

    private void AdvanceWarm()
    {
        bool isDeadlyToPlayer = this.notDeadlyTime == 0;

        if(this.notDeadlyTime > 0)
        {
            this.notDeadlyTime -= 1;
        }

        if(this.velocity != Vector2.zero)
        {
            this.savedVelocity = this.velocity;
        }

        if(isDeadlyToPlayer == true)
        {
            // used here to support invincibility
            EnemyHitDetectionAgainstPlayer();
        }

        // spawn flame particle
        if(this.map.frameNumber % 3 == 0)
        {
            Vector2 flamePartSpawnCenter = this.position.Add(0f, 1f);
            Vector2 vert1 = flamePartSpawnCenter.Add(0f, 0.5f);
            Vector2 vert2 = flamePartSpawnCenter.Add(-1f, -0.35f);
            Vector2 vert3 = flamePartSpawnCenter.Add(1f, -0.35f);

            Vector2 particleSpawnPos = GetRandomPositionWithinTriangle(vert1, vert2, vert3);
            Animated.Animation particleAnimation = UnityEngine.Random.value > 0.5f ?
                Assets.instance.flameAnimationA : Assets.instance.flameAnimationB;

            Particle flameParticle = Particle.CreateAndPlayOnce(
                particleAnimation,
                particleSpawnPos,
                this.transform
            );

            flameParticle.spriteRenderer.sortingLayerName = "Enemies";
            flameParticle.spriteRenderer.sortingOrder = 2;
            flameParticle.acceleration = Vector2.up * 0.005f;
        }

        bool hitWall = false;
        bool changeDirection = false;
        Vector2 frameVelocity = this.velocity;
        bool isHitHorizontal = false;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), false, 0.75f)
        );

        if (this.velocity.x > 0)
        {
            if(right.anything == true)
            {
                this.velocity.x = -1 / 16f;
                hitWall = true;
                changeDirection = true;
                isHitHorizontal = true;
                
                if(right.tiles.Length > 0 && right.tiles[0] != null)
                {
                    this.surfaceVelocity = right.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if(left.anything == true)
            {
                this.velocity.x = 1 / 16f;
                hitWall = true;
                changeDirection = true;
                isHitHorizontal = true;
                
                if(left.tiles.Length > 0 && left.tiles[0] != null)
                {
                    surfaceVelocity = left.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if (this.velocity.y > 0)
        {
            if(up.anything == true)
            {
                this.velocity.y = -1 / 16f;
                hitWall = true;
                isHitHorizontal = false;
                
                if(up.tiles.Length > 0 && up.tiles[0] != null)
                {
                    surfaceVelocity = up.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            bool hitWater = false;

            if(this.map.theme == Theme.SunkenIsland)
            {
                Waterline waterline = this.map.waterline;
                hitWater = waterline.IsMoving == false &&
                    down.End().y < waterline.currentLevel;
            }

            if(down.anything == true || hitWater == true)
            {
                this.velocity.y = 1 / 16f;
                hitWall = true;
                isHitHorizontal = false;
                
                if(down.tiles.Length > 0 && down.tiles[0] != null)
                {
                    surfaceVelocity = down.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if(hitWall == true && this.stopMovement == false)
        {
            this.hitWallVelocity = this.velocity;
            this.stopMovement = true;
            BounceMelt(isHitHorizontal,
                isHitHorizontal ? frameVelocity.x > 0f : frameVelocity.y > 0f);

            Audio.instance.PlaySfx(Assets.instance.sfxFireSkullCollide, position: this.position);

            if(changeDirection == true)
            {
                this.animated.PlayOnce(this.animationHitWall, () => {
                    RefreshScale();
                    this.animated.PlayOnce(this.animationTurn, () => {
                        this.animated.PlayAndLoop(this.animationIdle);
                    });
                });
            }
            else
            {
                this.animated.PlayOnce(this.animationHitWall, () => {
                    this.animated.PlayAndLoop(this.animationIdle);
                });
            }

            this.animated.OnFrame(4, () => 
            {
                this.stopMovement = false;
                this.surfaceVelocity = Vector2.zero;
                this.velocity = this.hitWallVelocity;
            });
        }
        
        if(this.stopMovement == true)
        {
            this.velocity = surfaceVelocity;
        }

        HandleCrush(left, right, up, down);
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(newTemperature == Map.Temperature.Hot)
        {
            Unfreeze();
        }
        else if(newTemperature == Map.Temperature.Cold)
        {
            Freeze();
        }
    }

    private void BounceMelt(bool isHorizontal, bool signPositive)
    {
        Rect thisRect = this.hitboxes[0].InWorldSpace();

        float size = 1f;
        float hitX = signPositive ? thisRect.xMax : thisRect.xMin - size;
        float hitY = signPositive ? thisRect.yMax : thisRect.yMin - size;
        float width = thisRect.width + 0.50f;
        float height = thisRect.height + 0.50f;

        if(isHorizontal)
        {
            hitY = thisRect.yMin;
            width = size;
        }
        else
        {
            hitX = thisRect.xMin;
            height = size;
        }
        
        Rect hitRect = new Rect(hitX, hitY, width, height);
        this.map.Melt(hitRect, this);
    }

    private void Freeze()
    {
        if(this.frozen == true) return;
        this.frozen = true;

        this.velocity = Vector2.zero;
        this.alignedPosition = RoundToNearest(this.transform.position, 0.5f);
        this.stopMovement = false;
        this.currentShakeAmount = 0f;

        this.animated.PlayOnce(this.animationFreeze);
        this.iceBlockSprite.enabled = true;
        this.glowAnimated.PlayOnce(this.animationGlowOff);
        this.freezeTimer = FreezeTime;

        RefreshHitbox();
    }

    private void Unfreeze()
    {
        if(this.frozen == false) return;
        this.frozen = false;

        this.velocity = this.savedVelocity;
        this.currentScale = 1.30f;

        void ThrowDebris()
        {
            for (int i = 0; i < this.debris.Length; i++)
            {
                var p = Particle.CreateWithSprite(
                    this.debris[i],
                    60,
                    this.position + this.velocity +
                        Vector2.one * UnityEngine.Random.Range(-1.50f, 1.50f),
                    this.transform.parent
                );
                float velocityX = 0.25f * (i % 2 == 0 ? 1f : -1f);
                float velocityY = 0.15f;

                float gravity = -0.025f;

                p.Throw(new Vector2(velocityX, velocityY), 0.1f, 0.1f,
                    new Vector2(0f, gravity));

                p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
                if (UnityEngine.Random.Range(0, 2) == 0)
                    p.spin *= -1;
            }
        }

        this.animated.PlayAndLoop(this.animationIdle);
        this.iceBlockSprite.enabled = false;
        this.glowAnimated.PlayOnce(this.animationGlow);

        ThrowDebris();
        RefreshHitbox();
    }

    private void RefreshScale()
    {
        if(Mathf.Approximately(this.velocity.x, 0)) return;

        this.scaleX = this.velocity.x > 0 ? 1f : -1f;

        this.currentScale = Util.Slide(this.currentScale, 1f, 0.05f);
        this.transform.localScale = new Vector3(
            this.scaleX * this.currentScale, this.currentScale, this.currentScale
        );
    }

    private void HandleCrush(
        Raycast.CombinedResults left,
        Raycast.CombinedResults right,
        Raycast.CombinedResults up,
        Raycast.CombinedResults down)
    {
        bool IsCollisionValid(Raycast.CombinedResults r)
        {
            foreach(var i in r.items)
            {
                if(i is MetalBlock && (i as MetalBlock).IsBeingHeadbutted() == true)
                {
                    return false;
                }
            }

            return true;
        }

        bool upAnything = up.anything;
        bool downAnything = down.anything;
        bool leftAnything = left.anything;
        bool rightAnything = right.anything;

        if(IsCollisionValid(up) == false) upAnything = false;
        if(IsCollisionValid(down) == false) downAnything = false;
        if(IsCollisionValid(left) == false) leftAnything = false;
        if(IsCollisionValid(right) == false) rightAnything = false;

        if (upAnything && downAnything &&
            up.distance + down.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }

        if (leftAnything && rightAnything &&
            left.distance + right.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }
    }

    public override void Reset()
    {
        this.position = this.spawnPosition;
        this.notDeadlyTime = NotDeadlyResetTime;

        TriggerRespawnAnimation();
        Init(def);
    }

    public override bool Kill(KillInfo info)
    {
        if (info.isBecauseLevelIsResetting == true)
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
            TriggerRespawnAnimation();

            Audio.instance.PlaySfx(Assets.instance.sfxFireSkullSpawn, position: this.position);
        }
        else if(info.itemDeliveringKill == this)
        {
            TriggerNewDeathAnimation(info, delegate
            {
                Reset();
            });
        }
        else
        {   
            TriggerNewDeathAnimation(info, delegate
            {
                Destroy(this.gameObject);
            });
            this.chunk.items.Remove(this);
            
            DropLuckyPickup();
        }

        return true;
    }

    private Vector2 RoundToNearest(Vector2 currentVector, float roundTo)
    {
        float x = RoundToGrid(currentVector.x);
        float y = RoundToGrid(currentVector.y);
        return new Vector2(x, y);
    }

    private float RoundToGrid(float currentNumber)
    {
        float integralPart = (float)Math.Truncate(currentNumber);
        return integralPart + 0.50f;
    }
}
