using UnityEngine;
using System;

public class FishSkull : Enemy
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationHitWall;
    public Animated.Animation animationTurn;
    public Animated.Animation animationFlopping;

    public enum State
    {
        // outside of water
        FloppingOnGround,
        FallingToGround,

        // inside water
        MovingInWater
    }

    [NonSerialized] public Vector2 velocity;
    private State state;
    private bool insideWater;
    private bool grounded;
    private Vector2 hitWallVelocity;
    private bool stopMovement;
    private Vector2 surfaceVelocity;
    private Vector2 spawnPosition;
    private Vector2 savedVelocity;
    private Vector2 alignedPosition;
    private float scaleX;
    private float currentScale;
    private int flopTimer;
    private Vector2 shakePosition;
    private float currentShakeAmount;
    private float currentAngle;
    private float targetAngle;

    private const float CrushDistance = 1.25f;
    private const float GravityAcceleration = 0.006f;
    private const float FlopVerticalForce = 0.22f;
    private const float ShakeAmount = 0.075f;
    private const float ShakeSpeed = 1f;
    private const float OutsideWaterAngleSpeed = 1.5f;
    private const int OutsideWaterTargetAngle = 180;
    private const int OutsideWaterAngleFlopRange = 5;
    private const int FlopTime = 2 * 60;
    private const int EscapeWaterTime = 60;
    
    private static Vector2 InitialVelocity = new Vector2(0.0625f, 0.0625f);
    private static Vector2 HitboxSize = new Vector2(0.75f, 0.75f);

    private float DistanceToPlayer =>
        Vector2.Distance(this.position, this.map.player.position);
    private bool SameChunkAsPlayer =>
        this.chunk == this.map.NearestChunkTo(this.map.player.position);
    
    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.animated.PlayAndLoop(this.animationIdle);

        this.spawnPosition = this.position;

        float initialDirectionValue = this.def.tile.flip ? -1f : 1f;
        this.velocity.x = InitialVelocity.x * initialDirectionValue;
        this.velocity.y = InitialVelocity.y;
        this.savedVelocity = this.velocity;

        this.hitWallVelocity = Vector2.zero;
        this.stopMovement = false;
        this.surfaceVelocity = Vector2.zero;
        this.scaleX = initialDirectionValue;
        this.currentScale = 1f;

        this.hitboxes = new [] {
            new Hitbox(
                -HitboxSize.x, HitboxSize.x, -HitboxSize.y, HitboxSize.y,
                this.rotation, Hitbox.NonSolid
            )
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        
        ChangeState(State.FallingToGround);
    }

    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        
        AdvanceWater();

        switch (state)
        {
            case State.FloppingOnGround:
                AdvanceStrandedOnGround();
                break;

            case State.FallingToGround:
                AdvanceFallingToGround();
                break;

            case State.MovingInWater:
                AdvanceMovingInWater();
                break;
        }

        EnemyHitDetectionAgainstPlayer();

        this.transform.position = this.position + this.shakePosition; // CHANGE THIS

        this.currentAngle = Util.Slide(this.currentAngle, this.targetAngle, 10);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.currentAngle);
        
        if(this.stopMovement == false &&
            this.animated.currentAnimation != this.animationHitWall &&
            this.animated.currentAnimation != this.animationTurn)
        {
            RefreshScale();
        }
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y &&
            (DistanceToPlayer < 15f || SameChunkAsPlayer == true))
        {
            // enter water
            this.insideWater = true;
            this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
            this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        }
        
        if(this.insideWater == false)
        {
            this.velocity += Vector2.down * GravityAcceleration;
        }
    }

    private void AdvanceStrandedOnGround()
    {
        float angleFlopSpeedRate = this.map.frameNumber * OutsideWaterAngleSpeed;
        float angleFlopRange =
            Mathf.PingPong(angleFlopSpeedRate, OutsideWaterAngleFlopRange * 2) - OutsideWaterAngleFlopRange;
        this.targetAngle = OutsideWaterTargetAngle + angleFlopRange;

        Integrate();

        if(this.grounded == true && this.flopTimer < FlopTime)
        {
            this.flopTimer += 1;
            
            this.currentShakeAmount = Util.Slide(this.currentShakeAmount, ShakeAmount, 0.001f);
            float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * this.currentShakeAmount;
            
            this.shakePosition += Vector2.one * shake;

            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemySkullfishFlopLoop))
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemySkullfishFlopLoop, 
                    position: this.position,
                    options: new Audio.Options(0.7f, false, 0)
                );
            }
            if (this.flopTimer == FlopTime)
            {
                // flop movement
                this.flopTimer = 0;
                this.velocity = Vector2.up * FlopVerticalForce;
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemySkullfishJump,
                    position: this.position
                );
                Audio.instance.StopSfxLoop(Assets.instance.sfxEnemySkullfishFlopLoop);
            }
        }
        else
        {
            this.shakePosition = Vector2.zero;
            this.currentShakeAmount = 0f;
            if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemySkullfishFlopLoop))
            {
                Audio.instance.StopSfx(Assets.instance.sfxEnemySkullfishFlopLoop);
            }
        }

        if(this.insideWater == true)
        {
            ChangeState(State.MovingInWater);
        }
    }

    private void AdvanceFallingToGround()
    {
        float angleFlopSpeedRate = this.map.frameNumber * OutsideWaterAngleSpeed;
        float angleFlopRange =
            Mathf.PingPong(angleFlopSpeedRate, OutsideWaterAngleFlopRange * 2) - OutsideWaterAngleFlopRange;
        this.targetAngle = OutsideWaterTargetAngle + angleFlopRange;

        Integrate();

        if(this.insideWater == true)
        {
            ChangeState(State.MovingInWater);
        }
        else if(this.grounded == true)
        {
            ChangeState(State.FloppingOnGround);
        }
    }

    private void AdvanceMovingInWater()
    {
        if(this.velocity != Vector2.zero)
        {
            this.savedVelocity = this.velocity;
        }

        bool hitWall = false;
        bool changeDirection = false;
        Vector2 frameVelocity = this.velocity;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), false, 0.75f)
        );

        if (this.velocity.x > 0)
        {
            if(right.anything == true)
            {
                this.velocity.x = -1 / 16f;
                hitWall = true;
                changeDirection = true;
                
                if(right.tiles.Length > 0 && right.tiles[0] != null)
                {
                    this.surfaceVelocity = right.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if(left.anything == true)
            {
                this.velocity.x = 1 / 16f;
                hitWall = true;
                changeDirection = true;
                
                if(left.tiles.Length > 0 && left.tiles[0] != null)
                {
                    surfaceVelocity = left.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if (this.velocity.y > 0)
        {
            if(up.anything == true || up.End().y > this.map.waterline.currentLevel)
            {
                this.velocity.y = -1 / 16f;
                hitWall = true;
                
                if(up.tiles.Length > 0 && up.tiles[0] != null)
                {
                    surfaceVelocity = up.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if(down.anything == true)
            {
                this.velocity.y = 1 / 16f;
                hitWall = true;
                
                if(down.tiles.Length > 0 && down.tiles[0] != null)
                {
                    surfaceVelocity = down.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if(hitWall == true && this.stopMovement == false)
        {
            this.hitWallVelocity = this.velocity;
            this.stopMovement = true;

            if(changeDirection == true)
            {
                this.animated.PlayOnce(this.animationTurn, () => {
                    this.animated.PlayAndLoop(this.animationIdle);
                    RefreshScale();
                });
            }
            else
            {
                this.animated.PlayOnce(this.animationHitWall, () => {
                    this.animated.PlayAndLoop(this.animationIdle);
                });
            }

            this.animated.OnFrame(1, () => 
            {
                this.stopMovement = false;
                this.surfaceVelocity = Vector2.zero;
                this.velocity = this.hitWallVelocity;
            });

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemySkullfishGround,
                position: this.position,
                options: new Audio.Options(0.6f, true, 0)
            );
        }
        
        if(this.stopMovement == true)
        {
            this.velocity = surfaceVelocity;
        }

        HandleCrush(left, right, up, down);

        if(this.insideWater == false)
        {
            ChangeState(State.FallingToGround);
        }
        
        this.position += this.velocity;
    }
    
    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.FloppingOnGround:
                EnterFloppingOnGround();
                break;

            case State.FallingToGround:
                EnterFallingToGround();
                break;

            case State.MovingInWater:
                EnterMovingInWater();
                break;
        }
    }

    private void EnterFloppingOnGround()
    {
        this.velocity = Vector2.zero;
        this.animated.PlayAndLoop(this.animationFlopping);
    }

    private void EnterFallingToGround()
    {
        this.velocity = Vector2.zero;
        this.targetAngle = 180;
        this.animated.PlayAndLoop(this.animationFlopping);
    }

    private void EnterMovingInWater()
    {
        this.velocity.x = InitialVelocity.x * this.scaleX;
        this.velocity.y = InitialVelocity.y;
        this.targetAngle = 0;
        this.stopMovement = false;
        this.animated.PlayAndLoop(this.animationIdle);
    }

    private void Integrate()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {

                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            this.grounded = false;

            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                
                if(result.tiles.Length > 0)
                {
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;
                }

                if(result.items.Length > 0 && result.items[0] is Raft)
                {
                    Raft raft = result.items[0] as Raft;
                    surfaceVelocity = raft.velocity;
                }

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.grounded = true;
            }
            else
            {
                this.position.y += this.velocity.y;
                this.grounded = false;
            }
        }
    }

    private void RefreshScale()
    {
        if(this.velocity != Vector2.zero)
            this.scaleX = this.velocity.x > 0 ? 1f : -1f;

        this.currentScale = Util.Slide(this.currentScale, 1f, 0.05f);
        this.transform.localScale = new Vector3(
            this.scaleX * this.currentScale, this.currentScale, this.currentScale
        );
    }

    private void HandleCrush(
        Raycast.CombinedResults left,
        Raycast.CombinedResults right,
        Raycast.CombinedResults up,
        Raycast.CombinedResults down)
    {
        bool IsCollisionValid(Raycast.CombinedResults r)
        {
            foreach(var i in r.items)
            {
                if(i is MetalBlock && (i as MetalBlock).IsBeingHeadbutted() == true)
                {
                    return false;
                }
            }

            return true;
        }

        bool upAnything = up.anything;
        bool downAnything = down.anything;
        bool leftAnything = left.anything;
        bool rightAnything = right.anything;

        if(IsCollisionValid(up) == false) upAnything = false;
        if(IsCollisionValid(down) == false) downAnything = false;
        if(IsCollisionValid(left) == false) leftAnything = false;
        if(IsCollisionValid(right) == false) rightAnything = false;

        if (upAnything && downAnything &&
            up.distance + down.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }

        if (leftAnything && rightAnything &&
            left.distance + right.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }
    }

}
