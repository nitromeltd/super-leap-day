using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingDuck : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurnGround;
    public Animated.Animation animationTurnWater;
    public Animated.Animation animationJump;
    public Animated.Animation animationJumpDrop;
    public Animated.Animation animationSwim;

    public Animated.Animation animationWaterSplash;

    private enum State
    {
        WalkOnLand,
        JumpToWater,
        SwimInWater,
        JumpToLand
    }
    
    private State state = State.WalkOnLand;
    private bool isTurning = false;
    private bool insideWater = false;
    private int stillTimer = 0;
    private int jumpTimer = 0;
    private float yScale;
    
    private const float GravityAcceleration = 0.006f;
    private const float WaterBuoyancy = 0.0025f;
    private const float WalkSpeed = 0.04f;
    private const float SwimSpeed = 0.03f;
    private const int ResumeWalkOnLandTime = 20;
    private const int JumpToWaterTime = 10;
    private const int JumpToLandTime = 5;

    private static Vector2 HitboxSize = new Vector2(0.65f, 0.9f);
    private static Vector2 HitboxOffset = new Vector2(0f, -0.10f);

    private Vector2 BuoyancyPointOffset => new Vector2(0f, 0.60f);
    private Vector2 BuoyancyPointPosition => this.position - BuoyancyPointOffset;
    
    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(
                HitboxOffset.x - HitboxSize.x,
                HitboxOffset.x + HitboxSize.x,
                HitboxOffset.y - HitboxSize.y,
                HitboxOffset.y + HitboxSize.y,
                this.rotation, Hitbox.NonSolid)
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.onTurnAround = OnTurnAround;
        this.yScale = 1f;

        ChangeState(State.WalkOnLand);
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        switch(state)
        {
            case State.WalkOnLand:
            {
                Turn(this.animationTurnGround);
            }
            break;

            case State.SwimInWater:
            {
                Turn(this.animationTurnWater);
            }
            break;
        }
    }

    private void Turn(Animated.Animation turnAnimation)
    {
        this.isTurning = true;
        this.velocity.x = 0f;

        this.animated.PlayOnce(turnAnimation, () =>
        {
            this.isTurning = false;
            this.transform.localScale = new Vector3(
                this.facingRight ? 1f : -1f, this.yScale, 1f
            );
            ChangeState(this.state);
        });
    }
    
    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        if (this.deathAnimationPlaying == true) return;

        AdvanceWater();
        
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        switch(this.state)
        {
            case State.WalkOnLand:
                AdvanceWalkOnLand(raycast);
                break;

            case State.JumpToWater:
                AdvanceJumpToWater();
                break;

            case State.SwimInWater:
                AdvanceSwimInWater(raycast);
                break;

            case State.JumpToLand:
                AdvanceJumpToLand();
                break;
        }
        
        Move(raycast);
        
        EnemyHitDetectionAgainstPlayer();
        this.transform.position = this.position;

        this.yScale = Util.Slide(this.yScale, 1f, 0.0075f);
        if(this.isTurning == true)
        {
            this.transform.localScale = new Vector3(
                this.facingRight ? -1f : 1f, this.yScale, 1f
            );
        }
        else
        {
            this.transform.localScale = new Vector3(
                this.facingRight ? 1f : -1f, this.yScale, 1f
            );
        }

    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > BuoyancyPointPosition.y)
        {
            // enter water
            this.insideWater = true;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < BuoyancyPointPosition.y)
        {
            // exit water
            this.insideWater = false;
        }
        
        if(this.insideWater == true)
        {
            float targetPositionY = waterline.currentLevel;
            Vector2 targetPosition = new Vector2(BuoyancyPointPosition.x, targetPositionY);
            Vector2 heading = targetPosition - BuoyancyPointPosition;
            float distance = heading.magnitude;

            if(waterline.IsMoving == true)
            {
                if(distance < waterline.VelocityThreshold * 2f)
                {
                    // sync with waterline velocity
                    this.velocity.y = waterline.Velocity().y;
                }
                else
                {
                    // raft is submerged, raise with buoyancy-like physics
                    this.velocity += Vector2.up * WaterBuoyancy;
                }
            }
            else
            {
                if(distance > 2f)
                {
                    // raft is submerged, raise with buoyancy-like physics
                    this.velocity += Vector2.up * WaterBuoyancy;
                }
                else if(distance > 0f)
                {
                    Vector2 direction = heading / distance;

                    if(this.velocity.y > distance)
                    {
                        // stop raft on settle position
                        this.position.y = targetPosition.y + BuoyancyPointOffset.y;
                        this.velocity.y = 0f;
                    }
                    else
                    {
                        // move raft slowly to settle position
                        this.velocity.y = direction.y * 0.02f;
                    }
                }
            }
        }
        else
        {
            this.velocity += Vector2.down * GravityAcceleration;
        }
    }

    private void AdvanceWalkOnLand(Raycast raycast)
    {
        bool SuitableWaterAhead(Raycast raycast)
        {
            var reach = edgeTurnSensorForward + 1f;
            var xOffset = this.facingRight ?
                this.hitboxes[0].xMax + reach :
                this.hitboxes[0].xMin - reach;

            var start = this.position + new Vector2(xOffset, 0);

            var maxDistance = -this.hitboxes[0].yMin + 3.50f;
            var r1 = raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance);
            var r2 = raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance);
            var r3 = raycast.Vertical(start + new Vector2( 0.375f, 0), false, maxDistance);
            
            var result = Raycast.CombineClosest(r1, r2, r3);
            return result.anything == false &&
                (start.y - maxDistance) < this.map.waterline.currentLevel;
        }

        if(this.stillTimer > 0)
        {
            this.stillTimer -= 1;

            if(this.stillTimer == 0)
            {
                this.velocity.x = WalkSpeed * (this.facingRight ? 1f : -1f);
            }
        }

        bool waterAhead = SuitableWaterAhead(raycast);

        if(this.insideWater == true)
        {
            ChangeState(State.SwimInWater);
        }
        else if(waterAhead == true)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyQuackerJump,
                position: this.position
            );
            ChangeState(State.JumpToWater);
        }
    }

    private void AdvanceJumpToWater()
    {
        if(this.jumpTimer > 0)
        {
            this.jumpTimer -= 1;

            if(this.jumpTimer == 0)
            {
                this.velocity += new Vector2(0.075f * (this.facingRight ? 1f : -1f), 0.1f);
                this.yScale = 0.90f;
            }
        }
        
        if(this.insideWater == true)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyQuackerWaterIn,
                position: this.position
            );

            var splash = Particle.CreateAndPlayOnce(
                animationWaterSplash,
                this.transform.position + new Vector3(0f, -0.5f, 0f),
                this.transform.parent
            );

            splash.spriteRenderer.sortingLayerName = "Items (Front)";
            splash.spriteRenderer.sortingOrder = -1;
            ChangeState(State.SwimInWater);
        }
        else if(this.onGround == true && this.jumpTimer == 0 && this.velocity.y <= 0f)
        {
            ChangeState(State.WalkOnLand);
        }
    }

    private void AdvanceSwimInWater(Raycast raycast)
    {
        bool SuitableLandAhead(Raycast raycast)
        {
            var rect = this.hitboxes[0];

            float forwardAmount = Mathf.Abs(velocity.x);
            float distance = forwardAmount + 1f;

            var tooHigh = this.position.Add(0, rect.yMax + 1.50f);
            var low  = this.position.Add(0, -0.30f);
            
            var maxDistance = this.facingRight ?
                rect.xMax + distance :
                -rect.xMin + distance;

            var rHigh = raycast.Horizontal(tooHigh, this.facingRight, maxDistance);
            var rLow  = raycast.Horizontal(low, this.facingRight, maxDistance);
            
            if (this.facingRight)
            {
                if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
                if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;
            }
            else
            {
                if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
                if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;
            }

            return rLow.anything == true && rHigh.anything == false;
        }

        if(this.isTurning == false)
        {
            this.velocity.x = this.map.waterline.IsMoving ?
                0f: SwimSpeed * (this.facingRight ? 1f : -1f);
        }

        bool landAhead = SuitableLandAhead(raycast);

        if(this.insideWater == false)
        {
            ChangeState(State.WalkOnLand);
        }
        else if(landAhead == true)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyQuackerWaterOut,
                position: this.position
            );
            ChangeState(State.JumpToLand);
        }
    }

    private void AdvanceJumpToLand()
    {
        if(this.jumpTimer > 0)
        {
            this.jumpTimer -= 1;

            if(this.jumpTimer == 0)
            {
                this.velocity += new Vector2(0.05f * (this.facingRight ? 1f : -1f), 0.2f);
                this.yScale = 0.90f;
            }
        }

        if(this.onGround == true)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyQuackerLand,
                position: this.position
            );
            ChangeState(State.WalkOnLand);
        }
        else if(this.insideWater == true && this.jumpTimer == 0 && this.velocity.y <= 0f)
        {
            ChangeState(State.SwimInWater);
        }
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch(newState)
        {
            case State.WalkOnLand:
                EnterWalkOnLand();
                break;

            case State.JumpToWater:
                EnterJumpToWater();
                break;

            case State.SwimInWater:
                EnterSwimInWater();
                break;

            case State.JumpToLand:
                EnterJumpToLand();
                break;
        }
    }

    private void EnterWalkOnLand()
    {
        this.animated.PlayAndLoop(this.animationWalk);
        this.stillTimer = ResumeWalkOnLandTime;
        this.velocity.x = 0f;
    }

    private void EnterJumpToWater()
    {
        this.animated.PlayOnce(this.animationJump);
        this.jumpTimer = JumpToWaterTime;
        this.velocity = Vector2.zero;
    }

    private void EnterSwimInWater()
    {
        this.animated.PlayAndLoop(this.animationSwim);
    }

    private void EnterJumpToLand()
    {
        this.animated.PlayOnce(this.animationJump);
        this.jumpTimer = JumpToLandTime;

        this.velocity.x = 0f;
        this.velocity.y = -0.10f;
    }
}
