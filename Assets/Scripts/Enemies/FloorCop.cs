﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorCop : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationAlert;
    public Animated.Animation animationDropIn;
    public Animated.Animation animationSpinAnticipation;
    public Animated.Animation animationSpin;
    public Animated.Animation animationSpinEnd;
    public Animated.Animation animationComeOut;
    public Animated.Animation animationShakeHead;
    public Animated.Animation animationTurn;
    public Animated.Animation animationFall;
    public Animated.Animation animationRise;
    public Animated.Animation animationHit;

    [Header("Lights")]
    public Animated lightsAnimated;
    public Animated.Animation animationLightsOff;
    public Animated.Animation animationLightsOn;

    private State state = State.Patrol;
    private State previousState = State.Patrol;
    private bool isTurning = false;
    private float visibleAngleDegrees = 0f;
    private float airAngleDegrees = 0f;
    private int lostPlayerTimer = 0;

    public enum State
    {
        Patrol, // waiting for player
        Alert, // detects player
        Chase, // spin towards player, when chasing you he can:
                    // jump up ledges
                    // jump across floor gaps (of a certain width)
                    // (!) go up curves and slopes (and onto the next floor if possible)
                    // (!!) follow you on grip ground, jumping when you jump
        Lost, // lost track of player
        Stun,
        Airborne
    }

    private const float DetectPlayerDistance = 10f;
    private const float DetectPlayerHeight = 7.00f;
    private const float LostPlayerDistance = DetectPlayerDistance + 7.5f;
    private const float PatrolSpeed = 0.04f;
    private const float ChaseSpeed = 0.25f;
    private const float Gravity = 0.055f;
    private const int MinChasePlayerTime = 45;

    private static Vector2 JumpHorizontalForce = new Vector2(0.48f, 0.62f);
    private static Vector2 JumpVerticalForce = new Vector2(0.10f, 0.66f);

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.edgeTurnSensorForward = 0f;
        
        this.onTurnAround = delegate
        {
            if(this.state == State.Patrol)
            {
                this.isTurning = true;
                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.isTurning = false;
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            }
        };

        ChangeState(State.Patrol);
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true)
            return;

        this.velocity.y -= Gravity;
        
        if(IsAirborne() == true &&
            this.state != State.Alert &&
            this.state != State.Chase &&
            this.state != State.Stun &&
            this.state != State.Airborne)
        {
            ChangeState(State.Airborne);
        }

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        switch(this.state)
        {
            case State.Patrol:
                AdvancePatrol(raycast);
                break;

            case State.Alert:
                AdvanceAlert(raycast);
                break;

            case State.Chase:
                AdvanceChase(raycast);
                break;

            case State.Lost:
                AdvanceLost(raycast);
                break;

            case State.Stun:
                AdvanceStun(raycast);
                break;

            case State.Airborne:
                AdvanceAirborne(raycast);
                break;
        }

        FollowPortals();
        EnemyHitDetectionAgainstPlayer();
        
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    public override void Hit(KillInfo killInfo)
    {
        if(killInfo.itemDeliveringKill is FloorCop) return;
        
        if(IsAirborne() == false)
        {
            ChangeState(State.Stun);
        }
    }

    private void RefreshRotationDefault()
    {
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
            
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 3f
        );

        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    private void ChangeState(State newState)
    {
        switch(this.state)
        {
            case State.Patrol:
                ExitPatrol();
                break;

            case State.Chase:
                ExitChase();
                break;
        }

        this.previousState = this.state;
        this.state = newState;

        switch(newState)
        {
            case State.Patrol:
                EnterPatrol();
                break;

            case State.Alert:
                EnterAlert();
                break;

            case State.Chase:
                EnterChase();
                break;

            case State.Lost:
                EnterLost();
                break;

            case State.Stun:
                EnterStun();
                break;

            case State.Airborne:
                EnterAirborne();
                break;
        }
    }

    private void AdvancePatrol(Raycast raycast)
    {
        this.velocity.x = Util.Slide(this.velocity.x, 
            PatrolSpeed * (facingRight ? 1f : -1f),
            0.01f);

        Move(raycast);
        RefreshRotationDefault();

        if(IsPlayerDetected(DetectPlayerDistance) == true &&
            this.isTurning == false)
        {
            ChangeState(State.Alert);
        }
    }

    private void AdvanceAlert(Raycast raycast)
    {
        Move(raycast);
        RefreshRotationDefault();
    }

    private void AdvanceChase(Raycast raycast)
    {
        this.velocity.x = Util.Slide(
            this.velocity.x,
            ChaseSpeed * (facingRight ? 1f : -1f),
            0.03f);

        bool lostPlayer = false;
        
        Move(raycast, stopHorizontally: true);
        
        if(IsPlayerDetected(LostPlayerDistance) == false)
        {
            this.lostPlayerTimer += 1;

            if(IsAirborne() == false &&
                this.lostPlayerTimer >= MinChasePlayerTime)
            {
                ChangeState(State.Lost);
                lostPlayer = true;
            }
        }
        else
        {
            this.lostPlayerTimer = 0;
        }

        if(lostPlayer == false)
        {
            Chase(raycast);
            RefreshRotationChase();
        }
        
        if (this.map.frameNumber % 2 == 0)
            CreateDustParticles();
        
        Rect thisRect = this.hitboxes[0].InWorldSpace();
        Rect hitRect =
            this.facingRight == true ?
            new Rect(thisRect.xMax, thisRect.yMin, 1, thisRect.height + 0.50f) :
            new Rect(thisRect.xMin - 1, thisRect.yMin, 1, thisRect.height + 0.50f);

        this.map.Damage(hitRect, new KillInfo { itemDeliveringKill = this });
    }

    private void RefreshRotationChase()
    {
        this.airAngleDegrees = this.velocity.y > 0f ? -10f : 10f;

        this.visibleAngleDegrees = onGround ?
            Util.Slide(this.visibleAngleDegrees, this.groundAngleDegrees, 5f) :
            Util.Slide(this.visibleAngleDegrees, this.airAngleDegrees, 5f);

        this.transform.rotation = Quaternion.Euler(0, 0,
            this.visibleAngleDegrees * (facingRight ? 1f : -1f));
    }

    private void CreateDustParticles()
    {
        float x = this.position.x;
        float y = this.position.y - 1.60f;

        Particle p = Particle.CreateAndPlayOnce(
            this.map.player.animationDustParticle,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.70f, 1.0f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.1f, 0.1f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void AdvanceLost(Raycast raycast)
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.005f);
        
        Move(raycast);
        RefreshRotationDefault();

        if(IsPlayerDetected(DetectPlayerDistance) == true &&
            this.animated.currentAnimation == this.animationShakeHead)
        {
            ChangeState(State.Alert);
        }
    }

    private void AdvanceStun(Raycast raycast)
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.02f);
        
        Move(raycast);
    }

    private void AdvanceAirborne(Raycast raycast)
    {
        Move(raycast);

        if(IsAirborne() == false)
        {
            ChangeState(State.Patrol);
        }
    }

    private void EnterPatrol()
    {
        this.state = State.Patrol;

        ChangeHitbox(false);
        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void ExitPatrol()
    {
        this.isTurning = false;
    }

    private void EnterAlert()
    {
        this.state = State.Alert;

        this.velocity.x = 0f;
        this.velocity.y = 0.25f;
        this.animated.PlayOnce(this.animationAlert, () => 
        {
            ChangeHitbox(true);
            this.animated.PlayOnce(this.animationDropIn, () =>
                { ChangeState(State.Chase); });
        });   
    }

    private void EnterChase()
    {
        this.state = State.Chase;

        this.animated.PlayOnce(this.animationSpinAnticipation, () =>
        {
            this.animated.PlayAndLoop(this.animationSpin);
            this.lightsAnimated.PlayAndLoop(this.animationLightsOn);
        });
    }

    private void ExitChase()
    {
        this.lightsAnimated.PlayOnce(this.animationLightsOff);
    }

    private void EnterLost()
    {
        this.state = State.Lost;

        this.lostPlayerTimer = 0;

        void ComeOut()
        {
            this.animated.PlayOnce(this.animationComeOut, () => 
            {
                this.animated.PlayOnce(this.animationShakeHead, () =>
                {
                    ChangeState(State.Patrol);
                });
            });
            
            ChangeHitbox(false);
            RefreshHitboxes();

            // break blocks over hitbox after coming out of tank
            Rect currentRect = this.hitboxes[0].InWorldSpace();
            currentRect.Inflate(1f);
            this.map.Damage(currentRect, new KillInfo { itemDeliveringKill = this });
        }

        if(this.previousState == State.Stun)
        {
            ComeOut();
        }
        else
        {
            this.animated.PlayOnce(this.animationSpinEnd, () => 
            {
                ComeOut();
            });
        }
    }

    private void EnterStun()
    {
        this.state = State.Stun;

        this.animated.PlayOnce(this.animationHit, () =>
        {
            ChangeState(State.Lost);
        });
    }

    private void EnterAirborne()
    {
        this.state = State.Airborne;

        Animated.Animation animationAirborne = this.velocity.y > 0f ?
            this.animationRise : this.animationFall;

        this.animated.PlayAndLoop(animationAirborne);
    }

    private bool IsPlayerDetected(float detectDistance)
    {
        var rect = this.hitboxes[0];
        Player player = this.map.player;

        float currentPlayerDistance =
            Vector2.Distance(player.position, this.position);
        bool isPlayerInDesiredHeight =
            player.position.y > this.position.y - DetectPlayerHeight &&
            player.position.y < this.position.y + DetectPlayerHeight;

        return (currentPlayerDistance < detectDistance) && isPlayerInDesiredHeight;
    }

    private void JumpHorizontally()
    {
        this.velocity.x = JumpHorizontalForce.x * (facingRight ? 1f : -1f);
        this.velocity.y = JumpHorizontalForce.y;
    }

    private void JumpVertically()
    {
        this.velocity.x = JumpVerticalForce.x * (facingRight ? 1f : -1f);
        this.velocity.y = JumpVerticalForce.y;
    }

    private void Chase(Raycast raycast)
    {
        float forwardAmount = Mathf.Abs(velocity.x);
        if (forwardAmount == 0)
            return;

        this.facingRight = velocity.x > 0;

        if (this.pauseAfterTurnRemainingTime > 0)
        {
            this.pauseAfterTurnRemainingTime -= 1;
            return;
        }

        this.trappedHorizontally = false;

        var forward = SenseHorizontal(
            raycast, this.facingRight, forwardAmount, out float correctionX
        );
        var turnHere =
            forward.anything == true ||
            (turnAtEdges == true && IsAtEdgeOfPlatform(raycast, this.facingRight, false));

        if(CanJumpWall(raycast))
        {
            JumpVertically();
            turnHere = false;
        }

        if (turnHere == false)
        {
            this.position.x += this.facingRight ? forwardAmount : -forwardAmount;
            return;
        }

        var backward = SenseHorizontal(
            raycast, !this.facingRight, forwardAmount + 1, out float backwardCorrectionX
        );
        var wouldAlsoTurnGoingBackwards =
            backward.anything == true ||
            (turnAtEdges == true && IsAtEdgeOfPlatform(raycast, !this.facingRight, true));

        if (wouldAlsoTurnGoingBackwards == false ||
            stopIfTrappedHorizontally == false)
        {
            var c = new Item.Collision { otherItem = this };
            if (this.facingRight == true)
            {
                foreach (var i in forward.items)
                    i.onCollisionFromLeft?.Invoke(c);
            }
            else
            {
                foreach (var i in forward.items)
                    i.onCollisionFromRight?.Invoke(c);
            }

            if(forward.anything == true )
            {
                this.position.x += correctionX;
                this.velocity.x *= -1;
                this.facingRight = (this.velocity.x > 0);
                this.pauseAfterTurnRemainingTime = 5;
            
                this.onTurnAround?.Invoke(new TurnInfo
                {
                    raycastResults = forward,
                    turnReason =
                        forward.anything == true ? TurnReason.Wall : TurnReason.Ledge
                });

                Audio.instance.PlaySfx(
                    Assets.instance.sfxHelmutHelmetBounce, position: this.position
                );
            }
            else if(IsGapSmallEnough())
            {
                JumpHorizontally();
            }
        }
        else
        {
            // stuck (neither direction has any exits)
            this.trappedHorizontally = true;
        }
    }

    protected bool CanJumpWall(Raycast raycast)
    {
        if(this.onGround == false) return false;

        var rect = this.hitboxes[0];

        float forwardAmount = Mathf.Abs(velocity.x);
        float distance = forwardAmount + 2f;

        var tooHigh = this.position.Add(0, rect.yMax + 1.25f);
        var low  = this.position.Add(0, rect.yMin + 0.02f);
        
        var maxDistance = this.facingRight ?
            rect.xMax + distance :
            -rect.xMin + distance;

        var rHigh = raycast.Horizontal(tooHigh, this.facingRight, maxDistance);
        var rLow  = raycast.Horizontal(low, this.facingRight, maxDistance);
        
        if (this.facingRight)
        {
            if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
            if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;
        }
        else
        {
            if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
            if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;
        }

        return rLow.anything == true && rHigh.anything == false;
    }

    private bool IsGapSmallEnough()
    {
        return true;
    }

    private void ChangeHitbox(bool insideTank)
    {
        if(insideTank == true)
        {
            this.hitboxes = new [] {
                new Hitbox(-1f, 1f, -1.62f, 0.35f, this.rotation, Hitbox.NonSolid)
            };
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-1f, 1f, -1.62f, 2f, this.rotation, Hitbox.NonSolid)
            };
        }
    }

    private bool IsAirborne()
    {
        return this.onGround == false;
    }
}
