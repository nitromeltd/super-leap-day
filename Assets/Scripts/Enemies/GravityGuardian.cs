using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityGuardian : Enemy
{
    public Animated.Animation animationPatrol;
    public Animated.Animation animationAlert;
    public Animated.Animation animationCharge;
    public Animated.Animation animationBurst;
    public Animated.Animation animationAttack;
    public Animated.Animation animationTurn;
    public Animated.Animation animationFinishBurst;

    [Header("Attack Sparks")]
    public Animated attackSparksAnimated;
    public Animated.Animation animationAttackSparksOff;
    public Animated.Animation animationAttackSparksOn;

    [Header("Attack Sphere")]
    public Animated attackSphereAnimated;
    public Animated.Animation animationAttackSphereOff;
    public Animated.Animation animationAttackSphereOn;

    [Header("Particles")]
    public Animated.Animation animationExclamation;
    public Animated.Animation animationJetpack;

    private State state = State.None;
    private Vector2 velocity;
    private bool facingRight;
    private int timer;
    private bool isTurning;
    private Vector2 initialShakePosition;
    private Vector2 surfaceVelocity;
    private bool bouncedOffWall;

    private const float GuardianGlobalScale = 0.8f*0.8f;
    private const float PatrolMoveSpeed = 0.045f;
    private const int ChargeBurstTime = 22 * 5;
    private const int ChargeAttackTime = 22;
    private const int ShockAttackTime = 90;
    private const int BurstTime = 90;
    private const float MaxPlayerDistanceToChase = 10f;
    private const float MaxPlayerDistanceToAttack = 5f;
    private const float BurstSpeed = 0.25f;
    private const float BurstDecay = 0.001f;
    private const float BumpWallFriction = 0.90f;
    private const float ShockAttackRange = 3.50f;
    private const float ShakeSpeed = 1.50f;
    private const float StartShakeAmount = 0.02f;
    private const float EndShakeAmount = 0.15f;
    private const float CrushDistance = 1.25f;

    private static Vector2 HitboxCenter = new Vector2(0f, -0.50f * GuardianGlobalScale);
    private static Vector2 HitboxSize = new Vector2(1.00f * GuardianGlobalScale, 1.25f * GuardianGlobalScale);
    private static Vector2 HitboxMin = new Vector2(
        HitboxCenter.x - HitboxSize.x, HitboxCenter.y - HitboxSize.y);
    private static Vector2 HitboxMax = new Vector2(
        HitboxCenter.x + HitboxSize.x, HitboxCenter.y + HitboxSize.y);

    public enum State
    {
        None,
        Patrol,
        DetectPlayer,
        ChargeBurst,
        ExecuteBurst,
        FinishBurst,
        ChargeAttack,
        ExecuteAttack,
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.facingRight = !def.tile.flip;

        this.hitboxes = new[] {
            new Hitbox(
                HitboxCenter.x - HitboxSize.x,
                HitboxCenter.x + HitboxSize.x,
                HitboxCenter.y - HitboxSize.y,
                HitboxCenter.y + HitboxSize.y,
                this.rotation, Hitbox.NonSolid
        )};

        ChangeState(State.Patrol);
        bouncedOffWall = false;
    }

    public override void Advance()
    {
        AdvanceStates();
        EnemyHitDetectionAgainstPlayer();

        this.position += this.velocity;
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? GuardianGlobalScale : -GuardianGlobalScale, GuardianGlobalScale, 1);
    }

    private void AdvanceStates()
    {
        switch (this.state)
        {
            case State.Patrol:
                AdvancePatrolState();
                break;

            case State.DetectPlayer:
                AdvanceDetectPlayerState();
                break;

            case State.ChargeBurst:
                AdvanceChargeBurstState();
                break;

            case State.ExecuteBurst:
                AdvanceExecuteBurstState();
                break;

            case State.FinishBurst:
                AdvanceFinishBurstState();
                break;

            case State.ChargeAttack:
                AdvanceChargeAttackState();
                break;

            case State.ExecuteAttack:
                AdvanceExecuteAttackState();
                break;
        }
    }

    private void LookAtPlayer()
    {
        Vector2 playerPosition = this.map.player.position;
        this.facingRight = playerPosition.x > this.position.x;
        this.transform.localScale = new Vector3(this.facingRight ? GuardianGlobalScale : -GuardianGlobalScale, GuardianGlobalScale, 1);
    }

    private void LookAtDirection()
    {
        if (this.velocity.x > 0f)
        {
            this.facingRight = true;
        }
        else if (this.velocity.x < 0f)
        {
            this.facingRight = false;
        }
    }

    private void AdvancePatrolState()
    {
        UpdatePosition();

        // if (!this.isTurning)
        // {
        //     CreateDustTrail(6);
        // }        

        if (IsPlayerCloseEnough(MaxPlayerDistanceToChase))
        {
            ChangeState(State.DetectPlayer);
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyGravityGuardian,
                position: this.position
            );
        }
    }

    private void UpdatePosition(bool allowTurn = true)
    {
        void Turn()
        {
            this.isTurning = true;

            this.animated.PlayOnce(this.animationTurn, () =>
            {
                this.isTurning = false;
                this.animated.PlayAndLoop(this.animationPatrol);
            });
        }

        Vector2 frameVelocity = this.velocity;
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);
        var maxDistance =  Mathf.Abs(this.velocity.x) + HitboxMax.x + 0.01f;
        var maxVerticalDistance = HitboxMax.y;

        var right = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, HitboxMax.y), true, maxDistance),
                raycast.Horizontal(this.position, true, maxDistance),
                raycast.Horizontal(this.position.Add(0, HitboxMin.y), true, maxDistance)
        );
        var left = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, HitboxMax.y), false, maxDistance),
                raycast.Horizontal(this.position, false, maxDistance),
                raycast.Horizontal(this.position.Add(0, HitboxMin.y), false, maxDistance)
        );

        float probingOffset = HitboxMax.x - 0.5f;
        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-probingOffset, 0), true, HitboxMax.y),
            raycast.Vertical(this.position, true, HitboxMax.y),
            raycast.Vertical(this.position.Add(probingOffset, 0), true, HitboxMax.y)
             
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-probingOffset, 0), false, Mathf.Abs(HitboxMin.y)),
            raycast.Vertical(this.position, false, Mathf.Abs(HitboxMin.y)),
            raycast.Vertical(this.position.Add(probingOffset, 0), false, Mathf.Abs(HitboxMin.y))
        );

        if(up.anything == true)
        {            
            if(HitboxMax.y > up.distance)
            {                
                this.position.y -= HitboxMax.y - up.distance + 0.01f;
                right = Raycast.CombineClosest(
                    raycast.Horizontal(this.position.Add(0, HitboxMax.y), true, maxDistance),
                    raycast.Horizontal(this.position, true, maxDistance),
                    raycast.Horizontal(this.position.Add(0, HitboxMin.y), true, maxDistance)
                );
                left = Raycast.CombineClosest(
                    raycast.Horizontal(this.position.Add(0, HitboxMax.y), false, maxDistance),
                    raycast.Horizontal(this.position, false, maxDistance),
                    raycast.Horizontal(this.position.Add(0, HitboxMin.y), false, maxDistance)
                );
            }
        }

        if(down.anything == true)
        {
            if(Mathf.Abs(HitboxMin.y) > down.distance)
            {                
                this.position.y += Mathf.Abs(HitboxMin.y) - down.distance + 0.01f;
                right = Raycast.CombineClosest(
                    raycast.Horizontal(this.position.Add(0, HitboxMax.y), true, maxDistance),
                    raycast.Horizontal(this.position, true, maxDistance),
                    raycast.Horizontal(this.position.Add(0, HitboxMin.y), true, maxDistance)
                );
                left = Raycast.CombineClosest(
                    raycast.Horizontal(this.position.Add(0, HitboxMax.y), false, maxDistance),
                    raycast.Horizontal(this.position, false, maxDistance),
                    raycast.Horizontal(this.position.Add(0, HitboxMin.y), false, maxDistance)
                );
            }
        }

        if (this.velocity.x > 0)
        {
            if(left.anything == true)
            {
                if(HitboxMax.x > left.distance)
                {
                    this.position.x += HitboxMax.x - left.distance + 0.02f;
                }
            }

            if(right.anything == true)
            {
                this.facingRight = false;
                if (!this.isTurning && allowTurn)
                {
                    this.isTurning = true;
                    Turn();
                }
                this.velocity.x = -PatrolMoveSpeed; 
                
                if(right.tiles.Length > 0 && right.tiles[0] != null)
                {
                    this.surfaceVelocity = right.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else if(this.velocity.x < 0)
        {
            if(right.anything == true)
            {
                bool validAngle = right.surfaceAngleDegrees > 45 && right.surfaceAngleDegrees < 135;
                if(HitboxMax.x > right.distance && validAngle)
                {
                    this.position.x -= HitboxMax.x - right.distance + 0.02f;
                }
            }

            if(left.anything == true)
            {                
                this.facingRight = true;
                if (!this.isTurning && allowTurn)
                {
                    this.isTurning = true;
                    Turn();
                }
                this.velocity.x = PatrolMoveSpeed;
                
                if(left.tiles.Length > 0 && left.tiles[0] != null)
                {
                    surfaceVelocity = left.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else if(this.velocity.x == 0)
        {
            if(left.anything == true)
            {
                if(HitboxMax.x > left.distance)
                {
                    this.position.x += HitboxMax.x - left.distance + 0.02f;
                }
            }

            if(right.anything == true)
            {
                bool validAngle = right.surfaceAngleDegrees > 45 && right.surfaceAngleDegrees < 135;
                if(HitboxMax.x > right.distance && validAngle)
                {
                    this.position.x -= HitboxMax.x - right.distance + 0.02f;
                }
            }
        }
        HandleCrush(left, right, up, down);        
    }

    private void HandleCrush(
        Raycast.CombinedResults left,
        Raycast.CombinedResults right,
        Raycast.CombinedResults up,
        Raycast.CombinedResults down)
    {
        if (up.anything &&
            down.anything &&
            up.distance + down.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }

        if (left.anything &&
            right.anything &&
            left.distance + right.distance < CrushDistance)
        {
            Kill(new KillInfo{ itemDeliveringKill = this });
            return;
        }
    }
 
    private bool IsPlayerCloseEnough(float distance)
    {
        Player player = this.map.player;

        if (player.alive == false)
        {
            return false;
        }

        var isPlayerInSameChunk =
            this.map.NearestChunkTo(player.position) == this.chunk;

        if (isPlayerInSameChunk == false)
        {
            return false;
        }

        float distanceToPlayer = Vector2.Distance(this.position, player.position);

        if (distanceToPlayer >= distance)
        {
            return false;
        }

        return true;
    }

    private void AdvanceDetectPlayerState()
    {
        UpdatePosition(false);
        LookAtPlayer();
    }

    private void AdvanceChargeBurstState()
    {
        var lastPosition = this.position;
        UpdatePosition(false);
        if(this.position != lastPosition)
        {
            this.initialShakePosition = this.position;
        }

        float t = Mathf.InverseLerp(ChargeBurstTime, 0, this.timer);
        float currentShakeAmount = Mathf.Lerp(StartShakeAmount, EndShakeAmount, t);

        float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * currentShakeAmount;
        this.position.x = this.initialShakePosition.x + shake;
        this.position.y = this.initialShakePosition.y + shake;

        if (this.timer > 0)
        {
            this.timer -= 1;

            if (this.timer <= 0)
            {
                ChangeState(State.ExecuteBurst);
            }
        }
    }

    private void AdvanceExecuteBurstState()
    {
        LookAtDirection();
        // CreateDustTrail(1);

        this.velocity.x = Util.Slide(this.velocity.x, 0f, BurstDecay);
        this.velocity.y = Util.Slide(this.velocity.y, 0f, BurstDecay);

        CheckCollideWithWall();

        if(this.bouncedOffWall)
        {
            this.velocity = 0.95f * this.velocity;
        }

        if (this.velocity.sqrMagnitude < 0.005f)
        {
            ChangeState(State.FinishBurst);
            this.bouncedOffWall = false;
        }
    }

    private void CheckCollideWithWall()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var rect = this.hitboxes[0];

        // HORIZONTAL
        if (this.velocity.x > 0f)
        {
            var maxDistance = this.velocity.x + rect.xMax;

            var right = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, HitboxMin.x), true, maxDistance),
                raycast.Horizontal(this.position, true, maxDistance),
                raycast.Horizontal(this.position.Add(0, HitboxMin.y), true, maxDistance)
            );

            if (right.anything == true)
            {
                CreateDustDrop(new Vector2(HitboxSize.x, 0f), false);

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyGravityGuardianBounce,
                    position: this.position
                );

                this.velocity.x = -this.velocity.x * BumpWallFriction;
                this.bouncedOffWall = true;
            }
        }
        else if (this.velocity.x < 0f)
        {
            var maxDistance = -this.velocity.x - rect.xMin;

            var left = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, HitboxMin.x), false, maxDistance),
                raycast.Horizontal(this.position, false, maxDistance),
                raycast.Horizontal(this.position.Add(0, HitboxMax.x), false, maxDistance)
            );

            if (left.anything == true)
            {
                CreateDustDrop(new Vector2(-HitboxSize.x, 0f), false);

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyGravityGuardianBounce,
                    position: this.position
                );

                this.velocity.x = -this.velocity.x * BumpWallFriction;
                this.bouncedOffWall = true;
            }
        }

        // VERTICAL
        if (this.velocity.y > 0f)
        {
            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;

            var up = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0f), true, maxDistance),
                raycast.Vertical(start + new Vector2(0f, 0f), true, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0f), true, maxDistance)
            );

            if (up.anything == true)
            {
                CreateDustDrop(new Vector2(0f, HitboxSize.y), true);

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyGravityGuardianBounce,
                    position: this.position
                );

                this.velocity.y = -this.velocity.y * BumpWallFriction;
                this.bouncedOffWall = true;
            }
        }
        else if (this.velocity.y < 0f)
        {
            var tolerance = 0.1f;
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin + tolerance;

            var down = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(0f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0f), false, maxDistance)
            );

            if (down.anything == true)
            {
                CreateDustDrop(new Vector2(0f, -HitboxSize.y), true);

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyGravityGuardianBounce,
                    position: this.position
                );

                this.velocity.y = -this.velocity.y * BumpWallFriction;
                this.bouncedOffWall = true;
            }
        }
    }

    private void AdvanceFinishBurstState()
    {
        UpdatePosition(false);
    }

    private void AdvanceChargeAttackState()
    {
        if (this.timer > 0)
        {
            this.timer -= 1;

            if (this.timer <= 0)
            {
                ChangeState(State.ExecuteAttack);
            }
        }
    }

    private void AdvanceExecuteAttackState()
    {
        UpdatePosition(false);

        if (IsPlayerCloseEnough(ShockAttackRange))
        {
            this.map.player.Hit(this.position);
        }

        if (this.timer > 0)
        {
            this.timer -= 1;

            if (this.timer <= 0)
            {

                if (IsPlayerCloseEnough(MaxPlayerDistanceToAttack))
                {
                    ChangeState(State.ChargeAttack);
                }
                else if (IsPlayerCloseEnough(MaxPlayerDistanceToChase))
                {
                    ChangeState(State.DetectPlayer);
                }
                else
                {
                    ChangeState(State.Patrol);
                }
            }
        }
    }

    private void ChangeState(State newState)
    {
        switch (this.state)
        {
            case State.ChargeBurst:
                ExitChargeBurstState();
                break;

            case State.ExecuteAttack:
                ExitExecuteAtackState();
                break;

            case State.Patrol:
                ExitPatrolState();
                break;
        }

        this.state = newState;

        switch (this.state)
        {
            case State.Patrol:
                EnterPatrolState();
                break;

            case State.DetectPlayer:
                EnterDetectPlayerState();
                break;

            case State.ChargeBurst:
                EnterChargeBurstState();
                break;

            case State.ExecuteBurst:
                EnterExecuteBurstState();
                break;

            case State.FinishBurst:
                EnterFinishBurstState();
                break;

            case State.ChargeAttack:
                EnterChargeAttackState();
                break;

            case State.ExecuteAttack:
                EnterExecuteAttackState();
                break;
        }
    }

    private void EnterPatrolState()
    {
        this.animated.PlayAndLoop(this.animationPatrol);
        this.timer = 0;
        this.velocity.x = PatrolMoveSpeed * (this.facingRight ? 1f : -1f);
        this.velocity.y = 0f;
    }

    private void ExitPatrolState()
    {
        this.isTurning = false;
    }

    private void EnterDetectPlayerState()
    {
        this.velocity = Vector2.zero;

        this.animated.PlayOnce(this.animationAlert, () =>
        {
            if (IsPlayerCloseEnough(MaxPlayerDistanceToAttack))
            {
                ChangeState(State.ChargeAttack);
            }
            else
            {
                ChangeState(State.ExecuteBurst);
            }
        });

        float directionValue = this.facingRight ? 1f : -1f;
        Particle p = Particle.CreateAndPlayOnce(
            this.animationExclamation,
            this.position.Add(0.20f * directionValue, 2.25f),
            this.transform
        );
        p.transform.localScale = new Vector3(directionValue, 1f, 1f);
    }

    private void EnterChargeBurstState()
    {
        this.animated.PlayAndLoop(this.animationCharge);

        this.initialShakePosition = this.position;
        this.timer = ChargeBurstTime;
    }

    private void ExitChargeBurstState()
    {
        this.position = this.initialShakePosition;
    }

    private void EnterExecuteBurstState()
    {
        LookAtPlayer();
        this.animated.PlayAndLoop(this.animationBurst);

        Vector2 playerPosition = this.map.player.position;

        Vector2 heading = playerPosition - this.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;

        this.velocity = direction * BurstSpeed;
        this.bouncedOffWall = false;
    }

    private void EnterFinishBurstState()
    {
        this.velocity = Vector2.zero;

        ChangeState(State.ExecuteAttack);
    }

    private void EnterChargeAttackState()
    {
        this.animated.PlayAndLoop(this.animationAttack);
        this.timer = ChargeAttackTime;
        this.velocity = Vector2.zero;
    }

    private void EnterExecuteAttackState()
    {
        this.timer = ShockAttackTime;
        this.animated.PlayAndLoop(this.animationAttack);
        this.attackSparksAnimated.PlayAndLoop(this.animationAttackSparksOn);
        this.attackSphereAnimated.PlayAndLoop(this.animationAttackSphereOn);
        this.velocity = Vector2.zero;
    }

    private void ExitExecuteAtackState()
    {
        this.attackSparksAnimated.PlayOnce(this.animationAttackSparksOff);
        this.attackSphereAnimated.PlayOnce(this.animationAttackSphereOff);
    }

    private void CreateDustTrail(float interval)
    {
        if (this.map.frameNumber % interval != 0) return;

        float x = this.position.x + (1.1f * (this.facingRight ? -1f : 1f));
        float y = this.position.y - 1.2f;

        int lifetime = UnityEngine.Random.Range(25, 35);

        Particle p = Particle.CreatePlayAndHoldLastFrame(
            this.map.player.animationDustParticle,
            lifetime,
            new Vector2(
                x + UnityEngine.Random.Range(-0.05f, 0.05f),
                y + UnityEngine.Random.Range(-0.05f, 0.05f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.65f, 0.8f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(0.18f, 0.22f) * (this.facingRight? 1 : -1),
            UnityEngine.Random.Range(-0.22f, -0.18f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void CreateDustDrop(Vector2 offset, bool horizontal)
    {
        float x = this.position.x + offset.x;
        float y = this.position.y + offset.y;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                originPosition,
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.65f, 0.65f, 1);

            float minVelocityX = 0.05f;
            float maxVelocityX = 0.10f;

            float accelerationValue = -0.02f;

            float localX =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(minVelocityX, maxVelocityX));
            float localY = UnityEngine.Random.Range(-0.05f, 0.05f);

            p.velocity.x = horizontal ? localX : localY;
            p.velocity.y = horizontal ? localY : localX;
            p.acceleration = p.velocity * accelerationValue;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
        }
    }

    public override void Hit(KillInfo killInfo)
    {
        float directionValue = this.facingRight ? 1f : -1f;
        Particle p = Particle.CreatePlayAndLoop(
            this.animationJetpack,
            60 * 6,
            this.position.Add(0.50f * -directionValue, -0.50f),
            this.transform.parent
        );
        p.transform.localScale = new Vector3(directionValue, 1f, 1f);
        p.Throw(new Vector2(0, 0.5f), 0.2f * -directionValue, 0.1f, new Vector2(0, -0.025f));
        p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
        if (UnityEngine.Random.Range(0, 2) == 0)
            p.spin *= -1;

        base.Hit(killInfo);
    }
}
