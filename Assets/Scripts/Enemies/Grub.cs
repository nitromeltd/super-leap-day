using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class Grub : WalkingEnemy
{

    public Animated.Animation animationCrawl;
    public Animated.Animation animationAlert;
    public Animated.Animation animationDig;
    public Animated.Animation animationGoingUp;
    public Animated.Animation animationFallingDown;
    public Animated.Animation animationTurn;
    public CameraCone cameraCone;

    private enum State
    {
        Crawl,
        Turn,
        Alert,
        Search,
        Dig,
        Hidden,
        GoingUp,
        FallingDown,
        
    }

    private const float HorizontalSpeed = 0.02f;
    private State state;
    private int alertDelay;
    private int playerSeenCooldown;
    private const int PlayerSeenDelay = 60*5;
    private bool alerted;
    private int lastExclamationMarkCreatedOnFrame;
    private bool deadly;
    private int digOutCounter;
    private const int DigOutDelay = 60;
    private int backToCrawlCounter;
    private const int BackToCrawlDelay = 60;
    private int crawlRescanCounter;    
    private const int CrawlRescanWhenAlertedDelay = 120;    
    private float visibleAngleDegrees = 0;

    private Vector2 respawnPoint;
    public override void Init(Def def)
    {
        base.Init(def);
        this.turnSensorForward = .3f;
        this.edgeTurnSensorForward = -.45f;
        this.onTurnAround = EnterTurn;
        this.facingRight = !def.tile.flip;
        this.velocity.x = HorizontalSpeed * (this.facingRight? 1: -1);
        this.turnAtEdges = true;
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, -1.0f, 1.0f, this.rotation, Hitbox.NonSolid)
        };
        this.alerted = false; 
        this.cameraCone.Init(this.map);
        this.cameraCone.fixedCone = true;
        this.deadly = true;
        this.cameraCone.SetCallbacks(ShouldCameraActivate, onPlayerSpotted:PlayerSpotted);
        EnterCrawl();        
    }

    public override void Advance()
    {
        // DebugDrawing.Draw(false, true);
        this.cameraCone.Advance();
        switch(this.state)
        {
            case State.Crawl:
                AdvanceCrawl();             
                break; 

            case State.Search:
                AdvanceSearch();             
                break; 

            case State.Hidden:
                AdvanceHidden();             
                break;             

            case State.GoingUp:
                AdvanceGoingUp();
                break;

            case State.FallingDown:
                AdvanceFallingDown();
                break;

            default:
                GenericAdvanceMove();
                break;
        }
 
        if(this.playerSeenCooldown > 0)
        {
            this.playerSeenCooldown -= 1;
        }

        if(!this.alerted)
        {
            if(this.map.desertAlertState.IsActive(this.position) && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay)
            {
                this.alerted = true;
                PlayerSpotted();
            }
            else if(this.map.desertAlertState.IsGloballyActive() && this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position))
            {
                this.alerted = true;
                PlayerSpotted();
            }
        }
        else
        {
            if(!this.map.desertAlertState.IsGloballyActive())
            {
                this.alerted = false;
            }
        }
        SetAngle();
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.P))
        {
            EnterAlert();
        }

        if(Input.GetKeyDown(KeyCode.O))
        {
            EnterGoingUp();
        }         
#endif
    }

    private void SetAngle()
    {        
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 3
        );
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    public void PlayerSpotted()
    {
        if(this.map.player.state == Player.State.InsideBox)
        {
            if (this.map.player.GetBox() != null)
                {
                    if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitBox();
                    }
                }

                if (this.map.player.GetHidingPlace() != null)
                {
                    if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                    {
                        return;
                    }
                    else
                    {
                        this.map.player.ExitHidingPlace();
                    }
                }
        }

        if(this.alerted)
        {
            //try to crawl towards player
            this.crawlRescanCounter = CrawlRescanWhenAlertedDelay;
        }

        if(this.playerSeenCooldown > 0)
        {
            return;
        }

        if (this.state == State.Crawl && !this.alerted)
        {
            EnterAlert();
        }

        this.alerted = true;
        this.map.desertAlertState.ActivateAlert(this.position);        
        this.playerSeenCooldown = PlayerSeenDelay;
    }

    private bool ShouldCameraActivate()
    {
        if((this.map.desertAlertState.IsActive(this.position) && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay) || this.alerted)
        {
            return true;
        }
        return false;
    }

    private void EnterTurn(TurnInfo turnInfo)
    {
        this.state = State.Turn;
        this.animated.PlayOnce(this.animationTurn, () => {
                EnterCrawl();
            }); 
    }

    private void EnterAlert()
    {
        this.backToCrawlCounter = BackToCrawlDelay;
        this.crawlRescanCounter = CrawlRescanWhenAlertedDelay;
        TurnCameraConeOnOff(false);
        this.velocity = Vector2.zero;
        this.state = State.Alert;
        this.animated.PlayOnce(this.animationAlert, ()=>
        {
            // EnterDig();
            EnterSearch();
        });
        this.animated.OnFrame(7, CreateExclamationMark);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyGrubAlert,
            position: this.position
        );
    }

    private void EnterSearch()
    {
        this.state = State.Search;
    }

    private void AdvanceSearch()
    {
        GenericAdvanceMove();
        if(this.map.frameNumber % 20 == 19)
        {
            if(CheckIfPlayerNearGround())
            {
                EnterDig();
            }
        }

        if(this.backToCrawlCounter > 0)
        {
            if(--this.backToCrawlCounter == 0)
            {
                EnterCrawl();
            }
        }
    }

    private void EnterDig()
    {
        this.state = State.Dig;
        this.animated.PlayOnce(this.animationDig, EnterHidden);
        for (int i = 9; i < 17; i++)
            this.animated.OnFrame(i, GenerateDiggingParticlesEachFrame);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyGrubDig,
            position: this.position
        );
    }

    private void GenerateDiggingParticlesEachFrame()
    {
        GenerateDiggingParticles(this.position + new Vector2(0,hitboxes[0].yMin), 1.25f);
    }

    private void EnterHidden()
    {
        this.deadly = false;
        this.digOutCounter = DigOutDelay;
        this.state = State.Hidden;
    }

    private void AdvanceHidden()
    {
        GenerateDiggingParticles(this.respawnPoint + new Vector2(0,hitboxes[0].yMin), 1.25f);
        if(this.digOutCounter > 0)
        {
            if(--this.digOutCounter == 0)
            {
                EnterGoingUp();
            }
        }
    }

    private void EnterCrawl()
    {
        this.deadly = true;
        this.velocity.x = HorizontalSpeed * (this.facingRight? 1: -1);
        TurnCameraConeOnOff(true);
        this.state = State.Crawl;
        this.animated.PlayAndLoop(this.animationCrawl);
        this.animated.OnFrame(11, GenerateCrawlParticles);
        this.animated.OnFrame(10, () =>
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyGrubWalk,
                position: this.position
            );
        });
    }

    private void AdvanceCrawl()
    {
        // DebugDrawing.Draw(true, true);
        GenericAdvanceMove();

        if(this.alerted)
        {
            if(this.crawlRescanCounter > 0)
            {
                this.crawlRescanCounter--;
            }

            if(this.crawlRescanCounter == 0)
            {
                EnterAlert();
            }
        }
    }

    private void GenericAdvanceMove()
    {
        this.velocity.y -= 0.5f / 16;
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        FollowPortals();
        if(this.deadly)
            EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    private void EnterGoingUp()
    {
        this.state = State.GoingUp;
        this.position = this.respawnPoint;
        this.transform.position = this.position;
        deadly = true;
        this.animated.PlayAndLoop(animationGoingUp);
        this.velocity = this.velocity + new Vector2(0, 0.3f);
        CreateDigUpExplosion();
        this.facingRight = this.map.player.position.x > this.position.x;
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyGrubPopout,
            position: this.position
        );
    }

    private void CreateDigUpExplosion()
    {
        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.respawnPoint + new Vector2(0,hitboxes[0].yMin),
            this.transform.parent
        );

        p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
    }

    private void AdvanceGoingUp()
    {
        GenericAdvanceMove();
        if(this.velocity.y  <= 0)
        {
            EnterFallingDown();
        }
    }

    private void EnterFallingDown()
    {
        this.state = State.FallingDown;
        this.animated.PlayOnce(animationFallingDown);
    }

    private void AdvanceFallingDown()
    {
        GenericAdvanceMove();
        if(this.velocity.y == 0)
        {
            EnterCrawl();
            // EnterAlert();
            this.backToCrawlCounter = BackToCrawlDelay;
        }
    }

    private void GenerateDiggingParticles(Vector2 position, float distance)
    {
        var up = new Vector2(0,1f);
        var forward = new Vector2(1f,0);
 
        for (var n = 0; n < 1; n += 1)
        {             
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                position +
                    new Vector2(Rnd.Range(-distance, distance), Random.Range(-0.2f,-0.0f)),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
             
            p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) , Rnd.Range(-0.00f, 0.02f));
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    private void GenerateCrawlParticles()
    {
        var up = new Vector2(0,1f);
        var forward = new Vector2(1f,0);
 
        for (var n = 0; n < 2; n += 1)
        {             
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                this.position +
                    new Vector2(Rnd.Range(-1.25f, 1.25f), Random.Range(-0.8f,-0.95f)),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
             
            p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) , Rnd.Range(-0.00f, 0.02f));
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    private void CreateExclamationMark()
    {
        if(this.map.frameNumber - this.lastExclamationMarkCreatedOnFrame < 1*60)
        {
            return;
        }

        this.lastExclamationMarkCreatedOnFrame = this.map.frameNumber;
        var exclamationMark = Particle.CreateAndPlayOnce(
                    Assets.instance.exclamationMarkAnimation,
                    this.position.Add(
                        this.facingRight? 0.5f:-0.5f,
                        3f
                    ),
                    this.transform.parent
                );
        exclamationMark.spriteRenderer.sortingLayerName = "Items";
        exclamationMark.spriteRenderer.sortingOrder = -1;
    }

    private void TurnCameraConeOnOff(bool shouldBeActive)
    {
        this.cameraCone.gameObject.SetActive(shouldBeActive);
    }

    private bool CheckIfPlayerNearGround()
    {
        float maxDistance = 2;
        var raycast = new Raycast(this.map, this.map.player.position, isCastByEnemy: true);
        var result = raycast.Vertical(this.map.player.position, false, maxDistance);

        if (result.anything == true && result.distance <= maxDistance && IsPlayerInSameChunkAsUs())
        {
            if(result.tile != null)
            {
                int x = result.tile.mapTx;
                int y = result.tile.mapTy;

                if(IsThereEnoughSpaceAroundTile(x,y))
                {
                    var rect = this.hitboxes[0];
                    respawnPoint = new Vector2(result.tile.mapTx + 0.5f, result.tile.mapTy + 1 + Mathf.Abs(rect.yMin));
                    return true;
                }
            }
        }
        return false;
    }

    private bool IsThereEnoughSpaceAroundTile(int x, int y)
    {
        if( this.map.TileAt(x-1,y, Layer.A) != null
            && this.map.TileAt(x,y, Layer.A) != null
            && this.map.TileAt(x+1,y, Layer.A) != null
            && this.map.TileAt(x-1,y-1, Layer.A) != null
            && this.map.TileAt(x,y-1, Layer.A) != null
            && this.map.TileAt(x+1,y-1, Layer.A) != null
        )
        return true;

        return false;
    }

    private bool IsPlayerInSameChunkAsUs()
    {
        return this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position);
    }
}