using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Profiling;
public class HauntedStatue : Enemy
{
    public enum ContactType
    {
        Deadly,
        NonDeadly
    };
    private ContactType whenCollidesWithPlayer = ContactType.NonDeadly;

    private int activityTimeInFrames = 0;
    private Vector2 velocity = Vector2.zero;
    private const float GravityForce = 0.02f;
    public Animated.Animation animationWakingUp;
    public Animated.Animation animationGoingToSleep;
    public Animated.Animation animationIdle;
    public Animated.Animation animationFlying;
    private bool floorAlreadyHitAfterTurningIntoStone = false;
    private const int MinimumActiveTimeInFrames = 79;
    public List<StatueTriggerBlock> statueTriggerBlockLeaders = new List<StatueTriggerBlock>();
    public override void Init(Def def)
    {
        base.Init(def);
        if(def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }
        SetHitbox(true);
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        SetNonDeadly();
        this.floorAlreadyHitAfterTurningIntoStone = false;        
        statueTriggerBlockLeaders.AddRange(this.chunk.items.OfType<StatueTriggerBlock>().Where(ItemIsStatueTriggerBlockLeader));
    }

    public override void Advance()
    {
        if (IsPlayerInSameChunk() && this.map.player.alive && this.activityTimeInFrames > 1)
        {
            chunk.pathfinding.Refresh();
            var pos = chunk.pathfinding.NextPointToPlayer(this.position);

            if (pos.HasValue)
            {
                Vector2 nPos = new Vector2(
                    Util.Slide(this.position.x, chunk.xMin + pos.Value.x, 0.1f),
                    Util.Slide(this.position.y, chunk.yMin + pos.Value.y, 0.1f)
                    );
                
                if(nPos.x - this.position.x > 0)
                {
                    this.transform.localScale = new Vector3(1, 1, 1);
                }
                else if (nPos.x - this.position.x < 0)
                {
                    this.transform.localScale = new Vector3(-1, 1, 1);
                }

                this.position = nPos;
                DebugPath();
            }
        }
        else if(this.activityTimeInFrames == 0)
        {
            var raycast = new Raycast(this.map, this.position, ignoreItem1: this, ignoreItemTypes:new System.Type[] {typeof(PlayerBlocker)});
            MoveVertically(raycast);
        }

        this.transform.position = this.position;
        if(this.activityTimeInFrames > 1)
        {
            this.activityTimeInFrames--;
            if(this.activityTimeInFrames == 1)
            {
                this.animated.PlayOnce(this.animationGoingToSleep, () =>
                {
                    this.floorAlreadyHitAfterTurningIntoStone = false;
                    this.activityTimeInFrames = 0;
                    SetNonDeadly();
                    this.animated.PlayOnce(this.animationIdle);
                });

                Audio.instance.PlaySfx(
                    Assets.instance.tombstoneHill.sfxGargoyleStone, null, this.position
                );
            }

            foreach(var l in statueTriggerBlockLeaders)
            {
                l.SetActivated((this.activityTimeInFrames % 20) >= 10);
            }
        }

        CheckIfPlayerHit();

        if(animated.currentAnimation == animationWakingUp && Time.frameCount % 8 == 1)
        {
            var p = Particle.CreateWithSprite(
                    Assets.instance.tombstoneHill.hauntedStatueStones[Random.Range(0,Assets.instance.tombstoneHill.hauntedStatueStones.Length)],
                    10,
                    this.position + new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 0.7f)),
                    this.transform.parent
                );
            p.Throw(new Vector2(0, 0), 0.0f, 0.0f, new Vector2(0, -0.01f));
        }
    }

    bool ItemIsStatueTriggerBlockLeader(Item item)
    {
        if(item is StatueTriggerBlock && (item as StatueTriggerBlock).GetLeader() == item)
        {
            return true;
        }

        return false;
    }


    private void ExplodeRocks()
    {
        int max = 4;
        for (int i = 0; i < max; i++)
        {
            var p = Particle.CreateWithSprite(
                        Assets.instance.tombstoneHill.hauntedStatueStones[Random.Range(0, Assets.instance.tombstoneHill.hauntedStatueStones.Length)],
                        20,
                        this.position + new Vector2(Random.Range(0.8f, 1.5f), Random.Range(0, 1f)),
                        this.transform.parent
                    );
            p.Throw(new Vector2(0.05f, 0.02f), 0.02f, 0.01f, new Vector2(0, -0.01f));

            var k = Particle.CreateWithSprite(
                        Assets.instance.tombstoneHill.hauntedStatueStones[Random.Range(0, Assets.instance.tombstoneHill.hauntedStatueStones.Length)],
                        20,
                        this.position + new Vector2(Random.Range(-1.5f, -0.8f), Random.Range(0, 1f)),
                        this.transform.parent
                    );
            k.Throw(new Vector2(-0.05f, 0.02f), 0.02f, 0.01f, new Vector2(0, -0.01f));

            CreateDust();
        }
    }

    private void CreateDust()
    {
        Particle.CreateDust(this.transform.parent, 1, 2,
            this.position, 1.2f, 1.2f, Vector2.zero, sortingLayerName: "Player Dust (AL)");
    }

    private void CheckIfPlayerHit()
    {
        if (this.whenCollidesWithPlayer == ContactType.NonDeadly)
            return;

        var ownRectangle = this.hitboxes[0].InWorldSpace();

        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return; 
 
        player.Hit((Vector2)this.transform.position);
    }

    public void TriggerStatue()
    {
        if(IsPlayerInSameChunk())
        {
            if (this.animated.currentAnimation == this.animationGoingToSleep)
            {
                this.activityTimeInFrames = MinimumActiveTimeInFrames;
                SetDeadly();
                this.animated.PlayAndLoop(this.animationFlying);
            }
            else if(this.activityTimeInFrames == 0)
            {
                if (this.animated.currentAnimation != this.animationWakingUp)
                {
                    this.animated.PlayOnce(this.animationWakingUp, () =>
                    {
                        this.activityTimeInFrames = MinimumActiveTimeInFrames;
                        SetDeadly();
                        this.animated.PlayAndLoop(this.animationFlying);
                    });

                    Audio.instance.PlaySfx(
                        Assets.instance.tombstoneHill.sfxGargoyleWake, null, this.position
                    );

                    this.animated.OnFrame(15, () => { ExplodeRocks(); });
                }
            }
            else
            {
                this.activityTimeInFrames = MinimumActiveTimeInFrames;
            }
        }
    }

    private void SetDeadly()
    {
        if (this.whenCollidesWithPlayer != ContactType.Deadly)
        {
            this.whenCollidesWithPlayer = ContactType.Deadly;
            SetHitbox(solid:false);
        }
    }

    private void SetNonDeadly()
    {
        if (this.whenCollidesWithPlayer != ContactType.NonDeadly)
        {
            this.whenCollidesWithPlayer = ContactType.NonDeadly;
            SetHitbox(solid:true);
        }
    }

    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        this.velocity.y -= 0.02f;

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var tolerance = 0.0f;
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin + tolerance;

            var otherStatue = OtherStatueIntersectingAbove();
            if (otherStatue != null)
            {
                raycast = new Raycast(
                    this.map, this.position,
                    ignoreItem1: this,
                    ignoreItem2: otherStatue,
                    isCastByEnemy: true
                );
            }

            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin + tolerance)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                if (result.tiles.Length > 0)
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                    {
                        surfaceVelocity.x = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed;
                        surfaceVelocity.y = 0;
                    }
                }

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;

                if(this.floorAlreadyHitAfterTurningIntoStone == false)
                {
                    this.floorAlreadyHitAfterTurningIntoStone = true;
                    for (int i = 0; i < 3; i++)
                        Particle.CreateDust(this.transform.parent, 1, 2,
                            this.position + new Vector2(0,-1.5f), 1.2f, 0f, Vector2.zero, sortingLayerName: "Player Dust (AL)");

                    Audio.instance.PlaySfx(
                        Assets.instance.tombstoneHill.sfxGargoyleLand, null, this.position
                    );
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private HauntedStatue OtherStatueIntersectingAbove()
    {
        foreach (var other in this.chunk.items)
        {
            var otherStatue = other as HauntedStatue;
            if (otherStatue == null) continue;
            if (otherStatue == this) continue;
            if (otherStatue.position.y < this.position.y) continue;
            if (otherStatue.hitboxes[0].InWorldSpace().Overlaps(
                this.hitboxes[0].InWorldSpace()
            ) == false) continue;

            return otherStatue;
        }

        return null;
    }

    private void SetHitbox(bool solid)
    {
        if(solid == true)
        {
            this.hitboxes = new[] {
            new Hitbox(
                -1, 1, -1.5f, 1.4f,
                this.rotation, Hitbox.Solid
            )};
        }
        else
        {
            this.hitboxes = new[] {
            new Hitbox(
                -1, 1, -1.5f, 1.4f,
                this.rotation, Hitbox.NonSolid
            )};
        }
    }

    private bool IsPlayerInSameChunk()
    {
        return this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position);
    }

    private void DebugPath()
    {
#if UNITY_EDITOR        
        var tPos = this.position;
        for (int i = 0; i < 60; i++)
        {
            var nextPos = chunk.pathfinding.NextPointToPlayer(tPos);
            if (nextPos.HasValue)
            {
                var newPosition = new Vector2(chunk.xMin, chunk.yMin) + nextPos.Value;
                Debug.DrawLine(tPos, newPosition, Color.green, 0);
                tPos = newPosition;
            }
        }
#endif            
    }
}
