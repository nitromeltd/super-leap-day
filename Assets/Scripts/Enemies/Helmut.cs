
using UnityEngine;
using System.Linq;

public class Helmut : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationStab;
    public Animated.Animation animationFall;
    public Animated.Animation animationRise;

    private int stabCooldownTime;
    private bool stabbing;
    private bool onGroundLast = false;

    private const float DefaultXVelocity = 0.0625f;
    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(
                -10 / 16f, 10 / 16f, -13 / 16f, 13 / 16f, this.rotation, Hitbox.NonSolid
            )
        };

        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = DefaultXVelocity * initialDirectionValue;
        this.animated.PlayAndLoop(this.animationWalk);

        this.onTurnAround = delegate
        {
            this.animated.PlayOnce(this.animationTurn, delegate {
                this.animated.PlayAndLoop(this.animationWalk);
            });
        };
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true)
            return;

        if (this.stabCooldownTime > 0)
            this.stabCooldownTime -= 1;

        this.velocity.y -= 0.5f / 16f;

        if (this.stabbing == true)
        {
            var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
            Move(raycast, stopHorizontally: true);
            EnemyHitDetectionAgainstPlayer();

            this.transform.position = this.position;
        }
        else
        {
            base.Advance();

            var playerRelative = this.map.player.position - this.position;
            var playerForward = playerRelative.x * Mathf.Sign(this.velocity.x);

            if (this.stabCooldownTime == 0 &&
                this.map.player.alive == true &&
                this.map.player.state != Player.State.Respawning &&
                Mathf.Abs(playerRelative.y) < 16 / 16f &&
                playerForward > 0 &&
                playerForward < 140 / 16f &&
                this.onGround == true)
            {
                this.stabbing = true;
                this.velocity.x = this.facingRight ? DefaultXVelocity : -DefaultXVelocity;
                this.animated.PlayOnce(this.animationStab, delegate {
                    this.stabbing = false;
                    this.animated.PlayAndLoop(this.animationWalk);
                    this.stabCooldownTime = 50;
                });
                this.animated.OnFrame(5, delegate {
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnemyHelmutAttack,
                        position: this.position
                    );
                });
                this.animated.OnFrame(9, delegate {
                    Stab();
                });
            }
        }
        
        CheckRiseAndFall();
        SwitchToNearestChunk();
        this.onGroundLast = this.onGround;
    }

    private void Stab()
    {
        var r = this.hitboxes[0].InWorldSpace();
        if (this.facingRight == true)
            r.xMax += 32 / 16f;
        else
            r.xMin -= 32 / 16f;
        EnemyHitDetectionAgainstPlayer(r);
    }

    public override bool Kill(KillInfo info)
    {
        if (info.isBecauseLevelIsResetting == false)
        {
            var helmet = HelmutHelmet.Create(this.position, this.chunk);
            helmet.velocity.x = this.facingRight ? 0.25f : -0.25f;
            
            foreach(var r in this.chunk.items.OfType<EnemyRespawner>())
                r.AddSpawnedEnemy((Enemy)this, (Enemy)helmet);
        }

        return base.Kill(info);
    }

    private void CheckRiseAndFall()
    {
        if (this.deathAnimationPlaying == true) return;
        if (this.stabbing == true) return;

        if(this.onGround == false && this.onGroundLast == true)
        {
            Animated.Animation animationAirborne = this.velocity.y > 0f ?
                this.animationRise : this.animationFall;

            this.animated.PlayAndLoop(animationAirborne);
        }
        
        if(this.onGround == true && this.onGroundLast == false)
        {
            this.animated.PlayAndLoop(animationWalk);
        }
    }
}
