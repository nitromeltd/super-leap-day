
using UnityEngine;
using System.Linq;

public class HelmutHelmet : WalkingEnemy
{
    public Animated.Animation animationSpin;
    public int disableAttackTime = 0;
    public int iframeTime = 15;

    public static HelmutHelmet Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.rainyRuins.helmutHelmet, chunk.transform);
        obj.name = "Spawned Helmut Helmet";
        var item = obj.GetComponent<HelmutHelmet>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.animated.PlayAndLoop(this.animationSpin);

        this.hitboxes = new [] {
            new Hitbox(
                -0.75f, 0.75f, -10/16f, 10/16f, this.rotation, Hitbox.NonSolid
            )
        };
        this.velocity = new Vector2(0.25f, 0);

        this.onTurnAround = OnTurnAround;
        this.whenHitFromAbove = HitFromAboveBehaviour.Nothing;
        this.turnAtEdges = false;
        this.stopIfTrappedHorizontally = false;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        if (turnInfo.raycastResults.HasValue == true)
        {
            Rect thisRect = this.hitboxes[0].InWorldSpace();
            Rect hitRect =
                this.facingRight == true ?
                new Rect(thisRect.xMin - 1, thisRect.yMin, 1, thisRect.height) :
                new Rect(thisRect.xMax, thisRect.yMin, 1, thisRect.height);

            this.map.Damage(hitRect, new KillInfo { itemDeliveringKill = this });

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyHelmutSkullCollide,
                position: this.position
            );
        }
    }

    public override void Advance()
    {
        if (this.iframeTime > 0)
        {
            this.iframeTime -= 1;
            if (this.iframeTime < 1)
                this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        }

        this.velocity.y -= 0.5f / 16;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer();
        SwitchToNearestChunk();

        this.transform.position = this.position;

        if (this.velocity.x > 0) this.transform.localScale = new Vector3(+1, 1, 1);
        if (this.velocity.x < 0) this.transform.localScale = new Vector3(-1, 1, 1);

        Particle.CreateDust(
            this.transform.parent,
            1, 1,
            this.position.Add(0, -10 / 16f),
            15 / 16f, 3 / 16f
        );

        if (this.disableAttackTime > 0)
            this.disableAttackTime -= 1;
        else
            this.map.Damage(
                this.hitboxes[0].InWorldSpace(),
                new KillInfo { itemDeliveringKill = this }
            );
    }

    public override bool Kill(KillInfo info)
    {
        if (this.iframeTime > 0)
            return false;

        Audio.instance.PlaySfx(Assets.instance.sfxHelmetDie, position: this.position);
        return base.Kill(info);
    }
}
