﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hippo : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationDetect;
    public Animated.Animation animationJumpUp;
    public Animated.Animation animationJumpDown;
    public Animated.Animation animationLandFromJump;
    public Animated.Animation animationLandFromDrop;
    public Animated.Animation animationPrepareHop;
    public Animated.Animation animationInsideGeyser;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;
    public Animated.Animation animationDrop;
    public Animated.Animation animationSlide;
    public SpriteRenderer handSprite;
    public AnimationCurve scaleOutCurve;
    public AnimationCurve iceAccelarationCurve;

    private State previousState = State.Walk;
    private State state = State.Walk;
    private Vector2 acceleration;
    private bool isTurning;
    private int jumpUpTimer;
    private Geyser insideGeyser;
    private int escapingTimer = 0;
    private Vector3 targetScale;
    private Vector3 scaleVelocity;
    private float visibleAngleDegrees = 0;
    private bool isDeadly = true;
    private int detectPlayerCooldownTimer = 0;
    private int prepareJumpDownTimer = 0;
    private const int PrepareJumpDownTime = 20;
    private const float WalkSpeed = 0.063f;
    private const float Gravity = 0.031f;
    private const float MaxPlayerVerticalDistance = 4f;
    private const float MaxPlayerHorizontalDistance = 7f;
    private const int JumpUpTotalTime = 15;
    private const int DetectPlayerCooldownTime = 60;

    public enum State
    {
        Walk,
        Detect,
        JumpUp,
        PrepareJumpDown,
        JumpDown,
        Land,
        PrepareHop,
        Hop,
        DetectGeyser,
        InsideGeyser,
        EscapeFromGeyser,
        Drop,
        Slide
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
           new Hitbox(-0.75f, 0.75f, -1.25f, 1.25f, this.rotation, Hitbox.NonSolid)
        };

        this.onTurnAround = OnTurnAround;
        this.turnAtEdges = false;
        
        ChangeState(State.Walk);
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        this.isTurning = true;

        this.animated.PlayOnce(this.animationTurn, () =>
        {
            this.isTurning = false;

            switch(this.state)
            {
                case State.Walk:
                default:
                    this.animated.PlayAndLoop(this.animationWalk);
                    break;

                case State.Slide:
                    this.animated.PlayAndLoop(this.animationSlide);
                    break;
            }
        });
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true) return;
        
        this.velocity += this.acceleration;

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.Detect:
                AdvanceDetect();
                break;

            case State.JumpUp:
                AdvanceJumpUp();
                break;

            case State.PrepareJumpDown:
                AdvancePrepareJumpDown();
                break;

            case State.JumpDown:
                AdvanceJumpDown();
                break;

            case State.Land:
                AdvanceLand();
                break;

            case State.PrepareHop:
                AdvancePrepareHop();
                break;

            case State.Hop:
                AdvanceHop();
                break;

            case State.DetectGeyser:
                AdvanceDetectGeyser();
                break;

            case State.InsideGeyser:
                AdvanceInsideGeyser();
                break;

            case State.EscapeFromGeyser:
                AdvanceEscapeFromGeyser();
                break;

            case State.Drop:
                AdvanceDrop();
                break;

            case State.Slide:
                AdvanceSlide();
                break;
        }

        FollowPortals();

        if(this.isDeadly == true)
        {
            EnemyHitDetectionAgainstPlayer();
        }
        
        this.transform.position = this.position;

        Vector3 absLocalScale = new Vector3(
            Mathf.Abs(this.transform.localScale.x),
            this.transform.localScale.y,
            this.transform.localScale.z
        );

        Vector3 currentScale = Vector3.SmoothDamp(
            absLocalScale,
            Vector3.one,
            ref scaleVelocity,
            0.05f
        );

        ChangeScale(currentScale);
    }

    private void ChangeScale(Vector3 newScale)
    {
        float xScale = Mathf.Abs(newScale.x);

        this.scaleVelocity = Vector3.zero;

        this.transform.localScale = new Vector3(
            this.facingRight ? xScale : -xScale,
            newScale.y,
            1f
        );
    }

    public void ChangeState(State newState)
    {
        this.previousState = this.state;

        switch(this.previousState)
        {
            case State.JumpUp:
                ExitJumpUp();
                break;

            case State.JumpDown:
                ExitJumpDown();
                break;

            case State.Land:
                ExitLand();
                break;

            case State.Hop:
                ExitHop();
                break;

            case State.InsideGeyser:
                ExitInsideGeyser();
                break;

            case State.EscapeFromGeyser:
                ExitEscapeFromGeyser();
                break;

            case State.Slide:
                ExitSlide();
                break;
        }

        this.state = newState;

        switch(newState)
        {
            case State.Walk:
                EnterWalk();
                break;

            case State.Detect:
                EnterDetect();
                break;

            case State.JumpUp:
                EnterJumpUp();
                break;

            case State.PrepareJumpDown:
                EnterPrepareJumpDown();
                break;

            case State.JumpDown:
                EnterJumpDown();
                break;

            case State.Land:
                EnterLand();
                break;

            case State.PrepareHop:
                EnterPrepareHop();
                break;

            case State.Hop:
                EnterHop();
                break;

            case State.DetectGeyser:
                EnterDetectGeyser();
                break;

            case State.InsideGeyser:
                EnterInsideGeyser();
                break;

            case State.EscapeFromGeyser:
                EnterEscapeFromGeyser();
                break;

            case State.Drop:
                EnterDrop();
                break;

            case State.Slide:
                EnterSlide();
                break;
        }
    }

    private void AdvanceWalk()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        
        if (this.map.frameNumber % 4 == 0)
        {
            CreateDustTrail();
        }

        if(this.detectPlayerCooldownTimer > 0)
        {
            this.detectPlayerCooldownTimer -= 1;
        }

        if(this.isTurning == true) return;

        if(this.onGround == false)
        {
            ChangeState(State.Drop);
        }
        else if (IsDetectingPlayer())
        {
            ChangeState(State.Detect);
        }
        else if(IsDetectingGeyser(raycast))
        {
            ChangeState(State.DetectGeyser);
        }
        else if(CanJumpWall(raycast))
        {
            ChangeState(State.PrepareHop);
        }
    }

    private void AdvanceDetect()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
    }

    private void EnterPrepareJumpDown()
    {
        prepareJumpDownTimer = PrepareJumpDownTime;
        ChangeScale(new Vector3(0.6f, 1.4f));
    }

    private void AdvancePrepareJumpDown()
    {
        if(prepareJumpDownTimer > 0)
        {
            prepareJumpDownTimer--;

            if(prepareJumpDownTimer == 0)
            {
                ChangeState(State.JumpDown);                
            }
        }
    }

    private void AdvanceJumpUp()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        if(this.jumpUpTimer > 0)
        {
            this.jumpUpTimer -= 1;

            if(this.jumpUpTimer <= 0)
            {
                ChangeState(State.PrepareJumpDown);
            }
        }
        
        if (this.jumpUpTimer > 8)
        {
            CreateDustTrail();
        }
    }

    private void AdvanceJumpDown()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        Rect currentRect = this.hitboxes[0].InWorldSpace();
        var tolerance = 0.1f;
        var maxDistance = Mathf.Abs(this.velocity.y) * 4f + tolerance;

        currentRect = currentRect.Inflate(new Vector2(0f, maxDistance));
        this.map.Damage(currentRect, new KillInfo { itemDeliveringKill = this });

        this.acceleration.y = Util.Slide(this.acceleration.y, -0.20f, 0.0025f);

        if(this.onGround == true)
        {
            ChangeState(State.Land);
        }
    }

    private void AdvanceLand()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
    }

    private void AdvancePrepareHop()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
    }

    private void AdvanceHop()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        AnimationAirborne();

        if(this.onGround == true)
        {
            ChangeState(State.Walk);
        }
    }

    private void AdvanceDetectGeyser()
    {
        Vector2 targetGeyserPos = this.insideGeyser.position.Add(0f, 0.7f);

        AnimationAirborne();

        float distanceToGeyser = Vector2.Distance(targetGeyserPos, this.position);
        bool isCloseToGeyser = distanceToGeyser < 0.50f;
        bool isInsideGeyser = distanceToGeyser < 0.05f;

        if(isCloseToGeyser == true)
        {
            this.position.x = Util.Slide(this.position.x, targetGeyserPos.x, 0.15f);
            this.position.y = Util.Slide(this.position.y, targetGeyserPos.y, 0.15f);
        }
        else
        {
            this.velocity.y -= Gravity;
            this.position += this.velocity;

            if(this.velocity.x > 0f && this.position.x > this.insideGeyser.position.x ||
                this.velocity.x < 0f && this.position.x < this.insideGeyser.position.x)
            {
                this.position.x = this.insideGeyser.position.x;
            }

            if(this.velocity.y < 0f && this.position.y < this.insideGeyser.position.y)
            {
                this.position.y = this.insideGeyser.position.y;
            }
        }

        if(isInsideGeyser == true)
        {
            ChangeState(State.InsideGeyser);
        }
    }

    private void AdvanceInsideGeyser()
    {
        if(this.map.currentTemperature == Map.Temperature.Cold)
        {
            ChangeState(State.EscapeFromGeyser);
        }
    }

    private void AdvanceEscapeFromGeyser()
    {        
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        this.escapingTimer += 1;
        if(this.escapingTimer > 15)
        {
            ChangeState(State.Drop);
        }
    }

    private void AdvanceDrop()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        if(this.onGround == true)
        {
            ChangeState(State.Land);
        }
    }

    private int iceTimer;

    private void AdvanceSlide()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        this.iceTimer += 1;

        // rotation
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 20
        );
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);

        if(IsOnIce() == false)
        {
            if(this.onGround == false)
            {
                ChangeState(State.Drop);
            }
            else
            {
                ChangeState(State.Walk);
            }
        }
    }

    private bool IsDetectingPlayer()
    {
        var playerRelative = this.map.player.position - this.position;

        if(this.detectPlayerCooldownTimer > 0)
        {
            return false;
        }

        if(this.map.player.alive == false)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        if(this.facingRight == true && this.map.player.position.x < this.position.x)
        {
            return false;
        }

        if(this.facingRight == false && this.map.player.position.x > this.position.x)
        {
            return false;
        }

        if(this.isTurning == true)
        {
            return false;
        }

        return true;
    }

    private bool IsDetectingGeyser(Raycast raycast)
    {
        this.insideGeyser = CheckForGeyser(raycast);
        return this.insideGeyser != null;
    }
    
    private bool IsOnIce()
    {
        return this.onGroundTile != null && this.onGroundTile.ice == true;
    }

    private Geyser CheckForGeyser(Raycast raycast)
    {
        var reach = edgeTurnSensorForward + 0.125f;
        var xOffset = this.facingRight ?
            this.hitboxes[0].xMax + reach :
            this.hitboxes[0].xMin - reach;

        var start = this.position + new Vector2(xOffset, 0);

        var maxDistance = -this.hitboxes[0].yMin + 20;
        var r1 = raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance);
        var r2 = raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance);
        var r3 = raycast.Vertical(start + new Vector2( 0.375f, 0), false, maxDistance);
        
        var result = Raycast.CombineClosest(r1, r2, r3);

        foreach(Item item in result.items)
        {
            if(item is Geyser && (item as Geyser).CanHippoEnter())
            {
                return item as Geyser;
            }
        }
        return null;
    }

    protected bool CanJumpWall(Raycast raycast)
    {
        if(this.onGround == false) return false;

        var rect = this.hitboxes[0];

        float forwardAmount = Mathf.Abs(velocity.x);
        float distance = forwardAmount + 1.75f;

        var tooHigh = this.position.Add(0, rect.yMax + 1.50f);
        var low  = this.position.Add(0, rect.yMin + 0.02f);
        
        var maxDistance = this.facingRight ?
            rect.xMax + distance :
            -rect.xMin + distance;

        var rHigh = raycast.Horizontal(tooHigh, this.facingRight, maxDistance);
        var rLow  = raycast.Horizontal(low, this.facingRight, maxDistance);
        
        if (this.facingRight)
        {
            if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
            if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;
        }
        else
        {
            if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
            if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;
        }

        return rLow.anything == true && rHigh.anything == false;
    }

    private void CreateDustTrail()
    {
        float x = this.position.x + 0.50f * (this.facingRight ? -1f : 1f);
        float y = this.position.y - 1f;

        int lifetime = UnityEngine.Random.Range(25, 35);

        Particle p = Particle.CreatePlayAndHoldLastFrame(
            this.map.player.animationDustParticle,
            lifetime,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.75f, 1f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.3f, 0.3f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void CreateDustDrop()
    {
        float x = this.position.x;
        float y = this.position.y - 1f;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                originPosition,
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            p.velocity.x =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(0.2f, 0.3f));
            p.velocity.y = UnityEngine.Random.Range(-0.05f, 0.05f);
            p.acceleration = p.velocity * -0.05f;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
        }
    }

    private void CreateDustTakeOff()
    {
        float x = this.position.x;
        float y = this.position.y - 1f;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                originPosition,
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.6f, 0.6f, 1);
            p.velocity.x =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(0.1f, 0.2f));
            p.velocity.y = UnityEngine.Random.Range(-0.05f, 0.05f);
            p.acceleration = p.velocity * -0.03f;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
        }
    }

    private void EnterWalk()
    {
        this.animated.PlayAndLoop(this.animationWalk);
        this.velocity.x = WalkSpeed * (this.facingRight ? 1f : -1f);
    }

    private void EnterDetect()
    {
        this.animated.PlayOnce(this.animationDetect, () =>
        {
            ChangeState(State.JumpUp);
        });

        ChangeScale(new Vector3(0.8f, 1.2f));

        this.velocity.x = 0f;
        this.velocity.y += 0.25f;

        this.detectPlayerCooldownTimer = DetectPlayerCooldownTime;
    }

    private void EnterJumpUp()
    {
        this.animated.PlayAndHoldLastFrame(this.animationJumpUp);

        this.isDeadly = false;
        ChangeScale(new Vector3(0.8f, 1.2f));
        float horizontalDistanceToPlayer =
            (this.map.player.position.x - this.position.x);

        this.jumpUpTimer = 40;

        this.velocity = new Vector2(this.facingRight ? 0.13f : -0.13f, 0.37f);
        this.acceleration = new Vector2(0,-0.01f);
        // this.canTurn = false;
        this.onTurnAround = null;
        CreateDustTakeOff();

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyHippoStompJump,
            position: this.position
        );
    }

    private float GetAcceleration(float distance, float initialVelocity, float time)
    {
        return (2 * distance) / (time * time) - (2 * initialVelocity) / time;
    }

    private void ExitJumpUp()
    {
        this.acceleration = Vector2.zero;
        this.isDeadly = true;
    }

    private void EnterJumpDown()
    {
        this.animated.PlayAndHoldLastFrame(this.animationJumpDown);

        this.velocity.x = 0f;
        this.velocity.y = 0.10f;
    }

    private void ExitJumpDown()
    {
        this.acceleration = Vector2.zero;
        ChangeScale(new Vector3(0.8f, 1.2f));
        
        this.canTurn = true;
    }

    private void EnterLand()
    {
        this.map.ScreenShakeAtPosition(this.position, new Vector2(0f, -0.8f));

        ChangeScale(new Vector3(1.3f, 0.7f));
        this.velocity.x = 0f;

        Rect currentRect = this.hitboxes[0].InWorldSpace();
        currentRect = currentRect.Inflate(new Vector2(1.5f, 0.5f));
        this.map.Damage(currentRect, new KillInfo { itemDeliveringKill = this });
        
        {
            Vector3 pPos = this.facingRight ?
                this.position.Add(0f, -0.50f) : this.position.Add(0.20f, -0.50f);

            var p = Particle.CreatePlayAndHoldLastFrame(
                Assets.instance.enemyHitPowAnimation,
                15,
                pPos,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Enemies";
            p.spriteRenderer.sortingOrder = -1;
            p.ScaleOut(this.scaleOutCurve);
        }

        Animated.Animation currentLandAnimation = this.previousState == State.JumpDown ?
            this.animationLandFromJump : this.animationLandFromDrop;
        this.animated.PlayOnce(currentLandAnimation, () =>
        {
            ChangeState(State.Walk);
        });

        CreateDustDrop();
        this.onTurnAround = OnTurnAround;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyHippoStompLand,
            position: this.position
        );
    }

    private void ExitLand()
    {
        // nothing
    }

    private void EnterPrepareHop()
    {
        this.velocity = Vector2.zero;
        ChangeScale(new Vector3(1.2f, 0.8f));

        this.animated.PlayOnce(this.animationPrepareHop, () =>
        {
            ChangeState(State.Hop);
        });
    }

    private void EnterHop()
    {
        ChangeScale(new Vector3(0.8f, 1.2f));

        this.velocity.x = 0.10f * (facingRight ? 1f : -1f);
        this.velocity.y = 0.60f;

        CreateDustTakeOff();

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyHippoJump,
            position: this.position
        );
    }

    private void ExitHop()
    {
        ChangeScale(new Vector3(1.2f, 0.8f));
    }

    private void EnterDetectGeyser()
    {
        this.insideGeyser.DetectHippo();

        bool geyserToTheRight = this.insideGeyser.position.x > this.position.x;
        bool facingGeyser = this.facingRight == geyserToTheRight;

        if(facingGeyser == false)
        {
            this.facingRight = !this.facingRight;
        }

        this.velocity.x = 0.15f * (facingRight ? 1f : -1f);
        this.velocity.y = 0.20f;
    }

    private void EnterInsideGeyser()
    {
        this.insideGeyser.EnterHippo();
        this.animated.PlayAndLoop(this.animationInsideGeyser);
        this.handSprite.enabled = true;
    }

    private void ExitInsideGeyser()
    {
        this.handSprite.enabled = false;
    }

    private void EnterEscapeFromGeyser()
    {
        this.animated.PlayAndLoop(this.animationDrop);

        this.velocity.x = 0f;
        this.velocity.y = 0.50f;
    }

    private void ExitEscapeFromGeyser()
    {
        this.insideGeyser.ExitHippo();
        this.insideGeyser = null;
        this.escapingTimer = 0;
    }

    private void EnterDrop()
    {
        this.animated.PlayAndLoop(this.animationDrop);
        this.velocity.y = 0.15f;
    }

    private void EnterSlide()
    {
        this.animated.PlayAndLoop(this.animationSlide);
    }

    private void ExitSlide()
    {
        this.visibleAngleDegrees = 0f;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);

        this.iceTimer = 0;
    }

    public override bool Kill(KillInfo killInfo)
    {
        base.Kill(killInfo);
        if(this.insideGeyser != null)
        {
            this.insideGeyser.ExitHippo();
            this.insideGeyser = null;
        }
        return true;
    }

    public override void Hit(KillInfo killInfo)
    {
        base.Kill(killInfo);

        if(this.insideGeyser != null)
        {
            this.insideGeyser.ExitHippo();
            this.insideGeyser = null;
        }
    }

    private void AnimationAirborne()
    {
        Animated.Animation animationAirborne = this.velocity.y > 0f ?
            this.animationRise : this.animationFall;

        if(this.animated.currentAnimation != animationAirborne)
        {
            this.animated.PlayAndLoop(animationAirborne);
        }
    }
}
