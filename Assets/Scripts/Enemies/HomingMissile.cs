﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class HomingMissile : Enemy
{
    public Animated.Animation animationInitialSpin;
    public Animated.Animation animationSecondaryWobble;
    public Animated.Animation animationMove;
    public Animated.Animation animationBurst;

    [NonSerialized] public Vector2 velocity;
    public bool alive = true;
    private float directionAngle;
    private Vector2 direction = Vector2.right;
    public Sprite particleSprite;
    public AnimationCurve scaleOutCurve;
    public Transform trailOrigin;
    public Transform headPoint;
    private Vector2 startPosition;
    private Action onExplode;

    public static HomingMissile Create(Vector2 position, Chunk chunk, float angle, Action actionOnExplode)
    {
        var obj = Instantiate(
            Assets.instance.conflictCanyon.homingMissile, chunk.transform
        );
        obj.name = "Spawned Homing Missile";
        var item = obj.GetComponent<HomingMissile>();
        item.Init(position, chunk, angle, actionOnExplode);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, float angle = 0, Action actionOnExplode = null)
    {
        base.Init(new Def(chunk));
        onExplode = actionOnExplode;
        this.transform.position = this.position = this.startPosition = position;
        this.chunk = chunk;

        this.hitboxes = new[] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid),
            new Hitbox( .5f, .75f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayOnce(this.animationInitialSpin);
        this.animated.onComplete = PlayWobble;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.directionAngle = angle;
        this.transform.rotation = Quaternion.Euler(0, 0, this.directionAngle);
    }

    public void PlayWobble()
    {
        this.animated.PlayAndLoop(this.animationSecondaryWobble);
    }

    public override void Advance()
    {
        if (this.alive == false) return;

        // DebugDrawing.Draw(false, true);
        var headPointRelative = this.headPoint.position - this.transform.position;
        base.Advance();
        Move();
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        EnemyHitDetectionAgainstPlayer(this.hitboxes[1].InWorldSpace());
        this.transform.position = this.position;
        AdvanceTrail();

        float halfSize = 0.35f;
        this.hitboxes = new[] {
            new Hitbox(-halfSize, halfSize, -halfSize, halfSize, this.rotation, Hitbox.NonSolid),
            new Hitbox( headPointRelative.x -halfSize, headPointRelative.x +halfSize, headPointRelative.y -halfSize, headPointRelative.y + halfSize, this.rotation, Hitbox.NonSolid)
        };
    }

    private void Move()
    {
        DetectPlayer();
        CalculateDirectionFromAngle(this.directionAngle);
        this.velocity = this.direction * 2f / 16;

        Vector2 headPos = this.headPoint.position;
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(headPos.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(headPos.Add(0, 6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(headPos.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(headPos.Add(0, 6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(headPos.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(headPos.Add(6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(headPos.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(headPos.Add(6 / 16f, 0), false, 0.75f)
        );

        bool hitWall = false;

        if (this.velocity.x > 0)
        {
            if (right.anything == true)
            {
                hitWall = true;
            }
        }
        else
        {
            if (left.anything == true)
            {
                hitWall = true;
            }
        }

        if (this.velocity.y > 0)
        {
            if (up.anything == true)
            {
                hitWall = true;
            }
        }
        else
        {
            if (down.anything == true)
            {
                hitWall = true;
            }
        }

        if (hitWall)
        {
            Collide();
        }

        this.directionAngle = Util.WrapAngle0To360(this.directionAngle);
        
        this.transform.rotation = Quaternion.Euler(0, 0, this.directionAngle);
        this.position += this.velocity;
        this.transform.position = this.position;
    }

    private void Collide()
    {
        this.alive = false;

        Particle.CreateAndPlayOnce(
            Assets.instance.explosionAnimation,
            this.position.Add(0, 0),
            this.transform.parent
        );

        CreateExplosionParticles();

        this.map.Damage(
                    this.hitboxes[1].InWorldSpace().Inflate(1f),
                    new Enemy.KillInfo { itemDeliveringKill = this }
                );

        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .5f);
        Audio.instance.PlaySfx(Assets.instance.sfxTrunkyProjectileLand, position: this.position);
        Audio.instance.PlaySfx(Assets.instance.sfxEnvExplosion, position: this.position);
        Destroy(this.gameObject);
        this.chunk.items.Remove(this);
        onExplode?.Invoke();
    }

    private void CalculateDirectionFromAngle(float angleDeg)
    {
        float angleRad = angleDeg * Mathf.Deg2Rad;
        this.direction.x = Mathf.Cos(angleRad);
        this.direction.y = Mathf.Sin(angleRad);
    }

    private void DetectPlayer()
    {
        var playerRelative = this.map.player.position - (Vector2)this.transform.position;
        float angleToPlayer = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));
        float delta = Util.WrapAngle0To360(angleToPlayer - this.directionAngle);

        if (delta > 180)
        {
            this.directionAngle -= 2;
        }
        else if (delta < 180)
        {
            this.directionAngle += 2;
        }
    }

    private void AdvanceTrail()
    {
        if (((Vector2)this.trailOrigin.position - this.startPosition).magnitude > 1f)
        {
            // start creating particles at some distance from player
            CreateTrailParticle(new Vector3(
                    this.trailOrigin.position.x + Rnd.Range(-0.1f, 0.1f),
                    this.trailOrigin.position.y + Rnd.Range(-0.2f, 0.2f)
                    ),
                    new Vector2(
                        Rnd.Range(-0.1f, 0.1f),
                        Rnd.Range(0, 0.2f)
                        ) / 16,
                    140
                    );
        }
    }

    private void CreateExplosionParticles()
    {
        for (int i = 0; i < 30; i++)
        {
            Vector3 particlePosition = new Vector3(
                this.transform.position.x + Rnd.Range(-0.2f, 0.2f),
                this.transform.position.y + Rnd.Range(-0.3f, 0.3f)
                );
            var particleVelocity = (particlePosition - this.transform.position).normalized * Rnd.Range(0.2f, 0.3f);
            particlePosition = this.transform.position + (particlePosition - this.transform.position).normalized * Rnd.Range(0.8f, 0.9f);
            CreateTrailParticle(particlePosition, particleVelocity / 16, 140, 64 + i);
        }
    }

    private void CreateTrailParticle(Vector3 particlePosition, Vector2 particleVelocity, int lifeTime, int sortingOrderOffset = 0)
    {
        Particle trailParticle = Particle.CreateWithSprite(
                this.particleSprite,
                lifeTime,
                particlePosition,
                this.transform.parent
            );
        float s = Rnd.Range(0.8f, 1.4f);
        trailParticle.transform.localScale = new Vector3(s, s, 1);
        trailParticle.ScaleOut(this.scaleOutCurve);
        trailParticle.velocity = particleVelocity;
        trailParticle.spriteRenderer.sortingLayerName = "Player Trail (BL)";
        trailParticle.spriteRenderer.sortingOrder = -(1 + sortingOrderOffset + (this.map.frameNumber % 32));
    }

    public override void Hit(KillInfo killInfo)
    {
        base.Kill(killInfo);
        onExplode?.Invoke();
    }
}
