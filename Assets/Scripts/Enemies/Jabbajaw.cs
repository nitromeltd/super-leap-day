﻿using System.Linq;
using UnityEngine;

public class Jabbajaw : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationAlert;
    public Animated.Animation animationCharge;
    // when he charges and hits the wall, we briefly use the fall frame
    // before he goes into his shake head animation
    public Animated.Animation animationHitWall;
    public Animated.Animation animationPostHitIdle;
    public Animated.Animation animationShake;
    public Animated.Animation animationPostShakeIdle;
    public Animated.Animation animationWobble;
    public Animated.Animation animationFall;
    public Animated.Animation animationRise;

    private enum State
    {
        Walk,
        Alert,
        Charge,
        HitWall,
        Shake,
        Wobble,
        Airborne,
        TurningInPlace
    };

    private Gravity gravity;
    private State state = State.Walk;
    private float visibleAngleDegrees = 0f;
    private bool isTurning = false;
    private bool snapToTargetRotation = false;
    private int chargeCooldownTime = 0;
    private bool isLastChargeDirectionRight = false;
    private bool isFirstCharge = true;
    private int shakeTime = 0;
    private bool insideWater = false;
    private int chargeTime = 0;
    private GravityAtmosphere underInfluenceFromGravityAtmosphere;
    private TurningInPlaceDetails turningInPlaceDetails;

    private const int MaxPlayerVerticalDistance = 4;
    private const int MaxPlayerHorizontalDistance = 6;
    private const float WalkSpeed = 0.05f;
    private const float ChargeSpeed = 0.1f;
    private const int SameDirectionChargeCooldownTime = 60;
    private const float GravityForce = 0.03125f;
    private const float GravityForceUnderwater = 0.01f;
    private static Vector2 HitWallPushbackForce = new Vector2(-0.1f, 0.2f);

    private float Halfwidth = 0.9f;
    private float Halfheight = 0.7f;
    private float CurrentGravityForce => this.insideWater == true ?
        GravityForceUnderwater : GravityForce;

    public struct Gravity
    {
        // remember, this isn't the same as player.groundState.orientation
        // this only changes in the space theme when the gravity gets flipped.
        public Jabbajaw jabbajaw;
        public Vector2? gravityDirection;

        public Player.Orientation Orientation()
        {
            if (this.gravityDirection?.y < -0.707f)
                return Player.Orientation.Normal;
            else if (this.gravityDirection?.x > 0.707f)
                return Player.Orientation.RightWall;
            else if (this.gravityDirection?.y > 0.707f)
                return Player.Orientation.Ceiling;
            else if (this.gravityDirection != null)
                return Player.Orientation.LeftWall;
            else
                return Player.Orientation.Normal;
                // return this.jabbajaw.groundState.orientation;
        }
        public bool IsZeroGravity() => gravityDirection == null;

        public Vector2 Down() =>
            this.gravityDirection ?? Player.Orientation.Normal switch
            {
                Player.Orientation.RightWall => Vector2.right,
                Player.Orientation.Ceiling   => Vector2.up,
                Player.Orientation.LeftWall  => Vector2.left,
                _                            => Vector2.down,
            };

        public Vector2 Right() { var down = Down(); return new Vector2(-down.y,  down.x); }
        public Vector2 Up()    { var down = Down(); return new Vector2(-down.x, -down.y); }
        public Vector2 Left()  { var down = Down(); return new Vector2( down.y, -down.x); }

        public int FloorAngle()
        {
            var right = Right();
            var angleDegrees = Mathf.Atan2(right.y, right.x) * 180 / Mathf.PI;
            var angleDegreesAsInt = Mathf.FloorToInt(angleDegrees);
            angleDegreesAsInt %= 360;
            if (angleDegreesAsInt < 0)
                angleDegreesAsInt += 360;
            return angleDegreesAsInt;
        }
        public int RightWallAngle() => (FloorAngle() + 90) % 360;
        public int CeilingAngle()   => (FloorAngle() + 180) % 360;
        public int LeftWallAngle()  => (FloorAngle() + 270) % 360;

        public float VelocityRight
        {
            get => Vector2.Dot(this.jabbajaw.velocity, Right());
            set {
                var right = Right();
                var across = new Vector2(-right.y, right.x);
                var speedRight = Vector2.Dot(right, this.jabbajaw.velocity);
                var speedAcross = Vector2.Dot(across, this.jabbajaw.velocity);
                speedRight = value;
                this.jabbajaw.velocity = (right * speedRight) + (across * speedAcross);
            }
        }

        public float VelocityUp
        {
            get => Vector2.Dot(this.jabbajaw.velocity, Up());
            set {
                var up = Up();
                var across = new Vector2(-up.y, up.x);
                var speedUp = Vector2.Dot(up, this.jabbajaw.velocity);
                var speedAcross = Vector2.Dot(across, this.jabbajaw.velocity);
                speedUp = value;
                this.jabbajaw.velocity = (up * speedUp) + (across * speedAcross);
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => new Vector2(
                Vector2.Dot(this.jabbajaw.velocity, Right()),
                Vector2.Dot(this.jabbajaw.velocity, Up())
            );
            set {
                this.jabbajaw.velocity = (Right() * value.x) + (Up() * value.y);
            }
        }
    }

    public struct TurningInPlaceDetails
    {
        public float targetAngleDegrees;
        public Vector2 currentCenter;
        public Vector2 idealCenter;
    };

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.turnSensorForward = .3f;
        this.edgeTurnSensorForward = -.45f;
        
        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = WalkSpeed * initialDirectionValue;

        if (this.map.theme == Theme.GravityGalaxy)
            Halfwidth = Halfheight;

        this.hitboxes = new [] {
            new Hitbox(-Halfwidth, Halfwidth, -Halfheight, Halfheight, this.rotation, Hitbox.NonSolid)
        };

        this.gravity = new Gravity
        {
            jabbajaw = this,
            gravityDirection = Vector2.down
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.onTurnAround = OnTurnAround;

        EnterWalk();
    }

    protected override Raycast.CombinedResults SenseHorizontal(
        Raycast raycast, bool right, float distance, out float correctionOffsetX
    )
    {
        var rect = this.hitboxes[0];
        var high = this.position + (this.gravity.Up() * (Halfheight - 0.1f));
        var mid  = this.position;
        var low  = this.position + (this.gravity.Up() * (-Halfheight + 0.1f));

        var maxDistance = right ?
            rect.xMax + turnSensorForward + distance :
            -rect.xMin + turnSensorForward + distance;
        var direction = right ? this.gravity.Right() : this.gravity.Left();

        var rHigh = raycast.Arbitrary(high, direction, maxDistance);
        var rMid  = raycast.Arbitrary(mid,  direction, maxDistance);
        var rLow  = raycast.Arbitrary(low,  direction, maxDistance);

        float RelativeAngle(float angle) =>
            (angle + 360 - this.gravity.FloorAngle()) % 360;

        if (right)
        {
            if (Util.ClampAngleDegrees(RelativeAngle(rHigh.surfaceAngleDegrees), 0, 180) <= 45)
                rHigh.anything = false;
            if (Util.ClampAngleDegrees(RelativeAngle(rMid.surfaceAngleDegrees), 0, 180) < 90)
                rMid.anything = false;
            if (Util.ClampAngleDegrees(RelativeAngle(rLow.surfaceAngleDegrees), 0, 180) < 90)
                rLow.anything = false;
        }
        else
        {
            if (Util.ClampAngleDegrees(RelativeAngle(rHigh.surfaceAngleDegrees), 180, 360) >= 315)
                rHigh.anything = false;
            if (Util.ClampAngleDegrees(RelativeAngle(rMid.surfaceAngleDegrees), 180, 360) > 270)
                rMid.anything = false;
            if (Util.ClampAngleDegrees(RelativeAngle(rLow.surfaceAngleDegrees), 180, 360) > 270)
                rLow.anything = false;
        }

        var result = Raycast.CombineClosest(rLow, rMid, rHigh);

        correctionOffsetX = right ?
            result.distance - maxDistance :
            maxDistance - result.distance;
        return result;
    }

    protected override bool IsAtEdgeOfPlatform(Raycast raycast, bool right, bool extended)
    {
        if (this.onGround == false) return false;

        var reach = edgeTurnSensorForward + (extended ? 1.125f : 0.125f);
        var xOffset = right ?
            this.hitboxes[0].xMax + reach :
            this.hitboxes[0].xMin - reach;

        var start = this.position + (this.gravity.Right() * xOffset);

        var portal = this.map.PortalAt(
            Mathf.FloorToInt(start.x + (right ? 0.38f : -0.38f)),
            Mathf.FloorToInt(start.y)
        );
        if (portal != null)
            return false;

        var maxDistance = -this.hitboxes[0].yMin + 20;
        var dx = this.groundSensorSpacing;
        var r1 = raycast.Arbitrary(start + (this.gravity.Right() * -dx), this.gravity.Down(), maxDistance);
        var r2 = raycast.Arbitrary(start,                                this.gravity.Down(), maxDistance);
        var r3 = raycast.Arbitrary(start + (this.gravity.Right() *  dx), this.gravity.Down(), maxDistance);

        // we're looking for big steps down or cliffs. if the raycast hits something
        // early, then this is a step up rather than a drop down. we don't want to
        // return true in that case, that's SenseHorizontal's responsibility.
        var minimumDistanceToBeDrop = -this.hitboxes[0].yMin + 0.5f;
        if (r1.distance < minimumDistanceToBeDrop &&
            r2.distance < minimumDistanceToBeDrop &&
            r3.distance < minimumDistanceToBeDrop)
        {
            return false;
        }
        else
        {
            if (r1.distance < minimumDistanceToBeDrop) r1.anything = false;
            if (r2.distance < minimumDistanceToBeDrop) r2.anything = false;
            if (r3.distance < minimumDistanceToBeDrop) r3.anything = false;
        }

        var result = Raycast.CombineClosest(r1, r2, r3);

        if (this.gravity.Orientation() == Player.Orientation.Normal)
        {
            // if we see tiles on the ground in front, check whether or not they are
            // topologically connected to what we are standing on. if yes, then it's
            // not a step down, it's just a curve curving down or something, so don't turn.
            if (this.onGroundTile != null && result.tiles.Length > 0)
            {
                var connected = this.onGroundTile.tileGrid.AreTileTopsConnected(
                    this.onGroundTile, result.tiles[0]
                );
                if (connected == true)
                    return false;
            }
        }
        else
        {
            // its too hard to replace that for this case.
            return false;
        }

        return
            result.anything == false ||
            result.distance > minimumDistanceToBeDrop;
    }

    protected override void Move(Raycast raycast, bool stopHorizontally = false)
    {
        if (stopHorizontally == false)
            MoveHorizontally();
        MoveVertically();

        void MoveHorizontally()
        {
            float forwardAmount = Mathf.Abs(this.gravity.VelocityRight);
            if (forwardAmount == 0)
                return;

            this.facingRight = this.gravity.VelocityRight > 0;

            if (this.pauseAfterTurnRemainingTime > 0)
            {
                this.pauseAfterTurnRemainingTime -= 1;
                return;
            }

            this.trappedHorizontally = false;

            var forward = SenseHorizontal(
                raycast, this.facingRight, forwardAmount, out float correctionX
            );
            var turnHere =
                forward.anything == true ||
                (turnAtEdges == true && IsAtEdgeOfPlatform(
                    raycast, this.facingRight, false
                ));

            if (turnHere == false)
            {
                this.position += this.gravity.Right() * (this.facingRight ? forwardAmount : -forwardAmount);
                return;
            }

            if(this.canTurn == false)
            {
                return;
            }

            var backward = SenseHorizontal(
                raycast,
                !this.facingRight,
                forwardAmount + 1,
                out float backwardCorrectionX
            );
            var wouldAlsoTurnGoingBackwards =
                backward.anything == true ||
                (turnAtEdges == true && IsAtEdgeOfPlatform(
                    raycast, !this.facingRight, true
                ));

            if (wouldAlsoTurnGoingBackwards == false ||
                stopIfTrappedHorizontally == false)
            {
                var c = new Item.Collision { otherItem = this };
                if (this.facingRight == true)
                {
                    foreach (var i in forward.items)
                        i.onCollisionFromLeft?.Invoke(c);
                }
                else
                {
                    foreach (var i in forward.items)
                        i.onCollisionFromRight?.Invoke(c);
                }

                this.position += this.gravity.Right() * correctionX;
                this.gravity.VelocityRight *= -1;
                this.facingRight = (this.gravity.VelocityRight > 0);
                this.onTurnAround?.Invoke(new TurnInfo
                {
                    raycastResults = forward,
                    turnReason =
                        forward.anything == true ? TurnReason.Wall : TurnReason.Ledge
                });
                this.pauseAfterTurnRemainingTime = 5;
            }
            else
            {
                // stuck (neither direction has any exits)
                this.trappedHorizontally = true;
            }
        }

        void MoveVertically()
        {
            this.onGround = false;
            this.onGroundTile = null;

            var rect = this.hitboxes[0];

            if (this.gravity.VelocityUp > 0)
            {
                var maxDistance = this.gravity.VelocityUp + rect.yMax;
                var result = raycast.Arbitrary(this.position, this.gravity.Up(), maxDistance);
                if (result.anything == true &&
                    result.distance < this.gravity.VelocityUp + rect.yMax)
                {
                    this.position += this.gravity.Up() * (result.distance - rect.yMax);
                    this.gravity.VelocityRight = 0;
                }
                else
                {
                    this.position += this.gravity.Up() * this.velocity.y;
                }

                this.groundAngleDegrees = this.gravity.FloorAngle();
            }
            else if (this.gravity.VelocityUp < 0)
            {
                var tolerance = 0.1f;
                var start = this.position;
                var maxDistance = -this.gravity.VelocityUp + Halfheight + tolerance;
                var dx = this.groundSensorSpacing;
                var r1 = raycast.Arbitrary(start + (this.gravity.Right() * -dx), this.gravity.Down(), maxDistance);
                var r2 = raycast.Arbitrary(start + (this.gravity.Right() *   0), this.gravity.Down(), maxDistance);
                var r3 = raycast.Arbitrary(start + (this.gravity.Right() *  dx), this.gravity.Down(), maxDistance);
                var result = Raycast.CombineClosest(r1, r2, r3);
                if (result.anything == true &&
                    result.distance < -this.gravity.VelocityUp + Halfheight + tolerance)
                {
                    Vector2 surfaceVelocity = Vector2.zero;
                    if (result.tiles.Length > 0)
                        surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                    foreach (var conveyor in result.items.OfType<Conveyor>())
                    {
                        surfaceVelocity.x = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed;  //0.5f
                        surfaceVelocity.y = 0;
                    }

                    foreach (var raft in result.items.OfType<Raft>())
                    {
                        surfaceVelocity = raft.velocity;
                    }

                    this.position.x += surfaceVelocity.x;
                    this.position.y += surfaceVelocity.y;
                    this.position += this.gravity.Up() * (-result.distance + Halfheight);
                    this.gravity.VelocityUp = 0;
                    this.onGround = true;
                    if (result.tiles.Length > 0)
                        this.onGroundTile = result.tiles[0];
                    this.groundAngleDegrees =
                        r2.anything ?
                        r2.surfaceAngleDegrees :
                        result.surfaceAngleDegrees;
                }
                else
                {
                    this.position += this.gravity.Up() * this.gravity.VelocityUp;
                }
            }
        }
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        switch(state)
        {
            case State.Walk:
            {
                if(turnInfo.turnReason != TurnReason.Player)
                {
                    Turn();
                }
            }
                break;

            case State.Charge:
            {
                if(turnInfo.turnReason == TurnReason.Wall)
                {
                    EnterHitWall();
                }
                else if (turnInfo.turnReason == TurnReason.Ledge)
                {
                    EnterWobble();
                }
            }
                break;
        }
    }
    
    public override void Advance()
    {
        // Debug.Log(string.Format(
        //     "[{0}]   " +
        //     "<color=grey>pos</color> <color=white><b>({1}, {2})</b></color> " +
        //     "<color=grey>vel</color> <color=white><b>({3}, {4})</b></color> " +
        //     "<color=grey>grav</color> <color=white><b>{5}</b></color> " +
        //     "<color=grey>gs</color> <color=white><b>({6} {7})</b></color> " +
        //     "<color=grey>|</color> <color=white><b>{8} {9}</b></color>",
        //     this.map.frameNumber,
        //     this.position.x.ToString("0.0000"),
        //     this.position.y.ToString("0.0000"),
        //     this.velocity.x.ToString("0.0000"),
        //     this.velocity.y.ToString("0.0000"),
        //     this.gravity.gravityDirection.HasValue ?
        //         this.gravity.gravityDirection.ToString() :
        //         "None",
        //     this.onGround,
        //     this.groundAngleDegrees,
        //     this.facingRight ? "→" : "←",
        //     this.state
        // ));

        if (this.deathAnimationPlaying == true)
            return;

        AdvanceGravity();
        AdvanceWater();
        
        this.gravity.VelocityUp -= CurrentGravityForce;
        
        if(IsAirborne() == true
            && state != State.HitWall)
        {
            EnterAirborne();
        }

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.Alert:
                AdvanceAlert();
                break;

            case State.Charge:
                AdvanceCharge();
                break;

            case State.HitWall:
                AdvanceHitWall();
                break;

            case State.Shake:
                AdvanceShake();
                break;

            case State.Wobble:
                AdvanceWobble();
                break;

            case State.Airborne:
                AdvanceAirborne();
                break;

            case State.TurningInPlace:
                AdvanceTurningInPlace();
                break;
        }
        
        EnemyHitDetectionAgainstPlayer();
        this.transform.position = this.position;
        RefreshRotation();
    }
    
    private void AdvanceGravity()
    {
        Player.Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        Vector2? intendedGravity = SampleAt(this.position) switch
        {
            Player.Orientation.Normal    => Vector2.down,
            Player.Orientation.RightWall => Vector2.right,
            Player.Orientation.Ceiling   => Vector2.up,
            Player.Orientation.LeftWall  => Vector2.left,
            _                     => null
        };;
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        if (this.gravity.gravityDirection == Vector2.up &&
            (intendedGravity == null || intendedGravity == Vector2.down) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Player.Orientation.Ceiling)
                intendedGravity = Vector2.up;
        }
        else if (this.gravity.gravityDirection == Vector2.down &&
            (intendedGravity == null || intendedGravity == Vector2.up) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Player.Orientation.Normal)
                intendedGravity = Vector2.down;
        }

        else if (this.underInfluenceFromGravityAtmosphere == null)
        {
            // we don't have one -- if we're near one, grab it
            var newAtmosphere = GravityAtmosphere.Nearest(this.map, this.position, 1.5f);
            if (newAtmosphere != null)
                this.underInfluenceFromGravityAtmosphere = newAtmosphere.atmosphere;
        }
        else
        {
            // let go of the one we're attached to if we get far enough away from it
            var atmosphereNearestPointToFeet =
                this.underInfluenceFromGravityAtmosphere.NearestPoint(this.position)
                    .positionOfNearestPoint;
            if ((this.position - atmosphereNearestPointToFeet).sqrMagnitude > 3 * 3)
                this.underInfluenceFromGravityAtmosphere = null;
        }

        if (this.underInfluenceFromGravityAtmosphere != null)
        {
            var current = this.underInfluenceFromGravityAtmosphere;
            var nearestOnCurrent = current.NearestPoint(this.position);

            bool IsNearAngle(float targetAngleDegrees) =>
                Mathf.Approximately(Util.WrapAngleMinus180To180(
                    this.groundAngleDegrees - targetAngleDegrees
                ), 0);

            if (nearestOnCurrent.amountThroughPath == 0 && this.facingRight == false)
            {
                var previous = current.ConnectedAtmosphereBackward();
                if (previous != null)
                {
                    if (this.state == State.Walk &&
                        this.onGround == true &&
                        IsNearAngle(previous.rotation) == false &&
                        Vector2.Dot(this.velocity, nearestOnCurrent.forward) < 0 &&
                        GravityAtmosphere.NeedsTurningInPlace(current, previous))
                    {
                        this.state = State.TurningInPlace;
                        this.turningInPlaceDetails = new TurningInPlaceDetails
                        {
                            currentCenter = nearestOnCurrent.positionOfNearestPoint,
                            idealCenter   = nearestOnCurrent.positionOfNearestPoint,
                            targetAngleDegrees = previous.rotation
                        };
                    }

                    var nearestOnPrevious = previous.NearestPoint(this.position);
                    if (nearestOnPrevious.squareDistanceToNearestPoint <
                        nearestOnCurrent.squareDistanceToNearestPoint)
                    {
                        this.underInfluenceFromGravityAtmosphere = previous;
                    }
                }
            }
            else if (nearestOnCurrent.amountThroughPath == 1 && this.facingRight == true)
            {
                var next = current.ConnectedAtmosphereForward();
                if (next != null)
                {
                    var b1 = this.state == State.Walk;
                    var b2 = this.onGround == true;
                    var b3 = IsNearAngle(next.rotation) == false;
                    var b4 = Vector2.Dot(this.velocity, nearestOnCurrent.forward) > 0;
                    var b5 = GravityAtmosphere.NeedsTurningInPlace(current, next);
                    if (this.state == State.Walk &&
                        this.onGround == true &&
                        IsNearAngle(next.rotation) == false &&
                        Vector2.Dot(this.velocity, nearestOnCurrent.forward) > 0 &&
                        GravityAtmosphere.NeedsTurningInPlace(current, next))
                    {
                        this.state = State.TurningInPlace;
                        this.turningInPlaceDetails = new TurningInPlaceDetails
                        {
                            currentCenter = nearestOnCurrent.positionOfNearestPoint,
                            idealCenter   = nearestOnCurrent.positionOfNearestPoint,
                            targetAngleDegrees = next.rotation
                        };
                    }

                    var nearestOnNext = next.NearestPoint(this.position);
                    if (nearestOnNext.squareDistanceToNearestPoint <
                        nearestOnCurrent.squareDistanceToNearestPoint)
                    {
                        this.underInfluenceFromGravityAtmosphere = next;
                    }
                }
            }

            intendedGravity =
                this.underInfluenceFromGravityAtmosphere.NearestPoint(this.position).down;

            Debug.DrawLine(
                this.position,
                this.underInfluenceFromGravityAtmosphere.NearestPoint(this.position).positionOfNearestPoint,
                Color.yellow,
                1f / 60f
            );
        }

        if (this.gravity.gravityDirection != intendedGravity)
        {
            this.gravity.gravityDirection = intendedGravity;

            // this.rotation = intendedGravity switch
            // {
            //     Player.Orientation.RightWall => 90,
            //     Player.Orientation.Ceiling   => 180,
            //     Player.Orientation.LeftWall  => 270,
            //     _                            => 0
            // };
            if (intendedGravity != null)
            {
                var angleRadians = Mathf.Atan2(intendedGravity.Value.y, intendedGravity.Value.x);
                this.rotation = Mathf.RoundToInt(angleRadians * 180 / Mathf.PI);
                this.rotation %= 360;
                if (this.rotation < 0)
                    this.rotation += 360;
                this.groundAngleDegrees = this.rotation;
            }

            {
                int simpleRotation = Mathf.RoundToInt(this.rotation / 90) * 90;
                this.hitboxes = new [] {
                    new Hitbox(-Halfwidth, Halfwidth, -Halfheight, Halfheight, simpleRotation, Hitbox.NonSolid)
                };
                this.RefreshHitboxes();
            }

            if (this.state == State.Walk && this.gravity.VelocityUp > 0)
                this.gravity.VelocityUp = 0;
        }
    }

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }
    
    private void AdvanceWalk()
    {
        base.Advance();
        
        this.chargeCooldownTime = this.chargeCooldownTime > 0 ?
            this.chargeCooldownTime -= 1 : 0;

        if (IsDetectingPlayer())
        {
            var isPlayerOnRight = Vector2.Dot(
                this.map.player.position - this.position, this.gravity.Right()
            ) > 0;
            if(this.facingRight != isPlayerOnRight)
            {
                this.gravity.VelocityRight *= -1;
                this.facingRight = this.gravity.VelocityRight > 0;
                this.onTurnAround?.Invoke(new TurnInfo {
                    raycastResults = null, turnReason = TurnReason.Player
                });

                // turn around before charging at the player
                return;
            }

            EnterAlert();
        }
        else
        {
            if (this.gravity.VelocityRight < -0.01f) this.facingRight = false;
            if (this.gravity.VelocityRight > 0.01f) this.facingRight = true;

            var target = WalkSpeed * (this.facingRight ? 1 : -1);
            this.gravity.VelocityRight = Util.Slide(
                this.gravity.VelocityRight, target, 0.01f
            );
        }
    }

    private bool IsDetectingPlayer()
    {
        var playerRelative = this.map.player.position - this.position;

        if(!this.map.player.alive)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        if(this.isTurning)
        {
            return false;
        }

        if(IsChargingToSameDirectionAgain()
            && this.chargeCooldownTime > 0)
        {
            return false;
        }

        return true;
    }

    private bool IsChargingToSameDirectionAgain()
    {
        if(this.isFirstCharge)
        {
            return false;
        }

        bool isCurrentChargeDirectionRight = 
            this.position.x < this.map.player.position.x;

        return this.isLastChargeDirectionRight == isCurrentChargeDirectionRight;
    }

    private void AdvanceAlert()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        
        Move(raycast, stopHorizontally: true);
    }

    private void AdvanceCharge()
    {
        this.chargeTime += 1;

        if(this.chargeTime > 60 * 10)
        {
            this.chargeTime = 0;
            EnterWobble();

            return;
        }

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        
        if (this.map.frameNumber % 2 == 0)
        {
            CreateDustParticles();
        }
    }

    private void CreateDustParticles()
    {
        float x = this.position.x;
        float y = this.position.y - 0.50f;

        int lifetime = UnityEngine.Random.Range(25, 35);

        Particle p = Particle.CreatePlayAndHoldLastFrame(
            this.map.player.animationDustParticle,
            lifetime,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.50f, 0.80f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.1f, 0.1f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void AdvanceHitWall()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);

        if(this.onGround == true)
        {
            EnterShakeFromWall();
        }
    }

    private void AdvanceShake()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast, stopHorizontally: true);

        if(this.shakeTime > 0)
        {
            this.shakeTime -= 1;
        }
        else if(this.animated.currentAnimation == this.animationShake)
        {
            this.animated.PlayOnce(this.animationPostShakeIdle, () =>
            {
                Turn();
            });
        }
    }

    private void AdvanceWobble()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast, stopHorizontally: true);
    }

    private void AdvanceAirborne()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);

        if(IsAirborne() == false)
        {
            EnterWalk();
        }
    }

    private void AdvanceTurningInPlace()
    {
        var target = this.turningInPlaceDetails.targetAngleDegrees;
        while (target < this.visibleAngleDegrees - 180) target += 360;
        while (target > this.visibleAngleDegrees + 180) target -= 360;

        var newAngleDegrees = Util.WrapAngle0To360(Util.Slide(
            this.visibleAngleDegrees, target, 7
        ));
        var offsetAngleRadians = (newAngleDegrees + 90) * Mathf.PI / 180;

        this.turningInPlaceDetails.currentCenter = Vector3.Lerp(
            this.turningInPlaceDetails.currentCenter,
            this.turningInPlaceDetails.idealCenter,
            0.5f
        );
        this.position = this.turningInPlaceDetails.currentCenter.Add(
            Mathf.Cos(offsetAngleRadians) * Halfheight,
            Mathf.Sin(offsetAngleRadians) * Halfheight
        );

        this.visibleAngleDegrees = newAngleDegrees;
        this.groundAngleDegrees = newAngleDegrees;

        var angleDifferenceAbsolute = Mathf.Abs(
            Util.WrapAngleMinus180To180(this.visibleAngleDegrees - target)
        );
        if (angleDifferenceAbsolute < 1)
        {
            this.visibleAngleDegrees = target;
            this.gravity.gravityDirection = new Vector2(
                Mathf.Cos((target - 90) * Mathf.PI / 180),
                Mathf.Sin((target - 90) * Mathf.PI / 180)
            );
            EnterWalk();
        }
    }

    private void EnterWalk()
    {
        this.state = State.Walk;

        this.gravity.VelocityRight = WalkSpeed * Mathf.Sign(this.gravity.VelocityRight);
        this.snapToTargetRotation = false;

        this.animated.PlayAndLoop(this.animationWalk);
    }

    private void EnterAlert()
    {
        this.state = State.Alert;

        this.animated.PlayOnce(this.animationAlert, () =>
        {
            EnterCharge();
        });
    }

    private void EnterCharge()
    {
        this.state = State.Charge;

        this.gravity.VelocityRight = ChargeSpeed * Mathf.Sign(this.gravity.VelocityRight);
        this.animated.PlayAndLoop(this.animationCharge);
        this.animated.OnFrame(2, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxJabbajawChomp, position: this.position);
        });
        this.chargeCooldownTime = SameDirectionChargeCooldownTime;
        this.isLastChargeDirectionRight = this.facingRight;
        this.isFirstCharge = false;
        this.chargeTime = 0;
    }

    private void EnterHitWall()
    {
        this.state = State.HitWall;
        
        float horizontalPushbackForce =
            Random.Range(-0.05f, 0.1f) + HitWallPushbackForce.x;
        float verticalPushbackForce =
            Random.Range(-0.035f, 0.035f) + HitWallPushbackForce.y;

        this.gravity.VelocityRight =
            horizontalPushbackForce * Mathf.Sign(this.gravity.VelocityRight);
        this.gravity.VelocityUp = verticalPushbackForce;
        this.animated.PlayAndHoldLastFrame(this.animationHitWall);
        
        var particlePos =
            this.position +
            (this.gravity.Right() * 1 * this.gravity.VelocityRight) +
            (this.gravity.Up() * -.5f);
        var p = Particle.CreateWithSprite(
            Assets.instance.jabbajawHitWallParticle,
            8,
            particlePos,
            this.map.transform
        );

        p.transform.localScale = new Vector3(0.7f, 1.3f, p.transform.localScale.z);
        p.scale = .05f;

        this.map.ScreenShakeAtPosition(this.position, new Vector2(.3f, 0.5f));
        Audio.instance.PlaySfx(Assets.instance.sfxJabbajawHitsWall,position: this.position);
        
        this.snapToTargetRotation = true;
    }

    private void EnterWobble()
    {
        this.state = State.Wobble;

        this.animated.PlayOnce(this.animationWobble, () =>
        {
            EnterShakeFromLedge();
        });
    }

    private void EnterShakeFromWall()
    {
        this.state = State.Shake;

        this.shakeTime = 0;
        this.animated.PlayOnce(this.animationPostHitIdle, () =>
        {
            this.animated.PlayAndLoop(this.animationShake);
            
            float animLostTimeInSeconds =
                (float)this.animationShake.sprites.Length / (float)this.animationShake.fps;
            int animLostTimeInFrames = Mathf.RoundToInt(animLostTimeInSeconds * 60);
            int animLookAroundInFrames = Mathf.RoundToInt(animLostTimeInFrames / 2f);

            float numberRepetitions = 1f + (0.5f * Random.Range(1, 7));
            // play and loop the shake animation a random number
            // of times to avoid clumping Jabbajaws together
            this.shakeTime = Mathf.RoundToInt(animLookAroundInFrames * numberRepetitions);
        });
    }

    private void EnterShakeFromLedge()
    {
        this.state = State.Shake;

        this.animated.PlayOnce(this.animationShake, () =>
        {
            this.animated.PlayOnce(this.animationAlert, () =>
            {
                Turn();
            });
        });
    }

    private void EnterAirborne()
    {
        this.state = State.Airborne;

        Animated.Animation animationAirborne = this.velocity.y > 0f ?
            this.animationRise : this.animationFall;

        this.animated.PlayAndLoop(animationAirborne);
    }

    private void Turn()
    {
        RefreshScale();

        this.isTurning = true;
        this.animated.PlayOnce(this.animationTurn, () =>
        {
            this.isTurning = false;
            EnterWalk();
        });
    }

    protected void RefreshScale()
    {
        var velocityRight = this.gravity.VelocityRight;
        if (velocityRight > 0) this.transform.localScale = new Vector3(+1, 1, 1);
        if (velocityRight < 0) this.transform.localScale = new Vector3(-1, 1, 1);
    }

    private void RefreshRotation()
    {
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;

        this.visibleAngleDegrees = this.snapToTargetRotation ?
            this.groundAngleDegrees :
            Util.Slide(this.visibleAngleDegrees, this.groundAngleDegrees, 3);

        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    private bool IsAirborne()
    {
        return this.onGround == false;
    }
}
