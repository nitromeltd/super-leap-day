using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jellyfish : Enemy
{
    public ElectricityMode electricityMode;

    public Animated.Animation animationFloating;
    public Animated.Animation animationFloatingShock;
    public Animated.Animation animationStranded;
    public Animated.Animation animationStrandedShock;
    public Animated.Animation animationDrop;
    public Animated.Animation animationRise;

    public enum ElectricityMode
    {
        AlwaysOn,
        Toggle
    }

    public enum State
    {
        // outside of water
        StrandedOnGround,
        FallingToGround,

        // inside water
        MovingToWaterOrigin,
        FloatingInWater,

        InsideBeachBall
    }

    public State state;
    private Vector2 velocity;
    private Vector2 originPosition;
    private Vector2 floatingPosition;
    private bool insideWater = false;
    private bool grounded = false;
    private float breatheTimer = 0f;
    private int electricityToggleTimeOn = ElectricityDefaultToggleTime;
    private int electricityToggleTimeOff = ElectricityDefaultToggleTime;
    private int electricityTimer = 0;
    private bool electricityActive = false;
    private int delayBeforeMovingtimer = 0;
    private int currentFrame = 0;

    private bool isPlayingWaterSfx = false;

    private const float BreathePeriod = 1.50f;
    private const float BreatheAmplitude = 0.25f;
    private const float MovementSpeed = 0.015f; //0.025f
    private const int ElectricityDefaultToggleTime = 5 * 60;
    private const int DelayBeforeMovingToOriginTime = 20;
    private const float GravityAcceleration = 0.006f;
    private static Vector2 HitboxSize = new Vector2(1f, 0.50f);

    public bool IsElectricityActive => this.electricityActive;
    public int CurrentFrame => this.currentFrame;

    public bool IsElectricityAffectingWater =>
        this.electricityActive == true && this.insideWater == true && IsInsideBeachBall == false;
    private bool IsInsideBeachBall => this.state == State.InsideBeachBall;
    
    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(
                -HitboxSize.x, HitboxSize.x, -HitboxSize.y, HitboxSize.y,
                this.rotation, Hitbox.NonSolid
        )};

        this.originPosition = this.position;

        this.electricityToggleTimeOn = (def.tile.properties?.ContainsKey("toggle_time_on") == true) ?
            def.tile.properties["toggle_time_on"].i * 60 : ElectricityDefaultToggleTime;
        this.electricityToggleTimeOff = (def.tile.properties?.ContainsKey("toggle_time_off") == true) ?
            def.tile.properties["toggle_time_off"].i * 60 : ElectricityDefaultToggleTime;

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;

        ChangeState(State.FallingToGround);
    }

    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        if(this.deathAnimationPlaying == true) return;

        if(IsInsideBeachBall == true)
        {
            AdvanceElectricity();
            return;
        }

        AdvanceWater();

        if(this.electricityMode == ElectricityMode.Toggle)
        {
            AdvanceElectricity();
        }

        if (this.electricityActive == true && (this.state == State.FallingToGround ||
        this.state == State.MovingToWaterOrigin))
        {
            this.electricityTimer = 0;
            PauseElectricity();
        }

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        switch (state)
        {
            case State.StrandedOnGround:
                AdvanceStrandedOnGround();
                break;

            case State.FallingToGround:
                AdvanceFallingToGround();
                break;

            case State.MovingToWaterOrigin:
                AdvanceMovingToWaterOrigin(raycast);
                break;

            case State.FloatingInWater:
                AdvanceFloatingInWater();
                break;
        }

        if(this.electricityActive == true)
        {
            Vector2 electricitySize = HitboxSize * 2f;
            Rect damageRect = new Rect(
                this.position.x - (electricitySize.x * 0.5f),
                this.position.y - (electricitySize.y * 0.5f),
                electricitySize.x,
                electricitySize.y
            );

            if(this.map.player.Rectangle().Overlaps(damageRect))
            {
                this.map.player.Hit(this.position);
            }
        }

        EnemyHitDetectionAgainstPlayer();
        Movement(raycast);

        this.position += this.velocity;
        this.transform.position = this.position;

        if(this.electricityActive == true)
        {
            currentFrame = animated.frameNumber;
        }

    }
    
    public override bool Kill(KillInfo info)
    {
        StopElectricity();

        return base.Kill(info);
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }

        if(this.insideWater == false)
        {
            this.velocity += Vector2.down * GravityAcceleration;
        }
    }

    private void AdvanceStrandedOnGround()
    {
        if(this.insideWater == true)
        {
            ChangeState(State.MovingToWaterOrigin);
        }
    }

    private void AdvanceFallingToGround()
    {
        if(this.grounded == true)
        {
            ChangeState(State.StrandedOnGround);
        }
        else if (this.insideWater == true)
        {
            ChangeState(State.MovingToWaterOrigin);
        }
    }

    private void AdvanceMovingToWaterOrigin(Raycast raycast)
    {
        if(this.delayBeforeMovingtimer > 0)
        {
            this.delayBeforeMovingtimer -= 1;
            return;
        }

        float distance = Vector2.Distance(this.position, this.originPosition);
        Vector2 heading = this.originPosition - this.position;
        Vector2 direction = (heading / distance).normalized;

        Raycast.Result r = raycast.Arbitrary(this.position, direction, 10f);
        bool solidAhead = r.anything == true && r.distance < 2f;

        if(this.insideWater == false)
        {
            ChangeState(State.FallingToGround);
        }
        else if(distance < 0.10f || solidAhead == true ||
            this.position.y > this.map.waterline.currentLevel - 2f)
        {
            ChangeState(State.FloatingInWater);
        }
        else
        {
            this.velocity = direction * MovementSpeed;
        }
    }

    private void AdvanceFloatingInWater()
    {
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float breatheDistance = BreatheAmplitude * Mathf.Sin(theta);
        
        Vector2 targetBreathePosition =
            this.floatingPosition + (Vector2)this.transform.up * breatheDistance;
        
        this.velocity = (targetBreathePosition - this.position) * 0.05f;

        if(this.insideWater == false)
        {
            ChangeState(State.FallingToGround);
        }
    }

    private void Movement(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {

                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            this.grounded = false;
            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                
                if(result.tiles.Length > 0)
                {
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;
                }

                if(result.items.Length > 0 && result.items[0] is Raft)
                {
                    Raft raft = result.items[0] as Raft;
                    surfaceVelocity = raft.velocity;
                }

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.grounded = true;
            }
            else
            {
                this.position.y += this.velocity.y;
                this.grounded = false;
            }
        }
    }

    private void AdvanceElectricity()
    {
        this.electricityTimer += 1;

        float currentToggleTime = this.electricityActive ?
            this.electricityToggleTimeOn : this.electricityToggleTimeOff;
        if (this.electricityTimer > currentToggleTime)
        {
            if(this.IsInsideBeachBall == false)
            {
                ToggleElectricity();
            }
            else
            {
                this.electricityActive = !this.electricityActive;
            }
            this.electricityTimer = 0;
        }
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.StrandedOnGround:
                EnterStrandedOnGround();
                break;

            case State.FallingToGround:
                EnterFallingToGround();
                break;

            case State.MovingToWaterOrigin:
                EnterMovingToWaterOrigin();
                break;

            case State.FloatingInWater:
                EnterFloatingInWater();
                break;
        }
    }

    private void EnterStrandedOnGround()
    {
        this.animated.PlayAndLoop(this.animationStranded);
        this.velocity = Vector2.zero;

        switch (this.electricityMode)
        {
            case ElectricityMode.AlwaysOn:
                PlayElectricity();
                break;

            case ElectricityMode.Toggle:
                PauseElectricity();
                break;
        }
    }

    private void EnterFallingToGround()
    {
        this.animated.PlayAndLoop(this.animationDrop);
        this.velocity = Vector2.zero;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyJellyfishFall,
            position: this.position
        );
    }

    private void EnterMovingToWaterOrigin()
    {
        this.animated.PlayOnce(this.animationRise, () => 
        {
            this.animated.PlayAndLoop(animationFloating);
        });
        this.delayBeforeMovingtimer = DelayBeforeMovingToOriginTime;
    }

    private void EnterFloatingInWater()
    {
        if(this.animated.currentAnimation != this.animationRise)
        {
            this.animated.PlayAndLoop(this.animationFloating);
        }
        this.velocity = Vector2.zero;
        this.floatingPosition = this.position;

        switch (this.electricityMode)
        {
            case ElectricityMode.AlwaysOn:
                PlayElectricity();
                break;

            case ElectricityMode.Toggle:
                PauseElectricity();
                break;
        }
    }

    private void ToggleElectricity()
    {
        if(this.electricityActive == true)
        {
            PauseElectricity();
        }
        else
        {
            PlayElectricity();
        }
    }

    private void PlayElectricity()
    {
        if(this.electricityActive == true) return;

        this.electricityActive = true;

        if(state == State.StrandedOnGround ||
            state == State.FallingToGround)
        {
            this.animated.PlayAndLoop(this.animationStrandedShock);
        }
        else
        {
            foreach (var item in this.map.NearestChunkTo(this.position).items)
            {
                if ((item is Jellyfish) && (item as Jellyfish).IsElectricityAffectingWater == true)
                {
                    (item as Jellyfish).RestartElectricityAnimation();
                }
            }
            this.animated.PlayAndLoop(this.animationFloatingShock);
        }

        this.spriteRenderer.sortingLayerName = "Default";
        this.spriteRenderer.sortingOrder = 100;

        if(this.insideWater == true)
        {
            this.isPlayingWaterSfx = true;
            Audio.instance.PlaySfxLoop(Assets.instance.sfxEnemyJellyfishElectricity, position: this.position);
        }
        else
        {
            this.isPlayingWaterSfx = false;
            Audio.instance.PlaySfxLoop(Assets.instance.sfxEnemyJellyfishElectricityOutside, position: this.position);
        }
    }

    private void PauseElectricity()
    {
        if(this.electricityActive == false) return;

        this.electricityActive = false;
        if (state == State.StrandedOnGround ||
            state == State.FallingToGround)
        {
            this.animated.PlayAndLoop(this.animationStranded);
        }
        else
        {
            if(state == State.MovingToWaterOrigin)
            {
                this.animated.PlayOnce(this.animationRise, () =>
                {
                    this.animated.PlayAndLoop(this.animationFloating);
                });
            }
            else
            {
                this.animated.PlayAndLoop(this.animationFloating);
            }
        }
        this.spriteRenderer.sortingLayerName = "Enemies";

        if(this.isPlayingWaterSfx == false)
        {
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnemyJellyfishElectricityOutside);
        }
        else
        {
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnemyJellyfishElectricity);
        }
    }

    private void StopElectricity()
    {
        PauseElectricity();
        this.electricityTimer = 0;
    }

    public void EnterBeachBallState()
    {
        this.state = State.InsideBeachBall;
        this.spriteRenderer.enabled = false;

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
    }

    public void ExitBeachBallState()
    {
        ChangeState(State.FallingToGround);
        this.spriteRenderer.enabled = true;

        //this.velocity = beachBallVelocity * 0.50f;
        this.velocity.x = 0f;
        this.velocity.y = 0.10f;
    }

    public void AdvanceInsideBeachBall(BeachBall beachBall)
    {
        this.position = beachBall.position;

        if(this.electricityMode == ElectricityMode.Toggle)
        {
            AdvanceElectricity();
        }
    }

    public void RestartElectricityAnimation()
    {
        this.animated.PlayAndLoop(this.animationFloatingShock);
    }

    public override void StopLoopedSfx()
    {
        base.StopLoopedSfx();

        if (this.isPlayingWaterSfx == false)
        {
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnemyJellyfishElectricityOutside);
        }
        else
        {
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnemyJellyfishElectricity);
        }
    }

    public override void Reset()
    {
        this.velocity = Vector2.zero;
        this.insideWater = false;
        this.grounded = false;
        this.breatheTimer = 0f;
        this.electricityTimer = 0;
        
        switch(this.electricityMode)
        {
            case ElectricityMode.AlwaysOn:
                PlayElectricity();
            break;
            
            case ElectricityMode.Toggle:
                PauseElectricity();
            break;
        }
    }
}
