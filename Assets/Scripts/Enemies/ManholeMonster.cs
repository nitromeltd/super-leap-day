﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManholeMonster : Enemy
{
    public Animated.Animation hideAnimation;
    public Animated.Animation appearAnimation;
    public Animated.Animation idleAnimation;
    public Animated.Animation shootLeftAnimation;
    public Animated.Animation shootRightAnimation;
    public Animated.Animation shootUpAnimation;
    public Animated.Animation disappearAnimation;
    
    private enum State
    {
        Hide,
        Appear,
        Idle,
        Shoot,
        Disappear
    };

    public enum ShotDirection
    {
        Right,
        Left,
        Up
    }
    
    private State state;
    private ShotDirection shotDirection;
    private bool facingRight;
    private int hideWaitTimer;
    private Vector2 positionDown;
    private Vector2 positionUp;

    private const int HideWaitTime = 180;
    private const float BulletSpeed = 0.20f;
    private const float VerticalOffset = 5f;

    private static Vector2 HBulletSpawnPos = new Vector2(1.5f, 0.70f);
    private static Vector2 VBulletSpawnPos = new Vector2(0.15f, 2.25f);

    private const float VerticalPositionUp = 1f;

    public override void Init(Def def)
    {
        base.Init(def);

        this.facingRight = !def.tile.flip;
        this.hideWaitTimer = 0;

        this.positionDown = this.position;
        
        if(this.rotation == 0f)
        {
            this.positionUp = this.position.Add(0, VerticalPositionUp);
        }
        else if(this.rotation == 180f)
        {
            this.positionUp = this.position.Add(0, -VerticalPositionUp);
        }
        else if(this.rotation == 90f)
        {
            this.positionUp = this.position.Add(-VerticalPositionUp, 0f);
        }
        else if(this.rotation == 270f)
        {
            this.positionUp = this.position.Add(VerticalPositionUp, 0f);
        }

        EnterHide();
    }

    public override void Advance()
    {
        switch(state)
        {
            case State.Hide:
                AdvanceHide();
                break;

            case State.Appear:
                AdvanceAppear();
                break;

            case State.Idle:
                AdvanceIdle();
                break;

            case State.Shoot:
                AdvanceShoot();
                break;

            case State.Disappear:
                AdvanceDisappear();
                break;
        }
        
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
    }

    private void AdvanceHide()
    {
        if(this.hideWaitTimer > 0)
        {
            this.hideWaitTimer -= 1;

            if(this.hideWaitTimer == 0)
            {
                EnterAppear();
            }
        }
    }

    private void AdvanceAppear()
    {
        EnemyHitDetectionAgainstPlayer();

        if(this.animated.frameNumber >= 3)
        {
            this.position.x = Util.Slide(this.position.x, this.positionUp.x, 0.25f);
            this.position.y = Util.Slide(this.position.y, this.positionUp.y, 0.25f);
        }
    }

    private void AdvanceIdle()
    {
        EnemyHitDetectionAgainstPlayer();
    }

    private void AdvanceShoot()
    {
        EnemyHitDetectionAgainstPlayer();
    }

    private void AdvanceDisappear()
    {
        EnemyHitDetectionAgainstPlayer();
        
        this.position.x = Util.Slide(this.position.x, this.positionDown.x, 0.25f);
        this.position.y = Util.Slide(this.position.y, this.positionDown.y, 0.25f);
    }

    private void EnterHide()
    {
        this.state = State.Hide;
        this.hideWaitTimer = HideWaitTime;
        this.animated.PlayAndLoop(this.hideAnimation);

        this.position = this.positionDown;

        ChangeHitbox(true);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyManholeMonsterHide,
            position: this.position
        );
    }

    private void EnterAppear()
    {
        
        this.state = State.Appear;

        TurnToPlayer();
        this.animated.PlayOnce(this.appearAnimation, () =>
        {
            EnterIdle();
        });

        this.animated.OnFrame(3, () =>
        {
            ChangeHitbox(false);
        });
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyManholeMonsterShow,
            position: this.position
        );
    }

    private void EnterIdle()
    {
        this.state = State.Idle;
        this.animated.PlayOnce(this.idleAnimation, () =>
        {
            EnterShoot();
        });
        
        this.position = this.positionUp;
    }

    private void EnterShoot()
    {
        this.state = State.Shoot;
        
        Animated.Animation currentShotAnimation = null;

        switch(this.shotDirection)
        {
            case ShotDirection.Right:
            default:
                currentShotAnimation = this.facingRight ?
                    this.shootRightAnimation : this.shootLeftAnimation;
            break;
            
            case ShotDirection.Left:
                currentShotAnimation = this.facingRight ?
                    this.shootLeftAnimation : this.shootRightAnimation;
            break;

            case ShotDirection.Up:
                currentShotAnimation = this.shootUpAnimation;
            break;
        }

        this.animated.PlayOnce(currentShotAnimation, () =>
        {
            EnterDisappear();
        });

        this.animated.OnFrame(5, CreateProjectile);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyManholeMonsterSpit,
            position: this.position
        );
    }

    private void EnterDisappear()
    {
        this.state = State.Disappear;
        
        this.animated.PlayOnce(this.disappearAnimation, () =>
        {
            EnterHide();
        });

        this.animated.OnFrame(3, () =>
        {
            ChangeHitbox(true);
        });
    }

    private void TurnToPlayer()
    {
        Player player = this.map.player;
        
        bool IsPlayerUpwards()
        {
            if(this.rotation == 180f)
            {
                return player.position.y < this.position.y - VerticalOffset;
            }
            else if(this.rotation == 90f)
            {
                return player.position.y > this.position.y - 5f &&
                    player.position.y < this.position.y + 5f &&
                    player.position.x < this.position.x - VerticalOffset;
            }
            else if(this.rotation == 270f)
            {
                return player.position.y > this.position.y - 5f &&
                    player.position.y < this.position.y + 5f &&
                    player.position.x > this.position.x + VerticalOffset;
            }

            return player.position.y > this.position.y + VerticalOffset;
        }
        
        bool IsPlayerToTheRight()
        {
            if(this.rotation == 180f)
            {
                return player.position.x < this.position.x;
            }
            else if(this.rotation == 90f)
            {
                return player.position.y > this.position.y;
            }
            else if(this.rotation == 270f)
            {
                return player.position.y < this.position.y;
            }

            return player.position.x > this.position.x;
        }

        bool playerUpwards = IsPlayerUpwards();
        bool playerToTheRight = IsPlayerToTheRight();

        if(playerUpwards == true)
        {
            this.shotDirection = ShotDirection.Up;
        }
        else
        {
            this.shotDirection = playerToTheRight ?
                ShotDirection.Right : ShotDirection.Left;
        }
    }

    private void CreateProjectile()
    {
        Vector2 bulletSpawnPos = this.position;
        Vector2 bulletVelocity = Vector2.zero;

        if(this.rotation == 0f)
        {
            switch(this.shotDirection)
            {
                case ShotDirection.Right:
                    bulletSpawnPos.x += HBulletSpawnPos.x;
                    bulletSpawnPos.y += HBulletSpawnPos.y;

                    bulletVelocity.x = BulletSpeed;
                    bulletVelocity.y = 0f;
                break;
                
                case ShotDirection.Left:
                    bulletSpawnPos.x += -HBulletSpawnPos.x;
                    bulletSpawnPos.y += HBulletSpawnPos.y;

                    bulletVelocity.x = -BulletSpeed;
                    bulletVelocity.y = 0f;
                break;

                case ShotDirection.Up:
                    bulletSpawnPos.x += VBulletSpawnPos.x;
                    bulletSpawnPos.y += VBulletSpawnPos.y;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = BulletSpeed;
                break;
            }
        }
        else if(this.rotation == 180f)
        {
            switch(this.shotDirection)
            {
                case ShotDirection.Right:
                    bulletSpawnPos.x += -HBulletSpawnPos.x;
                    bulletSpawnPos.y += -HBulletSpawnPos.y;

                    bulletVelocity.x = -BulletSpeed;
                    bulletVelocity.y = 0f;
                break;
                
                case ShotDirection.Left:
                    bulletSpawnPos.x += HBulletSpawnPos.x;
                    bulletSpawnPos.y += -HBulletSpawnPos.y;

                    bulletVelocity.x = BulletSpeed;
                    bulletVelocity.y = 0f;
                break;

                case ShotDirection.Up:
                    bulletSpawnPos.x += -VBulletSpawnPos.x;
                    bulletSpawnPos.y += -VBulletSpawnPos.y;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = -BulletSpeed;
                break;
            }
        }
        else if(this.rotation == 90f)
        {
            switch(this.shotDirection)
            {
                case ShotDirection.Right:
                    bulletSpawnPos.x += -HBulletSpawnPos.y;
                    bulletSpawnPos.y += HBulletSpawnPos.x;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = BulletSpeed;
                break;
                
                case ShotDirection.Left:
                    bulletSpawnPos.x += -HBulletSpawnPos.y;
                    bulletSpawnPos.y += -HBulletSpawnPos.x;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = -BulletSpeed;
                break;

                case ShotDirection.Up:
                    bulletSpawnPos.x += -VBulletSpawnPos.y;
                    bulletSpawnPos.y += VBulletSpawnPos.x;

                    bulletVelocity.x = -BulletSpeed;
                    bulletVelocity.y = 0f;
                break;
            }
        }
        else if(this.rotation == 270f)
        {
            switch(this.shotDirection)
            {
                case ShotDirection.Right:
                    bulletSpawnPos.x += HBulletSpawnPos.y;
                    bulletSpawnPos.y += -HBulletSpawnPos.x;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = -BulletSpeed;
                break;
                
                case ShotDirection.Left:
                    bulletSpawnPos.x += HBulletSpawnPos.y;
                    bulletSpawnPos.y += HBulletSpawnPos.x;

                    bulletVelocity.x = 0f;
                    bulletVelocity.y = BulletSpeed;
                break;

                case ShotDirection.Up:
                    bulletSpawnPos.x += VBulletSpawnPos.y;
                    bulletSpawnPos.y += VBulletSpawnPos.x;

                    bulletVelocity.x = BulletSpeed;
                    bulletVelocity.y = 0f;
                break;
            }
        }

        ManholeMonsterBullet bullet = ManholeMonsterBullet.Create(
            bulletSpawnPos,
            this.chunk
        );
        bullet.velocity = bulletVelocity;
    }

    private void ChangeHitbox(bool hide)
    {
        if(hide == true)
        {
            this.hitboxes = new [] {
                new Hitbox(-1f, 1f, -0.50f, 0f, this.rotation, Hitbox.Solid)
            };
            this.whenHitFromAbove = HitFromAboveBehaviour.Nothing;
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-0.5f, 0.5f, -1f, 2f, this.rotation, Hitbox.NonSolid)
            };
            this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        }
    }
}
