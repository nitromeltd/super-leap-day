﻿using System;
using UnityEngine;

public class ManholeMonsterBullet : Enemy
{
    public Pipe insidePipe;
    public Pipe.TrackingData pipeTrackingData;

    public Animated.Animation animationMove;
    public Animated.Animation animationBurst;

    [NonSerialized] public Vector2 velocity;
    [HideInInspector] public bool alive = true;

    private float scale;

    public static ManholeMonsterBullet Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(
            Assets.instance.capitalHighway.manholeMonsterBullet, chunk.transform
        );
        obj.name = "Spawned Manhole Monster Bullet";
        var item = obj.GetComponent<ManholeMonsterBullet>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }
    
    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndLoop(this.animationMove);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.scale = 0f;
        this.transform.localScale = Vector3.one * this.scale;
    }
    
    public override void Advance()
    {
        if (this.alive == false) return;

        if (this.insidePipe != null)
        {
            this.insidePipe.AdvanceManholeBullet(this);
            this.spriteRenderer.sortingLayerName = "Spikes";
            this.spriteRenderer.sortingOrder = -10;
        }
        else
        {
            AdvanceOutsidePipe();
        }
        
        this.map.Damage(
                this.hitboxes[0].InWorldSpace(),
                new KillInfo { itemDeliveringKill = this }
            );


        this.scale = Util.Slide(this.scale, 1f, 0.25f);
        this.transform.localScale = Vector3.one * this.scale;

        LimitPositionToChunk();
        this.transform.position = this.position;
    }

    private void LimitPositionToChunk()
    {
        if(this.position.y >= this.chunk.yMax + 1.5f ||
            this.position.y <= this.chunk.yMin + 0.5f ||
            this.position.x >= this.chunk.xMax ||
            this.position.x <= this.chunk.xMin)
        {
            Collide();
        }
    }
    
    private void AdvanceOutsidePipe()
    {
        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByEnemy: true,
            ignoreTileTopOnlyFlag: true
        );

        MoveHorizontally(raycast);
        MoveVertically(raycast);
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
    }
    
    protected void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        float xOffset = Mathf.Sign(this.velocity.x) * -.25f;

        var high    = this.position.Add(xOffset, rect.yMax - .5f);
        var mid     = this.position.Add(xOffset, (rect.yMin + rect.yMax) * 0.25f);
        var low     = this.position.Add(xOffset, rect.yMin + .5f);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid,  true, maxDistance),
                raycast.Horizontal(low,  true, maxDistance)
            );

            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid,  false, maxDistance),
                raycast.Horizontal(low,  false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }
        
    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
                Collide();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = raycast.Vertical(this.position, false, maxDistance);
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                Collide();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }
    
    private void Collide()
    {
        this.alive = false;

        this.animated.PlayOnce(this.animationBurst, delegate
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
        });

        Audio.instance.PlaySfx(
            Assets.instance.sfxTrunkyProjectileLand,
            position: this.position
        );
    }

    public override bool Kill(KillInfo info)
    {
        if(insidePipe != null) return false;

        return base.Kill(info);
    }
}
