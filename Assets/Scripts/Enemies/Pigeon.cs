
using UnityEngine;

public class Pigeon : Enemy
{
    public Animated.Animation animationIdleStill;
    public Animated.Animation animationIdleBlink;
    public Animated.Animation animationIdlePeck;
    public Animated.Animation animationIdleTurnIn;
    public Animated.Animation animationIdleTurnOut;
    public Animated.Animation animationIdleTurnStill;
    public Animated.Animation animationIdleTurnBlink;
    public Animated.Animation animationTakeFlight;
    public Animated.Animation animationFly;
    public Animated.Animation animationLayEgg;
    public Animated.Animation animationTurn;

    [Header("Particles")]
    public Animated.Animation animationSweat;
    public Animated.Animation animationFeathers;

    private State state = State.None;
    private Vector2 velocity;
    private bool facingRight;
    private int layEggTimer;
    private Vector2? targetFlightPosition;
    private Vector2 targetVelocity;
    private Vector2 initialPosition;
    private NitromeEditor.Path path;
    private float currentSpeed = 0;
    private bool isTurning = false;
    private bool isCloseToWall = false;
    private Animated.Animation[] animationsIdle;
    private Animated.Animation[] animationsIdleWithoutTurn;

    private const int LayEggTime = 60 * 4;
    private const float FlySpeed = 0.0625f;
    private const float TakeFlightSpeed = 0.12f;
    private const float MaxPlayerVerticalDistance = 3f;
    private const float MaxPlayerHorizontalDistance = 10f;
    private const float ShakeSpeed = 1.50f;
    private const float ShakeAmount = 0.03f;

    public enum State
    {
        None,
        Stand,
        TakeFlight,
        Fly,
        LayEgg
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.facingRight = !def.tile.flip;

        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };

        var nearestPathNode = this.chunk.pathLayer.NearestNodeTo(this.position);
        if (nearestPathNode != null &&
            Vector2.Distance(this.position, nearestPathNode.Position) < 1f)
        {
            this.path = nearestPathNode.path;
            if (this.path.nodes.Length < 2)
            {
                this.path = null;
            }
            else
            {
                this.targetFlightPosition = this.path.nodes[1].Position;
            }
        }

        this.animationsIdle = new Animated.Animation[]
        {
            this.animationIdleBlink,
            this.animationIdleBlink,
            this.animationIdleBlink,
            this.animationIdleTurnIn,
            this.animationIdlePeck
        };

        this.animationsIdleWithoutTurn = new Animated.Animation[]
        {
            this.animationIdleBlink,
            this.animationIdleBlink,
            this.animationIdleBlink,
            this.animationIdlePeck
        };
    }

    private bool IsOnGround()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        return raycast.Vertical(this.position, false, 0.75f * 2f).anything == true;
    }

    public override void Advance()
    {
        if(this.state == State.None)
        {
            ChangeState(IsOnGround() == true ? State.Stand : State.Fly);
        }
        
        AdvanceStates();
        EnemyHitDetectionAgainstPlayer();

        this.position += this.velocity;
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

        if(this.state == State.Fly)
        {
            this.animated.OnFrame(3, () =>
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyTerrorgeonFlap,
                    position: this.position
                );
            });
        }
    }

    private void AdvanceStates()
    {
        switch(this.state)
        {
            case State.Stand:
                AdvanceStandState();
            break;

            case State.TakeFlight:
                AdvanceTakeFlightState();
            break;

            case State.Fly:
                AdvanceFlyState();
            break;

            case State.LayEgg:
                AdvanceLayEggState();
            break;
        }
    }

    private void AdvanceStandState()
    {
        if(IsPlayerClose())
        {
            ChangeState(State.TakeFlight);
        }
    }

    private bool IsPlayerClose()
    {
        var playerRelative = this.map.player.position - this.position;

        if(this.map.player.alive == false)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        return true;
    }

    private void AdvanceTakeFlightState()
    {
        this.currentSpeed = Util.Slide(this.currentSpeed, TakeFlightSpeed, 0.008f);

        Vector2 heading = this.targetFlightPosition.Value - this.position;
        float distance = heading.magnitude;
        Vector2 direction = heading / distance;

        this.velocity = direction * this.currentSpeed;

        bool reachTarget =
            Vector2.Distance(this.position, this.targetFlightPosition.Value) < 0.1f;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        bool ceilingFound = raycast.Vertical(this.position, true, 0.75f * 2f).anything;

        float maxDistance = 1.50f;
        bool wallFound = this.facingRight ? 
                raycast.Horizontal(this.position, true, maxDistance).anything == true :
                raycast.Horizontal(this.position, false, maxDistance).anything == true;

        if(reachTarget == true || ceilingFound == true|| wallFound == true)
        {
            ChangeState(State.Fly);
        }
    }

    private void AdvanceFlyState()
    {
        FlyBackAndForth();
        
        if(this.layEggTimer > 0 && this.isTurning == false && this.isCloseToWall == false)
        {
            this.layEggTimer -= 1;

            if(this.layEggTimer <= 0)
            {
                ChangeState(State.LayEgg);
            }
        }
    }

    private void FlyBackAndForth()
    {
        void Turn()
        {
            this.isTurning = true;

            //float currentVelocityH = this.velocity.x;
            this.velocity.x = 0f;

            this.animated.PlayOnce(this.animationTurn, () => 
            {
                this.isTurning = false;
                //this.velocity.x = currentVelocityH * -1f;
                this.velocity = FlySpeed * (this.facingRight ? Vector2.right : Vector2.left);
                this.animated.PlayAndLoop(this.animationFly);
            });
        }       


        if(this.isTurning == true) return;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        float maxDistance = 1.00f;

        if (raycast.Horizontal(this.position, true, 1.5f*maxDistance).anything == true || raycast.Horizontal(this.position, false, 1.5f*maxDistance).anything == true)
        {
            this.isCloseToWall = true;
        }
        else
        {
            this.isCloseToWall = false;
        }


        if (this.facingRight == true)
        {
            if (raycast.Horizontal(this.position, true, maxDistance).anything == true)
            {
                this.facingRight = false;
                Turn();
            }
        }
        else
        {
            if (raycast.Horizontal(this.position, false, maxDistance).anything == true)
            {
                this.facingRight = true;
                Turn();
            }
        }
    }

    private void AdvanceLayEggState()
    {
        // sweat particles
        if (this.map.frameNumber % 10 == 0)
        {
            var center = this.position;

            var up = Vector2.up;
            var forward = Vector2.right;
            var ahead = forward * (this.facingRight ? 1 : -1);

            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationRunTooFastSweat,
                center +
                    (up * UnityEngine.Random.Range(1.25f, 1.625f)) +
                    (ahead * UnityEngine.Random.Range(0, 1)),
                this.transform.parent
            );

            if (this.map.frameNumber % 20 == 0)
            {
                p.Throw(
                    ((ahead * 1.3f) + (forward * 1.0f) + (up * 1.6f))
                        / 16,
                    0.5f / 16,
                    0.1f / 16,
                    ((ahead + up) * -0.1f) / 16
                );
            }
            else
            {
                p.Throw(
                    ((ahead * 0) + (forward * 0.5f) + (up * 1.6f))
                        / 16,
                    1f / 16,
                    0.1f / 16,
                    ((ahead + up) * -0.1f) / 16
                );
            }
        }

        // shake increansignly faster
        float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * ShakeAmount;
        this.position.x = this.initialPosition.x + shake;
        this.position.y = this.initialPosition.y + shake;
    }

    private void ChangeState(State newState)
    {
        switch(this.state)
        {
            case State.Fly:
                ExitFlyState();
            break;
        }

        this.state = newState;

        switch(this.state)
        {
            case State.Stand:
                EnterStandState();
            break;

            case State.TakeFlight:
                EnterTakeFlightState();
            break;

            case State.Fly:
                EnterFlyState();
            break;

            case State.LayEgg:
                EnterLayEggState();
            break;
        }
    }

    private void EnterStandState()
    {
        RestartIdleAnimation();
    }

    private Animated.Animation GetRandomIdleAnimation(bool avoidTurn = false)
    {
        if(avoidTurn == true)
        {
            return this.animationsIdleWithoutTurn[Random.Range(0, this.animationsIdleWithoutTurn.Length)];
        }

        return this.animationsIdle[Random.Range(0, this.animationsIdle.Length)];
    }

    private void RestartIdleAnimation(bool avoidTurn = false)
    {
        Animated.Animation idleAnimation = GetRandomIdleAnimation(avoidTurn);
        bool isAnimationIdleTurn = (idleAnimation == this.animationIdleTurnIn);
        this.animated.PlayOnce(idleAnimation, () =>
        {
            if(isAnimationIdleTurn == true)
            {
                NextIdleTurnAnimation();
            }
            else
            {
                NextIdleAnimation();
            }
        });
    }

    private void NextIdleAnimation()
    {
        float randomRoll = UnityEngine.Random.value;

        if(randomRoll < 0.40f)
        {
            this.animated.PlayOnce(this.animationIdleStill, NextIdleAnimation);
            return;
        }

        RestartIdleAnimation(avoidTurn: false);
    }

    private void NextIdleTurnAnimation()
    {
        float randomRoll = UnityEngine.Random.value;

        if(randomRoll < 0.20f)
        {
            this.animated.PlayOnce(this.animationIdleTurnBlink, NextIdleTurnAnimation);
            return;
        }

        if(randomRoll < 0.45f)
        {
            this.animated.PlayOnce(this.animationIdleTurnStill, NextIdleTurnAnimation);
            return;
        }

        this.animated.PlayOnce(this.animationIdleTurnOut, () => RestartIdleAnimation(avoidTurn: true));
    }

    private void EnterTakeFlightState()
    {
        this.animated.PlayAndLoop(this.animationTakeFlight);

        if(this.targetFlightPosition == null)
        {
            this.targetFlightPosition = this.position.Add(3f, 6f);
        }
        this.facingRight = this.targetFlightPosition.Value.x > this.position.x;

        CreateFeatherParticles(this.position, 8);
    }

    private void EnterFlyState()
    {
        this.animated.PlayAndLoop(this.animationFly);
        this.layEggTimer = LayEggTime;
        this.velocity = FlySpeed * (this.facingRight ? Vector2.right : Vector2.left);
    }

    private void ExitFlyState()
    {
        this.isTurning = false;
    }

    private void EnterLayEggState()
    {
        this.animated.PlayOnce(this.animationLayEgg, () => { ChangeState(State.Fly); });
        this.velocity = Vector2.zero;
        this.initialPosition = this.position;

        this.animated.OnFrame(3, CreateEgg);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyTerrorgeonEgg,
            position: this.position
        );
    }

    private void CreateEgg()
    {
        float eggPosX = this.position.x + 0.50f * (this.facingRight ? 1f : -1f);
        float eggPosY = this.position.y;
        Vector2 eggPosition = new Vector2(eggPosX, eggPosY);

        var egg = PigeonEgg.Create(
            eggPosition, this.chunk
        );
        egg.bird = this;
        egg.facingRight = this.facingRight;
        
        CreateFeatherParticles(eggPosition.Add(0f, -0.50f), 4, 3f);
    }
    
    private void CreateFeatherParticles(Vector2 position, int count, float animationSpeed = 1f)
    {
        for (var n = 0; n < count; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.animationFeathers,
                position + Random.insideUnitCircle * 0.75f,
                this.transform.parent
            );
            p.animated.playbackSpeed = animationSpeed;
            p.animated.frameNumber = UnityEngine.Random.Range(0, 5);
            p.velocity = new Vector2(
                UnityEngine.Random.Range(-0.1f,  0.1f),
                UnityEngine.Random.Range(-0.08f, -0.02f)
            );
            p.acceleration = new Vector2(-0.001f, -0.001f);
        }
    }

    public override void Hit(KillInfo killInfo)
    {
        base.Hit(killInfo);
        CreateFeatherParticles(this.position, 12);
    }
}
