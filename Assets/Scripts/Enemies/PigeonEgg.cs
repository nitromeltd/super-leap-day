
using UnityEngine;
using System;

public class PigeonEgg : Item
{
    public Pipe insidePipe;
    public Pipe.TrackingData pipeTrackingData;

    [HideInInspector] public Vector2 velocity;
    private Vector3 targetScale;
    private Vector3 scaleVelocity;
    [HideInInspector]public bool facingRight;
    private bool shotByPipe = false;
    private float currentSpinSpeed;

    private const float Gravity = 0.012f;
    private const float Size = 0.50f;
    private const float DefaultSpinSpeed = 1.50f;
    private const float PipeSpinSpeed = 12.50f;

    public static PigeonEgg Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(
            Assets.instance.capitalHighway.terrorgeonEgg, chunk.transform
        );
        obj.name = "Spawned Terrorgeon Egg";
        var pigeonEgg = obj.GetComponent<PigeonEgg>();
        pigeonEgg.Init(position, chunk);
        chunk.items.Add(pigeonEgg);
        return pigeonEgg;
    }

    public Sprite[] shellPieceSprites;

    [NonSerialized] public Pigeon bird;

    public override void Init(Def def) => throw new System.NotImplementedException();

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-Size, Size, -Size, Size, this.rotation, Hitbox.SolidOnTop)
        };
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.transform.localScale = Vector3.zero;
        this.targetScale = Vector3.one;
        this.currentSpinSpeed = DefaultSpinSpeed;
    }

    public override void Advance()
    {
        if (this.insidePipe != null)
        {
            this.currentSpinSpeed = PipeSpinSpeed;
            AdvanceInsidePipe();
        }
        else
        {
            AdvanceOutsidePipe();
        }
        
        // Rotation
        float spin = this.currentSpinSpeed * (facingRight ? 1f : -1f);
        this.transform.localRotation *= Quaternion.Euler(0, 0, spin);

        // Scale
        Vector3 absLocalScale = new Vector3(
            Mathf.Abs(this.transform.localScale.x),
            Mathf.Abs(this.transform.localScale.y),
            Mathf.Abs(this.transform.localScale.z));

        Vector3 currentScale =
            Vector3.SmoothDamp(absLocalScale, this.targetScale, ref scaleVelocity, 0.1f);

        this.transform.localScale = new Vector3(
            currentScale.x * (facingRight ? 1f : -1f),
            currentScale.y,
            currentScale.z
        );

        LimitPositionToChunk();
        this.transform.position = this.position;
    }

    private void LimitPositionToChunk()
    {
        if(this.position.y >= this.chunk.yMax + 1.5f ||
            this.position.y <= this.chunk.yMin + 0.5f ||
            this.position.x >= this.chunk.xMax ||
            this.position.x <= this.chunk.xMin)
        {
            Collide(this.position);
        }
    }

    private void AdvanceInsidePipe()
    {
        this.insidePipe.AdvancePigeonEgg(this);
        this.spriteRenderer.sortingLayerName = "Spikes";
        this.spriteRenderer.sortingOrder = -10;
    }

    private void AdvanceOutsidePipe()
    {
        if(this.shotByPipe == false)
        {
            this.velocity.y -= Gravity;
        }

        var raycast = new Raycast(
            this.map,
            this.position,
            ignoreItem1: this.bird,
            ignoreItemType: typeof(PigeonEgg),
            isCastByEnemy: true
        );

        Raycast.CombinedResults r = Raycast.CombineClosest(
            raycast.Vertical(this.position, false, -this.velocity.y + 0.75f),
            raycast.Vertical(this.position, true, this.velocity.y + 0.75f),
            raycast.Horizontal(this.position, true, this.velocity.x + 0.75f),
            raycast.Horizontal(this.position, false, -this.velocity.x + 0.75f)
        );

        if (r.anything == true)
        {
            Collide(r.End());
        }
        else
        {
            this.position += this.velocity;

            if (this.map.player.Rectangle().Overlaps(
                this.hitboxes[0].InWorldSpace()
            ) == true)
            {
                this.map.player.Hit(this.position);
            }
        }
    }

    private void Collide(Vector2 collisionPoint)
    {
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);

        foreach (var sprite in this.shellPieceSprites)
        {
            Vector2 shellPosition = this.position +
                (UnityEngine.Random.insideUnitCircle * Size * 2f) + (Vector2.up * -0.33f);
            var p = Particle.CreateWithSprite(
                sprite, 100, shellPosition, this.transform.parent
            );
            p.velocity = new Vector2(
                UnityEngine.Random.Range(-0.15f, 0.15f),
                UnityEngine.Random.Range(0.15f, 0.275f)
            );
            p.acceleration = new Vector2(0, -0.03f);
        }

        Particle.CreateDust(
            this.transform.parent,
            3,
            5,
            new Vector2(this.position.x, collisionPoint.y),
            variationX: 0.625f
        );

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyTerrorgeoneEggBreak,
            position: this.position
        );
    }

    public void EnterPipe()
    {
        this.currentSpinSpeed = DefaultSpinSpeed;
    }

    public void ExitPipe()
    {
        this.insidePipe = null;
        this.shotByPipe = true;
    }
}
