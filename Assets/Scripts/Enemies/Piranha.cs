using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Piranha : Enemy
{
    public Animated.Animation animationSwim;
    public Animated.Animation animationTurn;
    public Animated.Animation animationBite;
    public Animated.Animation animationCharge;
    public Animated.Animation animationStranded;
    public Animated.Animation animationDrop;
    public Animated.Animation animationWallSlam;
    public Animated.Animation animationAlerted;

    public enum State
    {
        // outside of water
        OutOfWater,
        ComingUpWater,

        // inside water
        PatrollingInWater,
        AlertPlayerInWater,
        ChasingPlayerInWater,
        HitWallInWater,
        LostPlayerInWater,
        ReturningToMoveInWater
    }
    
    public State state;
    private Vector2 velocity;
    private bool insideWater;
    private Vector2 originPosition;
    private float startDistance;
    private float currentAngleDegrees;
    private float targetAngleDegrees;
    [HideInInspector] private NitromeEditor.Path path;
    private bool facingRight;
    private bool upsideDown;
    private int currentPathIndex;
    private int alertTimer;
    private int lostTimer;

    private const float GravityAcceleration = 0.006f;
    private const float MinDistanceToChasePlayer = 8f;
    private const float MaxDistanceToChasePlayer = 12f;
    private const float MaxDistanceToMovePosition = 12f;
    private const float MoveSpeed = 0.075f;
    private const float ChasePlayerSpeed = 0.175f;
    private const float MaxPlayerVerticalDistance = 4;
    private const float MaxPlayerHorizontalDistance = 6;
    private const int AlertTime = 15;
    private const int LostTime = 25;

    private static Vector2 HitboxSize = new Vector2(0.75f, 0.45f);

    private bool isAlive = false;
    private bool isTurning = false;

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.originPosition = this.position;
        this.facingRight = true;
        this.upsideDown = false;

        this.hitboxes = new[] {
            new Hitbox(
                -HitboxSize.x, HitboxSize.x, -HitboxSize.y, HitboxSize.y,
                this.rotation, Hitbox.NonSolid
            )
        };

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.isAlive = false;
        ChangeState(State.OutOfWater);
        this.spriteRenderer.enabled = false;
    }

    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        
        AdvanceWater();

        switch (state)
        {
            case State.OutOfWater:
                AdvanceOutOfWater();
                break;

            case State.PatrollingInWater:
                AdvancePatrollingInWater();
                break;

            case State.AlertPlayerInWater:
                AdvanceAlertPlayerInWater();
                break;

            case State.ChasingPlayerInWater:
                AdvanceChasingPlayerInWater();
                break;

            case State.HitWallInWater:
                AdvanceHitWallInWater();
                break;

            case State.LostPlayerInWater:
                AdvanceLostPlayerInWater();
                break;

            case State.ReturningToMoveInWater:
                AdvanceReturningToMoveInWater();
                break;

            case State.ComingUpWater:
                AdvanceComingUpWater();
                break;
        }

        if(this.isAlive == true && this.state != State.ComingUpWater)
        {
            EnemyHitDetectionAgainstPlayer();
        }
        Movement();

        //this.position += this.velocity;
        this.transform.position = this.position;
        
        if (this.targetAngleDegrees < this.currentAngleDegrees - 180)
            this.currentAngleDegrees -= 360;
        if (this.targetAngleDegrees > this.currentAngleDegrees + 180)
            this.currentAngleDegrees += 360;

        this.currentAngleDegrees = Util.Slide(this.currentAngleDegrees, this.targetAngleDegrees, 12);
        this.upsideDown = Mathf.Abs(this.currentAngleDegrees) > 90f;
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.currentAngleDegrees);
        this.transform.localScale = new Vector3(
            1f,
            this.upsideDown ? -1f : 1f,
            1f
        );
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    private void AdvanceOutOfWater()
    {
        this.targetAngleDegrees = this.facingRight == true ? 0f : 179f;
        if (this.insideWater == true)
        {
            ChangeState(State.ComingUpWater);
        }
    }

    private float GetAngleDegreesFromDirection(Vector2 direction)
    {
        float angleDegrees = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        
        if (Mathf.Abs((angleDegrees % 90) - 45) < 5)
            angleDegrees = Mathf.Round(angleDegrees / 45) * 45;

        return angleDegrees;
    }

    private void AdvancePatrollingInWater()
    {
        if(this.isTurning == false)
        {
            this.velocity = MoveSpeed * (facingRight ? Vector2.right : Vector2.left);
            this.targetAngleDegrees = this.facingRight == true ? 0f : 179f;
        }


        var originRelative = this.originPosition - this.position;
        bool isNearOriginHeight = Mathf.Abs(originRelative.y) < 1f;
        
        if(this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
            this.isTurning = false;
        }
        else if(IsDetectingPlayer())
        {
            ChangeState(State.AlertPlayerInWater);
            this.isTurning = false;
        }
        else if(isNearOriginHeight == false && CanReturnToOrigin())
        {
             ChangeState(State.ReturningToMoveInWater);
            this.isTurning = false;
        }

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemyPiranhaSwimLoop))
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyPiranhaSwimLoop,
                position: this.position,
                options: new Audio.Options(0.5f, false, 0)
            );
        }
    }

    private bool IsDetectingPlayer()
    {
        Player player = this.map.player;
        var playerRelative = player.position - this.position;

        if(player.alive == false)
        {
            return false;
        }
        if(player.IsInsideWater == false)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        Raycast raycast = new Raycast(this.map, this.position);
        Raycast.Result result = raycast.Arbitrary(this.position, playerRelative.normalized, playerRelative.magnitude);
        if (result.anything == true)
        {
            return false;
        }

        /*if(IsChargingToSameDirectionAgain()
            && this.chargeCooldownTime > 0)
        {
            return false;
        }*/

        return true;
    }

    private void AdvanceAlertPlayerInWater()
    {
        Player player = this.map.player;
        bool isPlayerToTheRight = player.position.x > this.position.x;
        this.facingRight = isPlayerToTheRight;
        this.targetAngleDegrees = this.facingRight == true ? 0f : 179f;

        this.alertTimer += 1;

        if(this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
        }
        else if(this.alertTimer >= AlertTime)
        {
            this.alertTimer = 0;
            ChangeState(State.ChasingPlayerInWater);
        }
    }

    private const float HeightDeviationLimit = 0.50f; 

    private void AdvanceChasingPlayerInWater()
    {
        Player player = this.map.player;
        Vector2 playerPosition = player.position;
        Vector2 directionToPlayer = (playerPosition - this.position).normalized;

        directionToPlayer.y = Mathf.Clamp(directionToPlayer.y, -0.25f, 0.25f);

        if(player.IsInsideWater == false || player.alive == false)
        {
            directionToPlayer.y = 0f;
        }

        Vector2 originRelative = this.originPosition - this.position;
        float currentHeightDeviation = Mathf.InverseLerp(
            0, HeightDeviationLimit, originRelative.y
        );

        this.velocity.x = ChasePlayerSpeed * (facingRight ? 1f : -1f);
        this.velocity.y = ChasePlayerSpeed * directionToPlayer.y * (1 - currentHeightDeviation);

        this.targetAngleDegrees = GetAngleDegreesFromDirection(this.velocity.normalized);
        
        if(this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
        }
    }

    private void AdvanceHitWallInWater()
    {
        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.01f);
        this.velocity.y = Util.Slide(this.velocity.y, 0f, 0.01f);
    }

    private void AdvanceLostPlayerInWater()
    {
        this.lostTimer += 1;
        this.targetAngleDegrees = this.facingRight == true ? 0f : 179f;

        if (this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
        }
        else if(this.lostTimer >= LostTime)
        {
            this.lostTimer = 0;
            ChangeState(State.ReturningToMoveInWater);
        }
        else if(IsDetectingPlayer())
        {
            ChangeState(State.AlertPlayerInWater);
        }
    }

    private void AdvanceComingUpWater()
    {
        float distanceToLatestMove = Vector2.Distance(this.position, this.originPosition);
        Vector2 directionToOrigin = (this.originPosition - this.position).normalized;

        bool shouldStartPatrolling = false;

        if (distanceToLatestMove < MoveSpeed * 2)
        {
            shouldStartPatrolling = true;
        }

        if (this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
        }
        else if (shouldStartPatrolling == true)
        {
            ChangeState(State.PatrollingInWater);
        }
        else
        {
            if (distanceToLatestMove < startDistance / 4)
            {
                this.velocity = directionToOrigin * MoveSpeed * (2f - (startDistance - (4 * distanceToLatestMove)) / startDistance);
            }
            else
            {
                this.velocity = directionToOrigin * MoveSpeed * 2f;
            }
            this.targetAngleDegrees = GetAngleDegreesFromDirection(directionToOrigin);
            this.facingRight = directionToOrigin.x < 0f;
        }
    }

    private void AdvanceReturningToMoveInWater()
    {
        float distanceToLatestMove = Vector2.Distance(this.position, this.originPosition);
        Vector2 directionToOrigin = (this.originPosition - this.position).normalized;

        bool shouldStartPatrolling = false;

        if(CanReturnToOrigin() == false ||
            distanceToLatestMove < MoveSpeed * 2)
        {
            shouldStartPatrolling = true;
        }

        if(this.insideWater == false)
        {
            ChangeState(State.OutOfWater);
        }
        else if(shouldStartPatrolling == true)
        {
            ChangeState(State.PatrollingInWater);
        }
        else if(IsDetectingPlayer())
        {
            ChangeState(State.AlertPlayerInWater);
        }
        else
        {
            this.velocity = directionToOrigin * MoveSpeed;
            this.targetAngleDegrees = GetAngleDegreesFromDirection(directionToOrigin);
            this.facingRight = directionToOrigin.x < 0f;
        }
    }

    private bool CanReturnToOrigin()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        float distanceToLatestMove = Vector2.Distance(this.position, this.originPosition);
        Vector2 directionToOrigin = (this.originPosition - this.position).normalized;

        var r = raycast.Arbitrary(this.position, directionToOrigin, distanceToLatestMove);

        return r.anything == false &&
            this.originPosition.y < this.map.waterline.currentLevel;
    }

    private void Movement()
    {
        if (this.isTurning == true) return;
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                if(this.state == State.PatrollingInWater)
                {
                    this.position.x += result.distance - rect.xMax;
                    this.isTurning = true;
                    this.animated.PlayOnce(this.animationTurn, () =>
                    {
                        this.animated.PlayAndLoop(this.animationSwim);
                        this.facingRight = !this.facingRight;
                        this.currentAngleDegrees = this.facingRight == true ? 0f : 179f;
                        this.upsideDown = Mathf.Abs(this.currentAngleDegrees) > 90f;
                        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.currentAngleDegrees);
                        this.transform.localScale = new Vector3(
                            1f,
                            this.upsideDown ? -1f : 1f,
                            1f
                        );
                        this.isTurning = false;
                    });
                }
                else if(this.state == State.ChasingPlayerInWater)
                {
                    ChangeState(State.HitWallInWater);
                }
                else if(this.state == State.ComingUpWater)
                {
                    this.position.x += this.velocity.x;
                }
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                if(this.state == State.PatrollingInWater)
                {
                    this.position.x += -result.distance - rect.xMin;
                    this.isTurning = true;
                    this.animated.PlayOnce(this.animationTurn, () =>
                    {
                        this.animated.PlayAndLoop(this.animationSwim);
                        this.facingRight = !this.facingRight;
                        this.currentAngleDegrees = this.facingRight == true ? 0f : 179f;
                        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.currentAngleDegrees);
                        this.upsideDown = Mathf.Abs(this.currentAngleDegrees) > 90f;
                        this.transform.localScale = new Vector3(
                            1f,
                            this.upsideDown ? -1f : 1f,
                            1f
                        );
                        this.isTurning = false;
                    });
                }
                else if(this.state == State.ChasingPlayerInWater)
                {
                    ChangeState(State.HitWallInWater);
                }
                else if (this.state == State.ComingUpWater)
                {
                    this.position.x += this.velocity.x;
                }
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), true, maxDistance)
            );

            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax ||
                (result.End().y > this.map.waterline.currentLevel))
            {
                if (this.state == State.ComingUpWater)
                {
                    this.position.y += this.velocity.y;
                }
                else
                {
                    this.position.y += result.distance - rect.yMax;
                    this.velocity.y = 0;
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                if (this.state == State.ComingUpWater)
                {
                    this.position.y += this.velocity.y;
                }
                else
                {
                    this.position.y += -result.distance - rect.yMin;
                    this.velocity.y = 0;
                }

            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private void ShowExclamationMark()
    {
        var exclamationMark = Particle.CreateAndPlayOnce(
            Assets.instance.exclamationMarkAnimation,
            ((Vector2)this.transform.position).Add(
                0,
                2f
            ),
            this.transform.parent
        );
        exclamationMark.spriteRenderer.sortingLayerName = "Items (Front)";
        exclamationMark.spriteRenderer.sortingOrder = -1;
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.OutOfWater:
                EnterOutOfWater();
                break;

            case State.PatrollingInWater:
                EnterPatrollingInWater();
                break;

            case State.AlertPlayerInWater:
                EnterAlertPlayerInWater();
                break;

            case State.ChasingPlayerInWater:
                EnterChasingPlayerInWater();
                break;

            case State.HitWallInWater:
                EnterHitWallInWater();
                break;

            case State.LostPlayerInWater:
                EnterLostPlayerInWater();
                break;

            case State.ReturningToMoveInWater:
                EnterReturningToMoveInWater();
                break;

            case State.ComingUpWater:
                EnterComingUpWater();
                break;
        }
    }

    private void EnterOutOfWater()
    {
        this.animated.PlayAndLoop(this.animationStranded);

        if(this.isAlive == true)
        {
            this.isAlive = false;
            this.spriteRenderer.enabled = false;

            foreach (var debris in this.deathAnimation.debris)
            {
                var p = Particle.CreateWithSprite(
                    debris, 100, this.position, this.transform.parent
                );
                p.Throw(new Vector2(0, 0.5f), 0.2f, 0.1f, new Vector2(0, -0.025f));

                p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
                if (UnityEngine.Random.Range(0, 2) == 0)
                    p.spin *= -1;
            }
        }

        this.position = this.originPosition;
    }

    private void EnterComingUpWater()
    {
        this.animated.PlayAndLoop(this.animationSwim);
        this.velocity = Vector2.zero;
        this.isAlive = true;
        this.spriteRenderer.enabled = true;

        if(this.originPosition.y < Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, 0)).y)
        {
            this.position = originPosition - new Vector2(0, Camera.main.orthographicSize + 1);
        }
        else
        {
            this.position = new Vector2(originPosition.x, Camera.main.ViewportToWorldPoint(new Vector3(0f, -0.05f, 0)).y);
        }
        this.startDistance = Vector2.Distance(this.position, this.originPosition);
    }

    private void EnterPatrollingInWater()
    {
        this.animated.PlayAndLoop(this.animationSwim);
    }

    private void EnterAlertPlayerInWater()
    {
        this.animated.PlayAndLoop(this.animationAlerted);
        ShowExclamationMark();
        this.alertTimer = 0;
        this.velocity = Vector2.zero;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyPiranhaAggro,
            position: this.position
        );

        Audio.instance.StopSfx(Assets.instance.sfxEnemyPiranhaSwimLoop);
    }

    private void EnterChasingPlayerInWater()
    {
        this.animated.PlayAndLoop(this.animationCharge);
    }

    private void EnterHitWallInWater()
    {
        this.velocity.x = -this.velocity.x * 0.75f;
        this.velocity.y = 0f;
        
        this.map.ScreenShakeAtPosition(this.position, new Vector2(.3f, 0.5f));

        this.animated.PlayOnce(this.animationWallSlam, 
            () => { ChangeState(State.LostPlayerInWater); }
        );

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyPiranhaWall,
            position: this.position
        );
    }

    private void EnterLostPlayerInWater()
    {
        this.animated.PlayAndLoop(this.animationAlerted);
        this.lostTimer = 0;
        this.velocity = Vector2.zero;
        Audio.instance.StopSfx(Assets.instance.sfxEnemyPiranhaSwimLoop);
    }

    private void EnterReturningToMoveInWater()
    {
        this.animated.PlayAndLoop(this.animationSwim);
        this.velocity = Vector2.zero;
        Audio.instance.StopSfx(Assets.instance.sfxEnemyPiranhaSwimLoop);
    }
}
