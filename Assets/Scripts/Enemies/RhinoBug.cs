
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RhinoBug : Enemy
{
    private enum Move
    {
        WalkUp,
        WalkDown,
        AttackUp,
        AttackDown,
        FlyLeft,
        FlyRight
    };

    private Move move;
    private Bamboo bambooFrom;
    private Bamboo bambooTo;
    private bool onRightSide = false;
    private bool behind = false;

    private Animated dustSlideGoingDown;
    private Animated dustSlideGoingUp;
    private Animated scrape;
    private Coroutine currentCoroutine;

    public Animated.Animation animationTakeOff;
    public Animated.Animation animationFly;
    public Animated.Animation animationLand;
    public Animated.Animation animationRotate;
    public Animated.Animation animationWalkUp;
    public Animated.Animation animationWalkDown;
    public Animated.Animation animationPreattackUp;
    public Animated.Animation animationAttackUp;
    public Animated.Animation animationPreattackDown;
    public Animated.Animation animationAttackDown;
    public Animated.Animation animationSwitchSides;
    public Animated.Animation animationDustSlide;
    public Animated.Animation animationScrape;

    private const float WalkOffsetX = 1;

    public override void Init(Def def)
    {
        base.Init(def);

        this.dustSlideGoingDown =
            this.transform.Find("Dust Slide Going Down").GetComponent<Animated>();
        this.dustSlideGoingDown.gameObject.SetActive(false);
        this.dustSlideGoingUp =
            this.transform.Find("Dust Slide Going Up").GetComponent<Animated>();
        this.dustSlideGoingUp.gameObject.SetActive(false);
        this.scrape = this.transform.Find("Scrape").GetComponent<Animated>();
        this.scrape.gameObject.SetActive(false);

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.onRightSide = !def.tile.flip;

        this.bambooFrom =
            this.chunk.NearestItemTo<Bamboo>(this.position, Tile.Size * 1.5f);

        if (this.onRightSide == true)
            this.position.x = this.bambooFrom.position.x + WalkOffsetX;
        else
            this.position.x = this.bambooFrom.position.x - WalkOffsetX;
        UpdateSprite();

        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };
    }

    public void OnDisable()
    {
        // disabling a gameobject kills its coroutines.
        // we'll just try to snap to the end of the current movement to simplify things

        if (this.currentCoroutine != null)
        {
            StopCoroutine(this.currentCoroutine);
            this.currentCoroutine = null;
        }

        if (this.bambooTo != null)
        {
            this.bambooFrom = this.bambooTo;
            this.position = this.bambooFrom.position;

            if (this.onRightSide == true)
                this.position.x = this.bambooFrom.position.x + WalkOffsetX;
            else
                this.position.x = this.bambooFrom.position.x - WalkOffsetX;

            UpdateSprite();
        }
    }

    public override void Reset()
    {
        if (this.currentCoroutine != null)
        {
            StopCoroutine(this.currentCoroutine);
            this.currentCoroutine = null;
        }
    }

    public override void Advance()
    {
        if (this.currentCoroutine == null)
        {            
            (this.move, this.bambooTo) = NextMove();
            StartMove();
        }

        EnemyHitDetectionAgainstPlayer();
    }

    private void StartMove()
    {
        Action onComplete = () =>
        {
            this.bambooFrom = this.bambooTo;
            this.currentCoroutine = null;
        };

        switch (this.move)
        {
            case Move.FlyLeft:
                this.currentCoroutine = StartCoroutine(FlyLeftCoroutine(onComplete));
                break;
            case Move.FlyRight:
                this.currentCoroutine = StartCoroutine(FlyRightCoroutine(onComplete));
                break;
            case Move.WalkUp:
                this.currentCoroutine = StartCoroutine(WalkCoroutine(onComplete));
                break;
            case Move.WalkDown:
                this.currentCoroutine = StartCoroutine(WalkCoroutine(onComplete));
                break;
            case Move.AttackUp:
                this.currentCoroutine = StartCoroutine(AttackUpCoroutine(onComplete));
                break;
            case Move.AttackDown:
                this.currentCoroutine = StartCoroutine(AttackDownCoroutine(onComplete));
                break;
        }
    }

    private IEnumerator FlyLeftCoroutine(Action onComplete)
    {
        if (this.onRightSide == true)
        {
            this.behind = true;
            this.position.x -= 0.75f;
            UpdateSprite();

            bool done = false;
            this.animated.PlayOnce(this.animationSwitchSides, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();

            this.behind = false;
            this.onRightSide = false;
            this.position.x -= 1.25f;
            UpdateSprite();
        }

        {
            bool done = false;
            this.animated.PlayOnce(this.animationTakeOff, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();

            this.animated.PlayAndLoop(this.animationFly);
            this.position.x -= 1f;
            UpdateSprite();
        }

        while (this.position.x > this.bambooTo.position.x + WalkOffsetX)
        {
            if (Time.deltaTime == 0)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            this.position.x -= 3.75f * Time.deltaTime;
            UpdateSprite();
            yield return new WaitForEndOfFrame();
        }

        this.onRightSide = !this.onRightSide;

        if (this.onRightSide == true)
            this.position.x = this.bambooTo.position.x + WalkOffsetX;
        else
            this.position.x = this.bambooTo.position.x - WalkOffsetX;
        UpdateSprite();

        {
            bool done = false;
            this.animated.PlayOnce(this.animationLand, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();
        }

        onComplete();
    }

    private IEnumerator FlyRightCoroutine(Action onComplete)
    {
        if (this.onRightSide == false)
        {
            this.behind = true;
            this.position.x += 0.75f;
            UpdateSprite();

            bool done = false;
            this.animated.PlayOnce(this.animationSwitchSides, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();

            this.behind = false;
            this.onRightSide = true;
            this.position.x += 1.25f;
            UpdateSprite();
        }

        {
            bool done = false;
            this.animated.PlayOnce(this.animationTakeOff, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();

            this.animated.PlayAndLoop(this.animationFly);
            this.position.x += 1f;
            UpdateSprite();
        }

        while (this.position.x < this.bambooTo.position.x - WalkOffsetX)
        {
            if (Time.deltaTime == 0)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            this.position.x += 3.75f * Time.deltaTime;
            UpdateSprite();
            yield return new WaitForEndOfFrame();
        }

        this.onRightSide = !this.onRightSide;

        if (this.onRightSide == true)
            this.position.x = this.bambooTo.position.x + WalkOffsetX;
        else
            this.position.x = this.bambooTo.position.x - WalkOffsetX;
        UpdateSprite();

        {
            bool done = false;
            this.animated.PlayOnce(this.animationLand, delegate { done = true; });
            while (!done) yield return new WaitForEndOfFrame();
        }

        onComplete();
    }

    private IEnumerator WalkCoroutine(Action onComplete)
    {
        this.animated.PlayAndLoop(this.animationWalkUp);

        while (Mathf.Approximately(this.position.y, this.bambooTo.position.y) == false)
        {
            if (Time.deltaTime == 0)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            var lastPositionY = this.position.y;
            this.position.y = Util.Slide(
                this.position.y, this.bambooTo.position.y, 3.75f * Time.deltaTime
            );

            var bamboo = this.chunk.ItemAt<Bamboo>(
                this.bambooFrom.ItemDef.tx,
                Mathf.FloorToInt(this.position.y / Tile.Size)
            );
            if (bamboo?.twistyBottom == true)
            {
                var isAbove = this.position.y > bamboo.position.y;
                var wasAbove = lastPositionY > bamboo.position.y;
                if (isAbove != wasAbove)
                    this.onRightSide = !this.onRightSide;
            }

            var twisting = false;

            if (bamboo?.twisty == true &&
                bamboo.twistyTop == false &&
                (bamboo.twistyBottom == false || this.position.y > bamboo.position.y))
            {
                var dy = this.position.y - bamboo.position.y;
                var through = (dy - (-0.5f)) / (0.5f - (-0.5f));
                var frame = Mathf.FloorToInt(
                    through * this.animationRotate.sprites.Length
                );

                this.animated.Stop();
                this.spriteRenderer.sprite = this.animationRotate.sprites[frame];
                this.behind = frame >= 7;
                twisting = true;
            }
            else
            {
                if (this.position.y < this.bambooTo.position.y)
                    this.animated.PlayAndLoop(this.animationWalkUp);
                else
                    this.animated.PlayAndLoop(this.animationWalkDown);
                this.behind = false;
            }

            if (twisting == true)
                this.position.x = this.bambooTo.position.x;
            else if (this.onRightSide == true)
                this.position.x = this.bambooTo.position.x + WalkOffsetX;
            else
                this.position.x = this.bambooTo.position.x - WalkOffsetX;

            UpdateSprite();
            yield return new WaitForEndOfFrame();
        }

        this.behind = false;
        UpdateSprite();

        onComplete();
    }

    private IEnumerator AttackUpCoroutine(Action onComplete)
    {
        {
            bool done = false;
            Audio.instance.PlaySfx(Assets.instance.sfxRhinoBugHoofScrape, position: this.position);
            this.animated.PlayOnce(
                this.animationPreattackUp, delegate { done = true; }
            );
            this.scrape.gameObject.SetActive(true);
            this.scrape.PlayOnce(this.animationScrape, delegate
            {
                this.scrape.PlayOnce(this.animationScrape, delegate
                {
                    this.scrape.gameObject.SetActive(false);
                });
            });
            while (!done) yield return new WaitForEndOfFrame();
        }

        this.animated.PlayAndLoop(this.animationAttackUp);
        this.dustSlideGoingUp.gameObject.SetActive(true);
        this.dustSlideGoingUp.PlayAndLoop(this.animationDustSlide);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;

        while (this.position.y < this.bambooTo.position.y)
        {
            if (Time.deltaTime == 0)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            this.position.y += 16.875f * Time.deltaTime;
            UpdateSprite();
            yield return new WaitForEndOfFrame();
        }

        this.whenHitFromAbove = HitFromAboveBehaviour.Die;

        this.dustSlideGoingUp.Stop();
        this.dustSlideGoingUp.gameObject.SetActive(false);

        {
            this.animated.PlayOnce(this.animationLand, null);
            yield return new WaitForSeconds(0.3f);
        }

        onComplete();
    }

    private IEnumerator AttackDownCoroutine(Action onComplete)
    {
        {
            bool done = false;
            Audio.instance.PlaySfx(Assets.instance.sfxRhinoBugSpikeOut, position: this.position);
            this.animated.PlayOnce(
                this.animationPreattackDown, delegate { done = true; }
            );
            while (!done) yield return new WaitForEndOfFrame();
        }

        this.animated.PlayAndLoop(this.animationAttackDown);
        this.dustSlideGoingDown.gameObject.SetActive(true);
        this.dustSlideGoingDown.PlayAndLoop(this.animationDustSlide);

        while (this.position.y > this.bambooTo.position.y) 
        {
            if (Time.deltaTime == 0)
            {
                yield return new WaitForEndOfFrame();
                continue;
            }

            this.position.y -= 16.875f * Time.deltaTime;
            UpdateSprite();
            yield return new WaitForEndOfFrame();
        }

        this.dustSlideGoingDown.Stop();
        this.dustSlideGoingDown.gameObject.SetActive(false);

        {
            this.animated.PlayOnce(this.animationLand, null);
            yield return new WaitForSeconds(0.3f);
        }

        onComplete();
    }

    private void UpdateSprite()
    {
        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.onRightSide ? 1 : -1, 1, 1);
        if (this.behind == true)
        {
            this.spriteRenderer.sortingLayerName = "Spikes";
            this.spriteRenderer.sortingOrder = -10;
        }
        else
        {
            this.spriteRenderer.sortingLayerName = "Enemies";
            this.spriteRenderer.sortingOrder = 0;
        }
    }

    Bamboo FindBambooOnRight(int fromTx, int fromTy)
    {
        for (var tx = fromTx + 1; tx <= this.chunk.xMax; tx += 1)
        {
            if(this.chunk.TileAt(tx, fromTy, Layer.A) != null)
                break;
                
            var bamboo = this.chunk.ItemAt<Bamboo>(tx, fromTy);
            if (bamboo != null)
                return bamboo;
        }
        return null;
    }
    Bamboo FindBambooOnLeft(int fromTx, int fromTy)
    {
        for (var tx = fromTx - 1; tx >= this.chunk.xMin; tx -= 1)
        {
            if(this.chunk.TileAt(tx, fromTy, Layer.A) != null)
                break;

            var bamboo = this.chunk.ItemAt<Bamboo>(tx, fromTy);
            if (bamboo != null)
                return bamboo;
        }
        return null;
    }
    Bamboo FindBottomOfBamboo(int fromTx, int fromTy)
    {
        for (var ty = fromTy - 1; ty >= this.chunk.yMin; ty -= 1)
        {
            var bamboo = this.chunk.ItemAt<Bamboo>(fromTx, ty);
            var bambooNext = this.chunk.ItemAt<Bamboo>(fromTx, ty - 1);
            if (bamboo == null) break;
            if (bamboo != null && bambooNext == null)
                return bamboo;
        }
        return null;
    }
    Bamboo FindTopOfBamboo(int fromTx, int fromTy)
    {
        for (var ty = fromTy + 1; ty <= this.chunk.yMax; ty += 1)
        {
            var bamboo = this.chunk.ItemAt<Bamboo>(fromTx, ty);
            var bambooNext = this.chunk.ItemAt<Bamboo>(fromTx, ty + 1);
            if (bamboo == null) break;
            if (bamboo != null && bambooNext == null)
                return bamboo;
        }
        return null;
    }

    private (Move, Bamboo) NextMove()
    {
        var atTx = this.bambooFrom.ItemDef.tx;
        var atTy = this.bambooFrom.ItemDef.ty;

        (Move, Bamboo)? PickMove((Move, Bamboo) one, (Move, Bamboo) two)
        {
            if (one.Item2 == null && two.Item2 == null) return null;
            if (one.Item2 == null) return two;
            if (two.Item2 == null) return one;
            return (UnityEngine.Random.Range(0, 2) == 1) ? one : two;
        }

        var top   = FindTopOfBamboo   (atTx, atTy);
        var btm   = FindBottomOfBamboo(atTx, atTy);
        var left  = FindBambooOnLeft  (atTx, atTy);
        var right = FindBambooOnRight (atTx, atTy);

        {
            // head for player if they're one move away
            var playerIsAbove = this.map.player.position.y > this.position.y;
            if (playerIsAbove == true && IsPlayerOnSameBamboo(top))
                return (Move.AttackUp, top);
            if (playerIsAbove == false && IsPlayerOnSameBamboo(btm))
                return (Move.AttackDown, btm);
            if (left != null && IsPlayerOnSameBamboo(left))
                return (Move.FlyLeft, left);
            if (right != null && IsPlayerOnSameBamboo(right))
                return (Move.FlyRight, right);
        }

        if (this.move == Move.FlyLeft || this.move == Move.FlyRight)
        {
            var vert = PickMove((Move.WalkUp, top), (Move.WalkDown, btm));
            if (vert.HasValue) return vert.Value;

            var horz = PickMove((Move.FlyLeft, left), (Move.FlyRight, right));
            return horz.Value;
        }
        else
        {
            var horz = PickMove((Move.FlyLeft, left), (Move.FlyRight, right));
            if (horz.HasValue) return horz.Value;

            var vert = PickMove((Move.WalkUp, top), (Move.WalkDown, btm));
            return vert.Value;
        }
    }

    private bool IsPlayerOnSameBamboo(Bamboo b)
    {
        if (b == null) return false;

        var playerBamboo = this.map.player.OnBamboo;
        if (playerBamboo == null) return false;
        if (playerBamboo.chunk != b.chunk) return false;
        if (playerBamboo.ItemDef.tx != b.ItemDef.tx) return false;

        if (playerBamboo.ItemDef.ty > b.ItemDef.ty)
        {
            for (
                int ty = b.ItemDef.ty;
                ty <= playerBamboo.ItemDef.ty;
                ty += 1
            )
            {
                if (this.chunk.ItemAt<Bamboo>(b.ItemDef.tx, ty) == null)
                    return false;
            }
            return true;
        }
        else
        {
            for (
                int ty = playerBamboo.ItemDef.ty;
                ty <= b.ItemDef.ty;
                ty += 1
            )
            {
                if (this.chunk.ItemAt<Bamboo>(b.ItemDef.tx, ty) == null)
                    return false;
            }
            return true;
        }
    }
}
