
using UnityEngine;
using System;

public class RobotCopEnemy : Enemy
{
    private bool facingRight = true;
    private Player.Orientation orientation = Player.Orientation.Normal;
    private float angle = 0;
    private float groundspeed = 0;
    private bool onGround = false;
    [NonSerialized] public Vector2 velocity;
    private int excitedTime = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-1, 1, -1, 1, this.rotation, Hitbox.NonSolid)
        };
    }

    private bool IsPlayerAhead()
    {
        var player = this.map.player;
        if (Mathf.Abs(player.position.y - this.position.y) < 2 &&
            this.facingRight == (player.position.x > this.position.x))
        {
            return true;
        }
        return false;
    }

    private bool IsValidOrientationWithDiagonalAngle(
        Player.Orientation orientation, float angle
    )
    {
        if (angle == 45)
            return
                orientation == Player.Orientation.Normal ||
                orientation == Player.Orientation.RightWall;
        else if (angle == 135)
            return
                orientation == Player.Orientation.RightWall ||
                orientation == Player.Orientation.Ceiling;
        else if (angle == 225)
            return
                orientation == Player.Orientation.Ceiling ||
                orientation == Player.Orientation.LeftWall;
        else if (angle == 315)
            return
                orientation == Player.Orientation.LeftWall ||
                orientation == Player.Orientation.Normal;
        else
            return false;
    }

    public override void Advance()
    {
        if (this.excitedTime == 0 && IsPlayerAhead() == true)
            this.excitedTime = 240;
        else if (this.excitedTime > 0)
            this.excitedTime -= 1;

        var excited = this.excitedTime > 0;
        var speed = excited ? 0.25f : 0.062f;

        if (this.onGround == true && excited == false)
        {
            if (this.angle > 10 && this.angle < 180)
                this.facingRight = false;
            if (this.angle > 180 && this.angle < 350)
                this.facingRight = true;
        }

        if (this.facingRight == true)
            this.groundspeed = Util.Slide(this.groundspeed, speed, 0.1f / 16);
        else
            this.groundspeed = Util.Slide(this.groundspeed, -speed, 0.1f / 16);

        const int RaycastLength = 18;

        var down = new Vector2(
            +Mathf.Sin(this.angle * Mathf.PI / 180),
            -Mathf.Cos(this.angle * Mathf.PI / 180)
        );

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        if (this.onGround == true && this.facingRight == false)
        {
            var left = new Vector2(down.y, -down.x);
            var leftRaycast = raycast.Arbitrary(this.position, left, RaycastLength);
            if (leftRaycast.anything == true)
            {
                this.facingRight = true;
                if (this.groundspeed < 0)
                    this.groundspeed *= -0.5f;
            }
        }
        else if (this.onGround == true && this.facingRight == true)
        {
            var right = new Vector2(-down.y, down.x);
            var rightRaycast = raycast.Arbitrary(this.position, right, RaycastLength);
            if (rightRaycast.anything == true)
            {
                this.facingRight = false;
                if (this.groundspeed > 0)
                    this.groundspeed *= -0.5f;
            }
        }
        else if (this.onGround == false)
        {
            var left = new Vector2(down.y, -down.x);
            var leftRaycast = raycast.Arbitrary(this.position, left, RaycastLength);
            if (leftRaycast.anything == true)
            {
                this.onGround = true;
                this.angle = 270;
                this.orientation = Player.Orientation.LeftWall;
                this.groundspeed = -this.velocity.y;
                this.facingRight = this.groundspeed > 0;
            }
            else
            {
                var right = new Vector2(-down.y, down.x);
                var rightRaycast = raycast.Arbitrary(this.position, right, RaycastLength);
                if (rightRaycast.anything == true)
                {
                    this.onGround = true;
                    this.angle = 90;
                    this.orientation = Player.Orientation.RightWall;
                    this.groundspeed = this.velocity.y;
                    this.facingRight = this.groundspeed > 0;
                }
            }
        }

        down = new Vector2(
            +Mathf.Sin(this.angle * Mathf.PI / 180),
            -Mathf.Cos(this.angle * Mathf.PI / 180)
        );

        Raycast.Result r = raycast.Arbitrary(this.position, down, RaycastLength);

        if (r.anything == true)
        {
            this.position += (r.distance - 1) * r.direction;

            var surfaceAngle =
                r.tile != null ?
                r.tile.SurfaceAngleDegrees(
                    this.position - new Vector2(
                        r.tile.mapTx * Tile.Size,
                        r.tile.mapTy * Tile.Size
                    ),
                    r.direction
                ) :
                0;

            this.angle = surfaceAngle;
            this.angle %= 360;
            if (this.angle < 0) this.angle += 360;

            if (this.angle % 90 == 45 &&
                IsValidOrientationWithDiagonalAngle(this.orientation, this.angle))
            {
                // do nothing
            }
            else if (this.angle <= 45)
                this.orientation = Player.Orientation.Normal;
            else if (this.angle <= 135)
                this.orientation = Player.Orientation.RightWall;
            else if (this.angle <= 225)
                this.orientation = Player.Orientation.Ceiling;
            else if (this.angle <= 315)
                this.orientation = Player.Orientation.LeftWall;
            else
                this.orientation = Player.Orientation.Normal;

            Vector2 groundVector;
            groundVector.x = Mathf.Cos(this.angle * Mathf.PI / 180);
            groundVector.y = Mathf.Sin(this.angle * Mathf.PI / 180);

            if (this.onGround == false)
            {
                this.groundspeed = Vector2.Dot(this.velocity, groundVector);
                this.facingRight = this.groundspeed > 0;
            }

            this.velocity = this.groundspeed * groundVector;
            this.onGround = true;
        }
        else
        {
            this.angle = 0;
            this.orientation = Player.Orientation.Normal;
            this.velocity.y -= 0.5f / 16;
            this.onGround = false;
        }

        this.position += this.velocity;
        this.transform.position = this.position;
        this.transform.localRotation = Quaternion.Euler(0, 0, this.angle);
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

        EnemyHitDetectionAgainstPlayer();
    }
}
