
using UnityEngine;

public class Scuttlebug : Enemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationRotate1;
    public Animated.Animation animationRotate2;
    public Animated.Animation animationCharging;
    public Animated.Animation animationChargingRing;
    public Animated.Animation animationElectric;
    public Animated.Animation animationAfterElectric;

    private enum State
    {
        Walking,
        Charging,
        Electric,
        AfterElectric
    };

    private bool up = false;
    private State state = State.Walking;
    private Bamboo bamboo = null;
    private float yRelativeToBamboo = 0;

    private bool isPlayerOnSameBamboo = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new Hitbox[] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;

        this.animated.PlayAndLoop(this.animationWalk);
        this.up = (def.tile.rotation == 0);
        this.spriteRenderer.sortingOrder = 10;

        this.bamboo = Bamboo.BambooAt(
            this.map, this.position.x, Mathf.FloorToInt(this.position.y / Tile.Size)
        );
        if (this.bamboo != null)
            this.yRelativeToBamboo = (this.position.y - this.bamboo.position.y);
        else
            this.chunk.ReportError(this.position, "Missing bamboo underneath Scuttlebug");
    }

    public override void Advance()
    {
        if (this.state == State.Walking)
        {
            var same = IsPlayerOnSameBamboo();
            if (same == true && this.isPlayerOnSameBamboo == false)
                Electrify();

            this.isPlayerOnSameBamboo = same;
        }

        if (this.state == State.Walking)
        {
            AdvanceWalk();
        }
        else if (this.bamboo != null)
        {
            this.transform.position = this.position =
                this.bamboo.position.Add(0, this.yRelativeToBamboo);
        }

        EnemyHitDetectionAgainstPlayer();

        if (this.state == State.Electric &&
            (this.map.player.position - this.position).sqrMagnitude < 3.125f * 3.125f)
        {
            this.map.player.Hit(this.position);
        }
    }

    private void Electrify()
    {
        // we might be interrupting a turn before rotation is updated, so just do it now
        this.transform.localRotation = Quaternion.Euler(0, 0, this.up ? 0 : 180);

        Particle.CreateAndPlayOnce(
            this.animationChargingRing, this.position, this.transform.parent
        );

        this.state = State.Charging;
        this.animated.PlayOnce(this.animationCharging, delegate
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyScuttlebugZap, 
                position: this.position
            );

            this.state = State.Electric;
            this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
            this.animated.PlayOnce(this.animationElectric, delegate
            {
                this.state = State.AfterElectric;
                this.whenHitFromAbove = HitFromAboveBehaviour.Die;
                this.animated.PlayOnce(this.animationAfterElectric, delegate
                {
                    this.state = State.Walking;
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            });
        });
    }

    private void AdvanceWalk()
    {
        if (this.bamboo == null)
        {
            this.bamboo = Bamboo.BambooAt(
                this.map, this.position.x, Mathf.FloorToInt(this.position.y)
            );
            if (this.bamboo == null)
                return;
        }

        this.yRelativeToBamboo += this.up ? 0.0625f : -0.0625f;
        this.position = this.bamboo.position.Add(0, this.yRelativeToBamboo);
        this.transform.position = this.position;

        if (this.yRelativeToBamboo >= 0.5f)
        {
            var next = Bamboo.BambooAt(
                this.map, this.position.x, this.bamboo.ItemDef.ty + 1
            );
            if (next != null)
            {
                this.bamboo = next;
                this.yRelativeToBamboo -= 1;

                if (this.bamboo.twisty)
                    PlayTwistAnimation1();
                else
                    this.spriteRenderer.sortingOrder = 1;
            }
            else if (this.up == true)
            {
                this.up = false;
                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.transform.localRotation = Quaternion.Euler(0, 0, 180);
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            }
        }

        if (this.yRelativeToBamboo < -0.5f)
        {
            var next = Bamboo.BambooAt(
                this.map, this.position.x, this.bamboo.ItemDef.ty - 1
            );
            if (next != null)
            {
                this.bamboo = next;
                this.yRelativeToBamboo += 1;

                if (this.bamboo.twisty)
                    PlayTwistAnimation1();
                else
                    this.spriteRenderer.sortingOrder = 1;
            }
            else if (this.up == false)
            {
                this.up = true;
                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.transform.localRotation = Quaternion.Euler(0, 0, 0);
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            }
        }
    }

    private void PlayTwistAnimation1()
    {
        this.spriteRenderer.sortingOrder = 10;
        this.animated.PlayOnce
        (
            this.up ? this.animationRotate1 : this.animationRotate2,
            delegate
            {
                this.animated.PlayAndLoop(this.animationWalk);
            }
        );
        this.animated.OnFrame(2, delegate { this.spriteRenderer.sortingOrder = -10; });
        this.animated.OnFrame(5, delegate { this.spriteRenderer.sortingOrder =  10; });
    }

    private bool IsPlayerOnSameBamboo()
    {
        if (this.bamboo == null)
            return false;
        if (this.map.player.state != Player.State.SlidingOnBamboo)
            return false;

        var playerBamboo = this.map.player.OnBamboo;
        if (playerBamboo == null) return false;
        if (playerBamboo.ItemDef.tx != this.bamboo.ItemDef.tx) return false;

        var minTy = System.Math.Min(playerBamboo.ItemDef.ty, this.bamboo.ItemDef.ty);
        var maxTy = System.Math.Max(playerBamboo.ItemDef.ty, this.bamboo.ItemDef.ty);

        for (int ty = minTy + 1; ty < maxTy; ty += 1)
        {
            if (this.chunk.ItemAt<Bamboo>(this.bamboo.ItemDef.tx, ty) == null)
                return false;
        }

        return true;
    }
}
