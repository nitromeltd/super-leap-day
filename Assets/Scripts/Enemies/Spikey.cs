
using UnityEngine;
using System;

public class Spikey : Enemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;

    private bool walkingRight;
    private float visibleAngleDegrees;
    private TileGrid lastTileGridUnderFeet;

    private bool turningForwardInPlace;
    private Vector2 turningForwardPivot;

    private bool falling = false;
    [NonSerialized] public Vector2 velocity; // when falling

    private const float DistanceToFeet = 14 / 16f;
    private const float Gravity = 0.5f / 16f;

    public Vector2 Velocity => velocity;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-1, 1, -1, 1, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayAndLoop(this.animationWalk);
        this.walkingRight = !def.tile.flip;
        this.visibleAngleDegrees = this.rotation;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
    }

    private Vector2 Forward() => new Vector2(
        Mathf.Cos(this.rotation * Mathf.PI / 180),
        Mathf.Sin(this.rotation * Mathf.PI / 180)
    );

    private Vector2 Up()
    {
        var forward = Forward();
        return new Vector2(-forward.y, forward.x);
    }

    private Vector2 Down()
    {
        var forward = Forward();
        return new Vector2(forward.y, -forward.x);
    }

    public void BounceFromSpring(Vector2 velocity)
    {
        this.falling = true;
        this.velocity = velocity;
        this.rotation = 0;
        this.visibleAngleDegrees = 0;
    }

    public override void Advance()
    {
        base.Advance();

        if (this.turningForwardInPlace == true)
            TurnForwardInPlace();
        else
        {
            var raycast = new Raycast(
                this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true
            );
            {
                // before even moving anywhere, check below for
                // moving ground and adjust as appropriate
                var below = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
                if (below.tile != null)
                    this.position += below.tile.tileGrid.effectiveVelocity;
            }

            if (this.falling == true)
                Fall(raycast);
            else
                WalkForward(raycast);
        }
        
        EnemyHitDetectionAgainstPlayer();
        SwitchToNearestChunk();

        this.transform.position = this.position;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
        this.transform.localScale = new Vector3(this.walkingRight ? 1 : -1, 1, 1);
    }

    private void TurnForwardInPlace()
    {
        if (this.lastTileGridUnderFeet != null)
            this.turningForwardPivot += this.lastTileGridUnderFeet.effectiveVelocity;

        if (this.rotation < this.visibleAngleDegrees - 180)
            this.visibleAngleDegrees -= 360;
        if (this.rotation > this.visibleAngleDegrees + 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(this.visibleAngleDegrees, this.rotation, 7);

        this.position = this.turningForwardPivot;
        this.position.x +=
            Mathf.Cos((this.visibleAngleDegrees + 90) * Mathf.PI / 180) * 1.0f;
        this.position.y +=
            Mathf.Sin((this.visibleAngleDegrees + 90) * Mathf.PI / 180) * 1.0f;

        if (Mathf.Approximately(this.visibleAngleDegrees, this.rotation))
        {
            this.turningForwardInPlace = false;
        }

        this.velocity = Vector2.zero;
    }

    private bool IsOnOrNearGround(Raycast raycast)
    {
        var feet = this.position - (Up() * DistanceToFeet);
        var offset = Forward() * 6;
        return
            raycast.Arbitrary(feet, -Up(), DistanceToFeet + 4).anything ||
            raycast.Arbitrary(feet + offset, -Up(), DistanceToFeet + 4).anything ||
            raycast.Arbitrary(feet - offset, -Up(), DistanceToFeet + 4).anything;
    }

    private void Fall(Raycast raycast)
    {
        this.velocity.y -= Gravity;
        this.position += this.velocity;
        this.rotation = 0;

        if (this.velocity.y > 0)
        {
            this.animated.PlayAndLoop(this.animationRise);

            var r = raycast.Vertical(this.position, true, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Down() * DistanceToFeet);
                this.velocity.y = 0;
            }
        }
        else if (this.velocity.y < 0)
        {
            this.animated.PlayAndLoop(this.animationFall);

            var r = raycast.Vertical(this.position, false, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Up() * DistanceToFeet);
                this.falling = false;

                // dust particles
                for (var n = 0; n < 10; n += 1)
                {
                    var p = Particle.CreateAndPlayOnce(
                        this.map.player.animationDustLandParticle,
                        r.End(),
                        this.transform.parent
                    );
                    p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                    p.velocity.x =
                        (((n % 2 == 0) ? 1 : -1) *
                        UnityEngine.Random.Range(0.2f, 0.3f));
                    p.velocity.y = UnityEngine.Random.Range(-0.05f, 0.05f);
                    p.acceleration = p.velocity * -0.05f;
                    p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
                }
                
                this.animated.PlayAndLoop(animationWalk);
            }
        }
    }

    private void WalkForward(Raycast raycast)
    {
        {
            var ahead = raycast.Arbitrary(
                this.position + (Up() * -10 / 16f),
                this.walkingRight ? Forward() : -Forward(),
                1
            );
            var aheadPeg = ahead.item as Peg;
            if (aheadPeg != null)
            {
                aheadPeg.NotifyEnemyBouncedOff(new Collision { otherItem = this });

                this.walkingRight = !this.walkingRight;

                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.animated.PlayAndLoop(this.animationWalk);
                });
            }
        }

        this.position += (this.walkingRight ? 1 : -1) * Forward() / 16f;

        var forward = raycast.Arbitrary(
            this.position, this.walkingRight ? Forward() : -Forward(), 12 / 16f
        );
        if (forward.anything == true)
        {
            this.rotation = Mathf.RoundToInt(forward.surfaceAngleDegrees);
            this.position = forward.End() + (Up() * DistanceToFeet);
        }
        else
        {
            var below = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
            if (below.anything == true)
            {
                this.position += Up() * (DistanceToFeet - below.distance);
                this.rotation = Mathf.RoundToInt(below.surfaceAngleDegrees);
                this.lastTileGridUnderFeet = below.tile?.tileGrid;

                var doublecheck = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
                var groundThereAfterRotation =
                    doublecheck.anything == true &&
                    Mathf.Abs(
                        doublecheck.surfaceAngleDegrees - below.surfaceAngleDegrees
                    ) < 10.0f;

                if (groundThereAfterRotation == false)
                {
                    // we detected ground below us, but upon rotating to its angle,
                    // we could no longer find it. this happens when we are walking
                    // onto a downward slope and our offset above the ground
                    // brings the raycast too far back, so it doesn't reach the slope.
                    // a little boost forward would take care of it. basically,
                    // we need to rotate around the raycast end point, not
                    // spikey's centerpoint. the turning-forward system handles this.
                    this.turningForwardInPlace = true;
                    this.turningForwardPivot = below.End();
                }
                else
                {
                    if (this.rotation < this.visibleAngleDegrees - 180)
                        this.visibleAngleDegrees -= 360;
                    if (this.rotation > this.visibleAngleDegrees + 180)
                        this.visibleAngleDegrees += 360;
                    this.visibleAngleDegrees = Util.Slide(
                        this.visibleAngleDegrees, this.rotation, 7
                    );
                }
            }
            else
            {
                var ahead = this.walkingRight ? Forward() : -Forward();
                var up = Up();
                var rayStart = this.position + (ahead * 10 / 16f);
                var rayDelta = (up * -DistanceToFeet) + (ahead * -9 / 16f);
                var belowBehind = raycast.Arbitrary(
                    rayStart,
                    rayDelta.normalized,
                    rayDelta.magnitude + (5 / 16f)
                );
                if (belowBehind.anything == true)
                {
                    this.rotation = (int)belowBehind.surfaceAngleDegrees;
                    this.turningForwardInPlace = true;
                    this.turningForwardPivot = belowBehind.End();
                }
                else
                {
                    this.falling = true;
                }
            }
        }

        this.velocity = Vector2.zero;
    }
}
