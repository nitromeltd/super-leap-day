
using UnityEngine;

public class TrafficConeEnemy : WalkingEnemy
{
    [HideInInspector]public Pipe insidePipe;
    public Pipe.TrackingData pipeTrackingData;

    public Animated.Animation animationAsleep;
    public Animated.Animation animationAlert;
    public Animated.Animation animationChase;
    public Animated.Animation animationLost;
    public Animated.Animation animationLostStart;
    public Animated.Animation animationLostLoop;
    public Animated.Animation animationLostEnd;
    public Animated.Animation animationRoam;
    public Animated.Animation animationTurn;
    public Animated.Animation animationBump;
    public Animated.Animation animationInsidePipe;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;
    public Animated.Animation animationLand;
    public Animated.Animation animationSpin;

    [Header("Lights")]
    public Animated lightsAnimated;
    public Animated.Animation animationLightsOff;
    public Animated.Animation animationLightsOn;

    private State state = State.Asleep;
    private bool isTurning;
    private int playerGoneTimer;
    private int backToSleepTimer;
    private float visibleAngleDegrees;
    private bool deadly = false;
    private int lostTimer;

    private const float MaxPlayerVerticalDistance = 4f;
    private const float MaxPlayerHorizontalDistance = 20f;
    private const float ChaseSpeed = 0.15f;
    private const float RoamSpeed = 0.05f;
    private const int CheckPlayerGoneTime = 60;
    private const int GoBackToSleepTime = 490;
    private static Vector2 HitWallPushbackForce = new Vector2(0.1f, 0.2f);

    private bool isChasePlaying = false;

    public enum State
    {
        Asleep, // waiting for player
        Alert, // detect player
        Chase, // chasing player
        Bump, // crash into wall when chasing player
        Lost, // lost track of player
        Roam, // walk back and forth
        Pipe, // Inside Pipe
        Airborne, // falling or rising
        Land // after Airborne
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
           new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };

        this.isTurning = false;
        this.playerGoneTimer = 0;
        this.backToSleepTimer = 0;
        this.visibleAngleDegrees = 0f;
        this.turnAtEdges = false;
        this.deadly = false;
        
        this.onTurnAround = OnTurnAround;
        
        ChangeState(State.Asleep);
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true)
            return;
        
        if (this.insidePipe != null)
        {
            AdvancePipe();
        }
        else
        {
            AdvanceOutsidePipe();
        }
    }

    // turns only when encountering a Wall
    private void OnTurnAround(TurnInfo turnInfo)
    {
        switch(this.state)
        {
            case State.Lost:
            default:
            {
                // no additional action needed
            }
            break;

            case State.Roam:
            {
                this.isTurning = true;

                this.animated.PlayOnce(this.animationTurn, delegate
                {
                    this.isTurning = false;
                    this.animated.PlayAndLoop(this.animationRoam);
                });
            }
            break;

            case State.Chase:
            {
                ChangeState(State.Bump);
            }
            break;
        }
    }

    private void AdvancePipe()
    {
        this.insidePipe.AdvanceTrafficCone(this);
        this.spriteRenderer.sortingLayerName = "Spikes";
        this.spriteRenderer.sortingOrder = -10;
        
        this.visibleAngleDegrees += 15;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    private void AdvanceOutsidePipe()
    {
        this.velocity.y -= 0.5f / 16;
        
        if(IsAirborne() == true &&
            this.state != State.Asleep &&
            this.state != State.Alert &&
            this.state != State.Bump &&
            this.state != State.Airborne)
        {
            ChangeState(State.Airborne);
        }

        switch(this.state)
        {
            case State.Asleep:
                AdvanceAsleep();
                break;

            case State.Alert:
                AdvanceAlert();
                break;

            case State.Chase:
                AdvanceChase();
                break;

            case State.Bump:
                AdvanceBump();
                break;

            case State.Roam:
                AdvanceRoam();
                break;

            case State.Lost:
                AdvanceLost();
                break;

            case State.Airborne:
                AdvanceAirborne();
                break;
        }

        if(this.deadly == true)
        {
            EnemyHitDetectionAgainstPlayer();
        }

        this.transform.position = this.position;
        RefreshRotation();

        if(this.state != State.Bump)
        {
            this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        }
    }

    public void ChangeState(State newState)
    {
        switch(this.state)
        {
            case State.Chase:
                ExitChase();
                break;
        }

        this.state = newState;

        switch(newState)
        {
            case State.Asleep:
                EnterAsleep();
                break;

            case State.Alert:
                EnterAlert();
                break;

            case State.Chase:
                EnterChase();
                break;

            case State.Bump:
                EnterBump();
                break;

            case State.Roam:
                EnterRoam();
                break;

            case State.Lost:
                EnterLost();
                break;

            case State.Pipe:
                EnterPipe();
                break;

            case State.Airborne:
                EnterAirborne();
                break;

            case State.Land:
                EnterLand();
                break;
        }
    }

    private void AdvanceAsleep()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        
        DetectPlayer();
    }

    private void DetectPlayer()
    {
        if (IsDetectingPlayer())
        {
            if((this.facingRight && this.map.player.position.x < this.position.x) ||
                (!this.facingRight && this.map.player.position.x > this.position.x))
            {
                this.velocity.x *= -1;
                this.facingRight = (this.velocity.x > 0);
            }

            ChangeState(State.Alert);
        }
    }

    private bool IsDetectingPlayer()
    {
        var playerRelative = this.map.player.position - this.position;

        if(this.map.player.alive == false)
        {
            return false;
        }

        if(Mathf.Abs(playerRelative.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(playerRelative.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        if(this.isTurning == true)
        {
            return false;
        }

        return true;
    }

    private void AdvanceAlert()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
    }

    private void AdvanceChase()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        float targetVelocityX =  this.facingRight ? ChaseSpeed : -ChaseSpeed;
        this.velocity.x = Util.Slide(this.velocity.x, targetVelocityX, 0.005f);

        if (this.map.frameNumber % 2 == 0)
        {
            CreateDustParticles();
        }

        if(this.playerGoneTimer > 0)
        {
            this.playerGoneTimer -= 1;
        }

        if(IsPlayerGone())
        {
            if(this.playerGoneTimer == 0)
            {
                ChangeState(State.Lost);
            }
        }
        else
        {
            this.playerGoneTimer = CheckPlayerGoneTime;
        }

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemyConeChase) && this.isChasePlaying)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyConeChase,
                position: this.position
            );
        }
    }

    private void AdvanceBump()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        if(this.onGround == true)
        {
            EnterLost();
        }
    }

    private bool IsPlayerGone()
    {
        var playerRelative = this.map.player.position - this.position;

        if(Mathf.Abs(playerRelative.x) > MaxPlayerHorizontalDistance)
        {
            return true;
        }
        if(Mathf.Abs(playerRelative.y) > MaxPlayerVerticalDistance)
        {
            return true;
        }

        return false;
    }

    private void CreateDustParticles()
    {
        float x = this.position.x;
        float y = this.position.y - 0.50f;

        Particle p = Particle.CreateAndPlayOnce(
            this.map.player.animationDustParticle,
            new Vector2(
                x + UnityEngine.Random.Range(-0.2f, 0.2f),
                y + UnityEngine.Random.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = UnityEngine.Random.Range(0.50f, 0.80f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            UnityEngine.Random.Range(-0.1f, 0.1f),
            UnityEngine.Random.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -2;
    }

    private void AdvanceRoam()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        DetectPlayer();

        if(this.backToSleepTimer > 0)
        {
            this.backToSleepTimer -= 1;

            if(this.backToSleepTimer == 0 &&
                IsDetectingPlayer() == false)
            {
                ChangeState(State.Asleep);
            }
        }
    }

    private void AdvanceLost()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.01f);

        if(this.lostTimer > 0)
        {
            this.lostTimer -= 1;
            DetectPlayer();
        }
        else if(this.animated.currentAnimation == this.animationLostLoop)
        {
            this.lostTimer = 0;
            
            this.animated.PlayOnce(this.animationLostEnd, () =>
            {
                EnterRoam();
            });
        }
    }

    private void AdvanceAirborne()
    {
        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();

        if(IsAirborne() == false)
        {
            ChangeState(State.Land);
        }
    }

    private void EnterAsleep()
    {
        this.state = State.Asleep;
        
        this.velocity.x = 0f;
        this.animated.PlayAndLoop(this.animationAsleep);
        
        this.deadly = false;
    }

    private void EnterAlert()
    {
        this.state = State.Alert;
        
        this.velocity.x = 0f;
        this.velocity.y = (this.deadly == false) ? 0.325f :  0.20f;
        this.animated.PlayOnce(this.animationAlert, () =>
        {
            ChangeState(State.Chase);
        });

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyConeAlert,
            position: this.position
        );
    }

    private void EnterChase()
    {
        this.state = State.Chase;

        this.playerGoneTimer = CheckPlayerGoneTime;
        this.animated.PlayAndLoop(this.animationChase);
        this.lightsAnimated.PlayAndLoop(this.animationLightsOn);
        
        this.deadly = true;

        this.isChasePlaying = true;
    }

    private void EnterBump()
    {
        this.state = State.Bump;

        float horizontalPushbackForce =
            Random.Range(-0.025f, 0.025f) + HitWallPushbackForce.x;
        float verticalPushbackForce =
            Random.Range(-0.035f, 0.035f) + HitWallPushbackForce.y;

        this.facingRight = !this.facingRight;
        this.velocity.x = horizontalPushbackForce * Mathf.Sign(this.velocity.x);
        this.velocity.y = verticalPushbackForce;
        this.animated.PlayAndHoldLastFrame(this.animationBump);

        var p = Particle.CreateWithSprite(
                Assets.instance.jabbajawHitWallParticle,
                8,
                this.position.Add(1 * Mathf.Sign(this.velocity.x), -.5f),
                this.map.transform
            );

        p.transform.localScale = new Vector3(0.7f, 1.3f, p.transform.localScale.z);
        p.scale = .05f;

        this.map.ScreenShakeAtPosition(this.position, new Vector2(.3f, 0.5f));

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyConeWallBump,
            position: this.position
        );
    }

    private void ExitChase()
    {
        this.lightsAnimated.PlayOnce(this.animationLightsOff);
        Audio.instance.StopSfx(Assets.instance.sfxEnemyConeChase);
        this.isChasePlaying = false;
    }

    private void EnterLost()
    {
        this.state = State.Lost;

        this.lostTimer = 0;
        this.animated.PlayOnce(this.animationLostStart, () =>
        {
            this.animated.PlayAndLoop(this.animationLostLoop);
            
            float animLostTimeInSeconds =
                (float)this.animationLostLoop.sprites.Length / (float)this.animationLostLoop.fps;
            int animLostTimeInFrames = Mathf.RoundToInt(animLostTimeInSeconds * 60);
            int animLookAroundInFrames = Mathf.RoundToInt(animLostTimeInFrames / 2f);

            float numberRepetitions = 1f + (0.5f * Random.Range(1, 5));
            // play and loop the "look around" animation a random
            // number of times to avoid clumping enemies together
            this.lostTimer = Mathf.RoundToInt(animLookAroundInFrames * numberRepetitions);
        });
    }

    private void EnterRoam()
    {
        this.state = State.Roam;

        this.velocity.x = RoamSpeed * ((facingRight == true) ? 1f : -1f);
        this.backToSleepTimer = GoBackToSleepTime;
        this.animated.PlayAndLoop(this.animationRoam);
    }

    private void EnterAirborne()
    {
        this.state = State.Airborne;

        Animated.Animation animationAirborne = this.velocity.y > 0f ?
            this.animationRise : this.animationFall;
        this.animated.PlayAndLoop(animationAirborne);
    }

    private void EnterPipe()
    {
        this.state = State.Pipe;

        this.animated.PlayAndLoop(this.animationSpin);
    }

    private void EnterLand()
    {
        this.state = State.Land;

        this.animated.PlayOnce(this.animationLand, () =>
        {
            ChangeState(State.Roam);
        });
    }

    protected void RefreshScale()
    {
        if (this.velocity.x > 0) this.transform.localScale = new Vector3(+1, 1, 1);
        if (this.velocity.x < 0) this.transform.localScale = new Vector3(-1, 1, 1);
    }

    private void RefreshRotation()
    {
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;

        this.visibleAngleDegrees = Util.Slide(this.visibleAngleDegrees, this.groundAngleDegrees, 3);

        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
    }

    private bool IsAirborne()
    {
        return this.onGround == false;
    }    
}
