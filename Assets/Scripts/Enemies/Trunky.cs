
using UnityEngine;

public class Trunky : WalkingEnemy
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationShoot;
    public Animated.Animation animationSniffle;
    public Animated.Animation animationFall;
    public Animated.Animation animationRise;
    
    private int attackCooldownTimer;
    private bool shooting = false;
    private float visibleAngleDegrees = 0;
    private int sniffleTimer;
    private bool sniffling = false;
    private bool onGroundLast = false;
    private bool insideWater = false;

    private const int AfterAttackCooldownTime = 50;
    private const int MaxPlayerVerticalDistance = 2;
    private const int MaxPlayerHorizontalDistance = 16;
    private const int TurningCooldownTime = 15;
    private const int SniffleTimePeriod = 180;
    private const float GravityForce = 0.03125f;
    private const float GravityForceUnderwater = 0.01f;
    private static Vector2 BulletSpawnOffset = new Vector2(1.2f, 0f);

    private float CurrentGravityForce => this.insideWater == true ?
        GravityForceUnderwater : GravityForce;

    public override void Init(Def def)
    {
        base.Init(def);
        
        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = 0.05f * initialDirectionValue;
        this.turnSensorForward = .3f;
        this.edgeTurnSensorForward = -.45f;

        this.hitboxes = new [] {
            new Hitbox(-.9f, .9f, -0.9f, 0.7f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndLoop(this.animationWalk);

        this.onTurnAround = delegate
        {
            this.animated.PlayOnce(this.animationTurn, delegate
            {
                this.animated.PlayAndLoop(this.animationWalk);
            });
        };

        this.sniffleTimer = SniffleTimePeriod;
    }

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true)
            return;

        AdvanceWater();

        this.attackCooldownTimer = this.attackCooldownTimer > 0 ?
            this.attackCooldownTimer -= 1 : 0;

        this.velocity.y -= CurrentGravityForce;
        
        if(this.shooting == true && this.animated.currentAnimation != this.animationShoot)
        {
            this.shooting = false;
        }

        if (this.shooting == false && this.sniffling == false)
        {
            var raycast = new Raycast(
                this.map, this.position, isCastByEnemy: true, ignoreItem1: this
            );

            Move(raycast);
            FollowPortals();
            EnemyHitDetectionAgainstPlayer();

            this.transform.position = this.position;
            this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

            var playerRelative = this.map.player.position - this.position;
            var playerForward = playerRelative.x * Mathf.Sign(this.velocity.x);

            this.sniffleTimer = (this.sniffleTimer > 0) ? sniffleTimer - 1 : 0;
            
            Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
            
            if(this.sniffleTimer == 0)
            {
                Sniffle();
            }

            if (this.attackCooldownTimer == 0 &&
                this.map.player.alive == true &&
                Mathf.Abs(playerRelative.y) < MaxPlayerVerticalDistance &&
                playerForward > 0 &&
                playerForward < MaxPlayerHorizontalDistance &&
                this.sniffling == false &&
                this.onGround == true &&
                playerChunk == this.chunk)
            {
                Shoot();
            }
        }
        else
        {
            var raycast = new Raycast(
                this.map, this.position, isCastByEnemy: true, ignoreItem1: this
            );
            Move(raycast, stopHorizontally: true);
            FollowPortals();
            this.transform.position = this.position;

            EnemyHitDetectionAgainstPlayer();
        }

        if (this.trappedHorizontally == true)
            this.animated.PlayAndLoop(this.animationIdle);
        else if (this.animated.currentAnimation == this.animationIdle)
            this.animated.PlayAndLoop(this.animationWalk);
        
        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 3
        );
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);

        CheckRiseAndFall();
    }

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    private void Shoot()
    {
        this.shooting = true;

        this.animated.PlayOnce(this.animationShoot, delegate
        {
            this.animated.PlayAndLoop(this.animationWalk);
            this.shooting = false;
            this.attackCooldownTimer = AfterAttackCooldownTime;
        });

        this.animated.OnFrame(4, delegate
        {
            Audio.instance.PlaySfx(Assets.instance.sfxTrunkyWindup, position: this.position);
        });

        this.animated.OnFrame(11, delegate
        {
            var sign = Mathf.Sign(this.velocity.x);
            Vector2 bulletSpawnPos = this.position.Add(
                BulletSpawnOffset.x * sign,
                BulletSpawnOffset.y
            );

            var bullet = TrunkyBullet.Create(
                bulletSpawnPos,
                this.chunk
            );
            bullet.velocity.x = 3 * sign / 16f;

            Audio.instance.PlaySfx(Assets.instance.sfxTrunkyFire, position: this.position);
        });
    }

    private void Sniffle()
    {
        this.sniffling = true;

        this.animated.PlayOnce(this.animationSniffle, () => {
            this.sniffling = false;
            this.sniffleTimer = SniffleTimePeriod;
            this.animated.PlayAndLoop(this.animationWalk);
        });

        this.animated.OnFrame(12, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxTrunkyWindup, position: this.position);
        });
    }

    private void CheckRiseAndFall()
    {
        if(this.onGround == false)
        {
            Animated.Animation animationAirborne = this.velocity.y > 0f ?
                this.animationRise : this.animationFall;

            this.animated.PlayAndLoop(animationAirborne);

            if(this.sniffling == true)
            {
                this.sniffling = false;
                this.sniffleTimer = SniffleTimePeriod;
            }
        }
        
        if(this.onGround == true && this.onGroundLast == false)
        {
            this.animated.PlayAndLoop(animationWalk);
        }

        this.onGroundLast = this.onGround;
    }
}
