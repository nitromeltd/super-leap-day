﻿using System;
using UnityEngine;

public class TrunkyBullet : Enemy
{
    public Animated.Animation animationMove;
    public Animated.Animation animationBurst;

    [NonSerialized] public Vector2 velocity;
    public bool alive = true;
    
    public static TrunkyBullet Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.trunkyBullet, chunk.transform);
        obj.name = "Spawned Trunky Bullet";
        var item = obj.GetComponent<TrunkyBullet>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndLoop(this.animationMove);

        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }
    
    public override void Advance()
    {
        if (this.alive == false) return;

        base.Advance();

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        MoveHorizontally(raycast);
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        this.transform.position = this.position;
    }
    
    protected void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        float xOffset = Mathf.Sign(this.velocity.x) * -.25f;

        var high    = this.position.Add(xOffset, rect.yMax - .5f);
        var mid     = this.position.Add(xOffset, (rect.yMin + rect.yMax) * 0.25f);
        var low     = this.position.Add(xOffset, rect.yMin + .5f);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid,  true, maxDistance),
                raycast.Horizontal(low,  true, maxDistance)
            );

            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid,  false, maxDistance),
                raycast.Horizontal(low,  false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
                Collide();
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }
    
    private void Collide()
    {
        this.alive = false;

        this.animated.PlayOnce(this.animationBurst, delegate
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
        });

        Audio.instance.PlaySfx(Assets.instance.sfxTrunkyProjectileLand, position: this.position);
    }
}
