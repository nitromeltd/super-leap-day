﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TurtleCannon : Enemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationEnterShoot;
    public Animated.Animation animationLoopShoot;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;
    public Animated.Animation animationHide;
    public Sprite frozenSprite;
    public Sprite deathSprite;

    public Animated.Animation animationWalkHot;
    public Animated.Animation animationTurnHot;
    public Animated.Animation animationEnterShootHot;
    public Animated.Animation animationLoopShootHot;
    public Animated.Animation animationRiseHot;
    public Animated.Animation animationFallHot;
    public Animated.Animation animationHideHot;
    public Sprite frozenSpriteHot;
    public Sprite deathSpriteHot;

    private bool walkingRight;
    private float visibleAngleDegrees;
    private bool falling = false;
    private bool shooting = false;
    private Map.Temperature shootTemperature;
    private int shootTime = 0;
    private Vector2 velocity; // when falling
    private float speed;
    private int blocksAmount = 0;
    private Vector2? alignedPosition = null;
    private int afterShootTimer;
    private bool hiding = false;
    private float hideTimer = 0f;
    private Vector3 scaleVelocity;

    private const float DistanceToFeet = 1.05f;
    private const float Gravity = 0.5f / 16f;
    private const float WalkSpeed = 0.70f;
    private const float DetectPlayerHorizontal = 1f;
    private const float FlamethrowerLengthLimit = 5f;
    private const int AfterShootDelay = 25;
    private const int HideTime = 60;

    public Vector2 Velocity => velocity;

    public enum State
    {
        walking,
        shooting,
        rising,
        falling,
        hiding,
        turning
    }
    private State state;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.70f, 0.70f, -1f, 0.50f, this.rotation, Hitbox.Solid)
        };

        if (this.map.currentTemperature == Map.Temperature.Cold)
        {
            this.animated.PlayAndLoop(this.animationWalk);
        }
        else
        {
            this.animated.PlayAndLoop(this.animationWalkHot);
        }
        this.state = State.walking;
        this.walkingRight = !def.tile.flip;
        this.visibleAngleDegrees = this.rotation;
        this.whenHitFromAbove = HitFromAboveBehaviour.Nothing;
        this.speed = WalkSpeed;

        this.onCollisionFromAbove = OnCollisionAbove;
        this.onCollisionFromLeft = OnCollisionBelow;
        this.onCollisionFromRight = OnCollisionRight;
        this.onCollisionFromBelow = OnCollisionLeft;
    }
    
    private void OnCollisionAbove(Collision collision)
    {
        if(this.rotation == 0)
        {
            this.map.player.BounceOffHitEnemy(this.position, enemyHasSolidTop: true);
        }

        OnCollision(collision);
    }

    private void OnCollisionBelow(Collision collision)
    {
        if(this.rotation == 270)
        {
            this.map.player.BounceOffHitEnemy(this.position, enemyHasSolidTop: true);
        }

        OnCollision(collision);
    }
    
    private void OnCollisionRight(Collision collision)
    {
        if(this.rotation == 90)
        {
            this.map.player.BounceOffHitEnemy(this.position, enemyHasSolidTop: true);
        }

        OnCollision(collision);
    }
    
    private void OnCollisionLeft(Collision collision)
    {
        OnCollision(collision);
    }

    private void OnCollision(Collision collision)
    {
        ChangeScale(new Vector3(0.95f, 1.05f));

        if(this.shooting == false &&
            this.falling == false)
        {
            Hide();
        }
    }

    private void Hide()
    {
        this.hiding = true;

        if (this.map.currentTemperature == Map.Temperature.Cold)
        {
            this.animated.PlayAndHoldLastFrame(this.animationHide);
        }
        else
        {
            this.animated.PlayAndHoldLastFrame(this.animationHideHot);
        }
        this.state = State.hiding;

        this.hideTimer = HideTime;
        this.speed = 0f;
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyTurtlecannonHide,
            position: this.position
        );
    }

    private void EndHide()
    {
        this.hiding = false;

        // resume walking
        if (this.map.currentTemperature == Map.Temperature.Cold)
        {
            this.animated.PlayAndLoop(this.animationWalk);
        }
        else
        {
            this.animated.PlayAndLoop(this.animationWalkHot);
        }
        this.speed = WalkSpeed;
        this.state = State.walking;

        ChangeScale(new Vector3(1.2f, 0.8f));
    }

    private Vector2 Forward() => new Vector2(
        Mathf.Cos(this.rotation * Mathf.PI / 180),
        Mathf.Sin(this.rotation * Mathf.PI / 180)
    );

    private Vector2 Up()
    {
        var forward = Forward();
        return new Vector2(-forward.y, forward.x);
    }

    private Vector2 Down()
    {
        var forward = Forward();
        return new Vector2(forward.y, -forward.x);
    }

    public void BounceFromSpring(Vector2 velocity)
    {
        this.falling = true;
        this.velocity = velocity;
        this.rotation = 0;
        this.visibleAngleDegrees = 0;
    }

    public override void Advance()
    {
        if(this.deathAnimationPlaying == true) return;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true, ignoreItem1: this
        );
        {
            // before even moving anywhere, check below for
            // moving ground and adjust as appropriate
            var below = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
            if (below.tile != null)
                this.position += below.tile.tileGrid.effectiveVelocity;
        }

        if(this.afterShootTimer > 0)
        {
            this.afterShootTimer -= 1;
        }

        if(this.hideTimer > 0)
        {
            this.hideTimer -= 1;

            if(this.hideTimer == 0)
            {
                EndHide();
            }
        }

        if (this.falling == true)
            Fall(raycast);
        else
            WalkForward(raycast);

        if(this.shooting == true)
        {
            if(this.alignedPosition.HasValue)
            {
                this.position = new Vector2(
                    Util.Slide(this.position.x, this.alignedPosition.Value.x, 0.10f),
                    Util.Slide(this.position.y, this.alignedPosition.Value.y, 0.10f)
                );

                if(this.position == this.alignedPosition)
                {
                    this.alignedPosition = null;
                }
            }
            else
            {
                Shooting();
            }
        }
        else
        {
            DetectPlayer();
        }
        
        //EnemyHitDetectionAgainstPlayer();
        SwitchToNearestChunk();

        this.transform.position = this.position;
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);
        
        Vector3 absLocalScale = new Vector3(
            Mathf.Abs(this.transform.localScale.x),
            this.transform.localScale.y,
            this.transform.localScale.z
        );

        Vector3 currentScale = Vector3.SmoothDamp(
            absLocalScale,
            Vector3.one,
            ref scaleVelocity,
            0.05f
        );

        ChangeScale(currentScale);
    }

    private void ChangeScale(Vector3 newScale)
    {
        float xScale = Mathf.Abs(newScale.x);

        this.scaleVelocity = Vector3.zero;

        this.transform.localScale = new Vector3(
            this.walkingRight ? xScale : -xScale,
            newScale.y,
            1f
        );
    }

    private void Fall(Raycast raycast)
    {
        this.velocity.y -= Gravity;
        this.position += this.velocity;
        this.rotation = 0;

        if (this.velocity.y > 0)
        {
            if (this.map.currentTemperature == Map.Temperature.Cold)
            {
                this.animated.PlayAndLoop(this.animationRise);
            }
            else
            {
                this.animated.PlayAndLoop(this.animationRiseHot);
            }
            this.state = State.rising;

            var r = raycast.Vertical(this.position, true, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Down() * DistanceToFeet);
                this.velocity.y = 0;
            }
        }
        else if (this.velocity.y < 0)
        {
            if (this.map.currentTemperature == Map.Temperature.Cold)
            {
                this.animated.PlayAndLoop(this.animationFall);
            }
            else
            {
                this.animated.PlayAndLoop(this.animationFallHot);
            }
            this.state = State.falling;

            var r = raycast.Vertical(this.position, false, DistanceToFeet + 0.25f);
            if (r.anything == true)
            {
                this.position = r.End() + (Up() * DistanceToFeet);
                this.falling = false;

                // dust particles
                for (var n = 0; n < 10; n += 1)
                {
                    var p = Particle.CreateAndPlayOnce(
                        this.map.player.animationDustLandParticle,
                        r.End(),
                        this.transform.parent
                    );
                    p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                    p.velocity.x =
                        (((n % 2 == 0) ? 1 : -1) *
                        UnityEngine.Random.Range(0.2f, 0.3f));
                    p.velocity.y = UnityEngine.Random.Range(-0.05f, 0.05f);
                    p.acceleration = p.velocity * -0.05f;
                    p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
                }

                if (this.map.currentTemperature == Map.Temperature.Cold)
                {
                    this.animated.PlayAndLoop(animationWalk);
                }
                else
                {
                    this.animated.PlayAndLoop(animationWalkHot);
                }
                this.state = State.walking;
                this.speed = WalkSpeed;
            }
        }
    }

    private void WalkForward(Raycast raycast)
    {
        void Turn()
        {
            this.walkingRight = !this.walkingRight;
            this.speed = 0f;

            if(this.alignedPosition != null)
            {
                CalculateAlignedPosition();
            }

            this.state = State.turning;
            if(this.shooting == false)
            {
                if (this.map.currentTemperature == Map.Temperature.Cold)
                {
                    if (this.map.currentTemperature == Map.Temperature.Cold)
                    {
                        this.animated.PlayAndLoop(this.animationWalk);

                    }
                    else
                    {
                        this.animated.PlayAndLoop(this.animationWalkHot);
                    }
                    this.state = State.walking;
                    this.speed = WalkSpeed;
                    this.state = State.walking;
                }
                else
                {
                    this.animated.PlayOnce(this.animationTurnHot, delegate
                    {
                        if (this.map.currentTemperature == Map.Temperature.Cold)
                        {
                            this.animated.PlayAndLoop(this.animationWalk);

                        }
                        else
                        {
                            this.animated.PlayAndLoop(this.animationWalkHot);
                        }
                        this.state = State.walking;
                        this.speed = WalkSpeed;
                        this.state = State.walking;
                    });
                }
            }
        }

        {
            var ahead = raycast.Arbitrary(
                this.position + (Up() * -10 / 16f),
                this.walkingRight ? Forward() : -Forward(),
                1
            );
            var aheadPeg = ahead.item as Peg;
            if (aheadPeg != null)
            {
                aheadPeg.NotifyEnemyBouncedOff(new Collision { otherItem = this });
                Turn();
            }
        }

        this.position += (this.walkingRight ? 1 : -1) * (Forward() / 16f) * this.speed;

        var forward = raycast.Arbitrary(
            this.position, this.walkingRight ? Forward() : -Forward(), 12 / 16f
        );

        if (forward.anything == true)
        {
            // detected solid
            Turn();
        }
        else
        {
            
            var below = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
            if (below.anything == true)
            {
                this.position += Up() * (DistanceToFeet - below.distance);
                this.rotation = Mathf.RoundToInt(below.surfaceAngleDegrees);

                var doublecheck = raycast.Arbitrary(this.position, -Up(), 30 / 16f);
                var groundThereAfterRotation =
                    doublecheck.anything == true &&
                    Mathf.Abs(
                        doublecheck.surfaceAngleDegrees - below.surfaceAngleDegrees
                    ) < 10.0f;

                if (this.rotation < this.visibleAngleDegrees - 180)
                    this.visibleAngleDegrees -= 360;
                if (this.rotation > this.visibleAngleDegrees + 180)
                    this.visibleAngleDegrees += 360;
                this.visibleAngleDegrees = Util.Slide(
                    this.visibleAngleDegrees, this.rotation, 7
                );
            }

            var ahead = this.walkingRight ? Forward() : -Forward();
            var belowAhead = raycast.Arbitrary(
                this.position+ (ahead * 0.65f),
                -Up(),
                1.875f
            );

            if (belowAhead.anything == false)
            {                
                var up = Up();
                var rayStart = this.position + (ahead * 10 / 16f);
                var rayDelta = (up * -DistanceToFeet) + (ahead * -9 / 16f);
                var belowBehind = raycast.Arbitrary(
                    rayStart,
                    rayDelta.normalized,
                    rayDelta.magnitude + (5 / 16f)
                );

                if (belowBehind.anything == true)
                {
                    // detected gap
                    Turn();
                }
                else
                {
                    // floor disappeared
                    this.falling = true;
                }
            }
        }

        this.velocity = Vector2.zero;
    }

    private void DetectPlayer()
    {
        if(this.hiding == true) return;
        if(this.afterShootTimer > 0) return;
        
        var detectRaycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true, ignoreItem1: this
        );

        var detectSolid = detectRaycast.Arbitrary(
            this.position, Up(), 14f
        );

        float distanceToSolid = detectSolid.distance;

        if(distanceToSolid > 0f)
        {
            Rect GetDetectRectangle()
            {
                switch(this.rotation)
                {
                    case 0:
                    default:
                    {
                        Vector2 origin = this.position + new Vector2(
                            -(DetectPlayerHorizontal / 2), 0f
                        );

                        return new Rect(
                            origin.x, origin.y, DetectPlayerHorizontal, distanceToSolid
                        );
                    }

                    case 90:
                    {
                        Vector2 origin = this.position + new Vector2(
                            -distanceToSolid, -(DetectPlayerHorizontal / 2)
                        );

                        return new Rect(
                            origin.x, origin.y, distanceToSolid, DetectPlayerHorizontal
                        );
                    }

                    case 180:
                    {
                        Vector2 origin = this.position + new Vector2(
                            -(DetectPlayerHorizontal / 2), -distanceToSolid
                        );

                        return new Rect(
                            origin.x, origin.y, DetectPlayerHorizontal, distanceToSolid
                        );
                    }

                    case 270:
                    {
                        Vector2 origin = this.position + new Vector2(
                            0f, -(DetectPlayerHorizontal / 2)
                        );

                        return new Rect(
                            origin.x, origin.y, distanceToSolid, DetectPlayerHorizontal
                        );
                    }
                }
            }

            Rect detectRect = GetDetectRectangle();
            Player player = this.map.player;

            if(detectRect.Contains(player.position))
            {
                this.shootTemperature = this.map.currentTemperature;

                CalculateAlignedPosition();
                StartShoot();
            }
        }
    }

    private void CalculateAlignedPosition()
    {
        bool movingHorizontally = this.rotation == 0f || this.rotation == 180f;

        float alignedPositionX = movingHorizontally == true ? RoundToTileGrid(this.position.x)   : this.position.x;
        float alignedPositionY = movingHorizontally == false ? RoundToTileGrid(this.position.y) : this.position.y;
        this.alignedPosition = new Vector2(alignedPositionX, alignedPositionY);
    }

    private float RoundToTileGrid(float value)
    {
        float gridOffset = GetGridOffset();

        float GetGridOffset()
        {
            switch(this.rotation)
            {
                case 0:
                case 90:
                default:
                    return this.walkingRight ? 0.65f : 0.35f;

                case 180:
                case 270:
                    return this.walkingRight ? 0.35f : 0.65f;
            }
        }

        float integralPart = (float)Math.Truncate(value);
        float decimalValue = Mathf.Abs(value % 1);

        if(gridOffset > 0.50f)
        {
            if(decimalValue > gridOffset - 0.50f)
            {
                return integralPart + gridOffset;
            }
            else
            {
                return (integralPart - 1) + gridOffset;
            }
        }
        else
        {
            if(decimalValue < gridOffset + 0.50f)
            {
                return integralPart + gridOffset;
            }
            else
            {
                return (integralPart + 1) + gridOffset;
            }
        }
    }

    private void StartShoot()
    {
        this.speed = 0f;
        this.shooting = true;
        this.shootTime = 0;
        this.blocksAmount = 0;

        if (this.map.currentTemperature == Map.Temperature.Cold)
        {
            this.animated.PlayOnce(this.animationEnterShoot, () =>
            {
                this.animated.PlayAndLoop(this.animationLoopShoot);
            });
        }
        else
        {
            this.animated.PlayOnce(this.animationEnterShootHot, () =>
            {
                this.animated.PlayAndLoop(this.animationLoopShootHot);
            });
        }
        this.state = State.shooting;

        if (IsShootingHot())
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyTurtlecannonHot,
                position: this.position
            );
        }
        else
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyTurtlecannonCold,
                position: this.position
            );
        }
        
    }

    private const int FlamethrowerTotalTime = 120;

    private void Shooting()
    {
        this.shootTime += 1;
        
        float offsetX = 0.15f * (this.walkingRight ? -1f : 1f);
        float offsetY = 1.50f;

        Vector2 GetOffset()
        {
            switch(this.rotation)
            {
                case 0:
                default:
                    return new Vector2(offsetX, offsetY);

                case 90:
                    return new Vector2(-offsetY, offsetX);

                case 180:
                    return new Vector2(-offsetX, -offsetY);

                case 270:
                    return new Vector2(offsetY, -offsetX);
            }
        }
        Vector2 startPosition = this.position + GetOffset();
        
        var detectRaycast = new Raycast(
            this.map,
            startPosition,
            isCastByEnemy: true,
            ignoreTileTopOnlyFlag: true,
            ignoreItem1: this,
            ignoreItemType: IsShootingHot() ? typeof(TurtleFireBlock) : typeof(SnowBlock)
        );

        float maxDistance = IsShootingHot() ? FlamethrowerLengthLimit : 14f;

        var detectSolid = detectRaycast.Arbitrary(
            startPosition, Up(), maxDistance
        );

        float distanceToSolid = detectSolid.distance;

        float blockSize = IsShootingHot() ? TurtleFireBlock.BlockSize : SnowBlock.BlockSize;
        bool canSpawnMoreBlocks = (blockSize * this.blocksAmount) < distanceToSolid;
        float blockSpawnRate = IsShootingHot() ? 2 : 6;

        int totalNumberBlocks = Mathf.FloorToInt(distanceToSolid / blockSize) + 1;

        if(canSpawnMoreBlocks == true)
        {
            if(this.shootTime % blockSpawnRate == 0)
            {
                Vector2 blockPosition = startPosition + (Up() * (blockSize * this.blocksAmount));
                this.blocksAmount++;
                
                if(IsShootingHot() == true)
                {
                    float t = Mathf.InverseLerp(0, totalNumberBlocks, this.blocksAmount);
                    float fireBlockScale = Mathf.Lerp(0.75f, 1.75f, t);
                    
                    TurtleFireBlock tfBlock =
                        TurtleFireBlock.Create(blockPosition, this.chunk, this.rotation);

                    tfBlock.transform.localScale = Vector3.one * fireBlockScale;
                    tfBlock.lifeTimer = FlamethrowerTotalTime - this.blocksAmount;
                }
                else
                {
                    SnowBlock.Create(blockPosition, this.chunk);
                }
            }
        }
        else
        {
            float totalTime = IsShootingHot() ? FlamethrowerTotalTime : this.shootTime;

            if(this.shootTime >= totalTime)
            {
                EndShoot();
            }
        }
    }

    private void EndShoot()
    {
        this.shooting = false;
        if(this.map.currentTemperature == Map.Temperature.Cold)
        {
            this.animated.PlayAndLoop(this.animationWalk);
        }
        else
        {
            this.animated.PlayAndLoop(this.animationWalkHot);
        }
        this.state = State.walking;
        this.speed = WalkSpeed;
        this.afterShootTimer = AfterShootDelay;
    }

    private bool IsShootingHot()
    {
        return this.shootTemperature == Map.Temperature.Hot;
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        bool isTemperatureHot = (newTemperature == Map.Temperature.Hot);

        if (isTemperatureHot)
        {
            switch (this.state)
            {
                case State.turning:
                    break;
                case State.walking:
                    this.animated.PlayAndLoop(this.animationWalkHot);
                    break;
                case State.shooting:
                    this.animated.PlayAndLoop(this.animationLoopShootHot);
                    break;
                case State.hiding:
                    this.animated.PlayAndLoop(this.animationHideHot);
                    break;
                case State.falling:
                    this.animated.PlayAndLoop(this.animationFallHot);
                    break;
                case State.rising:
                    this.animated.PlayAndLoop(this.animationRiseHot);
                    break;
            }

            this.deathAnimation.frozenSprite = this.frozenSpriteHot;
            this.deathAnimation.debris[0] = this.deathSpriteHot; 
        }
        else
        {
            switch (this.state)
            {
                case State.turning:
                    break;
                case State.walking:
                    this.animated.PlayAndLoop(this.animationWalk);
                    break;
                case State.shooting:
                    this.animated.PlayAndLoop(this.animationLoopShoot);
                    break;
                case State.hiding:
                    this.animated.PlayAndLoop(this.animationHide);
                    break;
                case State.falling:
                    this.animated.PlayAndLoop(this.animationFall);
                    break;
                case State.rising:
                    this.animated.PlayAndLoop(this.animationRise);
                    break;
            }

            this.deathAnimation.frozenSprite = this.frozenSprite;
            this.deathAnimation.debris[0] = this.deathSprite;
        }
    }
}
