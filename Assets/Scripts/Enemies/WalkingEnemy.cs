
using UnityEngine;
using System;
using System.Linq;

public class WalkingEnemy : Enemy
{
    // This implementation will use .hitboxes[0] as the blueprint
    // for the sensor arrangement for detecting walls and floors.
    // You can add additional Hitboxes, but this class won't notice them.

    [NonSerialized] public Vector2 velocity;
    [NonSerialized] public bool facingRight;
    [NonSerialized] public bool onGround;
    private int portalDisableTime = 0;
    protected Tile onGroundTile = null;
    protected float groundAngleDegrees = 0;
    protected float groundSensorSpacing = 0.375f;
    protected bool turnAtEdges = true;
    protected float turnSensorForward = 0;
    protected float edgeTurnSensorForward = 0;
    protected bool trappedHorizontally = false;
    protected bool stopIfTrappedHorizontally = true;
    protected int pauseAfterTurnRemainingTime = 0;
    protected Action<TurnInfo> onTurnAround;
    protected bool canTurn = true;

    public enum TurnReason
    {
        Wall,
        Ledge,
        Player // namely, detecting the player
    }

    public struct TurnInfo
    {
        public float velocityXBefore;
        public bool facingRightBefore;
        public Raycast.CombinedResults? raycastResults;
        public TurnReason turnReason;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.velocity = new Vector2(1 / 16f, 0);
        this.facingRight = !def.tile.flip;
        this.onGround = false;
    }

    public override void Advance()
    {
        if (this.hitboxes == null || this.hitboxes.Length == 0)
        {
            Debug.LogWarning(
                $"WalkingEnemy ({gameObject.name}) won't work because " +
                "hitboxes[0] has not been set up."
            );
        }

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);

        Move(raycast);
        FollowPortals();
        EnemyHitDetectionAgainstPlayer();

        this.transform.position = this.position;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    protected virtual Raycast.CombinedResults SenseHorizontal(
        Raycast raycast, bool right, float distance, out float correctionOffsetX
    )
    {
        var rect = this.hitboxes[0];
        var high = this.position.Add(0, rect.yMax - 0.1f);
        var mid  = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
        var low  = this.position.Add(0, rect.yMin + 0.1f);

        var maxDistance = right ?
            rect.xMax + turnSensorForward + distance :
            -rect.xMin + turnSensorForward + distance;

        var rHigh = raycast.Horizontal(high, right, maxDistance);
        var rMid  = raycast.Horizontal(mid,  right, maxDistance);
        var rLow  = raycast.Horizontal(low,  right, maxDistance);

        if (right)
        {
            if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees < 90) rMid.anything = false;
            if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;
        }
        else
        {
            if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees > 270) rMid.anything = false;
            if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;
        }

        var result = Raycast.CombineClosest(rLow, rMid, rHigh);

        correctionOffsetX = right ?
            result.distance - maxDistance :
            maxDistance - result.distance;
        return result;
    }

    protected virtual bool IsAtEdgeOfPlatform(Raycast raycast, bool right, bool extended)
    {
        if (this.onGround == false) return false;

        var reach = edgeTurnSensorForward + (extended ? 1.125f : 0.125f);
        var xOffset = right ?
            this.hitboxes[0].xMax + reach :
            this.hitboxes[0].xMin - reach;

        var start = this.position + new Vector2(xOffset, 0);

        var portal = this.map.PortalAt(
            Mathf.FloorToInt(start.x + (right ? 0.38f : -0.38f)),
            Mathf.FloorToInt(start.y)
        );
        if (portal != null)
            return false;

        var maxDistance = -this.hitboxes[0].yMin + 20;
        var dx = this.groundSensorSpacing;
        var r1 = raycast.Vertical(start.Add(-dx, 0), false, maxDistance);
        var r2 = raycast.Vertical(start.Add(  0, 0), false, maxDistance);
        var r3 = raycast.Vertical(start.Add( dx, 0), false, maxDistance);

        // we're looking for big steps down or cliffs. if the raycast hits something
        // early, then this is a step up rather than a drop down. we don't want to
        // return true in that case, that's SenseHorizontal's responsibility.
        var minimumDistanceToBeDrop = -this.hitboxes[0].yMin + 0.5f;
        if (r1.distance < minimumDistanceToBeDrop &&
            r2.distance < minimumDistanceToBeDrop &&
            r3.distance < minimumDistanceToBeDrop)
        {
            return false;
        }
        else
        {
            if (r1.distance < minimumDistanceToBeDrop) r1.anything = false;
            if (r2.distance < minimumDistanceToBeDrop) r2.anything = false;
            if (r3.distance < minimumDistanceToBeDrop) r3.anything = false;
        }

        var result = Raycast.CombineClosest(r1, r2, r3);

        // if we see tiles on the ground in front, check whether or not they are
        // topologically connected to what we are standing on. if yes, then it's
        // not a step down, it's just a curve curving down or something, so don't turn.
        if (this.onGroundTile != null && result.tiles.Length > 0)
        {
            var connected = this.onGroundTile.tileGrid.AreTileTopsConnected(
                this.onGroundTile, result.tiles[0]
            );
            if (connected == true)
                return false;
        }

        return
            result.anything == false ||
            result.distance > minimumDistanceToBeDrop;
    }

    protected virtual void Move(Raycast raycast, bool stopHorizontally = false)
    {
        if (stopHorizontally == false)
            MoveHorizontally();
        MoveVertically();

        void MoveHorizontally()
        {
            float forwardAmount = Mathf.Abs(velocity.x);
            if (forwardAmount == 0)
                return;

            this.facingRight = velocity.x > 0;

            var velocityXBefore = this.velocity.x;
            var facingRightBefore = this.facingRight;

            if (this.pauseAfterTurnRemainingTime > 0)
            {
                this.pauseAfterTurnRemainingTime -= 1;
                return;
            }

            this.trappedHorizontally = false;

            var forward = SenseHorizontal(
                raycast, this.facingRight, forwardAmount, out float correctionX
            );
            var turnHere =
                forward.anything == true ||
                (turnAtEdges == true && IsAtEdgeOfPlatform(
                    raycast, this.facingRight, false
                ));

            if (turnHere == false)
            {
                this.position.x += this.facingRight ? forwardAmount : -forwardAmount;
                return;
            }

            if(this.canTurn == false)
            {
                return;
            }

            var backward = SenseHorizontal(
                raycast,
                !this.facingRight,
                forwardAmount + 1,
                out float backwardCorrectionX
            );
            var wouldAlsoTurnGoingBackwards =
                backward.anything == true ||
                (turnAtEdges == true && IsAtEdgeOfPlatform(
                    raycast, !this.facingRight, true
                ));

            if (wouldAlsoTurnGoingBackwards == false ||
                stopIfTrappedHorizontally == false)
            {
                var c = new Item.Collision { otherItem = this };
                if (this.facingRight == true)
                {
                    foreach (var i in forward.items)
                        i.onCollisionFromLeft?.Invoke(c);
                }
                else
                {
                    foreach (var i in forward.items)
                        i.onCollisionFromRight?.Invoke(c);
                }

                this.position.x += correctionX;
                this.velocity.x *= -1;
                this.facingRight = (this.velocity.x > 0);
                this.onTurnAround?.Invoke(new TurnInfo
                {
                    raycastResults = forward,
                    turnReason =
                        forward.anything == true ? TurnReason.Wall : TurnReason.Ledge,
                    velocityXBefore = velocityXBefore,
                    facingRightBefore = facingRightBefore
                });
                this.pauseAfterTurnRemainingTime = 5;
            }
            else
            {
                // stuck (neither direction has any exits)
                this.trappedHorizontally = true;
            }
        }

        void MoveVertically()
        {
            this.onGround = false;
            this.onGroundTile = null;

            var rect = this.hitboxes[0];

            if (this.velocity.y > 0)
            {
                var maxDistance = this.velocity.y + rect.yMax;
                var result = raycast.Vertical(this.position, true, maxDistance);
                if (result.anything == true &&
                    result.distance < this.velocity.y + rect.yMax)
                {
                    this.position.y += result.distance - rect.yMax;
                    this.velocity.y = 0;
                }
                else
                {
                    this.position.y += this.velocity.y;
                }

                this.groundAngleDegrees = 0;
            }
            else if (this.velocity.y < 0)
            {
                var tolerance = 0.1f;
                var start = this.position;
                var maxDistance = -this.velocity.y - rect.yMin + tolerance;
                var dx = this.groundSensorSpacing;
                var result = Raycast.CombineClosest(
                    raycast.Vertical(start.Add(-dx, 0), false, maxDistance),
                    raycast.Vertical(start.Add(  0, 0), false, maxDistance),
                    raycast.Vertical(start.Add( dx, 0), false, maxDistance)
                );
                if (result.anything == true &&
                    result.distance < -this.velocity.y - rect.yMin + tolerance)
                {
                    Vector2 surfaceVelocity = Vector2.zero;
                    if (result.tiles.Length > 0)
                        surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                    foreach (var conveyor in result.items.OfType<Conveyor>())
                    {
                        surfaceVelocity.x = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed;  //0.5f
                        surfaceVelocity.y = 0;
                    }

                    foreach (var raft in result.items.OfType<Raft>())
                    {
                        surfaceVelocity = raft.velocity;
                    }

                    this.position.x += surfaceVelocity.x;
                    this.position.y += surfaceVelocity.y;
                    this.position.y += -result.distance - rect.yMin;
                    this.velocity.y = 0;
                    this.onGround = true;
                    if (result.tiles.Length > 0)
                        this.onGroundTile = result.tiles[0];
                    this.groundAngleDegrees = result.surfaceAngleDegrees;
                }
                else
                {
                    this.position.y += this.velocity.y;
                }
            }
        }
    }

    protected void FollowPortals()
    {
        if (this.portalDisableTime > 0)
        {
            this.portalDisableTime -= 1;
            return;
        }

        var tx = Mathf.FloorToInt(this.position.x / Tile.Size);
        var ty = Mathf.FloorToInt(this.position.y / Tile.Size);
        var portal = this.map.PortalAt(tx, ty);
        if (portal == null) return;
        if (Vector2.Dot(portal.OutDirection(), this.velocity) > 0) return;

        portal.LightUp();
        portal.range.other.leader.LightUp();
        this.position = portal.OtherEndPosition(this.position);
        this.velocity = portal.OtherEndVelocity(this.velocity);
        this.portalDisableTime = 4;
    }

    protected bool IsSafeToJump(
        Raycast raycast, float distanceHorizontal, bool right, bool allowDropDown = false
    )
    {
        var rect = this.hitboxes[0].InWorldSpace();

        if (right)
        {
            var low  = new Vector2(rect.xMax, rect.yMin + 0.2f);
            var mid  = new Vector2(rect.xMax, rect.center.y);
            var high = new Vector2(rect.xMax, rect.yMax - 0.2f);
            var raycastRight = Raycast.CombineClosest(
                raycast.Horizontal(low,  true, distanceHorizontal),
                raycast.Horizontal(mid,  true, distanceHorizontal),
                raycast.Horizontal(high, true, distanceHorizontal)
            );
            if (raycastRight.anything == true)
            {
                if (raycastRight.distance < distanceHorizontal / 2)
                    return false;
                else
                    distanceHorizontal = raycastRight.distance;
            }
            if (allowDropDown == true)
                return true;

            var raycastDown = raycast.Vertical(
                this.position.Add(distanceHorizontal, 0),
                false,
                -this.hitboxes[0].yMin + 0.5f
            );
            return raycastDown.anything == true;
        }
        else
        {
            var low  = new Vector2(rect.xMin, rect.yMin + 0.2f);
            var mid  = new Vector2(rect.xMin, rect.center.y);
            var high = new Vector2(rect.xMin, rect.yMax - 0.2f);
            var raycastRight = Raycast.CombineClosest(
                raycast.Horizontal(low,  false, distanceHorizontal),
                raycast.Horizontal(mid,  false, distanceHorizontal),
                raycast.Horizontal(high, false, distanceHorizontal)
            );
            if (raycastRight.anything == true)
            {
                if (raycastRight.distance < distanceHorizontal / 2)
                    return false;
                else
                    distanceHorizontal = raycastRight.distance;
            }
            if (allowDropDown == true)
                return true;

            var raycastDown = raycast.Vertical(
                this.position.Add(-distanceHorizontal, 0),
                false,
                -this.hitboxes[0].yMin + 0.5f
            );
            return raycastDown.anything == true;

        }
    }
}
