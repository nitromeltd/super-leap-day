
using UnityEngine;
using System.Linq;

public class WildTrunky : WalkingEnemy
{
    public DeathAnimation spearguyDeathAnimation;

    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationShoot;
    public Animated.Animation animationRise;
    public Animated.Animation animationFall;
    public Animated.Animation animationWalkWithRider;
    public Animated.Animation animationTurnWithRider;
    public Animated.Animation animationShootWithRider;
    public Animated.Animation animationRiseWithRider;
    public Animated.Animation animationFallWithRider;

    private int attackCooldownTime;
    private bool shooting = false;
    private bool hasRider = true;
    private float visibleAngleDegrees = 0;
    private bool onGroundLast = false;

    public override void Init(Def def)
    {
        base.Init(def);

        float initialDirectionValue = this.facingRight ? 1f : -1f;
        this.velocity.x = 0.05f * initialDirectionValue;

        this.turnSensorForward = 10 / 16f;
        this.edgeTurnSensorForward = -1;
        this.hasRider = (def.tile.tile.Contains("rider"));
        SetupHitboxes();

        this.animated.PlayAndLoop(AnimationWalk());

        this.onTurnAround = delegate
        {
            this.shooting = false;
            this.animated.PlayOnce(AnimationTurn(), () =>
            {
                this.animated.PlayAndLoop(AnimationWalk());
            });
        };
    }

    private void SetupHitboxes()
    {
        var trunky =
            new Hitbox(-1.5f, 1.5f, -0.875f, 0.875f, this.rotation, Hitbox.NonSolid);
        var rider =
            new Hitbox(-1, 1, -1, 2.75f, this.rotation, Hitbox.NonSolid);

        if (this.hasRider == true)
            this.hitboxes = new [] { trunky, rider };
        else
            this.hitboxes = new [] { trunky };
    }

    private Animated.Animation AnimationWalk() =>
        this.hasRider ? this.animationWalkWithRider : this.animationWalk;
    private Animated.Animation AnimationTurn() =>
        this.hasRider ? this.animationTurnWithRider : this.animationTurn;
    private Animated.Animation AnimationShoot() =>
        this.hasRider ? this.animationShootWithRider : this.animationShoot;
    private Animated.Animation AnimationRise() =>
        this.hasRider ? this.animationRiseWithRider : this.animationRise;
    private Animated.Animation AnimationFall() =>
        this.hasRider ? this.animationFallWithRider : this.animationFall;

    public override void Advance()
    {
        if (this.deathAnimationPlaying == true)
            return;

        if (this.attackCooldownTime > 0)
            this.attackCooldownTime -= 1;

        this.velocity.y -= 0.5f / 16;

        if (this.shooting == false)
        {
            base.Advance();

            var playerRelative = this.map.player.position - this.position;
            var playerForward = playerRelative.x * Mathf.Sign(this.velocity.x);

            if (this.attackCooldownTime == 0 &&
                this.map.player.alive == true &&
                Mathf.Abs(playerRelative.y) < 2 &&
                playerForward > 0 &&
                playerForward < 16 &&
                this.onGround == true)
            {
                Shoot();
            }
        }
        else
        {
            var raycast = new Raycast(this.map, this.position);
            Move(raycast, stopHorizontally: true);
            this.transform.position = this.position;

            EnemyHitDetectionAgainstPlayer();
        }

        if (this.visibleAngleDegrees > this.groundAngleDegrees + 180)
            this.visibleAngleDegrees -= 360;
        if (this.visibleAngleDegrees < this.groundAngleDegrees - 180)
            this.visibleAngleDegrees += 360;
        this.visibleAngleDegrees = Util.Slide(
            this.visibleAngleDegrees, this.groundAngleDegrees, 3
        );
        this.transform.rotation = Quaternion.Euler(0, 0, this.visibleAngleDegrees);

        CheckRiseAndFall();
        SwitchToNearestChunk();
    }

    private void Shoot()
    {
        this.shooting = true;

        this.animated.PlayOnce(AnimationShoot(), delegate
        {
            this.animated.PlayAndLoop(AnimationWalk());
            this.shooting = false;
            this.attackCooldownTime = 50;
        });
        this.animated.OnFrame(8, delegate
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyWildTrunkyAttack,
                position: this.position
            );
        });
        this.animated.OnFrame(10, delegate
        {
            var sign = Mathf.Sign(this.velocity.x);

            var bone = WildTrunkyBone.Create(
                this.position.Add(sign, 0),
                this.chunk
            );
            bone.velocity.x = 3 * sign / 16f;
            bone.velocity.y = -6 / 16f;

            var particle = Particle.CreateAndPlayOnce(
                this.map.player.animationDustJump,
                this.position.Add(sign * 1.9f, -1),
                this.transform.parent
            );
            particle.transform.localScale =
                new Vector3(sign * 0.7f, 0.7f, 1);
        });
    }

    public override void Hit(KillInfo killInfo)
    {
        if (this.hasRider == true)
        {
            this.hasRider = false;
            SetupHitboxes();
            this.animated.PlayAndLoop(AnimationWalk());

            this.shooting = false;

            var sign = Mathf.Sign(this.velocity.x);
            var helmet = HelmutHelmet.Create(
                this.position.Add(-5 * sign / 16f, 40 / 16f),
                this.chunk
            );
            helmet.disableAttackTime = 30;
            helmet.velocity = new Vector4(4 * sign / 16f, 0);

            foreach(var r in this.chunk.items.OfType<EnemyRespawner>())
                r.AddSpawnedEnemy((Enemy)this, (Enemy)helmet);

            StartCoroutine(DeathAnimationCoroutineInPlace(
                this.spearguyDeathAnimation,
                this.position.Add(0, 3),
                this.position.Add(0, 4),
                null
            ));

            // Particle.CreateAndPlayOnce(
            //     Assets.instance.enemyDeath2Animation,
            //     this.position.Add(0, 32),
            //     this.transform.parent
            // );
        }
        else
            base.Hit(killInfo);
    }

    private void CheckRiseAndFall()
    {        
        if(this.onGround == false)
        {
            Animated.Animation animationAirborne = this.velocity.y > 0f ?
                AnimationRise() : AnimationFall();

            this.animated.PlayAndLoop(animationAirborne);
        }
        
        if(this.onGround == true && this.onGroundLast == false)
        {
            this.animated.PlayAndLoop(AnimationWalk());
            
            if(this.shooting == true)
            {
                this.shooting = false;
                this.attackCooldownTime = 50;
            }
        }

        this.onGroundLast = this.onGround;
    }
}
