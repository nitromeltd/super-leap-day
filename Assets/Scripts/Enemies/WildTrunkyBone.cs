
using UnityEngine;
using System;

public class WildTrunkyBone : Enemy
{
    [NonSerialized] public Vector2 velocity;
    private int bounces = 0;

    public static WildTrunkyBone Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.rainyRuins.wildTrunkyBone, chunk.transform);
        obj.name = "Spawned Wild Trunky Bone";
        var item = obj.GetComponent<WildTrunkyBone>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }

    public override void Advance()
    {
        base.Advance();

        this.velocity.y -= 0.5f / 16f;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        MoveHorizontally(raycast);
        MoveVertically(raycast);
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        this.transform.position = this.position;
        this.transform.rotation *= Quaternion.Euler(0, 0, 20);
    }

    protected void MoveHorizontally(Raycast raycast)
    {
        var rect    = this.hitboxes[0];
        var high    = this.position.Add(0, rect.yMax - 1);
        var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
        var low     = this.position.Add(0, rect.yMin + 1);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid,  true, maxDistance),
                raycast.Horizontal(low,  true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
                Particles(new Vector2(result.End().x, this.position.y), 0, 0.6f);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid,  false, maxDistance),
                raycast.Horizontal(low,  false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
                Particles(new Vector2(result.End().x, this.position.y), 0, 0.6f);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }

    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
                Particles(new Vector2(this.position.x, result.End().y), 0.6f, 0);
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.4f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(    0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.4f, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                float conveyorSpeed = 0;
                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                        conveyorSpeed = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed; //0.03
                }
                this.position.x += conveyorSpeed;
                this.position.y += -result.distance - rect.yMin;
                // this.velocity.x *= 0.8f;
                // this.velocity.y *= -0.9f;
                this.velocity.y *= -1;
                if (this.velocity.y > 0.5f)
                    this.velocity.y = 0.5f;
                Particles(new Vector2(this.position.x, result.End().y), 0.6f, 0);
                NotifyBounced();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private void Particles(Vector2 pos, float variationX, float variationY) =>
        Particle.CreateDust(
            this.transform.parent,
            3, 5,
            pos, variationX, variationY,
            Vector2.zero, 0.00625f, 0.00625f
        );

    private void NotifyBounced()
    {
        this.bounces += 1;

        if (this.bounces >= 6)
        {
            this.chunk.items.Remove(this);
            Destroy(this.gameObject);

            foreach (var sprite in Assets.instance.wildTrunkyBoneParticles)
            {
                var particle = Particle.CreateWithSprite(
                    sprite,
                    100,
                    this.position,
                    this.transform.parent
                );
                particle.Throw(
                    new Vector2(0, 7 / 16f), 3 / 16f, 3 / 16f, new Vector2(0, -0.5f / 16f)
                );
            }

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyWildTrunkyBoneBreak,
                position: this.position
            );
        }
        else
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnemyWildTrunkyBoneBounce,
                position: this.position
            );
        }
    }
}
