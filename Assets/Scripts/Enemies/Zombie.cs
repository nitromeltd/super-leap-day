using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Zombie : WalkingEnemy
{
    public Animated.Animation animationWalk;
    public Animated.Animation animationTurn;
    public Animated.Animation animationDrop;
    public Animated.Animation animationFakeDeath;
    public Animated.Animation animationResurrect;
    public Animated.Animation animationSpawn;
    public Animated.Animation animationHide;

    [Header("Pieces")]
    public SpriteRenderer head;
    public SpriteRenderer leftArm;
    public SpriteRenderer rightArm;
    public SpriteRenderer torso;
    public SpriteRenderer leftLeg;
    public SpriteRenderer rightLeg;
    public Transform piecesParent;

    private Vector2 HeadPiecePosition => this.head.transform.position;
    private Vector2 LeftArmPiecePosition => this.leftArm.transform.position;
    private Vector2 RightArmPiecePosition => this.rightArm.transform.position;
    private Vector2 TorsoPiecePosition => this.torso.transform.position;
    private Vector2 LeftLegPiecePosition => this.leftLeg.transform.position;
    private Vector2 RightLegPiecePosition => this.rightLeg.transform.position;

    private float HeadPieceRotation => this.head.transform.rotation.eulerAngles.z;
    private float LeftArmPieceRotation => this.leftArm.transform.rotation.eulerAngles.z;
    private float RightArmPieceRotation => this.rightArm.transform.rotation.eulerAngles.z;
    private float TorsoPieceRotation => this.torso.transform.rotation.eulerAngles.z;
    private float LeftLegPieceRotation => this.leftLeg.transform.rotation.eulerAngles.z;
    private float RightLegPieceRotation => this.rightLeg.transform.rotation.eulerAngles.z;

    private State state = State.None;
    private bool canBeTrulyKilled = false;
    private int fakeDeathTimer = 0;
    private bool hasPieces = false;
    private int spawnFrameNumber = 0;
    
    private const float Gravity = 0.031f;
    private const float WalkSpeed = 0.02f;
    private const int FakeDeathTotalTime = 60 * 5;
    private const int MaxPlayerVerticalDistance = 3;
    private const int MaxPlayerHorizontalDistance = 8;

    private ZombiePiece headPiece;
    private ZombiePiece leftArmPiece;
    private ZombiePiece rightArmPiece;
    private ZombiePiece torsoPiece;
    private ZombiePiece leftLegPiece;
    private ZombiePiece rightLegPiece;
    
    public enum State
    {
        None,
        Walk,
        Drop,
        Turn,
        FakeDeath,
        Resurrect,
        Spawn,
        Hide
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.turnSensorForward = 0.15f;
        this.groundSensorSpacing = 0.70f;

        this.hitboxes = new [] {
            new Hitbox(-0.65f, 0.65f, -1.40f, 0.60f, this.rotation, Hitbox.NonSolid)
        };
        
        this.onTurnAround = OnTurnAround;
        this.turnAtEdges = false;
        this.facingRight = !def.tile.flip;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        
        ChangeState(State.Walk);

        this.GetComponent<SortingGroup>().sortingOrder = GetUniqueSortingOrder();
    }

    private void OnTurnAround(TurnInfo turnInfo)
    {
        ChangeState(State.Turn);
    }
    
    public override void Advance()
    {
        if (this.deathAnimationPlaying == true) return;

        if(this.hasPieces == false)
        {
            CreatePieces();
        }

        switch(this.state)
        {
            case State.Walk:
                AdvanceWalk();
                break;

            case State.Drop:
                AdvanceDrop();
                break;

            case State.Turn:
                AdvanceTurn();
                break;

            case State.FakeDeath:
                AdvanceFakeDeath();
                break;

            case State.Resurrect:
                AdvanceResurrect();
                break;

            case State.Spawn:
                AdvanceSpawn();
                break;
        }

        this.transform.position = this.position;
    }

    public override bool Kill(KillInfo info)
    {
        if(this.canBeTrulyKilled == true ||
            info.isBecauseLevelIsResetting == true ||
            info.itemDeliveringKill is Guillotine)
        {
            return base.Kill(info);
        }
        else
        {
            ChangeState(State.FakeDeath);
            return false;
        }
    }

    private void CreatePieces()
    {
        this.hasPieces = true;
            
        this.headPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.Head, this.head);
        this.headPiece.transform.SetParent(this.piecesParent);
        
        this.leftArmPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.LeftArm, this.leftArm);
        this.leftArmPiece.transform.SetParent(this.piecesParent);
        
        this.rightArmPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.RightArm, this.rightArm);
        this.rightArmPiece.transform.SetParent(this.piecesParent);

        this.torsoPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.Torso, this.torso);
        this.torsoPiece.transform.SetParent(this.piecesParent);

        this.leftLegPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.LeftLeg, this.leftLeg);
        this.leftLegPiece.transform.SetParent(this.piecesParent);

        this.rightLegPiece = ZombiePiece.Create(this.position, this.chunk, ZombiePiece.Type.RightLeg, this.rightLeg);
        this.rightLegPiece.transform.SetParent(this.piecesParent);
        
        this.piecesParent.transform.SetParent(this.transform.parent);
        this.piecesParent.gameObject.name = $"Zombie Pieces from {this.gameObject.name}";
    }

    private void SpawnPieces()
    {
        if(this.hasPieces == false)
        {
            CreatePieces();
        }

        this.piecesParent.gameObject.SetActive(true);

        // Head Piece
        this.headPiece.position = HeadPiecePosition;
        Vector2 headThrownVelocity = new Vector2(
            this.facingRight ? -0.15f : 0.15f, 0.20f
        );
        this.headPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.headPiece.Throw(headThrownVelocity);

        // Left Arm Piece
        Vector2 leftArmThrownVelocity = new Vector2(
            this.facingRight ? -0.07f : 0.07f, 0.15f
        );
        this.leftArmPiece.position = LeftArmPiecePosition;
        this.leftArmPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.leftArmPiece.transform.localRotation =
            Quaternion.Euler(0f, 0f, 0f);
        this.leftArmPiece.Throw(leftArmThrownVelocity);

        // Right Arm Piece
        Vector2 rightArmThrownVelocity = new Vector2(
            this.facingRight ? 0.05f : -0.05f, 0.15f
        );
        this.rightArmPiece.position = RightArmPiecePosition;
        this.rightArmPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.rightArmPiece.transform.localRotation =
            Quaternion.Euler(0f, 0f, -180f);
        this.rightArmPiece.Throw(rightArmThrownVelocity);
        
        // Torso Piece
        Vector2 torsoThrownVelocity = new Vector2(
            this.facingRight ? 0.025f : -0.025f, 0.1f
        );
        this.torsoPiece.position = TorsoPiecePosition;
        this.torsoPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.torsoPiece.transform.localRotation =
            Quaternion.Euler(0f, 0f, this.facingRight ? -90f : 90f);
        this.torsoPiece.Throw(torsoThrownVelocity);

        // Left Leg Piece
        Vector2 leftLegThrownVelocity = new Vector2(
            this.facingRight ? -0.23f : 0.23f, 0.1f
        );
        this.leftLegPiece.position = RightArmPiecePosition;
        this.leftLegPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.leftLegPiece.transform.localRotation =
            Quaternion.Euler(0f, 0f, 180f);
        this.leftLegPiece.Throw(leftLegThrownVelocity);

        // Right Leg Piece
        Vector2 rightLegThrownVelocity = new Vector2(
            this.facingRight ? 0.15f : -0.15f, 0.1f
        );
        this.rightLegPiece.position = RightArmPiecePosition;
        this.rightLegPiece.transform.localScale =
            new Vector3(this.facingRight ? 1f : -1f, 1f, 1f);
        this.rightLegPiece.transform.localRotation =
            Quaternion.Euler(0f, 0f, 0f);
        this.rightLegPiece.Throw(rightLegThrownVelocity);
    }

    private void HidePieces()
    {
        this.piecesParent.gameObject.SetActive(false);

        this.headPiece.Hide();
        this.leftArmPiece.Hide();
        this.rightArmPiece.Hide();
        this.torsoPiece.Hide();
        this.leftLegPiece.Hide();
        this.rightLegPiece.Hide();
    }
    
    public void ChangeState(State newState)
    {
        if(this.state == newState) return;

        switch(this.state)
        {
            case State.Turn:
                ExitTurn();
                break;
        }

        this.state = newState;

        switch(newState)
        {
            case State.Walk:
                EnterWalk();
                break;

            case State.Drop:
                EnterDrop();
                break;

            case State.Turn:
                EnterTurn();
                break;

            case State.FakeDeath:
                EnterFakeDeath();
                break;

            case State.Resurrect:
                EnterResurrect();
                break;

            case State.Hide:
                EnterHide();
                break;

            case State.Spawn:
                EnterSpawn();
                break;
        }
    }
    
    private void AdvanceWalk()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        EnemyHitDetectionAgainstPlayer();

        if(IsDetectingPlayer(raycast))
        {
            bool playerToTheRight = this.map.player.position.x > this.position.x;

            if(playerToTheRight != facingRight)
            {
                this.facingRight = !this.facingRight;

                this.onTurnAround?.Invoke(new TurnInfo
                {
                    raycastResults = null,
                    turnReason = TurnReason.Player
                });
            }
        }
        else if(this.onGround == false)
        {
            ChangeState(State.Drop);
        }
        
#if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.K))
        {
            ChangeState(State.FakeDeath);
        }
#endif
    }

    private bool IsDetectingPlayer(Raycast raycast)
    {
        var headingToPlayer = this.map.player.position - this.position;
        float distanceToPlayer = headingToPlayer.magnitude;
        Vector2 directionToPlayer = headingToPlayer / distanceToPlayer;
        var toPlayer = raycast.Arbitrary(this.position, directionToPlayer, distanceToPlayer);

        if(toPlayer.anything == true)
        {
            return false;
        }

        if(this.map.player.alive == false)
        {
            return false;
        }

        if(Mathf.Abs(headingToPlayer.x) >= MaxPlayerHorizontalDistance)
        {
            return false;
        }
        if(Mathf.Abs(headingToPlayer.y) >= MaxPlayerVerticalDistance)
        {
            return false;
        }

        return true;
    }

    private void AdvanceDrop()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        EnemyHitDetectionAgainstPlayer();
        
        if(this.onGround == true)
        {
            ChangeState(State.Walk);
        }
    }
    
    private void AdvanceTurn()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);

        EnemyHitDetectionAgainstPlayer();
    }

    private void AdvanceFakeDeath()
    {
        this.velocity.y -= Gravity;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
        Move(raycast);
        
        if(this.fakeDeathTimer < FakeDeathTotalTime)
        {
            this.fakeDeathTimer += 1;

            if(this.fakeDeathTimer > FakeDeathTotalTime - 55 &&
                this.fakeDeathTimer < FakeDeathTotalTime - 21)
            {
                this.headPiece.Shake();
                this.leftArmPiece.Shake();
                this.rightArmPiece.Shake();
                this.torsoPiece.Shake();
                this.leftLegPiece.Shake();
                this.rightLegPiece.Shake();
            }

            if(this.fakeDeathTimer > FakeDeathTotalTime - 20)
            {
                this.headPiece.MoveBackTo(HeadPiecePosition, HeadPieceRotation);
                this.leftArmPiece.MoveBackTo(LeftArmPiecePosition, LeftArmPieceRotation);
                this.rightArmPiece.MoveBackTo(RightArmPiecePosition, RightArmPieceRotation);
                this.torsoPiece.MoveBackTo(TorsoPiecePosition, TorsoPieceRotation);
                this.leftLegPiece.MoveBackTo(LeftLegPiecePosition, LeftLegPieceRotation);
                this.rightLegPiece.MoveBackTo(RightLegPiecePosition, RightLegPieceRotation);
            }

            if(this.fakeDeathTimer >= FakeDeathTotalTime)
            {
                HidePieces();
                ChangeState(State.Resurrect);
            }
        }
    }

    private void AdvanceResurrect()
    {
        // blank
    }

    private void AdvanceHide()
    {
        // blank
    }

    private void AdvanceSpawn()
    {
        if(this.map.frameNumber % 10 == 0 &&
            this.map.frameNumber < spawnFrameNumber + (1 * 60))
        {
            Vector2 particlePos = new Vector2(
                this.position.x + Random.Range(-0.50f, 0.50f),
                this.position.y - 1.20f
            );

            Particle dustParticle = Particle.CreateAndPlayOnce(
                Assets.instance.genericDustParticleAnimation,
                particlePos,
                this.transform.parent
            );

            Vector2 averageVelocity = new Vector2(0f, 0.05f);
            dustParticle.Throw(averageVelocity, 0.05f, 0.02f, Vector2.down * 0.005f);
        }
    }
    
    private void EnterWalk()
    {
        this.animated.PlayAndLoop(this.animationWalk);
        this.velocity.x = WalkSpeed * (this.facingRight ? 1f : -1f);
    }
    
    private void EnterDrop()
    {
        this.animated.PlayAndLoop(this.animationDrop);
    }
    
    private void EnterTurn()
    {
        this.animated.PlayOnce(this.animationTurn,
            () =>
            {
                if(this.state == State.Turn)
                {
                    ChangeState(State.Walk);
                }
            }
        );
        this.velocity.x = 0f;
    }

    private void ExitTurn()
    {
        this.transform.localScale = new Vector3(
            this.facingRight ? 1f : -1f, 1f, 1f
        );
    }

    private void EnterFakeDeath()
    {
        SpawnPieces();

        this.animated.PlayOnce(this.animationFakeDeath);
        this.velocity.x = 0f;
        this.fakeDeathTimer = 0;
    }

    private void EnterResurrect()
    {
        this.animated.PlayOnce(this.animationResurrect,
            () => { ChangeState(State.Walk); }
        );

        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxZombieResurrect, null, this.position
        );

        this.velocity = Vector2.zero;
    }

    private void EnterHide()
    {
        this.animated.PlayOnce(this.animationHide);
    }

    private void EnterSpawn()
    {
        this.spawnFrameNumber = Map.instance.frameNumber;

        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxZombieRise, null, this.position
        );

        this.animated.PlayOnce(this.animationSpawn,
            () => { ChangeState(State.Walk); }
        );
    }

    // use this method first if Zombie spawning should be controlled by other element
    public void Hide()
    {
        ChangeState(State.Hide);
    }

    // use this method to make Zombie appear (controlled by other element)
    public void Spawn()
    {
        if(this.state == State.Hide)
            ChangeState(State.Spawn);
    }
}
