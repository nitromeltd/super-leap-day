#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_ANDROID && !UNITY_SWITCH && !UNITY_IOS && !UNITY_TVOS
#define USING_STEAMWORKS_ACHIEVEMENTS
#endif

using System.Collections.Generic;
using UnityEngine;
using MinigameType = minigame.Minigame.Type;

public static class Achievements
{
    public enum Achievement
    {
        TutorialCompleted,
        
        GolfBeginner,
        GolfIntermediate,
        GolfAdvanced,
        GolfExpert,
        
        KartBeginner,
        KartIntermediate,
        KartAdvanced,
        KartExpert,
        
        PinballBeginner,
        PinballIntermediate,
        PinballAdvanced,
        PinballExpert,
        
        FishingBeginner,
        FishingIntermediate,
        FishingAdvanced,
        FishingExpert,
        
        RollingBeginner,
        RollingIntermediate,
        RollingAdvanced,
        RollingExpert,
        
        AbseilingBeginner,
        AbseilingIntermediate,
        AbseilingAdvanced,
        AbseilingExpert,

        Deaths0Death,
        Deaths1Death,
        Deaths5Death,
        Deaths10Death,

        Timer5Minutes,
        Timer6Minutes,
        Timer7Minutes,
        Timer10Minutes,
        Timer15Minutes,
        Timer20Minutes,

        Days1Day,
        Days7Day,
        Days30Day,
        Days90Day,
        Days180Day,
        Days365Day,

        BestBronzeLevel,
        BestSilverLevel,
        BestGoldLevel,
        BestFruitLevel,

        Checkpoint1Checkpoint,
        Checkpoint2Checkpoint,
        Checkpoint3Checkpoint,
        Checkpoint4Checkpoint,
        Checkpoint5Checkpoint,

        None
    }

    public static Dictionary<Achievement, bool> AchievementsStatus;
    private static bool AreAchievementsReady => AchievementsStatus != null;


    public static void Init()
    {
#if UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX
        AchievementsApple.Init();
#elif USING_STEAMWORKS_ACHIEVEMENTS
        AchievementsSteam.Init();
#endif
    }


    public static void CompleteAchievement(Achievement a)
    {
        if (AreAchievementsReady == false)
        {
            Debug.Log($"Can't complete achievement {a}! Achievements not initialized...");
            return;
        }
        
        if (AchievementsStatus.ContainsKey(a) == false)
        {
            Debug.Log($"Can't complete achievement {a}! Achievement not found in achievements list.");
            return;
        }
        
        if (AchievementsStatus[a] == true)
        {
            //Debug.Log($"Can't complete again achievement {achievementName}! This achievement is already completed.");
            return;
        }

#if UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX
        AchievementsApple.CompleteAchievement(a);
#elif USING_STEAMWORKS_ACHIEVEMENTS
        AchievementsSteam.CompleteAchievement(a);
#endif
    }

    public static void CompleteMinigameAchievement(
        minigame.Minigame.Type minigameType,
        MinigameDifficulty minigameDifficulty
    )
    {
        Debug.Log(
            $"Completing minigame achievement with type {minigameType} " +
            $"and difficulty {minigameDifficulty}"
        );

        var achievement = GetMinigameAchievement(minigameType, minigameDifficulty);
        if (achievement != Achievement.None)
            CompleteAchievement(achievement);
    }

    private static Achievement GetMinigameAchievement(
        minigame.Minigame.Type minigameType,
        MinigameDifficulty minigameDifficulty
    ) => (minigameType, minigameDifficulty) switch
    {
        (MinigameType.Golf, MinigameDifficulty.Beginner)     => Achievement.GolfBeginner,
        (MinigameType.Golf, MinigameDifficulty.Intermediate) => Achievement.GolfIntermediate,
        (MinigameType.Golf, MinigameDifficulty.Advanced)     => Achievement.GolfAdvanced,
        (MinigameType.Golf, MinigameDifficulty.Expert)       => Achievement.GolfExpert,

        (MinigameType.Racing, MinigameDifficulty.Beginner)     => Achievement.KartBeginner,
        (MinigameType.Racing, MinigameDifficulty.Intermediate) => Achievement.KartIntermediate,
        (MinigameType.Racing, MinigameDifficulty.Advanced)     => Achievement.KartAdvanced,
        (MinigameType.Racing, MinigameDifficulty.Expert)       => Achievement.KartExpert,

        (MinigameType.Pinball, MinigameDifficulty.Beginner)     => Achievement.PinballBeginner,
        (MinigameType.Pinball, MinigameDifficulty.Intermediate) => Achievement.PinballIntermediate,
        (MinigameType.Pinball, MinigameDifficulty.Advanced)     => Achievement.PinballAdvanced,
        (MinigameType.Pinball, MinigameDifficulty.Expert)       => Achievement.PinballExpert,

        (MinigameType.Fishing, MinigameDifficulty.Beginner)     => Achievement.FishingBeginner,
        (MinigameType.Fishing, MinigameDifficulty.Intermediate) => Achievement.FishingIntermediate,
        (MinigameType.Fishing, MinigameDifficulty.Advanced)     => Achievement.FishingAdvanced,
        (MinigameType.Fishing, MinigameDifficulty.Expert)       => Achievement.FishingExpert,

        (MinigameType.Rolling, MinigameDifficulty.Beginner)     => Achievement.RollingBeginner,
        (MinigameType.Rolling, MinigameDifficulty.Intermediate) => Achievement.RollingIntermediate,
        (MinigameType.Rolling, MinigameDifficulty.Advanced)     => Achievement.RollingAdvanced,
        (MinigameType.Rolling, MinigameDifficulty.Expert)       => Achievement.RollingExpert,

        (MinigameType.Abseiling, MinigameDifficulty.Beginner)     => Achievement.AbseilingBeginner,
        (MinigameType.Abseiling, MinigameDifficulty.Intermediate) => Achievement.AbseilingIntermediate,
        (MinigameType.Abseiling, MinigameDifficulty.Advanced)     => Achievement.AbseilingAdvanced,
        (MinigameType.Abseiling, MinigameDifficulty.Expert)       => Achievement.AbseilingExpert,

        _ => Achievement.None
    };

    public static void CheckTimeAchievement(float timeInSeconds)
    {
        //Debug.Log($"Check TIME Achievements, time in seconds: {timeInSeconds}");

        int seconds = Mathf.FloorToInt(timeInSeconds);

        if (seconds <= 5 * 60)  CompleteAchievement(Achievement.Timer5Minutes);
        if (seconds <= 6 * 60)  CompleteAchievement(Achievement.Timer6Minutes);
        if (seconds <= 7 * 60)  CompleteAchievement(Achievement.Timer7Minutes);
        if (seconds <= 10 * 60) CompleteAchievement(Achievement.Timer10Minutes);
        if (seconds <= 15 * 60) CompleteAchievement(Achievement.Timer15Minutes);
        if (seconds <= 20 * 60) CompleteAchievement(Achievement.Timer20Minutes);
    }

    public static void CheckDaysAchievement(int completedDays)
    {
        //Debug.Log($"Check DAYS Achievements, completed days: {completedDays}");

        if (completedDays >= 365) CompleteAchievement(Achievement.Days365Day);
        if (completedDays >= 180) CompleteAchievement(Achievement.Days180Day);
        if (completedDays >= 90)  CompleteAchievement(Achievement.Days90Day);
        if (completedDays >= 30)  CompleteAchievement(Achievement.Days30Day);
        if (completedDays >= 7)   CompleteAchievement(Achievement.Days7Day);
        if (completedDays >= 1)   CompleteAchievement(Achievement.Days1Day);
    }

    public static void CheckCheckpointAchievement(int skippedCheckpoints)
    {
        //Debug.Log($"Check CHECKPOINT Achievements, skipped checkpoint: {skippedCheckpoints}");

        if (skippedCheckpoints >= 5) CompleteAchievement(Achievement.Checkpoint5Checkpoint);
        if (skippedCheckpoints >= 4) CompleteAchievement(Achievement.Checkpoint4Checkpoint);
        if (skippedCheckpoints >= 3) CompleteAchievement(Achievement.Checkpoint3Checkpoint);
        if (skippedCheckpoints >= 2) CompleteAchievement(Achievement.Checkpoint2Checkpoint);
        if (skippedCheckpoints >= 1) CompleteAchievement(Achievement.Checkpoint1Checkpoint);
    }

    public static void CheckDeathsAchievement(int totalDeaths)
    {
        //Debug.Log($"Check DEATHS Achievements, total deaths: {totalDeaths}");

        if (totalDeaths <= 0)  CompleteAchievement(Achievement.Deaths0Death);
        if (totalDeaths <= 1)  CompleteAchievement(Achievement.Deaths1Death);
        if (totalDeaths <= 5)  CompleteAchievement(Achievement.Deaths5Death);
        if (totalDeaths <= 10) CompleteAchievement(Achievement.Deaths10Death);
    }

    public static void CheckTrophyAchievement(SaveData.Trophy trophyType)
    {
        //Debug.Log($"Check TROPHY Achievements, trophy type: {trophyType}");

        switch (trophyType)
        {
            case SaveData.Trophy.Bronze:
                CompleteAchievement(Achievement.BestBronzeLevel);
                break;

            case SaveData.Trophy.Silver:
                CompleteAchievement(Achievement.BestSilverLevel);
                break;

            case SaveData.Trophy.Gold:
                CompleteAchievement(Achievement.BestGoldLevel);
                break;

            case SaveData.Trophy.Fruit:
                CompleteAchievement(Achievement.BestFruitLevel);
                break;
        }
    }
}
