#if UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX

using System;
using System.Linq;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;
using Achievement = Achievements.Achievement;

public static class AchievementsApple
{
    public static void Init()
    {
        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
        Social.LoadAchievements(OnAchievementsLoaded);
    }

    private static void OnAchievementsLoaded(IAchievement[] completedAchievements)
    {
        Achievements.AchievementsStatus = new();

        foreach (var achievement in (Achievement[])Enum.GetValues(typeof(Achievement)))
        {
            if (achievement == Achievement.None) continue;

            string id = AppleIdForAchievement(achievement);
            bool completed = completedAchievements.Any(_a => _a.id == id);
            Achievements.AchievementsStatus[achievement] = completed;
            Debug.Log($"Retrieved achievement {achievement} | completed = {completed}");
        }
    }

    public static void CompleteAchievement(Achievements.Achievement a)
    {
        IAchievement achievement = Social.CreateAchievement();
        achievement.id = AppleIdForAchievement(a);
        achievement.percentCompleted = 100f;
        achievement.ReportProgress(result =>
        {
            if(result == true)
            {
                Achievements.AchievementsStatus[a] = true;
                Debug.Log($"Successfully completed achievement {achievement.id}");
            }
            else
            {
                Debug.Log($"Failed to complete achievement {achievement.id}");
            }
        });
    }

    public static string AppleIdForAchievement(Achievement a) => a switch
    {
        Achievement.TutorialCompleted => "TutorialCompleted",
        
        Achievement.GolfBeginner => "GolfBeginner",
        Achievement.GolfIntermediate => "GolfIntermediate",
        Achievement.GolfAdvanced => "GolfAdvanced",
        Achievement.GolfExpert => "GolfExpert",
        
        Achievement.KartBeginner => "KartBeginner",
        Achievement.KartIntermediate => "KartIntermediate",
        Achievement.KartAdvanced => "KartAdvanced",
        Achievement.KartExpert => "KartExpert",
        
        Achievement.PinballBeginner => "PinballBeginner",
        Achievement.PinballIntermediate => "PinballIntermediate",
        Achievement.PinballAdvanced => "PinballAdvanced",
        Achievement.PinballExpert => "PinballExpert",
        
        Achievement.FishingBeginner => "FishingBeginner",
        Achievement.FishingIntermediate => "FishingIntermediate",
        Achievement.FishingAdvanced => "FishingAdvanced",
        Achievement.FishingExpert => "FishingExpert",
        
        Achievement.RollingBeginner => "RollingBeginner",
        Achievement.RollingIntermediate => "RollingIntermediate",
        Achievement.RollingAdvanced => "RollingAdvanced",
        Achievement.RollingExpert => "RollingExpert",
        
        Achievement.AbseilingBeginner => "AbseilingBeginner",
        Achievement.AbseilingIntermediate => "AbseilingIntermediate",
        Achievement.AbseilingAdvanced => "AbseilingAdvanced",
        Achievement.AbseilingExpert => "AbseilingExpert",

        Achievement.Deaths0Death => "DeathsCup0Death",
        Achievement.Deaths1Death => "DeathsCup1Death",
        Achievement.Deaths5Death => "DeathsCup5Death",
        Achievement.Deaths10Death => "DeathsCup10Death",

        Achievement.Timer5Minutes => "TimerCup5Minutes",
        Achievement.Timer6Minutes => "TimerCup6Minutes",
        Achievement.Timer7Minutes => "TimerCup7Minutes",
        Achievement.Timer10Minutes => "TimerCup10Minutes",
        Achievement.Timer15Minutes => "TimerCup15Minutes",
        Achievement.Timer20Minutes => "TimerCup20Minutes",

        Achievement.Days1Day => "DaysCup1Day",
        Achievement.Days7Day => "DaysCup7Day",
        Achievement.Days30Day => "DaysCup30Day",
        Achievement.Days90Day => "DaysCup90Day",
        Achievement.Days180Day => "DaysCup180Day",
        Achievement.Days365Day => "DaysCup365Day",

        Achievement.BestBronzeLevel => "BestCupBronze",
        Achievement.BestSilverLevel => "BestCupSilver",
        Achievement.BestGoldLevel => "BestCupGold",
        Achievement.BestFruitLevel => "BestCupFruit",

        Achievement.Checkpoint1Checkpoint => "CheckpointCup1Checkpoint",
        Achievement.Checkpoint2Checkpoint => "CheckpointCup2Checkpoint",
        Achievement.Checkpoint3Checkpoint => "CheckpointCup3Checkpoint",
        Achievement.Checkpoint4Checkpoint => "CheckpointCup4Checkpoint",
        Achievement.Checkpoint5Checkpoint => "CheckpointCup5Checkpoint",

        _ => null
    };
}

#endif
