#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_ANDROID && !UNITY_SWITCH && !UNITY_IOS && !UNITY_TVOS

using Steamworks;
using System;
using System.Collections;
using UnityEngine;
using Achievement = Achievements.Achievement;

public static class AchievementsSteam
{
    private static SteamCoroutineRunner steamCoroutineRunner;

    public static void Init()
    {
        static IEnumerator Sequence()
        {
            SteamUserStats.RequestCurrentStats();

            var callback = new Steam.WaitForCallback<UserStatsReceived_t>();
            yield return callback.Wait();

            Achievements.AchievementsStatus = new();

            foreach (var achievement in (Achievement[])Enum.GetValues(typeof(Achievement)))
            {
                if (achievement == Achievement.None) continue;

                string name = SteamNameForAchievement(achievement);
                SteamUserStats.GetAchievement(name, out var completed);
                Achievements.AchievementsStatus[achievement] = completed;
                Debug.Log($"Retrieved achievement {achievement} | completed = {completed}");
            }
        }

        Steam.CoroutineRunner().StartCoroutine(Sequence());
    }

    public static void CompleteAchievement(Achievements.Achievement a)
    {
        string name = SteamNameForAchievement(a);
        SteamUserStats.SetAchievement(name);
        SteamUserStats.StoreStats();
    }

    public static void ClearAllAchievements()
    {
        foreach (Achievement a in Enum.GetValues(typeof(Achievement)))
        {
            string name = SteamNameForAchievement(a);
            if (name != null)
                SteamUserStats.ClearAchievement(name);
        }

        SteamUserStats.StoreStats();
    }

    public static string SteamNameForAchievement(Achievement a) => a switch
    {
        Achievement.TutorialCompleted => "TutorialCompleted",
        
        Achievement.GolfBeginner => "GolfBeginner",
        Achievement.GolfIntermediate => "GolfIntermediate",
        Achievement.GolfAdvanced => "GolfAdvanced",
        Achievement.GolfExpert => "GolfExpert",
        
        Achievement.KartBeginner => "RacingBeginner",
        Achievement.KartIntermediate => "RacingIntermediate",
        Achievement.KartAdvanced => "RacingAdvanced",
        Achievement.KartExpert => "RacingExpert",
        
        Achievement.PinballBeginner => "PinballBeginner",
        Achievement.PinballIntermediate => "PinballIntermediate",
        Achievement.PinballAdvanced => "PinballAdvanced",
        Achievement.PinballExpert => "PinballExpert",
        
        Achievement.FishingBeginner => "FishingBeginner",
        Achievement.FishingIntermediate => "FishingIntermediate",
        Achievement.FishingAdvanced => "FishingAdvanced",
        Achievement.FishingExpert => "FishingExpert",
        
        Achievement.RollingBeginner => "RollingBeginner",
        Achievement.RollingIntermediate => "RollingIntermediate",
        Achievement.RollingAdvanced => "RollingAdvanced",
        Achievement.RollingExpert => "RollingExpert",
        
        Achievement.AbseilingBeginner => "AbseilingBeginner",
        Achievement.AbseilingIntermediate => "AbseilingIntermediate",
        Achievement.AbseilingAdvanced => "AbseilingAdvanced",
        Achievement.AbseilingExpert => "AbseilingExpert",

        Achievement.Deaths0Death => "Deaths0Death",
        Achievement.Deaths1Death => "Deaths1Death",
        Achievement.Deaths5Death => "Deaths5Death",
        Achievement.Deaths10Death => "Deaths10Death",

        Achievement.Timer5Minutes => "Timer5Minutes",
        Achievement.Timer6Minutes => "Timer6Minutes",
        Achievement.Timer7Minutes => "Timer7Minutes",
        Achievement.Timer10Minutes => "Timer10Minutes",
        Achievement.Timer15Minutes => "Timer15Minutes",
        Achievement.Timer20Minutes => "Timer20Minutes",

        Achievement.Days1Day => "Days1Day",
        Achievement.Days7Day => "Days7Day",
        Achievement.Days30Day => "Days30Day",
        Achievement.Days90Day => "Days90Day",
        Achievement.Days180Day => "Days180Day",
        Achievement.Days365Day => "Days365Day",

        Achievement.BestBronzeLevel => "BestCupBronze",
        Achievement.BestSilverLevel => "BestCupSilver",
        Achievement.BestGoldLevel => "BestCupGold",
        Achievement.BestFruitLevel => "BestCupFruit",

        Achievement.Checkpoint1Checkpoint => "Checkpoint1Checkpoint",
        Achievement.Checkpoint2Checkpoint => "Checkpoint2Checkpoint",
        Achievement.Checkpoint3Checkpoint => "Checkpoint3Checkpoint",
        Achievement.Checkpoint4Checkpoint => "Checkpoint4Checkpoint",
        Achievement.Checkpoint5Checkpoint => "Checkpoint5Checkpoint",

        _ => null
    };
}

public class SteamCoroutineRunner : MonoBehaviour
{
}

#endif