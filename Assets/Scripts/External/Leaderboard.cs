#if (UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX) && !UNITY_EDITOR
#define USING_APPLE_LEADERBOARDS
#elif (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_ANDROID && !UNITY_SWITCH && !UNITY_IOS && !UNITY_TVOS
#define USING_STEAMWORKS_LEADERBOARDS
#endif

using System;
using UnityEngine;

public class Leaderboard
{
    [Serializable]
    public class LeaderboardData
    {
        public DateTime date;
        public string LeaderboardId;
        public int LocalPlayerRank;
        public int LocalPlayerScore;
        public bool Success;
        public int TotalPlayerCount;
        public Entry[] Entries;

        [Serializable]
        public class Entry
        {
            public string PlayerIdentifier;
            public int Rank;
            public int Score;
            
            public void Print()
            {
                Debug.Log($@"PlayerIdentifier: {PlayerIdentifier}, Rank: {Rank}, Score: {Score}");
            }
        }

        public void Print()
        {
            Debug.Log($@"LeaderboardId: {LeaderboardId},
LocalPlayerRank: {LocalPlayerRank},
LocalPlayerScore: {LocalPlayerScore},
Success: {Success},
TotalPlayerCount: {TotalPlayerCount}");

            foreach(var e in Entries)
            {
                e.Print();
            }

            Debug.Log("-------");
        }
        
        public bool HaveEntries()
        {
            if(Entries == null)
            {
                return false;
            }
            
            if(Entries.Length <= 0)
            {
                return false;
            }
            
            return true;
        }
    }

    public static bool AreLeaderboardsAvailableOnThisPlatform()
    {
#if USING_STEAMWORKS_LEADERBOARDS
        return true;
#elif UNITY_EDITOR
        return false;
#elif USING_APPLE_LEADERBOARDS || UNITY_SWITCH
        return true;
#else
        return false;
#endif
    }

    public static bool IsLeaderboardDateAvailable(DateTime date)
    {
#if USING_APPLE_LEADERBOARDS
        return LeaderboardApple.IsLeaderboardDateAvailable(date);
#else
        return true;
#endif
    }

    public static void ReportUserTime(float timeInSeconds, DateTime date)
    {
        Achievements.CheckTimeAchievement(timeInSeconds);

#if USING_APPLE_LEADERBOARDS
        LeaderboardApple.ReportUserTime(timeInSeconds, date);
#elif USING_STEAMWORKS_LEADERBOARDS
        LeaderboardSteam.ReportUserTime(timeInSeconds, date);
#elif UNITY_SWITCH
        LeaderboardSwitch.ReportUserTime(timeInSeconds, date);
#endif
    }

    public static void FetchLeaderboardData(DateTime date, Action<LeaderboardData> callback = null)
    {
#if USING_APPLE_LEADERBOARDS
        LeaderboardApple.FetchLeaderboardData(date, callback);
#elif USING_STEAMWORKS_LEADERBOARDS
        LeaderboardSteam.FetchLeaderboardData(date, callback);
#elif UNITY_SWITCH
        LeaderboardSwitch.FetchLeaderboardData(date, callback);
#endif
    }
}
