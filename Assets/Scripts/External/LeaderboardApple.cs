#if UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS

using System;
using System.Runtime.InteropServices;
using UnityEngine;
using LeaderboardData = Leaderboard.LeaderboardData;

public static class LeaderboardApple
{
    private static DateTime FirstValidLeaderboardDate = new DateTime(2021, 07, 02);
    private const int NumberOfActiveLeaderboards = 30;
    public static Action<LeaderboardData> OnLoadLeaderboardData = null;
    
    public static void Init()
    {
		gameCenter_setLoadLeaderboardCallbackHandler(
            GameCenter_LoadLeaderboardCallbackHandler
        );
		gameCenter_setSubmitScoreLeaderboardCallbackHandler(
            GameCenter_SubmitScoreLeaderboardCallbackHandler
        );
    }

    // we use a system of N recurring leaderboards so
    // dates older than one week are deemed unavailable
    public static bool IsLeaderboardDateAvailable(DateTime dateTime)
    {
        return dateTime > DateTime.Today.AddDays(-NumberOfActiveLeaderboards)
            && dateTime >= FirstValidLeaderboardDate;
    }
    
    public static string GetAppleLeaderboardIdFromDate(DateTime date)
    {
        if (IsLeaderboardDateAvailable(date) == false)
            return null;

        int dayNumber = ((date - FirstValidLeaderboardDate).Days % 30) + 1;
        dayNumber = Mathf.Clamp(dayNumber, 1, 30);
        return $"MonthlyDay{dayNumber}";
    }

    public static void ReportUserTime(float timeInSeconds, DateTime date)
    {
        string leaderboardID = GetAppleLeaderboardIdFromDate(date);
        int timeInHundredthOfSecond = Mathf.RoundToInt(timeInSeconds * 100f);
        
        if (string.IsNullOrEmpty(leaderboardID))
        {
            Debug.Log("Report User Time: Leaderboard ID not found! Aborting...");
            return;
        }

        gameCenter_submitScoreLeaderboard(timeInHundredthOfSecond, leaderboardID);
    }

    public static void FetchLeaderboardData(DateTime date, Action<LeaderboardData> callback = null)
    {
        string leaderboardID = GetAppleLeaderboardIdFromDate(date);

        if (string.IsNullOrEmpty(leaderboardID))
        {
            Debug.Log("Fetch Leaderboard Data: Leaderboard ID not found! Aborting...");
            return;
        }

        OnLoadLeaderboardData = (data) => {
            data.date = date;
            callback.Invoke(data);
        };
        gameCenter_loadLeaderboard(leaderboardID);
    }

    #if UNITY_STANDALONE_OSX
    private const string dllSource = "LD2ExternalOSX";
    #else
    private const string dllSource = "__Internal";
    #endif

    // Link to native functions
    [DllImport(dllSource)]
	static extern void gameCenter_setLoadLeaderboardCallbackHandler(GameCenter_LoadLeaderboardCallback handler);
    [DllImport(dllSource)]
	static extern void gameCenter_setSubmitScoreLeaderboardCallbackHandler(GameCenter_SubmitScoreLeaderboardCallback handler);

	[DllImport(dllSource)]
	static extern void gameCenter_loadLeaderboard([MarshalAs(UnmanagedType.LPStr)]string leaderboardId);
    [DllImport(dllSource)]
	static extern void gameCenter_submitScoreLeaderboard(int score, [MarshalAs(UnmanagedType.LPStr)]string leaderboardId);

	delegate void GameCenter_LoadLeaderboardCallback(string str);
	delegate void GameCenter_SubmitScoreLeaderboardCallback(string str);

	[AOT.MonoPInvokeCallback(typeof(GameCenter_LoadLeaderboardCallback))]
	static void GameCenter_LoadLeaderboardCallbackHandler(string str)
	{
		Debug.Log($"Loading Leaderboard, Message received: {str}");

        if(string.IsNullOrEmpty(str) == false)
        {
            var leaderboardData = GetDeserializedLeaderboardData(str);
            OnLoadLeaderboardData?.Invoke(leaderboardData);
        }
	}

	[AOT.MonoPInvokeCallback(typeof(GameCenter_SubmitScoreLeaderboardCallback))]
	static void GameCenter_SubmitScoreLeaderboardCallbackHandler(string str)
	{
		Debug.Log($"Submitting Score, Message received: {str}");
	}

    public static LeaderboardData GetDeserializedLeaderboardData(string serializedData)
    {
        return JsonUtility.FromJson<LeaderboardData>(serializedData);
    }

    /*
    // built-in Apple's UI for leaderboards
    public static void ShowLeaderboard(DateTime date, TimeScope timeScope = TimeScope.AllTime)
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) 
        string leaderboardID = GetiOSLeaderboardIdFromDate(date);

        if(string.IsNullOrEmpty(leaderboardID))
        {
            Debug.Log("Show Leaderboard UI: Leaderboard ID not found! Aborting...");
            return;
        }

        UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowLeaderboardUI(
            leaderboardID, timeScope
        );
#endif
    }
    */
}

#endif
