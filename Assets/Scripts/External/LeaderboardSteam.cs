#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_ANDROID && !UNITY_SWITCH && !UNITY_IOS && !UNITY_TVOS

using System;
using System.Collections;
using Steamworks;
using UnityEngine;
using LeaderboardData = Leaderboard.LeaderboardData;

public static class LeaderboardSteam
{
    public static string GetSteamLeaderboardIdFromDate(DateTime date)
    {
        return "Leaderboard" + date.ToString("yyyy_MM_dd");
    }

    public static void ReportUserTime(float timeInSeconds, DateTime date)
    {
        IEnumerator Sequence()
        {
            int timeInMilliseconds = Mathf.RoundToInt(timeInSeconds * 1000);
            string leaderboardName = GetSteamLeaderboardIdFromDate(date);
            Debug.Log(
                $"Submitting score {timeInMilliseconds} to " +
                $"Steam leaderboard {leaderboardName}"
            );

            var find = SteamUserStats.FindOrCreateLeaderboard(
                leaderboardName,
                ELeaderboardSortMethod.k_ELeaderboardSortMethodAscending,
                ELeaderboardDisplayType.k_ELeaderboardDisplayTypeTimeMilliSeconds
            );
            var r = new Steam.WaitForCallResult<LeaderboardFindResult_t>();
            yield return r.Wait(find);
            if (r.result.m_bLeaderboardFound != 1 || r.ioFailure == true)
            {
                Debug.Log("Failed to find/create steam leaderboard (stopping here).");
                yield break;
            }

            var leaderboard = r.result.m_hSteamLeaderboard;

            var upload = SteamUserStats.UploadLeaderboardScore(
                leaderboard,
                ELeaderboardUploadScoreMethod.k_ELeaderboardUploadScoreMethodKeepBest,
                timeInMilliseconds,
                new int[0], 0
            );
            var r2 = new Steam.WaitForCallResult<LeaderboardScoreUploaded_t>();
            yield return r2.Wait(upload);
            if (r2.result.m_bSuccess != 1 || r2.ioFailure == true)
            {
                Debug.Log("Failed to upload score to steam leaderboard.");
                yield break;
            }
        }

        Steam.CoroutineRunner().StartCoroutine(Sequence());
    }

    public static void FetchLeaderboardData(DateTime date, Action<LeaderboardData> callback = null)
    {
        IEnumerator Sequence()
        {
            string leaderboardName = GetSteamLeaderboardIdFromDate(date);

            var find = SteamUserStats.FindLeaderboard(leaderboardName);
            var result = new Steam.WaitForCallResult<LeaderboardFindResult_t>();
            yield return result.Wait(find);
            if (result.result.m_bLeaderboardFound != 1 || result.ioFailure == true)
            {
                Debug.Log("Couldn't retrieve leaderboard info from Steam.");
                yield break;
            }

            var leaderboard = result.result.m_hSteamLeaderboard;

            var download = SteamUserStats.DownloadLeaderboardEntries(
                leaderboard, ELeaderboardDataRequest.k_ELeaderboardDataRequestGlobal, 1, 20
            );
            var downloadResult = new Steam.WaitForCallResult<LeaderboardScoresDownloaded_t>();
            yield return downloadResult.Wait(download);
            if (downloadResult.result.m_cEntryCount == 0 || result.ioFailure == true)
            {
                Debug.Log("Couldn't retrieve leaderboard entries from Steam.");
                yield break;
            }

            var data = new LeaderboardData
            {
                date = date,
                LeaderboardId = leaderboardName,
                Success = true,
                Entries = new LeaderboardData.Entry[downloadResult.result.m_cEntryCount]
            };

            for (int n = 0; n < data.Entries.Length; n += 1)
            {
                SteamUserStats.GetDownloadedLeaderboardEntry(
                    downloadResult.result.m_hSteamLeaderboardEntries,
                    n + 1, out var entry, new int[0], 0
                );
                data.Entries[n] = new();
                data.Entries[n].PlayerIdentifier =
                    SteamFriends.GetFriendPersonaName(entry.m_steamIDUser);
                data.Entries[n].Score = entry.m_nScore / 10;
                data.Entries[n].Rank = n + 1;
            }

            callback.Invoke(data);
        }

        Steam.CoroutineRunner().StartCoroutine(Sequence());
    }
}

#endif
