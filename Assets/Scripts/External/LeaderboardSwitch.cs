#if UNITY_SWITCH

using nn.npln.leaderboard;
using System;
using System.Linq;
using UnityEngine;
using LeaderboardData = Leaderboard.LeaderboardData;

public static class LeaderboardSwitch
{
    public static (string categoryType, int category)?
        GetSwitchLeaderboardIdFromDate(DateTime date)
    {
        if (Leaderboard.IsLeaderboardDateAvailable(date) == false)
            return null;

        string categoryName;
        if (date.Month % 2 == 0)
            categoryName = $"MonthEvenDay{date.Day}";
        else
            categoryName = $"MonthOddDay{date.Day}";

        return (categoryName, 1);
    }

    public static void ReportUserTime(float timeInSeconds, DateTime date)
    {
        async void Sequence()
        {
            var board = GetSwitchLeaderboardIdFromDate(date);
            if (board == null)
                return;

            var client = new LeaderboardClient(Switch.userContext);

            {
                var request = new SetHolderDataRequest();
                request.SetDisplayName(Switch.GetUserDetails().nickname);
                await client.SetHolderDataAsync(request);
            }

            {
                var categoryName = nn.npln.leaderboard.Methods.CreateCategoryName(
                    board.Value.categoryType, board.Value.category
                );
                var leaderboardRef = client.CreateLeaderboardReference(categoryName);

                // store the score remotely as hundredths of seconds.
                var score = Mathf.RoundToInt(timeInSeconds * 100);

                Debug.Log($"setting score to {score} in leaderboard {categoryName}");

                var request = new SetScoreRequest();
                request.SetScore(score);

                var result = await leaderboardRef.SetScoreAsync(request);
                if (result.IsOk() == true)
                    Debug.Log("submitted score to leaderboard ... ok!");
                else
                    Debug.Log($"unable to submit score to leaderboard ... {result}");
            }
        }
        Sequence();
    }

    public static void FetchLeaderboardData(DateTime date, Action<LeaderboardData> callback = null)
    {
        async void Sequence()
        {
            var board = GetSwitchLeaderboardIdFromDate(date);
            if (board == null)
                return;

            var client = new LeaderboardClient(Switch.userContext);
            var categoryName = nn.npln.leaderboard.Methods.CreateCategoryName(
                board.Value.categoryType, board.Value.category
            );
            var leaderboardRef = client.CreateLeaderboardReference(categoryName);

            var request = new GetRangeLeaderboardRequest();
            request.SetPageSize(10);
            request.SetOffset(0);

            var result = await leaderboardRef.GetLeaderboardAsync(request);
            if (result.IsOk() == false)
            {
                Debug.Log($"failed to get leaderboard {categoryName} ... {result}");
                return;
            }

            var snapshot = result.GetResponse();
            var list = snapshot.GetScoreDataList().ToArray();

            var data = new LeaderboardData
            {
                date = date,
                LeaderboardId = categoryName,
                Success = true,
                Entries = list.Select(e => new LeaderboardData.Entry
                {
                    PlayerIdentifier = e.GetHolderData().GetDisplayName(),
                    Rank = e.GetRankData().GetRank(),
                    Score = (int)e.GetScore()
                }).ToArray()
            };

            callback.Invoke(data);
        }
        Sequence();
    }
}

#endif
