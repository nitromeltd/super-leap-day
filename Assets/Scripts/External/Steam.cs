#if (UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN) && !UNITY_ANDROID && !UNITY_SWITCH && !UNITY_IOS && !UNITY_TVOS

using Steamworks;
using System.Collections;
using UnityEngine;

public static class Steam
{
    // A permanent place for steam coroutines to be runnable which won't be unloaded.

    private static SteamCoroutineRunner steamCoroutineRunner = null;

    public static SteamCoroutineRunner CoroutineRunner()
    {
        if (steamCoroutineRunner == null)
        {
            var go = new GameObject("Steam Coroutines");
            GameObject.DontDestroyOnLoad(go);
            steamCoroutineRunner = go.AddComponent<SteamCoroutineRunner>();
        }
        return steamCoroutineRunner;
    }

    public class SteamCoroutineRunner : MonoBehaviour
    {
    }

    // the Steam principle of callbacks is just not very Unity-friendly.
    // these classes makes it a little less of a mouthful.
    // they're designed for being used in a coroutine.

    public class WaitForCallback<T> where T : struct
    {
        public T result;
        public bool received = false;

        public IEnumerator Wait()
        {
            var callback = Callback<T>.Create((result) =>
            {
                this.result = result;
                this.received = true;
            });

            // impose a timeout of 15 sec so that the caller coroutine can't get jammed
            for (int n = 0; this.received == false && n < 900; n += 1)
            {
                SteamAPI.RunCallbacks();
                yield return null;
            }
        }
    }

    public class WaitForCallResult<T> where T : struct
    {
        public T result = default;
        public bool ioFailure = true;

        public IEnumerator Wait(SteamAPICall_t call)
        {
            bool receivedResponse = false;

            var callResult = CallResult<T>.Create();
            callResult.Set(call, (_result, _ioFailure) =>
            {
                this.result = _result;
                this.ioFailure = _ioFailure;
                receivedResponse = true;
            });

            // impose a timeout of 15 sec so that the caller coroutine can't get jammed
            for (int n = 0; receivedResponse == false && n < 900; n += 1)
            {
                SteamAPI.RunCallbacks();
                yield return null;
            }
        }
    }
}

#endif
