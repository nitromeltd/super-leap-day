#if UNITY_SWITCH

using nn.account;
using nn.npln;
using UnityEngine;
using UnityEngine.Switch;

public static class Switch
{
    public const string tenantIdSource = "t-aa98c31e";
    private static bool initialized = false;
    private static bool hasEnteredNetworkConnecting = false;
    public static UserHandle userHandle;
    public static UserContext userContext;
    private static ExecutorHolder executorHolder;

    public static void Initialize()
    {
        if (initialized == true)
            return;

        if (SubmitNetworkRequestAndWait() == false) // <-- CHECK: this looks very wrong
        {
            Debug.Log("CHRIS returning network request failed.");
            return;
        }
  
        userHandle = new UserHandle();
        Account.Initialize();
        Account.TryOpenPreselectedUser(ref userHandle);

        executorHolder = ExecutorHolder.Create();
        userContext = UserContext.Create(
            tenantIdSource,
            NsaIdTokenRetriever.Create(Switch.userHandle),
            UserConfig.CreateForPrearrangedUser(0),
            executorHolder
        );

        // Task.Run(async () => {
        //     var res = await NsaIdTokenRetriever.EnsureNetworkServiceAccountAvailableAsync(
        //         userHandle, executorHolder.GetExecutor(), new ThreadConfig()
        //     );
        //     Debug.Log("CHRIS result = " + res);

        //     var result = await userContext.WaitForAuthenticationAsync();
        //     if (result.IsOk() == false)
        //     {
        //         Debug.Log($"WaitForAuthenticationAsync() failed -- {result}");
        //         Debug.Log($"status code = {result.GetStatusCode()}");
        //         Debug.Log($"detail nn result = {result.GetDetailNnResult()}");
        //         Debug.Log($"message = {result.GetErrorMessage()}");
        //         return;
        //     }

        //     Debug.Log("WaitForAuthenticationAsync() success!");
        // });

        Debug.Log("userContext = " + userContext);

        initialized = true;
    }
    
    public static (Uid uid, string nickname) GetUserDetails()
    {
        Uid uid = default;
        Nickname nickname = default;

        var result = Account.GetUserId(ref uid, userHandle);
        if (result.IsSuccess() == false)
            return (default, null);

        result = Account.GetNickname(ref nickname, uid);
        if (result.IsSuccess() == false)
            return (uid, null);

        return (uid, nickname.ToString());
    }

    ///  <summary>
    ///  Submits a network request and waits until connection processing is complete.
    ///  </summary>
    ///  <returns><tt>true</tt> if the network is available.</returns>
    public static bool SubmitNetworkRequestAndWait()
    {
        if (!hasEnteredNetworkConnecting)
        {
            if (!NetworkInterfaceWrapper.EnterNetworkConnecting())
            {
                Debug.LogWarning("Cannot submit network request.");
                return false;
            }
            hasEnteredNetworkConnecting = true;
        }

        NetworkInterfaceWrapper.WaitForNetworkConnecting();

        if (!NetworkInterfaceWrapper.IsNetworkAvailable())
        {
            Debug.LogWarning("Network unavailable.");
            CancelNetworkRequest();
            return false;
        }

        return true;
    }

    ///  <summary>
    ///  Withdraws a submitted network connection request.
    ///  </summary>
    public static void CancelNetworkRequest()
    {
        if (hasEnteredNetworkConnecting)
        {
            NetworkInterfaceWrapper.LeaveNetworkConnecting();
            hasEnteredNetworkConnecting = false;
        }
    }

    ///  <summary>
    ///  Opens the user account used in the sample program.
    ///  </summary>
    public static bool TryOpenUser(out nn.account.UserHandle userHandle)
    {
        nn.account.Account.Initialize();

        userHandle = default;
        if (nn.account.Account.TryOpenPreselectedUser(ref userHandle))
        {
            return true;
        }

#if NN_ACCOUNT_OPENUSER_ENABLE
        Debug.LogWarning($"{nameof(nn.account.Account.TryOpenPreselectedUser)}() failed. Will try to open the first discovered user instead.");

        var uids = new nn.account.Uid[nn.account.Account.UserCountMax];
        var length = default(int);
        {
            var result = nn.account.Account.ListAllUsers(ref length, uids);
            if (!result.IsSuccess())
            {
                Debug.LogWarning($"{nameof(nn.account.Account.ListAllUsers)}() failed ({result}).");
                return false;
            }
        }
        if (length == 0)
        {
            Debug.LogWarning("No user found on the DevKit.");
            return false;
        }
        {
            var result = nn.account.Account.OpenUser(ref userHandle, uids[0]);
            if (!result.IsSuccess())
            {
                Debug.LogWarning($"{nameof(nn.account.Account.OpenUser)}() failed ({result}).");
                return false;
            }
        }
        return true;
#else
        Debug.LogWarning($"{nameof(nn.account.Account.TryOpenPreselectedUser)}() failed.");
        return false;
#endif
    }
}

#endif
