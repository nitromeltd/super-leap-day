using UnityEngine;
using System.Runtime.InteropServices;
using System;

namespace Apple.Arcade
{
    public class FairPlay
    {
        #if UNITY_EDITOR || UNITY_STANDALONE_OSX
        [DllImport("FairPlayBundleWrapper")]
        private static extern void DoRegisterAppWithFairPlay();
        #endif

        public static void RegisterWithFairPlay()
        {
            #if UNITY_EDITOR || UNITY_STANDALONE_OSX
            try
            {
                DoRegisterAppWithFairPlay();
            }
            catch (Exception e)
            {
                Debug.LogException(e);
                Application.Quit();
            }
            #endif
        }
    }
}
