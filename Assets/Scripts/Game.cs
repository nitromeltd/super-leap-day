using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;
using System.Linq;

public class Game : MonoBehaviour
{
    public static DateTime selectedDate = DateTime.Now.Date;

    public static DateTime DateOfOpeningTutorial => DateFirstAllowed.AddDays(-1);
    public static DateTime DateFirstAllowed      => new DateTime(2021, 8, 5);
    public static DateTime DateLastAllowed       => DateTime.Now.Date;

    public static bool IsOpeningTutorialSelected => selectedDate == DateOfOpeningTutorial;

    public static Game instance;

    private LevelGeneration.Row[] rows;
    [NonSerialized] public Map map1;
    [NonSerialized] public Map map2;
    [NonSerialized] public Map map3;
    [NonSerialized] public Map map4;
    [NonSerialized] public Map[] maps;
    [NonSerialized] public bool isFullyLoaded = false;
    [NonSerialized] public bool isLeaving = false;
    [NonSerialized] public MultiplayerLastPlayerCutoff multiplayerLastPlayerCutoff = new();
    private float accumulatedTime;
    private float approximateTimeSpentInMapAdvance;
    private Coroutine coroutineForMapNumberChange;
    private static bool didRandomlySelectSave = false;

    private int framesThisSecondSoFar;
    private int framesLastSecond;
    private int realtimeSecondLastUpdate;
    public int Fps => this.framesLastSecond;

    public void Start()
    {
        if (didRandomlySelectSave == false)
        {
            // RandomlySelectSave();
        }

        InitScene();
        instance = this;
        Assets.Init();
        LevelGeneration.Init();
        LevelPatching.Init();

        var theme = Theme.RainyRuins;
        LevelGeneration.Row[] rows = null;

        switch (DebugPanel.selectedMode)
        {
            case DebugPanel.Mode.EditorRainyRuins:
                theme = Theme.RainyRuins;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorCapitalHighway:
                theme = Theme.CapitalHighway;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorConflictCanyon:
                theme = Theme.ConflictCanyon;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorHotNColdSprings:
                theme = Theme.HotNColdSprings;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorGravityGalaxy:
                theme = Theme.GravityGalaxy;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorSunkenIsland:
                theme = Theme.SunkenIsland;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorTreasureMines:
                theme = Theme.TreasureMines;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorWindySkies:
                theme = Theme.WindySkies;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorTombstoneHill:
                theme = Theme.TombstoneHill;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorMoltenFortress:
                theme = Theme.MoltenFortress;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorVRTrainingRoom:
                theme = Theme.VRTrainingRoom;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.EditorOpeningTutorial:
                theme = Theme.OpeningTutorial;
                rows = LevelGeneration.GenerateTestArenaFromLevelTestingServer(theme);
                break;

            case DebugPanel.Mode.Algorithm:
            case DebugPanel.Mode.Algorithm2PSplitScreen: {
                var dateWithOffset =
                    Game.selectedDate.AddDays(SaveData.GetFuturePlayOffset()).Date;
                (theme, rows) = LevelGeneration.FetchOrGenerate(dateWithOffset);
                break;
            }

            case DebugPanel.Mode.AlgorithmRegenerate: {
                var dateWithOffset =
                    Game.selectedDate.AddDays(SaveData.GetFuturePlayOffset()).Date;
                (theme, rows) = LevelGeneration.Generate(dateWithOffset);
                break;
            }

            case DebugPanel.Mode.GenerateGravityGalaxyLevel: {
                theme = Theme.GravityGalaxy;
                rows = LevelGeneration.GenerateWithForcedTheme(
                    Game.selectedDate, theme
                );
                break;
            }

            case DebugPanel.Mode.GenerateTombstoneHillLevel: {
                theme = Theme.TombstoneHill;
                rows = LevelGeneration.GenerateWithForcedTheme(
                    Game.selectedDate, theme
                );
                break;
            }

            case DebugPanel.Mode.GenerateMoltenFortressLevel: {
                theme = Theme.MoltenFortress;
                rows = LevelGeneration.GenerateWithForcedTheme(
                    Game.selectedDate, theme
                );
                break;
            }

            case DebugPanel.Mode.KiavikGravityChunks: {
                var chunks = new [] {
                    "Gravity Galaxy Test/Kiavik_test1",
                    "Gravity Galaxy Test/Kiavik_test2",
                    "Gravity Galaxy Test/Kiavik_test3",
                    "Gravity Galaxy Test/Kiavik_test4",
                    "Gravity Galaxy Test/Kiavik_test5",
                    "Gravity Galaxy Test/Kiavik_test6",
                    "Gravity Galaxy Test/Kiavik_test7",
                    "Gravity Galaxy Test/Kiavik_test8",
                    "Gravity Galaxy Test/Kiavik_test9",
                    "Gravity Galaxy Test/Kiavik_test10",
                    "Gravity Galaxy Test/Kiavik_test11",
                    "Gravity Galaxy Test/Kiavik_test12"
                }.Select(
                    name => LoadChunkByName(name)
                ).ToArray();

                theme = Theme.GravityGalaxy;
                rows = LevelGeneration.GenerateTestArena(
                    chunks, Theme.GravityGalaxy
                );
                break;
            }

            case DebugPanel.Mode.AppleDemo:
                rows = new [] {
                    "start",
                    "stage 1", "stage 2", "stage 3", "stage 4", "stage 5",
                    "stage 6", "stage 7", "stage 8", "stage 9", "stage 10",
                    "stage 11", "stage 12", "stage 13", "stage 14", "stage 15",
                    "stage endzone"
                }.Select(
                    name => new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData { filename = name },
                        flipHorizontally = true
                    }
                ).ToArray();
                break;

            case DebugPanel.Mode.TestChunks:
                rows = new [] {
                    "start",
                    "Technical/connector_center_to_wall", "Technical/side_tunnel",
                    "marcin_htombstonehill_example_shrine_3",
                    "Technical/side_tunnel", "Technical/connector_wall_to_center",
                }.Select(
                    name => new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData { filename = name },
                        flipHorizontally = false
                    }
                ).ToArray();
                theme = Theme.TombstoneHill;
                break;
                
            case DebugPanel.Mode.ChunkReviewPack:
            {
                theme =
                    DebugPanel.selectedReviewPack.isGeneric ?
                    Util.RandomChoice(Themes.all) :
                    DebugPanel.selectedReviewPack.specificToTheme;
                rows = LevelGeneration.GenerateTestArena(
                    DebugPanel.selectedReviewPack, theme
                );
                break;
            }
            
            case DebugPanel.Mode.TombstoneHillChunkExamples:
                rows = new [] {
                    "start",
                    "marcin_tombstonehill_example_ghost_2", "checkpoint",
                    "marcin_tombstonehill_example_shrine_3", "checkpoint",
                    "marcin_tombstonehill_example_shrine_4", "checkpoint",
                    "marcin_tombstonehill_example_shrine_5", "checkpoint",
                    "marcin_tombstonehill_example_statue_2", "checkpoint",
                    "Technical/connector_center_to_wall", "Technical/side_tunnel", "marcin_htombstonehill_example_ghost_1", 
                    "Technical/side_tunnel", "marcin_htombstonehill_example_shrine_1", 
                    "Technical/side_tunnel", "marcin_htombstonehill_example_shrine_3", 
                    "Technical/side_tunnel", "marcin_htombstonehill_example_statue_1", 
                    "Technical/side_tunnel", "Technical/connector_wall_to_wide",
                    "gabriel_test_tombstonehill_easy_guillotine", "checkpoint",
                    "gabriel_test_tombstonehill_easy_zombie", "checkpoint",
                    "gabriel_test_tombstonehill_easy_zombiehand", "checkpoint",
                    "gabriel_test_tombstonehill_med_sticky", "checkpoint",
                    "gabriel_test_tombstonehill_med_zombiehand", "checkpoint",
                    "gabriel_test_tombstonehill_tutorial_sticky", "checkpoint",
                    "gabriel_test_tombstonehill_tutorial_zombie", "checkpoint",
                    
                }.Select(
                    name => new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData { filename = name }
                    }
                ).ToArray();
                theme = Theme.TombstoneHill;
                break;

            case DebugPanel.Mode.GravityAtmosphereExamples:
                rows = new [] {
                    "start",
                    "Technical/airlock",
                    "gravity_atmosphere_1",
                    "Technical/airlock",
                    "Technical/connector_center_to_wall",
                    "Technical/side_tunnel",
                    "Technical/airlock_horizontal",
                    "gravity_atmosphere_2",
                    "Technical/airlock_horizontal",
                    "Technical/horizontal_checkpoint",
                    "Technical/airlock_horizontal",
                    "gravity_atmosphere_3",
                    "Technical/airlock_horizontal",
                    "Technical/side_tunnel",
                    "Technical/connector_wall_to_center",
                    "Technical/airlock",
                    "gravity_atmosphere_1",
                    "Technical/airlock",
                    "Technical/test_exit_center"
                }.Select(
                    name => new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData { filename = name }
                    }
                ).ToArray();
                theme = Theme.GravityGalaxy;
                break;

            case DebugPanel.Mode.MoltenFortressExamples:
                rows = new [] {
                    "start",
                    "chunk_ash_skull",
                    "chunk_brainbox",
                    "chunk_swinging_platform",
                    "chunk_zipline",
                    "chunk_spin_block",
                    "chunk_bully",
                    "chunk_chain",
                    "Technical/test_exit_center"
                }.Select(
                    name => new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData { filename = name }
                    }
                ).ToArray();
                theme = Theme.MoltenFortress;
                break;

            default:
                rows = new [] {
                    new LevelGeneration.Row {
                        chunk = new ChunkLibrary.ChunkData {
                            filename = "Technical/nothing"
                        }
                    }
                };
                break;
        }

        foreach (var row in rows)
        {
            row.chunk.level ??= LoadChunkByName(row.chunk.filename);
        }

        // Assets.instance.PreloadAssetsForMap(
        //     this.theme, () => { NotifyAssetsLoaded(rows); }
        // );

        if (DebugPanel.selectedMode == DebugPanel.Mode.Algorithm2PSplitScreen)
            GameInput.UpdatePlayersPlaying(2);
        else
            GameInput.UpdatePlayersPlaying(1);

        IngameUI.instance = FindObjectOfType<IngameUI>();
        IngameUI.instance.gameObject.SetActive(false);

        this.rows = rows;

        Assets.instance.PreloadAssetsForMap(
            theme, () =>
            {
                Map MakeMap(int number)
                {
                    var gameObject = new GameObject($"Map {number}");
                    gameObject.transform.parent = this.transform;
                    return gameObject.AddComponent<Map>();
                }

                if (DebugPanel.selectedMode == DebugPanel.Mode.Algorithm2PSplitScreen)
                {
                    this.map1 = MakeMap(1);
                    this.map2 = MakeMap(2);
                    this.maps = new [] { this.map1, this.map2 };
                }
                else
                {
                    this.map1 = MakeMap(1);
                    this.map2 = null;
                    this.maps = new [] { this.map1 };
                    Map.instance = this.map1; // todo: delete me
                }

                this.map1?.NotifyAssetsLoaded(1, theme, rows);
                this.map2?.NotifyAssetsLoaded(2, theme, rows);
                this.map1?.CreateGhostPlayers();
                this.map2?.CreateGhostPlayers();
                this.isFullyLoaded = true;
            }
        );
    }

    public static void RandomlySelectSave()
    {
        int checkpointNumber;

        // randomizer
        selectedDate = DateTime.Now.Date.AddDays(UnityEngine.Random.Range(-100, 100));
        checkpointNumber = UnityEngine.Random.Range(0, 16);

        // // ridiculous and pointless pan up to the checkpoint?
        // selectedDate = new DateTime(2024, 3, 9);
        // checkpointNumber = 2;

        // // weird treasure mines parallax
        // selectedDate = new DateTime(2023, 12, 31);
        // checkpointNumber = 4;

        // // building animation
        // selectedDate = new DateTime(2023, 12, 16);
        // checkpointNumber = 11;

        SaveData.SetLatestAttemptForDate(selectedDate, new SaveData.AttemptForDate
        {
            checkpointNumber = checkpointNumber,
            coins = 40, deaths = 0, fruitCollected = 4, fruitCurrent = 4,
            silverCoins = 0, timeInSeconds = 0, timerCollected = 0
        });
        Debug.Log(
            selectedDate.ToLongDateString() +
            " checkpoint #" + checkpointNumber
        );

        didRandomlySelectSave = true;
    }

    public void OnDestroy()
    {
        GameCenterMultiplayer.NotifyLeavingGameScene();
    }

    NitromeEditor.Level LoadChunkByName(string chunkName)
    {
        if (chunkName.ToLowerInvariant().EndsWith(".json"))
        {
            chunkName = chunkName.Substring(0, chunkName.Length - 5);
            var r = Resources.Load<TextAsset>("Levels/" + chunkName);
            if (r == null)
                Debug.LogError($"Failed to load chunk {chunkName} (json)!");
            return NitromeEditor.Level.UnserializeFromJson(r.text);
        }
        else
        {
            var r = Resources.Load<TextAsset>("Levels/" + chunkName);
            if (r == null)
                Debug.LogError($"Failed to load chunk {chunkName} (bytes)!");
            return NitromeEditor.Level.UnserializeFromBytes(r.bytes);
        }
    }

    public void ChangeNumberOfMaps(int newNumberOfMaps, bool rebindControllers = true)
    {
        var oldNumberOfMaps = this.maps.Length;
        if (oldNumberOfMaps == newNumberOfMaps) return;
        if (newNumberOfMaps < 1 || newNumberOfMaps > 4) return;

        if (rebindControllers == true)
            GameInput.UpdatePlayersPlaying(newNumberOfMaps);
        IngameUI.instance.FadeToNewLayout(newNumberOfMaps);

        if (this.coroutineForMapNumberChange != null)
            StopCoroutine(this.coroutineForMapNumberChange);

        var mapList = this.maps.ToList();

        for (int n = oldNumberOfMaps + 1; n <= newNumberOfMaps; n += 1)
        {
            var gameObject = new GameObject("Map " + n);
            gameObject.transform.parent = this.transform;

            var newMap = gameObject.AddComponent<Map>();
            mapList.Add(newMap);

            this.maps = mapList.ToArray();
            this.map1 = this.maps.ElementAtOrDefault(0);
            this.map2 = this.maps.ElementAtOrDefault(1);
            this.map3 = this.maps.ElementAtOrDefault(2);
            this.map4 = this.maps.ElementAtOrDefault(3);

            this.map1.gameCamera.UpdateCullingMask();

            newMap.NotifyAssetsLoaded(n, this.map1.theme, this.rows);

            foreach (var map in this.maps)
            {
                map.CreateGhostPlayers();
            }
        }

        IEnumerator Open()
        {
            foreach (var map in this.maps)
                map.gameCamera.orthographicSizeDrivenByMapNumberChange = true;

            float s = 0.02f;
            while (s < 0.98f)
            {
                s = Util.Slide(s, 1, 0.02f);
                var sEased = (float)Easing.QuadEaseInOut(s, 0, 1, 1);

                for (int n = 0; n < this.maps.Length; n += 1)
                {
                    var map = this.maps[n];

                    var from = VisibleRectForMap(n + 1, oldNumberOfMaps);
                    var to   = VisibleRectForMap(n + 1, newNumberOfMaps);
                    map.gameCamera.Camera.rect = MakeRectSafe(new Rect(
                        Mathf.Lerp(from.xMin, to.xMin, sEased),
                        Mathf.Lerp(from.yMin, to.yMin, sEased),
                        Mathf.Lerp(from.width, to.width, sEased),
                        Mathf.Lerp(from.height, to.height, sEased)
                    ));

                    float size;
                    if (n > 0 && n >= oldNumberOfMaps || n >= newNumberOfMaps)
                        size = this.maps[0].gameCamera.Camera.orthographicSize;
                    else if (from.width <= 0.004f || from.height <= 0.004f)
                        size = map.gameCamera.HalfHeightWithCameraRect(to);
                    else if (to.width <= 0.004f || to.height <= 0.004f)
                        size = map.gameCamera.HalfHeightWithCameraRect(from);
                    else
                    {
                        var fromSize = map.gameCamera.HalfHeightWithCameraRect(from);
                        var toSize = map.gameCamera.HalfHeightWithCameraRect(to);
                        size = Mathf.Lerp(fromSize, toSize, sEased);
                    }
                    map.gameCamera.Camera.orthographicSize = size;
                }

                yield return null;
            }

            for (int n = 0; n < this.maps.Length; n += 1)
            {
                this.maps[n].gameCamera.Camera.rect =
                    VisibleRectForMap(n + 1, newNumberOfMaps);
                this.maps[n].gameCamera.orthographicSizeDrivenByMapNumberChange = false;
            }
            PostAnimationCleanup();
        }
        this.coroutineForMapNumberChange = StartCoroutine(Open());

        Rect MakeRectSafe(Rect r)
        {
            r.xMin = Mathf.Clamp01(r.xMin);
            r.xMax = Mathf.Clamp01(r.xMax);
            r.yMin = Mathf.Clamp01(r.yMin);
            r.yMax = Mathf.Clamp01(r.yMax);

            if (r.width < 0.002f)
            {
                r.width = 0.002f;
                if (r.xMax > 1)
                {
                    r.xMin -= 0.002f;
                    r.xMax = 1;
                }
            }
            if (r.height < 0.002f)
            {
                r.height = 0.002f;
                if (r.yMax > 1)
                {
                    r.yMin -= 0.002f;
                    r.yMax = 1;
                }
            }

            return r;
        }

        Rect VisibleRectForMap(int mapNumber, int mapCount)
        {
            float px = 0.002f;

            return ((mapNumber, of: mapCount)) switch
            {
                (1, of: 1) => Rect.MinMaxRect(0, 0, 1, 1),

                (1, of: 2) => Rect.MinMaxRect(0, 0, 0.5f - px, 1),
                (2, of: 2) => Rect.MinMaxRect(0.5f + px, 0, 1, 1),

                (1, of: 3) => Rect.MinMaxRect(0, 0, 0.333f - px, 1),
                (2, of: 3) => Rect.MinMaxRect(0.333f + px, 0, 0.667f - px, 1),
                (3, of: 3) => Rect.MinMaxRect(0.667f + px, 0, 1, 1),

                (1, of: 4) => Rect.MinMaxRect(0, 0, 0.25f - px, 1),
                (2, of: 4) => Rect.MinMaxRect(0.25f + px, 0, 0.50f - px, 1),
                (3, of: 4) => Rect.MinMaxRect(0.50f + px, 0, 0.75f - px, 1),
                (4, of: 4) => Rect.MinMaxRect(0.75f + px, 0, 1, 1),

                _ => new Rect(1 + (px * mapNumber * 2), 0, 0, 1),
            };
        }

        void PostAnimationCleanup()
        {
            if (newNumberOfMaps < oldNumberOfMaps)
            {
                // destroy extraneous maps
                for (int n = oldNumberOfMaps - 1; n > newNumberOfMaps - 1; n -= 1)
                {
                    Destroy(this.maps[n].gameCamera.gameObject);
                    Destroy(this.maps[n].gameObject);
                }

                this.maps = this.maps.Take(newNumberOfMaps).ToArray();
                this.map1 = this.maps.ElementAtOrDefault(0);
                this.map2 = this.maps.ElementAtOrDefault(1);
                this.map3 = this.maps.ElementAtOrDefault(2);
                this.map4 = this.maps.ElementAtOrDefault(3);

                for (int n = 0; n < this.maps.Length; n += 1)
                {
                    foreach (var g in this.maps[n].ghostPlayers)
                    {
                        Destroy(g.gameObject);
                    }

                    this.maps[n].ghostPlayers.Clear();
                    this.maps[n].CreateGhostPlayers();
                }
            }
        }
    }

    public void Update()
    {
        if (this.isFullyLoaded == false)
            return;

        UpdateCameraLetterboxing();

        if (this.isLeaving == true)
        {
            // nothing
        }
        else if (IngameUI.instance.IsGamePaused()/* ||
            this.timeFrozenByEnemyDeath == true*/)
        {
            Time.timeScale = 0;
            Screen.sleepTimeout = SleepTimeout.SystemSetting;
            GameInput.Advance();
            GameCenterMultiplayer.Advance();
        }
        else
        {
            Time.timeScale = 1;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            bool lockTo30FPS;
            #if UNITY_SWITCH
                lockTo30FPS = (this.maps.Length > 2);
            #else
                lockTo30FPS = false;
            #endif

            this.accumulatedTime += Time.deltaTime;
            int framesToAdvance;

            if (lockTo30FPS == true)
            {
                framesToAdvance = Mathf.FloorToInt(this.accumulatedTime * 60);
                if (framesToAdvance == 1)
                    framesToAdvance = 0;
                if (framesToAdvance > 4)
                    framesToAdvance = 4;

                this.accumulatedTime -= framesToAdvance / 60f;
            }
            else
            {
                framesToAdvance = Mathf.FloorToInt(this.accumulatedTime * 60);
                this.accumulatedTime -= framesToAdvance / 60f;

                if (this.approximateTimeSpentInMapAdvance >= 0.015f)
                    framesToAdvance = 1;
                else if (framesToAdvance > 5)
                    framesToAdvance = 5;
            }

            var timeBeforeAdvance = Time.realtimeSinceStartup;

            for (int n = 0; n < framesToAdvance; n += 1)
            {
                GameInput.Advance();
                this.multiplayerLastPlayerCutoff.Advance();
                foreach (var map in this.maps)
                {
                    map.Advance(n == framesToAdvance - 1);
                }
                IngameUI.instance.Advance();
                Animated.AdvanceAll(1 / 60f);
                AnimatedUI.AdvanceAll();
                Particle.AdvanceAll();
            }

            GameCenterMultiplayer.Advance();

            foreach (var map in this.maps)
            {
                foreach (var ghostPlayer in map.ghostPlayers)
                    ghostPlayer.Advance();
            }

            this.approximateTimeSpentInMapAdvance =
                (Time.realtimeSinceStartup - timeBeforeAdvance) / framesToAdvance;

            if (framesToAdvance > 0)
                this.framesThisSecondSoFar += 1;

            var realtimeSecond = Mathf.FloorToInt(Time.realtimeSinceStartup);
            if (realtimeSecond != this.realtimeSecondLastUpdate)
            {
                this.realtimeSecondLastUpdate = realtimeSecond;
                this.framesLastSecond = this.framesThisSecondSoFar;
                this.framesThisSecondSoFar = 0;
            }
        }
    }

    public static void InitScene()
    {
        SaveData.EnsureValid();

        Game.instance = null;
        Map.instance = null;
        Assets.instance = null;
        IngameUI.instance = null;

        Application.targetFrameRate = 60;
        Time.timeScale = 1;
        Screen.sleepTimeout = SleepTimeout.SystemSetting;

        UpdateCameraLetterboxing();
    }

    public static IEnumerator UnloadCurrentSceneThenLoadSceneAsync(string targetSceneName)
    {
        var asyncOperation = SceneManager.LoadSceneAsync("Empty");
        while (asyncOperation.isDone == false)
        {
            yield return null;
        }

        Assets.instance = null;
        Map.instance = null;
        Game.instance = null;
        IngameUI.instance = null;
        Resources.UnloadUnusedAssets();
        GC.Collect();
        yield return null;

        asyncOperation = SceneManager.LoadSceneAsync(targetSceneName);
        while (asyncOperation.isDone == false)
        {
            yield return null;
        }
    }

    public static void UpdateCameraLetterboxing()
    {
        if (Game.instance?.maps?.Length > 1)
            return;

        float aspectRatio = (float)Screen.width / (float)Screen.height;
        var camera = Camera.main;

        if (aspectRatio < 1f)
        {
            camera.rect = new Rect(0, 0, 1, 1);
        }
        else if(aspectRatio >= 16f / 10f && aspectRatio <= 16f / 9f)
        {
            camera.rect = new Rect(0, 0, 1, 1);
        }
        else if(aspectRatio < 16f / 10f)
        {
            float scaledHeight = aspectRatio / (16f / 10f);
            camera.rect = new Rect(0, (1.0f - scaledHeight) / 2.0f, 1f, scaledHeight);
        }
        else
        {
            float scaledHeight = aspectRatio / (16f / 9f);
            camera.rect = new Rect(0, (1.0f - scaledHeight) / 2.0f, 1f, scaledHeight);
        }
    }
}
