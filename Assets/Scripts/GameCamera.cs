using System;
using System.Collections;
using System.Linq;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    private Map map;
    private Camera cam;
    private Vector2 position; // without screen shake
    private Vector2 screenShakeAmount;
    [HideInInspector] public Chunk currentChunk;

    private bool inSideRoom = false;
    private bool horizontalTransition = false;
    private int  horizontalTransitionTime;

    private VisibleAreaDebugGuide visibleAreaDebugGuide;

    private class Realign
    {
        public const int DurationX = 25;
        public const int DurationY = 60;
        public Chunk alignToChunk;
        public int time;
    }

    // for moving the camera forward of an airlock in a gravity galaxy horizontal section
    private Realign realignX;
    // for moving between horizontal sections with different top/bottom edges
    private Realign realignY;

    private class ZoomOut
    {
        public const int Duration = 60;
        public float amount = 0f;
        public float biasLeftRight = 0f;
    }
    private ZoomOut zoomOut;

    private const int HorizontalTransitionDuration = 45;
    private readonly float[] ScreenShakeDeltas = new float[] { 1, 1, -1, -1, 1, -1 };

    public Camera Camera => this.cam;

    [NonSerialized] public bool orthographicSizeDrivenByMapNumberChange;

    public void Init(Map map)
    {
        this.map = map;

        this.cam = this.GetComponent<Camera>();
        this.cam.gameObject.name = $"Game Camera #{this.map.mapNumber}";
        this.currentChunk = this.map.chunks[0];

        if (Game.instance.maps.Length > 1)
        {
            if (this.map.mapNumber == 1)
                this.cam.rect = new Rect(0, 0, 0.498f, 1);
            else
                this.cam.rect = new Rect(0.502f, 0, 0.498f, 1);
        }

        this.cam.cullingMask = 1 << this.map.mapNumber;

        Advance(true);
    }

    public void UpdateCullingMask()
    {
        this.cam.cullingMask = 1 << this.map.mapNumber;
    }

    public static float ClampOrCenter(float value, float min, float max)
    {
        if (min <= max)
            return Mathf.Clamp(value, min, max);
        else
            return (min + max) * 0.5f;
    }

    public Rect VisibleRectangle()
    {
        float halfheight = this.cam.orthographicSize;
        float halfwidth = halfheight * this.cam.aspect;
        return new Rect(
            this.position.x - halfwidth,
            this.position.y - halfheight,
            halfwidth * 2,
            halfheight * 2
        );
    }

    public static bool IsLandscape() => Screen.width > Screen.height;

    public float HalfWidth()
    {
        float camWidth = Screen.width * this.cam.rect.width;
        float camHeight = Screen.height * this.cam.rect.height;

        if (this.orthographicSizeDrivenByMapNumberChange == true)
            return this.cam.orthographicSize * camWidth / camHeight;
        else if (camWidth > camHeight)
            return 9.75f * (camWidth / camHeight);
        else
            return Mathf.Lerp(8, 12, this.zoomOut?.amount ?? 0);
    }

    public float HalfHeight()
    {
        float camWidth = Screen.width * this.cam.rect.width;
        float camHeight = Screen.height * this.cam.rect.height;

        if (this.orthographicSizeDrivenByMapNumberChange == true)
            return this.cam.orthographicSize;
        else if (camWidth > camHeight)
            return 9.75f;
        else
            return Mathf.Lerp(8, 12, this.zoomOut?.amount ?? 0) / (camWidth / camHeight);
    }

    public float HalfHeightWithCameraRect(Rect cameraRect)
    {
        float camWidth = Screen.width * cameraRect.width;
        float camHeight = Screen.height * cameraRect.height;

        if (camWidth > camHeight)
            return 9.75f;
        else
            return Mathf.Lerp(8, 12, this.zoomOut?.amount ?? 0) / (camWidth / camHeight);
    }

    public (Chunk btm, Chunk top) ChunksInVerticalSection(Chunk fromChunk)
    {
        if (fromChunk.isHorizontal == true)
            return (fromChunk, fromChunk);

        var chunks = this.map.chunks;
        var min = fromChunk.index;
        var max = fromChunk.index;

        while (min > 0 && chunks[min - 1].exit.type != Chunk.MarkerType.Wall)
        {
            min -= 1;
        }
        while (max < chunks.Count - 1 && chunks[max].exit.type != Chunk.MarkerType.Wall)
        {
            max += 1;
        }

        return (chunks[min], chunks[max]);
    }

    public (Chunk, Chunk) ChunksInHorizontalSection(Chunk fromChunk)
    {
        if (fromChunk.isHorizontal == false)
            return (fromChunk, fromChunk);

        if (fromChunk.isAirlock == true)
            fromChunk = fromChunk.Next();

        var chunks = this.map.chunks;
        var min = fromChunk.index;
        var max = fromChunk.index;

        while (
            min > 0 &&
            chunks[min - 1].isHorizontal == true &&
            chunks[min - 1].isTransitionalTunnel == false &&
            chunks[min - 1].entry.type == Chunk.MarkerType.Wall &&
            chunks[min - 1].isAirlock == false
        )
        {
            min -= 1;
        }
        while (
            max < chunks.Count - 1 &&
            chunks[max + 1].isHorizontal == true &&
            chunks[max + 1].isTransitionalTunnel == false
        )
        {
            max += 1;
        }

        if (chunks[min].isAdditionalStepDownForHorizontal == true)
            min += 1;
        if (chunks[max].isAdditionalStepDownForHorizontal == true)
            max -= 1;

        return (chunks[min], chunks[max]);
    }

    public float XDistanceToChunk(Chunk chunk, float x)
    {
        if (x < chunk.xMin)
            return chunk.xMin - x;
        else if (x > chunk.xMax + 1)
            return x - (chunk.xMax + 1);
        else
            return 0;
    }

    public float CameraYWithinChunkRange(Chunk bottom, Chunk top, float halfheight)
    {
        float yMin = bottom.yMin + halfheight;
        float yMax = top.yMax + 1 - halfheight;
        if (IsLandscape() == false && bottom == this.map.chunks[0])
            yMin -= 2.5f; // so that Yolk doesn't get hidden behind the UI's progress-bar
        if (yMin > yMax)
            return (yMin + yMax) * 0.5f;

        if (top.exit.type == Chunk.MarkerType.Wall &&
            top.entry.type != Chunk.MarkerType.Wall)
        {
            // this assumes the next chunk is purely horizontal (no vertical scrolling)
            var nextChunk = this.map.chunks[top.index + 1];
            var desiredExitRelativeToCenter =
                nextChunk.entry.mapLocation.y -
                ((nextChunk.yMin + nextChunk.yMax + 1) * 0.5f);
            yMax = top.exit.mapLocation.y - desiredExitRelativeToCenter;
        }

        if (bottom.entry.type == Chunk.MarkerType.Wall &&
            bottom.exit.type != Chunk.MarkerType.Wall)
        {
            var prevChunk = this.map.chunks[bottom.index - 1];
            var desiredEntryRelativeToCenter =
                prevChunk.exit.mapLocation.y -
                ((prevChunk.yMin + prevChunk.yMax + 1) * 0.5f);
            yMin =
                bottom.entry.mapLocation.y -
                desiredEntryRelativeToCenter;
        }

        float cameraFocusY = this.map.player.position.y;
        if (IsLandscape())
        {
            cameraFocusY += (12 - 7.5f) / 2;
        }
        return ClampOrCenter(cameraFocusY, yMin, yMax);
    }

    bool DoesChunkTriggerRealignY(Chunk c) =>
        c.isAdditionalStepDownForHorizontal == true ||
        (c.checkpointNumber != null && c.isHorizontal == true);

    public static bool ShouldZoomOutForChunk(Chunk c) =>
        (c.isHorizontal == true && c.items.OfType<Minecart>().Any()) ||
        (c.gravityDirection == null) ||
        (c.isAirlock == true);

    public void Advance(bool snapToTarget = false)
    {
        if (this.map.finished) return;
        
        this.screenShakeAmount.x = Util.Slide(this.screenShakeAmount.x, 0, 0.08f);
        this.screenShakeAmount.y = Util.Slide(this.screenShakeAmount.y, 0, 0.08f);

        // Uncomment this for a simple continuous pan-up
        // if (snapToTarget == false)
        // {
        //     this.transform.position += new Vector3(0, 0.125f, 0);
        //     return;
        // }

        var player = this.map.player;

        if (player.alive == false && snapToTarget == false) return;

        var playerChunk = this.map.NearestChunkTo(player.position);
        if (playerChunk.isTransitionalTunnel == true)
        {
            var prevChunk = this.map.chunks[playerChunk.index - 1];
            var nextChunk = this.map.chunks[playerChunk.index + 1];

            var (leftChunk, rightChunk) =
                playerChunk.isFlippedHorizontally == true ?
                (nextChunk, prevChunk) :
                (prevChunk, nextChunk);

            playerChunk = (player.velocity.x > 0) ? rightChunk : leftChunk;
        }

        if (this.currentChunk != playerChunk &&
            this.currentChunk.gravityDirection == null &&
            playerChunk.Rectangle().Contains(player.position) == false)
        {
            // in gravity galaxy, if we're outside the space of the nearest chunk,
            // coming from a zero-gravity chunk, refuse to move on.
            playerChunk = this.currentChunk;
        }

        if (this.currentChunk != playerChunk)
        {
            if (this.currentChunk.isHorizontal != playerChunk.isHorizontal)
            {
                this.horizontalTransition = true;
                this.horizontalTransitionTime = 0;
            }

            if (
                DoesChunkTriggerRealignY(this.currentChunk) == false &&
                DoesChunkTriggerRealignY(playerChunk) == true
            )
            {
                var playerMovingForward =
                    (player.velocity.x > 0) ^
                    (this.currentChunk.isFlippedHorizontally == true);
                var playerMovingToChunk =
                    playerMovingForward ?
                    this.map.chunks[playerChunk.index + 1] :
                    this.map.chunks[playerChunk.index - 1];
                this.realignY = new Realign {
                    alignToChunk = playerMovingToChunk,
                    time = 0
                };
            }

            if (playerChunk.isAirlock == true &&
                playerChunk.isHorizontal == true)
            {
                this.realignX = new Realign {
                    alignToChunk = playerChunk,
                    time = 0
                };
            }

            this.currentChunk = playerChunk;
        }

        if (ShouldZoomOutForChunk(this.currentChunk) == true ||
            this.map.player.parachute.ShouldZoomOut == true)
        {
            this.zoomOut ??= new ZoomOut();
            this.zoomOut.amount = Util.Slide(
                this.zoomOut.amount, 1, 1f / ZoomOut.Duration
            );
            this.zoomOut.biasLeftRight = Util.Slide(
                this.zoomOut.biasLeftRight,
                Mathf.Clamp(this.map.player.velocity.x * 10, -1f, 1f),
                0.05f
            );
        }
        else if (this.zoomOut != null)
        {
            this.zoomOut.amount = Util.Slide(
                this.zoomOut.amount, 0, 1f / ZoomOut.Duration
            );
            if (this.zoomOut.amount == 0)
                this.zoomOut = null;
        }

        float halfwidth = HalfWidth();
        float halfheight = HalfHeight();
        if (this.orthographicSizeDrivenByMapNumberChange == true)
        {
            halfheight = this.cam.orthographicSize;
            halfwidth =
                halfheight * (Screen.width / Screen.height) *
                (this.cam.rect.width / this.cam.rect.height);
        }
        float x = player.position.x;
        float y = player.position.y;

        if (player.state == Player.State.WallSlide)
            x += (player.facingRight ? -1 : 1) * (3 / 16f);

        if (this.zoomOut != null && IsLandscape() == false)
            x += 4 * this.zoomOut.biasLeftRight * this.zoomOut.amount;

        if (this.currentChunk.isHorizontal == true)
        {
            {
                var (start, end) = ChunksInHorizontalSection(this.currentChunk);
                var (left, right) =
                    (start.isFlippedHorizontally) ? (end, start) : (start, end);
                var xMin = left.xMin + halfwidth;
                var xMax = right.xMax + 1 - halfwidth;

                if (left.isAirlock == true)
                    xMin -= 12;
                if (right.isAirlock == true)
                    xMax += 12;

                if (start.Previous().isHorizontal == true &&
                    start.Previous().isAirlock == true)
                {
                    if (start.isFlippedHorizontally == true)
                        xMax += 5;
                    else
                        xMin -= 5;
                }

                if (this.realignX != null)
                {
                    var xMinBefore = Mathf.Min(xMin, this.position.x);
                    var xMaxBefore = Mathf.Max(xMax, this.position.x);

                    float throughBefore = (float)Easing.QuadEaseInOut(
                        this.realignX.time, 0, 1, Realign.DurationX
                    );
                    float throughAfter = (float)Easing.QuadEaseInOut(
                        this.realignX.time + 1, 0, 1, Realign.DurationX
                    );
                    float s = Mathf.Clamp01(
                        (throughAfter - throughBefore) / (1 - throughBefore)
                    );
                    xMin = Mathf.Lerp(xMinBefore, xMin, s);
                    xMax = Mathf.Lerp(xMaxBefore, xMax, s);

                    this.realignX.time += 1;
                    if (this.realignX.time >= Realign.DurationX)
                        this.realignX = null;
                }

                x = ClampOrCenter(x, xMin, xMax);
            }

            if (this.realignY != null)
            {
                Chunk chunk = this.realignY.alignToChunk;
                float targetY = CameraYWithinChunkRange(chunk, chunk, halfheight);
                this.realignY.time += 1;

                float s = Mathf.Clamp01((float)this.realignY.time / Realign.DurationY);
                y = Mathf.Lerp(this.transform.position.y, targetY, s);

                if (s >= 1)
                {
                    if (this.currentChunk == this.realignY.alignToChunk)
                    {
                        // we walked forward into the intended chunk
                        this.realignY = null;
                    }
                    else if (
                        DoesChunkTriggerRealignY(this.currentChunk) == false
                    )
                    {
                        // we backtracked
                        this.realignY = new Realign {
                            alignToChunk = this.currentChunk,
                            time = 0
                        };
                    }
                }
            }
            else
            {
                y = CameraYWithinChunkRange(
                    this.currentChunk, this.currentChunk, halfheight
                );
            }
        }
        else
        {
            var visibleChunks = ChunksInVerticalSection(this.currentChunk);
            y = CameraYWithinChunkRange(
                visibleChunks.btm, visibleChunks.top, halfheight
            );
            if (this.currentChunk.entry.type == Chunk.MarkerType.Wall)
                x = this.currentChunk.exit.mapLocation.x;
            else
                x = this.currentChunk.entry.mapLocation.x;

            if (playerChunk.isHorizontal == false &&
                playerChunk.xMax - playerChunk.xMin + 1 > 16)
            {
                var inSideRoom = Mathf.Abs(
                    player.position.x - playerChunk.entry.mapLocation.x
                ) > 8;
                if (inSideRoom == true)
                {
                    if (player.position.x > playerChunk.entry.mapLocation.x)
                        x = playerChunk.entry.mapLocation.x + 16;
                    else
                        x = playerChunk.entry.mapLocation.x - 16;
                }

                if (inSideRoom != this.inSideRoom)
                {
                    this.horizontalTransition = true;
                    this.horizontalTransitionTime = 0;
                    this.inSideRoom = inSideRoom;
                }
            }
        }

        if (snapToTarget == false)
        {
            if (this.horizontalTransition == true)
            {
                this.horizontalTransitionTime += 1;
                if (this.horizontalTransitionTime >= HorizontalTransitionDuration)
                    this.horizontalTransition = false;
                else
                {
                    var remainingTime =
                        HorizontalTransitionDuration - this.horizontalTransitionTime;
                    var factor = 1.0f / remainingTime;
                    x = Mathf.Lerp(this.transform.position.x, x, factor);
                    y = Mathf.Lerp(this.transform.position.y, y, factor);
                }
            }
            else
            {
                y = Mathf.Lerp(this.transform.position.y, y, 0.3f);
            }
        }

        this.position = new Vector2(x, y);

        var positionWithScreenShake = this.position;

        positionWithScreenShake += new Vector2(
            this.screenShakeAmount.x *
            ScreenShakeDeltas[this.map.frameNumber % ScreenShakeDeltas.Length],
            this.screenShakeAmount.y *
            ScreenShakeDeltas[this.map.frameNumber % ScreenShakeDeltas.Length]
        );

        this.transform.position = new Vector3(
            positionWithScreenShake.x,
            positionWithScreenShake.y,
            -10
        );
        if (this.orthographicSizeDrivenByMapNumberChange == false)
            this.cam.orthographicSize = halfheight;

        if (DebugPanel.visibleAreaGuideEnabled == true)
        {
            if (this.visibleAreaDebugGuide == null)
            {
                var obj = Instantiate(Assets.instance.visibleAreaDebugGuide);
                this.visibleAreaDebugGuide = obj.GetComponent<VisibleAreaDebugGuide>();
                this.visibleAreaDebugGuide.Init(map);
            }
            this.visibleAreaDebugGuide?.Advance();
        }
        else if (this.visibleAreaDebugGuide != null)
        {
            Destroy(this.visibleAreaDebugGuide.gameObject);
            this.visibleAreaDebugGuide = null;
        }
    }

    public IEnumerator LookAtTrophy(Transform trophy)
    {
        float timer = 0;
        while (timer <= 5)
        {
            timer += Time.deltaTime;
            this.transform.position = Vector3.Lerp(this.transform.position, trophy.position + new Vector3(0,4,-10), timer / 5);
            this.position = this.transform.position;
            yield return null;
        }
    }

    // you probably want to be calling this.map.ScreenShakeAtPosition, not this
    public void ScreenShake(Vector2 vector)
    {
        this.screenShakeAmount = vector;
    }
}
