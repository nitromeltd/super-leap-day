using System;
using UnityEngine;
#if UNITY_IOS 
using UnityEngine.iOS;
#elif UNITY_TVOS
using UnityEngine.tvOS;
#endif 
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) 
using UnityEngine.SocialPlatforms.GameCenter;
#endif
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) && !UNITY_EDITOR
using GameCenterAccessPoint;
#endif

public static class GameCenter
{
    private static Version MinimumVersionOSForLeaderboards;
    private static Version CurrentVersionOS;

    public static bool IsValidOSVersion()
    {
#if UNITY_EDITOR
        return false;
#elif (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) 
        return CurrentVersionOS >= MinimumVersionOSForLeaderboards;
#elif (UNITY_STANDALONE_WIN || UNITY_SWITCH)
        return true;
#else
        return false;
#endif
    }

    public static void Init()
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) 
        if(Social.localUser.authenticated == true) return;
        
        GameCenterMultiplayer.Init();
        Social.localUser.Authenticate(ProcessAuthentication);
        LeaderboardApple.Init();
#endif

#if UNITY_IOS && !UNITY_EDITOR
        CurrentVersionOS = new Version(UnityEngine.iOS.Device.systemVersion);
        MinimumVersionOSForLeaderboards = new Version("14.0");
        
#elif UNITY_STANDALONE_OSX && !UNITY_EDITOR
        string macOSVersion = SystemInfo.operatingSystem;
        string macSubString = "Mac OS X ";

        if(macOSVersion.Contains(macSubString))
        {
            macOSVersion = macOSVersion.Replace("Mac OS X ", "");
            CurrentVersionOS = new Version(macOSVersion);
        }
        else
        {
            CurrentVersionOS = new Version("0.0");
        }

        MinimumVersionOSForLeaderboards = new Version("11.0");

#elif UNITY_TVOS && !UNITY_EDITOR
        CurrentVersionOS = new Version(UnityEngine.tvOS.Device.systemVersion);
        MinimumVersionOSForLeaderboards = new Version("14.0");
#endif
    }

    private static void ProcessAuthentication (bool success)
    {
        if (success)
        {
            Debug.Log ("[Game Center] Authenticated");
#if UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX
            AchievementsApple.Init();
#endif
        }
        else
        {
            Debug.Log ("[Game Center] Failed to authenticate");
        }     
    }

    // built-in Apple's UI for achivements
    public static void ShowAchievements()
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) 
        Social.ShowAchievementsUI();
#endif
    }

    public static void ShowAccessPoint(bool withHighlights = true)
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) && !UNITY_EDITOR
        // show Game Center dashboard button
        AccessPoint.ShowHighlights(false);
        AccessPoint.ShowAccessPoint(GKAccessPointLocation.GKAccessPointLocationTopLeading);
#endif
    }
    
    public static void HideAccessPoint()
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) && !UNITY_EDITOR
        // hide Game Center dashboard button
        AccessPoint.HideAccessPoint();
#endif
    } 
    
    public static void FocusAccessPoint(bool focused)
    {
#if UNITY_TVOS && !UNITY_EDITOR
        // (tvOS only) change the focus to the access point
        AccessPoint.ChangeFocus(focused);
#endif
    } 

    public static bool IsAccessPointFocused()
    {
#if UNITY_TVOS && !UNITY_EDITOR
        return AccessPoint.IsFocused();
#else
        return false;
#endif
    } 

    public static void TriggerAccessPoint()
    {
#if (UNITY_TVOS) && !UNITY_EDITOR
        // opens the Game Center dashboard
        AccessPoint.Trigger();
#endif
    }   
}
