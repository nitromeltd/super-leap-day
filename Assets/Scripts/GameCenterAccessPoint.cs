using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace GameCenterAccessPoint
{
    public struct CGRect
    {
        public CGPoint origin { get; set; }
        public CGSize size { get; set; }
    }

    public struct CGPoint
    {
        public float x { get; set; }
        public float y { get; set; }
    }

    public struct CGSize
    {
        public float width;
        public float height;
    }

    public enum GKAccessPointLocation
    {
        GKAccessPointLocationTopLeading,  //top left
        GKAccessPointLocationTopTrailing, //top right
        GKAccessPointLocationBottomLeading, //bottom left 
        GKAccessPointLocationBottomTrailing //bottom righr
    };

    public class AccessPoint
    {

#if (UNITY_IOS || UNITY_STANDALONE_OSX || UNITY_TVOS) && !UNITY_EDITOR

    #if UNITY_STANDALONE_OSX
        private const string dllSource = "LD2ExternalOSX";
    #else
        private const string dllSource = "__Internal";
    #endif
    
        [DllImport(dllSource)]
        private static extern void accessPoint_Show(int location);

        [DllImport(dllSource)]
        private static extern void accessPoint_Hide();

        [DllImport(dllSource)]
        private static extern CGRect accessPoint_getAccessPointFrameInScreenCoord();

        [DllImport(dllSource)]
        private static extern void accessPoint_showHighlights(bool visible);

        [DllImport(dllSource)]
        private static extern void accessPoint_trigger();

        [DllImport(dllSource)]
        private static extern bool accessPoint_isPresentingGameCenter();

        [DllImport(dllSource)]
        private static extern bool accessPoint_isVisible();

        [DllImport(dllSource)]
        private static extern bool accessPoint_isFocused();

        [DllImport(dllSource)]
        private static extern void accessPoint_changeFocus(bool focused);

        public static void ShowAccessPoint(GKAccessPointLocation location)
        {
            accessPoint_Show((int)location);
        }
        public static void HideAccessPoint() => accessPoint_Hide();
        public static void ShowHighlights(bool visible) => accessPoint_showHighlights(visible);
        public static CGRect GetAccessPointFrameInScreenCoord() => accessPoint_getAccessPointFrameInScreenCoord();
        public static bool IsPresentingGameCenter() => accessPoint_isPresentingGameCenter();
        public static bool IsVisible() => accessPoint_isVisible();
        public static bool IsFocused() => accessPoint_isFocused();
        public static void Trigger() => accessPoint_trigger();
        public static void ChangeFocus(bool focused) => accessPoint_changeFocus(focused);
#else

    public static void ShowAccessPoint(bool visible, GKAccessPointLocation location) => Debug.Log("GKAccessPoint not available");
    public static void ShowHighlights(bool visible) =>  Debug.Log("GKAccessPoint not available");
    public static void Trigger() =>  Debug.Log("GKAccessPoint not available");
    public static void ChangeFocus(bool focused) =>  Debug.Log("GKAccessPoint not available");

    public static CGRect GetAccessPointFrameInScreenCoord()
    {
        Debug.Log("GKAccessPoint not available");
        return new CGRect();
    }

    public static bool IsPresentingGameCenter()
    {
        Debug.Log("GKAccessPoint not available");
        return false;
    }

    public static bool IsFocused()
    {
        Debug.Log("GKAccessPoint not available");
        return false;
    }
#endif
    }
}
