using Steamworks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInitScreen : MonoBehaviour
{
    public void Start()
    {
        #if UNITY_STANDALONE_OSX && !UNITY_EDITOR && !UNITY_CLOUD_BUILD
        FairPlay.RegisterWithFairPlay();
        #endif

        #if (UNITY_IOS || UNITY_TVOS || UNITY_STANDALONE_OSX) && !UNITY_EDITOR
        CloudSaving_Start();
        #endif

        #if UNITY_SWITCH && !UNITY_EDITOR
        Switch.Initialize();
        #endif

        #if UNITY_STANDALONE_WIN
        if (SteamAPI.Init() == false)
            Debug.Log("SteamAPI.Init() failed :(");
        AchievementsSteam.Init();
        #endif

        LevelPatching.Init();
        Game.InitScene();

        SceneManager.LoadScene("Loading");
    }
}
