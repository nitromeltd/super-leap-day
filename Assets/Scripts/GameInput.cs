using Rewired;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameInput
{
    public class PlayerControls
    {
        public Rewired.Player rewiredPlayer;

        public bool holdingLeft    = false;
        public bool holdingRight   = false;
        public bool holdingDown    = false;
        public bool holdingUp      = false;
        public bool holdingJump    = false;
        public bool holdingPowerup = false;
        public bool holdingPause   = false;

        public bool pressedLeft     = false;
        public bool pressedRight    = false;
        public bool pressedJump     = false;
        public bool releasedJump    = false;
        public bool pressedPowerup  = false;
        public bool pressedPause    = false;

        public Vector2? tvOSMoveDirection;
    }

    public static PlayerControls Player(Player player) => Player(player.map.mapNumber);
    public static PlayerControls Player(Map map) => Player(map.mapNumber);
    public static PlayerControls Player(int indexOneBased) => indexOneBased switch
    {
        2 => player2Controls,
        3 => player3Controls,
        4 => player4Controls,
        _ => player1Controls
    };

    public static PlayerControls player1Controls = new PlayerControls();
    public static PlayerControls player2Controls = new PlayerControls();
    public static PlayerControls player3Controls = new PlayerControls();
    public static PlayerControls player4Controls = new PlayerControls();

    public static bool holdingConfirm = false;
    public static bool holdingMenu    = false;
    public static bool pressedConfirm  = false;
    public static bool releasedConfirm = false;
    public static bool pressedMenu     = false;

    public static Vector2? menuMoveDirection;
    public static Vector2 menuSlideInput;

    public static bool usingSelectionCursor = true;

    public static string recordingInput = "";
    public static string inputToReplay;
    public static int replayingInputFrameNumber = 0;

    private static Vector2 tvOSAccumulatedSwipe;
    private static Vector3 lastMousePosition;
    private static int mouseLiveTime;

    public enum InputType
    {
        Keyboard,
        Mouse,
        GamepadXbox,
        GamepadPlaystation,
        GamepadMfi,
        GamepadSwitch,
        SiriRemote,
        TouchScreen
    }

    // Rewired vars
    public static Rewired.Player inputRewired;
    public static InputType mostRecentInputType = DefaultInputType();
    public static Rewired.Controller mostRecentController = null;
    public static bool gamepadConnected = false;

    public const float StickDeadzone = 0.1f;
    public const float TvOSSwipeDistance = 50;
    public const float TvOSSliderSensitivity = 0.02f;
    public const float DistanceToActivateMouse = 3;

    public static readonly System.Guid HardwareGuidForSiriRemote = new System.Guid("bc043dba-df07-4135-929c-5b4398d29579");
    public static readonly System.Guid HardwareGuidForMfiController = new System.Guid("3d919cfa-468e-49f4-bce9-f6c43f2e7e62");
    public static readonly System.Guid HardwareGuidForPS2 = new System.Guid("c3ad3cad-c7cf-4ca8-8c2e-e3df8d9960bb");
    public static readonly System.Guid HardwareGuidForPS3 = new System.Guid("71dfe6c8-9e81-428f-a58e-c7e664b7fbed");
    public static readonly System.Guid HardwareGuidForPS4 = new System.Guid("cd9718bf-a87a-44bc-8716-60a0def28a9f");
    public static readonly System.Guid HardwareGuidForPS5 = new System.Guid("5286706d-19b4-4a45-b635-207ce78d8394");
    public static readonly System.Guid HardwareGuidForSwitchLeft          = new System.Guid("3eb01142-da0e-4a86-8ae8-a15c2b1f2a04");
    public static readonly System.Guid HardwareGuidForSwitchRight         = new System.Guid("605dc720-1b38-473d-a459-67d5857aa6ea");
    public static readonly System.Guid HardwareGuidForSwitchDual          = new System.Guid("521b808c-0248-4526-bc10-f1d16ee76bf1");
    public static readonly System.Guid HardwareGuidForSwitchHandheld      = new System.Guid("1fbdd13b-0795-4173-8a95-a2a75de9d204");
    public static readonly System.Guid HardwareGuidForSwitchProController = new System.Guid("7bf3154b-9db8-4d52-950f-cd0eed8a5819");

    private enum Direction { Down, Left, Right, Up };

    public static void UpdatePlayersPlaying(int count)
    {
        player1Controls.rewiredPlayer = ReInput.players.GetPlayer("Player 1");
        player2Controls.rewiredPlayer = ReInput.players.GetPlayer("Player 2");
        player3Controls.rewiredPlayer = ReInput.players.GetPlayer("Player 3");
        player4Controls.rewiredPlayer = ReInput.players.GetPlayer("Player 4");

        int previousCount = player2Controls.rewiredPlayer.isPlaying ? 2 : 1;
        if (player3Controls.rewiredPlayer.isPlaying == true) previousCount = 3;
        if (player4Controls.rewiredPlayer.isPlaying == true) previousCount = 4;

        player1Controls.rewiredPlayer.isPlaying = (count >= 1);
        player2Controls.rewiredPlayer.isPlaying = (count >= 2);
        player3Controls.rewiredPlayer.isPlaying = (count >= 3);
        player4Controls.rewiredPlayer.isPlaying = (count >= 4);

        if (count > previousCount)
        {
            foreach (
                var controller in player1Controls.rewiredPlayer.controllers.Controllers
            )
            {
                if (controller is Rewired.Keyboard) continue;
                if (controller is Rewired.Mouse) continue;
                player1Controls.rewiredPlayer.controllers.RemoveController(controller);
            }
        }
        else if (count < previousCount)
        {
            #if !(UNITY_SWITCH && !UNITY_EDITOR)
            {
                var controllersToReassign =
                    player2Controls.rewiredPlayer.controllers.Controllers.ToArray();

                foreach (var c in controllersToReassign)
                {
                    player1Controls.rewiredPlayer.controllers.AddController(c, true);
                }
            }
            #endif
        }

        ReInput.controllers.AutoAssignJoysticks();
    }

    private static void AdvancePlayer(int playerNumberOneBased)
    {
        PlayerControls controls = playerNumberOneBased switch
        {
            2 => player2Controls,
            3 => player3Controls,
            4 => player4Controls,
            _ => player1Controls
        };

        if (controls.rewiredPlayer == null)
        {
            controls.rewiredPlayer = Rewired.ReInput.players.GetPlayer(
                "Player " + playerNumberOneBased
            );
        }

        if (controls.rewiredPlayer.isPlaying == false)
        {
            controls.holdingLeft    = false;
            controls.holdingRight   = false;
            controls.holdingDown    = false;
            controls.holdingUp      = false;
            controls.holdingJump    = false;
            controls.holdingPowerup = false;
            controls.holdingPause   = false;

            controls.pressedLeft     = false;
            controls.pressedRight    = false;
            controls.pressedJump     = false;
            controls.releasedJump    = false;
            controls.pressedPowerup  = false;
            controls.pressedPause    = false;

            return;
        }

        var holdingLeftBefore    = controls.holdingLeft;
        var holdingRightBefore   = controls.holdingRight;
        var holdingJumpBefore    = controls.holdingJump;
        var holdingPowerupBefore = controls.holdingPowerup;
        var holdingPauseBefore   = controls.holdingPause;

        var stick = new Vector2(
            controls.rewiredPlayer.GetAxis("Horizontal"),
            controls.rewiredPlayer.GetAxis("Vertical")
        );
        if (stick.sqrMagnitude < StickDeadzone * StickDeadzone)
            stick = new Vector2(0, 0);
        else if (stick.sqrMagnitude > 1 * 1)
            stick.Normalize();

        controls.holdingLeft  = (stick.x < -0.707f);
        controls.holdingRight = (stick.x >  0.707f);
        controls.holdingDown  = (stick.y < -0.707f);
        controls.holdingUp    = (stick.y >  0.707f);

        controls.holdingJump    = false;
        controls.holdingPowerup = false;

        var isMouseOverUI = IngameUI.IsPositionOverUI(Input.mousePosition);

        controls.holdingJump = controls.rewiredPlayer.GetButton("Jump");
        if (controls.rewiredPlayer.controllers.hasMouse == true &&
            Input.GetMouseButton(0) == true &&
            isMouseOverUI == false)
        {
            controls.holdingJump = true;
        }
        controls.holdingPowerup = controls.rewiredPlayer.GetButton("Powerup");
        controls.holdingPause   = controls.rewiredPlayer.GetButton("Menu");

        #if UNITY_EDITOR
        if (playerNumberOneBased == 1 &&
            Game.instance != null &&
            Game.instance.map1 != null)
        {
            replayingInputFrameNumber += 1;
            int f = replayingInputFrameNumber;

            if (inputToReplay?.Length > f)
                controls.holdingJump = (inputToReplay[f] == 'J');

            if (recordingInput.Length > f)
                recordingInput = recordingInput[..f];
            recordingInput += controls.holdingJump ? "J" : " ";
        }
        #endif

        controls.pressedLeft =
            (controls.holdingLeft == true && holdingLeftBefore == false);
        controls.pressedRight =
            (controls.holdingRight == true && holdingRightBefore == false);
        controls.pressedJump =
            (controls.holdingJump == true && holdingJumpBefore == false);
        controls.pressedPowerup =
            (controls.holdingPowerup == true && holdingPowerupBefore == false);
        controls.releasedJump =
            (controls.holdingJump == false && holdingJumpBefore == true);
        controls.pressedPause =
            (controls.holdingPause == true && holdingPauseBefore == false);
    }

    public static void Advance()
    {
        // read from Rewired
        if (inputRewired == null)
            inputRewired = Rewired.ReInput.players.GetPlayer(0);

        gamepadConnected = inputRewired.controllers.joystickCount > 0;

        AdvancePlayer(1);
        AdvancePlayer(2);
        AdvancePlayer(3);
        AdvancePlayer(4);

        #if UNITY_TVOS && !UNITY_EDITOR
            UnityEngine.tvOS.Remote.allowExitToHome = false;
        #endif

        var holdingConfirmBefore = holdingConfirm;
        var holdingMenuBefore    = holdingMenu;

        UpdateMenuInputs();

        holdingConfirm = false;
        holdingMenu    = false;

        {
            var isMouseOverUI = IngameUI.IsPositionOverUI(Input.mousePosition);

            holdingConfirm = inputRewired.GetButton("Jump");
            holdingMenu    = inputRewired.GetButton("Menu");
        }

        pressedConfirm  = holdingConfirm == true  && holdingConfirmBefore == false;
        pressedMenu     = holdingMenu    == true  && holdingMenuBefore    == false;
        releasedConfirm = holdingConfirm == false && holdingConfirmBefore == true;

        UpdateMostRecentInputType();
        UpdateMostRecentController();
        UpdateUsingSelectionCursor();

        #if UNITY_EDITOR || DEVELOPMENT_BUILD
        {
            if (Input.GetKeyDown(KeyCode.F12))
            {
                ScreenshotGrabber.Capture4KScreenshots();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                Game.instance.ChangeNumberOfMaps(1);
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                Game.instance.ChangeNumberOfMaps(2);
                RedistributeControllersForDebugging();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                Game.instance.ChangeNumberOfMaps(3);
                RedistributeControllersForDebugging();
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                Game.instance.ChangeNumberOfMaps(4);
                RedistributeControllersForDebugging();
            }

            if (Input.GetKeyDown(KeyCode.R))
                Map.instance?.player?.Die();

            // if (Input.GetKeyDown(KeyCode.F) && Transition.instance == null)
            // {
            //     Game.RandomlySelectSave();
            //     Transition.GoToGame();
            // }

            if (Input.GetKeyDown(KeyCode.I))
                Map.instance?.player?.invincibility.Activate();

            // conflict with "Vertical"
            // if (Input.GetKeyDown(KeyCode.W))
            //     IngameUI.instance?.winMenu?.Open();

            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                GameInput.usingSelectionCursor = true;

                var debugPanel = IngameUI.instance?.debugPanel;
                if (debugPanel != null && debugPanel.gameObject.activeSelf == false)
                    debugPanel.gameObject.SetActive(true);
                else
                    debugPanel?.OnClickOpenClose();
            }
        }
        #endif
    }

    public static bool DidAnyonePressPause()
    {
        return
            player1Controls.pressedPause == true ||
            player2Controls.pressedPause == true ||
            player3Controls.pressedPause == true ||
            player4Controls.pressedPause == true;
    }

    private static void RedistributeControllersForDebugging()
    {
        var controllers = Rewired.ReInput.controllers.Controllers.ToArray();

        for (int n = 0; n < controllers.Length; n += 1)
        {
            var controller = controllers[n];
            Player((n % 4) + 1).rewiredPlayer.controllers.AddController(controller, true);
        }
    }

    private static void UpdateMenuInputs()
    {
        menuMoveDirection = null;
        menuSlideInput = Vector2.zero;

        if (player1Controls.holdingLeft)  menuMoveDirection = Vector2.left;
        if (player1Controls.holdingRight) menuMoveDirection = Vector2.right;
        if (player1Controls.holdingDown)  menuMoveDirection = Vector2.down;
        if (player1Controls.holdingUp)    menuMoveDirection = Vector2.up;

        if (player1Controls.holdingLeft)  menuSlideInput.x = -1;
        if (player1Controls.holdingRight) menuSlideInput.x = 1;
        if (player1Controls.holdingDown)  menuSlideInput.y = -1;
        if (player1Controls.holdingUp)    menuSlideInput.y = 1;

        player1Controls.tvOSMoveDirection = null;
        player2Controls.tvOSMoveDirection = null;

        #if UNITY_TVOS && !UNITY_EDITOR
        {
            if (Input.touches.Length > 0)
            {
                var touch = Input.touches[0];

                tvOSAccumulatedSwipe *= 0.96f;
                tvOSAccumulatedSwipe += touch.deltaPosition;

                if (tvOSAccumulatedSwipe.sqrMagnitude >
                    TvOSSwipeDistance * TvOSSwipeDistance)
                {
                    menuMoveDirection = tvOSAccumulatedSwipe.normalized;
                    tvOSAccumulatedSwipe = Vector2.zero;

                    bool PlayerHasSiriRemote(PlayerControls p) =>
                        p.rewiredPlayer.controllers.Controllers.Any(
                            c => c.hardwareTypeGuid == HardwareGuidForSiriRemote
                        );

                    if (PlayerHasSiriRemote(player1Controls))
                        player1Controls.tvOSMoveDirection = menuMoveDirection;
                    else if (PlayerHasSiriRemote(player2Controls))
                        player2Controls.tvOSMoveDirection = menuMoveDirection;
                }

                menuSlideInput = touch.deltaPosition * TvOSSliderSensitivity;
            }
            else
            {
                tvOSAccumulatedSwipe = Vector2.zero;
            }
        }
        #endif
    }

    private static InputType DefaultInputType()
    {
        #if UNITY_IOS && !UNITY_EDITOR
            return InputType.TouchScreen;
        #elif UNITY_TVOS && !UNITY_EDITOR
            return InputType.SiriRemote;
        #else
            return InputType.Keyboard;
        #endif
    }

    private static void UpdateMostRecentInputType()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            if (Input.GetMouseButton(0) == true)
            {
                mostRecentInputType = InputType.TouchScreen;
                return;
            }
        }
        #endif

        #if UNITY_SWITCH
        {
            mostRecentInputType = InputType.GamepadSwitch;
            return;
        }
        #else
        {
            if (inputRewired.GetAnyButton() == true)
            {
                var controller = inputRewired.controllers.GetLastActiveController();
                var inputType = InputTypeForController(controller);
                if (inputType != null)
                    mostRecentInputType = inputType.Value;
            }
        }
        #endif
    }

    public static InputType? InputTypeForController(Rewired.Controller controller)
    {
        if (controller == null)
            return null;

        if (controller.hardwareTypeGuid == HardwareGuidForSiriRemote)
            return InputType.SiriRemote;

        if (
            controller.hardwareTypeGuid == HardwareGuidForPS2 ||
            controller.hardwareTypeGuid == HardwareGuidForPS3 ||
            controller.hardwareTypeGuid == HardwareGuidForPS4 ||
            controller.hardwareTypeGuid == HardwareGuidForPS5 ||
            controller.name.ToLower().Contains("dualshock") == true ||
            controller.name.ToLower().Contains("dualsense") == true
        )
            return InputType.GamepadPlaystation;

        if (controller.hardwareTypeGuid == HardwareGuidForMfiController)
            return InputType.GamepadMfi;

        if (controller.type is ControllerType.Joystick or ControllerType.Custom)
            return InputType.GamepadXbox;

        if (controller is Rewired.Keyboard)
            return InputType.Keyboard;

        if (controller is Rewired.Mouse)
            return InputType.Mouse;

        return null;
    }

    private static void UpdateMostRecentController()
    {
        foreach (var c in ReInput.controllers.Controllers)
        {
            if (c.GetAnyButtonChanged() == true)
                mostRecentController = c;
        }
    }

    private static void UpdateUsingSelectionCursor()
    {
        #if UNITY_EDITOR || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
        {
            float sqDist = (Input.mousePosition - lastMousePosition).sqrMagnitude;
            lastMousePosition = Input.mousePosition;

            if (sqDist >= DistanceToActivateMouse * DistanceToActivateMouse ||
                Input.GetMouseButton(0) == true)
            {
                usingSelectionCursor = false;
                Cursor.visible = true;
            }
            
            if (player1Controls.holdingLeft || player1Controls.holdingRight ||
                player1Controls.holdingDown || player1Controls.holdingUp ||
                player1Controls.holdingJump || player1Controls.holdingPowerup ||
                holdingConfirm || holdingMenu)
            {
                usingSelectionCursor = true;
                Cursor.visible = false;

                ClearHoverStateForButtonUnderMouse();
            }
        }
        #elif UNITY_IOS || UNITY_ANDROID
        {
            // unclear whether filtering out empty-strings is necessary.
            // on windows, the list fills with empty-strings. god knows why.
            var joysticks = Input.GetJoystickNames().Where(s => s != "").ToArray();
            if (joysticks.Length > 0)
                usingSelectionCursor = true;
            else
                usingSelectionCursor = false;
        }
        #else // tvOS
        {
            usingSelectionCursor = true;
        }
        #endif
    }

    private static void ClearHoverStateForButtonUnderMouse()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if(results.Count > 0)
            results.FirstOrDefault(rr => rr.gameObject.GetComponent<UnityEngine.UI.Button>() != null)
                .gameObject?.GetComponent<UnityEngine.UI.Button>().OnPointerExit(null);
    }

    public static bool IsMouseOnScreen() =>
        Input.mousePosition.x >= 0 && Input.mousePosition.y >= 0 && Input.mousePosition.x < Screen.width && Input.mousePosition.y < Screen.height;

}
