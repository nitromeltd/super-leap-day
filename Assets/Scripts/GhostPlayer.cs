using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using System;
using System.Collections.Generic;
using System.Linq;

public class GhostPlayer : MonoBehaviour
{
    public struct SpriteData
    {
        public float xRelative;
        public float yRelative;
        public float xScale;
        public string animationId;
        public int animationFrame;
        public int sortingLayerID;
        public int sortingOrder;
    }

    public Player trackingPlayer;
    public string trackingMultiplayerPlayerId;

    private Map map;
    private List<SpriteRenderer> spriteRenderers = new();
    private Character? character;
    private AsyncOperationHandle<GameObject> characterAddressableToRelease;
    private Player playerToUseAsArtSource;
    private bool isActive = true;
    private float alpha = 1.0f;

    public PlayerOffscreenIndicator playerOffscreenIndicator;

    public Character? Character => this.character;
    public bool IsConnected => this.isActive;

    public void OnDestroy()
    {
        if (this.characterAddressableToRelease.IsValid())
            Addressables.Release(this.characterAddressableToRelease);

        Destroy(this.playerOffscreenIndicator.gameObject);

        IngameUI.instance.layoutPortrait.progressBar.NotifyGhostPlayerIsBeingDestroyed(this);
        IngameUI.instance.layoutLandscape.progressBar.NotifyGhostPlayerIsBeingDestroyed(this);
        IngameUI.instance.layoutLandscape2P.progressBar.NotifyGhostPlayerIsBeingDestroyed(this);
    }

    public static GhostPlayer Make(Map forMap, Player trackingPlayer)
    {
        var gameObject = new GameObject(
            $"Ghost of {trackingPlayer.name} for Map {forMap.mapNumber}"
        );
        gameObject.transform.parent = forMap.transform;

        var ghost = gameObject.AddComponent<GhostPlayer>();
        ghost.map = forMap;
        ghost.trackingPlayer = trackingPlayer;
        ghost.character = trackingPlayer.Character();
        ghost.playerToUseAsArtSource = trackingPlayer;
        ghost.spriteRenderers = new List<SpriteRenderer>();
        ghost.playerOffscreenIndicator = PlayerOffscreenIndicator.Make(forMap, ghost);

        Util.SetLayerRecursively(gameObject.transform, forMap.Layer(), true);

        return ghost;
    }

    public static GhostPlayer Make(Map forMap, string trackingMultiplayerPlayerId)
    {
        var gameObject = new GameObject(
            $"Ghost of multiplayer {trackingMultiplayerPlayerId} " +
            $"for Map {forMap.mapNumber}"
        );
        gameObject.transform.parent = forMap.transform;

        var ghost = gameObject.AddComponent<GhostPlayer>();
        ghost.map = forMap;
        ghost.trackingMultiplayerPlayerId = trackingMultiplayerPlayerId;
        ghost.spriteRenderers = new List<SpriteRenderer>();
        ghost.playerOffscreenIndicator = PlayerOffscreenIndicator.Make(forMap, ghost);

        Util.SetLayerRecursively(gameObject.transform, forMap.Layer(), true);

        return ghost;
    }

    public static SpriteData[] GetGhostSpriteData(Player player)
    {
        var headset = player.grabbingOntoItem as MultiplayerHeadset;
        if (headset != null)
        {
            var sprites = headset.CharacterSprites();
            return sprites.Select(
                sr => GetSpriteDataForSpriteRenderer(sr, player)
            ).ToArray();
        }

        if (player is PlayerPuffer)
        {
            var puffer = player as PlayerPuffer;
            return new []
            {
                GetSpriteDataForSpriteRenderer(puffer.headSpriteRenderer, player),
                GetSpriteDataForSpriteRenderer(player.spriteRenderer, player),
                GetSpriteDataForSpriteRenderer(player.frontArmSpriteRenderer, player),
                GetSpriteDataForSpriteRenderer(player.backArmSpriteRenderer, player)
            };
        }

        return new []
        {
            GetSpriteDataForSpriteRenderer(player.spriteRenderer, player),
            GetSpriteDataForSpriteRenderer(player.frontArmSpriteRenderer, player),
            GetSpriteDataForSpriteRenderer(player.backArmSpriteRenderer, player)
        };
    }

    private static SpriteData GetSpriteDataForSpriteRenderer(
        SpriteRenderer spriteRenderer, Player relatingToPlayer
    )
    {
        var sprite = spriteRenderer.sprite;
        var spriteData = new SpriteData
        {
            sortingLayerID = spriteRenderer.sortingLayerID,
            sortingOrder = spriteRenderer.sortingOrder,
            xScale = spriteRenderer.transform.lossyScale.x / relatingToPlayer.transform.localScale.x,
            xRelative = spriteRenderer.transform.localPosition.x,
            yRelative = spriteRenderer.transform.localPosition.y
        };
        {
            var pos = relatingToPlayer.transform.worldToLocalMatrix.MultiplyPoint3x4(
                spriteRenderer.transform.position
            );
            spriteData.xRelative = pos.x;
            spriteData.yRelative = pos.y;
        }

        SpriteData? MatchInAnim(string id, Animated.Animation checkAgainstAnimation)
        {
            if (checkAgainstAnimation?.sprites.Contains(sprite) != true)
                return null;

            spriteData.animationId = id;
            spriteData.animationFrame =
                Array.IndexOf(checkAgainstAnimation.sprites, sprite);
            return spriteData;
        }

        SpriteData? MatchInSpriteArray(string id, Sprite[] checkAgainstSprites)
        {
            if (checkAgainstSprites?.Contains(sprite) != true)
                return null;

            spriteData.animationId = id;
            spriteData.animationFrame = Array.IndexOf(checkAgainstSprites, sprite);
            return spriteData;
        }

        SpriteData? MatchingSprite(string id, Sprite checkAgainstSprite)
        {
            if (sprite != checkAgainstSprite)
                return null;

            spriteData.animationId = id;
            return spriteData;
        }

        var headsetSprites =
            Assets.instance.allCharacterSpritesInMultiplayerHeadset;
        var p = relatingToPlayer;
        var puffer = p as PlayerPuffer;

        return
            MatchInAnim("Standing",                     p.animationStanding) ??
            MatchInAnim("Running",                      p.animationRunning) ??
            MatchInAnim("RunningPanicked",              p.animationRunningPanicked) ??
            MatchInAnim("RunningTooFast",               p.animationRunningTooFast) ??
            MatchInAnim("Jumping",                      p.animationJumping) ??
            MatchInAnim("JumpingDown",                  p.animationJumpingDown) ??
            MatchInAnim("DoubleJumping",                p.animationDoubleJumping) ??
            MatchInAnim("StompFrame",                   p.animationStompFrame) ??
            MatchInAnim("AfterStomp",                   p.animationAfterStomp) ??
            MatchInAnim("WallSliding",                  p.animationWallSliding) ??
            MatchInAnim("IceSliding",                   p.animationIceSliding) ??
            MatchInAnim("PoleSliding",                  p.animationPoleSliding) ??
            MatchInAnim("PoleSlidingBackArm",           p.animationPoleSlidingBackArm) ??
            MatchInAnim("PoleSlidingTwist",             p.animationPoleSlidingTwist) ??
            MatchInAnim("Spring",                       p.animationSpring) ??
            MatchInAnim("Hanging",                      p.animationHanging) ??
            MatchInAnim("Ball",                         p.animationBall) ??
            MatchingSprite("HangingFrontArm",           p.animationHangingFrontArm) ??
            MatchInAnim("Hit",                          p.animationHit) ??
            MatchInAnim("Dead",                         p.animationDead) ??
            MatchInAnim("Respawn",                      p.animationRespawn) ??
            MatchInAnim("GrabSide",                     p.animationGrabSide) ??
            MatchingSprite("GrabSideFrontArm",          p.animationGrabSideFrontArm) ??
            MatchInAnim("GrabStand",                    p.animationGrabStand) ??
            MatchingSprite("GrabStandFrontArm",         p.animationGrabStandFrontArm) ??
            MatchInAnim("HoldingBreath",                p.animationHoldingBreath) ??
            MatchInAnim("SwimmingSurface",              p.animationSwimmingSurface) ??
            MatchInAnim("SwimmingCeiling",              p.animationSwimmingCeiling) ??
            MatchInAnim("TransitionWallBounce",         p.animationTransitionWallBounce) ??
            MatchInAnim("TransitionWallBouncePanicked", p.animationTransitionWallBouncePanicked) ??
            MatchInAnim("TransitionRunToWallSlide",     p.animationTransitionRunToWallSlide) ??
            MatchInAnim("TransitionWallSlideToRun",     p.animationTransitionWallSlideToRun) ??
            MatchInAnim("TransitionWallSlideToDrop",    p.animationTransitionWallSlideToDrop) ??
            MatchInAnim("TransitionJumpToJumpDown",     p.animationTransitionJumpToJumpDown) ??
            MatchInAnim("TransitionSwimToSwimUp",       p.animationTransitionSwimToSwimUp) ??

            MatchInAnim("HeadBlank",                          puffer?.animationHeadBlank) ??
            MatchInAnim("HeadRunning",                        puffer?.animationHeadRunning) ??
            MatchInAnim("HeadRunningPanicked",                puffer?.animationHeadRunningPanicked) ??
            MatchInAnim("HeadRunningTooFast",                 puffer?.animationHeadRunningTooFast) ??
            MatchInAnim("HeadJumping",                        puffer?.animationHeadJumping) ??
            MatchInAnim("HeadJumpingDown",                    puffer?.animationHeadJumpingDown) ??
            MatchInAnim("HeadStompFrame",                     puffer?.animationHeadStompFrame) ??
            MatchInAnim("HeadAfterStomp",                     puffer?.animationHeadAfterStomp) ??
            MatchInAnim("HeadWallSliding",                    puffer?.animationHeadWallSliding) ??
            MatchInAnim("HeadIceSliding",                     puffer?.animationHeadIceSliding) ??
            MatchInAnim("HeadPoleSliding",                    puffer?.animationHeadPoleSliding) ??
            MatchInAnim("HeadPoleSlidingTwist",               puffer?.animationHeadPoleSlidingTwist) ??
            MatchInAnim("HeadSpring",                         puffer?.animationHeadSpring) ??
            MatchInAnim("HeadHanging",                        puffer?.animationHeadHanging) ??
            MatchInAnim("HeadGrabSide",                       puffer?.animationHeadGrabSide) ??
            MatchInAnim("HeadGrabStand",                      puffer?.animationHeadGrabStand) ??
            MatchInAnim("HeadSwimmingCeiling",                puffer?.animationHeadSwimmingCeiling) ??
            MatchInAnim("HeadTransitionWallBounce",           puffer?.animationHeadTransitionWallBounce) ??
            MatchInAnim("HeadTransitionWallBouncePanicked",   puffer?.animationHeadTransitionWallBouncePanicked) ??
            MatchInAnim("HeadTransitionRunToWallSlide",       puffer?.animationHeadTransitionRunToWallSlide) ??
            MatchInAnim("HeadTransitionWallSlideToRun",       puffer?.animationHeadTransitionWallSlideToRun) ??
            MatchInAnim("HeadTransitionWallSlideToDrop",      puffer?.animationHeadTransitionWallSlideToDrop) ??
            MatchInAnim("HeadTransitionJumpToJumpDown",       puffer?.animationHeadTransitionJumpToJumpDown) ??
            MatchInAnim("HeadTransitionSwimToSwimUp",         puffer?.animationHeadTransitionSwimToSwimUp) ??
            MatchInAnim("BodyInflate1JumpUp",                 puffer?.animationBodyInflate1JumpUp) ??
            MatchInAnim("BodyInflate1JumpDown",               puffer?.animationBodyInflate1JumpDown) ??
            MatchInAnim("BodyInflate1JumpHold",               puffer?.animationBodyInflate1JumpHold) ??
            MatchInAnim("HeadInflate1",                       puffer?.animationHeadInflate1) ??
            MatchInAnim("BodyInflate2JumpUp",                 puffer?.animationBodyInflate2JumpUp) ??
            MatchInAnim("BodyInflate2JumpDown",               puffer?.animationBodyInflate2JumpDown) ??
            MatchInAnim("BodyInflate2JumpHold",               puffer?.animationBodyInflate2JumpHold) ??
            MatchInAnim("HeadInflate2",                       puffer?.animationHeadInflate2) ??
            MatchInAnim("BodyInflate3JumpUp",                 puffer?.animationBodyInflate3JumpUp) ??
            MatchInAnim("BodyInflate3JumpDown",               puffer?.animationBodyInflate3JumpDown) ??
            MatchInAnim("BodyInflate3JumpHold",               puffer?.animationBodyInflate3JumpHold) ??
            MatchInAnim("HeadInflate3",                       puffer?.animationHeadInflate3) ??
            MatchInAnim("BodyInflate4JumpUp",                 puffer?.animationBodyInflate4JumpUp) ??
            MatchInAnim("BodyInflate4JumpDown",               puffer?.animationBodyInflate4JumpDown) ??
            MatchInAnim("BodyInflate4JumpHold",               puffer?.animationBodyInflate4JumpHold) ??
            MatchInAnim("HeadInflate4",                       puffer?.animationHeadInflate4) ??
            MatchInAnim("HeadDead",                           puffer?.animationHeadDead) ??
            MatchInAnim("BodyShoes",                          puffer?.animationBodyShoes) ??
            MatchInAnim("EffectHeadPop",                      puffer?.animationEffectHeadPop) ??
            MatchInAnim("DeflatedHead",                       puffer?.animationDeflatedHead) ??

            MatchInSpriteArray("MultiplayerHeadsetCharacter",   headsetSprites) ??
            new SpriteData {};
    }

    public void Advance()
    {
        this.alpha = Util.Slide(this.alpha, this.isActive ? 1 : 0, 0.04f);

        if (this.trackingPlayer != null)
        {
            var sprites = GetGhostSpriteData(this.trackingPlayer);
            UpdateSprites(
                this.trackingPlayer.position.x,
                this.trackingPlayer.position.y,
                this.trackingPlayer.transform.rotation.z,
                this.trackingPlayer.transform.localScale.x,
                sprites
            );
        }
        else
        {
            foreach (var sr in this.spriteRenderers)
            {
                sr.color = new Color(1, 1, 1, this.alpha);
            }
        }

        this.playerOffscreenIndicator.Advance();
    }

    public void NotifyConnectionStateChanged(bool connected)
    {
        this.isActive = connected;
    }

    public void NotifyReceivedMessage(GameCenterMultiplayer.MsgHereIAm msg)
    {
        if (this.character == null && msg.character != null)
        {
            var addressableAsset = msg.character switch
            {
                global::Character.Goop   => Assets.instance.goop,
                global::Character.Sprout => Assets.instance.sprout,
                global::Character.Puffer => Assets.instance.puffer,
                global::Character.King   => Assets.instance.king,
                _                        => Assets.instance.yolk,
            };

            if (addressableAsset.OperationHandle.IsValid() == false)
            {
                // this asset is not loaded and hasn't been requested.
                // request it and mark it as something that we should release.
                this.characterAddressableToRelease =
                    addressableAsset.LoadAssetAsync<GameObject>();
            }
            else if (
                addressableAsset.OperationHandle.IsValid() == true &&
                addressableAsset.OperationHandle.IsDone == true
            )
            {
                // it's loaded, make use of it.
                this.character = msg.character;

                var result = addressableAsset.OperationHandle.Result as GameObject;
                this.playerToUseAsArtSource = result.GetComponent<Player>();
            }
        }

        UpdateSprites(msg.x, msg.y, msg.rotation, msg.xScale, msg.sprites);
    }

    private void UpdateSprites(
        float x, float y, float rotation, float xScale, SpriteData[] sprites
    )
    {
        this.transform.position = new Vector2(x, y);
        this.transform.localScale = new Vector3(xScale, 1, 1);
        this.transform.localRotation = Quaternion.Euler(0, 0, rotation);

        var p = this.playerToUseAsArtSource ?? Game.instance.map1.player;
        var puffer = p as PlayerPuffer;

        Sprite Frame(Animated.Animation a, int frame)
        {
            if (a == null) return null;
            return a.sprites[frame % a.sprites.Length];
        }
        Sprite SpriteForSpriteData(string animationId, int frame) => animationId switch
        {
            "Standing"                     => Frame(p.animationStanding,                     frame),
            "Running"                      => Frame(p.animationRunning,                      frame),
            "RunningPanicked"              => Frame(p.animationRunningPanicked,              frame),
            "RunningTooFast"               => Frame(p.animationRunningTooFast,               frame),
            "Jumping"                      => Frame(p.animationJumping,                      frame),
            "JumpingDown"                  => Frame(p.animationJumpingDown,                  frame),
            "DoubleJumping"                => Frame(p.animationDoubleJumping,                frame),
            "StompFrame"                   => Frame(p.animationStompFrame,                   frame),
            "AfterStomp"                   => Frame(p.animationAfterStomp,                   frame),
            "WallSliding"                  => Frame(p.animationWallSliding,                  frame),
            "IceSliding"                   => Frame(p.animationIceSliding,                   frame),
            "PoleSliding"                  => Frame(p.animationPoleSliding,                  frame),
            "PoleSlidingBackArm"           => Frame(p.animationPoleSlidingBackArm,           frame),
            "PoleSlidingTwist"             => Frame(p.animationPoleSlidingTwist,             frame),
            "Spring"                       => Frame(p.animationSpring,                       frame),
            "Hanging"                      => Frame(p.animationHanging,                      frame),
            "Ball"                         => Frame(p.animationBall,                         frame),
            "HangingFrontArm"              => p.animationHangingFrontArm,
            "Hit"                          => Frame(p.animationHit,                          frame),
            "Dead"                         => Frame(p.animationDead,                         frame),
            "Respawn"                      => Frame(p.animationRespawn,                      frame),
            "GrabSide"                     => Frame(p.animationGrabSide,                     frame),
            "GrabSideFrontArm"             => p.animationGrabSideFrontArm,
            "GrabStand"                    => Frame(p.animationGrabStand,                    frame),
            "GrabStandFrontArm"            => p.animationGrabStandFrontArm,
            "HoldingBreath"                => Frame(p.animationHoldingBreath,                frame),
            "SwimmingSurface"              => Frame(p.animationSwimmingSurface,              frame),
            "SwimmingCeiling"              => Frame(p.animationSwimmingCeiling,              frame),
            "TransitionWallBounce"         => Frame(p.animationTransitionWallBounce,         frame),
            "TransitionWallBouncePanicked" => Frame(p.animationTransitionWallBouncePanicked, frame),
            "TransitionRunToWallSlide"     => Frame(p.animationTransitionRunToWallSlide,     frame),
            "TransitionWallSlideToRun"     => Frame(p.animationTransitionWallSlideToRun,     frame),
            "TransitionWallSlideToDrop"    => Frame(p.animationTransitionWallSlideToDrop,    frame),
            "TransitionJumpToJumpDown"     => Frame(p.animationTransitionJumpToJumpDown,     frame),
            "TransitionSwimToSwimUp"       => Frame(p.animationTransitionSwimToSwimUp,       frame),

            "HeadBlank"                        => Frame(puffer?.animationHeadBlank, frame),
            "HeadRunning"                      => Frame(puffer?.animationHeadRunning, frame),
            "HeadRunningPanicked"              => Frame(puffer?.animationHeadRunningPanicked, frame),
            "HeadRunningTooFast"               => Frame(puffer?.animationHeadRunningTooFast, frame),
            "HeadJumping"                      => Frame(puffer?.animationHeadJumping, frame),
            "HeadJumpingDown"                  => Frame(puffer?.animationHeadJumpingDown, frame),
            "HeadStompFrame"                   => Frame(puffer?.animationHeadStompFrame, frame),
            "HeadAfterStomp"                   => Frame(puffer?.animationHeadAfterStomp, frame),
            "HeadWallSliding"                  => Frame(puffer?.animationHeadWallSliding, frame),
            "HeadIceSliding"                   => Frame(puffer?.animationHeadIceSliding, frame),
            "HeadPoleSliding"                  => Frame(puffer?.animationHeadPoleSliding, frame),
            "HeadPoleSlidingTwist"             => Frame(puffer?.animationHeadPoleSlidingTwist, frame),
            "HeadSpring"                       => Frame(puffer?.animationHeadSpring, frame),
            "HeadHanging"                      => Frame(puffer?.animationHeadHanging, frame),
            "HeadGrabSide"                     => Frame(puffer?.animationHeadGrabSide, frame),
            "HeadGrabStand"                    => Frame(puffer?.animationHeadGrabStand, frame),
            "HeadSwimmingCeiling"              => Frame(puffer?.animationHeadSwimmingCeiling, frame),
            "HeadTransitionWallBounce"         => Frame(puffer?.animationHeadTransitionWallBounce, frame),
            "HeadTransitionWallBouncePanicked" => Frame(puffer?.animationHeadTransitionWallBouncePanicked, frame),
            "HeadTransitionRunToWallSlide"     => Frame(puffer?.animationHeadTransitionRunToWallSlide, frame),
            "HeadTransitionWallSlideToRun"     => Frame(puffer?.animationHeadTransitionWallSlideToRun, frame),
            "HeadTransitionWallSlideToDrop"    => Frame(puffer?.animationHeadTransitionWallSlideToDrop, frame),
            "HeadTransitionJumpToJumpDown"     => Frame(puffer?.animationHeadTransitionJumpToJumpDown, frame),
            "HeadTransitionSwimToSwimUp"       => Frame(puffer?.animationHeadTransitionSwimToSwimUp, frame),
            "BodyInflate1JumpUp"               => Frame(puffer?.animationBodyInflate1JumpUp, frame),
            "BodyInflate1JumpDown"             => Frame(puffer?.animationBodyInflate1JumpDown, frame),
            "BodyInflate1JumpHold"             => Frame(puffer?.animationBodyInflate1JumpHold, frame),
            "HeadInflate1"                     => Frame(puffer?.animationHeadInflate1, frame),
            "BodyInflate2JumpUp"               => Frame(puffer?.animationBodyInflate2JumpUp, frame),
            "BodyInflate2JumpDown"             => Frame(puffer?.animationBodyInflate2JumpDown, frame),
            "BodyInflate2JumpHold"             => Frame(puffer?.animationBodyInflate2JumpHold, frame),
            "HeadInflate2"                     => Frame(puffer?.animationHeadInflate2, frame),
            "BodyInflate3JumpUp"               => Frame(puffer?.animationBodyInflate3JumpUp, frame),
            "BodyInflate3JumpDown"             => Frame(puffer?.animationBodyInflate3JumpDown, frame),
            "BodyInflate3JumpHold"             => Frame(puffer?.animationBodyInflate3JumpHold, frame),
            "HeadInflate3"                     => Frame(puffer?.animationHeadInflate3, frame),
            "BodyInflate4JumpUp"               => Frame(puffer?.animationBodyInflate4JumpUp, frame),
            "BodyInflate4JumpDown"             => Frame(puffer?.animationBodyInflate4JumpDown, frame),
            "BodyInflate4JumpHold"             => Frame(puffer?.animationBodyInflate4JumpHold, frame),
            "HeadInflate4"                     => Frame(puffer?.animationHeadInflate4, frame),
            "HeadDead"                         => Frame(puffer?.animationHeadDead, frame),
            "BodyShoes"                        => Frame(puffer?.animationBodyShoes, frame),
            "EffectHeadPop"                    => Frame(puffer?.animationEffectHeadPop, frame),
            "DeflatedHead"                     => Frame(puffer?.animationDeflatedHead, frame),

            "MultiplayerHeadsetCharacter"  => Assets.instance.allCharacterSpritesInMultiplayerHeadset[frame],
            _ => null
        };

        while (this.spriteRenderers.Count < sprites.Length)
        {
            var spriteObject = new GameObject("Sprite " + this.spriteRenderers.Count);
            spriteObject.layer = this.gameObject.layer;
            var spriteRenderer = spriteObject.AddComponent<SpriteRenderer>();
            spriteRenderer.transform.parent = gameObject.transform;
            spriteRenderer.material = new Material(Assets.instance.spritePlayerHologram);
            this.spriteRenderers.Add(spriteRenderer);
        }

        for (int n = 0; n < sprites.Length; n += 1)
        {
            var data = sprites[n];
            var sr = this.spriteRenderers[n];
            sr.sprite = SpriteForSpriteData(data.animationId, data.animationFrame);
            sr.color = new Color(1, 1, 1, this.alpha);
            sr.sortingLayerID = data.sortingLayerID;
            sr.sortingOrder = data.sortingOrder - 1;
            sr.transform.localPosition = new Vector2(data.xRelative, data.yRelative);
            sr.transform.localScale = new Vector2(data.xScale, 1);
            if (sr.gameObject.activeSelf == false)
                sr.gameObject.SetActive(true);
        }

        for (int n = sprites.Length; n < this.spriteRenderers.Count; n += 1)
        {
            var sr = this.spriteRenderers[n];
            if (sr.gameObject.activeSelf == true)
                sr.gameObject.SetActive(false);
        }
    }
}
