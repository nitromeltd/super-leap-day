
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;

public class GroupLayer
{
    //  A group is a set of items which will move in unison.
    //  One of the items is the leader and the rest are followers.
    //  Map.cs has affordance to call Init() and Advance() on some items later than the rest.
    //  Followers should always be late so that the leader has a chance to init/move first.

    public class Group
    {
        public struct FollowKey
        {
            public Item item;
            public int index;
        }

        public class FollowEntry
        {
            public FollowKey key;
            public Vector2 relativePosition;
            public Vector2 startPosition;
        }

        public Item leader;
        public Vector2 leaderHome;
        public Dictionary<FollowKey, FollowEntry> followers;
        public TileGrid followerTileGrid;
        public GameObject container;

        public Group(Chunk chunk, string colour)
        {
            this.followers = new Dictionary<FollowKey, FollowEntry>();

            this.container = new GameObject();
            this.container.name = colour;
            this.container.transform.parent = chunk.transform;
            var sortingGroup = this.container.AddComponent<SortingGroup>();
            sortingGroup.sortingLayerName = "Tiles - Layer B";
            sortingGroup.sortingOrder = 1;
        }

        public Vector2 FollowerPosition(Item follower, int slot = 0)
        {
            if (this.leader != null)
            {
                var key = new FollowKey { item = follower, index = slot };
                return this.leader.position + this.followers[key].relativePosition;
            }
            else
            {
                return follower.position;
            }
        }

        public void RemoveFollower(Item follower, int i = 0)
        {
            var key = new Group.FollowKey { item = follower, index = i};
            this.followers.Remove(key);
        }

        public Vector2 Offset() => this.leader.position - this.leaderHome;
        public float OffsetX() => this.leader.position.x - this.leaderHome.x;
        public float OffsetY() => this.leader.position.y - this.leaderHome.y;
    }

    public Chunk chunk;
    public Group[] allGroups;
    public Group[,] groupForCell;
    private int xMin;
    private int yMin;
    private int columns;
    private int rows;

    public GroupLayer(
        Chunk chunk,
        NitromeEditor.TileLayer tileLayer,
        int columns,
        int rows
    )
    {
        this.chunk = chunk;
        this.groupForCell = new Group[columns, rows];
        this.columns = columns;
        this.rows = rows;
        this.xMin = chunk.xMin - chunk.boundaryMarkers.bottomLeft.x;
        this.yMin = chunk.yMin - chunk.boundaryMarkers.bottomLeft.y;

        var groupList = new List<Group>();

        if (tileLayer == null) return;

        var start = FindStart(tileLayer, columns, rows);
        while (start.HasValue)
        {
            var g = FindGroup(
                chunk, tileLayer, columns, rows, start.Value.x, start.Value.y
            );
            groupList.Add(g);
            start = FindStart(tileLayer, columns, rows);
        }

        this.allGroups = groupList.ToArray();
    }

    public Group GetGroupForCell(int mapTx, int mapTy)
    {
        int tx = mapTx - this.xMin;
        int ty = mapTy - this.yMin;

        if (tx < 0 || tx >= this.columns) return null;
        if (ty < 0 || ty >= this.rows   ) return null;

        var g = this.groupForCell[tx, ty];
        return g;
    }

    public Group RegisterLeader(Item leader, int mapTx, int mapTy)
    {
        int tx = mapTx - this.xMin;
        int ty = mapTy - this.yMin;

        if (tx < 0 || tx >= this.columns) return null;
        if (ty < 0 || ty >= this.rows   ) return null;

        var g = this.groupForCell[tx, ty];
        if (g == null) return null;

        g.leader = leader;
        g.leaderHome = leader.position;

        // foreach (var k in g.followers.Keys.ToArray())
        foreach (var entry in g.followers)
        {
            entry.Value.relativePosition = entry.Value.startPosition - g.leader.position;
        }

        return g;
    }

    public Group RegisterFollower(
        Item follower, int slot, Vector2 position, int mapTx, int mapTy
    )
    {
        int tx = mapTx - this.xMin;
        int ty = mapTy - this.yMin;

        if (tx < 0 || tx >= this.columns) return null;
        if (ty < 0 || ty >= this.rows   ) return null;

        var g = this.groupForCell[tx, ty];
        if (g == null) return null;

        var key = new Group.FollowKey { item = follower, index = slot };
        if (g.leader != null)
        {
            g.followers[key] = new Group.FollowEntry
            {
                key = key,
                startPosition = position,
                relativePosition = position - g.leader.position
            };
        }
        else
        {
            // we'll fill in the relativePosition when the leader is added to the group
            g.followers[key] = new Group.FollowEntry
            {
                key = key,
                startPosition = position
            };
        }

        return g;
    }

    public Group RegisterFollower(Item follower, int mapTx, int mapTy)
    {
        return RegisterFollower(follower, 0, follower.position, mapTx, mapTy);
    }

    private XY? FindStart(
        NitromeEditor.TileLayer tileLayer,
        int columns,
        int rows
    )
    {
        for (int y = 0; y < rows; y += 1)
        {
            for (int x = 0; x < columns; x += 1)
            {
                if (this.groupForCell[x, y] != null) continue; // already taken

                var t = tileLayer.tiles[x + (columns * y)];
                if (t.tile != null && t.tile != "")
                    return new XY { x = x, y = y };
            }
        }

        return null;
    }

    private Group FindGroup(
        Chunk chunk,
        NitromeEditor.TileLayer tileLayer,
        int columns,
        int rows,
        int startX,
        int startY
    )
    {
        var colour = tileLayer.tiles[startX + (columns * startY)].tile;

        var group = new HashSet<XY>();
        var toAdd = new List<XY> {
            new XY { x = startX, y = startY }
        };

        while (toAdd.Count > 0)
        {
            var addThisPass = toAdd.ToArray();
            toAdd.Clear();

            foreach (var addThis in addThisPass)
            {
                group.Add(addThis);

                var neighbours = new []
                {
                    new XY(addThis.x - 1, addThis.y),
                    new XY(addThis.x + 1, addThis.y),
                    new XY(addThis.x, addThis.y - 1),
                    new XY(addThis.x, addThis.y + 1)
                };
                foreach (var n in neighbours)
                {
                    if (n.x < 0 || n.x >= columns) continue;
                    if (n.y < 0 || n.y >= rows) continue;
                    if (this.groupForCell[n.x, n.y] != null) continue;
                    if (tileLayer.tiles[n.x + (columns * n.y)].tile != colour) continue;
                    if (toAdd.Contains(n)) continue;
                    if (group.Contains(n)) continue;
                    toAdd.Add(n);
                }
            }
        }

        var result = new Group(chunk, colour);
        foreach (var xy in group)
            this.groupForCell[xy.x, xy.y] = result;
        return result;
    }

    public TileGrid TakeTilesIntoNewGrid(TileGrid fromGrid, Group group)
    {
        TileGrid tileGrid = null;

        for (var y = 0; y < this.rows; y += 1)
        {
            for (var x = 0; x < this.columns; x += 1)
            {
                if (this.groupForCell[x, y] != group) continue;

                var index = fromGrid.GetTileIndex(x, y).Value;
                var t = fromGrid.tiles[index];
                if (t == null) continue;

                if (tileGrid == null)
                    tileGrid = TileGrid.CreateSameSizeAs(fromGrid);

                tileGrid.containerChunk = fromGrid.containerChunk;

                tileGrid.tiles[index] = fromGrid.tiles[index];
                tileGrid.tilePatterns[index] = fromGrid.tilePatterns[index];
                fromGrid.tiles[index] = null;
                fromGrid.tilePatterns[index] = null;

                tileGrid.tiles[index].tileGrid = tileGrid;
            }
        }

        if (tileGrid != null)
        {
            group.followerTileGrid = tileGrid;
            tileGrid.group = group;
            tileGrid.ShrinkToFit(null);
        }

        return tileGrid;
    }
}
