
using UnityEngine;

public class Hitbox
{
    public struct SolidData
    {
        public SolidData(bool xMin, bool xMax, bool yMin, bool yMax)
        {
            this.solidXMin = xMin;
            this.solidXMax = xMax;
            this.solidYMin = yMin;
            this.solidYMax = yMax;
        }
        public bool solidXMin;
        public bool solidXMax;
        public bool solidYMin;
        public bool solidYMax;
    };
    public static SolidData Solid = new SolidData(true, true, true, true);
    public static SolidData NonSolid = new SolidData(false, false, false, false);
    public static SolidData SolidOnTop = new SolidData(false, false, false, true);

    public enum PositionType { Absolute, RelativeToItem };

    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public bool solidXMin;
    public bool solidXMax;
    public bool solidYMin;
    public bool solidYMax;
    public bool wallSlideAllowedOnLeft;
    public bool wallSlideAllowedOnRight;
    public PositionType positionType = PositionType.RelativeToItem;

    public float worldXMin; // set by Item.RefreshHitboxes()
    public float worldXMax;
    public float worldYMin;
    public float worldYMax;

    public abstract class CustomShape
    {
        public abstract bool IsSolidAtPoint(Vector2 relativePosition);

        // return value must be wrapped to 0° - 360° range
        public abstract float SurfaceAngleDegrees(
            Vector2 relativePosition, Vector2 incomingRayDirection
        );

        public bool IsSolidAnywhereInCell(int relativeTx, int relativeTy)
        {
            Vector2 bottomLeft = new Vector2(relativeTx, relativeTy);
            return
                IsSolidAtPoint(bottomLeft.Add(0.5f, 0.5f)) ||
                IsSolidAtPoint(bottomLeft.Add(0, 0)) ||
                IsSolidAtPoint(bottomLeft.Add(0, 0)) ||
                IsSolidAtPoint(bottomLeft.Add(0, 1)) ||
                IsSolidAtPoint(bottomLeft.Add(1, 1));
        }
    }
    public CustomShape customShape;

    public Hitbox(
        float xMin,
        float xMax,
        float yMin,
        float yMax,
        int rotation,
        SolidData solidData,
        bool wallSlideAllowedOnLeft = false,
        bool wallSlideAllowedOnRight = false,
        PositionType positionType = PositionType.RelativeToItem
    )
    {
        this.wallSlideAllowedOnLeft = wallSlideAllowedOnLeft;
        this.wallSlideAllowedOnRight = wallSlideAllowedOnRight;
        this.positionType = positionType;

        if (rotation == 0)
        {
            this.xMin = xMin;
            this.yMin = yMin;
            this.xMax = xMax;
            this.yMax = yMax;
            this.solidXMin = solidData.solidXMin;
            this.solidXMax = solidData.solidXMax;
            this.solidYMin = solidData.solidYMin;
            this.solidYMax = solidData.solidYMax;
        }
        else if (rotation == 90)
        {
            this.xMin = -yMax;
            this.yMin = +xMin;
            this.xMax = this.xMin + (yMax - yMin);
            this.yMax = this.yMin + (xMax - xMin);
            this.solidXMin = solidData.solidYMax;
            this.solidXMax = solidData.solidYMin;
            this.solidYMin = solidData.solidXMin;
            this.solidYMax = solidData.solidXMax;
        }
        else if (rotation == 180)
        {
            this.xMin = -xMax;
            this.yMin = -yMax;
            this.xMax = this.xMin + (xMax - xMin);
            this.yMax = this.yMin + (yMax - yMin);
            this.solidXMin = solidData.solidXMax;
            this.solidXMax = solidData.solidXMin;
            this.solidYMin = solidData.solidYMax;
            this.solidYMax = solidData.solidYMin;
        }
        else if (rotation == 270)
        {
            this.xMin = +yMin;
            this.yMin = -xMax;
            this.xMax = this.xMin + (yMax - yMin);
            this.yMax = this.yMin + (xMax - xMin);
            this.solidXMin = solidData.solidYMin;
            this.solidXMax = solidData.solidYMax;
            this.solidYMin = solidData.solidXMax;
            this.solidYMax = solidData.solidXMin;
        }
    }

    public Rect InWorldSpace() => new Rect(
        worldXMin,
        worldYMin,
        worldXMax - worldXMin,
        worldYMax - worldYMin
    );

    public float? DistanceToSolidPart(Vector2 relativeStart, Vector2 direction)
    {
        Debug.Assert(this.customShape != null);

        float byX1 = (this.xMin - relativeStart.x) / direction.x;
        float byX2 = (this.xMax - relativeStart.x) / direction.x;
        float byY1 = (this.yMin - relativeStart.y) / direction.y;
        float byY2 = (this.yMax - relativeStart.y) / direction.y;
        float min, max;

        if (Mathf.Abs(direction.x) < 0.001f)
        {
            min = Mathf.Min(byY1, byY2);
            max = Mathf.Max(byY1, byY2);
        }
        else if (Mathf.Abs(direction.y) < 0.001f)
        {
            min = Mathf.Min(byX1, byX2);
            max = Mathf.Max(byX1, byX2);
        }
        else
        {
            float byXMin = Mathf.Min(byX1, byX2);
            float byXMax = Mathf.Max(byX1, byX2);
            float byYMin = Mathf.Min(byY1, byY2);
            float byYMax = Mathf.Max(byY1, byY2);
            min = Mathf.Max(byXMin, byYMin);
            max = Mathf.Min(byXMax, byYMax);
        }

        if (min < 0) min = 0;
        if (max < 0) max = 0;

        for (float n = min; n < max; n += 1 / 16f)
        {
            var pos = relativeStart + (direction * n);
            if (this.customShape.IsSolidAtPoint(pos) == true)
                return n;
        }

        {
            var pos = relativeStart + (direction * max);
            if (this.customShape.IsSolidAtPoint(pos) == true)
                return max;
        }

        return null;
    }
}
