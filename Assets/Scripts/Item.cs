
using UnityEngine;
using System;

public class Item : MonoBehaviour
{
    protected Def def;
    protected Animated animated;
    protected SpriteRenderer spriteRenderer;

    [NonSerialized] public Map map;
    [NonSerialized] public Chunk chunk;
    public GroupLayer.Group group;

    public struct Def
    {
        public static readonly Def Blank = new Def(0, 0, default, null);

        public Def(Chunk startChunk)
        {
            // not sure if this is the right approach, but at least with a
            // custom constructor we can keep track of these
            this.tx = 0;
            this.ty = 0;
            this.tile = default;
            this.startChunk = startChunk;
        }

        public Def(int tx, int ty, NitromeEditor.TileLayer.Tile tile, Chunk startChunk)
        {
            this.tx = tx;
            this.ty = ty;
            this.tile = tile;
            this.startChunk = startChunk;
        }
    
        public int tx { get; } // these are in map space
        public int ty { get; }
        public NitromeEditor.TileLayer.Tile tile { get; }
        public Chunk startChunk { get; }
    }

    public enum ChunkResetBehaviour
    {
        // just leave the object as it is and call Reset().
        PersistAndCallReset,

        // completely destroy the gameobject.
        // it will be recreated from scratch (if it's an actual recorded part
        // of the level in the level file). or, if this is a temporary object
        // which should just go away forever, it'll do that.
        DestroyGameobject
    }

    public Def ItemDef { get => def; set => def = value; }

    [NonSerialized] public Hitbox[] hitboxes;
    [NonSerialized] public Vector2 position;
    [NonSerialized] public int rotation;
    [NonSerialized] public bool solidToPlayer = true;
    [NonSerialized] public bool solidToEnemy  = true;
    [NonSerialized] public ChunkResetBehaviour whenChunkResets;

    public struct Collision
    {
        public Player otherPlayer;
        public Item otherItem;
    }

    [NonSerialized] public Action<Collision> onCollisionFromAbove;
    [NonSerialized] public Action<Collision> onCollisionFromBelow;
    [NonSerialized] public Action<Collision> onCollisionFromLeft;
    [NonSerialized] public Action<Collision> onCollisionFromRight;
    [NonSerialized] public bool collisionsFollowRotation; // see Player::notifyContact

    [NonSerialized] public Action onCollisionWithPlayersHead;

    public enum GrabType { None, Floor, RightWall, Ceiling, LeftWall };
    public GrabInfo grabInfo;
    [NonSerialized] public Map.Temperature temperature;

    public struct GrabInfo
    {
        public Item item;
        public GrabType grabType;
        public Vector2? grabPositionRelativeToItem;
        public float autoReleaseDistance;

        public GrabInfo(
            Item item,
            GrabType grabType,
            Vector2? grabPositionRelativeToItem = null
        )
        {
            this.item = item;
            this.grabType = grabType;
            this.grabPositionRelativeToItem = grabPositionRelativeToItem;
            this.autoReleaseDistance = 0.5f;
        }

        public Vector2 GetGrabPositionInWorldSpace()
        {
            if (this.grabPositionRelativeToItem != null)
                return this.item.position + (Vector2)this.grabPositionRelativeToItem;
            else
                return this.item.position;
        }
    }

    public virtual void Init(Def def)
    {
        this.def = def;
        this.map = def.startChunk.map;
        this.chunk = def.startChunk;
        this.animated       = GetComponent<Animated>();
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.transform.position = new Vector3(
            (def.tx * Tile.Size) + (def.tile.offsetX / 16f),
            (def.ty * Tile.Size) + (def.tile.offsetY / 16f),
            0.0f
        );
        this.position = new Vector2(
            (def.tx * Tile.Size) + (def.tile.offsetX / 16f),
            (def.ty * Tile.Size) + (def.tile.offsetY / 16f)
        );
        this.transform.rotation = Quaternion.Euler(0, 0, def.tile.rotation);
        this.rotation = def.tile.rotation;
        this.hitboxes = new Hitbox[0];
        this.collisionsFollowRotation = true;

        if (this.spriteRenderer != null)
            this.spriteRenderer.enabled = true;

        this.grabInfo = new GrabInfo(this, GrabType.None);

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;

        Util.SetLayerRecursively(this.transform, this.map.Layer(), true);
    }

    public virtual void Advance()
    {
    }

    public virtual void Reset()
    {
    }

    public virtual void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
    }

    public virtual void NotifyNewPlayerCreatedForMultiplayer(Player replacementPlayer)
    {
    }

    public virtual void UpdateHitboxesForSpinBlock(int newRotation)
    {
    }

    public void RefreshHitboxes()
    {
        foreach (var hitbox in this.hitboxes)
        {
            if (hitbox.positionType == Hitbox.PositionType.RelativeToItem)
            {
                hitbox.worldXMin = this.position.x + hitbox.xMin;
                hitbox.worldXMax = this.position.x + hitbox.xMax;
                hitbox.worldYMin = this.position.y + hitbox.yMin;
                hitbox.worldYMax = this.position.y + hitbox.yMax;
            }
            else
            {
                hitbox.worldXMin = hitbox.xMin;
                hitbox.worldXMax = hitbox.xMax;
                hitbox.worldYMin = hitbox.yMin;
                hitbox.worldYMax = hitbox.yMax;
            }
        }
    }

    public void Invoke(Action action, float time)
    {
        System.Collections.IEnumerator Impl()
        {
            yield return new WaitForSeconds(time);
            action();
        }
        StartCoroutine(Impl());
    }

    public void SwitchToNearestChunk()
    {
        var nearest = this.map.NearestChunkTo(this.position);
        if (nearest != this.chunk)
        {
            this.chunk.items.Remove(this);
            nearest.items.Add(this);
            this.chunk = nearest;
            this.group = null;
            this.transform.SetParent(this.chunk.transform);
        }
    }

    public void AttachToGroup(GroupLayer.Group group)
    {
        this.group = group;

        if (this.group != null)
            this.transform.SetParent(group.container.transform);
        else
            this.transform.SetParent(this.chunk.transform);
    }
}
