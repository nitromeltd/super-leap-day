
using UnityEngine;
using System.Linq;

public class ABBlock : Item
{
    public enum Colour
    {
        RED,
        BLUE
    }

    public Colour colour;

    public Animated.Animation solidAnimation;
    public Animated.Animation nonsolidAnimation;
    public Animated.Animation appearAnimation;
    public Animated.Animation disappearAnimation;

    private bool isSolid;

    public override void Init(Def def)
    {
        base.Init(def);

        if (this.colour != this.chunk.abBlockColour)
        {
            this.animated.PlayOnce(this.nonsolidAnimation);
            this.hitboxes = new Hitbox[0];
        }
        else
        {
            this.animated.PlayOnce(this.solidAnimation);
            this.hitboxes = new [] {
                new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
            };
        }
    }

    public override void Advance()
    {
        if (this.isSolid == true && this.colour != this.chunk.abBlockColour)
        {
            ChangetoNonsolid();
        }

        if (this.isSolid == false && this.colour == this.chunk.abBlockColour)
        {
            ChangetoSolid();
        }
    }

    private void ChangetoSolid()
    {
        this.isSolid = true;

        this.animated.PlayOnce(this.appearAnimation, () =>
        {
            this.animated.PlayOnce(this.solidAnimation);
            
            this.hitboxes = new [] {
                new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
            };

            this.map.DamageEnemy(
                new Rect(this.position.x - 0.5f, this.position.y - 0.5f, 1f, 1f),
                new Enemy.KillInfo { itemDeliveringKill = this }
            );
        });
    }

    private void ChangetoNonsolid()
    {
        this.isSolid = false;

        this.animated.PlayOnce(this.disappearAnimation, () =>
        {
            this.animated.PlayAndLoop(this.nonsolidAnimation);
            
            this.hitboxes = new Hitbox[0];
        });
    }
}
