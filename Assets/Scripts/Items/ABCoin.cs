using UnityEngine;

public class ABCoin : Pickup
{
    public ABBlock.Colour colour;
    public bool isSilver;

    private int collectSparkleTime;
    private float sparkleOffset;
    private int pathFrameCount;

    public Animated.Animation solidAnimation;
    public Animated.Animation nonsolidAnimation;
    public Animated.Animation appearAnimation;
    public Animated.Animation disappearAnimation;

    [HideInInspector] public bool isSolid;

    public override void Init(Def def)
    {
        base.Init(def);

        this.sparkleOffset = this.isSilver ? 0.75f : 1.25f;
        this.hitboxSize = this.isSilver ? 0.40f : 0.80f;

        this.hitboxes = new [] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };

        if (this.colour != this.chunk.abBlockColour)
        {
            ChangetoNonsolid();
        }
        else
        {
            ChangetoSolid();
        }

        this.pathFrameCount = 0;
    }

    override public void Advance()
    {
        if (this.isSolid)
        {
            this.pathFrameCount++;
            base.Advance();
        }
        else
        {
            this.suckActive = false;
            this.movingFreely = false;
        }

        // sparkle particles
        if (this.collected == false && this.isSolid)
        {
            ApplyDragOnFloor();

            bool createSparkleParticle = this.isSilver ?
                this.map.frameNumber % 6 == 0 :
                this.map.frameNumber % 4 == 0;

            if (createSparkleParticle == true)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-this.sparkleOffset, this.sparkleOffset),
                        Random.Range(-this.sparkleOffset, this.sparkleOffset)
                    ),
                    this.transform.parent
                );
                sparkle.spriteRenderer.sortingLayerName = "Items";
                sparkle.spriteRenderer.sortingOrder = -1;
                if(this.colour == ABBlock.Colour.BLUE)
                {
                    sparkle.spriteRenderer.color = UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f ? new Color(0.41f, 0.47f, 1, 1) : new Color(1, 1, 1, 1);
                }
                else
                {
                    sparkle.spriteRenderer.color = UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f ? new Color(1, 0.41f, 0.41f, 1) : new Color(1, 1, 1, 1);
                }
            }
            else if (this.collectSparkleTime > 0)
            {
                this.collectSparkleTime -= 1;

                if (Random.Range(0, 10) > 2)
                {
                    var sparkle = Particle.CreateAndPlayOnce(
                        Assets.instance.coinSparkleAnimation,
                        this.map.player.position.Add(
                            Random.Range(-this.sparkleOffset, this.sparkleOffset),
                            Random.Range(-this.sparkleOffset, this.sparkleOffset)
                        ),
                        this.map.player.transform.parent
                    );
                    sparkle.spriteRenderer.sortingLayerName = "Items";
                    sparkle.spriteRenderer.sortingOrder = -1;
                }
            }
        }

        if (this.isSolid == true && this.colour != this.chunk.abBlockColour && this.collected == false)
        {
            ChangetoNonsolid();
        }

        if (this.isSolid == false && this.colour == this.chunk.abBlockColour && this.collected == false)
        {
            ChangetoSolid();
        }

        if (this.collected == true && this.collectSparkleTime > 0)
        {
            this.velocity.y = Util.Slide(this.velocity.y, 0.075f, 0.008f);
            this.position.y += this.velocity.y;
            this.transform.position = new Vector3(
                this.position.x, this.position.y, this.z
            );
        }
    }

    private void ChangetoSolid()
    {
        this.isSolid = true;

        this.animated.PlayOnce(this.appearAnimation, () =>
        {
            this.animated.PlayAndLoop(this.solidAnimation);
        });
    }

    private void ChangetoNonsolid()
    {
        this.isSolid = false;

        this.animated.PlayOnce(this.disappearAnimation, () =>
        {
            this.animated.PlayAndLoop(this.nonsolidAnimation);
        });
    }

    protected void MoveHorizontally(Raycast raycast)
    {
        if (!this.isSolid) return;
        var rect = this.hitboxes[0];
        var high = this.position.Add(0, rect.yMax - 1);
        var mid = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
        var low = this.position.Add(0, rect.yMin + 1);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid, true, maxDistance),
                raycast.Horizontal(low, true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid, false, maxDistance),
                raycast.Horizontal(low, false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }

    protected void MoveVertically(Raycast raycast)
    {
        if (!this.isSolid) return;
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(0, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0f), false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                float conveyorSpeed = 0;
                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                        conveyorSpeed = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed; //0.5
                }

                this.position.x += conveyorSpeed;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public override void Collect()
    {
        if (this.collected == true) return;

        base.Collect();

        if (this.isSilver == true)
        {
            this.map.AddSilverCoin();
        }
        else
        {
            this.map.coinsCollected += 1;
        }

        this.collectSparkleTime = 120;
        Audio.instance.PlaySfx(Assets.instance.sfxCoin, position: this.position, options: new Audio.Options(0.9f, true, 0.1f));
    }

    public override void CheckCollect()
    {

        if (this.disallowCollectTime > 0)
        {
            this.disallowCollectTime -= 1;
        }
        else if (this.collected == false && this.canPlayerCollect == true && this.isSolid)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = this.map.player.Rectangle();

            if (thisRect.Overlaps(playerRect))
                Collect();
        }
    }

    public override void PathMove()
    {
        this.position = this.path.PointAtTime(
                (this.pathFrameCount / 60.0f) + this.offsetThroughPath
            ).position;
    }
}

