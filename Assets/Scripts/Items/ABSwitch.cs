﻿using UnityEngine;
using System.Linq;

public class ABSwitch : Item
{
    public Animated.Animation idleRedAnimation;
    public Animated.Animation redToBlueAnimation;
    public Animated.Animation idleBlueAnimation;
    public Animated.Animation blueToRedAnimation;

    private ABBlock.Colour colour;
    private bool pressed;
    private int pressDisableTime;
    private bool isPlayerOverlapping;
    private bool engage;
    
    public override void Init(Def def)
    {
        base.Init(def);

        SetHitbox();

        this.colour = this.chunk.abBlockColour;
        this.pressed = false;
        this.engage = true;

        this.animated.PlayOnce(this.colour == ABBlock.Colour.RED ?
            this.idleRedAnimation : this.idleBlueAnimation);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void SetHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, 0f, 1f, this.rotation, Hitbox.NonSolid),
            // new Hitbox(-0.5f, 0.5f, 0f, 1f, this.rotation, Hitbox.NonSolid),
            new Hitbox(-1.2f, 1.2f, -0.5f, 1.0f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
            
        // Check for changing colour after another ABSwitch has been changed
        if(this.colour != this.chunk.abBlockColour)
        {
            ChangeColour(this.chunk.abBlockColour);
            return;
        }

        bool pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if (pressedLastFrame == false && this.pressed == true)
        {
            ChangeColour(this.colour == ABBlock.Colour.RED ?
                ABBlock.Colour.BLUE : ABBlock.Colour.RED);

            AudioClip sfxSwitch = engage == true?
                Assets.instance.sfxSwitchEngage : Assets.instance.sfxSwitchDisengage;
            Audio.instance.PlaySfx(sfxSwitch, position: this.position);
        }

        if (this.pressed == false && pressedLastFrame == true)
            this.pressDisableTime = 20;
        else if (this.pressDisableTime > 0)
            this.pressDisableTime -= 1;

    }

    private bool IsBeingPressed()
    {
        if (this.pressDisableTime > 0) return false;

        // when the player overlaps the hitbox, we start using a slightly
        // larger one, so that the player needs to go slightly further away
        // before the switch can be retriggered.
        // however, we DON'T do this for nonplayer things, because we
        // don't want to break any mechanisms that were built before
        // this change where a spikey is walking past a switch or something.

        var thisRect = this.hitboxes[0].InWorldSpace();
        var thisRectForPlayer =
            this.hitboxes[this.isPlayerOverlapping ? 1 : 0].InWorldSpace();

        if (this.map.player.Rectangle().Overlaps(thisRectForPlayer))
        {
            this.isPlayerOverlapping = true;
            return true;
        }
        else
        {
            this.isPlayerOverlapping = false;
        }

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
                return true;
        }

        (Vector2 outward, Vector2 across) Directions()
        {
            switch (this.rotation)
            {
                case 90:   return (Vector2.right, Vector2.up);
                case 180:  return (Vector2.up,    Vector2.left);
                case 270:  return (Vector2.left,  Vector2.down);
                default:   return (Vector2.down,  Vector2.right);
            }
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        var (outward, across) = Directions();
        Vector2[] sensors = new []
        {
            this.position + (outward * 0.25f) + (across * 0.5f),
            this.position + (outward * 0.25f),
            this.position + (outward * 0.25f) + (across * -0.5f)
        };

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }

    private void ChangeColour(ABBlock.Colour targetColour)
    {
        this.colour = this.chunk.abBlockColour = targetColour;

        Animated.Animation transitionAnimation = targetColour == ABBlock.Colour.RED ?
            this.blueToRedAnimation : this.redToBlueAnimation;

        Animated.Animation onCompleteAnimation = targetColour == ABBlock.Colour.RED ?
            this.idleRedAnimation : this.idleBlueAnimation;

        this.animated.PlayOnce(transitionAnimation, () =>
        {
            this.animated.PlayOnce(onCompleteAnimation);
        });
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox();
    }
}
