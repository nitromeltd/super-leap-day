using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AirCurrent : Item
{
    public enum Direction
    {
        Left, Right, Up, Down
    }
    
    public enum DirectionsType
    {
        None,
        Corner,
        Straight
    }

    public DirectionsType directionsType;

    [Header("Trails")]
    public GameObject trailPrefab;
    public AnimationCurve trailCurve;
    public GameObject windFunnelPrefab;
    
    public Direction[] connections;
    public AirCurrent currentNextToEnter = null;
    public AirCurrent currentNextToExit = null;
    [HideInInspector] public PlayerBlocker playerBlocker;

    public List<Dandelion> dandelions = new List<Dandelion>();
    public (AirCurrent[] currents, bool loop) route = (null, false);

    public AirCurrent Leader => this.route.currents[0];
    public bool IsLeader => (Leader == this);
    public int RouteLength => this.route.currents.Length;

    public class TrackingData
    {
        public Player player;
        public Dandelion dandelion;
        public float distanceAlongPath;

        public Vector2 Position
        {
            get {
                if (player != null) return player.position;
                if (dandelion != null) return dandelion.position;
                return player.position;
            }
            set {
                if (player != null) player.position = value;
                if (dandelion != null) dandelion.position = value;
            }
        }

        public Vector2 Velocity
        {
            get {
                if (player != null) return player.velocity;
                if (dandelion != null) return dandelion.velocity;
                return player.velocity;
            }
            set {
                if (player != null) player.velocity = value;
                if (dandelion != null) dandelion.velocity = value;
            }
        }

        public SpriteRenderer SpriteRenderer
        {
            get {
                if (player != null)
                    return player.GetComponent<SpriteRenderer>();
                else if(dandelion != null)
                    return dandelion.GetComponent<SpriteRenderer>();
                else
                    return player.GetComponent<SpriteRenderer>();
            }
        }

        public int LastFrameInsideAirCurrent
        {
            get {
                if (player != null) return player.lastFrameInsideAirCurrent;
                return 0;
            }
            set {
                if (player != null) player.lastFrameInsideAirCurrent = value;
            }
        }

        public int LastFrameEnteringAirCurrent
        {
            get {
                if (player != null) return player.lastFrameEnteringAirCurrent;
                return 0;
            }
            set {
                if (player != null) player.lastFrameEnteringAirCurrent = value;
            }
        }
    }
    private TrackingData trackingPlayer = null;

    private static readonly float PlayerMoveSpeed = 0.20f;
    private static readonly float DandelionMoveSpeed = 0.20f;
    private static Vector2 JumpForce = new Vector2(0.15f, 0.52f);
    private static float ExitSpeed = 0.1f;
    private static readonly int DisablePlayerReentryTime = 10;

    private bool IsCorner =>
        this.directionsType == DirectionsType.Corner;

    private Vector2 Center => this.position;

    private Direction EnterDirection => this.connections[0];
    private Direction ExitDirection => this.connections[1];
    public Rect EntryRect => this.hitboxes[0].InWorldSpace().Inflate(-0.50f);

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index - 2 && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.trackingPlayer = new TrackingData();
        this.trackingPlayer.player = this.map.player;

        switch(this.directionsType)
        {
            case DirectionsType.Straight:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, true, true)
                };
                if (def.tile.rotation == 0)
                    this.connections = new [] { Direction.Down, Direction.Up };
                else if (def.tile.rotation == 90)
                    this.connections = new [] { Direction.Right, Direction.Left };
                else if (def.tile.rotation == 180)
                    this.connections = new [] { Direction.Up, Direction.Down };
                else if (def.tile.rotation == 270)
                    this.connections = new [] { Direction.Left, Direction.Right };
            }
            break;

            case DirectionsType.Corner:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -1f, 1f, this.rotation, Hitbox.NonSolid, true, true)
                };

                this.rotation = def.tile.rotation;
                if (def.tile.flip == true)
                {
                    if (this.rotation == 0)
                        this.connections = new [] { Direction.Left, Direction.Up };
                    else if (this.rotation == 90)
                        this.connections = new [] { Direction.Down, Direction.Left };
                    else if (this.rotation == 180)
                        this.connections = new [] { Direction.Right, Direction.Down };
                    else if (this.rotation == 270)
                        this.connections = new [] { Direction.Up, Direction.Right };
                }
                else
                {
                    if (this.rotation == 0)
                        this.connections = new [] { Direction.Right, Direction.Up };
                    else if (this.rotation == 90)
                        this.connections = new [] { Direction.Up, Direction.Left };
                    else if (this.rotation == 180)
                        this.connections = new [] { Direction.Left, Direction.Down };
                    else if (this.rotation == 270)
                        this.connections = new [] { Direction.Down, Direction.Right };
                }
            }
            break;
        }

        float length = IsCorner ? 1.50f : 1.35f;
        
        Vector2 nextToEnterPosition =
            this.position + (DirectionToVector2(EnterDirection) * length);
        this.currentNextToEnter = this.chunk.NearestItemTo<AirCurrent>(
            nextToEnterPosition, 1f
        );
        if(this.currentNextToEnter == this)
        {
            this.currentNextToEnter = null;
        }

        Vector2 nextToExitPosition =
            this.position + (DirectionToVector2(ExitDirection) * length);
        this.currentNextToExit = this.chunk.NearestItemTo<AirCurrent>(
            nextToExitPosition, 1f
        );
        if(this.currentNextToExit == this)
        {
            this.currentNextToExit = null;
        }
        
        this.playerBlocker = this.chunk.NearestItemTo<PlayerBlocker>(
            this.position, 1f
        );

        this.dandelions = this.chunk.items.OfType<Dandelion>().ToList();

        this.spriteRenderer.enabled = false;
    }

    public override void NotifyNewPlayerCreatedForMultiplayer(Player replacementPlayer)
    {
        this.trackingPlayer.player = replacementPlayer;
    }

    public void NotifyOfNewDandelion(Dandelion dandelion)
    {
        this.dandelions.Add(dandelion);

        List<Dandelion> currentDandelions = new List<Dandelion>(this.dandelions);

        foreach(var d in currentDandelions)
        {
            if(d == null || d.IsDead == true)
            {
                this.dandelions.Remove(d);
            }
        }
    }

    private void CalculateRoute()
    {
        var currents = new List<AirCurrent> { this };
        var next = this.currentNextToExit;

        while (next != null && currents.Contains(next) == false)
        {
            currents.Add(next);
            next = next.currentNextToExit;
        }

        var prev = this.currentNextToEnter;

        while (prev != null && currents.Contains(prev) == false)
        {
            currents.Insert(0, prev);
            prev = prev.currentNextToEnter;
        }

        var loop = (next != null && currents.Contains(next) == true);
        var route = (currents: currents.ToArray(), loop: loop);

        foreach (var c in currents)
        {
            c.route = route;
        }
    }

    public Vector2 DirectionToVector2(Direction direction, float magnitude = 1)
    {
        switch (direction)
        {
            case Direction.Left:  return magnitude * Vector2.left;
            case Direction.Right: return magnitude * Vector2.right;
            case Direction.Down:  return magnitude * Vector2.down;
            case Direction.Up:    return magnitude * Vector2.up;
        }
        return default;
    }

    public Direction Vector2ToDirection(Vector2 vector)
    {
        if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
            return vector.x > 0 ? Direction.Right : Direction.Left;
        else
            return vector.y > 0 ? Direction.Up : Direction.Down;
    }

    public Direction Invert(Direction direction)
    {
        switch (direction)
        {
            case Direction.Left:  return Direction.Right;
            case Direction.Right: return Direction.Left;
            case Direction.Down:  return Direction.Up;
            case Direction.Up:    return Direction.Down;
        }
        return default;
    }

    private bool IsThisCurrentExit()
    {
        if(this.currentNextToExit != null)
        {
            return Invert(this.currentNextToExit.EnterDirection) !=
                ExitDirection;
        }
        else
        {
            return true;
        }
    }

    public float GetThingMoveSpeed(TrackingData thing)
    {
        if(thing.dandelion != null)
        {
            return DandelionMoveSpeed;
        }

        return PlayerMoveSpeed;
    }

    public override void Advance()
    {
        if (this.route.currents == null)
            CalculateRoute();

        foreach (var e in dandelions)
            TestForDandelionEntry(e);

        if(ShouldWaitForPlayer() == false)
        {
            TestForPlayerEntry();
            ManageFunnelVisuals();
        }
    }

    private bool IsPlayerStateValidForEntry()
    {
        var player = this.map.player;
        
        if(player.state == Player.State.Normal) return true;
        if(player.state == Player.State.WallSlide) return true;
        if(player.state == Player.State.Jump) return true;
        if(player.state == Player.State.Spring) return true;
        if(player.state == Player.State.SpringFromPushPlatform) return true;
        if(player.state == Player.State.DropVertically) return true;
        if(player.state == Player.State.UsingParachuteOpened) return true;
        if(player.state == Player.State.UsingParachuteClosed) return true;
        if(player.state == Player.State.BoostFlyingRing && player.velocity.y < 0f) return true;
        if(player is PlayerGoop && player.state == Player.State.GoopBallSpinning) return true;

        return false;
    }

    private void TestForPlayerEntry()
    {
        var player = this.map.player;
        var data = this.trackingPlayer;

        if (this.map.frameNumber < data.LastFrameInsideAirCurrent + DisablePlayerReentryTime)
            return;
        if (IsPlayerStateValidForEntry() == false)
            return;
        if(player.grabbingOntoItem != null)
            return;
        if(player.alive == false)
            return;

        if (player.Rectangle().Overlaps(EntryRect))
        {
            player.EnterAirCurrent(this);
            player.velocity = DirectionToVector2(ExitDirection, PlayerMoveSpeed);
            data.LastFrameEnteringAirCurrent = this.map.frameNumber;
            data.LastFrameInsideAirCurrent = this.map.frameNumber;
        }
    }

    private void TestForDandelionEntry(Dandelion dandelion)
    {
        if (dandelion.insideAirCurrent != null)
            return;

        var data = dandelion.airCurrentTrackingData;
        if (data == null)
            data = dandelion.airCurrentTrackingData = new TrackingData { dandelion = dandelion };
        // if (this.map.frameNumber < DisableCurrentEntryUntilFrameNumber)
            // return;

        if (dandelion.hitboxes[0].InWorldSpace().Overlaps(EntryRect))
        {
            dandelion.EnterAirCurrent(this);
            dandelion.velocity = DirectionToVector2(ExitDirection, DandelionMoveSpeed);
            // TimeInsideCurrent = 0;
        }
    }
    
    public void AdvancePlayer() =>
        AdvanceThing(this.trackingPlayer);

    public void AdvanceDandelion(Dandelion dandelion) =>
        AdvanceThing(dandelion.airCurrentTrackingData);

    private void AdvanceThing(TrackingData thing)
    {
        thing.LastFrameInsideAirCurrent = this.map.frameNumber;
        
        Direction forward = Vector2ToDirection(thing.Velocity);
        Direction backward = Invert(forward);

        var forwardVector = DirectionToVector2(forward);
        var forwardAmount = Vector2.Dot(thing.Position - Center, forwardVector);
        forwardAmount += GetThingMoveSpeed(thing);

        if (forwardAmount > 0 && this.connections.Contains(forward) == false)
        {
            forward = this.connections.Where(c => c != backward).FirstOrDefault();
            forwardVector = DirectionToVector2(forward);
        }

        int timeInsideCurrent = this.map.frameNumber - thing.LastFrameEnteringAirCurrent;

        Vector2 targetPosition = Center + (forwardVector * forwardAmount);
        Vector2 currentPosition = thing.Position;
        currentPosition.x = timeInsideCurrent < 5 ?
            Util.Slide(currentPosition.x, targetPosition.x, 0.15f) : targetPosition.x;
        currentPosition.y = timeInsideCurrent < 5 ?
            Util.Slide(currentPosition.y, targetPosition.y, 0.15f) : targetPosition.y;

        thing.Position = currentPosition;
        thing.Velocity = forwardVector * GetThingMoveSpeed(thing);

        float GetCurrentForward()
        {
            if(IsThisCurrentExit() && ExitDirection == forward)
            {
                return 1f;
            }

            if(IsCorner == true)
            {
                return 1f;
            }

            return 0.50f;
        }

        if (forwardAmount > GetCurrentForward())
        {
            if (IsThisCurrentExit() && ExitDirection == forward)
            {
                ThingExitAirCurrent(thing, forwardVector);
            }
            else
            {
                ThingTransitionToNextPipePiece(thing, forwardVector);
            }
        }
        
        if (thing.player != null)
        {
            this.map.player.transform.position = this.map.player.position;
        }
        
        if (thing.dandelion == true)
        {
            thing.dandelion.transform.position = thing.dandelion.position;
        }
    }

    private void ThingTransitionToNextPipePiece(
        TrackingData thing, Vector2 forwardVector
    )
    {
        var nextCurrentPiece = this.chunk.NearestItemTo<AirCurrent>(
            thing.Position, 2
        );
        
        if(nextCurrentPiece != null &&
            thing.player != null &&
            nextCurrentPiece.playerBlocker != null)
        {
            ThingExitAirCurrent(thing, -forwardVector);
            return;
        }

        if(nextCurrentPiece != null)
        {
            nextCurrentPiece.ThingEnterAirCurrent(thing);
            
            if (thing.player != null)
            {
                this.map.player.EnterAirCurrent(nextCurrentPiece);
            }
            
            if (thing.dandelion != null)
            {
                thing.dandelion.EnterAirCurrent(nextCurrentPiece);
            }
        }
    }

    private void ThingEnterAirCurrent(TrackingData thing)
    {
        if(thing.player != null)
        {
            Player player = this.map.player;

            if(player.facingRight == true && ExitDirection == Direction.Left)
            {
                player.facingRight = false;
            }

            if(player.facingRight == false && ExitDirection == Direction.Right)
            {
                player.facingRight = true;
            }
        }
        else if(thing.dandelion != null)
        {
            Dandelion dandelion = thing.dandelion;

            if(dandelion.facingRight == true && ExitDirection == Direction.Left)
            {
                dandelion.facingRight = false;
            }

            if(dandelion.facingRight == false && ExitDirection == Direction.Right)
            {
                dandelion.facingRight = true;
            }
        }
    }

    public void ThingExitAirCurrent(TrackingData thing, Vector2 forwardVector)
    {
        thing.Velocity = GetExitVelocity(thing, forwardVector);
        thing.Position = Center + (forwardVector * 1.5f);

        if(thing.player != null)
        {
            var player = this.map.player;
            player.NotifyExitAirCurrent();
        }
        else if(thing.dandelion != null)
        {
            thing.dandelion.NotifyExitAirCurrent();
        }
    }

    private Vector2 GetExitVelocity(TrackingData thing, Vector2 forwardVector)
    {
        Vector2 exitVelocity = thing.Velocity;

        if(thing.player != null)
        {
            if (forwardVector.y > 0)
            {
                return forwardVector * ExitSpeed * 3f;
            }
            else
            {
                return forwardVector * ExitSpeed;
            }
        }
        else if(thing.dandelion != null)
        {
            if (forwardVector.y > 0)
            {
                return forwardVector * ExitSpeed * 3f;
            }
            else
            {
                return forwardVector * ExitSpeed;
            }
        }

        return exitVelocity;
    }

    public void PlayerJumped()
    {
        Player player = this.map.player;

        Vector2 playerVelocity = new Vector2(JumpForce.x * (player.facingRight ? 1f : -1f), JumpForce.y);
        Direction direction = ExitDirection;

        if(CanPlayerJump(playerVelocity))
        {
            ThingExitAirCurrent(this.trackingPlayer, Vector2.up);
            player.velocity = playerVelocity;
        }
    }

    private bool CanPlayerJump(Vector2 playerVelocity)
    {
        Vector2 jumpDirection = playerVelocity.normalized;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var r = raycast.Arbitrary(this.position, jumpDirection, 1.50f);

        return r.anything == false;
    }

    /* WIND FUNNELS */
    [System.NonSerialized] public WindFunnel[] windFunnels = null;
    
    [System.Serializable] public class WindFunnel
    {
        public GameObject go;
        public AirCurrent airCurrentLeader;
        public float throughSequence;
        public SpriteRenderer spriteRenderer;

        public WindFunnel(GameObject go, AirCurrent airCurrent, int index, int count)
        {
            this.go = go;
            this.airCurrentLeader = airCurrent;
            this.throughSequence = (float)index / count;
            this.spriteRenderer = this.go.GetComponent<SpriteRenderer>();
            this.spriteRenderer.gameObject.layer = this.airCurrentLeader.map.Layer();
        }

        public void Advance()
        {
            var distance =
                (this.airCurrentLeader.map.frameNumber * 0.07f) +
                (this.throughSequence * this.airCurrentLeader.RouteLength);
            distance %= this.airCurrentLeader.RouteLength;
            var index = Mathf.FloorToInt(distance);
            var through = distance - index;

            var current = this.airCurrentLeader.route.currents[index];
            var entryVector = current.DirectionToVector2(current.EnterDirection);
            var exitVector = current.DirectionToVector2(current.ExitDirection);
            var offsetAmount =
                (current.directionsType == DirectionsType.Corner) ? 1 : 0.5f;
            var from = current.position + (entryVector * offsetAmount);
            var to   = current.position + (exitVector * offsetAmount);
            this.go.transform.position = Vector2.Lerp(from, to, through);

            var fromAngle = Mathf.Atan2(-entryVector.y, -entryVector.x) * 180 / Mathf.PI;
            var toAngle   = Mathf.Atan2(exitVector.y, exitVector.x) * 180 / Mathf.PI;
            if (fromAngle < toAngle - 180) fromAngle += 360;
            if (fromAngle > toAngle + 180) fromAngle -= 360;
            
            var angleDegrees = Mathf.Lerp(fromAngle, toAngle, through);
            this.go.transform.rotation = Quaternion.Euler(0, 0, angleDegrees);

            int frameNumber = this.airCurrentLeader.map.frameNumber;
            float theta = (frameNumber * -0.2f) + (distance * 1f);
            float scale = 0.6f + (0.15f * Mathf.Sin(theta));
            this.go.transform.localScale = new Vector3(scale, scale, 1);

            if (current.currentNextToEnter == null && through < 0.4f)
            {
                float alpha = through / 0.4f;
                this.spriteRenderer.color = new Color(1, 1, 1, alpha);
            }
            else if (current.currentNextToExit == null && through > 0.6f)
            {
                float alpha = (1.0f - through) / 0.4f;
                this.spriteRenderer.color = new Color(1, 1, 1, alpha);
            }
            else
            {
                this.spriteRenderer.color = Color.white;
            }
        }
    }

    private void ManageFunnelVisuals()
    {
        if (this.IsLeader == false) return;
        if (this.route.currents?.Length < 1) return;

        if (this.windFunnels == null)
        {
            var count = Mathf.RoundToInt(this.route.currents.Length * 1.6f);

            this.windFunnels = new WindFunnel[count];

            for (int n = 0; n < count; n += 1)
            {
                GameObject go = Instantiate(
                    this.windFunnelPrefab,
                    this.position,
                    Quaternion.identity,
                    this.transform
                );
                go.name = "Wind Funnel from Air Current";
                this.windFunnels[n] = new WindFunnel(go, this, n, count);
            }
        }

        for (int i = this.windFunnels.Length - 1; i >= 0 ; i--)
        {
            this.windFunnels[i].Advance();
        }
    }
}
