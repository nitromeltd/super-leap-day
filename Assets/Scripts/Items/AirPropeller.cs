using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class AirPropeller : Item
{
    [Header("Trails")]
    public GameObject trailPrefab;
    public AnimationCurve trailCurve;

    private AirPropeller propellerNextToEnter = null;
    private AirPropeller propellerNextToExit = null;
    private List<AirPropeller> airPropellerArea = new List<AirPropeller>();
    private bool isPartOfAirPropellerArea = false;
    public List<Dandelion> dandelions = new List<Dandelion>();

    public bool IsAreaLeader => this.airPropellerArea.Count > 0;

    private static Vector2 HitboxPos = new Vector2(0f, 0f);
    private static Vector2 HitboxSize = new Vector2(0.50f, 0.50f);

    public static Vector2[] Directions = new Vector2[]
    {
        Vector2.up,
        Vector2.left,
        Vector2.down,
        Vector2.right,
    };

    private Vector2 CurrentDirection =>
        this.rotation switch
        {
            0   => Directions[0],
            90  => Directions[1],
            180 => Directions[2],
            270 => Directions[3],
            _   => Directions[0]
        };

    private Vector2 EnterDirection => -CurrentDirection;
    private Vector2 ExitDirection => CurrentDirection;
        
    private Rect HitboxRect => this.hitboxes[0].InWorldSpace();

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new Hitbox[]
        {
            new Hitbox(
                HitboxPos.x - HitboxSize.x,
                HitboxPos.x + HitboxSize.x,
                HitboxPos.y - HitboxSize.y,
                HitboxPos.y + HitboxSize.y,
                0, Hitbox.NonSolid,
                wallSlideAllowedOnLeft: true,
                wallSlideAllowedOnRight: true)
        };
        
        float length = 1f;
        
        Vector2 nextToEnterPosition =
            this.position + (EnterDirection * length);
        this.propellerNextToEnter = this.chunk.NearestItemTo<AirPropeller>(
            nextToEnterPosition, 1f
        );
        if(this.propellerNextToEnter == this)
        {
            this.propellerNextToEnter = null;
        }

        Vector2 nextToExitPosition =
            this.position + (ExitDirection * length);
        this.propellerNextToExit = this.chunk.NearestItemTo<AirPropeller>(
            nextToExitPosition, 1f
        );
        if(this.propellerNextToExit == this)
        {
            this.propellerNextToExit = null;
        }

        this.dandelions = this.chunk.items.OfType<Dandelion>().ToList();

        this.spriteRenderer.enabled = false;
    }

    public void NotifyOfNewDandelion(Dandelion dandelion)
    {
        this.dandelions.Add(dandelion);

        List<Dandelion> currentDandelions = new List<Dandelion>(this.dandelions);

        foreach(var d in currentDandelions)
        {
            if(d == null || d.IsDead == true)
            {
                this.dandelions.Remove(d);
            }
        }
    }

    private void CheckAirPropellerArea(AirPropeller airPropeller, bool useEnter)
    {
        void End()
        {
            if(useEnter == true && airPropeller != this)
            {
                airPropeller.airPropellerArea.AddRange(this.airPropellerArea);
                this.airPropellerArea.Clear();
            }
        }

        if(airPropeller.isPartOfAirPropellerArea == true)
        {
            End();
            return;
        }

        AddToAirPropellerArea(airPropeller);
        airPropeller.isPartOfAirPropellerArea = true;

        AirPropeller nextAirPropeller = useEnter ?
            airPropeller.propellerNextToEnter : airPropeller.propellerNextToExit;

        if(nextAirPropeller == this)
        {
            this.isPartOfAirPropellerArea = true;
        }

        if(nextAirPropeller != null && nextAirPropeller.CurrentDirection == CurrentDirection)
        {
            CheckAirPropellerArea(nextAirPropeller, useEnter);
        }
        else
        {
            End();
        }
    }

    private void AddToAirPropellerArea(AirPropeller propeller)
    {
        if(this.airPropellerArea.Contains(propeller) == true) return;

        this.airPropellerArea.Add(propeller);
    }
    
    public override void Advance()
    {
        if(this.isPartOfAirPropellerArea == false)
        {
            bool useEnter = EnterDirection == Vector2.up || EnterDirection == Vector2.right;
            CheckAirPropellerArea(this, useEnter);
        }

        CheckForPlayer();

        foreach (var dandelion in this.dandelions)
        {
            Rect dandelionRect = dandelion.hitboxes[0].InWorldSpace();

            if(dandelionRect.Overlaps(HitboxRect))
            {
                dandelion.PushFromPropeller(CurrentDirection);
            }
        }
        
        ManageTrails();
    }

    private void CheckForPlayer()
    {
        Player player = Map.instance.player;
        Rect playerRect = player.Rectangle();

        if(playerRect.Overlaps(HitboxRect) &&
            player.ShouldCollideWithItems() &&
            CanInteractWithPlayer())
        {
            PushPlayer();
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxWindyAirPropellerBlowing))
            {
                Audio.instance.PlaySfx(Assets.instance.sfxWindyAirPropellerBlowing, position: this.position, options: new Audio.Options(1,false,0));
                Audio.instance.ChangeSfxVolume(Assets.instance.sfxWindyAirPropellerBlowing, 0, duration: .5f);
            }
        }
    }

    private bool CanInteractWithPlayer()
    {
        Player player = Map.instance.player;

        if(player.state == Player.State.Spring) return false;
        if(player.state == Player.State.SpringFromPushPlatform) return false;

        return true;
    }

    private void PushPlayer()
    {
        Map.instance.player.PushFromPropeller(CurrentDirection);
    }

    [HideInInspector] public float trailAmplitude = 0.45f;
    [HideInInspector] public float trailPeriod = 3.50f;
    [HideInInspector] public float trailSpeed = 0.25f;

    private List<WindTrail> activeTrails = new List<WindTrail>();
    private List<WindTrail> trailPool = new List<WindTrail>();
    private const int PoolSize = 3;

    [System.Serializable] private class WindTrail
    {
        public GameObject go;
        private int timer;
        private int resetTimer;
        private AirPropeller airPropeller;
        private Vector3 position;
        private Vector3 direction;
        private Vector3 currentVelocity;
        private TrailRenderer trailRenderer;

        private float amplitude;
        private float period;
        private float speed;

        private const int ResetTime = 24;

        public WindTrail(GameObject go, AirPropeller airPropeller)
        {
            this.go = go;
            this.timer = 0;
            this.airPropeller = airPropeller;
            this.trailRenderer = this.go.GetComponent<TrailRenderer>();

            Randomize();
        }

        public void Randomize()
        {
            Vector2 pos = this.airPropeller.position + (Random.insideUnitCircle * 0.50f);
            this.go.transform.position = this.position = pos;

            this.amplitude = Random.Range(airPropeller.trailAmplitude - 0.10f, airPropeller.trailAmplitude + 0.10f);
            this.period = Random.Range(airPropeller.trailPeriod - 0.20f, airPropeller.trailPeriod + 0.20f);
            this.speed = Random.Range(airPropeller.trailSpeed - 0.075f, airPropeller.trailSpeed + 0.075f);

            this.timer = Random.Range(-30, 0);
        }

        public void Advance()
        {
            Move();
        }

        private void Move()
        {
            AirPropeller closestAirPropeller =
                this.airPropeller.chunk.NearestItemTo<AirPropeller>(this.position, 1f);

            Vector3 breatheMove = Vector3.zero;
            this.timer += 1;

            if(this.timer >= 0)
            {
                if(closestAirPropeller != null &&
                closestAirPropeller.CurrentDirection ==  this.airPropeller.CurrentDirection)
                {
                    this.direction = this.airPropeller.CurrentDirection;

                    float t = 100f;
                    float v = (this.timer % t) / t;
                    float speed = this.airPropeller.trailCurve.Evaluate(v) * this.speed;
                    this.position += this.direction * speed;
                    
                    Vector3 right = Quaternion.AngleAxis(90f, Vector3.forward) * this.direction;

                    float theta = this.timer / this.period;
                    float distance = this.amplitude * Mathf.Sin(theta);

                    breatheMove = right * distance;
                    this.go.transform.position = this.position + breatheMove;
                }
                else
                {
                    Reset();
                }
            }
        }
        
        private void Reset()
        {
            this.resetTimer += 1;

            if(this.resetTimer >= ResetTime)
            {
                this.timer = 0;
                this.resetTimer = 0;
                
                Randomize();

                this.airPropeller.FinishWindTrail(this);
            }
        }
    }

    private void InitTrailPool()
    {
        for (int i = 0; i < PoolSize; i++)
        {
            GameObject trailGO =
                Instantiate(this.trailPrefab, this.position, Quaternion.identity);
            trailGO.transform.SetParent(this.transform);

            WindTrail windTrail = new WindTrail(trailGO, this);
            AddWindTrail(windTrail);
        }
    }

    private void AddWindTrail(WindTrail windTrail)
    {
        this.trailPool.Add(windTrail);
        windTrail.go.SetActive(false);
    }

    private void RemoveWindTrail(WindTrail windTrail)
    {
        this.trailPool.Remove(windTrail);
        windTrail.go.SetActive(true);
    }

    private WindTrail GetWindTrail()
    {
        WindTrail chosenWindTrail = this.trailPool[0];
        RemoveWindTrail(chosenWindTrail);
        return chosenWindTrail;
    }

    private void FinishWindTrail(WindTrail windTrail)
    {
        this.activeTrails.Remove(windTrail);
        AddWindTrail(windTrail);
    }

    private void ManageTrails()
    {
        if(IsAreaLeader == false) return;

        if(this.trailPool == null || this.trailPool.Count <= 0)
        {
            InitTrailPool();
        }

        if(this.map.frameNumber % 30 == 0)
        {
            WindTrail windTrail = GetWindTrail();
            this.activeTrails.Add(windTrail);
        }

        for (int i = this.activeTrails.Count - 1 - 1; i >= 0 ; i--)
        {
            this.activeTrails[i].Advance();
        }
    }
}
