using UnityEngine;
using System.Collections;

// this is the OLD airlock, which is now coming out of usage as i type this.
// the new airlock is AirlockAndCannon and has a cannon that shoots you out.

// you don't need to add this to the levels, it's the chunk's responsibility
// to add these between chunks whose gravity situations differ

public class Airlock : Item
{
    public Animated.Animation animationBoltUnlock;
    public Animated.Animation animationBoltLock;

    public Transform leftDoor;
    public Transform leftDoorMore;
    public Transform rightDoor;
    public Transform rightDoorMore;
    public Animated lockAnimated;

    private bool isVertical;
    private float doorOpenOffset;
    private bool isDoorOpen;
    private bool isLockOpen;
    private bool isDoorAnimating;
    private bool isLockAnimating;

    public override void Init(Def def)
    {
        base.Init(def);

        int chunkCenterTy = (def.startChunk.yMin + def.startChunk.yMax) / 2;
        var marker =
            (def.ty < chunkCenterTy) ? def.startChunk.entry : def.startChunk.exit;
        var flipFactor = def.startChunk.isFlippedHorizontally == true ? -1 : 1;

        switch (marker.type)
        {
            case Chunk.MarkerType.Left:
                this.position.x = marker.mapLocation.x - (4 * flipFactor);
                this.leftDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.rightDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.doorOpenOffset = 2.8f;
                break;
            case Chunk.MarkerType.Center:
                this.position.x = marker.mapLocation.x;
                this.leftDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.rightDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.doorOpenOffset = 2.8f;
                break;
            case Chunk.MarkerType.Right:
                this.position.x = marker.mapLocation.x + (4 * flipFactor);
                this.leftDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.rightDoorMore.localScale = new Vector3(2.1f, 1, 1);
                this.doorOpenOffset = 2.8f;
                break;
            case Chunk.MarkerType.Wide:
                this.position.x = marker.mapLocation.x;
                this.leftDoorMore.localScale = new Vector3(10.1f, 1, 1);
                this.rightDoorMore.localScale = new Vector3(10.1f, 1, 1);
                this.doorOpenOffset = 6.8f;
                break;
            case Chunk.MarkerType.Wall:
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
                this.position = new Vector2(
                    marker.mapLocation.x + 0.5f,
                    marker.mapLocation.y
                );
                this.leftDoorMore.localScale = new Vector3(4.1f, 1, 1);
                this.rightDoorMore.localScale = new Vector3(4.1f, 1, 1);
                this.doorOpenOffset = 3.8f;
                this.isVertical = true;
                break;
        }
        if (this.isVertical == false)
            this.position.y += 0.5f;
        this.transform.position = this.position;
    }

    public override void Advance()
    {
        var player = this.map.player;
        bool shouldBeOpen =
            (this.isVertical == true) ?
            shouldBeOpen = Mathf.Abs(player.position.x - this.position.x) < 4 :
            shouldBeOpen = Mathf.Abs(player.position.y - this.position.y) < 4;

        if (this.isLockAnimating == false && this.isDoorAnimating == false)
        {
            if (shouldBeOpen == true && isLockOpen == false)
            {
                this.isLockAnimating = true;
                this.lockAnimated.PlayOnce(this.animationBoltUnlock, delegate
                {
                    this.isLockOpen = true;
                    this.isLockAnimating = false;
                });
            }
            else if (shouldBeOpen == true && isDoorOpen == false)
            {
                StartCoroutine(DoorOpenCoroutine());
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvSpacedoorOpen,
                    position: this.position
                );
            }
            else if (shouldBeOpen == false && isDoorOpen == true)
            {
                StartCoroutine(DoorCloseCoroutine());
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvSpacedoorClose,
                    position: this.position
                );
            }
            else if (shouldBeOpen == false && isLockOpen == true)
            {
                this.isLockAnimating = true;
                this.lockAnimated.PlayOnce(this.animationBoltLock, delegate
                {
                    this.isLockOpen = false;
                    this.isLockAnimating = false;
                });
            }
        }
    }

    private IEnumerator DoorOpenCoroutine()
    {
        this.isDoorAnimating = true;

        for (float openAmount = 0; openAmount <= 1; openAmount += 0.22f)
        {
            this.leftDoor.localPosition = new Vector3(-openAmount * this.doorOpenOffset, 0, 0);
            this.rightDoor.localPosition = new Vector3(openAmount * this.doorOpenOffset, 0, 0);
            yield return null;
        }

        this.leftDoor.localPosition = new Vector3(-this.doorOpenOffset, 0, 0);
        this.rightDoor.localPosition = new Vector3(this.doorOpenOffset, 0, 0);
        this.isDoorAnimating = false;
        this.isDoorOpen = true;
    }

    private IEnumerator DoorCloseCoroutine()
    {
        this.isDoorAnimating = true;

        for (float openAmount = 1; openAmount >= 0; openAmount -= 0.22f)
        {
            this.leftDoor.localPosition = new Vector3(-openAmount * this.doorOpenOffset, 0, 0);
            this.rightDoor.localPosition = new Vector3(openAmount * this.doorOpenOffset, 0, 0);
            yield return null;
        }

        this.leftDoor.localPosition = new Vector3(0, 0, 0);
        this.rightDoor.localPosition = new Vector3(0, 0, 0);
        this.isDoorAnimating = false;
        this.isDoorOpen = false;
    }
}
