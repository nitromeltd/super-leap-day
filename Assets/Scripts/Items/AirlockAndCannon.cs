using System.Linq;
using UnityEngine;

public class AirlockAndCannon : Item
{
    [System.Serializable]
    public class Variant
    {
        public Transform container;
        public SpriteRenderer[] spritesOnLeft;
        public SpriteRenderer[] spritesOnRight;
        public Animated.Animation animationHatchClose;
        public Animated.Animation animationHatchOpen;
        public Animated.Animation animationHatchWalkOver;
        public Animated.Animation animationHatchStepOff;
        public Animated animatedEntrance;
        public Animated animatedExit;
        public SpriteRenderer spriteRendererEntrance;
        public SpriteRenderer spriteRendererExit;
    }
    public Variant vertical;
    public Variant horizontal;
    private Variant activeVariant;
    private bool isEntranceOpen = true;
    private bool isClosedAfterFiring = false;
    private bool hasBeenExtendedByOutsideBoundary = false;
    private float? requestedSizeLeft;
    private float? requestedSizeRight;

    public override void Init(Def def)
    {
        base.Init(def);

        this.activeVariant = (this.chunk.isHorizontal ? this.horizontal : this.vertical);
        this.vertical.container.gameObject.SetActive(this.chunk.isHorizontal == false);
        this.horizontal.container.gameObject.SetActive(this.chunk.isHorizontal == true);

        var lastChunk = this.map.chunks[this.chunk.index - 1];
        var nextChunk = this.map.chunks[this.chunk.index + 1];

        Chunk.MarkerType MaybeFlip(Chunk.MarkerType type, bool flip) =>
            (type, flip) switch
            {
                (Chunk.MarkerType.Left, true)  => Chunk.MarkerType.Right,
                (Chunk.MarkerType.Right, true) => Chunk.MarkerType.Left,
                _                              => type
            };

        var markerType = MaybeFlip(nextChunk.entry.type, nextChunk.isFlippedHorizontally);
        if (markerType == Chunk.MarkerType.Wide)
            markerType = MaybeFlip(lastChunk.exit.type, lastChunk.isFlippedHorizontally);

        if (markerType == Chunk.MarkerType.Left)
            this.position.x -= 4;
        if (markerType == Chunk.MarkerType.Right)
            this.position.x += 4;

        this.transform.position = this.position;
        this.transform.rotation = Quaternion.identity;
        if (this.chunk.isHorizontal == true && this.chunk.isFlippedHorizontally == true)
            this.transform.localScale = new Vector3(-1, 1, 1);

        UpdateHitboxes(true);
    }

    private Vector2 PositionOfEntrance()
    {        
        Vector2 entranceLocal =
            (this.activeVariant == this.vertical) ?
            new Vector2(0, -1.5f) :
            new Vector2(-1.5f, 0);
        return this.transform.TransformPoint(entranceLocal);
    }

    private Vector2 Direction()
    {
        Vector2 forwardLocal =
            (this.activeVariant == this.vertical) ? Vector2.up : Vector2.right;
        return this.transform.TransformVector(forwardLocal);
    }

    public override void Advance()
    {
        base.Advance();

        var player = this.map.player;
        var entrance = PositionOfEntrance();

        if (
            player.state != Player.State.InsidePipe &&
            player.state != Player.State.InsideAirlockCannon &&
            player.state != Player.State.InsideSpaceBooster &&
            player.state != Player.State.AttractedToAirlockCannon &&
            (player.position - entrance).sqrMagnitude < 4 * 4 &&
            Vector2.Dot(player.position - entrance, Direction()) < 0 &&
            (
                player.groundState.onGround == false ||
                (player.position - this.position).sqrMagnitude < 1.5f * 1.5f
            ) &&
            player.state != Player.State.WallSlide
        )
        {
            player.state = Player.State.AttractedToAirlockCannon;
        }

        if (
            player.state == Player.State.AttractedToAirlockCannon &&
            (player.position - entrance).sqrMagnitude < 8 * 8 &&
            Vector2.Dot(player.position - entrance, Direction()) < 0
        )
        {
            var direction = Direction();
            var across = new Vector2(direction.y, -direction.x);

            var amountForward = Vector2.Dot(player.position - entrance, direction);
            var amountSideways = Vector2.Dot(player.position - entrance, across);

            var forward = Vector2.Dot(player.velocity, direction);
            var sideways = Vector2.Dot(player.velocity, across);

            forward *= 0.99f;
            sideways *= 0.85f;
            forward += -amountForward * 0.003f;
            sideways += -amountSideways * 0.012f;

            player.velocity = (direction * forward) + (across * sideways);

            for (int n = 0; n < 10; n += 1)
            {
                player.position += player.velocity * 0.1f;
                player.CheckAllSensors();
            }
        }

        if (
            player.state != Player.State.InsideAirlockCannon &&
            (player.position - entrance).sqrMagnitude < 1 * 1 &&
            Vector2.Dot(player.position - this.position, Direction()) < 0
        )
        {
            player.EnterAirlockCannon(this);
        }

        if (
            player.alive == true &&
            this.map.NearestChunkTo(player.position).index < this.chunk.index &&
            this.isEntranceOpen == false
        )
        {
            // the player has died, so we need to reopen the entrance
            this.isEntranceOpen = true;
            this.isClosedAfterFiring = false;
            this.activeVariant.animatedEntrance.PlayAndHoldLastFrame(
                this.activeVariant.animationHatchOpen
            );
            this.activeVariant.animatedExit.Stop();
            this.activeVariant.spriteRendererExit.sprite =
                this.activeVariant.animationHatchClose.sprites.Last();
            this.activeVariant.spriteRendererExit.sortingOrder = 9;
        }

        if (this.isClosedAfterFiring == true)
        {
            if (player.groundState.onGround == true &&
                player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace()))
            {
                this.activeVariant.animatedExit.PlayAndHoldLastFrame(
                    this.activeVariant.animationHatchWalkOver
                );
                this.activeVariant.spriteRendererExit.sortingOrder = 11;
            }
            else
            {
                this.activeVariant.animatedExit.PlayAndHoldLastFrame(
                    this.activeVariant.animationHatchStepOff
                );
                this.activeVariant.animatedExit.GetComponent<SpriteRenderer>().
                    sortingOrder = 9;
            }
        }

        UpdateHitboxes();
    }

    private void UpdateHitboxes(bool force = false)
    {
        var direction = Direction();
        var playerRelative = (this.map.player.position - this.position);
        var playerForward = Vector2.Dot(playerRelative, direction);

        var isTopHitboxActive = /* true unless we just used the cannon */
            !(
                this.map.player.noGravityTimeWhenBoostFromAirlock > 0 &&
                (this.map.player.position - this.position).sqrMagnitude < 2.01f * 2.01f
            );

        var isBottomHitboxActive = /* true unless we're going into it or about to */
            !(
                (
                    this.map.player.state == Player.State.AttractedToAirlockCannon ||
                    this.map.player.state == Player.State.InsideAirlockCannon
                )
                && playerForward > -2
                && playerForward < 0
                && playerRelative.sqrMagnitude < 3 * 3
            );

        if (force == false)
        {
            var intendedNumberOfHitboxes =
                3 + (isTopHitboxActive ? 1 : 0) + (isBottomHitboxActive ? 1 : 0);
            if (intendedNumberOfHitboxes == this.hitboxes.Length)
                return; // we already have the right hitboxes set up
        }

        var (l, r) = (this.def.tile.rotation, this.def.tile.flip) switch
        {
            (270, false) => (
                // horizontal airlock facing right
                this.chunk.yMin + 1 - this.position.y,
                this.chunk.yMax - this.position.y
            ),
            (90, true) => (
                // horizontal airlock facing left
                this.chunk.yMin + 1 - this.position.y,
                this.chunk.yMax - this.position.y
            ),
            _ => (
                // standard vertical airlock facing upwards
                this.chunk.xMin + 1 - this.position.x,
                this.chunk.xMax - this.position.x
            )
        };

        l = this.requestedSizeLeft ?? l;
        r = this.requestedSizeRight ?? r;

        var rotation = this.def.tile.rotation;

        // in horizontal chunks, it makes sense to wall slide on the airlock sides.
        // in vertical chunks, the only walls you can slide on are the interior ones,
        // which is undesirable.
        var w = this.chunk.isHorizontal;

        var left   = new Hitbox( l, -1,   -1,     1, rotation, Hitbox.Solid,    w, w);
        var right  = new Hitbox( 1,  r,   -1,     1, rotation, Hitbox.Solid,    w, w);
        var btm    = new Hitbox(-1,  1,   -1, -0.8f, rotation, Hitbox.Solid,    w, w);
        var top    = new Hitbox(-1,  1, 0.8f,     1, rotation, Hitbox.Solid,    w, w);
        var topLid = new Hitbox(-1,  1,    1,  1.5f, rotation, Hitbox.NonSolid, w, w);

        this.hitboxes = (top: isTopHitboxActive, btm: isBottomHitboxActive) switch
        {
            (top: false, btm: false) => new [] { topLid, left,           right },
            (top: true,  btm: false) => new [] { topLid, left, top,      right },
            (top: false, btm: true)  => new [] { topLid, left,      btm, right },
            (top: true,  btm: true)  => new [] { topLid, left, top, btm, right }
        };

        {
            bool ShouldShow(Vector2 localPosition)
            {
                if (this.hasBeenExtendedByOutsideBoundary == true)
                    return true;
                else if (this.chunk.isHorizontal == true)
                    return localPosition.y > l && localPosition.y < r;
                else
                    return localPosition.x > l && localPosition.x < r;
            }

            foreach (var sprite in this.activeVariant.spritesOnLeft)
            {
                sprite.gameObject.SetActive(ShouldShow(sprite.transform.localPosition));
            }
            foreach (var sprite in this.activeVariant.spritesOnRight)
            {
                sprite.gameObject.SetActive(ShouldShow(sprite.transform.localPosition));
            }
        }
    }

    public void ExtendDownwards(int minTyMap)
    {
        var list = this.horizontal.spritesOnRight.ToList();
        var currentLowest =
            Mathf.FloorToInt(list.Min(item => item.transform.position.y));

        for (int y = currentLowest; y >= minTyMap; y -= 1)
        {
            var previous = list.Last();
            var instance = Instantiate(previous, previous.transform.parent);
            instance.gameObject.name = $"Airlock piece y = {y}";

            var pos = instance.transform.position;
            pos.y = y + 0.5f;
            instance.transform.position = pos;

            list.Add(instance);
        }

        this.horizontal.spritesOnRight = list.ToArray();
        this.hasBeenExtendedByOutsideBoundary = true;

        if (this.chunk.isFlippedHorizontally == false)
            this.requestedSizeRight = -(minTyMap - this.position.y);
        else
            this.requestedSizeLeft = minTyMap - this.position.y;
        UpdateHitboxes(true);
    }

    public void ExtendUpwards(int maxTyMap)
    {
        var list = this.horizontal.spritesOnLeft.ToList();
        var currentHighest =
            Mathf.FloorToInt(list.Max(item => item.transform.position.y));

        for (int y = currentHighest; y <= maxTyMap; y += 1)
        {
            var previous = list.Last();
            var instance = Instantiate(previous, previous.transform.parent);
            instance.gameObject.name = $"Airlock piece y = {y}";

            var pos = instance.transform.position;
            pos.y = y + 0.5f;
            instance.transform.position = pos;

            list.Add(instance);
        }

        this.horizontal.spritesOnLeft = list.ToArray();
        this.hasBeenExtendedByOutsideBoundary = true;

        if (this.chunk.isFlippedHorizontally == false)
            this.requestedSizeLeft = -(maxTyMap + 1 - this.position.y);
        else
            this.requestedSizeRight = maxTyMap + 1 - this.position.y;
        UpdateHitboxes(true);
    }

    public void AdvancePlayer()
    {
        var player = this.map.player;

        var entrance = PositionOfEntrance();
        var direction = Direction();

        if (Vector2.Dot(player.position - entrance, direction) < 0.1f &&
            (player.position - entrance).sqrMagnitude > 0.1f * 0.1f)
        {
            var to = Vector2.MoveTowards(player.position, entrance, 0.5f);
            player.velocity = to - player.position;

            for (int n = 0; n < 10; n += 1)
            {
                player.position += player.velocity * 0.1f;
                player.CheckAllSensors();
            }
        }
        else
        {
            var to = Vector2.MoveTowards(player.position, this.position, 0.5f);
            player.velocity = to - player.position;

            for (int n = 0; n < 10; n += 1)
            {
                player.position += player.velocity * 0.1f;
                player.CheckAllSensors();
            }

            if (this.isEntranceOpen == true)
            {
                this.isEntranceOpen = false;
                this.activeVariant.animatedEntrance.PlayAndHoldLastFrame(
                    this.activeVariant.animationHatchClose
                );
                this.activeVariant.animatedExit.PlayAndHoldLastFrame(
                    this.activeVariant.animationHatchOpen
                );
            }
        }

        this.map.player.groundState = Player.GroundState.Airborne(this.map.player);
    }

    public void FirePlayer()
    {
        var forward = Direction();

        if (this.chunk.isHorizontal == true)
            this.map.player.position += forward * 1.0f;
        else
            this.map.player.position += forward * 0.6f;

        this.map.player.velocity = forward * 0.6f;
        this.map.player.ExitAirlockCannon();

        UpdateHitboxes(true);

        Invoke(() => {
            this.activeVariant.animatedExit.PlayOnce(
                this.activeVariant.animationHatchClose,
                () => { this.isClosedAfterFiring = true; }
            );
        }, 0.2f);
    }
}
