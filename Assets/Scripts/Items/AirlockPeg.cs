using UnityEngine;

public class AirlockPeg : Item
{
    private bool forcePlayerRight;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new []
        {
            new Hitbox(-0.2f, 0.2f, 0, 0.6f, this.rotation, Hitbox.Solid)
        };
    }

    public void SetDirection(bool forcePlayerRight)
    {
        this.forcePlayerRight = forcePlayerRight;
    }

    public override void Advance()
    {
        var player = this.map.player;
        if (player.groundState.onGround == true &&
            player.facingRight != this.forcePlayerRight &&
            Mathf.Round(player.groundState.angleDegrees) == this.rotation &&
            this.hitboxes[0].InWorldSpace().Overlaps(player.Rectangle()))
        {
            player.facingRight = this.forcePlayerRight;
        }
    }
}
