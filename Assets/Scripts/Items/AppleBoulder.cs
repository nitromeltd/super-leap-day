
using UnityEngine;
using System.Linq;

public class AppleBoulder : Item
{
    public enum Size
    {
        Small,
        Big
    }

    public Size size;
    public Animated.Animation animationIdle;
    public Animated.Animation animationBreak;

    public Animated.Animation[] animationDebris;

    public Vector2 velocity;
    private float angleRadians;
    private float spin;
    private Transform edge;

    private bool active;
    private Vector2 initialPos;
    private float radius;
    private bool destroyed;
    private bool grounded;
    public Button button;
    private float smoothVelocity;

    private SpriteRenderer[] spriteRenderers;

    private const float Gravity = 0.032f;
    private const float RadiusSmall = 1.425f;
    private const float RadiusBig = 2.85f;
    private const float Bounciness = 0.05f;
    private const float MaxHorizontalSpeed = 0.225f;
    private Vector3[] particlePosition = { new Vector3(1.2f, 0.7f, 0), new Vector3(-1, -1, 0), new Vector3(-0.6f, 0.3f, 0), new Vector3(-1.3f, 1.4f, 0), new Vector3(1.5f, -1.4f, 0) };

    private void Awake()
    {
        this.spriteRenderers = this.GetComponentsInChildren<SpriteRenderer>();
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.edge = this.transform.Find("edge");

        this.initialPos = this.position;

        bool isSmall = this.size == Size.Small;
        this.radius = isSmall ? RadiusSmall : RadiusBig;

        this.destroyed = false;
        this.grounded = false;
        
        var nearestConnectionNode =
            this.chunk.connectionLayer.NearestNodeTo(this.position, 4);
        if (nearestConnectionNode != null)
        {
            var otherNode =
                nearestConnectionNode.Previous() ??
                nearestConnectionNode.Next();
            this.button = this.chunk.NearestItemTo<Button>(otherNode.Position);
        }

        this.active = (this.button == null);
        this.animated.PlayOnce(this.animationIdle);
    }

    public override void Advance()
    {
        if (this.active == false &&
            this.button != null &&
            this.button.PressedThisFrame == true)
        {
            this.active = true;
        }

        if(this.active == false) return;
        if(this.destroyed == true) return;

        this.velocity.x = Mathf.Clamp(
            this.velocity.x,
            -MaxHorizontalSpeed,
            MaxHorizontalSpeed
        );

        this.velocity.y -= Gravity;
        this.position += this.velocity;

        HandleCollision();
        HandleInteract();

        DetectGrounded();
        DetectChase();        

        this.transform.position = this.position;
        this.angleRadians += this.spin;
        this.edge.transform.rotation = Quaternion.Euler(0, 0, this.angleRadians);
    }

    private void HandleCollision()
    {
        Vector2? nearest = null;
        float squareDistance = this.radius * this.radius;
        
        void IsOverlappingBlock(Item item)
        {
            if(item.hitboxes.Length <= 0) return;

            Hitbox itemHitbox = item.hitboxes[0];
            Vector2 itemCenter = itemHitbox.InWorldSpace().center;

            Vector2 relativePosition = this.position.Add(
                -itemCenter.x, -itemCenter.y
            );

            Vector2 relativePt = new Vector2(
                Mathf.Clamp(relativePosition.x, itemHitbox.xMin, itemHitbox.xMax),
                Mathf.Clamp(relativePosition.y, itemHitbox.yMin, itemHitbox.yMax)
            );

            Vector2 pt = relativePt.Add(itemCenter.x, itemCenter.y);
            float ptSqDist = (pt - this.position).sqrMagnitude;
            
            if(ptSqDist < squareDistance)
            {
                nearest = pt;
                squareDistance = ptSqDist;
            }
        }

        {
            float centerX = (this.chunk.xMin + this.chunk.xMax) * 0.5f;
            float centerY = (this.chunk.yMin + this.chunk.yMax) * 0.5f;
            float xEdge =
                (this.position.x < centerX) ? this.chunk.xMin : this.chunk.xMax + 1;
            float yEdge =
                (this.position.y < centerY) ? this.chunk.yMin : this.chunk.yMax + 1;
            float xDistance = Mathf.Abs(this.position.x - xEdge);
            float yDistance = Mathf.Abs(this.position.y - yEdge);
            if (xDistance < this.radius && xDistance < yDistance)
            {
                nearest = new Vector2(xEdge, this.position.y);
                squareDistance = xDistance * xDistance;
            }
            /*else if (yDistance < this.radius)
            {
                nearest = new Vector2(this.position.x, yEdge);
                squareDistance = yDistance * yDistance;
            }*/
        }

        // collide with tiles
        foreach (var tileGrid in this.chunk.solidTilesA)
        {
            for (
                int tx = Mathf.FloorToInt(this.position.x - this.radius);
                tx <= this.position.x + this.radius;
                tx += 1
            )
            {
                for (
                    int ty = Mathf.FloorToInt(this.position.y - this.radius);
                    ty <= this.position.y + this.radius;
                    ty += 1
                )
                {
                    var tile = tileGrid.GetTile(
                        tx - this.chunk.xMin,
                        ty - this.chunk.yMin
                    );
                    if (tile == null) continue;
                    
                    var relativePt = tile.NearestSolidPointTo(
                        this.position.Add(-tile.mapTx, -tile.mapTy)
                    );
                    if (relativePt == null) continue;

                    Vector2 pt = relativePt.Value.Add(tile.mapTx, tile.mapTy);
                    float ptSqDist = (pt - this.position).sqrMagnitude;

                    if (ptSqDist < squareDistance)
                    {
                        nearest = pt;
                        squareDistance = ptSqDist;
                    }
                }
            }
        }

        // collide with other block-like items
        foreach (var item in chunk.items.OfType<ButtonTriggerPlatform>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<ABBlock>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<GateBlock>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<TimerBlock>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<MetalBlock>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<AppleBoulderEntranceCorner>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<LightUpBlock>())
        {
            IsOverlappingBlock(item);
        }
        foreach (var item in chunk.items.OfType<EnemyBlocker>())
        {
            IsOverlappingBlock(item);
        }
        if (nearest == null) return;

        Vector2 toPoint = nearest.Value - this.position;
        Vector2 into = toPoint.normalized;
        this.position -= into * (radius - toPoint.magnitude);
        float dot = Vector2.Dot(this.velocity, into);

        if (dot > 0.1f)
        {
            float multiplier = (this.size == Size.Small) ? 1f : 2f;
            this.map.ScreenShakeAtPosition(this.position, this.velocity * multiplier);
            
            float dotLeft = Vector2.Dot(into, Vector2.left);
            float dotRight = Vector2.Dot(into, Vector2.right);

            bool wallCollision = (dotLeft == 1 || dotRight == 1);

            if(wallCollision == true)
            {
                Break();
            }
        }

        Vector2 across = new Vector2(into.y, -into.x);
        this.velocity =
            (across * Vector2.Dot(across, this.velocity)) +
            (into   * Vector2.Dot(into,   this.velocity) * -Bounciness);
        this.spin = Vector2.Dot(across, this.velocity) * this.radius * Mathf.PI * 2 * 1.25f;
    }

    private void HandleInteract()
    {
        if(this.destroyed == true) return;

        float squareDistance = this.radius * this.radius;

        // kill player on contact
        {
            Player player = this.map.player;
            Rect playerRect = player.Rectangle();
            Vector2 playerCenter = playerRect.center;

            Vector2 relativePosition = this.position.Add(
                -playerCenter.x, -playerCenter.y
            );

            float playerMinX = playerRect.min.x - playerCenter.x;
            float playerMaxX = playerRect.max.x - playerCenter.x;
            float playerMinY = playerRect.min.y - playerCenter.y;
            float playerMaxY = playerRect.max.y - playerCenter.y;

            Vector2 relativePt = new Vector2(
                Mathf.Clamp(relativePosition.x, playerMinX, playerMaxX),
                Mathf.Clamp(relativePosition.y, playerMinY, playerMaxY)
            );

            Vector2 pt = relativePt.Add(playerCenter.x, playerCenter.y);
            float ptSqDist = (pt - this.position).sqrMagnitude;

            if (ptSqDist < squareDistance)
            {
                player.Hit(relativePt);
            }
        }

        // kill enemies on contact
        foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
        {            
            if (IsOverlapping(enemy))
            {
                enemy.Hit(
                    new Enemy.KillInfo { itemDeliveringKill = this }
                );
            }
        }

        // destroy breakable items
        {
            foreach (var item in chunk.subsetOfItemsOfTypeTNTBlock)
            {
                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    if (IsOverlapping(item))
                    {
                        item.Break();
                    }
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeBreakableBlock)
            {
                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    if (IsOverlapping(item))
                    {
                        item.Break(false);
                    }
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeRandomBox)
            {
                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    if (IsOverlapping(item))
                    {
                        item.Break();
                    }
                }
            }
        }
    }

    public bool IsOverlapping(Item item)
    {
        if(destroyed == true) return false;

        float squareDistance = this.radius * this.radius;

        Hitbox itemHitbox = item.hitboxes[0];
        Vector2 itemCenter = itemHitbox.InWorldSpace().center;

        Vector2 relativePosition = this.position.Add(
            -itemCenter.x, -itemCenter.y
        );

        Vector2 relativePt = new Vector2(
            Mathf.Clamp(relativePosition.x, itemHitbox.xMin, itemHitbox.xMax),
            Mathf.Clamp(relativePosition.y, itemHitbox.yMin, itemHitbox.yMax)
        );

        Vector2 pt = relativePt.Add(itemCenter.x, itemCenter.y);
        float ptSqDist = (pt - this.position).sqrMagnitude;
        
        return ptSqDist < squareDistance;
    }

    private void DetectGrounded()
    {
        this.grounded = false;

        if(this.velocity.y >= 0.10f) return;

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);

        float yMin = -radius;
        var tolerance = 0.1f;
        var start = this.position;
        var maxDistance = -this.velocity.y - yMin + tolerance;

        float hSpacing = 0.8f * this.transform.localScale.x;

        var result = Raycast.CombineClosest(
            raycast.Vertical(start + new Vector2(-hSpacing, 0), false, maxDistance),
            raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
            raycast.Vertical(start + new Vector2(hSpacing, 0), false, maxDistance)
        );

        if (result.anything == true &&
            result.distance < -this.velocity.y - yMin + tolerance)
        {
            this.grounded = true;
        }
    }

    private void DetectChase()
    {
        Player player = this.map.player;

        var isPlayerInSameChunk =
            this.map.NearestChunkTo(player.position) == this.chunk;

        bool movingRight = this.velocity.x > 0f;

        if(this.size == Size.Small) return;
        if(this.grounded == false) return;
        if(isPlayerInSameChunk == false) return;
        if(movingRight != player.facingRight) return;
        if(Mathf.Abs(this.velocity.x) < 0.10f) return;

        //float bounds = 5f;
        bool playerSameHorizontal = true;
            /*(this.position.y + bounds > player.position.y &&
            this.position.y - bounds < player.position.y);*/

        if(playerSameHorizontal == true) 
        {
            if(Mathf.Abs(player.velocity.x) > 0.15f)
            {
                this.velocity.x = Mathf.SmoothDamp(
                    this.velocity.x,
                    player.velocity.x,
                    ref smoothVelocity,
                    0.05f
                );
            }
        }
    }

    private void Break()
    {
        this.destroyed = true;

        this.spriteRenderers[1].enabled = false;

        this.animated.PlayOnce(this.animationBreak, () =>
        {
            foreach (var spriteRenderer in this.spriteRenderers)
            {
                this.spriteRenderer.enabled = false;
            }

            int i = 0;
            foreach (var debris in this.animationDebris)
            {
                float x = Random.Range(-2, 2);
                float y = Random.Range(-2, 2);

                var particle = Particle.CreatePlayAndHoldLastFrame(
                    debris, 100, this.transform.position + (particlePosition[i] * this.transform.localScale.x), this.transform.parent
                );

                particle.transform.localScale = this.transform.localScale;
                particle.spin = Random.Range(-0.5f, 0.5f);

                float direction = (Random.value > 0.50f) ? 1f : -1f;

                float hVelocity = Random.Range(0.03f, 0.06f);
                float vVelocity = Random.Range(0.3f, 0.4f);

                float vAcceleration = Random.Range(-0.02f, -0.04f);

                particle.velocity = new Vector2(hVelocity * direction, vVelocity);
                particle.acceleration = new Vector2(0f, vAcceleration);

                i++;
            }
        });
    }

    public override void Reset()
    {
        base.Reset();

        foreach(var spriteRenderer in spriteRenderers)
        {
            spriteRenderer.enabled = true;
        }
        this.animated.PlayOnce(this.animationIdle);
        
        this.active = (this.button == null);
        this.velocity = Vector2.zero;
        this.angleRadians = 0f;
        this.spin = 0f;
        this.destroyed = false;
        this.grounded = false;

        SpawnParticles();

        this.transform.position = this.position = this.initialPos;

        SpawnParticles();
    }

    private void SpawnParticles()
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );

        float scale = this.size == Size.Big ? 2f : 1f;
        p.transform.localScale = Vector3.one * scale;
    }
}
