using UnityEngine;

public class AppleBoulderEntrance : Item
{
   public bool isBig = true;

    public override void Init(Def def)
    {
        base.Init(def);

        if (this.isBig == true)
        {
            this.hitboxes = new[] {
                new Hitbox(-4f, 4f, 0f, 1f, this.rotation, Hitbox.Solid),
            };
        }
        else
        {
            this.hitboxes = new[] {
                new Hitbox(-1.5f, 1.5f, 0f, 1f, this.rotation, Hitbox.Solid),
            };
        }

        this.solidToEnemy = false;
        
    }

}
