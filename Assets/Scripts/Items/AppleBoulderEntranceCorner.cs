using UnityEngine;

public class AppleBoulderEntranceCorner : Item
{
    public bool isBig = true;

    public override void Init(Def def)
    {
        base.Init(def);

        Vector3 rotation = this.transform.localEulerAngles;

        if (this.isBig == true)
        {
            this.hitboxes = new[] {
                new Hitbox(-0.3f, 0.3f, -0.5f, 0.5f, this.rotation, Hitbox.Solid),
            };
        }
        else
        {
            this.hitboxes = new[] {
                new Hitbox(-0.2f, 0.2f, -0.5f, 0.5f, this.rotation, Hitbox.Solid),
            };
        }
        this.solidToPlayer = false;
    }
}
