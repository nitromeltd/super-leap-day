using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AshBlock : Item
{
    public Animated.Animation animationAshParticles;
    public SpriteRenderer backSpriteRend;
    public Sprite[] ashSprites;
    public Sprite[] backSprites;

    private bool disintegrated = false;
    private float currentAlpha = 1f;
    private float alphaCurrentVelocity = 0f;
    private int disintegrateTimer = 0;
    private bool playerInContact = false;

    public const int DisintegrateTime = 8;
    public static float BlockSize = 1f;

    public bool IsBlockActive => this.disintegrated == false;
    public bool IsPlayerInContact => this.playerInContact == true;

    public static AshBlock Create(Vector2 position, int rotation, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.moltenFortress.ashBlock, chunk.transform);
        obj.name = "Spawned Ash Block";
        var item = obj.GetComponent<AshBlock>();
        item.Init(position, chunk);
        
        item.transform.rotation = Quaternion.Euler(0, 0, rotation);
        item.rotation = rotation;

        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = this.position;

        int spriteIndex = Random.Range(0, this.ashSprites.Length);
        this.spriteRenderer.sprite = this.ashSprites[spriteIndex];
        this.backSpriteRend.sprite = this.backSprites[spriteIndex];
    }

    public override void Init(Def def)
    {
        base.Init(def);
        
        ToggleHitbox(true);
        
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromLeft  = OnCollision;
        this.onCollisionFromRight = OnCollision;

        this.disintegrated = false;

        int spriteIndex = Random.Range(0, this.ashSprites.Length);
        this.spriteRenderer.sprite = this.ashSprites[spriteIndex];
        this.backSpriteRend.sprite = this.backSprites[spriteIndex];

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }

    public override void Advance()
    {
        base.Advance();

        float targetAlpha = this.disintegrated == true ? 0f : 1f;
        this.currentAlpha = Mathf.SmoothDamp(
            this.spriteRenderer.color.a,
            targetAlpha,
            ref this.alphaCurrentVelocity,
            0.10f
        );

        this.spriteRenderer.color = new Color(1f, 1f, 1f, this.currentAlpha);
        this.backSpriteRend.color = new Color(1f, 1f, 1f, this.currentAlpha);
        
        if(this.playerInContact == true &&
            IsPlayerStillInContactWithThisBlock() == false)
        {
            this.playerInContact = false;

            if(this.chunk.IsPlayerInContactWithAnyAshBlock() == false)
            {
                Disintegrate();
            }
        }

        if(this.disintegrateTimer > 0)
        {
            this.disintegrateTimer -= 1;

            if(this.disintegrateTimer == 0)
            {
                EndDisintegrate();
            }
        }
        
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }
    }

    private bool IsPlayerStillInContactWithThisBlock()
    {
        return this.map.player.Rectangle().Inflate(0.20f).
            Overlaps(this.hitboxes[0].InWorldSpace());
    }

    private void OnCollision(Collision collision)
    {
        this.playerInContact = true;
    }
    
    public void Disintegrate()
    {
        if(this.disintegrated == true)
        {
            return;
        }

        this.disintegrated = true;
        this.disintegrateTimer = DisintegrateTime;
    }

    private void EndDisintegrate()
    {
        Rect disintegrateRect1 = new Rect(
            this.hitboxes[0].InWorldSpace().Inflate(Vector2.up * 0.20f)
        );
        Rect disintegrateRect2 = new Rect(
            this.hitboxes[0].InWorldSpace().Inflate(Vector2.right * 0.20f)
        );

        this.map.Disintegrate(disintegrateRect1, this);
        this.map.Disintegrate(disintegrateRect2, this);

        ToggleHitbox(false);
    }

    private void ToggleHitbox(bool solid)
    {
        float halfSize = BlockSize / 2f;
        Hitbox.SolidData solidData = solid == true ? Hitbox.Solid : Hitbox.NonSolid;

        this.hitboxes =
            new [] { new Hitbox(
                -halfSize, halfSize, -halfSize, halfSize,
                this.rotation, solidData, solid, solid
            )
        };
    }
}
