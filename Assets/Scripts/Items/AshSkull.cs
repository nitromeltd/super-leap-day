using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class AshSkull : Item
{
    public Animated.Animation animationEmpty;

    [Header("Furnace")]
    public Animated furnaceAnimated;
    public Animated.Animation animationFurnaceIdle;
    public Animated.Animation animationFurnaceReady;
    public Animated.Animation animationFurnaceHot;

    [Header("Skull Head")]
    public Transform headTransform;
    public Animated headAnimated;
    public Sprite headSprite;
    public Animated.Animation animationHeadHot;
    public Animated.Animation animationHeadCold;
    
    [Header("Lava Top")]
    public Animated lavaTopAnimated;
    public Animated.Animation animationLavaTopActive;
    public Animated.Animation animationLavaTopHotToCold;

    [Header("Lava Middle")]
    public Transform lavaFunnel;
    public Animated lavaFunnelFirstPiece;
    public SpriteMask lavaFunnelSpriteMask;
    public Animated.Animation animationLavaMiddleActive;
    public Animated.Animation animationLavaMiddleToAsh;
    public Animated.Animation animationAshCrumble;

    [Header("Lava Bottom")]
    public Animated lavaBottomAnimated;
    public Animated.Animation animationLavaBottomActive;

    public enum State
    {
        None,
        Idle,
        Detect,
        Grow,
        Stop,
        Ash,
        Restore
    }

    public enum Orientation
    {
        Up,
        Left,
        Down,
        Right
    }

    private Vector2 GetOrientationAsVector() => this.orientation switch
    {
        Orientation.Up    => Vector2.up,
        Orientation.Left  => Vector2.left,
        Orientation.Down  => Vector2.down,
        Orientation.Right => Vector2.right,
        _                 => Vector2.up
    };

    private State state;
    private Orientation orientation;
    private int timer = 0;
    private float extendDistance = 0;
    private int? timeToRestore = 0;
    private List<Animated> lavaFunnelPieces;

    private const int TimeBetweenStopAndAsh = 60 * 1;
    private const float ExtendSpeed = 0.20f;
    private const float HeadCenterYAboveBase = 0.4f;
    private const float LavaTopYAboveBase = -0.372f;
    private const float LavaPieceHeight = 85f / 70f;

    private Hitbox HeadHitbox => this.hitboxes[0];
    private Hitbox BodyHitbox => this.hitboxes[1];
    
    public override void Init(Def def)
    {
        base.Init(def);
        
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromLeft  = OnCollision;
        this.onCollisionFromRight = OnCollision;
        
        this.orientation = this.rotation switch
        {
            90  => Orientation.Left,
            180 => Orientation.Down,
            270 => Orientation.Right,
            _   => Orientation.Up
        };

        this.lavaFunnelPieces = new List<Animated> { this.lavaFunnelFirstPiece };
        this.lavaFunnelFirstPiece.transform.localPosition =
            new Vector2(0, -0.5f * LavaPieceHeight);

        this.furnaceAnimated.PlayAndLoop(this.animationFurnaceIdle);

        ChangeState(State.Idle);
    }
    
    private void OnCollision(Collision collision)
    {
        if (this.state == State.Idle)
        {
            ChangeState(State.Detect);
        }
        else if (this.state == State.Ash)
        {
            if (this.timeToRestore == null)
                this.timeToRestore = 30;
        }
    }
    
    public override void Advance()
    {
        switch (this.state)
        {
            case State.Idle:     AdvanceIdle();     break;
            case State.Detect:   AdvanceDetect();   break;
            case State.Grow:     AdvanceGrow();     break;
            case State.Stop:     AdvanceStop();     break;
            case State.Ash:      AdvanceAsh();      break;
            case State.Restore:  AdvanceRestore();  break;
        }
        
        if (this.map.player.alive == false &&
            this.chunk.index <= this.map.furthestChunkReached.index)
        {
            OnPlayerDeath();
        }
    }

    public override void Reset()
    {
        this.timer = 0;
        this.extendDistance = 0;

        this.headTransform.transform.localPosition =
            new Vector3(0, HeadCenterYAboveBase, 0);
        this.headTransform.gameObject.SetActive(true);

        ChangeState(State.Idle);
    }

    private void OnPlayerDeath()
    {
        // this.stopStates = true;

        // stop coroutine
        // if(TurnLavaToAshCoroutine != null)
        // {
        //     StopCoroutine(TurnLavaToAshCoroutine);
        // }

        // stop animation callbacks
        // this.lavaTopAnimated.onComplete = null;
        // for (int i = 0; i < this.allLavaMiddleAnimated.Length; i++)
        // {
        //     this.allLavaMiddleAnimated[i].onComplete = null;
        // }
    }

    private void AdvanceIdle()
    {
        // empty
    }

    private void AdvanceDetect()
    {
        this.timer += 1;
        if (this.timer > 40)
        {
            this.timer = 0;
            ChangeState(State.Grow);
        }
    }

    private void AdvanceGrow()
    {
        {
            var raycast = new Raycast(
                this.map, this.position, ignoreItem1: this, ignoreTileTopOnlyFlag: true
            );
            var direction = GetOrientationAsVector();
            var start = this.position + (direction * (this.extendDistance + 0.5f));
            var result = raycast.Arbitrary(start, direction, ExtendSpeed);
            if (result.anything == true)
            {
                this.extendDistance += result.distance;
                this.extendDistance = Mathf.Ceil(this.extendDistance);
                Debug.Log(this.extendDistance);
                ChangeState(State.Stop);
            }
            else
            {
                this.extendDistance += ExtendSpeed;
            }
        }

        {
            this.headTransform.localPosition =
                Vector2.up * (this.extendDistance + HeadCenterYAboveBase);

            this.lavaTopAnimated.transform.localPosition =
                Vector2.up * (this.extendDistance + LavaTopYAboveBase);

            this.lavaFunnelSpriteMask.transform.localScale =
                new Vector3(1, this.extendDistance, 1);
            this.lavaFunnel.transform.localPosition =
                new Vector3(0, this.extendDistance, 0);

            if (this.extendDistance + 1 > this.lavaFunnelPieces.Count * LavaPieceHeight)
            {
                var index = this.lavaFunnelPieces.Count;
                var newPiece = Instantiate(
                    this.lavaFunnelPieces[0], this.lavaFunnelPieces[0].transform.parent
                );
                newPiece.transform.localPosition =
                    new Vector2(0, (-index - 0.5f) * LavaPieceHeight);
                newPiece.PlayAndLoop(this.animationLavaMiddleActive);
                newPiece.Synchronize(this.map);
                this.lavaFunnelPieces.Add(newPiece);
            }
        }

        CheckForCollisionPlayerWithLava();
    }

    private void AdvanceStop()
    {
        CheckForCollisionPlayerWithLava();

        {
            var align = (this.map.frameNumber * ExtendSpeed) % 1.0f;
            this.lavaFunnel.transform.localPosition =
                new Vector3(0, this.extendDistance + align, 0);
        }

        if (this.timer > 0)
        {
            this.timer -= 1;

            if (this.timer == 0)
            {
                this.lavaFunnel.transform.localPosition =
                    new Vector3(0, this.extendDistance, 0);
                ChangeState(State.Ash);
            }
        }
    }

    private void AdvanceAsh()
    {
        if (this.timeToRestore != null)
        {
            this.timeToRestore -= 1;
            if (this.timeToRestore.Value < 1)
                ChangeState(State.Restore);
        }
    }

    private void AdvanceRestore()
    {
        if (this.map.frameNumber % 5 == 0)
        {
            if (this.lavaFunnelPieces.Count > 1)
            {
                var lastPiece = this.lavaFunnelPieces.First();
                var position = lastPiece.transform.position;
                this.lavaFunnelPieces.Remove(lastPiece);
                Destroy(lastPiece.gameObject);

                Particle.CreateAndPlayOnce(
                    this.animationAshCrumble, position, this.map.transform
                );

                lastPiece = this.lavaFunnelPieces.FirstOrDefault();
                this.extendDistance =
                    this.lavaFunnel.transform.localPosition.y +
                    lastPiece.transform.localPosition.y + 0.5f;
                if (this.extendDistance < 0)
                    this.extendDistance = 0;

                this.lavaTopAnimated.transform.localPosition =
                    Vector3.up * (this.extendDistance + LavaTopYAboveBase);
                RefreshHitboxes(true, true);

                if (this.extendDistance == 0)
                    this.lavaTopAnimated.gameObject.SetActive(false);
                if (this.extendDistance < 0.5f)
                {
                    this.headAnimated.transform.localPosition =
                        Vector3.up * HeadCenterYAboveBase;
                    this.headAnimated.gameObject.SetActive(true);
                }
            }
            else
            {
                this.lavaFunnelFirstPiece = this.lavaFunnelPieces.First();
                ChangeState(State.Idle);
            }
        }
    }

    private void ChangeState(State newState)
    {
        // if(this.stopStates == true) return;

        this.state = newState;

        switch (newState)
        {
            case State.Idle:     EnterIdle();     break;
            case State.Detect:   EnterDetect();   break;
            case State.Grow:     EnterGrow();     break;
            case State.Stop:     EnterStop();     break;
            case State.Ash:      EnterAsh();      break;
            case State.Restore:  EnterRestore();  break;
        }
    }

    private void EnterIdle()
    {
        if (this.map.frameNumber > 10)
            Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxLavaSpoutSpawn);

        this.lavaFunnelSpriteMask.transform.localScale = new Vector3(1, 0, 1);
        this.lavaTopAnimated.gameObject.SetActive(false);
        this.lavaBottomAnimated.gameObject.SetActive(false);
        this.furnaceAnimated.PlayAndLoop(this.animationFurnaceIdle);
        this.headAnimated.gameObject.SetActive(true);

        RefreshHitboxes(solidHead: true, solidLava: false);
    }

    private void EnterDetect()
    {
        this.timer = 0;
        this.furnaceAnimated.PlayAndLoop(this.animationFurnaceReady);
    }

    private void EnterGrow()
    {
        Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxLavaSpoutRise);
        this.furnaceAnimated.PlayAndLoop(this.animationFurnaceHot);
        this.headAnimated.PlayAndLoop(this.animationHeadHot);

        RefreshHitboxes(solidHead: false, solidLava: false);
        
        this.lavaTopAnimated.gameObject.SetActive(true);
        this.lavaTopAnimated.PlayAndLoop(this.animationLavaTopActive);
        this.lavaBottomAnimated.gameObject.SetActive(true);
        this.lavaBottomAnimated.PlayAndLoop(this.animationLavaBottomActive);

        for (int n = 0; n < this.lavaFunnelPieces.Count; n += 1)
        {
            var piece = this.lavaFunnelPieces[n];
            piece.transform.localPosition =
                new Vector2(0, (-n - 0.5f) * LavaPieceHeight);
            piece.PlayAndLoop(this.animationLavaMiddleActive);
            piece.Synchronize(this.map);
        }
    }

    private void EnterStop()
    {
        this.headAnimated.PlayAndLoop(this.animationHeadCold);
        this.timer = TimeBetweenStopAndAsh;
    }

    private void EnterAsh()
    {
        this.timeToRestore = null;

        this.furnaceAnimated.PlayAndLoop(this.animationFurnaceIdle);
        this.headAnimated.gameObject.SetActive(false);
        this.lavaTopAnimated.PlayOnce(this.animationLavaTopHotToCold);
        this.lavaBottomAnimated.gameObject.SetActive(false);

        foreach (var piece in this.lavaFunnelPieces)
        {
            piece.PlayAndHoldLastFrame(this.animationLavaMiddleToAsh);
        }

        {
            var headParticle = Particle.CreateWithSprite(
                this.headSprite, 100, this.headTransform.position, this.transform.parent
            );
            headParticle.transform.localRotation = this.transform.localRotation;
            headParticle.Throw(new Vector2(0, 0.25f), 0.2f, 0.1f, new Vector2(0, -0.025f));

            headParticle.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                headParticle.spin *= -1;
        }

        RefreshHitboxes(solidHead: false, solidLava: true);
    }

    private void EnterRestore()
    {
    }

    private void RefreshHitboxes(bool solidHead, bool solidLava)
    {
        Vector2 headHitboxPos = this.transform.up * 0.05f;
        Vector2 bodyHitboxPos = -this.transform.up * 0.80f;
        
        Hitbox headHitbox = new Hitbox(
            headHitboxPos.x - 0.5f,
            headHitboxPos.x + 0.5f,
            headHitboxPos.y - 0.5f,
            headHitboxPos.y + 0.5f,
            0, solidHead ? Hitbox.Solid : Hitbox.NonSolid,
            wallSlideAllowedOnLeft: true,
            wallSlideAllowedOnRight: true
        );

        Hitbox bodyHitbox = new Hitbox(
            bodyHitboxPos.x - 0.5f,
            bodyHitboxPos.x + 0.5f,
            bodyHitboxPos.y - 0.5f,
            bodyHitboxPos.y + 0.5f,
            0, Hitbox.Solid,
            wallSlideAllowedOnLeft: true,
            wallSlideAllowedOnRight: true
        );

        if (solidLava == true)
        {
            var lavaHitbox = new Hitbox(
                -0.5f, 0.5f, 0, this.extendDistance,
                this.rotation, Hitbox.Solid, true, true
            );
            this.hitboxes = new[] { headHitbox, bodyHitbox, lavaHitbox };
        }
        else
        {
            this.hitboxes = new[] { headHitbox, bodyHitbox };
        }
    }

    private void CheckForCollisionPlayerWithLava()
    {
        var pos = this.position;
        var distance = this.extendDistance;
        var lavaRectangle = this.orientation switch
        {
            Orientation.Up   => new Rect(pos.x - 0.5f, pos.y, 1, distance),
            Orientation.Left => new Rect(pos.x - distance, pos.y - 0.5f, distance, 1),
            Orientation.Down => new Rect(pos.x - 0.5f, pos.y - distance, 1, distance),
            _                => new Rect(pos.x, pos.y - 0.5f, distance, 1),
        };

        if (this.map.player.Rectangle().Overlaps(lavaRectangle))
            this.map.player.Hit(this.map.player.position);
    }
}
