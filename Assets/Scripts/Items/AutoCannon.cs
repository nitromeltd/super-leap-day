using UnityEngine;

public class AutoCannon : Item
{
    public Animated.Animation fireAnimation;
    public SpinBlock controlledBySpinBlock;
    private int lastShotTime;
    private int delayedShotTime;
    private bool flipped;
    private float shootingDelayTime = 1;

    public override void Init(Def def)
    {
        base.Init(def);
        
        SetHitbox();
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        lastShotTime = this.map.frameNumber;
        this.flipped = def.tile.flip;

        if (def.tile.properties?.ContainsKey("shooting_delay") == true)
            this.shootingDelayTime = Mathf.Clamp(def.tile.properties["shooting_delay"].f, 0.5f, 10);
        else
            this.shootingDelayTime = 1f;
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if(this.controlledBySpinBlock == null)
        {
            if (this.map.frameNumber - this.lastShotTime >= this.shootingDelayTime * 60)
            {
                this.lastShotTime = this.map.frameNumber;
                Shoot();
            }
        }
        else
        {
            if(this.delayedShotTime > 0)
            {
                this.delayedShotTime--;
                if(this.delayedShotTime == 0)
                {
                    Shoot();
                }
            }
        }
    }

    public void DelayedShot(int numberOfFramesDelayed)
    {
        this.delayedShotTime = numberOfFramesDelayed;
    }

    private void Shoot()
    {
        Vector2 dir;
        var rot = this.rotation + 90;
        dir.x = Mathf.Cos(rot * Mathf.Deg2Rad);
        dir.y = Mathf.Sin(rot * Mathf.Deg2Rad);

        var bullet = AutoCannonBullet.Create(
            (Vector2)this.transform.position + (dir * 26 / 16f),
            this.chunk
        );
        bullet.velocity = dir * 0.125f;
    }

    private void SetHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, 0f, 1f, this.rotation, Hitbox.Solid, wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
        };
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox();
    }
}
