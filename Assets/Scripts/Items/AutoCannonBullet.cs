using UnityEngine;

public class AutoCannonBullet : Item
{
    public Vector2 velocity;
    public bool alive = true;
    [System.NonSerialized] public SpinBlock fromSpinBlock;

    public Animated.Animation animationNormal;

    public static AutoCannonBullet Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(
            Assets.instance.moltenFortress.autoCannonBullet, chunk.transform
        );
        obj.name = "Spawned AutoCannonBullet";
        var fireball = obj.GetComponent<AutoCannonBullet>();
        fireball.Init(position, chunk);
        chunk.items.Add(fireball);
        return fireball;
    }

    public override void Init(Def def) => throw new System.NotImplementedException();

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.animated.PlayAndLoop(this.animationNormal);
        Audio.instance.PlaySfx(
            Assets.instance.moltenFortress.sfxSpinBlockFire, null, this.position
        );

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }

    public override void Advance()
    {
        if (this.alive == false) return;

        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByEnemy: true,
            ignoreTileTopOnlyFlag: true,
            ignoreItem1: this.fromSpinBlock
        );
        var r = raycast.Arbitrary(
            this.position,
            this.velocity.normalized,
            this.velocity.magnitude
        );

        this.position += this.velocity;
        this.transform.position = this.position;
        this.transform.localRotation = Quaternion.Euler(
            0, 0, Mathf.Atan2(this.velocity.y, this.velocity.x) * 180 / Mathf.PI
        );

        if (r.anything == true)
        {
            this.alive = false;
            CreateExplosionParticles();
            Audio.instance.PlaySfx(
                Assets.instance.moltenFortress.sfxSpinBlockExplode, null, this.position
            );

            this.map.Damage(
                this.hitboxes[0].InWorldSpace().Inflate(0.6f),
                new Enemy.KillInfo { itemDeliveringKill = this },
                true
            );

            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
            return;
        }

        var thisRect = this.hitboxes[0].InWorldSpace();
        if (thisRect.Overlaps(this.map.player.Rectangle()))
        {
            this.map.player.Hit((Vector2)this.transform.position);
        }
    }

    private void CreateExplosionParticles()
    {
        Particle.CreateAndPlayOnce(
            Assets.instance.explosionAnimation,
            this.position,
            this.transform.parent
        );

        for (int i = 0; i < 30; i++)
        {
            var angleRadians = Random.Range(0, Mathf.PI * 2);
            Vector2 direction = new Vector2(
                Mathf.Cos(angleRadians),
                Mathf.Sin(angleRadians)
            );
            var particlePosition = this.position + (direction * Random.Range(0.8f, 0.9f));
            var particleVelocity = direction * Random.Range(0.02f, 0.03f);

            Particle trailParticle = Particle.CreateAndPlayOnce(
                Assets.instance.genericDustParticleAnimation,
                particlePosition,
                this.transform.parent
            );

            float s = Random.Range(0.8f, 1.4f);
            trailParticle.transform.localScale = new Vector3(s, s, 1);
            // trailParticle.ScaleOut(this.scaleOutCurve);
            trailParticle.animated.playbackSpeed = Random.Range(0.3f, 0.5f);
            trailParticle.velocity = particleVelocity;
            trailParticle.spriteRenderer.sortingLayerName = "Player Trail (BL)";
            // trailParticle.spriteRenderer.sortingOrder = -(1 + sortingOrderOffset + (this.map.frameNumber % 32));
        }
    }
}
