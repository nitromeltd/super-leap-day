
using UnityEngine;
using System;

public class Bamboo : Item
{
    public Sprite spriteTop;
    public Sprite[] spritesMiddle;
    public Sprite spriteBottom;
    public Sprite spriteSquiggleTop;
    public Sprite spriteSquiggleMiddle;
    public Sprite spriteSquiggleBottom;
    public Sprite[] leafSprites;
    public Animated.Animation leafAnimation1;
    public Animated.Animation leafAnimation2;

    [NonSerialized] public bool twisty;
    [NonSerialized] public bool twistyTop;
    [NonSerialized] public bool twistyBottom;
    [NonSerialized] public SpriteRenderer leftLeafSprite;
    [NonSerialized] public SpriteRenderer rightLeafSprite;

    public override void Init(Def def)
    {
        base.Init(def);

        var above = this.chunk.ItemAt<Bamboo>(def.tx, def.ty + 1);
        var below = this.chunk.ItemAt<Bamboo>(def.tx, def.ty - 1);
        this.twisty = def.tile.tile.Contains("twist");
        if (this.twisty == true && (above == null || below == null))
            this.chunk.ReportError(this.position, "Bamboo can't be twisty at ends");
        this.twistyTop =
            this.twisty == true && above?.def.tile.tile.Contains("twist") == false;
        this.twistyBottom =
            this.twisty == true && below?.def.tile.tile.Contains("twist") == false;

        if (above == null)
            this.spriteRenderer.sprite = this.spriteTop;
        else if (below == null)
            this.spriteRenderer.sprite = this.spriteBottom;
        else if (this.twistyTop == true)
            this.spriteRenderer.sprite = this.spriteSquiggleTop;
        else if (this.twistyBottom == true)
            this.spriteRenderer.sprite = this.spriteSquiggleBottom;
        else if (this.twisty == true)
            this.spriteRenderer.sprite = this.spriteSquiggleMiddle;
        else
            this.spriteRenderer.sprite = Util.RandomChoice(this.spritesMiddle);

        this.leftLeafSprite =
            this.transform.Find("leaf left").GetComponent<SpriteRenderer>();
        this.rightLeafSprite =
            this.transform.Find("leaf right").GetComponent<SpriteRenderer>();
        this.leftLeafSprite.sprite  = Util.RandomChoice(this.leafSprites);
        this.rightLeafSprite.sprite = Util.RandomChoice(this.leafSprites);

        if (this.twisty == true || UnityEngine.Random.Range(0f, 1f) >= 0.2f)
        {
            Destroy(this.leftLeafSprite);
            this.leftLeafSprite = null;
        }
        if (this.twisty == true || UnityEngine.Random.Range(0f, 1f) >= 0.2f)
        {
            Destroy(this.rightLeafSprite);
            this.rightLeafSprite = null;
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public static Bamboo BambooAt(Map map, float x, int ty)
    {
        // if the bamboo is offset and running down a tile boundary, then
        // ItemAt<Bamboo>(tx, ty) isn't a safe way to find it.

        int minTx = Mathf.FloorToInt(x - 0.5f);
        int maxTx = Mathf.FloorToInt(x + 0.5f);

        for (int tx = minTx; tx <= maxTx; tx += 1)
        {
            var bamboo = map.ItemAt<Bamboo>(tx, ty);
            if (bamboo == null) continue;
            if (bamboo.position.x < x - 0.5f) continue;
            if (bamboo.position.x > x + 0.5f) continue;
            return bamboo;
        }
        return null;
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        CheckForPlayerAttaching();

        var player = this.map.player;
        if (player.state == Player.State.SlidingOnBamboo &&
            Mathf.Abs(player.position.x - this.position.x) < 0.5f &&
            Mathf.Abs(player.position.y - this.position.y) < 1)
        {
            if (this.leftLeafSprite)
            {
                CreateLeafParticles(this.position.Add(-0.375f, 0), 1);
                Destroy(this.leftLeafSprite.gameObject);
                this.leftLeafSprite = null;
            }
            if (this.rightLeafSprite)
            {
                CreateLeafParticles(this.position.Add(0.375f, 0), 1);
                Destroy(this.rightLeafSprite.gameObject);
                this.rightLeafSprite = null;
            }

            if (this.twisty == true)
            {
                if (Mathf.Abs(player.velocity.y) > 1.0f / 16f)
                {
                    var target = Util.Sign(player.velocity.y) * 1.0f / 16;
                    player.velocity.y = Mathf.Lerp(player.velocity.y, target, 0.2f / 16);
                    player.velocity.y = Util.Slide(player.velocity.y, target, 0.2f / 16);
                }
            }
        }
    }

    private void CreateLeafParticles(Vector2 position, int count)
    {
        for (var n = 0; n < count; n += 1)
        {
            var p = Particle.CreatePlayAndLoop(
                UnityEngine.Random.Range(0, 1) > 0.5f ?
                    this.leafAnimation1 :
                    this.leafAnimation2,
                200,
                position,
                this.transform.parent
            );
            p.animated.frameNumber = UnityEngine.Random.Range(0, 18);
            p.velocity = new Vector2(
                UnityEngine.Random.Range(-0.3f,  0.3f) / 16,
                UnityEngine.Random.Range(-0.6f, -0.2f) / 16
            );
            p.acceleration = new Vector2(-0.01f, -0.01f) / 16;
        }
    }

    public void CheckForPlayerAttaching()
    {
        var player = this.map.player;
        if (player.CanAttachToBamboo == false) return;
        if (player.state != Player.State.Normal &&
            player.state != Player.State.Jump &&
            player.state != Player.State.DropVertically) return;
        if (player.groundState.onGround == true) return;
        if (Mathf.Abs(player.position.x - this.position.x) > 0.5f) return;
        if (Mathf.Abs(player.position.y - this.position.y) > 0.5f) return;

        player.GrabBamboo(this);
    }
}
