using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class BeachBall : Item
{
    public Animated.Animation popParticleAnimation;

    public Animated animatedJellyfish;
    public Animated.Animation jellyfishOnAnimation;
    public Animated.Animation jellyfishOffAnimation;

    [HideInInspector] public Vector2 velocity = Vector2.zero;
    private bool insideWater = false;
    private int ignorePlayerTimer = 0;
    [HideInInspector] public bool popped = false;
    private Vector2 originPosition = Vector2.zero;
    private float rotationSpeed = 0f;
    private float rotationDirection = 1f;
    private Pickup pickup;
    private int lastPlayerHitFrame;
    private Jellyfish jellyfish;
    private Transform shadingObject;
    private Transform shockObject;
    private Transform frontObject;
    [HideInInspector] public WaterCurrent insideWaterCurrent;
    [HideInInspector] public WaterCurrent.TrackingData waterCurrentTrackingData;

    private const int IgnorePlayerAfterHitTime = 10;
    private const float GravityAcceleration = 0.01f;//0.006f;
    private const float WaterBuoyancy = 0.02f;
    private const float Bounciness = -0.75f;
    private const float Drag = 0.005f;
    private const float MinimumHitSpeed = 0.20f;
    private const float MaximumHitSpeedX = 0.55f;
    private const float MaximumHitSpeedY = 0.40f;
    private const float HitForce = 1.50f;

    private static Vector2 HitboxGroundSize = new Vector2(1.2f, 1.2f);
    private static Vector2 HitboxPlayerSize = new Vector2(0.8f, 0.8f);

    private bool IsInsideWaterCurrent => this.insideWaterCurrent != null;
    private Vector2 BuoyancyPointOffset => new Vector2(0f, 0f);
    private Vector2 BuoyancyPointPosition => this.position - BuoyancyPointOffset;
    private bool HasPickup => this.pickup != null;
    private bool HasJellyfish => this.jellyfish != null;
    private Vector2 Velocity => this.insideWater ? this.velocity / 2f : this.velocity;
    public Rect HitboxPlayerRect => this.hitboxes[1].InWorldSpace();

    private bool hasPlayedHitSfx = false;
    private int flashTimer = 0;

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.originPosition = this.position;
        
        this.hitboxes = new [] {
            
            new Hitbox(
                -HitboxGroundSize.x,
                HitboxGroundSize.x,
                -HitboxGroundSize.y,
                HitboxGroundSize.y,
                this.rotation, Hitbox.NonSolid),
            
            new Hitbox(
                -HitboxPlayerSize.x,
                HitboxPlayerSize.x,
                -HitboxPlayerSize.y,
                HitboxPlayerSize.y,
                this.rotation, Hitbox.NonSolid),

        };

        this.shadingObject = this.transform.Find("Shading");
        this.frontObject = this.transform.Find("Front");
        this.shockObject = this.transform.Find("Shock");
        this.jellyfish = this.chunk.NearestItemTo<Jellyfish>(this.position, 1f);
        
        if(HasJellyfish == true)
        {
            this.jellyfish.EnterBeachBallState();
            this.animatedJellyfish.gameObject.SetActive(true);
            this.animatedJellyfish.PlayAndLoop(this.jellyfishOffAnimation);
        }
        else
        {
            this.pickup = this.chunk.NearestItemTo<Pickup>(this.position, 1f);
            ControlPickup();
        }
    }

    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        if(this.popped == true) return;

        if(IsInsideWaterCurrent == true)
        {
            AdvanceInsideWaterCurrent();
            return;
        }

        AdvanceWater();
        CheckHitPlayer();
        ApplyDragOnFloor();

        Integrate();
        CheckForOtherBalls();

        this.transform.position = this.position;

        this.rotationSpeed = Mathf.Abs(Velocity.x) * 35f * this.rotationDirection;
        this.rotation += Mathf.RoundToInt(rotationSpeed);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotation);
        this.shadingObject.rotation = Quaternion.Euler(Vector3.zero);
        if(HasJellyfish == true)
        {
            this.jellyfish.AdvanceInsideBeachBall(this);

            if(this.jellyfish.IsElectricityActive == true &&
                this.animatedJellyfish.currentAnimation == this.jellyfishOffAnimation)
            {
                this.animatedJellyfish.PlayAndLoop(this.jellyfishOnAnimation);
                this.shockObject.gameObject.SetActive(true);
                this.frontObject.gameObject.SetActive(false);
            }

            if (this.jellyfish.IsElectricityActive == false &&
                this.animatedJellyfish.currentAnimation == this.jellyfishOnAnimation)
            {
                this.animatedJellyfish.PlayAndLoop(this.jellyfishOffAnimation);
                this.shockObject.gameObject.SetActive(false);
                this.frontObject.gameObject.SetActive(true);
            }

            if(this.jellyfish.IsElectricityActive == true)
            {
                flashTimer++;
                if (flashTimer > 4)
                {
                    this.shockObject.gameObject.SetActive(!this.shockObject.gameObject.activeSelf);
                    this.frontObject.gameObject.SetActive(!this.frontObject.gameObject.activeSelf);
                    flashTimer = 0;
                }
            }
        }
        else if(HasPickup == true)
        {
            this.pickup.position = PickupPositionInsideBall();
            //this.pickup.rotation = this.rotation;
            this.pickup.transform.rotation = this.transform.rotation;
        }
    }

    private Vector2 PickupPositionInsideBall()
    {
        if(this.pickup is BubblePickup)
        {
            return this.position + new Vector2(-0.12f, -0.02f);
        }
        
        return this.position;
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > BuoyancyPointPosition.y)
        {
            // enter water
            this.insideWater = true;

            // makes the player hitting the ball close to the surface more consistent
            if(this.map.frameNumber > lastPlayerHitFrame + 2)
            {
                this.velocity *= 0.50f;
            }
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < BuoyancyPointPosition.y)
        {
            // exit water
            this.insideWater = false;

            // makes the player hitting the ball close to the surface more consistent
            if(this.map.frameNumber > lastPlayerHitFrame + 2)
            {
                this.velocity *= 0.50f;
            }
        }

        if(this.insideWater == true)
        {
            this.velocity += Vector2.up * WaterBuoyancy;
        }
        else
        {
            this.velocity += Vector2.down * GravityAcceleration;
        }
    }
    
    private void CheckHitPlayer()
    {
        if(this.ignorePlayerTimer > 0)
        {
            this.ignorePlayerTimer -= 1;
            return;
        }

        Player player = this.map.player;

        if(player.Rectangle().Overlaps(HitboxPlayerRect))
        {
            this.ignorePlayerTimer = IgnorePlayerAfterHitTime;
            this.lastPlayerHitFrame = this.map.frameNumber;

            Vector2 hitVelocity = new Vector2(
                player.velocity.x * HitForce,
                player.velocity.y * HitForce
            );

            float xClamped = Mathf.Clamp(Mathf.Abs(hitVelocity.x), MinimumHitSpeed, MaximumHitSpeedX);
            float yClamped = Mathf.Clamp(Mathf.Abs(hitVelocity.y), MinimumHitSpeed, MaximumHitSpeedY);

            hitVelocity.x = xClamped * Mathf.Sign(hitVelocity.x);
            hitVelocity.y = yClamped * Mathf.Sign(hitVelocity.y);
            
            if(Mathf.Abs(hitVelocity.x) == MinimumHitSpeed)
            {
                hitVelocity.x = MinimumHitSpeed * (this.position.x > player.position.x ? 1f : -1f);
            }

            if(Mathf.Abs(hitVelocity.y) == MinimumHitSpeed)
            {
                hitVelocity.y = MinimumHitSpeed * (this.position.y > player.position.y ? 1f : -1f);
            }

            this.velocity = hitVelocity;
            this.rotationDirection = Mathf.Sign(this.velocity.x);


            var hitLocation = player.position + 0.75f * (this.position - player.position);
            var p = Particle.CreatePlayAndHoldLastFrame(
                Assets.instance.enemyHitPowAnimation,
                8,
                hitLocation,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 1;

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvBeachBallBounce,
                position: this.position
            );
        }
    }

    private void Integrate()
    {
        var raycast = new Raycast(
            this.map,
            this.position,
            ignoreItemType: typeof(SnowBlock), 
            isCastByEnemy: true//, ignoreTileTopOnlyFlag: true
        );

        var rect = this.hitboxes[0];
        var start = this.position;

        if (Velocity.x < 0)
        {
            var r = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(rect.xMin, -HitboxGroundSize.y / 2f), false, -Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMin, -HitboxGroundSize.y / 4f), false, -Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMin, 0f), false, -Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMin, HitboxGroundSize.y / 4f), false, -Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMin, HitboxGroundSize.y / 2f), false, -Velocity.x)
            );

            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity.x *= Bounciness;

                if(this.hasPlayedHitSfx == false)
                {
                    this.hasPlayedHitSfx = true;
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvBeachBallBounce,
                        position: this.position
                    );
                }
            }
            else
            {
                this.position.x += Velocity.x;
                this.hasPlayedHitSfx = false;
            }
        }
        else
        {
            var r = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(rect.xMax, -HitboxGroundSize.y / 2f), true, Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMax, -HitboxGroundSize.y / 4f), true, Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMax, 0f), true, Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMax, HitboxGroundSize.y / 4f), true, Velocity.x),
                raycast.Horizontal(start + new Vector2(rect.xMax, HitboxGroundSize.y / 2f), true, Velocity.x)
            );

            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity.x *= Bounciness;
            }
            else
            {
                this.position.x += Velocity.x;
            }
        }

        if (Velocity.y < 0)
        {
            var r = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxGroundSize.x / 2f, rect.yMin), false, -Velocity.y),
                raycast.Vertical(start + new Vector2(-HitboxGroundSize.x / 4f, rect.yMin), false, -Velocity.y),
                raycast.Vertical(start + new Vector2(0, rect.yMin), false, -Velocity.y),
                raycast.Vertical(start + new Vector2(HitboxGroundSize.x / 4f, rect.yMin), false, -Velocity.y),
                raycast.Vertical(start + new Vector2(HitboxGroundSize.x / 2f, rect.yMin), false, -Velocity.y)
            );

            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.velocity.y *= Bounciness;
            }
            else
            {
                this.position.y += Velocity.y;
                this.hasPlayedHitSfx = false;
            }
        }
        else
        {
            var r = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxGroundSize.x / 2f, rect.yMax), true, Velocity.y),
                raycast.Vertical(start + new Vector2(-HitboxGroundSize.x / 4f, rect.yMax), true, Velocity.y),
                raycast.Vertical(start + new Vector2(0, rect.yMax), true, Velocity.y),
                raycast.Vertical(start + new Vector2(HitboxGroundSize.x / 4f, rect.yMax), true, Velocity.y),
                raycast.Vertical(start + new Vector2(HitboxGroundSize.x / 2f, rect.yMax), true, Velocity.y)
            );

            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity.y *= Bounciness;
            }
            else
            {
                this.position.y += Velocity.y;
                this.hasPlayedHitSfx = false;
            }
        }
    }

    private void CheckForOtherBalls()
    {
        if(this.velocity.magnitude < 0.1f) return;

        foreach (var beachBall in this.chunk.subsetOfItemsOfTypeBeachBall)
        {
            if(beachBall == this) continue;
            if(beachBall.popped == true) continue;

            if(this.hitboxes[1].InWorldSpace().Overlaps(beachBall.hitboxes[1].InWorldSpace()))
            {
                Vector2 directionToOtherBall = (beachBall.position - this.position).normalized;
                Vector2 pushForce = directionToOtherBall * (this.velocity.magnitude * 1f);
                
                Push(-pushForce);
                (beachBall as BeachBall).Push(pushForce);
            }
        }
    }

    public void Push(Vector2 forcePush)
    {
        this.velocity = forcePush;
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBeachBallNonPlayer,
            position: this.position,
            options: new Audio.Options(.5f, true, 0)
        );
    }

    private void ApplyDragOnFloor()
    {
        var rFloor = new Raycast(this.map, this.position).Vertical(
            this.position + Vector2.down * HitboxGroundSize, false, 0.10f
        );

        if(rFloor.anything == true)
        {
            this.velocity.x = Util.Slide(this.velocity.x, 0f, Drag);
        }
    }
    
    public void Pop(SpikeTile spikeTile)
    {
        if(this.popped == true) return;
        this.popped = true;

        this.gameObject.SetActive(false);

        if(HasJellyfish == true)
        {
            this.jellyfish.ExitBeachBallState();
            this.animatedJellyfish.gameObject.SetActive(false);
        }
        else if(HasPickup == true)
        {
            this.pickup.velocity = Velocity * 0.50f;
            ReleasePickup();
        }

        Particle.CreateAndPlayOnce(this.popParticleAnimation, this.position, this.map.transform);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBeachBallPop,
            position: this.position
        );
    }

    public override void Reset()
    {
        if(HasJellyfish == true)
        {
            this.jellyfish.EnterBeachBallState();
            this.animatedJellyfish.gameObject.SetActive(true);
            this.animatedJellyfish.PlayAndLoop(this.jellyfishOffAnimation);
            this.shockObject.gameObject.SetActive(false);
            this.frontObject.gameObject.SetActive(true);
        }
        else
        {
            ControlPickup();
        }

        this.position = this.originPosition;
        this.transform.position = this.position;
        this.popped = false;
        this.rotation = 0;
        this.rotationSpeed = 0f;
        this.velocity = Vector2.zero;
        this.insideWater = false;
        this.gameObject.SetActive(true);
        RefreshHitboxes();
    }

    private void ControlPickup()
    {
        if(HasPickup == false) return;

        this.pickup.NotifyControlledByOtherItem();
        this.pickup.canPlayerCollect = false;
        this.pickup.movingFreely = false;
        this.pickup.velocity = Vector2.zero;
        this.pickup.ChangeSorting("Items", 3);
    }

    private void ReleasePickup()
    {
        if(HasPickup == false) return;
        
        this.pickup.StopControlledByOtherItem();
        this.pickup.canPlayerCollect = true;
        //this.pickup.movingFreely = true;
        this.pickup.rotation = 0;
        this.pickup.transform.rotation = Quaternion.identity;
        this.pickup.ChangeSorting("Pickups", 0);
        this.pickup.Collect();
    }

    private static Vector2 OutOfCurrentForce = new Vector2(0.15f, 0.35f);

    private void AdvanceInsideWaterCurrent()
    {
        this.insideWaterCurrent.AdvanceBeachBall(this);
        Integrate();
        CheckForOtherBalls();
        
        Player player = this.map.player;

        if(player.Rectangle().Overlaps(HitboxPlayerRect))
        {
            bool movingHorizontally = Mathf.Abs(this.velocity.x) > Mathf.Abs(this.velocity.y);
            bool isPlayerToTheLeft = this.position.x > player.position.x;
            bool isPlayerUpwards = this.position.y > player.position.y;

            Vector2 forward;

            if(movingHorizontally == true)
            {
                forward.x = OutOfCurrentForce.x * (isPlayerToTheLeft ? 1f : -1f);
                forward.y = OutOfCurrentForce.y;
            }
            else
            {
                forward.x = OutOfCurrentForce.y * (isPlayerToTheLeft ? 1f : -1f);
                forward.y = 0f;
            }

            this.insideWaterCurrent.ThingExitWaterCurrent(this.waterCurrentTrackingData, forward);
        }

        this.transform.position = this.position;

        this.rotation += 5;
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotation);
    }

    public void EnterWaterCurrent(WaterCurrent waterCurrent)
    {
        if (this.popped == true) return;

        this.insideWaterCurrent = waterCurrent;
    }

    public void NotifyExitWaterCurrent()
    {
        this.insideWaterCurrent = null;
    }
    public bool IsPopped()
    {
        return this.popped;
    }
}
