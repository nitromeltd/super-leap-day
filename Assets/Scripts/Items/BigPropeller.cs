using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigPropeller : Item
{
    public int width;
    public Material airMaterial;

    private TileSection[] tileSections;
    private bool on = false; 
    private Button[] buttons;
    private Vector2 originPosition;
    private Vector2 shakePosition;

    private static Vector2 ShakeAmount = Vector2.one * 0.02f;
    private const float ShakeSpeed = 0.80f;

    private Vector2 Direction =>
        this.rotation switch
        {
            0   => Vector2.up,
            90  => Vector2.right,
            180 => Vector2.down,
            270 => Vector2.left,
            _   => Vector2.up
        };

    private static Vector2 HitboxPos = new Vector2(0f, 0f);
    private static Vector2 HitboxSize = new Vector2(2.50f, 0.50f);

    [System.Serializable] public struct TileSection
    {
        public GameObject textureGO;
        public Vector2 tilePosition;
        public MeshRenderer tileMeshRenderer;
        public MaterialPropertyBlock tilePropertyBlock;

        public TileSection(GameObject gameObject, Vector2 tilePosition, Material material)
        {
            this.textureGO = gameObject;
            this.tilePosition = tilePosition;
            this.tileMeshRenderer = this.textureGO.GetComponent<MeshRenderer>();
            this.tileMeshRenderer.material = material;
            this.tilePropertyBlock = new MaterialPropertyBlock();

            Destroy(this.textureGO.GetComponent<MeshCollider>());
        }
        
        public void RefreshRenderer(float distance)
        {
            float positionY = 1f + (distance - 1) * 0.5f;
            float textureTilingY = positionY;

            this.textureGO.transform.localPosition =
                new Vector2(this.tilePosition.x, positionY);
            this.textureGO.transform.localScale = new Vector3(1f, distance, 1f);

            //this.currentSmokeOffsetY += SmokeOffsetSpeed;
            Vector2 textureScale = new Vector2(1f, textureTilingY);
            Vector4 st = new Vector4(textureScale.x, textureScale.y, 0f, 0f);
            
            this.tilePropertyBlock.SetVector("_MainTex_ST", st);
            this.tileMeshRenderer.SetPropertyBlock(this.tilePropertyBlock);
        }

        public void HideRenderer()
        {
            this.tilePropertyBlock.SetVector("_MainTex_ST", Vector3.zero);
            this.tileMeshRenderer.SetPropertyBlock(this.tilePropertyBlock);
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        CreateTiles();

        this.originPosition = this.position;

        this.hitboxes = new Hitbox[]
        {
            new Hitbox(
                HitboxPos.x - HitboxSize.x,
                HitboxPos.x + HitboxSize.x,
                HitboxPos.y - HitboxSize.y,
                HitboxPos.y + HitboxSize.y,
                0, Hitbox.Solid),
        };

        var buttons = new List<Button>();
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                var button = this.chunk.NearestItemTo<Button>(otherNode.Position);
                buttons.Add(button);
            }
        }
        this.buttons = buttons.ToArray();

        this.on = (def.tile.properties?.ContainsKey("on") == true) ?
            def.tile.properties["on"].b : false;
    }

    private void CreateTiles()
    {
        this.tileSections = new TileSection[this.width];
        Vector2 initialTilePosition = new Vector2((-(float)this.width / 2f) + 0.50f, 0f);

        for (int i = 0; i < this.tileSections.Length; i++)
        {
            GameObject go = GameObject.CreatePrimitive(PrimitiveType.Quad);
            go.name = $"Tile Section {i}";
            go.transform.SetParent(this.transform);

            Vector2 tilePosition = initialTilePosition + (Vector2.right * i);
            this.tileSections[i] = new TileSection(go, tilePosition, this.airMaterial);
        }
    }

    public override void Advance()
    {
        base.Advance();
        
        var pressed = false;
        foreach (var button in this.buttons)
        {
            if (button.PressedThisFrame == true)
                pressed = true;
        }

        if (pressed == true)
        {
            ButtonPressed();
        }
        
        foreach(var t in this.tileSections)
        {
            CheckTilePush(t);
        }

        if(this.on == true)
        {
            Shake();
        }

        this.transform.position = this.position + this.shakePosition;
    }

    private void ButtonPressed()
    {
        ToggleOn(!this.on);
    }

    private void CheckTilePush(TileSection tileSection)
    {
        if(this.on == false) return;

        Vector2 startPosition = this.position + tileSection.tilePosition + (Direction * 0.50f);

        Raycast raycast = new Raycast(
            this.map,
            startPosition,
            ignoreChunksOutsideRange: 20f,
            ignoreItem1: this,
            ignoreTileTopOnlyFlag: true
        );
            
        var detectSolid = raycast.Arbitrary(
            startPosition, Direction, 100f
        );
        
        float solidDistance = detectSolid.distance;
        float distanceToChunkTop = (this.chunk.yMax + 1) - startPosition.y;

        if(solidDistance > distanceToChunkTop)
        {
            solidDistance = distanceToChunkTop;
        }

        tileSection.RefreshRenderer(solidDistance);

        Vector2 rectangleOrigin = startPosition - new Vector2(0.50f, 0.50f);
        Rect pushRect = new Rect(rectangleOrigin.x, rectangleOrigin.y, 1f, solidDistance);
        
        Player player = Map.instance.player;
        if(player.Rectangle().Overlaps(pushRect) && player.ShouldCollideWithItems())
        {
            PushPlayer();
        }
    }

    private void PushPlayer()
    {
        Map.instance.player.PushFromPropeller(Direction);
    }

    private void Shake()
    {
        Vector2 shake = 
            Mathf.Sin(Map.instance.frameNumber * ShakeSpeed) * ShakeAmount;

        float shakeX = Random.Range(0.50f, 1f) * shake.x;
        float shakeY = Random.Range(0.50f, 1f) * shake.y;
        this.shakePosition = new Vector2(shakeX, shakeY);
    }

    public void ToggleOn(bool on)
    {
        this.on = on;

        foreach(var t in this.tileSections)
        {
            t.HideRenderer();
        }
    }
}
