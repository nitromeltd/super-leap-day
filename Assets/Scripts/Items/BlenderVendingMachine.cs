using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class BlenderVendingMachine : Item
{
    public Animated.Animation animationBlenderBase;
    public Animated.Animation animationSparkle;
    public Animated.Animation animationBlade;
    public Animated.Animation animationJuiceEmpty;
    public Animated.Animation animationJuiceBlendSmall;
    public Animated.Animation animationJuiceRise;
    public Animated.Animation animationJuiceBlendBig;
    public Animated.Animation animationJuiceSteady;
    public Animated.Animation animationButtonUp;
    public Animated.Animation animationButtonDown;

    private Animated blenderBase;
    private SpriteRenderer arrowDown;
    private Animated blade;
    private Animated juice;
    private Animated button;
    private TextMeshPro fruitCounterText;

    private bool active;
    private float arrowDistance;
    private float arrowAlpha;
    private bool buttonPressed;
    private MaterialPropertyBlock propertyBlock;
    private int fruitCost;

    public readonly Vector2 arrowPosition = new Vector2(0, 2.5f);

    public override void Init(Def def)
    {
        base.Init(def);

        int blendersBeforeThis = 0;
        for (int n = 0; n < this.chunk.index; n += 1)
        {
            if (this.map.chunks[n].AnyItem<BlenderVendingMachine>() != null)
                blendersBeforeThis += 1;
        }
        this.fruitCost = blendersBeforeThis switch
        {
            0 => 20,
            1 => 35,
            _ => 50
        };

        this.active = false;

        this.buttonPressed = false;
        this.blenderBase = this.transform.Find("blender base").GetComponent<Animated>();
        this.arrowDown = this.transform.Find("arrow down").GetComponent<SpriteRenderer>();
        this.blade = this.transform.Find("blade").GetComponent<Animated>();
        this.button = this.transform.Find("button").GetComponent<Animated>();
        this.juice = this.transform.Find("juice").GetComponent<Animated>();
        this.fruitCounterText = this.transform.Find("fruits text").GetComponent<TextMeshPro>();

        this.blade.PlayAndLoop(this.animationBlade);
        this.blenderBase.PlayAndLoop(this.animationBlenderBase);
        this.juice.PlayOnce(this.animationJuiceEmpty);

        this.propertyBlock = new MaterialPropertyBlock();

        this.fruitCounterText.text = this.fruitCost.ToString();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.hitboxes = new Hitbox[]
        {
            // blender base hitbox
            new Hitbox(-0.9f, 0.9f, 0f, 2.25f, 0, Hitbox.Solid),
            // button hitbox
            new Hitbox(-0.75f, 0.75f, 2.2f, 2.7f, 0, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if (IsBeingPressed() && this.buttonPressed == false)
        {
            Hit();
        }
        AdvanceArrows((this.position - this.map.player.position).sqrMagnitude < 10 * 10);
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[1].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;
        return false;
    }

    private void AdvanceArrows(bool isPlayerWithinRange)
    {
        bool showArrows =
            this.active == false &&
            this.map.NearestChunkTo(this.map.player.position) == this.chunk &&
            isPlayerWithinRange == true &&
            this.map.fruitCurrent >= this.fruitCost;

        if (this.arrowAlpha == 0 && showArrows == true)
            this.arrowDistance = 5;
        else
            this.arrowDistance = Util.Slide(this.arrowDistance, 1.2f, 0.5f);

        if (this.active == true)
            this.arrowAlpha = 0;
        else
            this.arrowAlpha = Util.Slide(this.arrowAlpha, showArrows ? 1 : 0, 0.06f);

        this.arrowDown.gameObject.SetActive(this.arrowAlpha > 0);

        if (this.arrowAlpha > 0)
        {
            Color c = new Color(1, 1, 1, this.arrowAlpha);
            this.arrowDown.color = c;

            float s =
                this.arrowDistance +
                Mathf.Sin(this.map.frameNumber * 0.1f) * 0.12f;
            this.arrowDown.transform.localPosition = arrowPosition.Add(0, +s);
        }
    }

    private void Hit()
    {
        if (this.active == false)
        {
            if (this.map.fruitCurrent >= this.fruitCost)
            {
                Activate();
                this.buttonPressed = true;
                this.button.PlayOnce(this.animationButtonDown);
                Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);
            }
            else
            {
                IngameUI.instance.FlashFruitCounter(0.30f, Color.red, this.map);
                this.button.PlayOnce(this.animationButtonDown, () =>
                {
                    this.button.PlayOnce(this.animationButtonUp);
                    Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);
                });
            }
        }
    }

    public void Activate()
    {
        this.active = true;
        this.map.RemoveFruit(this.fruitCost);

        // effect to move fruits from UI to the blender position
        IngameUI.instance.uiFruit.MoveFruit(
            this.map, 20, this.juice.transform, this.fruitCounterText
        );
        IngameUI.instance.FlashFruitCounter(1f, Color.black, this.map);

        StartCoroutine(TintFlashCoroutine());
        StartCoroutine(ActivateCoroutine());
    }

    private IEnumerator ActivateCoroutine()
    {
        yield return new WaitForSeconds(.5f);
        this.juice.PlayOnce(this.animationJuiceBlendSmall, () =>
        {
            this.juice.PlayOnce(this.animationJuiceRise, () =>
            {
                this.juice.PlayOnce(this.animationJuiceBlendBig, () =>
                {
                    this.juice.PlayAndLoop(this.animationJuiceSteady);
                });
            });
        });

        yield return new WaitForSeconds(2.5f);

        StartCoroutine(TintFlashCoroutine());
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .4f);

        yield return new WaitForSeconds(.1f);

        PowerupPickup powerupSlot = PowerupPickup.CreateRandomPowerup(
            this.position + new Vector2(0, 1) + Vector2.up * 1.52f, this.chunk);
        powerupSlot.isInBlender = true;
        powerupSlot.gameObject.GetComponent<SpriteRenderer>().sortingOrder = -7;
        powerupSlot.gameObject.transform.Find("Icon").GetComponent<SpriteRenderer>().sortingOrder = -6;
    }

    private IEnumerator TintFlashCoroutine()
    {
        Tint(Color.black, 1);
        this.fruitCounterText.outlineColor = Color.black;

        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(new Color32(179, 94, 121, 255), 1);
        this.fruitCounterText.outlineColor = new Color32(179, 94, 121, 255);

        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 1);
        this.fruitCounterText.outlineColor = Color.white;

        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 0);
        this.fruitCounterText.outlineColor = Color.black;
    }

    private void Tint(Color color, float intensity)
    {
        void TintSprite(SpriteRenderer sr)
        {
            sr.GetPropertyBlock(this.propertyBlock);
            this.propertyBlock.SetColor("_Color", color);
            this.propertyBlock.SetFloat("_Intensity", intensity);
            sr.SetPropertyBlock(this.propertyBlock);
        }

        TintSprite(this.blenderBase.GetComponent<SpriteRenderer>());
        TintSprite(this.blade.GetComponent<SpriteRenderer>());
        TintSprite(this.juice.GetComponent<SpriteRenderer>());
        TintSprite(this.button.GetComponent<SpriteRenderer>());
        this.fruitCounterText.color = color;
    }
}
