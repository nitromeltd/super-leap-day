using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class BonusLift : Item
{
    [Header("Exterior")]
    public Animated exteriorAnimated;
    public Animated.Animation animationExteriorIdle;
    public Animated.Animation animationExteriorOpen;
    public Animated.Animation animationExteriorClose;
    public Animated.Animation animationExteriorVibrate;
    public AnimationCurve animVibrateSpeedCurve;
    public Animated.Animation animationDustLandParticle;

    [Header("Button")]
    public Animated buttonAnimated;
    public Animated.Animation animationButtonIdle;
    public Animated.Animation animationButtonPress;
    public Animated.Animation animationButtonPressed;
    public Animated.Animation animationButtonHide;
    
    [Header("Arrow")]
    public Animated arrowAnimated;
    public Animated.Animation animationArrowBobbing;

    public SpriteRenderer pictureSR;
    public Sprite golfPicture;
    public Sprite pinballPicture;
    public Sprite racingPicture;
    public Sprite fishingPicture;
    public Sprite rollingPicture;
    public Sprite abseilingPicture;


    private minigame.Minigame.Type minigameType;
    private bool willAllowPlayerToEnter = true;
    private bool buttonPressed = false;
    private bool doorsOpened = false;
    private bool isGoingAway = false;
    private bool reachFinalPosition = false;
    private bool emerging = false;
    private bool disableLift = false;
    private int goingAwayMovingTimer;

    private Vector2 positionWhenRaised;
    private Vector2 PositionWhenLowered => this.positionWhenRaised + GoAwayOffset;
    private float yOffsetFromRaisedPosition;

    private Transform spriteMask;
    private Transform sprites;
    private const int GoingAwayMovingTime = 60;
    private static Vector2 GoAwayOffset = new Vector2(0f, -4.50f);

    private Rect ButtonRect => this.hitboxes[0].InWorldSpace();
    private Rect EntranceRect => this.hitboxes[1].InWorldSpace();

    [NonSerialized] public Layer layer = Layer.A;
    private string SortingLayerForDust() =>
        (this.layer == Layer.A) ? "Player Dust (AL)" : "Player Dust (BL)";

    public Map.DetailsToPersist detailsToPersist;


    public override void Init(Def def)
    {
        base.Init(def);
        this.detailsToPersist = null;
        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);
        this.buttonAnimated.PlayOnce(this.animationButtonIdle);
        this.arrowAnimated.PlayAndLoop(this.animationArrowBobbing);

        {
            var rnd = new System.Random(
                (Game.selectedDate.DayOfYear * 100) + this.chunk.index
            );

            var candidates = new List<minigame.Minigame.Type>();
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigameGolfBeginner))
                candidates.Add(minigame.Minigame.Type.Golf);
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigameRacingBeginner))
                candidates.Add(minigame.Minigame.Type.Racing);
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigamePinballBeginner))
                candidates.Add(minigame.Minigame.Type.Pinball);
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigameFishingBeginner))
                candidates.Add(minigame.Minigame.Type.Fishing);
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigameRollingBeginner))
                candidates.Add(minigame.Minigame.Type.Rolling);
            if (SaveData.IsItemPurchased(SaveData.ShopItem.MinigameAbseilingBeginner))
                candidates.Add(minigame.Minigame.Type.Abseiling);

            if (candidates.Count > 0)
                this.minigameType = candidates[rnd.Next() % candidates.Count];
            else
                this.minigameType = minigame.Minigame.Type.Golf;

            this.pictureSR.sprite = this.minigameType switch
            {
                minigame.Minigame.Type.Golf      => this.golfPicture,
                minigame.Minigame.Type.Racing    => this.racingPicture,
                minigame.Minigame.Type.Fishing   => this.fishingPicture,
                minigame.Minigame.Type.Rolling   => this.rollingPicture,
                minigame.Minigame.Type.Abseiling => this.abseilingPicture,
                _                                => this.pinballPicture,
            };
        }

        Vector2 buttonPosition = this.buttonAnimated.transform.localPosition;
        Vector2 buttonHitboxSize = new Vector2(0.50f, 1f);

        this.hitboxes = new [] {

            // button
            new Hitbox(
                buttonPosition.x - buttonHitboxSize.x,
                buttonPosition.x + buttonHitboxSize.x,
                buttonPosition.y + 0.1f,
                buttonPosition.y + buttonHitboxSize.y,
                this.rotation, Hitbox.NonSolid),

            // entrance
            new Hitbox(-0.20f, 0.20f, 0f, 0.50f, this.rotation, Hitbox.NonSolid),

            // solid top under button
            new Hitbox(-0.75f, 0.75f, 3.75f, 4.4f, this.rotation, Hitbox.Solid),
        };

        this.spriteMask = this.transform.Find("Lift Sprite Mask").GetComponent<Transform>();
        this.sprites = this.transform.Find("Lift Sprites").GetComponent<Transform>();

        this.positionWhenRaised = this.position;
        this.spriteMask.position = this.position + GoAwayOffset;

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if(Multiplayer.IsMultiplayerGame())
        {
            // Create Chest with Power-up in place of the Bonus Lift in multiplayer games
            Chest chest = this.chunk.AnyItem<Chest>();
            bool hasPowerupChest = false;

            if(chest != null)
            {
                hasPowerupChest = true;
            }

            if(hasPowerupChest == false)
            {
                Chest.CreateChestWithPowerup(this.position, this.chunk);
            }
            
            // Bonus Lift should not be shown or interactable in multiplayer games
            this.gameObject.SetActive(false);
            this.hitboxes = new Hitbox[0];
            return;
        }

        if (this.group != null)
        {
            this.positionWhenRaised = this.group.FollowerPosition(this);
        }

        Player player = this.map.player;

        if (player.Rectangle().Overlaps(ButtonRect) &&
            this.willAllowPlayerToEnter == true)
        {
            PressButton();
        }

        if(player.Rectangle().Overlaps(EntranceRect) &&
            player.state == Player.State.Normal &&
            this.willAllowPlayerToEnter == true)
        {
            EnterPlayer();   
        }

        if(player.state == Player.State.InsideLift && player.InsideBonusLift == this)
        {
            player.position = this.sprites.position + new Vector3(0f, 0.95f, 0f);
        }

        if(this.goingAwayMovingTimer > 0)
        {
            this.goingAwayMovingTimer -= 1;

            float t = Mathf.InverseLerp(GoingAwayMovingTime, 0, this.goingAwayMovingTimer);
            this.exteriorAnimated.playbackSpeed =
                Mathf.Lerp(0f, 1f, this.animVibrateSpeedCurve.Evaluate(t));
        }

        if(this.reachFinalPosition == false)
        {
            if(this.isGoingAway == true)
            {
                if(this.goingAwayMovingTimer == 0)
                {
                    this.yOffsetFromRaisedPosition = Util.Slide(
                        this.yOffsetFromRaisedPosition, GoAwayOffset.y, 0.05f
                    );
                    if (this.yOffsetFromRaisedPosition == GoAwayOffset.y)
                    {
                        ReachFinalPosition();
                    }
                }

                SpawnDustParticles();
            }
        }
        else if(this.emerging == true)
        {
            this.yOffsetFromRaisedPosition = Util.Slide(
                this.yOffsetFromRaisedPosition, 0, 0.05f
            );
            if (this.yOffsetFromRaisedPosition == 0)
            {
                ReachInitialPosition();
            }

            SpawnDustParticles();
        }

        this.transform.position = this.position =
            this.positionWhenRaised.Add(0, this.yOffsetFromRaisedPosition);

        this.spriteMask.position = this.positionWhenRaised;
        this.sprites.position = this.position;
    }

    private void PressButton()
    {
        if(this.buttonPressed == true) return;
        this.buttonPressed = true;

        this.buttonAnimated.PlayOnce(this.animationButtonPress, OpenDoors);
        Audio.instance.PlaySfx(Assets.instance.sfxButtonPress);
    }

    private void OpenDoors()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleOpen);
        this.exteriorAnimated.PlayOnce(this.animationExteriorOpen, () => 
        {
            this.doorsOpened = true;
            this.arrowAnimated.gameObject.SetActive(true);
        });
    }

    private void EnterPlayer()
    {
        if(this.doorsOpened == false) return;

        this.doorsOpened = false;
        Player player = this.map.player;
        player.state = Player.State.InsideLift;
        player.InsideBonusLift = this;
        this.arrowAnimated.gameObject.SetActive(false);
        player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        if (player is PlayerKing)
        {
            (player as PlayerKing).hatSpriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleClose);
        this.exteriorAnimated.PlayOnce(this.animationExteriorClose, GoAway);
    }

    private void GoAway()
    {
        if(this.isGoingAway == true) return;

        this.isGoingAway = true;
        this.goingAwayMovingTimer = GoingAwayMovingTime;

        this.exteriorAnimated.PlayAndLoop(this.animationExteriorVibrate);
        this.buttonAnimated.PlayAndHoldLastFrame(this.animationButtonHide);
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleDig);
    }

    private void ReachFinalPosition()
    {
        if (this.reachFinalPosition == true) return;
        this.reachFinalPosition = true;

        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);
        Player player = this.map.player;


        if (player.state == Player.State.InsideLift)
        {
            var currentlyActivePowerups = new List<PowerupPickup.Type>();
            int yellowPetHearts = 0;
            int tornadoPetHearts = 0;
            int shields = 0;
            int swordAndShields = 0;

            if (this.map.player.petYellow.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.PetYellow);
                yellowPetHearts = this.map.player.petYellow.hearts;
            }

            if (this.map.player.petTornado.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.PetTornado);
                tornadoPetHearts = this.map.player.petTornado.hearts;
            }

            if (this.map.player.shield.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.Shield);
                shields = this.map.player.shield.currentActiveShields;
            }

            if (this.map.player.swordAndShield.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.SwordAndShield);
                swordAndShields = this.map.player.swordAndShield.currentActiveShields;
            }

            if (this.map.player.midasTouch.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.MidasTouch);
            }

            if (this.map.player.infiniteJump.isActive == true)
            {
                currentlyActivePowerups.Add(PowerupPickup.Type.InfiniteJump);
            }

            Map.detailsToPersist = new Map.DetailsToPersist
            {
                returnDate               = Game.selectedDate,
                returnChunkIndex         = this.chunk.index,
                bonusLiftPositionInMap   = new XY(this.def.tx, this.def.ty),
                playerCheckpointPosition = this.map.player.checkpointPosition,
                coins                    = this.map.coinsCollected,
                silverCoins              = this.map.silverCoinsCollected,
                fruitCollected           = this.map.fruitCollected,
                fruitCurrent             = this.map.fruitCurrent,
                timerCollected           = this.map.timerCollected,
                activePowerups           = currentlyActivePowerups.ToArray(),
                slottedPowerup           = this.map.currentPowerupType,
                yellowPetHearts          = yellowPetHearts,
                tornadoPetHearts         = tornadoPetHearts,
                shields                  = shields,
                swordShields             = swordAndShields
            };
            Transition.GoToMinigame(this.minigameType, minigame.Minigame.Mode.BonusLift);
            Audio.instance.PlaySfx(Assets.instance.sfxUIScreenwipeIn);
            Audio.instance.PlayMusic(Assets.instance.musicTransitionToMini);
        }
        if (this.disableLift == true)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void ComeBackFromMinigame()
    {
        Player player = this.map.player;
        player.state = Player.State.InsideLift;
        player.InsideBonusLift = this;
        player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        if (player is PlayerKing)
        {
            (player as PlayerKing).hatSpriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }

        this.willAllowPlayerToEnter = false;
        this.position = this.PositionWhenLowered;
        this.yOffsetFromRaisedPosition = GoAwayOffset.y;
        this.reachFinalPosition = true;

        this.emerging = true;
        this.isGoingAway = false;
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleDig);

        this.exteriorAnimated.PlayAndLoop(this.animationExteriorVibrate);
        this.buttonAnimated.PlayAndHoldLastFrame(this.animationButtonHide);
        Audio.instance.PlaySfx(Assets.instance.sfxUIScreenwipeOut);
    }

    private void ReachInitialPosition()
    {
        if(this.emerging == false) return;
        this.emerging = false;
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleOpen);
        this.exteriorAnimated.PlayOnce(this.animationExteriorOpen, ExitPlayer);
        this.buttonAnimated.PlayOnce(this.animationButtonPressed);
    }

    private void ExitPlayer()
    {
        Player player = this.map.player;
        if (player.state == Player.State.InsideLift && player.InsideBonusLift == this)
        {
            player.state = Player.State.Normal;
            player.InsideBonusLift = null;
            player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
            if (player is PlayerKing)
            {
                (player as PlayerKing).hatSpriteRenderer.maskInteraction = SpriteMaskInteraction.None;
            }
            player.position.y = this.position.y + 0.9375f;
        }

        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleClose);

        this.reachFinalPosition = false;
        this.disableLift = true;
        this.exteriorAnimated.PlayOnce(this.animationExteriorClose, GoAway);

        if (this.detailsToPersist.minigamePrizePowerup != PowerupPickup.Type.None)
        {
            PowerupPickup.Create(player.position, this.chunk, this.detailsToPersist.minigamePrizePowerup);
        }
        else if (this.detailsToPersist.prizeCoins > 0)
        {
            CoinPrizeFromMinigame.Create(player.position, this.chunk, this.detailsToPersist.prizeCoins);
        }

        this.detailsToPersist = null;
    }

    private void SpawnDustParticles()
    {
        float x = this.positionWhenRaised.x;
        float y = this.positionWhenRaised.y;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 2; n += 1)
        {
            float horizontalSpeed =
                ((n % 2 == 0) ? 1 : -1) * UnityEngine.Random.Range(0.4f, 2.3f) / 16;
            float verticalSpeed = UnityEngine.Random.Range(-0.2f, 0.2f) / 16;

            Transform parent;
            if (this.group != null)
            {
                parent = this.transform;
            }
            else
            {
                parent = this.transform.parent;
            }
            var p = Particle.CreateAndPlayOnce(
                this.animationDustLandParticle,
                originPosition + new Vector2(UnityEngine.Random.Range(-1.5f, 1.5f), 0),
                parent
            );
            var tangent = new Vector2(
                Mathf.Cos(1 * Mathf.PI / 180),
                Mathf.Sin(1 * Mathf.PI / 180)
            );
            var normal = new Vector2(-tangent.y, tangent.x);
            p.velocity = (tangent * horizontalSpeed) + (normal * verticalSpeed);
            p.acceleration = p.velocity * -0.05f / 16;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = SortingLayerForDust();
        }
    }
}
