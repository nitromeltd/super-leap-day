
using UnityEngine;

public class BoostPad : Item
{
    public bool left;
    public Animated.Animation animationBoost;

    public Animated backPieceAnimated;
    public Animated.Animation animationBackPiece;

    public override void Init(Def def)
    {
        base.Init(def);

        this.animated.PlayAndLoop(this.animationBoost);
        this.backPieceAnimated.PlayAndLoop(this.animationBackPiece);

        this.transform.localScale =
            new Vector3(def.tile.flip ? -1 : 1, 1, 1);
    }

    private float deltaForward;
    private float deltaUp;

    public override void Advance()
    {
        Vector2 forward = Forward();
        Vector2 up      = new Vector2(-forward.y, forward.x);

        Vector2 center = (Vector2)this.transform.position;

        var delta        = this.map.player.position - center;
        var deltaForward = Vector2.Dot(delta, forward);
        var deltaUp      = Vector2.Dot(delta, up);
        Player player = this.map.player;

        if (deltaUp > 0 &&
            deltaUp < 0.6f &&
            Mathf.Abs(deltaForward) < 2 &&
            player.TimeSinceJump > 5 &&
            player.ShouldCollideWithItems())
        {
            if (this.map.player.groundState.onGround == false ||
                this.map.player.state != Player.State.Jump)
            {
                this.map.player.state = Player.State.Normal;
            }

            var o = Player.Orientation.Normal;
            int a = this.def.tile.rotation;
            if (a ==  90) o = Player.Orientation.RightWall;
            if (a == 180) o = Player.Orientation.Ceiling;
            if (a == 270) o = Player.Orientation.LeftWall;
            this.map.player.groundState =
                new Player.GroundState(true, o, a, false, false);
            this.map.player.groundspeed =
                ((this.def.tile.flip != left) ? -1 : 1) * (10 / 16f);

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvGripGroundsBoost,
                position: this.position
            );
        }
    }

    private Vector2 Forward()
    {
        switch (this.def.tile.rotation)
        {
            case   0:  return new Vector2( 1,  0);
            case  90:  return new Vector2( 0,  1);
            case 180:  return new Vector2(-1,  0);
            case 270:  return new Vector2( 0, -1);
        }
        return Vector2.zero;
    }
}
