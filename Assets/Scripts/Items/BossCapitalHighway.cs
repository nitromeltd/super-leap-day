using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

public class BossCapitalHighway : Item
{
    /* The Capital Highway boss is supposed to be placed in the top half of the chunk that is supposed to loop.
    The looping chunk should appear right before the ending chunk.*/
    
    public static BossCapitalHighway boss;

    public Animated button;
    public Animated.Animation unpressedAnimation;
    public Animated.Animation pressedAnimation;

    public static bool alreadyCreated = false;
    public static Vector2? nextPosition;

    private bool hasDissapeared = false;
    private bool chunkCreated = false;
    private float timer = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        if(alreadyCreated == false)
        {
            alreadyCreated = true;
            boss = this;
        }
        else
        {
            nextPosition = this.position;
            this.gameObject.SetActive(false);
            return;
        }
        this.hitboxes = new Hitbox[]
        {
            // base hitbox
            new Hitbox(-1f, 1f, -1f, 1f, 0, Hitbox.Solid),
            // button hitbox
            new Hitbox(-1f, 1f, -1.5f, -1, 0, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        if(this == boss && this.hasDissapeared == false)
        {
            if (this.map.NearestChunkTo(this.map.player.position) == this.chunk)
            {
                timer += Time.deltaTime;
                Dissapear();
                if ( timer > 5 &&this.position.y - this.map.player.position.y < 10 && chunkCreated == false)
                {
                    this.map.InsertChunkBeforeEnding(this.chunk);
                    chunkCreated = true;
                    timer = 0;
                }

                // Shoot projectiles at the player
                ShootProjectiles();
            }

            if (chunkCreated == true && nextPosition != null)
            {
                this.position = Vector2.MoveTowards(this.position, nextPosition.Value, 20 * Time.deltaTime);
                this.transform.position = this.position;
                if (Vector2.Distance(this.position, nextPosition.Value) < .01f)
                {
                    chunkCreated = false;
                    nextPosition = null;
                    this.chunk.items.Remove(this);
                    this.chunk = this.map.NearestChunkTo(this.position);
                    this.chunk.items.Add(this);
                }
            }
        }
    }

    private void Dissapear()
    {
        var thisRect = this.hitboxes[1].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
        {
            this.hasDissapeared = true;
            this.button.PlayAndLoop(pressedAnimation);
            this.hitboxes = new Hitbox[0];
            this.spriteRenderer.enabled = false;
            this.button.gameObject.SetActive(false);
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.transform.position,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 1f;
        }
    }

    private void ShootProjectiles()
    {

    }

    public override void Reset()
    {
        base.Reset();
        //this.map.RemoveInsertedChunks();
    }
}
