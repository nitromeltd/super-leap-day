using UnityEngine;
using System.Linq;

public class BossEntryGate : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation fadeInAnimation;

    private bool close;
    private bool closing;
    private int? timeToCloseNeighbours;

    private const float MIN_PLAYER_DISTANCE_CLOSE = 1f;

    private bool endBlock = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.close = false;
        this.closing = false;

        if (def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        this.spriteRenderer.enabled = false;

        this.hitboxes = new Hitbox[0];

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        bool rightEnd = true;
        bool leftEnd = true;
        foreach (var block in this.chunk.items.OfType<BossEntryGate>().ToList())
        {
            if(block.transform.position.x > this.transform.position.x && rightEnd == true)
            {
                rightEnd = false;

            }

            if (block.transform.position.x < this.transform.position.x && leftEnd == true)
            {
                leftEnd = false;
            }
        }

        if(leftEnd || rightEnd)
        {
            this.endBlock = true;
        }
    }

    public override void Advance()
    {
        CloseNeightbours();
        if(this.endBlock == true)
        {
            CheckClose();
        }

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
    }

    private void CloseNeightbours()
    {
        if (this.timeToCloseNeighbours.HasValue &&
            this.timeToCloseNeighbours > 0)
        {
            this.timeToCloseNeighbours -= 1;
            if (this.timeToCloseNeighbours == 0)
            {
                foreach (var block in this.chunk.items.OfType<BossEntryGate>())
                {
                    if (block.close == true) continue;

                    var dx = block.def.tx - this.def.tx;
                    var dy = block.def.ty - this.def.ty;
                    if (dx == 0 && Mathf.Abs(dy) == 1 ||
                        dy == 0 && Mathf.Abs(dx) == 1)
                    {
                        block.Close();
                    }
                }
            }
        }
    }

    private void CheckClose()
    {
        if (this.close == true && this.closing == true) return;

        float distYToPlayer = this.map.player.position.y - this.position.y;

        if (distYToPlayer > MIN_PLAYER_DISTANCE_CLOSE)
        {
            this.closing = true;
            this.Close();
        }
    }

    public void Close()
    {
        if (this.close == true) return;

        this.close = true;

        this.animated.PlayOnce(this.fadeInAnimation, () => {
            this.animated.PlayOnce(idleAnimation);
        });
        MakeSolid();
        this.spriteRenderer.enabled = true;

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.hitboxes[0].InWorldSpace().center, 1.5f, 1.5f);

        this.timeToCloseNeighbours = 13;

        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .3f);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvGateBlockBurst,
            position: this.position
            );
    }

    private void MakeSolid()
    {
        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };
    }

    public override void Reset()
    {
        base.Reset();

        if (this.close == true)
        {
            this.close = false;
            this.closing = false;
            this.timeToCloseNeighbours = null;
            this.spriteRenderer.enabled = false;

            this.hitboxes = new Hitbox[0];

            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );
        }
    }
}
