using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using TMPro;

public class BossExitGate : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation fadeAnimation;

    private bool open;
    private int? timeToOpenNeighbours;

    public bool openWhenAllEnemiesAreDead;
    public TextMeshPro remainingEnemiesText;
    private int remainingEnemies = 0;
    private int enemiesAtStart = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        this.open = false;

        if (def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        this.spriteRenderer.enabled = true;

        MakeSolid();
        if (this.openWhenAllEnemiesAreDead == true)
        {
            foreach (var path in this.chunk.connectionLayer.paths)
            {
                var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
                if (nearestConnectionNode != null)
                {
                    remainingEnemies++;
                }
            }
            this.remainingEnemiesText.text = remainingEnemies.ToString();
            this.enemiesAtStart = remainingEnemies;
        }

        //this.remainingEnemiesText.gameObject.GetComponent<Renderer>().sortingLayerName = "Items (Front)";
        //this.remainingEnemiesText.gameObject.GetComponent<Renderer>().sortingOrder = 2;
    }


    private void MakeSolid()
    {
        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };
    }

    public override void Advance()
    {
        OpenNeightbours();
    }

    private void OpenNeightbours()
    {
        if (this.timeToOpenNeighbours.HasValue &&
            this.timeToOpenNeighbours > 0)
        {
            this.timeToOpenNeighbours -= 1;
            if (this.timeToOpenNeighbours == 0)
            {
                foreach (var block in this.chunk.items.OfType<BossExitGate>())
                {
                    if (block.open == true) continue;

                    var dx = block.def.tx - this.def.tx;
                    var dy = block.def.ty - this.def.ty;
                    if (dx == 0 && Mathf.Abs(dy) == 1 ||
                        dy == 0 && Mathf.Abs(dx) == 1)
                    {
                        block.Open();
                    }
                }
            }
        }
    }

    public void Open()
    {
        if (this.open == true) return;

        this.open = true;

        this.animated.PlayOnce(this.fadeAnimation, () => {
            this.spriteRenderer.enabled = false;
        });

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.hitboxes[0].InWorldSpace().center, .5f, .5f);

        this.hitboxes = new Hitbox[0];

        this.timeToOpenNeighbours = 13;

        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .3f);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvGateBlockBurst,
            position: this.position
            );
    }

    public override void Reset()
    {
        base.Reset();

        if (this.open == true)
        {
            this.open = false;
            this.timeToOpenNeighbours = null;
            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(idleAnimation);

            MakeSolid();

            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );
        }

        if (this.openWhenAllEnemiesAreDead == true)
        {
            this.remainingEnemiesText.gameObject.SetActive(true);
            this.remainingEnemies = enemiesAtStart;
            this.remainingEnemiesText.text = enemiesAtStart.ToString();
        }
    }

    public void DecreaseEnemy()
    {
        this.remainingEnemies--;
        this.remainingEnemiesText.text = remainingEnemies.ToString();
        if(this.remainingEnemies == 0)
        {
            Open();
            this.remainingEnemiesText.gameObject.SetActive(false);
        }
    }
}
