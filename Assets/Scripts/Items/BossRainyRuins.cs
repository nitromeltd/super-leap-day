using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class BossRainyRuins : Item
{
    /* The Rainy Ruins boss uses multiple chunks to work. 
    The starting chunk has the Entry Gates (They Appear when the player goes above them) and the next three chunks have 2 Hand Blocks each with various values for path, position and offset values.
    The final chunk has the boss head connected to one of the exit gates so it opens when the boss is defeated.
    Another use of the exit gates is by using Exit Remaining Enemies. It can be connected to all the enemies in the chunk so it opens the other exit gates when the enemies are defeated. */

    public Animated button;
    public Animated.Animation unpressedAnimation;
    public Animated.Animation pressedAnimation;

    private int numberOfPresses = 0;

    private bool hasDissapeared = false;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;

    private const float BreathePeriod = 0.5f;
    private const float BreatheAmplitude = 0.25f;

    private BossExitGate exitGate;

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        this.spriteRenderer.enabled = true;
        MakeSolid();

        var path = this.chunk.connectionLayer.NearestPathTo(this.position, 1);
        if (path != null)
        {
            var node = path.NearestNodeTo(this.position);
            if (node != null)
            {
                var otherNode =
                    node.Previous() ??
                    node.Next();
                var Gate = this.chunk.NearestItemTo<BossExitGate>(otherNode.Position);
                this.exitGate = Gate;
            }
        }
    }

    public override void Advance()
    {
        base.Advance();

        // breathe movement
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);
        Vector2 breathePos = this.breatheMovement ? Vector2.up * distance : Vector2.zero;
        this.transform.position = this.position + breathePos;

        if (this.hasDissapeared == false)
        {
            Dissapear();
        }
    }

    private void Dissapear()
    {
        var thisRect = this.hitboxes[1].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
        {
            StartCoroutine(_Dissapear());
            this.hasDissapeared = true;
            this.button.PlayAndLoop(pressedAnimation);
            this.numberOfPresses++;
        }
    }

    private System.Collections.IEnumerator _Dissapear()
    {
        yield return new WaitForSeconds(.1f);

        BossRainyRuinsPlatform.leftHand.MoveAside();
        BossRainyRuinsPlatform.rightHand.MoveAside();

        this.hitboxes = new Hitbox[0];
        this.spriteRenderer.enabled = false;
        this.button.gameObject.SetActive(false);
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.transform.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Items (Front)";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1f;

        if (this.numberOfPresses >= 3)
        {
            this.exitGate.Open();
        }

        yield return new WaitForSeconds(5f);

        if(this.numberOfPresses <= 3)
        {
            this.spriteRenderer.enabled = true;
            MakeSolid();
            this.hasDissapeared = false;
            this.button.gameObject.SetActive(true);
            this.button.PlayAndLoop(unpressedAnimation);

            p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.transform.position,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 1f;
            BossRainyRuinsPlatform.leftHand.MoveBack();
            BossRainyRuinsPlatform.rightHand.MoveBack();
        }
    }

    public override void Reset()
    {
        base.Reset();
        this.spriteRenderer.enabled = true;
        MakeSolid();
        this.hasDissapeared = false;
        this.button.gameObject.SetActive(true);
        this.button.PlayAndLoop(unpressedAnimation);
        this.numberOfPresses = 0;
    }

    private void MakeSolid()
    {
        this.hitboxes = new Hitbox[]
        {
            // base hitbox
            new Hitbox(-1f, 1f, -1f, 1f, 0, Hitbox.Solid),
            // button hitbox
            new Hitbox(-1f, 1f, 1f, 1.5f, 0, Hitbox.NonSolid)
        };
    }
}
