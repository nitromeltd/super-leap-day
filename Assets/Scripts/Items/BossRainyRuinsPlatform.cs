using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class BossRainyRuinsPlatform : Item
{
    private NitromeEditor.Path path;
    private NitromeEditor.Path tempPath;
    private List<NitromeEditor.Path> paths = new List<NitromeEditor.Path>() { };

    private List<Vector2> positions = new List<Vector2>() { };
    private Vector2 tempPosition;

    private List<Direction> directions = new List<Direction>() { };
    private float tempDirection;

    private List<Vector2> offsets = new List<Vector2>() { };
    private Vector2 tempOffset = new Vector2(-1,3);
    private Vector2 offset;

    public static bool bossHandsSet = false;

    public static BossRainyRuinsPlatform leftHand;
    public static BossRainyRuinsPlatform rightHand;

    public List<BossRainyRuinsPlatform> otherRightHands = new List<BossRainyRuinsPlatform>() { };
    public List<BossRainyRuinsPlatform> otherLefttHands = new List<BossRainyRuinsPlatform>() { };

    public bool thisIsLH = false;
    public bool thisIsRH = false;
    private int currentPath = 0;
    private float timer = 0;
    private float timer2 = 0;

    private bool pathSet = false;
    public AnimationCurve speedCurve;
    private Vector2 startPosition;
    private Vector2 targetPosition;
    private float startRotation;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;

    private const float BreathePeriod = 0.5f;
    private const float BreatheAmplitude = 0.25f;

    public enum State
    {
        Path,
        Transition,
        MoveAside,
        MoveBack,
        StayStill
    }

    public State state;

    public enum Direction { Horz, Vert };
    public Direction direction;

    public override void Init(Def def)
    {
        base.Init(def);

        this.path = (this.path == null) ?
            chunk.pathLayer.NearestPathTo(this.position, 2) : this.path;
        if (this.path != null)
        {
            this.path = Util.MakeClosedPathByPingPonging(this.path);
            this.tempPath = path;
        }

        if (def.tile.properties?.ContainsKey("offset_x") == true)
        {
            this.offset.x = def.tile.properties["offset_x"].f;
        }
        else
        {
            this.offset.x = 0f;
        }

        if (def.tile.properties?.ContainsKey("offset_y") == true)
        {
            this.offset.y = def.tile.properties["offset_y"].f;
        }
        else
        {
            this.offset.y = 0f;
        }

        if (def.tile.rotation == 0 || def.tile.rotation == 180)
        {
            this.direction = Direction.Horz;
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
        else
        {
            this.direction = Direction.Vert;
            this.transform.rotation = Quaternion.Euler(0, 0, 90);
        }

        if (BossRainyRuinsPlatform.bossHandsSet == false)
        {
            bossHandsSet = true;

            foreach (var platform in this.chunk.items.OfType<BossRainyRuinsPlatform>().ToList())
            {
                if(this != platform)
                {
                    if (platform.transform.position.x > this.transform.position.x)
                    {
                        rightHand = platform;
                        leftHand = this;
                        thisIsLH = true;
                        platform.thisIsRH = true;
                    }
                    else
                    {
                        rightHand = this;
                        leftHand = platform;
                        thisIsRH = true;
                        platform.thisIsLH = true;
                    }
                }
            }
        }
        
        if( leftHand != this && rightHand!= this)
        {
            foreach (var platform in this.chunk.items.OfType<BossRainyRuinsPlatform>().ToList())
            {
                if (this != platform)
                {
                    if (platform.transform.position.x > this.transform.position.x)
                    {
                        leftHand.otherLefttHands.Add(this);
                    }
                    else
                    {
                        rightHand.otherRightHands.Add(this);
                    }
                }
            }
            this.spriteRenderer.enabled = (false);
            this.gameObject.SetActive(false);
            return;
        }

        this.hitboxes = new[] {
            new Hitbox(-1.5f, 1.5f, -0.75f, 0.75f, this.rotation, Hitbox.Solid, true, true)
        };
        this.state = State.Path;
    }

    public override void Advance()
    {
        base.Advance();

        if ( leftHand == this || rightHand == this)
        {
            if( this.pathSet == false)
            {
                SetPaths();
            }

            switch (this.state)
            {
                case State.Path:
                    AdvanvcePaths();
                    break;
                case State.Transition:
                    AdvanceTransition();
                    break;
                case State.MoveAside:
                    AdvanceMoveAside();
                    break;
                case State.MoveBack:
                    AdvanceMoveBack();
                    break;
            }
        }
    }

    public static float TimeNearestToPoint(NitromeEditor.Path path, Vector2 pt)
    {
        var edgeIndex = path.NearestEdgeIndexTo(pt).Value;
        var edge = path.edges[edgeIndex];

        var edgeDelta = edge.to.Position - edge.from.Position;
        var through =
            Vector2.Dot(edgeDelta, pt - edge.from.Position) / edgeDelta.sqrMagnitude;
        through = Mathf.Clamp01(through);

        float result = 0.0f;
        for (var n = 0; n < edgeIndex; n += 1)
        {
            result += path.edges[n].distance / path.edges[n].speedPixelsPerSecond;
        }
        result += through * (edge.distance / edge.speedPixelsPerSecond);
        return result;
    }


    public override void Reset()
    {
        base.Reset();
    }

    public void SetPaths()
    {
        this.pathSet = true;
        this.paths.Add(this.path);
        this.positions.Add(this.position);
        this.directions.Add(this.direction);
        this.offsets.Add(this.offset);

        if (this == leftHand)
        {
            foreach (var hand in this.otherLefttHands)
            {
                this.positions.Add(hand.position);
                this.directions.Add(hand.direction);
                this.offsets.Add(hand.offset);

                if (hand.path == null)
                {
                    this.paths.Add(null);
                }
                else
                {
                    this.paths.Add(hand.path);
                }
            }
        }
        else if(this == rightHand)
        {
            foreach (var hand in this.otherRightHands)
            {
                this.positions.Add(hand.position);
                this.directions.Add(hand.direction);
                this.offsets.Add(hand.offset);

                if (hand.path == null)
                {
                    this.paths.Add(null);
                }
                else
                {
                    this.paths.Add(hand.path);
                }
            }
        }
    }

    private void AdvanvcePaths()
    {
        this.timer += Time.deltaTime;

        if(this.tempPath != null)
        {
            this.position = this.tempPath.PointAtTime((this.map.frameNumber / 60.0f)).position + this.tempOffset;
            this.transform.position = this.position;
        }
        else
        {
            this.breatheTimer += Time.deltaTime;
            float theta = this.breatheTimer / BreathePeriod;
            float distance = BreatheAmplitude * Mathf.Sin(theta);
            Vector2 breathePos = this.breatheMovement ? Vector2.up * distance : Vector2.zero;
            this.transform.position = this.position + breathePos + this.tempOffset;
        }

        if (this.timer > 6)
        {
            this.currentPath++;
            if (this.currentPath > this.paths.Count - 1)
            {
                this.currentPath = 0;
            }
            this.tempPath = this.paths[this.currentPath];
            this.tempPosition = this.positions[this.currentPath];
            this.timer = 0;
            this.timer2 = 0;
            this.state = State.Transition;
            this.startPosition = this.position;

            if (this.directions[this.currentPath] == Direction.Horz)
            {
                this.tempDirection = 0;
                HitboxHor();
            }
            else
            {
                this.tempDirection = 90;
                HitboxVert();
            }
            this.startRotation = this.transform.rotation.z;

            this.tempOffset = this.offsets[currentPath];
        }
    }

    private void AdvanceTransition()
    {
        this.timer2 += Time.deltaTime;

        float z = Mathf.Lerp(this.startRotation, this.tempDirection, this.speedCurve.Evaluate(this.timer2));
        this.transform.rotation = Quaternion.Euler(0, 0, z);

        if (this.tempPath != null)
        {
            this.position = Vector2.Lerp(this.startPosition, this.tempPath.PointAtTime((this.map.frameNumber / 60.0f)).position + this.tempOffset, this.speedCurve.Evaluate(this.timer2));
            this.transform.position = this.position;

            if (Vector2.Distance(this.position, this.tempPath.PointAtTime((this.map.frameNumber / 60.0f)).position + this.tempOffset) < .01f)
            {
                this.state = State.Path;
                this.timer2 = 0;
            }
        }
        else
        {
            this.position = Vector2.Lerp(this.startPosition, this.tempPosition + this.tempOffset, this.speedCurve.Evaluate(this.timer2));
            this.transform.position = this.position;

            if (Vector2.Distance(this.position, this.tempPosition + this.tempOffset) < .01f)
            {
                this.state = State.Path;
                this.timer2 = 0;
            }
        }
    }

    public void MoveAside()
    {
        this.state = State.MoveAside;
        if(this == leftHand)
        {
            this.targetPosition = this.position + new Vector2(-10, 0);
        }
        else
        {
            this.targetPosition = this.position + new Vector2(10, 0);
        }
        this.timer2 = 0;
        this.startPosition = this.position;
    }

    public void MoveBack()
    {
        this.state = State.MoveBack;
        if (this == leftHand)
        {
            this.targetPosition = this.position + new Vector2(10, 0);
        }
        else
        {
            this.targetPosition = this.position + new Vector2(-10, 0);
        }
        this.timer2 = 0;
        this.startPosition = this.position;
    }

    private void AdvanceMoveBack()
    {
        this.timer2 += Time.deltaTime;
        this.position = Vector2.Lerp(this.startPosition, this.targetPosition, this.speedCurve.Evaluate(this.timer2));
        this.transform.position = this.position;

        if (Vector2.Distance(this.position, this.targetPosition) < .01f)
        {
            this.state = State.Path;
            this.timer2 = 0;
        }
    }

    private void AdvanceMoveAside()
    {
        this.timer2 += Time.deltaTime;
        this.position = Vector2.Lerp(this.startPosition, this.targetPosition, this.speedCurve.Evaluate(this.timer2));
        this.transform.position = this.position;

        if (Vector2.Distance(this.position, this.targetPosition) < .01f)
        {
            this.state = State.StayStill;
            this.timer2 = 0;
        }
    }

    private void HitboxHor()
    {
        this.hitboxes = new[] {
            new Hitbox(-1.5f, 1.5f, -0.75f, 0.75f, this.rotation, Hitbox.Solid, true, true)
        };
    }

    private void HitboxVert()
    {
        this.hitboxes = new[] {
            new Hitbox( -0.75f, 0.75f, -1.5f, 1.5f, this.rotation, Hitbox.Solid, true, true)
        };
    }
}
