﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;
public class Box : Item
{
    public Animated.Animation animationEnter;
    public Animated.Animation animationBlink;
    public Animated.Animation animationExit;
    public Animated.Animation animationGoingUp;
    public Animated.Animation animationGoingDown;
    public Animated.Animation animationHitTheGround;
    public Animated.Animation animationIdle;
    public Animated.Animation animationEmptyIdle;
    public Animated.Animation animationArrowBobbing;

    public Animated arrowAnimated;

    private const float jumpVerticalVelocity = 0.3f;
    private const float jumpHorizontalVelocity = 0.1f;
    public Vector2 velocity;
    private bool facingRight;
    private const float GravityForce = 0.02f;
    private bool exitBoxOnJump;
    private int boxReenterDelayCounter;
    private int boxWobbleDelayCounter;
    private const int BoxReenterDelay = 120;
    private const int BoxWobbleDelay = 120;
    private int blinkDelayCounter;
    private bool lastOnGround;
    private bool onGround;
    private Vector2 surfaceVelocity;

    public bool IsMoving => Mathf.Approximately(this.velocity.sqrMagnitude, 0f) == false;

    public override void Init(Def def)
    {
        base.Init(def);
        this.facingRight = !def.tile.flip;
        if (!this.facingRight)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }

        this.exitBoxOnJump = false;
        this.solidToEnemy = false;
        this.hitboxes = new[] {
            new Hitbox(-1.25f, 1.25f, -1f, 1.0f, this.rotation, Hitbox.NonSolid)
        };

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        arrowAnimated.PlayAndLoop(animationArrowBobbing);
    }

    private void CheckIfPlayerGetsInsideBox()
    {
        if (this.boxReenterDelayCounter > 0)
        {
            this.boxReenterDelayCounter--;
            return;
        }
        var player = this.map.player;
        bool isFallingInsideBox = (Mathf.Abs(player.position.x - this.position.x) < 1.25f) && (player.velocity.y < 0) && (player.position.y - this.position.y > 1.5f && player.position.y - this.position.y < 2f);

        if ((player.state == Player.State.Normal || player.state == Player.State.Jump)
            && isFallingInsideBox)
        {
            player.EnterBox(this);
            this.animated.PlayOnce(this.animationEnter);
            CreateLidParticles();

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvBoxEnter,
                position: this.position
            );
        }
    }

    public void CheckIfBoxShouldWobble()
    {
        if (boxWobbleDelayCounter > 0)
        {
            boxWobbleDelayCounter--;
            return;
        }
        var player = this.map.player;
        if (player.state == Player.State.Normal)
        {
            if (Mathf.Abs(player.position.x - this.position.x) < 1.25f)
            {
                if (Mathf.Abs(player.position.y - this.position.y) < 0.1f)
                {
                    boxWobbleDelayCounter = BoxWobbleDelay;
                    this.velocity = this.velocity + new Vector2(0, 0.1f);
                }
            }
        }
    }

    public override void Advance()
    {
        base.Advance();
        UpdateArrow();
        CheckIfPlayerGetsInsideBox();
        CheckIfBoxShouldWobble();
        AdvanceFall();

        if(IsMoving == true)
        {
            if (this.velocity.x > 0)
            {
                this.facingRight = true;
                this.transform.localScale = new Vector3(1, 1, 1);
            }
            else if (this.velocity.x < 0)
            {
                this.facingRight = false;
                this.transform.localScale = new Vector3(-1, 1, 1);
            }
        }

        CheckForBlink();

        if (this.map.player.state == Player.State.InsideBox && this.map.player.GetBox() == this)
        {
            this.map.player.position = this.position;
        }

        SetCorrectAnimation();
    }

    private void UpdateArrow()
    {
        if (this.map.player.state == Player.State.InsideBox && this.map.player.GetBox() == this)
        {
            if (arrowAnimated.gameObject.activeSelf)
            {
                arrowAnimated.gameObject.SetActive(false);
            }
        }
        else
        {
            if (!arrowAnimated.gameObject.activeSelf)
            {
                arrowAnimated.gameObject.SetActive(true);
            }
        }
    }

    private void CheckForBlink()
    {
        if (!(this.map.player.state == Player.State.InsideBox) || this.map.player.GetBox() != this)
        {
            return;
        }

        if (--this.blinkDelayCounter < 0)
        {
            this.blinkDelayCounter = Random.Range(180, 240);
            this.animated.PlayOnce(this.animationBlink);
        }
    }

    private void MakeBoxJump()
    {
        this.velocity = this.velocity + new Vector2((this.facingRight ? 1f : -1f) * jumpHorizontalVelocity, jumpVerticalVelocity);
        this.exitBoxOnJump = true;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBoxJump,
            position: this.position
        );
    }

    private void AdvanceFall()
    {
        this.velocity.y -= GravityForce;

        // ignore self collision
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        this.surfaceVelocity = Vector2.zero;
        MoveVertically(raycast);
        MoveHorizontally(raycast);
        this.transform.position = this.position;
        CheckIfGrounded(raycast);
    }

    private void CheckIfGrounded(Raycast raycast)
    {
        this.lastOnGround = this.onGround;

        var rect = this.hitboxes[0];
        var maxDistance = Mathf.Abs(this.velocity.y) + Mathf.Abs(rect.yMin) + 0.1f;   //extra offset to make sure it hits ground
        var result = raycast.Vertical(this.position, false, maxDistance);    //add some extra stuff
        // Debug.DrawLine(this.position, this.position - new Vector2(0,maxDistance), Color.magenta, 0);

        if (result.anything == true && result.distance <= maxDistance)
        {
            //we are grounded or about to get grounded
            this.onGround = true;
        }
        else
        {
            this.onGround = false;
        }

        if (this.lastOnGround && this.onGround && this.velocity.y <= 0)
        {
            this.velocity.x *= 0.5f;

            if(Mathf.Abs(this.velocity.x) < 0.01f)
            {
                this.velocity.x = 0f;
            }
        }
    }

    private void SetCorrectAnimation()
    {
        if (boxWobbleDelayCounter > 0)
        {
            return;
        }

        if (animated.currentAnimation != this.animationEnter && animated.currentAnimation != this.animationExit)
        {
            if (!this.lastOnGround && this.onGround)
            {
                if (!(this.map.player.state == Player.State.InsideBox))
                {
                    animated.PlayOnce(animationEmptyIdle);
                }
                else
                {
                    animated.PlayOnce(animationHitTheGround, () => { animated.PlayOnce(animationIdle); });
                }
                CreateLandingParticles();
            }
            else if (!this.onGround && this.velocity.y > 0)
            {
                animated.PlayOnce(animationGoingUp);
            }
            else if (!this.onGround && this.velocity.y < 0)
            {
                animated.PlayOnce(animationGoingDown);
            }
        }
    }

    private void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];        

        if (this.velocity.x > 0 || this.surfaceVelocity.x > 0)
        {
            var high    = this.position.Add(0, rect.yMax - 0.250f);
            var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
            var low     = this.position.Add(0, rect.yMin + 0.250f);
            var maxDistanceHorizontal = this.velocity.x + rect.xMax;

            var resultHorizontal = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistanceHorizontal),
                raycast.Horizontal(mid,  true, maxDistanceHorizontal),
                raycast.Horizontal(low,  true, maxDistanceHorizontal)
            );

            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= this.velocity.x + rect.xMax)
            {
                this.position.x += resultHorizontal.distance - rect.xMax;
                this.velocity.x = -this.velocity.x;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0 || this.surfaceVelocity.x < 0)
        {
            var high    = this.position.Add(0, rect.yMax - 0.250f);
            var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
            var low     = this.position.Add(0, rect.yMin + 0.250f);

            var maxDistanceHorizontal = -(this.velocity.x + rect.xMin);
            // var resultHorizontal = raycast.Horizontal(this.position, false, maxDistanceHorizontal);
            var resultHorizontal = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistanceHorizontal),
                raycast.Horizontal(mid,  false, maxDistanceHorizontal),
                raycast.Horizontal(low,  false, maxDistanceHorizontal)
            );

            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= -(this.velocity.x + rect.xMin))
            {
                this.position.x += -(resultHorizontal.distance + rect.xMin);
                this.velocity.x = -this.velocity.x;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }

    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var tolerance = 0.0f;
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin + tolerance;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin + tolerance)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                if (result.tiles.Length > 0)
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                    {
                        surfaceVelocity.x = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed;
                        surfaceVelocity.y = 0;
                    }
                }

                this.surfaceVelocity = surfaceVelocity;

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.exitBoxOnJump = false;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void AdvancePlayer()
    {

    }

    public bool IsPlayerDetectableInBox()
    {
        if (velocity.magnitude > 0.1f)
        {
            return true;
        }

        return false;
    }

    public void ExitBoxCleanup()
    {
        this.boxReenterDelayCounter = BoxReenterDelay;
        this.map.player.velocity = new Vector2(this.facingRight ? 0.2f : -0.2f, 0.3f);
        this.map.player.position = this.position + new Vector2(0, 1.5f);
        this.map.player.facingRight = this.facingRight;
        this.animated.PlayOnce(animationExit);

        var raycast = new Raycast(this.map, this.position, isCastByPlayer: true);
        if (this.map.player.WillBeCrushed(raycast) == true)
            this.map.player.position += new Vector2(0, -0.5f);

        CreateLidParticles();
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBoxExit,
            position: this.position
        );
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvPipesCannon,
            position: this.position
        );
    }

    private void CreateLandingParticles()
    {
        var tangent = new Vector2(
            Mathf.Cos(0 * Mathf.PI / 180),
            Mathf.Sin(0 * Mathf.PI / 180)
        );
        var normal = new Vector2(-tangent.y, tangent.x);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                this.position - (normal * 1) + new Vector2(this.facingRight ? 0.5f : -0.5f, 0),
                this.transform.parent
            );
            p.velocity.x =
                ((n % 2 == 0) ? 1 : -1) *
                Rnd.Range(0.4f, 2.3f) / 16;
            p.velocity.y = Rnd.Range(-0.2f, 0.2f) / 16;
            p.acceleration = p.velocity * -0.05f / 16;
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    private void CreateLidParticles()
    {
        var up = new Vector2(0, 1f);
        var forward = new Vector2(1f, 0);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                this.position +
                    new Vector2(Rnd.Range(-1.25f, 1.25f), 0.8f),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            float speed = Rnd.Range(0.3f, 0.7f) / 16f;
            p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) + this.velocity.x, this.velocity.y + Rnd.Range(0, 100) > 50 ? speed : -speed);
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    public void PlayerJumped()
    {
        if (this.map.player.state == Player.State.InsideBox && this.map.player.GetBox() == this)
        {
            if (this.exitBoxOnJump)
            {
                this.map.player.ExitBox();
            }
            else
            {
                MakeBoxJump();
            }
        }
    }
}
