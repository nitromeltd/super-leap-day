using UnityEngine;

public class BrainBox : Item
{
    public float blockSize = 1f;

    [Header("Animations")]
    public Animated.Animation animationIdle;
    public Animated.Animation animationSmash;
    public Animated.Animation animationTurnClockwise;
    public Animated.Animation animationTurnAnticlockwise;

    public Animated faceAnimated;

    public enum State
    {
        None,
        Sleep,
        Detect,
        Chosen,
        Move,
        Collide,
        Dizzy
    }

    public enum Direction
    {
        None,

        Up,
        Right,
        Down,
        Left
    }

    private Vector2[] DirectionVector = new Vector2[]
    {
        Vector2.zero,

        Vector2.up,
        Vector2.right,
        Vector2.down,
        Vector2.left
    };

    public Vector2 GetVectorFromDirection(Direction direction)
    {
        return DirectionVector[(int)direction];
    }

    private State state = State.None;
    private Direction targetDirection = Direction.None;
    private Direction facingDirection = Direction.Down;
    private Vector2 velocity;
    private int timer;
    private float amountOfSubsteppingApplied = 0.0f;

    public const int ChosenTime = 30;
    public const int DizzyTime = 60 * 2;
    private const float DistanceToDetectPlayer = 20f;
    private const float MoveAcceleration = 0.01f;
    private const float MaxHorizontalVelocity = 0.70f;
    private const float MaxVerticalVelocity = 0.70f;

    private Hitbox BlockHitbox => this.hitboxes[0];
    private Vector2 CurrentAcceleration =>
        GetVectorFromDirection(this.targetDirection) * MoveAcceleration;
    private float ShakeAmount => 0.50f * this.blockSize;

    public Vector2 Velocity => this.velocity;

    public override void Init(Def def)
    {
        base.Init(def);

        float halfSize = this.blockSize / 2f;        
        this.hitboxes = new[] {
            new Hitbox(
                -this.blockSize, this.blockSize,
                -this.blockSize, this.blockSize,
                this.rotation, Hitbox.Solid,
                wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
        };

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        ChangeState(State.Sleep);
    }
    
    public override void Advance()
    {
        var raycast = new Raycast(
            this.map, this.position, ignoreItem1: this, isCastByEnemy: true
        );

        switch (this.state)
        {
            case State.Sleep:
                AdvanceSleep();
                break;

            case State.Detect:
                AdvanceDetect(raycast);
                break;

            case State.Chosen:
                AdvanceChosen();
                break;

            case State.Move:
                AdvanceMove(raycast);
                break;

            case State.Collide:
                AdvanceCollide();
                break;

            case State.Dizzy:
                AdvanceDizzy();
                break;
        }

        this.transform.position = this.position;
    }

    private void AdvanceSleep()
    {
        if(IsPlayerCloseEnough() == true)
        {
            ChangeState(State.Detect);
        }
    }

    private bool IsPlayerCloseEnough()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        if(playerChunk != this.chunk) return false;

        float currentDistanceToPlayer =
            Vector2.Distance(this.map.player.position, this.position);

        return currentDistanceToPlayer < DistanceToDetectPlayer;
    }

    private void AdvanceDetect(Raycast raycast)
    {
        Direction ClosestGeneralDirection(Raycast raycast)
        {
            Direction hDirection = (this.map.player.position.x > this.position.x) ?
                    Direction.Right : Direction.Left;
            Direction vDirection = (this.map.player.position.y > this.position.y) ?
                    Direction.Up : Direction.Down;

            float hDistance = Mathf.Abs(this.map.player.position.x - this.position.x);
            float vDistance = Mathf.Abs(this.map.player.position.y - this.position.y);

            Direction closestDirection =
                (hDistance > vDistance) ? hDirection : vDirection;

            if(IsDirectionFree(closestDirection, raycast))
            {
                return closestDirection;
            }

            return Direction.None;
        }

        this.targetDirection = ClosestGeneralDirection(raycast);

        if (this.targetDirection != Direction.None &&
            this.facingDirection == this.targetDirection)
        {
            ChangeState(State.Chosen);
        }
        else if (
            this.targetDirection != Direction.None &&
            this.faceAnimated.currentAnimation != this.animationTurnClockwise &&
            this.faceAnimated.currentAnimation != this.animationTurnAnticlockwise)
        {
            Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxBrainboxTurn);
            if (this.targetDirection == TurnAnticlockwise(this.facingDirection))
            {
                RotateFace(this.targetDirection);
                this.faceAnimated.PlayOnce(
                    this.animationTurnAnticlockwise,
                    () => {
                        this.facingDirection = TurnAnticlockwise(this.facingDirection);
                    }
                );
            }
            else
            {
                this.faceAnimated.PlayOnce(
                    this.animationTurnClockwise,
                    () => {
                        this.facingDirection = TurnClockwise(this.facingDirection);
                        RotateFace(this.facingDirection);
                    }
                );
            }
        }
        else if(IsPlayerCloseEnough() == false)
        {
            ChangeState(State.Sleep);
        }
    }

    private Direction TurnClockwise(Direction d) => d switch
    {
        Direction.Down  => Direction.Left,
        Direction.Left  => Direction.Up,
        Direction.Up    => Direction.Right,
        Direction.Right => Direction.Down,
        _               => d
    };

    private Direction TurnAnticlockwise(Direction d) => d switch
    {
        Direction.Down  => Direction.Right,
        Direction.Right => Direction.Up,
        Direction.Up    => Direction.Left,
        Direction.Left  => Direction.Down,
        _               => d
    };

    private void RotateFace(Direction newDirection)
    {
        float angleDegrees = newDirection switch
        {
            Direction.Right => 90,
            Direction.Up    => 180,
            Direction.Left  => 270,
            _               => 0
        };
        this.faceAnimated.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);
    }

    private bool IsDirectionFree(Direction direction, Raycast raycast)
    {
        Vector2 directionVector = GetVectorFromDirection(direction);
        Vector2 perpendicular = new Vector2(directionVector.y, -directionVector.x);

        for (int n = 0; n < 5; n += 1)
        {
            var across = n switch {
                0 => -0.9f,
                1 => -0.4f,
                2 => 0,
                3 => 0.4f,
                _ => 0.9f
            };
            var start =
                this.position +
                (blockSize * directionVector) +
                (perpendicular * blockSize * across);
            if (raycast.Arbitrary(start, directionVector, 1f).anything == true)
                return false;
        }

        return true;
    }

    private void AdvanceChosen()
    {
        if(this.timer > 0)
        {
            this.timer -= 1;

            if(this.timer == 0)
            {
                ChangeState(State.Move);
            }
        }
    }

    private void AdvanceMove(Raycast raycast)
    {
        this.velocity += CurrentAcceleration;
        this.velocity.x = Mathf.Clamp(
            this.velocity.x, -MaxHorizontalVelocity, MaxHorizontalVelocity
        );
        this.velocity.y = Mathf.Clamp(
            this.velocity.y, -MaxVerticalVelocity, MaxVerticalVelocity
        );

        ApplyMovement(1 - this.amountOfSubsteppingApplied);
        this.amountOfSubsteppingApplied = 0f;
    }

    public void ApplyMovement(float proportionOfFrameMovement)
    {
        if (this.state != State.Move)
            return;

        if (proportionOfFrameMovement + this.amountOfSubsteppingApplied >= 1f)
        {
            proportionOfFrameMovement = 1 - this.amountOfSubsteppingApplied;
            if (proportionOfFrameMovement == 0)
                return;
        }

        this.amountOfSubsteppingApplied += proportionOfFrameMovement;

        var dx = this.velocity.x * proportionOfFrameMovement;
        var dy = this.velocity.y * proportionOfFrameMovement;
        var raycast = new Raycast(
            this.map, this.position, ignoreItem1: this, isCastByEnemy: true
        );

        if (dy > 0f)
        {
            var maxDistance = dy + BlockHitbox.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(this.position.Add(BlockHitbox.xMin * 0.9f, 0), true, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMin * 0.4f, 0), true, maxDistance),
                raycast.Vertical(this.position.Add(0,                       0), true, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMax * 0.4f, 0), true, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMax * 0.9f, 0), true, maxDistance)
            );

            if(result.End().y > this.chunk.yMax)
            {
                result.anything = true;
                result.distance = Mathf.Abs(this.chunk.yMax - result.start.y) + 1f;
            }

            if (result.anything == true &&
                result.distance < dy + BlockHitbox.yMax)
            {
                this.position.y += result.distance - BlockHitbox.yMax;
                this.velocity.y = 0f;

                SmashBreakableBlocks(result);
                ChangeState(State.Collide);
                this.map.ScreenShakeAtPosition(this.position, Vector2.up * ShakeAmount);
            }
            else
            {
                this.position.y += dy;
            }
        }
        else if (dy < 0f)
        {
            var maxDistance = -dy - BlockHitbox.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(this.position.Add(BlockHitbox.xMin * 0.9f, 0), false, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMin * 0.4f, 0), false, maxDistance),
                raycast.Vertical(this.position.Add(0,                       0), false, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMax * 0.4f, 0), false, maxDistance),
                raycast.Vertical(this.position.Add(BlockHitbox.xMax * 0.9f, 0), false, maxDistance)
            );

            if(result.End().y < this.chunk.yMin)
            {
                result.anything = true;
                result.distance = Mathf.Abs(this.chunk.yMin - result.start.y) + 1;
            }

            if (result.anything == true &&
                result.distance < -dy - BlockHitbox.yMin)
            {
                this.position.y += -result.distance - BlockHitbox.yMin;
                this.velocity.y = 0f;

                SmashBreakableBlocks(result);
                ChangeState(State.Collide);
                this.map.ScreenShakeAtPosition(this.position, Vector2.down * ShakeAmount);
            }
            else
            {
                this.position.y += dy;
            }
        }
        
        if (dx > 0f)
        {
            var maxDistance = dx + BlockHitbox.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMin * 0.9f), true, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMin * 0.4f), true, maxDistance),
                raycast.Horizontal(this.position.Add(0, 0),                       true, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMax * 0.4f), true, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMax * 0.9f), true, maxDistance)
            );

            if(result.End().x > this.chunk.xMax)
            {
                result.anything = true;
                result.distance = Mathf.Abs(this.chunk.xMax - result.start.x) + 1f;
            }

            if (result.anything == true &&
                result.distance < dx + BlockHitbox.xMax)
            {
                this.position.x += result.distance - BlockHitbox.xMax;
                this.velocity.x = 0f;

                SmashBreakableBlocks(result);
                ChangeState(State.Collide);
                this.map.ScreenShakeAtPosition(this.position, Vector2.right * ShakeAmount);
            }
            else
            {
                this.position.x += dx;
            }
        }
        else if (dx < 0f)
        {
            var maxDistance = -this.velocity.y - BlockHitbox.yMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMin * 0.9f), false, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMin * 0.4f), false, maxDistance),
                raycast.Horizontal(this.position.Add(0, 0),                       false, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMax * 0.4f), false, maxDistance),
                raycast.Horizontal(this.position.Add(0, BlockHitbox.yMax * 0.9f), false, maxDistance)
            );

            if(result.End().x < this.chunk.xMin)
            {
                result.anything = true;
                result.distance = Mathf.Abs(this.chunk.xMin - result.start.x) + 1;
            }

            if (result.anything == true &&
                result.distance < -dx - BlockHitbox.xMin)
            {
                this.position.x += -result.distance - BlockHitbox.xMin;
                this.velocity.x = 0f;

                SmashBreakableBlocks(result);
                ChangeState(State.Collide);
                this.map.ScreenShakeAtPosition(this.position, Vector2.left * ShakeAmount);
            }
            else
            {
                this.position.x += dx;
            }
        }
    }

    private void SmashBreakableBlocks(Raycast.CombinedResults raycastResult)
    {
        foreach (var item in raycastResult.items)
        {
            (item as BreakableBlock)?.Break(false);
            (item as TNTBlock)?.Break();
        }
    }

    private void AdvanceCollide()
    {
        // nothing
    }

    private void AdvanceDizzy()
    {
        if(this.timer > 0)
        {
            this.timer -= 1;

            if(this.timer == 0)
            {
                ChangeState(State.Sleep);
            }
        }
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (newState)
        {
            case State.Sleep:
                EnterSleep();
                break;

            case State.Detect:
                EnterDetect();
                break;

            case State.Chosen:
                EnterChosen();
                break;

            case State.Move:
                EnterMove();
                break;

            case State.Collide:
                EnterCollide();
                break;

            case State.Dizzy:
                EnterDizzy();
                break;
        }
    }

    private void EnterSleep()
    {
        this.faceAnimated.PlayAndLoop(this.animationIdle);
        this.timer = 0;
    }

    private void EnterDetect()
    {
        //this.faceAnimated.PlayOnce(this.animationDetect);
    }

    private void EnterChosen()
    {
        this.timer = ChosenTime;
        this.faceAnimated.PlayAndLoop(this.animationSmash);
    }

    private void EnterMove()
    {
        Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxBrainboxSlide);
        this.faceAnimated.PlayAndLoop(this.animationSmash);
        this.timer = 0;
    }

    private void EnterCollide()
    {
        Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxBrainboxSlam);
        this.velocity = Vector2.zero;
        this.faceAnimated.PlayOnce(this.animationIdle,
            () => { ChangeState(State.Dizzy); }
        );
    }

    private void EnterDizzy()
    {
        this.faceAnimated.PlayAndLoop(this.animationIdle);
        this.timer = DizzyTime;
    }

}
