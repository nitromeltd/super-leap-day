
using UnityEngine;
using System;
using System.Collections;
using System.Linq;

public class BreakableBlock : Item
{
    public Animated.Animation animationNormal;
    public Animated.Animation animationBreak;
    public Animated.Animation animationDebrisLeft;
    public Animated.Animation animationDebrisRight;

    [HideInInspector] public bool destroyed;
    private int spanX;
    private int spanY;
    private Vector2 initialPosition;
    private Player.Orientation lastGravityOrientation;

    // Sprout sucking ability
    protected bool suckActive = false;
    protected int shakeTimer = 0;
    protected Vector2 smoothSuckVel;
    protected float smoothSuckTime;
    protected Vector2 shakeAmount;
    protected const int SuckShakeTime = 20;
    protected readonly float[] shakeDeltas = new float[] { 1, 1, 0, 0, -1, -1, 1, -1, 0 };

    private bool becameGold = false;

    override public void Init(Def def)
    {
        base.Init(def);

        this.initialPosition = this.position;
        this.destroyed = false;

        if      (def.tile.tile.EndsWith("1x2")) { this.spanX = 1; this.spanY = 2; }
        else if (def.tile.tile.EndsWith("2x1")) { this.spanX = 2; this.spanY = 1; }
        else if (def.tile.tile.EndsWith("2x2")) { this.spanX = 2; this.spanY = 2; }
        else                                    { this.spanX = 1; this.spanY = 1; }

        MakeSolid();

        this.onCollisionWithPlayersHead = () => Break(true);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromLeft = OnCollision;
        this.onCollisionFromRight = OnCollision;

        this.becameGold = false;
    }

    private void MakeSolid()
    {
        var hitbox = new Hitbox(
            this.spanX * -0.5f,
            this.spanX *  0.5f,
            this.spanY * -0.5f,
            this.spanY *  0.5f,
            this.rotation,
            Hitbox.Solid,
            true,
            true
        );
        this.hitboxes = new [] { hitbox };
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
            
        UpdateSuck();

        this.lastGravityOrientation = this.map.player.gravity.Orientation();
    }

    public void Break(bool brokenByPlayer)
    {
        if (this.destroyed == true) return;

        if(this.map.player.midasTouch.isActive == true && brokenByPlayer == true)
        {
            StartCoroutine(BecomeGold());
            return;
        }

        destroyed = true;
        this.hitboxes = new Hitbox[] {};

        Vector2 leftPosition  = this.position.Add(this.spanX * -0.25f, 0);
        Vector2 rightPosition = this.position.Add(this.spanX *  0.25f, 0);

        var left = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisLeft, 100, leftPosition, this.transform.parent
        );
        left.velocity     = new Vector2(-1f, 7f) / 16f;
        left.acceleration = new Vector2(0f, -0.5f) / 16f;
        left.canBeSucked = true;

        var right = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisRight, 100, rightPosition, this.transform.parent
        );
        right.velocity     = new Vector2(1f, 7f) / 16f;
        right.acceleration = new Vector2(0f, -0.5f) / 16f;
        right.canBeSucked = true;

            int countMin, countMax;
        if (this.spanX == 2 && this.spanY == 2)
            (countMin, countMax) = (11, 13);
        else if (this.spanX == 2 || this.spanY == 2)
            (countMin, countMax) = (8, 12);
        else
            (countMin, countMax) = (5, 11);

        Particle.CreateDust(
            this.transform.parent,
            countMin,
            countMax,
            this.position,
            this.spanX * 0.5f,
            this.spanY * 0.5f
        );

        var breakAnimation = Particle.CreateAndPlayOnce(
            this.animationBreak, this.position, this.transform.parent
        );
        breakAnimation.transform.localScale = new Vector3(
            this.spanX * 0.5f, this.spanY * 0.5f, 1.0f
        );

        // we need to kill enemies "above" the breakable block.
        // in zero-g, when hitting the ceiling, gravity will flip 180°.
        // when determining which direction is "above", we want to refer
        // to the gravity BEFORE this flip happens.
        // this.lastGravityOrientation lags 1 frame behind to give us this info.

        Rect damageRect = new Rect(
            this.position.x - (this.spanX * 0.5f),
            this.position.y - (this.spanY * 0.5f),
            this.spanX,
            this.spanY
        );
        switch (this.lastGravityOrientation)
        {
            case Player.Orientation.Normal:
            default:
                damageRect.yMin = damageRect.yMax;
                damageRect.yMax += 0.2f;
                break;
            case Player.Orientation.RightWall:
                damageRect.xMax = damageRect.xMin;
                damageRect.xMin -= 0.2f;
                break;
            case Player.Orientation.Ceiling:
                damageRect.yMax = damageRect.yMin;
                damageRect.yMax -= 0.2f;
                break;
            case Player.Orientation.LeftWall:
                damageRect.xMin = damageRect.xMax;
                damageRect.xMax += 0.2f;
                break;
        }
        this.map.DamageEnemy(
            damageRect,
            new Enemy.KillInfo { itemDeliveringKill = this, freezeTime = brokenByPlayer }
        );

        this.spriteRenderer.enabled = false;

        Audio.instance.PlaySfx(Assets.instance.sfxBreakBlock, position: this.position);
    }

    public override void Reset()
    {
        base.Reset();
        if (this.destroyed == true)
        {
            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(this.animationNormal);
            MakeSolid();
            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.hitboxes[0].InWorldSpace().center,
                this.transform.parent
            );
            this.destroyed = false;
        }

        if(this.becameGold == true)
        {
            this.becameGold = false;
            this.spriteRenderer.material = Assets.instance.spritesDefaultMaterial;
        }
    }
    
    private void UpdateSuck()
    {
        if(this.suckActive == false) return;

        var positionWithShake = this.position;

        positionWithShake += new Vector2(
            shakeAmount.x *
            shakeDeltas[this.map.frameNumber % shakeDeltas.Length],
            shakeAmount.y *
            shakeDeltas[this.map.frameNumber % shakeDeltas.Length]
        );
        
        this.transform.position = new Vector2(
            positionWithShake.x,
            positionWithShake.y);
    }

    public void EnterSuck()
    {
        if(this.suckActive == true) return;

        this.suckActive = true;
        this.shakeAmount = Vector2.one * 0.02f;
    }

    public void ExitSuck()
    {
        if(this.suckActive == false) return;

        this.suckActive = false;
        this.transform.position = this.initialPosition;
    }

    IEnumerator BecomeGold()
    {
        this.becameGold = true;
        this.destroyed = true;

        // kill enemies above
        Rect damageRect = new Rect(
            this.position.x - (this.spanX * 0.5f),
            this.position.y - (this.spanY * 0.5f),
            this.spanX,
            this.spanY
        );

        switch (this.map.player.gravity.Orientation())
        {
            case Player.Orientation.Normal:
            default:
                damageRect.yMin = damageRect.yMax;
                damageRect.yMax += 0.2f;
                break;
            case Player.Orientation.RightWall:
                damageRect.xMax = damageRect.xMin;
                damageRect.xMin -= 0.2f;
                break;
            case Player.Orientation.Ceiling:
                damageRect.yMax = damageRect.yMin;
                damageRect.yMax -= 0.2f;
                break;
            case Player.Orientation.LeftWall:
                damageRect.xMin = damageRect.xMax;
                damageRect.xMax += 0.2f;
                break;
        }

        this.map.DamageEnemy(
            damageRect,
            new Enemy.KillInfo { itemDeliveringKill = this, freezeTime = true }
        );

        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.white;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.black;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = new Color(255, 165, 0);
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spriteGoldTint2;
        yield return new WaitForSeconds(.5f);

        if(spanX == 1 && spanY == 1)
        {
            this.map.DropLuckyPickup(Pickup.LuckyDrop.SilverCoin, this.position, this.chunk);
        }
        else
        {
            if (UnityEngine.Random.value > .5f)
            {
                this.map.DropLuckyPickup(Pickup.LuckyDrop.SilverCoin, this.position, this.chunk);
            }
            else
            {
                this.map.DropLuckyPickup(Pickup.LuckyDrop.SilverCoin, this.position + new Vector2(0.4f, 0f), this.chunk);
                this.map.DropLuckyPickup(Pickup.LuckyDrop.SilverCoin, this.position + new Vector2(-0.4f, 0f), this.chunk);
            }
        }


        this.hitboxes = new Hitbox[] { };

        Vector2 leftPosition = this.position.Add(this.spanX * -0.25f, 0);
        Vector2 rightPosition = this.position.Add(this.spanX * 0.25f, 0);

        var left = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisLeft, 100, leftPosition, this.transform.parent
        );
        left.velocity = new Vector2(-1f, 7f) / 16f;
        left.acceleration = new Vector2(0f, -0.5f) / 16f;
        left.canBeSucked = true;

        var right = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisRight, 100, rightPosition, this.transform.parent
        );
        right.velocity = new Vector2(1f, 7f) / 16f;
        right.acceleration = new Vector2(0f, -0.5f) / 16f;
        right.canBeSucked = true;

        left.spriteRenderer.material = Assets.instance.spriteGoldTint2;
        right.spriteRenderer.material = Assets.instance.spriteGoldTint2;

        int countMin, countMax;
        if (this.spanX == 2 && this.spanY == 2)
            (countMin, countMax) = (11, 13);
        else if (this.spanX == 2 || this.spanY == 2)
            (countMin, countMax) = (8, 12);
        else
            (countMin, countMax) = (5, 11);

        Particle.CreateDust(
            this.transform.parent,
            countMin,
            countMax,
            this.position,
            this.spanX * 0.5f,
            this.spanY * 0.5f
        );

        var breakAnimation = Particle.CreateAndPlayOnce(
            this.animationBreak, this.position, this.transform.parent
        );
        breakAnimation.transform.localScale = new Vector3(
            this.spanX * 0.5f, this.spanY * 0.5f, 1.0f
        );

        this.spriteRenderer.enabled = false;

        Audio.instance.PlaySfx(Assets.instance.sfxBreakBlock, position: this.position);
    }

    private void OnCollision(Collision collision)
    {
        if (this.map.player.midasTouch.isActive == true && 
            this.becameGold == false && collision.otherPlayer != null && this.destroyed == false)
        {
            StartCoroutine(BecomeGold());
        }
    }
}
