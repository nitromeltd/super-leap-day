using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BubblePickup : Pickup
{
    public Animated.Animation idleAnimation;
    public Animated.Animation popAnimation;

    [Header("Icon")]
    public SpriteRenderer iconSprite;
    public Sprite starIcon;
    public Sprite petYellowIcon;
    public Sprite petTornadoIcon;
    public Sprite shieldIcon;
    public Sprite swordAndshieldIcon;
    public Sprite wingsIcon;
    public Sprite midasTouchIcon;
    public Sprite refereeIcon;
    public Sprite baseball;
    public Sprite ink;
    public Sprite mines1;
    public Sprite mines2;
    public Sprite mines3;
    public Sprite mines4;
    public Sprite mines5;

    private PowerupPickup.Type powerupType;

    private Vector3 scaleTarget = Vector3.one;
    private Vector3 scaleFinal = Vector3.one;
    private Vector3 scaleVelocity = Vector3.zero;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;
    private float BreathePeriod = .3f;
    private float BreatheAmplitude = .4f;
    private Vector2 breathePos;

    private bool isMoving = true;
    private bool StayStillAtStart = false;
    private bool canCollect = true;
    private int timer;
    private int stillTimer;
    private const int StillTime = 30;
    private const int TotalTime = 8 * 60;

    public bool stayOnSpot = true;
    Pickup.LuckyDrop insertedPickup;

    private bool fromReferee = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.animated.PlayAndLoop(idleAnimation);

        this.timer = TotalTime;
        this.movingFreely = false;

        if (this.stayOnSpot == true)
        {
            string pickupName;
            if (def.tile.properties?.ContainsKey("contains_powerup") == true)
                pickupName = def.tile.properties["contains_powerup"].s;
            else
                pickupName = "none";

            this.insertedPickup = Pickup.GetPickupByPropertyName(pickupName);
            if (insertedPickup == Pickup.LuckyDrop.None)
                insertedPickup = Pickup.LuckyDrop.BubblePowerupInvincibility;

            switch (this.insertedPickup)
            {
                case Pickup.LuckyDrop.BubblePowerupInvincibility:
                    this.iconSprite.sprite = this.starIcon;
                    break;

                case Pickup.LuckyDrop.BubblePowerupYellowPet:
                    this.iconSprite.sprite = this.petYellowIcon;
                    break;

                case Pickup.LuckyDrop.BubblePowerupTornadoPet:
                    this.iconSprite.sprite = this.petTornadoIcon;
                    break;

                case Pickup.LuckyDrop.BubblePowerupShield:
                    this.iconSprite.sprite = this.shieldIcon;
                    break;
            }
            this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

            this.hitboxes = new[] {
                new Hitbox(-.8f, .8f, -.8f, .8f, this.rotation, Hitbox.NonSolid)
            };

            this.StayStillAtStart = false;
            this.stillTimer = StillTime;
            this.isMoving = true;

            this.scaleTarget = new Vector3(0.8f, 0.8f, 1f);
            this.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            this.transform.Find("Icon").localScale = new Vector3(1.3f, 1.3f, 1);

            BreatheAmplitude = .2f;
        }
    }

    public static BubblePickup Create(Vector2 position, Chunk chunk, PowerupPickup.Type type, bool fromReferee = false)
    {
        var obj = Instantiate(Assets.instance.bubble, chunk.transform);
        obj.name = "Spawned Bubble Power-up";
        var item = obj.GetComponent<BubblePickup>();
        item.Init(position, chunk, type, fromReferee);
        chunk.items.Add(item);
        return item;
    }

    public static BubblePickup CreateRandomPowerup(Vector2 position, Chunk chunk)
    {
        List<PowerupPickup.Type> UpdatedBubblePowerUps = PowerupPickup.BubblePickups;

        if (SaveData.IsInfiniteJumpPurchased() == true)
        {
            UpdatedBubblePowerUps = UpdatedBubblePowerUps.Append(PowerupPickup.Type.BubbleInfiniteJump).ToList();
        }
        if (SaveData.IsSwordAndShieldPurchased() == true)
        {
            UpdatedBubblePowerUps = UpdatedBubblePowerUps.Append(PowerupPickup.Type.BubbleSwordAndShield).ToList();
        }
        if (SaveData.IsMidasTouchPurchased() == true)
        {
            UpdatedBubblePowerUps = UpdatedBubblePowerUps.Append(PowerupPickup.Type.BubbleMidasTouch).ToList();
        }

        return Create(position, chunk, UpdatedBubblePowerUps[Random.Range(0, UpdatedBubblePowerUps.Count)]);
    }

    public void Init(Vector2 position, Chunk chunk, PowerupPickup.Type type, bool fromReferee)
    {
        this.stayOnSpot = false;
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;

        this.powerupType = type;

        this.fromReferee = fromReferee;
        if (fromReferee == false)
        {
            this.hitboxes = new[] {
                new Hitbox(-.8f, .8f, -.8f, .8f, this.rotation, Hitbox.NonSolid)
            };

            this.StayStillAtStart = true;
            this.stillTimer = StillTime;
            this.isMoving = false;

            this.scaleFinal = new Vector3(0.8f, 0.8f, 1f);
            this.transform.localScale = new Vector3(0.3f, 0.3f, 1);
            this.transform.Find("Icon").localScale = new Vector3(1.3f, 1.3f, 1);
            StartCoroutine(_SquashStretch(new Vector3(0.3f, 0.3f, 1f), .05f));

            switch (this.powerupType)
            {
                case PowerupPickup.Type.BubbleInvincibility:
                    this.iconSprite.sprite = this.starIcon;
                    break;

                case PowerupPickup.Type.BubblePetYellow:
                    this.iconSprite.sprite = this.petYellowIcon;
                    break;

                case PowerupPickup.Type.BubblePetTornado:
                    this.iconSprite.sprite = this.petTornadoIcon;
                    break;

                case PowerupPickup.Type.BubbleShield:
                    this.iconSprite.sprite = this.shieldIcon;
                    break;

                case PowerupPickup.Type.BubbleSwordAndShield:
                    this.iconSprite.sprite = this.swordAndshieldIcon;
                    break;

                case PowerupPickup.Type.BubbleMidasTouch:
                    this.iconSprite.sprite = this.midasTouchIcon;
                    break;

                case PowerupPickup.Type.BubbleInfiniteJump:
                    this.iconSprite.sprite = this.wingsIcon;
                    break;

                case PowerupPickup.Type.BubbleMultiplayerBaseball:
                    this.iconSprite.sprite = this.baseball;
                    break;

                case PowerupPickup.Type.BubbleMultiplayerInk:
                    this.iconSprite.sprite = this.ink;
                    break;

                case PowerupPickup.Type.BubbleMultiplayerMines:
                    this.iconSprite.sprite = this.mines5;
                    break;
            }
        }
        else
        {
            this.iconSprite.sprite = this.refereeIcon;
            this.iconSprite.transform.localScale = new Vector3(0.5f, 0.5f, 1);
            this.hitboxes = new[] {
                new Hitbox(-1f, 1f, -1f, 1f, this.rotation, Hitbox.NonSolid)
            };
            this.scaleFinal = new Vector3(1f, 1f, 1f);
            this.transform.localScale = this.scaleTarget;
        }
    }

    override public void Advance()
    {
        if(stayOnSpot == true)
        {
            base.Advance();
        }

        this.transform.localScale = Vector3.SmoothDamp(this.transform.localScale,
            this.scaleTarget, ref scaleVelocity, .05f);

        SwitchToNearestChunk();
        CheckCollect();

        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;

        if(this.controlledByOtherItem == true)
        {
            theta = 0f;
        }

        float distance = BreatheAmplitude * Mathf.Sin(theta);
        this.breathePos = this.breatheMovement ? Vector2.right * distance : Vector2.zero;

        if (this.StayStillAtStart == true)
        {
            this.stillTimer -= 1;
            if(this.stillTimer <= 0)
            {
                this.StayStillAtStart = false;
                this.isMoving = true;
            }
            this.transform.position = this.position + breathePos;
        }

        if(this.isMoving == true)
        {
            if(this.stayOnSpot == false)
            {
                this.position += new Vector2(0, 0.02f);
            }
            this.transform.position = this.position + breathePos;
        }

        if(this.StayStillAtStart == false && this.timer > 0 && this.stayOnSpot == false)
        {
            this.timer -= 1;
            if (this.timer <= 0 )
            {
                Pop();
            }
        }

        if(this.map.player.alive == false && this.stayOnSpot == false)
        {
            Pop();
        }
    }

    private System.Collections.IEnumerator _SquashStretch(Vector2 squashScale, float time)
    {
        this.scaleTarget = new Vector3(squashScale.x , squashScale.y, 1f);
        yield return new WaitForSeconds(time);
        this.scaleTarget = scaleFinal;
    }

    public override void Collect()
    {
        if (this.collected == true || this.canCollect == false) return;

        Audio.instance.PlaySfx(Assets.instance.sfxFruit);
        this.isMoving = false;

        this.collected = true;

        if (this.animated == true)
        {
            this.animated.PlayOnce(this.animationCollect, delegate
            {
                this.spriteRenderer.enabled = false;
            });
        }

        this.animated.PlayOnce(popAnimation, () =>
        {
            if(this.stayOnSpot == false)
            {
                this.chunk.items.Remove(this);
                Destroy(this.gameObject);
            }
            else
            {
                this.spriteRenderer.enabled = false;
            }
        });

        this.animated.OnFrame(3, delegate
        {
            this.transform.Find("Icon").gameObject.SetActive(false);
            if(this.stayOnSpot == false)
            {
                switch (this.powerupType)
                {
                    case PowerupPickup.Type.BubbleInvincibility:
                        this.map.player.invincibility.Activate(activatedByBubble: true, activatedByReferee: fromReferee);
                        break;

                    case PowerupPickup.Type.BubblePetYellow:
                        this.map.player.petYellow.Activate(activatedByBubble: true, bubbleTransform: this.transform);
                        break;

                    case PowerupPickup.Type.BubblePetTornado:
                        this.map.player.petTornado.Activate(activatedByBubble: true, bubbleTransform: this.transform);
                        break;

                    case PowerupPickup.Type.BubbleShield:
                        this.map.player.shield.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleInfiniteJump:
                        this.map.player.infiniteJump.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleMidasTouch:
                        this.map.player.midasTouch.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleSwordAndShield:
                        this.map.player.swordAndShield.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleMultiplayerBaseball:
                        this.map.player.baseball.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleMultiplayerInk:
                        this.map.player.ink.Activate(activatedByBubble: true);
                        break;

                    case PowerupPickup.Type.BubbleMultiplayerMines:
                        this.map.player.mines.Activate(activatedByBubble: true, 5);
                        break;
                }
            }
            else
            {
                switch (this.insertedPickup)
                {
                    case Pickup.LuckyDrop.BubblePowerupInvincibility:
                        this.map.player.invincibility.Activate(activatedByBubble: true);
                        break;

                    case Pickup.LuckyDrop.BubblePowerupYellowPet:
                        this.map.player.petYellow.Activate(activatedByBubble: true, bubbleTransform: this.transform);
                        break;

                    case Pickup.LuckyDrop.BubblePowerupTornadoPet:
                        this.map.player.petTornado.Activate(activatedByBubble: true, bubbleTransform: this.transform);
                        break;

                    case Pickup.LuckyDrop.BubblePowerupShield:
                        this.map.player.shield.Activate(activatedByBubble: true);
                        break;
                }
            }
        });
    }

    public void Pop()
    {
        this.canCollect = false;
        this.animated.PlayOnce(popAnimation, () => 
        {
            this.chunk.items.Remove(this);
            Destroy(this.gameObject);
        });
        this.animated.OnFrame(3, delegate
        {
            this.transform.Find("Icon").gameObject.SetActive(false);
            this.isMoving = false;
        });
    }

    public override void CheckCollect()
    {
        if (this.disallowCollectTime > 0)
        {
            this.disallowCollectTime -= 1;
        }
        else if (this.collected == false && this.canPlayerCollect == true)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = this.map.player.Rectangle();

            if (thisRect.Overlaps(playerRect))
                Collect();
        }
    }

    public override void ChangeSorting(string layerName, int orderInLayer)
    {
        base.ChangeSorting(layerName, orderInLayer);

        this.iconSprite.sortingLayerName = layerName;
        this.iconSprite.sortingOrder = orderInLayer;
    }
}

