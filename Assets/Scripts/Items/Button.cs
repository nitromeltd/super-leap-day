
using UnityEngine;
using System.Linq;

public class Button : Item
{
    public Animated.Animation animationUp;
    public Animated.Animation animationDown;
    private bool pressed;
    private bool pressedThisFrame;

    public bool Pressed { get => pressed; }
    public bool PressedThisFrame { get => pressedThisFrame; }

    override public void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, 0, 0.5f, this.rotation, Hitbox.NonSolid)
        };
        this.pressed = false;
        this.pressedThisFrame = false;

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    override public void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        var pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if (this.pressed == true)
        {
            this.animated.PlayAndHoldLastFrame(this.animationDown);
            this.pressedThisFrame = (pressedLastFrame == false);
        }
        else
        {
            this.animated.PlayAndHoldLastFrame(this.animationUp);
            this.pressedThisFrame = false;
        }

        if (pressedLastFrame == false && this.pressed == true)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);
        }

        if (pressedLastFrame == true && this.pressed == false)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxButtonUnpress, position:this.position);
        }
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
                return true;
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        Vector2[] SensorsWhenHorizontal() => new []
        {
            new Vector2(hitboxRect.xMin,     hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.xMax,     hitboxRect.center.y)
        };
        Vector2[] SensorsWhenVertical() => new []
        {
            new Vector2(hitboxRect.center.x, hitboxRect.yMin),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.yMax)
        };

        var sensors =
            (this.rotation == 0 || this.rotation == 180) ?
            SensorsWhenHorizontal() :
            SensorsWhenVertical();

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, 0, 0.5f, this.rotation, Hitbox.NonSolid)
        };
    }
}
