
using UnityEngine;
using System;
using System.Collections.Generic;

public class ButtonTriggerPlatform : Item
{
    public enum ButtonType
    {
        Button_1x1,
        Button_3x3
    }
    public ButtonType buttonType;
    public Animated.Animation animationActive;
    public Animated.Animation animationInactive;

    private NitromeEditor.Path path;
    [NonSerialized] public Button[] buttons;
    private int targetIndex;
    private bool forwards = true;
    private float along = 0;
    private float speed = 0;
    private bool isActive = false;
    private bool wasActiveLastFrame = false;
    private int noSpeedFrames = ACTIVE_SPEED_FRAMES;
    private float speedLastFrame = 0;
    private bool Hascollided = true;

    // minimum amount of frames with no speed to consider the block inactive
    private const int ACTIVE_SPEED_FRAMES = 20;

    public override void Init(Def def)
    {
        base.Init(def);

        this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty);


        if(buttonType == ButtonType.Button_1x1)
        {
            this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)};
        }
        else
        {
            this.hitboxes = new[] {
            new Hitbox(-1.5f, 1.5f, -1.5f, 1.5f, this.rotation, Hitbox.Solid, true, true)};
        }

        var nearestPathNode = this.chunk.pathLayer.NearestNodeTo(this.position);
        if (nearestPathNode != null)
        {
            this.path = nearestPathNode.path;
            if (this.path.nodes.Length < 2)
                this.path = null;
        }

        var buttons = new List<Button>();
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                var button = this.chunk.NearestItemTo<Button>(otherNode.Position);
                buttons.Add(button);
            }
        }
        this.buttons = buttons.ToArray();

        this.targetIndex = this.path?.NearestNodeIndexTo(this.position) ?? 0;
        this.along = this.path?.DistanceAtNodeIndex(this.targetIndex) ?? 0;

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }

    public override void Advance()
    {
        if (this.path == null || this.buttons.Length == 0) return;

        var pressed = false;
        foreach (var button in this.buttons)
        {
            if (button.PressedThisFrame == true)
                pressed = true;
        }

        if (pressed == true)
        {
            ButtonPressed();
        }

        {
            float targetAlong = 0;
            for (var e = 0; e < this.targetIndex; e += 1)
                targetAlong += this.path.edges[e].distance;

            if (this.along < targetAlong)
            {
                this.speed += 0.2f / 16;
                this.along += this.speed;
                if (this.along >= targetAlong)
                {
                    this.along = targetAlong - 0.01f;
                    this.speed = Mathf.Abs(this.speed) * -0.4f;
                }
            }
            else if (this.along > targetAlong)
            {
                this.speed -= 0.2f / 16;
                this.along += this.speed;
                if (this.along <= targetAlong)
                {
                    this.along = targetAlong + 0.01f;
                    this.speed = Mathf.Abs(this.speed) * 0.4f;
                }
            }

            this.position = this.path.PointAtDistanceAlongPath(this.along).position;
            this.transform.position = this.position;
        }

        bool isCloseToPathEndPoint = this.along < .5f ||
            this.along > this.path.length - .5f;

        if(Mathf.Abs(this.speed) > .1f)
        {
            this.noSpeedFrames = 0;
        }
        else
        {
            this.noSpeedFrames++;
        }

        if(this.noSpeedFrames > ACTIVE_SPEED_FRAMES)
        {
            this.noSpeedFrames = ACTIVE_SPEED_FRAMES;
        }

        this.isActive = (this.noSpeedFrames < ACTIVE_SPEED_FRAMES);

        if(!isCloseToPathEndPoint && !this.isActive)
        {
            this.isActive = true;
        }

        if(this.isActive && !this.wasActiveLastFrame)
        {
            this.animated.PlayAndHoldLastFrame(this.animationActive);
        }

        if(!this.isActive && this.wasActiveLastFrame)
        {
            this.animated.PlayAndHoldLastFrame(this.animationInactive);
            
        }

        if(this.isActive && this.speedLastFrame * this.speed < 0 && !this.Hascollided)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvMovingBlockImpactLarge,
                position: this.position
            );
            this.Hascollided = true;
        }

        this.wasActiveLastFrame = this.isActive;
        this.speedLastFrame = this.speed;           
    }

    private void ButtonPressed()
    {
        if (this.targetIndex == 0)
        {
            this.targetIndex += 1;
            this.forwards = true;
        }
        else if (this.targetIndex == this.path.nodes.Length - 1)
        {
            this.targetIndex -= 1;
            this.forwards = false;
        }
        else
        {
            this.targetIndex += forwards ? 1 : -1;
        }
        this.Hascollided = false;
    }

    private void OnDestroy()
    {
        if(this.gameObject.scene.isLoaded == false) return;

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }
}
