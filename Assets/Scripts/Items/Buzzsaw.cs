
public class Buzzsaw : Item
{
    private NitromeEditor.Path path;
    private float startingTime;

    public override void Init(Def def)
    {
        base.Init(def);

        this.path = this.chunk.pathLayer.NearestPathTo(this.position);
        this.startingTime = this.path.NearestPointTo(this.position).Value.timeSeconds;
    }

    public override void Advance()
    {
        base.Advance();

        var time = (this.map.frameNumber / 60.0f) + this.startingTime;
        this.transform.position = this.position = this.path.PointAtTime(time).position;

        if ((this.position - this.map.player.position).sqrMagnitude < 32 * 32)
        {
            this.map.player.Hit(this.position);
        }
    }
}
