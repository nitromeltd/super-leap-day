﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCTVCamera : Item
{
    private enum CameraFacingDirection
    {
        Right,
        Left,
        Down
    }
    
    public Animated cameraHeadAnimated;
    public Animated.Animation animationSurprise;
    public CameraCone cameraCone;
    public GameObject cameraHead;
    public bool coneShouldPauseWhenChangingDirection;
    private Item connectedItem;
    private bool alerted;
    private int alertDelay;
    private int spotDelay;
    private bool flipped;
    
    public override void Init(Def def)
    {
        base.Init(def);

        if(!cameraCone.fixedCone)
        {
            //by default camera is searching
            this.cameraCone.SetMinMaxAngles(-80, 45, this.coneShouldPauseWhenChangingDirection);
        }

        this.flipped = def.tile.flip;
        if (this.flipped)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }

        if (rotation == 90)
        {
            this.cameraCone.SetMinMaxAngles(-80, 80, this.coneShouldPauseWhenChangingDirection);
        }

        if (rotation == 270)
        {
            transform.rotation = Quaternion.Euler(0, 0, -90);
            this.cameraCone.SetMinMaxAngles(-80, 80, this.coneShouldPauseWhenChangingDirection);
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        var node = this.chunk.connectionLayer.NearestNodeTo(this.position, 2.5f);
        if (node != null)
        {
            var index = Array.IndexOf(node.path.nodes, node);
            var otherIndex = index == 0 ? node.path.nodes.Length - 1 : 0;
            var otherNode = node.path.nodes[otherIndex];
            this.connectedItem = this.chunk.NearestItemTo<Item>(otherNode.Position);
        }
        this.alerted = false;
        this.alertDelay = UnityEngine.Random.Range(20, 60);
        this.spotDelay = 0;
        this.cameraCone.Init(this.map, this);
        this.cameraCone.SetCallbacks(shouldCameraActivate: ShouldCameraActivate, onPlayerSpotted: ActivateAlarm);
        UpdateCCTVSettings();
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        this.cameraCone.Advance();

        if (!this.alerted)
        {
            if (this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - alertDelay)
            {
                this.alerted = true;
                ShowExclamationMark();
                this.cameraHeadAnimated.PlayAndHoldLastFrame(this.animationSurprise);
            }
        }
        else
        {
            if (!this.map.desertAlertState.IsGloballyActive())
            {
                this.alerted = false;
                DeactivateConnectedItem();
            }
        }

        if(this.spotDelay > 0)
            this.spotDelay--;
    }

    private void UpdateCCTVSettings()
    {
        NitromeEditor.Path path;
        path = this.chunk.pathLayer.NearestNodeTo(
            new Vector2(
                this.transform.position.x,
                this.transform.position.y
            ),
            1
        )?.path;

        if (path != null && path.nodes.Length < 2)
                path = null;

        if (path != null && path.closed)
        {
            float angle1, angle2;
            Vector3 fromVector;

            switch (this.rotation)
            {
                case 90: fromVector = Vector3.up; break;
                case 180: fromVector = (this.flipped ? Vector3.right : Vector3.left); break;
                case 270: fromVector = Vector3.down; break;
                default:
                case 0: fromVector = (this.flipped ? Vector3.left : Vector3.right); break;
            }

            angle1 = (Vector3.SignedAngle(fromVector, path.nodes[1].Position - path.nodes[0].Position, Vector3.forward));
            angle2 = (Vector3.SignedAngle(fromVector, path.nodes[2].Position - path.nodes[0].Position, Vector3.forward));

            (float, float) v1 = (angle1, path.nodes[1].arcLength * 60);
            (float, float) v2 = (angle2, path.nodes[2].arcLength * 60);

            if (v1.Item1 >= v2.Item1)
            {
                this.cameraCone.SetMinMaxAngles(v2.Item1, v1.Item1, this.coneShouldPauseWhenChangingDirection);
                this.cameraCone.SetCameraTurnDelays(
                                                    upToDownDelay:(v1.Item2 == 0) ? 60 : (int)v1.Item2, 
                                                    downToUpDelay:(v2.Item2 == 0) ? 60 : (int)v2.Item2
                                                    );

            }
            else
            {
                this.cameraCone.SetMinMaxAngles(v1.Item1, v2.Item1, this.coneShouldPauseWhenChangingDirection);

                this.cameraCone.SetCameraTurnDelays(
                                                    upToDownDelay:(v2.Item2 == 0) ? 60 : (int)v2.Item2,
                                                    downToUpDelay:(v1.Item2 == 0) ? 60 : (int)v1.Item2
                                                    );
            } 
        }
    }

    private void ShowExclamationMark()
    {
        var exclamationMark = Particle.CreateAndPlayOnce(
                    Assets.instance.exclamationMarkAnimation,
                    ((Vector2)this.cameraHead.transform.position).Add(
                        0,
                        2f
                    ),
                    this.cameraHead.transform
                );

        exclamationMark.transform.localPosition = new Vector2(0, 2.2f);
        exclamationMark.transform.rotation = Quaternion.Euler(0, 0, this.cameraHead.transform.rotation.eulerAngles.z);
        exclamationMark.spriteRenderer.sortingLayerName = "Items (Front)";
        exclamationMark.spriteRenderer.sortingOrder = -1;
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnemyAlert,
            position: this.position
        );
    }

    private void ActivateAlarm()
    {
        if (this.map.player.state == Player.State.InsideBox)
        {
            if (this.map.player.GetBox() != null)
            {
                if (!this.map.player.GetBox().IsPlayerDetectableInBox())
                {
                    return;
                }
                else
                {
                    this.map.player.ExitBox();
                }
            }

            if (this.map.player.GetHidingPlace() != null)
            {
                if (!this.map.player.GetHidingPlace().IsPlayerDetectableInHidingPlace())
                {
                    return;
                }
                else
                {
                    this.map.player.ExitHidingPlace();
                }
            }
        }

        this.alerted = true;
        this.map.desertAlertState.ActivateAlert(this.cameraHead.transform.position);
        
        if(this.spotDelay == 0)
        {
            ShowExclamationMark();
            this.spotDelay = 60;
            this.cameraHeadAnimated.PlayAndHoldLastFrame(this.animationSurprise);
        }
        ActivateConnectedItem();
    }

    private void ActivateConnectedItem()
    {
        if(connectedItem == null)
        {
            return;
        }

        if(connectedItem is Conveyor)
        {
            ((Conveyor)connectedItem).StopConveyor();
        }
    }

    private void DeactivateConnectedItem()
    {
        if(connectedItem == null)
        {
            return;
        }

        if(connectedItem is Conveyor)
        {
            ((Conveyor)connectedItem).StartConveyor();
        }
    }

    private bool ShouldCameraActivate()
    {
        if ((this.map.desertAlertState.IsGloballyActive() && this.map.desertAlertState.alertTimerInFrames < Map.DesertAlertState.DefaultAlarmDurationInFrames - this.alertDelay) || this.alerted)
        {
            return true;
        }
        return false;
    }
}