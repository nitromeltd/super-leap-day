﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCTVCameraDoor : Item
{
    public int spanX;
    public int spanY;

    public override void Init(Def def)
    {
        base.Init(def);
        MakeSolid();
    }

    public override void Advance()
    {
        base.Advance();
    }

    public void Activate()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        this.hitboxes = new Hitbox[] {};
    }

    public void Deactivate()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        MakeSolid();
    }

    private void MakeSolid()
    {
        var hitbox = new Hitbox(
            this.spanX * -0.5f,
            this.spanX *  0.5f,
            this.spanY * -0.5f,
            this.spanY *  0.5f,
            this.rotation,
            Hitbox.Solid,
            true,
            true
        );
        this.hitboxes = new [] { hitbox };
    }
}
