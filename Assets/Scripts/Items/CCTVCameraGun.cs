﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class CCTVCameraGun : Item
{

    public Animated.Animation animationPrepareAndFire;
    private bool alerted;

    public override void Init(Def def)
    {
        base.Init(def);
        if(def.tile.flip)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);

            switch(this.rotation)
            {
                case 0:
                    this.rotation = 180;
                    break;
                case 90:
                    this.rotation = 270;
                    break;
                case 180:
                    this.rotation = 0;
                    break;
                case 270:
                    this.rotation = 90;
                    break;
                default:
                    break;
            }            
        }

        this.hitboxes = new[] {
            new Hitbox(-1.0f, 0.0f, -1.0f, 1.0f, this.rotation, Hitbox.Solid)
        };
        alerted = false;
    }

    public override void Advance()
    {
        // DebugDrawing.Draw(false, true); 
        base.Advance();

        if(!this.alerted)
        {
            if(this.map.desertAlertState.IsActive(this.position))
            {
                this.alerted = true;
                PrepareAndFireMissile();
            }
        }
        else
        {
            if(!this.map.desertAlertState.IsActive(this.position))
            {
                this.alerted = false;
            }
        }
    }

    public void Activate()
    {
        PrepareAndFireMissile();
    }

    private void PrepareAndFireMissile()
    {
        this.animated.PlayOnce(this.animationPrepareAndFire);
        this.animated.OnFrame(24, CreateFiringExplosion);
        this.animated.OnFrame(27, FireMissile);
    }

    private void CreateFiringExplosion()
    {
        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position + 0.5f * Forward(),
            this.transform.parent
        );
        
        p.spriteRenderer.sortingLayerName = "Player Trail (AL)";
    }

    private void FireMissile()
    {
        Vector2 offset = Vector2.right;
        var forward = Forward();
        var missile = HomingMissile.Create(
                this.position + (forward * 1.5f),
                this.chunk,
                this.rotation,
                ChildRocketDestroyed
            );
        missile.velocity = forward * 4f/16;
    }

    public void Deactivate()
    {
        
    }

    void ChildRocketDestroyed()
    {
        if (this.map.player.alive && this.map.desertAlertState.IsActive(this.position))
        {
            PrepareAndFireMissile();
        }        
    }

    private Vector2 Forward() 
    {
        return new Vector2(Mathf.Cos(this.rotation * Mathf.Deg2Rad), Mathf.Sin(this.rotation * Mathf.Deg2Rad));
    }
}
