﻿using UnityEngine;

public class CCTVGateBlock : Item
{
    public GateType gateType;
    public Animated.Animation closeAnimation;
    public Animated.Animation openAnimation;

    public enum GateType
    {
        Horizontal,
        Vertical,
        Corner
    }

    private enum State
    {
        Open,
        Closed,
    }

    private State state;
    private int? timeToOpenNeighbours;
    private int? timeToCloseNeighbours;

    public override void Init(Def def)
    {
        base.Init(def);
        this.state = State.Open;

        if (def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        this.hitboxes = new Hitbox[0];
        this.timeToCloseNeighbours = this.timeToOpenNeighbours = null;

        if(this.gateType == GateType.Corner)
        {
            MakeSolid();
        }
         AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void MakeSolid()
    {
        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true),
        };
    }

    public override void Advance()
    {
        switch (this.state)
        {
            case State.Open:
                {
                    AdvanceOpen();
                    break;
                }

            case State.Closed:
                {
                    AdvanceClose();
                    break;
                }

            default:
                break;
        }

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
            
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.P) && this.state == State.Open && gateType == GateType.Corner)
        {
            this.state = State.Closed;
            StartClosingDoor();
        }
#endif

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.O) && this.state == State.Closed && gateType == GateType.Corner)
        {
            this.state = State.Open;
            StartOpeningDoor();
        }
#endif
    }

    private void AdvanceOpen()
    {
        if (this.gateType == GateType.Corner)
        {
            if (this.state == State.Open)
            {
                if (this.map.desertAlertState.IsGloballyActive())
                {
                    StartClosingDoor();
                    this.state = State.Closed;                     
                }
                else
                {
                    StartOpeningDoor();
                }
            }
        }

        OpenNeighbours();
    }

    private void AdvanceClose()
    {
        if (this.gateType == GateType.Corner)
        {
            if (this.state == State.Closed)
            {
                if (!this.map.desertAlertState.IsGloballyActive())
                {
                    StartOpeningDoor();
                    this.state = State.Open;                     
                }
                else
                {
                    StartClosingDoor();
                }
            }
        }

        CloseNeighbours();
    }

    private void StartOpeningDoor()
    {
        foreach (var block in this.chunk.subsetOfItemsOfTypeCCTVGateBlock)
        {
            if (block.IsOpen())
                continue;

            var dx = block.def.tx - this.def.tx;
            var dy = block.def.ty - this.def.ty;
            if (dx == 0 && dy == 1 ||
                dy == 0 && dx == -1)
            {
                block.Open();
            }
        }
    }

    private void StartClosingDoor()
    {
        foreach (var block in this.chunk.subsetOfItemsOfTypeCCTVGateBlock)
        {
            if (block.IsClosed())
                continue;

            var dx = block.def.tx - this.def.tx;
            var dy = block.def.ty - this.def.ty;
            if (dx == 0 && dy == -1 ||
                dy == 0 && dx == 1)
            {
                block.Close();
            }
        }
    }

    public void Close()
    {
        if (this.state == State.Closed || this.gateType == GateType.Corner)
            return;

        this.state = State.Closed;
        this.spriteRenderer.enabled = true;
        this.animated.PlayOnce(this.closeAnimation, () =>
        {
        });

        // Particle.CreateDust(this.transform.parent, 5, 7,
        // this.hitboxes[0].InWorldSpace().center, .5f, .5f);

        MakeSolid();
        this.timeToCloseNeighbours = 13;
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .3f);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvGateBlock,
            options: new Audio.Options(1f, true, .02f),
            position: this.position 
        );
    }

    public void Open()
    {
        if (this.state == State.Open || this.gateType == GateType.Corner)
            return;
        this.state = State.Open;
        this.animated.PlayOnce(this.openAnimation, () =>
        {
            this.spriteRenderer.enabled = false;
        });

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.hitboxes[0].InWorldSpace().center, .5f, .5f);
        MakeSolid();
        this.hitboxes = new Hitbox[0];
        this.timeToOpenNeighbours = 13;
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .3f);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvGateBlock,
            options: new Audio.Options(1f, true, .02f),
            position: this.position
        );
    }

    private void CloseNeighbours()
    {
        if (this.timeToCloseNeighbours.HasValue &&
            this.timeToCloseNeighbours > 0)
        {
            this.timeToCloseNeighbours -= 1;
            if (this.timeToCloseNeighbours == 0)
            {
                foreach (var block in this.chunk.subsetOfItemsOfTypeCCTVGateBlock)
                {
                    if (block.IsClosed())
                        continue;

                    var dx = block.def.tx - this.def.tx;
                    var dy = block.def.ty - this.def.ty;
                    if (dx == 0 && Mathf.Abs(dy) == 1 ||
                        dy == 0 && Mathf.Abs(dx) == 1)
                    {
                        block.Close();
                    }
                }
            }
        }
    }

    private void OpenNeighbours()
    {
        if (this.timeToOpenNeighbours.HasValue &&
            this.timeToOpenNeighbours > 0)
        {
            this.timeToOpenNeighbours -= 1;
            if (this.timeToOpenNeighbours == 0)
            {
                foreach (var block in this.chunk.subsetOfItemsOfTypeCCTVGateBlock)
                {
                    if (block.IsOpen())
                        continue;

                    var dx = block.def.tx - this.def.tx;
                    var dy = block.def.ty - this.def.ty;
                    if (dx == 0 && Mathf.Abs(dy) == 1 ||
                        dy == 0 && Mathf.Abs(dx) == 1)
                    {
                        block.Open();
                    }
                }
            }
        }
    }

    public bool IsOpen()
    {
        return this.state == State.Open;
    }

    public bool IsClosed()
    {
        return this.state == State.Closed;
    }
}
