﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CameraCone : MonoBehaviour
{
    public Map map;
    public float coneCenterlineAngle;         
    public float coneFieldOfViewAngle;
    public float insideNotchDistance = 1f;
    private float maxDetectionDistance = 12; 
    public GameObject cameraSpriteObject;   // object to rotate
    public MeshRenderer meshRenderer;
    public Mesh mesh;

    public delegate bool CameraConeActivationDelegate();
    private CameraConeActivationDelegate shouldCameraActivate;
    private Action onPlayerSpotted = null;
    private Action onPlayerOutsideOfCone = null;
    
    public bool fixedCone;
    public float itemRotation;
    private Color currentConeColor = ColorIdle;

    public static Color ColorIdle = new Color(1f, 1f,0f, 0.4f);
    public static Color ColorAlerted = new Color(1f, 0.1f, 0.1f, 0.4f);

    private enum Direction  //search movement direction
    {
        Up,
        Down,
    };   

    private class SearchMovement
    {
        public float minAngle;
        public float maxAngle; 
        public float searchRotationSpeed;
        public Direction searchRotationDirection = Direction.Up;

        public int directionSwitchPauseCounter;
        public bool shouldPauseWhenSwitchingDirection;
        public bool isCurrentlySearching;
        public int upToDownDelay = 60;
        public int downToUpDelay = 60;

        public SearchMovement(float minAngle, float maxAngle, float searchRotationSpeed = 0.5f, Direction searchRotationDirection = Direction.Up, bool shouldPauseWhenSwitchingDirection = true, bool isSearching = true)
        {
            this.minAngle = minAngle;
            this.maxAngle = maxAngle;
            this.searchRotationSpeed = searchRotationSpeed;
            this.searchRotationDirection = searchRotationDirection;
            this.shouldPauseWhenSwitchingDirection = shouldPauseWhenSwitchingDirection;
            this.isCurrentlySearching = isSearching;
        }
    }

    private SearchMovement searchMovement;   

    void Start()
    {
        
    }

    public void Init(Map map, Item item = null)
    {
        this.map = map;

        if(item)
        {
            itemRotation = item.rotation;
        }

        var gameObject = new GameObject("Mesh");
        gameObject.layer = this.map.Layer();
        gameObject.transform.parent = this.transform;
        gameObject.transform.localPosition = Vector3.zero;
        var meshFilter = gameObject.AddComponent<MeshFilter>();
        this.meshRenderer = gameObject.AddComponent<MeshRenderer>();
        this.meshRenderer.material = Assets.instance.spritesDefaultMaterial;
        this.meshRenderer.sortingLayerName = "Enemies";
        this.meshRenderer.sortingOrder = -1;
        meshFilter.mesh = this.mesh = new Mesh();
    }

    public void Advance()
    {
        UpdateConeRotation();
        SetConeBoundaryRays();
        CheckPlayerVisibility();
        CheckConeColor();
        RecalculateAndUpdateConeMesh();
    }

    public void RecalculateAndUpdateConeMesh()
    {
        float topRayAngle = GetTopAngleFlipAdjusted();
        float bottomRayAngle = GetBottomAngleFlipAdjusted();

        Vector2 offset = Vector3.zero;

        var t = Time.realtimeSinceStartup;
        var result = this.map.lineOfSightCone.Calculate(
            this.map,
            this.transform.TransformPoint(offset),
            bottomRayAngle,
            topRayAngle,
            maxDetectionDistance,
            insideNotchDistance
        );
        // Debug.Log(Time.realtimeSinceStartup - t);

        this.meshRenderer.transform.localPosition = offset;

        var vertices = new List<Vector3>();
        var uvs = new List<Vector2>();
        var colors = new List<Color>();
        var indices = new List<int>();

        foreach (var slice in result.slices)
        {
            var firstVertex = vertices.Count;
            vertices.Add(slice.insideFromPosition - result.start);
            vertices.Add(slice.insideToPosition - result.start);
            vertices.Add(slice.fromPosition - result.start);
            vertices.Add(slice.toPosition - result.start);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            colors.Add(this.currentConeColor);
            colors.Add(this.currentConeColor);
            colors.Add(this.currentConeColor);
            colors.Add(this.currentConeColor);
            indices.Add(firstVertex + 0);
            indices.Add(firstVertex + 1);
            indices.Add(firstVertex + 2);
            indices.Add(firstVertex + 2);
            indices.Add(firstVertex + 1);
            indices.Add(firstVertex + 3);
        }
 
        var outlineColor = this.currentConeColor;
        outlineColor.a = 0.8f;

        for (var n = 0; n < result.border.Length; n += 1)
        {
            var firstVertex = vertices.Count;
            var a = result.border[n];
            var b = result.border[(n + 1) % result.border.Length];
            vertices.Add(a.outer - result.start);
            vertices.Add(a.inner - result.start);
            vertices.Add(b.outer - result.start);
            vertices.Add(b.inner - result.start);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            uvs.Add(Vector2.zero);
            colors.Add(outlineColor);
            colors.Add(outlineColor);
            colors.Add(outlineColor);
            colors.Add(outlineColor);
            indices.Add(firstVertex + 0);
            indices.Add(firstVertex + 1);
            indices.Add(firstVertex + 2);
            indices.Add(firstVertex + 2);
            indices.Add(firstVertex + 1);
            indices.Add(firstVertex + 3);
        }

        this.mesh.Clear();
        this.mesh.SetVertices(vertices);
        this.mesh.SetUVs(0, uvs);
        this.mesh.SetColors(colors);
        this.mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
        this.meshRenderer.transform.rotation = Quaternion.identity;

        bool flip = false;
        for (Transform _t = this.transform.parent; _t != null; _t = _t.parent)
        {
            if (_t.localScale.x < 0)
                flip = !flip;
        }
        this.meshRenderer.transform.localScale = new Vector3(flip ? -1 : 1, 1, 1);
    }

    private void CheckConeColor()
    {
        if(this.map.desertAlertState.IsGloballyActive())
        {
            SetConeColor(ColorAlerted);
        }
        else
        {
            SetConeColor(ColorIdle);
        }
    }

    public void SetCallbacks(CameraConeActivationDelegate shouldCameraActivate = null, Action onPlayerSpotted = null, Action onPlayerOutsideOfCone = null)
    {
        this.shouldCameraActivate = shouldCameraActivate;
        this.onPlayerSpotted = onPlayerSpotted;
        this.onPlayerOutsideOfCone = onPlayerOutsideOfCone;
    }


    public void SetConeColor(Color color)
    {
        this.currentConeColor = color;
    }

    public void SetConeFieldOfViewAngle(float angle)
    {        
        this.coneFieldOfViewAngle = angle;
    }

    public void SetCameraTurnDelays(int upToDownDelay = 60, int downToUpDelay = 60)
    {
        if (this.searchMovement != null)
        {
            this.searchMovement.upToDownDelay = upToDownDelay;
            this.searchMovement.downToUpDelay = downToUpDelay;
        }
    }

    public void SetMinMaxAngles(float minAngle, float maxAngle, bool pauseWhenSwitchingDirection)
    {
        float midAngle = minAngle + ((maxAngle - minAngle) / 2);
 
        if(midAngle + this.coneFieldOfViewAngle / 2 > maxAngle && midAngle - this.coneFieldOfViewAngle / 2 < minAngle)
        {
            this.fixedCone = true;    // dont move camera at all if too big cone
            coneCenterlineAngle = midAngle;
            this.searchMovement = null; // camera is fixed!
            return;
        }

        coneCenterlineAngle = midAngle;
        this.searchMovement = new SearchMovement(minAngle, maxAngle, shouldPauseWhenSwitchingDirection: pauseWhenSwitchingDirection);

        UpdateConeRotation(true);
    }

    public void SetConeSearchMovement(bool movementAllowed)
    {
        if(this.searchMovement != null)
        {
            this.searchMovement.isCurrentlySearching = movementAllowed;
        }
    }

    private void UpdateConeRotation(bool force = false)
    {
        if(this.searchMovement == null)
        {
            return;
        }

        if(!force && this.searchMovement.directionSwitchPauseCounter > 0)
        {
            this.searchMovement.directionSwitchPauseCounter--;
            return;
        }

        if ((!this.fixedCone && this.searchMovement != null && this.searchMovement.isCurrentlySearching) || force)
        {
            if (!force)
            {
                if (this.searchMovement.searchRotationDirection == Direction.Up)
                {
                    float maxAngle = this.searchMovement.maxAngle - this.coneFieldOfViewAngle / 2;
                    this.coneCenterlineAngle = Util.Slide(this.coneCenterlineAngle, maxAngle, this.searchMovement.searchRotationSpeed);

                    if (this.coneCenterlineAngle >= maxAngle)
                    {
                        this.searchMovement.searchRotationDirection = Direction.Down;
                        if (this.searchMovement.shouldPauseWhenSwitchingDirection)
                        {
                            this.searchMovement.directionSwitchPauseCounter = this.searchMovement.upToDownDelay;
                        }
                    }                     
                }
                else if (this.searchMovement.searchRotationDirection == Direction.Down)
                {
                    float minAngle = this.searchMovement.minAngle + this.coneFieldOfViewAngle / 2;
                    this.coneCenterlineAngle = Util.Slide(this.coneCenterlineAngle, minAngle, this.searchMovement.searchRotationSpeed);

                    if (this.coneCenterlineAngle <= minAngle)
                    {
                        this.searchMovement.searchRotationDirection = Direction.Up;
                        if(this.searchMovement.shouldPauseWhenSwitchingDirection)
                        {
                            this.searchMovement.directionSwitchPauseCounter = this.searchMovement.downToUpDelay;
                        }
                    }
                }
            }
            this.cameraSpriteObject.transform.localRotation = Quaternion.Euler(0, 0, this.coneCenterlineAngle);
        }
    }

    //will be removed
    private float GetObjectAngle0to360FlipAdjusted(GameObject objectToCheck)
    {
        bool isFacingRight = (transform.lossyScale.x > 0);
        return Util.WrapAngle0To360(objectToCheck.transform.rotation.eulerAngles.z + (isFacingRight ? 0: 180));
    }

    private bool IsAngleWithinRange(float testAngle, float angle1, float angle2)
    {
        angle1 -= testAngle;
        angle2 -= testAngle;
        angle1 = Util.WrapAngleMinus180To180(angle1);
        angle2 = Util.WrapAngleMinus180To180(angle2);

        if(angle1 * angle2 >= 0)
            return false;

        return Mathf.Abs(angle1 - angle2) < 180;
    }

    private void CheckPlayerVisibility()
    {
        var playerRelative = this.map.player.position - (Vector2)this.transform.position;
#if UNITY_EDITOR
        Debug.DrawLine(this.transform.position, this.transform.position + (Vector3)playerRelative.normalized * playerRelative.magnitude, Color.red, 0);   // n*mag, so it matches raycast code EXACTLY
#endif
        float topAngle = GetTopAngleFlipAdjusted();
        float bottomAngle = GetBottomAngleFlipAdjusted();

        float angle = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, playerRelative, Vector3.forward));

        if(IsAngleWithinRange(angle, topAngle, bottomAngle))
        {
#if UNITY_EDITOR
            Debug.DrawLine(this.transform.position, this.transform.position + (Vector3)playerRelative.normalized * playerRelative.magnitude, Color.yellow, 0);
#endif
            var raycast = new Raycast(this.map, this.transform.position, isCastByEnemy: true);       
            var hit = raycast.Arbitrary(this.transform.position, playerRelative.normalized, playerRelative.magnitude);

            if(!hit.anything && playerRelative.magnitude <= this.maxDetectionDistance)
            {
#if UNITY_EDITOR                
                Debug.DrawLine(this.transform.position, this.transform.position + (Vector3)playerRelative.normalized * hit.distance, Color.white, 0);
#endif                
                if (this.map.player.ShouldCollideWithItems() && this.map.player.alive)
                {
                    //player spotted               
                    this.onPlayerSpotted?.Invoke();
                    if(!this.fixedCone && this.searchMovement != null)
                    {
                        this.searchMovement.isCurrentlySearching = false;
                    }
                }
            }
            else
            {
                this.onPlayerOutsideOfCone?.Invoke();
            }
        }
        else
        {
            this.onPlayerOutsideOfCone?.Invoke();
        }

        if(this.map.desertAlertState.IsActive(this.transform.position))
        {
            if (shouldCameraActivate == null || (shouldCameraActivate != null && shouldCameraActivate()))
            {
                SetConeColor(ColorAlerted);
                if (!this.fixedCone && this.searchMovement != null)
                {
                    this.searchMovement.isCurrentlySearching = false;
                }
            }
        }
        else
        {
            SetConeColor(ColorIdle);
            if (!this.fixedCone && this.searchMovement != null)
            {
                this.searchMovement.isCurrentlySearching = true;
            }
        }
    }

    public void SetConeBoundaryRays()
    {
        float topRayAngle = GetTopAngleFlipAdjusted();
        float bottomRayAngle = GetBottomAngleFlipAdjusted();
 
        Vector3 topNormalized = new Vector3(Mathf.Cos(topRayAngle * Mathf.Deg2Rad), Mathf.Sin(topRayAngle * Mathf.Deg2Rad)).normalized;
        Vector3 bottomNormalized = new Vector3(Mathf.Cos(bottomRayAngle * Mathf.Deg2Rad), Mathf.Sin(bottomRayAngle * Mathf.Deg2Rad)).normalized;
 
        Debug.DrawLine(this.transform.position, this.transform.position + topNormalized * maxDetectionDistance, Color.yellow, 0);
        Debug.DrawLine(this.transform.position, this.transform.position + bottomNormalized * maxDetectionDistance, Color.blue, 0);
    }

    private float GetTopAngleFlipAdjusted()
    {
        bool isFacingRight = (transform.lossyScale.x > 0);
        if(isFacingRight)
            return Util.WrapAngle0To360(cameraSpriteObject.transform.localRotation.eulerAngles.z*0 + 1*itemRotation + this.coneCenterlineAngle + this.coneFieldOfViewAngle/2);
        else
            return Util.WrapAngle0To360(-cameraSpriteObject.transform.localRotation.eulerAngles.z*0 + 1*itemRotation + 180 - this.coneCenterlineAngle + this.coneFieldOfViewAngle/2);
    }

    private float GetBottomAngleFlipAdjusted()
    {
        bool isFacingRight = (transform.lossyScale.x > 0);
        if (isFacingRight)
            return Util.WrapAngle0To360(cameraSpriteObject.transform.localRotation.eulerAngles.z*0 + 1*itemRotation + this.coneCenterlineAngle - this.coneFieldOfViewAngle/2);
        else
            return Util.WrapAngle0To360(-cameraSpriteObject.transform.localRotation.eulerAngles.z*0 + 1*itemRotation + 180 - (this.coneCenterlineAngle + this.coneFieldOfViewAngle/2));
    }
}
