
using UnityEngine;
using System.Linq;

public class Chain : Item
{
    public Sprite ropeSprite;

    private Transform pin1;
    private Transform pin2;
    private PathRenderer pathRenderer;

    private Vector2 point1;
    private Vector2 point2;
    private float originalLength;
    private float length;
    private Vector2 point2Previous;
    private bool point2IsFixed;

    private float midpointDistanceAlongLine;
    private Vector2 midpoint;
    private Vector2 midpointPrevious;
    private float playerSlidingSpeed;
    private int delayBeforeSlide;
    private bool canSlide = false;
    private Coroutine sfxPitchCoroutine;

    private GroupLayer.Group point1Group;
    private GroupLayer.Group point2Group;

    public struct Knot
    {
        public float distanceAlong;
        public float factorAlong;
        public Transform transform;
    };
    private Knot[] knots;

    private int ignoreTime;

    public override void Init(Def def)
    {
        base.Init(def);

        var nearest = this.chunk.pathLayer.NearestNodeTo(this.position, 2);
        var path = nearest.path;

        this.point1 = path.nodes[0].Position;
        this.point2 = path.nodes.Last().Position;
        this.point2IsFixed = def.tile.tile.Contains("fixed");
        this.point2Previous = this.point2;

        this.length = (this.point1 - this.point2).magnitude;
        this.originalLength = this.length;

        this.midpointDistanceAlongLine = this.length / 2;
        this.midpoint = Vector2.Lerp(this.point1, this.point2, 0.5f);
        this.midpointPrevious = this.midpoint;

        this.pin1    = this.transform.Find("pin 1");
        this.pin2    = this.transform.Find("pin 2");

        if (this.point2IsFixed == true)
        {
            this.pin1.position    = this.point1;
            this.pin2.position    = this.point2;
        }
        else
        {
            this.pin1.position    = this.point1;
            this.pin2.gameObject.SetActive(false);
        }

        this.pathRenderer =
            this.transform.Find("rope_renderer").GetComponent<PathRenderer>();

        var templateKnot = this.transform.Find("knot");

        this.knots = new Knot[path.nodes.Length - 2];
        for (var n = 0; n < this.knots.Length; n += 1)
        {
            this.knots[n].distanceAlong =
                (path.nodes[n + 1].Position - path.nodes[0].Position).magnitude;
            this.knots[n].factorAlong = this.knots[n].distanceAlong / this.length;
            this.knots[n].transform = Instantiate(
                templateKnot.gameObject, this.transform
            ).transform;
        }

        Destroy(templateKnot.gameObject);

        this.point1Group = this.chunk.groupLayer.RegisterFollower(
            this, 0, this.point1, def.tx, def.ty
        );

        if (this.point2IsFixed == true)
        {
            this.point2Group = this.chunk.groupLayer.RegisterFollower(
                this,
                1,
                this.point2,
                Mathf.FloorToInt(this.point2.x),
                Mathf.FloorToInt(this.point2.y)
            );
        }
    }

    private void UpdateGrabInfo()
    {
        this.grabInfo = new GrabInfo(
            this,
            GrabType.Ceiling,
            this.midpoint - this.position
        );
    }

    private float HangOffsetYForFixed()
    {
        if (this.midpointDistanceAlongLine < 1)
            return Util.MapRange(this.midpointDistanceAlongLine, 0, 1, 0, 0.5f);

        if (this.midpointDistanceAlongLine > this.length - 1)
            return Util.MapRange(
                this.midpointDistanceAlongLine,
                this.length - 1,
                this.length,
                0.5f,
                0
            );

        return 0.5f;
    }

    public override void Advance()
    {
        if (this.point1Group != null)
            this.point1 = this.point1Group.FollowerPosition(this, 0);

        if (this.point2IsFixed == true && this.point2Group != null)
            this.point2 = this.point2Group.FollowerPosition(this, 1);
        
        this.length = (this.point2 - this.point1).magnitude;
        for (int n = 0; n < this.knots.Length; n += 1)
            this.knots[n].distanceAlong = this.knots[n].factorAlong * this.length;

        if (this.map.player.grabbingOntoItem == this)
            this.ignoreTime = 12;
        else if (this.ignoreTime > 0)
            this.ignoreTime -= 1;

        var playerCanGrab =
            this.map.player.ShouldCollideWithItems() == true &&
            this.map.player.grabbingOntoItem == null &&
            this.ignoreTime == 0 &&
            (
                this.map.player.state == Player.State.Normal ||
                this.map.player.state == Player.State.Jump ||
                this.map.player.state == Player.State.DropVertically
            );

        if (playerCanGrab == true)
            CheckIfPlayerHasGrabbedRope();
        else if (this.map.player.grabbingOntoItem == this)
            AdvancePlayerSliding();

        if (this.map.player.grabbingOntoItem != this && this.sfxPitchCoroutine != null)
        {
            StopCoroutine(this.sfxPitchCoroutine);
            Audio.instance.StopSfx(Assets.instance.moltenFortress.sfxChainSlideLoop);
            Audio.instance.PlaySfx(Assets.instance.moltenFortress.sfxChainJump);
            this.sfxPitchCoroutine = null;
        }

        if (this.midpointDistanceAlongLine < 0.25f)
            this.midpointDistanceAlongLine = 0.25f;
        if (this.midpointDistanceAlongLine > this.length - 0.25f)
            this.midpointDistanceAlongLine = this.length - 0.25f;

        AdvanceRopeBouncing();
        UpdateGrabInfo();

        UpdateDisplay();
    }

    private void CheckIfPlayerHasGrabbedRope()
    {
        Vector2 lineVector = this.point2 - this.point1;
        float along = Vector2.Dot(
            this.map.player.position - this.point1, lineVector
        ) / lineVector.sqrMagnitude;
        if (this.point2IsFixed == true)
            along = Mathf.Clamp(along, 1f / this.length, 1f - (1f / this.length));
        else
            along = Mathf.Clamp01(along);
        Vector2 nearest = Vector2.Lerp(this.point1, this.point2, along);
        this.canSlide = false;
        if ((nearest - this.map.player.position).sqrMagnitude >= 0.5f * 0.5f)
            return;

        this.midpointDistanceAlongLine = along * this.length;
        if (this.point2IsFixed == true)
        {
            Vector2 velocity = new Vector2(0, Mathf.Clamp(
                this.map.player.velocity.y * 0.8f, -0.4f, 0.4f
            ));
            this.midpoint = nearest;
            this.midpointPrevious = this.midpoint - velocity;
        }
        else
        {
            Vector2 down = (this.point2 - this.point1).normalized;
            Vector2 across = new Vector2(down.y, -down.x);

            Vector2 perpendicularVelocity =
                across * Vector2.Dot(across, this.map.player.velocity);
            if (perpendicularVelocity.sqrMagnitude > 0.4f * 0.4f)
            {
                perpendicularVelocity.Normalize();
                perpendicularVelocity *= 0.4f;
            }

            this.midpointDistanceAlongLine = along * this.length;
            this.midpoint = nearest;
            this.midpointPrevious = nearest - (perpendicularVelocity * 1.0f);
            this.point2Previous = this.point2 - (perpendicularVelocity * 1.2f);
        }
        this.playerSlidingSpeed = 0;
        this.delayBeforeSlide = 20;

        UpdateGrabInfo(); // GrabItem() needs this to be configured to work right
        this.map.player.GrabItem(this);

        Audio.instance.PlaySfx(
            Assets.instance.moltenFortress.sfxChainGrab,
            position: this.position
        );

        this.canSlide = true;
    }

    private void AdvancePlayerSliding()
    {
        if (this.delayBeforeSlide > 0)
            this.delayBeforeSlide -= 1;
        else if (this.point2.y > this.point1.y)
            this.playerSlidingSpeed -= 0.006f;
        else if (this.point2.y < this.point1.y)
            this.playerSlidingSpeed += 0.006f;

        float positionBefore = this.midpointDistanceAlongLine;

        Vector2 offsetBefore = this.midpoint - Vector2.Lerp(
            this.point1, this.point2, positionBefore / this.length
        );
        Vector2 along = (this.point2 - this.point1).normalized;
        Vector2 across = new Vector2(along.y, -along.x);
        offsetBefore = Vector2.Dot(across, offsetBefore) * across;

        this.midpointDistanceAlongLine += this.playerSlidingSpeed;

        foreach (var knot in this.knots)
        {
            if (positionBefore < knot.distanceAlong)
            {
                if (this.midpointDistanceAlongLine > knot.distanceAlong - 0.25f)
                {
                    this.midpointDistanceAlongLine = knot.distanceAlong - 0.25f;
                    if (this.playerSlidingSpeed > 0)
                        this.playerSlidingSpeed = 0;
                }
            }
            else
            {
                if (this.midpointDistanceAlongLine < knot.distanceAlong + 0.25f)
                {
                    this.midpointDistanceAlongLine = knot.distanceAlong + 0.25f;
                    if (this.playerSlidingSpeed < 0)
                        this.playerSlidingSpeed = 0;
                }
            }
        }

        if (this.point2IsFixed == true)
        {
            if (this.midpointDistanceAlongLine > this.length - 1 ||
                this.midpointDistanceAlongLine < 1)
            {
                this.map.player.grabbingOntoItem = null;
                this.map.player.state = Player.State.DropVertically;
                this.map.player.groundState =
                    Player.GroundState.Airborne(this.map.player);
            }
            
            this.midpointDistanceAlongLine = Mathf.Clamp(
                this.midpointDistanceAlongLine,
                1,
                this.length - 1
            );
        }

        this.midpoint = Vector2.Lerp(
            this.point1, this.point2, this.midpointDistanceAlongLine / this.length
        ) + offsetBefore;

        if (this.canSlide && playerSlidingSpeed>0)
        {
            Audio.instance.PlaySfxLoop(
                Assets.instance.moltenFortress.sfxChainSlideLoop,
                position: this.position
            );
            this.sfxPitchCoroutine = StartCoroutine(Audio.instance.ChangeSfxPitch(
                Assets.instance.moltenFortress.sfxChainSlideLoop,
                1.5f, 1.0f, 4
            ));
            this.canSlide = false;
        }
    }

    private void AdvanceRopeBouncing()
    {
        Vector2 midpointHome = Vector2.Lerp(
            this.point1, this.point2, this.midpointDistanceAlongLine / this.length
        );

        Vector2 nextMidpoint;
        Vector2 nextPoint2Position;

        void Push(ref Vector2 previous, Vector2 acceleration) =>
            previous -= acceleration;
        void Damp(ref Vector2 previous, Vector2 current, float factor) =>
            previous = Vector2.Lerp(previous, current, factor);

        if (this.point2IsFixed == true)
        {
            if (this.map.player.grabbingOntoItem == this)
            {
                Vector2 target = midpointHome.Add(0, -HangOffsetYForFixed());
                Push(ref this.midpointPrevious, (target - this.midpoint) * 0.1f);
                Damp(ref this.midpointPrevious, this.midpoint, 0.2f);
            }
            else
            {
                Vector2 target = midpointHome;
                Push(ref this.midpointPrevious, (target - this.midpoint) * 0.1f);
                Damp(ref this.midpointPrevious, this.midpoint, 0.05f);
            }

            nextMidpoint = this.midpoint + this.midpoint - this.midpointPrevious;
            nextPoint2Position = this.point2;
            this.midpointPrevious = this.midpoint;
        }
        else
        {
            Damp(ref this.point2Previous, this.point2, 0.001f);
            Push(ref this.point2Previous, new Vector2(0, -0.02f));

            nextMidpoint = this.midpoint + this.midpoint - this.midpointPrevious;
            nextPoint2Position = this.point2 + this.point2 - this.point2Previous;
            this.midpointPrevious = this.midpoint;
            this.point2Previous = this.point2;

            Vector2 nextMidpointHome = Vector2.Lerp(
                this.point1,
                nextPoint2Position,
                this.midpointDistanceAlongLine / this.length
            );
            nextMidpoint = Vector2.MoveTowards(nextMidpoint, nextMidpointHome, 0.002f);

            {
                Vector2 delta = nextMidpoint - this.point1;
                Vector2 direction = delta.normalized;
                float currentDistance = Vector2.Dot(direction, delta);
                Vector2 correction =
                    direction * (this.midpointDistanceAlongLine - currentDistance);
                nextMidpoint += correction * 0.2f;
            }

            {
                Vector2 delta = nextPoint2Position - nextMidpoint;
                Vector2 direction = delta.normalized;
                float currentDistance = Vector2.Dot(direction, delta);
                float intendedDistance =
                    this.originalLength - this.midpointDistanceAlongLine;
                Vector2 correction = direction * (intendedDistance - currentDistance);
                nextMidpoint -= correction * 0.2f;
                nextPoint2Position += correction * 0.2f;
            }
        }
        if (this.map.player.grabbingOntoItem == this)
        {
            var lastMidpoint = this.midpoint;
            var lastPoint2 = this.point2;
            var lastPlayerPosition = this.map.player.position;

            for (int n = 0; n < 4; n += 1)
            {
                float factor = (n + 1) / 4.0f;
                this.midpoint = Vector2.Lerp(lastMidpoint, nextMidpoint, factor);
                this.point2 = Vector2.Lerp(lastPoint2, nextPoint2Position, factor);

                Vector2 pos = Vector2.Lerp(
                    this.map.player.position,
                    this.midpoint.Add(0, -Player.HangDistance),
                    1.0f / n
                );
                this.map.player.velocity = pos - lastPlayerPosition;
                this.map.player.position = pos;
                this.map.player.CheckAllSensors();

                if ((this.map.player.position - pos).sqrMagnitude > 0.01f * 0.01f)
                {
                    Vector2 correction = this.map.player.position - pos;
                    this.midpoint += correction;
                    break;
                }
            }
        }
        else
        {
            this.midpoint = nextMidpoint;
            this.point2 = nextPoint2Position;
        }
    }

    private void UpdateDisplay()
    {
        this.pin1.transform.position = this.point1;
        this.pin2.transform.position = this.point2;

        Vector2[] path = new [] { this.point1, this.midpoint, this.point2 };
        this.pathRenderer.Draw(
            this.ropeSprite,
            path,
            false,
            "Items (Front)",
            this.length / this.originalLength
        );

        for (var n = 0; n < this.knots.Length; n += 1)
        {
            Vector2 pt1, pt2;
            float distance;

            if (this.knots[n].distanceAlong < this.midpointDistanceAlongLine)
            {
                pt1 = this.point1;
                pt2 = this.midpoint;
                distance = this.knots[n].distanceAlong;
            }
            else
            {
                pt1 = this.midpoint;
                pt2 = this.point2;
                distance =
                    this.knots[n].distanceAlong - this.midpointDistanceAlongLine;
            }

            Vector2 delta = pt2 - pt1;
            float angleDegrees = Mathf.Atan2(delta.y, delta.x) * 180 / Mathf.PI;

            this.knots[n].transform.position = pt1 + (delta.normalized * distance);
            this.knots[n].transform.rotation = Quaternion.Euler(0, 0, angleDegrees + 90);
        }
    }
}
