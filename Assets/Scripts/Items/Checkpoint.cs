
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Checkpoint : Item
{
    public Animated.Animation animationShine;
    public Animated.Animation animationBulb;
    public Animated.Animation animationBulbSmack;
    public Animated.Animation animationFlagIdle;
    public Animated.Animation animationFlagUnfold;
    public Animated.Animation animationSparkle;
    public Animated.Animation animationRaySpears;

    [HideInInspector] public Animated single;
    [HideInInspector] public GameObject pieces;
    private Animated flag;
    private Transform pole;
    private Transform poleBase;
    [HideInInspector] public Animated bulb;
    private SpriteRenderer arrowTL;
    private SpriteRenderer arrowTR;
    private SpriteRenderer arrowBL;
    private SpriteRenderer arrowBR;

    [HideInInspector] public bool active;
    private Vector2 bulbVelocity;
    private bool playerIsIntersecting;
    private float arrowDistance;
    private float arrowAlpha;
    private int timeSinceLastHit;
    [HideInInspector]public float flagUnfoldWaitTime = 1.5f;
    private KingHat kingHat;

    private MaterialPropertyBlock propertyBlock;

    public readonly Vector2 NormalBulbPosition = new Vector2(0, 4.695f) * 0.75f;
    public readonly Vector2 NormalFlagPosition = new Vector2(0.16f, 2f) * 0.75f;
    public const float BulbAttachOffset = -0.69f * 0.75f;
    public const float NormalPoleLength = 3.251f * 0.75f;

    public bool IsKingHatActive => this.kingHat != null;

    public Animated.Animation animationEvolve1;
    public Animated.Animation animationEvolve2;
    public Animated.Animation animationEvolve3;
    public Animated.Animation animationEvolve4;
    public Animated.Animation animationEvolve5;
    public Transform[] whiteLines;

    [HideInInspector] public int currentEvolution;

    public override void Init(Def def)
    {
        base.Init(def);

        this.active = false;

        this.single   = this.transform.Find("single").GetComponent<Animated>();
        this.pieces   = this.transform.Find("pieces").gameObject;
        this.flag     = this.transform.Find("pieces/flag").GetComponent<Animated>();
        this.pole     = this.transform.Find("pieces/pole");
        this.poleBase = this.transform.Find("pieces/pole base");
        this.bulb     = this.transform.Find("pieces/bulb").GetComponent<Animated>();
        this.arrowTL  = this.transform.Find("arrow tl").GetComponent<SpriteRenderer>();
        this.arrowTR  = this.transform.Find("arrow tr").GetComponent<SpriteRenderer>();
        this.arrowBL  = this.transform.Find("arrow bl").GetComponent<SpriteRenderer>();
        this.arrowBR  = this.transform.Find("arrow br").GetComponent<SpriteRenderer>();

        this.single.gameObject.SetActive(true);
        this.pieces.gameObject.SetActive(false);

        this.single.PlayOnce(this.animationShine);

        this.propertyBlock = new MaterialPropertyBlock();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.currentEvolution = 0;
        List < Checkpoint> checkpoints = this.map.AllItems().OfType<Checkpoint>().ToList();
        switch (checkpoints.IndexOf(this))
        {
            case 1:
                this.single.gameObject.SetActive(false);
                this.pieces.gameObject.SetActive(true);
                this.bulb.GetComponent<SpriteRenderer>().sprite = this.animationEvolve1.sprites[0];
                this.currentEvolution = 1;
                break;
            case 2:
                this.single.gameObject.SetActive(false);
                this.pieces.gameObject.SetActive(true);
                this.bulb.GetComponent<SpriteRenderer>().sprite = this.animationEvolve2.sprites[0];
                this.currentEvolution = 2;
                break;
            case 3:
                this.single.gameObject.SetActive(false);
                this.pieces.gameObject.SetActive(true);
                this.bulb.GetComponent<SpriteRenderer>().sprite = this.animationEvolve3.sprites[0];
                this.currentEvolution = 3;
                break;
            case 4:
                this.single.gameObject.SetActive(false);
                this.pieces.gameObject.SetActive(true);
                this.bulb.GetComponent<SpriteRenderer>().sprite = this.animationEvolve4.sprites[0];
                this.currentEvolution = 4;
                break;
            case 5:
                this.single.gameObject.SetActive(false);
                this.pieces.gameObject.SetActive(true);
                this.bulb.GetComponent<SpriteRenderer>().sprite = this.animationEvolve5.sprites[0];
                this.currentEvolution = 5;
                break;
        }
    }

    public static Checkpoint Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.checkpoint, chunk.transform);
        obj.name = "Spawned Checkpoint";
        var item = obj.GetComponent<Checkpoint>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));
        
        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        this.timeSinceLastHit += 1;

        if (this.active == false && this.map.frameNumber % 80 == 0)
        {
            this.single.PlayOnce(this.animationShine);
        }

        var bulbPosition = (Vector2)this.bulb.transform.TransformPoint(Vector2.zero);
        var playerPosition = this.map.player.position;

        var radius = this.playerIsIntersecting ? 2.5f : 1.5f;
        var intersecting =
            this.map.player.ShouldCollideWithItems() == true &&
            (bulbPosition - playerPosition).sqrMagnitude < radius * radius;

        if (intersecting == true &&
            this.playerIsIntersecting == false &&
            this.timeSinceLastHit > 30)
        {
            this.timeSinceLastHit = 0;

            Vector2 toBulb = (bulbPosition - playerPosition).normalized;
            this.bulbVelocity = toBulb * 0.5f;

            bool wasActive = true;
            if (this.active == false)
            {
                Activate(false);

                for (var n = 0; n < 10; n += 1)
                {
                    var delay = UnityEngine.Random.Range(0.1f, 1.5f);
                    Invoke(() =>
                    {
                        Particle.CreateAndPlayOnce(
                            this.animationSparkle,
                            bulbPosition.Add(
                                UnityEngine.Random.Range(-1.2f, 1.2f),
                                UnityEngine.Random.Range(-1.2f, 1.2f)
                            ),
                            this.map.transform
                        );
                    }, delay);
                }

                wasActive = false;
            }

            Audio.instance.PlaySfx(Assets.instance.sfxCheckpointActivate, position: this.position);
            
            StartCoroutine(TintFlashCoroutine());
            {
                var p = Particle.CreateAndPlayOnce(
                    this.animationBulbSmack, bulbPosition, this.bulb.transform
                );

                if(this.currentEvolution != 0)
                {
                    if (wasActive == false)
                    {
                        var p2 = Particle.CreateAndPlayOnce(
                            this.animationRaySpears, bulbPosition, this.bulb.transform
                        );

                        switch (this.currentEvolution)
                        {
                            case 1:
                                p.transform.localScale = Vector3.one * 1.5f;
                                p2.transform.localScale = Vector3.one * 0.75f;
                                break;
                            case 2:
                                p.transform.localScale = Vector3.one * 2f;
                                p2.transform.localScale = Vector3.one * 1f;
                                break;
                            case 3:
                                p.transform.localScale = Vector3.one * 2.5f;
                                p2.transform.localScale = Vector3.one * 1.5f;
                                break;
                            case 4:
                                p.transform.localScale = Vector3.one * 2.5f;
                                p2.transform.localScale = Vector3.one * 1.5f;
                                break;
                        }
                    }
                    else
                    {
                        switch (this.currentEvolution)
                        {
                            case 1:
                                p.transform.localScale = Vector3.one * 1.5f;
                                break;
                            case 2:
                                p.transform.localScale = Vector3.one * 2f;
                                break;
                            case 3:
                                p.transform.localScale = Vector3.one * 2f;
                                break;
                            case 4:
                                p.transform.localScale = Vector3.one * 2f;
                                break;
                        }
                    }
                    
                }
            }
        }
        else
        {
            Vector2 targetVelocity =
                (NormalBulbPosition - (Vector2)this.bulb.transform.localPosition) * 0.2f;

            this.bulbVelocity *= 0.95f;
            this.bulbVelocity += targetVelocity * 0.2f;
        }
        this.playerIsIntersecting = intersecting;

        this.bulb.transform.position += (Vector3)this.bulbVelocity;

        {
            Vector2 top = this.bulb.transform.localPosition;
            Vector2 bottom = this.pole.localPosition;
            Vector2 vector = top - bottom;
            float scale = vector.magnitude / NormalPoleLength;
            float angleDegrees = (Mathf.Atan2(vector.y, vector.x) * 180 / Mathf.PI) - 90;

            this.pole.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);
            this.pole.transform.localScale = new Vector3(1, scale, 1);
            this.flag.transform.localRotation = this.pole.transform.localRotation;
            this.flag.transform.position = this.pole.transform.TransformPoint(
                new Vector2(
                    NormalFlagPosition.x,
                    (NormalFlagPosition.y + (
                        vector.magnitude + BulbAttachOffset - NormalPoleLength
                    )) / scale
                )
            );
            this.bulb.transform.localRotation = this.pole.transform.localRotation;
        };

        AdvanceArrows((bulbPosition - playerPosition).sqrMagnitude < 10 * 10);

        if(this.map.NearestChunkTo(this.map.player.position) == this.map.chunks[this.map.chunks.IndexOf(this.chunk) + 1])
        {
            this.map.lastCheckpoint = this;
        }
    }

    private void AdvanceArrows(bool isPlayerWithinRange)
    {
        bool showArrows =
            this.active == false &&
            this.map.NearestChunkTo(this.map.player.position) == this.chunk &&
            isPlayerWithinRange == true;

        if (this.arrowAlpha == 0 && showArrows == true)
            this.arrowDistance = 10;
        else
            this.arrowDistance = Util.Slide(this.arrowDistance, 1.2f, 0.5f);

        if (this.active == true)
            this.arrowAlpha = 0;
        else
            this.arrowAlpha = Util.Slide(this.arrowAlpha, showArrows ? 1 : 0, 0.06f);

        this.arrowTL.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowTR.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowBL.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowBR.gameObject.SetActive(this.arrowAlpha > 0);

        if (this.arrowAlpha > 0)
        {
            Color c = new Color(1, 1, 1, this.arrowAlpha);
            this.arrowTL.color = c;
            this.arrowTR.color = c;
            this.arrowBL.color = c;
            this.arrowBR.color = c;

            float s =
                this.arrowDistance +
                Mathf.Sin(this.map.frameNumber * 0.1f) * 0.12f;
            this.arrowTL.transform.localPosition = NormalBulbPosition.Add(-s, +s);
            this.arrowTR.transform.localPosition = NormalBulbPosition.Add(+s, +s);
            this.arrowBL.transform.localPosition = NormalBulbPosition.Add(-s, -s);
            this.arrowBR.transform.localPosition = NormalBulbPosition.Add(+s, -s);
        }
    }

    public void Activate(bool skipFlagAnimation)
    {
        this.active = true;

        this.single.gameObject.SetActive(false);
        this.pieces.SetActive(true);

        this.bulb.Stop();

        if(this.currentEvolution == 0)
        {
            this.bulb.GetComponent<SpriteRenderer>().sprite =
            this.animationBulb.sprites[1];
        }
        
        if (skipFlagAnimation == true)
        {
            this.flag.GetComponent<SpriteRenderer>().enabled = true;
            this.flag.PlayAndLoop(this.animationFlagIdle);
        }
        else
        {
            Invoke(() =>
            {
                this.flag.GetComponent<SpriteRenderer>().enabled = true;
                this.flag.PlayOnce(this.animationFlagUnfold, delegate
                {
                    this.flag.PlayAndLoop(this.animationFlagIdle);
                });
            }, this.flagUnfoldWaitTime);
        }

        ThrowKingHat();

        this.map.player.SetCheckpointPosition(
            this.position + new Vector2(0, Player.PivotDistanceAboveFeet)
        );
        
        int checkpointNumber = this.chunk.checkpointNumber.Value;
        int checkpointsSkipped = SaveData.GetNumberOfCheckpointsSkipped(checkpointNumber);
        Achievements.CheckCheckpointAchievement(checkpointsSkipped);

        this.map.SaveDataOfAttempt(checkpointNumber);
        this.map.currentCheckpoint = this.transform;
        this.map.checkpointDeaths = 0;
        this.map.checkpointTimer = 0;

        this.map.SaveCoins();
        List<Checkpoint> checkpoints = this.map.AllItems().OfType<Checkpoint>().ToList();

        ResetEvolution();

        if(this.currentEvolution != 0 && skipFlagAnimation == false)
        {
            StartCoroutine(_WhiteLinesScaleEffect(.75f));
            StartCoroutine(_GenerateRewards());
        }
    }

    private IEnumerator _GenerateRewards()
    {
        yield return new WaitForSeconds(1.5f);
        GenerateReward();
    }

    private IEnumerator TintFlashCoroutine()
    {
        Tint(Color.black, 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(new Color32(179, 94, 121, 255), 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 0);
    }

    private void Tint(Color color, float intensity)
    {
        void TintSprite(SpriteRenderer sr)
        {
            sr.GetPropertyBlock(this.propertyBlock);
            this.propertyBlock.SetColor("_Color", color);
            this.propertyBlock.SetFloat("_Intensity", intensity);
            sr.SetPropertyBlock(this.propertyBlock);
        }

        TintSprite(this.single  .GetComponent<SpriteRenderer>());
        TintSprite(this.bulb    .GetComponent<SpriteRenderer>());
        TintSprite(this.poleBase.GetComponent<SpriteRenderer>());
        TintSprite(this.pole    .GetComponent<SpriteRenderer>());
        TintSprite(this.flag    .GetComponent<SpriteRenderer>());
    }

    public void PlaceKingHat(KingHat kingHat)
    {
        this.kingHat = kingHat;
    }

    public void RemoveKingHat()
    {
        this.kingHat = null;
    }

    private void ThrowKingHat()
    {
        if(this.kingHat == null) return;
        
        this.kingHat.Throw(this.map.player.velocity);
        RemoveKingHat();
    }

    private void ResetEvolution()
    {
        List<Checkpoint> checkpoints = this.map.AllItems().OfType<Checkpoint>().ToList();

        int x = checkpoints.IndexOf(this);
        for(int n = 0; n<=5; n++)
        {
            x++;
            if (x < checkpoints.Count)
            {
                checkpoints[x].single.gameObject.SetActive(false);
                checkpoints[x].pieces.gameObject.SetActive(true);
                checkpoints[x].currentEvolution = n;
                Sprite sprite = checkpoints[x]?.animationBulb.sprites[0];
                switch (n)
                {
                    case 0:
                        sprite = checkpoints[x]?.animationBulb.sprites[0];
                        break;
                    case 1:
                        sprite = checkpoints[x]?.animationEvolve1.sprites[0];
                        break;
                    case 2:
                        sprite = checkpoints[x]?.animationEvolve2.sprites[0];
                        break;
                    case 3:
                        sprite = checkpoints[x]?.animationEvolve3.sprites[0];
                        break;
                    case 4:
                        sprite = checkpoints[x]?.animationEvolve4.sprites[0];
                        break;
                    case 5:
                        sprite = checkpoints[x]?.animationEvolve5.sprites[0];
                        break;
                }
                checkpoints[x].bulb.GetComponent<SpriteRenderer>().sprite = sprite;
            }
        }
    }

    private IEnumerator _WhiteLinesScaleEffect(float targetTime)
    {
        float initialTime = 0f;
        ToggleWhiteLines(true);

        while (initialTime < targetTime)
        {
            initialTime += Time.deltaTime;

            for (int i = 0; i < this.whiteLines.Length; i++)
            {
                this.whiteLines[i].localScale =
                    Vector3.Lerp(new Vector3(this.whiteLines[i].localScale.x, 0.1f), new Vector3(this.whiteLines[i].localScale.x, 1.5f), initialTime / targetTime);

                if (initialTime > 0.5f)
                {
                    float alpha = Mathf.Lerp(1, 0, (initialTime -0.5f) / (targetTime - 0.5f));
                    Color t = this.whiteLines[i].GetComponent<SpriteRenderer>().color;
                    this.whiteLines[i].GetComponent<SpriteRenderer>().color = new Color(t.r, t.g, t.b, alpha);
                }
            }

            yield return null;
        }

        ToggleWhiteLines(false);

    }

    private void ToggleWhiteLines(bool activate)
    {
        for (int i = 0; i < this.whiteLines.Length; i++)
        {
            this.whiteLines[i].gameObject.SetActive(activate);
        }
    }

    private void GenerateReward()
    {
        if(SaveData.GetMaxCheckpointsSkipped() < this.currentEvolution)
        {
            SaveData.SetMaxCheckpointsSkipped(this.currentEvolution);
        }

        var bulbPosition = (Vector2)this.bulb.transform.TransformPoint(Vector2.zero);

        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnerActivateAnimation, bulbPosition, this.bulb.transform
        );
        p.transform.localScale *= 1.5f;
        p.spriteRenderer.sortingLayerName = "Pickups";
        p.spriteRenderer.sortingOrder = 20;

        float rewardNumber = 1;
        float delay = 1;
        TimerPickup t; 
        switch (this.currentEvolution)
        {
            case 1:
                rewardNumber = 2;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(0, 2, 0), delay);
                break;

            case 2:
                rewardNumber = 4;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                break;

            case 3:
                rewardNumber = 8;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3((-2f), 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3((2f), 0.75f, 0), delay);
                t.SetLayerOrder(15);
                break;

            case 4:
                rewardNumber = 12;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-2.65f, 0.36f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-2f, 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(2f, 0.75f, 0), delay);
                t.SetLayerOrder(16);
                delay += 0.3f;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(2.65f, 0.36f, 0), delay);
                t.SetLayerOrder(15);
                break;

            case 5:
                rewardNumber = 15;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-2.65f, 0.34f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-2f, 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(-1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(2f, 0.75f, 0), delay);
                t.SetLayerOrder(16);
                delay += 0.3f;
                t = TimerPickup.Create(this.bulb.GetComponent<Transform>().position, this.chunk, this.bulb.GetComponent<Transform>().position + new Vector3(2.65f, 0.34f, 0), delay);
                t.SetLayerOrder(15);
                break;
        }

        float hVelocity = UnityEngine.Random.Range(0.1f, 0.5f);
        float vVelocity = UnityEngine.Random.Range(0.1f, 0.3f);

        float start = -1f;
        for (int i = 1; i <= rewardNumber - 1; i++)
        {
            Coin coin = Coin.Create(this.bulb.GetComponent<Transform>().position, this.chunk);
            coin.movingFreely = true;
            coin.velocity = new Vector2(hVelocity * start, vVelocity);
            start += (2 / (rewardNumber - 1));
        }

        start = -1f;
        for (int i = 1; i <= rewardNumber; i++)
        {
            Coin coin = Coin.CreateSilver(this.bulb.GetComponent<Transform>().position, this.chunk);
            coin.movingFreely = true;
            coin.velocity = new Vector2(hVelocity * start, vVelocity);
            start += (2 / (rewardNumber - 1));
        }
    }
}
