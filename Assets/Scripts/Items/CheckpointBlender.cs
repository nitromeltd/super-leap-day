﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckpointBlender : Item
{
    public Animated.Animation animationShine;
    public Animated.Animation animationBulb;
    public Animated.Animation animationBulbSmack;
    public Animated.Animation animationFlagIdle;
    public Animated.Animation animationFlagUnfold;
    public Animated.Animation animationSparkle;
    public Animated.Animation animationBlade;
    public Animated.Animation animationJuiceEmpty;
    public Animated.Animation animationJuiceBlendSmall;
    public Animated.Animation animationJuiceRise;
    public Animated.Animation animationJuiceBlendBig;
    public Animated.Animation animationJuiceSteady;

    private Animated single;
    private GameObject pieces;
    private Animated flag;
    private Transform pole;
    private Transform blenderBase;
    private Animated bulb;
    private SpriteRenderer arrowTL;
    private SpriteRenderer arrowTR;
    private SpriteRenderer arrowBL;
    private SpriteRenderer arrowBR;
    private Animated blade;
    private Animated juice;

    private bool active;
    private Vector2 bulbVelocity;
    private bool playerIsIntersecting;
    private float arrowDistance;
    private float arrowAlpha;
    private int timeSinceLastHit;

    private MaterialPropertyBlock propertyBlock;

    public readonly Vector2 NormalBulbPosition = new Vector2(-0.091f, 5.465f);
    public readonly Vector2 NormalFlagPosition = new Vector2(0.12f, 1.5f);
    public const float BulbAttachOffset = -0.5175f;
    public const float NormalPoleLength = 2.4382f;
    public const float ExtraBlenderHeight = 2f;
    public const int FruitCost = 20;

    public override void Init(Def def)
    {
        base.Init(def);

        this.active = false;

        this.single   = this.transform.Find("single").GetComponent<Animated>();
        this.pieces   = this.transform.Find("pieces").gameObject;
        this.flag     = this.transform.Find("pieces/flag").GetComponent<Animated>();
        this.pole     = this.transform.Find("pieces/pole");
        this.blenderBase = this.transform.Find("pieces/blender base");
        this.bulb     = this.transform.Find("pieces/bulb").GetComponent<Animated>();
        this.arrowTL  = this.transform.Find("arrow tl").GetComponent<SpriteRenderer>();
        this.arrowTR  = this.transform.Find("arrow tr").GetComponent<SpriteRenderer>();
        this.arrowBL  = this.transform.Find("arrow bl").GetComponent<SpriteRenderer>();
        this.arrowBR  = this.transform.Find("arrow br").GetComponent<SpriteRenderer>();
        this.blade     = this.transform.Find("blade").GetComponent<Animated>();
        this.juice     = this.transform.Find("pieces/blender base/juice").GetComponent<Animated>();

        this.single.gameObject.SetActive(true);
        this.pieces.gameObject.SetActive(false);

        this.single.PlayOnce(this.animationShine);
        this.blade.PlayAndLoop(this.animationBlade);
        this.juice.PlayOnce(this.animationJuiceEmpty);

        this.propertyBlock = new MaterialPropertyBlock();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.hitboxes = new Hitbox[]
        {
            // blender base hitbox
            new Hitbox(-0.9f, 0.9f, 0f, 2.25f, 0, Hitbox.Solid)
        };
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        this.timeSinceLastHit += 1;

        if (this.active == false && this.map.frameNumber % 80 == 0)
        {
            this.single.PlayOnce(this.animationShine);
        }

        var bulbPosition = (Vector2)this.bulb.transform.TransformPoint(Vector2.zero);
        var playerPosition = this.map.player.position;

        var radius = this.playerIsIntersecting ? 2.5f : 1.5f;
        var intersecting = (bulbPosition - playerPosition).sqrMagnitude < radius * radius;

        if (intersecting == true &&
            this.playerIsIntersecting == false &&
            this.timeSinceLastHit > 30)
        {
            this.timeSinceLastHit = 0;

            Vector2 toBulb = (bulbPosition - playerPosition).normalized;
            this.bulbVelocity = toBulb * 0.5f;

            if (this.active == false)
            {
                Hit();

                if(this.active == true)
                {
                    for (var n = 0; n < 10; n += 1)
                    {
                        var delay = UnityEngine.Random.Range(0.1f, 1.5f);
                        Invoke(() =>
                        {
                            Particle.CreateAndPlayOnce(
                                this.animationSparkle,
                                bulbPosition.Add(
                                    UnityEngine.Random.Range(-1.2f, 1.2f),
                                    UnityEngine.Random.Range(-1.2f, 1.2f)
                                ),
                                this.map.transform
                            );
                        }, delay);
                    }
                }
            }

            Audio.instance.PlaySfx(Assets.instance.sfxCheckpointActivate, position: this.position);

            StartCoroutine(TintFlashCoroutine());
            {
                var p = Particle.CreateAndPlayOnce(
                    this.animationBulbSmack, bulbPosition, this.bulb.transform
                );
            }
        }
        else
        {
            Vector2 targetVelocity =
                (NormalBulbPosition - (Vector2)this.bulb.transform.localPosition) * 0.2f;

            this.bulbVelocity *= 0.95f;
            this.bulbVelocity += targetVelocity * 0.2f;
        }
        this.playerIsIntersecting = intersecting;

        this.bulb.transform.position += (Vector3)this.bulbVelocity;

        {
            Vector2 top = this.bulb.transform.localPosition;
            Vector2 bottom = this.pole.localPosition;
            Vector2 vector = top - bottom;
            float scale = vector.magnitude / NormalPoleLength;
            float angleDegrees = (Mathf.Atan2(vector.y, vector.x) * 180 / Mathf.PI) - 90;

            this.pole.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);
            this.pole.transform.localScale = new Vector3(1, scale, 1);
            this.flag.transform.localRotation = this.pole.transform.localRotation;
            this.flag.transform.position = this.pole.transform.TransformPoint(
                new Vector2(
                    NormalFlagPosition.x,
                    (NormalFlagPosition.y + (
                        vector.magnitude + BulbAttachOffset - NormalPoleLength
                    )) / scale
                )
            );
            this.bulb.transform.localRotation = this.pole.transform.localRotation;
        };

        AdvanceArrows((bulbPosition - playerPosition).sqrMagnitude < 10 * 10);
    }

    private void AdvanceArrows(bool isPlayerWithinRange)
    {
        bool showArrows =
            this.active == false &&
            this.map.NearestChunkTo(this.map.player.position) == this.chunk &&
            isPlayerWithinRange == true &&
            this.map.fruitCurrent >= FruitCost;

        if (this.arrowAlpha == 0 && showArrows == true)
            this.arrowDistance = 10;
        else
            this.arrowDistance = Util.Slide(this.arrowDistance, 1.2f, 0.5f);

        if (this.active == true)
            this.arrowAlpha = 0;
        else
            this.arrowAlpha = Util.Slide(this.arrowAlpha, showArrows ? 1 : 0, 0.06f);

        this.arrowTL.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowTR.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowBL.gameObject.SetActive(this.arrowAlpha > 0);
        this.arrowBR.gameObject.SetActive(this.arrowAlpha > 0);

        if (this.arrowAlpha > 0)
        {
            Color c = new Color(1, 1, 1, this.arrowAlpha);
            this.arrowTL.color = c;
            this.arrowTR.color = c;
            this.arrowBL.color = c;
            this.arrowBR.color = c;

            float s =
                this.arrowDistance +
                Mathf.Sin(this.map.frameNumber * 0.1f) * 0.12f;
            this.arrowTL.transform.localPosition = NormalBulbPosition.Add(-s, +s);
            this.arrowTR.transform.localPosition = NormalBulbPosition.Add(+s, +s);
            this.arrowBL.transform.localPosition = NormalBulbPosition.Add(-s, -s);
            this.arrowBR.transform.localPosition = NormalBulbPosition.Add(+s, -s);
        }
    }

    private void Hit()
    {
        this.single.gameObject.SetActive(false);
        this.pieces.SetActive(true);

        this.bulb.Stop();
        this.bulb.GetComponent<SpriteRenderer>().sprite =
            this.animationBulb.sprites[0];

        if(this.active == false)
        {
            if(this.map.fruitCurrent >= FruitCost)
            {
                Activate();
            }
            else
            {
                // effect to indicate the player does not have enough fruits
                IngameUI.instance.FlashFruitCounter(0.30f, Color.red, this.map);
            }
        }
    }

    public void Activate()
    {
        this.active = true;
        this.map.RemoveFruit(FruitCost);
        
        // effect to move fruits from UI to the blender position
        //IngameUI.instance.uiFruit.MoveFruit(10, this.juice.transform);
        IngameUI.instance.FlashFruitCounter(0.50f, Color.black, this.map);

        Audio.instance.PlaySfx(Assets.instance.sfxReachCheckpoint);

        this.juice.PlayOnce(this.animationJuiceBlendSmall, () =>
        {
            this.juice.PlayOnce(this.animationJuiceRise, () =>
            {
                this.juice.PlayOnce(this.animationJuiceBlendBig, () =>
                {
                    this.juice.PlayAndLoop(this.animationJuiceSteady);

                    this.flag.GetComponent<SpriteRenderer>().enabled = true;
                    this.flag.PlayOnce(this.animationFlagUnfold, delegate
                    {
                        this.flag.PlayAndLoop(this.animationFlagIdle);
                    });
                });
            });
        });

        this.map.player.SetCheckpointPosition(
            this.position + new Vector2(0, Player.PivotDistanceAboveFeet + ExtraBlenderHeight)
        );
    }

    private IEnumerator TintFlashCoroutine()
    {
        Tint(Color.black, 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(new Color32(179, 94, 121, 255), 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 1);
        do
        {
            yield return new WaitForSeconds(1 / 15f);
        }
        while (Time.timeScale == 0);

        Tint(Color.white, 0);
    }

    private void Tint(Color color, float intensity)
    {
        void TintSprite(SpriteRenderer sr)
        {
            sr.GetPropertyBlock(this.propertyBlock);
            this.propertyBlock.SetColor("_Color", color);
            this.propertyBlock.SetFloat("_Intensity", intensity);
            sr.SetPropertyBlock(this.propertyBlock);
        }

        TintSprite(this.single  .GetComponent<SpriteRenderer>());
        TintSprite(this.bulb    .GetComponent<SpriteRenderer>());
        TintSprite(this.blenderBase.GetComponent<SpriteRenderer>());
        TintSprite(this.pole    .GetComponent<SpriteRenderer>());
        TintSprite(this.flag    .GetComponent<SpriteRenderer>());
        TintSprite(this.blade    .GetComponent<SpriteRenderer>());
        TintSprite(this.juice    .GetComponent<SpriteRenderer>());
    }
}
