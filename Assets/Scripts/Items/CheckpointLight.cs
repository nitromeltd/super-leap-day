
using UnityEngine;

public class CheckpointLight : Item
{
    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
    }
}
