using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Item
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationOpen;

    public enum Prize
    {
        NONE,
        ONE_COIN,
        THREE_COINS,
        POWERUP,
        KEY,
        CHECKPOINT,
        Length
    }

    public bool randomPrize = false;
    public Prize prize = Prize.NONE;

    [Header("Flying Chest")]
    public bool isFlying = false;
    public Animated wingRightAnimated;
    public Animated wingLeftAnimated;
    public Animated.Animation animationWing;

    [Header("Locked Chest")]
    public bool isLocked = false;
    private const float MinPlayerDistanceOpen = 4f;
    public GameObject LockedChains;
    public Animated.Animation animationChainBreak;
    public Animated.Animation animationChainInvisible;

    [Header("Parts")]
    public bool useParts;
    public SpriteRenderer top;
    public SpriteRenderer bottom;
    public AnimationCurve checkPointScaleCurve;

    // path handling
    private NitromeEditor.Path path;
    private float offsetThroughPath;

    private bool open = false;
    private bool opening = false;
    private Vector2 initialPosition;
    private bool hopping = false;
    private int hopTimer = 0;
    private const int HopTotalTime = 30;

    public bool Opened => this.open == true;

    private Vector3 scaleTarget = Vector3.one;
    private Vector3 scaleVelocity = Vector3.zero;
    private float xScaleValue = 0f;

    // gravity
    private Gravity gravity;
    private Vector2 velocity;
    private float gravityAcceleration = 0.02f;
    private Tile onGroundTile = null;
    private Item onGroundItem = null;
    private bool IsOnGround => this.onGroundTile != null || this.onGroundItem is StartBarrier;
    private float Bounciness = -0.05f;

    private bool becameGold = false;
    private bool hopOpen = false;

    public struct Gravity
    {
        public Chest chest;
        public Player.Orientation gravityDirection;

        public Player.Orientation Orientation() => this.gravityDirection;

        private static Vector2[] Directions =
            new [] { Vector2.down, Vector2.right, Vector2.up, Vector2.left };

        public Vector2 Down()  => Directions[(int)Orientation()];
        public Vector2 Right() => Directions[((int)Orientation() + 1) % 4];
        public Vector2 Up()    => Directions[((int)Orientation() + 2) % 4];
        public Vector2 Left()  => Directions[((int)Orientation() + 3) % 4];

        public int OrientationAngle()
        {
            switch(Orientation())
            {
                case Player.Orientation.Normal:
                default:
                    return 0;

                case Player.Orientation.LeftWall:
                    return 270;

                case Player.Orientation.RightWall:
                    return 90;

                case Player.Orientation.Ceiling:
                    return 180;
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => Orientation() switch
            {
                Player.Orientation.Normal    => chest.velocity,
                Player.Orientation.RightWall => new Vector2(chest.velocity.y, -chest.velocity.x),
                Player.Orientation.Ceiling   => new Vector2(-chest.velocity.x, -chest.velocity.y),
                _                            => new Vector2(-chest.velocity.y, chest.velocity.x)
            };
            set {
                switch (Orientation())
                {
                    case Player.Orientation.Normal:    chest.velocity = value; break;
                    case Player.Orientation.RightWall: chest.velocity = new Vector2(-value.y, value.x); break;
                    case Player.Orientation.Ceiling:   chest.velocity = new Vector2(-value.x, -value.y); break;
                    default:                           chest.velocity = new Vector2(value.y, -value.x); break;
                }
            }
        }
    }

    // flying
    private Vector2 basePosition;
    private Vector2 targetPos;
    private float breatheTimer;

    private const float BreathePeriod = .5f;
    private const float BreatheAmplitude = 1f;

    private Item prizeItem = null;

    // Prize: Five Coins
    public struct CoinPrize
    {
        public Coin coin;
        public Vector2 targetPos;
        public Vector2 smoothVelocity;

        public CoinPrize(Coin _coin, Vector2 _targetPos)
        {
            coin = _coin;
            targetPos = _targetPos;
            smoothVelocity = Vector2.zero;
        }
    }

    private CoinPrize[] prizeCoins = null;
    private bool moveFiveCoins = false;

    // Prize: Checkpoint
    private float checkpointScaleY;
    private float checkpointScaleTimer;
    private const float CheckpointExtraHeight = 0.5f;
    private const int CheckpointScaleTotalTime = 15;

    // Sprout sucking ability
    protected bool suckActive = false;
    protected int suckShakeTimer = 0;
    protected Vector2 smoothSuckVel;
    protected float smoothSuckTime;
    protected Vector2 suckShakeAmount;
    protected const int SuckShakeTime = 20;
    protected readonly float[] suckShakeDeltas = new float[] { 1, 1, 0, 0, -1, -1, 1, -1, 0 };

    // Lightup Block
    [HideInInspector] public bool isConnectedToLightBlock = false;
    private LightUpBlock connectedLightBlock;

    public static Chest.Prize[] PossiblePrizes = new Chest.Prize[]
    {
        Chest.Prize.ONE_COIN,
        Chest.Prize.THREE_COINS,
        Chest.Prize.POWERUP
    };

    public override void Init(Def def)
    {
        base.Init(def);

        if(this.isFlying == true)
        {
            SetUpFollowingMethod(def.startChunk);
        }

        this.xScaleValue = def.tile.flip ? -1f : 1f;
        this.scaleTarget = new Vector3(xScaleValue, 1f, 1f);
        this.transform.localScale = this.scaleTarget;

        this.open = false;

        this.solidToEnemy = false;

        this.basePosition = this.position;
        this.onCollisionFromBelow = OnCollisionFromBelow;
        this.onCollisionFromAbove = OnCollisionFromAbove;
        this.onCollisionFromLeft = OnCollisionFromLeft;
        this.onCollisionFromRight = OnCollisionFromRight;
        
        this.animated.PlayOnce(this.animationIdle);

        if(this.randomPrize)
        {
            this.prize =
                PossiblePrizes[UnityEngine.Random.Range(0, PossiblePrizes.Length)];
        }

        if(this.wingRightAnimated != null)
        {
            this.wingRightAnimated.PlayAndLoop(animationWing);
        }

        if(this.wingLeftAnimated != null)
        {
            this.wingLeftAnimated.PlayAndLoop(animationWing);
        }
        
        this.gravity = new Gravity {
                chest = this,
                gravityDirection = this.map.player.gravity.Orientation()
        };

        this.isConnectedToLightBlock = false;
        var node = this.chunk.connectionLayer.NearestNodeTo(this.position, 2.5f);
        if (node != null)
        {
            var index = Array.IndexOf(node.path.nodes, node);
            var otherIndex = index == 0 ? node.path.nodes.Length - 1 : 0;
            var otherNode = node.path.nodes[otherIndex];
            this.connectedLightBlock = this.chunk.NearestItemTo<LightUpBlock>(otherNode.Position);
            if(this.connectedLightBlock != null)
            {
                this.isConnectedToLightBlock = true;
                this.spriteRenderer.color = new Color(1, 1, 1, 0);
                this.hitboxes = new[] {
                    new Hitbox(-0.5f, 0.5f, 0f, 1.5f, this.rotation, Hitbox.NonSolid)
                };

                if (this.isLocked == true)
                {
                    this.LockedChains.GetComponent<SpriteRenderer>().enabled = false;
                }
                else if(this.isFlying == true)
                {
                    this.wingLeftAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                    this.wingRightAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
                }
            }
            else
            {
                this.isConnectedToLightBlock = false;
                ToggleSolidHitbox(false);
            }
        }
        else
        {
            this.isConnectedToLightBlock = false;
            ToggleSolidHitbox(true);
        }

        this.becameGold = false;
        this.hopOpen = false;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;
    }

    public static Chest CreateChestWithPowerup(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.chestPowerup, chunk.transform);
        obj.name = "Chest with Power-up";
        var item = obj.GetComponent<Chest>();
        item.Init(position, chunk);
        chunk.items.Add(item);

        return item;
    }

    public static Chest CreateFlyingChestWithPowerup(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.chestPowerupFlying, chunk.transform);
        obj.name = "Flying Chest with Power-up";
        var item = obj.GetComponent<Chest>();
        item.isFlying = true;
        item.path = Chest.PathForFlyingChestWithPowerup(position);
        item.Init(position, chunk);
        item.basePosition = position;
        chunk.items.Add(item);

        return item;
    }

    private static NitromeEditor.Path PathForFlyingChestWithPowerup(Vector2 position)
    {
        Vector2 initialPosition = position;
        Vector2 finalPosition = position.Add(8f, 0f);

        var path = new NitromeEditor.Path();
        path.nodes = new NitromeEditor.Path.Node[] {
            new NitromeEditor.Path.Node(path, initialPosition.x, initialPosition.y),
            new NitromeEditor.Path.Node(path, finalPosition.x, finalPosition.y),
        };
        path.closed = true;
        path.RecalculateEdgesAndLengths();
        foreach (var edge in path.edges)
            edge.speedPixelsPerSecond = 4;
        path.Recalculate();
        
        return path;
    }

    private void ToggleSolidHitbox(bool activateSolid)
    {
        float yMax = this.open ? 1.225f : 1.5f;
        Hitbox.SolidData solidData = activateSolid ? Hitbox.Solid : Hitbox.NonSolid;

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, 0f, yMax, this.rotation, solidData)
        };
    }

    private void SetUpFollowingMethod(Chunk chunk)
    {
        this.group = null;
        var g = chunk.groupLayer.RegisterFollower(this, def.tx, def.ty);
        if (g != null)
        {
            AttachToGroup(g);
            return;
        }

        this.path = (this.path == null) ?
            chunk.pathLayer.NearestPathTo(this.position, 2) : this.path;
        if (this.path != null)
        {
            this.offsetThroughPath = TimeNearestToPoint(this.path, this.position);
            this.path = Util.MakeClosedPathByPingPonging(this.path);
            return;
        }

        var followPath =
            chunk.connectionLayer.NearestPathTo(this.position, 2f);

        if (followPath != null)
        {
            var followPosition = followPath.nodes[0].Position;
        }
    }

    public static float TimeNearestToPoint(NitromeEditor.Path path, Vector2 pt)
    {
        var edgeIndex = path.NearestEdgeIndexTo(pt).Value;
        var edge = path.edges[edgeIndex];

        var edgeDelta = edge.to.Position - edge.from.Position;
        var through =
            Vector2.Dot(edgeDelta, pt - edge.from.Position) / edgeDelta.sqrMagnitude;
        through = Mathf.Clamp01(through);

        float result = 0.0f;
        for (var n = 0; n < edgeIndex; n += 1)
        {
            result += path.edges[n].distance / path.edges[n].speedPixelsPerSecond;
        }
        result += through * (edge.distance / edge.speedPixelsPerSecond);
        return result;
    }
    
    public override void Advance()
    {
        // smooth scale manipulation for squash & strecth effect
        this.transform.localScale = Vector3.SmoothDamp(this.transform.localScale,
            this.scaleTarget, ref scaleVelocity, .05f);

        AdvancePrizes();

        // movement
        if(this.isFlying == true)
        {
            if (this.group != null)
            {
                this.basePosition = this.group.FollowerPosition(this);
            }
            else if (this.path != null)
            {
                this.basePosition = this.path.PointAtTime(
                    (this.map.frameNumber / 60.0f) + this.offsetThroughPath
                ).position;
            }

            AdvanceFly();

            // adjust wing flapping animation based on velocity
            float clampMagnitude = Mathf.Clamp( this.velocity.magnitude, 0.05f, 0.1f);
            this.animationWing.fps = Mathf.RoundToInt(
                Mathf.Lerp(30, 60, Mathf.InverseLerp(.05f, 0.1f, clampMagnitude))
            );
        }
        else
        {
            UpdateGravity();
            CheckHop();

            if (this.group != null)
            {
                this.transform.position = this.position =
                    this.group.FollowerPosition(this);
            }

            AdvanceFall();

            if (this.isLocked == true)
            {
                CheckOpen();
            }
        }
        
        UpdateSuck();
    }

    private void CheckHop()
    {
        var centerpoint = this.position + (this.gravity.Up() * 0.5f);

        var playerRelative = new Vector2(
            Vector2.Dot(this.gravity.Right(), this.map.player.position - centerpoint),
            Vector2.Dot(this.gravity.Up(), this.map.player.position - centerpoint)
        );
        var playerForward =
            playerRelative.x * Mathf.Sign(Vector2.Dot(this.velocity, this.gravity.Right()));
        float verticalDistance = 1f;
        float horizontalDistance = 1.50f;

        if(this.hopTimer > 0)
        {
            this.hopTimer -= 1;

            if(this.hopTimer == 0)
            {
                this.hopping = false;
                ToggleSolidHitbox(true);
                if(this.map.player.midasTouch.isActive == true && this.hopOpen == true)
                {
                    Open();
                }
            }
        }
        
        if (this.hopping == false &&
            this.map.player.alive == true &&
            this.map.player.state != Player.State.Respawning &&
            Mathf.Abs(playerRelative.y) < verticalDistance &&
            (playerForward > -horizontalDistance && playerForward < horizontalDistance) &&
            (this.hitboxes.Length > 0 && this.hitboxes[0].solidXMax == true))
        {
            this.hopping = true;
            this.hopTimer = HopTotalTime;
            ToggleSolidHitbox(false);
            this.velocity += this.gravity.Up() * 0.25f;
            
            Particle.CreateDust(this.transform.parent, 5, 7,
                this.position + this.gravity.Up() * 0.25f, .5f, .5f, this.gravity.Left() * .05f);

            Particle.CreateDust(this.transform.parent, 5, 7,
                this.position + this.gravity.Up() * 0.25f, .5f, .5f, this.gravity.Right() * .05f);

            if (this.map.player.midasTouch.isActive == true && 
                this.becameGold == false && 
                this.open == false && 
                this.prize != Prize.KEY)
            {
                StartCoroutine(TurnGold(false));
                this.hopOpen = true;
            }
        }
    }

    private void AdvancePrizes()
    {
        switch(this.prize)
        {
            case Prize.THREE_COINS:
            {
                MoveThreeCoins();
            }
            break;

            case Prize.POWERUP:
            {
                if(prizeItem as PowerupPickup)
                { 
                    if(this.prizeItem != null &&
                        (this.prizeItem as PowerupPickup).suckActive == false &&
                        (this.prizeItem as PowerupPickup).collected == false)
                    {
                        this.prizeItem.position = this.position + (Vector2)this.transform.up * 2f;
                    }
                }
            }
            break;

            case Prize.KEY:
            {
                if(this.prizeItem != null &&
                    (this.prizeItem as GateKey).collected == false &&
                    (this.prizeItem as GateKey).suckActive == false)
                {
                    this.prizeItem.position = this.position + (Vector2)this.transform.up * 2.5f;
                }
            }
            break;

            case Prize.CHECKPOINT:
            {
                MoveCheckpoint();
            }
            break;
        }
    }

    private void AdvanceFly()
    {
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);
        
        // ignore breathe movement if is following path or is part of group
        if(this.path != null || this.group != null)
        {
            distance = 0f;
        }   

        this.targetPos = this.basePosition + (Vector2)this.transform.up * distance;
        
        Vector2 targetVel = (targetPos - this.position) * .05f;

        this.velocity *= 0.95f;
        this.velocity += targetVel * .2f;

        this.position += this.velocity;
        this.transform.position = this.position;
    }

    private void AdvanceFall()
    {
        AdvanceGravity();
        Integrate();

        if(this.hopping == false)
        {
            // check grounded
            var rFloor = new Raycast(this.map, this.position).Arbitrary(
                this.position + this.gravity.Down() * 0.15f,
                this.gravity.Down(),
                0.10f
            );
            this.onGroundTile = rFloor.tile;
            this.onGroundItem = rFloor.item;
        }

        if(this.onGroundTile != null)
        {
            Vector2 surfaceVelocity = this.onGroundTile.tileGrid.effectiveVelocity;
            this.position.x += surfaceVelocity.x;
            this.position.y += surfaceVelocity.y;
        }

        this.transform.position = this.position;
    }
    
    protected void UpdateGravity()
    {
        Player.Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        var intendedGravity = SampleAt(this.position);
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        if (this.gravity.gravityDirection == Player.Orientation.Ceiling &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Normal) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Player.Orientation.Ceiling)
                intendedGravity = Player.Orientation.Ceiling;
        }
        else if (this.gravity.gravityDirection == Player.Orientation.Normal &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Ceiling) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Player.Orientation.Normal)
                intendedGravity = Player.Orientation.Normal;
        }

        // the chest doesn't really benefit from a "null" gravity state, since it
        // always falls back to the floor even in zero-g.
        if (intendedGravity == null)
        {
            intendedGravity = this.rotation switch
            {
                90  => Player.Orientation.RightWall,
                180 => Player.Orientation.Ceiling,
                270 => Player.Orientation.LeftWall,
                _   => Player.Orientation.Normal
            };
        }

        if (this.gravity.gravityDirection != intendedGravity)
        {
            this.gravity.gravityDirection = intendedGravity.Value;
        }
    }

    protected void AdvanceGravity()
    {
        var gravityDirectionVector = this.gravity.Down();
        this.velocity += this.gravityAcceleration * gravityDirectionVector;
    }
    
    private void Integrate()
    {
        // ignore self collision (rectangle is solid)
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        var rect = this.hitboxes[0];

        if (this.velocity.x < 0f)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0),
                false,
                -this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity *=  Vector2.one * Bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if(this.velocity.x > 0f)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0),
                true,
                this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity *=  Vector2.one * Bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y < 0f)
        {
            var start = this.position.Add(0, rect.yMin);
            var maxDistance = -this.velocity.y;
            var r = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.375f, 0f), false, maxDistance)
            );

            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.velocity *=  Vector2.one * Bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if(this.velocity.y > 0f)
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMax),
                true,
                this.velocity.y
            );
            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity *=  Vector2.one * Bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private void OnCollisionFromBelow(Collision collision)
    {
        if(this.isFlying)
        {
            Vector2 playerMoveDir = this.map.player.velocity.normalized;
            playerMoveDir.x *= 0.01f;
            playerMoveDir.y = 0.2f;
            this.velocity = playerMoveDir;
        }
        else
        {
            this.velocity.y = .1f;
        }

        StartCoroutine(_SquashStretch(new Vector3(0.8f, 1.2f, 1f), .05f));

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.left * .05f);

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.position + Vector2.up * 0.25f, .5f, .5f, Vector2.right * .05f);

        if(CanBeOpened() == true)
        {
            this.opening = true;
            Open();
        }
    }

    private void OnCollisionFromAbove(Collision collision)
    {
        if (this.opening == false &&
            this.open == false &&
            this.map.player.gravity.VelocityInGravityReferenceFrame.y <= -0.1f)
        {
            TriggerFromAbove();
        }

        if(this.isFlying == true)
        {
            Vector2 playerMoveDir = this.map.player.velocity.normalized;
            playerMoveDir.x *= 0.01f;
            playerMoveDir.y = -0.1f;
            this.velocity = playerMoveDir;
        }
    }

    private void OnCollisionFromLeft(Collision collision)
    {
        if(this.isFlying)
        {
            Vector2 playerMoveDir = this.map.player.velocity.normalized;
            playerMoveDir.x = 0.1f;
            playerMoveDir.y *= 0.01f;
            this.velocity = playerMoveDir;
        }
    }

    private void OnCollisionFromRight(Collision collision)
    {
        if(this.isFlying)
        {
            Vector2 playerMoveDir = this.map.player.velocity.normalized;
            playerMoveDir.x = -0.1f;
            playerMoveDir.y *= 0.01f;
            this.velocity = playerMoveDir;
        }
    }

    private void TriggerFromAbove()
    {
        if(CanBeOpened() == false) return;

        this.opening = true;

        if(this.isFlying)
        {
            Vector2 playerMoveDir = this.map.player.gravity.VelocityInGravityReferenceFrame.normalized;
            playerMoveDir.x *= 0.01f;
            playerMoveDir.y = -0.2f;

            this.velocity = playerMoveDir;
        }

        this.map.player.BounceOffHitItem(this.transform.position, true);

        Particle.CreateDust(this.transform.parent, 8, 10,
            this.position + (Vector2)this.transform.up * 1f, .5f, .5f, Vector2.left * .1f);

        Particle.CreateDust(this.transform.parent, 8, 10,
            this.position + (Vector2)this.transform.up * 1f, .5f, .5f, Vector2.right * .1f);

        StartCoroutine(_SquashStretch(new Vector3(1.4f, 0.6f, 1f), .1f, Open));
    }

    public void OnCollisionFromSide()
    {
        if(CanBeOpened() == false) return;

        this.opening = true;

        StartCoroutine(_SquashStretch(new Vector3(0.8f, 1.2f, 1f), .05f, Open));
    }

    private System.Collections.IEnumerator _SquashStretch(Vector2 squashScale,
        float time, System.Action onComplete = null)
    {
        this.scaleTarget = new Vector3(squashScale.x * xScaleValue, squashScale.y, 1f);
        yield return new WaitForSeconds(time);
        this.scaleTarget = new Vector3(xScaleValue, 1f, 1f);

        if(onComplete != null)
        {
            onComplete();
        }
    }

    public bool CanBeOpened()
    {
        if(this.isLocked == true) return false;
        if(this.open == true) return false;
        if(this.opening == true) return false;

        return true;
    }

    private void CheckOpen()
    {
        if (this.open == true) return;

        if (this.opening == false && this.isLocked)
        {
            float distToPlayer = Vector2.Distance(this.map.player.position, this.position);

            if (distToPlayer < MinPlayerDistanceOpen && this.map.player.HaveAvailableKey())
            {
                this.opening = true;
                this.map.player.OpenChestWithKey(this);
            }
        }
    }

    public void Open()
    {
        if (this.open == true) return;

        this.open = true;

        if (this.isLocked == false)
        {
            if(this.map.player.midasTouch.isActive == true && this.prize != Prize.KEY)
            {
                if(becameGold == true)
                {
                    GoldOpen();
                }
                else
                {
                    StartCoroutine(TurnGold(true));
                }
                return;
            }
            this.animated.PlayAndHoldLastFrame(this.animationOpen);

            if (this.isFlying == true)
            {
                this.animated.OnFrame(7, () => {
                    this.wingRightAnimated.transform.localPosition = new Vector3(1.257f, 2.3f);
                    this.wingLeftAnimated.transform.localPosition = new Vector3(1.04f, 2.3f);
                });
            }
            
            this.animated.OnFrame(10, () =>
            {
                this.opening = false;
                if (this.useParts == true)
                {
                    this.spriteRenderer.enabled = false;

                    this.top.enabled = true;
                    this.bottom.enabled = true;
                }

                CreatePrize();
            });

            if(this.hopping == false)
            {
                ToggleSolidHitbox(true);
            }

            Audio.instance.PlaySfx(Assets.instance.sfxChestOpen,        position: this.position);
            Audio.instance.PlaySfx(Assets.instance.sfxChestMimicUnlock, position: this.position);
        }
        else
        {
            StartCoroutine(OpenLock());
        }
    }

    public void CloseFromFlyingRingSequence()
    {
        this.open = false;
        this.animated.PlayOnce(this.animationIdle);

        this.spriteRenderer.color = new Color(0f, 0f, 0f, 0f);
        ToggleSolidHitbox(false);
        
        if (this.isLocked == true)
        {
            this.LockedChains.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (this.isFlying == true)
        {
            this.wingLeftAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            this.wingRightAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }

        if(this.prizeItem != null)
        {
            this.chunk.items.Remove(this.prizeItem);
            Destroy(this.prizeItem.gameObject);
        }
    }

    private void CreatePrize()
    {
        switch(prize)
        {
            case Prize.ONE_COIN:
            {
                Coin coin = Coin.Create(
                    this.position + (Vector2)this.transform.up * 2.5f, this.chunk);
                coin.transform.rotation = this.transform.rotation;

                StartCoroutine(_CollectDelay(coin, 0.1f));
            }
            break;

            case Prize.THREE_COINS:
            {
                StartCoroutine(_CreateThreeCoins());
            }
            break;

            case Prize.POWERUP:
            {
                PowerupPickup powerupPickup;
                if (SaveData.IsInfiniteJumpPurchased() == true && SaveData.RecievedInfiniteJump() == false)
                {
                    SaveData.SetRecievedInfiniteJump(true);
                    powerupPickup = PowerupPickup.Create(this.position + (Vector2)this.transform.up * 2f, this.chunk, PowerupPickup.Type.InfiniteJump);
                }
                else if (SaveData.IsSwordAndShieldPurchased() == true && SaveData.RecievedSwordAndShield() == false)
                {
                    SaveData.SetRecievedSwordAndShield(true);
                    powerupPickup = PowerupPickup.Create(this.position + (Vector2)this.transform.up * 2f, this.chunk, PowerupPickup.Type.SwordAndShield);
                }
                else if (SaveData.IsMidasTouchPurchased() == true && SaveData.RecievedMidasTouch() == false)
                {
                    SaveData.SetRecievedMidasTouch(true);
                    powerupPickup = PowerupPickup.Create(this.position + (Vector2)this.transform.up * 2f, this.chunk, PowerupPickup.Type.MidasTouch);
                }
                else
                {
                    if(Multiplayer.IsMultiplayerGame() && UnityEngine.Random.value <= PowerupPickup.MultiplayerPowerupChance)
                    {
                        powerupPickup = PowerupPickup.CreateRandomMultiplayerPowerup(this.position + (Vector2)this.transform.up * 2f, this.chunk);
                    }
                    else
                    {
                        if(UnityEngine.Random.value <= 0.50f)
                        {
                            powerupPickup = PowerupPickup.CreateRandomPowerup(this.position + (Vector2)this.transform.up * 2f, this.chunk);
                        }
                        else
                        {
                            BubblePickup bubble = BubblePickup.CreateRandomPowerup(this.position + (Vector2)this.transform.up * 2f, this.chunk);
                            powerupPickup = null;
                            this.prizeItem = bubble;
                        }
                    }
                } 

                if(powerupPickup != null)
                {
                    powerupPickup.transform.rotation = this.transform.rotation;
                    powerupPickup.initialPos = powerupPickup.targetPos = powerupPickup.position + (Vector2)this.transform.up * 1f;
                    this.prizeItem = powerupPickup;
                }
            }
            break;

            case Prize.KEY:
            {
                GateKey key = GateKey.Create(
                    this.position + (Vector2)this.transform.up * 2.5f, this.chunk);
                this.prizeItem = key;
                key.rotation = this.rotation;
                    
                key.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
            }
            break;

            case Prize.CHECKPOINT:
            {
                Checkpoint checkpoint = Checkpoint.Create(
                    this.position + (Vector2)this.transform.up * CheckpointExtraHeight, this.chunk);
                this.prizeItem = checkpoint;

                checkpoint.transform.localScale = Vector3.zero;

                checkpoint.flagUnfoldWaitTime = 0.25f;
                checkpoint.Activate(false);
            }
            break;
        }
    }

    private System.Collections.IEnumerator _CollectDelay(Pickup pickup, float delay)
    {
        yield return new WaitForSeconds(delay);
        pickup.Collect();
    }

    private System.Collections.IEnumerator _CreateThreeCoins()
    {
        int coinsAmount = 3;
        float circleRadius = 1.75f;

        this.prizeCoins = new CoinPrize[coinsAmount];
        Vector2 initialPos = this.position + (Vector2)this.transform.up * 2f;
        Vector2 centerPos = this.position + (Vector2)this.transform.up * 4f;

        for (int i = 0; i < prizeCoins.Length; i++)
        {
            Coin coin = Coin.Create(initialPos, this.chunk);
            coin.transform.rotation = this.transform.rotation;
            prizeCoins[i] = new CoinPrize(coin, centerPos);
        }

        this.moveFiveCoins = true;

        yield return new WaitForSeconds(.5f);

        for (int i = prizeCoins.Length - 1; i >= 0; i--)
        {
            if(prizeCoins[i].coin == null) continue;

            float currentAngle = i * Mathf.PI * 2f / coinsAmount;

            prizeCoins[i].targetPos = centerPos + 
                new Vector2(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle))
                * circleRadius;
            
            //yield return new WaitForSeconds(.1f); // add delay to coins movement
        }

        yield return new WaitForSeconds(.5f);

        for (int i = prizeCoins.Length - 1; i >= 0; i--)
        {
            if(prizeCoins[i].coin == null) continue;

            if(prizeCoins[i].coin.suckActive == true)
            {
                this.moveFiveCoins = false;
                break;
            }
            
            (prizeCoins[i].coin as Coin)?.Collect();
            yield return new WaitForSeconds(.1f);
        }
        
        this.moveFiveCoins = false;
    }

    private void MoveThreeCoins()
    {
        if(this.moveFiveCoins == false) return;

        for (int i = 0; i < this.prizeCoins.Length; i++)
        {
            if(this.prizeCoins[i].coin == null) continue;

            prizeCoins[i].coin.position = Vector2.SmoothDamp(prizeCoins[i].coin.position,
                prizeCoins[i].targetPos, ref prizeCoins[i].smoothVelocity, 0.2f);
            
            prizeCoins[i].coin.transform.position = prizeCoins[i].coin.position;
        }
    }

    private void MoveCheckpoint()
    {
        if(this.prizeItem == null) return;

        this.prizeItem.transform.position =
            this.position.Add(0f, CheckpointExtraHeight);

        this.checkpointScaleTimer += 1;

        float t = Mathf.InverseLerp(0, CheckpointScaleTotalTime, this.checkpointScaleTimer);
        this.checkpointScaleY = this.checkPointScaleCurve.Evaluate(t);

        this.prizeItem.transform.localScale = new Vector3(1f, this.checkpointScaleY, 1f);
    }

    private void UpdateSuck()
    {
        if (this.isLocked == true || this.suckActive == false) return;

        var positionWithShake = this.position;

        positionWithShake += new Vector2(
            suckShakeAmount.x *
            suckShakeDeltas[this.map.frameNumber % suckShakeDeltas.Length],
            suckShakeAmount.y *
            suckShakeDeltas[this.map.frameNumber % suckShakeDeltas.Length]
        );
        
        this.transform.position = new Vector2(
            positionWithShake.x,
            positionWithShake.y);
            
        if(this.map.frameNumber % 12 == 0)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                this.hitboxes[0].InWorldSpace().center.Add(
                    UnityEngine.Random.Range(-0.5f, 0.5f),
                    UnityEngine.Random.Range(-0.5f, 0.5f)
                ),
                this.transform.parent
            );
            sparkle.spriteRenderer.sortingLayerName = "Items";
            sparkle.spriteRenderer.sortingOrder = 1;
        }
    }

    public void EnterSuck()
    {
        if (this.isLocked == true || this.suckActive == true) return;

        this.suckActive = true;
        this.suckShakeAmount = Vector2.one * 0.02f;
        this.initialPosition = this.position;
    }

    public void ExitSuck()
    {
        if (this.isLocked == true || this.suckActive == false) return;

        this.suckActive = false;
        this.transform.position = this.initialPosition;
    }

    public Vector2 GetLockPosition()
    {
        return this.LockedChains.transform.position;
    }

    public void LightUpReveal()
    {
        Show();
    }

    public void Show()
    {
        this.spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
        StartCoroutine(_SquashStretch(new Vector3(0.5f, 0.5f, 1f), .1f));

        ToggleSolidHitbox(true);

        Particle.CreateDust(this.transform.parent, 20, 30, this.position, 1.5f, 1.5f);
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .5f);

        if (this.isLocked == true)
        {
            this.LockedChains.GetComponent<SpriteRenderer>().enabled = true;
        }
        else if (this.isFlying == true)
        {
            this.wingLeftAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            this.wingRightAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = true;
        }
    }

    public void Hide()
    {
        this.spriteRenderer.color = new Color(0f, 0f, 0f, 0f);

        ToggleSolidHitbox(false);
        
        if (this.isLocked == true)
        {
            this.LockedChains.GetComponent<SpriteRenderer>().enabled = false;
        }
        else if (this.isFlying == true)
        {
            this.wingLeftAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
            this.wingRightAnimated.gameObject.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    private IEnumerator OpenLock()
    {
        LockedChains.GetComponent<Animated>().PlayOnce(this.animationChainBreak, () =>
        {
            LockedChains.GetComponent<Animated>().PlayAndLoop(this.animationChainInvisible);
        });
        LockedChains.GetComponent<Animated>().OnFrame(3, () =>
        {
            this.map.ScreenShakeAtPosition(this.LockedChains.transform.position, Vector2.one * .3f);
        });

        yield return new WaitForSeconds(.6f);

        this.animated.PlayAndHoldLastFrame(this.animationOpen);

        this.animated.OnFrame(10, () =>
        {
            this.opening = false;
            if (this.useParts == true)
            {
                this.spriteRenderer.enabled = false;

                this.top.enabled = true;
                this.bottom.enabled = true;
            }

            CreatePrize();
        });

        if(this.hopping == false)
        {
            ToggleSolidHitbox(true);
        }

        Audio.instance.PlaySfx(Assets.instance.sfxChestOpen, position: this.position);

        StartCoroutine(_SquashStretch(new Vector3(0.8f, 1.2f, 1f), .05f));
        this.isLocked = false;
    }

    private IEnumerator TurnGold(bool open)
    {
        this.becameGold = true;
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.white;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.black;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = new Color(255, 165, 0);
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spriteGoldTint;
        yield return new WaitForSeconds(.5f);  
        if(open == true)
        {
            GoldOpen();
        }
    }

    private void GoldOpen()
    {
        this.animated.PlayAndHoldLastFrame(this.animationOpen);

        this.animated.OnFrame(10, () =>
        {
            this.opening = false;
            if (this.useParts == true)
            {
                this.spriteRenderer.enabled = false;

                this.top.enabled = true;
                this.bottom.enabled = true;
            }

            this.prize = Prize.THREE_COINS;
            CreatePrize();
        });

        if (this.hopping == false)
        {
            ToggleSolidHitbox(true);
        }

        Audio.instance.PlaySfx(Assets.instance.sfxChestOpen, position: this.position);

        StartCoroutine(_SquashStretch(new Vector3(0.8f, 1.2f, 1f), .05f));
    }

    public override void Reset()
    {
        base.Reset();
        if(this.becameGold == true)
        {
            this.spriteRenderer.material = Assets.instance.spritesDefaultMaterial;
        }
    }
}
