using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityCannon : Item
{

    public GameObject lightGO;
    public Transform topTransform;
    public Transform bottomTransform;
    public Transform boltTransform;

    private Transform door1;
    private Transform door2;

    private Vector2 originPosition;
    private Vector2 velocity;
    private NitromeEditor.Path movePath;
    private NitromeEditor.Path rotPath;
    private float currentSpeed;
    private float time;
    private Vector2 bottomPositionVelocity;
    private Vector2 topScaleVelocity;
    private bool lightOn = false;
    private bool playedRotateSfx = false;

    private int timeSinceFire = 0;
    private float visibleAngleRadians = 0;
    private float finalAngleRadians; // from editor
    private float targetAngleRadians;

    private const float CannonScale = 0.8f;
    private const float MoveToTargetSpeed = 10.00f;
    private const float ReturnToOriginSpeed = 3.50f;
    private const float ShotAngleRadians = 180f * Mathf.Deg2Rad;
    private const float MoveToTargetAngleSpeed = 0.25f;
    private static Vector2 FireForce = new Vector2(0.5f, 0.7f);
    private static Vector2 BottomDefaultPosition = Vector2.up * -1f;
    private static Vector2 BottomFirePosition = Vector2.up * -0.10f;
    private static Vector2 TopFireScale = Vector3.one * 1.15f * CannonScale;

    private Vector2 positionLastFrame;
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.velocity = Vector2.zero;
        var posNode = this.chunk.pathLayer.NearestNodeTo(this.position, 0.50f);
        Vector2 lastNodePos = this.position;

        if(posNode != null)
        {
            this.movePath = posNode.path;

            lastNodePos = this.movePath.nodes[this.movePath.nodes.Length - 1].Position;

            // Create Lane
            float totalDistance = 0f;

            for (int i = 0; i < this.movePath.edges.Length; i++)
            {
                var edge = this.movePath.edges[i];

                float edgeMidDistance = totalDistance + (edge.distance / 2f);
                Vector2 edgeMidPoint = this.movePath.PointAtDistanceAlongPath(edgeMidDistance).position;
                
                GameObject laneGO = Instantiate(
                    Assets.instance.capitalHighway.touchPlatformPathLane,
                    edgeMidPoint,
                    Quaternion.identity,
                    this.transform.parent
                ).gameObject;
                laneGO.name = "city cannon path lane";

                laneGO.transform.localRotation =
                    Quaternion.LookRotation(Vector3.forward, edge.direction);
                laneGO.transform.localScale = new Vector3(1f, 1f * edge.distance, 1f);

                totalDistance += edge.distance;

                void DrawLaneStop(Vector2 stopPosition)
                {
                    GameObject laneEndGO = Instantiate(
                        Assets.instance.capitalHighway.touchPlatformPathLaneStop,
                        stopPosition,
                        Quaternion.identity,
                        this.transform.parent
                    ).gameObject;
                    laneGO.name = "city cannon path lane stop";
                }

                void DrawLaneEnd(Vector2 endPosition, bool flipped)
                {
                    GameObject laneEndGO = Instantiate(
                        Assets.instance.capitalHighway.touchPlatformPathLaneEnd,
                        endPosition,
                        Quaternion.identity,
                        this.transform.parent
                    ).gameObject;
                    laneGO.name = "city cannon path lane end";

                    laneEndGO.transform.localRotation =
                        Quaternion.LookRotation(Vector3.forward, edge.direction);
                    laneEndGO.transform.localScale = new Vector3(1f, flipped ? -1f : 1f, 1f);
                }

                Vector2 toNodePosition = this.movePath.PointAtDistanceAlongPath(totalDistance).position;

                if(this.movePath.closed == true)
                {
                    DrawLaneStop(toNodePosition);
                }
                else
                {
                    if(i < this.movePath.edges.Length - 1)
                    {
                        DrawLaneStop(toNodePosition);
                    }

                    if(i == 0)
                    {
                        DrawLaneEnd(movePath.PointAtDistanceAlongPath(0f).position, false);
                    }

                    if(i == this.movePath.edges.Length - 1)
                    {
                        DrawLaneEnd(toNodePosition, true);
                    }
                }
            }
        }
    
        var rotNode = this.chunk.connectionLayer.NearestNodeTo(lastNodePos, 0.50f);

        if(rotNode != null)
        {
            this.rotPath = rotNode.path;
            
            NitromeEditor.Path.Edge firstRotEdge = this.rotPath.edges[0];
            this.finalAngleRadians =
                Mathf.Atan2(firstRotEdge.direction.y, firstRotEdge.direction.x) + (90f * Mathf.Deg2Rad);
        }

        this.transform.localScale = new Vector3(def.tile.flip ? -CannonScale : CannonScale, CannonScale, 1);
    }

    public override void Advance()
    {
        base.Advance();

        var player = this.map.player;
        
        if(this.movePath != null)
        {
            AdvanceWithPath(player);
        }
        else
        {
            AdvanceWithNoPath(player);
        }

        ControlLight();

        this.transform.localRotation = Quaternion.Euler(
            0, 0, this.visibleAngleRadians * 180 / Mathf.PI
        );
        this.boltTransform.localRotation = Quaternion.Euler(
            0, 0, this.visibleAngleRadians * 180 / Mathf.PI
        );
        // Check enter cannon
        if (this.timeSinceFire == 0 &&
            (player.state == Player.State.Normal ||
            player.state == Player.State.Jump||
            player.state == Player.State.FireFromCityCannon) &&
            player.groundState.onGround == false
        )
        {
            if ((player.position - this.position).sqrMagnitude < 2 * 2)
            {
                player.EnterCityCannon(this);
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvPipeIn,
                    options: new Audio.Options(1f, false, 0f),
                    position: this.position
                );
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnemyConeAlert,
                    options: new Audio.Options(1f, false, 0f),
                    position: this.position
                );
            }
        }

        this.topTransform.localScale = Vector2.SmoothDamp(
            this.topTransform.localScale,
            Vector3.one, 
            ref topScaleVelocity,
            0.10f
        );

        this.bottomTransform.localPosition = Vector2.SmoothDamp(
            this.bottomTransform.localPosition,
            BottomDefaultPosition, 
            ref bottomPositionVelocity,
            0.10f
        );

        this.transform.position = this.position;
    }
    
    private void AdvanceWithPath(Player player)
    {
        // move to target rotation
        float currentTargetAngleRadians = this.targetAngleRadians;

        if (player.InsideCityCannon == this)
        {
            // move to target position
            this.currentSpeed = Util.Slide(this.currentSpeed, MoveToTargetSpeed, 0.50f);

            this.time = this.time < this.movePath.duration ?
                this.time + this.currentSpeed / 60.0f : this.movePath.duration;
            
            this.position = this.movePath.PointAtTime(time).position;

            currentTargetAngleRadians =
                (this.time >= this.movePath.duration) ? this.finalAngleRadians : 0f;
        }
        else
        {
            if (this.timeSinceFire > 0)
            {
                this.timeSinceFire -= 1;

                if (this.timeSinceFire < 1)
                {
                    if (player.state == Player.State.FireFromCityCannon)
                        player.state = Player.State.Normal;
                }

                Particle.CreateDust(this.chunk.transform, 1, 2, player.position);
            }
            
            if(this.timeSinceFire < 15)
            {
                // go back to initial path position
                if(this.time > 0f)
                {
                    this.currentSpeed = ReturnToOriginSpeed;
                    this.time -= this.currentSpeed / 60.0f;
                    this.position = this.movePath.PointAtTime(time).position;
                }

                currentTargetAngleRadians = 0f;
            }
            
            // follow player
            var toPlayer = player.position - this.position;
            bool following = (toPlayer.sqrMagnitude < 8 * 8);

            if(following == true &&
                toPlayer.sqrMagnitude > 0.1f &&
                (player.state == Player.State.Normal || player.state == Player.State.Jump) &&
                this.time <= 0)
            {
                Vector2 directionToPlayer = toPlayer / toPlayer.magnitude;
                currentTargetAngleRadians =
                    Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) + (90f * Mathf.Deg2Rad);

                if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSpaceboosterOpen) && this.playedRotateSfx == false)
                {
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvSpaceboosterOpen,
                        options: new Audio.Options(1f, false, 0f),
                        position: this.position
                    );
                    this.playedRotateSfx = true;
                }
            }
            else
            {
                if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSpaceboosterOpen) || this.playedRotateSfx == true)
                {
                    Audio.instance.StopSfx(Assets.instance.sfxEnvSpaceboosterOpen);
                    this.playedRotateSfx = false;
                }
            }
        }
        
        this.targetAngleRadians = Util.WrapAngleMinus180To180(
            targetAngleRadians - currentTargetAngleRadians
        ) + currentTargetAngleRadians;

        this.targetAngleRadians = currentTargetAngleRadians;

        this.visibleAngleRadians = Util.Slide(
            this.visibleAngleRadians, targetAngleRadians, MoveToTargetAngleSpeed
        );

        if (this.positionLastFrame != this.position &&
            !Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvCannonMove))
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvCannonMove,
                options: new Audio.Options(1f, false, 0f),
                position: this.position
            );
        }
        else if (this.positionLastFrame == this.position &&
            Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvCannonMove))
        {
            Audio.instance.StopSfx(Assets.instance.sfxEnvCannonMove);
        }
        this.positionLastFrame = this.position;
    }

    private void AdvanceWithNoPath(Player player)
    {
        if (player.InsideCityCannon == this)
        {
            this.targetAngleRadians = this.finalAngleRadians;
        }
        else
        {
            if (this.timeSinceFire > 0)
            {
                this.timeSinceFire -= 1;

                if (this.timeSinceFire < 1)
                {
                    if (player.state == Player.State.FireFromCityCannon)
                        player.state = Player.State.Normal;
                }

                Particle.CreateDust(this.chunk.transform, 1, 2, player.position);
            }

            // follow player
            var toPlayer = player.position - this.position;
            bool following = (toPlayer.sqrMagnitude < 8 * 8);

            if(following == true &&
                toPlayer.sqrMagnitude > 0.1f &&
                (player.state == Player.State.Normal || player.state == Player.State.Jump))
            {
                Vector2 directionToPlayer = toPlayer / toPlayer.magnitude;
                this.targetAngleRadians =
                    Mathf.Atan2(directionToPlayer.y, directionToPlayer.x) + (90f * Mathf.Deg2Rad);

                if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSpaceboosterOpen) && this.playedRotateSfx == false)
                {
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvSpaceboosterOpen,
                        options: new Audio.Options(1f, false, 0f),
                        position: this.position
                    );
                    this.playedRotateSfx = true;
                }
            }
            else if(this.timeSinceFire < 15)
            {
                this.targetAngleRadians = 0f;
                if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSpaceboosterOpen) || this.playedRotateSfx == true)
                {
                    Audio.instance.StopSfx(Assets.instance.sfxEnvSpaceboosterOpen);
                    this.playedRotateSfx = false;
                }
            }
            else
            {
                if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSpaceboosterOpen) || this.playedRotateSfx == true)
                {
                    Audio.instance.StopSfx(Assets.instance.sfxEnvSpaceboosterOpen);
                    this.playedRotateSfx = false;
                }
            }
        }

        // move to target rotation
        this.visibleAngleRadians = Util.WrapAngleMinus180To180(
            visibleAngleRadians - this.targetAngleRadians
        ) + this.targetAngleRadians;

        this.visibleAngleRadians = Util.Slide(
            this.visibleAngleRadians, this.targetAngleRadians, MoveToTargetAngleSpeed
        );
    }

    private void ControlLight()
    {
        bool isPlayerInside = this.map.player.InsideCityCannon == this;

        if(this.map.frameNumber % 8 == 0)
        {
            this.lightOn = !this.lightOn;
        }

        this.lightGO.SetActive(lightOn && isPlayerInside);
    }

    public void AdvancePlayer()
    {
        var player = this.map.player;
        player.position = this.position;
        player.velocity = this.velocity;
    }

    public void FirePlayer()
    {
        this.map.player.ExitCityCannon();

        float fireAngleRad = this.visibleAngleRadians - (90f * Mathf.Deg2Rad);
        Vector2 fireDirection = new Vector2(
            Mathf.Cos(fireAngleRad),
            Mathf.Sin(fireAngleRad)
        );

        this.map.player.velocity = fireDirection * FireForce;
        this.map.player.state = Player.State.FireFromCityCannon;
        this.timeSinceFire = 25;
        this.topTransform.localScale = TopFireScale;
        this.bottomTransform.localPosition = BottomFirePosition;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvCannonCityLaunch,
            position: this.position
        );
    }
}
