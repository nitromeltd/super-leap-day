using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clam : Item
{
    public Transform grabTransform;
    public GameObject bottomGO;

    public Animated.Animation openAnimation;
    public Animated.Animation closeAnimation;
    public Animated.Animation idleAnimation;
    public Animated.Animation biteAnimation;
    public Animated.Animation bopAnimation;
    public Animated.Animation chewAnimation;
    public Animated.Animation confusedAnimation;
    
    public enum State
    {
        CloseOutsideWater,
        OpenInsideWater,
        AlertPlayer,
        BitePlayer
    }
    
    private State state;
    private bool insideWater;
    private int ignoreGrabTime = 0;
    private int alertShakeTimer = 0;
    private int biteTimer = 0;
    private Vector2 initialPosition;
    private Pickup pickup;
    
    private const int StartDetectingPlayerTime = 30;
    private const int AlertShakeTime = 60;
    private const int BiteTime = 90;
    private const float GrabSize = 0.40f;
    private const float ShakeSpeed = 1.50f;
    private const float ShakeAmount = 0.03f;

    private static Vector2 BottomHitboxPosition = new Vector2(0f, -0.75f);
    private static Vector2 BottomHitboxSize = new Vector2(1f, 0.25f);
    private static Vector2 FullHitboxPosition = new Vector2(0f, -0.50f);
    private static Vector2 FullHitboxSize = new Vector2(1f, 0.50f);

    private static Vector2 TriggerBitePosition = new Vector2(-0.50f, -0.50f);
    private static Vector2 TriggerBiteSize = new Vector2(1f, 1f);

    public bool HasPickup => this.pickup != null;
    
    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.grabInfo =
            new GrabInfo(this, GrabType.Floor, this.grabTransform.localPosition);

        this.initialPosition = this.position;
        this.onCollisionFromAbove = OnCollisionFromAbove;

        this.pickup = this.chunk.NearestItemTo<Pickup>(this.position, 1f);

        ChangeState(State.CloseOutsideWater);
    }
    
    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        
        AdvanceWater();

        switch (state)
        {
            case State.CloseOutsideWater:
                AdvanceCloseOutsideWater();
                break;

            case State.OpenInsideWater:
                AdvanceOpenInsideWater();
                break;

            case State.AlertPlayer:
                AdvanceAlertPlayer();
                break;

            case State.BitePlayer:
                AdvanceBitePlayer();
                break;
        }
        
        AdvanceHanging();

        if(HasPickup == true && this.pickup.collected == true)
        {
            this.pickup = null;
        }

        this.transform.position = this.position;
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreGrabTime = 30;
            }
            return;
        }
        if (this.ignoreGrabTime > 0)
        {
            this.ignoreGrabTime -= 1;
            return;
        }

        var delta = player.position - (Vector2)this.grabTransform.position;
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreGrabTime = 15;
        }
    }

    private void AdvanceCloseOutsideWater()
    {
        if(this.insideWater == true)
        {
            ChangeState(State.OpenInsideWater);
        }
    }

    private void AdvanceOpenInsideWater()
    {
        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        var rectTrigger = new Rect(
            this.position + TriggerBitePosition,
            TriggerBiteSize
        );
        
        if (player.ShouldCollideWithItems() == true &&
            rectTrigger.Overlaps(playerRectangle))
        {
            ChangeState(State.AlertPlayer);
        }
        else if(this.insideWater == false)
        {
            ChangeState(State.CloseOutsideWater);
        }
    }

    private void AdvanceAlertPlayer()
    {
        float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * ShakeAmount;
        this.position = this.initialPosition + Vector2.one * shake;

        if(this.alertShakeTimer > 0)
        {
            this.alertShakeTimer -= 1;

            if(this.alertShakeTimer <= 0)
            {
                this.alertShakeTimer = 0;
                ChangeState(State.BitePlayer);
            }
        }
    }

    private void AdvanceBitePlayer()
    {
        void BitePlayer()
        {
            var player = this.map.player;
            var playerRectangle = player.Rectangle();
            
            var rectTrigger = new Rect(
                this.position + TriggerBitePosition,
                TriggerBiteSize
            );

            if (player.ShouldCollideWithItems() == true &&
                rectTrigger.Overlaps(playerRectangle))
            {
                player.Hit(this.position);
            }
        }

        if(this.biteTimer > 0)
        {
            this.biteTimer -= 1;

            if(this.biteTimer == 0)
            {
                if(this.insideWater == true)
                {
                    ChangeState(State.OpenInsideWater);
                }
                else
                {
                    ChangeState(State.CloseOutsideWater);
                }
            }
            else if(biteTimer > BiteTime - 5)
            {
                BitePlayer();
            }
        }
    }
    
    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (state)
        {
            case State.CloseOutsideWater:
                EnterCloseOutsideWater();
                break;

            case State.OpenInsideWater:
                EnterOpenInsideWater();
                break;

            case State.AlertPlayer:
                EnterAlertPlayer();
                break;

            case State.BitePlayer:
                EnterBitePlayer();
                break;
        }
    }

    private void EnterCloseOutsideWater()
    {
        Close();

        this.animated.PlayOnce(this.closeAnimation, () => 
        {
            this.animated.PlayOnce(this.idleAnimation);
        });

        this.animated.OnFrame(5, () =>
        {
            this.grabTransform.gameObject.SetActive(false);
        });
    }

    private void EnterOpenInsideWater()
    {
        Open();

        this.animated.PlayAndHoldLastFrame(this.openAnimation);
        this.animated.OnFrame(4, () =>
        {
            this.grabTransform.gameObject.SetActive(true);
        });
    }

    private void EnterAlertPlayer()
    {
        this.alertShakeTimer = AlertShakeTime;
    }

    private void EnterBitePlayer()
    {
        Close();

        this.biteTimer = BiteTime;
        this.animated.PlayOnce(this.biteAnimation, () =>
        {
            if(this.insideWater == true)
            {
                ChangeState(State.OpenInsideWater);
            }
            else
            {
                ChangeState(State.CloseOutsideWater);
            }
        });

        this.animated.OnFrame(4, () =>
        {
            this.grabTransform.gameObject.SetActive(false);
        });

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvClamBite,
            position: this.position
        );
    }

    private void Open()
    {
        Hitbox bottomHitbox = new Hitbox(
            BottomHitboxPosition.x - BottomHitboxSize.x,
            BottomHitboxPosition.x + BottomHitboxSize.x,
            BottomHitboxPosition.y - BottomHitboxSize.y,
            BottomHitboxPosition.y + BottomHitboxSize.y,
            this.rotation,
            Hitbox.Solid
        );

        this.hitboxes = new [] { bottomHitbox };
        //this.bottomGO.SetActive(true);

        if(HasPickup == true)
        {
            this.pickup.gameObject.SetActive(true);
            this.pickup.canPlayerCollect = true;
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvClamOpen,
            position: this.position
        );
    }

    private void Close()
    {
        Hitbox fullHitbox = new Hitbox(
            FullHitboxPosition.x - FullHitboxSize.x,
            FullHitboxPosition.x + FullHitboxSize.x,
            FullHitboxPosition.y - FullHitboxSize.y,
            FullHitboxPosition.y + FullHitboxSize.y,
            this.rotation,
            Hitbox.Solid
        );

        this.hitboxes = new [] { fullHitbox };
        //this.bottomGO.SetActive(false);
        
        if(HasPickup == true)
        {
            this.pickup.gameObject.SetActive(false);
            this.pickup.canPlayerCollect = false;
        }
    }

    private void OnCollisionFromAbove(Collision collision)
    {
        if(this.state == State.CloseOutsideWater)
        {
            this.map.player.BounceOffHitItem(this.transform.position, true);
            this.animated.PlayOnce(this.confusedAnimation);
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvClamJump,
                position: this.position
            );
        }
    }

    public override void Reset()
    {
        this.pickup = this.chunk.NearestItemTo<Pickup>(this.position, 1f);

        ChangeState(State.CloseOutsideWater);
    }
}
