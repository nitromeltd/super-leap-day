using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using Rnd = UnityEngine.Random;

public class CloudBlock : Item
{
    public Sprite[] topSprites;
    public Sprite[] bottomSprites;
    public Sprite[] outlineSprites;
    public Sprite[] silhouetteSprites;

    public SpriteRenderer top;
    public SpriteRenderer bottom;
    public SpriteRenderer outline;
    public SpriteRenderer silhouette;
    public SpriteRenderer square;
    public SpriteRenderer squareSilhouette;

    private Transform topTransform;
    private Transform bottomTransform;
    private Transform outlineTransform;
    private Transform silhouetteTransform;

    public CloudBlock topBlock;
    public CloudBlock bottomBlock;
    public CloudBlock leftBlock;
    public CloudBlock rightBlock;

    public int topIgnoreTime;
    public int bottomIgnoreTime;
    public int leftIgnoreTime;
    public int rightIgnoreTime;

    [HideInInspector]
    public bool collidesWithPlayer;
    [HideInInspector]
    public CloudBlock leader;
    public static bool isPlayerInside;
    public static bool wasPlayerInside;

    private float timeOffset;
    private int selectedSpriteIndex;
    public bool insideBlock = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };

        FindNeighbours(def.tx, def.ty);
        if(topBlock != null && bottomBlock != null && leftBlock != null && rightBlock != null)
            this.insideBlock = true;

        this.timeOffset = Rnd.Range(0f, 100f);
        this.selectedSpriteIndex = Rnd.Range(0, this.outlineSprites.Length);
        SetCorrectSprites();

        this.topTransform = this.top.transform;
        this.bottomTransform = this.bottom.transform;
        this.outlineTransform = this.outline.transform;
        this.silhouetteTransform = this.silhouette.transform;

        if(this.leader == null)
        {
            this.leader = this;
            var cloudBlocks = this.chunk.subsetOfItemsOfTypeCloudBlock;

            foreach(var cb in cloudBlocks)
            {
                if(cb.leader == null)
                {
                    cb.leader = this;
                }
            }
        }
    }

    private void FindNeighbours(int x, int y)
    {
        this.topBlock = this.chunk.ItemAt<CloudBlock>(x, y + 1);
        this.bottomBlock = this.chunk.ItemAt<CloudBlock>(x, y - 1);
        this.leftBlock = this.chunk.ItemAt<CloudBlock>(x - 1, y);
        this.rightBlock = this.chunk.ItemAt<CloudBlock>(x + 1, y);
    }

    public bool IsOverlappingTop(Rect rectToCheck)
    {
        return rectToCheck.Overlaps(new Rect(this.position.x - 0.5f, this.position.y + 0.49f, 1f, 0.01f));
    }

    public bool IsOverlappingBottom(Rect rectToCheck)
    {
        return rectToCheck.Overlaps(new Rect(this.position.x - 0.5f, this.position.y - 0.5f, 1f, 0.01f));
    }

    public bool IsOverlappingLeft(Rect rectToCheck)
    {
        return rectToCheck.Overlaps(new Rect(this.position.x - 0.5f, this.position.y - 0.5f, 0.01f, 1f));
    }

    public bool IsOverlappingRight(Rect playerRect)
    {
        return playerRect.Overlaps(new Rect(this.position.x + 0.49f, this.position.y - 0.5f, 0.01f, 1f));
    }

    public void SetCorrectSprites()
    {
        this.top.sprite = this.topSprites[this.selectedSpriteIndex];
        this.bottom.sprite = this.bottomSprites[this.selectedSpriteIndex];
        this.outline.sprite = this.outlineSprites[this.selectedSpriteIndex];
        this.silhouette.sprite = this.silhouetteSprites[this.selectedSpriteIndex];

        if(this.insideBlock == true)
        {
            this.top.gameObject.SetActive(false);
            this.bottom.gameObject.SetActive(false);
            this.outline.gameObject.SetActive(false);
            this.silhouette.gameObject.SetActive(false);
            this.square.gameObject.SetActive(true);
            this.squareSilhouette.gameObject.SetActive(true);
        }
    }

    public override void Advance()
    {
        var yDiff = this.map.player.position.y - this.position.y;

        if (this.insideBlock == false)
        {
            if (this.map.gameCamera.VisibleRectangle().Inflate(20).Contains(this.position) == true)
            {
                CheckEdges();
                AdvanceStretching();
            }
        }
        
        if(yDiff > -5 && yDiff < 5)
            this.collidesWithPlayer = GetPlayerFootRectangle().Overlaps(this.hitboxes[0].InWorldSpace());
        else
            this.collidesWithPlayer = false;

        if( this.collidesWithPlayer
            && this.map.player.state != Player.State.Normal /*&& this.map.player.state != Player.State.WallSlide*/
            && this.map.player.state != Player.State.InsideAirCurrent
            && this.map.player.state != Player.State.Respawning
            && this.map.player.velocity.y <= 0
            )
        {
            this.map.player.state = Player.State.Normal;
            this.map.player.groundState = new Player.GroundState(true, Player.Orientation.Normal, 0, false, false);
            this.map.player.groundspeed = 0;
            this.map.player.ResetDoubleJump();
        }

        if(this.leader == this)
        {
            bool isPlayerInside = IsPlayerInsideClouds(this.map.player);

            if (isPlayerInside && !Audio.instance.IsSfxPlaying(Assets.instance.sfxWindyCloudInside))
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxWindyCloudInside,
                    position: this.position,
                    options: new Audio.Options(1, false, 0)
                );
            }

            if (isPlayerInside == false && wasPlayerInside == true)
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxWindyCloudPopOut,
                    position: this.position,
                    options: new Audio.Options(1, false, 0)
                );
                Audio.instance.StopSfx(Assets.instance.sfxWindyCloudInside);
            }
            else if(isPlayerInside == true && wasPlayerInside == false)
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxWindyCloudLand,
                    position: this.position,
                    options: new Audio.Options(1, false, 0)
                );
            }
            wasPlayerInside = isPlayerInside;
        }
    }
    private const int EdgeIgnoreTime = 10;

    public void SpawnParticlesOnTopOfTheBlock(int howManyParticles, float speedMult)
    {
        this.topIgnoreTime = EdgeIgnoreTime;
        for (int n = 0; n < howManyParticles; n++)
        {
            SpawnCloudParticle(this.position + new Vector2(Rnd.Range(-0.4f, 0.4f), 0.5f), new Vector2(Rnd.Range(-0.005f, 0.005f), speedMult * Rnd.Range(0.01f, 0.017f)));
        }
    }

    public void SpawnParticlesOnBottomOfTheBlock(int howManyParticles, float speedMult)
    {
        this.bottomIgnoreTime = EdgeIgnoreTime;
        for (int n = 0; n < howManyParticles; n++)
        {
            SpawnCloudParticle(this.position + new Vector2(Rnd.Range(-0.4f, 0.4f), -0.5f), new Vector2(Rnd.Range(-0.005f, 0.005f), speedMult * Rnd.Range(-0.017f, -0.01f)));
        }
    }

    public void SpawnParticlesOnLeftOfTheBlock(int howManyParticles, float speedMult)
    {
        this.leftIgnoreTime = EdgeIgnoreTime;
        for (int n = 0; n < howManyParticles; n++)
        {
            SpawnCloudParticle(this.position + new Vector2(-0.5f, Rnd.Range(-0.4f, 0.4f)), new Vector2(speedMult * Rnd.Range(-0.017f, -0.01f), Rnd.Range(-0.005f, 0.005f)));
        }
    }

    public void SpawnParticlesOnRightOfTheBlock(int howManyParticles, float speedMult)
    {
        this.rightIgnoreTime = EdgeIgnoreTime;
        for (int n = 0; n < howManyParticles; n++)
        {
            SpawnCloudParticle(this.position + new Vector2(0.5f, Rnd.Range(-0.4f, 0.4f)), new Vector2(speedMult * Rnd.Range(0.01f, 0.017f), Rnd.Range(-0.005f, 0.005f)));
        }
    }

    private void CheckEdges()
    {
        if(this.topIgnoreTime > 0)      this.topIgnoreTime--;
        if(this.bottomIgnoreTime > 0)   this.bottomIgnoreTime--;
        if(this.leftIgnoreTime > 0)     this.leftIgnoreTime--;
        if(this.rightIgnoreTime > 0)    this.rightIgnoreTime--;

        var playerRect = this.map.player.Rectangle();

        var birds = this.chunk.subsetOfItemsOfTypeCloudBird;

        if(this.topIgnoreTime == 0 && this.topBlock == null)
        {
            if(IsOverlappingTop(playerRect) == true)
            {
                SpawnParticlesOnTopOfTheBlock(1, 1);
            }
        }

        if(this.bottomIgnoreTime == 0 && this.bottomBlock == null)
        {
            if(IsOverlappingBottom(playerRect) == true)
            {
                SpawnParticlesOnBottomOfTheBlock(1, 1);
            }
        }

        if(this.leftIgnoreTime == 0  && this.leftBlock == null)
        {
            if(IsOverlappingLeft(playerRect) == true)
            {
                SpawnParticlesOnLeftOfTheBlock(1, 1);
            }
        }

        if(this.rightIgnoreTime == 0 && this.rightBlock == null)
        {
            if(IsOverlappingRight(playerRect) == true)
            {
                SpawnParticlesOnRightOfTheBlock(1, 1);
            }
        }
    }

    private void SpawnCloudParticle(Vector2 pos, Vector2 velocity)
    {
         
        int r = Rnd.Range(0, Assets.instance.windySkies.cloudParticlesOutline.Length);
        Particle p = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesOutline[r], 60, pos, this.map.transform);
        Particle p1 = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesBack[r], 60, pos, this.map.transform);
        Particle p2 = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesFront[r], 60, pos, this.map.transform);

        p1.velocity = p2.velocity = p.velocity = velocity;
        p1.acceleration = p2.acceleration = p.acceleration = Vector2.zero;

        p.spriteRenderer.sortingLayerName = p1.spriteRenderer.sortingLayerName = p2.spriteRenderer.sortingLayerName = "Clouds";
        p.spriteRenderer.sortingOrder = 0;
        p1.spriteRenderer.sortingOrder = 1;
        p2.spriteRenderer.sortingOrder = 2;

        p.ScaleOut(Assets.instance.windySkies.cloudParticleScaleOutCurve);
        p1.ScaleOut(Assets.instance.windySkies.cloudParticleScaleOutCurve);
        p2.ScaleOut(Assets.instance.windySkies.cloudParticleScaleOutCurve);
    }

    private void AdvanceStretching()
    {
        Vector3 scale = Vector3.zero;
        scale.x = 1.1f + 0.1f * Mathf.Sin(Time.timeSinceLevelLoad + this.timeOffset);
        scale.y = 1.1f + 0.1f * Mathf.Cos(Time.timeSinceLevelLoad + this.timeOffset);

        this.topTransform.localScale = scale;
        this.bottomTransform.localScale = scale;
        this.outlineTransform.localScale = scale;
        this.silhouetteTransform.localScale = scale;
    }

    private Rect GetPlayerFootRectangle()
    {
        if (this.map.player.alive == false)
            return Rect.zero;
        else
            return new Rect(
                this.map.player.position.x - 10f / 16,
                this.map.player.position.y - 13f / 16,
                2*10f / 16f,
                0.5f
            );
    }

    public static bool IsPlayerInsideClouds(Player player)
    {
        var r = player.Rectangle();

        var txMin = Mathf.FloorToInt(r.xMin);
        var txMax = Mathf.FloorToInt(r.xMax);
        var tyMin = Mathf.FloorToInt(r.yMin);
        var tyMax = Mathf.FloorToInt(r.yMax);
        var chunk = player.map.NearestChunkTo(player.position);

        for (int tx = txMin; tx <= txMax; tx += 1)
        {
            if (tx < chunk.xMin || tx >= chunk.xMax) continue;

            for (int ty = tyMin; ty <= tyMax; ty += 1)
            {
                var cloudBlock = chunk.CloudBlockAt(tx, ty);
                if (cloudBlock != null && cloudBlock.collidesWithPlayer == true)
                    return true;
            }
        }
        return false;
    }
}