
using UnityEngine;

public class CloudMovingPlatform : Item
{
    private NitromeEditor.Path path;

    public override void Init(Def def)
    {
        base.Init(def);

        this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty);

        this.hitboxes = new [] {
            new Hitbox(0, 4, 1, 2, this.rotation, Hitbox.SolidOnTop)
        };

        this.path = this.chunk.pathLayer.NearestNodeTo(
            new Vector2(
                this.transform.position.x,
                this.transform.position.y
            )
        ).path;
        if (this.path.nodes.Length < 2)
            this.path = null;
    }

    override public void Advance()
    {
        if (this.path != null)
        {
            var s = Mathf.Sin(this.map.frameNumber * 0.01f) * 0.5f + 0.5f;
            var p = Vector2.Lerp(
                this.path.nodes[0].Position,
                this.path.nodes[1].Position,
                s
            );
            this.position = p;
            this.transform.position = this.position;
        }
    }
}
