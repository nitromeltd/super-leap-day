
using UnityEngine;
using System;
using System.Collections;

public class Coin : Pickup
{
    public bool isSilver;

    private int collectSparkleTime;
    private float sparkleOffset;
    private float velocityY;

    // Timer coin
    public Animated.Animation timerNormalAnimation;
    public Animated.Animation timerCollectAnimation;
    private bool isTimerCoin = false;
    private bool isStuckInWall = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.sparkleOffset = this.isSilver ? 0.75f : 1.25f;
        this.hitboxSize = this.isSilver ? 0.40f : 0.80f;

        this.hitboxes = new[] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };

        if (this.isTimerCoin == true)
        {
            this.isStuckInWall = true;
            this.animated.PlayAndLoop(timerNormalAnimation);
        }

        if(this.map.player.midasTouch.isActive == true && this.isSilver == true)
        {
            this.GetComponent<SpriteRenderer>().material = Assets.instance.spriteGoldTint2;
        }
    }

    public static Coin Create(Vector2 position, Chunk chunk, bool? isTimerCoin = false)
    {
        var obj = Instantiate(Assets.instance.coin, chunk.transform);
        obj.name = "Spawned Coin";
        var item = obj.GetComponent<Coin>();
        if (isTimerCoin == true)
        {
            item.isTimerCoin = isTimerCoin.Value;
        }
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public static Coin CreateSilver(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.silverCoin, chunk.transform);
        obj.name = "Spawned Coin";
        var item = obj.GetComponent<Coin>();
        item.Init(position, chunk);
        chunk.items.Add(item);

        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;

        if (this.isTimerCoin)
        {
            this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        }
        else
        {
            this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        }

        this.gravityAcceleration = 0.015f;
        this.gravityAccelerationUnderwater = 0.005f;
        this.bounciness = this.isSilver ? -0.25f : -0.5f;
    }

    override public void Advance()
    {
        base.Advance();

        // sparkle particles
        if (this.collected == false)
        {
            this.velocityY = this.velocity.y;

            CoinWallPass();

            ApplyDragOnFloor();

            bool createSparkleParticle = this.isSilver ?
                this.map.frameNumber % 6 == 0 :
                this.map.frameNumber % 4 == 0;

            if(createSparkleParticle == true)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset),
                        UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset)
                    ),
                    this.transform.parent
                );
                sparkle.spriteRenderer.sortingLayerName = "Items";
                sparkle.spriteRenderer.sortingOrder = -1;
            }
            else if (this.collectSparkleTime > 0)
            {
                this.collectSparkleTime -= 1;

                if (UnityEngine.Random.Range(0, 10) > 2)
                {
                    var sparkle = Particle.CreateAndPlayOnce(
                        Assets.instance.coinSparkleAnimation,
                        this.map.player.position.Add(
                            UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset),
                            UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset)
                        ),
                        this.map.player.transform.parent
                    );
                    sparkle.spriteRenderer.sortingLayerName = "Items";
                    sparkle.spriteRenderer.sortingOrder = -1;
                }
            }
        }
        
        if (this.collected == true && this.collectSparkleTime > 0)
        {
            this.velocityY = Util.Slide(this.velocityY, 0.075f, 0.008f);
            this.position += this.velocityY * this.gravity.Up();

            this.transform.position = new Vector3(
                this.position.x, this.position.y, this.z
            );
        }
    }
    
    protected void MoveHorizontally(Raycast raycast)
    {
        var rect    = this.hitboxes[0];
        var high    = this.position.Add(0, rect.yMax - 1);
        var mid     = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
        var low     = this.position.Add(0, rect.yMin + 1);

        if (this.velocity.x > 0)
        {
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, true, maxDistance),
                raycast.Horizontal(mid,  true, maxDistance),
                raycast.Horizontal(low,  true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = -Mathf.Abs(this.velocity.x);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(high, false, maxDistance),
                raycast.Horizontal(mid,  false, maxDistance),
                raycast.Horizontal(low,  false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {
                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = Mathf.Abs(this.velocity.x);
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
    }
    
    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.375f, 0f), false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                float conveyorSpeed = 0;
                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                        conveyorSpeed = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed; //0.5
                }
                
                this.position.x += conveyorSpeed;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void CoinWallPass()
    {
        if (this.isTimerCoin == true && this.isStuckInWall == true)
        {
            Raycast raycast = new Raycast(this.map, this.position);
            //Raycast.Result result = raycast.Arbitrary(this.position, (this.map.player.position - this.position).normalized, (this.map.player.position - this.position).magnitude);
            Raycast.Result result1 = raycast.Vertical(this.position, true, 1);
            Raycast.Result result2 = raycast.Vertical(this.position, false, 1);
            if (result1.tile != null && result2.tile != null)
            {
                velocity.y = -0.1f;
                this.position.y += this.velocity.y;
            }
            else
            {
                this.isStuckInWall = false;
            }
        }
    }

    public override void Collect()
    {
        if (this.collected == true || this.canPlayerCollect == false) return;
        
        base.Collect();

        if (this.isSilver == true)
        {
            this.map.AddSilverCoin();
        }
        else
        {
            this.map.coinsCollected += 1;
        }

        this.collectSparkleTime = 120;
        Audio.instance.PlaySfx(Assets.instance.sfxCoin, position: this.position, options: new Audio.Options(0.9f, true, 0.1f));

        if (this.isTimerCoin == true)
        {
            this.animated.PlayOnce(this.timerCollectAnimation, delegate
            {
                this.spriteRenderer.enabled = false;
            });
        }

        this.velocityY = 0;

        if (this.map.player.midasTouch.isActive == true && this.isSilver == false && this.isTimerCoin == false && UnityEngine.Random.value < .4f)
        {
            this.animated.OnFrame(8, () =>
            {
                float hVelocity = UnityEngine.Random.Range(0.05f, 0.15f);
                float vVelocity = UnityEngine.Random.Range(0.25f, 0.35f);

                for(int i = -1; i <= 1; i++)
                {
                    Coin coin = Coin.CreateSilver(this.position - Vector2.right * .5f, this.chunk);
                    coin.spriteRenderer.material = Assets.instance.spriteGoldTint2;
                    coin.movingFreely = true;
                    coin.velocity = new Vector2(hVelocity * i, vVelocity);
                    StartCoroutine(coin.CannotCollect());
                }
            });
        }
    }

    public IEnumerator CannotCollect()
    {
        this.canPlayerCollect = false;

        yield return new WaitForSeconds(.5f);

        this.canPlayerCollect = true;
    }
}
