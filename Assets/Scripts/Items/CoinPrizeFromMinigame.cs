﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinPrizeFromMinigame : Pickup
{
    public Animated coinAnimated;

    [Header("Icon")]
    public SpriteRenderer iconSprite;
    public Sprite coinIcon;

    [Header("Shine")]
    public Animated.Animation animationSpin;
    public Animated shineAnimated;
    public Animated.Animation animationShineBlank;
    public Animated.Animation animationShine;
    public Animated.Animation animationCoin;

    private PowerupPickup.Type powerupType;
    
    // shine animation
    private int spinShineTimer = 0;
    private const int SpinShineTime = 60 * 3;

    // collect animation
    private int collectAnimTime;

    // movement
    private Vector2 initialPos;
    private Vector2 targetPos;
    private float breatheTimer;
    private float timerBlender = 0;
    private const float BREATHE_PERIOD = .5f;
    private const float BREATHE_AMPLITUDE = .3f;
    private bool breatheMove = false;
    [HideInInspector] public bool isInBlender = false;

    private const float HorizontalMomentum = 0.5f;

    private SpriteRenderer[] allSpriteRenderers;

    public AnimationCurve speedCurve;
    private int howManyCoins;

    private void Awake()
    {
        allSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
    }    

    public static CoinPrizeFromMinigame Create(Vector2 position, Chunk chunk, int numberOfCoins)
    {
        var obj = Instantiate(Assets.instance.coinPickup, chunk.transform);
        obj.name = "Spawned Coin Prize From Minigame";
        var item = obj.GetComponent<CoinPrizeFromMinigame>();
        item.Init(position, chunk, numberOfCoins);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, int numberOfCoins)
    {
        base.Init(new Def(chunk));

        this.howManyCoins = numberOfCoins;
        this.transform.position = this.position = position;
        this.chunk = chunk;
        this.iconSprite.sprite = null; 
        this.coinAnimated.PlayAndLoop(this.animationCoin);         

        this.bounciness = -0.5f;
        
        this.spinShineTimer = SpinShineTime;
        this.initialPos = this.targetPos = this.position + Vector2.up * 1f;

        this.animated.PlayAndHoldLastFrame(animationNormal);

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        
        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, -0.75f, 0.75f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        base.Advance();
        
        if(this.collected == false &&
            this.suckActive == false)
        {
            BreatheMovement();
            Shine();
            // ApplyDragOnFloor();
            
            if (this.map.frameNumber % 6 == 0)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)
                    ),
                    this.transform.parent
                );
            }
        }

        this.collectAnimTime = (this.collectAnimTime > 0) ? collectAnimTime - 1 : 0;

        if (this.collected == true && this.collectAnimTime > 0)
        {
            this.velocity.y = Util.Slide(this.velocity.y, 0.075f, 0.008f);
        }

        if(this.movingFreely == false &&
            this.collected == false &&
            this.suckActive == false)
        {
            this.position += this.velocity;
            this.transform.position = this.position;
        }
    }

    private void BreatheMovement()
    {
        if(this.movingFreely == true) return;

        if(this.breatheMove == true)
        {
            this.breatheTimer += Time.deltaTime;
        }

        float theta = this.breatheTimer / BREATHE_PERIOD;
        float distance = BREATHE_AMPLITUDE * Mathf.Sin(theta);

        if(this.breatheMove == true)
        {
            this.targetPos = this.initialPos + Vector2.up * distance;
        }

        Vector2 targetVel;
        if (this.isInBlender)
        {
            targetVel = (targetPos - this.position) * .25f;
            this.timerBlender += Time.deltaTime;
        }
        else
        {
            targetVel = (targetPos - this.position) * .05f;
        }

        this.velocity *= 0.95f;
        this.velocity += targetVel * .2f;

        if(this.timerBlender < 0.3 && this.isInBlender)
        {
            this.transform.localScale = Vector3.one * this.timerBlender / 0.3f;
        }
        else
        {
            this.transform.localScale = Vector3.one;
        }
    }

    private void Shine()
    {
        if(this.spinShineTimer > 0)
        {
            this.spinShineTimer--;
        }
        else
        {
            this.spinShineTimer = SpinShineTime;
            this.animated.PlayOnce(this.animationSpin, () => 
            {
                if(this.collected == false)
                {
                    this.animated.PlayAndHoldLastFrame(this.animationNormal);
                    this.shineAnimated.PlayOnce(this.animationShine);
                }
            });
        }

        if(this.animated.currentAnimation == this.animationSpin)
        {
            float yRotValue = Mathf.Lerp(0, 360f,
                Mathf.InverseLerp(0, this.animationSpin.sprites.Length, this.animated.frameNumber));
            this.iconSprite.transform.localRotation = Quaternion.Euler(Vector3.up * yRotValue); 
            
            this.iconSprite.gameObject.SetActive(this.animated.frameNumber < 7 || this.animated.frameNumber > 17);
        } 
    }

    public override void Collect()
    {
        if(this.collected == true) return;

        this.collected = true;

        this.collectAnimTime = 120;
        this.velocity.y = .02f;

        this.shineAnimated.PlayAndHoldLastFrame(animationShineBlank);
        this.iconSprite.transform.localRotation = Quaternion.identity;
        this.iconSprite.gameObject.SetActive(true);

        Audio.instance.PlaySfx(Assets.instance.sfxPowerupCollect, position: this.position);

        this.animated.PlayOnce(this.animationCollect, delegate
        {
            this.spriteRenderer.enabled = false;
        });

        StartCoroutine(PowerUpMove());
    }

    private IEnumerator PowerUpMove()
    {
        Transform powerup = this.iconSprite.transform;

        float timer = 0f;
        while (timer < .5)
        {
            timer += Time.deltaTime;

            CreateSparkleParticles();

            float x = Mathf.Lerp(0.8f, 1.1f, timer/0.5f);
            powerup.localScale = new Vector3(x, x, 1);
            yield return null;
        }

        powerup.localScale = new Vector3(1, 1, 1);
        Vector3 initialPosition = powerup.position;
        Vector3 targetPosition = powerup.position + new Vector3(0,1,0);

        timer = 0f;

        while (timer < .5)
        {
            timer += Time.deltaTime;

            CreateSparkleParticles();

            powerup.position = Vector2.Lerp(initialPosition, targetPosition, timer / 0.5f);
            yield return null;
        }

        float minMoveSpeed = 5f * Time.deltaTime;
        float maxMoveSpeed = 30f * Time.deltaTime;
        float reachMaxSpeedTime = 0.50f;

        timer = 0f;

        bool moving = true;
        while(moving == true)
        {
            timer += Time.deltaTime;

            CreateSparkleParticles();

            moving = false;

            Transform targetTransform = IngameUI.instance.CoinImageRectTransform(this.map);
            float t = Mathf.InverseLerp(0f, reachMaxSpeedTime, timer);
            float currentMoveSpeed =
                Mathf.Lerp(minMoveSpeed, maxMoveSpeed, this.speedCurve.Evaluate(t));

            Vector2 currentPosition = powerup.position;

            Vector2 targetWorldPosition = IngameUI.instance.PositionForWorldFromUICameraPosition(
                this.map, targetTransform.position
            );
            Vector3 heading = targetWorldPosition - currentPosition;
            float distance = heading.magnitude;
            Vector3 direction = heading / distance;

            if (distance > currentMoveSpeed)
            {
                powerup.position += direction * currentMoveSpeed;
                moving = true;
            }

            yield return new WaitForEndOfFrame();
        }

        this.map.CollectPowerup(this.powerupType);
        this.map.ScreenShakeAtPosition(
            IngameUI.instance.CoinImageRectTransform(this.map).position, Vector2.one * 0.7f
        );

        StartCoroutine(IngameUI.instance.TintFlashCoinSymbol(this.map));
        StartCoroutine(IngameUI.instance.ShakeCoinSymbol(this.map));

        this.map.coinsCollected += this.howManyCoins;

        this.iconSprite.enabled = false;
    }

    private void CreateSparkleParticles()
    {
        float sparkleOffset = 0.5f;

        bool createSparkleParticle = this.map.frameNumber % 2 == 0;
        Vector2 copy = this.iconSprite.transform.position;
        if (createSparkleParticle == true)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                copy.Add(
                    UnityEngine.Random.Range(-sparkleOffset, sparkleOffset),
                    UnityEngine.Random.Range(-sparkleOffset, sparkleOffset)
                ),
                this.transform.parent
            );
            sparkle.spriteRenderer.sortingLayerName = "Items (Front)";
            sparkle.spriteRenderer.sortingOrder = 2;
        }
    }
}
