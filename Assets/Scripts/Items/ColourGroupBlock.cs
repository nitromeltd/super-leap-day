
using UnityEngine;
using System.Linq;

public class ColourGroupBlock : Item
{
    private int type;
    private int time;

    public Animated.Animation normalAnimation;
    public Animated.Animation nonsolidAnimation;

    public override void Init(Def def)
    {
        base.Init(def);

        this.type = def.tile.tile.Last() - '0';

        this.hitboxes = new [] {
            new Hitbox(0, 1, 0, 1, this.rotation, Hitbox.Solid, true, true)
        };

        this.onCollisionFromBelow = OnCollisionFromBelow;
    }

    public void OnCollisionFromBelow(Collision collision)
    {
        foreach (var block in this.chunk.items.OfType<ColourGroupBlock>())
        {
            if (block.type != this.type) continue;

            block.animated.PlayOnce(block.nonsolidAnimation);
            block.time = 60 * 8;
            block.hitboxes = new Hitbox[0];
        }
    }

    public override void Advance()
    {
        if (this.time > 0)
        {
            this.time -= 1;
            if (this.time < 1)
            {
                this.animated.PlayOnce(this.normalAnimation);
                this.hitboxes = new [] {
                    new Hitbox(0, 1, 0, 1, this.rotation, Hitbox.Solid, true, true)
                };
            }
        }
    }
}
