using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComingSoonBillboard : Item
{
    public Animated BillBoardMan;
    public Animated.Animation animationBillBoardMan;
    public SpriteRenderer BG;

    public List<Sprite> billboardSprites;
    public List<GameObject> UpdateText;
    public GameObject ComingSoonText;

    public override void Init(Def def)
    {
        base.Init(def);

        this.BillBoardMan.PlayAndLoop(this.animationBillBoardMan);

        this.ComingSoonText.GetComponent<Renderer>().sortingLayerName = "Tiles - Back 1";
        this.ComingSoonText.GetComponent<Renderer>().sortingOrder = 105;

        int billboardNumber = Random.Range(0, billboardSprites.Count);
        this.BG.sprite = billboardSprites[billboardNumber];
        this.UpdateText[billboardNumber].SetActive(true);
        this.UpdateText[billboardNumber].GetComponent<Renderer>().sortingLayerName = "Tiles - Back 1";
        this.UpdateText[billboardNumber].GetComponent<Renderer>().sortingOrder = 9;

        this.hitboxes = new[] {
            new Hitbox(
                -7f, 7f, 9, 9.35f,
                this.rotation, Hitbox.SolidOnTop, true, true)
        };
    }
}

