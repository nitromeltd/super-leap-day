
using UnityEngine;

public class ContactTriggerPlatform : Item
{
    private NitromeEditor.Path path;
    private int nodeIndex;
    private float distanceAlong;
    private bool movingForward;
    private bool movingBackward;
    private float speed;

    public override void Init(Def def)
    {
        base.Init(def);

        this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty);

        this.hitboxes = new [] {
            new Hitbox(0, 3, 0, 3, this.rotation, Hitbox.Solid, true, true)
        };

        this.path = this.chunk.pathLayer.NearestNodeTo(
            new Vector2(
                this.transform.position.x,
                this.transform.position.y
            )
        ).path;
        if (this.path.nodes.Length < 2)
            this.path = null;

        this.nodeIndex = 0;
        this.distanceAlong = 0;
        this.movingForward = false;
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromLeft  = OnCollision;
        this.onCollisionFromRight = OnCollision;
    }

    public override void Advance()
    {
        if (this.movingForward == true)
        {
            float target = 0;
            for (var n = 0; n < this.nodeIndex; n += 1)
                target += this.path.edges[n].distance;

            this.speed += 0.1f / 16;
            this.distanceAlong += this.speed;
            if (this.distanceAlong > target)
            {
                this.distanceAlong = target;
                this.speed *= -0.4f;

                if (Mathf.Abs(this.speed) < 0.01f)
                {
                    this.movingForward = false;
                    this.movingBackward = true;
                    this.speed = 0;
                }
            }
        }
        else if (this.movingBackward == true)
        {
            this.speed = Util.Slide(this.speed, -1.5f / 16, 0.1f / 16);
            this.distanceAlong += this.speed;

            if (this.distanceAlong < 0)
            {
                this.distanceAlong = 0;
                this.speed *= -0.4f;

                if (Mathf.Abs(this.speed) < 0.1f)
                    this.movingBackward = false;
            }

            this.nodeIndex = this.path.PointAtDistanceAlongPath(this.distanceAlong).edgeIndex;
        }

        this.position = this.path.PointAtDistanceAlongPath(this.distanceAlong).position;
        this.transform.position = this.position;
    }

    private void OnCollision(Collision collision)
    {
        if (this.movingForward == false &&
            this.movingBackward == false &&
            this.nodeIndex <= this.path.edges.Length - 1)
        {
            this.nodeIndex += 1;
            this.movingForward = true;
        }
    }
}
