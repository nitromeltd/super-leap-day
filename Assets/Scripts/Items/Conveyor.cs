using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Conveyor : Item
{
    private float conveyorSpeed;
    public float ConveyorSpeed { get => this.leader.conveyorStopped ? 0 : this.leader.conveyorSpeed; set => conveyorSpeed = value; }
    public bool right;
    public bool rightAtStart;
    public bool autotileInitialized;
    public bool buttonSwitchedIndicator;
    public Conveyor leader;
    public Button button;
    public Autotile[] autotiles;
    public Neighbours myNeighbours;

    private static Vector2? playerContactDirection;
    private float effectiveness;
    private bool lastButtonTrigger;
    private bool conveyorStopped;
    public SpecialType specialType;
    public enum SpecialType
    {
        None,
        TopRight,
        TopLeft
    }
    
    public enum Type
    {
        Regular,
        Fast,
        Slow
    }

    public Type conveyorType;

    [System.Serializable]
    public struct Autotile
    {
        public Animated.Animation animation;
        public bool useCorners;
        public Neighbours neighbours;
        public SpecialType specialType;
    }

    [System.Serializable]
    public struct Neighbours
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool upLeft;
        public bool upRight;
        public bool downLeft;
        public bool downRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool upLeft, bool upRight, bool downLeft, bool downRight)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
            this.upLeft = upLeft;
            this.upRight = upRight;
            this.downLeft = downLeft;
            this.downRight = downRight;
        }

        public bool IsEqual(Neighbours n, bool useCorners = false)
        {
            bool nonCornersEqual =
                this.up == n.up &&
                this.down == n.down &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual =
                this.upLeft == n.upLeft &&
                this.upRight == n.upRight &&
                this.downLeft == n.downLeft &&
                this.downRight == n.downRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);
        this.right = (def.tile.tile.Contains("right"));
        conveyorStopped = false;
        if (def.tile.flip)
        {
            this.right = !this.right;
        }
        this.rightAtStart = this.right;

        if (def.tile.tile.Contains("fast"))
        {
            conveyorType = Type.Fast;
            this.ConveyorSpeed = 0.17f;
        }
        else if (def.tile.tile.Contains("slow"))
        {
            conveyorType = Type.Slow;
            this.ConveyorSpeed = 0.09166f;
        }
        else
        {
            conveyorType = Type.Regular;
            this.ConveyorSpeed = 0.15f;
        }

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };

        this.onCollisionFromAbove = (_) => OnCollisionFromAbove(Vector2.right);
        this.onCollisionFromBelow = (_) => OnCollision(Vector2.left);
        this.onCollisionFromLeft = (_) => OnCollision(Vector2.up);
        this.onCollisionFromRight = (_) => OnCollision(Vector2.down);
        this.collisionsFollowRotation = false;

        playerContactDirection = null;
        ApplyAutotiling(def.tx, def.ty);

        if (this.leader == null)
        {
            SetConveyorLeader(def.tx, def.ty, this);
        }

        var nearestConnectionNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 4);
        if (nearestConnectionNode != null && this.leader != null)
        {
            var otherNode =
                nearestConnectionNode.Previous() ??
                nearestConnectionNode.Next();
            this.leader.button = this.chunk.NearestItemTo<Button>(otherNode.Position);
        }

    }

    public override void Reset()
    {
        if (this.right != this.rightAtStart)
        {
            this.animated.PlayAndLoop(this.animated.currentAnimation.Reversed());
            SetCorrectFPS();
            this.right = this.rightAtStart;
        }
    }

    public void SetConveyorLeader(int x, int y, Conveyor leader)
    {
        if (this.leader != null)
        {
            return;
        }

        this.leader = leader;
        this.chunk.ItemAt<Conveyor>(x, y + 1)?.SetConveyorLeader(x, y + 1, leader);
        this.chunk.ItemAt<Conveyor>(x, y - 1)?.SetConveyorLeader(x, y - 1, leader);
        this.chunk.ItemAt<Conveyor>(x + 1, y)?.SetConveyorLeader(x + 1, y, leader);
        this.chunk.ItemAt<Conveyor>(x - 1, y)?.SetConveyorLeader(x - 1, y, leader);
    }

    public override void Advance()
    {
        base.Advance();

        if (playerContactDirection != null)
        {
            this.effectiveness = Util.Slide(this.effectiveness, 1.0f, 0.2f);
            this.map.player.position +=
                playerContactDirection.Value * this.effectiveness;
            playerContactDirection = null;
        }
        else
        {
            this.effectiveness = 0;
        }

        if (this.button != null && this == this.leader)
        {
            if (this.button.PressedThisFrame)
            {
                this.buttonSwitchedIndicator = !this.buttonSwitchedIndicator;
            }
        }

        bool buttonTrigger = leader.buttonSwitchedIndicator;
        if (buttonTrigger != this.lastButtonTrigger)
        {
            this.animated.PlayAndLoop(this.animated.currentAnimation.Reversed());
            SetCorrectFPS();
            this.right = !this.right;
        }

        if (this.leader.animated.playbackSpeed != this.animated.playbackSpeed)
        {
            this.animated.playbackSpeed = this.leader.animated.playbackSpeed;
        }

        this.lastButtonTrigger = buttonTrigger;

        if (this.leader != null)
        {
            //Debug.Log("Leader");
        }
    }

    public void StopConveyor()
    {
        this.leader.animated.playbackSpeed = 0;
        this.leader.conveyorStopped = true;
    }

    public void StartConveyor()
    {
        this.leader.animated.playbackSpeed = 1;
        this.leader.conveyorStopped = false;
    }

    private void SetCorrectFPS()
    {
        if (this.conveyorType == Type.Fast)
        {
            this.animated.currentAnimation.fps = this.animated.currentAnimation.fps * 2;
        }
        else if (this.conveyorType == Type.Slow)
        {
            this.animated.currentAnimation.fps = this.animated.currentAnimation.fps / 2;
        }
    }

    public void OnCollisionFromAbove(Vector2 vectorRight)
    {
        playerContactDirection = (this.right ? vectorRight : -vectorRight) * this.leader.ConveyorSpeed;

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvConveyorBeltNeutral) && !this.leader.conveyorStopped)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvConveyorBeltNeutral,
                position: this.position
            );
        }
        
    }

    public void OnCollision(Vector2 vectorRight)
    {
        if (this.specialType == SpecialType.TopRight && !this.right)
        {
            playerContactDirection = (this.right ? vectorRight : -vectorRight) * this.leader.ConveyorSpeed + new Vector2(-0.2f, 0.2f);
            this.map.player.facingRight = false;
        }
        else if (this.specialType == SpecialType.TopLeft && this.right)
        {
            playerContactDirection = (this.right ? vectorRight : -vectorRight) * this.leader.ConveyorSpeed + new Vector2(0.2f, 0.2f);
            this.map.player.facingRight = true;
        }
        else
        {
            playerContactDirection = (this.right ? vectorRight : -vectorRight) * this.leader.ConveyorSpeed;
        }

    }

    public void ApplyAutotiling(int x, int y)
    {
        if (this.chunk.TileAt(x, y + 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x, y + 1) == null
            || this.chunk.TileAt(x, y - 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x, y - 1) == null
            || this.chunk.TileAt(x - 1, y, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x - 1, y) == null
            || this.chunk.TileAt(x + 1, y, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x + 1, y) == null
        )
        {
            // there are regular tiles next to it

            bool tileUp = this.chunk.ItemAt<Conveyor>(x, y + 1) != null || !this.chunk.IsLocationValid(x, y + 1) || (this.chunk.TileAt(x, y + 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x, y + 1) == null);
            bool tileDown = this.chunk.ItemAt<Conveyor>(x, y - 1) != null || !this.chunk.IsLocationValid(x, y - 1) || (this.chunk.TileAt(x, y - 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x, y - 1) == null);
            bool tileLeft = this.chunk.ItemAt<Conveyor>(x - 1, y) != null || !this.chunk.IsLocationValid(x - 1, y) || (this.chunk.TileAt(x - 1, y, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x - 1, y) == null);
            bool tileRight = this.chunk.ItemAt<Conveyor>(x + 1, y) != null || !this.chunk.IsLocationValid(x + 1, y) || (this.chunk.TileAt(x + 1, y, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x + 1, y) == null);

            bool tileUpLeft = this.chunk.ItemAt<Conveyor>(x - 1, y + 1) != null || !this.chunk.IsLocationValid(x - 1, y + 1) || (this.chunk.TileAt(x - 1, y + 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x - 1, y + 1) == null);
            bool tileUpRight = this.chunk.ItemAt<Conveyor>(x + 1, y + 1) != null || !this.chunk.IsLocationValid(x + 1, y + 1) || (this.chunk.TileAt(x + 1, y + 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x + 1, y + 1) == null);
            bool tileDownLeft = this.chunk.ItemAt<Conveyor>(x - 1, y - 1) != null || !this.chunk.IsLocationValid(x - 1, y - 1) || (this.chunk.TileAt(x - 1, y - 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x - 1, y - 1) == null);
            bool tileDownRight = this.chunk.ItemAt<Conveyor>(x + 1, y - 1) != null || !this.chunk.IsLocationValid(x + 1, y - 1) || (this.chunk.TileAt(x + 1, y - 1, Layer.A) != null && this.chunk.ItemAt<Conveyor>(x + 1, y - 1) == null);

            this.myNeighbours = new Conveyor.Neighbours(
                tileUp, tileDown, tileLeft, tileRight, tileUpLeft, tileUpRight, tileDownLeft, tileDownRight
            );

            foreach (Conveyor.Autotile at in this.autotiles)
            {
                if (this.myNeighbours.IsEqual(at.neighbours, at.useCorners))
                {
                    if (this.right)
                    {
                        this.animated.PlayAndLoop(at.animation);
                    }
                    else
                    {
                        this.animated.PlayAndLoop(at.animation.Reversed());
                    }
                    SetCorrectFPS();
                }
            }
            return;
        }

        {
            bool tileUp = this.chunk.ItemAt<Conveyor>(x, y + 1) != null || !this.chunk.IsLocationValid(x, y + 1);
            bool tileDown = this.chunk.ItemAt<Conveyor>(x, y - 1) != null || !this.chunk.IsLocationValid(x, y - 1);
            bool tileLeft = this.chunk.ItemAt<Conveyor>(x - 1, y) != null || !this.chunk.IsLocationValid(x - 1, y);
            bool tileRight = this.chunk.ItemAt<Conveyor>(x + 1, y) != null || !this.chunk.IsLocationValid(x + 1, y);

            bool tileUpLeft = this.chunk.ItemAt<Conveyor>(x - 1, y + 1) != null || !this.chunk.IsLocationValid(x - 1, y + 1);
            bool tileUpRight = this.chunk.ItemAt<Conveyor>(x + 1, y + 1) != null || !this.chunk.IsLocationValid(x + 1, y + 1);
            bool tileDownLeft = this.chunk.ItemAt<Conveyor>(x - 1, y - 1) != null || !this.chunk.IsLocationValid(x - 1, y - 1);
            bool tileDownRight = this.chunk.ItemAt<Conveyor>(x + 1, y - 1) != null || !this.chunk.IsLocationValid(x + 1, y - 1);

            this.myNeighbours = new Conveyor.Neighbours(
                tileUp, tileDown, tileLeft, tileRight, tileUpLeft, tileUpRight, tileDownLeft, tileDownRight
            );

            foreach (Conveyor.Autotile at in this.autotiles)
            {
                if (this.myNeighbours.IsEqual(at.neighbours, at.useCorners))
                {
                    if (this.right)
                    {
                        this.animated.PlayAndLoop(at.animation);
                    }
                    else
                    {
                        this.animated.PlayAndLoop(at.animation.Reversed());
                    }
                    this.specialType = at.specialType;
                    SetCorrectFPS();
                }
            }
        }
    }
}
