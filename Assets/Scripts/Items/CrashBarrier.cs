﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashBarrier : Item
{
    public Animated.Animation animationNormal;
    public Animated.Animation animationBreak;
    public Animated.Animation animationDebrisLeft;
    public Animated.Animation animationDebrisCenter;
    public Animated.Animation animationDebrisRight;

    [Header("Warning Sign")]
    public SpriteRenderer signSpriteRend;
    
    [Header("Size")]
    public int spanX;
    public int spanY;

    [HideInInspector]public bool destroyed;
    private Animated.Animation animationDebrisSign = new Animated.Animation();
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.destroyed = false;
        this.animationDebrisSign.sprites = new Sprite[] { this.signSpriteRend.sprite };

        MakeSolid();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        this.onCollisionWithPlayersHead = CollisionWithPlayerHead;
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if(hitboxes.Length > 0)
        {
            Rect trigger = hitboxes[0].InWorldSpace().Inflate(0.50f);

            Player player = this.map.player;

            if(trigger.Overlaps(player.Rectangle()))
            {
                Collide();
            }
        }
    }

    private void MakeSolid()
    {
        var hitbox = new Hitbox(
            this.spanX * -0.5f,
            this.spanX *  0.5f,
            this.spanY * -0.5f,
            this.spanY *  0.5f,
            this.rotation,
            Hitbox.Solid,
            true,
            true
        );
        this.hitboxes = new [] { hitbox };
    }

    private void CollisionWithPlayerHead()
    {
        float minVelocity = 0.18f;  //default

        if(this.map.player.state == Player.State.Spring
            || this.map.player.state == Player.State.SpringFromPushPlatform)
        {
            minVelocity = 0.1f;
        }

        if (this.map.player.velocity.magnitude > minVelocity)
        {
            Break(true);
        }
    }

    private void Collide()
    {
        if (this.map.player.state == Player.State.RunningOnGripGround)
        {
            Break(true);
        }
    }

    public void Break(bool brokenByPlayer)
    {
        if (this.destroyed == true) return;

        var center = this.hitboxes[0].InWorldSpace().center;

        destroyed = true;
        this.hitboxes = new Hitbox[] {};

        int horizontalSpan = this.spanX - 1;
        Vector2 leftPosition  = this.position.Add(horizontalSpan * -0.5f, 0);
        Vector2 rightPosition = this.position.Add(horizontalSpan *  0.5f, 0);

        var left = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisLeft, 100, leftPosition, this.transform.parent
        );
        left.velocity     = new Vector2(-1f, 7f) / 16f;
        left.acceleration = new Vector2(0f, -0.5f) / 16f;

        var right = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisRight, 100, rightPosition, this.transform.parent
        );
        right.velocity     = new Vector2(1f, 7f) / 16f;
        right.acceleration = new Vector2(0f, -0.5f) / 16f;

        int centerDebrisAmount = spanX - 2;
        for (int i = 0; i < centerDebrisAmount; i++)
        {
            Vector2 centerPosition = leftPosition + Vector2.right * (1.0f * (i + 1));
            
            var centerDebris = Particle.CreatePlayAndHoldLastFrame(
                this.animationDebrisCenter, 100, centerPosition, this.transform.parent
            );

            float debrisDirection = centerPosition.x > center.x ? 1f : -1f;
            float xVelocity = Random.Range(0.50f, 1.50f) * debrisDirection;
            float yVelocity = Random.Range(5.50f, 7.50f);
            float spin = Random.Range(0f, 0.35f);

            centerDebris.velocity     = new Vector2(xVelocity, yVelocity) / 16f;
            centerDebris.acceleration = new Vector2(0f, -0.5f) / 16f;
            centerDebris.spin = spin;
        }

        int countMin, countMax;
        if (this.spanX == 2 && this.spanY == 2)
            (countMin, countMax) = (11, 13);
        else if (this.spanX == 2 || this.spanY == 2)
            (countMin, countMax) = (8, 12);
        else
            (countMin, countMax) = (5, 11);

        Particle.CreateDust(
            this.transform.parent,
            countMin,
            countMax,
            center,
            this.spanX * 0.5f,
            this.spanY * 0.5f
        );

        Vector2 signParticlePosition = this.signSpriteRend.transform.position;
        var signAnimation = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisSign, 100, signParticlePosition, this.transform.parent
        );
        float signDirection = Random.value > 0.5f ? 1f : -1f;
        signAnimation.velocity     = new Vector2(1f * signDirection, 10f) / 16f;
        signAnimation.acceleration = new Vector2(0f, -0.5f) / 16f;
        signAnimation.spin = 0.5f * signDirection;

        // kill enemies above
        Rect damageRect = new Rect(
            this.position.x - (this.spanX * 0.5f),
            this.position.y + (this.spanY * 0.5f),
            this.spanX,
            0.2f
        );
        this.map.DamageEnemy(
            damageRect,
            new Enemy.KillInfo { itemDeliveringKill = this, freezeTime = brokenByPlayer }
        );

        this.spriteRenderer.enabled = false;
        this.signSpriteRend.enabled = false;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBarrierBreak,
            position: this.position
        );
    }

    public void InsidePipe()
    {
        this.spriteRenderer.sortingLayerName = "Spikes";
        this.spriteRenderer.sortingOrder = -10;

        this.signSpriteRend.sortingLayerName = "Spikes";
        this.signSpriteRend.sortingOrder = -9;
    }

    public override void Reset()
    {
        base.Reset();

        if (this.destroyed == true)
        {
            this.spriteRenderer.enabled = true;
            this.signSpriteRend.enabled = true;
            this.animated.PlayOnce(this.animationNormal);

            MakeSolid();

            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.hitboxes[0].InWorldSpace().center,
                this.transform.parent
            );

            this.destroyed = false;
        }
    }
}
