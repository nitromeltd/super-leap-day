﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crowd : Item
{
    public Animated kingAnimated;
    public Animated lilSkullAnimated;
    public Animated whippyAnimated;
    public Animated trunkyAnimated;
    public Animated treeAnimated;
    public Animated handyAnimated;
    public Animated bananaAnimated;
    public Animated pyramidAnimated;
    public Animated vineAnimated;
    public Animated rockyAnimated;
    public Animated fishyAnimated;
    public Animated flamerAnimated;
    public Animated ballChainAnimated;
    public Animated speakersAnimated;

    [Header("King Base")]
    public Animated.Animation animationKingIdleLeft;
    public Animated.Animation animationKingIdleRight;
    public Animated.Animation animationKingTurnToLeft;
    public Animated.Animation animationKingTurnToRight;

    [Header("King Apple Sequence")]
    public Animated.Animation animationKingAppleEat;
    public Animated.Animation animationKingAppleNoTongue;
    public Animated.Animation animationKingAppleChew;
    public Animated.Animation animationKingAppleSwallow;
    public Animated.Animation animationKingAppleTongueDrop;
    public Animated.Animation animationKingAppleTongueHide;

    [Header("Fish Apple Sequence")]
    public Animated.Animation animationFishyIdle;
    public Animated.Animation animationFishyEat;
    public Animated.Animation animationFishyLose;

    [Header("King Right Sequence")]
    public Animated.Animation animationKingRightNoTongue;
    public Animated.Animation animationKingRightChew;
    public Animated.Animation animationKingRightSwallow;
    public Animated.Animation animationKingRightTongueDrop;
    public Animated.Animation animationKingRightTongueHide;
    
    [Header("King Pear Sequence")]
    public Animated.Animation animationKingPearEat;
    
    [Header("Tree Pear Sequence")]
    public Animated.Animation animationTreeIdle;
    public Animated.Animation animationTreeEat;
    public Animated.Animation animationTreeLose;
    
    [Header("King Banana Sequence")]
    public Animated.Animation animationKingBananaEat;
    public Animated.Animation animationKingBananaSpit;
    
    [Header("Banana Banana Sequence")]
    public Animated.Animation animationBananaIdle;
    public Animated.Animation animationBananaStop;
    public Animated.Animation animationBananaDizzy;
    public Animated.Animation animationBananaEmpty;

    [Header("Extra Animations")]
    public Animated.Animation animationLilSkullIdle;
    public Animated.Animation animationWhippyIdle;
    public Animated.Animation animationTrunkyIdle;
    public Animated.Animation animationHandyIdle;
    public Animated.Animation animationPyramidIdle;
    public Animated.Animation animationVineIdle;
    public Animated.Animation animationRockyIdle;
    public Animated.Animation animationFlamerIdle;
    public Animated.Animation animationBallChainIdle;
    public Animated.Animation animationSpeakersIdle;

    [Header("Background")]
    public Transform[] backgroundCrowd;

    [Header("Calendar")]
    public SpriteRenderer calendarSprite;
    public TMPro.TMP_Text calendarMonth;
    public TMPro.TMP_Text calendarDay;

    public enum KingTarget
    {
        NONE,

        APPLE,
        BANANA,
        PEAR,

        Length
    }

    private static bool[] KingTargetFacingLeft = new bool[]
    {
        true,

        true,
        false,
        false
    };

    // crowd breathe movement
    private float[] breatheTimers;
    private Vector2[] breatheInitPositions;
    private const float BreathePeriod = .08f;
    private const float BreatheAmplitude = .3f;

    // king
    private KingTarget latestKingTarget;
    private bool kingSequenceActive;
    private bool isKingFacingLeft;
    private int kingEatTimer;
    private const int KingEatTimePeriod = 300;

    // banana
    private bool moveBananaEat;
    private Vector2 bananaInitialPos;
    private Vector2 bananaCurrentTargetPos;
    private static Vector2 BananaHoldTargetPos = new Vector2(3.69f, 11.3f);
    private static Vector2 BananaEatTargetPos = new Vector2(0f, 8.25f);

    // handy
    private bool moveHandy;
    private Vector2 handyInitialPos;
    private Vector2 handyCurrentTargetPos;
    private static Vector2 HandyTargetPosUp = new Vector2(5.68f, 12.8f);
    private static Vector2 HandyTargetPosDown = new Vector2(5.51f, 11.46f);

    public bool atStart = true;

    public override void Init(Def def)
    {
        base.Init(def);

        if(this.atStart == true)
        {
            this.kingAnimated.PlayAndLoop(this.animationKingIdleLeft);
            this.kingSequenceActive = false;
            this.isKingFacingLeft = true;
            this.kingEatTimer = 0;
            this.latestKingTarget = KingTarget.NONE;

            this.moveBananaEat = false;
            this.bananaInitialPos = this.bananaAnimated.transform.localPosition;

            this.moveHandy = false;
            this.handyInitialPos = this.handyAnimated.transform.localPosition;

            RefreshCalendarDate();

            Audio.instance.PlaySfxLoop(Assets.instance.sfxLoopCrowd, this.transform.position);
        }


        this.lilSkullAnimated.PlayAndLoop(this.animationLilSkullIdle);
        this.whippyAnimated.PlayAndLoop(this.animationWhippyIdle);
        this.trunkyAnimated.PlayAndLoop(this.animationTrunkyIdle);
        this.treeAnimated.PlayAndLoop(this.animationTreeIdle);
        this.handyAnimated.PlayAndLoop(this.animationHandyIdle);
        this.bananaAnimated.PlayAndLoop(this.animationBananaIdle);
        this.pyramidAnimated.PlayAndLoop(this.animationPyramidIdle);
        this.vineAnimated.PlayAndLoop(this.animationVineIdle);
        this.rockyAnimated.PlayAndLoop(this.animationRockyIdle);
        this.fishyAnimated.PlayAndLoop(this.animationFishyIdle);
        this.flamerAnimated.PlayAndLoop(this.animationFlamerIdle);
        this.ballChainAnimated.PlayAndLoop(this.animationBallChainIdle);
        this.speakersAnimated.PlayAndLoop(this.animationSpeakersIdle);

        this.breatheTimers = new float[this.backgroundCrowd.Length];
        this.breatheInitPositions = new Vector2[this.backgroundCrowd.Length];

        for (int i = 0; i < this.backgroundCrowd.Length; i++)
        {
            this.breatheInitPositions[i] = this.backgroundCrowd[i].transform.position;
            this.breatheTimers[i] += Random.Range(0, 2f);
        }        
    }

    public override void Advance()
    {
        // crowd breathe movement
        for (int i = 0; i < this.breatheTimers.Length; i++)
        {
            this.breatheTimers[i] += Time.deltaTime;
            float theta = this.breatheTimers[i] / BreathePeriod;
            float distance = BreatheAmplitude * Mathf.Sin(theta);
            
            this.backgroundCrowd[i].transform.position = 
                this.breatheInitPositions[i] + Vector2.up * distance;
        }

        if (this.atStart == true)
        {
            if (this.kingSequenceActive == false)
            {
                if (kingEatTimer < KingEatTimePeriod)
                {
                    kingEatTimer += 1;
                }
                else
                {
                    PrepareNextEatSequence();
                }
            }

            if (this.moveBananaEat == true)
            {
                this.bananaAnimated.transform.localPosition = Vector2.MoveTowards(
                    this.bananaAnimated.transform.localPosition,
                    this.bananaCurrentTargetPos,
                    Time.deltaTime * 30f
                );

                if (Vector2.Distance(this.bananaAnimated.transform.localPosition, this.bananaCurrentTargetPos) < .01f)
                {
                    this.moveBananaEat = false;
                }
            }

            if (this.moveHandy == true)
            {
                this.handyAnimated.transform.localPosition = Vector2.MoveTowards(
                    this.handyAnimated.transform.localPosition,
                    this.handyCurrentTargetPos,
                    Time.deltaTime * 20f
                );

                if (Vector2.Distance(this.bananaAnimated.transform.localPosition, this.handyCurrentTargetPos) < .01f)
                {
                    this.moveHandy = false;
                }
            }
        }
    }

    private void RefreshCalendarDate()
    {
        var date = Game.selectedDate.AddDays(SaveData.GetFuturePlayOffset());
        var str = Localization.GetTextForMonthName(date).ToUpper();
        calendarMonth.text = str.Substring(0, Mathf.Min(3, str.Length));
        calendarDay.text = date.Day.ToString("00");
    }

    private void PrepareNextEatSequence()
    {
        this.kingSequenceActive = true;

        KingTarget nextKingTarget = (int)this.latestKingTarget + 1 >= (int) KingTarget.Length ?
            (KingTarget)1 : (KingTarget)this.latestKingTarget + 1;

        bool isNextTargetFacingLeft = KingTargetFacingLeft[(int)nextKingTarget];

        if(isNextTargetFacingLeft != this.isKingFacingLeft)
        {
            if(this.isKingFacingLeft == true)
            {
                PlayTurnSequence(isNextTargetFacingLeft, nextKingTarget);
            }
            else
            {
                this.kingAnimated.PlayOnce(this.animationKingRightTongueHide, () =>
                {
                    PlayTurnSequence(isNextTargetFacingLeft, nextKingTarget);
                });
            }
                
        }
        else
        {
            PlayNextKingSequence(nextKingTarget);
        }

    }

    private void PlayTurnSequence(bool isNextTargetFacingLeft, KingTarget nextKingTarget)
    {
        Animated.Animation animationKingTurn = isNextTargetFacingLeft == true ?
            this.animationKingTurnToLeft : this.animationKingTurnToRight;;

        Animated.Animation animationTongueDrop = isNextTargetFacingLeft == true ?
            this.animationKingAppleTongueDrop : this.animationKingRightTongueDrop;
            
        this.kingAnimated.PlayOnce(animationKingTurn, () =>
        {
            this.kingAnimated.PlayOnce(animationTongueDrop, () =>
            {
                this.isKingFacingLeft = isNextTargetFacingLeft;
                PlayNextKingSequence(nextKingTarget);
            });
        });
    }

    private void PlayNextKingSequence(KingTarget nextKingTarget)
    {
        switch(nextKingTarget)
        {
            case KingTarget.APPLE:
                PlayAppleSequence();
                break;

            case KingTarget.BANANA:
                PlayBananaSequence();
                break;

            case KingTarget.PEAR:
                PlayPearSequence();
                break;
        }

        this.latestKingTarget = nextKingTarget;
    }

    private void PlayAppleSequence()
    {
        this.kingAnimated.PlayOnce(animationKingAppleEat, () => {
            this.kingAnimated.PlayOnce(animationKingAppleChew, () => {
                this.kingAnimated.PlayOnce(animationKingAppleSwallow, () => {
                    this.kingAnimated.PlayOnce(animationKingAppleNoTongue, () => {
                        this.kingAnimated.PlayOnce(animationKingAppleTongueDrop, () => {
                            this.kingAnimated.PlayAndLoop(animationKingIdleLeft);
                            PlaySequenceEnd();
                        });
                    });
                });
            });
        });

        this.kingAnimated.OnFrame(3, () => {
            // King steals apple from Fish
            this.fishyAnimated.PlayOnce(animationFishyLose, () => {
                this.fishyAnimated.PlayAndLoop(this.animationFishyIdle);
            });
            
            // Tree takes a bite off its pear
            this.treeAnimated.PlayOnce(this.animationTreeEat, () => {
                this.treeAnimated.PlayAndLoop(this.animationTreeIdle);
            });
            
            this.treeAnimated.OnFrame(1, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosUp;
            });

            this.treeAnimated.OnFrame(15, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosDown;
            });

            this.treeAnimated.OnFrame(18, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = handyInitialPos;
            });
        });
    }

    private void PlayPearSequence()
    {
        this.kingAnimated.PlayOnce(animationKingPearEat, () => {
            this.kingAnimated.PlayOnce(animationKingRightChew, () => {
                this.kingAnimated.PlayOnce(animationKingRightSwallow, () => {
                    this.kingAnimated.PlayOnce(animationKingRightNoTongue, () => {
                        this.kingAnimated.PlayOnce(animationKingRightTongueDrop, () => {
                            this.kingAnimated.PlayAndLoop(animationKingIdleRight);
                            PlaySequenceEnd();
                        });
                    });
                });
            });
        });

        // King steals pear from Tree
        this.kingAnimated.OnFrame(4, () => {
            this.treeAnimated.PlayOnce(animationTreeLose, () => {
                this.treeAnimated.PlayAndLoop(this.animationTreeIdle);
            });
            
            // move Handy (monster on top of tree)
            this.treeAnimated.OnFrame(3, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosUp;
            });

            this.treeAnimated.OnFrame(11, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosDown;
            });

            this.treeAnimated.OnFrame(28, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = handyInitialPos;
            });
        });
        
        // Fish takes a bite off its apple
        this.fishyAnimated.PlayOnce(this.animationFishyEat, () => {
            this.fishyAnimated.PlayAndLoop(this.animationFishyIdle);
        });
    }

    private void PlayBananaSequence()
    {
        this.kingAnimated.PlayOnce(animationKingBananaEat, () => {
            this.kingAnimated.PlayOnce(animationKingRightChew, () => {
                this.kingAnimated.PlayOnce(animationKingBananaSpit, () => {
                    this.kingAnimated.PlayOnce(animationKingRightNoTongue, () => {
                        this.kingAnimated.PlayOnce(animationKingRightTongueDrop, () => {
                            this.kingAnimated.PlayAndLoop(animationKingIdleRight);
                            PlaySequenceEnd();
                        });
                    });
                });

                this.kingAnimated.OnFrame(12, () => {
                    this.bananaAnimated.PlayOnce(this.animationBananaDizzy, () => {
                        this.bananaAnimated.PlayAndLoop(this.animationBananaIdle);
                    });
                    this.moveBananaEat = true;
                    this.bananaCurrentTargetPos = this.bananaInitialPos;
                });
            });
        });

        // King holds Banana with tongue
        this.kingAnimated.OnFrame(8, () => {
            this.moveBananaEat = true;
            this.bananaCurrentTargetPos = BananaHoldTargetPos;
            this.bananaAnimated.PlayOnce(this.animationBananaStop);
        });

        this.kingAnimated.OnFrame(9, () => {
            this.moveBananaEat = true;
            this.bananaCurrentTargetPos = bananaInitialPos;
        });

        // start moving Banana towards King's mouth
        this.kingAnimated.OnFrame(14, () => {
            this.moveBananaEat = true;
            this.bananaCurrentTargetPos = BananaEatTargetPos;
        });

        // hide Banana
        this.kingAnimated.OnFrame(17, () => {
            this.bananaAnimated.PlayOnce(this.animationBananaEmpty);
        });

        // Fish and Tree take a bite off their fruits
        this.kingAnimated.OnFrame(3, () => {
            this.fishyAnimated.PlayOnce(this.animationFishyEat, () => {
                this.fishyAnimated.PlayAndLoop(this.animationFishyIdle);
            });

            this.treeAnimated.PlayOnce(this.animationTreeEat, () => {
                this.treeAnimated.PlayAndLoop(this.animationTreeIdle);
            });
            
            // move Handy (monster on top of tree)
            this.treeAnimated.OnFrame(1, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosUp;
            });

            this.treeAnimated.OnFrame(15, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = HandyTargetPosDown;
            });

            this.treeAnimated.OnFrame(18, () => {
                this.moveHandy = true;
                this.handyCurrentTargetPos = handyInitialPos;
            });
        });
    }

    private void PlaySequenceEnd()
    {
        this.kingEatTimer = 0;
        this.kingSequenceActive = false;
    }
}
