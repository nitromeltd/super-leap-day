using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalBarrierBlock : Item
{
    public Autotile[] autotiles;
    public Sprite[] particles;
    public AnimationCurve particleScaleOutCurve;

    [HideInInspector] public Neighbours myNeighbours;
    [HideInInspector] public CrystalBarrierLeader leader;
    [HideInInspector] public int gridX;
    [HideInInspector] public int gridY;
    [HideInInspector] public bool shattered = false;

    private Sprite RandomParticle =>
        this.particles[Random.Range(0, this.particles.Length)];

    [System.Serializable]
    public struct Autotile
    {
        public enum Type
        {
            Single,
            Horizontal,
            Vertical 
        }

        public Sprite[] sprites;
        public Neighbours neighbours;
    }

    [System.Serializable]
    public struct Neighbours
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;

        public Neighbours(bool up, bool down, bool left, bool right)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;

            bool isVertical = up || down;
            bool isHorizontal = left || right;
        }
        
        public bool IsEqual(Neighbours n) 
        {
            return this.up == n.up &&
                this.down == n.down &&
                this.left == n.left &&
                this.right == n.right;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        ToggleHitbox(true);
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        Player player = this.map.player;
        var thisRect = this.hitboxes[0].InWorldSpace();
        foreach (var minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if(minecart.HasCrashed() == true) continue;

            Rect minecartRect = minecart.hitboxes[0].InWorldSpace();
            minecartRect.x += this.map.player.velocity.x;
            minecartRect.y += this.map.player.velocity.y;

            if(thisRect.Overlaps(minecartRect) && this.shattered == false)
            {
                Shatter(minecart.velocity);
            }
        }
    }

    public override void Reset()
    {
        base.Reset();

        this.shattered = false;
        this.spriteRenderer.enabled = true;
        ToggleHitbox(true);
    }

    public void ApplyAutotiling()
    {
        foreach(CrystalBarrierBlock.Autotile at in autotiles)
        {
            if(myNeighbours.IsEqual(at.neighbours))
            {
                spriteRenderer.sprite =
                    at.sprites[Random.Range(0, at.sprites.Length)];
            }
        }
    }

    public IEnumerator WaitAndShatter(Vector2 minecartVelocity)
    {
        if(this.map.player.alive == false) yield break;

        yield return new WaitForSeconds(0.02f);
        Shatter(minecartVelocity);
    }

    public void Shatter(Vector2 minecartVelocity)
    {
        if(this.map.player.alive == false) return;
        if(this.shattered == true) return;
        this.shattered = true;
        this.spriteRenderer.enabled = false;
        
        if(this.leader != null)
        {
            this.leader.NotifyBlockShattered(this, minecartVelocity);
        }

        ToggleHitbox(false);
        this.map.gameCamera.ScreenShake(minecartVelocity.normalized * 0.20f);
        
        int particlesAmount = 3;
        float angleRange = 45f;
        int lifetime = 30;
        float spin = 10f;
        for (int i = 0; i < particlesAmount; i++)
        {
            float speed = Random.Range(0.25f, 0.40f);
            float acceleration = speed / (float)(lifetime);

            Vector2 particlePosition = this.position + Random.insideUnitCircle * 0.15f;

            Particle p = Particle.CreateWithSprite(
                RandomParticle, lifetime, particlePosition, this.transform.parent
            );

            float angle = Random.Range(-angleRange, angleRange);
            Vector2 direction =
                Quaternion.Euler(Vector3.forward * angle) * minecartVelocity.normalized;
            p.velocity = direction * speed;
            p.acceleration = -direction * acceleration;
            p.ScaleOut(this.particleScaleOutCurve);
            p.spin = Random.Range(-spin, spin);
        }
        Audio.instance.PlaySfx(Assets.instance.sfxEnvCrystalWallShatter, position: this.position, options: new Audio.Options(1, true, 0.1f));
    }

    private void ToggleHitbox(bool solid)
    {
        Hitbox.SolidData solidData = solid ? Hitbox.Solid : Hitbox.NonSolid;
        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, solidData, true, true)
        };
    }
}
