using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CrystalBarrierLeader : Item
{
    private CrystalBarrierBlock[,] grid;

    public override void Init(Def def)
    {
        base.Init(def);

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        SetupAutotiling();
    }

    private void SetupAutotiling()
    {
        bool IsItemInGrid(int x, int y)
        {
            if(x < 0 || x >= this.grid.GetLength(0)) return false;
            if(y < 0 || y >= this.grid.GetLength(1)) return false;

            return this.grid[x, y] != null;
        }

        List<CrystalBarrierBlock> crystalBlocks = new List<CrystalBarrierBlock>();
        foreach(var f in this.group.followers)
        {
            if(f.Key.item is CrystalBarrierBlock)
            {
                CrystalBarrierBlock block = f.Key.item as CrystalBarrierBlock;
                crystalBlocks.Add(block);
                block.leader = this;
            }
        }

        if(crystalBlocks.Count <= 0)
        {
            this.chunk.ReportError(this.position, "Crystal Barrier Leader is missing blocks!");
            return;
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(crystalBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(crystalBlocks.Min(t => t.position.y));
        
        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(crystalBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(crystalBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new CrystalBarrierBlock[sizeX, sizeY];
        
        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;
        
        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in crystalBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;

            t.gridX = gridX;
            t.gridY = gridY;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGrid(x, y + 1);
                bool down = IsItemInGrid(x, y - 1);
                bool left = IsItemInGrid(x - 1, y);
                bool right = IsItemInGrid(x + 1, y);

                this.grid[x, y].myNeighbours =
                    new CrystalBarrierBlock.Neighbours(up, down, left, right);
            }
        }

        // choose autotile sprite
        foreach(var t in crystalBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    public void NotifyBlockShattered(CrystalBarrierBlock block, Vector2 minecartVelocity)
    {
        if(block.myNeighbours.up == true)
        {
            CrystalBarrierBlock blockUp = this.grid[block.gridX, block.gridY + 1];

            if(blockUp.shattered == false)
            {
                StartCoroutine(blockUp.WaitAndShatter(minecartVelocity));
            }
        }

        if(block.myNeighbours.down == true)
        {
            CrystalBarrierBlock blockDown = this.grid[block.gridX, block.gridY - 1];

            if(blockDown.shattered == false)
            {
                StartCoroutine(blockDown.WaitAndShatter(minecartVelocity));
            }
        }
        
        if(block.myNeighbours.left == true)
        {
            CrystalBarrierBlock blockLeft = this.grid[block.gridX - 1, block.gridY];

            if(blockLeft.shattered == false)
            {
                StartCoroutine(blockLeft.WaitAndShatter(minecartVelocity));
            }
        }

        if(block.myNeighbours.right == true)
        {
            CrystalBarrierBlock blockRight = this.grid[block.gridX + 1, block.gridY];

            if(blockRight.shattered == false)
            {
                StartCoroutine(blockRight.WaitAndShatter(minecartVelocity));
            }
        }
    }
}
