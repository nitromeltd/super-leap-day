using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrystalFruit : Pickup
{
    public Animated.Animation animationFruit;
    public Animated.Animation animationCollect2;
    public Animated.Animation animationShine;
    public Sprite[] crystalParticles;
    public AnimationCurve particleScaleOutCurve;

    private bool shattered = false;
    private Vector2 originPosition;
    private Vector2 nudgeVelocity;
    private float nudgeMagnitude;

    private const float NudgeForce = 0.15f;

    private bool IsNudged => this.nudgeVelocity.magnitude > 0.05f;

    private int shineTimer = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        this.originPosition = this.position;
    }

    public override void CheckCollect()
    {
        if (this.disallowCollectTime > 0)
        {
            this.disallowCollectTime -= 1;
        }
        else if (this.shattered == false)
        {
            Player player = this.map.player;

            if(this.canPlayerCollect == false) return;
            if(player.state == Player.State.Respawning) return;

            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = player.Rectangle();
            Vector2 playerPosition = player.position;
            Vector2 direction = (this.position - playerPosition).normalized;

            foreach (var minecart in this.chunk.subsetOfItemsOfTypeMinecart)
            {
                if(minecart.isPlayerInsideCart == false) continue;

                Rect minecartRect = minecart.hitboxes[0].InWorldSpace();

                if(thisRect.Overlaps(minecartRect) && this.shattered == false)
                {
                    ShakeAndShatter(minecart.velocity);
                }
            }

            if (thisRect.Overlaps(playerRect) && this.shattered == false)
            {
                this.nudgeVelocity = direction * 0.09f;
            }
        }
    }

    private void ShakeAndShatter(Vector2 minecartVelocity)
    {
        this.map.gameCamera.ScreenShake(minecartVelocity.normalized * 0.20f);
        Audio.instance.PlaySfx(Assets.instance.sfxEnvCrystalFruitShatter, position: this.position, options: new Audio.Options(1, true, 0.1f));
        Shatter(minecartVelocity);
    }

    private void Shatter(Vector2 minecartVelocity)
    {
        if(this.shattered == true) return;
        this.shattered = true;
        this.animated.PlayAndLoop(this.animationFruit);

        this.position = IngameUI.instance.MoveTransformToUILayer(this.map, this.transform);

        int particlesAmount = 3;
        float angleRange = 45f;
        int lifetime = 30;
        float spin = 10f;
        for (int i = 0; i < particlesAmount; i++)
        {
            float speed = Random.Range(0.25f, 0.40f);
            float acceleration = speed / (float)(lifetime);

            Sprite randomSprite =
                this.crystalParticles[Random.Range(0, this.crystalParticles.Length)];

            Vector2 particlePosition = this.position + Random.insideUnitCircle * 0.15f;

            Particle p = Particle.CreateWithSprite(
                randomSprite, lifetime, particlePosition, this.transform.parent
            );

            float angle = Random.Range(-angleRange, angleRange);
            Vector2 direction =
                Quaternion.Euler(Vector3.forward * angle) * minecartVelocity.normalized;
            p.velocity = direction * speed;
            p.acceleration = -direction * acceleration;
            p.ScaleOut(this.particleScaleOutCurve);
            p.spin = Random.Range(-spin, spin);
        }
    }

    public override void Collect()
    {
        if(this.collected == true) return;
        this.collected = true;

        if(this.animated == true)
        {
            this.animated.PlayOnce(this.animationCollect, delegate
            {
                this.spriteRenderer.enabled = false;
            });
        }

        this.animated.OnFrame(5, delegate
        {
            Audio.instance.PlaySfx(Assets.instance.sfxFruit, position: this.position, options: new Audio.Options(1,true,0.2f));
        });
        this.animated.OnFrame(8, delegate
        {
            var p = Particle.CreateAndPlayOnce(
                this.animationCollect2, 
                this.position,
                this.transform.parent
            );

            IngameUI.instance.MoveTransformToUILayer(this.map, p.transform);
        });

        this.map.AddFruit(1);
    }

    public override void Advance()
    {
        base.Advance();

        if(this.collected == false)
        {
            Shine();
        }
        if (this.shattered == true)
        {
            ShatteredMovement();
        }
        else
        {
            Movement();
        }

        this.nudgeMagnitude = this.nudgeVelocity.magnitude;
    }
    private void Shine()
    {
        if (this.shineTimer > 0)
        {
            this.shineTimer--;
        }
        else
        {
            this.shineTimer = 60 * 3;
            if (this.collected == false)
            {
                this.animated.PlayOnce(this.animationShine, () => {
                    this.animated.PlayAndHoldLastFrame(this.animationNormal);
                });
            }
        }
    }
    protected override void FollowGroup()
    {
        Vector2 groupPosition = this.group.FollowerPosition(this);

        if(IsNudged == false && Vector2.Distance(this.position, groupPosition) < 0.10f)
        {
            this.position = this.group.FollowerPosition(this);
        }
    }

    private void Movement()
    {
        if(this.group != null)
        {
            this.originPosition = this.group.FollowerPosition(this);
        }

        Vector2 breathePosition = Vector2.zero;
        if (this.movingFreely == false &&
            this.collected == false &&
            this.suckActive == false)
        {
            float centerY = this.position.y;
            var theta =
                this.map.frameNumber * 0.08f +
                this.def.tx * (0.5f + Util.Tau / 4) +
                this.def.ty * (0.5f + Util.Tau / 4);
            breathePosition.y += Mathf.Sin(theta) * 2.5f / 16;
        }
        
        Vector2 targetVelocity =
            (this.originPosition - (Vector2)this.position) * NudgeForce;

        this.nudgeVelocity *= 0.95f;
        this.nudgeVelocity += targetVelocity * NudgeForce;

        this.position += this.nudgeVelocity;
        this.transform.position = this.position + breathePosition;
    }

    private void ShatteredMovement()
    {
        if(this.spriteRenderer.enabled == false) return;

        Vector2 targetPosition = IngameUI.instance.FruitCounterPosition(this.map);

        this.position.x = Util.Slide(this.position.x, targetPosition.x, 1f);
        this.position.y = Util.Slide(this.position.y, targetPosition.y, 1f);

        if(Vector2.Distance(this.position, targetPosition) < 0.02f)
        {
            Collect();
        }

        this.transform.position = this.position;
    }
}
