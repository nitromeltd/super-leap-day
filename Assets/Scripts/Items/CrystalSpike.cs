using UnityEngine;
using System.Linq;
using System;

public class CrystalSpike : Item
{
    public Animated.Animation animationNormal;
    public Animated.Animation animationShine;
    public Animated.Animation animationEmpty;
    private const float HitboxSingle = 0.25f;
    private SnowBlock snowBlock;
    private float shineTimer = 0;

    public bool isBroken = false;
    public bool isGrowing = false;
    public bool playerCrossedChunk = false;

    public Vector3 originalPosition;

    public override void Init(Def def)
    {
        base.Init(def);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        float rand = UnityEngine.Random.Range(4, 7);
        this.shineTimer = rand;
        this.animated.PlayAndHoldLastFrame(this.animationNormal);

        this.hitboxes = new[] {
            new Hitbox(-HitboxSingle, HitboxSingle, -HitboxSingle, HitboxSingle, this.rotation, Hitbox.NonSolid)
        };
        this.transform.position = new Vector3(transform.position.x, transform.position.y + UnityEngine.Random.Range(-0.2f, 0.2f));
        this.transform.localRotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-5f, 5f));
        originalPosition = transform.position;
    }

    public override void Advance()
    {
        if (this.map.NearestChunkTo(this.map.player.position) == this.map.chunks[this.map.chunks.IndexOf(chunk) + 2])
        {
            this.playerCrossedChunk = true;
        }

        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if (IsDeadly() == true)
        {
            var player = this.map.player;
            var thisRect = this.hitboxes[0].InWorldSpace();
            if (player.ShouldCollideWithItems() && thisRect.Overlaps(player.Rectangle()))
            {
                this.map.player.Hit(this.position);
            }
            else
            {
                var enemies =
                    this.chunk.subsetOfItemsOfTypeEnemy.OfType<WalkingEnemy>().ToArray();
                foreach (var enemy in enemies)
                {
                    if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                        enemy.Kill(new Enemy.KillInfo { itemDeliveringKill = this });
                }
            }
        }

        if(this.isBroken == false)
        {
            Shine();

            if(this.isGrowing == true)
            {
                CreateDust();
                if (transform.position.y <= originalPosition.y)
                {
                    transform.position = transform.position + new Vector3(0, 0.01f) + new Vector3(UnityEngine.Random.Range(-0.01f, 0.01f), UnityEngine.Random.Range(-0.01f, 0.01f)); ;
                }
                else
                {
                    this.isGrowing = false;
                }
            }
        }
        else
        {
            if (this.map.player.alive != true || (this.playerCrossedChunk == true && this.map.player.position.y < this.transform.position.y))
            {
                Regrow();
                this.playerCrossedChunk = false;
            }
        }
    }

    private void Shine()
    {
        if (this.shineTimer > 0)
        {
            this.shineTimer -= Time.deltaTime;
        }
        else
        {
            float rand = UnityEngine.Random.Range(4, 7);
            this.shineTimer = rand;
            this.animated.PlayOnce(this.animationShine, () => {
                this.animated.PlayAndHoldLastFrame(this.animationNormal);
            });
        }
    }

    public void Regrow()
    {
        this.isBroken = false;
        this.isGrowing = true;
        this.animated.PlayAndLoop(this.animationNormal);
        this.transform.position = originalPosition - new Vector3(0, 1.5f);
    }

    public void Break(Vector2 dir)
    {
        if (this.isBroken == true || this.isGrowing == true) return;
        this.isBroken = true;
        this.animated.PlayAndLoop(this.animationEmpty);
        Particle p = Particle.CreatePlayAndHoldLastFrame(
                this.animationNormal,
                300,
                this.transform.position,
                this.transform.parent
            );

        p.Throw(new Vector2(dir.normalized.x / 3.25f, 0.225f), 0.12f, 0.1f, new Vector2(0, -0.02f));

        p.spin = UnityEngine.Random.Range(1.5f, 7.0f);
        if (UnityEngine.Random.Range(0, 2) == 0)
            p.spin *= -1;
        p.spriteRenderer.sortingLayerName = "Items (Front)";
        p.spriteRenderer.sortingOrder = 0;
        Audio.instance.PlaySfx(Assets.instance.sfxEnvCrystalFruitShatter, position: this.position, options: new Audio.Options(1, true, 0.05f));
    }

    private bool IsDeadly()
    {
        if (IsCoveredWithSnow() == true)
        {
            return false;
        }

        if(this.isBroken == true)
        {
            return false;
        }

        return true;
    }

    public void CreateDust()
    {
        if (UnityEngine.Random.value > 0.025f) return;
        Particle.CreateDust(
            this.chunk.transform, 1, 2, 
            this.transform.position + new Vector3(UnityEngine.Random.Range(-0.1f, 0.1f), UnityEngine.Random.Range(-0.1f, 0.1f)),
            velocity: new Vector2(0, -0.02f),
            sortingLayerName: "Items"
        );
    }

    public void CoverWithSnow()
    {
        if (this.snowBlock == null)
        {
            this.snowBlock = SnowBlock.Create(this.position, this.chunk);
            var nearestSpikeTile = this.chunk.NearestItemTo<SpikeTile>(this.position, 0.5f);
            if (nearestSpikeTile != null)
                this.snowBlock.AttachToGroup(this.chunk.groupLayer.RegisterFollower(this.snowBlock, nearestSpikeTile.ItemDef.tx, nearestSpikeTile.ItemDef.ty));
        }
        else
        {
            this.snowBlock.ForceUnmelt();
        }
    }

    public void AssignNewSnowBlock(SnowBlock snowBlock)
    {
        if (this.snowBlock != null && this.snowBlock != snowBlock)
        {
            this.snowBlock.Melt();
        }

        this.snowBlock = snowBlock;
    }

    public bool IsCoveredWithSnow()
    {
        if (this.snowBlock != null)
        {
            return this.snowBlock.IsSnowActive();
        }

        return false;
    }
}
