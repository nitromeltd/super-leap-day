using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;

public class CursedShrine : Item
{
    public bool invisibleCursedShrine;
    private List<Item> connectedItems = new List<Item>();
    private Vector2 startPosition;
    private bool lateInitialized = false;
    public Animated.Animation idleAnimation;
    public Animated.Animation activatedAnimation;
    public Animated.Animation activatedIdleAnimation;
    public Animated.Animation deactivatedAnimation;
    private int triggerDelayTime = 0;

    public override void Init(Def def)
    {
        base.Init(def);
        this.startPosition = this.position;
        this.hitboxes = new[] {
            new Hitbox(-1f, 1f, 0f, 3f, this.rotation, Hitbox.NonSolid)
        };

        if(this.invisibleCursedShrine == false)
        {
            this.animated.PlayAndLoop(this.idleAnimation);
        }
    }

    public RumbleGround NearestRumbleGroundStartingAt(
        Vector2 position,
        float maxDistance = float.PositiveInfinity
    )
    {
        RumbleGround result = null;
        float maxSqDistance = maxDistance * maxDistance;

        foreach (var rg in this.chunk.subsetOfItemsOfTypeRumbleGround)
        {
            var dx = rg.StartingPosition.x - position.x;
            var dy = rg.StartingPosition.y - position.y;
            var sqDist = dx * dx + dy * dy;
            if (sqDist < maxSqDistance)
            {
                maxSqDistance = sqDist;
                result = rg;
            }
        }

        return result;
    }

    private void SetUpConnectedItems()
    {
        connectedItems.Clear();
        var connectedPaths = NearestPathsTo(this.chunk.connectionLayer, this.startPosition, 2f);
        // DebugConnections(connectedPaths);
        foreach(var path in connectedPaths)
        {
            if(path.edges.Length > 0)
            {
                var start = path.edges[0].from;
                if((this.position - start.Position).magnitude <= 0.5f)
                {
                    //outgoing path
                    foreach(var edge in path.edges)
                    {
                        var rumbleGround = this.NearestRumbleGroundStartingAt(edge.to.Position, 0.5f);
                        if(rumbleGround != null)
                        {
                            connectedItems.Add((Item)rumbleGround);
                        }

                        var zombie = this.chunk.NearestItemTo<Zombie>(edge.to.Position, 2f);
                        if(zombie != null)
                        {
                            zombie.Hide();
                            connectedItems.Add((Item)zombie);
                        }

                        var zombieHand = this.chunk.NearestItemTo<ZombieHand>(edge.to.Position, 2f);
                        if(zombieHand != null)
                        {
                            zombieHand.Hide();
                            connectedItems.Add((Item)zombieHand);
                        }

                        var snakeGhostGrave = this.chunk.NearestItemTo<SnakeGhostGrave>(edge.to.Position, 2f);
                        if(snakeGhostGrave != null)
                        {
                            snakeGhostGrave.Hide();
                            connectedItems.Add((Item)snakeGhostGrave);
                        }
                    }
                }
            }
        }
    }

    private void DebugConnections(List<Path> itemToDebug)
    {
        int i = 0;
        foreach(var p in itemToDebug)
        {
            int j = 0;
            foreach(var e in p.edges)
            {
                Debug.Log($"{i}.{j}:{e.from.Position}=>{e.to.Position}");
                j++;
            }
            i++;
        }
    }

    override public void Advance()
    {
        if(lateInitialized == false)
        {
            lateInitialized = true;
            SetUpConnectedItems();
        }

        if (this.map.player.ShouldCollideWithItems())
        {
            var playerRect = this.map.player.Rectangle();
            if(playerRect.Overlaps(this.hitboxes[0].InWorldSpace()) == true)
            {
                TriggerShrine();
            }
        }

        if(this.triggerDelayTime > 0)
        {
            this.triggerDelayTime--;
        }
    }

    public void TriggerShrine()
    {
        if(this.triggerDelayTime > 0)
            return;

        foreach(var item in connectedItems)
        {
            if(item is RumbleGround)
            {
                ((RumbleGround)item).TriggerGround();
            }

            if(item is Zombie)
            {
                ((Zombie)item).Spawn();
            }

            if(item is ZombieHand)
            {
                ((ZombieHand)item).Spawn();
            }

            if(item is SnakeGhostGrave)
            {
                ((SnakeGhostGrave)item).Spawn();
            }
        }

        if(this.invisibleCursedShrine == true)
        {
            this.triggerDelayTime = 120;
            return;
        }

        if(this.animated.currentAnimation == this.idleAnimation)
        {
            this.animated.PlayOnce(this.activatedAnimation, () =>
            {
                this.animated.PlayAndLoop(this.activatedIdleAnimation);
            });
            Audio.instance.PlaySfx(
                Assets.instance.tombstoneHill.sfxTombstoneTriggerOn, null, this.position
            );
            this.triggerDelayTime = 120;
        }
        // else
        // {
        //     this.animated.PlayOnce(this.deactivatedAnimation, () =>
        //     {
        //         this.animated.PlayAndLoop(this.idleAnimation);
        //     });
        //     Audio.instance.PlaySfx(
        //         Assets.instance.tombstoneHill.sfxTombstoneTriggerOff, null, this.position
        //     );
        //     this.triggerDelayTime = 120;
        // }
    }

    public override void Reset()
    {
        lateInitialized = false;
        if (this.invisibleCursedShrine == false)
        {
            this.animated.PlayAndLoop(this.idleAnimation);
        }
    }

    public List<Path> NearestPathsTo(PathLayer layer, Vector2 pt, float maxDistance = Mathf.Infinity)
        {
            var lowestSqDist = maxDistance * maxDistance;
            List<Path> result = new List<Path>();

            foreach (var p in layer.paths)
            {
                if (p.nodes.Length > 1)
                {
                    bool closer = false;
                    foreach (var e in p.edges)
                    {
                        var sqDist = e.SquareDistanceToPoint(pt);

                        if(sqDist <= maxDistance)
                        {
                            closer = true;
                        }
                    }
                    if(closer == true)
                        result.Add(p);
                }
                else if (p.nodes.Length == 1)
                {
                    var n = p.nodes[0];
                    var dx = n.x - pt.x;
                    var dy = n.y - pt.y;
                    var sqDist = dx * dx + dy * dy;
                    if (sqDist <= lowestSqDist)
                        result.Add(p);
                }
            }
            return result;
        }
}