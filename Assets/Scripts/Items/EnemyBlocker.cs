
using UnityEngine;

public class EnemyBlocker : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation swivelForwardAnimation; // player moving up or right
    public Animated.Animation swivelBackwardAnimation; // player moving down or left
    public bool isHorizontal;

    private float swivelAnimFPS;
    private bool swivelForward;

    private const float DecreaseSpeedForce = 20f;
    private const int MinToIdleAnimFPS = 15;
    private const int MinSwivelAnimFPS = 30;
    private const int MaxSwivelAnimFPS = 90;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid)
        };
        this.solidToPlayer = false;

        this.swivelAnimFPS = 0;
        this.animated.PlayOnce(this.idleAnimation);
    }

    public override void Advance()
    {
        if (this.map.player.Rectangle().Overlaps(
            this.hitboxes[0].InWorldSpace()
        ))
        {
            Swivel();
        }

        if(this.swivelAnimFPS > MinToIdleAnimFPS)
        {
            this.swivelAnimFPS -= DecreaseSpeedForce * Time.deltaTime;

            this.animated.PlayAndLoop(this.swivelForward ?
                this.swivelForwardAnimation : this.swivelBackwardAnimation);

            this.animated.currentAnimation.fps = Mathf.RoundToInt(this.swivelAnimFPS);
        }
        else if(this.animated.frameNumber == 0)
        {
            this.animated.PlayOnce(this.idleAnimation);
            this.animated.currentAnimation.fps = 30;
        }
    }

    private void Swivel()
    {
        Vector2 playerVelocity = this.map.player.velocity;

        float playerSpeed = isHorizontal ? playerVelocity.x : playerVelocity.y;

        this.swivelForward = playerSpeed >= 0f;
        this.swivelAnimFPS = Mathf.Lerp(
            MinSwivelAnimFPS, MaxSwivelAnimFPS, Mathf.InverseLerp(0f, 1f, Mathf.Abs(playerSpeed))
        );
    }
}
