using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyRespawner : Item
{
    public Animated.Animation animationOrbIdle;
    private List<LightningBolt> lightningBolts = new List<LightningBolt>();

    public class LightningBolt
    {
        private Vector2 startPosition, endPosition;
        private float angle;
        private int numberOfPoints;
        private int currentPoint;
        private int delay;
        private Map map;
        private float delta;
        private Vector2 deltaV;
        const float boltLenght = 4.7f;
        public LightningBolt(Vector2 startPosition, Vector2 endPosition, Map map)
        {
            this.startPosition = startPosition;
            this.endPosition = endPosition;             

            var relative = endPosition - startPosition;
            this.angle = Util.WrapAngle0To360(Vector3.SignedAngle(Vector3.right, relative, Vector3.forward));
            this.numberOfPoints = 1;
            this.map = map;
            if(relative.magnitude - boltLenght > 0)
            {
                this.numberOfPoints = 1 + Mathf.FloorToInt((relative.magnitude - boltLenght)/2);
            }

            this.delay = 1;

            this.delta = (relative.magnitude - boltLenght)/ numberOfPoints;
            this.deltaV = delta * relative.normalized;
        }

        public bool Advance()
        {
            if(this.delay > 0)
            {
                if(--this.delay == 0)
                {
                    this.delay = 5;
                    CreateBoltParticle(startPosition + currentPoint * deltaV);
            
                    if(++currentPoint == numberOfPoints)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void CreateBoltParticle(Vector3 position)
        {

            var bolt = Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnerLightningBoltAnimation,
                position,
                this.map.transform
            );
            bolt.transform.rotation = Quaternion.Euler(0, 0, this.angle);
            bolt.spriteRenderer.sortingLayerName = "Items (Front)";
            bolt.spriteRenderer.sortingOrder = 2;
        }
    }

    public class ProtectedEnemy
    {
        public List<Enemy> enemyAndSpawnedThings;
        public Def definition;
        public int delayCnt;

        public ProtectedEnemy(Enemy enemy, Def definition)
        {
            this.enemyAndSpawnedThings = new List<Enemy>();
            this.enemyAndSpawnedThings.Add(enemy);
            this.definition = definition;
        }

        public bool IsEnemyCompletelyDead()
        {
            foreach(var e in enemyAndSpawnedThings)
            {
                if(e != null)
                    return false;
            }
            return true;
        }
    }

    public override void Reset()
    {
        initialized = false;
        enemiesToBeProtected.Clear();
    }

    private List<ProtectedEnemy> enemiesToBeProtected = new List<ProtectedEnemy>();

    private bool initialized = false;

    public override void Init(Def def)
    {
        base.Init(def);
        this.initialized = false;
        enemiesToBeProtected.Clear();
        this.animated.PlayAndLoop(animationOrbIdle);
        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
    }

    private void GetEnemies(Def def, float maxDistance = Mathf.Infinity)
    {
        List<(NitromeEditor.Path path, NitromeEditor.Path.Node node)> results = new List<(NitromeEditor.Path, NitromeEditor.Path.Node)>();

        var lowestSqDist = maxDistance * maxDistance;
        foreach (var p in this.chunk.connectionLayer.paths)
        {
            foreach (var n in p.nodes)
            {
                var dx = n.x - def.tx;
                var dy = n.y - def.ty;
                var sqDist = dx * dx + dy * dy;
                if (sqDist < lowestSqDist)
                {
                    results.Add((p, n));
                }
            }
        }

        foreach (var r in results)
        {
            if (r.path.nodes.Length > 1)
            {
                NitromeEditor.Path.Node targetNode = (r.path.nodes[0] == r.node) ? r.path.nodes[1] : r.path.nodes[0];

                var enemyFound = this.chunk.NearestItemTo<Enemy>(new Vector2(targetNode.x, targetNode.y), 1);
                if (enemyFound != null)
                {
                    var protectedEnemy = new ProtectedEnemy(enemyFound, enemyFound.ItemDef);
                    enemiesToBeProtected.Add(protectedEnemy);
                }
            }
        }
    }

    public override void Advance()
    {
        base.Advance();
#if UNITY_EDITOR
        // if(Input.GetKeyDown(KeyCode.L))
        // {
        //     this.map.DropLuckyPickup(Pickup.LuckyDrop.PowerupYellowPet, this.position, this.chunk);
        // }
#endif
        if (!this.initialized)
        {
            this.initialized = true;
            GetEnemies(def, 2);
        }

        Player player = this.map.player;
        if (player.alive == false ||
            player.state == Player.State.Respawning ||
            player.state == Player.State.Finished)
            return;

        for (int i = this.enemiesToBeProtected.Count - 1; i >= 0; i--)
        {
            var e = this.enemiesToBeProtected[i];
            if (e.IsEnemyCompletelyDead() == true)
            {
                if (e.delayCnt == 0)
                {
                    e.delayCnt = 3 * 60;   
                }
                else
                {
                    e.delayCnt--;
                    
                    var pos = new Vector3(
                                            (e.definition.tx * Tile.Size) + (e.definition.tile.offsetX / 16f),
                                            (e.definition.ty * Tile.Size) + (e.definition.tile.offsetY / 16f),
                                            0.0f
                                        );

                    if (e.delayCnt == 0)
                    {
                        RespawnEnemy(e);
                    }
                    else if(e.delayCnt == 10)
                    {
                        CreateRespawnOrbParticle(pos);
                        CreateRespawnOrbLightningParticle(pos);

                        Audio.instance.PlaySfx(Assets.instance.sfxEnvSpawnerLand);
                    }
                    else if(e.delayCnt == 20)
                    {
                        lightningBolts.Add(new LightningBolt(this.transform.position, pos, this.map));

                        Audio.instance.PlaySfx(Assets.instance.sfxEnvSpawnerShoot);
                    }
                    else if(e.delayCnt == 24)
                    {
                        CreateActivateOrbParticle(this.position);
                    }
                }
            }
        }

        for (int i = lightningBolts.Count - 1; i >= 0; i--)
        {
            if(lightningBolts[i].Advance())
            {
                lightningBolts.RemoveAt(i);
            }
        }
    }

    private void CreateActivateOrbParticle(Vector3 position)
    {
        var orb = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnerActivateAnimation,
            position,
            this.map.transform
        );
        orb.spriteRenderer.sortingLayerName = "Items";
        orb.spriteRenderer.sortingOrder = 1;
    }

    private void CreateRespawnOrbParticle(Vector3 position)
    {
        var bolt = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnerRespawnOrbAnimation,
            position,
            this.map.transform
        );
        bolt.spriteRenderer.sortingLayerName = "Enemies";
        bolt.spriteRenderer.sortingOrder = -10;
    }

    private void CreateRespawnOrbLightningParticle(Vector3 position)
    {
        var bolt = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnerRespawnOrbLightningBoltsAnimation,
            position,
            this.map.transform
        );
    
        bolt.spriteRenderer.sortingLayerName = "Items (Front)";
        bolt.spriteRenderer.sortingOrder = 3;
    }

    private void RespawnEnemy(ProtectedEnemy protectedEnemy)
    {
        var prefab = Assets.instance.GetPrefabByTileName(protectedEnemy.definition.tile.tile);
        if (prefab == null || prefab.GetComponent<Item>() == null)
        {
            return;
        }
        GameObject obj = Instantiate(prefab, this.chunk.transform);

        var item = obj.GetComponent<Item>();
        item.Init(protectedEnemy.definition);
        this.chunk.items.Add(item);

        protectedEnemy.enemyAndSpawnedThings.Clear();
        protectedEnemy.enemyAndSpawnedThings.Add((Enemy)item);
        protectedEnemy.definition = item.ItemDef;
        protectedEnemy.delayCnt = 0;

        this.map.player.petYellow?.IgnoreEnemy((Enemy)item);

        this.chunk.timeToRemainActive = 30;
    }

    public void AddSpawnedEnemy(Enemy parentEnemy, Enemy spawnedEnemy)
    {
        for (int i = 0; i < enemiesToBeProtected.Count; i++)
        {
            if(enemiesToBeProtected[i].enemyAndSpawnedThings.Find(x => x == parentEnemy) != null)
            {
                enemiesToBeProtected[i].enemyAndSpawnedThings.Add(spawnedEnemy);
            }
        }
    }
}
