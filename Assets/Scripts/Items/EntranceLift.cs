using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EntranceLift : Item
{
    [Header("Exterior")]
    public Animated exteriorAnimated;
    public Animated.Animation animationExteriorIdle;
    public Animated.Animation animationExteriorOpen;
    public Animated.Animation animationExteriorClose;
    public Animated.Animation animationDustLandParticle;

    //private bool isPlayerInside = false;
    private bool isGoingAway = false;
    private bool reachFinalPosition = false;
    private bool emerging = false;
    [NonSerialized] public bool atTrophy = false;

    private int goingAwayMovingTimer;
    private Vector2 finalPosition;
    private Vector2 initialPosition;
    private const int GoingAwayMovingTime = 30;
    private static Vector2 GoAwayOffset = new Vector2(0f, -5.5f);
    private Transform lift;

    [NonSerialized] public Checkpoint checkpoint;
    [NonSerialized] public Trophy trophy;

    [NonSerialized] public Layer layer = Layer.A;
    private string SortingLayerForDust() =>
        (this.layer == Layer.A) ? "Player Dust (AL)" : "Player Dust (BL)";

    public override void Init(Def def)
    {
        base.Init(def);

        this.lift = this.transform.Find("Lift Sprites").GetComponent<Transform>();
        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);
    }

    public static EntranceLift Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.entranceLift, chunk.transform);
        obj.name = "Spawned Entrance Lift";
        var item = obj.GetComponent<EntranceLift>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;
    }

    public override void Advance()
    {
        Player player = this.map.player;
        Vector2 liftPosition = lift.position;
        if (player.state == Player.State.InsideLift && player.InsideEntranceLift == this)
        {
            player.position = liftPosition.Add(0f, 0.95f);
        }

        if (this.goingAwayMovingTimer > 0)
        {
            this.goingAwayMovingTimer -= 1;
        }

        if (this.reachFinalPosition == false)
        {
            if (this.isGoingAway == true)
            {
                if (this.goingAwayMovingTimer == 0)
                {
                    liftPosition.y = Util.Slide(liftPosition.y, this.finalPosition.y, 0.05f);
                    this.lift.position = liftPosition;
                    if (Vector2.Distance(lift.position, this.finalPosition) < 0.01f)
                    {
                        ReachFinalPosition();
                    }
                    SpawnDustParticles();
                }
            }
        }
        else if (this.emerging == true)
        {
            liftPosition.y = Util.Slide(liftPosition.y, this.initialPosition.y, 0.05f);

            this.lift.position = liftPosition;

            if (Vector2.Distance(lift.position, this.initialPosition) < 0.01f)
            {
                ReachInitialPosition();
            }
            SpawnDustParticles();
        }

        this.finalPosition = new Vector2(this.transform.position.x, this.transform.position.y) + GoAwayOffset;
        if (checkpoint != null)
        {
            this.transform.position = checkpoint.position.Add(0, 1) + new Vector2(0, -0.9375f);
        }
    }

    public void RefreshTargetPosition()
    {
        this.transform.position = this.map.player.position;
        this.transform.position -= new Vector3(0,0.9375f,5);
        this.finalPosition = new Vector2(this.transform.position.x, this.transform.position.y) + GoAwayOffset;
        this.map.player.position = this.position;
        this.map.player.position.y = this.position.y + 0.9375f;
        this.initialPosition = this.transform.position;
        this.lift.position = this.finalPosition;
        ComeUp();
    }

    public void ComeUp()
    {
        Player player = this.map.player;
        player.state = Player.State.InsideLift;
        player.InsideEntranceLift = this;
        player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        if(player is PlayerKing)
        {
            (player as PlayerKing).hatSpriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }

        if (checkpoint != null)
        {
            lift.Find("Lift Exterior").GetComponent<SpriteRenderer>().sortingLayerName = SortingLayerForDust();
            lift.Find("Lift Exterior").GetComponent<SpriteRenderer>().sortingOrder = 10;
            lift.Find("Lift Interior").GetComponent<SpriteRenderer>().sortingLayerName = SortingLayerForDust();
            lift.Find("Lift Interior").GetComponent<SpriteRenderer>().sortingOrder = 7;
            lift.Find("Lift Interior/Light").GetComponent<SpriteRenderer>().sortingLayerName = SortingLayerForDust();
            lift.Find("Lift Interior/Light").GetComponent<SpriteRenderer>().sortingOrder = 8;
        }
        this.reachFinalPosition = true;

        this.emerging = true;
        this.isGoingAway = false;

        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleDig);
    }

    private void ReachInitialPosition()
    {
        if (this.emerging == false) return;
        this.emerging = false;
        StartCoroutine(OpenLift(.2f));
    }

    IEnumerator OpenLift(float duration)
    {
        int timer = 0;
        while(timer< duration * 60)
        {
            timer++;
            yield return null;
        }
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleOpen);
        this.exteriorAnimated.PlayOnce(this.animationExteriorOpen, ExitPlayer);
    }

    private void ExitPlayer()
    {
        Player player = this.map.player;
        if (player.state == Player.State.InsideLift && player.InsideEntranceLift == this)
        {
            player.state = Player.State.Normal;
            player.InsideEntranceLift = null;
            player.position.y = this.lift.position.y + 0.9375f;
            player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
            if (player is PlayerKing)
            {
                (player as PlayerKing).hatSpriteRenderer.maskInteraction = SpriteMaskInteraction.None;
            }
        }
        StartCoroutine(CloseLift(.5f));
    }

    IEnumerator CloseLift(float duration)
    {
        int timer = 0;
        while (timer < duration * 60)
        {
            timer++;
            yield return null;
        }
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleClose);
        this.reachFinalPosition = false;
        this.exteriorAnimated.PlayOnce(this.animationExteriorClose, GoAway);
    }

    private void GoAway()
    {
        if (this.isGoingAway == true) return;
        this.isGoingAway = true;
        this.goingAwayMovingTimer = GoingAwayMovingTime;
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleDig);
    }

    private void ReachFinalPosition()
    {
        if (this.reachFinalPosition == true) return;
        this.reachFinalPosition = true;

        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);
        if (this.atTrophy == true)
        {
            this.trophy.inEntranceLift = false;
        }
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }

    public void SpawnDustParticles()
    {
        for (var n = 0; n < 2; n += 1)
        {
            float horizontalSpeed =
                ((n % 2 == 0) ? 1 : -1) * UnityEngine.Random.Range(0.4f, 2.3f) / 16;
            float verticalSpeed = UnityEngine.Random.Range(-0.2f, 0.2f) / 16;

            var p = Particle.CreateAndPlayOnce(
                this.animationDustLandParticle,
                this.transform.position + new Vector3(UnityEngine.Random.Range(-1.5f, 1.5f), 0, 0),
                this.transform.parent
            );
            var tangent = new Vector2(
                Mathf.Cos(1 * Mathf.PI / 180),
                Mathf.Sin(1 * Mathf.PI / 180)
            );
            var normal = new Vector2(-tangent.y, tangent.x);
            p.velocity = (tangent * horizontalSpeed) + (normal * verticalSpeed);
            p.acceleration = p.velocity * -0.05f / 16;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
            if(checkpoint == null)
            {
                p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                p.spriteRenderer.sortingOrder = 25;
            }
            else
            {
                p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                p.spriteRenderer.sortingOrder = 100;

            }
        }
    }
}
