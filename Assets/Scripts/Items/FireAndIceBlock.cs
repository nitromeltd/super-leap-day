
using UnityEngine;
using System.Linq;

public class FireAndIceBlock : Item
{
    public Animated.Animation offAnimation;
    public Animated.Animation fireLightAnimation;
    public Animated.Animation iceLightAnimation;
    public Animated.Animation fireAnimation;

    private Animated[] up;
    private Animated[] right;
    private Animated[] down;
    private Animated[] left;
    private Animated[] all;
    private Hitbox hitboxUp;
    private Hitbox hitboxRight;
    private Hitbox hitboxDown;
    private Hitbox hitboxLeft;

    private bool activated;
    private Animated[] side;
    private int delayBeforeLighting;
    private int lightTime;

    public override void Init(Def def)
    {
        base.Init(def);

        this.up = new [] {
            this.transform.Find("up 1").GetComponent<Animated>()
        };
        this.right = new [] {
            this.transform.Find("right 1").GetComponent<Animated>()
        };
        this.down = new [] {
            this.transform.Find("down 1").GetComponent<Animated>()
        };
        this.left = new [] {
            this.transform.Find("left 1").GetComponent<Animated>()
        };

        this.all = new [] { this.up, this.right, this.down, this.left }
            .SelectMany(a => a).ToArray();

        foreach (var a in this.all) a.gameObject.SetActive(false);

        float halfSize = Tile.Size / 2f;

        this.hitboxes = new [] {
            new Hitbox(-halfSize, halfSize, -halfSize, halfSize, this.rotation, Hitbox.Solid, true, true)
        };

        float flameHeight = 1f;
        float flameWidth = 1f;

        float halfFlameHeight = flameHeight / 2f;
        float halfFlameWidth = flameWidth / 2f;

        Hitbox Rect(float xMin, float xMax, float yMin, float yMax) =>
            new Hitbox(xMin, xMax, yMin, yMax, this.rotation, Hitbox.NonSolid);
        this.hitboxUp    = Rect( -halfFlameWidth, halfFlameWidth, halfSize, halfSize + halfFlameHeight);
        this.hitboxRight = Rect(halfSize, halfSize + halfFlameHeight, -halfFlameWidth, halfFlameWidth);
        this.hitboxDown  = Rect(-halfFlameWidth,  halfFlameWidth, -halfSize,  -(halfSize + halfFlameHeight));
        this.hitboxLeft  = Rect(-(halfSize + halfFlameHeight),  -halfSize, -halfFlameWidth, halfFlameWidth);

        this.activated = false;

        this.onCollisionFromAbove = delegate { OnCollision(this.up);    };
        this.onCollisionFromBelow = delegate { OnCollision(this.down);  };
        this.onCollisionFromLeft  = delegate { OnCollision(this.left);  };
        this.onCollisionFromRight = delegate { OnCollision(this.right); };

        this.animated.PlayOnce(this.offAnimation);
    }
    

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(this.map.currentTemperature == Map.Temperature.Cold)
        {

        }

        if(this.map.currentTemperature == Map.Temperature.Hot)
        {

        }
    }

    public override void Advance()
    {
        if (this.activated == true)
        {
            if (this.delayBeforeLighting > 0)
            {
                this.delayBeforeLighting -= 1;
                if (this.delayBeforeLighting == 0)
                {
                    this.animated.PlayAndHoldLastFrame(this.fireLightAnimation);
                    foreach (var a in this.side)
                    {
                        a.gameObject.SetActive(true);
                        a.PlayAndLoop(this.fireAnimation);
                    }
                }
            }
            else if (this.lightTime > 0)
            {
                this.lightTime -= 1;
                if (this.lightTime == 0)
                {
                    this.animated.PlayAndHoldLastFrame(this.offAnimation);
                    this.activated = false;
                    foreach (var a in this.side)
                    {
                        a.gameObject.SetActive(false);
                    }
                }
                else
                {
                    Rect? hitRectWorld = null;
                    if (this.side == this.up)
                        hitRectWorld = this.hitboxUp.InWorldSpace();
                    if (this.side == this.right)
                        hitRectWorld = this.hitboxRight.InWorldSpace();
                    if (this.side == this.down)
                        hitRectWorld = this.hitboxDown.InWorldSpace();
                    if (this.side == this.left)
                        hitRectWorld = this.hitboxLeft.InWorldSpace();

                    if (hitRectWorld.HasValue == true &&
                        hitRectWorld.Value.Overlaps(this.map.player.Rectangle()))
                    {
                        this.map.player.Hit(hitRectWorld.Value.center);
                    }
                }
            }
        }
    }

    private void OnCollision(Animated[] side)
    {
        if (this.activated == false)
        {
            this.activated = true;
            this.side = side;
            this.delayBeforeLighting = 20;
            this.lightTime = 30;
        }
    }
}
