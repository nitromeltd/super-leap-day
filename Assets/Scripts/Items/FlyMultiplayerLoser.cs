using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyMultiplayerLoser : Item
{
    public Animated.Animation animationIdle;

    private Vector3 movePosition;
    private Vector3 meetPosition;
    private bool facingRight = false;
    private int moveTimer = 0;
    private int currentMoveTime = DefaultTimeMove;

    private const int DefaultTimeMove = 150;

    [HideInInspector] public struct Data
    {
        public Transform spawnPoint;
        public Transform movePoint;
        public Transform meetPoint;
        public bool faceRight;

        public Data(Transform spawnPoint, Transform movePoint,
            Transform meetPoint, bool faceRight)
        {
            this.spawnPoint = spawnPoint;
            this.movePoint = movePoint;
            this.meetPoint = meetPoint;
            this.faceRight = faceRight;
        }
    }
    
    public static FlyMultiplayerLoser Create(Vector2 position, Chunk chunk, Data flyData)
    {
        var obj = Instantiate(Assets.instance.flyMultiplayerLoser, chunk.transform);
        obj.name = "Fly Multiplayer Loser";
        var item = obj.GetComponent<FlyMultiplayerLoser>();
        item.Init(position, chunk, flyData);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, Data flyData)
    {
        base.Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;

        this.movePosition = flyData.movePoint.position;
        this.meetPosition = flyData.meetPoint.position;
        this.facingRight = flyData.faceRight;
        this.moveTimer = currentMoveTime - Random.Range(0, 60);
        
        this.animated.PlayAndLoop(this.animationIdle);
    }
    
    public override void Advance()
    {
        if(this.moveTimer < this.currentMoveTime)
        {
            this.moveTimer += 1;
        }
        else
        {
            Vector2 targetPosition = this.movePosition;
            Vector2 heading = targetPosition - this.position;
            float distance = heading.magnitude;
            Vector3 direction = heading / distance;

            this.facingRight = direction.x > 0f;

            float moveSpeed = 0.20f;
            this.position.x = Util.Slide(this.position.x, targetPosition.x, moveSpeed);
            this.position.y = Util.Slide(this.position.y, targetPosition.y, moveSpeed);

            if(Vector2.Distance(this.position, targetPosition) < moveSpeed)
            {
                this.moveTimer = 0;
                this.currentMoveTime = DefaultTimeMove + Random.Range(-30, 30);
                this.movePosition = new Vector3(
                    this.meetPosition.x + Random.Range(-3.50f, 3.50f),
                    this.meetPosition.y + Random.Range(-1.50f, 1.50f)
                );
            }
        }

        this.transform.position = this.position;

        float scale = this.facingRight == true ? -1f : 1f;
        this.transform.localScale = new Vector3(scale, 1f, 1f);
    }
}
