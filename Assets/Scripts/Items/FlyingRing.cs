using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingRing : Item
{
    public enum Aim
    {
        Vertical,
        Angled,
        Horizontal
    }

    public Aim aim;
    public SpriteRenderer[] ringSRs;
    public Animated frontRingAnimated;
    public Animated backRingAnimated;
    public Animated.Animation animationAppear;
    public Animated.Animation animationDissapear;
    public Animated.Animation animationEmpty;
    public Animated.Animation animationFrontRing;
    public Animated.Animation animationBackRing;
    public Transform angledRingTransform;

    private bool isVisible = true;
    private Vector2 velocity;
    private NitromeEditor.Path path;
    private float breatheTimer;
    private Vector2 originPosition;
    private Vector2 targetBreathePosition;
    private float offsetThroughPath;
    private int activatedTimer = 0;
    private Vector3 smoothScaleVelocity;
    private Color initialColor;
    private FlyingRingSequence sequenceController;
    private float xScaleValue;
    private SpriteRenderer[] spriteRenderers;

    private const int ActivateTime = 30;
    private const float BreathePeriod = 0.50f;
    private const float BreatheAmplitude = 0.25f;

    private static Vector2 HitboxSize = new Vector2(0.50f, 0.50f);

    private Rect UpperHitbox => this.hitboxes[0].InWorldSpace();
    private Rect LowerHitbox => this.hitboxes[1].InWorldSpace();
    public bool IsAngled => this.aim == Aim.Angled;
    public Vector2 CurrentAngledDirection =>
        (this.angledRingTransform != null) ? (Vector2)this.angledRingTransform.up : Vector2.zero;
    public float CurrentAngledValue => Vector2.Angle(CurrentAngledDirection, Vector2.up) * this.xScaleValue;
    public Vector3 TargetScale => new Vector3(this.xScaleValue, 1f, 1f);

    private void Awake()
    {
        this.spriteRenderers = GetComponentsInChildren<SpriteRenderer>();
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.originPosition = this.position;

        Vector2 hb1Pos = Vector2.up * 0.75f;
        Vector2 hb2Pos = Vector2.down * 0.75f;

        Vector2 hitboxSize = HitboxSize;

        if(this.aim == Aim.Angled)
        {
            hitboxSize = new Vector2(HitboxSize.x * 1.5f, HitboxSize.y);
        }
        
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(
                hb1Pos.x - hitboxSize.x,
                hb1Pos.x + hitboxSize.x,
                hb1Pos.y - hitboxSize.y,
                hb1Pos.y + hitboxSize.y,
                0, Hitbox.NonSolid),

            new Hitbox(
                hb2Pos.x - hitboxSize.x,
                hb2Pos.x + hitboxSize.x,
                hb2Pos.y - hitboxSize.y,
                hb2Pos.y + hitboxSize.y,
                0, Hitbox.NonSolid),
        };
        
        this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty);

        this.path = chunk.pathLayer.NearestPathTo(this.position, 2);

        if(this.path != null)
        {
            this.offsetThroughPath = Chest.TimeNearestToPoint(this.path, this.position);
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }

        this.initialColor = this.ringSRs[0].color;

        this.xScaleValue = def.tile.flip == true ? -1f : 1f;
        this.transform.localScale = TargetScale;

        PlayLoopAnimation();

        if(this.rotation == 90 || this.rotation == 270)
        {
            this.aim = Aim.Horizontal;
        }
    }

    public override void Advance()
    {
        if(this.isVisible == false) return;

        base.Advance();
        
        if (this.path != null)
        {
            this.originPosition = this.path.PointAtTime(
                (Map.instance.frameNumber / 60.0f) + this.offsetThroughPath
            ).position;
        }

        BreatheMovement();
        CheckForPlayer();

        if(this.activatedTimer > 0)
        {
            this.activatedTimer -= 1;
        }

        this.position += this.velocity;
        this.transform.position = this.position;

        this.transform.localScale = Vector3.SmoothDamp(
            this.transform.localScale, TargetScale, ref smoothScaleVelocity, 0.50f);

        foreach(var sr in this.ringSRs)
        {
            sr.color = Color.Lerp(sr.color, this.initialColor, 0.10f);
        }
    }

    private void BreatheMovement()
    {
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);
        
        this.targetBreathePosition = this.originPosition + (Vector2)this.transform.up * distance;
        
        Vector2 targetVel = (this.targetBreathePosition - this.position) * .05f;

        this.velocity *= 0.95f;
        this.velocity += targetVel * .2f;
    }

    private void CheckForPlayer()
    {
        Rect playerRect = Map.instance.player.Rectangle();

        if(playerRect.Overlaps(LowerHitbox) && playerRect.Overlaps(UpperHitbox))
        {
            ActivateRing();
        }
    }

    // go through ring
    private void ActivateRing()
    {
        if(this.activatedTimer > 0) return;

        this.activatedTimer = ActivateTime;

        if(this.aim != Aim.Angled)
        {
            ChangeScale(new Vector3(1.3f, 0.7f, 1f));
        }

        foreach(var sr in this.ringSRs)
        {
            sr.color = Color.yellow;
        }

        Boost();

        if(this.sequenceController != null)
        {
            this.sequenceController.RingActivated(this);
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyFlyingRing, 
            position: this.position, 
            options: new Audio.Options(1, false, 0)
        );
    }

    private void ChangeScale(Vector3 newScale)
    {
        this.transform.localScale = new Vector3(
            this.xScaleValue * newScale.x,
            newScale.y,
            newScale.z
        );
    }

    private void Boost()
    {
        var player = Map.instance.player;
        player.position = this.position;
        player.CancelLateJumpTime();
        player.ResetDoubleJump();
        
        player.velocity = VelocityAfterSpringing(player.velocity);
        player.state = Player.State.BoostFlyingRing;

        if(this.aim == Aim.Horizontal)
        {
            player.noGravityTimeWhenBoostFromFlyingRing = 20;
        }
    }
    
    private Vector2 VelocityAfterSpringing(Vector2 before)
    {
        float GetStrength(bool isDirectlyAgainstGravity)
        {
            return isDirectlyAgainstGravity ? 1f :  0.56f;
        }

        bool movingUpwards = before.y > 0f;
        //bool sameDirection = Mathf.Sign(CurrentAngledDirection.x) == Mathf.Sign(before.x);

        if(this.aim == Aim.Vertical)
        {
            if(movingUpwards)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2(0f,  GetStrength(true));
                    case 90:   return new Vector2(-GetStrength(false), 0f);
                    case 180:  return new Vector2(0f,  -GetStrength(false));
                    case 270:  return new Vector2(GetStrength(false), 0f);
                }
            }
            else
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2(0f,  -GetStrength(false));
                    case 90:   return new Vector2(GetStrength(false), 0f);
                    case 180:  return new Vector2(0f,  GetStrength(true));
                    case 270:  return new Vector2(-GetStrength(false), 0f);
                }
            }
        }
        else if(this.aim == Aim.Horizontal)
        {
            switch (this.rotation)
            {
                case 90:   return new Vector2(-GetStrength(false), 0f);
                case 270:  return new Vector2(GetStrength(false), 0f);
            }
        }
        else // Angled
        {
            bool toTheRight = this.map.player.position.x > this.position.x;
            bool toTheBottom = this.map.player.position.y < this.position.y;
            bool onBottomLeft = toTheBottom == true && toTheRight == false;
            bool onBottomRight = toTheBottom == true && toTheRight == true;
            bool alwaysUpwards = (CurrentAngledDirection.x > 0f && onBottomLeft == true) ||
                (CurrentAngledDirection.x < 0f && onBottomRight == true);

            if(movingUpwards == true || alwaysUpwards == true)
            {
                return CurrentAngledDirection * GetStrength(true);
            }
            else
            {
                return CurrentAngledDirection * -GetStrength(true);
            }
        }

        return before;
    }

    public void SequenceController(FlyingRingSequence sequenceController)
    {
        this.sequenceController = sequenceController;
    }

    public void Show()
    {
        this.isVisible = true;
        this.gameObject.SetActive(true);

        this.frontRingAnimated.PlayOnce(this.animationAppear, PlayLoopAnimation);
        this.backRingAnimated.PlayAndLoop(this.animationEmpty);
    }

    public void Hide()
    {
        this.isVisible = false;
        
        this.frontRingAnimated.PlayOnce(this.animationDissapear, () => 
        {
            this.gameObject.SetActive(false);
        });
        this.backRingAnimated.PlayAndLoop(this.animationEmpty);
    }

    private void PlayLoopAnimation()
    {
        this.frontRingAnimated.PlayAndLoop(this.animationFrontRing);
        this.backRingAnimated.PlayAndLoop(this.animationBackRing);
    }
}
