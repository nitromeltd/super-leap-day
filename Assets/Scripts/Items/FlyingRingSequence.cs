using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingRingSequence : Item
{
    private List<FlyingRing> flyingRings = new List<FlyingRing>();
    private int activeRingIndex = 0;
    private Pickup rewardPickup;
    private Chest rewardChest;
    private int resetTimer = 0;

    private const int ResetTimeAfterActivate = 60 * 5;

    private FlyingRing FirstRing => this.flyingRings[0];
    private FlyingRing ActiveRing => this.flyingRings[this.activeRingIndex];

    private bool IsRewardDone()
    {
        if(this.rewardPickup != null)
        {
            return this.rewardPickup.collected == true;
        }

        if(this.rewardChest != null)
        {
            return this.rewardChest.Opened == true;
        }

        return false;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        var nearestConnectionNode =
            this.chunk.connectionLayer.NearestNodeTo(this.position, 2f);

        if(nearestConnectionNode != null)
        {
            foreach(var n in nearestConnectionNode.path.nodes)
            {
                FlyingRing flyingRing = this.chunk.NearestItemTo<FlyingRing>(n.Position, 2f);

                if(flyingRing != null)
                {
                    this.flyingRings.Add(flyingRing);

                    if(flyingRing != FirstRing)
                    {
                        flyingRing.Hide();
                    }
                    flyingRing.SequenceController(this);
                }
            }

            NitromeEditor.Path.Node lastNode = 
                nearestConnectionNode.path.nodes[nearestConnectionNode.path.nodes.Length - 1];
            this.rewardChest = this.chunk.NearestItemTo<Chest>(lastNode.Position, 2f);
            this.rewardPickup = this.chunk.NearestItemTo<Pickup>(lastNode.Position, 2f);
            ToggleActivateReward(false, false);
            
            FirstRing.Show();
        }
    }

    public override void Advance()
    {
        base.Advance();

        if(this.resetTimer > 0)
        {
            this.resetTimer -= 1;

            if(this.resetTimer == 0)
            {
                ResetSequence(resetFromPlayerDeath: false);
            }
        }
    }

    private void Next()
    {
        if(this.activeRingIndex < this.flyingRings.Count - 1)
        {
            ActivateNextRing();
        }
        else
        {
            CompleteSequence();
        }
    }

    private void ActivateNextRing()
    {
        this.resetTimer = ResetTimeAfterActivate;
        this.activeRingIndex += 1;
        ActiveRing.Show();
    }

    private void CompleteSequence()
    {
        this.resetTimer = ResetTimeAfterActivate;
        ToggleActivateReward(true, false);
    }

    public void RingActivated(FlyingRing ring)
    {
        ring.Hide();
        Next();
    }

    public override void Reset()
    {
        base.Reset();

        ResetSequence(resetFromPlayerDeath: true);
    }

    private void ResetSequence(bool resetFromPlayerDeath)
    {
        this.activeRingIndex = 0;

        foreach(var r in this.flyingRings)
        {
            r.Hide();
        }

        ToggleActivateReward(false, resetFromPlayerDeath);
        FirstRing.Show();
    }

    private void ToggleActivateReward(bool activate, bool resetFromPlayerDeath)
    {
        if(IsRewardDone())
        {
            if(resetFromPlayerDeath == true &&
                this.rewardChest != null &&
                this.rewardChest.prize == Chest.Prize.KEY)
            {
                this.rewardChest.CloseFromFlyingRingSequence();
            }

            return;
        }
        
        if(this.rewardChest != null)
        {
            if(activate == true)
            {
                this.rewardChest.Show();
            }
            else
            {
                this.rewardChest.Hide();
            }
        }
        else if(this.rewardPickup != null)
        {
            this.rewardPickup.gameObject.SetActive(activate);
        }
    }
}
