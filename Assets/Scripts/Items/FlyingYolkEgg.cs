using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingYolkEgg : Item
{
    public AnimationCurve scaleCurve;

    [Header("Wings")]
    public Animated wingRightAnimated;
    public Animated wingLeftAnimated;
    public Animated.Animation animationWing;

    [HideInInspector] public bool canRespawnPlayer = false;
    private bool spawning = true;
    private float currentScale = 0f;
    private int respawningTimer = 0;
    private float breatheTimer = 0;
    private Vector2 basePosition;
    private Vector2 breatheDirection;
    private Vector2 targetBreatheDirection;

    private const float MoveSpeed = 0.175f;
    private const int ScaleTime = 20;
    private const float BreathePeriod = .25f;
    private const float BreatheAmplitude = 0.50f;

    private bool hasPlayedSpawnSfx = false;
    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.position = position;
        this.basePosition = position;
        this.transform.position = position;
        this.chunk = chunk;
        
        this.wingRightAnimated.PlayAndLoop(animationWing);
        this.wingLeftAnimated.PlayAndLoop(animationWing);
        this.breatheDirection = this.targetBreatheDirection = Vector2.one;

        this.transform.localScale = Vector3.zero;

        this.hasPlayedSpawnSfx = false;
    }

    public override void Advance()
    {
        if(this.gameObject.activeSelf == false) return;
        SwitchToNearestChunkWithLocalPosition();

        if(this.map.frameNumber % 2 == 0)
        {
            float sparkleRadius = 1;
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                this.position + (UnityEngine.Random.insideUnitCircle * sparkleRadius),
                this.transform.parent
            );

            bool front = Random.value > 0.75f;
            sparkle.spriteRenderer.sortingLayerName = front ? "Items (Front)" : "Items";
            sparkle.spriteRenderer.sortingOrder = front ? 11 : 1;
        }

        if(this.spawning == true)
        {
            Spawning();
            return;
        }

        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);

        MoveBackToCheckpoint();

        this.breatheDirection.x = Util.Slide(this.breatheDirection.x, this.targetBreatheDirection.x, 0.02f);
        this.breatheDirection.y = Util.Slide(this.breatheDirection.y, this.targetBreatheDirection.y, 0.02f);

        this.position = this.basePosition + (this.breatheDirection * distance);
        this.transform.position = this.position;
    }
    
    public void SwitchToNearestChunkWithLocalPosition()
    {
        var nearest = this.map.NearestChunkTo(this.basePosition);
        if (nearest != this.chunk)
        {
            this.chunk.items.Remove(this);
            nearest.items.Add(this);
            this.chunk = nearest;
            this.group = null;
            this.transform.SetParent(this.chunk.transform);
        }
    }

    private void Spawning()
    {
        this.respawningTimer += 1;

        if(this.respawningTimer > 30)
        {
            float t = (float)(respawningTimer - 30) / (float)ScaleTime;
            this.currentScale = Mathf.Lerp(0f, 2f, this.scaleCurve.Evaluate(t));
            this.transform.localScale = Vector3.one * currentScale;
            if (this.hasPlayedSpawnSfx == false)
            {
                this.hasPlayedSpawnSfx = true;
                Audio.instance.PlaySfx(Assets.instance.sfxYolkEggRespawnGrow);
                Audio.instance.PlaySfxLoop(Assets.instance.sfxYolkEggRespawnSparkle);
                Audio.instance.PlaySfxLoop(Assets.instance.sfxYolkEggRespawnFlap);
            }
        }

        if(this.respawningTimer > 30 + ScaleTime)
        {
            this.spawning = false;
            this.canRespawnPlayer = true;
        }
    }

    private void MoveBackToCheckpoint()
    {
        bool reachEndPosition = Vector2.Distance(this.basePosition, GetFinalPosition()) < 0.10f;

        if(reachEndPosition == true)
        {
            this.targetBreatheDirection = Vector2.up;
        }
        else
        {
            Vector2 currentTarget = GetCurrentTarget();
            Vector2 heading = currentTarget - this.basePosition;
            float distance = heading.magnitude;
            Vector2 direction = heading / distance;      

            this.basePosition += direction * MoveSpeed;
        }
    }

    private Vector2 GetCurrentTarget()
    {
        Chunk checkpointChunk =
            this.map.NearestChunkTo(GetFinalPosition());

        if(checkpointChunk == this.chunk)
        {
            return GetFinalPosition();
        }
        else
        {
            bool isCheckpointAhead = checkpointChunk.index > this.chunk.index;

            XY xyTarget = isCheckpointAhead ?
                this.chunk.exit.mapLocation : this.chunk.entry.mapLocation;

            Vector2 posTarget = new Vector2(xyTarget.x, xyTarget.y);
            return posTarget + GetExtraDirection(posTarget, isCheckpointAhead);
        }
    }

    private Vector2 GetExtraDirection(Vector2 currentPosTarget, bool isCheckpointAhead)
    {
        float extraValue = 1f;

        int nextChunkIndex = this.chunk.index + (isCheckpointAhead ? 1 : -1);

        if(nextChunkIndex >= 0 && nextChunkIndex < this.map.chunks.Count)
        {
            Chunk nextChunk = this.map.chunks[nextChunkIndex];
            Vector2 nextChunkCenter = new Rect(
                nextChunk.xMin,
                nextChunk.yMin,
                nextChunk.xMax - nextChunk.xMin,
                nextChunk.yMax - nextChunk.yMin
            ).center;

            Vector2 heading = nextChunkCenter - currentPosTarget;
            float distance = heading.magnitude;
            Vector2 direction = heading / distance;    

            return direction * extraValue;
        }
        else
        {
            return Vector2.zero;
        }
    }

    private Vector2 GetFinalPosition()
    {
        return this.map.player.checkpointPosition + (Vector2.up * 2f);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}
