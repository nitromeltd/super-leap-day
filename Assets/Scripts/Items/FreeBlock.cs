
using UnityEngine;

public class FreeBlock : Item
{
    private Vector2 velocity;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-1.5f, 1.5f, -1.5f, 1.5f, this.rotation, Hitbox.Solid)
        };
        this.velocity = Vector2.zero;
    }

    public override void Advance()
    {
        this.velocity.y -= 0.09375f / 16;
        Integrate();
    }

    public void Integrate()
    {
        var conveyorSpeed = 0.0f;

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);

        if (this.velocity.y < 0)
        {
            var r = Raycast.CombineClosest(
                raycast.Vertical(this.position.Add(-1.4f, -1.5f), false, -this.velocity.y),
                raycast.Vertical(this.position.Add( 0.7f, -1.5f), false, -this.velocity.y),
                raycast.Vertical(this.position.Add(    0, -1.5f), false, -this.velocity.y),
                raycast.Vertical(this.position.Add( 0.7f, -1.5f), false, -this.velocity.y),
                raycast.Vertical(this.position.Add( 1.4f, -1.5f), false, -this.velocity.y)
            );
            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.velocity.y *= -0.2f;

                foreach (var i in r.items)
                {
                    var conveyor = i as Conveyor;
                    if (conveyor != null)
                        conveyorSpeed = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed; //-1
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y > 0)
        {
            var r = Raycast.CombineClosest(
                raycast.Vertical(this.position.Add(-1.4f, 1.5f), true, this.velocity.y),
                raycast.Vertical(this.position.Add(-0.7f, 1.5f), true, this.velocity.y),
                raycast.Vertical(this.position.Add(    0, 1.5f), true, this.velocity.y),
                raycast.Vertical(this.position.Add( 0.7f, 1.5f), true, this.velocity.y),
                raycast.Vertical(this.position.Add( 1.4f, 1.5f), true, this.velocity.y)
            );
            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity.y *= -0.2f;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }

        if (this.velocity.x + conveyorSpeed < 0)
        {
            Raycast.Result Left(Vector2 pos) => raycast.Horizontal(
                pos,
                false,
                -this.velocity.x + conveyorSpeed
            );
            var r = Raycast.CombineClosest(
                Left(this.position + new Vector2(-1.5f, -1.4f)),
                Left(this.position + new Vector2(-1.5f, -0.7f)),
                Left(this.position + new Vector2(-1.5f,     0)),
                Left(this.position + new Vector2(-1.5f,  0.7f)),
                Left(this.position + new Vector2(-1.5f,  1.4f))
            );
            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity.x *= -0.2f;
            }
            else
            {
                this.position.x += this.velocity.x + conveyorSpeed;
            }
        }
        else if (this.velocity.x + conveyorSpeed > 0)
        {
            Raycast.Result Right(Vector2 pos) => raycast.Horizontal(
                pos,
                false,
                this.velocity.x + conveyorSpeed
            );
            var r = Raycast.CombineClosest(
                Right(this.position + new Vector2(1.5f, -1.4f)),
                Right(this.position + new Vector2(1.5f, -0.7f)),
                Right(this.position + new Vector2(1.5f,     0)),
                Right(this.position + new Vector2(1.5f,  0.7f)),
                Right(this.position + new Vector2(1.5f,  1.4f))
            );
            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity.x *= -0.2f;
            }
            else
            {
                this.position.x += this.velocity.x + conveyorSpeed;
            }
        }

        this.transform.position = this.position;
    }
}
