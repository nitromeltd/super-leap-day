
using UnityEngine;

public class Fruit : Pickup
{
    public Animated.Animation animationCollect2;

    public override void Collect()
    {
        base.Collect();

        this.animated.OnFrame(5, delegate
        {
            Audio.instance.PlaySfx(Assets.instance.sfxFruit, position: this.position, options: new Audio.Options(1,true,0.2f));
        });
        this.animated.OnFrame(8, delegate
        {
            var p = Particle.CreateAndPlayOnce(
                this.animationCollect2, 
                this.position,
                this.transform.parent
            );
        });

        this.map.AddFruit(1);

        this.spriteRenderer.sortingOrder = (this.def.tx % 2) + ((this.def.ty % 2) * 2);
    }

    public override void Advance()
    {
        base.Advance();

        if (this.movingFreely == false &&
            this.collected == false &&
            this.suckActive == false)
        {
            float centerY = this.position.y;
            var theta =
                this.map.frameNumber * 0.08f +
                this.def.tx * (0.5f + Util.Tau / 4) +
                this.def.ty * (0.5f + Util.Tau / 4);
            Vector3 displayPosition = this.position;
            displayPosition.y += Mathf.Sin(theta) * 2.5f / 16;
            displayPosition.z = this.z;
            this.transform.position = displayPosition;
        }
    }
}
