using UnityEngine;
public class FruitFish : Pickup
{
    public Animated.Animation animationFishSwim;
    public Animated.Animation animationFishTurn;
    public Animated.Animation animationChangeToFish;
    public Animated.Animation animationChangeToFruit;
    public Animated.Animation animationCollect2;

    public bool inWater = false;
    public bool facingRight = true;
    public bool isTurning = false;
    public bool isChanging = false;

    private const float SwimSpeed = 0.0625f;
    public bool isMelon = false;

    public override void Init(Def def)
    {
        this.facingRight = !def.tile.flip;
        base.Init(def);
    }

    public override void Collect()
    {
        base.Collect();

        this.animated.OnFrame(5, delegate
        {
            Audio.instance.PlaySfx(Assets.instance.sfxFruit, position: this.position, options: new Audio.Options(1, true, 0.2f));
        });

        this.animated.OnFrame(8, delegate
        {
            var p = Particle.CreateAndPlayOnce(
                this.animationCollect2,
                this.position,
                this.transform.parent
            );
        });

        this.map.AddFruit(1);

        this.spriteRenderer.sortingOrder = (this.def.tx % 2) + ((this.def.ty % 2) * 2);
        velocity = Vector2.zero;
    }

    public override void Advance()
    {
        if (this.collected == true) return;

        CheckCollect();
        AdvanceWater();

        if(this.inWater == true && this.isChanging == false)
        {
            FlyBackAndForth();
        }

        this.position += this.velocity;
        this.transform.position = this.position;

        if (this.isMelon == false)
        {
            if (this.isTurning == false)
            {
                this.transform.localScale = new Vector3((this.facingRight) ? 1 : -1, 1, 1);
            }
            else
            {
                this.transform.localScale = new Vector3((this.facingRight) ? -1 : 1, 1, 1);
            }
        }
        else
        {
            if (this.isTurning == false)
            {
                this.transform.localScale = new Vector3((this.facingRight) ? -1 : 1, 1, 1);
            }
            else
            {
                this.transform.localScale = new Vector3((this.facingRight) ? 1 : -1, 1, 1);
            }
        }
    }

    public override void CheckCollect()
    {
        base.CheckCollect();
    }


    private new void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if (this.inWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            this.inWater = true;
            this.isChanging = true;
            this.animated.PlayOnce(this.animationChangeToFish, () =>
            {
                this.isChanging = false;
                this.animated.PlayAndLoop(this.animationFishSwim);
                this.velocity = SwimSpeed * (this.facingRight ? Vector2.right : Vector2.left);
            });
            var p = Particle.CreateAndPlayOnce(
                this.animationCollect2,
                this.position,
                this.transform.parent
            );
        }

        if (this.inWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            velocity = Vector2.zero;
            this.inWater = false;
            this.isTurning = false;
            this.isChanging = true;
            this.animated.PlayOnce(this.animationChangeToFruit, () =>
            {
                this.isChanging = false;
                this.animated.PlayAndLoop(this.animationNormal);
            });
            this.animated.OnFrame(2, () =>
            {
                var p = Particle.CreateAndPlayOnce(
                    this.animationCollect2,
                    this.position,
                    this.transform.parent
                );
            });
        }
    }

    private void FlyBackAndForth()
    {
        void Turn()
        {
            this.isTurning = true;
            this.velocity.x = 0f;

            this.animated.PlayOnce(this.animationFishTurn, () =>
            {
                this.isTurning = false;
                this.velocity = SwimSpeed * (this.facingRight ? Vector2.right : Vector2.left);
                this.animated.PlayAndLoop(this.animationFishSwim);
                if (this.isMelon == false)
                {
                    this.transform.localScale = new Vector3((this.facingRight) ? 1 : -1, 1, 1);

                }
                else
                {
                    this.transform.localScale = new Vector3((this.facingRight) ? -1 : 1, 1, 1);
                }
            });
        }

        if (this.isTurning == true) return;

        var raycast = new Raycast(this.map, this.position, isCastByEnemy: false);
        float maxDistance = 0.50f;

        if (this.facingRight == true)
        {
            if (raycast.Horizontal(this.position, true, maxDistance).anything == true)
            {
                this.facingRight = false;
                Turn();
            }
        }
        else
        {
            if (raycast.Horizontal(this.position, false, maxDistance).anything == true)
            {
                this.facingRight = true;
                Turn();
            }
        }
    }
}

