﻿using UnityEngine;
using System.Linq;

public class GateBlock : Item
{
    public GateType gateType;
    public Animated.Animation idleAnimation;
    public Animated.Animation fadeAnimation;

    [Header("Block Button")]
    public Animated.Animation animationBlockBtnDebrisLeft;
    public Animated.Animation animationBlockBtnDebrisRight;

    public Animated buttonAnimated;
    public Animated.Animation buttonAnimationUp;
    public Animated.Animation buttonAnimationDown;

    public enum GateType
    {
        LOCK,
        HORIZONTAL,
        VERTICAL,
        CORNER,
        BUTTON_HORIZONTAL,
        BUTTON_VERTICAL
    }

    private bool open;
    private bool opening;
    private int? timeToOpenNeighbours;

    // Button
    private Hitbox buttonSolidRect;
    private bool pressed;

    private const float MIN_PLAYER_DISTANCE_OPEN = 4f;

    public override void Init(Def def)
    {
        base.Init(def);

        this.open = false;
        this.opening = false;

        if (def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);
        }

        if(IsGateTypeButton())
        {
            switch(gateType)
            {
                case GateType.BUTTON_HORIZONTAL:
                    this.buttonSolidRect = new Hitbox(
                        this.buttonAnimated.transform.position.x - 0.25f,
                        this.buttonAnimated.transform.position.x + 0.25f,
                        this.buttonAnimated.transform.position.y - 0.3f,
                        this.buttonAnimated.transform.position.y + 0.3f,
                        this.rotation,
                        Hitbox.NonSolid, positionType: Hitbox.PositionType.Absolute
                        );
                break;

                case GateType.BUTTON_VERTICAL:
                    this.buttonSolidRect = new Hitbox(
                        this.buttonAnimated.transform.position.x - 0.3f,
                        this.buttonAnimated.transform.position.x + 0.3f,
                        this.buttonAnimated.transform.position.y - 0.25f,
                        this.buttonAnimated.transform.position.y + 0.25f,
                        this.rotation,
                        Hitbox.NonSolid, positionType: Hitbox.PositionType.Absolute
                        );
                break;
            }

            this.pressed = false;
            this.buttonAnimated.PlayAndHoldLastFrame(this.buttonAnimationUp);
        }
        else
        {
            this.buttonSolidRect = new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid);
        }

        MakeSolid();
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private bool IsGateTypeButton()
    {
        return (
            this.gateType == GateType.BUTTON_HORIZONTAL ||
            this.gateType == GateType.BUTTON_VERTICAL
        );
    }

    private void MakeSolid()
    {
        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true),
            this.buttonSolidRect
        };
    }

    public override void Advance()
    {
        if(IsGateTypeButton())
        {
            CheckPressed();
        }

        OpenNeightbours();
        CheckOpen();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
    }
    
    private void CheckPressed()
    {
        if(this.open == true) return;
        
        var buttonRect = this.buttonSolidRect.InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(buttonRect))
        {
            Press();
        }
        else
        {
            foreach (var enemy in this.chunk.subsetOfItemsOfTypeWalkingEnemy)
            {
                if (enemy.hitboxes[0].InWorldSpace().Overlaps(buttonRect))
                {
                    Press();
                    break;
                }
            }
        }
    }

    private void OpenNeightbours()
    {
        if (this.timeToOpenNeighbours.HasValue &&
            this.timeToOpenNeighbours > 0)
        {
            this.timeToOpenNeighbours -= 1;
            if (this.timeToOpenNeighbours == 0)
            {
                foreach (var block in this.chunk.items.OfType<GateBlock>())
                {
                    if (block.open == true) continue;

                    var dx = block.def.tx - this.def.tx;
                    var dy = block.def.ty - this.def.ty;
                    if (dx == 0 && Mathf.Abs(dy) == 1 ||
                        dy == 0 && Mathf.Abs(dx) == 1)
                    {
                        block.Open();
                    }
                }
            }
        }
    }

    private void CheckOpen()
    {
        if(this.open == true) return;

        // Check open for LOCK gate type
        if(this.gateType == GateType.LOCK && this.opening == false)
        {
            float distToPlayer = Vector2.Distance(this.map.player.position, this.position);

            if(distToPlayer < MIN_PLAYER_DISTANCE_OPEN && this.map.player.HaveAvailableKey())
            {
                this.opening = true;
                bool openWithKey = this.map.player.OpenGateWithKey(this);

                if(openWithKey == false)
                {
                    Open();
                }
            }
        }
    }

    private void Press()
    {
        if (this.open == true || this.pressed == true) return;

        this.pressed = true;
        Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);

        Open();
    }


    public void Open()
    {
        if (this.open == true) return;

        this.open = true;

        this.animated.PlayOnce(this.fadeAnimation, () => {
            this.spriteRenderer.enabled = false;
        });

        if(IsGateTypeButton())
        {
            this.buttonAnimated.PlayAndHoldLastFrame(this.buttonAnimationDown);

            // block button particle debris
            var left = Particle.CreatePlayAndHoldLastFrame(
                this.animationBlockBtnDebrisLeft, 100, this.position, this.transform.parent
            );
            left.velocity     = new Vector2(-1f, 7f) / 16f;
            left.acceleration = new Vector2(0f, -0.5f) / 16f;

            var right = Particle.CreatePlayAndHoldLastFrame(
                this.animationBlockBtnDebrisRight, 100, this.position, this.transform.parent
            );
            right.velocity     = new Vector2(1f, 7f) / 16f;
            right.acceleration = new Vector2(0f, -0.5f) / 16f;
        }

        Particle.CreateDust(this.transform.parent, 5, 7,
            this.hitboxes[0].InWorldSpace().center, .5f, .5f);

        this.hitboxes = new Hitbox[0];

        this.timeToOpenNeighbours = 13;
        
        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .3f);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvGateBlockBurst,
            position: this.position
            );
    }
    
    public override void Reset()
    {
        base.Reset();

        if (this.open == true)
        {
            this.open = false;
            this.opening = false;
            this.pressed = false;
            this.timeToOpenNeighbours = null;
            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(idleAnimation);

            MakeSolid();

            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );

            if(IsGateTypeButton())
            {
                this.pressed = false;
                this.buttonAnimated.PlayAndHoldLastFrame(this.buttonAnimationUp);
            }
        }
    }

    public Vector2 GetLockPosition()
    {
        return this.position;
    }
}
