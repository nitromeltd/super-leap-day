﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GateKey : Pickup
{
    public Animated.Animation animationOpenGate;

    private GateBlock targetGate = null;
    private Vector2 smoothVelocity = Vector2.zero;
    private bool used = false;
    private bool openingGate = false;
    private Vector2 initalPosition;
    private bool breatheMovement = true;
    private float breatheTimer;

    private const float FollowSmoothTime = 0.3f;
    private const float OpenGateSpeed = 15f;
    private const float BreathePeriod = .4f;
    private const float BreatheAmplitude = .5f;

    private Chest targetChest = null;
    private bool openingChest = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.position = this.initalPosition = this.transform.position;
        
        this.gravityAcceleration = 0.015f;
        this.gravityAccelerationUnderwater = 0.0075f;
        this.bounciness = -0.5f;
        this.horizontalMomentum = 0.25f;
        
        this.hitboxSize = 0.75f;
        this.hitboxes = new [] {
            new Hitbox(-0.3f, 0.3f, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };
    }

    public static GateKey Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.gateKey, chunk.transform);
        obj.name = "Spawned Key";
        var item = obj.GetComponent<GateKey>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));
        
        this.position = this.initalPosition = position;
        this.transform.position = position;
        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
    }
    
    public override void Advance()
    {
        if(this.used == true) return;

        SwitchToNearestChunk();
        UpdateGravity();
        CheckCollect();

        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;

        Player player = this.map.player;

        if(player.keys.Contains(this))
        {
            theta += .5f * player.keys.IndexOf(this);
        }

        if(this.controlledByOtherItem == true)
        {
            theta = 0f;
        }

        float distance = BreatheAmplitude * Mathf.Sin(theta);
        Vector2 breathePos = this.breatheMovement ? (Vector2)this.transform.up * distance : Vector2.zero;
        
        if(this.collected == true)
        {
            float targetRotation = Util.Slide(this.rotation, 0f, 10f);
            this.rotation = Mathf.RoundToInt(targetRotation);

            if (this.targetGate == null && this.targetChest == null)
            {
                if (CanFollowPlayer() == true)
                {
                    Vector2 followPlayerPos = player.KeyFollowPosition(this);
                    this.position = Vector2.SmoothDamp(this.position, followPlayerPos,
                        ref this.smoothVelocity, FollowSmoothTime);
                }
                else
                {
                    this.position = this.initalPosition;
                }
            }
            else if (this.targetGate != null)
            {
                this.position = Vector2.MoveTowards(
                    this.position, this.targetGate.GetLockPosition(), OpenGateSpeed * Time.deltaTime
                );

                if (Vector2.Distance(this.position, this.targetGate.GetLockPosition()) < .015f)
                {
                    OpenGate();
                }
            }
            else if (this.targetChest != null)
            {
                this.position = Vector2.MoveTowards(
                    this.position, this.targetChest.GetLockPosition(), OpenGateSpeed * Time.deltaTime
                );

                if (Vector2.Distance(this.position, this.targetChest.GetLockPosition()) < .015f)
                {
                    OpenChest();
                }
            }
        }
        else
        {
            if(this.group != null)
            {
                this.position = this.group.FollowerPosition(this);
            }
            else if (this.path != null)
            {
                this.position = this.path.PointAtTime(
                    (this.map.frameNumber / 60.0f) + this.offsetThroughPath
                ).position;
            }

            if(this.movingFreely == true)
            {
                AdvanceGravity();
                AdvanceWater();
                Integrate();
                
                ApplyDragOnFloor();
                breathePos = Vector2.zero;
            }
            
            if(this.suckActive == true)
            {
                this.transform.position = new Vector3(this.position.x, this.position.y, this.z);

                UpdateSuck();
            }

            if (this.map.frameNumber % 6 == 0)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)
                    ),
                    this.transform.parent
                );
                sparkle.spriteRenderer.sortingLayerID = this.spriteRenderer.sortingLayerID;
                sparkle.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder + 1;
            }
        }

        if(this.suckActive == false)
        {
            this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotation);
            this.transform.position = this.position + breathePos;
        }
    }

    public override void Collect()
    {
        if (this.collected == true) return;
        
        this.collected = true;

        this.map.player.AddKey(this);
        this.transform.parent = this.map.player.transform.parent;

        Audio.instance.PlaySfx(Assets.instance.sfxCoin, position: this.position, options: new Audio.Options(0.9f, true, 0.1f));
    }

    public void MoveToGate(GateBlock gate)
    {
        this.targetGate = gate;
        this.breatheMovement = false;
    }

    public void MoveToChest(Chest chest)
    {
        this.targetChest = chest;
        this.breatheMovement = false;
    }

    private void OpenGate()
    {
        if(this.openingGate)
            return;

        this.openingGate = true;
        this.transform.rotation = Quaternion.identity;

        Audio.instance.PlaySfx(Assets.instance.sfxGateUnlock, position: this.position);

        this.animated.PlayOnce(this.animationOpenGate, () =>
        {
            this.spriteRenderer.enabled = false;
            this.targetGate.Open();
            this.used = true;
        });
    }

    private void OpenChest()
    {
        if (this.openingChest)
            return;

        this.openingChest = true;
        this.transform.rotation = Quaternion.identity;

        Audio.instance.PlaySfx(Assets.instance.sfxGateUnlock, position: this.position);

        this.animated.PlayOnce(this.animationOpenGate, () =>
        {
            this.spriteRenderer.enabled = false;
            targetChest.Open();
            this.used = true;
        });
    }

    public void Hide()
    {
        this.spriteRenderer.enabled = false;

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnAnimation,
            this.hitboxes[0].InWorldSpace().center,
            this.transform.parent
        );

        p.transform.localScale = Vector3.one * 0.5f;
    }

    public override void Reset()
    {
        base.Reset();

        this.animated.PlayAndLoop(this.animationNormal);
        this.spriteRenderer.enabled = true;

        this.position = this.initalPosition;
        this.transform.rotation = Quaternion.identity;

        this.collected = false;
        this.targetGate = null;
        this.targetChest = null;
        this.used = false;

        this.smoothVelocity = Vector2.zero;
        this.breatheMovement = true;
        this.breatheTimer = 0f;
        this.openingGate = false;
        this.openingChest = false;
    }

    private bool CanFollowPlayer()
    {
        Player player = this.map.player;

        if(player.alive == false ||
            player.state == Player.State.Respawning ||
            player.state == Player.State.Finished)
        {
            return false;
        }

        return true;
    }
}
