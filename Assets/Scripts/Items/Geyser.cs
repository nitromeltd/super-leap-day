﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Geyser : Item
{
    public Animated.Animation animationCold;
    public Animated.Animation animationHot;

    [Header("Smoke Rendering")]
    public MeshRenderer smokeRenderer;
    public MeshRenderer smokeHighlightRenderer;
    public MeshRenderer smokeTopRenderer;
    public Transform smokeTransform;
    public Texture[] animationSmoke;
    public Texture[] animationSmokeHighlight;
    public Texture[] animationSmokeTop;
    public Texture[] animationSmokeEnd;
    public Texture[] animationSmokeHighlightEnd;
    public Texture[] animationSmokeTopEnd;

    [Header("Water")]
    public Animated waterAnimated;
    public Animated.Animation animationWaterBlank;
    public Animated.Animation animationWaterSplash;

    private bool hotAirActive;
    private bool playerInside;
    private int exitGeyserTimer;
    private float currentSmokeOffsetY;
    private float currentColorAlpha;
    private float targetColorAlpha;
    private int smokeFrameNumber;
    private float timeThroughFrame;
    private bool hasPlayedSteamSfx = false;
    private float timerSteam = 0;
    private TextureAnimator smokeAnimator;
    private TextureAnimator smokeHighlightAnimator;
    private TextureAnimator smokeTopAnimator;

    public const float MaxPlayerVerticalSpeed = 0.35f;
    public const float ImpulseForce = 0.01f;
    private static int ExitGeyserTime = 30;
    private const float SmokeOffsetSpeed = -0.0045f;

    private MeshRenderer[] smokeAndHighlightRenderers;
    private MeshRenderer[] allSmokeMeshRenderers;
    private MaterialPropertyBlock[] blocks;

    private const int DefaultAnimationSpeed = 20;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);

        ToggleHitbox(true);
        this.playerInside = false;

        this.allSmokeMeshRenderers = new MeshRenderer[]
        {
            this.smokeRenderer,
            this.smokeHighlightRenderer,
            this.smokeTopRenderer
        };

        this.smokeAndHighlightRenderers = new MeshRenderer[]
        {
            this.smokeRenderer,
            this.smokeHighlightRenderer
        };

        this.blocks = new MaterialPropertyBlock[this.allSmokeMeshRenderers.Length];
        for (int i = 0; i < this.blocks.Length; i++)
        {
            this.blocks[i] = new MaterialPropertyBlock();
        }

        this.smokeRenderer.sortingLayerName = "Items (Front)";
        this.smokeRenderer.sortingOrder = 5;
        this.smokeHighlightRenderer.sortingLayerName = "Items (Front)";
        this.smokeHighlightRenderer.sortingOrder = 6;
        this.smokeTopRenderer.sortingLayerName = "Items (Front)";
        this.smokeTopRenderer.sortingOrder = 6;

        this.smokeAnimator = new TextureAnimator(this.smokeRenderer, this.blocks[0]);
        this.smokeHighlightAnimator =
            new TextureAnimator(this.smokeHighlightRenderer, this.blocks[1]);
        this.smokeTopAnimator =
            new TextureAnimator(this.smokeTopRenderer, this.blocks[2]);

        this.waterAnimated.PlayOnce(this.animationWaterBlank);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        NotifyChangeTemperature(this.map.currentTemperature);
        this.timerSteam = 0;
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(newTemperature == Map.Temperature.Hot)
        {
            this.animated.PlayOnce(this.animationHot);
            ResumeHotAir();
            this.solidToPlayer = false;
        }
        else
        {
            this.animated.PlayOnce(this.animationCold);
            StopHotAir();
            this.solidToPlayer = true;
        }
    }

    private void RefreshSmoke(float solidDistance)
    {
        float positionY = 1.25f + (solidDistance - 1) * 0.5f;
        float textureTilingY = positionY;
        float scaleY = solidDistance + 0.50f;

        this.smokeTransform.localPosition = Vector2.up * positionY;
        this.smokeTransform.localScale = new Vector3(4f, scaleY, 1f);

        this.currentSmokeOffsetY += SmokeOffsetSpeed;
        Vector2 textureScale = new Vector2(1f, textureTilingY);
        Vector4 st = new Vector4(textureScale.x, textureScale.y, 0f, 0f);
        
        for (int i = 0; i < this.smokeAndHighlightRenderers.Length; i++)
        {
            blocks[i].SetVector("_MainTex_ST", st);
            this.smokeAndHighlightRenderers[i].SetPropertyBlock(blocks[i]);
        }

        float topPositionY = solidDistance + 2.50f;
        this.smokeTopRenderer.transform.localPosition = Vector2.up * topPositionY;

        float tilingDecimal = textureTilingY - (int)textureTilingY;
        float topOffsetY = Mathf.Lerp(0f, 0.60f, tilingDecimal);
        Vector4 stTop = new Vector4(1f, 1f, 0f, topOffsetY);
        
        blocks[2].SetVector("_MainTex_ST", stTop);
        this.smokeTopRenderer.SetPropertyBlock(blocks[2]);
    }

    public override void Advance()
    {
        if(this.exitGeyserTimer > 0)
        {
            this.exitGeyserTimer -= 1;
        }
        
        this.targetColorAlpha = this.hotAirActive == true ? 0.50f : 0f;
        this.currentColorAlpha = Util.Slide(
            this.currentColorAlpha, 
            this.targetColorAlpha, 
            this.hotAirActive ? 0.05f : 0.005f
        );

        for (int i = 0; i < this.allSmokeMeshRenderers.Length; i++)
        {
            blocks[i].SetColor("_Color", new Color(1f, 1f, 1f, this.currentColorAlpha));
            this.allSmokeMeshRenderers[i].SetPropertyBlock(blocks[i]);
        }

        this.smokeAnimator.Advance();
        this.smokeHighlightAnimator.Advance();
        this.smokeTopAnimator.Advance();

        if(this.hotAirActive == true)
        {
            Vector2 startPos = this.position.Add(0f, 1f);
            Raycast raycast = new Raycast(
                this.map,
                startPos,
                ignoreChunksOutsideRange: 20f,
                ignoreItem1: this,
                ignoreTileTopOnlyFlag: true,
                isCastByEnemy: true
            );
            
            var detectSolid = raycast.Arbitrary(
                startPos, Vector2.up, 50f
            );

            float solidDistance = detectSolid.distance;

            float distanceToChunkTop = (this.chunk.yMax + 1) - startPos.y;

            if(solidDistance > distanceToChunkTop)
            {
                solidDistance = distanceToChunkTop;
            }

            RefreshSmoke(solidDistance - 1);
            RefreshHitboxes();
            float originX = this.hitboxes[0].InWorldSpace().xMin;
            float originY = this.hitboxes[0].InWorldSpace().yMin;
            Rect impulseRect = new Rect(originX, originY, 3f, solidDistance);

            Player player = this.map.player;

            if(player.Rectangle().Overlaps(impulseRect) && player.ShouldCollideWithItems())
            {
                this.playerInside = true;
                LiftPlayer();
                
                if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSteamLoop))
                {
                    Audio.instance.PlaySfxLoop(Assets.instance.sfxEnvSteamLoop);
                }
            }
            else
            {
                if(this.playerInside == true)
                {
                    ExitPlayer();
                }
            }
        }
        else
        {
            if(this.playerInside == true)
            {
                ExitPlayer();
            }
        }
    }

    private void LiftPlayer()
    {
        if(this.exitGeyserTimer > 0) return;

        if(timerSteam < .1f)
        {
            timerSteam += 1 / 60.0f;
        }
        else
        {
            if (this.hasPlayedSteamSfx == false && !Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvSteam))
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnvSteam);
                this.hasPlayedSteamSfx = true;
            }
        }
        Player player = this.map.player;

        float targetVelocityX = player.position.x > this.transform.position.x ?
            -0.05f : 0.05f;
        player.velocity.x = Util.Slide(player.velocity.x, targetVelocityX, 0.01f);

        Vector2 minVerticalPosition = this.position.Add(0, 2f);
        if(player.position.y < minVerticalPosition.y)
        {
            player.position.y = minVerticalPosition.y;
            player.velocity.y = 0f;
        }
        
        if(player.state != Player.State.Geyser)
        {
            EnterPlayer();
        }
        else
        {
            if(player.TimeSinceJump <= 1)
            {
                ExitPlayer();
            }
        }
    }

    private void EnterPlayer()
    {
        Player player = this.map.player;

        player.state = Player.State.Geyser;
        player.ResetDoubleJump();
    }

    private void ExitPlayer()
    {
        this.playerInside = false;
        this.exitGeyserTimer = ExitGeyserTime;
        this.map.player.state = Player.State.Jump;
        Audio.instance.StopSfxLoop(Assets.instance.sfxEnvSteamLoop);
        Audio.instance.StopSfx(Assets.instance.sfxEnvSteam);
        this.hasPlayedSteamSfx = false;
        timerSteam = 0;
    }

    public bool CanHippoEnter()
    {
        return this.temperature == Map.Temperature.Hot;
    }

    public void DetectHippo()
    {
        ToggleHitbox(false);
    }

    public void EnterHippo()
    {
        if(this.map.currentTemperature == Map.Temperature.Hot)
        {
            StopHotAir();
            
            this.waterAnimated.PlayOnce(this.animationWaterSplash, () =>
            {
                this.waterAnimated.PlayOnce(this.animationWaterBlank);
            });
        }
    }


    public void ExitHippo()
    {
        ToggleHitbox(true);

        if(this.map.currentTemperature == Map.Temperature.Hot)
        {
            ResumeHotAir();
        }
    }

    private void ToggleHitbox(bool solid)
    {
        Hitbox.SolidData solidData = solid == true ? Hitbox.Solid : Hitbox.NonSolid;
        
        this.hitboxes = new [] {
            new Hitbox(
                -1.5f, 1.5f, 0f, 1f, this.rotation, solidData
            )
        };
    }

    private void ResumeHotAir()
    {
        this.hotAirActive = true;

        this.smokeAnimator.PlayLoop(this.animationSmoke, DefaultAnimationSpeed);
        this.smokeHighlightAnimator.PlayLoop(this.animationSmokeHighlight, DefaultAnimationSpeed);
        this.smokeTopAnimator.PlayLoop(this.animationSmokeTop, DefaultAnimationSpeed);
    }

    private void StopHotAir()
    {
        this.hotAirActive = false;

        this.smokeAnimator.PlayOnce(this.animationSmokeEnd, DefaultAnimationSpeed);
        this.smokeHighlightAnimator.PlayOnce(this.animationSmokeHighlightEnd, DefaultAnimationSpeed);
        this.smokeTopAnimator.PlayOnce(this.animationSmokeTopEnd, DefaultAnimationSpeed);
    }

    public override void Reset()
    {
        ToggleHitbox(true);
        NotifyChangeTemperature(this.map.currentTemperature);
    }

    [System.Serializable]
    private struct TextureAnimator
    {
        private MeshRenderer meshRenderer;
        private MaterialPropertyBlock matPropertyBlock;
        private int frameNumber;
        private float timeThroughFrame;
        private Texture[] currentAnimation;
        private bool loop;
        private int fps;
        private float playbackSpeed;

        public TextureAnimator(MeshRenderer rend,  MaterialPropertyBlock block)
        {
            this.meshRenderer = rend;
            this.matPropertyBlock = block;
            this.frameNumber = 0;
            this.timeThroughFrame = 0f;
            this.currentAnimation = null;
            this.loop = false;
            this.fps = 0;
            this.playbackSpeed = 1f;
        }

        public void Advance()
        {
            if(this.currentAnimation == null) return;
            if(this.fps == 0) return;

            float time = this.fps / 60.0f;
            this.timeThroughFrame += time * this.playbackSpeed;

            int framesToAdvance = Mathf.FloorToInt(this.timeThroughFrame);
            this.frameNumber += framesToAdvance;
            this.timeThroughFrame -= framesToAdvance;

            if(this.loop)
            {
                this.frameNumber %= this.currentAnimation.Length;
            }
            else if(this.frameNumber >= this.currentAnimation.Length)
            {
                this.frameNumber = this.currentAnimation.Length - 1;
            }

            matPropertyBlock.SetTexture("_MainTex", this.currentAnimation[this.frameNumber]);
            meshRenderer.SetPropertyBlock(matPropertyBlock);
        }

        public void PlayOnce(Texture[] animation, int fps)
        {
            this.fps = fps;
            this.frameNumber = 0;
            this.timeThroughFrame = 0f;
            this.currentAnimation = animation;
            this.loop = false;
        }

        public void PlayLoop(Texture[] animation, int fps)
        {
            this.fps = fps;
            this.frameNumber = 0;
            this.timeThroughFrame = 0f;
            this.currentAnimation = animation;
            this.loop = true;
        }
    }
}
