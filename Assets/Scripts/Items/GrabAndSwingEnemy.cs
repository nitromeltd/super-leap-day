
using UnityEngine;
using System.Linq;

public class GrabAndSwingEnemy : Item
{
    private Transform connection;

    private bool dragging = false;
    private float distance;

    public static GrabAndSwingEnemy NearestToPlayer(Player player)
    {
        var chunk = player.map.NearestChunkTo(player.position);

        var candidates = chunk.items
            .OfType<GrabAndSwingEnemy>()
            .Where(c => c.IsPlayerWithinRange())
            .ToArray();
        return candidates.MinBy(
            c => (c.position - player.position).sqrMagnitude
        );
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.connection = this.transform.Find("Connection");
        this.connection.gameObject.SetActive(false);
    }

    private bool IsPlayerWithinRange() =>
        (this.map.player.position - this.position).sqrMagnitude < 80 * 80;

    public override void Advance()
    {
        var player = this.map.player;

        if (this.dragging == false &&
            GameInput.Player(this.map).pressedJump == true &&
            IsPlayerWithinRange() == true)
        {
            this.dragging = true;
            this.distance = (player.position - this.position).magnitude;
        }

        if (this.dragging == true && GameInput.Player(this.map).holdingJump == false)
        {
            this.dragging = false;
        }

        if (this.dragging == true)
        {
            var playerRelative = (player.position - this.position);
            var direction = playerRelative.normalized;
            var distance = playerRelative.magnitude;

            if (distance > this.distance)
            {
                distance = this.distance;
            }

            var oldPlayerPosition = player.position;
            var oldPlayerSpeed = player.velocity.magnitude;

            var newPlayerPosition = this.position + (direction * distance);
            player.position = newPlayerPosition;
            player.velocity += (newPlayerPosition - oldPlayerPosition);

            player.velocity = Vector2.Lerp(
                player.velocity,
                player.velocity.normalized * oldPlayerSpeed,
                0.5f
            );

            var angle = Mathf.Atan2(direction.y, direction.x) * 180 / Mathf.PI;
            this.connection.transform.localPosition = Vector2.zero;
            this.connection.transform.localScale = new Vector3(distance / 32, 1, 1);
            this.connection.transform.localRotation = Quaternion.Euler(0, 0, angle);
            this.connection.gameObject.SetActive(true);
        }
        else
        {
            this.connection.gameObject.SetActive(false);
        }

        if ((player.position - this.position).sqrMagnitude < 1 * 1)
        {
            player.Hit(this.position);
        }
    }
}
