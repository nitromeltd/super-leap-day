﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHook : Item
{
    public Animated.Animation animationFloorIdle;
    public Animated.Animation animationFloorShine;

    public Animated.Animation animationWallLeftIdle;
    public Animated.Animation animationWallLeftShine;

    public Animated.Animation animationWallRightIdle;
    public Animated.Animation animationWallRightShine;

    public Animated.Animation animationCeilingIdle;
    public Animated.Animation animationCeilingShine;

    private int ignoreTime = 0;
    private int shineTimer = 0;

    private const int SHINE_TIME = 30 * 5;

    public override void Init(Def def)
    {
        base.Init(def);

        GrabType grabHookType = GrabType.None;
        Vector2 grabHookPosRelativeToItem = Vector2.zero;

        // define GrabType base on rotation
        if (this.rotation == 0)
        {
            grabHookType = GrabType.Ceiling;
            grabHookPosRelativeToItem = Vector2.up * -0.2f;
            //this.spriteRenderer.sprite = this.ceilingSprite;
            this.animated.PlayAndHoldLastFrame(animationCeilingIdle);
        }
        else if (this.rotation == 90)
        {
            grabHookType = GrabType.LeftWall;
            //spriteRenderer.sprite = this.wallLeftSprite;
            this.animated.PlayAndHoldLastFrame(animationWallLeftIdle);
        }
        else if (this.rotation == 180)
        {
            grabHookType = GrabType.Floor;
            //this.spriteRenderer.sprite = this.floorSprite;
            this.animated.PlayAndHoldLastFrame(animationFloorIdle);
        }
        else if (this.rotation == 270)
        {
            grabHookType = GrabType.RightWall;
            //this.spriteRenderer.sprite = this.wallRightSprite;
            this.animated.PlayAndHoldLastFrame(animationWallRightIdle);
        }

        this.grabInfo = new GrabInfo(this, grabHookType, grabHookPosRelativeToItem);

        //this.rotation = 0;
        this.transform.rotation = Quaternion.identity;

        this.hitboxes = new [] {
            new Hitbox(
                grabHookPosRelativeToItem.x - 0.4f,
                grabHookPosRelativeToItem.x + 0.4f,
                grabHookPosRelativeToItem.y - 0.4f,
                grabHookPosRelativeToItem.y + 0.4f,
                this.rotation,
                Hitbox.NonSolid
            )
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        AdvanceShine();
        AdvanceHanging();
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreTime = 30;
            }
            return;
        }
        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - SensorPosition();
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreTime = 15;
        }
    }

    public Vector2 SensorPosition()
    {
        switch(this.grabInfo.grabType)
        {
            case Item.GrabType.Floor:
                return this.position + (Vector2.up * 1f);

            case Item.GrabType.RightWall:
            case Item.GrabType.LeftWall:
            case Item.GrabType.Ceiling:
            default:
                return this.position;

        }
    }

    private void AdvanceShine()
    {
        if(this.shineTimer > 0)
        {
            this.shineTimer--;
        }
        else
        {
            this.shineTimer = SHINE_TIME;
            
            if (this.rotation == 0)
            {
                this.animated.PlayOnce(animationCeilingShine, () => 
                {
                    this.animated.PlayAndHoldLastFrame(animationCeilingIdle);
                });
            }
            else if (this.rotation == 90)
            {
                this.animated.PlayOnce(animationWallLeftShine, () => 
                {
                    this.animated.PlayAndHoldLastFrame(animationWallLeftIdle);
                });
            }
            else if (this.rotation == 180)
            {
                this.animated.PlayOnce(animationFloorShine, () => 
                {
                    this.animated.PlayAndHoldLastFrame(animationFloorIdle);
                });
            }
            else if (this.rotation == 270)
            {
                this.animated.PlayOnce(animationWallRightShine, () => 
                {
                    this.animated.PlayAndHoldLastFrame(animationWallRightIdle);
                });
            }
        }
    }
}