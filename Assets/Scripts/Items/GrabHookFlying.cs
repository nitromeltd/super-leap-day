﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabHookFlying : Item
{
    public Animated.Animation animationFlying;
    public Animated.Animation animationShine;

    // path handling
    protected NitromeEditor.Path path;
    protected float offsetThroughPath;

    private Vector2 basePosition;
    private Vector2 velocity = Vector2.zero;
    private Vector2 moveDir = Vector2.zero;
    private Vector2 targetPos;

    private enum State { Floating, Moving };
    private State state;

    private int pushTime = 0;
    private int ignoreTime = 0;
    private int shineTimer = 0;

    private const int SHINE_TIME = 30 * 5;

    // breathe movement
    private float breatheTimer;
    private const float BREATHE_PERIOD = .5f;
    private const float BREATHE_AMPLITUDE = 1f;
    
    public override void Init(Def def)
    {
        base.Init(def);
        SetUpFollowingMethod(def.startChunk);

        this.animated.PlayAndLoop(this.animationFlying);
        this.basePosition = this.position;
        this.state = State.Floating;

        this.grabInfo = new GrabInfo(this, GrabType.Ceiling, Vector2.up * -0.2f);
    }

    private void SetUpFollowingMethod(Chunk chunk)
    {
        this.group = null;
        this.path = null;

        var g = chunk.groupLayer.RegisterFollower(this, def.tx, def.ty);
        if (g != null)
        {
            AttachToGroup(g);
            return;
        }

        this.path = chunk.pathLayer.NearestPathTo(this.position, 2);
        if (this.path != null)
        {
            this.offsetThroughPath = TimeNearestToPoint(this.path, this.position);
            this.path = Util.MakeClosedPathByPingPonging(this.path);
            return;
        }

        var followPath =
            chunk.connectionLayer.NearestPathTo(this.position, 2);
        if (followPath != null)
        {
            var followPosition = followPath.nodes[0].Position;
        }
    }

    private static float TimeNearestToPoint(NitromeEditor.Path path, Vector2 pt)
    {
        var edgeIndex = path.NearestEdgeIndexTo(pt).Value;
        var edge = path.edges[edgeIndex];

        var edgeDelta = edge.to.Position - edge.from.Position;
        var through =
            Vector2.Dot(edgeDelta, pt - edge.from.Position) / edgeDelta.sqrMagnitude;
        through = Mathf.Clamp01(through);

        float result = 0.0f;
        for (var n = 0; n < edgeIndex; n += 1)
        {
            result += path.edges[n].distance / path.edges[n].speedPixelsPerSecond;
        }
        result += through * (edge.distance / edge.speedPixelsPerSecond);
        return result;
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.basePosition = this.group.FollowerPosition(this);
        }
        else if (this.path != null)
        {
            this.basePosition = this.path.PointAtTime(
                (this.map.frameNumber / 60.0f) + this.offsetThroughPath
            ).position;
        }

        BreatheMovement();
        AdvanceShine();
        AdvanceHanging();

        switch(this.state)
        {
            case State.Floating:
                AdvanceFloating();
                return;

            case State.Moving:
                AdvanceMoving();
                return;
        }
    }

    private void BreatheMovement()
    {
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BREATHE_PERIOD;
        float distance = BREATHE_AMPLITUDE * Mathf.Sin(theta);

        // ignore breathe movement if is following path or is part of group
        if(this.path != null || this.group != null)
        {
            distance = 0f;
        }   

        this.targetPos = this.basePosition + Vector2.up * distance;
    }

    private void AdvanceFloating()
    {
        Vector2 targetVel = (this.targetPos - this.position) * .05f;

        this.velocity *= 0.95f;
        this.velocity += targetVel * .2f;

        this.position += this.velocity;
        this.transform.position = this.position;
    }

    private void AdvanceMoving()
    {
        this.pushTime--;
        this.velocity *= 0.95f;

        if(this.pushTime <= 0)
        {
            this.pushTime = 0;
            this.state = State.Floating;
        }
        
        this.position += this.velocity;
        this.transform.position = this.position;
    }

    private void AdvanceShine()
    {
        if(this.shineTimer > 0)
        {
            this.shineTimer--;
        }
        else
        {
            this.shineTimer = SHINE_TIME;

            this.animated.PlayOnce(animationShine, () => 
            {
                this.animated.PlayAndLoop(animationFlying);
            });
        }
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null) return;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - this.position;
        if (Mathf.Abs(delta.x) < 1 &&
            Mathf.Abs(delta.y) < 1 &&
            player.groundState.onGround == false)
        {
            player.GrabItem(this);

            // move with player velocity
            this.moveDir = this.map.player.velocity.normalized;
            this.velocity = this.moveDir * 0.1f;
            this.pushTime = 15;
            this.ignoreTime = 15;
            this.state = State.Moving;
        }

    }
}
