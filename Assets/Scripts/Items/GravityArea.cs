using UnityEngine;

public class GravityArea : Item
{
    [System.NonSerialized] public Player.Orientation? gravityDirection;

    public Sprite spriteForNoGravity;
    public Tileset tilesetForUp;
    public Tileset tilesetForRight;
    public Tileset tilesetForDown;
    public Tileset tilesetForLeft;

    public Animated.Animation animationUp1;
    public Animated.Animation animationUp2;
    public Animated.Animation animationRight1;
    public Animated.Animation animationRight2;
    public Animated.Animation animationDown1;
    public Animated.Animation animationDown2;
    public Animated.Animation animationLeft1;
    public Animated.Animation animationLeft2;

    public Player.Orientation? GravityDirection()
    {
        if (def.tile.tile.Contains("none"))
            return null;

        switch (def.tile.rotation)
        {
            case 0:   return Player.Orientation.Ceiling;
            case 90:  return Player.Orientation.LeftWall;
            default:  return Player.Orientation.Normal;
            case 270: return Player.Orientation.RightWall;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        Tileset tileset = null;

        this.gravityDirection = GravityDirection();

        if (this.gravityDirection == null)
        {
            SpriteRenderer front =
                this.transform.Find("front").GetComponent<SpriteRenderer>();
            front.sprite = this.spriteForNoGravity;
        }
        else
        {
            switch (this.gravityDirection)
            {
                case Player.Orientation.Ceiling:   tileset = this.tilesetForUp;    break;
                case Player.Orientation.LeftWall:  tileset = this.tilesetForLeft;  break;
                case Player.Orientation.Normal:    tileset = this.tilesetForDown;  break;
                case Player.Orientation.RightWall: tileset = this.tilesetForRight; break;
            }
        }

        if (tileset != null)
        {
            SpriteRenderer front;
            front = this.transform.Find("front").GetComponent<SpriteRenderer>();
            front.transform.position = this.position.Add(-0.5f, -0.5f);
            front.transform.rotation = Quaternion.identity;

            bool MatchingGravityAreaAt(int _mapTx, int _mapTy)
            {
                var result = this.chunk.GravityAreaAt(_mapTx, _mapTy);
                if (result?.GravityDirection() == this.gravityDirection)
                    return true;
                else
                    return false;
            }
            var l = MatchingGravityAreaAt(this.def.tx - 1, this.def.ty);
            var r = MatchingGravityAreaAt(this.def.tx + 1, this.def.ty);
            var u = MatchingGravityAreaAt(this.def.tx, this.def.ty + 1);
            var d = MatchingGravityAreaAt(this.def.tx, this.def.ty - 1);
            var lu = MatchingGravityAreaAt(this.def.tx - 1, this.def.ty + 1);
            var ru = MatchingGravityAreaAt(this.def.tx + 1, this.def.ty + 1);
            var ld = MatchingGravityAreaAt(this.def.tx - 1, this.def.ty - 1);
            var rd = MatchingGravityAreaAt(this.def.tx + 1, this.def.ty - 1);

            if (l == false && r == true && u == true && d == true)
                front.sprite = Util.RandomChoice(tileset.left);
            else if (l == true && r == false && u == true && d == true)
                front.sprite = Util.RandomChoice(tileset.right);
            else if (l == true && r == true && u == false && d == true)
                front.sprite = Util.RandomChoice(tileset.top);
            else if (l == true && r == true && u == true && d == false)
                front.sprite = Util.RandomChoice(tileset.bottom);
            else if (l == false && r == true && u == false && d == true)
                front.sprite = Util.RandomChoice(tileset.topLeft);
            else if (l == true && r == false && u == false && d == true)
                front.sprite = Util.RandomChoice(tileset.topRight);
            else if (l == false && r == true && u == true && d == false)
                front.sprite = Util.RandomChoice(tileset.bottomLeft);
            else if (l == true && r == false && u == true && d == false)
                front.sprite = Util.RandomChoice(tileset.bottomRight);
            else if (u == true && r == true && ru == false)
                front.sprite = Util.RandomChoice(tileset.insideTopRight);
            else if (u == true && l == true && lu == false)
                front.sprite = Util.RandomChoice(tileset.insideTopLeft);
            else if (d == true && r == true && rd == false)
                front.sprite = Util.RandomChoice(tileset.insideBottomRight);
            else if (d == true && l == true && ld == false)
                front.sprite = Util.RandomChoice(tileset.insideBottomLeft);

            var back = this.transform.Find("back");
            back.transform.position = this.position;
            back.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.5f);
            var backAnimated = back.gameObject.GetComponent<Animated>();

            switch (this.gravityDirection)
            {
                case Player.Orientation.Ceiling:
                    backAnimated.PlayAndLoop(
                        this.def.tx % 2 == 0 ? this.animationUp1 : this.animationUp2
                    );
                    back.transform.rotation = Quaternion.Euler(0, 0, 90);
                    break;
                case Player.Orientation.RightWall:
                    backAnimated.PlayAndLoop(
                        this.def.ty % 2 == 0 ? this.animationRight1 : this.animationRight2
                    );
                    back.transform.rotation = Quaternion.identity;
                    break;
                case Player.Orientation.Normal:
                    backAnimated.PlayAndLoop(
                        this.def.tx % 2 == 0 ? this.animationDown1 : this.animationDown2
                    );
                    back.transform.rotation = Quaternion.Euler(0, 0, -90);
                    break;
                case Player.Orientation.LeftWall:
                    backAnimated.PlayAndLoop(
                        this.def.ty % 2 == 0 ? this.animationLeft1 : this.animationLeft2
                    );
                    back.transform.rotation = Quaternion.Euler(0, 0, 180);
                    break;
            }
        }
    }
}
