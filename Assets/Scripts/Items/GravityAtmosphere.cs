using System.Linq;
using UnityEngine;

public class GravityAtmosphere : Item
{
    public enum Shape { Flat, CurveBR3Plus1, CurveOutBR3, Corner }

    public class Result
    {
        public GravityAtmosphere atmosphere;
        public float amountThroughPath;
        public Vector2 positionOfNearestPoint;
        public float squareDistanceToNearestPoint;
        public Vector2 down;
        public Vector2 forward;
    }

    public const float Range = 2.0f;

    private Shape shape;
    private bool isSpriteSet;

    public Sprite spriteFlatNormal;
    public Sprite spriteFlatLeft;
    public Sprite spriteFlatRight;

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.tile == "gravity_atmosphere_curvebr3plus1")
            this.shape = Shape.CurveBR3Plus1;
        else if (def.tile.tile == "gravity_atmosphere_curveoutbr3")
            this.shape = Shape.CurveOutBR3;
        else if (def.tile.tile == "gravity_atmosphere_corner")
            this.shape = Shape.Corner;
        else
            this.shape = Shape.Flat;

        // this.rotation is altered now if the object is flipped,
        // so when we get to the connection logic below, we can disregard flips.
        this.rotation = this.def.tile.rotation;
        if (def.tile.flip == true)
        {
            if (this.shape == Shape.CurveBR3Plus1)
                this.rotation = (this.rotation + 270) % 360;
            else if (this.shape == Shape.CurveOutBR3)
                this.rotation = (this.rotation + 270) % 360;
            else if (this.shape == Shape.Corner)
                this.rotation = (this.rotation + 90) % 360;
        }

        this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
    }

    public override void Advance()
    {
        if (this.isSpriteSet == false)
        {
            this.isSpriteSet = true;

            if (this.shape == Shape.Flat && ConnectedAtmosphereBackward() == null)
                this.spriteRenderer.sprite = this.spriteFlatLeft;
            else if (this.shape == Shape.Flat && ConnectedAtmosphereForward() == null)
                this.spriteRenderer.sprite = this.spriteFlatRight;
        }
    }

    public Result NearestPoint(Vector2 position, float maxDistance = Mathf.Infinity)
    {
        Vector2 nearest = default;
        float through = default;
        Vector2 down = default;

        if (this.shape == Shape.Flat)
        {
            var nearestLocal = this.transform.InverseTransformPoint(position);
            nearestLocal.x = Mathf.Clamp(nearestLocal.x, -0.5f, 0.5f);
            nearestLocal.y = 0;
            nearest = this.transform.TransformPoint(nearestLocal);
            through = nearestLocal.x + 0.5f;
            down = this.transform.TransformDirection(Vector2.down);
        }
        else
        {
            var positionLocal = this.transform.InverseTransformPoint(position);
            positionLocal = positionLocal.normalized * 3;
            if (positionLocal.y > -positionLocal.x)
            {
                if (positionLocal.y > 0)
                    positionLocal = new Vector3(3, 0);
            }
            else
            {
                if (positionLocal.x < 0)
                    positionLocal = new Vector3(0, -3);
            }
            nearest = this.transform.TransformPoint(positionLocal);

            if (this.shape == Shape.CurveBR3Plus1)
            {
                through = positionLocal.x / 3;
                down = this.transform.TransformVector(positionLocal).normalized;
            }
            else if (this.shape == Shape.CurveOutBR3)
            {
                through = 1 - (positionLocal.x / 3);
                down = -this.transform.TransformVector(positionLocal).normalized;
            }
        }

        var forward = new Vector2(-down.y, down.x);

        var squareDistance = (nearest - position).sqrMagnitude;
        if (squareDistance > maxDistance * maxDistance)
            return null;

        return new Result
        {
            atmosphere = this,
            amountThroughPath = through,
            positionOfNearestPoint = nearest,
            squareDistanceToNearestPoint = squareDistance,
            down = down,
            forward = forward
        };
    }

    public static Result Nearest(
        Map map,
        Vector2 position,
        float maxDistance = Mathf.Infinity,
        GravityAtmosphere preferredAtmosphere = null
    )
    {
        var chunk = map.NearestChunkTo(position);
        Result result = null;

        /* The reason for preferredAtmosphere argument here, is that Nearest()
           can't otherwise distinguish between the two atmospheres that meet
           at a corner, but we'd like the player to stay on one side unless
           there's compelling reason to switch, as this changes their gravity
           and affects the arc of their jump. */

        foreach (var atmosphere in chunk.subsetOfItemsOfTypeGravityAtmosphere)
        {
            if (atmosphere.shape == Shape.Corner) continue;

            var thisResult = atmosphere.NearestPoint(position, maxDistance);
            if (thisResult == null) continue;

            if (result == null)
            {
                result = thisResult;
                continue;
            }

            float oldSqDist = result.squareDistanceToNearestPoint;
            if (result.atmosphere == preferredAtmosphere)
                oldSqDist -= 0.001f;

            float thisSqDist = thisResult.squareDistanceToNearestPoint;
            if (thisResult.atmosphere == preferredAtmosphere)
                thisSqDist -= 0.001f;

            if (thisSqDist < oldSqDist)
            {
                result = thisResult;
            }
        }

        return result;
    }

    public GravityAtmosphere ConnectedAtmosphereBackward()
    {
        var pos = new Vector2Int(this.def.tx, this.def.ty);
        var rot = 0;

        {
            Vector2Int forward = this.rotation switch
            {
                270 => Vector2Int.down,
                180 => Vector2Int.left,
                90  => Vector2Int.up,
                _   => Vector2Int.right
            };
            Vector2Int up = new Vector2Int(-forward.y, forward.x);

            switch (this.shape)
            {
                case Shape.Flat:
                    pos += forward * -1;
                    rot = this.rotation;
                    break;
                case Shape.CurveBR3Plus1:
                    pos += up * -2;
                    pos += forward * -1;
                    rot = this.rotation;
                    break;
                case Shape.CurveOutBR3:
                    pos += forward * 3;
                    pos += up;
                    rot = (this.rotation + 270) % 360;
                    break;
            }
        }

        // at this point, pos contains the (tx, ty) where the next
        // atmosphere should start, and rot contains the angle it should
        // lie on when it starts.

        {
            (int tx, int ty, int rotation) lookingForCorner =
                (pos.x, pos.y, (rot + 90) % 360);

            foreach (var a in this.chunk.subsetOfItemsOfTypeGravityAtmosphere)
            {
                if (a.shape != Shape.Corner) continue;

                var location = (a.def.tx, a.def.ty, a.rotation);
                if (location != lookingForCorner) continue;

                rot = (rot + 90) % 360;
                if (rot == 0) pos.x -= 1;
                if (rot == 90) pos.y -= 1;
                if (rot == 180) pos.x += 1;
                if (rot == 270) pos.y += 1;
                break;
            }
        }

        (int tx, int ty, int rotation) lookingForFlat = default;
        (int tx, int ty, int rotation) lookingForCurveBR3Plus1 = default;
        (int tx, int ty, int rotation) lookingForCurveOutBR3 = default;

        if (rot == 0)
        {
            lookingForFlat = (pos.x, pos.y, 0);
            lookingForCurveBR3Plus1 = (pos.x, pos.y + 2, 270);
            lookingForCurveOutBR3 = (pos.x, pos.y - 3, 180);
        }
        else if (rot == 90)
        {
            lookingForFlat = (pos.x, pos.y, 90);
            lookingForCurveBR3Plus1 = (pos.x - 2, pos.y, 0);
            lookingForCurveOutBR3 = (pos.x + 3, pos.y, 270);
        }
        else if (rot == 180)
        {
            lookingForFlat = (pos.x, pos.y, 180);
            lookingForCurveBR3Plus1 = (pos.x, pos.y - 2, 90);
            lookingForCurveOutBR3 = (pos.x, pos.y + 3, 0);
        }
        else if (rot == 270)
        {
            lookingForFlat = (pos.x, pos.y, 270);
            lookingForCurveBR3Plus1 = (pos.x + 2, pos.y, 180);
            lookingForCurveOutBR3 = (pos.x - 3, pos.y, 90);
        }

        foreach (var a in this.chunk.subsetOfItemsOfTypeGravityAtmosphere)
        {
            var location = (a.def.tx, a.def.ty, a.rotation);

            if (a.shape == Shape.Flat && location == lookingForFlat)
                return a;
            if (a.shape == Shape.CurveBR3Plus1 && location == lookingForCurveBR3Plus1)
                return a;
            if (a.shape == Shape.CurveOutBR3 && location == lookingForCurveOutBR3)
                return a;
        }

        return null;
    }

    public GravityAtmosphere ConnectedAtmosphereForward()
    {
        var pos = new Vector2Int(this.def.tx, this.def.ty);
        var rot = 0;

        {
            Vector2Int forward = this.rotation switch
            {
                270 => Vector2Int.down,
                180 => Vector2Int.left,
                90  => Vector2Int.up,
                _   => Vector2Int.right
            };
            Vector2Int up = new Vector2Int(-forward.y, forward.x);

            switch (this.shape)
            {
                case Shape.Flat:
                    pos += forward;
                    rot = this.rotation;
                    break;
                case Shape.CurveBR3Plus1:
                    pos += forward * 2;
                    pos += up;
                    rot = (this.rotation + 90) % 360;
                    break;
                case Shape.CurveOutBR3:
                    pos += up * -3;
                    pos += forward * -1;
                    rot = (this.rotation + 180) % 360;
                    break;
            }
        }

        // at this point, pos contains the (tx, ty) where the next
        // atmosphere should start, and rot contains the angle it should
        // lie on when it starts.

        {
            (int tx, int ty, int rotation) lookingForCorner = (pos.x, pos.y, rot);

            foreach (var a in this.chunk.subsetOfItemsOfTypeGravityAtmosphere)
            {
                if (a.shape != Shape.Corner) continue;

                var location = (a.def.tx, a.def.ty, a.rotation);
                if (location != lookingForCorner) continue;

                rot = (rot + 270) % 360;
                if (rot == 0) pos.x += 1;
                if (rot == 90) pos.y += 1;
                if (rot == 180) pos.x -= 1;
                if (rot == 270) pos.y -= 1;
                break;
            }
        }

        (int tx, int ty, int rotation) lookingForFlat = default;
        (int tx, int ty, int rotation) lookingForCurveBR3Plus1 = default;
        (int tx, int ty, int rotation) lookingForCurveOutBR3 = default;

        if (rot == 0)
        {
            lookingForFlat = (pos.x, pos.y, 0);
            lookingForCurveBR3Plus1 = (pos.x, pos.y + 2, 0);
            lookingForCurveOutBR3 = (pos.x, pos.y - 3, 90);
        }
        else if (rot == 90)
        {
            lookingForFlat = (pos.x, pos.y, 90);
            lookingForCurveBR3Plus1 = (pos.x - 2, pos.y, 90);
            lookingForCurveOutBR3 = (pos.x + 3, pos.y, 180);
        }
        else if (rot == 180)
        {
            lookingForFlat = (pos.x, pos.y, 180);
            lookingForCurveBR3Plus1 = (pos.x, pos.y - 2, 180);
            lookingForCurveOutBR3 = (pos.x, pos.y + 3, 270);
        }
        else if (rot == 270)
        {
            lookingForFlat = (pos.x, pos.y, 270);
            lookingForCurveBR3Plus1 = (pos.x + 2, pos.y, 270);
            lookingForCurveOutBR3 = (pos.x - 3, pos.y, 0);
        }

        foreach (var a in this.chunk.subsetOfItemsOfTypeGravityAtmosphere)
        {
            var location = (a.def.tx, a.def.ty, a.rotation);

            if (a.shape == Shape.Flat && location == lookingForFlat)
                return a;
            if (a.shape == Shape.CurveBR3Plus1 && location == lookingForCurveBR3Plus1)
                return a;
            if (a.shape == Shape.CurveOutBR3 && location == lookingForCurveOutBR3)
                return a;
        }

        return null;
    }

    public Player.Orientation OrientationAt(Vector2 worldPosition)
    {
        Vector2 vector;

        if (this.def.tile.tile == "gravity_atmosphere_curveoutbr3")
            vector = (this.position - worldPosition).normalized;
        else
            vector = this.transform.TransformVector(Vector2.down);

        if      (vector.x >=  0.707f) return Player.Orientation.RightWall;
        else if (vector.x <= -0.707f) return Player.Orientation.LeftWall;
        else if (vector.y >=  0.707f) return Player.Orientation.Ceiling;
        else                          return Player.Orientation.Normal;
    }

    public static bool NeedsTurningInPlace(GravityAtmosphere from, GravityAtmosphere to)
    {
        var angleOfFirstAtEnd = from.shape switch
        {
            Shape.CurveBR3Plus1 => (from.rotation + 90) % 360,
            Shape.CurveOutBR3   => (from.rotation + 180) % 360,
            _                   => from.rotation,
        };
        var angleOfSecondAtStart = to.shape switch
        {
            Shape.CurveOutBR3 => (to.rotation + 270) % 360,
            _                 => to.rotation
        };
        return angleOfSecondAtStart == (angleOfFirstAtEnd + 270) % 360;
    }
}
