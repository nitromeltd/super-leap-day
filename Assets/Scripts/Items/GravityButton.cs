using UnityEngine;
using System;
using System.Linq;
using Orientation = Player.Orientation;

public class GravityButton : Item
{
    [NonSerialized] public Orientation targetGravityDirection;
    public SpriteRenderer orbSpriteRenderer;
    public Sprite spriteUnpressed;
    public Sprite spritePressed;
    public Sprite spriteOrbUp;
    public Sprite spriteOrbDown;
    public Sprite spriteOrbLeft;
    public Sprite spriteOrbRight;

    public Animated.Animation animationIdle;
    public Animated.Animation animationHit;
    public Animated orbAnimated;

    private Vector3 orbInitialLocalPosition;
    private Vector2 orbVelocity;
    private bool playerIsIntersecting;
    private int timeSinceLastHit;

    public override void Init(Def def)
    {
        base.Init(def);
        this.orbVelocity = Vector2.zero;
        this.orbInitialLocalPosition = this.orbSpriteRenderer.transform.localPosition;

        this.orbAnimated.PlayOnce(this.animationIdle);

        switch (def.tile.tile.Split('_').Last())
        {
            case "right":
            {
                this.targetGravityDirection = Orientation.RightWall;
                this.orbSpriteRenderer.sprite = this.spriteOrbRight;
                break;
            }
            case "up":
            {
                this.targetGravityDirection = Orientation.Ceiling;
                this.orbSpriteRenderer.sprite = this.spriteOrbUp;
                break;
            }
            case "left":
            {
                this.targetGravityDirection = Orientation.LeftWall;
                this.orbSpriteRenderer.sprite = this.spriteOrbLeft;
                break;
            }
            default:
            {
                this.targetGravityDirection = Orientation.Normal;
                this.orbSpriteRenderer.sprite = this.spriteOrbDown;
                break;
            }
        }
        if (this.chunk.isFlippedHorizontally == true)
        {
            if (this.targetGravityDirection == Orientation.RightWall)
            {
                this.targetGravityDirection = Orientation.LeftWall;
                this.orbSpriteRenderer.sprite = this.spriteOrbLeft;
            }
            else if (this.targetGravityDirection == Orientation.LeftWall)
            {
                this.targetGravityDirection = Orientation.RightWall;
                this.orbSpriteRenderer.sprite = this.spriteOrbRight;
            }
        }

        this.orbSpriteRenderer.transform.localRotation = Quaternion.Euler(0,0, -this.transform.rotation.eulerAngles.z);

        var p = this.orbSpriteRenderer.transform.localPosition;
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(p.x-0.5f, p.x+0.5f, p.y-0.5f, p.y+0.5f, this.rotation, Hitbox.NonSolid)
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        // DebugDrawing.Draw(false, true);        
        
        if (this.chunk.gravityDirection != this.targetGravityDirection && IsBeingPressed() == true)
        {
            this.chunk.gravityDirection = this.targetGravityDirection;                
        }

        AdvanceOrb();

        var p = this.orbSpriteRenderer.transform.localPosition;
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(p.x-0.5f, p.x+0.5f, p.y-0.5f, p.y+0.5f, this.rotation, Hitbox.NonSolid)
        };
    }

    private const float BobRadiusX = 0.02f;
    private const float BobRadiusY = 0.1f;
    private void AdvanceOrb()
    {
        this.timeSinceLastHit += 1;

        Vector3 bobOffset = new Vector3(BobRadiusX * Mathf.Sin(this.map.frameNumber * 2 * Mathf.Deg2Rad), BobRadiusY * Mathf.Cos(this.map.frameNumber * 2 *Mathf.Deg2Rad));

        var orbPosition = (Vector2)this.orbSpriteRenderer.transform.position;
        var playerPosition = this.map.player.position;

        var radius = this.playerIsIntersecting ? 1.5f : 1.0f;
        var intersecting = this.map.player.ShouldCollideWithItems() == true && (orbPosition - playerPosition).sqrMagnitude < radius * radius;

        if (intersecting == true &&
            this.playerIsIntersecting == false &&
            this.timeSinceLastHit > 30)
        {
            this.timeSinceLastHit = 0;
            Vector2 toOrb = (orbPosition - playerPosition).normalized;
            this.orbVelocity = toOrb * 0.3f;
            this.orbAnimated.PlayOnce(this.animationHit);
 
            // player hit the orb, let's change the gravity
            if (this.chunk.gravityDirection != this.targetGravityDirection)
            {
                this.chunk.gravityDirection = this.targetGravityDirection;
            }
            Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvButtonGravity,
                    position: this.position
                );
        }
        else
        {
            Vector2 targetVelocity =
                    (this.transform.TransformPoint(this.orbInitialLocalPosition + bobOffset) - this.orbSpriteRenderer.transform.position) * 0.2f;

            this.orbVelocity *= 0.95f;
            this.orbVelocity += targetVelocity * 0.2f;
        }
        this.playerIsIntersecting = intersecting;
        this.orbSpriteRenderer.transform.position += (Vector3)this.orbVelocity;
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        // player collision is covered by the orb
        // if (this.map.player.Rectangle().Overlaps(thisRect))
        //     return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
                return true;
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        Vector2[] SensorsWhenHorizontal() => new []
        {
            new Vector2(hitboxRect.xMin,     hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.xMax,     hitboxRect.center.y)
        };
        Vector2[] SensorsWhenVertical() => new []
        {
            new Vector2(hitboxRect.center.x, hitboxRect.yMin),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.yMax)
        };

        var sensors =
            (this.rotation == 0 || this.rotation == 180) ?
            SensorsWhenHorizontal() :
            SensorsWhenVertical();

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }
}
