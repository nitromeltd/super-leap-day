using UnityEngine;
using System;

public class GravityDisplay : Item
{
    [NonSerialized] public GameObject down;
    [NonSerialized] public GameObject left;
    [NonSerialized] public GameObject up;
    [NonSerialized] public GameObject right;
    [NonSerialized] public GameObject off;
    public Animated.Animation downAnimation;
    public Animated.Animation leftAnimation;
    public Animated.Animation upAnimation;
    public Animated.Animation rightAnimation;

    public override void Init(Def def)
    {
        base.Init(def);

        this.down  = this.transform.Find("down").gameObject;
        this.left  = this.transform.Find("left").gameObject;
        this.up    = this.transform.Find("up").gameObject;
        this.right = this.transform.Find("right").gameObject;
        this.off   = this.transform.Find("off").gameObject;
        this.down .GetComponent<Animated>().PlayAndLoop(this.downAnimation);
        this.left .GetComponent<Animated>().PlayAndLoop(this.leftAnimation);
        this.up   .GetComponent<Animated>().PlayAndLoop(this.upAnimation);
        this.right.GetComponent<Animated>().PlayAndLoop(this.rightAnimation);
    }

    public override void Advance()
    {
        base.Advance();

        var playerChunk = this.map.NearestChunkTo(this.map.player.position);
        var dir = playerChunk.gravityDirection;

        this.down .SetActive(dir == Player.Orientation.Normal);
        this.left .SetActive(dir == Player.Orientation.LeftWall);
        this.up   .SetActive(dir == Player.Orientation.Ceiling);
        this.right.SetActive(dir == Player.Orientation.RightWall);
        this.off  .SetActive(dir == null);
    }
}
