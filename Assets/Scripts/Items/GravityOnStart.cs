using UnityEngine;

public class GravityOnStart : Item
{
    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.tile.Contains("none"))
        {
            this.chunk.gravityDirection = null;
        }
        else
        {
            switch (def.tile.rotation)
            {
                case 0:
                    this.chunk.gravityDirection = Player.Orientation.Ceiling;
                    break;
                case 90:
                    this.chunk.gravityDirection = Player.Orientation.LeftWall;
                    break;
                case 180:
                    this.chunk.gravityDirection = Player.Orientation.Normal;
                    this.chunk.ReportError(
                        this.position, "No need to indicate normal gravity"
                    );
                    break;
                case 270:
                    this.chunk.gravityDirection = Player.Orientation.RightWall;
                    break;
            }
        }

        this.chunk.initialGravityDirection = this.chunk.gravityDirection;
    }
}
