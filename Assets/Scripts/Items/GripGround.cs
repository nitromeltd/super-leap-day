﻿using System;
using UnityEngine;

public class GripGround : Item
{
    public enum Shape { Square, CurveBL3Plus1, CurveOut3 }

    private Shape shape;

    public Sprite squareCornerSprite;
    public Sprite squareInsideCornerSprite;
    public Sprite squareThinSprite;
    public Sprite squareThinEndSprite;

    public Sprite litSquareCornerSprite;
    public Sprite litSquareThinSprite;
    public Sprite litSquareThinEndSprite;

    [System.Serializable]
    public class SmallLight
    {
        public SpriteRenderer spriteRenderer;
        [NonSerialized] public int litRemainingTime;
    }
    public SmallLight[] smallLights;

    public static readonly int LightTime = 35;

    public class CurveBL3Plus1HitboxShape : Hitbox.CustomShape
    {
        public override bool IsSolidAtPoint(Vector2 relativePosition) =>
            relativePosition.sqrMagnitude >= 3 * 3;

        public override float SurfaceAngleDegrees(
            Vector2 relativePosition, Vector2 incomingRayDirection
        )
        {
            var angleRadians = Mathf.Atan2(relativePosition.y, relativePosition.x);
            var angleDegrees = Util.WrapAngle0To360(
                (angleRadians * 180 / Mathf.PI) + 90
            );

            if (Mathf.Abs((angleDegrees % 90) - 45) < 5)
                angleDegrees = Mathf.Round(angleDegrees / 45) * 45;

            return angleDegrees;
        }
    }

    public class CurveOut3HitboxShape : Hitbox.CustomShape
    {
        public override bool IsSolidAtPoint(Vector2 relativePosition) =>
            relativePosition.sqrMagnitude <= 3 * 3;

        public override float SurfaceAngleDegrees(
            Vector2 relativePosition, Vector2 incomingRayDirection
        )
        {
            var angleRadians = Mathf.Atan2(relativePosition.y, relativePosition.x);
            var angleDegrees = Util.WrapAngle0To360(
                (angleRadians * 180 / Mathf.PI) + 270
            );

            if (Mathf.Abs((angleDegrees % 90) - 45) < 5)
                angleDegrees = Mathf.Round(angleDegrees / 45) * 45;

            return angleDegrees;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.tile.Contains("3_plus_1"))
        {
            this.shape = Shape.CurveBL3Plus1;

            Hitbox hitbox;
            if (def.tile.flip == true)
            {
                hitbox = new Hitbox(
                    -4, 0, -4, 0, this.rotation, Hitbox.Solid, true, true
                );
            }
            else
            {
                hitbox = new Hitbox(
                    0, 4, -4, 0, this.rotation, Hitbox.Solid, true, true
                );
            }
            hitbox.customShape = new CurveBL3Plus1HitboxShape();
            this.hitboxes = new [] { hitbox };

            if (def.tile.flip == true)
                this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else if (def.tile.tile.Contains("out_3"))
        {
            this.shape = Shape.CurveOut3;

            Hitbox hitbox;
            if (def.tile.flip == true)
            {
                hitbox = new Hitbox(
                    0, 3, 0, 3, this.rotation, Hitbox.Solid, true, true
                );
            }
            else
            {
                hitbox = new Hitbox(
                    -3, 0, 0, 3, this.rotation, Hitbox.Solid, true, true
                );
            }
            hitbox.customShape = new CurveOut3HitboxShape();
            this.hitboxes = new [] { hitbox };

            if (def.tile.flip == true)
                this.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            this.shape = Shape.Square;

            var hitbox = new Hitbox(
                -0.5f, 0.5f, -0.5f, 0.5f, 0, Hitbox.Solid, true, true
            );
            this.hitboxes = new [] { hitbox };
        }

        this.onCollisionFromLeft = (_) => OnCollision();
        this.onCollisionFromRight = (_) => OnCollision();
        this.onCollisionFromAbove = (_) => OnCollision();
        this.onCollisionFromBelow = (_) => OnCollision();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public void Autotile()
    {
        if (this.shape == Shape.Square)
        {
            bool IsFilled(int tx, int ty)
            {
                var t = this.chunk.TileAt(tx, ty, Layer.A);
                if (t != null && t.spec.shape != Tile.Shape.TopLine) return true;

                var i = this.chunk.ItemAt<GripGround>(tx, ty);
                if (i != null) return true;

                return false;
            }

            var t = IsFilled(def.tx, def.ty + 1);
            var b = IsFilled(def.tx, def.ty - 1);
            var l = IsFilled(def.tx - 1, def.ty);
            var r = IsFilled(def.tx + 1, def.ty);
            var tl = IsFilled(def.tx - 1, def.ty + 1);
            var tr = IsFilled(def.tx + 1, def.ty + 1);
            var bl = IsFilled(def.tx - 1, def.ty - 1);
            var br = IsFilled(def.tx + 1, def.ty - 1);

            if (tl == true && t == true && l == true && r == false && b == false)
            {
                this.spriteRenderer.sprite = this.squareCornerSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 270);
            }
            else if (tr == true && t == true && r == true && l == false && b == false)
            {
                this.spriteRenderer.sprite = this.squareCornerSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 180);
            }
            else if (bl == true && b == true && l == true && r == false && t == false)
            {
                this.spriteRenderer.sprite = this.squareCornerSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (br == true && b == true && r == true && l == false && t == false)
            {
                this.spriteRenderer.sprite = this.squareCornerSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
            }

            else if (t == true && r == true && tr == false)
            {
                this.spriteRenderer.sprite = this.squareInsideCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (t == true && l == true && tl == false)
            {
                this.spriteRenderer.sprite = this.squareInsideCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
            }
            else if (b == true && r == true && br == false)
            {
                this.spriteRenderer.sprite = this.squareInsideCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 270);
            }
            else if (b == true && l == true && bl == false)
            {
                this.spriteRenderer.sprite = this.squareInsideCornerSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 180);
            }

            else if (t == true && b == true && l == false && r == false)
            {
                this.spriteRenderer.sprite = this.squareThinSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (t == false && b == false && l == true && r == true)
            {
                this.spriteRenderer.sprite = this.squareThinSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
            }

            else if (t == true && b == false && l == false && r == false)
            {
                this.spriteRenderer.sprite = this.squareThinEndSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinEndSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
            else if (t == false && b == true && l == false && r == false)
            {
                this.spriteRenderer.sprite = this.squareThinEndSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinEndSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 180);
            }
            else if (t == false && b == false && l == true && r == false)
            {
                this.spriteRenderer.sprite = this.squareThinEndSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinEndSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
            }
            else if (t == false && b == false && l == false && r == true)
            {
                this.spriteRenderer.sprite = this.squareThinEndSprite;
                this.smallLights[0].spriteRenderer.sprite = this.litSquareThinEndSprite;
                this.transform.rotation = Quaternion.Euler(0, 0, 270);
            }

            else if (b == false && t == true)
                this.transform.rotation = Quaternion.Euler(0, 0, 270);
            else if (l == true && r == false)
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
            else if (l == false && r == true)
                this.transform.rotation = Quaternion.Euler(0, 0, 180);
            else
                this.transform.rotation = Quaternion.Euler(0, 0, 90);
        }
    }

    private void OnCollision()
    {
        foreach (var light in this.smallLights)
        {
            bool activate;
            if (this.shape == Shape.Square)
            {
                activate = true;
            }
            else
            {
                Vector2 lightPosition =
                    light.spriteRenderer.gameObject.transform.position;
                Vector2 playerToLight = lightPosition - this.map.player.position;
                activate = (playerToLight.sqrMagnitude < 1.25f * 1.25f);
            }

            if (activate == true)
            {
                light.spriteRenderer.gameObject.SetActive(true);
                light.litRemainingTime = LightTime;
            }
        }
        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvGripGroundsLoop))
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvGripGroundsLoop,
                position: this.position
            );
        }
    }

    public override void Advance()
    {
        foreach (var light in this.smallLights)
        {
            light.litRemainingTime -= 1;

            if (light.litRemainingTime == 1)
                light.spriteRenderer.color = new Color(1, 1, 1, 0.5f);
            else
                light.spriteRenderer.color = Color.white;

            light.spriteRenderer.gameObject.SetActive(light.litRemainingTime > 0);
        }
    }
}
