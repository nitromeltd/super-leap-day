
using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Guillotine : Item
{
    public Transform postsParentTransform;

    public Transform ropeParentTransform;
    public Animated firstRopeAnimated;
    public GameObject ropeSegmentPrefab;

    public Transform extensionParentTransform;
    public GameObject extensionSegmentPrefab;
    
    public Transform bottomTransform;

    public Transform sparkleParticlesParent;
    public Transform[] sparklePositions;

    [Header("Animations")]
    public Animated bladeAnimated;
    public Animated.Animation bladeIdleAnimation;
    public Animated.Animation bladeShineAnimation;

    public Animated.Animation ropeTautAnimation;
    public Animated.Animation ropeDropAnimation;

    public Animated.Animation sparkleAnimation;

    private enum State
    {
        Wait,
        Drop,
        StuckInGround,
        Raise
    }

    private State state;
    private float velocityY;
    private int stuckInGroundTime;
    private Raycast.CombinedResults downRaycast;
    private Vector2 initialPosition;
    private List<Animated> ropes = new List<Animated>();
    private int shineTimer = 0;

    private float BladeSolidY => this.position.y + BladeSolidOffsetMinY;
    private float BottomY => this.downRaycast.End().y;

    private const float RopeLength = 2.125f;
    private const float MaxRopeHeightForMask = 3.8f;

    private const float BladeSolidMinY = -0.75f;
    private const float BladeSolidHeight = 0.75f;

    private const float BladeMinY = -3.25f;
    private const float BladeHeight = 2.5f;

    private const float BladeSolidOffsetMinY = 0.9f;
    
    private const int ShineTotalTime = 30 * 5;

    public override void Init(Def def)
    {
        base.Init(def);

        this.initialPosition = this.position;

        Hitbox bladeSolidHitbox = new Hitbox(
            -1.25f, 
            1.25f, 
            BladeSolidMinY, 
            BladeSolidMinY + BladeSolidHeight,
            this.rotation,
            Hitbox.Solid,
            wallSlideAllowedOnLeft: true,
            wallSlideAllowedOnRight: true
        );

        Hitbox bladeHitbox = new Hitbox(
            -1.1f, 
            1.1f, 
            BladeMinY, 
            BladeMinY + BladeHeight,
            this.rotation,
            Hitbox.NonSolid
        );

        this.hitboxes = new [] { bladeSolidHitbox, bladeHitbox };
        
        this.postsParentTransform.gameObject.name = $"Guillotine Posts from {this.gameObject.name}";
        this.postsParentTransform.SetParent(this.transform.parent);

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        if (this.extensionParentTransform != null)
            AddExtensions();

        AddRopeSegments();
    }

    private void AddExtensions()
    {
        // segments
        Vector2 initialExtensionPos =
            this.extensionParentTransform.position + (Vector3.up * 0.50f);

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        Raycast.Result r = raycast.Vertical(initialExtensionPos, false, 32f);

        int numberSegments = Mathf.RoundToInt(r.distance);

        for (int i = 0; i < numberSegments; i++)
        {
            GameObject segment = Instantiate(this.extensionSegmentPrefab);
            segment.transform.SetParent(this.extensionParentTransform);
            segment.transform.localPosition = Vector3.down * i;
        }

        // bottoms
        this.bottomTransform.localPosition = Vector2.down * (r.distance - 6);
    }

    private void AddRopeSegments()
    {
        this.ropes.Add(this.firstRopeAnimated);

        int numberRopes = 15; // this number plucked from thin air because idk
        
        for (int i = 0; i < numberRopes; i++)
        {
            GameObject rope = Instantiate(this.ropeSegmentPrefab);
            rope.transform.SetParent(this.ropeParentTransform);

            int index = i + 1;
            rope.transform.localPosition = Vector3.up * RopeLength * index;

            this.ropes.Add(rope.GetComponent<Animated>());
        }
    }

    public override void Advance()
    {
        if(this.transform.parent != this.postsParentTransform)
        {
            this.transform.SetParent(this.postsParentTransform);
        }

        Vector2 downRaycastStart = this.initialPosition + (Vector2.down * 3f);

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this, isCastByEnemy: true);
        this.downRaycast = Raycast.CombineClosest(
            raycast.Vertical(downRaycastStart + new Vector2(-1.25f, 0), false, 16),
            raycast.Vertical(downRaycastStart + new Vector2(-0.6f, 0), false, 16),
            raycast.Vertical(downRaycastStart + new Vector2(    0, 0), false, 16),
            raycast.Vertical(downRaycastStart + new Vector2( 0.6f, 0), false, 16),
            raycast.Vertical(downRaycastStart + new Vector2( 1.25f, 0), false, 16)
        );

        switch (this.state)
        {
            case State.Wait:
                AdvanceWait();
                break;

            case State.Drop:
                AdvanceDrop();
                break;

            case State.StuckInGround:
                AdvanceStuckInGround();
                break;

            case State.Raise:
                AdvanceRaise();
                break;
        }

        CheckForPlayer();
        CheckForEnemies();
        UpdateShine();

        this.transform.position = this.position;
        UpdateRopesPosition();
    }

    private void UpdateShine()
    {
        if(this.shineTimer > 0)
        {
            this.shineTimer--;
        }
        else
        {
            this.shineTimer = ShineTotalTime;

            this.bladeAnimated.PlayOnce(this.bladeShineAnimation, () => 
            {
                this.bladeAnimated.PlayAndHoldLastFrame(this.bladeIdleAnimation);
            });

            StartCoroutine(_CreateSparkleParticles());
        }
    }

    private IEnumerator _CreateSparkleParticles()
    {
        foreach(var sparklePos in this.sparklePositions)
        {
            Particle sparkle = Particle.CreateAndPlayOnce(
                this.sparkleAnimation,
                sparklePos.position,
                this.sparkleParticlesParent
            );

            sparkle.spriteRenderer.sortingLayerName = "Items";
            sparkle.spriteRenderer.sortingOrder = 10;

            yield return new WaitForSeconds(0.09f);
        }
    }

    private void UpdateRopesPosition()
    {
        for (int i = 0; i < this.ropes.Count; i++)
        {
            float positionY = MaxRopeHeightForMask + (RopeLength * i);

            float bladePositionX = this.bladeAnimated.transform.position.x;
            float bladePositionY = this.bladeAnimated.transform.position.y + positionY;

            if(bladePositionY > this.postsParentTransform.transform.position.y + 6.425f)
            {
                bladePositionY = this.postsParentTransform.transform.position.y + 6.425f;
            }

            this.ropes[i].transform.position =
                new Vector3(bladePositionX, bladePositionY);
        }
    }

    private void CheckForPlayer()
    {
        var playerRect = this.map.player.Rectangle();
        if (this.hitboxes[1].InWorldSpace().Overlaps(playerRect))
        {
            this.map.player.Hit(this.position);
        }
    }

    private void CheckForEnemies()
    {
        foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
        {
            if(enemy.hitboxes != null && enemy.hitboxes.Length >= 1)
            {
                if (this.hitboxes[1].InWorldSpace().Overlaps(enemy.hitboxes[0].InWorldSpace()))
                {
                    enemy.Kill(
                        new Enemy.KillInfo { itemDeliveringKill = this }
                    );
                }
            }
        }
    }

    private void AdvanceWait()
    {
        var playerPosition = this.map.player.position;
        if (playerPosition.y > BottomY &&
            playerPosition.y < this.initialPosition.y &&
            playerPosition.x > this.initialPosition.x - 1.25f &&
            playerPosition.x < this.initialPosition.x + 1.25f)
        {
            ChangeState(State.Drop);
        }
    }

    private void AdvanceDrop()
    {
        this.velocityY -= 0.012f;
        this.position += new Vector2(0, this.velocityY);
        if (BladeSolidY < BottomY)
        {
            Audio.instance.PlaySfx(
                Assets.instance.tombstoneHill.sfxGuillotineLand, null, this.position
            );
            ChangeState(State.StuckInGround);
        }
    }

    private void AdvanceStuckInGround()
    {
        this.stuckInGroundTime += 1;
        if (this.stuckInGroundTime > 30)
        {
            this.stuckInGroundTime = 0;
            ChangeState(State.Raise);
        }
    }

    private void AdvanceRaise()
    {
        this.position.y += 0.06f;
        if (this.position.y >= this.initialPosition.y)
        {
            ChangeState(State.Wait);
        }
    }

    private void ChangeState(State newState)
    {
        this.state = newState;

        switch(newState)
        {
            case State.Wait:
                EnterWait();
            break;

            case State.Drop:
                EnterDrop();
            break;

            case State.StuckInGround:
                EnterStuckToGround();
            break;

            case State.Raise:
                EnterRaise();
            break;
        }
    }

    private void EnterWait()
    {
        this.position.y = this.initialPosition.y;
    }

    private void EnterDrop()
    {
        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxGuillotineDrop, null, this.position
        );

        foreach(var rope in this.ropes)
        {
            rope.PlayAndLoop(this.ropeDropAnimation);
        }
    }

    private void EnterStuckToGround()
    {
        this.position.y = BottomY + BladeSolidOffsetMinY;
        this.velocityY = 0;

        this.map.ScreenShakeAtPosition(this.position, Vector2.down);
        
        foreach(var rope in this.ropes)
        {
            rope.PlayAndHoldLastFrame(this.ropeTautAnimation);
        }
    }

    private void EnterRaise()
    {
        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxGuillotineRise, null, this.position
        );
    }
}
