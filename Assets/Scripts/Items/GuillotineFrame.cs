using UnityEngine;

public class GuillotineFrame : Item
{
    public enum Type { LeftPillar, TopMiddle, RightPillar }

    public Type type;
    public Sprite[] pillarSprites;
    public Sprite pillarBottomSprite;
    public Sprite topPartExtensionSprite;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);

        var raycast = new Raycast(this.map, this.position);
        var result = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-0.3f, 0), false, 32),
            raycast.Vertical(this.position.Add(+0.3f, 0), false, 32)
        );

        if (this.type == Type.LeftPillar || this.type == Type.RightPillar)
        {
            for (int n = 0; n < result.distance - 1; n += 1)
            {
                var gameObject = new GameObject($"Pillar {n}");
                gameObject.transform.SetParent(this.transform, false);
                gameObject.transform.localPosition = new Vector3(0, -n, 0);
                var sr = gameObject.AddComponent<SpriteRenderer>();
                sr.sprite = Util.RandomChoice(this.pillarSprites);
                sr.sortingOrder = -1;
            }

            {
                var gameObject = new GameObject("Pillar Bottom");
                gameObject.transform.SetParent(this.transform, false);
                gameObject.transform.localPosition = new Vector3(0, -result.distance, 0);
                var sr = gameObject.AddComponent<SpriteRenderer>();
                sr.sprite = Util.RandomChoice(this.pillarBottomSprite);
                sr.sortingOrder = -1;
            }
        }

        if (this.type != Type.RightPillar)
        {
            var next = NextPieceOnRight(raycast);
            if (next != null)
                ConnectAlongTop(next);
        }
    }

    private GuillotineFrame NextPieceOnRight(Raycast raycast)
    {
        var maxDistance = Mathf.FloorToInt(
            raycast.Horizontal(this.position, true, 32f).distance - 0.5f
        );

        for (int n = 1; n <= maxDistance; n += 1)
        {
            var g = this.chunk.ItemAt<GuillotineFrame>(this.def.tx + n, this.def.ty);
            if (g != null)
                return g;
        }

        return null;
    }
    
    private void ConnectAlongTop(GuillotineFrame next)
    {
        int minTx = this.def.tx + ((this.type == Type.TopMiddle) ? 3 : 1);
        int maxTx = next.def.tx + ((next.type == Type.TopMiddle) ? -3 : -1);

        for (int tx = minTx; tx <= maxTx; tx += 1)
        {
            var gameObject = new GameObject("Extension");
            gameObject.transform.SetParent(this.transform, false);
            gameObject.transform.localPosition = new Vector3(tx - this.def.tx, 0, 0);
            var sr = gameObject.AddComponent<SpriteRenderer>();
            sr.sprite = Util.RandomChoice(this.topPartExtensionSprite);
            sr.sortingOrder = -1;
        }
    }
}
