
using UnityEngine;

public class HangPoint : Item
{
    public int ignoreTime = 0;

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.grabInfo = new GrabInfo(this, GrabType.Ceiling, new Vector2(0, -2));
    }

    public override void Advance()
    {
        AdvanceHanging();
    }

    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.grabbingOntoItem != null) return;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var thisPosition = this.position + new Vector2(0, -2);
        var delta = player.position - thisPosition;
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.grabbingOntoItem = this;
            this.ignoreTime = 15;
        }
    }
}
