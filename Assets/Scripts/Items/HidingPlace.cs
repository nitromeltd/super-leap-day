﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Rnd = UnityEngine.Random;
public class HidingPlace : Item
{
    public enum HidingPlaceType
    {
        Barrel,
        Cactus,
        Locker
    }

    public HidingPlaceType type;


    public Animated.Animation animationEnter;
    public Animated.Animation animationBlink;
    public Animated.Animation animationExit;
    public Animated.Animation animationGoingUp;
    public Animated.Animation animationGoingDown;
    public Animated.Animation animationHitTheGround;
    public Animated.Animation animationIdle;
    public Animated.Animation animationEmptyIdle;
    public Animated.Animation animationArrowBobbing;

    public Animated arrowAnimated;
    public GameObject cactusLid;

    private const float jumpVerticalVelocity = 0.3f;
    private const float jumpHorizontalVelocity = 0.1f;
    public Vector2 velocity;
    private bool facingRight;
    private const float GravityForce = 0.02f;
    private int hidingPlaceReenterDelayCounter;
    private int hidingPlaceWobbleDelayCounter;
    private int cactusLidWobbleCounter;
    private const int CactusLidWobbleMax = 20;
    private const float CactusLidWobbleOffset= 0.3f;
    private const int HidingPlaceReenterDelay = 120;
    private const int HidingPlaceWobbleDelay = 120;
    private int blinkDelayCounter;
    private bool lastOnGround;
    private bool onGround;
    private Vector3 cactusLidInititialLocalPosition;
    private int leaveHidingSpaceDelayCounter;

    public override void Init(Def def)
    {
        base.Init(def);
        this.facingRight = !def.tile.flip;
        if(!this.facingRight)
        {
            this.transform.localScale = new Vector3(-1,1,1);
        }

        this.solidToEnemy = false;
        
        if (this.type == HidingPlaceType.Barrel)
        {
            this.hitboxes = new[] {
            new Hitbox(-1.0f, 1.0f, -1.3f, 1.2f, this.rotation, Hitbox.NonSolid)
            };
        }
        else if (this.type == HidingPlaceType.Cactus)
        {
            this.hitboxes = new[] {
            new Hitbox(-1.0f, 1.0f, -0.8f, 1.0f, this.rotation, Hitbox.NonSolid)
            };
            cactusLidInititialLocalPosition = cactusLid.transform.localPosition;
            AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        }
        else if (this.type == HidingPlaceType.Locker)
        {
            this.hitboxes = new[] {
            new Hitbox(-1.0f, 1.0f, -2.0f, 1.5f, this.rotation, Hitbox.NonSolid)
            };
            
        }
        
        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        arrowAnimated.PlayAndLoop(animationArrowBobbing);
        animated.PlayOnce(animationEmptyIdle);
    }

    private void CheckIfPlayerGetsInsideHidingPlace()
    {
        if(leaveHidingSpaceDelayCounter > 0)
        {
            leaveHidingSpaceDelayCounter--;
        }

        if(this.hidingPlaceReenterDelayCounter > 0)
        {
            this.hidingPlaceReenterDelayCounter--;
            return;
        }

        if(this.type == HidingPlaceType.Locker)
        {
            if(this.map.player.state != Player.State.InsideBox && GameInput.Player(this.map).pressedJump == true)
            {           
                if(this.hitboxes[0].InWorldSpace().Contains(this.map.player.position))
                {
                    EnterHidingPlace();
                }
            }
            return;
        }

        var player = this.map.player;

        if (this.rotation == 0)
        {
            bool isFallingInsideHidingPlace = (Mathf.Abs(player.position.x - this.position.x) < 1.25f) && (player.velocity.y < 0) && (player.position.y - this.position.y > 1.5f && player.position.y - this.position.y < 2f);

            if ((player.state == Player.State.Normal || player.state == Player.State.Jump)
                && isFallingInsideHidingPlace)
            {
                EnterHidingPlace();
            }
        }
        else if (this.rotation == 180)
        {
            bool isFallingInsideHidingPlace = (Mathf.Abs(player.position.x - this.position.x) < 1.25f) && (player.velocity.y > 0) && (player.position.y - this.position.y < -1.5f && player.position.y - this.position.y > -2f);

            if ((player.state == Player.State.Normal || player.state == Player.State.Jump)
                && isFallingInsideHidingPlace)
            {
                EnterHidingPlace();
            }
        }
        else if(this.rotation == 90)
        {
            bool isFallingInsideHidingPlace = (Mathf.Abs(player.position.y - this.position.y) < 1.25f) && (player.velocity.x > 0) && (player.position.x - this.position.x < -1.5f && player.position.x - this.position.x > -2f);

            if ((player.state == Player.State.Normal || player.state == Player.State.Jump)
                && isFallingInsideHidingPlace)
            {
                EnterHidingPlace();
            }
        }
        else if(this.rotation == 270)
        {
            bool isFallingInsideHidingPlace = (Mathf.Abs(player.position.y - this.position.y) < 1.25f) && (player.velocity.x < 0) && (player.position.x - this.position.x > 1.5f && player.position.x - this.position.x < 2f);

            if ((player.state == Player.State.Normal || player.state == Player.State.Jump)
                && isFallingInsideHidingPlace)
            {
                EnterHidingPlace();
            }
        }
    }

    private void EnterHidingPlace()
    {
        leaveHidingSpaceDelayCounter = 5;

        if(this.type == HidingPlaceType.Locker)
        {
            this.map.player.EnterLocker(this.position + new Vector2(0,this.hitboxes[0].yMin + Player.PivotDistanceAboveFeet));
            this.animated.PlayOnce(this.animationEnter);
            this.animated.OnFrame(2, ()=> {this.map.player.EnterHidingPlace(this);});
            CreateLidParticles();
        }
        else
        {
            this.map.player.EnterHidingPlace(this);
            this.animated.PlayOnce(this.animationEnter);
            CreateLidParticles();
        }

        

        if(this.type == HidingPlaceType.Cactus)
        {
            cactusLidWobbleCounter = CactusLidWobbleMax;
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvHidezoneCactusEnter,
                position: this.position
            );
        }
        else
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvHidezoneMetalEnter,
                position: this.position
            );
        }
    }

    public void CheckIfHidingPlaceShouldWobble()
    {
        if(hidingPlaceWobbleDelayCounter > 0)
        {
            hidingPlaceWobbleDelayCounter--;
            return;
        }
        var player = this.map.player;
        if(player.state == Player.State.Normal)
        {
            if (this.type == HidingPlaceType.Barrel)
            {
                if ((player.position - this.position).sqrMagnitude < 1 * 1)
                {
                    hidingPlaceWobbleDelayCounter = HidingPlaceWobbleDelay;
                    this.velocity = this.velocity + new Vector2(0, 0.1f);
                }
            }
            else if (this.type == HidingPlaceType.Cactus)
            {
                // do nothing, cactus doesnt move (never did)
            }
            else if (this.type == HidingPlaceType.Locker)
            {
                if ((player.position - this.position).sqrMagnitude < 2 * 2)
                {
                    hidingPlaceWobbleDelayCounter = HidingPlaceWobbleDelay;
                    this.velocity = this.velocity + new Vector2(0, 0.1f);
                }
            }
        }
    }

    public override void Advance()
    {
        UpdateArrow();
        CheckIfPlayerGetsInsideHidingPlace();
        CheckIfHidingPlaceShouldWobble();
        AdvanceFall();
        CheckAndAdvanceCactusLid();

        if(this.velocity.x > 0)
        {
            this.facingRight = true;
            this.transform.localScale = new Vector3(1,1,1);
        }
        else if(this.velocity.x < 0)
        {
            this.facingRight = false;
            this.transform.localScale = new Vector3(-1,1,1);
        }

        CheckForBlink();

        if(this.map.player.state == Player.State.InsideBox && this.map.player.GetHidingPlace() == this)
        {
            this.map.player.position = this.position;
        }

        SetCorrectAnimation();


// #if UNITY_EDITOR
//         if(Input.GetKeyDown(KeyCode.P))
//         {
//             MakeBoxJump();
//         }
// #endif        
    }

    private void CheckAndAdvanceCactusLid()
    {
        if(this.type != HidingPlaceType.Cactus || this.cactusLidWobbleCounter == 0)
        {
            return;
        }

        this.cactusLidWobbleCounter--;

        this.cactusLid.transform.localPosition = this.cactusLidInititialLocalPosition + new Vector3(0, CactusLidWobbleOffset * Mathf.Sin(((CactusLidWobbleMax - this.cactusLidWobbleCounter) * 180 * Mathf.Deg2Rad) / CactusLidWobbleMax), 0);

        if(this.cactusLidWobbleCounter > 2 * CactusLidWobbleMax / 3)
        {
            this.cactusLid.transform.localRotation = Quaternion.Euler(0, 0, 10);
        }
        else if(cactusLidWobbleCounter > CactusLidWobbleMax / 3)
        {
            this.cactusLid.transform.localRotation = Quaternion.Euler(0, 0, -10);
        }
        else
        {
            this.cactusLid.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }
    }


    private void UpdateArrow()
    {
        if(this.map.player.state == Player.State.InsideBox && this.map.player.GetHidingPlace() == this)
        {
            if (this.arrowAnimated.gameObject.activeSelf)
            {
                this.arrowAnimated.gameObject.SetActive(false);
            }
        }
        else
        {
            if(!arrowAnimated.gameObject.activeSelf)
            {
                this.arrowAnimated.gameObject.SetActive(true);
            }
        }
    }

    private void CheckForBlink()
    {
        if(!(this.map.player.state == Player.State.InsideBox) || this.map.player.GetHidingPlace() != this)
        {
            return;
        }

        if(--this.blinkDelayCounter < 0)
        {
            this.blinkDelayCounter = Random.Range(180, 240);
            this.animated.PlayOnce(this.animationBlink);
        }
    }

    private void AdvanceFall()
    {
        if (this.type == HidingPlaceType.Cactus)
        {
            if (this.group != null)
                this.transform.position = this.position = this.group.FollowerPosition(this);
            return;
        }

        this.velocity.y -= (this.lastOnGround ? 5:1) * GravityForce;
        
        // ignore self collision (rectangle is solid)
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        MoveVertically(raycast);
        MoveHorizontally(raycast);
        this.transform.position = this.position;
        CheckIfGrounded(raycast);
    }

    private void CheckIfGrounded(Raycast raycast)
    {
        this.lastOnGround = this.onGround;
        var rect = this.hitboxes[0];
        var maxDistance = Mathf.Abs(this.velocity.y) + Mathf.Abs(rect.yMin) + 0.1f;

        var result = raycast.Vertical(this.position, false, maxDistance);
        if (result.anything == true && result.distance <= maxDistance)
        {
            //we are grounded or about to get grounded
            this.onGround = true;
        }
        else
        {
            this.onGround = false;
        }

        if(this.lastOnGround && this.onGround && this.velocity.y <= 0)
        {
            this.velocity.x *= 0.5f;
        }
    }

    private void SetCorrectAnimation()
    {
        if(hidingPlaceWobbleDelayCounter > 0)
        {
            return;
        }

        if(animated.currentAnimation != this.animationEnter && animated.currentAnimation != this.animationExit)
        {
            if(!this.lastOnGround && this.onGround)
            {
                if(!(this.map.player.state == Player.State.InsideBox))
                {
                    animated.PlayOnce(animationEmptyIdle);
                }
                else
                {
                    animated.PlayOnce(animationHitTheGround, () => { animated.PlayOnce(animationIdle); });
                }
                CreateLandingParticles();
            }
            else if(!this.onGround && this.velocity.y > 0 && this.map.player.GetHidingPlace() == this)
            {
                animated.PlayOnce(animationGoingUp);
            }
            else if(!this.onGround && this.velocity.y < 0 && this.map.player.GetHidingPlace() == this)
            {
                animated.PlayOnce(animationGoingDown);
            }
        }
    }

    private void MoveHorizontally(Raycast raycast)
    {
        var rect = this.hitboxes[0];        

        if(this.velocity.x > 0)
        {
            var maxDistanceHorizontal = this.velocity.x + rect.xMax;
            var resultHorizontal = raycast.Horizontal(this.position, true, maxDistanceHorizontal);
            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= this.velocity.x + rect.xMax)
            {
                this.position.x += resultHorizontal.distance - rect.xMax;
                this.velocity.x = -this.velocity.x;                
            }
            else
            {
                this.position.x += this.velocity.x;
            }

        }
        else if (this.velocity.x < 0)
        {
            var maxDistanceHorizontal = -(this.velocity.x + rect.xMin);
            var resultHorizontal = raycast.Horizontal(this.position, false, maxDistanceHorizontal);
            if (resultHorizontal.anything == true &&
                    resultHorizontal.distance <= -(this.velocity.x + rect.xMin))
            {
                this.position.x += -(resultHorizontal.distance + rect.xMin);
                this.velocity.x = -this.velocity.x;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }        
    }

    protected void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.5f, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0f), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.5f, 0f), false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                if (result.tiles.Length > 0)
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                foreach (var item in result.items)
                {
                    var conveyor = item as Conveyor;
                    if (conveyor != null)
                    {
                        surfaceVelocity.x = conveyor.right ? conveyor.ConveyorSpeed : -conveyor.ConveyorSpeed;
                        surfaceVelocity.y = 0;
                    }
                }

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void AdvancePlayer()
    {
        
    }

    public bool IsPlayerDetectableInHidingPlace()
    {
        if(this.velocity.magnitude > 0.1f)
        {
            return true;
        }

        return false;
    }

    public void ExitHidingPlaceCleanup()
    {
        this.hidingPlaceReenterDelayCounter = HidingPlaceReenterDelay;

        if(this.type == HidingPlaceType.Locker)
        {
            this.map.player.velocity = new Vector2(this.map.player.facingRight ? 0.2f : -0.2f, 0.0f);
            this.map.player.position = this.position + new Vector2(0, -1.5f);
        }
        else
        {
            if (this.rotation == 0)
            {
                this.map.player.velocity = new Vector2(this.map.player.facingRight ? 0.2f : -0.2f, 0.3f);
                this.map.player.position = this.position + new Vector2(0, 1.5f);
            }
            else if (this.rotation == 180)
            {
                this.map.player.velocity = new Vector2(this.map.player.facingRight ? 0.2f : -0.2f, 0.0f);
                this.map.player.position = this.position - new Vector2(0, 1.5f);
            } 
            else if(this.rotation == 90)
            {
                this.map.player.velocity = new Vector2( -0.2f, 0.3f);
                this.map.player.position = this.position + new Vector2(-1.5f, 0);
                this.map.player.facingRight = false;
            }
            else if(this.rotation == 270)
            {
                this.map.player.velocity = new Vector2( 0.2f, 0.3f);
                this.map.player.position = this.position + new Vector2(1.5f, 0);
                this.map.player.facingRight = true;
            }
        }
        
        this.animated.PlayOnce(animationExit);
        
        if(type == HidingPlaceType.Cactus)
        {
            cactusLidWobbleCounter = CactusLidWobbleMax;
            CreateLidParticles();
        }
        else if(type == HidingPlaceType.Barrel)
        {
            CreateLidParticles();
        }
        else if(type == HidingPlaceType.Locker)
        {
            CreateLockerParticles();
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvBoxExit,
            position: this.position
        );
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvPipesCannon,
            position: this.position
        );
    }

    private void CreateLandingParticles()
    {
        var tangent = new Vector2(
            Mathf.Cos(0 * Mathf.PI / 180),
            Mathf.Sin(0 * Mathf.PI / 180)
        );
        var normal       = new Vector2(-tangent.y, tangent.x);

        for (var n = 0; n < 10; n += 1)
            {
                var p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position - (normal * 1) + new Vector2(this.facingRight? 0.5f: -0.5f, 0),
                    this.transform.parent
                );
                p.velocity.x =
                    ((n % 2 == 0) ? 1 : -1) *
                    Rnd.Range(0.4f, 2.3f) / 16;
                p.velocity.y = Rnd.Range(-0.2f, 0.2f) / 16;
                p.acceleration = p.velocity * -0.05f / 16;
                p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
            }
    }

    private void CreateLidParticles()
    {
        var up = new Vector2(0,1f);
        var forward = new Vector2(1f,0);
 
        for (var n = 0; n < 10; n += 1)
        {
            Particle p;
            if (this.rotation == 180)
            {
                p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position +
                        new Vector2(Rnd.Range(-1.25f, 1.25f), -0.8f),
                    this.transform.parent
                );
                float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) + this.velocity.x, this.velocity.y + Rnd.Range(0, 100) > 50 ? speed : -speed);
            }
            else if (this.rotation == 90)
            {
                p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position +
                        new Vector2(-0.8f, Rnd.Range(-1.25f, 1.25f) ),
                    this.transform.parent
                );
                float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                p.velocity = new Vector2(this.velocity.x + Rnd.Range(0, 100) > 50 ? speed : -speed, this.velocity.y + Rnd.Range(-0.01f, 0.01f) );
            }
            else if (this.rotation == 270)
            {
                p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position +
                        new Vector2(0.8f, Rnd.Range(-1.25f, 1.25f) ),
                    this.transform.parent
                );
                float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                p.velocity = new Vector2(this.velocity.x + Rnd.Range(0, 100) > 50 ? speed : -speed, this.velocity.y + Rnd.Range(-0.01f, 0.01f) );
            }
            else
            {
                // rot 0
                p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position +
                        new Vector2(Rnd.Range(-1.25f, 1.25f), 0.8f),
                    this.transform.parent
                );
                float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) + this.velocity.x, this.velocity.y + Rnd.Range(0, 100) > 50 ? speed : -speed);
            }

            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    private void CreateLockerParticles()
    {
        var up = new Vector2(0,1f);
        var forward = new Vector2(1f,0);
 
        for (var n = 0; n < 10; n += 1)
        {             
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                this.position +
                    new Vector2(Rnd.Range(-1.0f, 1.0f), Rnd.Range(-2f, 1.5f)),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            float speed = Rnd.Range(0.3f, 0.7f) / 16f;
            p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) + this.velocity.x, this.velocity.y + Rnd.Range(0, 100) > 50 ? speed : -speed);
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    public void PlayerJumped()
    {
        if (this.map.player.state == Player.State.InsideBox && this.map.player.GetHidingPlace() == this)
        {
            if(this.leaveHidingSpaceDelayCounter == 0)
            {
                this.map.player.ExitHidingPlace();
            }
        }
    }

    public override void Reset()
    {
        base.Reset();
    }
}
