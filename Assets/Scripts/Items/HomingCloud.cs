using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using Rnd = UnityEngine.Random;

public class HomingCloud : Enemy
{
    public enum State
    {
        Idle,
        Charge,
        Attack,
        Stop
    }

    public Sprite particleSprite;
    public Transform spawnPoint;
    public AnimationCurve scaleOutCurve;
    public AnimationCurve fadeOutCurve;
    public AnimationCurve attackCurve;

    private NitromeEditor.Path path;
    private Vector2 currentPoint;
    private Vector2 nextPoint;

    private Vector2 velocity;
    private Vector2 tempVelocity;
    private Vector2 surfaceVelocity;
    private bool stopMovement;
    public Animated.Animation animationIdle;
    public Animated.Animation animationCharge;
    public Animated.Animation animationAttack;
    private int chargeTime;
    private int stopTime;

    const int MaxChargeTime = 120;
    const float IdleSpeed = 0.5f / 16;
    const float MaxMovementDistance = 15f;
    private Vector2 targetPosition;

    private State state;
    private float attackProgress;
    private float lastBubbleDistance;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
                new Hitbox(-1.35f, 1.35f, -1.0f, 1.0f, this.rotation, Hitbox.NonSolid)
            };
 
        this.velocity = IdleSpeed * new Vector2(1, 1).normalized;
        this.animated.PlayAndLoop(this.animationIdle);
        this.chargeTime = 0;
        this.whenHitFromAbove = HitFromAboveBehaviour.Die;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.state = State.Idle;
        this.currentPoint = this.nextPoint = this.position;
    }

    public override void Advance()
    {
        switch(this.state)
        {
            case State.Idle:
                AdvanceIdle();
                break;

            case State.Charge:
                AdvanceCharge();
                break;    

            case State.Attack:
                AdvanceAttack();
                break;       

            case State.Stop:
                AdvanceStop();
                break;
            default:
                break;
        }
        
        HitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());        
    }

    protected void HitDetectionAgainstPlayer(Rect ownRectangle)
    {
        if (this.deathAnimationPlaying == true)
            return;

        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        if (player.ShouldCollideWithEnemies() == false)
            return;

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return;

        var playerAbove =
            playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.VelocityLastFrame.y) > ownRectangle.yMax - 0.1f;
        var enemyVelocityY = 0;
        var playerGoingDown =
            player.velocity.y < enemyVelocityY ||
            player.VelocityLastFrame.y < enemyVelocityY;

        if (player.invincibility.isActive == true)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Die)
        {
            if(this.state == State.Charge)
            {
                player.Hit((Vector2)this.transform.position);
            }
            else
            {
                player.BounceOffHitEnemy(this.transform.position);
                Hit(new KillInfo
                {
                    playerDeliveringKill = this.map.player,
                    freezeTime = true
                });
                Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
            }
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            this.whenHitFromAbove == HitFromAboveBehaviour.Nothing)
        {
            player.BounceOffHitEnemy((Vector2)this.transform.position);
        }
        else
        {
            player.Hit((Vector2)this.transform.position);
        }
    }

    private void AdvanceIdle()
    {
        if (this.map.player.alive == false) return;
        if (this.map.player.state == Player.State.Respawning) return;

        this.transform.position = this.position;
        Vector2 v = this.map.player.position - this.position;
        bool isSameChunk = Map.instance.NearestChunkTo(this.map.player.position) == this.chunk;

        if (isSameChunk == true)
        {
            this.targetPosition = this.map.player.position;
            if(v.magnitude > MaxMovementDistance)
            {
                this.targetPosition = this.position + v.normalized * MaxMovementDistance;
            }
            
            this.chargeTime = MaxChargeTime;
            this.animated.PlayAndLoop(this.animationCharge);
            this.state = State.Charge;

            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyHomingCloudElectrify,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
        }        
    }

    private void AdvanceCharge()
    {
        this.transform.position = this.position;
        this.chargeTime--;
        
        if(this.chargeTime <= 0)
        {
            AttackPlayer();
        }
    }

    private void AdvanceAttack()
    {
        if(this.nextPoint.x - this.position.x > 0)
        {
            this.transform.localScale = new Vector3(1,1,1);
        }
        else if(this.nextPoint.x - this.position.x < 0)
        {
            this.transform.localScale = new Vector3(-1,1,1);
        }       

        var d = this.nextPoint - this.currentPoint;
        this.attackProgress += 0.01f;
        float progressAlong = this.attackCurve.Evaluate(this.attackProgress);
        
        this.transform.position = this.position = this.currentPoint + progressAlong * d;

        var dist = (this.position - this.currentPoint).magnitude;
        if(dist > this.lastBubbleDistance)
        {
            CreateParticle();
            this.lastBubbleDistance += 1f;
        }

        if(progressAlong == 1)
        {
            this.position = this.currentPoint = this.nextPoint;
            this.state = State.Idle;
            this.animated.PlayAndLoop(this.animationIdle);
        }
    }

    private void AdvanceStop()
    {
        this.stopTime--;

        if(this.stopTime <= 0)
        {
            this.velocity = IdleSpeed * this.tempVelocity.normalized;
            this.state = State.Idle;
            this.animated.PlayAndLoop(this.animationIdle);
        }
    }

    private void AttackPlayer()
    {
        this.animated.PlayAndLoop(this.animationAttack);
        this.state = State.Attack;
        this.nextPoint = this.targetPosition;
        this.currentPoint = this.position;
        this.lastBubbleDistance = 0;
        this.attackProgress = 0;

        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyHomingCloudBlowAway,
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );
    }    


    private void CreateParticle()
    {
        var pos = this.spawnPoint.position;
        int r = 0;
        Particle p = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesOutline[r], 25, pos, this.map.transform);
        Particle p1 = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesBack[r], 25, pos, this.map.transform);
        Particle p2 = Particle.CreateWithSprite(Assets.instance.windySkies.cloudParticlesFront[r], 25, pos, this.map.transform);

        p1.velocity = p2.velocity = p.velocity = Vector2.zero;
        p1.acceleration = p2.acceleration = p.acceleration = Vector2.zero;

        p.spriteRenderer.sortingLayerName = p1.spriteRenderer.sortingLayerName = p2.spriteRenderer.sortingLayerName = "Clouds";
        p.spriteRenderer.sortingOrder = 0;
        p1.spriteRenderer.sortingOrder = 1;
        p2.spriteRenderer.sortingOrder = 2;

        p.ScaleOut(this.scaleOutCurve);
        p1.ScaleOut(this.scaleOutCurve);
        p2.ScaleOut(this.scaleOutCurve);

        p.FadeOut(this.fadeOutCurve);
        p1.FadeOut(this.fadeOutCurve);
        p2.FadeOut(this.fadeOutCurve);
    }
}
