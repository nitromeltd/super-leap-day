﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HotColdPlatformBlock : Item
{
    public Animated internalTextureAnimated;
    public const int InternalTextureWidth = 4;
    public const int InternalTextureHeight = 4;
    public int tx, ty, index;

    public Animated.Animation[] internalTextureAnimatedTiles = new Animated.Animation[InternalTextureWidth * InternalTextureHeight];
    public Animated.Animation[] internalTextureAnimatedTilesCold = new Animated.Animation[InternalTextureWidth * InternalTextureHeight];

    public Autotile[] autotiles;
    public Neighbours myNeighbours;

    private TouchPlatformCog leaderCog;
    private int rndValue;

    [System.Serializable]
    public struct Autotile
    {
        public Animated.Animation[] hotAnimation;
        public Animated.Animation[] coldAnimation;

        public Sprite[] hotSprite;
        public Sprite[] coldSprite;
        public bool useCorners;
        public Neighbours neighbours;

        public Sprite GetTemperatureSprite(Map.Temperature temperature, int rnd)
        {
            if(temperature == Map.Temperature.Hot)
            {
                if(hotSprite.Length > 0)
                    return hotSprite[rnd % hotSprite.Length];
            }
            else
            {
                if(coldSprite.Length > 0)
                    return coldSprite[rnd % hotSprite.Length];
            }
            return null;
        }

        public Animated.Animation GetTemperatureAnimation(
            Map.Temperature temperature, int rnd
        )
        {
            if(temperature == Map.Temperature.Hot)
            {
                if(hotAnimation.Length > 0)
                    return hotAnimation[rnd % hotAnimation.Length];
            }
            else
            {
                if(coldAnimation.Length > 0)
                    return coldAnimation[rnd % coldAnimation.Length];
            }
            return null;
        }      
    }

    [System.Serializable]
    public struct Neighbours
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool upLeft;
        public bool upRight;
        public bool downLeft;
        public bool downRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool upLeft, bool upRight, bool downLeft, bool downRight)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
            this.upLeft = upLeft;
            this.upRight = upRight;
            this.downLeft = downLeft;
            this.downRight = downRight;
        }
        
        public bool IsEqual(Neighbours n, bool useCorners = false) 
        {
            bool nonCornersEqual = 
                this.up == n.up &&
                this.down == n.down &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual = 
                this.upLeft == n.upLeft &&
                this.upRight == n.upRight &&
                this.downLeft == n.downLeft &&
                this.downRight == n.downRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);
        rndValue = Random.Range(0, 100);
        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if(this.leaderCog == null)
        {
            this.leaderCog = this.group.leader as TouchPlatformCog;
        }
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        ApplyAutotiling();
    }

    private int GetInternalTextureIndex(int x, int y)
    {
        int cx = x % InternalTextureWidth;
        int cy = y % InternalTextureHeight;
        if (cx < 0) cx += InternalTextureWidth;
        if (cy < 0) cy += InternalTextureHeight;
        cy = InternalTextureHeight - 1 - cy;
        return (cy * InternalTextureWidth) + cx;
    }

    public void ApplyAutotiling()
    {
        int index = GetInternalTextureIndex(this.def.tx, this.def.ty);
        tx = def.tx;
        ty = def.ty;
        this.index = index;

        if (this.map.currentTemperature == Map.Temperature.Hot)
        {
            this.internalTextureAnimated.PlayAndLoop(this.internalTextureAnimatedTiles[index]);
        }
        else
        {
            this.internalTextureAnimated.PlayOnce(this.internalTextureAnimatedTilesCold[index]);
        }
        
        foreach(HotColdPlatformBlock.Autotile at in autotiles)
        {
            if(myNeighbours.IsEqual(at.neighbours, at.useCorners))
            {
                var a = at.GetTemperatureAnimation(
                    this.map.currentTemperature, this.rndValue
                );
                if(a != null)
                {
                    this.animated.PlayAndLoop(a);
                }
                else
                {
                    this.spriteRenderer.sprite = at.GetTemperatureSprite(
                        this.map.currentTemperature, this.rndValue
                    );
                }
            }
        }
    }
}
