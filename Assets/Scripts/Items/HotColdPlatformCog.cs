﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class HotColdPlatformCog : Item
{
    public Transform cogTransform;

    private NitromeEditor.Path path;
    private float currentSpeed;
    private float targetSpeed;
    private HotColdPlatformBlock[,] grid;
    private float time;

    private const float DefaultSpeed = 1.00f;

    bool pathCreated = false;
    public override void Init(Def def)
    {
        base.Init(def);
        this.spriteRenderer.enabled = false;

        var node = this.chunk.pathLayer.NearestNodeTo(this.position, 0.25f);

        if(node != null)
        {
            this.path = node.path;
        }

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        NotifyChangeTemperature(this.map.currentTemperature);
        this.currentSpeed = this.targetSpeed;

        this.pathCreated = false;
    }

    public override void Advance()
    {
        if(this.pathCreated == false)
        {
            CreatePath();
            this.pathCreated = true;
        }

        this.currentSpeed = Util.Slide(this.currentSpeed, this.targetSpeed, 0.025f);

        if(this.grid == null)
        {
            SetupAutotiling();
        }

        this.time += this.currentSpeed / 60.0f;

        float rot = this.currentSpeed * 50f;
        this.cogTransform.Rotate(Vector3.forward * rot);
            
        this.position = this.path.PointAtTime(time).position;
        this.transform.position = this.position;

        foreach(var f in this.group.followers)
        {
            f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
            f.Key.item.transform.position = f.Key.item.position;
        }
    }

    public void CreatePath()
    {
        if (this.path != null)
        {
            float totalDistance = 0f;

            for (int i = 0; i < this.path.edges.Length; i++)
            {
                var edge = this.path.edges[i];

                float edgeMidDistance = totalDistance + (edge.distance / 2f);
                Vector2 edgeMidPoint = path.PointAtDistanceAlongPath(edgeMidDistance).position;

                GameObject laneGO = HotColdPlatformRail.Create(
                    edgeMidPoint, 
                    this.chunk, 
                    HotColdPlatformRail.Type.Lane, 
                    this.transform.parent
                ).gameObject;

                laneGO.transform.localRotation =
                    Quaternion.LookRotation(Vector3.forward, edge.direction);
                laneGO.transform.localScale = new Vector3(1f, 1f * edge.distance, 1f);

                totalDistance += edge.distance;

                void DrawLaneStop(Vector2 stopPosition)
                {
                    GameObject laneEndGO = HotColdPlatformRail.Create(
                        stopPosition, 
                        this.chunk, 
                        HotColdPlatformRail.Type.LaneStop, 
                        this.transform.parent
                    ).gameObject;
                }

                void DrawLaneEnd(Vector2 endPosition, bool flipped)
                {
                    GameObject laneEndGO = HotColdPlatformRail.Create(
                        endPosition, 
                        this.chunk, 
                        HotColdPlatformRail.Type.LaneEnd, 
                        this.transform.parent
                    ).gameObject;
                    laneEndGO.transform.localRotation =
                        Quaternion.LookRotation(Vector3.forward, edge.direction);
                    laneEndGO.transform.localScale = new Vector3(1f, flipped ? -1f : 1f, 1f);
                }

                Vector2 toNodePosition = path.PointAtDistanceAlongPath(totalDistance).position;

                if (this.path.closed == true)
                {
                    DrawLaneStop(toNodePosition);
                }
                else
                {
                    if (i < this.path.edges.Length - 1)
                    {
                        DrawLaneStop(toNodePosition);
                    }

                    if (i == 0)
                    {
                        DrawLaneEnd(path.PointAtDistanceAlongPath(0f).position, false);
                    }

                    if (i == this.path.edges.Length - 1)
                    {
                        DrawLaneEnd(toNodePosition, true);
                    }
                }
            }

            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
    }
    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(newTemperature == Map.Temperature.Hot)
        {
            this.targetSpeed = DefaultSpeed;
        }
        else // Cold
        {
            this.targetSpeed = 0f;
        }
    }

    private void SetupAutotiling()
    {
        List<HotColdPlatformBlock> touchBlocks = new List<HotColdPlatformBlock>();
        foreach(var f in this.group.followers)
        {
            if(f.Key.item is HotColdPlatformBlock)
            {
                touchBlocks.Add(f.Key.item as HotColdPlatformBlock);
            }
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(touchBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(touchBlocks.Min(t => t.position.y));
        
        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(touchBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(touchBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new HotColdPlatformBlock[sizeX, sizeY];

        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;
        
        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in touchBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGrid(x, y + 1);
                bool down = IsItemInGrid(x, y - 1);
                bool left = IsItemInGrid(x - 1, y);
                bool right = IsItemInGrid(x + 1, y);

                bool upLeft = IsItemInGrid(x - 1, y + 1);
                bool upRight = IsItemInGrid(x + 1, y + 1);
                bool downLeft = IsItemInGrid(x - 1, y - 1);
                bool downRight = IsItemInGrid(x + 1, y - 1);

                this.grid[x, y].myNeighbours = new HotColdPlatformBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }

        // choose autotile sprite
        foreach(var t in touchBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    public bool IsItemInGrid(int x, int y)
    {
        if(x < 0 || x >= this.grid.GetLength(0)) return false;
        if(y < 0 || y >= this.grid.GetLength(1)) return false;

        return this.grid[x, y] != null;
    }

    public override void Reset()
    {
        this.time = 0f;

        this.position =
            this.path.PointAtDistanceAlongPath(0f).position;
        this.transform.position = this.position;

        foreach(var f in this.group.followers)
        {
            f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
            f.Key.item.transform.position = f.Key.item.position;
        }
    }
}
