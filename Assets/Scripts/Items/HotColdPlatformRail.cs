using UnityEngine;
using System.Collections.Generic;

public class HotColdPlatformRail : Item
{
    public Sprite hotSprite;
    public Sprite coldSprite;

    public Sprite backSpriteHot;
    public Sprite backSpriteCold;

    private SpriteRenderer spriteRen;
    private SpriteRenderer backSprite;

    public enum Type
    {
        Lane,
        LaneEnd,
        LaneStop
    }
    public Type type;

    public override void Init(Def def)
    {
        base.Init(def);

        spriteRenderer = this.GetComponent<SpriteRenderer>();
        if(this.type == Type.LaneStop)
        {
            backSprite = this.transform.Find("Back Sprite").GetComponent<SpriteRenderer>();
        }
        NotifyChangeTemperature(this.map.currentTemperature);
    }

    public static HotColdPlatformRail Create(Vector2 position, Chunk chunk, HotColdPlatformRail.Type type, Transform parent)
    {
        GameObject obj;
        if (type == Type.Lane)
        {
            obj = Instantiate(Assets.instance.hotNColdSprings.touchPlatformPathLane, parent);
            obj.name = "hotcold platform path lane";
        }
        else if(type == Type.LaneEnd)
        {
            obj = Instantiate(Assets.instance.hotNColdSprings.touchPlatformPathLaneEnd, parent);
            obj.name = "hotcold platform path lane end";
        }
        else
        {
            obj = Instantiate(Assets.instance.hotNColdSprings.touchPlatformPathLaneStop, parent);
            obj.name = "hotcold platform path lane stop";
        }
        
        var item = obj.GetComponent<HotColdPlatformRail>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if (newTemperature == Map.Temperature.Hot)
        {
            spriteRenderer.sprite = hotSprite;
            if(type == Type.LaneStop)
            {
                backSprite.sprite = backSpriteHot;
            }
        }
        else if (newTemperature == Map.Temperature.Cold)
        {
            spriteRenderer.sprite = coldSprite;
            if (type == Type.LaneStop)
            {
                backSprite.sprite = backSpriteCold;
            }
        }
    }
}
