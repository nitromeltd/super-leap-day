﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icicle : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation fallingAnimation;
    public Animated.Animation meltAnimation;
    public Animated.Animation unmeltAnimation;

    public Sprite[] debris;

    private Vector2 startingPosition;
    private Vector2 velocity;
    private bool falling;
    private bool melting;
    private bool broken;
    private int shakeTimer;
    private Vector2 shakePosition;

    private const float DetectPlayerHorizontal = 1.00f;
    private const float Gravity = 0.04f;
    private const float ShakeSpeed = 1f;
    private const float ShakeAmount = 0.050f;

    private const int ShakeTime = 20;
    
    public override void Init(Def def)
    {
        base.Init(def);
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.startingPosition = this.position;

        this.falling = false;
        this.melting = false;
        this.broken = false;
        this.shakeTimer = 0;

        this.hitboxes = new [] {
            new Hitbox(-0.30f, 0.30f, -1f, 0.5f, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayOnce(this.idleAnimation);

        NotifyChangeTemperature(this.map.currentTemperature);
    }
    
    public override void Advance()
    {
        base.Advance();

        if(IsDeadly() == true)
        {
            Player player = this.map.player;
            Rect playerRect = player.Rectangle();

            if(this.hitboxes[0].InWorldSpace().Overlaps(playerRect))
            {
                player.Hit(this.position);
            }
        }

        if(IsDetectingPlayer())
        {
            Shake();
        }

        if(IsShaking())
        {
            this.shakeTimer -= 1;

            float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * ShakeAmount;
            this.shakePosition.x = shake;

            if(this.shakeTimer == 0)
            {
                this.shakePosition = Vector2.zero;
                Fall();
            }
        }

        if(this.falling == true)
        {
            this.velocity.y -= Gravity;
            Move();
        }

        if (this.group != null && this.falling == false && this.broken == false)
            this.transform.position = this.position =
                this.group.FollowerPosition(this) + this.shakePosition;
        else
            this.transform.position = this.position + this.shakePosition;
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(newTemperature == Map.Temperature.Hot)
        {
            Melt();
        }
        else if(newTemperature == Map.Temperature.Cold)
        {
            Unmelt();
        }
    }

    public override void Reset()
    {
        this.transform.position = this.position = GetStartingPosition();
        this.animated.PlayOnce(this.idleAnimation);

        this.falling = false;
        this.melting = false;
        this.broken = false;
        this.shakeTimer = 0;

        NotifyChangeTemperature(this.map.currentTemperature);
    }

    private bool IsDeadly()
    {
        if(this.melting == true) return false;
        if(this.broken == true) return false;

        return true;
    }

    private void Move()
    {
        var raycast = new Raycast(this.map, this.position);
        var rect = this.hitboxes[0];

        var tolerance = 0.1f;
        var start = this.position;
        var maxDistance = -this.velocity.y - rect.yMin + tolerance;
        var result = Raycast.CombineClosest(
            raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
            raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
            raycast.Vertical(start + new Vector2( 0.375f, 0), false, maxDistance)
        );
        if (result.anything == true &&
            result.distance < -this.velocity.y - rect.yMin + tolerance)
        {
            Break();
        }
        else
        {
            this.position.y += this.velocity.y;
        }
    }

    private bool IsDetectingPlayer()
    {
        if(this.rotation != 0) return false;
        if(IsShaking() == true) return false;
        if(this.melting == true) return false;
        if(this.falling == true) return false;
        if(this.broken == true) return false;
        
        var detectRaycast = new Raycast(
            this.map,
            this.position,
            ignoreTileTopOnlyFlag: true,
            ignoreItem1: this
        );

        var detectSolid = detectRaycast.Arbitrary(
            this.position, Vector2.down, 20f
        );
        float distanceToSolid = detectSolid.distance;

        Vector2 origin = this.position + new Vector2(
            -(DetectPlayerHorizontal / 2), -distanceToSolid
        );
        Rect detectRect =  new Rect(
            origin.x, origin.y, DetectPlayerHorizontal, distanceToSolid
        );

        return detectRect.Contains(this.map.player.position);
    }

    private bool IsShaking()
    {
        return this.shakeTimer > 0;
    }

    private void Shake()
    {
        this.shakeTimer = ShakeTime;

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvIcicleShake,
            position: this.position
        );
    }

    private void Fall()
    {
        if(this.falling == true) return;
        this.falling = true;
        
        this.animated.PlayOnce(this.fallingAnimation);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvIcicleFall,
            position: this.position
        );
    }

    private void Break()
    {
        if(this.broken == true) return;
        
        void ThrowDebris()
        {
            this.spriteRenderer.sprite = null;
            this.animated?.Stop();

            foreach (var debris in this.debris)
            {
                var p = Particle.CreateWithSprite(
                    debris,
                    100,
                    this.position + this.velocity + Vector2.up * Random.Range(-1.5f, -0.5f),
                    this.transform.parent
                );
                p.Throw(new Vector2(0, 0.3f), 0.1f, 0.1f, new Vector2(0, -0.025f));

                p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
                if (UnityEngine.Random.Range(0, 2) == 0)
                    p.spin *= -1;
            }
        }

        this.falling = false;
        this.broken = true;
        this.velocity = Vector2.zero;
        
        ThrowDebris();

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvIcicleBreak,
            position: this.position
        );
    }

    private void Melt()
    {
        if(this.melting == true) return;
        if(this.broken == true) return;

        this.melting = true;
        this.animated.PlayOnce(this.meltAnimation);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSnowMelt,
            position: this.position
        );
    }

    private void Unmelt()
    {
        if(this.broken == true)
        {
            this.broken = false;
            this.position = GetStartingPosition();
        }

        this.melting = false;
        this.animated.PlayOnce(this.unmeltAnimation);

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvIcicleSpawn,
            position: this.position
        );
    }

    private Vector2 GetStartingPosition()
    {
        if (this.group != null)
        {
            return this.group.FollowerPosition(this);
        }
        
        return this.startingPosition;
    }
    
}
