using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingHat : Item
{
    public Animated.Animation animationAirborne;
    public Animated.Animation animationGrounded;
    public Animated.Animation animationFloating;

    private bool isOnKing;
    private bool movingFreely;
    private float bounciness;
    private float smoothVelocityH = 0f;
    private float spin;
    private bool returningToHead;
    private Vector2 returnHeadStartPoint;
    private int returnHeadTimer;
    private float playerFacingDirectionOnContact;
    private bool canLandOnHead;
    private Checkpoint checkpoint;
    private Fruit fruit;
    private float rotationCurrentZ = 0;
    private float rotationTargetZ = 0;
    private float rotationSmoothZ = 0;
    private bool checkForInsideSomething = false;
    private int timerCheckInsideSomething = 0;
    private bool insideWater = false;

    private const int ReturnHeadTime = 30;
    private const float HorizontalMomentum = 0.5f;
    private const float HitboxSize = 0.5f;

    // gravity
    private Gravity gravity;
    private Vector2 velocity;
    private Tile onGroundTile = null;
    private Item onGroundItem = null;
    private bool IsOnGround => this.onGroundTile != null || this.onGroundItem != null;
    private static readonly float FreeFloatingMaxSpeed = 0.31f;
    private static readonly float FreeFloatingMinSpeed = 0.0625f;
    protected static readonly float GravityAcceleration = 0.020f;
    protected static readonly float GravityAccelerationUnderwater = 0.010f;

    private float CurrentGravityAcceleration => this.insideWater == true ?
        GravityAccelerationUnderwater : GravityAcceleration;

    public struct Gravity
    {
        public KingHat kingHat;
        public Player.Orientation? gravityDirection;

        public Player.Orientation Orientation() =>
            this.gravityDirection ?? Player.Orientation.Normal;
        public bool IsZeroGravity() => gravityDirection == null;

        private static Vector2[] Directions =
            new [] { Vector2.down, Vector2.right, Vector2.up, Vector2.left };

        public Vector2 Down()  => Directions[(int)Orientation()];
        public Vector2 Right() => Directions[((int)Orientation() + 1) % 4];
        public Vector2 Up()    => Directions[((int)Orientation() + 2) % 4];
        public Vector2 Left()  => Directions[((int)Orientation() + 3) % 4];

        public int OrientationAngle()
        {
            if(IsZeroGravity() == true) return 0;

            switch(Orientation())
            {
                case Player.Orientation.Normal:
                default:
                    return 0;

                case Player.Orientation.LeftWall:
                    return 270;

                case Player.Orientation.RightWall:
                    return 90;

                case Player.Orientation.Ceiling:
                    return 180;
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => Orientation() switch
            {
                Player.Orientation.Normal    => kingHat.velocity,
                Player.Orientation.RightWall => new Vector2(kingHat.velocity.y, -kingHat.velocity.x),
                Player.Orientation.Ceiling   => new Vector2(-kingHat.velocity.x, -kingHat.velocity.y),
                _                            => new Vector2(-kingHat.velocity.y, kingHat.velocity.x)
            };
            set {
                switch (Orientation())
                {
                    case Player.Orientation.Normal:    kingHat.velocity = value; break;
                    case Player.Orientation.RightWall: kingHat.velocity = new Vector2(-value.y, value.x); break;
                    case Player.Orientation.Ceiling:   kingHat.velocity = new Vector2(-value.x, -value.y); break;
                    default:                           kingHat.velocity = new Vector2(value.y, -value.x); break;
                }
            }
        }
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.hitboxes = new [] {
            new Hitbox(-HitboxSize, HitboxSize, -HitboxSize, HitboxSize, this.rotation, Hitbox.NonSolid)
        };

        this.gravity = new Gravity {
                kingHat = this,
                gravityDirection = this.map.player.gravity.Orientation()
        };
        this.bounciness = -0.50f;

        this.isOnKing = true;

        if(this.map.player.midasTouch.isActive == true)
        {
            this.spriteRenderer.material = Assets.instance.spriteGoldTint;
        }
        else
        {
            this.spriteRenderer.material = Assets.instance.spritesDefaultMaterial;
        }
    }

    public void Throw(Vector2 throwVelocity)
    {
        this.gameObject.SetActive(true);
        this.isOnKing = false;
        this.movingFreely = true;
        this.velocity = throwVelocity;
        this.checkpoint = null;
        this.fruit = null;
        this.timerCheckInsideSomething = 15;

        this.spin = 0f;

        this.rotationCurrentZ = this.rotationTargetZ = this.gravity.OrientationAngle();
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotationCurrentZ);

        this.animated.PlayAndLoop(this.animationAirborne);
        Audio.instance.PlaySfx(Assets.instance.sfxKingHatLose);
    }

    public void ThrowOnStompDrop()
    {
        Throw(Vector2.up * 0.10f);
        this.canLandOnHead = true;
        this.transform.localScale =
            new Vector3(this.map.player.facingRight ? 1f : -1f, 1f, 1f);
    }   

    public override void Advance()
    {
        Player player = this.map.player;
        if((player is PlayerKing) == false) return;

        if(this.timerCheckInsideSomething > 0)
        {
            this.timerCheckInsideSomething -= 1;

            if(this.timerCheckInsideSomething == 0)
            {
                this.checkForInsideSomething = true;
            }
        }

        SwitchToNearestChunk();
        UpdateGravity();

        bool shouldSpawnDust = false;
        if(this.movingFreely == true)
        {
            shouldSpawnDust = this.velocity.magnitude > 0.01;

            AdvanceGravity();
            AdvanceWater();
            Integrate();
            
            // check grounded
            var rFloor = new Raycast(this.map, this.position).Arbitrary(
                this.position + this.gravity.Down() * HitboxSize,
                this.gravity.Down(),
                0.10f
            );
            this.onGroundTile = rFloor.tile;
            this.onGroundItem = rFloor.item;

            if(IsOnGround == true)
            {
                this.canLandOnHead = false;
            }

            if(this.onGroundTile != null)
            {
                Vector2 surfaceVelocity = this.onGroundTile.tileGrid.effectiveVelocity;
                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;

                if(surfaceVelocity != Vector2.zero)
                {
                    shouldSpawnDust = false;
                }
            }

            // apply drag on floor
            if(IsOnGround == true)
            {
                float newVelocityOnGravityX = Mathf.SmoothDamp(
                    this.gravity.VelocityInGravityReferenceFrame.x, 0f, ref smoothVelocityH, HorizontalMomentum
                );

                this.gravity.VelocityInGravityReferenceFrame =  new Vector2(
                    newVelocityOnGravityX,
                    this.gravity.VelocityInGravityReferenceFrame.y
                );
            }

            // rotation when moving freely
            this.rotationCurrentZ = Mathf.SmoothDampAngle(
                this.rotationCurrentZ, this.rotationTargetZ, ref rotationSmoothZ, .15f);
            this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotationCurrentZ);
        }

        Vector2 playerHeadPosition = (player as PlayerKing).HeadPosition;

        if(this.isOnKing == true && player.state != Player.State.InsideLift)
        {
            this.position = playerHeadPosition;
            
            if(IsCollidingWhileOnTopOfPlayer())
            {
                (player as PlayerKing).LoseHatOnCollision();
            }
        }
        else if(this.returningToHead == true)
        {
            shouldSpawnDust = true;

            this.spin = 20f * playerFacingDirectionOnContact;
            this.transform.localScale = new Vector3(playerFacingDirectionOnContact, 1f, 1f);

            Vector2 targetPoint = playerHeadPosition;

            float controlPointHorizontal = 1f * playerFacingDirectionOnContact;
            Vector2 controlPoint = targetPoint + this.gravity.Right() * controlPointHorizontal + this.gravity.Up() * 5f;

            this.returnHeadTimer += 1;
            float t = (float)this.returnHeadTimer / (float)ReturnHeadTime;

            this.position = QuadraticBezier(
                this.returnHeadStartPoint,
                controlPoint,
                targetPoint,
                t
            );

            if(Vector2.Distance(this.position, targetPoint) < 0.01f)
            {
                ReachHead();
            }
        }
        else
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = this.map.player.Rectangle();

            float v = this.velocity.magnitude;

            bool isContactingPlayer = thisRect.Overlaps(playerRect);

            if(IsOnGround == true &&
                v < 0.05f)
            {
                if(this.animated.currentAnimation == this.animationAirborne)
                {
                    this.animated.PlayAndLoop(this.animationGrounded);
                }

                if(isContactingPlayer == true)
                {
                    ReturnToHead();
                }
            }
            else if(this.fruit && isContactingPlayer == true)
            {
                ReturnToHead();
            }
            else if(this.gravity.IsZeroGravity() && isContactingPlayer == true)
            {
                ReturnToHead();
            }
        }
        
        if(shouldSpawnDust == true && this.map.frameNumber % 2 == 0)
        {
            float x = this.position.x;
            float y = this.position.y;

            int lifetime = UnityEngine.Random.Range(25, 35);

            Particle p = Particle.CreatePlayAndHoldLastFrame(
                this.map.player.animationDustParticle,
                lifetime,
                new Vector2(
                    x + UnityEngine.Random.Range(-0.2f, 0.2f),
                    y + UnityEngine.Random.Range(-0.2f, 0.2f)
                ),
                this.transform.parent
            );
            float s = UnityEngine.Random.Range(0.75f, 1f);
            p.transform.localScale = new Vector3(s, s, 1);

            p.spriteRenderer.sortingLayerName = "Enemies";
            p.spriteRenderer.sortingOrder = -2;
        }

        if(this.canLandOnHead == true)
        {
            this.spin = 0f;

            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = this.map.player.Rectangle();

            if(thisRect.Overlaps(playerRect) &&
                this.velocity.y < 0f)
            {
                ReachHead();
            }
        }
        
        if (this.spin != 0)
        {
            this.transform.localRotation *= Quaternion.Euler(0, 0, this.spin);
        }

        if(this.checkpoint != null)
        {
            this.position = checkpoint.position + new Vector2(0.20f, 4.30f);   
        }

        if(this.fruit != null)
        {
            var theta =
                this.map.frameNumber * 0.08f +
                this.def.tx * (0.5f + Util.Tau / 4) +
                this.def.ty * (0.5f + Util.Tau / 4);

            this.position = this.fruit.position + (Vector2.up * Mathf.Sin(theta) * 2.5f / 16);

            this.animated.PlayAndLoop(this.animationFloating);
        }

        this.transform.position = new Vector2(this.position.x, this.position.y);
    }
    

    private void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    private void UpdateGravity()
    {
        Player.Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        var intendedGravity = SampleAt(this.position);
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        if (this.gravity.gravityDirection == Player.Orientation.Ceiling &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Normal) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Player.Orientation.Ceiling)
                intendedGravity = Player.Orientation.Ceiling;
        }
        else if (this.gravity.gravityDirection == Player.Orientation.Normal &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Ceiling) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Player.Orientation.Normal)
                intendedGravity = Player.Orientation.Normal;
        }

        if (this.gravity.gravityDirection != intendedGravity)
        {
            this.gravity.gravityDirection = intendedGravity;
        }
    }

    private void AdvanceGravity()
    {
        var gravityDirectionVector = this.gravity.Down();

        if (this.gravity.IsZeroGravity() == true)
        {
            var speed = this.velocity.magnitude;
            if (speed > FreeFloatingMaxSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMaxSpeed, 0.01f);
                this.velocity.Normalize();
                this.velocity *= speed;
            }
            else if (speed < FreeFloatingMinSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMinSpeed, 0.001f);
                if (this.velocity.sqrMagnitude == 0)
                    this.velocity = this.gravity.Down();
                else
                    this.velocity.Normalize();
                this.velocity *= speed;
            }

            this.rotationTargetZ = Util.WrapAngle0To360(this.rotationTargetZ + 0.5f);
        }
        else
        {
            this.velocity += CurrentGravityAcceleration * gravityDirectionVector;
            
            if(IsOnGround == true && IsInsideSomething() == false)
            {
                float GetGroundAngleDegrees()
                {
                    if(this.onGroundTile != null)
                    {
                        return this.onGroundTile.SurfaceAngleDegrees(this.position, this.gravity.Down());
                    }

                    return 0f;
                }

                this.rotationTargetZ = GetGroundAngleDegrees();
            }
            
            if(this.gravity.IsZeroGravity())
            {
                this.rotationTargetZ = this.gravity.OrientationAngle();
            }
        }
    }

    private bool IsCollidingWhileOnTopOfPlayer()
    {
        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByPlayer: true,
            ignoreItemType: typeof(StartBarrier),
            ignoreItem1: this,
            ignoreTileTopOnlyFlag: false
        );
        var rect = this.hitboxes[0];

        Player player = this.map.player;
        //float maxDistance = 0.15f;

        var left = raycast.Arbitrary(
            this.position,// + new Vector2(rect.xMin, 0),
            this.gravity.Left(),
            0.25f
        );
        
        var right = raycast.Arbitrary(
            this.position,// + new Vector2(rect.xMax, 0),
            this.gravity.Right(),
            0.50f
        );
        
        /*var down = raycast.Arbitrary(
            this.position,// + new Vector2(0, rect.yMin),
            this.gravity.Down(),
            -this.velocity.y
        );*/
        var up = raycast.Arbitrary(
            this.position,// + new Vector2(0, rect.yMax),
            this.gravity.Up(),
            0.50f
        );

        return left.anything == true || 
            right.anything == true ||
            //down.anything == true ||
            up.anything == true;
    }

    private bool IsInsideSomething()
    {
        if(this.checkForInsideSomething == false) return false;

        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByPlayer: true,
            ignoreItemType: typeof(StartBarrier),
            ignoreItem1: this,
            ignoreTileTopOnlyFlag: false
        );
        float maxDistance = 0.05f;

        var up = raycast.Vertical(this.position, true, maxDistance);
        var down = raycast.Vertical(this.position, false, maxDistance);
        var right = raycast.Horizontal(this.position, true, maxDistance);
        var left = raycast.Horizontal(this.position, false, maxDistance);

        return this.movingFreely == true &&
            left.anything == true && 
            right.anything == true &&
            down.anything == true &&
            up.anything == true;
    }
    
    private void Integrate()
    {
        var raycast = new Raycast(this.map, this.position);
        var rect = this.hitboxes[0];

        bool isInsideSomething = IsInsideSomething();

        if (this.velocity.x < 0)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0),
                false,
                -this.velocity.x
            );
            if (r.anything == true && isInsideSomething == false)
            {
                this.position.x -= r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0),
                true,
                this.velocity.x
            );
            if (r.anything == true && isInsideSomething == false)
            {
                this.position.x += r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y < 0)
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMin),
                false,
                -this.velocity.y
            );
            if (r.anything == true && isInsideSomething == false)
            {
                this.position.y -= r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMax),
                true,
                this.velocity.y
            );
            if (r.anything == true && isInsideSomething == false)
            {
                this.position.y += r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    private void ReturnToHead()
    {
        PlayerKing playerKing = this.map.player as PlayerKing;

        if(playerKing == null) return;
        if(playerKing.CanRecoverHat() == false) return;

        this.returningToHead = true;
        this.movingFreely = false;
        this.returnHeadStartPoint = this.position;
        this.playerFacingDirectionOnContact =
            (this.map.player.facingRight ? 1f : -1f);
        this.checkpoint = null;
        this.fruit = null;
            
        this.animated.PlayAndLoop(this.animationAirborne);
        Audio.instance.PlaySfx(Assets.instance.sfxKingRise);
    }

    public void ReachHead()
    {
        this.canLandOnHead = false;
        this.returningToHead = false;
        this.movingFreely = false;
        this.isOnKing = true;
        this.returnHeadTimer = 0;
        this.spin = 0f;
        this.checkpoint = null;
        this.fruit = null;

        PlayerKing playerKing = this.map.player as PlayerKing;
        this.position = playerKing.HeadPosition;

        playerKing.RecoverHat();
        this.gameObject.SetActive(false);
        Audio.instance.PlaySfx(Assets.instance.sfxKingHatReturn);
    }

    private Vector3 QuadraticBezier(Vector3 startP, Vector3 controlP, Vector3 endP, float bTime)
    {
        Vector3 quadraticCurve = new Vector3();

        quadraticCurve.x =
                (
                    ((1 - bTime) * (1 - bTime)) * startP.x
                )
            +
                (
                    2 * bTime * (1 - bTime) * controlP.x
                )
            +
                (
                    (bTime * bTime) * endP.x
                );

        quadraticCurve.y =
                (
                    ((1 - bTime) * (1 - bTime)) * startP.y
                )
            +
                (
                    2 * bTime * (1 - bTime) * controlP.y
                )
            +
                (
                    (bTime * bTime) * endP.y
                );

        quadraticCurve.z = 0f;

        return quadraticCurve;
    }

    public void PlaceOnCheckpoint(Checkpoint checkpoint)
    {
        if(this.checkpoint != null)
        {
            this.checkpoint.RemoveKingHat();
            this.checkpoint = null;
        }

        this.movingFreely = false;
        //this.grounded = false;

        this.checkpoint = checkpoint;
        this.checkpoint.PlaceKingHat(this);

        this.position = checkpoint.position.Add(0.20f, 4.30f);
        this.transform.localRotation = Quaternion.Euler(Vector3.forward * -25f);

        SwitchToNearestChunk();
    }

    public bool CanGoToCheckpoint()
    {
        if(this.isOnKing == true) return false;
        if(this.checkpoint != null) return false;
        if(this.fruit != null) return false;
        if(this.canLandOnHead == true) return false;

        return true;
    }

    public void ReplaceFruit(Fruit fruit)
    {
        this.movingFreely = false;
        //this.grounded = false;

        this.fruit = fruit;
        this.fruit.gameObject.SetActive(false);

        this.position = this.fruit.position;

        SwitchToNearestChunk();
    }

    public bool CanReplaceFruit()
    {
        if(this.isOnKing == true) return false;
        if(this.checkpoint != null) return false;
        if(this.fruit != null) return false;
        if(this.canLandOnHead == true) return false;

        return true;
    }

    public void Deactivate()
    {
        if(this.checkpoint != null)
        {
            this.checkpoint.RemoveKingHat();
            this.checkpoint = null;
        }

        if(this.fruit != null && this.fruit.collected == false)
        {
            this.fruit.gameObject.SetActive(true);
        }

        this.gameObject.SetActive(false);
    }
}
