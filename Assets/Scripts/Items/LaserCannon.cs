using UnityEngine;
using UnityEngine.Rendering;
using System;

public class LaserCannon : Item
{
    [NonSerialized] public float angleDegrees;

    public Transform beamCenter;
    public Transform beamOuter;
    public Transform endGlow;
    public Transform rotationBolt;
    public Animated particles;
    public Animated.Animation particleAnimation;

    private float rotationSpeed;

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.properties?.ContainsKey("rotation_speed") == true)
            this.rotationSpeed = def.tile.properties["rotation_speed"].f;
        else
            this.rotationSpeed = 0;

        if (this.chunk.isFlippedHorizontally == true)
            this.rotationSpeed *= -1;

        this.angleDegrees = def.tile.rotation + 90;

        this.particles.PlayAndLoop(this.particleAnimation);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        this.angleDegrees += this.rotationSpeed * 0.01f * 180 / Mathf.PI;
        this.angleDegrees %= 360;
        this.transform.rotation = Quaternion.Euler(0, 0, this.angleDegrees - 90);
        this.rotationBolt.rotation = Quaternion.identity;

        var direction = new Vector2(
            Mathf.Cos(this.angleDegrees * Mathf.PI / 180),
            Mathf.Sin(this.angleDegrees * Mathf.PI / 180)
        );

        var raycast = new Raycast(
            this.map, this.position,
            ignoreTileTopOnlyFlag: false, stopAtPortals: true,
            ignoreChunksOutsideRange: 40, isCastByEnemy: true
        );
        var result = raycast.Arbitrary(this.position + (direction * 3f), direction, 30);
        float actualDistance = result.distance + 3f;
        this.beamCenter.localScale = new Vector3(
            1, (actualDistance - this.beamCenter.localPosition.y) / 4.32f, 1
        );
        this.beamOuter.localScale = new Vector3(
            1, (actualDistance - this.beamOuter.localPosition.y) / 4.62f, 1
        );
        this.endGlow.localPosition = new Vector3(0, actualDistance, 0);

        CheckIfPlayerHit(direction, actualDistance);

        var playerPosition = this.map.player.position;
        var isPlayerInSameChunk =
            this.map.NearestChunkTo(playerPosition) == this.chunk;

        if (isPlayerInSameChunk)
        {
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvLaser))
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvLaser,
                    new Audio.Options(0.7f, false,0),
                    position: this.position
                );
            }
        }
        else
        {
            if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvLaser))
            {
                Audio.instance.StopSfx(Assets.instance.sfxEnvLaser);
            }
        }
    }

    private void CheckIfPlayerHit(Vector2 direction, float distance)
    {
        var player = this.map.player;

        Vector2 end = this.position + (direction * distance);
        float s = Vector2.Dot(
            player.position - this.position, direction * distance
        ) / (distance * distance);
        var nearest = Vector2.Lerp(this.position, end, s);

        if ((player.position - nearest).sqrMagnitude < 1 * 1)
        {
            player.Hit(nearest);
        }
    }
}
