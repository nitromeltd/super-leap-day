using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaMarker : Item
{
    public bool loop;

    private bool connected = false;
    private List<LavaMarker> markersLoop = new List<LavaMarker>();
    public bool isLoopFollower = false;
    private bool controlByExternalItem;

    // editor properties
    private float riseSpeed;
    private float delayTime;
    private bool closedLoop;

    private bool SameChunkAsPlayer =>
        this.chunk == this.map.NearestChunkTo(this.map.player.position);
    public bool AutoTrigger =>
        this.connected == false &&
        this.isLoopFollower == false &&
        SameChunkAsPlayer == true &&
        this.map.player.alive == true &&
        this.map.player.state != Player.State.InsideLift;
    
    public Lavaline.LoopPoint GenerateLoopPoint()
    {
        return new Lavaline.LoopPoint(this.position.y, riseSpeed, delayTime);
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.riseSpeed = (def.tile.properties?.ContainsKey("rise_speed") == true) ?
            def.tile.properties["rise_speed"].f : Lavaline.DefaultRiseSpeed;
        this.delayTime = (def.tile.properties?.ContainsKey("delay_time") == true) ?
            def.tile.properties["delay_time"].f : 0f;
        this.closedLoop = (def.tile.properties?.ContainsKey("closed_loop") == true) ?
            def.tile.properties["closed_loop"].b : false;

        if(this.loop == true)
        {
            foreach (var path in this.chunk.pathLayer.paths)
            {
                var nearestPathNode = path.NearestNodeTo(this.position, 2);
                var nearestNodeIndex = path.NearestNodeIndexTo(this.position, 2);

                if (nearestPathNode != null && nearestNodeIndex == 0)
                {
                    foreach(var node in nearestPathNode.path.nodes)
                    {
                        LavaMarker lavaMarker =
                            this.chunk.NearestItemTo<LavaMarker>(node.Position, 2f);

                        lavaMarker.isLoopFollower = true;
                        this.markersLoop.Add(lavaMarker);
                        this.isLoopFollower = false;
                    }
                }
            }
        }
    }

    public override void Advance()
    {
        if(this.spriteRenderer.enabled == true && DebugPanel.chunkNamesEnabled == false)
        {
            this.spriteRenderer.enabled = false;
        }

        if(this.spriteRenderer.enabled == false && DebugPanel.chunkNamesEnabled == true)
        {
            this.spriteRenderer.enabled = true;
        }
    }

    public void Connect(Item connectorItem)
    {
        this.connected = true;

        /*if(connectorItem is WaterRaiserSwitch || connectorItem is WaterRaiserButton)
        {
            this.controlByExternalItem = true;
        }*/
    }
    
    public void RaiseLava(bool forceFirstNode = false)
    {
        if(this.loop == true)
        {
            if(this.markersLoop != null && this.markersLoop.Count > 1)
            {
                this.map.lavaline.LoopLevel(
                    this.markersLoop,
                    controlByExternalItem: controlByExternalItem,
                    closedLoop: closedLoop,
                    forceFirstNode: forceFirstNode
                );
            }
        }
        else
        {
            this.map.lavaline.ChangeLevel(
                this.position.y,
                riseSpeed: this.riseSpeed,
                false
            );
        }
    }
}
