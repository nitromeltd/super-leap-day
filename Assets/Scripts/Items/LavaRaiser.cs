using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LavaRaiser : Item
{
    public Orientation orientation;

    private bool alreadyTriggered = false;
    private LavaMarker connectedMarker;
    private bool wasPlayerInThisChunkLastFrame = false;

    public enum Orientation
    {
        Horizontal,
        Vertical
    }

    public override void Init(Def def)
    {
        base.Init(def);

        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                this.connectedMarker =
                    this.chunk.NearestItemTo<LavaMarker>(otherNode.Position);
                this.connectedMarker.Connect(this);
            }
        }
    }

    public override void Advance()
    {
        CheckForPlayerExit();
        
        if(this.spriteRenderer.enabled == true && DebugPanel.chunkNamesEnabled == false)
        {
            this.spriteRenderer.enabled = false;
        }

        if(this.spriteRenderer.enabled == false && DebugPanel.chunkNamesEnabled == true)
        {
            this.spriteRenderer.enabled = true;
        }
        
        Player player = this.map.player;
        Chunk playerChunk = this.map.NearestChunkTo(player.position);

        if(playerChunk != this.chunk) return;
        if(player.state == Player.State.InsideLift) return;

        Chunk itemChunk = this.map.NearestChunkTo(this.position);
        Chunk checkpointChunk = this.map.NearestChunkTo(player.checkpointPosition);

        if(itemChunk.index < checkpointChunk.index) return;

        switch(this.orientation)
        {
            case Orientation.Horizontal:
            {
                if(player.position.y > this.position.y)
                {
                    RaiseLava();
                }
            }
            break;

            case Orientation.Vertical:
            {
                if(chunk.isHorizontal == true &&
                    (chunk.isFlippedHorizontally == false && player.position.x > this.position.x ||
                    chunk.isFlippedHorizontally == true && player.position.x < this.position.x))
                {
                    RaiseLava();
                }
            }
            break;
        }

        this.spriteRenderer.color = this.alreadyTriggered ?
            Color.white : new Color(1f, 1f, 1f, 0.50f);
    }

    private void CheckForPlayerExit()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        bool isPlayerInThisChunk = playerChunk == this.chunk;

        if(wasPlayerInThisChunkLastFrame == true && isPlayerInThisChunk == false)
        {
            // player exiting chunk
            Reset();
        }

        this.wasPlayerInThisChunkLastFrame = isPlayerInThisChunk;
    }

    public override void Reset()
    {
        this.alreadyTriggered = false;
    }

    private void RaiseLava()
    {
        if(this.alreadyTriggered == true) return;
        if(this.connectedMarker == null) return;

        this.alreadyTriggered = true;
        this.connectedMarker.RaiseLava();
    }
}
