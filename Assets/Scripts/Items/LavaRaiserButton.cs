using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LavaRaiserButton : Item
{
    public Animated.Animation animationUp;
    public Animated.Animation animationDown;

    private bool pressed;
    private bool pressedThisFrame;
    private LavaMarker connectedMarker;
    private bool wasPlayerInThisChunkLastFrame = false;

    public bool Pressed { get => pressed; }
    public bool PressedThisFrame { get => pressedThisFrame; }

    override public void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.75f, 0.75f, 0, 0.5f, this.rotation, Hitbox.NonSolid),
            new Hitbox(-1, 1, -1, 0, this.rotation, Hitbox.Solid)
        };
        this.pressed = false;
        this.pressedThisFrame = false;

        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                this.connectedMarker =
                    this.chunk.NearestItemTo<LavaMarker>(otherNode.Position);
                this.connectedMarker.Connect(this);
            }
        }
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    override public void Advance()
    {
        CheckForPlayerExit();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        var pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if (this.pressed == true)
        {
            this.animated.PlayAndHoldLastFrame(this.animationDown);
            this.pressedThisFrame = (pressedLastFrame == false);
        }
        else
        {
            this.animated.PlayAndHoldLastFrame(this.animationUp);
            this.pressedThisFrame = false;
        }

        if (pressedLastFrame == false && this.pressed == true)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);

            if(this.connectedMarker != null)
            {
                this.connectedMarker.RaiseLava();
            }
        }

        if (pressedLastFrame == true && this.pressed == false)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxButtonUnpress, position:this.position);
        }
    }

    private void CheckForPlayerExit()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        bool isPlayerInThisChunk = playerChunk == this.chunk;

        if(wasPlayerInThisChunkLastFrame == true && isPlayerInThisChunk == false)
        {
            // player exiting chunk
            Reset();
        }

        this.wasPlayerInThisChunkLastFrame = isPlayerInThisChunk;
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
                return true;
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        Vector2[] SensorsWhenHorizontal() => new []
        {
            new Vector2(hitboxRect.xMin,     hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.xMax,     hitboxRect.center.y)
        };
        Vector2[] SensorsWhenVertical() => new []
        {
            new Vector2(hitboxRect.center.x, hitboxRect.yMin),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.yMax)
        };

        var sensors =
            (this.rotation == 0 || this.rotation == 180) ?
            SensorsWhenHorizontal() :
            SensorsWhenVertical();

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }
}
