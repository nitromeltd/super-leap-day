using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LavaRaiserSwitch : Item
{
    public Animated.Animation idleOneAnimation;
    public Animated.Animation oneToTwoAnimation;
    public Animated.Animation idleTwoAnimation;
    public Animated.Animation twoToOneAnimation;

    public enum Side
    {
        One,
        Two
    }

    private Side side;
    private bool pressed;
    private int pressDisableTime;
    private bool engage;
    private LavaMarker connectedMarker;
    private bool wasPlayerInThisChunkLastFrame = false;
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, 0f, 1f, this.rotation, Hitbox.NonSolid)
        };

        this.pressed = false;
        this.engage = true;

        this.animated.PlayOnce(this.side == Side.One ?
            this.idleOneAnimation : this.idleTwoAnimation);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                this.connectedMarker =
                    this.chunk.NearestItemTo<LavaMarker>(otherNode.Position);
                this.connectedMarker.Connect(this);
            }
        }
        
        ChangeSide(Side.One);
    }
    
    public override void Advance()
    {
        CheckForPlayerExit();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        bool pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if (pressedLastFrame == false && this.pressed == true)
        {
            Press();
        }

        if (this.pressed == false && pressedLastFrame == true)
            this.pressDisableTime = 20;
        else if (this.pressDisableTime > 0)
            this.pressDisableTime -= 1;
    }

    private void CheckForPlayerExit()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        bool isPlayerInThisChunk = playerChunk == this.chunk;

        if(wasPlayerInThisChunkLastFrame == true && isPlayerInThisChunk == false)
        {
            // player exiting chunk
            Reset();
        }

        this.wasPlayerInThisChunkLastFrame = isPlayerInThisChunk;
    }

    public override void Reset()
    {
        ChangeSide(Side.One);
    }

    private bool IsBeingPressed()
    {
        if (this.pressDisableTime > 0) return false;

        var thisRect = this.hitboxes[0].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        (Vector2 outward, Vector2 across) Directions()
        {
            switch (this.rotation)
            {
                case 90:   return (Vector2.right, Vector2.up);
                case 180:  return (Vector2.up,    Vector2.left);
                case 270:  return (Vector2.left,  Vector2.down);
                default:   return (Vector2.down,  Vector2.right);
            }
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        var (outward, across) = Directions();
        Vector2[] sensors = new []
        {
            this.position + (outward * 0.25f) + (across * 0.5f),
            this.position + (outward * 0.25f),
            this.position + (outward * 0.25f) + (across * -0.5f)
        };

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }

    private void Press()
    {
        if(this.connectedMarker != null)
        {
            this.connectedMarker.RaiseLava();
        }

        ChangeSide(this.side == Side.One ? Side.Two : Side.One);

        AudioClip sfxSwitch = (engage == true) ?
            Assets.instance.sfxSwitchEngage : Assets.instance.sfxSwitchDisengage;
        Audio.instance.PlaySfx(sfxSwitch, position: this.position);
    }

    private void ChangeSide(Side targetSide)
    {
        this.side = targetSide;

        Animated.Animation transitionAnimation = targetSide == Side.One ?
            this.twoToOneAnimation : this.oneToTwoAnimation;

        Animated.Animation onCompleteAnimation = targetSide == Side.One ?
            this.idleOneAnimation : this.idleTwoAnimation;

        this.animated.PlayOnce(transitionAnimation, () =>
        {
            this.animated.PlayOnce(onCompleteAnimation);
        });
    }
}
