using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class LightUpBlock : Item
{
    public Animated.Animation animationOff;
    public Animated.Animation animationOn;
    public Animated.Animation animationTurnOn;

    private LightUpBlock leader;

    private Vector3 scaleTarget = Vector3.one;
    private Vector3 scaleVelocity = Vector3.zero;
    private float xScaleValue = 0f;

    [NonSerialized] public Chest[] connectedChests;

    private bool haveChestsAppeared;
    private bool hasPlayedOnce;

    private int totalLightBlocks;

    private SpriteRenderer glow;

    [HideInInspector] public bool hasLitUp;
    public override void Init(Def def)
    {
        base.Init(def);

        this.xScaleValue = def.tile.flip ? -1f : 1f;
        this.scaleTarget = new Vector3(xScaleValue, 1f, 1f);
        this.transform.localScale = this.scaleTarget;

        this.hitboxes = new[] {
            new Hitbox(
                -0.5f, 0.5f, -0.5f, 0.5f,
                this.rotation, Hitbox.Solid, true, true)
        };

        this.solidToEnemy = true;

        this.onCollisionFromBelow = OnCollisionFromBelow;
        this.onCollisionFromAbove = OnCollisionFromAbove;
        this.onCollisionFromLeft = OnCollisionFromLeft;
        this.onCollisionFromRight = OnCollisionFromRight;

        this.animated.PlayOnce(this.animationOff);
        this.hasLitUp = false;

        this.glow = this.transform.Find("glow").GetComponent<SpriteRenderer>();
        this.glow.enabled = false;

        var chests = new List<Chest>();
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                var chest = this.chunk.NearestItemTo<Chest>(otherNode.Position);
                chests.Add(chest);
                this.leader = this;
            }
        }
        this.connectedChests = chests.ToArray();

        if (this == this.leader)
        {
            foreach (var lightBlock in this.chunk.items.OfType<LightUpBlock>())
            {
                this.totalLightBlocks++;
                lightBlock.leader = this;
            }
        }

        this.haveChestsAppeared = false;
        this.hasPlayedOnce = false;
    }


    public override void Advance()
    {
        this.transform.localScale = Vector3.SmoothDamp(this.transform.localScale,
            this.scaleTarget, ref scaleVelocity, .05f);

        
        if (this.hasLitUp == true && this.hasPlayedOnce == false)
        {
            this.hasPlayedOnce = true;
            StartCoroutine(_SquashStretch(new Vector3(0.7f, .7f, 1f), .05f));

            this.animated.PlayOnce(this.animationTurnOn, () =>
            {
                this.animated.PlayAndLoop(this.animationOn);
                this.glow.enabled = true;
                this.glow.color = new Color(1, 1, 1, 0.5f);
            });

            if (this.leader != null)
            {
                this.leader.totalLightBlocks--;
            }

            this.animated.OnFrame(2, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnvLightBlock);
            });
        }

        if (this.leader != null)
        {
            if (this.haveChestsAppeared == false && this.leader.totalLightBlocks == 0)
            {
                for (int i = 0; i < this.leader.connectedChests.Length; i++)
                {
                    this.leader.connectedChests[i].LightUpReveal();
                }
                this.haveChestsAppeared = true;
                foreach (var lightBlock in this.chunk.subsetOfItemsOfTypeLightUpBlock)
                {
                    lightBlock.haveChestsAppeared = true;
                }
            }
        }
    }

    private void OnCollisionFromBelow(Collision collision)
    {
        this.hasLitUp = true;
    }

    private void OnCollisionFromAbove(Collision collision)
    {
        this.hasLitUp = true;
    }

    private void OnCollisionFromLeft(Collision collision)
    {
        this.hasLitUp = true;
    }

    private void OnCollisionFromRight(Collision collision)
    {
        this.hasLitUp = true;
    }

    private System.Collections.IEnumerator _SquashStretch(Vector2 squashScale, float time)
    {
        this.scaleTarget = new Vector3(squashScale.x * xScaleValue, squashScale.y, 1f);
        yield return new WaitForSeconds(time);
        this.scaleTarget = new Vector3(xScaleValue, 1f, 1f);
    }

    public override void Reset()
    {
        base.Reset();
        if(this.haveChestsAppeared == false)
        {
            this.hasLitUp = false;
            this.glow.enabled = false;
            this.animated.PlayAndLoop(this.animationOff);
            this.hasPlayedOnce = false;

            if (this.leader != null)
            {
                if (this == this.leader)
                {
                    this.totalLightBlocks = 0;
                    foreach (var lightBlock in this.chunk.subsetOfItemsOfTypeLightUpBlock)
                    {
                        this.totalLightBlocks++;
                    }
                }
            }
        }
    }
}
