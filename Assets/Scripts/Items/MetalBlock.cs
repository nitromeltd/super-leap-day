﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MetalBlock : Item
{
    public Sprite[] sprites;
    public int spanX;
    public int spanY;

    public Transform spriteTransform;
    public Animated spriteAnimated;
    public SpriteRenderer spriteRend;
    public Animated.Animation animationHit;
    public SpinBlock controlledBySpinBlock;

    private Vector2 initialPosition;
    private Vector2 targetPosition;
    private Vector2 smoothVelocity;
    private float smoothTime;
    private int headbuttTimer = 0;
    private Sprite initialSprite;

    private const int ReturnTime = 3;

    private bool becameGold = false;
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.spriteRend.sprite = this.initialSprite = Util.RandomChoice(this.sprites);

        MakeSolid();
        this.onCollisionFromBelow = (_) => Headbutt();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        this.targetPosition = this.initialPosition = this.spriteTransform.position = this.position;
        this.smoothTime = 0f;
        this.becameGold = false;

        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromLeft = OnCollision;
        this.onCollisionFromRight = OnCollision;
    }

    private void MakeSolid()
    {
        var hitbox = new Hitbox(
            this.spanX * -0.5f,
            this.spanX * 0.5f,
            this.spanY * -0.5f,
            this.spanY * 0.5f,
            this.rotation,
            Hitbox.Solid,
            true,
            true
        );
        this.hitboxes = new [] { hitbox };
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
        {
            this.position = this.group.FollowerPosition(this);
        }

        if(this.headbuttTimer > 0)
        {
            this.headbuttTimer -= 1;

            if(this.headbuttTimer <= 0)
            {
                EndHeadbutt();
            }
        }

        if (this.controlledBySpinBlock == null)
        {
            if (Vector2.Distance(this.spriteTransform.position, this.targetPosition) > .1f)
            {
                this.spriteTransform.position = Vector2.SmoothDamp(this.spriteTransform.position, this.targetPosition, ref smoothVelocity, this.smoothTime);
            }
            else
            {
                this.spriteTransform.position = this.targetPosition;
            }
        }

        this.transform.position = this.position;
    }

    private void Headbutt()
    {
        Player player = this.map.player;
        if(player.gravity.IsZeroGravity() == true && player.state == Player.State.Normal) return;
        if(player.state == Player.State.InsideWater) return;

        this.targetPosition = this.initialPosition.Add(0f, 1f);
        this.smoothTime = .025f;

        this.spriteAnimated.PlayOnce(animationHit, () => 
        {
            this.spriteRend.sprite = this.initialSprite;
        });

        var center = this.hitboxes[0].InWorldSpace().center;

        int countMin, countMax;
        if (this.spanX == 2 && this.spanY == 2)
            (countMin, countMax) = (11, 13);
        else if (this.spanX == 2 || this.spanY == 2)
            (countMin, countMax) = (8, 12);
        else
            (countMin, countMax) = (5, 11);
        
        Particle.CreateDust(
            this.transform.parent,
            countMin,
            countMax,
            center,
            this.spanX * 0.5f,
            this.spanY * 0.5f,
            velocityVariationY: 0.05f
        );
        
        this.headbuttTimer = ReturnTime;

        if(this.map.player.midasTouch.isActive == true && this.becameGold == false)
        {
            StartCoroutine(BecomeGold());
        }
    }

    private void EndHeadbutt()
    {
        this.targetPosition = this.initialPosition;
        this.smoothTime = .05f;
        
        this.spriteRend.sprite = this.initialSprite;
        
        Rect hitbox = new Rect(
            this.position.x - (this.spanX * 0.5f),
            this.position.y + (this.spanY * 0.5f),
            this.spanX,
            .2f
        );
        this.map.Damage(
            hitbox,
            new Enemy.KillInfo { itemDeliveringKill = this, freezeTime = true }
        );
    }

    public bool IsBeingHeadbutted()
    {
        return this.headbuttTimer > 0;
    }

    IEnumerator BecomeGold()
    {
        this.becameGold = true;
        this.spriteRend.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRend.material.color = Color.white;
        yield return new WaitForSeconds(.1f);

        Coin coin = Coin.CreateSilver(this.position + Vector2.up * .5f, this.chunk);
        coin.movingFreely = false;
        coin.Collect();

        this.spriteRend.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRend.material.color = Color.black;
        yield return new WaitForSeconds(.1f);
        this.spriteRend.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRend.material.color = new Color(255, 165, 0);
        yield return new WaitForSeconds(.1f);
        this.spriteRend.material = Assets.instance.spriteGoldTint;
        yield return new WaitForSeconds(.5f);
    }

    private void OnCollision(Collision collision)
    {
        if (this.map.player.midasTouch.isActive == true && this.becameGold == false && collision.otherPlayer != null)
        {
            StartCoroutine(BecomeGold());
        }
    }

    public override void Reset()
    {
        base.Reset();
        if (this.becameGold == true)
        {
            this.becameGold = false;
            this.spriteRend.material = Assets.instance.spritesDefaultMaterial;
        }
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        MakeSolid();
    }
}
