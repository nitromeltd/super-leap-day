﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class Mine : Item
{    
    public Sprite particleSprite;
    public Sprite idleSprite;
    public AnimationCurve scaleOutCurve;
    public Sprite[] blinkSprites = new Sprite[2];

    private int blinkCounter;
    private int explodeCounter;
    private bool alive;
    private const int ExplodeDelay = 30;
    private const float ExplodeKillRadius = 2f;
    private const float TriggerRadius = 2f;
    private const float MaxDetectRadius = 10f;
    public override void Init(Def def)
    {
        base.Init(def);
        this.alive = true;
        blinkCounter = 0;
        explodeCounter = 0;
    }

    public override void Advance()
    {
        base.Advance();
        
        if(!this.alive)
        {
            return;
        }

        if(!this.map.desertAlertState.IsGloballyActive() )
        {
            if(spriteRenderer.sprite != idleSprite)
                spriteRenderer.sprite = idleSprite;
            return;
        }

        float distance =  (this.map.player.position - this.position).magnitude;        
        if(this.explodeCounter > 0)
        {            
            spriteRenderer.sprite = blinkSprites[this.explodeCounter % 2];          

            this.explodeCounter--;
            if(this.explodeCounter == 0)
            {
                Particle.CreateAndPlayOnce(
                    Assets.instance.explosionAnimation,
                    this.position,
                    this.transform.parent
                );
                CreateExplosionParticles(this.position);
                Destroy(this.gameObject);
                this.chunk.items.Remove(this);
                this.alive = false;
                Rect explosionRect = new Rect(this.position.x - 2, this.position.y - 2, 4, 4);
                this.map.Damage(
                    explosionRect,
                    new Enemy.KillInfo { itemDeliveringKill = this }
                );

                if(distance < ExplodeKillRadius)
                {
                    this.map.player.Hit(this.position);
                }

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvExplosion,
                    position: this.position
                );
            }
            return;
        }

        if (this.map.desertAlertState.IsGloballyActive())
        {
            if (distance > MaxDetectRadius)
            {
                if (spriteRenderer.sprite != blinkSprites[0])
                {
                    spriteRenderer.sprite = blinkSprites[0];                    
                }
                blinkCounter = 0;
            }
            else
            {
                int max = Mathf.CeilToInt(distance);
                if (++blinkCounter > max)
                {
                    blinkCounter = 0;
                    if (this.spriteRenderer.sprite == this.blinkSprites[0])
                    {
                        this.spriteRenderer.sprite = this.blinkSprites[1];
                    }
                    else
                    {
                        this.spriteRenderer.sprite = this.blinkSprites[0];
                    }
                }
            }

            if(distance < TriggerRadius)
            {
                this.explodeCounter = ExplodeDelay;
            }

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvMine,
                position: this.position
            );
        }
        else
        {
            if (this.spriteRenderer.sprite != this.blinkSprites[0])
            {
                this.spriteRenderer.sprite = this.blinkSprites[0];                    
            }
        }
    }

    private void CreateExplosionParticles(Vector3 centrePos)
    {
        for (int i = 0; i < 30; i ++)
        {
            Vector3 position = new Vector3(
                centrePos.x + Rnd.Range(-0.2f, 0.2f),
                centrePos.y + Rnd.Range(-0.3f, 0.3f)
                );
            Vector2 velocity = (position - centrePos).normalized * Rnd.Range(0.2f,0.3f);  
            position = centrePos + (position - centrePos).normalized * Rnd.Range(0.8f, 0.9f);              
            CreateTrailParticle(position, velocity / 16, 140, 64 + i);
        }
    }

    private void CreateTrailParticle(Vector3 particlePosition, Vector2 particleVelocity, int lifeTime, int sortingOrderOffset = 0)
    {
        Particle trailParticle = Particle.CreateWithSprite(
                this.particleSprite,
                lifeTime,
                particlePosition,
                this.transform.parent
            );
        float s = Rnd.Range(0.8f, 1.4f);
        trailParticle.transform.localScale = new Vector3(s, s, 1);
        trailParticle.ScaleOut(this.scaleOutCurve);
        trailParticle.velocity = particleVelocity;
        trailParticle.spriteRenderer.sortingLayerName = "Player Trail (BL)";
        trailParticle.spriteRenderer.sortingOrder = -(1 + sortingOrderOffset + (this.map.frameNumber % 32));
    }

    private Vector2 Forward() 
    {
        return new Vector2(Mathf.Cos(this.rotation * Mathf.Deg2Rad), Mathf.Sin(this.rotation * Mathf.Deg2Rad));
    }
}
