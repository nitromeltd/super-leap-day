using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using Rnd = UnityEngine.Random;

public class MineCannon : Item
{
    public Animated.Animation animationNozzleIdle;
    public Animated.Animation animationNozzleFire;

    private Animated nozzle;
    private Transform cannon;
    private Transform explosion;
    private SpriteRenderer baseSR;
    private SpriteRenderer baseOutlineSR;
    private SpriteRenderer backBitSR;
    private SpriteRenderer textureSR;
    private SpriteRenderer nozzleSR;

    public const int MaximumTimeOfFlyingAsBall = 60;
    public const int MaximumTimeOfFlyingWithoutGravity = 50;
    public Sprite particleSprite;
    public AnimationCurve scaleOutCurve;
    private int mineCannonReenterDelayTime = 0;    
    private float targetSpeedOnPath;
    private float currentSpeedOnPath;
    private bool isPlayerInsideCannon = false;
    public const int PlayerFlyAsBallFrames = 60;
    private NitromeEditor.Path path;
    private float currentTimeOnPath = 0;
    private float currentRotation = 0;
    private float startAngleDegrees;
    private float endAngleDegrees;

    private float? autoRotationMinAngleDegress;
    private float? autoRotationMaxAngleDegress;
    private float autoRotationTargetAngleDegress;
    private int autoRotationSwitchDirectionDelay;

    private int snapBackTime;
    private int turnAtEndRotationDelay;

    private const int TurnAtEndMaxRotationDelay = 90;
    private const float DefaultCannonSpeed = 2;

    //editor properties
    private string endBehaviour;
    private string movementType;
    private string rotationType;
    private float coneFOVAngle;
    private float rotationSpeed;
    private int movementSwitchDirectionDelay;
    private Tweener tweener;
    private Coroutine throbbingCoroutine;

    public override void Init(Def def)
    {
        base.Init(def);

        this.cannon = this.transform.Find("Cannon").transform;
        this.explosion = this.transform.Find("Explosion").transform;
        this.nozzle = cannon.Find("Nozzle").GetComponent<Animated>();
        this.nozzle.PlayOnce(this.animationNozzleIdle);
        this.baseSR = this.transform.Find("Base").GetComponent<SpriteRenderer>();
        this.baseOutlineSR = cannon.Find("BaseOutline").GetComponent<SpriteRenderer>();
        this.backBitSR = cannon.Find("BackBit").GetComponent<SpriteRenderer>();
        this.textureSR = this.transform.Find("Texture").GetComponent<SpriteRenderer>();
        this.nozzleSR = cannon.Find("Nozzle").GetComponent<SpriteRenderer>();
        this.startAngleDegrees = this.rotation;

        if(def.tile.flip)
        {
            switch(this.rotation)
            {
                case 0:
                    this.startAngleDegrees = this.currentRotation = this.rotation =  180;
                    break;
                case 90:
                    this.startAngleDegrees = this.currentRotation = this.rotation = 270;
                    break;
                case 180:
                    this.startAngleDegrees = this.currentRotation = this.rotation = 0;
                    break;
                case 270:
                    this.startAngleDegrees = this.currentRotation = this.rotation = 90;
                    break;
                default:
                    break;
            }
            cannon.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        }

        var directionNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 0.50f);
        if(directionNode != null)
        {            
            var edge = directionNode.path.edges[0];
            float angle = Vector2.SignedAngle(Vector2.right, edge.direction);
            this.rotation = 0;
            this.startAngleDegrees = this.currentRotation = angle;
            cannon.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
        else
        {
            this.startAngleDegrees = this.currentRotation = this.rotation;
        }

        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 0.50f);

        if(this.path != null)
        {
            var lastPointOnPath = this.path.PointAtDistanceAlongPath(this.path.length).position;
            var lastDirectionNode = this.chunk.connectionLayer.NearestNodeTo(lastPointOnPath, 0.50f);
            if(lastDirectionNode != null)
            {
                var edge = lastDirectionNode.path.edges[0];
                float angle = Vector2.SignedAngle( Vector2.right, edge.direction);
                this.endAngleDegrees = angle;                
            }

            DrawPathDots();
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));


        if (def.tile.properties?.ContainsKey("end_behaviour") == true)
            this.endBehaviour = def.tile.properties["end_behaviour"].s;
        else
            this.endBehaviour = "stop";

        if (def.tile.properties?.ContainsKey("movement_type") == true)
            this.movementType = def.tile.properties["movement_type"].s;
        else
            this.movementType = "when_player_in";

        if(this.movementType == "always")
        {
            this.targetSpeedOnPath = DefaultCannonSpeed;
        }

        if (def.tile.properties?.ContainsKey("rotation_type") == true)
        {
            this.rotationType = def.tile.properties["rotation_type"].s;
            if(this.chunk.isFlippedHorizontally)
            {
                if (this.rotationType == "clockwise_continuous")
                {
                    this.rotationType = "counterclockwise_continuous";
                }
                else if (this.rotationType == "counterclockwise_continuous")
                {
                    this.rotationType = "clockwise_continuous";
                }
            }
        }
        else
            this.rotationType = "none";

        if (def.tile.properties?.ContainsKey("cone_fov_angle") == true)
            this.coneFOVAngle = def.tile.properties["cone_fov_angle"].f;
        else
            this.coneFOVAngle = 0;

        if (def.tile.properties?.ContainsKey("rotation_speed") == true)
            this.rotationSpeed = def.tile.properties["rotation_speed"].f;
        else
            this.rotationSpeed = 1;

        if (this.rotationType == "cone")
        {
            this.autoRotationMinAngleDegress = this.startAngleDegrees - this.coneFOVAngle / 2;
            this.autoRotationMaxAngleDegress = this.startAngleDegrees + this.coneFOVAngle / 2;
            this.autoRotationTargetAngleDegress = this.autoRotationMaxAngleDegress.Value;
        }

        this.cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        CreateSpawnParticles(Vector2.zero);
    }

    private void CreateSpawnParticles(Vector2 offset)
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position + offset,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }


    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
            AdvanceInPlace();
        }

        if(this.path != null)
        {
            AdvanceOnPath();
        }
        else
        {
            AdvanceInPlace();
        } 
        UpdatePlayerPosition();
        AdvanceAutoRotation();
        CheckIfShouldRemovedNoGravity();
    }

    private IEnumerator ThrobCannon()
    {
        var tweener = new Tweener();
        tweener.ScaleLocal(this.transform, new Vector3(0.9f,1.1f,1), 0.5f, Easing.Linear);
        
        while (tweener.IsDone() == false)
        {
            tweener.Advance(1.0f / 60f);
            yield return null;
        }

        tweener = new Tweener();
        tweener.ScaleLocal(this.transform, new Vector3(1.1f,0.9f,1), 0.5f, Easing.Linear);
        while (tweener.IsDone() == false)
        {
            tweener.Advance(1.0f / 60f);
            yield return null;
        }

        this.throbbingCoroutine = StartCoroutine(ThrobCannon());
    }

    private void SnapBackWhenShoot()
    {
        if(this.snapBackTime > 0)
        {
            snapBackTime--;
            cannon.localPosition = new Vector3(-0.2f, 0);
            
            if(snapBackTime == 0)
            {
                cannon.localPosition = Vector3.zero;
                
            }

        }
    }

    private void CheckIfShouldRemovedNoGravity()
    {
        var player = this.map.player;
        if(player.state == Player.State.FireFromMineCannon)
        {
            if(Mathf.Abs(player.velocity.magnitude) < 0.02f)
            {
                player.timeOfFlyingAsBallWhenFiredFromMineCannon = 0;
            }
        }
    }

    private void AdvanceAutoRotation()
    {
        double ElasticEaseOut(double t, double b, double c, double d)
        {
            if((t /= d) == 1)
                return b + c;

            double p = d * .5;    
            double s = p / 4;

            return (c * Math.Pow(2, -10 * t) * Math.Sin((t * d - s) * (2 * Math.PI) / p) + c + b);
        }

        if (this.rotationType == "cone")
        {
            if (this.autoRotationSwitchDirectionDelay > 0)
            {
                this.autoRotationSwitchDirectionDelay--;
                return;
            }

            if (this.autoRotationMaxAngleDegress != null && this.autoRotationMinAngleDegress != null)
            {
                if (this.currentRotation < this.autoRotationTargetAngleDegress)
                {
                    this.currentRotation += this.rotationSpeed;
                    if (this.currentRotation >= this.autoRotationTargetAngleDegress)
                    {
                        this.autoRotationTargetAngleDegress = this.autoRotationMinAngleDegress.Value;
                        this.autoRotationSwitchDirectionDelay = 60;
                    }
                }

                if (this.currentRotation > this.autoRotationTargetAngleDegress)
                {
                    this.currentRotation -= this.rotationSpeed;
                    if (this.currentRotation <= this.autoRotationTargetAngleDegress)
                    {
                        this.autoRotationTargetAngleDegress = this.autoRotationMaxAngleDegress.Value;
                        this.autoRotationSwitchDirectionDelay = 60;
                    }
                }
                cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
            }
        }
        else if(this.rotationType == "turn_at_end")
        {
            if(this.turnAtEndRotationDelay > 0)
            {
                this.turnAtEndRotationDelay--;
                var t = (TurnAtEndMaxRotationDelay - this.turnAtEndRotationDelay) / (float)TurnAtEndMaxRotationDelay;
                if(this.currentTimeOnPath == 0)
                {
                    t = (float)ElasticEaseOut(t,0,1,1);
                    this.currentRotation = this.endAngleDegrees + (this.startAngleDegrees - this.endAngleDegrees) * t;
                }
                else
                {
                    t = (float)ElasticEaseOut(t,0,1,1);
                    this.currentRotation = this.startAngleDegrees + (this.endAngleDegrees - this.startAngleDegrees) * t; 
                }
                cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
            }
        }
    }

    private void AdvanceInPlace()
    {
        CheckIfPlayerGetsInsideCannon();

        if (this.rotationType == "clockwise_continuous")
        {
            this.currentRotation = Util.WrapAngle0To360(this.currentRotation - this.rotationSpeed);
            cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
        }
        else if (this.rotationType == "counterclockwise_continuous")
        {
            this.currentRotation = Util.WrapAngle0To360(this.currentRotation + this.rotationSpeed);
            cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
        }
    }

    private void AdvanceOnPath()
    {
        CheckIfPlayerGetsInsideCannon();

        if (this.rotationType == "clockwise_continuous")
        {
            this.currentRotation = Util.WrapAngle0To360(this.currentRotation - this.rotationSpeed);
            cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
        }
        else if (this.rotationType == "counterclockwise_continuous")
        {
            this.currentRotation = Util.WrapAngle0To360(this.currentRotation + this.rotationSpeed);
            cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
        }

        if(this.movementSwitchDirectionDelay > 0)
        {
            this.movementSwitchDirectionDelay--;
            return;
        }

        if(this.turnAtEndRotationDelay > 0)
        {
            return;
        }

        if((IsPlayerInsideThisCannon()) || this.movementType == "always")
        {
            this.currentTimeOnPath = this.currentTimeOnPath + this.currentSpeedOnPath / 60.0f;

            if(this.currentTimeOnPath > this.path.duration)
            {
                this.currentTimeOnPath = this.path.duration;
                if (this.endBehaviour == "stop")
                {
                    this.currentSpeedOnPath = this.targetSpeedOnPath = 0;
                }
                else if (this.endBehaviour == "bounce")
                {
                    this.movementSwitchDirectionDelay = 60;
                    this.targetSpeedOnPath = -this.targetSpeedOnPath;
                    this.currentSpeedOnPath = -this.currentSpeedOnPath;
                    if (this.rotationType == "turn_at_end")
                    {
                        this.turnAtEndRotationDelay = TurnAtEndMaxRotationDelay;
                    }
                }
            }

            if(this.currentTimeOnPath < 0)
            {
                this.currentTimeOnPath = 0;
                if (this.endBehaviour == "stop" || (this.movementType == "when_player_in_or_return" && IsPlayerInsideThisCannon() == false))
                {
                    this.currentSpeedOnPath = this.targetSpeedOnPath = 0;
                }
                else if (this.endBehaviour == "bounce")
                {
                    this.movementSwitchDirectionDelay = 60;
                    this.targetSpeedOnPath = -this.targetSpeedOnPath;
                    this.currentSpeedOnPath = -this.currentSpeedOnPath;
                    if (this.rotationType == "turn_at_end")
                    {
                        this.turnAtEndRotationDelay = TurnAtEndMaxRotationDelay;
                    }
                }
            }
 
            this.position = this.path.PointAtTime(this.currentTimeOnPath).position;
        }
        else if(IsPlayerInsideThisCannon() == false && this.movementType == "when_player_in_or_return")
        {
            this.currentTimeOnPath = this.currentTimeOnPath + this.currentSpeedOnPath / 60.0f;

            if(this.currentTimeOnPath < 0)
            {
                this.currentTimeOnPath = 0;
                if (this.endBehaviour == "stop" || (this.movementType == "when_player_in_or_return" && IsPlayerInsideThisCannon() == false))
                {
                    this.currentSpeedOnPath = this.targetSpeedOnPath = 0;
                }
                else if (this.endBehaviour == "bounce")
                {
                    this.movementSwitchDirectionDelay = 60;
                    this.targetSpeedOnPath = -this.targetSpeedOnPath;
                    this.currentSpeedOnPath = -this.currentSpeedOnPath;
                    if (this.rotationType == "turn_at_end")
                    {
                        this.turnAtEndRotationDelay = TurnAtEndMaxRotationDelay;
                    }
                }
            }
            
            this.position = this.path.PointAtTime(this.currentTimeOnPath).position;
        }

        this.currentSpeedOnPath = Util.Slide(this.currentSpeedOnPath, this.targetSpeedOnPath, 0.50f);       

        if (this.rotationType == "tweening_start_end")
        {
            this.currentRotation = this.startAngleDegrees + (this.endAngleDegrees - this.startAngleDegrees) * (this.currentTimeOnPath / this.path.duration);
        }


        this.transform.position = this.position;
        cannon.transform.rotation = Quaternion.Euler(0, 0, this.currentRotation);
    }

    private void CheckIfPlayerGetsInsideCannon()
    {
        if(this.isPlayerInsideCannon)
        {
            return;
        }

        if(this.mineCannonReenterDelayTime>0)
        {
            this.mineCannonReenterDelayTime--;
            return;
        }

        var player = this.map.player;

        bool isOverlapping = (player.position - this.position).magnitude < 1.5f;

        if ((player.state == Player.State.Normal 
            || player.state == Player.State.Jump 
            || player.state == Player.State.FireFromMineCannon
            || player.state == Player.State.InsideMinecart
            )
            && isOverlapping)
        {
            if(player.state == Player.State.InsideMinecart)
            {
                player.GetMinecart()?.DestroyMinecartAndEjectPlayer();
            }

            player.EnterMineCannon(this);
            this.isPlayerInsideCannon = true;
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvBoxEnter,
                position: this.position
            );
            Audio.instance.PlaySfx(Assets.instance.sfxEnvPipeIn, position: this.position, options: new Audio.Options(1, false, 0));
            CreateEnterLeaveParticles();
            FlashCannon();
            StartCoroutine("VibrateCannon");

            this.throbbingCoroutine = StartCoroutine(ThrobCannon());

            if(this.path != null)
            {
                if(this.currentTimeOnPath == 0)
                {
                    this.targetSpeedOnPath = DefaultCannonSpeed;
                }

                if(this.currentTimeOnPath == this.path.duration)
                {
                    this.targetSpeedOnPath = -DefaultCannonSpeed;
                }
            }
        }
    }
    
    IEnumerator VibrateCannon()
    {
        this.tweener = new Tweener();
        this.tweener.MoveLocal(this.cannon, new Vector3(-0.4f,0), 3f/60);
        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        this.tweener = new Tweener();
        this.tweener.MoveLocal(this.cannon, new Vector3(0.4f,0), 6f/60);
        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        this.tweener = new Tweener();
        this.tweener.MoveLocal(this.cannon, Vector3.zero, 3f/60);
        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
    }

    private void CreateEnterLeaveParticles()
    {
        for (int i = 0; i < 30; i ++)
        {
            Vector3 particlePosition = new Vector3(
                this.transform.position.x + Rnd.Range(-0.2f, 0.2f),
                this.transform.position.y + Rnd.Range(-0.3f, 0.3f)
                );
            var particleVelocity = (particlePosition - this.transform.position).normalized * Rnd.Range(0.8f,0.9f);  
            particlePosition = this.transform.position + (particlePosition - this.transform.position).normalized * Rnd.Range(0.8f, 0.9f);              
            CreateTrailParticle(particlePosition, particleVelocity / 16, 30, 64 + i);
        }
    }

    private void CreateTrailParticle(Vector3 particlePosition, Vector2 particleVelocity, int lifeTime, int sortingOrderOffset = 0)
    {
        Particle trailParticle = Particle.CreateWithSprite(
                this.particleSprite,
                lifeTime,
                particlePosition,
                this.transform.parent
            );
        float s = Rnd.Range(0.8f, 1.4f);
        trailParticle.transform.localScale = new Vector3(s, s, 1);
        trailParticle.ScaleOut(this.scaleOutCurve);
        trailParticle.velocity = particleVelocity;
        trailParticle.spriteRenderer.sortingLayerName = "Items (Front)";
        trailParticle.spriteRenderer.sortingOrder = 500 + (1 + sortingOrderOffset + (this.map.frameNumber % 32));
    }


    private bool IsPlayerInsideThisCannon()
    {
        return (this.map.player.GetMineCannon() == this);
    }

    public void PlayerJumped()
    {
        if (this.map.player.GetMineCannon() == this)
        {
            if(this.movementType == "when_player_in_or_return")
            {
                this.currentSpeedOnPath = this.targetSpeedOnPath = -DefaultCannonSpeed;
            }
            this.map.player.ExitMineCannon();
            this.mineCannonReenterDelayTime = 10;
            this.map.player.velocity = cannon.transform.rotation * new Vector2(0.4f,0);
            this.map.player.position = this.position + (Vector2)(cannon.transform.rotation * new Vector2(1.5f, 0));
            this.isPlayerInsideCannon = false;
            this.map.player.state = Player.State.FireFromMineCannon;
            this.map.player.timeOfFlyingAsBallWhenFiredFromMineCannon = MaximumTimeOfFlyingAsBall;
            this.nozzle.PlayOnce(this.animationNozzleFire);
            this.snapBackTime = 10;
            CreateCannonFiredExplosion();
            SnapBackCannon(new Vector3(-0.2f, 0));
            FlashCannon();
            if (this.throbbingCoroutine != null)
            {
                StopCoroutine(this.throbbingCoroutine);
                this.throbbingCoroutine = null;
            }
            StartCoroutine("ScaleBackToOne");
            Audio.instance.PlaySfx(Assets.instance.sfxEnvPipesCannon, position: this.position, options: new Audio.Options(1, false, 0));
        }
    }

    private IEnumerator ScaleBackToOne()
    {
        var tweener = new Tweener();
        tweener.ScaleLocal(this.transform, new Vector3(1f,1f,1), 0.2f, Easing.Linear);
        
        while (tweener.IsDone() == false)
        {
            tweener.Advance(1.0f / 60f);
            yield return null;
        }
    }

    private void SnapBackCannon(Vector2 localOffset)
    {
        cannon.localPosition = localOffset;
        Invoke("TurnOffSnapBack", 10f / 60);
    }

    private void TurnOffSnapBack()
    {
        cannon.localPosition = Vector3.zero;
    }

    private void FlashCannon()
    {
        baseSR.material.SetFloat("_Intensity", 1);
        baseOutlineSR.material.SetFloat("_Intensity", 1);
        backBitSR.material.SetFloat("_Intensity", 1);
        textureSR.material.SetFloat("_Intensity", 1);
        nozzleSR.material.SetFloat("_Intensity", 1);
        Invoke("TurnOffTint", 4f / 60);
    }

    private void TurnOffTint()
    {
        baseSR.material.SetFloat("_Intensity", 0);
        baseOutlineSR.material.SetFloat("_Intensity", 0);
        backBitSR.material.SetFloat("_Intensity", 0);
        textureSR.material.SetFloat("_Intensity", 0);
        nozzleSR.material.SetFloat("_Intensity", 0);
    }

    private void CreateCannonFiredExplosion()
    {
        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position + (Vector2)(cannon.transform.rotation * new Vector2(2.5f, 0)),
            this.transform.parent
        );

        p.spriteRenderer.sortingLayerName = "Items (Front)";
    }

    private void UpdatePlayerPosition()
    {
        if (this.map.player.GetMineCannon() == this)
        {
            float currentAngle = Util.WrapAngleMinus180To180(cannon.transform.rotation.eulerAngles.z - 90);
            this.map.player.velocity = Vector2.zero;
            this.map.player.position = this.position;
            this.map.player.groundState.angleDegrees = currentAngle;
        }
    } 

    public void ExitMineCannonCleanup()
    {

    }

    private void DrawPathDots()
    {
        // if another MovementAnchor earlier in the chunk's item list already
        // drew dots for this path, then don't redraw them again.
        foreach (var item in this.chunk.items)
        {
            if (item == this) break;
            if ((item as MineCannon)?.path == this.path) return;
        }

        var edges = new List<Path.Edge>();
        float startDistance = 0;

        void DrawEdges()
        {
            if (edges.Count == 0) return;

            float totalLength = 0;
            foreach (var e in edges)
                totalLength += e.distance;

            int totalDots = Mathf.RoundToInt(totalLength);
            if (totalDots < 1)
                totalDots = 1;

            for (int n = 0; n < totalDots; n += 1)
            {
                float through = (float)n / (float)totalDots;
                float dist = startDistance + (through * totalLength);
                Vector2 pt = this.path.PointAtDistanceAlongPath(dist).position;
                Instantiate(
                    Assets.instance.movementPathDot,
                    pt,
                    Quaternion.identity,
                    this.chunk.transform
                ).name = "dot MineCannon";
            }

            startDistance += totalLength;
            edges.Clear();
        }

        foreach (var edge in this.path.edges)
        {
            edges.Add(edge);
            if (edge.to.arcLength == 0)
                DrawEdges();
        }
        DrawEdges();
    }

    private void OnDestroy()
    {
        if(this.gameObject.scene.isLoaded == false) return;

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }
}