using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Rnd = UnityEngine.Random;
public class Minecart : Item
{
    public bool minecartWithEnemy;
    public Animated.Animation animationIdle;
    public Animated.Animation animationWobble;
    public Animated.Animation animationTumble;
    public Animated.Animation animationFlip;
    public Animated.Animation animationSparks;
    public Animated.Animation animationEmpty;    
    public Animated.Animation animationLand;
    public Animated.Animation animationJump;
    public Animated.Animation animationExplosionParticle;
    public Animated.Animation animationEnemyIdle;
    public Animated.Animation animationEnemyWobble;
    public Animated.Animation animationEnemyLand;
    public Animated.Animation animationEnemyJump;
    public Animated.Animation animationEnemyHide;
    public Animated.Animation animationEnemyPopOut;
    public Animated.Animation animationEnemyRideHidden;
    public Animated.Animation animationRailParticle;
    public Animated.Animation animationRailLandingBigParticle;
    public Animated.Animation animationRailLandingParticle;
    public Enemy.DeathAnimation deathAnimation;

    public RailDoor insideRailDoor = null;
    protected string movementType;
    public bool facingRight = false;        //on tiles
    private bool initialFacingRight;

    public Vector2 velocity;
    NitromeEditor.Path currentRail = null;
    
    private float targetRotation;
    private bool crashed = false;
    protected GameObject lightObject;
    protected GameObject enemyLightObject;
    protected GameObject enemyObject;
    private GameObject enemyHelmetObject;
    private GameObject enemyHelmetObjectUpsideDown;
    private GameObject backWheelsObject;

    const float RegularSpeedOnRail  = 0.20f;
    const float UpwardSpeedOnRail   = 0.15f;
    const float DownwardSpeedOnRail = 0.40f;
    const float BoostSpeedOnRail    = 0.30f;
    const float MaximumSpeedOnRail  = 0.40f;
    const float Acceleration        = 0.0025f;
    int ignoreNFrames = 0;
    int boostTime = 0;
    private int framesInAir = 0;
    private bool freeFalling = false;    
    [HideInInspector] public bool isPlayerInsideCart = false;    
    public float currentDistanceOnRail = 0;
    public float lastDistanceOnRail = 0;
    float currentSpeedOnRail = 0f;
    float targetSpeedOnRail = 0f;

    private bool reversedPath;
    private bool headlightTurnedOn;

    private int playerDoesntReenterForFrames = 0;
    private int dontReattachToRailForFrames = 0;
    private int dontCheckForDoorFrames = 0;
    private int insideDoorDelayTime = 0;
    private int tryCatchingPlayerForFrames = 0;
    private bool minecartExitedDoor = false;

    public List<Tile> lastTouchedTiles = new List<Tile>();
    private Vector2 spawnPosition;
    
    private Animated sparksAnimated;
    private const float GravityForce = 0.02f;
    private const int yValuesMaxCount = 10;
    private const float MaxSpeedOnRail = 0.3f;
    private const float MinecartDestroyedVelocityY = 0.3f;
    private const float JumpStrength = 0.55f;
    private Vector2[] lightOffsetWhenDoingFlip = {
        new Vector2(1.04f, 0.5f),
        new Vector2(1.04f, 0.5f),
        new Vector2(1.04f, 0.02f),
        new Vector2(1.15f, 0.03f),
        new Vector2(1.17f, -0.29f),
        new Vector2(1.2f, -0.29f)
    };

    private int correctionFrames;
    private Vector2 correctionPerFrame;

    public bool isTrigger = false;
    private float enterMinecartWobbleCnt = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(-0.6f, 0.6f, -1f, 1.5f, this.rotation, Hitbox.NonSolid),
            new Hitbox(-0.5f, 0.5f, 0.8f, 1.0f, this.rotation, Hitbox.NonSolid)
        };

        this.lightObject = this.transform.Find("Light").gameObject;
        if (this.minecartWithEnemy)
        {
            this.enemyLightObject = this.transform.Find("EnemyLight").gameObject;
            this.enemyObject = this.transform.Find("Enemy").gameObject;
            this.enemyHelmetObject = this.transform.Find("Helmet").gameObject;
            this.enemyHelmetObjectUpsideDown = this.transform.Find("HelmetUpsideDown").gameObject;
        }
        
        this.sparksAnimated = this.transform.Find("Sparks").GetComponent<Animated>();
        this.backWheelsObject = this.transform.Find("BackWheels").gameObject;

        this.spawnPosition = this.transform.position;
        this.facingRight = this.initialFacingRight = !def.tile.flip;

        if (def.tile.properties?.ContainsKey("movement_type") == true)
            this.movementType = def.tile.properties["movement_type"].s;
        else
            this.movementType = "when_player_in";

        if (this.minecartWithEnemy == false)
        {
            this.animated.PlayAndLoop(this.animationIdle);
        }
        else
        {
            if(isTrigger == false)
            {
                this.animated.PlayAndLoop(this.animationIdle);
            }
            else
            {
                this.animated.PlayAndLoop(this.animationEnemyIdle);
            }
            this.movementType = "always";
        }
        this.minecartExitedDoor = false;

        CreateSpawnParticles(Vector2.zero);
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.boostTime = 0;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
    }

    private void CreateSpawnParticles(Vector2 offset)
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position + offset,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }

    public virtual void ResetPosition()
    {
        this.position = this.transform.position = this.spawnPosition;
    }

    //////////////////////////
    // GENERIC PART
    //////////////////////////
    
    public bool HasCrashed()
    {
        return this.crashed;
    }

    public void GenerateSparksParticles()
    {
        if (this.sparksAnimated.currentAnimation == this.animationSparks && Time.frameCount % 20 == 1)
        {
            for (int i = 0; i < 4; i++)
            {
                Vector2 offsetSpark = new Vector2(this.facingRight ? -0.05f : 0.05f, 0.05f).normalized;
                offsetSpark = transform.rotation * offsetSpark;
                Particle spark = Particle.CreateAndPlayOnce(
                                    animationRailParticle,
                                    this.sparksAnimated.transform.position + (Vector3)offsetSpark,
                                    this.transform
                                );
                spark.spriteRenderer.sortingLayerName = "Items";
                spark.spriteRenderer.sortingOrder = -1;
                spark.spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
                Vector2 velocitySpark = new Vector2((this.facingRight ? -1f : 1f) * Random.Range(0.1f, 0.9f), Random.Range(0.1f, 0.9f)).normalized;
                velocitySpark = transform.rotation * velocitySpark;

                spark.velocity = Random.Range(0.03f, 0.06f) * velocitySpark;
            }
        }
    }

    public Vector2 GetWheelsPosition()
    {
        Vector2 wheelsOffset = new Vector2(0, -1);
        wheelsOffset = transform.rotation * wheelsOffset;

        return this.position + wheelsOffset;
    }


    public void BounceFromSpring(Vector2 velocity)
    {
        velocity.y = velocity.y / 1.5f;
        this.velocity = velocity;
    }

    public void BounceFromPushPlatform(Vector2 velocity, Vector2 correction, int durationFrames)
    {
        velocity.y = velocity.y / 1.5f;
        this.velocity = velocity;
        this.correctionFrames = durationFrames;
        this.correctionPerFrame = correction / durationFrames;
    }

    public override void Advance()
    {
        SetCorrectLayersAndMask();

        lastTouchedTiles.Clear();
        if(this.ignoreNFrames > 0)
            this.ignoreNFrames--;

        if(this.boostTime > 0)
            this.boostTime--;

        if (this.tryCatchingPlayerForFrames > 0)
            this.tryCatchingPlayerForFrames -= 1;

        if(this.crashed)
        {
            this.velocity.y -= GravityForce;
            this.position += this.velocity;
            this.transform.position = this.position;

            Rect cameraRectangle = this.map.gameCamera.VisibleRectangle().Inflate(4);

            if (cameraRectangle.Contains(this.position) == false)
            {
                this.currentDistanceOnRail = 0;
                this.currentSpeedOnRail = this.targetSpeedOnRail = 0;
                this.currentRail = null;
                ResetPosition();
                this.crashed = false;
                this.transform.rotation = Quaternion.identity;
                this.boostTime = 0;
                this.playerDoesntReenterForFrames = this.correctionFrames = this.dontCheckForDoorFrames = this.dontReattachToRailForFrames = 0;
                this.facingRight = this.initialFacingRight;
                this.lightObject.SetActive(false);
                this.headlightTurnedOn = false;
                this.velocity = Vector2.zero;
                this.minecartExitedDoor = false;
                this.insideRailDoor = null;
                if(this.minecartWithEnemy)
                {
                    this.enemyHelmetObject.SetActive(false);
                    this.enemyHelmetObjectUpsideDown.SetActive(false);

                    if (this.isTrigger == true)
                    {
                        this.enemyLightObject.SetActive(false);
                        this.enemyObject.SetActive(true);
                        this.animated.PlayAndLoop(this.animationIdle);
                    }
                    else
                    {
                        this.enemyLightObject.SetActive(true);
                        this.enemyObject.SetActive(false);
                        this.animated.PlayAndLoop(this.animationEnemyIdle);
                    }
                }
                else
                {
                    this.animated.PlayAndLoop(this.animationIdle);
                }
                CreateSpawnParticles(Vector2.zero);
            }
            return;
        }
        

        var raycast = new Raycast(this.map, this.position, ignoreItem1: this, ignoreItemTypes: new System.Type[] {typeof(Peg), typeof(CrystalBarrierBlock)});
        CheckIfPlayerGetsInsideCart();
        DebugDrawRotatedHitBox(Color.red);

        GenerateSparks();
        GenerateSparksParticles();
        UpdateDriveSfx();

        if (currentRail != null)
        {
            if(headlightTurnedOn == false && currentSpeedOnRail != 0)
            {
                this.lightObject.SetActive(true);
                headlightTurnedOn = true;
            }

            AdvanceOnRail(raycast);
            UpdateBackWheelsVisibility();
            CheckForCollisionsWithOtherCarts();
            if(this.minecartWithEnemy == true)
            {
                CheckForEnemyCollisionWithPlayer();
            }
            DebugDrawRotatedHitBox(Color.green);
            if (currentRail != null)
                UpdatePlayerPosition();

            this.correctionFrames = 0;
            return;
        }
        else
        {
            if(headlightTurnedOn == false && this.velocity.x != 0)
            {
                this.lightObject.SetActive(true);
                headlightTurnedOn = true;
            }
            this.velocity.y -= GravityForce;
 
            if (ShouldBeReattachedToRail())
            {
                UpdateAndSetRotation();
                DebugDrawRotatedHitBox(Color.green);
                UpdatePlayerPosition();
                UpdateBackWheelsVisibility();
                CheckForCollisionsWithOtherCarts();
                if (this.minecartWithEnemy == true)
                {
                    CheckForEnemyCollisionWithPlayer();
                }
                correctionFrames = 0;
                return;
            }

            if(this.correctionFrames > 0)
            {
                this.correctionFrames -= 1;
                this.position += this.correctionPerFrame;
            }

            AdvanceOnTiles();
            UpdateBackWheelsVisibility();
            CheckForCollisionsWithOtherCarts();
            if (this.minecartWithEnemy == true)
            {
                CheckForEnemyCollisionWithPlayer();
            }

            if(this.freeFalling == false)
            {
                this.correctionFrames = 0;
            }
        }        
    }

    private void SetCorrectLayersAndMask()
    {
        if(this.crashed == true && this.spriteRenderer.maskInteraction != SpriteMaskInteraction.None)
        {
            this.spriteRenderer.sortingLayerName = "Default";
            this.spriteRenderer.maskInteraction = SpriteMaskInteraction.None;
        }
        else if(this.crashed == false && this.spriteRenderer.maskInteraction != SpriteMaskInteraction.VisibleOutsideMask)
        {
            this.spriteRenderer.sortingLayerName = "Minecart";
            this.spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
        }
    }

    private void CheckForCollisionsWithOtherCarts()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if(minecart == this) continue;
            if (minecart.crashed == true || this.crashed == true) continue;

            if (minecart.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                minecart.DestroyMinecartAndEjectPlayer();
                this.DestroyMinecartAndEjectPlayer();
            }
        }
    }

    private void CheckForCollisionsWithItems()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        foreach (Chest chest in this.chunk.subsetOfItemsOfTypeChest)
        {
            if(chest.CanBeOpened() && 
                chest.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                chest.Open();
            }
        }
    }

    private void CheckForEnemyCollisionWithPlayer()
    {
        var player = this.map.player;
        if (CheckIfRectangleOverlapsEnemyCircles(player.Rectangle()) && player.InsideMinecart == null)
        {
            player.Hit((Vector2)this.transform.position);
            this.DestroyMinecartAndEjectPlayer();
        }
    }

    private void UpdateBackWheelsVisibility()
    {
        if(this.animated.currentAnimation == this.animationFlip || this.animated.currentAnimation == this.animationJump
            || this.animated.currentAnimation == this.animationEnemyJump)
        {
            if(this.backWheelsObject.activeInHierarchy == true)
                this.backWheelsObject.SetActive(false);
        }
        else
        {
            if(this.backWheelsObject.activeInHierarchy == false)
                this.backWheelsObject.SetActive(true);
        }
    }

    public Rect Rectangle()
    {
        return this.hitboxes[0].InWorldSpace();
    }

    public bool IsMinecartMovingOnRail()
    {
        return (this.currentSpeedOnRail != 0);
    }

    public bool IsMinecartDoingFlip()
    {
        return (this.animated.currentAnimation == this.animationFlip);
    }

    public bool IsMinecartPlayingJumpAnim()
    {
        return (this.animated.currentAnimation == this.animationJump || this.animated.currentAnimation == this.animationEnemyJump);
    }

    public NitromeEditor.Path GetPathMinecartIsAttachedTo()
    {
        return this.currentRail;
    }

    public void UpdateEnemySpriteWhileDoingFlip()
    {
        if(this.minecartWithEnemy == false)
            return;

        if (this.animated.currentAnimation == this.animationFlip)
        {
            int n = this.animated.frameNumber;
            bool enableEnemy = true;
            bool enableHelmet = false;
            bool enableHelmetUpsideDown = false;
            switch(n)
            {
                case 6:
                    enableEnemy = true;
                    enableHelmet = false;
                    break;

                case 4:
                case 5:
                    enableEnemy = false;
                    enableHelmet = false;
                    enableHelmetUpsideDown = true;
                    break;

                case 2:
                case 3:                                
                    enableEnemy = false;
                    enableHelmet = false;
                    break;

                case 0:
                case 1:                
                    enableEnemy = false;
                    enableHelmet = true;
                    break;

                default:
                    enableEnemy = true;
                    enableHelmet = false;
                    enableHelmetUpsideDown = false;
                    break;
            }
            this.enemyHelmetObject.SetActive(enableHelmet);
            this.enemyHelmetObjectUpsideDown.SetActive(enableHelmetUpsideDown);
            this.enemyLightObject.SetActive(enableEnemy);
            this.enemyObject.SetActive(false);
        }
    }

    public bool ShouldPlayerDisappearWhileMinecartFlip()
    {
        if (this.animated.currentAnimation == this.animationFlip)
        {
            if(this.animated.frameNumber <= 1 || this.animated.frameNumber >= 5)
                return false;

            return true;
        }
        return false;
    }

    private void GenerateSparks()
    {
        bool generateSparks = false;

        Chunk chunk = this.map.NearestChunkTo(this.transform.position);
        if (this.currentRail != null)
        {
            if(Mathf.Abs(this.currentSpeedOnRail) > 0.02f)
            {
                generateSparks = true;
                foreach (var spike in chunk.items.OfType<CrystalSpike>())
                {
                    if (spike.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
                    {
                        spike.Break(velocity);
                    }
                }
                foreach (var stopper in chunk.items.OfType<MinecartStopper>())
                {
                    if (stopper.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
                    {
                        DestroyMinecartAndEjectPlayer();
                        stopper.Bounce();
                    }
                }
            }
        }
        else
        { 
            if(Mathf.Abs(this.velocity.x) > 0.02f && this.freeFalling == false)
            {
                generateSparks = true;
                foreach (var spike in chunk.items.OfType<CrystalSpike>())
                {
                    if (spike.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
                    {
                        spike.Break(velocity);
                    }
                }
                foreach (var stopper in chunk.items.OfType<MinecartStopper>())
                {
                    if (stopper.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
                    {
                        DestroyMinecartAndEjectPlayer();
                        stopper.Bounce();
                    }
                }
            }
        }

        if(generateSparks == true)
        {
            sparksAnimated.PlayAndLoop(this.animationSparks);
        }
        else
        {
            sparksAnimated.PlayOnce(this.animationEmpty);
        }
    }
    
    private void UpdateDriveSfx()
    {
        if (this.isPlayerInsideCart == false) return;
        if(this.crashed == false || this.freeFalling == false || (velocity.y > -0.1f && velocity.y < 0.1))
        {
            if (this.currentRail != null)
            {
                if (Mathf.Abs(this.currentSpeedOnRail) > 0.02f)
                {
                    if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvMinecartDriveRail) == false)
                    {
                        Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartDriveRail, options: new Audio.Options(1, false, 0));
                    }
                    Audio.instance.ChangeSfxPosition(Assets.instance.sfxEnvMinecartDriveRail, updatedPosition: this.position);
                    return;
                }
            }
            else
            {
                if (Mathf.Abs(this.velocity.x) > 0.02f && this.freeFalling == false)
                {
                    if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvMinecartDriveTiles) == false)
                    {
                        Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartDriveTiles, options: new Audio.Options(1, false, 0), position: this.position);
                    }
                    Audio.instance.ChangeSfxPosition(Assets.instance.sfxEnvMinecartDriveTiles, updatedPosition: this.position);
                    return;
                }
            }
        }
        Audio.instance.StopSfx(Assets.instance.sfxEnvMinecartDriveRail);
        Audio.instance.StopSfx(Assets.instance.sfxEnvMinecartDriveTiles);
    }
    private void DestroyEnemyCart()
    {
        if(this.minecartWithEnemy == false)
            return;
    }

    private void GenerateMullDeathParticles()
    {
        foreach (var debris in this.deathAnimation.debris)
        {
            var p = Particle.CreateWithSprite(
                debris, 100, this.position, this.transform.parent
            );
            p.Throw(new Vector2(0, 0.6f), 0.3f, 0.4f, new Vector2(0, -0.02f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                p.spin *= -1;
        }
        Vector2 offsetDynamite = new Vector2(this.facingRight ? -0.3f: 0.3f, 1f );
        offsetDynamite = transform.rotation * offsetDynamite;
        Particle explosionParticle = Particle.CreateAndPlayOnce(
            animationExplosionParticle, this.position + offsetDynamite, this.transform.parent
        );

        CreateSpawnParticles(offsetDynamite);

        this.enemyHelmetObject.SetActive(false);
        this.enemyHelmetObjectUpsideDown.SetActive(false);
        this.enemyLightObject.SetActive(false);
        this.enemyObject.SetActive(false);
        this.crashed = true; 
        this.velocity.y += MinecartDestroyedVelocityY;
        this.animated.PlayAndLoop(this.animationTumble);
        // Debug.Break();
        Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartCrash, position: this.position);
        Audio.instance.PlaySfx(Assets.instance.sfxEnvMullInMinecartDie, position: this.position, options: new Audio.Options(1, false, 0));
    }


    private void GenerateSparkParticle()
    {
        var p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustLandParticle,
                    this.position +
                        new Vector2(Rnd.Range(-1.25f, 1.25f), -0.8f),
                    this.transform.parent
                );
    }

    private bool IsRectangleOverlappingCircle(Rect rect, Vector2 circleOrigin, float circleRadius)
    {
        Vector2 delta = new Vector2(
            circleOrigin.x - Mathf.Max(rect.x, Mathf.Min(circleOrigin.x, rect.xMax)),
            circleOrigin.y - Mathf.Max(rect.y, Mathf.Min(circleOrigin.y, rect.yMax))
        );

        return (delta.x * delta.x + delta.y * delta.y) < (circleRadius * circleRadius);
    }

    private bool IsCircleOverlappingCircle(Vector2 circleAPos, float circleARadius, Vector2 circleBPos, float circleBRadius)
    {
        return (circleBPos - circleAPos).sqrMagnitude <= (circleARadius + circleBRadius) * (circleARadius + circleBRadius);
    }

    private Vector2 GetBottomCirclePosition()
    {
        Vector2 v = new Vector2(this.facingRight? 0.2f:-0.2f, 0f);
        v = transform.rotation * v;
        return (Vector2)this.transform.position + v;
    }

    private Vector2 GetTopCirclePosition()
    {
        Vector2 v = new Vector2(this.facingRight? 0.6f:-0.6f, 1.5f);
        v = transform.rotation * v;
        return (Vector2)this.transform.position + v;
    }

    private bool IsCircleOverlappingEnemyHitboxCircles(Vector2 circlePos, float circleRadius)
    {
        return (IsCircleOverlappingCircle(circlePos, circleRadius, GetBottomCirclePosition(), 1) || 
               IsCircleOverlappingCircle(circlePos, circleRadius, GetTopCirclePosition(), 1));
    }
 
    private bool CheckIfRectangleOverlapsEnemyCircles(Rect rect)
    {
        bool isOverlapping = false;

        isOverlapping |= IsRectangleOverlappingCircle(rect, GetBottomCirclePosition(), 1f);
        // DrawCircle(GetBottomCirclePosition(), 1f, Color.white, 360, 0.05f);

        isOverlapping |= IsRectangleOverlappingCircle(rect, GetTopCirclePosition(), 1f);
        // DrawCircle(GetTopCirclePosition(), 1f, Color.white, 360, 0.05f);
        return isOverlapping;
    }

    public static void DrawCircle(Vector2 position, float radius, Color color, int howManyPoints = 16, float width = 0.01f)
    {
        float da = 360f / howManyPoints;
        for (int i = 0; i < howManyPoints; i++)
        {
            Vector2 v = new Vector2(radius, 0);
            v = Quaternion.Euler(0, 0, i * da) * v;
            DrawCross(position + v, color, width);
        }
    }

    public static void DrawCross(Vector2 position, Color c, float width = 0.01f)
    {
        Vector2 w1 = new Vector2(width / 2, width / 2);
        Vector2 w2 = new Vector2(-width / 2, width / 2);
        Debug.DrawLine(position - w1, position + w1, c);
        Debug.DrawLine(position + w2, position - w2, c);
    }

    public void PlayerDied() //NotifyPlayerDied
    {
        this.isPlayerInsideCart = false; 
        this.crashed = true;
        this.velocity.y = MinecartDestroyedVelocityY;
        this.animated.PlayAndLoop(this.animationTumble);
        this.map.player.ExitMinecart();        
    }

    public void DestroyMinecartAndEjectPlayer() //CrashAndEjectPlayer
    {
        currentRail = null;
        if(this.crashed == true)
            return;

        if(this.minecartWithEnemy == true)
        {
            GenerateMullDeathParticles();
        }
        else
        {
            EjectPlayer();
            CrashMinecart();
            Audio.instance.StopSfx(Assets.instance.sfxEnvMinecartDriveRail);
            Audio.instance.StopSfx(Assets.instance.sfxEnvMinecartDriveTiles);
        }
    }

    public void CrashMinecart()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartCrash, position: this.position);
        currentRail = null;        
        this.crashed = true; 
        this.velocity.y += MinecartDestroyedVelocityY;
        this.animated.PlayAndLoop(this.animationTumble);
    }

    public void EjectPlayer()   //NotifyPlayerExited
    {
        if(this.isPlayerInsideCart == true)
        {
            this.map.player.ExitMinecart();                
            this.map.player.velocity = new Vector2(this.velocity.x > 0 ? 0.2f : -0.2f, 0.3f);
            this.map.player.position = this.position + new Vector2(0, 0*1.5f);
            this.map.player.facingRight = (this.velocity.x > 0);
            this.isPlayerInsideCart = false;
            this.playerDoesntReenterForFrames = 10;
        }        
    }

    public void PlayerJumped() //NotifyPlayerJUmped
    {
        if(this.crashed)
        {
            return;
        }

        float currentRotation = Util.WrapAngle0To360(transform.rotation.eulerAngles.z);
        float dividingAngle = 60;
        float firstQuarterAngle = dividingAngle;
        float secondQuarterAngle = 180 - dividingAngle;
        float thirdQuarterAngle = 180 + dividingAngle;
        float fourthQuarterAngle = 360 - dividingAngle;        

        if (currentRail != null)
        {          
            CreateMinecartParticles();
            // check in which direction cart is looking at, quarters are CCW
            if(currentRotation > fourthQuarterAngle || currentRotation < firstQuarterAngle)
            {
                // Debug.Log("JUMP V UP");
                this.velocity = new Vector2(this.velocity.x, JumpStrength);
                this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationEnemyJump: this.animationJump);
            }
            else if(currentRotation >= firstQuarterAngle && currentRotation < secondQuarterAngle)
            {
                // Debug.Log("JUMP V LEFT");
                PlayFlipAnimation();
                this.velocity = new Vector2(-JumpStrength/2, this.velocity.y);
            }
            else if(currentRotation >= secondQuarterAngle && currentRotation < thirdQuarterAngle)
            {
                // Debug.Log("JUMP V DOWN");
                PlayFlipAnimation();
                this.velocity = new Vector2(this.velocity.x, -JumpStrength);
            }
            else if(currentRotation >= thirdQuarterAngle && currentRotation < fourthQuarterAngle)
            {
                // Debug.Log("JUMP V RIGHT");
                PlayFlipAnimation();
                this.velocity = new Vector2(JumpStrength/2, this.velocity.y);
            }

            this.currentRail = null;
            this.dontReattachToRailForFrames = 5;
        }
        else
        {
            if (this.freeFalling)
            {
                if (this.velocity.x == 0 || this.correctionFrames > 0)
                {
                    this.velocity = new Vector2(this.facingRight ? 0.2f : -0.2f, JumpStrength);
                }
                else
                {
                    this.map.player.ExitMinecart();
                    this.playerDoesntReenterForFrames = 10;
                    // this.map.player.velocity = new Vector2(this.velocity.x > 0 ? 0.15f : -0.15f, 0.55f);
                    this.map.player.velocity = new Vector2(this.velocity.x, 0.55f);
                    this.map.player.position = this.position + new Vector2(0, 0*1.6f);
                    this.map.player.facingRight = (this.velocity.x > 0);
                    this.isPlayerInsideCart = false;
                    this.tryCatchingPlayerForFrames = 120;
                    if(this.minecartWithEnemy == false)
                    {
                        this.animated.PlayAndLoop(this.animationIdle);
                    }
                    else
                    {
                        this.animated.PlayAndLoop(this.animationEnemyIdle);
                    }
                }
                return;
            }
            else
            {
                CreateMinecartParticles();
                this.velocity = new Vector2(this.velocity.x, JumpStrength);
            }
        }

        Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartJump, position: this.position);
    }

    private void DebugDrawRotatedHitBox(Color c)
    {
        Vector2 rearBottom = new Vector2(hitboxes[0].xMin, hitboxes[0].yMin);
        Vector2 rearTop = new Vector2(hitboxes[0].xMin, hitboxes[0].yMax);
        Vector2 frontBottom = new Vector2(hitboxes[0].xMax, hitboxes[0].yMin);
        Vector2 frontTop = new Vector2(hitboxes[0].xMax, hitboxes[0].yMax);

        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        rearBottom = Quaternion.Euler(0, 0, currentRotation) * rearBottom;
        rearTop = Quaternion.Euler(0, 0, currentRotation) * rearTop;
        frontBottom = Quaternion.Euler(0, 0, currentRotation) * frontBottom;
        frontTop = Quaternion.Euler(0, 0, currentRotation) * frontTop;

        Vector2 start = this.position;
        Debug.DrawLine(start + rearBottom, start + rearTop, c);
        Debug.DrawLine(start + rearTop, start + frontTop, c);
        Debug.DrawLine(start + frontTop, start + frontBottom, c);
        Debug.DrawLine(start + frontBottom, start + rearBottom, c);
    }

    private void UpdateAndSetRotation(float amount = 0.15f)
    {
        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        if(this.animated.currentAnimation == this.animationFlip)
        {
            return;
        }
        currentRotation = currentRotation + amount * (targetRotation - currentRotation);
        this.transform.rotation = Quaternion.Euler(0, 0, currentRotation);
    }

    public Vector2 GetRotatedHitBoxPositionAlongFrontEdge(float alongBottomToTop)
    {
        Vector2 frontBottom = new Vector2(hitboxes[0].xMax, hitboxes[0].yMin);
        Vector2 frontTop = new Vector2(hitboxes[0].xMax, hitboxes[0].yMax);

        Vector2 vectorAlong = frontBottom + alongBottomToTop * (frontTop - frontBottom);
        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        vectorAlong = Quaternion.Euler(0, 0, currentRotation) * vectorAlong;
        return vectorAlong;
    }

    public float GetBottomFrontPointConvertedToAlongPath()
    {
        if(this.currentRail == null)
            return 0;

        Vector2 frontBottomOffset =  new Vector2(this.currentSpeedOnRail >= 0 ? hitboxes[0].xMax : hitboxes[0].xMin, hitboxes[0].yMin);
        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        frontBottomOffset = Quaternion.Euler(0, 0, currentRotation) * frontBottomOffset;
        Vector2 frontBottomFinalPoint = this.position + frontBottomOffset;
        var closestPointOnPath = this.currentRail.NearestPointTo(frontBottomFinalPoint);
        return closestPointOnPath.Value.distanceAlongPath;
    }

    public bool IsMinecartOnRail()
    {
        return (this.currentRail != null);
    }

    private void CreateMinecartParticles()
    {
        Vector2 offsetBottomBack = new Vector2(this.facingRight ? -0.9f: 0.9f, -0.85f );
        offsetBottomBack = transform.rotation * offsetBottomBack; 
        Vector2 offsetBottom = new Vector2(0, -1f );
        offsetBottom = transform.rotation * offsetBottom;

        for (var n = 0; n < 4; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.animationRailLandingParticle,
                this.position + offsetBottomBack,
                this.transform
            );
            float speed = Rnd.Range(0.7f, 1.4f) / 16f;
            // p.velocity = speed * (offsetBottom - offsetBottomBack).normalized;
            p.velocity = speed * new Vector2(Random.Range(-0.1f,0.0f), Random.Range(0.05f,0.1f)).normalized;
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }

        var b = Particle.CreateAndPlayOnce(
                this.animationRailLandingBigParticle,
                this.position + offsetBottomBack,
                this.transform
            );
        b.spriteRenderer.sortingLayerName = "Player Dust (AL)";
    }

    //////////////////////////
    // TILE-BASED PART
    //////////////////////////

    private void AdvanceOnTiles()
    {
        UpdateEnemySpriteWhileDoingFlip();

        if(this.insideRailDoor != null && this.minecartExitedDoor == false)
        {
            AdvanceApproachingMineDoor();
            this.position += this.velocity;
        }
        else
        {
            var raycast = new Raycast(this.map, this.position, ignoreItem1: this, ignoreItemTypes: new System.Type[] {typeof(Peg), typeof(CrystalBarrierBlock),typeof(Chest)});
            MoveHorizontallyOnTiles(raycast);
            raycast = new Raycast(this.map, this.position, ignoreItem1: this, ignoreItemTypes: new System.Type[] {typeof(Peg), typeof(CrystalBarrierBlock), typeof(Chest)});
            DebugDrawRotatedHitBox(Color.cyan);
            MoveVertically2RaycastsRotateOrigins_2(raycast);
            AdjustVelocityOnTiles();
        }        
        
        UpdatePlayerPosition();
        CheckForCollisionsWithItems();
        this.transform.position = this.position;        
    }

    private void AdjustVelocityOnTiles()
    {
        if (this.movementType == "when_player_in" && this.isTrigger == true && this.map.player.InsideMinecart != this)
        {
            this.velocity.x = 0f;
            return;
        }

        if (this.isPlayerInsideCart == false && !this.freeFalling)
        {
            var horizontalDistanceFromPlayer = Mathf.Abs(this.map.player.position.x - this.position.x);

            if(horizontalDistanceFromPlayer > 0.3f && (this.velocity.x > 0 && this.map.player.position.x < this.position.x
                || this.velocity.x < 0 && this.map.player.position.x > this.position.x)
            )
            {
                if(this.velocity.x > 0)
                {
                    this.velocity.x = 0.1f;
                }
                else if(this.velocity.x < 0)
                {
                    this.velocity.x = -0.1f;
                }
                return;
            }
        }

        if(this.freeFalling == false)
        {
            if (this.velocity.x > 0)
            {
                this.velocity.x = 0.2f;
            }
            else if (this.velocity.x < 0)
            {
                this.velocity.x = -0.2f;
            }
            else if(this.isPlayerInsideCart == true || this.minecartWithEnemy == true)
            {
                this.velocity.x = (this.facingRight ? 0.2f: -0.2f);
            }
        }
    }

    private bool IsCartUpsideDown()
    {
        float currentRotation = Util.WrapAngle0To360(transform.rotation.eulerAngles.z);

        if (currentRotation >= 180-60 && currentRotation <= 180 + 60)
        {
            return true;
        }   
        return false;
    }

    private bool ShouldBeReattachedToRail()
    {
        if(this.crashed)
        {
            return false;
        }

        if (this.dontReattachToRailForFrames > 0)
        {
            this.dontReattachToRailForFrames--;
            return false;
        }

        Chunk currentChunk = this.map.NearestChunkTo(this.transform.position);

        if (this.currentRail != null || currentChunk.subsetOfItemsOfTypeRail.Count == 0)
        {
            return false;
        }

        Vector2 wheelsOffset = new Vector2(0, -1);
        wheelsOffset = transform.rotation * wheelsOffset;

        Vector2 initialWheelsPosition = this.position + wheelsOffset;

        Color[] cols = { Color.green, Color.white };
        foreach (Rail rail in currentChunk.subsetOfItemsOfTypeRail)
        {
            var tempRail = rail.path;
            float stepLen = 0.1f;
            // Vector2 last = this.position + wheelsOffset;
            for (int k = 0; k <= Mathf.Floor(this.velocity.magnitude / stepLen) + 1; k += 1)
            {
                var wheelsPosition = this.position + wheelsOffset + (k * stepLen > this.velocity.magnitude ? this.velocity.magnitude : k * stepLen) * this.velocity.normalized;
                var closestPoint = tempRail.NearestPointTo(wheelsPosition);

                // DrawCross(wheelsPosition, cols[k%2]);

                // last = wheelsPosition;
                if (closestPoint.HasValue && (closestPoint.Value.position - wheelsPosition).magnitude <= 0.1f && this.velocity.y < 0)
                {
                    //we hit the rail and we are going down
                    
                    var closestPointRecalculated = tempRail.NearestPointTo(this.position);
                    this.currentDistanceOnRail = this.lastDistanceOnRail = closestPointRecalculated.Value.distanceAlongPath;
                    var edge = closestPointRecalculated.Value.edge;

                    var vertical = Mathf.Abs(Vector2.Dot(edge.direction.normalized, Vector2.up)) > 0.9f;
                    if(vertical)
                    {
                        return false;
                    }
                    currentRail = tempRail;
                    var perpendicular = Vector2.Perpendicular(edge.direction.normalized);
                    this.reversedPath = Vector2.Dot(perpendicular.normalized, this.velocity.normalized) > 0;

                    var tangent = currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail).tangent;

                    // var s = Vector2.Dot(this.velocity.normalized, tangent);
                    var s = Vector2.Dot(new Vector2(this.velocity.x, 0), new Vector2(tangent.x, 0));
                    if(s > 0)
                    {
                        //same direction
                        this.currentSpeedOnRail = this.targetSpeedOnRail = RegularSpeedOnRail;
                    }
                    else if(s < 0)
                    {
                        // different direction
                        this.currentSpeedOnRail = this.targetSpeedOnRail = -RegularSpeedOnRail;
                    }

                    var cartContactData = GetAngleAndRotatedPositionForOffset(new Vector2(0, 1), this.currentDistanceOnRail, tempRail); 
                    this.transform.rotation = Quaternion.Euler(0, 0, cartContactData.angle);

                    // Debug.Log("===== assign to rail!");
                    // Debug.Log("v:" + this.velocity.normalized+ ",tangent:" + tangent);
                    // Debug.Log("angle:" + cartContactData.angle + ",reversed:" + this.reversedPath);
                    // Debug.Log("s:" + s + "/currentSpeed:" +this.currentSpeedOnRail);
                    // Debug.Break();
                    if (this.movementType == "always")
                    {
                        StartMinecartMovementWhenPlayerGetsInsideIt();
                    }
                    if (this.currentSpeedOnRail != 0)
                    {
                        PlayMinecartLandedAnimation();
                    }
                    return true;
                }
                else
                {
                    float magnitude = 0;

                    if (closestPoint.HasValue)
                    {
                        magnitude = (closestPoint.Value.position - wheelsPosition).magnitude;
                        // DrawCross(closestPoint.Value.position, Color.red);
                        // Debug.DrawLine(wheelsPosition, closestPoint.Value.position, Color.yellow);
                    }
                }
            }
        }
        return false;
    }

    private void CheckIfPlayerGetsInsideCart()
    {
        if(this.playerDoesntReenterForFrames>0)
        {
            this.playerDoesntReenterForFrames--;
            return;
        }

        if(this.isPlayerInsideCart || this.minecartWithEnemy)
        {
            return;
        }

        var player = this.map.player;
        bool isOverlapping = (player.position - this.position).magnitude < 1;

        if ((player.state == Player.State.Normal ||
            player.state == Player.State.Jump ||
            player.state == Player.State.FireFromMineCannon ||
            player.state == Player.State.DropVertically ||
            player.state == Player.State.Spring ||
            player.state == Player.State.SpringFromPushPlatform)
            && isOverlapping)
        {
            player.EnterMinecart(this);
            this.isPlayerInsideCart = true;
            this.enterMinecartWobbleCnt = 1;
            
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvBoxEnter,
                position: this.position
            );
            StartMinecartMovementWhenPlayerGetsInsideIt();
        }
    }

    private void PlayMinecartLandedAnimation()
    {
        var animToPlay = this.minecartWithEnemy ? this.animationEnemyLand : this.animationLand;
        if (this.velocity.x == 0)
        {
            if (this.minecartWithEnemy || this.movementType == "always")
            {
                this.velocity.x = (this.facingRight ? 0.001f : -0.001f);
                if (this.isTrigger == true && this.movementType == "when_player_in" && this.map.player.InsideMinecart != this)
                {
                    this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationIdle : this.animationIdle);
                }
                else
                {
                    this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationEnemyWobble : this.animationWobble);
                }
            }
            return;
        }

        if(this.velocity.x == 0)
            return;        

        this.animated.PlayOnce(animToPlay, delegate
        {
            if(this.isTrigger == true && this.movementType == "when_player_in" && this.map.player.InsideMinecart != this)
            {
                this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationIdle : this.animationIdle);
            }
            else
            {
                this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationEnemyWobble : this.animationWobble);
            }
        });
        CreateMinecartParticles();
        if(currentRail == null)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartLandTiles, position: this.position);
        }
        else
        {
            Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartLandRail, position: this.position);
        }
    }


    private void StartMinecartMovementWhenPlayerGetsInsideIt()
    {
        var animToPlay = this.minecartWithEnemy ? this.animationEnemyLand : this.animationLand;
        if(this.currentRail == null)
        {
            this.velocity = new Vector2(this.facingRight ? 0.2f : -0.2f, 0);
            this.animated.PlayOnce(animToPlay, delegate
            {
                this.animated.PlayAndLoop(this.minecartWithEnemy? this.animationEnemyWobble : this.animationWobble);
            });
            return;
        }

        var tangent = currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail).tangent;
        
        var s = Vector2.Dot(new Vector2(this.facingRight ? 1 : -1, 0).normalized, tangent);
        
        if(s > 0)
        {
            this.currentSpeedOnRail = this.targetSpeedOnRail = RegularSpeedOnRail;
            this.animated.PlayOnce(animToPlay, delegate
            {
                this.animated.PlayAndLoop(this.minecartWithEnemy? this.animationEnemyWobble : this.animationWobble);
            });
        }
        else if(s < 0)
        {
            this.currentSpeedOnRail = this.targetSpeedOnRail = -RegularSpeedOnRail;
            this.animated.PlayOnce(animToPlay, delegate
                {
                    this.animated.PlayAndLoop(this.minecartWithEnemy? this.animationEnemyWobble : this.animationWobble);
                });
        }
        else
        {
            this.currentSpeedOnRail = this.targetSpeedOnRail = 0;
        }
    }

    protected void MoveHorizontallyOnTiles(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        var high = new Vector2(0, rect.yMax - 0.2f);
        var low = new Vector2(0, rect.yMin + 0.25f);
        high = this.transform.rotation * high;
        low = this.transform.rotation * low;
        var ajustedVelX = this.velocity.x;
        if (ajustedVelX > 0)
        {
            var maxDistance = ajustedVelX + rect.xMax;
            var highEnd = new Vector2(0, rect.yMax - 0.2f) + new Vector2(maxDistance, 0);
            var lowEnd = new Vector2(0, rect.yMin + 0.25f) + new Vector2(maxDistance, 0);

            highEnd = this.transform.rotation * highEnd;
            lowEnd = this.transform.rotation * lowEnd;

            Debug.DrawLine(this.position + high, this.position + highEnd, Color.green);
            Debug.DrawLine(this.position + low, this.position + lowEnd, Color.green);

            var result = Raycast.CombineClosest(
                raycast.Arbitrary(this.position + high, (highEnd - high).normalized, maxDistance),
                raycast.Arbitrary(this.position + low, (lowEnd - low).normalized, maxDistance)
            );

            if (result.anything == true &&
                result.distance < ajustedVelX + rect.xMax && ignoreNFrames == 0)
            {
                this.position.x += result.distance - rect.xMax;
                bool sameMinecart = this.map.player.InsideMinecart == this;
                DestroyMinecartAndEjectPlayer();
                Audio.instance.PlaySfx(Assets.instance.sfxEnvBarrierMinecartHit, position: this.position, options: new Audio.Options(1, false, 0));
                if (result.items.Length > 0)
                {
                    RailBarrier railBarrier = result.items[0] as RailBarrier;
                    if (railBarrier != null && railBarrier.deadlyRailBarrier == true)
                    {
                        if (sameMinecart)
                        {
                            this.map.player.Hit(this.position);
                        }
                        railBarrier.DestroyBarrier();
                    }
                }
                this.lastTouchedTiles.AddRange(result.tiles.ToArray());
            }
            else
            {
                this.position.x += ajustedVelX;
            }
            this.facingRight = true;
        }
        else if (ajustedVelX < 0)
        {
            var maxDistance = -ajustedVelX - rect.xMin;
            var highEnd = new Vector2(0, rect.yMax - 0.2f) + new Vector2(-maxDistance, 0);
            var lowEnd = new Vector2(0, rect.yMin + 0.25f) + new Vector2(-maxDistance, 0);
            highEnd = this.transform.rotation * highEnd;
            lowEnd = this.transform.rotation * lowEnd;

            Debug.DrawLine(this.position + high, this.position + highEnd, Color.yellow);
            Debug.DrawLine(this.position + low, this.position + lowEnd, Color.yellow);

            var result = Raycast.CombineClosest(
                raycast.Arbitrary(this.position + high, (highEnd - high).normalized, maxDistance),
                raycast.Arbitrary(this.position + low, (lowEnd - low).normalized, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -ajustedVelX - rect.xMin && ignoreNFrames == 0)
            {
                this.position.x += -result.distance - rect.xMin;                

                bool sameMinecart = this.map.player.InsideMinecart == this;
                DestroyMinecartAndEjectPlayer();
                Audio.instance.PlaySfx(Assets.instance.sfxEnvBarrierMinecartHit, position: this.position, options: new Audio.Options(1, false, 0));
                if (result.items.Length > 0)
                {
                    RailBarrier railBarrier = result.items[0] as RailBarrier;
                    if (railBarrier != null && railBarrier.deadlyRailBarrier == true)
                    {
                        if (sameMinecart)
                        {
                            this.map.player.Hit(this.position);
                        }
                        railBarrier.DestroyBarrier();
                    }
                }
                this.lastTouchedTiles.AddRange(result.tiles.ToArray());
            }
            else
            {
                this.position.x += ajustedVelX;
            }             
            this.facingRight = false;
        }

        if (this.animated.currentAnimation != this.animationFlip)
        {
            if (this.facingRight)
            {
                this.transform.localScale = new Vector3(IsCartUpsideDown() ? -1 : 1, 1, 1);
                if (this.isPlayerInsideCart)
                {
                    this.map.player.facingRight = IsCartUpsideDown() ? false : true;
                }
            }
            else
            {
                this.transform.localScale = new Vector3(IsCartUpsideDown() ? 1 : -1, 1, 1);
                if (this.isPlayerInsideCart)
                {
                    this.map.player.facingRight = IsCartUpsideDown() ? true : false;
                }
            }
        }
    }

    protected void MoveVertically2RaycastsRotateOrigins_2(Raycast raycast)
    {
        freeFalling = true;

        //2 raycasts, rays' origins rotating with currentRotation, also trying to affect rotation on hit
        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                bool ignoreCollision = false;

                var c = new Item.Collision { otherPlayer = null };
                if(result.item != null)
                {
                    if(result.item is BreakableBlock)
                    {
                        result.item?.onCollisionWithPlayersHead();
                    }
                    else if(result.item is CrystalBarrierBlock)
                    {
                        ignoreCollision = true;
                    }
                    else
                    {
                        result.item?.onCollisionFromBelow?.Invoke(c);
                    }                    
                }

                if (ignoreCollision == false)
                {
                    this.position.y += result.distance - rect.yMax;
                    this.velocity.y = 0;
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            int maxIterations = 1;
            
            if(IsMinecartDoingFlip())
            {
                this.position.y += this.velocity.y;
                UpdateAndSetRotation(0.1f);
                return;
            }

            for (int i = 0; i < maxIterations; i++)
            {
                float rotAmount = 5f / (i + 1);
                float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
                float depth = 1.0f;

                var start = this.position;

                // get bottom corners and rotate them to correct angle
                Vector2 frontBottomCorner = new Vector2(0.55f, -1);
                Vector2 rearBottomCorner = new Vector2(-0.55f, -1);
                frontBottomCorner = Quaternion.Euler(0, 0, currentRotation) * frontBottomCorner;
                rearBottomCorner = Quaternion.Euler(0, 0, currentRotation) * rearBottomCorner;

                //raycast from points position offsetted by (0,1)
                float raycastLength = -this.velocity.y / maxIterations + depth;
                var frontResult = raycast.Vertical(start + frontBottomCorner + new Vector2(0, 1f), false, raycastLength);
                var rearResult = raycast.Vertical(start + rearBottomCorner + new Vector2(0, 1f), false, raycastLength);

                //draw debug raycast lines
                Debug.DrawLine(start + frontBottomCorner + new Vector2(0, 1f), start + frontBottomCorner + new Vector2(0, 1f) - new Vector2(0, raycastLength), Color.yellow);
                Debug.DrawLine(start + rearBottomCorner + new Vector2(0, 1f), start + rearBottomCorner + new Vector2(0, 1f) - new Vector2(0, raycastLength), Color.grey);

                //default angles
                float frontAngle = 0;
                float rearAngle = 0;

                //distances to raycast hits
                float frontDistance = float.MaxValue;
                float rearDistance = float.MaxValue;

                float rearCorrection = 0;
                float frontCorrection = 0;

                if (frontResult.anything && rearResult.anything)
                {
                    frontDistance = frontResult.distance;
                    frontAngle = Util.WrapAngleMinus180To180(frontResult.surfaceAngleDegrees);

                    rearDistance = rearResult.distance;
                    rearAngle = Util.WrapAngleMinus180To180(rearResult.surfaceAngleDegrees);

                    if (frontDistance <= raycastLength)
                    {
                        frontCorrection = raycastLength - frontDistance; ;
                        AdjustTargetRotation(rotAmount);
                    }

                    if (rearDistance <= raycastLength)
                    {
                        rearCorrection = raycastLength - rearDistance;
                        AdjustTargetRotation(-rotAmount);
                    }

                    if(frontCorrection == raycastLength)
                    {
                        frontCorrection = 0.0f;
                        AdjustTargetRotation(rotAmount);
                    }

                    if(rearCorrection == raycastLength)
                    {
                        rearCorrection = 0.0f;
                        AdjustTargetRotation(-rotAmount);
                    }

                    if(frontResult.anything && frontResult.tile != null)
                        this.lastTouchedTiles.Add(frontResult.tile);

                    if(rearResult.anything && rearResult.tile != null)
                        this.lastTouchedTiles.Add(rearResult.tile);

                    this.position.y += this.velocity.y/maxIterations;
                    this.position.y += Mathf.Max(frontCorrection, rearCorrection);
                    this.velocity.y = 0;
                    freeFalling = false;
                }
                else if (frontResult.anything && !rearResult.anything)
                {
                    frontDistance = frontResult.distance;
                    frontAngle = Util.WrapAngleMinus180To180(frontResult.surfaceAngleDegrees);

                    if(frontResult.anything && frontResult.tile != null)
                        this.lastTouchedTiles.Add(frontResult.tile);

                    if (frontDistance <= raycastLength)
                    {

                        frontCorrection = raycastLength - frontDistance; ;
                        AdjustTargetRotation(rotAmount);
                    }

                    if(frontCorrection == raycastLength)
                    {
                        frontCorrection = 0.0f;
                        AdjustTargetRotation(rotAmount);
                    }

                    this.position.y += this.velocity.y/maxIterations;
                    this.position.y += frontCorrection;
                    this.velocity.y = 0;
                    freeFalling = false;
                }
                else if (!frontResult.anything && rearResult.anything)
                {
                    rearDistance = rearResult.distance;
                    rearAngle = Util.WrapAngleMinus180To180(rearResult.surfaceAngleDegrees);

                    if(rearResult.anything && rearResult.tile != null)
                        this.lastTouchedTiles.Add(rearResult.tile);

                    if (rearDistance <= raycastLength)
                    {
                        this.velocity.y = 0;
                        rearCorrection = raycastLength - rearDistance;
                        AdjustTargetRotation(-rotAmount);
                    }

                    if(rearCorrection == raycastLength)
                    {
                        rearCorrection = 0.0f;
                        AdjustTargetRotation(-rotAmount);
                    }

                    this.position.y += this.velocity.y/maxIterations;
                    this.position.y += rearCorrection;
                    this.velocity.y = 0;
                    freeFalling = false;
                }
                else
                {
                    this.position.y += this.velocity.y/maxIterations;
                    if(i == 0)
                        this.targetRotation = 0;
                }
                UpdateAndSetRotation(0.1f/maxIterations);
            }
        }
        else
        {
            freeFalling = false;
        }

        if (freeFalling == false)
        {
            if (this.framesInAir > 3)
            {
                PlayMinecartLandedAnimation();
            }
            this.framesInAir = 0;
        }
        else
        {
            this.framesInAir++;
        }
    }

    void AdjustTargetRotation(float amount)
    {
        this.targetRotation = this.targetRotation + amount;
    }

    //////////////////////////
    // RAIL-BASED PART
    //////////////////////////

    public void AdvanceOnRail(Raycast raycast)
    {
        this.lastDistanceOnRail = GetBottomFrontPointConvertedToAlongPath();

        if ((this.reversedPath ? -1f : 1f) * this.currentSpeedOnRail > 0)
        {
            this.transform.localScale = new Vector3(1, 1, 1);
            this.facingRight = true;
            if (isPlayerInsideCart)
                this.map.player.facingRight = true;

        }
        else if ((this.reversedPath ? -1f : 1f) * this.currentSpeedOnRail < 0)
        {
            this.facingRight = false;
            this.transform.localScale = new Vector3(-1, 1, 1);
            if (isPlayerInsideCart)
                this.map.player.facingRight = false;
        }
        
        if(this.insideDoorDelayTime > 0)
        {
            this.insideDoorDelayTime--;
            return;
        }
        AdvanceApproachingMineDoor();
        CheckIfMinecartShouldFellOffRail();
        CheckSensorsWhileOnRail(raycast);

        if (this.currentRail == null)
        {
            return;
        }

        var pointOnPathInfo = this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail);
        var dir = (this.reversedPath ? -1f : 1f) * (this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail + 0.01f).position - this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail - 0.01f).position);
        float angle = Vector2.SignedAngle(Vector2.right, dir);

        AdjustSpeedOnRail();

        this.currentSpeedOnRail = Mathf.Clamp(this.currentSpeedOnRail, -5 * MaximumSpeedOnRail, 5 * MaximumSpeedOnRail);
        this.transform.rotation = Quaternion.Euler(0, 0, angle);
        Vector2 positionOffset = new Vector2(0, 1);
        this.targetRotation = angle;
        positionOffset = Quaternion.Euler(0, 0, angle) * positionOffset;
        this.position = pointOnPathInfo.position + positionOffset;
        this.transform.position = this.position;
        CheckForCollisionsWithItems();
    }

    private void AdjustSpeedOnRail()
    {
        float targetSpeed;
        float acceleration = Acceleration;

        var pt = this.currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail);

        if (this.isPlayerInsideCart == false &&
            Mathf.Abs(this.currentSpeedOnRail) < 0.001f &&
            this.movementType != "always")
        {
            if (pt.tangent.y > 0.01f)
                targetSpeed = -DownwardSpeedOnRail;
            else if (pt.tangent.y < -0.01f)
                targetSpeed = DownwardSpeedOnRail;
            else
                targetSpeed = 0;
        }
        else
        {
            if ((this.currentSpeedOnRail > 0 && pt.tangent.y > 0) ||
                (this.currentSpeedOnRail < 0 && pt.tangent.y < 0))
            {
                var upness = Mathf.Abs(pt.tangent.y);
                targetSpeed = Mathf.Lerp(RegularSpeedOnRail, UpwardSpeedOnRail, upness);
                acceleration = Mathf.Lerp(Acceleration, 0.005f, upness);
            }
            else
            {
                var downness = Mathf.Abs(pt.tangent.y);
                targetSpeed = Mathf.Lerp(
                    RegularSpeedOnRail, DownwardSpeedOnRail, downness
                );
            }

            if (pt.tangent.x * (this.reversedPath ? -1 : 1) < -0.9f)
                targetSpeed = 0;

            if (this.currentSpeedOnRail < 0 || 
                ((initialFacingRight == false && !chunk.isFlippedHorizontally) || (initialFacingRight == true && chunk.isFlippedHorizontally)) && 
                this.isTrigger == true)
            {
                targetSpeed *= -1;
            }
        }

        if (this.boostTime > 0 && this.currentSpeedOnRail >= 0)
        {
            if (targetSpeed < BoostSpeedOnRail)
                targetSpeed = BoostSpeedOnRail;
            if (acceleration < 0.02f)
                acceleration = 0.02f;
        }
        else if (this.boostTime > 0 && this.currentSpeedOnRail < 0)
        {
            if (targetSpeed > -BoostSpeedOnRail)
                targetSpeed = -BoostSpeedOnRail;
            if (acceleration < 0.02f)
                acceleration = 0.02f;
        }

        if (this.isPlayerInsideCart == false &&
            this.tryCatchingPlayerForFrames > 0 &&
            this.map.player.position.y > this.position.y &&
            Mathf.Abs(pt.tangent.x) > 0.2f)
        {
            var horizontalDistanceFromPlayer = Mathf.Abs(
                this.map.player.position.x - this.position.x
            );
            if (
                horizontalDistanceFromPlayer > 3 &&
                (
                    this.velocity.x > 0 && this.map.player.position.x < this.position.x ||
                    this.velocity.x < 0 && this.map.player.position.x > this.position.x
                )
            )
            {
                if (this.currentSpeedOnRail > 0)
                    targetSpeed = 0.01f;
                else if (this.currentSpeedOnRail < 0)
                    targetSpeed = -0.01f;
            }
        }

        this.currentSpeedOnRail = Util.Slide(
            this.currentSpeedOnRail, targetSpeed, acceleration
        );
        
        // Debug.Log(
        //     $"current speed = {this.currentSpeedOnRail}, " +
        //     $"target speed = {targetSpeed}, " +
        //     $"distance = {this.currentDistanceOnRail} / {this.currentRail.length}, " +
        //     $"boost time = {this.boostTime}"
        // );
    }

    private void UpdatePlayerPosition()
    {
        float currentAngle = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        float wobbleYOffset = -0.3f * Mathf.Sin(this.enterMinecartWobbleCnt * Mathf.PI);
        Vector2 offset = new Vector2(this.facingRight? -0.1f: 0.1f, 1.3f + wobbleYOffset);
        offset = transform.rotation * offset;
        this.enterMinecartWobbleCnt = Util.Slide(this.enterMinecartWobbleCnt, 0, 0.05f);

        if (this.map.player.GetMinecart() == this && this.isPlayerInsideCart)
        {            
            this.map.player.velocity = this.velocity;
            this.map.player.position = this.position + offset;
            this.map.player.groundState.angleDegrees = currentAngle;
            if(this.animated.currentAnimation == this.animationFlip)
            {
                int currentFrame = this.animated.frameNumber;
                var flippedPosition = this.position - offset;

                float progress = (float) currentFrame / 6f;
                var delta = -2 * offset;

                if(currentFrame >= 3)
                {
                    this.map.player.facingRight = this.facingRight;
                    this.map.player.groundState.angleDegrees = currentAngle - 180;
                }
                this.map.player.position = this.position + offset + progress * delta;
            }
            else if(this.animated.currentAnimation == this.animationJump)
            {
                this.map.player.groundState.angleDegrees = currentAngle + (this.facingRight ? 10 : -10);
            }
        }
        
        //light
        if(this.animated.currentAnimation == this.animationFlip)
        {
            int currentFrame = this.animated.frameNumber;
            var flippedPosition = this.position - offset;
            this.lightObject.transform.localPosition = this.lightOffsetWhenDoingFlip[currentFrame];
        }
        else if(this.animated.currentAnimation == this.animationJump || this.animated.currentAnimation  == this.animationEnemyJump)
        {
            this.lightObject.transform.localPosition = new Vector2(1.2f, 0.8f);                
        }
        else
        {
            this.lightObject.transform.localPosition = new Vector2(1.03f, 0.52f);
        }

        //enemy head light
        if (this.minecartWithEnemy)
        {
            if (this.animated.currentAnimation == this.animationEnemyJump)
            {
                this.enemyLightObject.transform.localPosition = new Vector2(0.71f, 2.74f);
            }
            else if (this.animated.currentAnimation == this.animationEnemyRideHidden)
            {
                this.enemyLightObject.transform.localPosition = new Vector2(0.95f, 1.65f);
            }
            else if (this.animated.currentAnimation == this.animationEnemyHide)
            {
                if (this.animated.frameNumber <= 2)
                {
                    this.enemyLightObject.transform.localPosition = new Vector2(1.22f, 2.37f);
                }
                else if (this.animated.frameNumber <= 3)
                {
                    this.enemyLightObject.transform.localPosition = new Vector2(1.22f, 2.1f);
                }
                else if (this.animated.frameNumber <= 4)
                {
                    this.enemyLightObject.transform.localPosition = new Vector2(1.22f, 1.75f);
                }
            }
            else
            {
                this.enemyLightObject.transform.localPosition = new Vector2(1.41f, 2.24f);
            }
            DebugAnimations();
        }
    }    

    public (float angle, Vector2 offset) GetAngleAndRotatedPositionForOffset(Vector2 offset, float distanceAlongPath, NitromeEditor.Path path)
    {
        float currentAngle = Vector2.SignedAngle(Vector2.right,
            path.PointAtDistanceAlongPath(distanceAlongPath).tangent);

        if(this.reversedPath)
        {
            currentAngle += 180;
        }

        offset = Quaternion.Euler(0, 0, currentAngle) * offset;
        return (currentAngle, offset);
    }

    public void CheckIfMinecartShouldFellOffRail()
    {
        if (this.currentRail == null)
        {
            return;
        }

        if(this.minecartWithEnemy && IsCartUpsideDown() && this.animated.currentAnimation != this.animationEnemyRideHidden && this.animated.currentAnimation != this.animationEnemyHide)
        {
            this.animated.PlayOnce(this.animationEnemyHide, delegate {
                this.animated.PlayAndLoop(this.animationEnemyRideHidden);
            });
        }

        //check if player should fall off the rail (upside down only)
        float nextDistanceOnRail = currentDistanceOnRail + currentSpeedOnRail;
        var currentPointOnRail = this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail);
        var nextPointOnRail = this.currentRail.PointAtDistanceAlongPath(nextDistanceOnRail);

        bool cartGoingTooSlow = Mathf.Abs(this.currentSpeedOnRail) < 0.075f;
        bool endOfTheRail = (nextDistanceOnRail > currentRail.length || nextDistanceOnRail < 0);

        if(endOfTheRail && currentRail.closed)
        {
            if(nextDistanceOnRail < 0)
            {
                nextDistanceOnRail = currentRail.length - nextDistanceOnRail;
                endOfTheRail = false;
            }
            else if(nextDistanceOnRail > currentRail.length)
            {
                nextDistanceOnRail = nextDistanceOnRail - currentRail.length;
                endOfTheRail = false;
            }
        }

        if ((IsCartUpsideDown() && cartGoingTooSlow)
            || endOfTheRail)
        {
            if(this.insideRailDoor != null)
            {
                this.currentDistanceOnRail = Mathf.Clamp(this.currentDistanceOnRail, 0 , currentRail.length);
                return;
            }

            var perpendicular = Vector2.Perpendicular(currentPointOnRail.tangent.normalized);
            var upsideDown = Vector2.Dot(this.reversedPath? -perpendicular.normalized : perpendicular.normalized, Vector2.down) > 0;

            if(upsideDown)
            {
                PlayFlipAnimation();
            }
            
            this.velocity = currentSpeedOnRail * currentPointOnRail.tangent.normalized;
            this.currentRail = null;
            this.dontReattachToRailForFrames = (this.targetSpeedOnRail == 0)? 10: 5;
            
            return;
        }

        // check if not going on the outside
        var perpendicularB = (this.reversedPath? -1f : 1f) * Vector2.Perpendicular(currentPointOnRail.tangent.normalized);
        var tangentInDirection = (this.reversedPath? -1f : 1f) * (this.facingRight? 1: -1) * currentPointOnRail.tangent.normalized;
        var tangentInDirectionNextPoint = (this.reversedPath? -1f : 1f) * (this.facingRight? 1: -1) * nextPointOnRail.tangent.normalized;
        Debug.DrawLine(currentPointOnRail.position, currentPointOnRail.position + perpendicularB, Color.magenta);
        Debug.DrawLine(currentPointOnRail.position, currentPointOnRail.position + tangentInDirection, Color.blue);

        // Debug.Log("reversed path:" + this.reversedPath + ", facingRight:" + this.facingRight + ", sp:" + this.currentSpeedOnRail + ",tan:" + currentPointOnRail.tangent.normalized + ", tanInDir:" + tangentInDirection);

        float angleAtCurrentPoint = Vector2.SignedAngle(Vector2.right, tangentInDirection);
        float angleAtNextPoint = Vector2.SignedAngle(Vector2.right, tangentInDirectionNextPoint);

        if(Vector2.Dot(tangentInDirection, Vector2.down) == 1f)
        {
            if(angleAtNextPoint < angleAtCurrentPoint && Vector2.Dot(perpendicularB, Vector2.right) > 0.99f)
            {
                this.velocity = currentSpeedOnRail * currentPointOnRail.tangent.normalized;
                this.velocity = new Vector2(this.velocity.x > 0 ? -0.01f : 0.01f, -0.5f);
                this.currentRail = null;
                this.dontReattachToRailForFrames = 10;
            }

            if(angleAtNextPoint > angleAtCurrentPoint && Vector2.Dot(perpendicularB, Vector2.left) > 0.99f)
            {
                // PlayFlipAnimation();
                this.velocity = currentSpeedOnRail * currentPointOnRail.tangent.normalized;
                this.velocity = new Vector2(this.velocity.x > 0 ? -0.01f : 0.01f, -0.5f);
                this.currentRail = null;
                this.dontReattachToRailForFrames = 10;
            }
        }
    }

    private void PlayFlipAnimation()
    {
        if (this.animated.currentAnimation != this.animationFlip)
        {
            this.animated.PlayOnce(this.animationFlip, delegate
            {
                this.animated.PlayAndLoop(this.minecartWithEnemy ? this.animationEnemyJump : this.animationJump);
                this.ignoreNFrames = 5;
                float newAngle = Util.WrapAngle0To360(transform.rotation.eulerAngles.z + 180);
                this.transform.rotation = Quaternion.Euler(0, 0, newAngle);
                this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

                if (this.isPlayerInsideCart)
                {
                    this.map.player.facingRight = this.facingRight ? true : false;
                }

                UpdatePlayerPosition();
                this.lightObject.transform.localPosition = new Vector2(1.12f, 0.61f);

                if (this.minecartWithEnemy)
                {
                    this.enemyHelmetObject.SetActive(false);
                    this.enemyLightObject.SetActive(true);
                    this.enemyObject.SetActive(false);
                    this.enemyHelmetObjectUpsideDown.SetActive(false);
                }
            });

            if(this.map.player is PlayerKing)
            {
                (this.map.player as PlayerKing).LoseHatOnCollision();
            }
            Audio.instance.PlaySfx(Assets.instance.sfxEnvMinecartFlip, position: this.position);
        }

        UpdateEnemySpriteWhileDoingFlip();
    }

    public void AdvanceApproachingMineDoor()
    {
        if(this.dontCheckForDoorFrames > 0)
        {
            this.dontCheckForDoorFrames--;
            if(this.dontCheckForDoorFrames == 0)
            {
                this.insideRailDoor = null;
            }
        }

        RailDoor collidingDoor = null;

        foreach (var door in this.chunk.subsetOfItemsOfTypeRailDoor)
        {
            if(door.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
            {
                collidingDoor = door;
                break;
            }
        }

        if(collidingDoor == null)
        {
            this.insideRailDoor = null;
            if(this.minecartWithEnemy == true)
            {
                if (this.animated.currentAnimation == this.animationEnemyRideHidden)
                {
                    this.animated.PlayOnce(this.animationEnemyPopOut, delegate {
                            this.animated.PlayAndLoop(this.animationEnemyWobble);
                        });
                }
            }
        }

        if(this.insideRailDoor == null)
        {
                if(collidingDoor != null)
                {
                    this.insideRailDoor = collidingDoor;
                    if(this.minecartWithEnemy == true)
                    {
                        collidingDoor.interactingWithMinecart = true;
                        collidingDoor.otherDoor.interactingWithMinecart = true;
                        Audio.instance.PlaySfx(Assets.instance.sfxEnvEnterDoor, position: this.position, options: new Audio.Options(1, false, 0));
                        if (this.animated.currentAnimation != this.animationEnemyHide && this.animated.currentAnimation != this.animationEnemyRideHidden)
                        {
                            this.animated.PlayOnce(this.animationEnemyHide, delegate
                            {
                                this.animated.PlayAndLoop(this.animationEnemyRideHidden);
                            });
                        }
                    }
                    return;
                }
            return;
        }
        else
        {
            bool shouldOpenDoor = this.minecartWithEnemy == true ||
                                this.isPlayerInsideCart == true && this.insideRailDoor.doorAllowsPlayerIn == true ||
                                this.isPlayerInsideCart == false && this.minecartWithEnemy == false
                                ;

            if(this.isPlayerInsideCart == true && this.insideRailDoor.doorAllowsPlayerIn == false)
            {
                if(this.insideRailDoor.hitboxes[2].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
                {
                    DestroyMinecartAndEjectPlayer();
                    return;
                }
            }

            if (shouldOpenDoor)
            {
                this.insideRailDoor.interactingWithMinecart = true;
                this.insideRailDoor.otherDoor.interactingWithMinecart = true;
            }

            if(this.insideRailDoor.hitboxes[1].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
            {                
                this.currentDistanceOnRail = this.insideRailDoor.otherDoor.distanceAlongPath;

                if(this.currentDistanceOnRail < 2f)
                {
                    this.currentSpeedOnRail = this.targetSpeedOnRail = RegularSpeedOnRail;
                }
                else
                {
                    this.currentSpeedOnRail = this.targetSpeedOnRail = -RegularSpeedOnRail;
                }
                
                this.currentRail = this.insideRailDoor.otherDoor.path;

                // Debug.Log(this.insideRailDoor.otherDoor.gameObject.name + " other door tangent:" + this.currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail).tangent + "(" + this.currentDistanceOnRail + "/" + this.currentRail.length + ")");

                //check new rail orientation
                var perpendicular = Vector2.Perpendicular(this.currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail).tangent);
                var tangent = this.currentRail.PointAtDistanceAlongPath(this.currentDistanceOnRail).tangent;
                Vector2 doorExitVector = this.insideRailDoor.otherDoor.doorExitVector;
                Vector2 doorUpVector = this.insideRailDoor.otherDoor.doorUpVector;

                if(Vector2.Dot(tangent, doorExitVector) > 0)
                {
                    if(Vector2.Dot(perpendicular, doorUpVector) > 0)
                    {
                        this.reversedPath = false;
                    }
                    else
                    {
                        this.reversedPath = true;
                    }
                    this.facingRight = (this.currentSpeedOnRail > 0) ?  true : false;
                }
                else
                {
                    if(Vector2.Dot(perpendicular, doorUpVector) > 0)
                    {
                        this.reversedPath = false;
                    }
                    else
                    {
                        this.reversedPath = true;
                    }
                    this.facingRight = (this.currentSpeedOnRail > 0) ?  false : true;
                }
                this.dontCheckForDoorFrames = 30;
                this.minecartExitedDoor = true;
            }
            else
            {
                if(this.insideRailDoor.otherDoor.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()) == false &&
                this.insideRailDoor.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()) == false
                )
                {
                    if(this.minecartWithEnemy == true)
                    {
                        this.animated.PlayOnce(this.animationEnemyPopOut, delegate {
                            this.animated.PlayAndLoop(this.animationEnemyWobble);
                        });
                    }
                }
            }
        }
    }

    public void CheckSensorsWhileOnRail(Raycast raycast)
    {
        if (currentRail == null)
        {
            return;
        }

        float multiplierForArc = Util.AdjustmentFactorForCompressedArcs(
            this.currentRail, this.currentDistanceOnRail
        );
        float speed = this.currentSpeedOnRail * multiplierForArc;
        float nextDistanceOnRail = currentDistanceOnRail + speed;

        if(this.insideRailDoor != null)
        {
            currentDistanceOnRail += speed;
            return;
        }

        //check for collisions
        Color col = Color.yellow;
        
        Vector2 nextPositionOnRail = this.currentRail.PointAtDistanceAlongPath(nextDistanceOnRail).position;

        var currentTopSensorAngleAndOffset = GetAngleAndRotatedPositionForOffset(new Vector2((this.reversedPath ? -1f : 1f) * Mathf.Sign(speed) * 0.6f, 1.99f),
                                                                        currentDistanceOnRail,
                                                                        this.currentRail
                                                                        );
        Vector2 currentPositionOnRail = this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail).position;


        var nextTopSensorAngleAndOffset = GetAngleAndRotatedPositionForOffset(new Vector2((this.reversedPath ? -1f : 1f) * Mathf.Sign(speed) * 0.6f, 1.99f),
                                                                    nextDistanceOnRail,
                                                                    this.currentRail
                                                                    );

        Vector2 startTopRaycastPosition = currentPositionOnRail + currentTopSensorAngleAndOffset.offset;
        Vector2 endTopRaycastPosition = nextPositionOnRail + nextTopSensorAngleAndOffset.offset;

        var currentBottomSensorAngleAndOffset = GetAngleAndRotatedPositionForOffset(new Vector2((this.reversedPath ? -1f : 1f) * Mathf.Sign(speed) * 0.6f, 0.25f),
                                                                        currentDistanceOnRail,
                                                                        this.currentRail
                                                                        );

        var nextBottomSensorAngleAndOffset = GetAngleAndRotatedPositionForOffset(new Vector2((this.reversedPath ? -1f : 1f) * Mathf.Sign(speed) * 0.6f, 0.25f),
                                                                        nextDistanceOnRail,
                                                                        this.currentRail
                                                                        );

        Vector2 startBottomRaycastPosition = currentPositionOnRail + currentBottomSensorAngleAndOffset.offset;
        Vector2 endBottomRaycastPosition = nextPositionOnRail + nextBottomSensorAngleAndOffset.offset;

        Debug.DrawLine(startTopRaycastPosition, endTopRaycastPosition, Color.white);
        Debug.DrawLine(startBottomRaycastPosition, endBottomRaycastPosition, Color.white);

        float lengthBetweenTopSensors = (endTopRaycastPosition - startTopRaycastPosition).magnitude;
        float lengthBetweenBottomSensors = (endBottomRaycastPosition - startBottomRaycastPosition).magnitude;

        var raycastResult = Raycast.CombineClosest(

                raycast.Arbitrary(startTopRaycastPosition, (endTopRaycastPosition - startTopRaycastPosition).normalized, lengthBetweenTopSensors),
                raycast.Arbitrary(startBottomRaycastPosition, (endBottomRaycastPosition - startBottomRaycastPosition).normalized, lengthBetweenBottomSensors)
        );

        //store velocity
        this.velocity = endTopRaycastPosition - startTopRaycastPosition;

        float distanceCorrection = 0;
        if (raycastResult.anything)
        {
            float percentage = raycastResult.distance / lengthBetweenTopSensors;
            distanceCorrection = (1 - percentage) * (nextDistanceOnRail - currentDistanceOnRail);
            currentDistanceOnRail += speed - distanceCorrection;

            bool sameMinecart = this.map.player.InsideMinecart == this;
            DestroyMinecartAndEjectPlayer();
            Audio.instance.PlaySfx(Assets.instance.sfxEnvBarrierMinecartHit, position: this.position, options: new Audio.Options(1, false, 0));
            if (raycastResult.items.Length > 0)
            {
                RailBarrier railBarrier = raycastResult.items[0] as RailBarrier;
                if (railBarrier != null && railBarrier.deadlyRailBarrier == true)
                {
                    if (sameMinecart)
                    {
                        this.map.player.Hit(this.position);
                    }
                    railBarrier.DestroyBarrier();
                }
            }
        }
        else
        {
            //update position
            currentDistanceOnRail += speed;
        }
    }

    public void FlipVelocityDirectionOnRail()
    {
        this.currentSpeedOnRail = -this.currentSpeedOnRail;
        this.targetSpeedOnRail = -this.targetSpeedOnRail;
        this.facingRight = !this.facingRight;
    }

    public bool IsUpsideDownOnRail()
    {
        if (this.currentRail == null)
            return false;

        var pointOnPathInfo = this.currentRail.PointAtDistanceAlongPath(currentDistanceOnRail);
        return (Vector2.Dot(Vector2.Perpendicular(pointOnPathInfo.tangent.normalized), Vector2.down) > 0.9f);
    }

    public void IncreaseMinecartSpeed()
    {
        this.boostTime = Mathf.Max(this.boostTime, 300);
    }

    public void DebugAnimations()
    {
        // if (this.animated.currentAnimation == this.animationEnemyHide)
        // {
        //     Debug.Log("animationEnemyHide:" + this.animated.frameNumber);
        // }
        // else if (this.animated.currentAnimation == this.animationEnemyRideHidden)
        // {
        //     Debug.Log("animationEnemyRideHidden:" + this.animated.frameNumber);
        // }
        // else if (this.animated.currentAnimation == this.animationEnemyWobble)
        // {
        //     Debug.Log("animationEnemyWobble:" + this.animated.frameNumber);
        // }
        // else if (this.animated.currentAnimation == this.animationEnemyPopOut)
        // {
        //     Debug.Log("animationEnemyPopOut:" + this.animated.frameNumber);
        // }
        // else
        // {
        //     Debug.Log("unknown:" + this.animated.frameNumber);
        // }
    }

    public void DebugMinecart()
    {
        // Debug.Log("===========");
        // Debug.Log("BoostTime:" + this.boostTime);
        // Debug.Log("CurrentSpeedOnRail:" + this.currentSpeedOnRail);
        // Debug.Log("TargetSpeedOnRail" + this.targetSpeedOnRail);
        // Debug.Log("IsUpsideDown:" + IsCartUpsideDown());
        // Debug.Log("FacignRight:" + this.facingRight);        
    }

    private void OnDestroy()
    {
        if(this.gameObject.scene.isLoaded == false) return;

        this.EjectPlayer();

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }
}
