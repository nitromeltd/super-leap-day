using UnityEngine;
using System.Collections;

public class MinecartStopper : Item
{
    private Transform buffer;
    public AnimationCurve speedCurve;

    private float BufferOriginalPosX;
    private float BufferOffsetPosX;

    private bool isBouncing = false;
    public override void Init(Def def)
    {
        base.Init(def);


        if (chunk.isFlippedHorizontally)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
            if (!def.tile.flip)
            {
                this.transform.localScale = new Vector3(-this.transform.localScale.x, 1, 1);
                this.hitboxes = new[] {
                    new Hitbox(-2.5f, -1.5f, 0, 1.5f, this.rotation, Hitbox.NonSolid)
                };
            }
            else
            {
                this.hitboxes = new[] {
                    new Hitbox(1.5f, 2.5f, 0, 1.5f, this.rotation, Hitbox.NonSolid)
                };
            }
        }
        else
        {
            if (def.tile.flip)
            {
                this.transform.localScale = new Vector3(-1, 1, 1);
                this.hitboxes = new[] {
                    new Hitbox(1.5f, 2.5f, 0, 1.5f, this.rotation, Hitbox.NonSolid)
                };
            }
            else
            {
                this.hitboxes = new[] {
                    new Hitbox(-2.5f, -1.5f, 0, 1.5f, this.rotation, Hitbox.NonSolid)
                };
            }
        }

        this.buffer = this.transform.Find("Buffer");
        BufferOriginalPosX = buffer.position.x;
        BufferOffsetPosX = BufferOriginalPosX + 0.2f;
    }

    public override void Advance()
    {
        base.Advance();
    }

    public void Bounce()
    {
        if(this.isBouncing == false)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxEnvRailRebound, position: this.position, options: new Audio.Options(1, false, 0));
            if (this.gameObject.activeInHierarchy == true)
                StartCoroutine(_Bounce());
        }
    }

    private IEnumerator _Bounce()
    {
        this.isBouncing = true;
        float timer = 0;
        while (timer < .25f)
        {
            timer += Time.deltaTime;
            buffer.position = new Vector3(Mathf.Lerp(BufferOriginalPosX, BufferOffsetPosX, this.speedCurve.Evaluate(timer*4)), buffer.position.y);
            yield return null;
        }
        this.isBouncing = false;
    }
}
