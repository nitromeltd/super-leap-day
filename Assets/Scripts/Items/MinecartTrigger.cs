using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinecartTrigger : Minecart
{
    private float triggerRadius = 15;
    private Vector2 triggerPosition;
    public override void Init(Def def)
    {
        base.Init(def);
        this.isTrigger = true;
        this.movementType = "when_player_in";
        if (def.tile.properties?.ContainsKey("trigger_radius") == true)
        {
            this.triggerRadius = def.tile.properties["trigger_radius"].f;
        }
        var connection = chunk.connectionLayer.NearestNodeTo(this.position, 2);
        if (connection != null)
        {
            var otherNode =
                connection.Previous() ??
                connection.Next();
            this.triggerPosition = otherNode.Position;
        }
        else
        {
            this.triggerPosition = this.position;
        }

        if (this.minecartWithEnemy && this.isTrigger == true)
        {
            this.enemyLightObject.SetActive(false);
            this.enemyObject.SetActive(true);
            this.lightObject.SetActive(false);
        }
    }

    public override void Advance()
    {
        base.Advance();

        if (Vector2.Distance(this.map.player.position, triggerPosition) <= triggerRadius)
        {
            this.movementType = "always";
            if (this.minecartWithEnemy && this.isTrigger == true)
            {
                this.enemyLightObject.SetActive(true);
                this.enemyObject.SetActive(false);
                this.animated.PlayAndLoop(this.animationEnemyWobble);
            }
        }
    }

    public override void ResetPosition()
    {
        base.ResetPosition();
        this.movementType = "when_player_in";
    }
}
