
using UnityEngine;
using NitromeEditor;
using System.Collections.Generic;

public class MovementAnchor : Item
{
    private Path path;
    private float timeAtStart;

    override public void Init(Def def)
    {
        base.Init(def);

        this.path = this.chunk.pathLayer.NearestPathTo(this.position);
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        if (this.path != null)
        {
            this.timeAtStart = this.path.NearestPointTo(this.position)?.timeSeconds ?? 0;

            DrawPathDots();
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
    }

    private void DrawPathDots()
    {
        // if another MovementAnchor earlier in the chunk's item list already
        // drew dots for this path, then don't redraw them again.
        foreach (var item in this.chunk.items)
        {
            if (item == this) break;
            if ((item as MovementAnchor)?.path == this.path) return;
        }

        var edges = new List<Path.Edge>();
        float startDistance = 0;

        void DrawEdges()
        {
            if (edges.Count == 0) return;

            float totalLength = 0;
            foreach (var e in edges)
                totalLength += e.distance;

            int totalDots = Mathf.RoundToInt(totalLength);
            if (totalDots < 1)
                totalDots = 1;

            for (int n = 0; n < totalDots; n += 1)
            {
                float through = (float)n / (float)totalDots;
                float dist = startDistance + (through * totalLength);
                Vector2 pt = this.path.PointAtDistanceAlongPath(dist).position;
                Instantiate(
                    Assets.instance.movementPathDot,
                    pt,
                    Quaternion.identity,
                    this.chunk.transform
                ).name = "dot";
            }

            startDistance += totalLength;
            edges.Clear();
        }

        foreach (var edge in this.path.edges)
        {
            edges.Add(edge);
            if (edge.to.arcLength == 0)
                DrawEdges();
        }
        DrawEdges();
    }

    // happens before Advance()
    public void AdvanceSubstep(float throughFrame)
    {
        if (this.path == null) return;

        var startTime = TimeForFrame(this.map.frameNumber - 1);
        var endTime   = TimeForFrame(this.map.frameNumber);
        var time      = Mathf.Lerp(startTime, endTime, throughFrame) % this.path.duration;

        this.position = this.path.PointAtTime(time).position;
        this.transform.position = this.position;

        var tileGrid = this.group?.followerTileGrid;
        if (tileGrid != null)
            tileGrid.offset = this.position - this.group.leaderHome;
    }

    override public void Advance()
    {
        if (this.path == null) return;

        var time = TimeForFrame(this.map.frameNumber) % this.path.duration;

        this.position = this.path.PointAtTime(time).position;
        this.transform.position = this.position;
    }

    private float TimeForFrame(int frameNumber) =>
        this.timeAtStart + (frameNumber / 60.0f);
}
