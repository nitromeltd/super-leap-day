using UnityEngine;
using System.Collections;

public class MultiplayerHeadset : Item
{
    public Vector2 home;

    public Transform yolk;
    public Transform sprout;
    public Transform puffer;
    public Transform goop;
    public Transform king;
    public Transform golfbot;

    public Transform joinIcon;
    public SpriteRenderer flash;

    public Animated controllerLines;
    public Animated controllerWhite;
    public Animated.Animation animationControllerLines;
    public Animated.Animation animationControllerWhite;
    public SpriteRenderer arrowLeft;
    public SpriteRenderer arrowRight;

    public override void Init(Def def)
    {
        base.Init(def);

        this.home = this.position;

        this.grabInfo.grabPositionRelativeToItem = new Vector2(0, -3.5f);
        this.controllerLines.PlayAndLoop(this.animationControllerLines);
        this.controllerWhite.PlayAndLoop(this.animationControllerWhite);
        this.arrowLeft.gameObject.SetActive(false);
        this.arrowRight.gameObject.SetActive(false);
    }

    public void ShowSmallArrows()
    {
        this.arrowLeft.gameObject.SetActive(true);
        this.arrowRight.gameObject.SetActive(true);
    }

    public void HideSmallArrows()
    {
        this.arrowLeft.gameObject.SetActive(false);
        this.arrowRight.gameObject.SetActive(false);
    }

    public override void Advance()
    {
        float a = 0.4f + (0.4f * Mathf.Sin(this.map.frameNumber * 1.3f));
        this.flash.color = new Color(1, 1, 1, a);
    }

    public void ShowCharacter(Character? character)
    {
        this.yolk  .gameObject.SetActive(character == Character.Yolk);
        this.sprout.gameObject.SetActive(character == Character.Sprout);
        this.puffer.gameObject.SetActive(character == Character.Puffer);
        this.goop  .gameObject.SetActive(character == Character.Goop);
        this.king  .gameObject.SetActive(character == Character.King);
    }

    public SpriteRenderer[] CharacterSprites()
    {
        Transform ch = null;
        if (this.yolk  .gameObject.activeSelf == true) ch = this.yolk;
        if (this.sprout.gameObject.activeSelf == true) ch = this.sprout;
        if (this.puffer.gameObject.activeSelf == true) ch = this.puffer;
        if (this.goop  .gameObject.activeSelf == true) ch = this.goop;
        if (this.king  .gameObject.activeSelf == true) ch = this.king;

        if (ch != null)
            return ch.GetComponentsInChildren<SpriteRenderer>();
        else
            return null;
    }

    public void ShowJoinIcon() => this.joinIcon.gameObject.SetActive(true);

    public void HideJoinIcon() => this.joinIcon.gameObject.SetActive(false);

    public void Retract()
    {
        ShowCharacter(null);

        IEnumerator Sequence()
        {
            var up = this.home.Add(0, 8);

            var tweener = new Tweener();
            tweener.Move(this.transform, up, 0.8f, Easing.QuadEaseInOut);
            
            while (tweener.IsDone() == false)
            {
                tweener.Advance(1 / 60f);
                this.position = this.transform.position;
                yield return null;
            }
        }
        StartCoroutine(Sequence());
    }

    public void ReturnToDownPosition()
    {
        if ((this.position - this.home).sqrMagnitude < 1 * 1) return;

        IEnumerator Sequence()
        {
            var tweener = new Tweener();
            tweener.Move(this.transform, this.home, 0.8f, Easing.QuadEaseInOut);
            
            while (tweener.IsDone() == false)
            {
                tweener.Advance(1 / 60f);
                this.position = this.transform.position;
                yield return null;
            }
        }
        StartCoroutine(Sequence());
    }
}

