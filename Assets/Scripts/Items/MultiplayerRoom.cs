using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.AddressableAssets;
using TMPro;

public class MultiplayerRoom : Item
{
    public enum State
    {
        Waiting1P,
        PlayerAttachedWaitingForJoin,
        Joined2PWaitingForCharacterSelection,
        CharacterSelectionConfirmedAndWaitingForLightsToStart,
        TrafficLights,
        TwoPlayerHasStarted
    }

    public enum SelectionInCharacterSelect
    {
        Left, Right, Yes, No
    }

    public enum InteractionStyle { Cursor, Direct }
    /*
        There are two different ways of interacting with this room, which have
        a different control scheme and subtly different appearance.

        Cursor [iOS, tvOS, macOS]:
            Show left and right physical buttons drop down around the player.
            Show 'yes' and 'no' physical buttons underneath.
            Add a "fake" selection cursor so that the player can press these buttons.
            (The normal selection cursor lives on the UI layer, so isn't useful here.)

        Direct [Switch]:
            Player selects character with left, right, and A to confirm (or equivalent).
            Show small arrows around character to indicate selectability.
            Flip up tick icon when complete.
    */

    public SpriteRenderer monitorFlash;
    public List<MultiplayerHeadset> headsets;
    public SpriteRenderer trafficLightsVertical;
    public SpriteRenderer trafficLightsHorizontal;
    public Transform bottomTextPanel;
    public LocalizeText bottomText;
    public SpriteRenderer surroundCursorStyle;
    public SpriteRenderer surroundDirectStyle;
    public Animated buttonYes1;
    public Animated buttonNo1;
    public Animated buttonYes2;
    public Animated buttonNo2;
    public SpriteRenderer buttonYes1SpriteRenderer;
    public SpriteRenderer buttonNo1SpriteRenderer;
    public SpriteRenderer buttonYes2SpriteRenderer;
    public SpriteRenderer buttonNo2SpriteRenderer;
    public Transform buttonContainer;
    public Animated buttonLeft;
    public Animated buttonRight;
    public Transform doorBottom;
    public Transform doorTop;
    public MultiplayerRoomSelectionIndicator buttonLeftSelectionIndicator;
    public MultiplayerRoomSelectionIndicator buttonRightSelectionIndicator;
    public MultiplayerRoomSelectionIndicator buttonYes1SelectionIndicator;
    public MultiplayerRoomSelectionIndicator buttonNo1SelectionIndicator;
    public MultiplayerRoomSelectionIndicator buttonYes2SelectionIndicator;
    public MultiplayerRoomSelectionIndicator buttonNo2SelectionIndicator;
    public Animated[] animationCharacterSelectionConfirmed;

    public Animated.Animation animationYesButtonLift;
    public Animated.Animation animationYesButtonDrop;
    public Animated.Animation animationNoButtonLift;
    public Animated.Animation animationNoButtonDrop;
    public Animated.Animation animationLeftButtonPress;
    public Animated.Animation animationRightButtonPress;
    public Sprite spriteButtonYesNormal;
    public Sprite spriteButtonYesPress;
    public Sprite spriteButtonNoNormal;
    public Sprite spriteButtonNoPress;
    public Sprite[] spritesTrafficLightsVertical;
    public Sprite[] spritesTrafficLightsHorizontal;

    private int disableGrabTime;
    private State state = State.Waiting1P;
    private Character selectedCharacter;
    private SelectionInCharacterSelect selectionInCharacterSelect;
    private bool characterSelectButtonPressed;
    private Rewired.Controller assignedControllerForThisMap; // see Rebind()
    private int timeInCharacterSelectionState;

    public Character SelectedCharacter => this.selectedCharacter;

    private (
        InteractionStyle interactionStyle,
        bool hasFourHeadsets,
        bool useNativeControllerConnection
    ) GetBehaviour()
    {
        #if UNITY_SWITCH && !UNITY_EDITOR
            return (InteractionStyle.Direct, /*4*/ true, /*native*/ true);
        #elif UNITY_IOS
            return (InteractionStyle.Cursor, /*4*/ true, false);
        #elif UNITY_TVOS
            return (InteractionStyle.Direct, /*2*/ false, false);
        #elif UNITY_STANDALONE_OSX
            return (InteractionStyle.Direct, /*4*/ true, false);
        #elif UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            return (InteractionStyle.Direct, /*4*/ true, false);
        #else
            return (InteractionStyle.Direct, /*2*/ false, false);
        #endif
    }

    private InteractionStyle GetInteractionStyle() => GetBehaviour().interactionStyle;

    public bool HasCharacterBeenSelected() => this.state switch
    {
        State.Waiting1P                            => false,
        State.PlayerAttachedWaitingForJoin         => false,
        State.Joined2PWaitingForCharacterSelection => false,
        _ => true
    };

    public static bool IsMultiplayerStyleSplitscreen()
    {
        #if UNITY_TVOS || UNITY_STANDALONE_OSX || UNITY_SWITCH
            return true;
        #elif UNITY_IOS
            return false;
        #else
            return GameCamera.IsLandscape();
        #endif
    }

    public int? HeadsetIndexWithPlayer()
    {
        for (int n = 0; n < this.headsets.Count; n += 1)
        {
            if (this.map.player.grabbingOntoItem == this.headsets[n])
                return n;
        }
        return null;
    }

    private static int[] HeadsetIndexesWithPlayersAllMaps()
    {
        var result = new List<int>();

        foreach (var map in Game.instance.maps)
        {
            var room = MultiplayerRoomInMap(map);
            var headsetIndex = room.HeadsetIndexWithPlayer();
            if (headsetIndex != null)
                result.Add(headsetIndex.Value);
        }

        return result.ToArray();
    }

    public static bool IsHeadsetFree(int index)
    {
        for (int mapNumber = 1; mapNumber <= Game.instance.maps.Length; mapNumber += 1)
        {
            var map = Game.instance.maps[mapNumber - 1];
            var room = MultiplayerRoomInMap(map);
            var player = map.player;
            if (player.grabbingOntoItem == room.headsets[index])
                return false;
        }
        return true;
    }

    public int? FirstFreeHeadset()
    {
        for (int result = 0; result < this.headsets.Count; result += 1)
        {
            if (IsHeadsetFree(result) == true)
                return result;
        }
        return null;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.headsets = this.chunk.items.OfType<MultiplayerHeadset>().ToList();

        if (GetBehaviour().hasFourHeadsets == true)
        {
            var centerX =
                this.headsets.Select(h => h.transform.localPosition.x).Average();

            while (this.headsets.Count < 4)
            {
                var newHeadset = Instantiate(this.headsets[0], this.chunk.transform);
                newHeadset.Init(this.headsets[0].ItemDef);
                this.headsets.Add(newHeadset);
                this.chunk.items.Add(newHeadset);
            }

            for (int n = 0; n < this.headsets.Count; n += 1)
            {
                var h = this.headsets[n];
                var pos = h.transform.localPosition;
                pos.x = centerX + ((n - 1.5f) * 3f);
                pos.y -= 0.5f;
                h.transform.localPosition = pos;
                h.home = h.position = h.transform.position;

                if (Multiplayer.StyleOnThisPlatform() == Multiplayer.Style.SplitScreen)
                {
                    var c = this.animationCharacterSelectionConfirmed[n];
                    pos = c.transform.position;
                    pos.x = this.headsets[n].transform.position.x;
                    c.transform.position = pos;
                    c.gameObject.SetActive(true);
                }
            }
        }

        if (GetBehaviour().interactionStyle == InteractionStyle.Cursor)
        {
            this.surroundCursorStyle.gameObject.SetActive(true);
            this.surroundDirectStyle.gameObject.SetActive(false);
            this.bottomTextPanel.gameObject.SetActive(false);
            this.buttonNo1SpriteRenderer.gameObject.SetActive(true);
            this.buttonNo2SpriteRenderer.gameObject.SetActive(true);
            this.buttonYes1SpriteRenderer.gameObject.SetActive(true);
            this.buttonYes2SpriteRenderer.gameObject.SetActive(true);
        }
        else
        {
            this.surroundCursorStyle.gameObject.SetActive(false);
            this.surroundDirectStyle.gameObject.SetActive(true);
            this.bottomTextPanel.gameObject.SetActive(true);
            this.buttonNo1SpriteRenderer.gameObject.SetActive(false);
            this.buttonNo2SpriteRenderer.gameObject.SetActive(false);
            this.buttonYes1SpriteRenderer.gameObject.SetActive(false);
            this.buttonYes2SpriteRenderer.gameObject.SetActive(false);
        }

        UpdateHitboxes(false);
        SetBottomText("Enter VR helmet to add players", false);

        this.selectedCharacter = SaveData.GetSelectedCharacter();

        Audio.instance.PlaySfxLoop(Assets.instance.sfxMpArcadeAmb, position: this.position);
        Audio.instance.ChangeSfxVolume(Assets.instance.sfxMpArcadeAmb, setTo: 1, startFrom: 0, duration: 0.1f);
    }

    public void OnDestroy()
    {
        RestoreControllerAutoAssignmentBehaviour();
    }

    private void UpdateHitboxes(bool doorHitbox)
    {
        this.hitboxes = new []
        {
            new Hitbox(-8,  8, 0, 1, 0, Hitbox.Solid),
            new Hitbox(-8,  8, 8, 9, 0, Hitbox.Solid),
            new Hitbox( 7,  8, 1, 8, 0, Hitbox.Solid),
            new Hitbox(-9, -7, 1, 2, 0, Hitbox.Solid),
            new Hitbox(-9, -7, 7, 8, 0, Hitbox.Solid)
        };

        if (doorHitbox == true)
        {
            var list = this.hitboxes.ToList();
            list.Add(new Hitbox(-8.5f, -7, 2, 7, 0, Hitbox.Solid));
            this.hitboxes = list.ToArray();
        }
    }

    public void NotifyWeHaveJoinedGameIOS(int warpToHeadsetNumber /* 1-based */)
    {
        var headset = this.headsets[warpToHeadsetNumber - 1];
        var player = this.map.player;

        player.position = headset.position.Add(0, -3);
        player.velocity = Vector2.zero;
        player.state = Player.State.Normal;
        player.InsideEntranceLift = null;
        player.spriteRenderer.maskInteraction = SpriteMaskInteraction.None;
        player.GrabItem(headset);
        headset.ShowCharacter(player.Character());

        foreach (var h in this.headsets)
        {
            if (h != headset)
                h.ShowCharacter(null);
        }

        CloseDoorsInstantly();
        SetStateJoined();
    }

    public void NotifyOtherPlayerIsActiveIOS()
    {
        if (this.state == State.PlayerAttachedWaitingForJoin)
        {
            SetStateJoined();
        }
    }

    public void NotifyMatchmakerPopupWasCancelledIOS()
    {
        Invoke(CancelPlayerAttached, 0.3f);
    }

    override public void Advance()
    {
        {
            float a = 0.04f + (0.04f * Mathf.Sin(this.map.frameNumber * 1.3f));
            this.monitorFlash.color = new Color(1, 1, 1, a);
        }
        foreach (var h in this.headsets)
        {
            h.Advance();
        }

        switch (this.state)
        {
            case State.Waiting1P:
            {
                var player = this.map.player;
                if (player.grabbingOntoItem == null)
                {
                    if (this.disableGrabTime > 0)
                    {
                        this.disableGrabTime -= 1;
                    }
                    else
                    {
                        foreach (var headset in this.headsets)
                        {
                            var pos = headset.position.Add(0, -3);
                            if ((player.position - pos).sqrMagnitude < 1 * 1)
                            {
                                SetStatePlayerAttached(headset);
                            }
                        }
                    }
                }
                break;
            }
            case State.PlayerAttachedWaitingForJoin:
            {
                if (GameInput.Player(1).pressedJump == true)
                {
                    CancelPlayerAttached();
                }

                break;
            }
            case State.Joined2PWaitingForCharacterSelection:
            {
                this.timeInCharacterSelectionState += 1;

                if (GameCenterMultiplayer.IsActive == true &&
                    GameCenterMultiplayer.HasAnyoneLeftIntentionally() == true)
                {
                    CancelPlayerAttached();
                    GameCenterMultiplayer.EndMultiplayerGame();
                    break;
                }

                if (GetInteractionStyle() == InteractionStyle.Cursor)
                {
                    AdvanceCursorInput(GameInput.Player(this.map));
                    UpdateCharacterSelectButtons();
                }
                else
                {
                    AdvanceDirectInput(GameInput.Player(this.map));
                }

                break;
            }
            case State.CharacterSelectionConfirmedAndWaitingForLightsToStart:
            {
                if (IsMultiplayerStyleSplitscreen() == true)
                {
                    var allRooms =
                        Game.instance.maps.Select(MultiplayerRoomInMap).ToArray();
                    if (allRooms.All(room => room.state == this.state))
                    {
                        foreach (var room in allRooms)
                        {
                            room.SetStateTrafficLights();
                        }
                    }
                }
                else
                {
                    if (GameCenterMultiplayer.HasAnyoneLeftIntentionally() == true)
                    {
                        CancelPlayerAttached();
                        GameCenterMultiplayer.EndMultiplayerGame();
                        break;
                    }

                    if (GameCenterMultiplayer.HasEveryoneElseConfirmedCharacter() == true)
                        SetStateTrafficLights();
                }
                break;
            }
        }

        // if player 4 jumps, it's an incoming player joining.
        if (Multiplayer.StyleOnThisPlatform() == Multiplayer.Style.SplitScreen &&
            this.map == Game.instance.map1 &&
            this.state
                is State.PlayerAttachedWaitingForJoin
                or State.Joined2PWaitingForCharacterSelection
                or State.CharacterSelectionConfirmedAndWaitingForLightsToStart &&
            Game.instance.maps.Length < this.headsets.Count &&
            GameInput.Player(4).pressedJump == true)
        {
            int nextPlayerNumber = Game.instance.maps.Length + 1;

            if (this.state == State.PlayerAttachedWaitingForJoin)
                SetStateJoined();
            else
                AddAdditionalMapForNewlyArrivedPlayer(nextPlayerNumber);

            MultiplayerRoomInMap(nextPlayerNumber).assignedControllerForThisMap =
                LastPressedControllerP4();
            RebindControllersForMultiplayer();
        }

        if (GetInteractionStyle() == InteractionStyle.Direct)
        {
            bool arrows = (this.state == State.Joined2PWaitingForCharacterSelection);
            bool join =
                (
                    this.state == State.Joined2PWaitingForCharacterSelection ||
                    this.state == State.PlayerAttachedWaitingForJoin
                ) &&
                GetBehaviour().useNativeControllerConnection == false;
            int firstFreeHeadset = FirstFreeHeadset() ?? 4;
            var playerHeadset = HeadsetPlayerIsInside();

            for (int n = 0; n < this.headsets.Count; n += 1)
            {
                if (arrows == true && this.headsets[n] == playerHeadset)
                    this.headsets[n].ShowSmallArrows();
                else
                    this.headsets[n].HideSmallArrows();

                if (join == true && n == firstFreeHeadset)
                    this.headsets[n].ShowJoinIcon();
                else
                    this.headsets[n].HideJoinIcon();
            }
        }

        UpdateSelectionIndicator();
    }

    private Map OtherMap() =>
        (this.map == Game.instance.map1) ? Game.instance.map2 : Game.instance.map1;

    private static MultiplayerRoom MultiplayerRoomInMap(int mapNumber)
    {
        if (mapNumber >= 1 && mapNumber <= Game.instance.maps.Length)
            return MultiplayerRoomInMap(Game.instance.maps[mapNumber - 1]);
        else
            return null;
    }

    private static MultiplayerRoom MultiplayerRoomInMap(Map map)
    {
        foreach (var chunk in map.chunks)
        {
            var room = chunk.AnyItem<MultiplayerRoom>();
            if (room != null)
                return room;
        }
        return null;
    }

    private void SetStatePlayerAttached(MultiplayerHeadset headset)
    {
        if (IsMultiplayerStyleSplitscreen() == true)
        {
            var p1 = GameInput.Player(1).rewiredPlayer;
            var p2 = GameInput.Player(2).rewiredPlayer;
            var p3 = GameInput.Player(3).rewiredPlayer;
            var p4 = GameInput.Player(4).rewiredPlayer;
            var p1Controller = GameInput.mostRecentController ?? p1.controllers.Keyboard;

            #if UNITY_SWITCH && !UNITY_EDITOR
            {
                IEnumerator RequireMultipleControllersOnSwitch()
                {
                    yield return new WaitForSeconds(0.3f);
                    if (this.state != State.PlayerAttachedWaitingForJoin) yield break;

                    var arg = new nn.hid.ControllerSupportArg
                    {
                        playerCountMin = 2,
                        playerCountMax = 4,
                        enablePermitJoyDual = true
                    };
                    var result = nn.hid.ControllerSupport.Show(arg, false);
                    if (result.IsSuccess() == false)
                    {
                        yield return new WaitForSeconds(0.3f);
                        if (this.state == State.PlayerAttachedWaitingForJoin)
                        {
                            CancelPlayerAttached();
                            yield break;
                        }
                    }

                    yield return new WaitForSeconds(1.0f);
                    p2.isPlaying = true;
                    p3.isPlaying = true;
                    p4.isPlaying = true;
                    Rewired.ReInput.controllers.AutoAssignJoysticks();

                    SetStateJoined();
                }
                StartCoroutine(RequireMultipleControllersOnSwitch());
            }
            #else
            {
                MultiplayerRoomInMap(1).assignedControllerForThisMap = p1Controller;
                RebindControllersForMultiplayer();
            }
            #endif

            IngameUI.instance.layoutLandscape2P
                .Player(1).controllerIndicator.ShowController(p1Controller);
            IngameUI.instance.layoutLandscape2P
                .Player(2).controllerIndicator.ShowController(null);
        }
        else
        {
            GameCenterMultiplayer.PresentMatchmaker();
        }

        this.state = State.PlayerAttachedWaitingForJoin;
        this.map.player.GrabItem(headset);
        this.map.player.facingRight = false;
        this.disableGrabTime = 15;

        headset.ShowCharacter(this.selectedCharacter);

        if (IsMultiplayerStyleSplitscreen() == true)
        {
            var otherHeadset = this.headsets.FirstOrDefault(h => h != headset);
            otherHeadset.ShowJoinIcon();
        }

        CloseDoors();

        SetBottomText("Press any button to join", 4);
    }

    private Rewired.Controller LastPressedControllerP4()
    {
        var p4 = GameInput.Player(4).rewiredPlayer;
        return p4.controllers.Controllers.MaxBy(c => c.GetLastTimeAnyButtonPressed());
    }

    private void RebindControllersForMultiplayer()
    {
        /* 
            I'm using .assignedControllerForThisMap as the single-source-of-truth
            for which controller is meant to be bound to which map in a split-screen
            game. Whenever this changes, calling this function will ensure that
            Rewired is up to speed.

            Any controllers allocated to a specific map [.assignedControllerForThisMap]
            go to the corresponding rewired player.

            If there are player slots available, remaining controllers will be
            handed to P4. If any of the P4 controllers jump, that's someone joining.
            If there are no player slots available, remaining controllers are
            removed from all Rewired players and ignored.

            n.b. #1. The mouse alone can't actually select a character with
            InteractionStyle = Direct, so it can't be a controller. So, I'm treating
            the keyboard + mouse as one single controller, and moving them together.

            n.b. #2. Since we'll be messing with the Rewired players'
            .excludeFromControllerAutoAssignment property to make this possible,
            we do need to make SURE that we are resetting that afterwards
            no matter how we exit this situation
            (whether that's starting the game, cancelling, or exitting the game).

            n.b. #3. none of this happens on Switch, because the platform itself
            can manage controller-to-player bindings better than we can.
            All this "joining" stuff gets skipped in that case.
        */

        #if UNITY_SWITCH && !UNITY_EDITOR
            return;
        #endif

        var controllers = new HashSet<Rewired.Controller>();
        controllers.UnionWith(GameInput.Player(1).rewiredPlayer.controllers.Controllers);
        controllers.UnionWith(GameInput.Player(2).rewiredPlayer.controllers.Controllers);
        controllers.UnionWith(GameInput.Player(3).rewiredPlayer.controllers.Controllers);
        controllers.UnionWith(GameInput.Player(4).rewiredPlayer.controllers.Controllers);

        for (int n = 1; n <= 4; n += 1)
        {
            var player = GameInput.Player(n).rewiredPlayer;

            player.isPlaying = true;
            if (this.state < State.TrafficLights)
                player.controllers.excludeFromControllerAutoAssignment = (n < 4);

            var controllerForPlayer =
                MultiplayerRoomInMap(n)?.assignedControllerForThisMap;
            if (controllerForPlayer == null) continue;

            controllers.Remove(controllerForPlayer);
            player.controllers.AddController(controllerForPlayer, true);

            if (controllerForPlayer is Rewired.Mouse or Rewired.Keyboard)
            {
                var others = controllers.Where(
                    c => c is Rewired.Mouse or Rewired.Keyboard
                ).ToArray();

                foreach (var other in others)
                {
                    controllers.Remove(other);
                    player.controllers.AddController(other, true);
                }
            }
        }

        foreach (var controller in controllers)
        {
            GameInput.Player(4).rewiredPlayer.controllers.AddController(controller, true);
        }
    }

    private void RestoreControllerAutoAssignmentBehaviour()
    {
        for (int n = 1; n <= 4; n += 1)
        {
            GameInput.Player(n).rewiredPlayer.controllers
                .excludeFromControllerAutoAssignment = false;
        }
    }

    private void CancelControllerRebindings()
    {
        var p1 = GameInput.Player(1).rewiredPlayer;
        var p2 = GameInput.Player(2).rewiredPlayer;
        var p3 = GameInput.Player(3).rewiredPlayer;
        var p4 = GameInput.Player(4).rewiredPlayer;

        #if !(UNITY_SWITCH && !UNITY_EDITOR)
        {
            /* When cancelling multiplayer, return all controllers to P1. */

            var controllers = new HashSet<Rewired.Controller>();
            controllers.UnionWith(p2.controllers.Controllers);
            controllers.UnionWith(p3.controllers.Controllers);
            controllers.UnionWith(p4.controllers.Controllers);

            foreach (var controller in controllers)
            {
                p1.controllers.AddController(controller, true);
            }
        }
        #endif

        p2.isPlaying = false;
        p3.isPlaying = false;
        p4.isPlaying = false;
    }

    private void AdvanceCursorInput(GameInput.PlayerControls controls)
    {
        if (this.characterSelectButtonPressed == true)
            return;

        #if UNITY_IOS || UNITY_EDITOR
        if (controls.rewiredPlayer.controllers.hasMouse == true)
        {
            if (Input.GetMouseButtonDown(0) == true)
            {
                var camera = this.map.gameCamera.Camera;
                var ray = camera.ScreenPointToRay(Input.mousePosition);
                var results = Physics2D.GetRayIntersection(
                    ray, Mathf.Infinity, 1 << this.map.Layer()
                );
                bool DidHit(GameObject go) => results.collider?.gameObject == go;

                if (DidHit(this.buttonYes1.gameObject) ||
                    DidHit(this.buttonYes2.gameObject))
                {
                    this.selectionInCharacterSelect = SelectionInCharacterSelect.Yes;
                    PressYesButton();
                }
                else if (DidHit(this.buttonNo1.gameObject) ||
                    DidHit(this.buttonNo2.gameObject))
                {
                    this.selectionInCharacterSelect = SelectionInCharacterSelect.No;
                    PressNoButton();
                }
                else if (DidHit(this.buttonLeft.gameObject))
                {
                    this.selectionInCharacterSelect = SelectionInCharacterSelect.Left;
                    PressLeftButton();
                }
                else if (DidHit(this.buttonRight.gameObject))
                {
                    this.selectionInCharacterSelect = SelectionInCharacterSelect.Right;
                    PressRightButton();
                }
            }

            return;
        }
        #endif

        if (controls.holdingUp == true || controls.tvOSMoveDirection?.y > 0)
        {
            if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Yes)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Left;
            else if (this.selectionInCharacterSelect == SelectionInCharacterSelect.No)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Right;
        }
        else if (controls.holdingDown == true || controls.tvOSMoveDirection?.y < 0)
        {
            if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Left)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Yes;
            else if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Right)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.No;
        }
        else if (controls.holdingLeft == true || controls.tvOSMoveDirection?.x < 0)
        {
            if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Right)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Left;
            else if (this.selectionInCharacterSelect == SelectionInCharacterSelect.No)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Yes;
        }
        else if (controls.holdingRight == true || controls.tvOSMoveDirection?.x > 0)
        {
            if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Left)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.Right;
            else if (this.selectionInCharacterSelect == SelectionInCharacterSelect.Yes)
                this.selectionInCharacterSelect = SelectionInCharacterSelect.No;
        }

        if (controls.pressedJump == true &&
            !(
                controls.rewiredPlayer.controllers.hasMouse == true &&
                Input.GetMouseButton(0) == true
            ))
        {
            switch (this.selectionInCharacterSelect)
            {
                case SelectionInCharacterSelect.Left:   PressLeftButton();   break;
                case SelectionInCharacterSelect.Right:  PressRightButton();  break;
                case SelectionInCharacterSelect.Yes:    PressYesButton();    break;
                case SelectionInCharacterSelect.No:     PressNoButton();     break;
            }
        }

        void PressLeftButton()
        {
            this.buttonLeft.PlayOnce(this.animationLeftButtonPress);
            this.characterSelectButtonPressed = true;
            Invoke(() =>
            {
                OnPressCharacterLeft();
                this.characterSelectButtonPressed = false;
            }, 0.15f);
        }
        void PressRightButton()
        {
            this.buttonRight.PlayOnce(this.animationRightButtonPress);
            this.characterSelectButtonPressed = true;
            Invoke(() =>
            {
                OnPressCharacterRight();
                this.characterSelectButtonPressed = false;
            }, 0.15f);
        }
        void PressYesButton()
        {
            this.characterSelectButtonPressed = true;
            Invoke(OnPressYes, 0.15f);
        }
        void PressNoButton()
        {
            this.characterSelectButtonPressed = true;
            Invoke(OnPressNo, 0.15f);
        }
    }

    private void AdvanceDirectInput(GameInput.PlayerControls controls)
    {
        if (controls.pressedLeft == true)
            OnPressCharacterLeft();
        else if (controls.pressedRight == true)
            OnPressCharacterRight();
        else if (controls.pressedJump == true)
            OnPressYes();
    }

    public void OnPressCharacterLeft()
    {
        var values = (Character[])Enum.GetValues(typeof(Character));
        var index = Array.IndexOf(values, this.selectedCharacter);
        index += values.Length - 1;
        index %= values.Length;
        this.selectedCharacter = values[index];
        HeadsetPlayerIsInside()?.ShowCharacter(this.selectedCharacter);
        Audio.instance.PlaySfx(Assets.instance.sfxUIConfirm);
    }

    public void OnPressCharacterRight()
    {
        var values = (Character[])Enum.GetValues(typeof(Character));
        var index = Array.IndexOf(values, this.selectedCharacter);
        index += 1;
        index %= values.Length;
        this.selectedCharacter = values[index];
        HeadsetPlayerIsInside()?.ShowCharacter(this.selectedCharacter);
        Audio.instance.PlaySfx(Assets.instance.sfxUIConfirm);
    }

    public void OnPressYes()
    {
        if (this.timeInCharacterSelectionState < 15) return;

        SetStateCharacterSelected();
        Audio.instance.PlaySfx(Assets.instance.sfxUIConfirm);
    }

    public void OnPressNo()
    {
        CancelPlayerAttached();
        if (IsMultiplayerStyleSplitscreen() == true)
        {
            MultiplayerRoomInMap(OtherMap()).CancelPlayerAttached();
            Game.instance.ChangeNumberOfMaps(1);
        }
        else
        {
            GameCenterMultiplayer.EndMultiplayerGame();
        }
        Audio.instance.PlaySfx(Assets.instance.sfxUIBack);
    }

    private void UpdateCharacterSelectButtons()
    {
        var triggeringHeadset = this.map.player.grabbingOntoItem as MultiplayerHeadset;
        SpriteRenderer yes, no;

        if (triggeringHeadset == this.headsets[0])
            (yes, no) = (this.buttonYes1SpriteRenderer, this.buttonNo1SpriteRenderer);
        else if (triggeringHeadset == this.headsets[1])
            (yes, no) = (this.buttonYes2SpriteRenderer, this.buttonNo2SpriteRenderer);
        else
            return;

        yes.sprite =
            (
                this.selectionInCharacterSelect == SelectionInCharacterSelect.Yes &&
                this.characterSelectButtonPressed == true
            ) ?
            this.spriteButtonYesPress :
            this.spriteButtonYesNormal;

        no.sprite =
            (
                this.selectionInCharacterSelect == SelectionInCharacterSelect.No &&
                this.characterSelectButtonPressed == true
            ) ?
            this.spriteButtonNoPress :
            this.spriteButtonNoNormal;
    }

    private void UpdateSelectionIndicator()
    {
        var showIndicator =
            this.state == State.Joined2PWaitingForCharacterSelection;
        var triggeringHeadset = this.map.player.grabbingOntoItem as MultiplayerHeadset;
        var selectedLeft =
            (this.selectionInCharacterSelect == SelectionInCharacterSelect.Left);
        var selectedRight =
            (this.selectionInCharacterSelect == SelectionInCharacterSelect.Right);
        var selectedYes =
            (this.selectionInCharacterSelect == SelectionInCharacterSelect.Yes);
        var selectedNo =
            (this.selectionInCharacterSelect == SelectionInCharacterSelect.No);

        this.buttonLeftSelectionIndicator.SetVisible(showIndicator && selectedLeft);
        this.buttonRightSelectionIndicator.SetVisible(showIndicator && selectedRight);
        this.buttonYes1SelectionIndicator.SetVisible(
            showIndicator && selectedYes && triggeringHeadset == this.headsets[0]
        );
        this.buttonYes2SelectionIndicator.SetVisible(
            showIndicator && selectedYes && triggeringHeadset == this.headsets[1]
        );
        this.buttonNo1SelectionIndicator.SetVisible(
            showIndicator && selectedNo && triggeringHeadset == this.headsets[0]
        );
        this.buttonNo2SelectionIndicator.SetVisible(
            showIndicator && selectedNo && triggeringHeadset == this.headsets[1]
        );
    }

    private void SetStateJoined()
    {
        if (Multiplayer.StyleOnThisPlatform() == Multiplayer.Style.GameKit &&
            DebugPanel.selectedMode != DebugPanel.Mode.Algorithm)
        {
            Debug.LogWarning(
                "Multiplayer mode disabled because DebugPanel.selectedMode != Algorithm."
            );
            return;
        }

        this.state = State.Joined2PWaitingForCharacterSelection;

        #if UNITY_SWITCH && !UNITY_EDITOR
        if (IsMultiplayerStyleSplitscreen() == true)
        {
            var playerCount = 0;
            for (int n = 1; n <= 4; n += 1)
            {
                var player = GameInput.Player(n).rewiredPlayer;
                if (player.controllers.Controllers.Any())
                    playerCount += 1;
            }

            Game.instance.ChangeNumberOfMaps(playerCount);

            foreach (var headset in this.headsets)
            {
                headset.HideJoinIcon();
                if (headset != this.headsets[0])
                    headset.ShowCharacter(null);
            }

            for (int n = 0; n < playerCount; n += 1)
            {
                var otherMap = Game.instance.maps[n];
                var otherPlayer = otherMap.player;
                var otherChunk = otherMap.NearestChunkTo(this.position);
                var otherRoom = otherChunk.NearestItemTo<MultiplayerRoom>(this.position);

                otherPlayer.position =
                    this.headsets[n].grabInfo.GetGrabPositionInWorldSpace();
                otherPlayer.velocity = Vector2.zero;
                otherPlayer.state = Player.State.Normal;
                otherPlayer.facingRight = false;
                otherPlayer.GrabItem(otherRoom.headsets[n]);

                otherRoom.headsets[n].ShowCharacter(otherPlayer.Character());

                otherPlayer.map.gameCamera.Advance(true);

                otherRoom.state = State.Joined2PWaitingForCharacterSelection;
                otherRoom.OpenCharacterSelectButtons();
                otherRoom.CloseDoorsInstantly();

                otherRoom.SetBottomText(
                    "Choose a character",
                    "<sprite=0><sprite=3><sprite=1>",
                    "Accept",
                    "<sprite=4>"
                );

                for (int h = playerCount; h < this.headsets.Count; h += 1)
                {
                    otherRoom.headsets[h].Retract();
                }

                var controllerIndicatorP2 =
                    IngameUI.instance.layoutLandscape2P.Player(2).controllerIndicator;
                var p2 = GameInput.Player(2).rewiredPlayer;
                controllerIndicatorP2.ShowController(
                    p2.controllers.Controllers.FirstOrDefault()
                );
            }
        }
        else
        #endif

        if (IsMultiplayerStyleSplitscreen() == true && Game.instance.map2 == null)
        {
            OpenCharacterSelectButtons();
            AddAdditionalMapForNewlyArrivedPlayer(2);
        }
        else if (
            IsMultiplayerStyleSplitscreen() == false
        )
        {
            foreach (var headset in this.headsets)
            {
                headset.HideJoinIcon();
            }

            OpenCharacterSelectButtons();
        }

        SetBottomText(
            "Choose a character",
            "<sprite=0><sprite=3><sprite=1>",
            "Accept",
            "<sprite=4>"
        );
    }

    private void AddAdditionalMapForNewlyArrivedPlayer(int newMapCount)
    {
        // the parameter is really just there to make things clearer at the call site.
        // this should really only be called as we add +1 maps to the game.

        Game.instance.ChangeNumberOfMaps(newMapCount, false);

        var otherMap = Game.instance.maps[newMapCount - 1];
        var otherPlayer = otherMap.player;
        var otherHeadsetInThisWorld = this.headsets[FirstFreeHeadset() ?? 0];
        var otherHeadsetInOtherWorld =
            otherMap.NearestChunkTo(otherHeadsetInThisWorld.position)
                .NearestItemTo<MultiplayerHeadset>(otherHeadsetInThisWorld.position);

        otherPlayer.position =
            otherHeadsetInOtherWorld.grabInfo.GetGrabPositionInWorldSpace();
        otherPlayer.velocity = Vector2.zero;
        otherPlayer.state = Player.State.Normal;
        otherPlayer.facingRight = false;
        otherPlayer.GrabItem(otherHeadsetInOtherWorld);
        otherPlayer.map.gameCamera.Advance(true);

        otherHeadsetInThisWorld.HideJoinIcon();
        otherHeadsetInOtherWorld.ShowCharacter(otherPlayer.Character());

        OpenCharacterSelectButtons();

        var otherRoom = MultiplayerRoomInMap(otherMap);
        otherRoom.SetStateJoined();
        otherRoom.OpenCharacterSelectButtons();
        otherRoom.CloseDoorsInstantly();

        for (int mapIndex = 0; mapIndex < Game.instance.maps.Length; mapIndex += 1)
        {
            var room = MultiplayerRoomInMap(Game.instance.maps[mapIndex]);
            if (room.state == State.CharacterSelectionConfirmedAndWaitingForLightsToStart)
            {
                var headsetIndex = room.HeadsetIndexWithPlayer() ?? 0;
                otherRoom.animationCharacterSelectionConfirmed[headsetIndex].PlayOnce(
                    this.animationYesButtonLift
                );
            }
        }
    }

    private void CancelPlayerAttached()
    {
        if (this.state == State.Joined2PWaitingForCharacterSelection)
        {
            CloseCharacterSelectButtons();
        }

        foreach (var headset in this.headsets)
        {
            headset.ShowCharacter(null);
            headset.HideJoinIcon();
            headset.ReturnToDownPosition();
        }

        this.map.player.grabbingOntoItem = null;
        this.disableGrabTime = 15;
        this.state = State.Waiting1P;

        #if UNITY_SWITCH && !UNITY_EDITOR
        {
            GameInput.Player(2).rewiredPlayer.isPlaying = false;
            GameInput.Player(3).rewiredPlayer.isPlaying = false;
            GameInput.Player(4).rewiredPlayer.isPlaying = false;
            Rewired.ReInput.controllers.AutoAssignJoysticks();
        }
        #else
        {
            // return every controller to P1
            var controllersOnP2 =
                GameInput.Player(2).rewiredPlayer.controllers.Controllers.ToArray();

            GameInput.Player(2).rewiredPlayer.isPlaying = false;
            foreach (var c in controllersOnP2)
                GameInput.Player(1).rewiredPlayer.controllers.AddController(c, true);
        }
        #endif

        OpenDoors();

        SetBottomText("Enter VR helmet to add players", false);
    }

    private MultiplayerHeadset HeadsetPlayerIsInside()
    {
        return this.map.player.grabbingOntoItem as MultiplayerHeadset;
    }

    private void OpenCharacterSelectButtons()
    {
        if (GetInteractionStyle() == InteractionStyle.Direct)
        {
            return;
        }

        this.selectionInCharacterSelect = SelectionInCharacterSelect.Right;
        this.characterSelectButtonPressed = false;

        var triggeringHeadset =
            this.map.player.grabbingOntoItem as MultiplayerHeadset;

        var (yes, no) =
            (triggeringHeadset == this.headsets[0]) ?
            (this.buttonYes1, this.buttonNo1) :
            (this.buttonYes2, this.buttonNo2);

        yes.PlayOnce(this.animationYesButtonLift);
        no.PlayOnce(this.animationNoButtonLift);

        Audio.instance.PlaySfx(Assets.instance.sfxMpVRButtonsDown, position: this.transform.position);
        Audio.instance.PlaySfx(Assets.instance.sfxMpVRGearDown, position: this.transform.position);

        IEnumerator LowerButtons()
        {
            var down = new Vector2(
                triggeringHeadset.transform.position.x,
                this.position.y + 8
            );
            var up = down.Add(0, 6);

            this.buttonContainer.transform.position = up;
            this.buttonContainer.gameObject.SetActive(true);

            var tweener = new Tweener();
            tweener.Move(this.buttonContainer, down, 0.8f, Easing.QuadEaseInOut);

            while (tweener.IsDone() == false)
            {
                tweener.Advance(1 / 60f);
                yield return null;
            }
        }
        StartCoroutine(LowerButtons());
    }

    private void CloseCharacterSelectButtons()
    {
        if (GetInteractionStyle() == InteractionStyle.Direct)
        {
            return;
        }

        var triggeringHeadset =
            this.map.player.grabbingOntoItem as MultiplayerHeadset;

        var (yes, no) =
            (triggeringHeadset == this.headsets[0]) ?
            (this.buttonYes1, this.buttonNo1) :
            (this.buttonYes2, this.buttonNo2);

        yes.PlayOnce(this.animationYesButtonDrop);
        no.PlayOnce(this.animationNoButtonDrop);

        Audio.instance.PlaySfx(Assets.instance.sfxMpVRButtonsUp, position: this.transform.position);
        IEnumerator RaiseButtons()
        {
            var down = new Vector2(
                triggeringHeadset.transform.position.x,
                this.position.y + 8
            );
            var up = down.Add(0, 6);

            var tweener = new Tweener();
            tweener.Move(this.buttonContainer, up, 0.8f, Easing.QuadEaseInOut);

            while (tweener.IsDone() == false)
            {
                tweener.Advance(1 / 60f);
                yield return null;
            }

            this.buttonContainer.gameObject.SetActive(false);
        }
        StartCoroutine(RaiseButtons());
    }

    private void SetStateCharacterSelected()
    {
        this.state = State.CharacterSelectionConfirmedAndWaitingForLightsToStart;

        GameCenterMultiplayer.NotifyCharacterSelectionConfirmed();

        var assetReference = this.selectedCharacter switch
        {
            Character.Sprout => Assets.instance.sprout,
            Character.Goop   => Assets.instance.goop,
            Character.Puffer => Assets.instance.puffer,
            Character.King   => Assets.instance.king,
            _                => Assets.instance.yolk
        };
        var operation = assetReference.InstantiateAsync(this.map.transform);
        operation.Completed += (handleToNewPlayer) =>
        {
            var oldPlayer = this.map.player;
            var newPlayer = handleToNewPlayer.Result.GetComponent<Player>();
            newPlayer.Init(this.map, 8, 1);
            newPlayer.position         = oldPlayer.position;
            newPlayer.state            = oldPlayer.state;
            newPlayer.grabbingOntoItem = oldPlayer.grabbingOntoItem;
            newPlayer.facingRight      = oldPlayer.facingRight;
            Util.SetLayerRecursively(newPlayer.transform, this.map.Layer(), true);

            Addressables.ReleaseInstance(oldPlayer.gameObject);
            Destroy(oldPlayer.gameObject);
            this.map.player = newPlayer;

            foreach (var chunk in this.map.chunks)
            {
                foreach (var item in chunk.items)
                {
                    // this is to stop items storing an old, destroyed Player in some field
                    // somewhere. If item.map == null, it means Init() hasn't been called yet,
                    // so that item probably hasn't stored a reference to the old Player away
                    if (item.map != null)
                        item.NotifyNewPlayerCreatedForMultiplayer(newPlayer);
                }
            }

            foreach (var map in Game.instance.maps)
                map.CreateGhostPlayers();
        };

        CloseCharacterSelectButtons();

        if (this.headsets.Count == 2)
            SetBottomText("Waiting for other player...", false);
        else
            SetBottomText("Waiting for other players...", false);

        int headsetIndex = HeadsetIndexWithPlayer() ?? 0;
        foreach (var map in Game.instance.maps)
        {
            var room = MultiplayerRoomInMap(map);
            room.animationCharacterSelectionConfirmed[headsetIndex].PlayOnce(
                this.animationYesButtonLift
            );
        }
    }

    public void SetStateTrafficLights()
    {
        this.state = State.TrafficLights;

        var triggeringHeadset =
            this.map.player.grabbingOntoItem as MultiplayerHeadset;

        // we're now definitely past any weird controller assignment stuff,
        // so we can remove the custom behaviour established earlier.
        RestoreControllerAutoAssignmentBehaviour();

        IEnumerator Sequence()
        {
            var (lights, sprites, up, down) =
                (GetInteractionStyle() == InteractionStyle.Cursor) ?
                (
                    this.trafficLightsVertical, this.spritesTrafficLightsVertical,
                    new Vector3(0, 8, 0), Vector3.zero
                ) :
                (
                    this.trafficLightsHorizontal, this.spritesTrafficLightsHorizontal,
                    new Vector3(0, 3, 0), Vector3.zero
                );

            lights.transform.localPosition = up;
            lights.gameObject.SetActive(true);
            SetBottomText("Ready", false);

            var filledHeadsetIndexes = HeadsetIndexesWithPlayersAllMaps();

            var tweener = new Tweener();
            tweener.MoveLocal(
                lights.transform, down, 0.8f, Easing.QuadEaseInOut
            );
            tweener.Callback(2, () => {
                lights.sprite = sprites[1];
                Audio.instance.PlaySfx(Assets.instance.sfxMpReadySetGo1, options: new Audio.Options(1, false, 0));
            });
            tweener.Callback(3, () => {
                lights.sprite = sprites[2];
                Audio.instance.PlaySfx(Assets.instance.sfxMpReadySetGo1, options: new Audio.Options(1, false, 0));
                SetBottomText("Set", false);
            });
            tweener.Callback(4, () => {
                lights.sprite = sprites[3];
                SetBottomText("GO!", false);

                this.map.player.grabbingOntoItem = null;
                this.map.player.state = Player.State.DropVertically;
                this.map.player.facingRight = false;
                this.headsets.ForEach(h => h.Retract());
                Audio.instance.PlaySfx(Assets.instance.sfxMpReadySetGo2, options: new Audio.Options(1, false, 0));
                OpenDoors();

                foreach (var headsetIndex in filledHeadsetIndexes)
                {
                    this.animationCharacterSelectionConfirmed[headsetIndex].PlayOnce(
                        this.animationYesButtonDrop
                    );
                }

                if (GameCenterMultiplayer.IsActive == true)
                    GameCenterMultiplayer.NotifyMatchHasBegun();
            });
            tweener.MoveLocal(
                lights.transform, up, 0.8f, Easing.QuadEaseInOut, 5
            );

            Audio.instance.PlaySfx(Assets.instance.sfxMpVRGearUp, options: new Audio.Options(1, false, 0));

            while (tweener.IsDone() == false)
            {
                tweener.Advance(Time.deltaTime);
                yield return null;
            }

            lights.gameObject.SetActive(false);
            this.state = State.TwoPlayerHasStarted;
        }
        StartCoroutine(Sequence());
    }

    private void CloseDoors()
    {
        this.doorBottom.gameObject.SetActive(true);
        this.doorTop   .gameObject.SetActive(true);
        UpdateHitboxes(true);

        IEnumerator Sequence()
        {
            var tweener = new Tweener();
            tweener.MoveLocal(this.doorBottom, Vector3.zero, 0.4f, Easing.QuadEaseIn);
            tweener.MoveLocal(this.doorTop,    Vector3.zero, 0.4f, Easing.QuadEaseIn);

            while (tweener.IsDone() == false)
            {
                tweener.Advance(Time.deltaTime);
                yield return null;
            }
        }
        StartCoroutine(Sequence());
    }

    private void CloseDoorsInstantly()
    {
        this.doorBottom.gameObject.SetActive(true);
        this.doorTop   .gameObject.SetActive(true);
        this.doorBottom.transform.localPosition = Vector2.zero;
        this.doorTop   .transform.localPosition = Vector2.zero;
        UpdateHitboxes(true);
    }

    private void OpenDoors()
    {
        UpdateHitboxes(false);

        IEnumerator Sequence()
        {
            var tweener = new Tweener();
            tweener.MoveLocal(
                this.doorBottom, new Vector3(0, -2.8f, 0), 0.4f, Easing.QuadEaseOut
            );
            tweener.MoveLocal(
                this.doorTop, new Vector3(0, 2.8f, 0), 0.4f, Easing.QuadEaseOut
            );

            while (tweener.IsDone() == false)
            {
                tweener.Advance(1 / 60f);
                yield return null;
            }

            this.doorBottom.gameObject.SetActive(false);
            this.doorTop   .gameObject.SetActive(false);
        }
        StartCoroutine(Sequence());
    }

    private void SetBottomText(string text, bool large)
    {
        this.bottomText.SetText(text);
        this.bottomText.GetComponent<TMP_Text>().fontSize = large ? 9 : 5;
    }

    private void SetBottomText(string text, int spriteId)
    {
        this.bottomText.SetText(
            "MULTIPLAYER_ROOM_INSTRUCTION_1_LINE",
            Localization.GetTextWithoutFixes(text.ToUpper()),
            $"<sprite={spriteId}>"
        );
        this.bottomText.GetComponent<TMP_Text>().fontSize = 5;
    }

    private void SetBottomText(
        string top, string spriteTagsTop, string bottom, string spriteTagsBottom
    )
    {
        this.bottomText.SetText(
            "MULTIPLAYER_ROOM_INSTRUCTION_2_LINES",
            Localization.GetTextWithoutFixes(top.ToUpper()),
            spriteTagsTop,
            Localization.GetTextWithoutFixes(bottom.ToUpper()),
            spriteTagsBottom
        );
        this.bottomText.GetComponent<TMP_Text>().fontSize = 5;
    }

    /*private void SetBottomText(
        bool large = false,
        params object[] textOrSpriteIds
    )
    {
        string text = "";
        object last = null;

        foreach (var element in textOrSpriteIds)
        {
            if (element is string s)
            {
                if (last != null)
                    text += "\n";
                text += Localization.GetText(s.ToUpper());
            }
            else if (element is int spriteId)
            {
                if (last is string)
                    text += "  ";
                text += $"<sprite={spriteId}>";
            }
            last = element;
        }

        if (large == true)
            text = $"<size=180%>{text}</size>";

        this.bottomText.text = text;
        this.bottomText.font = Localization.font;
    }*/
}
