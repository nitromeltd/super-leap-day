using UnityEngine;

public class MultiplayerRoomSelectionIndicator : MonoBehaviour
{
    public Transform tl;
    public Transform tr;
    public Transform bl;
    public Transform br;
    public Vector2 homeTL;
    public Vector2 homeTR;
    public Vector2 homeBL;
    public Vector2 homeBR;

    public void Awake()
    {
        this.tl = this.transform.Find("tl");
        this.tr = this.transform.Find("tr");
        this.bl = this.transform.Find("bl");
        this.br = this.transform.Find("br");
        this.homeTL = this.tl.localPosition;
        this.homeTR = this.tr.localPosition;
        this.homeBL = this.bl.localPosition;
        this.homeBR = this.br.localPosition;
    }

    public void SetVisible(bool visible)
    {
        if (this.gameObject.activeSelf != visible)
            this.gameObject.SetActive(visible);
    }

    public void Update()
    {
        if (this.tl == null) return;

        var offset = 0.06f + (Mathf.Sin(Game.instance.map1.frameNumber * 0.15f) * 0.06f);

        this.tl.transform.localPosition = this.homeTL.Add( offset, offset);
        this.tr.transform.localPosition = this.homeTR.Add(-offset, offset);
        this.bl.transform.localPosition = this.homeBL.Add( offset, -offset);
        this.br.transform.localPosition = this.homeBR.Add(-offset, -offset);
    }
}
