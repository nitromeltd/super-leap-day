﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoGrabBlock : Item
{
    public bool facingRight;

    public Animated.Animation idleAnimation;
    public Animated.Animation lightAnimation;

    public Animated roundBitAnimated;
    public Animated.Animation roundBitIdleAnimation;
    public Animated.Animation roundBitGetOutAnimation;
    public Animated.Animation roundBitGetInAnimation;

    public override void Init(Def def)
    {
        base.Init(def);

        if(def.tile.flip == true)
        {
            this.facingRight = !this.facingRight;
        }

        this.transform.localScale = new Vector3(def.tile.flip ? -1f : 1f, 1f, 1f);

        this.hitboxes = new [] {
            new Hitbox(
                -0.5f, 0.5f, -0.5f, 0.5f, this.rotation,
                Hitbox.Solid, this.facingRight, !this.facingRight
            )
        };

        this.animated.PlayOnce(idleAnimation);
        this.roundBitAnimated.PlayOnce(roundBitIdleAnimation);

        this.onCollisionFromLeft = OnCollisionFromLeft;
        this.onCollisionFromRight = OnCollisionFromRight;

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
    }

    private void OnCollisionFromLeft(Collision collision)
    {
        if(facingRight) return;

        Trigger();
    }

    private void OnCollisionFromRight(Collision collision)
    {
        if(!facingRight) return;

        Trigger();
    }

    private void Trigger()
    {
        this.animated.PlayOnce(lightAnimation, () => 
        {
            this.animated.PlayOnce(idleAnimation);
        });

        this.roundBitAnimated.PlayOnce(roundBitGetOutAnimation, () =>
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvNoGrabBlockOut,
                position: this.position
            );
            this.roundBitAnimated.PlayOnce(roundBitGetInAnimation, () =>
            {
                this.roundBitAnimated.PlayOnce(roundBitIdleAnimation);

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvNoGrabBlockIn,
                    position: this.position
                );
            });
        });
    }
}
