using UnityEngine;

public class OneWayFlap : Item
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationActivate;
    private bool isOpen = false;

    public override void Init(Def def)
    {
        base.Init(def);    

        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, 0.55f, 1f, this.rotation, Hitbox.SolidOnTop, true, true),
            new Hitbox(-1f, 1f, -1f, 0.55f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayOnce(this.animationIdle);
    }

    public override void Advance()
    { 
        if (this.hitboxes[0].InWorldSpace().Contains(this.map.player.position))
        {
            if (this.animated.currentAnimation != this.animationActivate)
            {
                this.animated.PlayOnce(this.animationActivate, () => { this.animated.PlayOnce(this.animationIdle); });
            }
        }

        var playerRect = this.map.player.Rectangle();
        var overlapping0 = this.hitboxes[0].InWorldSpace().Overlaps(playerRect);
        var overlapping1 = this.hitboxes[1].InWorldSpace().Overlaps(playerRect);

        if (overlapping1 == true)
            this.isOpen = true;
        else if (overlapping0 == false && overlapping1 == false)
            this.isOpen = false;

        if (isOpen == true)
        {
            this.hitboxes[0] = new Hitbox(
                -1f, 1f, 0.55f, 1f, this.rotation, Hitbox.NonSolid
            );
        }
        else
        {
            this.hitboxes[0] = new Hitbox(
                -1f, 1f, 0.55f, 1f, this.rotation, Hitbox.SolidOnTop, true, true
            );
        }
    }
}
