using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class OpeningTutorialLift : Item
{
    [Header("Exterior")]
    public Animated exteriorAnimated;
    public Animated.Animation animationExteriorIdle;
    public Animated.Animation animationExteriorOpen;
    public Animated.Animation animationExteriorClose;
    public Animated.Animation animationDustLandParticle;

    [Header("Arrow")]
    public Animated arrowAnimated;
    public Animated.Animation animationArrowBobbing;

    private bool willAllowPlayerToEnter = true;
    private bool doorsOpened = false;
    private bool isGoingAway = false;
    private bool reachFinalPosition = false;
    private bool disableLift = false;
    private int goingAwayMovingTimer;
    private Vector2 initialPosition;
    private Vector2 goAwayFinalPosition;

    private Transform spriteMask;
    private Vector2 initialMaskPosition;
    private const int GoingAwayMovingTime = 60;
    private static Vector2 GoAwayOffset = new Vector2(0f, -5.5f);

    private Rect EntranceRect => this.hitboxes[0].InWorldSpace();

    [NonSerialized] public Layer layer = Layer.A;
    private string SortingLayerForDust() =>
        (this.layer == Layer.A) ? "Player Dust (AL)" : "Player Dust (BL)";

    public override void Init(Def def)
    {
        base.Init(def);

        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);
        this.arrowAnimated.PlayAndLoop(this.animationArrowBobbing);


        this.hitboxes = new[] {
            // entrance
            new Hitbox(-0.20f, 0.20f, 0f, 0.50f, this.rotation, Hitbox.NonSolid),

            // solid top under button
            new Hitbox(-0.75f, 0.75f, 3.75f, 4.4f, this.rotation, Hitbox.Solid),
        };

        this.spriteMask = this.transform.Find("Lift Sprite Mask").GetComponent<Transform>();
        this.initialMaskPosition = this.spriteMask.position;

        this.initialPosition = this.position;
        this.goAwayFinalPosition = this.position + GoAwayOffset;
        this.spriteMask.position = this.goAwayFinalPosition;
    }

    public override void Advance()
    {
        Player player = this.map.player;

        if (Vector2.Distance(player.position, this.position) < 10 && this.doorsOpened == false && player.state != Player.State.InsideLift)
        {
            this.doorsOpened = true;
            Audio.instance.PlaySfx(Assets.instance.sfxCapsuleOpen);

            this.exteriorAnimated.PlayOnce(this.animationExteriorOpen, () =>
            {
                this.arrowAnimated.gameObject.SetActive(true);
                this.arrowAnimated.PlayAndLoop(this.animationArrowBobbing);
            });
        }

        if (player.Rectangle().Overlaps(EntranceRect) &&
            player.state == Player.State.Normal &&
            this.willAllowPlayerToEnter == true)
        {
            EnterPlayer();
        }

        if (player.state == Player.State.InsideLift && player.InsideOpeningTutorialLift == this)
        {
            player.position = this.position.Add(0f, 0.95f);
        }

        if (this.goingAwayMovingTimer > 0)
        {
            this.goingAwayMovingTimer -= 1;
        }

        if (this.reachFinalPosition == false)
        {
            if (this.isGoingAway == true)
            {
                if (this.goingAwayMovingTimer == 0)
                {
                    this.position.y = Util.Slide(this.position.y, this.goAwayFinalPosition.y, 0.05f);

                    if (Vector2.Distance(this.position, this.goAwayFinalPosition) < 0.01f)
                    {
                        ReachFinalPosition();
                    }
                }

                SpawnDustParticles();
            }
        }

        this.spriteMask.position = this.initialMaskPosition;

        this.transform.position = this.position;
    }

    private void EnterPlayer()
    {
        if (this.doorsOpened == false) return;

        this.doorsOpened = false;
        Player player = this.map.player;
        player.state = Player.State.InsideLift;
        player.InsideOpeningTutorialLift = this;
        player.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;

        this.arrowAnimated.gameObject.SetActive(false);

        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleClose);
        this.exteriorAnimated.PlayOnce(this.animationExteriorClose, GoAway);
    }

    private void GoAway()
    {
        if (this.isGoingAway == true) return;

        this.isGoingAway = true;
        this.goingAwayMovingTimer = GoingAwayMovingTime;

        this.exteriorAnimated.PlayAndLoop(this.animationExteriorIdle);
        Audio.instance.PlaySfx(Assets.instance.sfxCapsuleDig);
    }

    private void ReachFinalPosition()
    {
        if (this.reachFinalPosition == true) return;
        this.reachFinalPosition = true;

        this.exteriorAnimated.PlayOnce(this.animationExteriorIdle);

        Player player = this.map.player;
        if (player.state == Player.State.InsideLift)
        {
            if (SaveData.HasCompletedOpeningTutorial() == false)
            {
                SaveData.SetHasCompletedOpeningTutorial(true);
                Achievements.CompleteAchievement(Achievements.Achievement.TutorialCompleted);

                // first run -- move onto the actual current day
                Game.selectedDate = DateTime.Now.Date;
                Map.detailsToPersist = new Map.DetailsToPersist
                {
                    returnDate = Game.selectedDate,
                    returnChunkIndex = 0,
                    playerCheckpointPosition = new Vector2(8, 1),
                    coins = this.map.coinsCollected,
                    silverCoins = this.map.silverCoinsCollected,
                    fruitCollected = this.map.fruitCollected,
                    fruitCurrent = this.map.fruitCurrent
                };
                Transition.GoToGame();
                Audio.instance.PlayMusic(Assets.instance.musicTransitionToMain);
            }
            else
            {
                // later run -- we came here from the calendar, so go back to it
                Transition.GoSimple("Calendar");
            }
        }
        if (this.disableLift == true)
        {
            this.gameObject.SetActive(false);
        }
    }

    private void SpawnDustParticles()
    {
        float x = this.initialPosition.x;
        float y = this.initialPosition.y;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 2; n += 1)
        {
            float horizontalSpeed =
                ((n % 2 == 0) ? 1 : -1) * UnityEngine.Random.Range(0.4f, 2.3f) / 16;
            float verticalSpeed = UnityEngine.Random.Range(-0.2f, 0.2f) / 16;

            var p = Particle.CreateAndPlayOnce(
                this.animationDustLandParticle,
                originPosition + new Vector2(UnityEngine.Random.Range(-1.5f, 1.5f), 0),
                this.transform.parent
            );
            var tangent = new Vector2(
                Mathf.Cos(1 * Mathf.PI / 180),
                Mathf.Sin(1 * Mathf.PI / 180)
            );
            var normal = new Vector2(-tangent.y, tangent.x);
            p.velocity = (tangent * horizontalSpeed) + (normal * verticalSpeed);
            p.acceleration = p.velocity * -0.05f / 16;
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = SortingLayerForDust();
        }
    }
}
