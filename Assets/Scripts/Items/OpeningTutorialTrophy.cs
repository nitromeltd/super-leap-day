using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class OpeningTutorialTrophy : Item
{
    public Animated.Animation animationHide;
    public Animated.Animation animationDrop;
    public Animated.Animation animationWobble;
    public Animated.Animation animationShine;
    public Animated.Animation animationBlank;
    public Animated.Animation animationSwing;

    [Header("Effects")]
    public Animated appearAnim;
    public Animated.Animation animationAppear;

    public AnimationCurve flashFadeIn;
    public AnimationCurve flashFadeOut;

    public Transform whiteLinesParent;
    public Transform[] whiteLines;

    [Header("Hop")]
    public float hopForce;

    private Vector2 velocity;
    private bool checkForPlayerNear = true;
    private bool canDrop = false;
    private bool onGround = false;
    private bool canShine = false;
    private int shineTimer = 0;
    private bool rotateWhiteLines = false;
    private bool isSwinging = false;

    private AudioClip TrophyAppearSFX() => Assets.instance.sfxSparkleBronzeTrophyAppear;

    private AudioClip TrophyFanfare() => Assets.instance.sfxFanfareBronzeSliver;

    private AudioClip TrophyWobbleSFX() => Assets.instance.sfxWobbleBronzeTrophy;

    private AudioClip TrophyJumpSFX() => Assets.instance.sfxJumpBronzeTrophy;

    private const float GRAVITY_FORCE = 0.02f;
    private const int SHINE_TIME = 30 * 5;
    private static Vector2 PLAYER_CHUNK_OFFSET = Vector2.up * -.25f;
    private static Vector2 DROP_POS_OFFSET = Vector2.up * 6f;
    private const int SPARKLES_AMOUNT = 10;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new[] {
            new Hitbox(-1f, 1.25f, 0, 3f, this.rotation, Hitbox.NonSolid)
        };
     
        this.animated.PlayAndHoldLastFrame(this.animationHide);

    }

    public override void Advance()
    {
        CheckPlayerNear();

        var raycast = new Raycast(this.map, this.position);

        if (this.canDrop == true)
        {
            MoveVertically(raycast);
        }

        this.transform.position = this.position;

        if (this.onGround == true)
        {
            Player player = this.map.player;

            bool playerColliding =
                player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace());

            bool playerJumpingUpwards =
                player.groundState.onGround == false && player.velocity.y > 0f;

            if (playerColliding == true)
            {
                bool swingLeft = this.map.player.transform.position.x > this.position.x;

                if (this.isSwinging == false)
                {
                    Swing();
                }

                if (playerJumpingUpwards == true && this.hopForce > 0f)
                {
                    if (this.isSwinging == true)
                    {
                        this.isSwinging = false;
                    }

                    this.velocity = Vector2.up * this.hopForce;
                    this.animated.PlayAndHoldLastFrame(this.animationDrop);
                    //this.spriteRenderer.flipX = (swingLeft == true);
                    Audio.instance.PlaySfx(TrophyJumpSFX(), position: this.position);
                }
            }
        }

        if (this.canShine == true &&
            this.isSwinging == false &&
            this.onGround == true)
        {
            if (this.shineTimer > 0)
            {
                this.shineTimer--;
            }
            else
            {
                this.shineTimer = SHINE_TIME;
                this.canShine = false;

                this.animated.PlayOnce(this.animationShine, () => {
                    this.canShine = true;
                });
            }
        }

        if (this.rotateWhiteLines == true)
        {
            this.whiteLinesParent.Rotate(Vector3.forward * 100f * Time.deltaTime);
        }
    }

    private void CheckPlayerNear()
    {
        if (!this.checkForPlayerNear) return;

        Vector2 playerPos = this.map.player.position + PLAYER_CHUNK_OFFSET;

        if (playerPos.y > this.position.y)
        {
            Appear();
        }
    }

    private void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        this.velocity.y -= GRAVITY_FORCE;

        bool wasAirborne = (this.onGround == false);
        this.onGround = false;

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.375f, 0), false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.onGround = true;

                if (wasAirborne == true)
                {
                    HitGround();
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void Appear()
    {
        this.map.player.SetCheckpointPosition(
            this.position + new Vector2(0, Player.PivotDistanceAboveFeet)
        );

        // increase trophy vertical starting position so it can drop to the ground
        this.position += DROP_POS_OFFSET;

        this.checkForPlayerNear = false;
        StartCoroutine(_Appear());
    }

    private IEnumerator _Appear()
    {
        Audio.instance.PlaySfx(TrophyAppearSFX(), position: this.position);

        Audio.instance.PlaySfx(
            TrophyFanfare(),
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );

        this.map.DecreaseMusicVolume();
        StartCoroutine(IncreaseVolumeCoroutine(2.5f));

        // sparkle particles effect
        for (int i = 0; i < SPARKLES_AMOUNT; i++)
        {
            yield return new WaitForSeconds(0.05f);

            Vector2 sparklePos = (Vector2)this.appearAnim.transform.position +
                UnityEngine.Random.insideUnitCircle * 1.5f;

            var p = Particle.CreateAndPlayOnce(
                    Assets.instance.trophySparkleAnimation,
                    sparklePos,
                    null
                );
        }
        yield return new WaitForSeconds(.1f);

        // circle effect
        this.appearAnim.PlayAndHoldLastFrame(this.animationAppear);

        float animationAppearTime = (float)this.animationAppear.sprites.Length
            / (float)this.animationAppear.fps;
        yield return new WaitForSeconds(animationAppearTime - .2f);

        // white line effect
        this.rotateWhiteLines = true;
        yield return StartCoroutine(_WhiteLinesScaleEffect(.1f, 1f));
        yield return new WaitForSeconds(.05f);
        yield return StartCoroutine(_WhiteLinesScaleEffect(.1f, 0f));
        this.rotateWhiteLines = false;

        // flash effect
        IngameUI.instance.FlashFadeIn(.2f, this.flashFadeIn);
        yield return new WaitForSeconds(.3f);

        this.appearAnim.PlayOnce(animationBlank);

        // trophy appear
        this.canDrop = true;
        this.animated.PlayAndHoldLastFrame(this.animationDrop);
        IngameUI.instance.FlashFadeOut(.1f, flashFadeOut);
    }

    private IEnumerator _WhiteLinesScaleEffect(float targetTime, float targetScaleValue)
    {
        float initialTime = 0f;
        Vector3 initialScale = this.whiteLines[0].localScale;
        Vector3 targetScale =
                new Vector3(initialScale.x, targetScaleValue, initialScale.z);

        while (initialTime < targetTime)
        {
            initialTime += Time.deltaTime;

            for (int i = 0; i < this.whiteLines.Length; i++)
            {
                this.whiteLines[i].localScale =
                    Vector3.Lerp(initialScale, targetScale, initialTime / targetTime);
            }

            yield return null;
        }
    }

    private void HitGround()
    {
        this.map.ScreenShakeAtPosition(
                        this.transform.position, Vector2.up * .2f);


        this.animated.PlayOnce(this.animationWobble, () =>
        {
            this.canShine = true;
            this.shineTimer = SHINE_TIME;
            //this.spriteRenderer.flipX = false;
            //this.hasBeenOnGroundOnce = true;
        });
    }


    private void Swing()
    {
        if (this.animationSwing.sprites.Length <= 0) return;

        this.isSwinging = true;

        this.animated.PlayOnce(this.animationSwing, () =>
        {
            this.isSwinging = false;
            this.canShine = true;
        });

        Audio.instance.PlaySfx(TrophyWobbleSFX(), position: this.position);
    }

    private IEnumerator IncreaseVolumeCoroutine(float duration)
    {
        yield return new WaitForSeconds(duration);
        this.map.IncreaseMusicVolume();
    }
}
