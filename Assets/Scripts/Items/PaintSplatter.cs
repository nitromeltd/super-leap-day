using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintSplatter : Item
{
    public Animated outline;

    public GameObject highlight1;
    public GameObject highlight2;
    public GameObject highlight3;

    [HideInInspector] public Animated.Animation appearAnimation;
    [HideInInspector] public Animated.Animation appearShadowAnimation;
    [HideInInspector] public Animated.Animation fadeAnimation;
    [HideInInspector] public Animated.Animation fadeShadowAnimation;

    private float timer;
    private bool visible = false;

    private const float TotalActiveTime = 12f;

    public static PaintSplatter Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.paintSplatter, chunk.transform);
        obj.name = "Paint Splatter (Power-up)";
        var item = obj.GetComponent<PaintSplatter>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));
        this.transform.position = this.position = position;
        this.chunk = chunk;
    }

    public override void Advance()
    {
        base.Advance();
        this.timer += Time.fixedDeltaTime;

        if (this.visible == false && this.timer < 1)
        {
            this.visible = true;
            this.animated.PlayAndHoldLastFrame(this.appearAnimation);
            this.outline.PlayAndHoldLastFrame(this.appearShadowAnimation);
        }

        if (this.timer > TotalActiveTime && this.visible == true)
        {
            this.visible = false;
            this.animated.PlayOnce(this.fadeAnimation, () =>
            {
                Destroy(this.gameObject);
                this.chunk.items.Remove(this);
            });
            this.outline.PlayAndHoldLastFrame(this.fadeShadowAnimation);
            this.highlight1.SetActive(false);
            this.highlight2.SetActive(false);
            this.highlight3.SetActive(false);
        }
    }
}
