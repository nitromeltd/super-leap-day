using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parachute : Item
{
    private bool available = false;
    public bool zoomOut = true;
    private static Vector2 HitboxSize = new Vector2(1f, 0.50f);
    private static Vector2 HitboxOffset = new Vector2(0f, 0f);

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(
                HitboxOffset.x - HitboxSize.x,
                HitboxOffset.x + HitboxSize.x,
                HitboxOffset.y - HitboxSize.y,
                HitboxOffset.y + HitboxSize.y,
                rotation: 0,
                Hitbox.NonSolid
            )
        };

        this.zoomOut = (def.tile.properties?.ContainsKey("zoom_out") == true) ?
            def.tile.properties["zoom_out"].b : false;
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        Player player = this.map.player;

        if(this.available == true)
        {
            if(this.hitboxes[0].InWorldSpace().Overlaps(player.Rectangle()))
            {
                Map.instance.player.parachute.Enter(this);
                this.available = false;
            }
        }
        else
        {
            if(player.parachute.IsAvailable == false)
            {
                this.available = true;
            }
        }

        this.spriteRenderer.enabled = this.available;
    }

}
