using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParachuteBlock : Item
{
    public Type type;

    public enum Type
    {
        On,
        Off
    }

    private const float HitboxSize = 0.50f;
    public Rect HitboxPlayerRect => this.hitboxes[0].InWorldSpace();

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(-HitboxSize, HitboxSize, -HitboxSize, HitboxSize, 0, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        Player player = Map.instance.player;

        if(player.Rectangle().Overlaps(HitboxPlayerRect))
        {
            if(this.type == Type.On)
            {
                //player.EnterParachuteZone();
            }
            else
            {
                player.parachute.Exit();
            }
        }
    }
}
