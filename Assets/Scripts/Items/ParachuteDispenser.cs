using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParachuteDispenser : Item
{
    private int ignoreGrabTime = 0;
    private bool playerGrabbingThisItem = false;
    private bool lastFramePlayerGrabbingThisItem = false;

    private const float HitboxSize = 0.50f;
    private static Vector2 GrabOffset = new Vector2(0f, 1f);

    private Vector2 GrabPosition => this.position + GrabOffset;

    public override void Init(Def def)
    {
        base.Init(def);
        
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(-HitboxSize, HitboxSize, -HitboxSize, HitboxSize, 0, Hitbox.Solid)
        };

        this.grabInfo = new GrabInfo(this, GrabType.Floor, GrabOffset);
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        AdvanceGrabbing();

        if(this.playerGrabbingThisItem == false &&
            this.lastFramePlayerGrabbingThisItem == true)
        {
            ReleaseGrabbing();
        }

        this.lastFramePlayerGrabbingThisItem = this.playerGrabbingThisItem;
    }

    private void AdvanceGrabbing()
    {
        var player = Map.instance.player;
        
        this.playerGrabbingThisItem =
            player.grabbingOntoItem != null && player.grabbingOntoItem == this;

        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreGrabTime = 30;
            }
            return;
        }

        if (this.ignoreGrabTime > 0)
        {
            this.ignoreGrabTime -= 1;
            return;
        }

        var delta = player.position - GrabPosition;
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreGrabTime = 15;
        }
    }
    
    private void ReleaseGrabbing()
    {
        //Map.instance.player.EnterParachuteZone();
    }
}
