
using UnityEngine;

public class Peg : Item
{    
    public Animated.Animation animationWobbleSide;
    public Animated.Animation animationWobbleDown;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, 0, 0.5f, this.rotation, Hitbox.Solid)
        };
        this.solidToPlayer = false;

        this.onCollisionFromLeft = NotifyEnemyBouncedOff;
        this.onCollisionFromRight = NotifyEnemyBouncedOff;
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if (this.map.player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace()))
        {
            if (this.animated.currentAnimation != this.animationWobbleDown)
            {
                this.animated.PlayOnce(this.animationWobbleDown, delegate
                {
                    this.animated.Stop();
                });

                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvButtonNeutral,
                    position: this.position
                );
            }
        }
    }

    public void NotifyEnemyBouncedOff(Collision collision)
    {
        if (this.animated.currentAnimation != this.animationWobbleSide)
        {
            this.animated.PlayOnce(this.animationWobbleSide, delegate
            {
                this.animated.Stop();
            });
        }
    }
}
