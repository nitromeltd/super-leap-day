using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class PetBaseball : Item
{
    public bool alive = true;

    public Map otherMap;

    public Animated.Animation glovesReadyAnimation;
    public Animated.Animation batSmackAnimation;
    public Animated.Animation smackEffectAnimation;
    public Animated.Animation ballFlyAnimation;
    public Animated.Animation ballWingsOpenAnimation;
    public Sprite ballSprite;
    public Sprite ballLongSprite;

    public Animated baseball;
    public Animated glove;
    public Animated bat;

    private bool followingOtherPlayer = true;
    private bool followingCurrentPlayer = true;
    private bool movingStraight = true;
    private bool moveDirectly = false;
    private bool movingDown = false;
    private float xPos;
    private float timer;
    private Vector3 vel;

    private RectTransform warningSign;
    public AnimationCurve speedCurve;
    private float distanceFromCenter;
    private bool goToOtherDevice = false;
    private bool skipInitialSetup = false;
    
    public Vector3 OtherPlayerPosition()
    {
        if(this.map.ghostPlayers != null && this.map.ghostPlayers.Count > 0)
        {
            return this.map.ghostPlayers[0].transform.position;
        }
        
        return this.otherMap.player.position;
    }
    
    public Map OtherPlayerMap()
    {
        if(this.map.ghostPlayers != null && this.map.ghostPlayers.Count > 0 && this.skipInitialSetup == true)
        {
            return this.map;
        }
        
        return this.otherMap;
    }
    
    public static PetBaseball Create(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petBaseball, chunk.transform);
        obj.name = "Spawned Pet Baseball (Power-up)";
        var item = obj.GetComponent<PetBaseball>();
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public static PetBaseball CreateP1(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petBaseball, chunk.transform);
        obj.name = "Spawned Pet Baseball (Power-up) for P1";
        var item = obj.GetComponent<PetBaseball>();
        item.goToOtherDevice = true;
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public static PetBaseball CreateP2(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        position = Map.instance.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 1.1f));
        chunk = Map.instance.NearestChunkTo(Map.instance.player.position);
        
        var obj = Instantiate(Assets.instance.petBaseball, chunk.transform);
        obj.name = "Spawned Pet Baseball (Power-up) for P2";
        var item = obj.GetComponent<PetBaseball>();
        item.skipInitialSetup = true;
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        base.Init(new Def(chunk));
        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.hitboxes = new[] {
            new Hitbox(-.25f, .25f, -.25f, .25f, this.rotation, Hitbox.NonSolid),
            new Hitbox( .5f, .75f, -.25f, .25f, this.rotation, Hitbox.NonSolid)
        };

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.otherMap = Multiplayer.BestTargetForWeaponInSplitScreenGame(this.map);
        
        if(this.otherMap == null)
        {
            this.otherMap = this.map;
        }

        if(this.skipInitialSetup == true)
        {
            StartCoroutine(MoveToCurrentPlayerChunk());
        }
        else
        {
            StartCoroutine(BaseballAnimation());

            this.moveDirectly = false;
            this.movingStraight = false;
            this.followingOtherPlayer = false;
            this.followingCurrentPlayer = false;
            this.timer = 0;
            this.distanceFromCenter = 0;
        }
    }

    public override void Advance()
    {
        SwitchToNearestChunk();
        
        if (this.movingStraight == true)
        {
            if(this.skipInitialSetup == true)
            {
                MoveToTargetPlayer();
            }
            else
            {
                MoveToOtherPlayer();
            }
        }
        else if(this.followingOtherPlayer == true)
        {
            FollowingOtherPlayer();
        }
        else if(this.followingCurrentPlayer == true)
        {
            FollowingCurrentPlayer();
        }
    }
    
    private void FollowingOtherPlayer()
    {
        this.timer += Time.fixedDeltaTime;
        this.baseball.transform.position += vel;

        if (this.otherMap.player.alive == true)
        {
            this.baseball.transform.localScale = new Vector3(this.otherMap.player.position.x > this.baseball.transform.position.x ? -1 : 1, 1, 1);
        }

        this.vel = (this.otherMap.player.transform.position - this.baseball.transform.position).normalized / 10;

        if (Vector2.Distance(this.baseball.transform.position, this.otherMap.player.position) < 1.5f)
        {
            this.otherMap.player.Hit(this.position);
            this.followingOtherPlayer = false;
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.baseball.transform.position,
                this.otherMap.player.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 0.6f;

            this.baseball.gameObject.SetActive(false);
        }

        if(this.timer > 15)
        {
            this.followingOtherPlayer = false;
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.baseball.transform.position,
                this.otherMap.player.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 0.6f;

            this.baseball.gameObject.SetActive(false);
        }
    }
    
    private void FollowingCurrentPlayer()
    {
        this.timer += Time.fixedDeltaTime;
        this.baseball.transform.position += vel;
        if (this.otherMap.player.alive)
            this.baseball.transform.localScale = new Vector3(this.otherMap.player.position.x > this.baseball.transform.position.x ? -1 : 1, 1, 1);
        this.vel = (this.otherMap.player.transform.position - this.baseball.transform.position).normalized / 10;

        if (Vector2.Distance(this.baseball.transform.position, this.otherMap.player.position) < 1.5f)
        {
            this.otherMap.player.Hit(this.position);
            this.followingCurrentPlayer = false;
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.baseball.transform.position,
                this.otherMap.player.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 0.6f;

            this.baseball.gameObject.SetActive(false);
        }

        if(this.timer > 15)
        {
            this.followingCurrentPlayer = false;
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.baseball.transform.position,
                this.otherMap.player.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Items (Front)";
            p.spriteRenderer.sortingOrder = 101;
            p.transform.localScale = Vector3.one * 0.6f;

            this.baseball.gameObject.SetActive(false);
        }
    }

    public void MoveToOtherPlayer()
    {
        if (this.moveDirectly == true)
        {
            Vector2 center;
            if (GameCamera.IsLandscape() == true)
            {
                if (this.movingDown == true)
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.8f));
                }
                else
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.2f));
                }
            }
            else
            {
                if (this.movingDown == true)
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.8f));
                }
                else
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.2f));
                }
            }

            float currentMoveSpeed = 17;
            if (Vector2.Distance(this.baseball.transform.position, center) < 1)
            {
                if (distanceFromCenter == 0)
                {
                    distanceFromCenter = Vector2.Distance(this.baseball.transform.position, center);
                }
                currentMoveSpeed = Mathf.Lerp(17, 5, this.speedCurve.Evaluate(Vector2.Distance(this.baseball.transform.position, center) / distanceFromCenter));
            }

            this.baseball.transform.position = Vector2.MoveTowards(
                this.baseball.transform.position, center,
                currentMoveSpeed * Time.deltaTime
            );

            if (Vector2.Distance(this.baseball.transform.position, center) < .5f)
            {
                this.movingStraight = false;
                OpenWings();
            }
        }
        else
        {
            Vector2 moveto;
            this.xPos = this.map.gameCamera.Camera.WorldToViewportPoint(this.baseball.transform.position).x;

            if (this.movingDown == true)
            {
                moveto = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(this.xPos, -0.1f));
            }
            else
            {
                moveto = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(this.xPos, 1.1f));
            }

            this.baseball.transform.position = Vector2.MoveTowards(
                this.baseball.transform.position, moveto,
                17 * Time.deltaTime
            );

            if (Vector2.Distance(this.baseball.transform.position, moveto) < .5f)
            {
                if(this.goToOtherDevice == true)
                {
                    MoveToOtherDevicePlayerChunk();
                }
                else
                {
                    MoveToLocalPlayerChunk();
                }
            }
        }
    }

    public void MoveToTargetPlayer()
    {
        if (this.moveDirectly == true)
        {
            Vector2 center;
            if (GameCamera.IsLandscape() == true)
            {
                if (this.movingDown == true)
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.8f));
                }
                else
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.2f));
                }
            }
            else
            {
                if (this.movingDown == true)
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.8f));
                }
                else
                {
                    center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 0.2f));
                }
            }

            float currentMoveSpeed = 17;
            if (Vector2.Distance(this.baseball.transform.position, center) < 1)
            {
                if (distanceFromCenter == 0)
                {
                    distanceFromCenter = Vector2.Distance(this.baseball.transform.position, center);
                }
                currentMoveSpeed = Mathf.Lerp(17, 5, this.speedCurve.Evaluate(Vector2.Distance(this.baseball.transform.position, center) / distanceFromCenter));
            }

            this.baseball.transform.position = Vector2.MoveTowards(
                this.baseball.transform.position, center,
                currentMoveSpeed * Time.deltaTime
            );

            if (Vector2.Distance(this.baseball.transform.position, center) < .5f)
            {
                this.movingStraight = false;
                OpenWings();
            }
        }
    }

    private void MoveToLocalPlayerChunk()
    {
        this.moveDirectly = true;
        this.baseball.gameObject.layer = 2;
        StartCoroutine(WarningPause());
        this.baseball.transform.SetParent(OtherPlayerMap().transform);
        this.baseball.gameObject.layer = OtherPlayerMap().Layer();
        this.chunk.items.Remove(this);
        this.otherMap.NearestChunkTo(OtherPlayerPosition()).items.Add(this);
    }

    private void MoveToOtherDevicePlayerChunk()
    {
        this.gameObject.SetActive(false);
        this.chunk.items.Remove(this);

        Vector2 playerPosition = this.map.player.position;
        Chunk otherPlayerChunk =
            this.otherMap.NearestChunkTo(OtherPlayerPosition());

        GameCenterMultiplayer.SendMultiplayerPowerup(
            PowerupPickup.Type.MultiplayerBaseball, playerPosition, otherPlayerChunk
        );
    }

    private IEnumerator MoveToCurrentPlayerChunk()
    {
        this.movingDown = OtherPlayerPosition().y > this.map.player.position.y;
        this.xPos = this.map.gameCamera.Camera.WorldToViewportPoint(map.player.transform.position).x;
        float y = 0f;

        var cameraRect = this.map.gameCamera.VisibleRectangle();
        var rect = this.map.gameCamera.VisibleRectangle().Inflate(-2);
        rect.yMin += 0.8f;
        if (GameCamera.IsLandscape() == false)
        {
            rect.yMin += 2.5f;
            rect.yMax -= 3f;
        }

        var camRectTop = cameraRect.yMax;
        var camRectBottom = cameraRect.yMin;

        var rectTop = rect.yMax;
        var rectBottom = rect.yMin;

        if (movingDown == true)
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectTop);
        }
        else
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectBottom);
        }

        this.warningSign = IngameUI.instance.CreateWarningSign(new Vector2(xPos, y), this.movingDown);

        yield return new WaitForSeconds(3f);
        
        IngameUI.instance.HideWarningSign(this.warningSign);
        this.baseball.GetComponent<SpriteRenderer>().sprite = ballLongSprite;
        this.baseball.gameObject.SetActive(true);
        this.moveDirectly = true;
        this.baseball.gameObject.layer = 2;
        
        if (this.movingDown == true)
        {
            this.baseball.transform.position = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 1.1f));
        }
        else
        {
            this.baseball.transform.position = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, -0.1f));
        }
    }
    
    private IEnumerator WarningPause()
    {
        this.movingStraight = false;
        this.baseball.gameObject.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        this.movingStraight = true;
        IngameUI.instance.HideWarningSign(warningSign);
        this.baseball.gameObject.SetActive(true);
        if (this.movingDown == true)
        {
            this.baseball.transform.position = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, 1.1f));
        }
        else
        {
            this.baseball.transform.position = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(xPos, -0.1f));
        }
    }

    private void OpenWings()
    {
        this.baseball.PlayOnce(ballWingsOpenAnimation, () =>
        {
            this.baseball.PlayAndLoop(ballFlyAnimation);
            this.baseball.OnFrame(5, () =>
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxMpPowerupBatFlap,
                    position: this.transform.position
                );
            });
            
            if(this.skipInitialSetup == true)
            {
                this.followingCurrentPlayer = true;
            }
            else
            {
                this.followingOtherPlayer = true;
            }
        });
        Audio.instance.PlaySfx(
            Assets.instance.sfxMpPowerupBatTwirl,
            position: this.transform.position
        );
    }

    private IEnumerator BaseballAnimation()
    {
        if (this.map.player.position.y >= OtherPlayerPosition().y)
        {
            this.movingDown = true;
        }
        else
        {
            this.movingDown = false;
        }

        var tweener = new Tweener();

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Items (Front)";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 0.8f;
        p.animated.OnFrame(4, () =>
        {
            glove.gameObject.SetActive(true);
            glove.transform.position = this.position.Add(0, 1.67f);
        });

        yield return new WaitForSeconds(0.5f);

        this.glove.PlayAndHoldLastFrame(glovesReadyAnimation);
        bool moveGlove = false;
        this.glove.OnFrame(4, () =>
        {
            this.baseball.gameObject.SetActive(true);
            this.baseball.transform.localPosition = new Vector3(0.061f, 0.4f, 0);
            Audio.instance.PlaySfx(
                Assets.instance.sfxMpPowerupBatBallup,
                position: this.transform.position
            );
        });
        this.glove.OnFrame(5, () =>
        {
            this.baseball.GetComponent<SpriteRenderer>().sprite = ballSprite;
            tweener.MoveLocal(baseball.transform, new Vector2(0.061f, 5f), .7f, Easing.QuadEaseOut);
            tweener.ScaleLocal(baseball.transform, new Vector2(.81f, 1.3f), .5f, Easing.QuadEaseOut, 0.1f);
            tweener.MoveLocal(baseball.transform, new Vector2(0.061f, 3.5f), .3f, Easing.CircEaseIn, 0.7f);
            tweener.ScaleLocal(baseball.transform, new Vector2(1, 1), .3f, Easing.QuadEaseOut, 0.6f);
            moveGlove = true;
        });


        while (moveGlove == false)
        {
            yield return null;
        }

        float frames = 0;
        bool batCreated = false;
        bool exit = false;
        while (exit == false)
        {
            frames += 1;
            if(frames > 0.3f && batCreated == false)
            {
                batCreated = true;

                p = Particle.CreateAndPlayOnce(
                    Assets.instance.enemyDeath2Animation,
                    this.position,
                    this.transform
                );
                p.spriteRenderer.sortingLayerName = "Items (Front)";
                p.spriteRenderer.sortingOrder = 101;
                p.transform.localScale = Vector3.one * 0.8f;
                if (movingDown == true)
                {
                    p.transform.localPosition = new Vector3(2.49f, 4.33f, 0);

                }
                else
                {
                    p.transform.localPosition = new Vector3(-1.82f, 4.2f, 0);

                }

                p.animated.OnFrame(5, () => {
                    this.bat.gameObject.SetActive(true);
                    if (movingDown == true)
                    {
                        this.bat.transform.localPosition = new Vector3(1.67f, 4.33f, 0);
                        this.bat.transform.localRotation = Quaternion.Euler(0, 0, -90);
                    }
                    else
                    {
                        this.bat.transform.localPosition = new Vector3(-0.68f, 4.2f, 0);
                        this.bat.transform.localRotation = Quaternion.Euler(0, 0, 90);
                    }
                    this.bat.PlayAndHoldLastFrame(batSmackAnimation);
                    this.bat.OnFrame(3, () =>
                    {
                        Audio.instance.PlaySfx(
                            Assets.instance.sfxMpPowerupBatSwing,
                            position: this.transform.position
                        );
                    });
                    this.bat.OnFrame(5, () =>
                    {
                        p = Particle.CreateAndPlayOnce(
                            smackEffectAnimation,
                            this.position,
                            this.transform
                        );
                        p.spriteRenderer.sortingLayerName = "Items (Front)";
                        p.spriteRenderer.sortingOrder = 101;
                        p.transform.localScale = Vector3.one;
                        if (movingDown == true)
                        {
                            p.transform.localPosition = this.baseball.transform.localPosition + new Vector3(+0.51f, 0f, 0);
                        }
                        else
                        {
                            p.transform.localPosition = this.baseball.transform.localPosition + new Vector3(-0.51f, 0, 0);
                        }
                    });
                    this.bat.OnFrame(6, () =>
                    {
                        this.baseball.GetComponent<SpriteRenderer>().sprite = ballLongSprite;
                        exit = true;
                    });
                });
            }

            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        this.movingStraight = true;

        this.xPos = this.map.gameCamera.Camera.WorldToViewportPoint(this.baseball.transform.position).x;
        float x, y;
        
        var cameraRect = this.map.gameCamera.VisibleRectangle();
        var rect = this.map.gameCamera.VisibleRectangle().Inflate(-2);
        rect.yMin += 0.8f;
        if (GameCamera.IsLandscape() == false)
        {
            rect.yMin += 2.5f;
            rect.yMax -= 3f;
        }

        var camRectTop = cameraRect.yMax;
        var camRectBottom = cameraRect.yMin;

        var rectTop = rect.yMax;
        var rectBottom = rect.yMin;

        if (movingDown == true)
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectTop);
        }
        else
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectBottom);
        }

        if (GameCamera.IsLandscape() == true)
        {
            if (this.map.mapNumber == 1)
            {
                x = 0.5f + (xPos / 2);
            }
            else
            {
                x = (xPos / 2);
            }
        }
        else
        {
            if (this.map.mapNumber == 1)
            {
                x = xPos;
            }
            else
            {
                x = xPos;
            }
        }

        if(this.goToOtherDevice == false)
        {
            this.warningSign = IngameUI.instance.CreateWarningSign(new Vector2(x, y), this.movingDown);
        }

        yield return new WaitForSeconds(1f);

        p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform
        );
        p.spriteRenderer.sortingLayerName = "Items (Front)";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 0.8f;
        if(movingDown == true)
        {
            p.transform.localPosition = new Vector3(1f, 3.28f, 0);
        }
        else
        {
            p.transform.localPosition = new Vector3(-0f, 5.45f, 0);
        }
        p.animated.OnFrame(5, () =>
        {
            this.bat.gameObject.SetActive(false);
        });

        p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform
        );
        p.spriteRenderer.sortingLayerName = "Items (Front)";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 0.8f;
        p.transform.localPosition = new Vector3(0.13f, 0.78f, 0);
        p.animated.OnFrame(5, () =>
        {
            this.glove.gameObject.SetActive(false);
        });
    }
}
