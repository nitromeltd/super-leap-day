using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class PetMine : Item
{
    public Animated visualThisMap;
    public Animated visualOtherMap;

    public Animated.Animation mineIdleAnimation;
    public Animated.Animation mineTriggerAnimation;
    public Animated.Animation ExplodeAnimation;

    private Map otherMap;
    private float timer = 0;

    private bool canExplode;
    private bool hasExploded;
    private bool triggered;

    private float breatheTimer;
    private const float BreathePeriod = .3f;
    private const float BreatheAmplitude = .15f;
    private Vector3 initialPos;

    public static PetMine Create(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petMine, chunk.transform);
        obj.name = "Spawned Pet Mine (Power-up)";
        var item = obj.GetComponent<PetMine>();
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        base.Init(new Def(chunk));
        this.transform.position = this.position = position;
        this.chunk = chunk;
        this.hitboxes = new[] {
            new Hitbox(-.5f, .5f, -.5f, .5f, this.rotation, Hitbox.NonSolid),
        };

        foreach (var otherMap in Game.instance.maps)
        {
            if (otherMap == this.map) continue;

            this.otherMap = otherMap;
        }
        
        if(this.otherMap == null)
        {
            this.otherMap = this.map;
        }

        this.visualThisMap.PlayAndLoop(mineIdleAnimation);
        this.visualOtherMap.PlayAndLoop(mineIdleAnimation);
        this.visualThisMap.gameObject.transform.parent = this.map.player.transform.parent;
        this.visualOtherMap.gameObject.transform.parent = this.otherMap.player.transform.parent;
        this.visualThisMap.gameObject.layer = this.map.mapNumber;
        this.visualOtherMap.gameObject.layer = this.otherMap.mapNumber;

        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Items";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1f;
        p.gameObject.layer = this.map.mapNumber;

        p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Items";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1f;
        p.gameObject.layer = this.otherMap.mapNumber;

        this.hasExploded = false;
        this.canExplode = false;
        this.triggered = false;

        initialPos = this.transform.position;

        Audio.instance.PlaySfx(
            Assets.instance.sfxMpPowerupMineAppear,
            position: this.transform.position
        );
    }

    public override void Advance()
    {
        if (this.hasExploded == true) return;
        this.breatheTimer += Time.deltaTime;
        
        if(this.triggered == false)
        {
            this.triggered = true;
            StartCoroutine(PlayTriggerAnimation());
        }

        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);

        visualThisMap.transform.position = initialPos + this.transform.up * distance;
        visualOtherMap.transform.position = initialPos + this.transform.up * distance;

        this.timer += Time.deltaTime;

        if ((this.canExplode && this.hasExploded == false && this.hitboxes[0].InWorldSpace().Overlaps(this.map.player.Rectangle())) || 
            (this.hitboxes[0].InWorldSpace().Overlaps(this.otherMap.player.Rectangle()) && this.hasExploded == false && this.canExplode) ||
            (this.timer > 20))
        {
            this.hasExploded = true;
            if (this.hitboxes[0].InWorldSpace().Overlaps(otherMap.player.Rectangle()))
            {
                this.otherMap.player.Hit(this.position);
            }
            if (this.hitboxes[0].InWorldSpace().Overlaps(this.map.player.Rectangle()))
            {
                this.map.player.Hit(this.position);
            }
            Particle.CreateAndPlayOnce(ExplodeAnimation, this.transform.position, this.map.player.transform.parent);
            Particle.CreateAndPlayOnce(ExplodeAnimation, this.transform.position, this.otherMap.player.transform.parent);
            this.visualThisMap.gameObject.SetActive(false);
            this.visualOtherMap.gameObject.SetActive(false);
            DestroyMine();
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvExplosion,
                position: this.transform.position
            );
        }

        if(this.map.NearestChunkTo(this.map.player.position) != this.chunk)
        {
            this.chunk.items.Remove(this);
            this.chunk = this.map.NearestChunkTo(this.map.player.position);
            this.chunk.items.Add(this);
        }
    }

    private IEnumerator PlayTriggerAnimation()
    {
        yield return new WaitForSeconds(2.0f);
        this.visualThisMap.PlayAndLoop(mineTriggerAnimation);
        this.visualOtherMap.PlayAndLoop(mineTriggerAnimation);
        this.canExplode = true;
        Audio.instance.PlaySfx(
            Assets.instance.sfxMpPowerupMineTrigger,
            position: this.transform.position
        );
    }

    public void DestroyMine()
    {
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
        Destroy(this.visualOtherMap.gameObject);
        Destroy(this.visualThisMap.gameObject);
    }
}
