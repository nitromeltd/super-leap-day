using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class PetPainter : Item
{
    private Map otherMap;

    public Animated visualThisMap;
    public Animated visualOtherMap;

    public Animated.Animation painterIdleAnimation;
    public Animated.Animation painterSneezeAnimation;

    public Animated.Animation paintSplatAppear1Animation;
    public Animated.Animation paintSplatAppear1ShadowAnimation;
    public Animated.Animation paintSplatFade1Animation;
    public Animated.Animation paintSplatFade1ShadowAnimation;

    public Animated.Animation paintSplatAppear2Animation;
    public Animated.Animation paintSplatAppear2ShadowAnimation;
    public Animated.Animation paintSplatFade2Animation;
    public Animated.Animation paintSplatFade2ShadowAnimation;

    public Animated.Animation paintSplatAppear3Animation;
    public Animated.Animation paintSplatAppear3ShadowAnimation;
    public Animated.Animation paintSplatFade3Animation;
    public Animated.Animation paintSplatFade3ShadowAnimation;

    public Animated.Animation paintSplatAppear4Animation;
    public Animated.Animation paintSplatAppear4ShadowAnimation;
    public Animated.Animation paintSplatFade4Animation;
    public Animated.Animation paintSplatFade4ShadowAnimation;

    public Animated.Animation paintSplatParticleAnimation;
    public Animated.Animation SmackParticleAnimation;

    private bool moving = true;
    private bool hasPainted = false;
    private bool moveDirectly = false;
    private float timer = 0;
    private bool goToOtherDevice = false;
    private bool skipInitialSetup = false;
    
    public Vector3 OtherPlayerPosition()
    {
        if(this.map.ghostPlayers != null && this.map.ghostPlayers.Count > 0)
        {
            return this.map.ghostPlayers[0].transform.position;
        }
        
        return this.otherMap.player.position;
    }

    public static PetPainter Create(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petPainter, chunk.transform);
        obj.name = "Spawned Pet Painter (Power-up)";
        var item = obj.GetComponent<PetPainter>();
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public static PetPainter CreateP1(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petPainter, chunk.transform);
        obj.name = "Spawned Pet Painter (Power-up) for P1";
        var item = obj.GetComponent<PetPainter>();
        item.goToOtherDevice = true;
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public static PetPainter CreateP2(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        position = Map.instance.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 1.1f));
        chunk = Map.instance.NearestChunkTo(Map.instance.player.position);
        
        var obj = Instantiate(Assets.instance.petPainter, chunk.transform);
        obj.name = "Spawned Pet Painter (Power-up) for P2";
        var item = obj.GetComponent<PetPainter>();
        item.skipInitialSetup = true;
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        base.Init(new Def(chunk));
        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.otherMap = Multiplayer.BestTargetForWeaponInSplitScreenGame(this.map);

        if(this.otherMap == null)
        {
            this.otherMap = this.map;
        }

        if(this.skipInitialSetup == false)
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.position,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Pickups Above UI";
            p.spriteRenderer.sortingOrder = 12;
            p.transform.localScale = Vector3.one * 1.4f;
            p.gameObject.layer = this.map.mapNumber;

            p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyDeath2Animation,
                this.position,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Pickups Above UI";
            p.spriteRenderer.sortingOrder = 12;
            p.transform.localScale = Vector3.one * 1.4f;
            p.gameObject.layer = this.otherMap.mapNumber;
        }

        this.visualThisMap.PlayAndLoop(this.painterIdleAnimation);
        this.visualOtherMap.PlayAndLoop(this.painterIdleAnimation);
        this.visualThisMap.gameObject.layer = this.map.mapNumber;
        this.visualOtherMap.gameObject.layer = this.otherMap.mapNumber;

        if (this.map.NearestChunkTo(this.map.player.position).chunkName == this.otherMap.NearestChunkTo(OtherPlayerPosition()).chunkName || 
            Vector2.Distance(this.map.player.position, OtherPlayerPosition()) < 30f)
        {
            if(this.goToOtherDevice == false && this.skipInitialSetup == false)
            {
                this.moveDirectly = true;
            }
        }

        if(this.skipInitialSetup == true)
        {
            StartCoroutine(MoveToCurrentPlayerChunk());
        }
    }

    private IEnumerator MoveToCurrentPlayerChunk()
    {
        bool movingDown = OtherPlayerPosition().y > this.map.player.position.y;
        Vector2 initialPosition = movingDown == true ?
            (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 1.1f)) :
            (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, -0.1f));
        this.transform.position = initialPosition;

        float y;

        var cameraRect = this.map.gameCamera.VisibleRectangle();
        var rect = this.map.gameCamera.VisibleRectangle().Inflate(-2);
        rect.yMin += 0.8f;
        if (GameCamera.IsLandscape() == false)
        {
            rect.yMin += 2.5f;
            rect.yMax -= 3f;
        }

        var camRectTop = cameraRect.yMax;
        var camRectBottom = cameraRect.yMin;

        var rectTop = rect.yMax;
        var rectBottom = rect.yMin;

        if (movingDown == true)
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectTop);
        }
        else
        {
            y = Mathf.InverseLerp(camRectBottom, camRectTop, rectBottom);
        }

        RectTransform warningSign = IngameUI.instance.CreateWarningSign(new Vector2(0.5f, y), movingDown);

        yield return new WaitForSeconds(3f);
        
        IngameUI.instance.HideWarningSign(warningSign);

        this.moveDirectly = true;
    }

    public override void Advance()
    {
        SwitchToNearestChunk();
        
       if(this.moving == true)
       {
           MoveToOtherPlayer();
       }
       else
       {
           AdvancePaint();
       }
    }

    public void MoveToOtherPlayer()
    {
        if(moveDirectly == true)
        {
            Vector2 center;
            if (GameCamera.IsLandscape() == true)
            {
                center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 0.7f));
            }
            else
            {
                center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 0.8f));
            }

            this.transform.position = Vector2.MoveTowards(
                this.transform.position, center,
                7 * Time.deltaTime
            );
            this.position = transform.position;
            if (Vector2.Distance(this.transform.position, center) < .5f)
            {
                this.moving = false;
            }
        }
        else if(this.skipInitialSetup == false)
        {
            Vector2 moveto;
            if (this.map.player.position.y > OtherPlayerPosition().y)
            {
                moveto = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, -0.1f));
            }
            else
            {
                moveto = (Vector2)this.map.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 1.1f));
            }

            this.transform.position = Vector2.MoveTowards(
                this.transform.position, moveto,
                7 * Time.deltaTime
            );
            this.position = transform.position;
            if (Vector2.Distance(this.transform.position, moveto) < .5f)
            {
                if(this.goToOtherDevice == true)
                {
                    this.gameObject.SetActive(false);
                    this.goToOtherDevice = false;

                    GameCenterMultiplayer.SendMultiplayerPowerup(
                        PowerupPickup.Type.MultiplayerInk, position, chunk
                    );
                }
                else
                {
                    MovedOutOfScreen();
                }
            }
        }
    }

    private void MovedOutOfScreen()
    {
        this.moveDirectly = true;
        if (this.map.player.position.y > OtherPlayerPosition().y)
        {
            this.transform.position = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 1.1f));
        }
        else
        {
            this.transform.position = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, -0.1f));
        }

        this.position = this.transform.position;

        if(this.otherMap != this.map)
        {
            this.map = this.otherMap;
        }
    }

    private void AdvancePaint()
    {
        this.timer += Time.fixedDeltaTime;

        if(this.hasPainted == false && this.timer > 0.5f)
        {
            this.hasPainted = true;
            this.visualThisMap.PlayOnce(this.painterSneezeAnimation, () =>
            {
                visualThisMap.PlayAndLoop(this.painterIdleAnimation);
            });
            Audio.instance.PlaySfx(
                Assets.instance.sfxMpPowerupPaintSneeze,
                position: this.transform.position
            );

            this.visualOtherMap.PlayOnce(this.painterSneezeAnimation, () =>
            {
                this.visualOtherMap.PlayAndLoop(this.painterIdleAnimation);
            });

            this.visualThisMap.OnFrame(13, () =>
            {
                SplatterPaint();
            });
        }

        if (this.timer > 7f)
        {
            Dissappear();
        }
    }

    public void SplatterPaint()
    {
        Vector2 center;
        int splatterNumber;
        if (GameCamera.IsLandscape() == true)
        {
            center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 0.4f));
            splatterNumber = Rnd.Range(75, 85);
            for (int i = 0; i < splatterNumber; i++)
            {
                CreateSplatter(new Vector2(center.x + Rnd.Range(-7.0f, 7.0f), center.y + Rnd.Range(-4.0f, 25.0f) - 1), 2 * i);
            }
        }
        else
        {
            center = (Vector2)this.otherMap.gameCamera.Camera.ViewportToWorldPoint(new Vector2(0.5f, 0.4f));
            splatterNumber = Rnd.Range(75, 85);
            for (int i = 0; i < splatterNumber; i++)
            {
                CreateSplatter(new Vector2(center.x + Rnd.Range(-7.0f, 7.0f), center.y + Rnd.Range(-6.0f, 27.0f) - 1), 2 * i);
            }
        }
    }

    public void CreateSplatter(Vector2 position, int z)
    {
        Chunk splatterChunk = this.otherMap == this.map ?
            this.map.NearestChunkTo(position) : this.otherMap.NearestChunkTo(OtherPlayerPosition());
        PaintSplatter paintSplatter = PaintSplatter.Create(position, splatterChunk);
        paintSplatter.transform.position = new Vector3(paintSplatter.transform.position.x, paintSplatter.transform.position.y, 40 - z);
        Particle p = Particle.CreateAndPlayOnce(
            this.SmackParticleAnimation,
            position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Pickups Above UI";
        p.spriteRenderer.sortingOrder = 11;
        p.transform.localScale = Vector3.one * 2f;
        p.gameObject.layer = otherMap.mapNumber;

        int animationNumber = Rnd.Range(0, 4);

        switch (animationNumber)
        {
            case 0:
                paintSplatter.appearAnimation       = this.paintSplatAppear1Animation;
                paintSplatter.appearShadowAnimation = this.paintSplatAppear1ShadowAnimation;
                paintSplatter.fadeAnimation         = this.paintSplatFade1Animation;
                paintSplatter.fadeShadowAnimation   = this.paintSplatFade1ShadowAnimation;
                paintSplatter.highlight1.SetActive(true);
                break;
            case 1:
                paintSplatter.appearAnimation       = this.paintSplatAppear2Animation;
                paintSplatter.appearShadowAnimation = this.paintSplatAppear2ShadowAnimation;
                paintSplatter.fadeAnimation         = this.paintSplatFade2Animation;
                paintSplatter.fadeShadowAnimation   = this.paintSplatFade2ShadowAnimation;
                paintSplatter.highlight2.SetActive(true);
                break;
            case 2:
                paintSplatter.appearAnimation       = this.paintSplatAppear3Animation;
                paintSplatter.appearShadowAnimation = this.paintSplatAppear3ShadowAnimation;
                paintSplatter.fadeAnimation         = this.paintSplatFade3Animation;
                paintSplatter.fadeShadowAnimation   = this.paintSplatFade3ShadowAnimation;
                paintSplatter.highlight3.SetActive(true);
                break;
            case 3:
                paintSplatter.appearAnimation       = this.paintSplatAppear4Animation;
                paintSplatter.appearShadowAnimation = this.paintSplatAppear4ShadowAnimation;
                paintSplatter.fadeAnimation         = this.paintSplatFade4Animation;
                paintSplatter.fadeShadowAnimation   = this.paintSplatFade4ShadowAnimation;
                paintSplatter.highlight3.SetActive(true);
                break;
        }

        for(int i = 0 ; i < 3 ; i++)
        {
            Vector2 dir = new Vector2(Rnd.Range(-1.0f, 1.0f), Rnd.Range(-1.0f, 1.0f));

            p = Particle.CreateAndPlayOnce(
                paintSplatParticleAnimation,
                position + dir,
                this.transform.parent
            );
            p.spriteRenderer.sortingLayerName = "Pickups Above UI";
            p.spriteRenderer.sortingOrder = 12;
            p.transform.localScale = Vector3.one * 1.5f;
            p.gameObject.layer = otherMap.mapNumber;
            p.velocity = dir * 0.2f;
        }
    }

    public void Dissappear()
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Pickups Above UI";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1.4f;
        p.gameObject.layer = this.map.mapNumber;

        p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Pickups Above UI";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1.4f;
        p.gameObject.layer = this.otherMap.mapNumber;

        Audio.instance.PlaySfx(
            Assets.instance.sfxMpPowerupPaintDisappear,
            position: this.transform.position
        );
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }
}
