using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetSword : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation rotateAnimation;
    public Animated.Animation slashAnimation;
    public Animated.Animation topBroken;
    public Animated.Animation BottomBroken;

    // Target
    [NonSerialized] public Item itemTarget;
    private bool usePlayerPosition;

    private const float MaxTargetDistance = 10f;
    private const float MaxPlayerDistance = 22f;

    private float ySmoothVelocity;
    private bool spriteInFront;
    private Vector2 initialLaunchTarget;

    public enum State
    {
        INIT,
        GO_TO_PLAYER,
        FOLLOW_PLAYER,
        LAUNCH,
        MOVE_TARGET,
        ATTACK_TARGET,
        RETURN_TO_PLAYER,
    }

    private State state = State.INIT;
    private List<Enemy> enemiesToIgnore = new List<Enemy>();

    public AnimationCurve speedCurve;
    private float launchTimer = 0;

    private bool isHidden = false;

    public static PetSword Create(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petSword, chunk.transform);
        obj.name = "Spawned Sword Pet (Power-up)";
        var item = obj.GetComponent<PetSword>();
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        base.Init(new Def(chunk));

        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, false, false)
        };

        if (activatedByBubble == false)
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );
            p.transform.localScale = Vector3.one * 0.5f;
        }

        ChangeState(State.FOLLOW_PLAYER);
        this.position = this.map.player.position;
        this.transform.position = this.map.player.position;

        this.spriteInFront = false;
        this.ySmoothVelocity = 0f;
        this.launchTimer = 0f;
        this.initialLaunchTarget = Vector2.zero;
    }

    public override void Advance()
    {
        if (this.isHidden == true)
            return;
        SwitchToNearestChunk();

        switch (state)
        {
            case State.FOLLOW_PLAYER:
                AdvanceFollowPlayerState();
                break;

            case State.LAUNCH:
                AdvanceLaunchState();
                break;

            case State.MOVE_TARGET:
                AdvanceMoveTargetState();
                break;

            case State.ATTACK_TARGET:
                AdvanceAttackTargetState();
                break;

            case State.GO_TO_PLAYER:
                AdvanceGoToPlayerState();
                break;
        }

        this.transform.position = this.position;

        if(this.map.player.swordAndShield.isActive == false)
        {
            DestroyPet();
        }
    }

    private void AdvanceFollowPlayerState()
    {
        if (this.map.player.swordAndShield.isActive == false)
            return;

        float extraTime;
        if (this.map.player.shield.isActive == false)
            extraTime = ((PlayerSwordAndShieldEffect.Period * 6f) / (this.map.player.swordAndShield.playerShieldEffect.currentShields.Count +
                this.map.player.swordAndShield.petSwords.Count)) * (this.map.player.swordAndShield.playerShieldEffect.currentShields.Count + this.map.player.swordAndShield.petSwords.IndexOf(this));
        else
            extraTime = ((PlayerSwordAndShieldEffect.Period * 6f) / (this.map.player.swordAndShield.playerShieldEffect.currentShields.Count + 
                this.map.player.shield.currentActiveShields + this.map.player.swordAndShield.petSwords.Count)) * 
                (this.map.player.swordAndShield.playerShieldEffect.currentShields.Count + this.map.player.shield.currentActiveShields + this.map.player.swordAndShield.petSwords.IndexOf(this));

        float finalTimer = this.map.player.swordAndShield.playerShieldEffect.timer + extraTime;
        Move(this.map.player.swordAndShield.playerShieldEffect.transform.position, finalTimer, 90f);
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
        CheckForTarget();
    }

    public Vector2 Move(Vector3 center, float timer, float angle, bool? returnValue = false)
    {
        float theta = timer / PlayerSwordAndShieldEffect.Period;
        float distance = PlayerSwordAndShieldEffect.Amplitude * Mathf.Sin(theta);

        Vector2 targetPos = center +
            ((Quaternion.AngleAxis(-angle, Vector3.forward) * Vector3.up) * distance);

        float ySmoothTargetPos = Mathf.SmoothDamp(
            this.transform.position.y,
            targetPos.y,
            ref ySmoothVelocity,
            .05f
        );

        Vector2 smoothTargetPos = new Vector2(targetPos.x, ySmoothTargetPos);

        bool shouldUseSmoothing = Vector2.Distance(this.transform.position, targetPos) > 0.1f;

        if(returnValue == true)
        {
            return shouldUseSmoothing ? smoothTargetPos : targetPos;
        }

        this.position = shouldUseSmoothing ? smoothTargetPos : targetPos;

        if (this.spriteInFront == true && distance > PlayerSwordAndShieldEffect.Amplitude - 0.1f)
        {
            this.spriteInFront = false;
        }

        if (this.spriteInFront == false && distance < -PlayerSwordAndShieldEffect.Amplitude + 0.1f)
        {
            this.spriteInFront = true;
        }

        this.spriteRenderer.sortingLayerName = this.spriteInFront == true ? "Player (AL)" : "Player (BL)";
        this.spriteRenderer.sortingOrder = this.spriteInFront == true ? 10 : -10;

        return position;
    }

    private void AdvanceLaunchState()
    {
        this.launchTimer += Time.deltaTime;
        this.position = Vector3.Lerp(this.position, this.initialLaunchTarget, speedCurve.Evaluate(launchTimer * 1.5f));

        if (Vector2.Distance(this.position, this.initialLaunchTarget) < .01f)
        {
            ChangeState(State.MOVE_TARGET);
        }
    }

    private void AdvanceMoveTargetState()
    {
        if (IsValidTarget(this.itemTarget) == false)
        {
            ChangeState(State.GO_TO_PLAYER);
            return;
        }

        MoveToTarget();

        if (Vector2.Distance(this.position, this.itemTarget.transform.position - new Vector3(1, 0, 0)) < .01f)
        {
            ChangeState(State.ATTACK_TARGET);
        }

        Vector2 targetRot = itemTarget.position;
        targetRot.x = targetRot.x - this.position.x;
        targetRot.y = targetRot.y - this.position.y;

        float angle = Mathf.Atan2(targetRot.y, targetRot.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 2);
    }

    private void AdvanceAttackTargetState()
    {
        if (IsValidTarget(itemTarget) == false)
        {
            ChangeState(State.GO_TO_PLAYER);
            return;
        }

        MoveToTarget();

        Vector2 targetRot = itemTarget.position;
        targetRot.x = targetRot.x - this.position.x;
        targetRot.y = targetRot.y - this.position.y;

        float angle = Mathf.Atan2(targetRot.y, targetRot.x) * Mathf.Rad2Deg;
        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, angle - 90));
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 2);
    }

    private void MoveToTarget()
    {
        this.position = Vector2.MoveTowards(
            this.position, this.itemTarget.transform.position - new Vector3(1, 0, 0),
            17 * Time.deltaTime
        );

        Vector2 heading = (Vector2)this.itemTarget.transform.position - this.position;
    }

    private void AdvanceGoToPlayerState()
    {
        this.position = Vector2.MoveTowards(
            this.position, this.map.player.transform.position,
            17 * Time.deltaTime
        );

        Quaternion targetRotation = Quaternion.Euler(new Vector3(0, 0, 0));
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, 5);

        if (Vector2.Distance(this.position, this.map.player.transform.position) < .8f)
        {
            ChangeState(State.FOLLOW_PLAYER);
        }
    }

    private void CheckForTarget()
    {
        IEnumerable<Chunk> chunksWithinRange =
            this.map.ChunksWithinDistance(this.transform.position, MaxTargetDistance);

        float minDistance = MaxTargetDistance;

        Vector2 myPosition = this.usePlayerPosition ?
            this.map.player.position : this.position;

        foreach (Chunk currentChunk in chunksWithinRange)
        {
            foreach (var item in currentChunk.items)
            {
                if (item.gameObject.activeSelf == false) continue;
                if (item.position.y < this.position.y + 1) continue;
                if (IsValidTarget(item) == false) continue;

                float distanceToItem = Vector2.Distance(myPosition, item.position);

                if (distanceToItem < minDistance)
                {
                    minDistance = distanceToItem;
                    this.itemTarget = item;
                }
            }
        }

        if (this.itemTarget)
        {
            ChangeState(State.LAUNCH);
        }
    }

    private bool IsValidTarget(Item item)
    {
        if (item == null) return false;

        if (item is Enemy)
        {
            if (enemiesToIgnore.Find(x => x == (Enemy)item) != null)
            {
                return false;
            }

            if (item is WildTrunkyBone) return false;
            if (item is HelmutHelmet) return false;
            if (item is TrunkyBullet) return false;
            if (item is Bully) return false;

            if (item is ChestMimic &&
                ((item as ChestMimic).state == ChestMimic.State.IdleClose ||
                (item as ChestMimic).state == ChestMimic.State.Uncover ||
                (item as ChestMimic).state == ChestMimic.State.Dizzy))
            {
                return false;
            }

            if((item as Enemy).becameGold == true)
            {
                return false;
            }

            if (this.map.player.petYellow.isActive == true)
            {
                for (int i = this.map.player.petYellow.yellowPets.Count; i > 0; i--)
                {
                    if (item == this.map.player.petYellow.yellowPets[i - 1].itemTarget)
                    {
                        return false;
                    }
                }
            }

            for (int i = this.map.player.swordAndShield.petSwords.Count; i > 0; i--)
            {
                if(this.map.player.swordAndShield.petSwords[i - 1] != this)
                {
                    if (item == this.map.player.swordAndShield.petSwords[i - 1].itemTarget)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        if (item is Pickup)
        {
            return false;
        }

        return false;
    }

    private bool IsPlayerCloseEnough()
    {
        float distanceToPlayer =
            Vector2.Distance(this.map.player.position, this.position);

        return distanceToPlayer < MaxPlayerDistance;
    }

    private void ChangeState(State newState)
    {
        if (this.state == newState) return;

        this.state = newState;

        switch (state)
        {
            case State.FOLLOW_PLAYER:
                EnterFollowPlayerState();
                break;

            case State.LAUNCH:
                EnterLaunchState();
                break;

            case State.MOVE_TARGET:
                EnterMoveTargetState();
                break;

            case State.ATTACK_TARGET:
                EnterAttackTargetState();
                break;

            case State.GO_TO_PLAYER:
                EnterGoToPlayer();
                break;
        }
    }

    private void EnterFollowPlayerState()
    {
        this.itemTarget = null;
        this.usePlayerPosition = IsPlayerCloseEnough() == false;

        this.animated.PlayAndLoop(this.rotateAnimation);
    }

    private void EnterLaunchState()
    {
        this.spriteRenderer.sortingLayerName = "Player (AL)";
        this.animated.PlayAndLoop(this.idleAnimation);
        this.initialLaunchTarget = new Vector2(this.position.x, this.map.player.position.y - 1.5f);
        this.launchTimer = 0f;
    }

    private void EnterGoToPlayer()
    {
        this.animated.PlayAndLoop(this.idleAnimation);
    }

    private void EnterMoveTargetState()
    {
        this.animated.PlayAndLoop(this.idleAnimation);
    }

    private void EnterAttackTargetState()
    {
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, 40));

        this.animated.PlayOnce(this.slashAnimation, () =>
        {
            ChangeState(State.GO_TO_PLAYER);
        });

        this.animated.OnFrame(7, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxPowerupSwordAttack);
        });

        this.animated.OnFrame(10, () =>
        {
            AttackTarget();
        });
    }

    private void AttackTarget()
    {
        if (this.itemTarget is Enemy)
        {
            (this.itemTarget as Enemy).Kill(new Enemy.KillInfo { itemDeliveringKill = this });
        }
        else if (this.itemTarget is Pickup)
        {
            (this.itemTarget as Pickup).Collect();
        }
    }

    public void AddEnemyToIgnoreList(Enemy enemy)
    {
        enemiesToIgnore.Add(enemy);
    }

    public void DestroyPet()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxPowerupShieldBreak);

        // broken sword particle debris
        var top = Particle.CreatePlayAndHoldLastFrame(
            this.topBroken, 100, this.position, this.transform.parent
        );
        top.velocity = new Vector2(-1f, 7f) / 16f;
        top.acceleration = new Vector2(0f, -0.5f) / 16f;

        var bottom = Particle.CreatePlayAndHoldLastFrame(
            this.BottomBroken, 100, this.position, this.transform.parent
        );
        bottom.velocity = new Vector2(1f, 7f) / 16f;
        bottom.acceleration = new Vector2(0f, -0.5f) / 16f;
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }

    public void Hide()
    {
        this.isHidden = true;
        this.spriteRenderer.enabled = false;
    }

    public void Show()
    {
        this.isHidden = false;
        this.spriteRenderer.enabled = true;
        this.state = State.FOLLOW_PLAYER;
        EnterFollowPlayerState();
    }
}
