﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetYellow : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation flyFastAnimation;
    public Animated.Animation chompAnimation;

    // Movement
    private bool facingRight;
    private Vector2 smoothVelocity;

    // Target
    [NonSerialized] public Item itemTarget;
    private bool usePlayerPosition;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;

    private const float BreathePeriod = 0.5f;
    private const float BreatheAmplitude = 0.25f;
    private const float FollowSmoothTime = 0.4f;
    private const float MaxTargetDistance = 10f;
    private const float MoveToTargetSpeed = 10f;
    private const float MaxPlayerDistance = 22f;

    // Hearts
    public Animated.Animation heartSolidAnimation;
    public Animated.Animation heartDisappearAnimation;
    public Animated.Animation heartAppearAnimation;
    private Transform heartsParent;
    private Transform heart1;
    private Transform heart3;
    private Transform heart2;

    bool stopMoving = false;

    public enum State
    {
        INIT,
        FOLLOW_PLAYER,
        MOVE_TARGET,
        ATTACK_TARGET
    }

    private State state = State.INIT;
    private List<Enemy> enemiesToIgnore = new List<Enemy>();

    public static PetYellow Create(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        var obj = Instantiate(Assets.instance.petYellow, chunk.transform);
        obj.name = "Spawned Yellow Pet (Power-up)";
        var item = obj.GetComponent<PetYellow>();
        item.Init(position, chunk, activatedByBubble);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, bool activatedByBubble = false)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, false, false)
        };

        if (activatedByBubble == false)
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );
            p.transform.localScale = Vector3.one * 0.5f;
        }
        
        this.facingRight = true;
        this.breatheMovement = false;

        ChangeState(State.FOLLOW_PLAYER);

        heartsParent = this.transform.Find("Hearts").transform;
        heart1 = this.transform.Find("Hearts/1").transform;
        heart2 = this.transform.Find("Hearts/2").transform;
        heart3 = this.transform.Find("Hearts/3").transform;
        heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);

        if (this.map.player.petYellow.hearts == 3)
        {

        }
        else if (this.map.player.petYellow.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
        }
        else if (this.map.player.petYellow.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
        }
        else
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(false);
        }

        heartsParent.gameObject.SetActive(false);
    }

    public override void Advance()
    {
        if (this.stopMoving == true)
            return;
        SwitchToNearestChunk();
        
        // breathe movement
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);
        Vector2 breathePos = this.breatheMovement ? Vector2.up * distance : Vector2.zero;
        
        switch(state)
        {
            case State.FOLLOW_PLAYER:
                AdvanceFollowPlayerState();
            break;

            case State.MOVE_TARGET:
                AdvanceMoveTargetState();
            break;

            case State.ATTACK_TARGET:
                AdvanceAttackTargetState();
            break;
        }
        
        this.transform.position = this.position + breathePos;
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
        if(facingRight == true)
        {
            heartsParent.localScale = new Vector3(1, 1, 1);
        }
        else
        {
            heartsParent.localScale = new Vector3(-1, 1, 1);
        }

        if (this.map.player.alive == false)
        {
            DestroyPet();
        }
    }

    private void AdvanceFollowPlayerState()
    {
        int petYellowIndex = this.map.player.petYellow.yellowPets.IndexOf(this);
        Vector2 followPlayerPos = this.map.player.PetYellowFollowPosition(petYellowIndex);
        this.position = Vector2.SmoothDamp(this.position, followPlayerPos,
            ref this.smoothVelocity, FollowSmoothTime);
            
        this.facingRight = (this.smoothVelocity.x > 0f);

        CheckForTarget();
    }

    private void AdvanceMoveTargetState()
    {
        if(IsValidTarget(this.itemTarget) == false)
        {
            ChangeState(State.FOLLOW_PLAYER);
            return;
        }

        MoveToTarget();
        
        if(Vector2.Distance(this.position, this.itemTarget.transform.position) < .01f)
        {
            ChangeState(State.ATTACK_TARGET);
        }
    }

    private void AdvanceAttackTargetState()
    {
        if(IsValidTarget(itemTarget) == false)
        {
            ChangeState(State.FOLLOW_PLAYER);
            return;
        }

        MoveToTarget();
    }

    private void MoveToTarget()
    {
        this.position = Vector2.MoveTowards(
            this.position, this.itemTarget.transform.position,
            MoveToTargetSpeed * Time.deltaTime
        );
        
        Vector2 heading = (Vector2)this.itemTarget.transform.position - this.position;

        if(heading.x != 0f)
        {
            this.facingRight = (heading.x > 0f);
        }
    }

    private void CheckForTarget()
    {
        IEnumerable<Chunk> chunksWithinRange =
            this.map.ChunksWithinDistance(this.transform.position, MaxTargetDistance);

        float minDistance = MaxTargetDistance;

        Vector2 myPosition = this.usePlayerPosition ?
            this.map.player.position : this.position;

        foreach(Chunk currentChunk in chunksWithinRange)
        {
            foreach (var item in currentChunk.items)
            {
                if(item.gameObject.activeSelf == false) continue;
                if (IsValidTarget(item) == false) continue;

                float distanceToItem = Vector2.Distance(myPosition, item.position);

                if(distanceToItem < minDistance)
                {
                    minDistance = distanceToItem;
                    this.itemTarget = item;
                }
            }
        }

        if(this.itemTarget)
        {
            ChangeState(State.MOVE_TARGET);
        }
    }

    private bool IsValidTarget(Item item)
    {
        if(item == null) return false;

        if (item is Enemy)
        {
            if(enemiesToIgnore.Find(x => x == (Enemy)item) != null)
            {
                return false;
            }

            if(item is WildTrunkyBone) return false;
            if(item is HelmutHelmet) return false;
            if(item is TrunkyBullet) return false;
            if(item is Bully) return false;

            if(item is ChestMimic &&
                ((item as ChestMimic).state == ChestMimic.State.IdleClose ||
                (item as ChestMimic).state == ChestMimic.State.Uncover ||
                (item as ChestMimic).state == ChestMimic.State.Dizzy))
            {
                return false;
            }

            if ((item as Enemy).becameGold == true)
            {
                return false;
            }

            if (this.map.player.swordAndShield.isActive == true)
            {
                for (int i = this.map.player.swordAndShield.petSwords.Count; i > 0; i--)
                {
                    if (item == this.map.player.swordAndShield.petSwords[i - 1].itemTarget)
                    {
                        return false;
                    }
                }
            }

            for (int i = this.map.player.petYellow.yellowPets.Count; i > 0; i--)
            {
                if (this.map.player.petYellow.yellowPets[i - 1] != this)
                {
                    if (item == this.map.player.petYellow.yellowPets[i - 1].itemTarget)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        if(item is Pickup)
        {
            if((item as Pickup).collected == true) return false;
            if(item is PowerupPickup) return false;
            if(item is BubblePickup) return false;
            if (item is ABCoin)
            {
                if ((item as ABCoin).isSolid == false)
                {
                    return false;
                }
            }
            if (item is TimerCoin)
            {
                if ((item as TimerCoin).isActive == false || (item as TimerCoin).collected == true)
                {
                    return false;
                }
            }

            if(item is PowerupHeart)
            {
                return false;
            }

            for (int i = this.map.player.petYellow.yellowPets.Count; i > 0; i--)
            {
                if (this.map.player.petYellow.yellowPets[i - 1] != this)
                {
                    if (item == this.map.player.petYellow.yellowPets[i - 1].itemTarget)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        return false;
    }

    private bool IsPlayerCloseEnough()
    {
        float distanceToPlayer =
            Vector2.Distance(this.map.player.position, this.position);

        return distanceToPlayer < MaxPlayerDistance;
    }

    private void ChangeState(State newState)
    {
        if(this.state == newState) return;

        this.state = newState;

        switch(state)
        {
            case State.FOLLOW_PLAYER:
                EnterFollowPlayerState();
            break;

            case State.MOVE_TARGET:
                EnterMoveTargetState();
            break;

            case State.ATTACK_TARGET:
                EnterAttackTargetState();
            break;
        }
    }

    private void EnterFollowPlayerState()
    {
        this.breatheMovement = true;
        this.itemTarget = null;
        this.usePlayerPosition = IsPlayerCloseEnough() == false;

        this.animated.PlayAndLoop(this.idleAnimation);
    }

    private void EnterMoveTargetState()
    {
        this.breatheMovement = false;
        this.animated.PlayAndLoop(this.flyFastAnimation);
    }

    private void EnterAttackTargetState()
    {
        this.breatheMovement = false;
        this.animated.PlayOnce(this.chompAnimation, () =>
        {
            ChangeState(State.FOLLOW_PLAYER);
        });

        this.animated.OnFrame(4, () => 
        {
            AttackTarget();
            Audio.instance.PlaySfx(Assets.instance.sfxPowerupYellowpetChomp);
        });
    }

    private void AttackTarget()
    {
        if(this.itemTarget is Enemy)
        {
            // to-do: should use Hit instead of Kill?
            (this.itemTarget as Enemy).Kill(new Enemy.KillInfo { itemDeliveringKill = this });
        }
        else if(this.itemTarget is Pickup)
        {
            (this.itemTarget as Pickup).Collect();
        }
    }

    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawSphere(myPosition, .2f);
    }*/

    public void AddEnemyToIgnoreList(Enemy enemy)
    {
        enemiesToIgnore.Add(enemy);
    }


    public void LoseHeart()
    {
        StartCoroutine(_LoseHeart());
    }

    public IEnumerator _LoseHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.map.player.petYellow.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.map.player.petYellow.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.map.player.petYellow.hearts == 0)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.5f);

        this.stopMoving = true;
        Vector3 originalPosition;
        if (this.map.player.petYellow.hearts == 2)
        {
            heart2.gameObject.SetActive(true);
            heart1.gameObject.SetActive(true);
            originalPosition = heart3.transform.localPosition;
            heart3.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart3.transform.localPosition = originalPosition;
                heart3.gameObject.SetActive(false);
            });
        }
        else if (this.map.player.petYellow.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            originalPosition = heart2.transform.localPosition;
            heart2.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart2.transform.localPosition = originalPosition;
                heart2.gameObject.SetActive(false);
            });
        }
        else if (this.map.player.petYellow.hearts == 0)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            originalPosition = heart1.transform.localPosition;
            heart1.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart1.transform.localPosition = originalPosition;
                heart1.gameObject.SetActive(false);
            });
        }
        float timer = 0;
        while (timer < .5f)
        {
            timer += Time.deltaTime;
            if (this.map.player.petYellow.hearts == 2)
            {
                heart3.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.map.player.petYellow.hearts == 1)
            {
                heart2.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.map.player.petYellow.hearts == 0)
            {
                heart1.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            yield return null;
        }

        this.stopMoving = false;

        yield return new WaitForSeconds(.5f);

        heartsParent.gameObject.SetActive(false);
    }

    public void GainHeart()
    {
        StartCoroutine(_GainHeart());
    }

    public IEnumerator _GainHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.map.player.petYellow.hearts == 3)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.map.player.petYellow.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.map.player.petYellow.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.25f);
        this.stopMoving = true;

        if (this.map.player.petYellow.hearts == 3)
        {
            heart3.gameObject.SetActive(true);
            heart3.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.map.player.petYellow.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.map.player.petYellow.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
        }

        yield return new WaitForSeconds(.5f);
        this.stopMoving = false;
        yield return new WaitForSeconds(1f);
        heartsParent.gameObject.SetActive(false);
    }

    public void DestroyPet()
    {
        Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.position,
                this.transform.parent
            );
        p.transform.localScale = Vector3.one * 0.5f;
        this.map.player.petYellow.needsHeart = false;
        this.map.player.petYellow.yellowPets.Remove(this);
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }
}
