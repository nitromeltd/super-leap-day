
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;

public class Pickup : Item
{
    public enum LuckyDrop
    {
        None,

        SilverCoin,
        GoldCoin,
        PowerupHeart,
        PowerupInvincibility,
        PowerupShield,
        PowerupYellowPet,
        PowerupTornadoPet,
        PowerupInfiniteJump,
        PowerupSwordAndShield,
        PowerupMidasTouch,
        PowerupMultiplayerBaseball,
        PowerupMultiplayerInk,
        PowerupMultiplayerMines,
        PowerupMultiplayerMinesTap,
        BubblePowerupInvincibility,
        BubblePowerupShield,
        BubblePowerupYellowPet,
        BubblePowerupTornadoPet,
        BubblePowerupSwordAndShield,
        BubblePowerupMidasTouch,
        BubblePowerupInfiniteJump,
        BubblePowerupMultiplayerBaseball,
        BubblePowerupMultiplayerInk,
        BubblePowerupMultiplayerMines,
        Key
    }

    public static LuckyDrop DefaultDrop => LuckyDrop.SilverCoin;
    
    public static List<LuckyDrop> RareDrops = new List<LuckyDrop>()
    {
        LuckyDrop.GoldCoin,
        LuckyDrop.PowerupInvincibility,
        LuckyDrop.PowerupShield,
        LuckyDrop.PowerupYellowPet,
        LuckyDrop.PowerupTornadoPet,
        LuckyDrop.BubblePowerupInvincibility,
        LuckyDrop.BubblePowerupShield,
        LuckyDrop.BubblePowerupYellowPet,
        LuckyDrop.BubblePowerupTornadoPet
        //LuckyDrop.Key
    };

    public static LuckyDrop[] CanDropMultipleTimes = new LuckyDrop[]
    {
        LuckyDrop.PowerupInvincibility,
        LuckyDrop.PowerupShield,
        LuckyDrop.PowerupYellowPet,
        LuckyDrop.PowerupTornadoPet,
        LuckyDrop.BubblePowerupInvincibility,
        LuckyDrop.BubblePowerupShield,
        LuckyDrop.BubblePowerupYellowPet,
        LuckyDrop.BubblePowerupTornadoPet,
        LuckyDrop.Key
    };

    public static LuckyDrop[] MultiplayerPickups = new LuckyDrop[]
    {
        LuckyDrop.PowerupMultiplayerBaseball,
        LuckyDrop.PowerupMultiplayerInk,
        LuckyDrop.PowerupMultiplayerMines,
        LuckyDrop.BubblePowerupMultiplayerBaseball,
        LuckyDrop.BubblePowerupMultiplayerInk,
        LuckyDrop.BubblePowerupMultiplayerMines,
    };

    public static LuckyDrop GetRandomRareDrop()
    {
        List<LuckyDrop> updatedRareDrops = Pickup.RareDrops;
        if (SaveData.IsInfiniteJumpPurchased() == true)
        {
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.PowerupInfiniteJump).ToList();
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.BubblePowerupInfiniteJump).ToList();
        }
        if (SaveData.IsSwordAndShieldPurchased() == true)
        {
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.PowerupSwordAndShield).ToList();
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.BubblePowerupSwordAndShield).ToList();
        }
        if (SaveData.IsMidasTouchPurchased() == true)
        {
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.PowerupMidasTouch).ToList();
            updatedRareDrops = updatedRareDrops.Append(Pickup.LuckyDrop.BubblePowerupMidasTouch).ToList();
        }

        if (Game.instance.map2 != null)
        {
            updatedRareDrops.AddRange(MultiplayerPickups);
        }

        return updatedRareDrops[UnityEngine.Random.Range(0, updatedRareDrops.Count)];
    }

    public static bool CanPickupDropMultipleTimes(LuckyDrop pickup)
    {
        return CanDropMultipleTimes.Contains(pickup);
    }

    [NonSerialized] public bool collected = false;
    [NonSerialized] public bool canPlayerCollect = true;
    //[NonSerialized] public Vector2 velocity = Vector2.zero;
    [NonSerialized] public bool movingFreely = false;
    [NonSerialized] public int disallowCollectTime = 0;
    [NonSerialized] public int? lifetime;
    [NonSerialized] public SpinBlock controlledBySpinBlock;

    protected NitromeEditor.Path path;
    protected float offsetThroughPath;

    protected Item followItem;
    protected float followDistance;
    protected List<Vector2> followFutureLocations;

    protected bool controlledByOtherItem;

    protected float z;

    public Animated.Animation animationNormal;
    public Animated.Animation animationCollect;

    //protected float gravity;
    protected float bounciness;

    // Sprout sucking ability
    [HideInInspector] public bool suckActive = false;
    protected int shakeTimer = 0;
    protected Vector2 smoothSuckVel;
    protected float smoothSuckTime;
    protected Vector2 shakeAmount;
    protected const int SuckShakeTime = 20;
    protected readonly float[] shakeDeltas = new float[] { 1, 1, 0, 0, -1, -1, 1, -1, 0 };

    private float rotationCurrentZ = 0;
    private float rotationTargetZ = 0;
    private float rotationSmoothZ = 0;
    protected float hitboxSize;
    private bool insideWater = false;

    // gravity
    protected Gravity gravity;
    [HideInInspector] public Vector2 velocity;
    protected float gravityAcceleration = 0.00586f;
    protected float gravityAccelerationUnderwater = 0.003f;
    private Tile onGroundTile = null;
    private Item onGroundItem = null;
    private float smoothVelocityH = 0f;
    protected float horizontalMomentum = 0.50f;
    private bool IsOnGround => this.onGroundTile != null || this.onGroundItem is StartBarrier;
    private static readonly float FreeFloatingMaxSpeed = 0.31f;
    private static readonly float FreeFloatingMinSpeed = 0.0625f;

    private float CurrentGravityAcceleration => this.insideWater == true ?
        this.gravityAccelerationUnderwater : this.gravityAcceleration;

    public struct Gravity
    {
        public Pickup pickup;
        public Player.Orientation? gravityDirection;

        public Player.Orientation Orientation() =>
            this.gravityDirection ?? Player.Orientation.Normal;
        public bool IsZeroGravity() => gravityDirection == null;

        private static Vector2[] Directions =
            new [] { Vector2.down, Vector2.right, Vector2.up, Vector2.left };

        public Vector2 Down()  => Directions[(int)Orientation()];
        public Vector2 Right() => Directions[((int)Orientation() + 1) % 4];
        public Vector2 Up()    => Directions[((int)Orientation() + 2) % 4];
        public Vector2 Left()  => Directions[((int)Orientation() + 3) % 4];

        public int OrientationAngle()
        {
            if(IsZeroGravity() == true) return 0;

            switch(Orientation())
            {
                case Player.Orientation.Normal:
                default:
                    return 0;

                case Player.Orientation.LeftWall:
                    return 270;

                case Player.Orientation.RightWall:
                    return 90;

                case Player.Orientation.Ceiling:
                    return 180;
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => Orientation() switch
            {
                Player.Orientation.Normal    => pickup.velocity,
                Player.Orientation.RightWall => new Vector2(pickup.velocity.y, -pickup.velocity.x),
                Player.Orientation.Ceiling   => new Vector2(-pickup.velocity.x, -pickup.velocity.y),
                _                            => new Vector2(-pickup.velocity.y, pickup.velocity.x)
            };
            set {
                switch (Orientation())
                {
                    case Player.Orientation.Normal:    pickup.velocity = value; break;
                    case Player.Orientation.RightWall: pickup.velocity = new Vector2(-value.y, value.x); break;
                    case Player.Orientation.Ceiling:   pickup.velocity = new Vector2(-value.x, -value.y); break;
                    default:                           pickup.velocity = new Vector2(value.y, -value.x); break;
                }
            }
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.chunk = def.startChunk;
        SetUpFollowingMethod(def.startChunk);

        this.z = UnityEngine.Random.Range(0f, 1f);

        if (this.animated)
            this.animated.PlayAndLoop(this.animationNormal);

        this.hitboxSize = 0.50f;
        this.hitboxes = new [] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };

        this.gravity = new Gravity {
                pickup = this,
                gravityDirection = this.map.player.gravity.Orientation()
        };
        this.bounciness = -0.75f;

        this.rotationCurrentZ = this.rotationTargetZ = this.gravity.OrientationAngle();
    }

    private void SetUpFollowingMethod(Chunk chunk)
    {
        this.group = null;
        this.path = null;
        this.followItem = null;

        var g = chunk.groupLayer.RegisterFollower(this, def.tx, def.ty);
        if (g != null)
        {
            AttachToGroup(g);
            return;
        }

        this.path = chunk.pathLayer.NearestPathTo(this.position, 0.4f);
        if (this.path != null)
        {
            if (this.path.closed == false)
                this.path = Util.MakeClosedPathByPingPonging(this.path);

            this.offsetThroughPath = TimeNearestToPoint(this.path, this.position);
            return;
        }

        var followPath =
            chunk.connectionLayer.NearestPathTo(this.position, 0.4f);
        if (followPath != null)
        {
            var followPosition = followPath.nodes[0].Position;
            this.followItem = chunk.NearestItemTo<Item>(followPosition);

            if (this.followItem != null)
            {
                var nodeIndex = followPath.NearestNodeIndexTo(this.position).Value;
                this.followDistance = followPath.DistanceAtNodeIndex(nodeIndex);
                this.followFutureLocations = new List<Vector2> {
                    this.position,
                    this.followItem.position
                };
            }
        }
    }

    private static float TimeNearestToPoint(NitromeEditor.Path path, Vector2 pt)
    {
        var edgeIndex = path.NearestEdgeIndexTo(pt).Value;
        var edge = path.edges[edgeIndex];

        var edgeDelta = edge.to.Position - edge.from.Position;
        var through =
            Vector2.Dot(edgeDelta, pt - edge.from.Position) / edgeDelta.sqrMagnitude;
        through = Mathf.Clamp01(through);

        float result = 0.0f;
        for (var n = 0; n < edgeIndex; n += 1)
        {
            result += path.edges[n].distance / path.edges[n].speedPixelsPerSecond;
        }
        result += through * (edge.distance / edge.speedPixelsPerSecond);
        return result;
    }

    public void NotifyControlledByOtherItem() => this.controlledByOtherItem = true;
    public void StopControlledByOtherItem() => this.controlledByOtherItem = false;

    public override void Advance()
    {
        if (this.lifetime.HasValue)
        {
            this.lifetime -= 1;
            if (this.lifetime <= 0)
            {
                this.chunk.items.Remove(this);
                Destroy(this.gameObject);
            }
            else if (this.lifetime < 96)
            {
                var dim = (this.lifetime % 6) >= 3;
                this.spriteRenderer.color = dim ? new Color(1, 1, 1, 0.5f) : Color.white;
            }
        }

        UpdateGravity();

        if (this.movingFreely == true && this.collected == false)
        {
            AdvanceGravity();
            AdvanceWater();
            Integrate();

            // rotation when moving freely
            this.rotationCurrentZ = Mathf.SmoothDampAngle(
                this.rotationCurrentZ, this.rotationTargetZ, ref rotationSmoothZ, .15f);
            this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotationCurrentZ);
        }
        else if (this.controlledByOtherItem == true)
        {
            // let other item take control
        }
        else if (this.group != null && this.collected == false)
        {
            FollowGroup();
        }
        else if (this.path != null)
        {
            PathMove();
        }
        else if (this.followItem != null)
        {
            Follow();
        }
        else
        {
            // nothing
        }
    
        this.transform.position = new Vector3(this.position.x, this.position.y, this.z);

        CheckCollect();
        UpdateSuck();
    }

    protected void AdvanceWater()
    {
        if(this.map.theme != Theme.SunkenIsland) return;
        if(this.map.waterline == null) return;

        void PlaySplashWater()
        {
            Particle p = Particle.CreateAndPlayOnce(
                Assets.instance.sunkenIsland.waterSplash,
                new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                this.map.waterline.waterTransform
            );
        }
        
        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            Vector2 up = this.transform.up;
            Vector2 forward = this.transform.right;
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + UnityEngine.Random.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -0.95f) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = UnityEngine.Random.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity.y = 0f;
            
            PlaySplashWater();
            GenerateBubbles();
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }

    protected virtual void FollowGroup()
    {
        this.position = this.group.FollowerPosition(this);
    }

    public virtual void PathMove()
    {
        this.position = this.path.PointAtTime(
                (this.map.frameNumber / 60.0f) + this.offsetThroughPath
            ).position;
    }

    public virtual void CheckCollect()
    {
        if (this.disallowCollectTime > 0)
        {
            this.disallowCollectTime -= 1;
        }
        else if (this.collected == false)
        {
            Player player = this.map.player;

            if(this.canPlayerCollect == false) return;
            if(player.state == Player.State.Respawning) return;

            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = player.Rectangle();

            if (thisRect.Overlaps(playerRect))
            {
                Collect();
            }
            else if(player.InsideMinecart != null)
            {
                if(player.InsideMinecart.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                {
                    Collect();
                }
            }
        }
    }

    public virtual void Collect()
    {
        if(this.collected == true) return;

        this.collected = true;
        this.transform.localScale = new Vector3(
            UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f ? 1.0f : -1.0f,
            UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f ? 1.0f : -1.0f,
            1.0f
        );

        if(this.animated == true)
        {
            this.animated.PlayOnce(this.animationCollect, delegate
            {
                this.spriteRenderer.enabled = false;
            });
        }
    }

    public void Follow()
    {
        if ((this.followItem.position - this.followFutureLocations.Last())
            .sqrMagnitude > 0.05f * 0.05f)
        {
            this.followFutureLocations.Add(this.followItem.position);
        }

        var totalDistance = 0.0f;
        for (var n = this.followFutureLocations.Count - 2; n >= 0; n -= 1)
        {
            var a = this.followFutureLocations[n];
            var b = this.followFutureLocations[n + 1];
            var dist = (a - b).magnitude;
            if (totalDistance + dist > this.followDistance)
            {
                var remainingDist = this.followDistance - totalDistance;
                this.position = Vector2.Lerp(b, a, remainingDist / dist);

                if (n > 0)
                    this.followFutureLocations.RemoveRange(0, n);

                break;
            }
            else
            {
                totalDistance += dist;
            }
        }
    }

    protected void Integrate()
    {
        var raycast = new Raycast(this.map, this.position);
        var rect = this.hitboxes[0];
        var start = this.position;

        if (this.velocity.x < 0)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0),
                false,
                -this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0),
                true,
                this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y < 0)
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMin),
                false,
                -this.velocity.y
            );
            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMax),
                true,
                this.velocity.y
            );
            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void SpawnLuckyPickup()
    {
        this.movingFreely = true;
        this.disallowCollectTime = 30;
        this.whenChunkResets = Item.ChunkResetBehaviour.DestroyGameobject;
    }

    protected void UpdateSuck()
    {
        if(this.suckActive == false) return;

        if(this.shakeTimer > 0)
        {
            this.shakeTimer -= 1;

            var positionWithShake = this.position;

            positionWithShake += new Vector2(
                shakeAmount.x *
                shakeDeltas[this.map.frameNumber % shakeDeltas.Length],
                shakeAmount.y *
                shakeDeltas[this.map.frameNumber % shakeDeltas.Length]
            );
            
            this.transform.position = new Vector2(
                positionWithShake.x,
                positionWithShake.y);
        }
        else
        {
            this.position = Vector2.SmoothDamp(
                this.position,
                this.map.player.position,
                ref smoothSuckVel,
                smoothSuckTime
            );
        }
    }

    public void EnterSuck()
    {
        if(this.suckActive == true) return;

        this.suckActive = true;
        this.shakeAmount = Vector2.one * 0.05f;

        this.movingFreely = false;
        this.controlledByOtherItem = true;
        
        this.shakeTimer = SuckShakeTime;
        this.smoothSuckTime = UnityEngine.Random.Range(0.15f, 0.2f);
    }

    public void ExitSuck()
    {
        if(this.suckActive == false) return;
        if(IsInsideSomething() == true) return;

        this.suckActive = false;
        this.movingFreely = true;
        this.shakeTimer = 0;

        // if not collected, restart position on chunk reset
        if(this.collected == false)
        {
            this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        }
    }

    private bool IsInsideSomething()
    {
        var raycast = new Raycast(
            this.map,
            this.position,
            isCastByPlayer: true,
            ignoreItemType: typeof(StartBarrier),
            ignoreItem1: this,
            ignoreTileTopOnlyFlag: false
        );
        float maxDistance = 0.15f;

        var up = raycast.Vertical(this.position, true, maxDistance);
        var down = raycast.Vertical(this.position, false, maxDistance);
        var right = raycast.Horizontal(this.position, true, maxDistance);
        var left = raycast.Horizontal(this.position, false, maxDistance);

        return 
            (up.anything == true && down.anything == true) ||
            (right.anything == true && left.anything == true);
    }

    protected void UpdateGravity()
    {
        Player.Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        var intendedGravity = SampleAt(this.position);
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        if (this.gravity.gravityDirection == Player.Orientation.Ceiling &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Normal) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Player.Orientation.Ceiling)
                intendedGravity = Player.Orientation.Ceiling;
        }
        else if (this.gravity.gravityDirection == Player.Orientation.Normal &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Ceiling) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Player.Orientation.Normal)
                intendedGravity = Player.Orientation.Normal;
        }

        if (this.gravity.gravityDirection != intendedGravity)
        {
            this.gravity.gravityDirection = intendedGravity;
        }
    }

    protected void AdvanceGravity()
    {
        var gravityDirectionVector = this.gravity.Down();

        if (this.gravity.IsZeroGravity() == true)
        {
            var speed = this.velocity.magnitude;
            if (speed > FreeFloatingMaxSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMaxSpeed, 0.01f);
                this.velocity.Normalize();
                this.velocity *= speed;
            }
            else if (speed < FreeFloatingMinSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMinSpeed, 0.001f);
                if (this.velocity.sqrMagnitude == 0)
                    this.velocity = this.gravity.Down();
                else
                    this.velocity.Normalize();
                this.velocity *= speed;
            }

            this.rotationTargetZ = Util.WrapAngle0To360(this.rotationTargetZ + 0.5f);
        }
        else
        {
            this.velocity += CurrentGravityAcceleration * gravityDirectionVector;
            
            if(IsOnGround == true && IsInsideSomething() == false)
            {
                float GetGroundAngleDegrees()
                {
                    if(this.onGroundTile != null)
                    {
                        return this.onGroundTile.SurfaceAngleDegrees(this.position, this.gravity.Down());
                    }

                    return 0f;
                }

                this.rotationTargetZ = GetGroundAngleDegrees();
            }
            
            if(this.gravity.IsZeroGravity())
            {
                this.rotationTargetZ = this.gravity.OrientationAngle();
            }
        }
    }
    
    protected void ApplyDragOnFloor()
    {
        // check grounded
        var rFloor = new Raycast(this.map, this.position).Arbitrary(
            this.position + this.gravity.Down() * this.hitboxSize,
            this.gravity.Down(),
            0.10f
        );
        this.onGroundTile = rFloor.tile;
        this.onGroundItem = rFloor.item;

        if(IsOnGround == true)
        {
            float newVelocityOnGravityX = Mathf.SmoothDamp(
                this.gravity.VelocityInGravityReferenceFrame.x, 0f, ref smoothVelocityH, this.horizontalMomentum
            );

            this.gravity.VelocityInGravityReferenceFrame =  new Vector2(
                newVelocityOnGravityX,
                this.gravity.VelocityInGravityReferenceFrame.y
            );
        }
    }

    public static Pickup.LuckyDrop GetPickupByPropertyName(string name) => name switch
    {
        "key"                               => Pickup.LuckyDrop.Key,
        "powerup_invincibility"             => Pickup.LuckyDrop.PowerupInvincibility,
        "powerup_shield"                    => Pickup.LuckyDrop.PowerupShield,
        "powerup_yellow_pet"                => Pickup.LuckyDrop.PowerupYellowPet,
        "powerup_tornado_pet"               => Pickup.LuckyDrop.PowerupTornadoPet,
        "bubble_with_powerup_invincibility" => Pickup.LuckyDrop.BubblePowerupInvincibility,
        "bubble_with_powerup_shield"        => Pickup.LuckyDrop.BubblePowerupShield,
        "bubble_with_powerup_yellow_pet"    => Pickup.LuckyDrop.BubblePowerupYellowPet,
        "bubble_with_powerup_tornado_pet"   => Pickup.LuckyDrop.BubblePowerupTornadoPet,
        "powerup_heart"                     => Pickup.LuckyDrop.PowerupHeart,
        "coin"                              => Pickup.LuckyDrop.GoldCoin,
        "none"                              => Pickup.LuckyDrop.None,
        _                                   => Pickup.LuckyDrop.None
    };

    public virtual void ChangeSorting(string layerName, int orderInLayer)
    {
        this.spriteRenderer.sortingLayerName = layerName;
        this.spriteRenderer.sortingOrder = orderInLayer;
    }

    public void OnDestroy()
    {
        this.group?.RemoveFollower(this);
        this.controlledBySpinBlock?.DetachItem(this as Item);
    }
}