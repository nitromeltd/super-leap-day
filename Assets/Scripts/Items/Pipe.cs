
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rnd = UnityEngine.Random;

public class Pipe : Item
{
    public enum Direction
    {
        Left, Right, Up, Down
    }

    public enum Action
    {
        None,
        Stop,    // stop at junction, tap to continue
        Select,  // stop at junction, flips between available directions, tap to choose
        Cannon   // stop at section, tap to shoot out of pipe
    }
    
    public enum DirectionsType
    {
        None,
        AllDirections,
        LeftDownRight,
        UpLeftRight,
        UpRightDown,
        DownLeft,
        UpLeft,
        Vertical,
        Horizontal,
        Corner,
        Straight,
        Exit
    }

    public Sprite[] alternativeSprites;
    public Action action;
    public DirectionsType directionsType;
    public Sprite arrowSprite;
    public bool isSewerPipe;
    public SpriteRenderer topSpriteRend;
    public GameObject pipeEndForWindySkies;
    private bool isCorner;
    public Direction[] connections;
    private Direction? exit;

    public AnimationCurve cannonCurve;

    private Direction pathExitDirection;
    private NitromeEditor.Path path;
    private Pipe pipePieceAtOtherEndOfPath;
    private bool transitPathForwards;

    private GameObject arrowGO;
    private PipeCover pipeCover; 
    private ManholeMonster manholeMonster;
    private CrashBarrier crashBarrier;

    private bool isPlayerInside;
    private bool isEnemyInside;

    public bool isBroken = false;
    public class TrackingData
    {
        public Player player;
        public TrafficConeEnemy trafficConeEnemy;
        public ManholeMonsterBullet manholeMonsterBullet;
        public PigeonEgg pigeonEgg;
        public int disableEntryUntilFrameNumber = int.MinValue;
        public bool transittingPath;
        public float distanceAlongPath;

        public Vector2 Position
        {
            get {
                if (player == true) return player.position;
                if (trafficConeEnemy != null) return trafficConeEnemy.position;
                if (manholeMonsterBullet != null) return manholeMonsterBullet.position;
                if (pigeonEgg != null) return pigeonEgg.position;
                return player.position;
            }
            set {
                if (player == true) player.position = value;
                if (trafficConeEnemy != null) trafficConeEnemy.position = value;
                if (manholeMonsterBullet != null) manholeMonsterBullet.position = value;
                if (pigeonEgg != null) pigeonEgg.position = value;
            }
        }

        public Vector2 Velocity
        {
            get {
                if (player == true) return player.velocity;
                if (trafficConeEnemy != null) return trafficConeEnemy.velocity;
                if (manholeMonsterBullet != null) return manholeMonsterBullet.velocity;
                if (pigeonEgg != null) return pigeonEgg.velocity;
                return player.velocity;
            }
            set {
                if (player == true) player.velocity = value;
                if (trafficConeEnemy != null) trafficConeEnemy.velocity = value;
                if (manholeMonsterBullet != null) manholeMonsterBullet.velocity = value;
                if (pigeonEgg != null) pigeonEgg.velocity = value;
            }
        }

        public SpriteRenderer SpriteRenderer
        {
            get {
                if (player != null)
                    return player.GetComponent<SpriteRenderer>();
                else if(trafficConeEnemy != null)
                    return trafficConeEnemy.GetComponent<SpriteRenderer>();
                else if(manholeMonsterBullet != null)
                    return manholeMonsterBullet.GetComponent<SpriteRenderer>();
                else if(pigeonEgg != null)
                    return pigeonEgg.GetComponent<SpriteRenderer>();
                else
                    return player.GetComponent<SpriteRenderer>();
            }
        }
    }
    private TrackingData trackingPlayer;
    
    // Stop
    private bool shouldStop = false;
    private bool thingStopped = false;

    // Select
    private Direction? selectedDirection = null;
    private int selectedDirectionIndex;
    private List<Direction> possibleDirections = new List<Direction>();
    private int timeSinceThingEnter;
    private int timeSinceLastPossibleDirection;
    private Direction nonPlayerDesiredDirection;

    private static bool CannonActive = false;

    public static readonly float PlayerSpeed = 0.375f;
    public static readonly float PlayerSpeedCannon = 0.75f;
    public static readonly float PlayerExitSpeedUpward = 5 / 16f;
    public static readonly float PlayerExitSpeedOther = 1.5f / 16f;
    public static readonly float DefaultExitSpeed = 5 / 16f;
    public static readonly float TopSpriteChance = 0.15f;
    public static readonly int DirectionSelectTime = 30;
    public static readonly int NonPlayerDecisionTime = 20;

    public override void Init(Def def)
    {
        base.Init(def);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        if(this.group != null && this.isSewerPipe == true)
        {
            SortingGroup sg = this.group.container.GetComponent<SortingGroup>();

            if(sg != null)
            {
                sg.sortingLayerName = "Spikes";
                sg.sortingOrder = -15;
            }
        }

        if(this.topSpriteRend != null)
        {
            this.topSpriteRend.enabled = Random.value < TopSpriteChance;
        }

        if (this.alternativeSprites?.Length >= 2)
            this.spriteRenderer.sprite = Util.RandomChoice(this.alternativeSprites);

        this.trackingPlayer = new TrackingData();
        this.trackingPlayer.player = this.map.player;
        
        var node = this.chunk.connectionLayer.NearestNodeTo(
            this.position, 1.5f
        );
        if (node != null)
        {
            this.path = node.path;
            this.pathExitDirection = Vector2ToDirection(node.Position - Center());

            var otherNode =
                this.path.nodes[0] == node ?
                this.path.nodes.Last() :
                this.path.nodes[0];

            this.pipePieceAtOtherEndOfPath =
                this.chunk.NearestItemTo<Pipe>(otherNode.Position, 2);

            this.transitPathForwards = (node == node.path.nodes[0]);
        }

        if (def.tile.tile.Contains("corner"))
        {
            this.rotation = def.tile.rotation;
            if (def.tile.flip == true)
            {
                if (this.rotation == 0)
                    this.rotation = 90;
                else if (this.rotation == 90)
                    this.rotation = 180;
                else if (this.rotation == 180)
                    this.rotation = 270;
                else if (this.rotation == 270)
                    this.rotation = 0;
                this.transform.localRotation = Quaternion.Euler(0, 0, this.rotation);
            }

            this.hitboxes = new [] {
                new Hitbox(-1, 1, 0, 1, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(0, 1, -1, 1, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(-0.5f, 0, -0.5f, 0, this.rotation, Hitbox.Solid, true, true)
            };

            if (this.rotation == 0)
                this.connections = new [] { Direction.Up, Direction.Right };
            else if (this.rotation == 90)
                this.connections = new [] { Direction.Up, Direction.Left };
            else if (this.rotation == 180)
                this.connections = new [] { Direction.Down, Direction.Left };
            else if (this.rotation == 270)
                this.connections = new [] { Direction.Down, Direction.Right };

            this.exit = null;
            this.isCorner = true;
        }
        else if (this.action != Action.None)
        {
            this.rotation = def.tile.rotation;

            if(this.directionsType != DirectionsType.Straight &&
                this.directionsType != DirectionsType.Exit)
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -1f, 1f, this.rotation, Hitbox.Solid, true, true)
                };
            }
            else
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
                };
            }

            if(def.tile.flip == true)
            {
                Direction[] GetFlippedConnections()
                {
                    switch(this.directionsType)
                    {
                        case DirectionsType.UpRightDown:
                            return new [] { Direction.Up, Direction.Left, Direction.Down };

                        case DirectionsType.DownLeft:
                            return new [] { Direction.Down, Direction.Right };

                        case DirectionsType.UpLeft:
                            return new [] { Direction.Up, Direction.Right };

                        default:
                            return this.connections;
                    }
                }

                this.connections = GetFlippedConnections();
                this.transform.localScale = new Vector3(-1f, 1f, 1f);
            }
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-1, 1, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
            };
            if (def.tile.rotation == 0 || def.tile.rotation == 180)
                this.connections = new [] { Direction.Up, Direction.Down };
            else
                this.connections = new [] { Direction.Left, Direction.Right };

            this.exit = null;
            this.isCorner = false;

            if (def.tile.tile.Contains("exit"))
            {
                if      (def.tile.rotation ==   0) this.exit = Direction.Up;
                else if (def.tile.rotation ==  90) this.exit = Direction.Left;
                else if (def.tile.rotation == 180) this.exit = Direction.Down;
                else if (def.tile.rotation == 270) this.exit = Direction.Right;
            }
            else if (this.rotation >= 180 && this.isBroken == false)
            {
                // force editor pieces to be the right way round if possible
                this.rotation -= 180;
                this.transform.localRotation = Quaternion.Euler(0, 0, this.rotation);
            }

            this.transform.localScale = new Vector3(Util.RandomChoice(1, -1), 1, 1);
        }

        ShowOrHidePipeEndForWindySkies();

        if(this.action == Action.Select)
        {
            this.arrowGO = new GameObject("Pipe Select Arrow");
            this.arrowGO.transform.SetParent(this.transform);
            this.arrowGO.transform.localPosition = Vector2.zero;
            SpriteRenderer arrowSR = this.arrowGO.AddComponent<SpriteRenderer>();
            arrowSR.sprite = this.arrowSprite;
            this.arrowGO.SetActive(false);
        }
    }

    public override void NotifyNewPlayerCreatedForMultiplayer(Player replacementPlayer)
    {
        this.trackingPlayer.player = replacementPlayer;
    }

    private void ShowOrHidePipeEndForWindySkies()
    {
        if (this.pipeEndForWindySkies == null)
            return;

        this.pipeEndForWindySkies.gameObject.SetActive(false);

        if (this.map.theme != Theme.WindySkies)
            return;

        foreach (var direction in this.connections)
        {
            var offset = DirectionToVector2(direction);
            var tx = Mathf.FloorToInt(this.position.x + offset.x);
            var ty = Mathf.FloorToInt(this.position.y + offset.y);
            if (this.map.ChunkAt(tx, ty) == this.chunk) continue;

            this.pipeEndForWindySkies.gameObject.SetActive(true);
            this.pipeEndForWindySkies.transform.position =
                this.position + (offset * 1.5f);
            this.pipeEndForWindySkies.transform.rotation = Quaternion.Euler(
                0, 0, (Mathf.Atan2(offset.y, offset.x) * 180 / Mathf.PI) + 90
            );
            break;
        }
    }

    private Vector2 Center() => this.position;

    public Vector2 DirectionToVector2(Direction direction, float magnitude = 1)
    {
        switch (direction)
        {
            case Direction.Left:  return magnitude * Vector2.left;
            case Direction.Right: return magnitude * Vector2.right;
            case Direction.Down:  return magnitude * Vector2.down;
            case Direction.Up:    return magnitude * Vector2.up;
        }
        return default;
    }

    public Direction Vector2ToDirection(Vector2 vector)
    {
        if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
            return vector.x > 0 ? Direction.Right : Direction.Left;
        else
            return vector.y > 0 ? Direction.Up : Direction.Down;
    }

    private Direction DirectionToPlayer(TrackingData thing)
    {
        Vector2 headingToPlayer = this.map.player.position - thing.Position;
        Vector2 directionToPlayer = headingToPlayer / headingToPlayer.magnitude;
        return Vector2ToDirection(directionToPlayer);
    }

    public Direction Invert(Direction direction)
    {
        switch (direction)
        {
            case Direction.Left:  return Direction.Right;
            case Direction.Right: return Direction.Left;
            case Direction.Down:  return Direction.Up;
            case Direction.Up:    return Direction.Down;
        }
        return default;
    }

    public bool IsPlayerInsideInvisiblePath() => this.trackingPlayer.transittingPath;

    public override void Advance()
    {
        if(this.exit != null)
        {
            if(this.pipeCover == null)
            {
                this.pipeCover = this.chunk.NearestItemTo<PipeCover>(
                    this.position, 2f
                );
            }

            // ideally we will do this on Reset() once, but enemies can get destroyed
            // and might call Reset() after this Pipe's own Reset(), so we need to
            // search for the enemy here to make sure we don't miss it
            if(this.manholeMonster == null)
            {
                this.manholeMonster = this.chunk.NearestItemTo<ManholeMonster>(
                    this.position, 2f
                );
            }
        }
        
        if(this.crashBarrier == null)
        {
            this.crashBarrier = this.chunk.NearestItemTo<CrashBarrier>(
                this.position, 1f
            );

            if(this.crashBarrier != null)
            {
                this.crashBarrier.InsidePipe();

                if(this.topSpriteRend.enabled == true)
                {
                    this.topSpriteRend.enabled = false;
                }
            }
        }

        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if (this.exit != null)
        {
            TestForPlayerEntry();

            foreach (var e in this.chunk.items.OfType<TrafficConeEnemy>())
                TestForTrafficConeEntry(e);
            foreach (var e in this.chunk.items.OfType<ManholeMonsterBullet>())
                TestForManholeBulletEntry(e);
            foreach (var e in this.chunk.items.OfType<PigeonEgg>())
                TestForPigeonEggEntry(e);
        }

        if(this.thingStopped == true)
        {
            this.timeSinceThingEnter += 1;
            this.timeSinceLastPossibleDirection += 1;

            if(this.timeSinceThingEnter >= DirectionSelectTime)
            {
                int currentDirectionIndex = this.selectedDirectionIndex;

                float currentTime = this.timeSinceThingEnter / DirectionSelectTime;
                this.selectedDirectionIndex =
                    Mathf.FloorToInt(currentTime % this.possibleDirections.Count);

                if(this.action == Action.Select &&
                    currentDirectionIndex != this.selectedDirectionIndex)
                {
                    ShowDirectionArrow();
                    this.timeSinceLastPossibleDirection = 0;
                }
            }
        }
    }

    private void ShowDirectionArrow()
    {
        Vector2 arrowDirection = DirectionToVector2(this.possibleDirections[selectedDirectionIndex]);
        this.arrowGO.transform.rotation = Quaternion.LookRotation(Vector3.forward, arrowDirection);

        if(def.tile.flip == true)
        {
            arrowDirection.x = -arrowDirection.x;
        }

        this.arrowGO.transform.localPosition = arrowDirection * 1.5f;
    }

    private float GetPlayerSpeed()
    {
        if(CannonActive == true)
        {
            return PlayerSpeedCannon;
        }

        return PlayerSpeed;
    }

    private bool IsPlayerStateValidForEntry()
    {
        var player = this.map.player;
        
        if(player.state == Player.State.Normal) return true;
        if(player.state == Player.State.Jump) return true;
        if(player.state == Player.State.Spring) return true;
        if(player.state == Player.State.SpringFromPushPlatform) return true;
        if(player.state == Player.State.InsideWater) return true;
        if(player.state == Player.State.RunningOnGripGround) return true;
        if(player.state == Player.State.UsingParachuteClosed) return true;
        if(player.state == Player.State.UsingParachuteOpened) return true;
        if(player is PlayerGoop && player.state == Player.State.GoopBallSpinning) return true;

        return false;
    }

    private void TestForPlayerEntry()
    {
        var player = this.map.player;
        var data = this.trackingPlayer;

        if(this.pipeCover != null)
            return;
        if(this.manholeMonster != null)
            return;
        if (this.map.frameNumber < data.disableEntryUntilFrameNumber)
            return;
        if (IsPlayerStateValidForEntry() == false)
            return;
        if(player.grabbingOntoItem != null)
            return;
        if(player.alive == false)
            return;
        if (Vector2.Dot(player.velocity, DirectionToVector2(this.exit.Value)) >= 0.01f)
            return;

        Vector2 entryPoint = Center() + DirectionToVector2(this.exit.Value, 1);

        if ((player.position - entryPoint).sqrMagnitude < 1 * 1)
        {
            player.GoInsidePipe(this);
            player.velocity = DirectionToVector2(Invert(this.exit.Value), GetPlayerSpeed());
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPipeIn,
                position: this.position
            );
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvPipesLoop) && isSewerPipe)
            {
                Audio.instance.PlaySfxLoop(
                    Assets.instance.sfxEnvPipesLoop,
                    position: this.position
                );
            }
        }
    }

    private void TestForTrafficConeEntry(TrafficConeEnemy enemy)
    {
        if(this.pipeCover != null)
            return;
        if(this.manholeMonster != null)
            return;
        if (enemy.insidePipe != null) return;
        if (Vector2.Dot(enemy.velocity, DirectionToVector2(this.exit.Value)) >= 0.01f)
            return;

        var data = enemy.pipeTrackingData;
        if (data == null)
            data = enemy.pipeTrackingData = new TrackingData { trafficConeEnemy = enemy };
        if (this.map.frameNumber < data.disableEntryUntilFrameNumber)
            return;

        Vector2 entryPoint = Center() + DirectionToVector2(this.exit.Value, 1);

        if ((enemy.position - entryPoint).sqrMagnitude < 1 * 1)
        {
            enemy.insidePipe = this;
            enemy.ChangeState(TrafficConeEnemy.State.Pipe);
            enemy.velocity = DirectionToVector2(Invert(this.exit.Value), GetPlayerSpeed());
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPipeIn,
                position: this.position
            );
        }
    }
    
    private void TestForManholeBulletEntry(ManholeMonsterBullet enemy)
    {
        if(this.pipeCover != null)
            return;
        if(this.manholeMonster != null)
            return;
        if (enemy.insidePipe != null) return;
        if (Vector2.Dot(enemy.velocity, DirectionToVector2(this.exit.Value)) >= 0.01f)
            return;

        var data = enemy.pipeTrackingData;
        if (data == null)
            data = enemy.pipeTrackingData = new TrackingData { manholeMonsterBullet = enemy };
        if (this.map.frameNumber < data.disableEntryUntilFrameNumber)
            return;

        Vector2 entryPoint = Center() + DirectionToVector2(this.exit.Value, 1);

        if ((enemy.position - entryPoint).sqrMagnitude < 1 * 1)
        {
            enemy.insidePipe = this;
            enemy.velocity = DirectionToVector2(Invert(this.exit.Value), GetPlayerSpeed());
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPipeIn,
                position: this.position
            );
        }
    }
    
    private void TestForPigeonEggEntry(PigeonEgg enemy)
    {
        if(this.pipeCover != null)
            return;
        if(this.manholeMonster != null)
            return;
        if (enemy.insidePipe != null) return;
        if (Vector2.Dot(enemy.velocity, DirectionToVector2(this.exit.Value)) >= 0.01f)
            return;

        var data = enemy.pipeTrackingData;
        if (data == null)
            data = enemy.pipeTrackingData = new TrackingData { pigeonEgg = enemy };
        if (this.map.frameNumber < data.disableEntryUntilFrameNumber)
            return;

        Vector2 entryPoint = Center() + DirectionToVector2(this.exit.Value, 1);

        if ((enemy.position - entryPoint).sqrMagnitude < 1 * 1)
        {
            enemy.insidePipe = this;
            enemy.velocity = DirectionToVector2(Invert(this.exit.Value), GetPlayerSpeed());
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPipeIn,
                position: this.position
            );
        }
    }

    public void AdvancePlayer()
    {
        this.trackingPlayer.player = this.map.player;
        AdvanceThing(this.trackingPlayer);
    }

    public void AdvanceTrafficCone(TrafficConeEnemy enemy) =>
        AdvanceThing(enemy.pipeTrackingData);

    public void AdvanceManholeBullet(ManholeMonsterBullet enemy) =>
        AdvanceThing(enemy.pipeTrackingData);

    public void AdvancePigeonEgg(PigeonEgg enemy) =>
        AdvanceThing(enemy.pipeTrackingData);

    private bool ShouldNonPlayerDecide(TrackingData thing)
    {
        Direction selectedDirection = this.possibleDirections[this.selectedDirectionIndex];
        bool decisionTimeOver = this.timeSinceLastPossibleDirection > NonPlayerDecisionTime;

        return selectedDirection == this.nonPlayerDesiredDirection &&
            decisionTimeOver == true;
    }

    private void AdvanceThing(TrackingData thing)
    {
        if(this.action != Action.None)
        {
            if(this.thingStopped == true)
            {
                if((thing.player != null && GameInput.Player(this.map).pressedJump == true) ||
                    (thing.player == null && ShouldNonPlayerDecide(thing)))
                {
                    this.shouldStop = false;
                    this.thingStopped = false;

                    if(this.action == Action.Select)
                    {
                        this.selectedDirection = this.possibleDirections[selectedDirectionIndex];
                        this.arrowGO.SetActive(false);
                    }
                    else if(this.action == Action.Cannon)
                    {
                        CannonActive = true;
                        PlayCannonAnimation();
                    }
                }
            }

            if(this.shouldStop == true)
            {
                float centerOffset = 0.10f;

                bool isInCenter = thing.Velocity.y > 0f ? 
                    thing.Position.y > this.position.y - centerOffset :
                    thing.Position.y < this.position.y + centerOffset;

                if(this.thingStopped == false && isInCenter == true)
                {
                    this.thingStopped = true;
                    thing.Position = new Vector2(thing.Position.x, this.position.y + centerOffset);
                }

                if(this.thingStopped == true)
                {
                    thing.Position = Center();
                    return;
                }
            }
        }

        if (thing.transittingPath == true)
        {
            if (this.transitPathForwards == true)
            {
                thing.distanceAlongPath += GetPlayerSpeed();
                if (thing.distanceAlongPath > this.path.length)
                {
                    thing.transittingPath = false;
                    thing.SpriteRenderer.enabled = true;
                    thing.Position = this.pipePieceAtOtherEndOfPath.position;
                    thing.Velocity = this.path.edges.Last().direction;
                    ThingTransitionToNextPipePiece(thing, thing.Velocity);
                }
            }
            else
            {
                thing.distanceAlongPath -= GetPlayerSpeed();
                if (thing.distanceAlongPath < 0)
                {
                    thing.transittingPath = false;
                    thing.SpriteRenderer.enabled = true;
                    thing.Position = this.pipePieceAtOtherEndOfPath.position;
                    thing.Velocity = -this.path.edges[0].direction;
                    ThingTransitionToNextPipePiece(thing, thing.Velocity);
                }
            }

            if (thing.transittingPath == true)
            {
                var point = this.path.PointAtDistanceAlongPath(thing.distanceAlongPath);
                thing.Position = point.position;
                thing.Velocity = point.edge.direction * GetPlayerSpeed();
            }
            return;
        }

        Direction forward = Vector2ToDirection(thing.Velocity);
        Direction backward = Invert(forward);

        var forwardVector = DirectionToVector2(forward);
        var forwardAmount = Vector2.Dot(thing.Position - Center(), forwardVector);
        forwardAmount += GetPlayerSpeed();

        if (forwardAmount > 0 && this.connections.Contains(forward) == false)
        {
            forward = this.connections.Where(c => c != backward).FirstOrDefault();
            forwardVector = DirectionToVector2(forward);
        }

        if(this.action == Action.Select && this.selectedDirection.HasValue)
        {
            forward = this.selectedDirection.Value;
            forwardVector = DirectionToVector2(forward);
        }

        thing.Position = Center() + (forwardVector * forwardAmount);
        thing.Velocity = forwardVector * GetPlayerSpeed();

        float GetCurrentForward()
        {
            if(this.isCorner == true)
            {
                return 1f;
            }
            
            if(this.action != Action.None)
            {
                return 1f;
            }

            return 0.50f;
        }

        if (forwardAmount > GetCurrentForward())
        {
            if (this.exit == forward)
            {
                ThingExitPipe(thing, forwardVector);
            }
            else if (this.path != null &&
                this.path.length > 0 &&
                this.pathExitDirection == forward)
            {
                thing.transittingPath = true;
                thing.distanceAlongPath = this.transitPathForwards ? 0 : this.path.length;
                thing.SpriteRenderer.enabled = false;
            }
            else
            {
                ThingTransitionToNextPipePiece(thing, forwardVector);
            }
        }

        if (thing.player != null)
            thing.player.transform.position = thing.player.position;

        if (thing.trafficConeEnemy != null)
        {
            float angle = Mathf.Atan2(
                thing.Velocity.y, thing.Velocity.x
            ) * 180 / Mathf.PI + 270;
            if (thing.trafficConeEnemy.insidePipe == null) angle = 0;

            thing.trafficConeEnemy.transform.rotation = Quaternion.Euler(0, 0, angle);
            thing.trafficConeEnemy.transform.position = thing.trafficConeEnemy.position;
        }
    }

    private void ThingTransitionToNextPipePiece(
        TrackingData thing, Vector2 forwardVector
    )
    {
        var nextPipePiece = this.chunk.NearestItemTo<Pipe>(
            thing.Position, 2
        );

        if(nextPipePiece != null &&
            nextPipePiece.crashBarrier != null &&
            nextPipePiece.crashBarrier.destroyed == false)
        {
            bool canBreakBlock =
                CannonActive == true ||
                thing.manholeMonsterBullet == true ||
                thing.pigeonEgg == true;

            if(canBreakBlock == true)
            {
                nextPipePiece.crashBarrier.Break(thing.player != null);
            }
            else
            {
                var currentDirection = Vector2ToDirection(thing.Velocity);
                var direction = Vector2ToDirection(this.Center() - thing.Position);
                thing.Velocity = DirectionToVector2(direction, GetPlayerSpeed());
                return;
            }
        }

        if (nextPipePiece != null)
        {
            nextPipePiece.ThingEnterPipe(thing);

            if (thing.player != null)
            {
                this.isPlayerInside = false;
                thing.player.GoInsidePipe(nextPipePiece);
            }
            
            if (thing.trafficConeEnemy != null)
            {
                this.isEnemyInside = false;
                thing.trafficConeEnemy.insidePipe = nextPipePiece;
            }

            if (thing.manholeMonsterBullet != null)
            {
                this.isEnemyInside = false;
                thing.manholeMonsterBullet.insidePipe = nextPipePiece;
            }

            if (thing.pigeonEgg != null)
            {
                this.isEnemyInside = false;
                thing.pigeonEgg.insidePipe = nextPipePiece;
            }

            var currentDirection = Vector2ToDirection(thing.Velocity);
            if (nextPipePiece.connections.Contains(Invert(currentDirection)) == false)
            {
                var direction = Vector2ToDirection(this.Center() - thing.Position);
                thing.Velocity = DirectionToVector2(direction, GetPlayerSpeed());
            }
        }
        else
        {
            ThingExitPipe(thing, forwardVector);
        }
    }

    private void ThingEnterPipe(TrackingData thing)
    {
        if(thing.player != null)
        {
            if(this.isEnemyInside == true)
            {
                thing.player.Hit(thing.Position);
                return;
            }

            this.isPlayerInside = true;
        }
        else
        {
            if(this.isPlayerInside == true)
            {
                this.map.player.Hit(thing.Position);
                return;
            }

            this.isEnemyInside = true;
        }

        if(this.action != Action.None)
        {
            bool isBullet = thing.manholeMonsterBullet != null || thing.pigeonEgg != null;

            this.shouldStop = !isBullet;
            this.thingStopped = false;
            CannonActive = false;

            Direction currenDirection = Invert(Vector2ToDirection(thing.Velocity));
            this.possibleDirections = this.connections.ToList();
            this.nonPlayerDesiredDirection =
                this.possibleDirections[Random.Range(0, this.possibleDirections.Count)];

            this.timeSinceThingEnter = 0;
            this.timeSinceLastPossibleDirection = 0;

            if(this.action == Action.Select)
            {
                this.selectedDirection = null;
                this.selectedDirectionIndex = 0;
                
                if(isBullet == false)
                {
                    this.arrowGO.SetActive(true);
                    ShowDirectionArrow();
                }

                if(thing.player == null)
                {
                    if(thing.trafficConeEnemy != null)
                    {
                        Direction directionToPlayer = DirectionToPlayer(thing);

                        if(this.possibleDirections.Contains(directionToPlayer))
                        {
                            this.nonPlayerDesiredDirection = directionToPlayer;
                        }
                    }
                    else if(thing.manholeMonsterBullet != null || thing.pigeonEgg != null)
                    {
                        Direction forward = Vector2ToDirection(thing.Velocity);

                        // Bullet always tries to select the forward direction
                        if(this.possibleDirections.Contains(forward))
                        {
                            this.selectedDirection =
                                this.nonPlayerDesiredDirection = forward;
                        }
                        // if there's no forward direction, randomly choose
                        // one of the available side directions (left or right)
                        else
                        {
                            List<Direction> sidesDirections = this.connections.Where(
                                c => (c != currenDirection && c != forward)
                            ).ToList();

                            this.selectedDirection = this.nonPlayerDesiredDirection =
                                sidesDirections[Random.Range(0, sidesDirections.Count)];
                        }
                        
                        this.selectedDirection = this.nonPlayerDesiredDirection;
                    }
                }
            }
        }
    }

    private void ThingExitPipe(TrackingData thing, Vector2 forwardVector)
    {
        thing.Velocity = GetExitVelocity(thing, forwardVector);
        thing.Position = Center() + (forwardVector * 1.5f);

        if (thing.player != null)
        {
            this.isPlayerInside = false;

            var player = this.map.player;

            if (Vector2.Dot(forwardVector, player.gravity.Down()) > 0)
                player.state = Player.State.DropVertically;
            else
                player.state = Player.State.Normal;

            player.NotifyExitedPipe();

            var raycast = new Raycast(this.map, this.position, isCastByPlayer: true);
            if (player.gravity.IsZeroGravity() == true)
            {
                Player.Orientation Orientation(Vector2 v)
                {
                    if (v.y > 0) return Player.Orientation.Ceiling;
                    if (v.x > 0) return Player.Orientation.RightWall;
                    if (v.y < 0) return Player.Orientation.Normal;
                    if (v.x < 0) return Player.Orientation.LeftWall;
                    return Player.Orientation.Normal;
                }

                var left  = new Vector2(-forwardVector.y, forwardVector.x);
                var right = new Vector2(forwardVector.y, -forwardVector.x);
                var leftResult = raycast.Arbitrary(
                    this.position + forwardVector, left, 1.2f
                );
                var rightResult = raycast.Arbitrary(
                    this.position + forwardVector, right, 1.2f
                );

                if (leftResult.anything == true &&
                    rightResult.anything == true)
                {
                    // if there's ground on both sides, when exiting a pipe,
                    // then favour player's previous orientation
                    if (Vector2.Dot(rightResult.direction, player.gravity.Down()) > 0.9f)
                        leftResult.anything = false;
                    if (Vector2.Dot(leftResult.direction, player.gravity.Down()) > 0.9f)
                        rightResult.anything = false;
                }

                if (leftResult.anything == true)
                {
                    float angle = leftResult.surfaceAngleDegrees;
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Orientation(left), angle, false, false
                    );
                    player.groundspeed = -PlayerExitSpeedOther;
                    player.facingRight = player.groundspeed > 0;
                }
                else if (rightResult.anything == true)
                {
                    float angle = rightResult.surfaceAngleDegrees;
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Orientation(right), angle, false, false
                    );
                    player.groundspeed = PlayerExitSpeedOther;
                    player.facingRight = player.groundspeed > 0;
                }
                else
                {
                    player.groundState = Player.GroundState.Airborne(player);
                }
            }
            else if (Mathf.Approximately(
                Vector2.Dot(player.gravity.Down(), forwardVector), 0
            ))
            {
                // pipe points sideways
                var downward = raycast.Arbitrary(
                    this.position + forwardVector, player.gravity.Down(), 1.2f
                );
                if (downward.anything == true)
                {
                    float angle = downward.surfaceAngleDegrees;
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, player.gravity.Orientation(), angle, false, false
                    );

                    var rightVector = new Vector2(forwardVector.y, -forwardVector.x);
                    if (Vector2.Dot(rightVector, player.gravity.Down()) > 0.9f)
                        player.groundspeed = PlayerExitSpeedOther;
                    else
                        player.groundspeed = -PlayerExitSpeedOther;

                    player.facingRight = player.groundspeed > 0;
                }
                else
                {
                    player.groundState = Player.GroundState.Airborne(player);
                }
            }
            else
            {
                // pipe points up or down
                player.groundState = Player.GroundState.Airborne(player);
            }

            thing.disableEntryUntilFrameNumber = this.map.frameNumber + 5;
            
            if(CannonActive == true)
            {
                player.ResetDoubleJump();
                player.firedFromPipeCannon = true;
                CannonActive = false;
            }
        }
        else
        {
            if (thing.trafficConeEnemy != null)
            {
                this.isEnemyInside = false;
                thing.trafficConeEnemy.insidePipe = null;
                thing.trafficConeEnemy.ChangeState(TrafficConeEnemy.State.Airborne);
                thing.disableEntryUntilFrameNumber = this.map.frameNumber + 5;
            }

            if (thing.manholeMonsterBullet != null)
            {
                this.isEnemyInside = false;
                thing.manholeMonsterBullet.insidePipe = null;
                thing.disableEntryUntilFrameNumber = this.map.frameNumber + 5;
            }

            if (thing.pigeonEgg != null)
            {
                this.isEnemyInside = false;
                thing.pigeonEgg.ExitPipe();
                thing.disableEntryUntilFrameNumber = this.map.frameNumber + 5;
            }
        }

        SpawnExitParticles();
        
        if (this.exit != null)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPipeOut,
                position: this.position
            );
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnvPipesLoop);
        }

        if(this.pipeCover != null)
        {
            this.pipeCover.Open();
        }

        if(this.manholeMonster != null && thing.player != null)
        {
            this.manholeMonster.Hit(new Enemy.KillInfo {
                playerDeliveringKill = thing.player,
                freezeTime = true
            });
        }
    }

    private void SpawnExitParticles()
    {
        if (this.exit == null) return;

        var outDirection = DirectionToVector2(this.exit.Value);
        var across = new Vector2(outDirection.y, -outDirection.x);
        var thetaRadians = Mathf.Atan2(outDirection.y, outDirection.x);
        var exitPoint = Center() + (outDirection * 0.5f);

        // particles moving with player velocity
        for (var n = 0; n < 6; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                exitPoint + (across * Rnd.Range(-1, 1)),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
            p.velocity = outDirection * Rnd.Range(0.08f, 0.14f);
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
        // half-circle above ground
        for (var n = 0; n < 10; n += 1)
        {
            var theta =
                thetaRadians + Rnd.Range(Mathf.PI * -0.5f, Mathf.PI * 0.5f);
            var cos_theta = Mathf.Cos(theta);
            var sin_theta = Mathf.Sin(theta);
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                exitPoint + (outDirection * -0.5f) + new Vector2(cos_theta, sin_theta),
                this.transform.parent
            );
            float speed = Rnd.Range(0.3f, 0.7f) / 16f;
            p.velocity = new Vector2(
                cos_theta * speed,
                sin_theta * speed
            );
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
        // a few particles moving along the ground plane
        for (var n = 0; n < 2; n += 1)
        {
            var theta = thetaRadians + ((n - 0.5f) * Mathf.PI);
            var cos_theta = Mathf.Cos(theta);
            var sin_theta = Mathf.Sin(theta);
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                exitPoint,
                this.transform.parent
            );
            float speed = Rnd.Range(0.7f, 1.4f) / 16f;
            p.velocity = new Vector2(
                cos_theta * speed,
                sin_theta * speed
            );
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvPipesCannon,
            position: this.position
        );
    }

    private Vector2 GetExitVelocity(TrackingData thing, Vector2 forwardVector)
    {
        Vector2 exitVelocity = thing.Velocity;

        if(thing.player != null ||
            thing.trafficConeEnemy != null)
        {
            if (forwardVector.y > 0)
            {
                float playerExitSpeed = CannonActive ?
                    PlayerExitSpeedUpward * 2f : PlayerExitSpeedUpward;
                thing.Velocity = forwardVector * playerExitSpeed;
            }
            else
            {
                float playerExitSpeed = CannonActive ?
                    PlayerExitSpeedOther * 2f : PlayerExitSpeedOther;
                thing.Velocity = forwardVector * playerExitSpeed;
            }
        }
        else
        {
            float defaultExitSpeed = CannonActive ?
                DefaultExitSpeed * 2f : DefaultExitSpeed;
            thing.Velocity = forwardVector * defaultExitSpeed;
        }

        return exitVelocity;
    }

    public override void Reset()
    {
        base.Reset();

        this.isPlayerInside = false;
        this.isEnemyInside = false;
    }

    public void PlayCannonAnimation()
    {
        StartCoroutine(_PlayCannonAnimation());
    }

    private IEnumerator _PlayCannonAnimation()
    {
        float currentScaleX = this.transform.localScale.x;
        float initialTime = 0f;
        float totalTime = 0.20f;

        while (initialTime < totalTime)
        {
            initialTime += Time.deltaTime;

            currentScaleX = Mathf.Lerp(1f, 1.50f, cannonCurve.Evaluate(initialTime / totalTime));

            float currentScaleY = 1f - (currentScaleX - 1f);
            currentScaleY = Mathf.Clamp(currentScaleY, 0.80f, 1.20f);
            this.transform.localScale = new Vector3(currentScaleX, currentScaleY, 1f);
            yield return null;
        }

        this.transform.localScale = Vector3.one;
    }
}
