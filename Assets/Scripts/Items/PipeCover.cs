﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeCover : Item
{
    public enum CoverType
    {
        Lid,
        Manhole
    }

    public CoverType coverType;
    public Animated.Animation closeAnimation;
    public Animated.Animation openAnimation;

    private int closeTimer;

    private const int CloseWaitTime = 20;

    public override void Init(Def def)
    {
        base.Init(def);

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        MakeSolid();

        this.animated.PlayOnce(this.closeAnimation);
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if(this.closeTimer > 0)
        {
            this.closeTimer -= 1;

            if(this.closeTimer == 0)
            {
                Close();
            }
        }
    }

    public void Open()
    {
        this.closeTimer = CloseWaitTime;
        this.animated.PlayOnce(this.openAnimation);

        this.spriteRenderer.sortingLayerName = "Spikes";
        this.spriteRenderer.sortingOrder = -15;

        this.hitboxes = new Hitbox[0];
        Audio.instance.StopSfxLoop(Assets.instance.sfxEnvPipesLoop);
    }

    private void Close()
    {
        this.animated.PlayOnce(this.closeAnimation, () =>
        {
            this.spriteRenderer.sortingLayerName = "Items";
            this.spriteRenderer.sortingOrder = 0;
        });

        MakeSolid();
    }

    private void MakeSolid()
    {
        if(this.coverType == CoverType.Lid)
        {
            this.hitboxes = new [] {
                new Hitbox(-1f, 1f, 0f, 1f, this.rotation, Hitbox.Solid, true, true)
            };
        }
        else if(this.coverType == CoverType.Manhole)
        {
            this.hitboxes = new [] {
                new Hitbox(-1f, 1f, -0.15f, 0.15f, this.rotation, Hitbox.Solid, true, true)
            };
        }
    }
}
