
using UnityEngine;
using System.Linq;

public class PlayerBlocker : Item
{
    public Animated.Animation idleAnimation;
    public Animated.Animation swivelForwardAnimation; // enemy moving up or right
    public Animated.Animation swivelBackwardAnimation; // enemy moving down or left
    public bool isHorizontal;

    private float swivelAnimFPS;
    private bool swivelForward;

    private const float DecreaseSpeedForce = 20f;
    private const int MinToIdleAnimFPS = 15;
    private const int MinSwivelAnimFPS = 30;
    private const int MaxSwivelAnimFPS = 90;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid,
                wallSlideAllowedOnLeft: true,
                wallSlideAllowedOnRight: true)
        };
        this.solidToEnemy = false;

        this.swivelAnimFPS = 0;
        this.animated.PlayOnce(this.idleAnimation);
    }

    public override void Advance()
    {
        foreach (var enemy in this.chunk.subsetOfItemsOfTypeEnemy)
        {
            if (enemy == this) continue;
            for (var sri = 0; sri < enemy.hitboxes.Length; sri += 1)
            {
                var rectEnemy = enemy.hitboxes[0].InWorldSpace();
                var rectThis  = this.hitboxes[0].InWorldSpace();

                if (rectEnemy.Overlaps(rectThis))
                {
                    Swivel(enemy);
                }
            }
        }

        if(this.swivelAnimFPS > MinToIdleAnimFPS)
        {
            this.swivelAnimFPS -= DecreaseSpeedForce * Time.deltaTime;

            this.animated.PlayAndLoop(this.swivelForward ?
                this.swivelForwardAnimation : this.swivelBackwardAnimation);

            this.animated.currentAnimation.fps = Mathf.RoundToInt(this.swivelAnimFPS);
        }
        else if(this.animated.frameNumber == 0)
        {
            this.animated.PlayOnce(this.idleAnimation);
            this.animated.currentAnimation.fps = 30;
        }
    }

    private void Swivel(Enemy enemy)
    {
        float enemySpeed =GetEnemySpeed(enemy);

        this.swivelForward = enemySpeed >= 0f;
        this.swivelAnimFPS = Mathf.Lerp(
            MinSwivelAnimFPS, MaxSwivelAnimFPS, Mathf.InverseLerp(0f, 1f, Mathf.Abs(enemySpeed))
        );
    }

    private float GetEnemySpeed(Enemy enemy)
    {
        Vector2 enemyVelocity = Vector2.zero;

        if(enemy is WalkingEnemy)
            enemyVelocity = (enemy as WalkingEnemy).velocity;
        else if(enemy is RobotCopEnemy)
            enemyVelocity = (enemy as RobotCopEnemy).velocity;
        else if(enemy is Spikey)
            enemyVelocity = (enemy as Spikey).velocity;
        else if(enemy is TrunkyBullet)
            enemyVelocity = (enemy as TrunkyBullet).velocity;
        else if(enemy is WildTrunkyBone)
            enemyVelocity = (enemy as WildTrunkyBone).velocity;
        else if(enemy is FireSkull)
            enemyVelocity = (enemy as FireSkull).velocity;

        return this.isHorizontal ? enemyVelocity.x : enemyVelocity.y;
    }
}
