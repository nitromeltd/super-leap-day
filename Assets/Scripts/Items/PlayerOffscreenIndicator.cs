using UnityEngine;
using System;

public class PlayerOffscreenIndicator : MonoBehaviour
{
    [NonSerialized] public GhostPlayer trackingWithGhostPlayer;
    [NonSerialized] public Map map;

    public TMPro.TMP_Text textDistance;
    public SpriteRenderer spriteCircle;
    public SpriteRenderer spriteArrow;
    public SpriteRenderer spriteYolk;
    public SpriteRenderer spriteGoop;
    public SpriteRenderer spriteSprout;
    public SpriteRenderer spritePuffer;
    public SpriteRenderer spriteKing;

    private float alpha = 0;

    public static PlayerOffscreenIndicator Make(Map forMap, GhostPlayer ghostPlayer)
    {
        var go = Instantiate(Assets.instance.playerOffscreenIndicator, forMap.transform);
        var result = go.GetComponent<PlayerOffscreenIndicator>();
        result.Init(forMap, ghostPlayer);
        return result;
    }

    public void Init(Map forMap, GhostPlayer ghostPlayer)
    {
        Util.SetLayerRecursively(this.transform, LayerMask.NameToLayer("UI"), true);

        this.trackingWithGhostPlayer = ghostPlayer;
        this.map = forMap;
    }

    public void Advance()
    {
        var allowedRect = this.map.gameCamera.VisibleRectangle().Inflate(-2);
        allowedRect.yMin += 0.8f;
        if (GameCamera.IsLandscape() == false)
        {
            allowedRect.yMin += 2.5f;
            allowedRect.yMax -= 3f;
        }

        var consideredVisibleRect = this.map.gameCamera.VisibleRectangle().Inflate(1);

        var position = (Vector2)this.trackingWithGhostPlayer.transform.position;
        var positionWithinAllowedRect = new Vector2(
            Mathf.Clamp(position.x, allowedRect.xMin, allowedRect.xMax),
            Mathf.Clamp(position.y, allowedRect.yMin, allowedRect.yMax)
        );

        this.transform.position = IngameUI.instance.PositionForUICameraFromWorldPosition(this.map, positionWithinAllowedRect);

        bool shouldShowIndicator = !consideredVisibleRect.Contains(position);

        if(IngameUI.instance.winMenu?.isOpen == true)
            shouldShowIndicator = false;

        if (this.map.finished == true && this.trackingWithGhostPlayer != null)
        {
            if (this.trackingWithGhostPlayer.trackingPlayer?.map.finished == true)
                shouldShowIndicator = false;
            
            if (this.trackingWithGhostPlayer.trackingMultiplayerPlayerId != null &&
                GameCenterMultiplayer.HasRemotePlayerReachedTheGoldTrophy(
                    this.trackingWithGhostPlayer.trackingMultiplayerPlayerId
                ) == true)
            {
                shouldShowIndicator = false;
            }
        }

        if (this.trackingWithGhostPlayer != null &&
            this.trackingWithGhostPlayer.IsConnected == false)
        {
            shouldShowIndicator = false;
        }

        this.alpha = Util.Slide(this.alpha, shouldShowIndicator ? 1 : 0, 0.05f);

        int distance = Mathf.RoundToInt((position - this.map.player.position).magnitude);
        this.textDistance.text = $"<color=#2b8ae9>{distance}m</color>";
        this.textDistance.alpha = this.alpha;

        var c = new Color(1, 1, 1, this.alpha);
        this.spriteCircle.color = c;

        void UpdateCharacter(Character character, SpriteRenderer sr)
        {
            var shouldBeActive = (character == this.trackingWithGhostPlayer.Character);
            if (sr.gameObject.activeSelf != shouldBeActive)
                sr.gameObject.SetActive(shouldBeActive);
            sr.color = c;
        }
        UpdateCharacter(Character.Yolk,   this.spriteYolk);
        UpdateCharacter(Character.Goop,   this.spriteGoop);
        UpdateCharacter(Character.Sprout, this.spriteSprout);
        UpdateCharacter(Character.Puffer, this.spritePuffer);
        UpdateCharacter(Character.King,   this.spriteKing);

        var outwardVector = position - positionWithinAllowedRect;
        this.spriteArrow.transform.localRotation = Quaternion.Euler(
            0, 0, (Mathf.Atan2(outwardVector.y, outwardVector.x) * 180 / Mathf.PI) - 90
        );
        this.spriteArrow.transform.localPosition = outwardVector.normalized * 1.08f;
        this.spriteArrow.color = new Color(
            this.spriteArrow.color.r,
            this.spriteArrow.color.g,
            this.spriteArrow.color.b,
            this.alpha
        );

        this.textDistance.transform.localPosition = new Vector2(
            0, Mathf.Min(-1.4f, (outwardVector.normalized.y * 1.08f) - 0.8f)
        );
    }
}
