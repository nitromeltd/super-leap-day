
using UnityEngine;
using System;
using System.Collections.Generic;

public class Portal : Item
{
    public class Range
    {
        public Portal[] portals;
        public Portal leader;
        public Player.Orientation orientation;
        public Range other;

        public Vector2 outDirection;
        public Vector2 acrossDirection;

        public override string ToString() =>
            "portal range " +
            $"({portals[0].def.tx}, {portals[0].def.ty}) -> " +
            $"({portals[^1].def.tx}, {portals[^1].def.ty})";
    }
    public Range range;
    public Animated.Animation lightAnimation;
    public Animated.Animation lightAnimation2;

    [Header("Teleporter")]
    public SpriteRenderer teleporterSR;
    public Sprite leftTeleporter;
    public Sprite middleTeleporter;
    public Sprite rightTeleporter;

    [Header("Glow")]
    public SpriteRenderer glowSR;
    public Sprite leftGlow;
    public Sprite middleGlow;
    public Sprite rightGlow;

    [Header("Particles")]
    public Sprite[] particles;
    public AnimationCurve particleFadeOutCurve;
    
    [Header("White")]
    public Animated animatedWhiteEffect;
    public Animated.Animation emptyAnimation;
    public Animated.Animation whiteAnimation;

    private int particleRatioOffset;

    private const int ParticleRatio = 25;

    public override void Init(Def def)
    {
        base.Init(def);

        this.FindRange();
        this.FindOtherRange();

        this.hitboxes = new [] {
            new Hitbox(
                -8/16f, 8/16f, -8/16f, 8/16f, this.rotation, Hitbox.Solid
            )
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        int index = Array.IndexOf(range.portals, this);
        this.animated.PlayAndLoop(index % 2 == 0 ? this.lightAnimation : this.lightAnimation2);

        Sprite teleporterSprite, glowSprite;

        if(index == 0)
        {
            teleporterSprite = leftTeleporter;
            glowSprite = leftGlow;
        }
        else if(index == range.portals.Length - 1)
        {
            teleporterSprite = rightTeleporter;
            glowSprite = rightGlow;
        }
        else
        {
            teleporterSprite = middleTeleporter;
            glowSprite = middleGlow;
        }

        this.teleporterSR.sprite = teleporterSprite;
        this.glowSR.sprite = glowSprite;

        this.particleRatioOffset = UnityEngine.Random.Range(0, ParticleRatio);
        
        this.animatedWhiteEffect.PlayOnce(this.emptyAnimation);
    }

    public void FindRange()
    {
        if (this.range != null) return;

        XY Along(XY xy, int distance) => this.def.tile.rotation switch
        {
              0 => new XY(xy.x + distance, xy.y),
             90 => new XY(xy.x, xy.y + distance),
            180 => new XY(xy.x - distance, xy.y),
              _ => new XY(xy.x, xy.y - distance)
        };
        XY Left (XY xy) => Along(xy, -1);
        XY Right(XY xy) => Along(xy, +1);

        var portals = new List<Portal> { this };

        XY first, last;
        first = last = new XY(this.def.tx, this.def.ty);

        while (true)
        {
            first = Left(first);
            var prevPortal = this.chunk.PortalAt(first.x, first.y);
            if (prevPortal?.def.tile.rotation == this.def.tile.rotation)
                portals.Insert(0, prevPortal);
            else
                break;
        }
        while (true)
        {
            last = Right(last);
            var nextPortal = this.chunk.PortalAt(last.x, last.y);
            if (nextPortal?.def.tile.rotation == this.def.tile.rotation)
                portals.Add(nextPortal);
            else
                break;
        }

        for (int n = 0; n < portals.Count; n += 1)
        {
            if (portals[n].range != null)
            {
                this.range = portals[n].range;
                return;        
            }
        }

        Player.Orientation orientation = this.def.tile.rotation switch
        {
              0 => Player.Orientation.Normal,
             90 => Player.Orientation.RightWall,
            180 => Player.Orientation.Ceiling,
              _ => Player.Orientation.LeftWall
        };

        Vector2 outDirection = orientation switch
        {
            Player.Orientation.RightWall => Vector2.left,
            Player.Orientation.Ceiling   => Vector2.down,
            Player.Orientation.LeftWall  => Vector2.right,
            _                            => Vector2.up
        };
        Vector2 acrossDirection = orientation switch
        {
            Player.Orientation.RightWall => Vector2.up,
            Player.Orientation.Ceiling   => Vector2.left,
            Player.Orientation.LeftWall  => Vector2.down,
            _                            => Vector2.right
        };
        float acrossMin = Vector2.Dot(acrossDirection, portals[0].position) - 0.5f;
        float acrossMax = Vector2.Dot(acrossDirection, portals[^1].position) + 0.5f;

        this.range = new Range
        {
            portals = portals.ToArray(),
            leader = portals[0],
            orientation = orientation,
            outDirection = outDirection,
            acrossDirection = acrossDirection
        };
    }

    private Rect RectangleOfPortalRange()
    {
        var firstPosition = this.range.portals[0].position;
        var lastPosition = this.range.portals[^1].position;
        var rect = new Rect();
        rect.xMin = Mathf.Min(firstPosition.x, lastPosition.x) - 0.5f;
        rect.xMax = Mathf.Max(firstPosition.x, lastPosition.x) + 0.5f;
        rect.yMin = Mathf.Min(firstPosition.y, lastPosition.y) - 0.5f;
        rect.yMax = Mathf.Max(firstPosition.y, lastPosition.y) + 0.5f;
        return rect;
    }

    public void FindOtherRange()
    {
        if (this.range.other != null) return;

        NitromeEditor.Path.Node nodeAtOtherEnd = null;

        var rangeRect = RectangleOfPortalRange().Inflate(0.01f);
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            foreach (var node in path.nodes)
            {
                if (rangeRect.Contains(node.Position))
                {
                    nodeAtOtherEnd =
                        (node == path.nodes[0]) ? path.nodes[^1] : path.nodes[0];
                    goto found;
                }
            }
        }
        found:

        if (nodeAtOtherEnd == null) return;

        var otherPortal = this.chunk.NearestItemTo<Portal>(nodeAtOtherEnd.Position);
        if (otherPortal == null) return;
        if (otherPortal.range == null)
            otherPortal.FindRange();

        this.range.other = otherPortal.range;
        this.range.other.other = this.range;
        // Debug.Log(this.range + " connected to " + this.range.other);
    }

    private Player.Orientation Invert(Player.Orientation o) => o switch
    {
        Player.Orientation.RightWall => Player.Orientation.LeftWall,
        Player.Orientation.Ceiling   => Player.Orientation.Normal,
        Player.Orientation.LeftWall  => Player.Orientation.RightWall,
        _                            => Player.Orientation.Ceiling,
    };

    private bool ArePortalsFacingEachOther() =>
        this.range.orientation == this.range.other.orientation ||
        this.range.orientation == Invert(this.range.other.orientation);

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if((this.map.frameNumber + this.particleRatioOffset) % ParticleRatio == 0)
        {
            EmitParticles();
        }

        if (this.range.leader != this) return;
        if (this.range.other == null) return;

        var player = this.map.player;
        var center = this.position.Add(0.5f, 0.5f);
        if (RectangleOfPortalRange().Overlaps(player.Rectangle()) &&
            //this.range.rect.Contains(player.position) &&
            // Vector2.Dot(this.range.outDirection, player.position - center) < 6/16f &&
            Vector2.Dot(this.range.outDirection, player.velocity) < 0)
        {
            Vector2 pointOfDeparture = OtherEndPosition(player.position);
            Vector2 departureVelocity = OtherEndVelocity(player.velocity, 0.2f);

            player.position = pointOfDeparture;
            player.velocity = departureVelocity;
            player.groundState = Player.GroundState.Airborne(player);
            player.portalsEnteredInARowWhileAirborne += 1;
            if (player.velocity.x > 0) player.facingRight = true;
            if (player.velocity.x < 0) player.facingRight = false;
            
            LightUp();
            this.range.other.leader.LightUp();
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPortal,
                position: this.position
            );
        }
    }

    public bool IsRayEnteringPortal(Vector2 mapPosition, Vector2 normalizedDirection)
    {
        if (this.range.other == null)
            return false;

        if (this.hitboxes[0].InWorldSpace().Inflate(0.01f).Contains(mapPosition))
            return true;

        return Vector2.Dot(this.range.outDirection, normalizedDirection) < -0.999f;
    }

    public Vector2 OutDirection() => this.range.outDirection;

    public Vector2 OtherEndPosition(Vector2 inPosition)
    {
        var firstPortal = this.range.portals[0];
        var lastPortal = this.range.portals[^1];
        var acrossDirection = this.range.acrossDirection;

        float min = Vector2.Dot(firstPortal.position, acrossDirection) - 0.5f;
        float max = Vector2.Dot(lastPortal.position, acrossDirection) + 0.5f;
        float playerAcross = Vector2.Dot(inPosition, acrossDirection);
        float s = (playerAcross - min) / (max - min);
        if (float.IsNaN(s)) s = 0;

        if (ArePortalsFacingEachOther() == true)
            s = 1 - s;

        float outAlong = s * this.range.other.portals.Length;
        outAlong = Mathf.Clamp(
            outAlong, 0.5f, this.range.other.portals.Length - 0.5f
        );
        Vector2 pointOfDeparture =
            this.range.other.portals[0].position +
            (this.range.other.outDirection * 0.8f) +
            (this.range.other.acrossDirection * (outAlong - 0.5f));
        return pointOfDeparture;
    }

    public Vector2 OtherEndVelocity(Vector2 inVelocity, float minimumExitSpeed = 0)
    {
        float inSpeed = Vector2.Dot(inVelocity, -this.range.outDirection);
        float acrossSpeed = Vector2.Dot(inVelocity, this.range.acrossDirection);
        float outSpeed = inSpeed;
        if (outSpeed < minimumExitSpeed)
            outSpeed = minimumExitSpeed;
        if (ArePortalsFacingEachOther() == true)
            acrossSpeed *= -1;

        return
            (this.range.other.outDirection * outSpeed) +
            (this.range.other.acrossDirection * acrossSpeed);
    }

    public void LightUp()
    {
        foreach(var portalBit in range.portals)
        {
            portalBit.animatedWhiteEffect.PlayOnce(portalBit.whiteAnimation, () => 
            {
                portalBit.animatedWhiteEffect.PlayOnce(portalBit.emptyAnimation);
            });
        }


        
    }

    private void EmitParticles()
    {
        float size = 0.40f;
        Vector2 randomPosition = new Vector2(
            UnityEngine.Random.Range(-size, size),
            UnityEngine.Random.Range(-size, size)
        );

        Particle particle = Particle.CreateWithSprite(
            this.particles[UnityEngine.Random.Range(0, this.particles.Length)],
            18,
            this.position + randomPosition,
            this.transform.parent
        );

        float speed = 0.08f;
        particle.velocity = this.range.outDirection * speed;

        particle.FadeOut(this.particleFadeOutCurve);

        particle.spriteRenderer.sortingLayerName = "Warps";
        particle.spriteRenderer.sortingOrder = -5;
    }
}
