using UnityEngine;

public class PowerupHeart : Pickup
{
    public Animated.Animation animationShine;
    public Animated.Animation animationSpin;

    private int collectSparkleTime;
    private float sparkleOffset;
    private float velocityY;

    private int shineTimer = 0;

    private Transform moveTowards;

    public override void Init(Def def)
    {
        base.Init(def);

        this.sparkleOffset = 0.75f;
        this.hitboxSize = 0.5f;

        this.hitboxes = new[] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayOnce(this.animationSpin, () => {
            this.animated.PlayOnce(this.animationSpin, () => {
                this.animated.PlayAndHoldLastFrame(this.animationNormal);
            });
        });
    }

    public static PowerupHeart Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.PowerupHeart, chunk.transform);
        obj.name = "Spawned Coin";
        var item = obj.GetComponent<PowerupHeart>();
        item.Init(position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;

        this.gravityAcceleration = 0.015f;
        this.gravityAccelerationUnderwater = 0.0075f;
        this.bounciness = -0.25f;
    }

    override public void Advance()
    {
        base.Advance();

        if (this.collected == false)
        {
            this.velocityY = this.velocity.y;
            ApplyDragOnFloor();
            Shine();
            bool createSparkleParticle = this.map.frameNumber % 6 == 0;

            if (createSparkleParticle == true)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-this.sparkleOffset, this.sparkleOffset),
                        Random.Range(-this.sparkleOffset, this.sparkleOffset)
                    ),
                    this.transform.parent
                );
                sparkle.spriteRenderer.sortingLayerName = "Items";
                sparkle.spriteRenderer.sortingOrder = -1;
            }
            else if (this.collectSparkleTime > 0)
            {
                this.collectSparkleTime -= 1;

                if (Random.Range(0, 10) > 2)
                {
                    var sparkle = Particle.CreateAndPlayOnce(
                        Assets.instance.coinSparkleAnimation,
                        this.map.player.position.Add(
                            Random.Range(-this.sparkleOffset, this.sparkleOffset),
                            Random.Range(-this.sparkleOffset, this.sparkleOffset)
                        ),
                        this.map.player.transform.parent
                    );
                    sparkle.spriteRenderer.sortingLayerName = "Items";
                    sparkle.spriteRenderer.sortingOrder = -1;
                }
            }

        }

        if (this.collected == true && this.collectSparkleTime > 0)
        {
            this.velocityY = Util.Slide(this.velocityY, 0.07f, 0.008f);
            this.position += this.velocityY * this.gravity.Up();

            this.transform.position = new Vector3(
                this.position.x, this.position.y, this.z
            );
        }
    }

    private void Shine()
    {
        if (this.shineTimer > 0)
        {
            this.shineTimer--;
        }
        else
        {
            this.shineTimer = 60 * 3;
            if(this.collected == false)
            {
                if(Random.value < 0.4f)
                {
                    this.animated.PlayOnce(this.animationShine, () => {
                        this.animated.PlayAndHoldLastFrame(this.animationNormal);
                    });
                }
                else
                {
                    this.animated.PlayOnce(this.animationSpin, () => {
                        this.animated.PlayAndHoldLastFrame(this.animationNormal);
                    });
                }
            }
        }
    }

    public override void Collect()
    {
        if (this.collected == true) return;

        this.collected = true;

        if (this.animated == true)
        {
            this.animated.PlayOnce(this.animationCollect, delegate
            {
                this.spriteRenderer.enabled = false;
                
                if(this.map.player.petYellow.needsHeart == true)
                {
                    this.map.player.petYellow.AddHearts();
                }
                else if (this.map.player.petTornado.needsHeart == true)
                {
                    this.map.player.petTornado.AddHearts();
                }
                else if (this.map.player.infiniteJump.needsHeart == true && this.map.player.infiniteJump.activatedByReferee == false)
                {
                    this.map.player.infiniteJump.AddHearts();
                }
                else if (this.map.player.midasTouch.needsHeart == true)
                {
                    this.map.player.midasTouch.AddHearts();
                }
            });
        }

        this.collectSparkleTime = 120;
        Audio.instance.PlaySfx(Assets.instance.sfxCoin, position: this.position, options: new Audio.Options(0.9f, true, 0.1f));
        this.velocityY = 0;
    }
}
