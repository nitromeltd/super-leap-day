﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PowerupPickup : Pickup
{
    public enum Type
    {
        None,

        Invincibility,
        PetYellow,
        PetTornado,
        Shield,
        SwordAndShield,
        InfiniteJump,
        MidasTouch,
        MultiplayerBaseball,
        MultiplayerInk,
        MultiplayerMines,
        MultiplayerMinesTap,
        BubbleInvincibility,
        BubbleShield,
        BubblePetYellow,
        BubblePetTornado,
        BubbleSwordAndShield,
        BubbleMidasTouch,
        BubbleInfiniteJump,
        BubbleMultiplayerBaseball,
        BubbleMultiplayerInk,
        BubbleMultiplayerMines,
        Length
    }

    [Header("Icon")]
    public SpriteRenderer iconSprite;
    public Sprite starIcon;
    public Sprite petYellowIcon;
    public Sprite petTornadoIcon;
    public Sprite shieldIcon;
    public Sprite wingIcon;
    public Sprite midasTouch;
    public Sprite swordAndShield;
    public Sprite baseball;
    public Sprite ink;
    public Sprite mines;
    public Sprite minesTap;

    [Header("Shine")]
    public Animated.Animation animationSpin;
    public Animated shineAnimated;
    public Animated.Animation animationShineBlank;
    public Animated.Animation animationShine;

    private Type powerupType;
    
    // shine animation
    private int spinShineTimer = 0;
    private const int SpinShineTime = 60 * 3;

    // collect animation
    private int collectAnimTime;

    // movement
    [HideInInspector] public Vector2 initialPos;
    [HideInInspector] public Vector2 targetPos;
    private float breatheTimer;
    private float timerBlender = 0;
    private bool breatheMove = false;
    [HideInInspector] public bool isInBlender = false;

    private const float BreathePeriod = .5f;
    private const float BreatheAmplitude = .3f;
    //private const int FirstPowerup = (int)Type.Invincibility;

    private SpriteRenderer[] allSpriteRenderers;

    public AnimationCurve speedCurve;

    public static List<Type> PowerUpPickups = new List<Type>()
    {
        Type.Invincibility,
        Type.Shield,
        Type.PetYellow,
        Type.PetTornado
    };

    public static List<Type> BubblePickups = new List<Type>()
    {
        Type.BubbleInvincibility,
        Type.BubbleShield,
        Type.BubblePetYellow,
        Type.BubblePetTornado,
    };

    public static List<Type> MultiplayerPowerups = new List<Type>()
    {
        Type.MultiplayerBaseball,
        Type.MultiplayerInk,
        Type.MultiplayerMinesTap,
    };

    public static List<Type> BubbleMultiplayerPowerups = new List<Type>()
    {
        Type.BubbleMultiplayerBaseball,
        Type.BubbleMultiplayerInk,
        Type.BubbleMultiplayerMines,
    };

    public const float MultiplayerPowerupChance = 0.50f;

    private void Awake()
    {
        allSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();
    }
    
    public static PowerupPickup CreateRandomPowerup(Vector2 position, Chunk chunk)
    {
        List<Type> UpdatedPowerUps = PowerupPickup.PowerUpPickups;

        if (SaveData.IsInfiniteJumpPurchased() == true)
        {
            UpdatedPowerUps = UpdatedPowerUps.Append(PowerupPickup.Type.InfiniteJump).ToList();
        }
        if (SaveData.IsSwordAndShieldPurchased() == true)
        {
            UpdatedPowerUps = UpdatedPowerUps.Append(PowerupPickup.Type.SwordAndShield).ToList();
        }
        if (SaveData.IsMidasTouchPurchased() == true)
        {
            UpdatedPowerUps = UpdatedPowerUps.Append(PowerupPickup.Type.MidasTouch).ToList();
        }

        return Create(position, chunk, UpdatedPowerUps[Random.Range(0 , UpdatedPowerUps.Count)]);
    }

    public static PowerupPickup CreateRandomMultiplayerPowerup(Vector2 position, Chunk chunk)
    {
        int multiplayerPowerupIndex = Random.Range(0 , MultiplayerPowerups.Count);
        Type multiplayerPowerup = MultiplayerPowerups[multiplayerPowerupIndex];

        return Create(position, chunk, multiplayerPowerup);
    }

    public static PowerupPickup Create(Vector2 position, Chunk chunk, PowerupPickup.Type type)
    {
        var obj = Instantiate(Assets.instance.powerupStar, chunk.transform);
        obj.name = "Spawned PowerupStar";
        var item = obj.GetComponent<PowerupPickup>();
        item.Init(position, chunk, type);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, PowerupPickup.Type type)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;

        this.powerupType = type;

        switch(this.powerupType)
        {
            case Type.Invincibility:
                this.iconSprite.sprite = this.starIcon;
            break;

            case Type.PetYellow:
                this.iconSprite.sprite = this.petYellowIcon;
            break;

            case Type.PetTornado:
                this.iconSprite.sprite = this.petTornadoIcon;
            break;

            case Type.Shield:
                this.iconSprite.sprite = this.shieldIcon;
            break;

            case Type.SwordAndShield:
                this.iconSprite.sprite = this.swordAndShield;
                break;

            case Type.InfiniteJump:
                this.iconSprite.sprite = this.wingIcon;
            break;

            case Type.MidasTouch:
                this.iconSprite.sprite = this.midasTouch;
            break;

            case Type.MultiplayerBaseball:
                this.iconSprite.sprite = this.baseball;
                break;

            case Type.MultiplayerInk:
                this.iconSprite.sprite = this.ink;
                break;

            case Type.MultiplayerMines:
                this.iconSprite.sprite = this.mines;
                break;

            case Type.MultiplayerMinesTap:
                this.iconSprite.sprite = this.minesTap;
                break;
        }

        this.gravityAcceleration = 0.015f;
        this.gravityAccelerationUnderwater = 0.0075f;
        this.bounciness = -0.5f;
        
        this.spinShineTimer = SpinShineTime;
        this.initialPos = this.targetPos = this.position + (Vector2)this.transform.up * 1f;

        this.animated.PlayAndHoldLastFrame(animationNormal);

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        
        this.hitboxSize = 0.75f;
        this.hitboxes = new [] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        base.Advance();
        
        if(this.collected == false &&
            this.suckActive == false)
        {
            BreatheMovement();
            Shine();
            ApplyDragOnFloor();
            
            if (this.map.frameNumber % 6 == 0)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-1f, 1f),
                        Random.Range(-1f, 1f)
                    ),
                    this.transform.parent
                );
            }
        }

        this.collectAnimTime = (this.collectAnimTime > 0) ? collectAnimTime - 1 : 0;

        if (this.collected == true && this.collectAnimTime > 0)
        {
            this.velocity.y = Util.Slide(this.velocity.y, 0.075f, 0.008f);
        }

        if(this.movingFreely == false &&
            this.collected == false &&
            this.suckActive == false)
        {
            this.position += this.velocity;
            this.transform.position = this.position;
        }
    }

    private void BreatheMovement()
    {
        if(this.movingFreely == true) return;

        if(this.breatheMove == true)
        {
            this.breatheTimer += Time.deltaTime;
        }

        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);

        if(this.breatheMove == true)
        {
            this.targetPos = this.initialPos + (Vector2)this.transform.up * distance;
        }

        Vector2 targetVel;
        if (this.isInBlender)
        {
            targetVel = (targetPos - this.position) * .25f;
            this.timerBlender += Time.deltaTime;
        }
        else
        {
            targetVel = (targetPos - this.position) * .05f;
        }

        this.velocity *= 0.95f;
        this.velocity += targetVel * .2f;

        if(this.timerBlender < 0.3 && this.isInBlender)
        {
            this.transform.localScale = Vector3.one * this.timerBlender / 0.3f;
        }
        else
        {
            this.transform.localScale = Vector3.one;
        }
    }

    private void Shine()
    {
        if(this.spinShineTimer > 0)
        {
            this.spinShineTimer--;
        }
        else
        {
            this.spinShineTimer = SpinShineTime;
            this.animated.PlayOnce(this.animationSpin, () => 
            {
                if(this.collected == false)
                {
                    this.animated.PlayAndHoldLastFrame(this.animationNormal);
                    this.shineAnimated.PlayOnce(this.animationShine);
                }
            });
        }

        if(this.animated.currentAnimation == this.animationSpin)
        {
            float yRotValue = Mathf.Lerp(0, 360f,
                Mathf.InverseLerp(0, this.animationSpin.sprites.Length, this.animated.frameNumber));
            this.iconSprite.transform.localRotation = Quaternion.Euler(Vector3.up * yRotValue); 
            
            this.iconSprite.gameObject.SetActive(this.animated.frameNumber < 7 || this.animated.frameNumber > 17);
        } 
    }

    public override void Collect()
    {
        if(this.collected == true) return;

        this.collected = true;

        this.collectAnimTime = 120;
        this.velocity.y = .02f;

        this.shineAnimated.PlayAndHoldLastFrame(animationShineBlank);
        this.iconSprite.transform.localRotation = Quaternion.identity;
        this.iconSprite.gameObject.SetActive(true);

        Audio.instance.PlaySfx(Assets.instance.sfxPowerupCollect, position: this.position);

        this.animated.PlayOnce(this.animationCollect, delegate
        {
            this.spriteRenderer.enabled = false;
        });

        StartCoroutine(PowerUpMove());
        IngameUI.instance.MoveTransformToUILayer(this.map, this.iconSprite.transform);
    }

    private IEnumerator PowerUpMove()
    {
        Transform powerup = this.iconSprite.transform;

        float timer = 0f;
        while (timer < .5)
        {
            timer += Time.deltaTime;

            CreateSparkleParticles();

            float x = Mathf.Lerp(1, 1.3f, timer/0.5f);
            powerup.localScale = new Vector3(x, x, 1);
            yield return null;
        }

        powerup.localScale = new Vector3(1, 1, 1);
        Vector3 initialPosition = powerup.position;
        Vector3 targetPosition = powerup.position + new Vector3(0,1,0);

        timer = 0f;

        while (timer < .5)
        {
            timer += Time.deltaTime;

            CreateSparkleParticles();

            powerup.position = Vector2.Lerp(initialPosition, targetPosition, timer / 0.5f);
            yield return null;
        }

        float minMoveSpeed = 5f * Time.deltaTime;
        float maxMoveSpeed = 30f * Time.deltaTime;
        float reachMaxSpeedTime = 0.50f;

        timer = 0f;

        bool moving = true;
        while(moving == true)
        {
            if(IngameUI.instance.pauseMenu.isOpen == false)
            {
                timer += Time.deltaTime;

                CreateSparkleParticles();

                moving = false;

                Transform targetTransform = IngameUI.instance.PowerUpImageRectTransform(this.map);

                float t = Mathf.InverseLerp(0f, reachMaxSpeedTime, timer);
                float currentMoveSpeed =
                    Mathf.Lerp(minMoveSpeed, maxMoveSpeed, this.speedCurve.Evaluate(t));

                Vector2 currentPosition = powerup.position;
                Vector2 targetWorldPosition = targetTransform.position + new Vector3(0, 0.2f, 0);
                Vector3 heading = targetWorldPosition - currentPosition;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                if (distance > currentMoveSpeed)
                {
                    powerup.position += direction * currentMoveSpeed;
                    moving = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        this.map.CollectPowerup(this.powerupType);
        // this.map.ScreenShakeAtPosition(IngameUI.instance.PowerUpImageRectTransform().position, Vector2.one * 0.7f);
        var shakePos = (Vector2)this.map.gameCamera.Camera.ScreenToWorldPoint(IngameUI.instance.PowerUpImageRectTransform(this.map).position) + new Vector2(0, 0.2f);
        this.map.ScreenShakeAtPosition(shakePos, Vector2.one * 0.7f);

        StartCoroutine(IngameUI.instance.TintFlashPowerupBox(this.map));
        StartCoroutine(IngameUI.instance.ShakePowerupBox(this.map));

        this.iconSprite.enabled = false;

        Audio.instance.PlaySfx(Assets.instance.sfxPowerupSlot);
    }

    private void CreateSparkleParticles()
    {
        float sparkleOffset = 0.5f;

        bool createSparkleParticle = this.map.frameNumber % 2 == 0;

        Vector2 pos = IngameUI.instance.PositionForWorldFromUICameraPosition(
            map, this.iconSprite.transform.position
        );

        if (createSparkleParticle == true)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                pos.Add(
                    UnityEngine.Random.Range(-sparkleOffset, sparkleOffset),
                    UnityEngine.Random.Range(-sparkleOffset, sparkleOffset)
                ),
                this.transform.parent
            );
            sparkle.spriteRenderer.sortingLayerName = "Items (Front)";
            sparkle.spriteRenderer.sortingOrder = 2;
        }
    }
}
