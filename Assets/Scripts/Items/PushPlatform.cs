﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PushPlatform : Item
{
    public Animated.Animation animationStatic;
    public Animated.Animation animationUp;
    public Animated.Animation animationDown;
    
    private int correctionTime;
    private Vector2 correction;

    private int disableBounceTime = 0;

    public const float VerticalForce = 1f;
    public const float HorizontalForce = 0.50f;

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.flip == true)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        }

        this.animated.PlayOnce(this.animationStatic);
        SetHitbox();
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void SetHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, 0, 1f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if (this.disableBounceTime > 0)
            this.disableBounceTime -= 1;

        var player = this.map.player;
        var thisRect = this.hitboxes[0].InWorldSpace();
        var collision =
            player.ShouldCollideWithItems() &&
            player.Rectangle().Overlaps(thisRect);

        if (this.correctionTime > 0)
        {
            this.correctionTime -= 1;
            if (this.correctionTime < 1)
            {
                if (player.state == Player.State.SpringFromPushPlatform)
                    player.velocity += this.correction;

                this.correction = new Vector2();

                if(Mathf.Abs(player.velocity.x) < 0.05f)
                {
                    player.velocity.x = 0f;
                }

                if(Mathf.Abs(player.velocity.y) < 0.05f)
                {
                    player.velocity.y = 0f;
                }
            }
        }

        if (this.disableBounceTime == 0 && collision == true)
            Bounce();
            

        foreach (var enemy in this.chunk.subsetOfItemsOfTypeWalkingEnemy)
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.velocity = VelocityAfterSpringing(enemy.velocity);
            }
        }
        foreach (var enemy in this.chunk.items.OfType<Spikey>())
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.BounceFromSpring(VelocityAfterSpringing(enemy.Velocity));
            }
        }
        foreach (var enemy in this.chunk.items.OfType<TurtleCannon>())
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.BounceFromSpring(VelocityAfterSpringing(enemy.Velocity));
            }
        }
        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if (minecart.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                minecart.BounceFromPushPlatform(VelocityAfterSpringing(minecart.velocity), new Vector2((this.position - minecart.position).x,0), 60);
            }
        }
    }

    public void PlayBounceAnimation()
    {
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSpringGreen,
            position: this.position
        );
        this.animated.PlayOnce(this.animationUp, delegate
        {
            this.animated.PlayOnce(this.animationDown);
        });
    }
    
    public Vector2 VelocityAfterSpringing(Vector2 before)
    {
        switch (this.rotation)
        {
            case 0:    return new Vector2(0f, VerticalForce);
            case 90:   return new Vector2(-HorizontalForce, 0f);
            case 180:  return new Vector2(0f, -VerticalForce);
            case 270:  return new Vector2(HorizontalForce, 0f);
        }

        return before;
    }

    private void Bounce()
    {
        this.disableBounceTime = 10;

        PlayBounceAnimation();

        var player = this.map.player;
        player.CancelLateJumpTime();
        player.ResetDoubleJump();
        player.velocity = VelocityAfterSpringing(player.velocity);

        var offset = player.position - (Vector2)this.transform.position;

        if (player.state == Player.State.WallSlide ||
            player.HasWallOnSide(player.facingRight, 0.5f))
        {
            player.facingRight = !player.facingRight;
            player.velocity.x =
                Mathf.Abs(player.velocity.x) * (player.facingRight ? 1 : -1);
        }
        
        player.state = Player.State.SpringFromPushPlatform;
        player.groundState = Player.GroundState.Airborne(player);
        player.position += player.velocity * 0.3f;

        {
            const int Duration = 3;

            Vector2 parallel = player.velocity.normalized;
            Vector2 perpendicular = new Vector2(parallel.y, -parallel.x);
            Vector2 correction = perpendicular * Vector2.Dot(-offset, perpendicular);

            player.velocity += correction / Duration;
            this.correction = -correction / Duration;
            this.correctionTime = Duration;
        }
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox();
    }
}
