using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSign : Item
{
    public Animated.Animation animationLeftIdle;
    public Animated.Animation animationRightIdle;
    public Animated.Animation animationCeilingIdle;
    public Animated.Animation animationGroundIdle;

    public Animated.Animation animationLeftSpin;
    public Animated.Animation animationRightSpin;
    public Animated.Animation animationCeilingSpin;
    public Animated.Animation animationGroundSpin;

    public Animated.Animation animationShine;

    private enum Orientation
    {
        Right,
        Left,
        Ground,
        Ceiling
    }

    private Orientation orientation;

    private bool isSpinning;
    public SpriteRenderer shine;
    private float shineTimer = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        if(def.tile.rotation == 90)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            this.animated.PlayAndLoop(animationRightIdle);
            this.orientation = Orientation.Right;
            this.shine.transform.localPosition = new Vector3(-1.65f, 0.08f, 0);
        }
        else if(def.tile.rotation == 270)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            this.animated.PlayAndLoop(animationLeftIdle);
            this.orientation = Orientation.Left;
            this.shine.transform.localPosition = new Vector3(1.65f, 0.08f, 0);
        }
        else if (def.tile.rotation == 180)
        {
            transform.localRotation = Quaternion.Euler(-1.65f, 0, 0);
            this.animated.PlayAndLoop(animationCeilingIdle);
            this.orientation = Orientation.Ceiling;
            this.shine.transform.localPosition = new Vector3(0, -1.32f, 0);
        }
        else
        {
            this.animated.PlayAndLoop(animationGroundIdle);
            this.orientation = Orientation.Ground;
            this.shine.transform.localPosition = new Vector3(0, 1.48f, 0);
        }

        this.hitboxes = new[] {
            new Hitbox(-1f, 1f, 0.5f, 2f, this.rotation, Hitbox.NonSolid)
        };

        this.shineTimer = 0;
        shine.enabled = false;
        this.isSpinning = false;
    }

    public override void Advance()
    {
        base.Advance();

        this.shineTimer += Time.deltaTime;
        if(this.shineTimer > 4 && this.isSpinning == false)
        {
            this.shineTimer = 0;
            this.shine.enabled = true;
            this.shine.GetComponent<Animated>().PlayOnce(animationShine, () =>
            {
                this.shine.enabled = false;
            });
        }

        var thisRect = this.hitboxes[0].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect) && this.isSpinning == false)
        {
            this.shine.enabled = false;
            this.isSpinning = true;
            if (this.orientation == Orientation.Right)
            {
                this.animated.PlayOnce(animationRightSpin, () =>
                {
                    this.animated.PlayOnce(animationRightSpin, () =>
                    {
                        this.animated.PlayAndLoop(animationRightIdle);
                        this.isSpinning = false;
                    });
                });
            }
            else if (this.orientation == Orientation.Left)
            {
                this.animated.PlayOnce(animationLeftSpin, () =>
                {
                    this.animated.PlayOnce(animationLeftSpin, () =>
                    {
                        this.animated.PlayAndLoop(animationLeftIdle);
                        this.isSpinning = false;
                    });
                });
            }
            else if (this.orientation == Orientation.Ground)
            {
                this.animated.PlayOnce(animationGroundSpin, () =>
                {
                    this.animated.PlayOnce(animationGroundSpin, () =>
                    {
                        this.animated.PlayAndLoop(animationGroundIdle);
                        this.isSpinning = false;
                    });
                });
            }
            else
            {
                this.animated.PlayOnce(animationCeilingSpin, () =>
                {
                    this.animated.PlayOnce(animationCeilingSpin, () =>
                    {
                        this.animated.PlayAndLoop(animationCeilingIdle);
                        this.isSpinning = false;
                    });
                });
            }
        }

    }
}