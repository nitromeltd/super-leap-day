using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raft : Item
{
    public int width;

    [HideInInspector] public Vector2 velocity;
    private Vector2 initialPosition;
    private bool insideWater = false;
    private bool sinkEffect = false;
    private float currentAngle;
    private float angleVelocity;
    private float motorSpeed;
    private bool isPlayerFarEnough;

    protected const float GravityAcceleration = 0.006f;
    private const float WaterBuoyancy = 0.0025f;
    private const float HitboxTolerance = 0.05f;

    public bool IsInsideWater => this.insideWater;

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        float halfWidth = (float)this.width / 2f;

        this.hitboxes = new [] {
            new Hitbox(-halfWidth, halfWidth, -0.5f, 0.5f, this.rotation, Hitbox.Solid)
        };

        this.initialPosition = this.position;

        this.onCollisionFromAbove = OnCollisionFromAbove;
    }
    
    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;

        this.velocity.x = Util.Slide(this.velocity.x, this.motorSpeed, 0.003f);

        AdvanceWater();
        Integrate();

        {
            // spring-like motion for rotation
            float targetVelocity = -this.currentAngle;

            this.angleVelocity *= 0.80f;
            this.angleVelocity += targetVelocity * 0.05f;
            this.currentAngle += this.angleVelocity;
        }
        
        this.transform.position = new Vector3(this.position.x, this.position.y, 0f);
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.currentAngle);

        this.motorSpeed = 0f;
    }

    public override void Reset()
    {
        this.position = this.initialPosition;
    }

    private void AdvanceWater()
    {
        Vector2 toPlayer = this.map.player.position - this.position;
        this.isPlayerFarEnough =
            Mathf.Abs(toPlayer.x) > (float)this.width * 0.90f || Mathf.Abs(toPlayer.y) > 2f;

        if(this.sinkEffect == true && isPlayerFarEnough)
        {
            this.sinkEffect = false;
        }

        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }

        if(this.insideWater == true)
        {
            float targetPositionY = waterline.currentLevel;
            Vector2 targetPosition = new Vector2(this.position.x, targetPositionY);
            Vector2 heading = targetPosition - this.position;
            float distance = heading.magnitude;

            if(waterline.IsMoving == true)
            {
                if(distance < waterline.VelocityThreshold * 2f)
                {
                    // sync with waterline velocity
                    this.velocity.y = waterline.Velocity().y;
                }
                else
                {
                    // raft is submerged, raise with buoyancy-like physics
                    this.velocity += Vector2.up * WaterBuoyancy;
                }
            }
            else
            {
                if(distance > 2f)
                {
                    // raft is submerged, raise with buoyancy-like physics
                    this.velocity += Vector2.up * WaterBuoyancy;
                }
                else if(distance > 0f)
                {
                    Vector2 direction = heading / distance;

                    if(this.velocity.y > distance)
                    {
                        // stop raft on settle position
                        this.position = targetPosition;
                        this.velocity = Vector2.zero;
                    }
                    else
                    {
                        // move raft slowly to settle position
                        this.velocity.y = direction.y * 0.02f;
                    }
                }
            }
        }
        else
        {
            // raft is outside of water, just apply gravity
            this.velocity += Vector2.down * GravityAcceleration;
        }
    }

    private void OnCollisionFromAbove(Collision collision)
    {
        Player player = this.map.player;

        if(player.IsInsideWater == true) return;

        if (this.insideWater == true && this.sinkEffect == false)
        {
            this.sinkEffect = true;

            //this.velocity.x = 0.12f * (player.facingRight ? 1f : -1f);
            float playerVerticalVelocity = Mathf.Clamp(Mathf.Abs(player.velocity.y), 0f, 0.30f);
            float verticalFactor = Mathf.Lerp(0f, 3f, playerVerticalVelocity / 0.50f);
            this.velocity.y = -0.20f * verticalFactor;

            float halfWidth = this.width / 2f;
            float hContactPoint = Mathf.Clamp(
                this.position.x - player.position.x, -halfWidth, halfWidth);

            if(player.grabbingOntoItem != null &&
                player.grabbingOntoItem.grabInfo.grabType == GrabType.Floor)
            {
                hContactPoint = 0.1f;
            }

            this.angleVelocity = hContactPoint * 1.25f;
        }
    }

    protected void Integrate()
    {
        var raycast = new Raycast(
            this.map, this.position, ignoreItem1: this, ignoreTileTopOnlyFlag: true, isCastByEnemy: true, ignoreItemType: typeof(SnowBlock)
        );
        var rect = this.hitboxes[0];
        var start = this.position;

        if (this.velocity.x < 0f)
        {
            start = this.position + new Vector2(rect.xMin, 0f);
            var maxDistance = -this.velocity.x;

            var r1 = raycast.Horizontal(start, false, -this.velocity.x);

            var r = Raycast.CombineClosest(r1, GetGroupLeftRaycasts(raycast, start, maxDistance));

            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity.x = 0f;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else
        {
            start = this.position + new Vector2(rect.xMax, 0f);
            var maxDistance = this.velocity.x;

            var r1 = raycast.Horizontal(start, true, this.velocity.x);

            var r = Raycast.CombineClosest(r1, GetGroupRightRaycasts(raycast, start, maxDistance));

            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity.x = 0f;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y < 0f)
        {
            start = this.position + new Vector2(0f, rect.yMin);
            var maxDistance = -this.velocity.y;

            var r1 = raycast.Vertical(start, false, maxDistance);
            var r = Raycast.CombineClosest(r1, GetGroupDownRaycasts(raycast, start, maxDistance));

            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.velocity.y = 0f;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else
        {
            start = this.position + new Vector2(0f, rect.yMax);
            var maxDistance = this.velocity.y;
            
            var r1 = raycast.Vertical(start, true, maxDistance);
            var r = Raycast.CombineClosest(r1, GetGroupUpRaycasts(raycast, start, maxDistance));

            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity.y = 0f;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void MoveFromMotor(float speed)
    {
        this.motorSpeed = speed;
    }

    public void AdjustPosition(Vector2 offset)
    {
        this.position += offset;
    }

    private Raycast.Result[] GetGroupLeftRaycasts(Raycast raycast, Vector2 start, float maxDistance)
    {
        List<Raycast.Result> results = new List<Raycast.Result>();

        if(this.group != null && this.group.leader == this)
        {
            foreach(var i in this.group.followers)
            {
                Item item = i.Key.item;
                if(item.hitboxes == null || item.hitboxes.Length <= 0) continue;

                Hitbox itemRect = item.hitboxes[0];

                var iStart = item.position + new Vector2(itemRect.xMin, 0f);

                results.Add(raycast.Horizontal(iStart + new Vector2(0f, itemRect.yMin + HitboxTolerance), false, maxDistance));
                results.Add(raycast.Horizontal(iStart, false, maxDistance));
                results.Add(raycast.Horizontal(iStart + new Vector2(0f, itemRect.yMax - HitboxTolerance), false, maxDistance));
            }
        }

        return results.ToArray();
    }

    private Raycast.Result[] GetGroupRightRaycasts(Raycast raycast, Vector2 start, float maxDistance)
    {
        List<Raycast.Result> results = new List<Raycast.Result>();

        if(this.group != null && this.group.leader == this)
        {
            foreach(var i in this.group.followers)
            {
                Item item = i.Key.item;
                if(item.hitboxes == null || item.hitboxes.Length <= 0) continue;

                Hitbox itemRect = item.hitboxes[0];

                var iStart = item.position + new Vector2(itemRect.xMax, 0f);

                results.Add(raycast.Horizontal(iStart + new Vector2(0f, itemRect.yMin + HitboxTolerance), true, maxDistance));
                results.Add(raycast.Horizontal(iStart, true, maxDistance));
                results.Add(raycast.Horizontal(iStart + new Vector2(0f, itemRect.yMax - HitboxTolerance), true, maxDistance));
            }
        }

        return results.ToArray();
    }

    private Raycast.Result[] GetGroupDownRaycasts(Raycast raycast, Vector2 start, float maxDistance)
    {
        List<Raycast.Result> results = new List<Raycast.Result>();
        float halfWidth = (float)this.width / 2f;

        for (int i = 0; i < this.width + 1; i++)
        {
            float xOffset = (i * 1) - halfWidth;

            Vector2 rStart = start + new Vector2(xOffset, 0);
            if(rStart.x > this.position.x) rStart.x -= HitboxTolerance;
            if(rStart.x < this.position.x) rStart.x += HitboxTolerance;

            results.Add(raycast.Vertical(rStart, false, maxDistance));
        }
        
        if(this.group != null && this.group.leader == this)
        {
            foreach(var i in this.group.followers)
            {
                Item item = i.Key.item;
                if(item.hitboxes == null || item.hitboxes.Length <= 0) continue;

                Hitbox itemRect = item.hitboxes[0];

                var iStart = item.position + new Vector2(0f, itemRect.yMin);

                results.Add(raycast.Vertical(iStart + new Vector2(itemRect.xMin + HitboxTolerance, 0f), false, maxDistance));
                results.Add(raycast.Vertical(iStart, false, maxDistance));
                results.Add(raycast.Vertical(iStart + new Vector2(itemRect.xMax - HitboxTolerance, 0f), false, maxDistance));
            }
        }

        return results.ToArray();
    }

    private Raycast.Result[] GetGroupUpRaycasts(Raycast raycast, Vector2 start, float maxDistance)
    {
        List<Raycast.Result> results = new List<Raycast.Result>();
        float halfWidth = (float)this.width / 2f;

        for (int i = 0; i < this.width + 1; i++)
        {
            float xOffset = (i * 1) - halfWidth;

            Vector2 rStart = start + new Vector2(xOffset, 0);
            if(rStart.x > this.position.x) rStart.x -= HitboxTolerance;
            if(rStart.x < this.position.x) rStart.x += HitboxTolerance;

            results.Add(raycast.Vertical(rStart, true, maxDistance));
        }
        
        if(this.group != null && this.group.leader == this)
        {
            foreach(var i in this.group.followers)
            {
                Item item = i.Key.item;
                if(item.hitboxes == null || item.hitboxes.Length <= 0) continue;

                Hitbox itemRect = item.hitboxes[0];

                var iStart = item.position + new Vector2(0f, itemRect.yMax);

                results.Add(raycast.Vertical(iStart + new Vector2(itemRect.xMin + HitboxTolerance, 0f), true, maxDistance));
                results.Add(raycast.Vertical(iStart, true, maxDistance));
                results.Add(raycast.Vertical(iStart + new Vector2(itemRect.xMax - HitboxTolerance, 0f), true, maxDistance));
            }
        }

        return results.ToArray();
    }
}
