using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaftMotor : Item
{
    public Transform parentTransform;
    public Animated animatedPropeller;
    public Animated.Animation propellerSpinAnimation;
    public MotorDirection motorDirection;

    private Raft raft;
    private int ignoreTime = 0;
    private float motorSpeed = DefaultMotorSpeed;
    private bool isFlipped = false;
    private float animSpeed = 0f;
    private bool wasPlayerOnTheRight = false;
    private Vector2 originParentPosition;
    private Vector2 shakeParentPosition;
    private GrabType grabType;
    private bool isPlayingsfx = false;
    private bool hasPlayedHitSfx = false;

    private const float DefaultMotorSpeed = 0.1f;
    private static Vector2 RelativeGrabOffset = new Vector2(1f, 1f);
    private const float ShakeSpeed = 1f;
    private const float ShakeAmount = 0.015f;
    private static Vector2 PropellerOffsetLeft = new Vector2(-0.65f, -1);
    private static Vector2 PropellerOffsetRight = new Vector2(0.65f, -1);

    private bool IsPlayerOnTheRight => this.map.player.position.x > this.position.x;

    public enum MotorDirection
    {
        Left,
        Right
    }

    private Vector2 GetGrabRelativePosition()
    {
        float directionValue = motorDirection == MotorDirection.Left ? 1f : -1f;
        return new Vector2(RelativeGrabOffset.x * directionValue, RelativeGrabOffset.y);
    }
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.isFlipped = def.tile.flip;
        this.transform.localScale = new Vector3(this.isFlipped ? -1 : 1, 1, 1);

        if(this.isFlipped == true)
        {
            this.motorDirection = (this.motorDirection == MotorDirection.Left) ?
                MotorDirection.Right : MotorDirection.Left;
        }

        bool isFacingLeft = this.motorDirection == MotorDirection.Left;

        this.grabType = GrabType.Floor;

        Vector2 hitboxPosition = new Vector2(
            (RelativeGrabOffset.x - 0.10f) * (isFacingLeft ? 1f : -1f),
            RelativeGrabOffset.y
        );

        this.motorSpeed = (def.tile.properties?.ContainsKey("motor_speed") == true) ?
            def.tile.properties["motor_speed"].f : DefaultMotorSpeed;

        this.hitboxes = new [] {
            new Hitbox(
                hitboxPosition.x - 0.4f,
                hitboxPosition.x + 0.4f,
                hitboxPosition.y - 0.4f,
                hitboxPosition.y + 0.4f,
                this.rotation,
                Hitbox.NonSolid
            )
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        if(this.group != null && this.group.leader is Raft)
        {
            this.raft = this.group.leader as Raft;
        }
        
        this.animatedPropeller.PlayAndLoop(this.propellerSpinAnimation);
        
        this.originParentPosition = this.shakeParentPosition =
            this.parentTransform.localPosition;
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            Integrate();
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        AdvanceHanging();
        
        bool isMotorActive = false;
        
        Player player = this.map.player;
        if(this.raft != null && this.raft.IsInsideWater == true &&
            player.grabbingOntoItem == this)
        {
            this.raft.MoveFromMotor(
                this.motorSpeed * (this.motorDirection == MotorDirection.Right ? 1f : -1f)
            );

            isMotorActive = true;
            this.wasPlayerOnTheRight = IsPlayerOnTheRight;
        }
        
        if(isMotorActive == true)
        {
            float shake = Mathf.Sin(this.map.frameNumber * ShakeSpeed) * ShakeAmount;
            this.shakeParentPosition += Vector2.one * shake;

            if (this.isPlayingsfx == false)
            {
                this.isPlayingsfx = true;
                Audio.instance.PlaySfxLoop(
                    Assets.instance.sfxEnvRaftLoop,
                    position: this.position
                );
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvRaftOn,
                    position: this.position
                );
            }
        }
        else
        {
            this.shakeParentPosition = this.originParentPosition;

            if (this.isPlayingsfx == true)
            {
                this.isPlayingsfx = false;
                Audio.instance.StopSfxLoop(
                    Assets.instance.sfxEnvRaftLoop
                );
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvRaftOff,
                    position: this.position
                );
            }
        }
        
        this.parentTransform.localPosition = this.shakeParentPosition;
        
        float targetAnimSpeed = isMotorActive ? 1f : 0f;
        this.animSpeed = Util.Slide(this.animSpeed, targetAnimSpeed, 0.05f);

        this.animatedPropeller.playbackSpeed = this.animSpeed;

        if(this.animatedPropeller.playbackSpeed == 0f)
        {
            this.animatedPropeller.frameNumber = 0;
        }
        else if(this.raft != null &&
            this.raft.IsInsideWater == true &&
            this.map.frameNumber % 2 == 0)
        {
            SpawnBubbles(this.motorDirection == MotorDirection.Left);
        }

        this.grabInfo = new GrabInfo(this, this.grabType, GetGrabRelativePosition());

        ReadjustPositionIfNeeded();
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreTime = 30;
            }
            return;
        }
        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - SensorPosition();
        if (Mathf.Abs(delta.x) < 1.5 && Mathf.Abs(delta.y) < 1)
        {
            ReadjustPositionIfNeeded(0.3f);
            player.GrabItem(this);
            this.ignoreTime = 15;

            if(player.facingRight == false && this.motorDirection == MotorDirection.Right)
            {
                player.facingRight = true;
            }

            if(player.facingRight == true && this.motorDirection == MotorDirection.Left)
            {
                player.facingRight = false;
            }
        }
    }

    private void ReadjustPositionIfNeeded(float minDistance = 0.2f)
    {
        if(this.raft != null && this.raft.IsInsideWater == true && this.raft.velocity.y == 0)
        {
            var raycast = new Raycast(
                this.map, this.position, ignoreItem1: this, ignoreItem2: raft, ignoreTileTopOnlyFlag: true, ignoreItemType: typeof(SnowBlock)
            );
            var rect = this.hitboxes[0];

            var left = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0f),
                false,
                minDistance
            );

            if (left.anything == true)
            {
                this.raft.AdjustPosition(new Vector2(minDistance - left.distance, 0));
                this.raft.velocity.x = 0f;
            }

            var right = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0f),
                true,
                minDistance
            );

            if (right.anything == true)
            {              
                this.raft.AdjustPosition(new Vector2(-(minDistance - right.distance), 0));
                this.raft.velocity.x = 0f;
            }            
        }
    }

    public Vector2 SensorPosition()
    {
        return this.position + GetGrabRelativePosition();
    }
    
    protected void Integrate()
    {
        if(this.raft == null) return;

        var raycast = new Raycast(
            this.map, this.position, ignoreItem1: this, ignoreItem2: raft, ignoreTileTopOnlyFlag: true, ignoreItemType: typeof(SnowBlock)
        );
        var rect = this.hitboxes[0];
        var start = this.position;

        if (this.raft.velocity.x < 0f)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0f),
                false,
                -this.raft.velocity.x
            );

            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.raft.velocity.x = 0f;
                if (this.hasPlayedHitSfx == false)
                {
                    this.hasPlayedHitSfx = true;
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvRaftWall,
                        position: this.position
                    );
                }
            }
            else
            {
                this.position.x += this.raft.velocity.x;
                this.hasPlayedHitSfx = false;
            }
        }
        else
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0f),
                true,
                this.raft.velocity.x
            );

            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.raft.velocity.x = 0f;
                if (this.hasPlayedHitSfx == false)
                {
                    this.hasPlayedHitSfx = true;
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvRaftWall,
                        position: this.position
                    );
                }
            }
            else
            {
                this.position.x += this.raft.velocity.x;
                this.hasPlayedHitSfx = false;
            }
        }

        if (this.raft.velocity.y < 0f)
        {
            start = this.position + new Vector2(0f, rect.yMin);
            var maxDistance = -this.raft.velocity.y;
            var r = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-1.50f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(-0.50f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(0.50f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(1.50f, 0), false, maxDistance)
            );

            if (r.anything == true)
            {
                this.position.y -= r.distance;
                this.raft.velocity.y = 0f;
            }
            else
            {
                this.position.y += this.raft.velocity.y;
            }
        }
        else
        {
            start = this.position + new Vector2(0f, rect.yMax);
            var maxDistance = this.raft.velocity.y;
            var r = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-1.50f, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(-0.50f, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(0.50f, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(1.50f, 0), true, maxDistance)
            );

            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.raft.velocity.y = 0f;
            }
            else
            {
                this.position.y += this.raft.velocity.y;
            }
        }
    }
    
    private void SpawnBubbles(bool right)
    {
        Vector2 spawnPosition = this.position + (right ? PropellerOffsetRight : PropellerOffsetLeft);
        Vector2 spawnDirection = right ? Vector2.right : Vector2.left;

        spawnPosition += Vector2.up * Random.Range(-0.25f, 0.25f);

        Sprite randomBubbleSprite = Assets.instance.sunkenIsland.RandomBubbleParticle;
        Particle p = Particle.CreateWithSprite(randomBubbleSprite, 15, spawnPosition, this.map.transform);
        p.spriteRenderer.sortingLayerName = "Player Dust (AL)";

        p.Throw(spawnDirection * 0.20f, 0.1f, 0.01f, Vector2.zero);
    }
}
