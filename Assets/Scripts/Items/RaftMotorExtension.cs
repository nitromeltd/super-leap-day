using UnityEngine;

public class RaftMotorExtension : Item
{
    public Transform parentTransform;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            if(this.parentTransform == null)
            {
                RaftMotor raftMotor = this.map.ItemAt<RaftMotor>(this.def.tx, this.def.ty);
                if(raftMotor != null)
                {
                    this.parentTransform = raftMotor.parentTransform;
                    this.transform.parent = this.parentTransform;
                }

                RaftMotorBoth raftMotorBoth = this.map.ItemAt<RaftMotorBoth>(this.def.tx, this.def.ty);
                if(raftMotorBoth != null)
                {
                    this.parentTransform = raftMotorBoth.parentTransform;
                    this.transform.parent = this.parentTransform;
                }
            }
        }
    }
}