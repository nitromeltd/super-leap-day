using UnityEngine;
using System.Linq;
public class RaftMotorRod : Item
{
    public Transform parentTransform;
    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            if(this.parentTransform == null)
            {
                foreach (var raftMotor in this.chunk.subsetOfItemsOfTypeRaftMotor)
                {
                    if ((raftMotor.position - this.position).magnitude < 2.5f)
                    {
                        this.parentTransform = raftMotor.parentTransform;
                        this.transform.parent = this.parentTransform;
                        return;
                    }
                }

                foreach (var raftMotorBoth in this.chunk.subsetOfItemsOfTypeRaftMotorBoth)
                {
                    if ((raftMotorBoth.position - this.position).magnitude < 2.5f)
                    {
                        this.parentTransform = raftMotorBoth.parentTransform;
                        this.transform.parent = this.parentTransform;
                        return;
                    }
                }
            }
        }
    }
}