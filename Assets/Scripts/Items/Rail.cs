using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Rail : Item
{
    public Sprite railSprite;
    public Sprite[] beamSprites;
    public Sprite beamTopSprite;
    public Sprite beamExtenderSprite;
    public Sprite joinerSprite;
    public GameObject railStartPrefab;
    public GameObject railEndPrefab;
    MeshRenderer meshRenderer;
    Mesh mesh;
    PathRenderer pathRenderer;
    MeshFilter meshFilter;


    GameObject gameObj;
    private int meshGenerated;
    public List<NitromeEditor.Path.PointOnPathInfo> pillars = new List<NitromeEditor.Path.PointOnPathInfo>();
    public NitromeEditor.Path path;
    public override void Init(Def def)
    {
        base.Init(def);

        gameObj = new GameObject("Rail Mesh");
        gameObj.transform.parent = this.map.transform;
        gameObj.transform.SetParent(this.chunk.transform, false);
        gameObj.transform.position = Vector3.zero;
        
        this.meshFilter = gameObj.AddComponent<MeshFilter>();
        this.meshRenderer = gameObj.AddComponent<MeshRenderer>();
        this.meshRenderer.material = Assets.instance.railMaterial;
        this.meshRenderer.sortingLayerName = "Enemies";
        this.meshRenderer.sortingOrder = -1;
        this.meshFilter.mesh = this.mesh = new Mesh();
        this.path = chunk.pathLayer.NearestPathTo(this.position, 1.1f);
        this.meshGenerated = 1;
    }

    public override void Advance()
    {
        if(meshGenerated > 0 && this.path != null)
        {
            meshGenerated--;
            if (meshGenerated == 0)
            {
                RegenerateMesh(this.path, railSprite);
            }
        }
    }
    
    public bool IsTooCloseToOtherPillars(NitromeEditor.Path.PointOnPathInfo currentPointInfo)
    {
        foreach (var pointInfo in this.pillars)
        {
            if (pointInfo.distanceAlongPath == currentPointInfo.distanceAlongPath)
            {
                return false;   //we check only previous pillars
            }

            if (Mathf.Abs(pointInfo.position.x - currentPointInfo.position.x) < 1.5f)
            {
                return true; 
            }
        }
        return false;
    }

    private bool CheckPillarsInOtherRails(NitromeEditor.Path.PointOnPathInfo currentPointInfo)
    {
        foreach(Rail rail in this.chunk.subsetOfItemsOfTypeRail)
        {
            if(rail.IsTooCloseToOtherPillars(currentPointInfo))
                return true;
        }
        return false;
    }

    public void RegenerateMesh(NitromeEditor.Path path, Sprite sprite)
    {      
        List<Vector2> positions = new List<Vector2>();

        positions.Add(path.PointAtDistanceAlongPath(0).position);
        pillars.Add(path.PointAtDistanceAlongPath(0));
        float lastDistancePillarWasAdded = 0;
        int extraNode = path.closed ? 1 : 0;
        for (int i = 0; i < path.nodes.Length + extraNode; i++)
        {
            var firstNode = path.nodes[i % path.nodes.Length];
            var nextNode = path.nodes[(i + 1) % path.nodes.Length];

            float segmentLength = 0;
            if(i < path.nodes.Length - 1)
            {
                segmentLength = nextNode.distanceAlongPath - firstNode.distanceAlongPath;
            }
            else
            {
                segmentLength = path.length - firstNode.distanceAlongPath;
            }

            float n = Mathf.Floor(segmentLength / 1f);
            float exactSubstepLength = segmentLength / (n > 0 ? n : 1);
            for (int k = 1; k <= n; k++)
            {
                positions.Add(path.PointAtDistanceAlongPath(firstNode.distanceAlongPath + exactSubstepLength * k).position);
            }

            //now generate data for pillars
            float p = Mathf.Floor(segmentLength / 8);
            float exactSubstepLengthForPillar = segmentLength / (p > 0 ? p : 1);
            for (int k = 1; k <= p; k++)
            {
                pillars.Add(path.PointAtDistanceAlongPath(firstNode.distanceAlongPath + exactSubstepLengthForPillar * k));
                lastDistancePillarWasAdded = firstNode.distanceAlongPath + exactSubstepLengthForPillar * k;
            }
        }

        if(lastDistancePillarWasAdded < path.length)
        {
            pillars.Add(path.PointAtDistanceAlongPath(path.length));
        }
        
        Draw(sprite, positions.ToArray(), false, "Items");
        
        // process the pillars
        int pillarOrder = 0;
        for (int j = 0; j < pillars.Count; j++)
        {
            var pillarPointInfo = pillars[j];
            var position = pillarPointInfo.position;

            if (Vector2.Dot(Vector2.Perpendicular(pillarPointInfo.tangent.normalized), Vector2.down) > 0.9f
                && (j != 0)
                && (j != pillars.Count - 1)
            )
            {
                //dont draw pillars for upside down rails, exept start and end
                continue;
            }

            if (CheckPillarsInOtherRails(pillarPointInfo))
            {
                continue;
            }

            if (Mathf.Abs(Vector2.Dot(pillarPointInfo.tangent.normalized, Vector2.right)) < 0.3f)
                continue;

            int height = 0;

            // void DebugTile(Tile tile)
            // {
            //     if(tile == null)
            //         return;

            //     Debug.Log("============");
            //     Debug.Log(tile.spriteRenderer.gameObject.name + ": gridT(" + tile.gridTx + "," + tile.gridTy + "), mapT:(" + tile.mapTx + "," + tile.mapTy + ")");
            //     Debug.Log("============");
            // }

            while (true)
            {
                var tile0 = this.chunk.TileAt(position - new Vector2(0, height), Layer.A);
                if (tile0 != null && tile0.tileGrid?.group == null)
                {
                    var tile1 = this.chunk.TileAt(position - new Vector2(1, height), Layer.A);

                    if (tile1 != null && tile1.tileGrid?.group == null)
                    {
                        break;
                    }
                }

                if (height > 100)
                    break;

                var newPosition = position - new Vector2(0, height);
                Sprite pillarSprite;

                if (height == 0)
                {
                    if (Mathf.Abs(Vector2.Dot(pillarPointInfo.tangent.normalized, Vector2.right)) < 0.9f || pillarPointInfo.distanceAlongPath == 0 || pillarPointInfo.distanceAlongPath == path.length)
                    {
                        pillarSprite = this.beamExtenderSprite;
                        newPosition += new Vector2(0, -0.25f);
                    }
                    else
                    {
                        pillarSprite = this.beamTopSprite;
                    }
                    GameObject gameObjectJoiner = new GameObject("joiner");
                    gameObjectJoiner.layer = this.map.Layer();
                    gameObjectJoiner.transform.SetParent(this.chunk.transform, false);
                    gameObjectJoiner.transform.position = position - new Vector2(0, height + 0.25f);
                    var joinerSpriteRenderer = gameObjectJoiner.AddComponent<SpriteRenderer>();
                    joinerSpriteRenderer.sprite = this.joinerSprite;
                    joinerSpriteRenderer.sortingLayerName = "Tiles - Layer B";
                    joinerSpriteRenderer.sortingOrder = 0;
                }
                else
                {
                    pillarSprite = this.beamSprites[Random.Range(0, this.beamSprites.Length)];
                }

                var name = "pillar_" + pillarOrder + "_" + height;

                if (tile0 != null)
                    name += "(" + tile0.gridTx + "," + tile0.gridTy + ")";

                GameObject gameObj = new GameObject(name);
                gameObj.layer = this.map.Layer();
                gameObj.transform.SetParent(this.chunk.transform, false);
                gameObj.transform.position = newPosition;
                var sr = gameObj.AddComponent<SpriteRenderer>();
                sr.sprite = pillarSprite;
                sr.sortingLayerName = "Tiles - Foliage";
                sr.sortingOrder = -100 - pillarOrder;

                height++;
            }
            pillarOrder++;
        }

        //add rail ends

        var startPoint = path.PointAtDistanceAlongPath(0);
        var endPoint = path.PointAtDistanceAlongPath(path.length);

        GameObject startCap = Instantiate<GameObject>(railStartPrefab, this.chunk.transform);
        startCap.layer = this.map.Layer();
        startCap.transform.position = startPoint.position;
        float startAngle = Vector2.SignedAngle(Vector2.right,startPoint.tangent);
        startCap.transform.rotation = Quaternion.Euler(0, 0, startAngle);

        GameObject endCap = Instantiate<GameObject>(railEndPrefab, this.chunk.transform);
        endCap.layer = this.map.Layer();
        endCap.transform.position = endPoint.position;
        float endAngle = Vector2.SignedAngle(Vector2.right,endPoint.tangent);
        endCap.transform.rotation = Quaternion.Euler(0, 0, endAngle);
    }    

    public void Draw(
    Sprite sprite,
    Vector2[] positions,
    bool loopPath,
    string sortingLayerName,
    float spriteStretchFactor = 1
    )
    {
        float thickness = sprite.rect.height / sprite.pixelsPerUnit;
        float lengthOfSprite =
            spriteStretchFactor * sprite.rect.width / sprite.pixelsPerUnit;

        for (int n = 0; n < positions.Length; n += 1)
            positions[n] = this.gameObj.transform.InverseTransformPoint(positions[n]);

        Vector2[] vertexDirections;
        float[] vertexScales;
        Vector2 EdgeDirection(int n) => (positions[n + 1] - positions[n]).normalized;

        if (loopPath)
        {
            vertexDirections = new Vector2[positions.Length];
            vertexScales = new float[positions.Length];
            for (var n = 0; n < positions.Length; n += 1)
            {
                var a = EdgeDirection(n);
                var b = EdgeDirection((n + positions.Length - 1) % positions.Length);
                vertexDirections[n] = (a + b).normalized;
                vertexScales[n] = CornerScale(a, b);
            }
        }
        else
        {
            vertexDirections = new Vector2[positions.Length];
            vertexScales = new float[positions.Length];
            for (var n = 0; n < positions.Length; n += 1)
            {
                Vector2 a = (n == 0) ?
                    EdgeDirection(0) :
                    EdgeDirection(n - 1);
                Vector2 b = (n == positions.Length - 1) ?
                    EdgeDirection(n - 1) :
                    EdgeDirection(n);
                vertexDirections[n] = (a + b).normalized;
                vertexScales[n] = CornerScale(a, b);
            }
        }

        var edgeCount = positions.Length + (loopPath ? 0 : -1);
        var uMin = sprite.uv.Select(uv => uv.x).Min();
        var uMax = sprite.uv.Select(uv => uv.x).Max();
        var vMin = sprite.uv.Select(uv => uv.y).Max();
        var vMax = sprite.uv.Select(uv => uv.y).Min();

        var vertices = new List<Vector3>();
        var colours = new List<Color>();
        var uvs = new List<Vector2>();
        var indices = new List<int>();

        float distanceAlongSprite = 0;

        for (var e = 0; e < edgeCount; e += 1)
        {
            float distanceAlongEdge = 0;

            var fromIndex = e;
            var toIndex = (e + 1) % positions.Length;
            var from = positions[fromIndex];
            var to = positions[toIndex];
            var edgeLength = (to - from).magnitude;
            if (edgeLength < 0.01f) continue;

            while (distanceAlongEdge < edgeLength - 0.001f)
            {
                float pieceLength = lengthOfSprite - distanceAlongSprite;
                if (pieceLength > edgeLength - distanceAlongEdge)
                    pieceLength = edgeLength - distanceAlongEdge;

                var s = distanceAlongEdge / edgeLength;
                var position = Vector2.Lerp(from, to, s);
                var parallel = Vector2.Lerp(
                    vertexDirections[fromIndex], vertexDirections[toIndex], s
                ).normalized;
                var perpendicular =
                    new Vector2(parallel.y, -parallel.x) * thickness / 2;
                var u = Mathf.Lerp(uMin, uMax, distanceAlongSprite / lengthOfSprite);
                var width = Mathf.Lerp(
                    vertexScales[fromIndex], vertexScales[toIndex], s
                );
                int firstIndex = vertices.Count;

                vertices.Add(position - (perpendicular * width));
                vertices.Add(position + (perpendicular * width));
                colours.Add(Color.white);
                colours.Add(Color.white);
                uvs.Add(new Vector2(u, vMin));
                uvs.Add(new Vector2(u, vMax));

                distanceAlongEdge += pieceLength;
                distanceAlongSprite += pieceLength;

                s = distanceAlongEdge / edgeLength;
                position = Vector2.Lerp(from, to, s);
                parallel = Vector2.Lerp(
                    vertexDirections[fromIndex], vertexDirections[toIndex], s
                ).normalized;
                perpendicular = new Vector2(parallel.y, -parallel.x) * thickness / 2;
                u = Mathf.Lerp(uMin, uMax, distanceAlongSprite / lengthOfSprite);
                width = Mathf.Lerp(
                    vertexScales[fromIndex], vertexScales[toIndex], s
                );

                vertices.Add(position - (perpendicular * width));
                vertices.Add(position + (perpendicular * width));
                colours.Add(Color.white);
                colours.Add(Color.white);
                uvs.Add(new Vector2(u, vMin));
                uvs.Add(new Vector2(u, vMax));

                indices.Add(firstIndex + 0);
                indices.Add(firstIndex + 2);
                indices.Add(firstIndex + 1);
                indices.Add(firstIndex + 1);
                indices.Add(firstIndex + 2);
                indices.Add(firstIndex + 3);

                if (distanceAlongSprite >= lengthOfSprite - 0.001f)
                    distanceAlongSprite = 0;
            }
        }
        
        mesh.name = $"Mesh generated by Minecart path renderer ({gameObject.name})";
        mesh.Clear();
        mesh.SetVertices(vertices);
        mesh.SetColors(colours);
        mesh.SetUVs(0, uvs);
        mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
        mesh.MarkModified();

        var meshFilter =
            this.gameObj.GetComponent<MeshFilter>() ??
            this.gameObj.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        var meshRenderer =
            this.gameObj.GetComponent<MeshRenderer>() ??
            this.gameObj.AddComponent<MeshRenderer>();
        meshRenderer.material.mainTexture = sprite.texture;
        meshRenderer.sortingLayerName = sortingLayerName;
    }

    private Vector2[] VertexDirections(NitromeEditor.Path path)
    {
        var result = new Vector2[path.nodes.Length];

        if (path.closed)
        {
            for (var n = 0; n < result.Length; n += 1)
            {
                var a = path.edges[n].direction;
                var b = path.edges[(n + result.Length - 1) % result.Length].direction;
                result[n] = (a + b).normalized;
            }
        }
        else
        {
            result[0] = path.edges[0].direction;
            result[result.Length - 1] = path.edges[path.edges.Length - 1].direction;

            for (var n = 1; n < result.Length - 1; n += 1)
            {
                var a = path.edges[n - 1].direction;
                var b = path.edges[n].direction;
                result[n] = (a + b).normalized;
            }
        }

        return result;
    }

    private float CornerScale(Vector2 a, Vector2 b)
    {
        // i don't think this is quite right yet

        float dot = Vector2.Dot(a.normalized, b.normalized);
        if (dot > 1) dot = 1;
        if (dot < -1) dot = -1;
        float angleRadians = Mathf.Acos(dot);

        if (dot >= 0)
        {
            float s = Mathf.Sin(angleRadians / 2);
            return Mathf.Sqrt(1 + s * s);
        }
        else
        {
            float multiplier = Mathf.Tan(angleRadians / 2);
            if (multiplier > 2) multiplier = 2;
            return multiplier;
        }
    }

    private Vector2[] VertexDirections(Vector2[] edgeDirections)
    {
        var result = new Vector2[edgeDirections.Length + 1];
        result[0] = edgeDirections[0];
        result[result.Length - 1] = edgeDirections[edgeDirections.Length - 1];

        for (var n = 1; n < result.Length - 1; n += 1)
        {
            result[n] = (edgeDirections[n - 1] + edgeDirections[n]).normalized;
        }

        return result;
    }
}
