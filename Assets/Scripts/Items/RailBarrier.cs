using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RailBarrier : Item
{
    public bool deadlyRailBarrier;

    private NitromeEditor.Path path = null;
    private float timeAtStart;
    private bool exploded;

    public Sprite regularBarrierSprite;
    public Sprite explodingBarrierSprite;
    public Animated.Animation animationExplosionParticle;
    public Enemy.DeathAnimation deathAnimation;

    public override void Init(Def def)
    {
        base.Init(def);
 
        if(def.startChunk.isFlippedHorizontally)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
        }

        if(this.deadlyRailBarrier)
        {
            this.GetComponent<SpriteRenderer>().sprite = explodingBarrierSprite;
            this.hitboxes = new[] {
                new Hitbox(-0.5f, 0.5f, -1.7f, 1.7f, this.rotation, Hitbox.NonSolid, true, true),
                new Hitbox(-0.45f, 0.4f, -1.6f, 1.55f, this.rotation, Hitbox.Solid, true, true)
            };
        }
        else
        {
            this.GetComponent<SpriteRenderer>().sprite = regularBarrierSprite;
            this.hitboxes = new[] {
                new Hitbox(-0.45f, 0.4f, -1.55f, 1.55f, this.rotation, Hitbox.Solid, true, true)
            };
        }

        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 0.50f);

        if(this.path != null)
        {
            this.timeAtStart = this.path.NearestPointTo(this.position)?.timeSeconds ?? 0;
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
    }

    public override void Advance()
    {
        AdvanceOnPath();

        var player = this.map.player;
        if (this.deadlyRailBarrier && player.InsideMinecart == null && this.exploded == false)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            if (player.ShouldCollideWithItems() && thisRect.Overlaps(player.Rectangle()))
            {
                this.map.player.Hit(this.position);
                DestroyBarrier();
            }
        }
    }

    public void DestroyBarrier()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxEnvExplosiveBarrierExplosion, position: this.position, options: new Audio.Options(1, false, 0));
        GenerateExplosionAndParticles();
        GetComponent<SpriteRenderer>().sprite = null;
    }

    private void GenerateExplosionAndParticles()
    {
        if(this.exploded)
            return;

        foreach (var debris in this.deathAnimation.debris)
        {
            var p = Particle.CreateWithSprite(
                debris, 100, this.position, this.transform.parent
            );
            p.Throw(new Vector2(0, 0.7f), 0.3f, 0.5f, new Vector2(0, -0.02f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                p.spin *= -1;
        }
        Vector2 offsetDynamite = new Vector2(0, 0 );
        
        Particle explosionParticle = Particle.CreateAndPlayOnce(
            animationExplosionParticle, this.position + offsetDynamite, this.transform.parent
        );
        this.hitboxes = new[] {
                new Hitbox(-0.45f, 0.4f, -1.6f, 1.55f, this.rotation, Hitbox.NonSolid)
            };
        exploded = true;
        // Debug.Break();
    }

    private void AdvanceOnPath()
    {
        if (this.path == null)
            return;

        if(this.exploded)
            return;

        if (this.path == null) return;

        var time = TimeForFrame(this.map.frameNumber) % this.path.duration;

        this.position = this.path.PointAtTime(time).position;
        this.transform.position = this.position;
    }

    private float TimeForFrame(int frameNumber) =>
        this.timeAtStart + (frameNumber / 60.0f);
     
}