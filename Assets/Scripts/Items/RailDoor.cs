using UnityEngine;
using System.Linq;

public class RailDoor : Item
{
    public bool doorAllowsPlayerIn;
    [HideInInspector] public bool interactingWithMinecart;
    [HideInInspector] public RailDoor otherDoor;
    [HideInInspector] public NitromeEditor.Path path;
    [HideInInspector] public float distanceAlongPath;
    // public Vector3 disappearTargetPosition;

    public Animated.Animation animationDoorOpen;
    public Animated.Animation animationDoorClose;

    private Animated barrierAnimated;
    public Vector2 doorExitVector;
    public Vector2 doorUpVector;

    public override void Init(Def def)
    {
        base.Init(def);       
        if(def.tile.flip)
        {
            transform.localScale = new Vector3(-1, 1, 1);
            this.hitboxes = new[] {
                new Hitbox(-2f, 3f, 0f, 2f, this.rotation, Hitbox.NonSolid),
                new Hitbox( -1.8f, -1.5f, 0f, 2f, this.rotation, Hitbox.NonSolid),
                new Hitbox(-1.1f, 0.3f, -0.1f, 4.5f, this.rotation, Hitbox.Solid, false, true)
            };
        }
        else
        {
            this.hitboxes = new[] {
                new Hitbox(-3f, 2f, 0f, 2f, this.rotation, Hitbox.NonSolid),
                new Hitbox( 1.5f, 1.8f, 0f, 2f, this.rotation, Hitbox.NonSolid),
                new Hitbox( -0.3f, 1.1f, -0.1f, 4.5f, this.rotation, Hitbox.Solid, true, false)
            };
        }

        Vector2 exit = transform.rotation * Vector2.left;
        Vector2 up = transform.rotation * Vector2.up;
        if (def.tile.flip)
        {
            exit = -exit;
        }

        this.doorExitVector = exit;
        this.doorUpVector = up;

        var nearestConnectionNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 4);
        if (nearestConnectionNode != null && this.otherDoor == null)
        {
            var otherNode =
                nearestConnectionNode.Previous() ??
                nearestConnectionNode.Next();
            this.otherDoor = this.chunk.NearestItemTo<RailDoor>(otherNode.Position);

            if(this.otherDoor != null)
            {
                this.otherDoor.otherDoor = this;
            }
        }

        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 2f);

        if(this.path != null)
        {
            var closestNodeIndex = this.path.NearestNodeIndexTo(this.position).Value;
            this.distanceAlongPath = path.nodes[closestNodeIndex].distanceAlongPath;
        }

        this.barrierAnimated = this.transform.Find("Barrier").GetComponent<Animated>();

        if(this.doorAllowsPlayerIn == false)
        {
            this.barrierAnimated.PlayOnce(this.animationDoorClose);
        }
        else
        {
            this.barrierAnimated.gameObject.SetActive(false);
        }

        // this.disappearTargetPosition = this.transform.Find("Target").gameObject.transform.position;
        // Debug.Log(this.gameObject.name + " target v:" + (this.disappearTargetPosition - this.transform.position));
        // AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void OpenDoor()
    {
        this.barrierAnimated.PlayAndHoldLastFrame(this.animationDoorOpen);
    }

    private void CloseDoor()
    {
        this.barrierAnimated.PlayAndHoldLastFrame(this.animationDoorClose);
    }


    public override void Reset()
    {
        if (this.doorAllowsPlayerIn == false)
        {
            CloseDoor();
        }
    }

    public override void Advance()
    {
        // if (this.group != null)
        //     this.transform.position = this.position = this.group.FollowerPosition(this);

        if(this.interactingWithMinecart == true || this.otherDoor?.interactingWithMinecart == true)
        {
            OpenDoor();
        }
        else
        {
            CloseDoor();
        }
        interactingWithMinecart = false;
    }
}
