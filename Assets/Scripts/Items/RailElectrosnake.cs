using UnityEngine;
using System.Linq;

public class RailElectrosnake : Item
{
    public NitromeEditor.Path path;
    // public float distanceAlongPath;
    public Animated.Animation animationIdleFront;
    public Animated.Animation animationIdleBack;

    public Animated.Animation animationShockFront;
    public Animated.Animation animationShockBack;
    public Enemy.DeathAnimation deathAnimation;

    private Animated animatedFront;
    private Animated animatedBack;

    private int shockDelayTime;
    private const int DelayBetweenShocks = 5 * 60;
    private float closestAlongThePath;
    private bool isDead;

    public override void Init(Def def)
    {
        base.Init(def);
        isDead = false;
        if(def.tile.flip)
        {
            transform.localScale = new Vector3(-1, 1, 1);
        }
        
        this.hitboxes = new[] {
                new Hitbox(-1f, 1f, -1f, 1f, this.rotation, Hitbox.NonSolid)
            };
        this.animatedFront = this.transform.Find("Front").GetComponent<Animated>();
        this.animatedBack = this.transform.Find("Back").GetComponent<Animated>();
        this.animatedFront.PlayAndLoop(this.animationIdleFront);
        this.animatedBack.PlayAndLoop(this.animationIdleBack);
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 2f);

        if(this.path != null)
        {
            var closestPointInfo = this.path.NearestPointTo(this.position).Value;
            this.position = this.transform.position = closestPointInfo.position;
            this.closestAlongThePath = closestPointInfo.distanceAlongPath;
            var closestNodeIndex = this.path.NearestNodeIndexTo(this.position).Value;
        }

        this.shockDelayTime = DelayBetweenShocks;

        CreateSpawnParticles(Vector2.zero);

        // Debug.Log(this.gameObject.name + " target v:" + (this.disappearTargetPosition - this.transform.position));
        // AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void CreateSpawnParticles(Vector2 offset)
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position + offset,
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.50f;
    }

    public override void Advance()
    {
        if(isDead == true)
            return;

        if(this.shockDelayTime > 0)
        {
            this.shockDelayTime--;

            if(this.shockDelayTime == 0)
            {
                this.animatedBack.PlayOnce(this.animationShockBack);
                Audio.instance.PlaySfx(Assets.instance.sfxEnvElectrosnakeDie, position: this.position);
                this.animatedFront.PlayOnce(this.animationShockFront, delegate
                {
                    this.animatedFront.PlayAndLoop(this.animationIdleFront);
                    this.animatedBack.PlayAndLoop(this.animationIdleBack);
                    this.shockDelayTime = DelayBetweenShocks;

                    RailShock.Create(this.map, this.chunk, this.path, this.closestAlongThePath, -0.1f);
                    RailShock.Create(this.map, this.chunk, this.path, this.closestAlongThePath, 0.1f);
                });
            }
        }

        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if (minecart.hitboxes[0].InWorldSpace().Overlaps(this.hitboxes[0].InWorldSpace()))
            {
                if(IsSnakeDeadly() == true)
                {
                    minecart.DestroyMinecartAndEjectPlayer();
                }
                else
                {
                    GenerateDeathParticlesAndDie();
                    return;
                }
            }
        }

        HitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
    }

    private bool IsSnakeDeadly()
    {
        return (this.animatedFront.currentAnimation == this.animationShockFront);
    }

    public void GenerateDeathParticlesAndDie()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxEnvElectrosnakeDie, position: this.position, options: new Audio.Options(1, false, 0));
        foreach (var debris in this.deathAnimation.debris)
        {
            var p = Particle.CreateWithSprite(
                debris, 100, this.position, this.transform.parent
            );
            p.Throw(new Vector2(0, 0.7f), 0.3f, 0.5f, new Vector2(0, -0.02f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
                p.spin *= -1;
        }
        this.animatedFront.gameObject.SetActive(false);
        this.animatedBack.gameObject.SetActive(false);
        this.isDead = true;
    }

    private void HitDetectionAgainstPlayer(Rect ownRectangle)
    {
        var player = this.map.player;
        if (player.InsideMinecart != null)
        {
            return;
        }

        if (player.ShouldCollideWithEnemies() == false)
        {
            return;
        }

        var playerRectangle = player.Rectangle();
        if (ownRectangle.Overlaps(playerRectangle) == false)
            return;

        var playerAbove =
            playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f ||
            (playerRectangle.yMin - player.VelocityLastFrame.y) > ownRectangle.yMax - 0.1f;
        var enemyVelocityY = 0;
        var playerGoingDown =
            player.velocity.y < enemyVelocityY ||
            player.VelocityLastFrame.y < enemyVelocityY;

        if (player.invincibility.isActive == true)
        {
            GenerateDeathParticlesAndDie();
        }
        else if (playerAbove == true &&
            playerGoingDown == true &&
            IsSnakeDeadly() == false)
        {
            player.BounceOffHitEnemy(this.transform.position);
            GenerateDeathParticlesAndDie();
        }
        else
        {
            player.Hit((Vector2)this.transform.position);
        }
    }
}
