using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RailRebounder : Item
{
    private Rail railAttachedTo;
    private bool initialized;
    private float rebounderDistanceAlongPath;
    private int ignoreTime = 0;
    public Animated.Animation animationRebound;
    private Animated leftAnimated;
    private Animated rightAnimated;
    private Transform leftSmackTransform;
    private Transform rightSmackTransform;
    private bool onReversedPath;
    public Animated.Animation animationSmackParticle;
    private float wobbleCount;
    private float initialAngle;

    public override void Init(Def def)
    {
        base.Init(def);
        this.leftAnimated = this.transform.Find("Left").GetComponent<Animated>();
        this.rightAnimated = this.transform.Find("Right").GetComponent<Animated>();
        this.leftSmackTransform = this.transform.Find("LeftSmack");
        this.rightSmackTransform = this.transform.Find("RightSmack");
    }

    private Vector2 GetUpVector()
    {
        switch (this.rotation)
        {
            case 0:    return Vector2.up;
            case 90:   return Vector2.left;
            case 180:  return Vector2.down;
            case 270:  return Vector2.right;
        }
        return Vector2.up;
    }

    public override void Advance()
    {
        if(!initialized)
        {
            initialized = true;
            SetRebounderOrientation();
        }

        this.transform.position = this.position;
        CheckIfPlayerBounces();
        CheckIfRebounderShouldWobble();
    }

    private void CheckIfRebounderShouldWobble()
    {
        var player = this.map.player;
        var dist = (player.transform.position - this.transform.position).sqrMagnitude;
        if(dist < 1 && this.wobbleCount == 0)
        {
            this.wobbleCount = 1f;
            CreateSmackParticle(this.transform.position + 0.75f * (this.transform.rotation * Vector3.up));
        }
        this.transform.rotation = Quaternion.Euler(0, 0, this.initialAngle + 7f*Mathf.Sin(2 * Mathf.PI * this.wobbleCount));
        this.wobbleCount = Util.Slide(this.wobbleCount, 0, 0.05f);
    }

    private void SetRebounderOrientation()
    {
        Rail closestRail = null;
        float closestDist = float.MaxValue;

        foreach (Rail rail in this.chunk.subsetOfItemsOfTypeRail)
        {
            var closestPoint = rail.path.NearestPointTo(this.position);
            if(closestPoint.HasValue)
            {
                float sqrDist = (closestPoint.Value.position - this.position).sqrMagnitude;
                if(sqrDist < closestDist)
                {
                    closestRail = rail;
                    closestDist = sqrDist;
                }
            }
        }

        if(closestRail != null)
        {
            this.railAttachedTo = closestRail;
            var pointInfo = this.railAttachedTo.path.NearestPointTo(this.position);

            this.rebounderDistanceAlongPath = pointInfo.Value.distanceAlongPath;

            this.position = pointInfo.Value.position;
            float angle = Vector2.SignedAngle(Vector2.right, pointInfo.Value.tangent.normalized);

            Vector2 up = GetUpVector();
            var perpendicular = Vector2.Perpendicular(pointInfo.Value.tangent.normalized);
            this.onReversedPath = Vector2.Dot(perpendicular, up) < 0;

            if(this.onReversedPath)
            {
                angle += 180;
            } 
            this.initialAngle = angle;
            this.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }

    private void CheckIfPlayerBounces()
    {
        if(this.ignoreTime > 0)
        {
            ignoreTime--;
            return;
        }

        Vector2 heightVector = new Vector2(0, 4);
        float length = heightVector.magnitude;

        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        heightVector = Quaternion.Euler(0, 0, currentRotation) * heightVector;

        float along = Vector2.Dot(
            this.map.player.position - this.position, heightVector
        ) / heightVector.sqrMagnitude;

        var alongPoint = this.position + along * length * heightVector.normalized;

        var minecart = this.map.player.InsideMinecart;

        CheckBouncers();
    }

    private void CheckBouncers()
    {
        float leftRebounderDist = this.rebounderDistanceAlongPath - 1.5f;
        float rightRebounderDist = this.rebounderDistanceAlongPath + 1.5f;

        if(this.onReversedPath)
        {
            leftRebounderDist = this.rebounderDistanceAlongPath + 1.5f;
            rightRebounderDist = this.rebounderDistanceAlongPath - 1.5f;
        }


#if UNITY_EDITOR
        var leftPoint = railAttachedTo.path.PointAtDistanceAlongPath(leftRebounderDist);
        var rightPoint = railAttachedTo.path.PointAtDistanceAlongPath(rightRebounderDist);
        // Debug.DrawLine(leftPoint.position, leftPoint.position + 5 * Vector2.Perpendicular(leftPoint.tangent), Color.red);
        // Debug.DrawLine(rightPoint.position, rightPoint.position + 5 * Vector2.Perpendicular(rightPoint.tangent), Color.red);
#endif

        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if (minecart.GetPathMinecartIsAttachedTo() == this.railAttachedTo.path)
            {
                float leftA = minecart.lastDistanceOnRail - leftRebounderDist;
                float leftB = minecart.GetBottomFrontPointConvertedToAlongPath() - leftRebounderDist;

                // if (minecart.lastDistanceOnRail < leftRebounderDist && minecart.GetBottomFrontPointConvertedToAlongPath() >= leftRebounderDist)
                if(leftA * leftB <= 0)
                {
                    this.leftAnimated.PlayOnce(this.animationRebound);
                    minecart.FlipVelocityDirectionOnRail();
                    CreateSmackParticle(this.leftSmackTransform.position);
                    this.ignoreTime = 10;
                    continue;
                }

                float rightA = minecart.lastDistanceOnRail - rightRebounderDist;
                float rightB = minecart.GetBottomFrontPointConvertedToAlongPath() - rightRebounderDist;

                // if (minecart.lastDistanceOnRail > rightRebounderDist && minecart.GetBottomFrontPointConvertedToAlongPath() <= rightRebounderDist)
                if(rightA * rightB <= 0)
                {
                    this.rightAnimated.PlayOnce(this.animationRebound);
                    minecart.FlipVelocityDirectionOnRail();
                    CreateSmackParticle(this.rightSmackTransform.position);
                    this.ignoreTime = 10;
                    continue;
                }
            }
        }
    }

    private void CreateSmackParticle(Vector2 pos)
    {
        // Vector2 offsetSpark = new Vector2(this.facingRight ? -0.05f : 0.05f, 0.05f).normalized;
        //         offsetSpark = transform.rotation * offsetSpark;
        Particle spark = Particle.CreateAndPlayOnce(
                            this.animationSmackParticle,
                            pos,
                            this.transform
                        );
        spark.spriteRenderer.sortingLayerName = "Items";
        spark.spriteRenderer.sortingOrder = 2;
        Audio.instance.PlaySfx(Assets.instance.sfxEnvRailRebound, position: this.position, options: new Audio.Options(1, false, 0));
    }
}
