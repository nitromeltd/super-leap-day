using UnityEngine;
using System.Linq;

public class RailShock : Item
{
    public NitromeEditor.Path path;
    public float currentDistanceOnRail;
    private float speed;
    public Animated.Animation animationIdleFront;
    public Animated.Animation animationIdleBack;
    private Animated animatedFront;
    private Animated animatedBack;

    public static RailShock Create(Map map, Chunk chunk, NitromeEditor.Path path, float along, float speed)
    {
        var obj = Instantiate(
            Assets.instance.treasureMines?.railShock, chunk.transform
        );
        obj.name = "Spawned Rail Shock";
        var item = obj.GetComponent<RailShock>();
        item.Init(map, chunk, path, along, speed);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Map map,Chunk chunk, NitromeEditor.Path path, float distanceAlongPath, float speed)
    {
        this.map = map;
        this.chunk = chunk;
        this.path = path;
        this.currentDistanceOnRail = distanceAlongPath;
        this.speed = speed;

        Util.SetLayerRecursively(this.transform, map.Layer(), true);

        this.hitboxes = new[] {
                new Hitbox(-0.2f, 0.2f, -0.2f, 0.2f, this.rotation, Hitbox.NonSolid)
            };

        this.animatedFront = this.transform.Find("Front").GetComponent<Animated>();
        this.animatedBack = this.transform.Find("Back").GetComponent<Animated>();
        this.animatedFront.PlayAndLoop(this.animationIdleFront);
        this.animatedBack.PlayAndLoop(this.animationIdleBack);

        this.transform.position = this.position = this.path.PointAtDistanceAlongPath(distanceAlongPath).position;
        Audio.instance.PlaySfxLoop(Assets.instance.sfxEnvElectrosnakeShockMoving, position: this.position);
    }

    public override void Advance()
    {
        var nextDistanceOnRail = this.currentDistanceOnRail + speed;
        if(nextDistanceOnRail >= this.path.length || nextDistanceOnRail <= 0)
        {
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
            Audio.instance.StopSfxLoop(Assets.instance.sfxEnvElectrosnakeShockMoving);
            return;
        }

        this.currentDistanceOnRail = nextDistanceOnRail;
        this.transform.position = this.position = this.path.PointAtDistanceAlongPath(currentDistanceOnRail).position;

        // foreach (var minecart in this.chunk.items.OfType<Minecart>().ToArray())
        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if(minecart.GetPathMinecartIsAttachedTo() != this.path)
                continue;

            if(Mathf.Abs(minecart.currentDistanceOnRail - this.currentDistanceOnRail) < 1 && minecart.IsMinecartMovingOnRail())
            {
                minecart.DestroyMinecartAndEjectPlayer();
            }
        }

        if (this.map.player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace()))
        {
            this.map.player.Hit(this.position);
        }

    }
}
