using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RailSpeedBooster : Item
{
    private Rail railAttachedTo;
    private bool initialized;
    private int ignoreTime = 0;
    private float speedUpDistanceAlongPath = 0;

    private Animated frontAnimated;
    private Animated backAnimated;
    private Animated middleAnimated;

    public Animated.Animation animationRailCasing;
    public Animated.Animation animationMiddleArrows;

    private GameObject connector;
    private GameObject railConnector;
    private GameObject rod;
    private bool onReversedPath;

    public override void Init(Def def)
    {
        base.Init(def);  
        this.frontAnimated = this.transform.Find("FrontRail").GetComponent<Animated>(); 
        this.backAnimated = this.transform.Find("BackRail").GetComponent<Animated>(); 
        this.middleAnimated = this.transform.Find("MiddleArrows").GetComponent<Animated>();

        this.frontAnimated.PlayAndLoop(animationRailCasing);
        this.backAnimated.PlayAndLoop(animationRailCasing);
        this.middleAnimated.PlayAndLoop(animationMiddleArrows);

        this.connector = this.transform.Find("Connector").gameObject;
        this.railConnector = this.transform.Find("RailConnector").gameObject;
        this.rod = this.transform.Find("Rod").gameObject;

        this.hitboxes = new[] {
            new Hitbox(-1.5f, 1.5f, 0.1f, 1.4f, this.rotation, Hitbox.NonSolid)
        };

    }

    public override void Advance()
    {
        if(!initialized)
        {
            initialized = true;
            SetSpeedBoosterOrientation();
        }
        CheckIfPlayerSpeedsUp();
        this.transform.position = this.position;
    }

    private void SetSpeedBoosterOrientation()
    {
        Rail closestRail = null;
        float closestDist = float.MaxValue;

        foreach (Rail rail in this.chunk.subsetOfItemsOfTypeRail)
        {
            var closestPoint = rail.path.NearestPointTo(this.position);
            if(closestPoint.HasValue)
            {
                float sqrDist = (closestPoint.Value.position - this.position).sqrMagnitude;
                if(sqrDist < closestDist)
                {
                    closestRail = rail;
                    closestDist = sqrDist;
                }
            }
        }

        if(closestRail != null)
        {
            if (closestDist > 2)
            {
                if (ExtendSpeedBooster() == true)
                {
                    this.railAttachedTo = null;
                    return;
                }
            }

            this.railAttachedTo = closestRail;
            var pointInfo = this.railAttachedTo.path.NearestPointTo(this.position);
            this.position = pointInfo.Value.position;
            this.speedUpDistanceAlongPath = pointInfo.Value.distanceAlongPath;
            float angle = Vector2.SignedAngle(Vector2.right, pointInfo.Value.tangent.normalized);
            Vector2 up = GetUpVector();
            var perpendicular = Vector2.Perpendicular(pointInfo.Value.tangent.normalized);
            this.onReversedPath = Vector2.Dot(perpendicular, up) < 0;

            if(this.onReversedPath)
            {
                angle += 180;
            } 

            this.transform.rotation = Quaternion.Euler(0, 0, angle);
        }
    }

    private Vector2 GetUpVector()
    {
        switch (this.rotation)
        {
            case 0:    return Vector2.up;
            case 90:   return Vector2.left;
            case 180:  return Vector2.down;
            case 270:  return Vector2.right;
        }
        return Vector2.up;
    }

    private bool ExtendSpeedBooster()
    {
        Rail closestRail = null;
        float closestDistance = float.MaxValue;
        Vector2 closestPoint = Vector2.zero;

        foreach (var rail in this.chunk.subsetOfItemsOfTypeRail)
        {
            for (int i = 0; i < 10; i++)
            {
                var positionToTest = this.position + new Vector2(0, -i);
                var tempClosestPoint = rail.path.NearestPointTo(positionToTest);

                if (tempClosestPoint.HasValue)
                {
                    if ((positionToTest - tempClosestPoint.Value.position).magnitude <= 0.5f)
                    {
                        //closest
                        if((tempClosestPoint.Value.position - this.position).magnitude < closestDistance)
                        {
                            closestDistance = (tempClosestPoint.Value.position - this.position).magnitude;
                            closestRail = rail;
                            closestPoint = tempClosestPoint.Value.position;
                        }
                    }
                }
            }
        }

        if(closestRail != null)
        {
            this.connector.SetActive(true);
            this.railConnector.SetActive(true);
            this.railConnector.transform.position = closestPoint;
            this.rod.SetActive(true);
            this.rod.transform.localScale = new Vector3(1, this.position.y - closestPoint.y, 1);
            return true;
        }

        return false;
    }

    private void CheckIfPlayerSpeedsUp()
    {
        if(this.ignoreTime > 0)
        {
            ignoreTime--;
            return;
        }
 
        Vector2 heightVector = new Vector2(0, 4);
        float length = heightVector.magnitude;

        float currentRotation = Util.WrapAngleMinus180To180(transform.rotation.eulerAngles.z);
        heightVector = Quaternion.Euler(0, 0, currentRotation) * heightVector;

        float along = Vector2.Dot(
            this.map.player.position - this.position, heightVector
        ) / heightVector.sqrMagnitude;

        var alongPoint = this.position + along * length * heightVector.normalized;

        var minecart = this.map.player.InsideMinecart;

        if(minecart != null)
        {
            if(minecart.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace()))
            {
                minecart.IncreaseMinecartSpeed();
                this.ignoreTime = 60;
                Audio.instance.PlaySfx(Assets.instance.sfxEnvBoostActivate, position: this.position, options: new Audio.Options(1, false, 0));
            }

            // Debug.Log("speedup:" + this.speedUpDistanceAlongPath + ", last:" + minecart.lastDistanceOnRail + ", current:" + minecart.GetBottomFrontPointConvertedToAlongPath());
            // if(minecart.lastDistanceOnRail < this.speedUpDistanceAlongPath && minecart.GetBottomFrontPointConvertedToAlongPath() >= this.speedUpDistanceAlongPath)
            // {
            //     Debug.Log("speedUp dir left -> right!");
            //     minecart.IncreaseMinecartSpeed();
            //     this.ignoreTime = 10;
            // }
            // else if(minecart.lastDistanceOnRail > this.speedUpDistanceAlongPath && minecart.GetBottomFrontPointConvertedToAlongPath() <= this.speedUpDistanceAlongPath)
            // {
            //     Debug.Log("speedUp dir right -> left!");
            //     minecart.IncreaseMinecartSpeed();
            //     this.ignoreTime = 10;
            // }
        }
    }    
}