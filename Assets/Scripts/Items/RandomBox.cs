﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomBox : Item
{
    public Animated.Animation animationNormal;
    public Animated.Animation animationBreak;
    public Animated.Animation animationDebrisLeft;
    public Animated.Animation animationDebrisRight;
    
    public int blockSize;

    [HideInInspector] public bool destroyed;
    private Vector2 center;
    private Vector2 initialPosition;

    private Pickup.LuckyDrop? pickup;
    private bool canSpawnPickup;

    // Sprout sucking ability
    protected bool suckActive = false;
    protected int shakeTimer = 0;
    protected Vector2 smoothSuckVel;
    protected float smoothSuckTime;
    protected Vector2 shakeAmount;
    protected const int SuckShakeTime = 20;
    protected readonly float[] shakeDeltas = new float[] { 1, 1, 0, 0, -1, -1, 1, -1, 0 };

    private bool becameGold = false;
         
    Pickup.LuckyDrop insertedPickup;

    public override void Init(Def def)
    {
        base.Init(def);

        this.destroyed = false;
        this.initialPosition = this.position;

        MakeSolid();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        string pickupName;
        if (def.tile.properties?.ContainsKey("contains_item") == true)
            pickupName = def.tile.properties["contains_item"].s;
        else
            pickupName = "none";

        this.insertedPickup = Pickup.GetPickupByPropertyName(pickupName);
        this.canSpawnPickup = true;
        this.pickup = null;
        this.becameGold = false;

        this.onCollisionFromAbove = (_) => {
            if (this.map.player.midasTouch.isActive == true && this.insertedPickup == Pickup.LuckyDrop.None)
            {
                OnCollision();
            }
            else if (this.map.player.gravity.Orientation() == Player.Orientation.Ceiling)
                Break();

        };
        this.onCollisionFromRight = (_) => {
            if (this.map.player.midasTouch.isActive == true && this.insertedPickup == Pickup.LuckyDrop.None)
            {
                OnCollision();
            }
            else if (this.map.player.gravity.Orientation() == Player.Orientation.RightWall)
                Break();
        };
        this.onCollisionFromBelow = (_) => {
            if (this.map.player.midasTouch.isActive == true && this.insertedPickup == Pickup.LuckyDrop.None)
            {
                OnCollision();
            }
            else if (this.map.player.gravity.Orientation() == Player.Orientation.Normal)
                Break();
        };
        this.onCollisionFromLeft = (_) => {
            if (this.map.player.midasTouch.isActive == true && this.insertedPickup == Pickup.LuckyDrop.None)
            {
                OnCollision();
            }
            else if (this.map.player.gravity.Orientation() == Player.Orientation.LeftWall)
                Break();
        };
    }

    private void MakeSolid()
    {
        float halfSize = (float)this.blockSize / 2f;

        this.hitboxes = new [] {
            new Hitbox(
                -halfSize, halfSize, -halfSize, halfSize,
                this.rotation, Hitbox.Solid, true, true)
        };
    }

    public override void Advance()
    {
        base.Advance();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        UpdateSuck();
    }

    public void Break()
    {
        if (this.destroyed == true) return;

        this.center = this.hitboxes[0].InWorldSpace().center;

        this.destroyed = true;
        this.hitboxes = new Hitbox[] {};

        this.animated.PlayOnce(this.animationBreak, () =>
        {
            this.spriteRenderer.enabled = false;

            Vector2 leftPosition = this.center.Add(-.25f, 0f);
            Vector2 rightPosition = this.center.Add(.25f, 0f);
            
            var left = Particle.CreatePlayAndHoldLastFrame(
                this.animationDebrisLeft, 100, leftPosition, this.transform.parent
            );
            left.velocity     = new Vector2(-1f, 7f) / 16f;
            left.acceleration = new Vector2(0f, -0.5f) / 16f;
            left.canBeSucked = true;

            var right = Particle.CreatePlayAndHoldLastFrame(
                this.animationDebrisRight, 100, rightPosition, this.transform.parent
            );
            right.velocity     = new Vector2(1f, 7f) / 16f;
            right.acceleration = new Vector2(0f, -0.5f) / 16f;
            right.canBeSucked = true;

            Particle.CreateDust(this.transform.parent, 5, 7, this.center, .5f, .5f);
        });

        // kill enemies above
        Rect damageRect = new Rect(
            this.position.x - (this.blockSize * 0.5f),
            this.position.y - (this.blockSize * 0.5f),
            this.blockSize,
            this.blockSize
        );
        switch (this.map.player.gravity.Orientation())
        {
            case Player.Orientation.Normal:
            default:
                damageRect.yMin = damageRect.yMax;
                damageRect.yMax += 0.2f;
                break;
            case Player.Orientation.RightWall:
                damageRect.xMax = damageRect.xMin;
                damageRect.xMin -= 0.2f;
                break;
            case Player.Orientation.Ceiling:
                damageRect.yMax = damageRect.yMin;
                damageRect.yMax -= 0.2f;
                break;
            case Player.Orientation.LeftWall:
                damageRect.xMin = damageRect.xMax;
                damageRect.xMax += 0.2f;
                break;
        }

        this.map.DamageEnemy(
            damageRect, new Enemy.KillInfo { itemDeliveringKill = this }
        );

        Audio.instance.PlaySfx(Assets.instance.sfxBreakBlock, position: this.position);

        if (this.insertedPickup != Pickup.LuckyDrop.None)
        {
            this.map.DropLuckyPickup(this.insertedPickup, this.center, this.chunk);
        }
        else
        {
            if (this.canSpawnPickup == true)
            {
                if(pickup == null)
                {
                    pickup = this.map.RandomlySelectLuckyPickup();
                }
                this.map.DropLuckyPickup(this.pickup.Value, this.center, this.chunk);
                this.canSpawnPickup = Pickup.CanPickupDropMultipleTimes(this.pickup.Value);
            }
        }

        this.suckActive = false;
    }

    IEnumerator BecomeGold()
    {
        this.destroyed = true;
        this.becameGold = true;

        // kill enemies above
        Rect damageRect = new Rect(
            this.position.x - (this.blockSize * 0.5f),
            this.position.y - (this.blockSize * 0.5f),
            this.blockSize,
            this.blockSize
        );
        switch (this.map.player.gravity.Orientation())
        {
            case Player.Orientation.Normal:
            default:
                damageRect.yMin = damageRect.yMax;
                damageRect.yMax += 0.2f;
                break;
            case Player.Orientation.RightWall:
                damageRect.xMax = damageRect.xMin;
                damageRect.xMin -= 0.2f;
                break;
            case Player.Orientation.Ceiling:
                damageRect.yMax = damageRect.yMin;
                damageRect.yMax -= 0.2f;
                break;
            case Player.Orientation.LeftWall:
                damageRect.xMin = damageRect.xMax;
                damageRect.xMax += 0.2f;
                break;
        }

        this.map.DamageEnemy(
            damageRect, new Enemy.KillInfo { itemDeliveringKill = this }
        );

        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.white;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = Color.black;
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
        this.spriteRenderer.material.color = new Color(255, 165, 0);
        yield return new WaitForSeconds(.1f);
        this.spriteRenderer.material = Assets.instance.spriteGoldTint;
        yield return new WaitForSeconds(.5f);
        this.hitboxes = new Hitbox[] { };
        this.animated.PlayOnce(this.animationBreak, () =>
        {
            this.spriteRenderer.enabled = false;

            Vector2 leftPosition = this.position.Add(-.25f, 0f);
            Vector2 rightPosition = this.position.Add(.25f, 0f);

            var left = Particle.CreatePlayAndHoldLastFrame(
                this.animationDebrisLeft, 100, leftPosition, this.transform.parent
            );
            left.velocity = new Vector2(-1f, 7f) / 16f;
            left.acceleration = new Vector2(0f, -0.5f) / 16f;
            left.canBeSucked = true;

            var right = Particle.CreatePlayAndHoldLastFrame(
                this.animationDebrisRight, 100, rightPosition, this.transform.parent
            );
            right.velocity = new Vector2(1f, 7f) / 16f;
            right.acceleration = new Vector2(0f, -0.5f) / 16f;
            right.canBeSucked = true;

            left.spriteRenderer.material = Assets.instance.spriteGoldTint;
            right.spriteRenderer.material = Assets.instance.spriteGoldTint;

            Particle.CreateDust(this.transform.parent, 5, 7, this.position, .5f, .5f);
        });

        Audio.instance.PlaySfx(Assets.instance.sfxBreakBlock, position: this.position);

        this.map.DropLuckyPickup(Pickup.LuckyDrop.GoldCoin, this.position, this.chunk);

        this.suckActive = false;
    }

    public override void Reset()
    {
        base.Reset();

        if (this.destroyed == true)
        {
            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(this.animationNormal);
            MakeSolid();
            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.hitboxes[0].InWorldSpace().center,
                this.transform.parent
            );
            this.destroyed = false;
        }

        if(this.becameGold == true)
        {
            this.becameGold = false;
            this.spriteRenderer.material = Assets.instance.spritesDefaultMaterial;
        }
    }

    private void UpdateSuck()
    {
        if(this.suckActive == false) return;

        var positionWithShake = this.position;

        positionWithShake += new Vector2(
            shakeAmount.x *
            shakeDeltas[this.map.frameNumber % shakeDeltas.Length],
            shakeAmount.y *
            shakeDeltas[this.map.frameNumber % shakeDeltas.Length]
        );
        
        this.transform.position = new Vector2(
            positionWithShake.x,
            positionWithShake.y);

        if(this.canSpawnPickup == true && this.map.frameNumber % 12 == 0)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                this.hitboxes[0].InWorldSpace().center.Add(
                    UnityEngine.Random.Range(-0.5f, 0.5f),
                    UnityEngine.Random.Range(-0.5f, 0.5f)
                ),
                this.transform.parent
            );
            sparkle.spriteRenderer.sortingLayerName = "Items";
            sparkle.spriteRenderer.sortingOrder = 1;
        }
    }

    public void EnterSuck()
    {
        if(this.suckActive == true) return;

        this.suckActive = true;
        this.shakeAmount = Vector2.one * 0.02f;
    }

    public void ExitSuck()
    {
        if(this.suckActive == false) return;
        
        this.suckActive = false;
        this.transform.position = this.initialPosition;
    }

    public bool HaveRarePickup()
    {
        return this.pickup != Pickup.DefaultDrop;
    }

    public void ShakeIfRarePickup()
    {
        if(HaveRarePickup())
        {
            EnterSuck();
        }
    }

    public void ExtractPickup()
    {
        if(this.canSpawnPickup == true)
        {
            this.pickup = this.map.RandomlySelectLuckyPickup();
            this.map.DropLuckyPickup(this.pickup.Value, this.hitboxes[0].InWorldSpace().center, this.chunk);
            this.canSpawnPickup = false;
        }

        EnterSuck();
    }

    private void OnCollision()
    {
        if (this.becameGold == false && this.destroyed == false)
        {
            StartCoroutine(BecomeGold());
        }
    }
}
