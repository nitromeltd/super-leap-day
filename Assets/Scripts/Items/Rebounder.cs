
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Rebounder : Item
{
    public Sprite rebounderSprite;

    private Transform pin1;
    private Transform pin2;
    private Transform attach1;
    private Transform attach2;
    private Transform end;
    private PathRenderer pathRenderer;

    private Vector2 point1;
    private Vector2 point2;
    private float originalLength;
    private float length;
    private Vector2 point2Previous;
    private bool point2IsFixed;

    private float midpointDistanceAlongLine;
    private Vector2 midpoint;
    private Vector2 midpointPrevious;
    private float playerSlidingSpeed;
    private int delayBeforeSlide;   

    private GroupLayer.Group point1Group;
    private GroupLayer.Group point2Group;

    private Vector2 lastPlayerPosition;

    public struct Knot
    {
        public float distanceAlong;
        public float factorAlong;
        public Transform transform;
    };
    private Knot[] knots;

    private int ignoreTime;

    public override void Init(Def def)
    {
        base.Init(def);

        var nearest = this.chunk.pathLayer.NearestNodeTo(this.position, 2);
        var path = nearest.path;

        this.point1 = path.nodes[0].Position;
        this.point2 = path.nodes.Last().Position;
        this.point2IsFixed = true;//def.tile.tile.Contains("fixed");
        this.point2Previous = this.point2;

        this.length = (this.point1 - this.point2).magnitude;
        this.originalLength = this.length;

        this.midpointDistanceAlongLine = this.length / 2;
        this.midpoint = Vector2.Lerp(this.point1, this.point2, 0.5f);
        this.midpointPrevious = this.midpoint;

        this.pin1    = this.transform.Find("pin 1");
        this.pin2    = this.transform.Find("pin 2");
        this.attach1 = this.transform.Find("attach 1");
        this.attach2 = this.transform.Find("attach 2");
        this.end     = this.transform.Find("end");


        float pinAngle = Vector2.SignedAngle(Vector2.right, this.point2 - this.point1);
        this.pin1.rotation = Quaternion.Euler(0, 0, pinAngle);
        this.pin2.rotation = Quaternion.Euler(0, 0, pinAngle);

        this.pin1.position    = this.point1;
        this.pin2.position    = this.point2;
        this.attach1.position = this.point1;
        this.attach2.position = this.point2;
        this.end.gameObject.SetActive(false);
        
        this.pathRenderer =
            this.transform.Find("rebounder_renderer").GetComponent<PathRenderer>();

        var templateKnot = this.transform.Find("knot");

        this.knots = new Knot[path.nodes.Length - 2];
        for (var n = 0; n < this.knots.Length; n += 1)
        {
            this.knots[n].distanceAlong =
                (path.nodes[n + 1].Position - path.nodes[0].Position).magnitude;
            this.knots[n].factorAlong = this.knots[n].distanceAlong / this.length;
            this.knots[n].transform = Instantiate(
                templateKnot.gameObject, this.transform
            ).transform;
        }

        Destroy(templateKnot.gameObject);

        this.point1Group = this.chunk.groupLayer.RegisterFollower(
            this, 0, this.point1, def.tx, def.ty
        );
        
        this.point2Group = this.chunk.groupLayer.RegisterFollower(
            this,
            1,
            this.point2,
            Mathf.FloorToInt(this.point2.x),
            Mathf.FloorToInt(this.point2.y)
        );
        
    }

    private void UpdateGrabInfo()
    {
        this.grabInfo = new GrabInfo(
            this,
            GrabType.Ceiling,
            this.midpoint - this.position
        );
    }

    private float HangOffsetYForFixed()
    {
        if (this.midpointDistanceAlongLine < 1)
            return Util.MapRange(this.midpointDistanceAlongLine, 0, 1, 0, 0.5f);

        if (this.midpointDistanceAlongLine > this.length - 1)
            return Util.MapRange(
                this.midpointDistanceAlongLine,
                this.length - 1,
                this.length,
                0.5f,
                0
            );

        return 0.5f;
    }

    public override void Advance()
    {
        if (this.point1Group != null)
            this.point1 = this.point1Group.FollowerPosition(this, 0);

        if (this.point2IsFixed == true && this.point2Group != null)
            this.point2 = this.point2Group.FollowerPosition(this, 1);
        
        this.length = (this.point2 - this.point1).magnitude;
        for (int n = 0; n < this.knots.Length; n += 1)
            this.knots[n].distanceAlong = this.knots[n].factorAlong * this.length;

        if (this.map.player.grabbingOntoItem == this)
            this.ignoreTime = 12;
        else if (this.ignoreTime > 0)
            this.ignoreTime -= 1;

        var playerCanBounce =
            this.map.player.ShouldCollideWithItems() == true &&
            this.map.player.grabbingOntoItem == null &&
            this.ignoreTime == 0 &&
            (
                this.map.player.state == Player.State.Normal ||
                this.map.player.state == Player.State.FireFromMineCannon ||
                this.map.player.state == Player.State.Jump                
            );

        if(this.map.player.state == Player.State.InsideMinecart && this.ignoreTime == 0)
        {
            playerCanBounce = true;
        }

        if (playerCanBounce == true)
            CheckIfPlayerBounces();
 
        if (this.midpointDistanceAlongLine < 0.25f)
            this.midpointDistanceAlongLine = 0.25f;
        if (this.midpointDistanceAlongLine > this.length - 0.25f)
            this.midpointDistanceAlongLine = this.length - 0.25f;

        AdvanceRopeBouncing();

        UpdateDisplay();

        if(this.map.player.state == Player.State.InsideMinecart)
        {
            lastPlayerPosition = this.map.player.GetMinecart().GetWheelsPosition();
        }
        else
        {
            lastPlayerPosition = this.map.player.position;
        }
    }

    private void CheckIfPlayerBounces()
    {
        Vector2 playerPos = this.map.player.position;
        if(this.map.player.state == Player.State.InsideMinecart)
        {
            playerPos = this.map.player.GetMinecart().GetWheelsPosition();
        }

        Vector2 lineVector = this.point2 - this.point1;
        float along = Vector2.Dot(
            playerPos - this.point1, lineVector
        ) / lineVector.sqrMagnitude;

        // var perpendicular = Vector2.Perpendicular(lineVector);
        // Debug.DrawLine(this.point1, this.point1 + perpendicular, Color.yellow);

        //always fixed
        along = Mathf.Clamp(along, 1f / this.length, 1f - (1f / this.length));

        bool onRightSide = false;
        if(WhichSideOfLinePointIs(point1, point2, this.lastPlayerPosition) > 0)
        {
            onRightSide = true;
        }

        Vector2 nearest = Vector2.Lerp(this.point1, this.point2, along);    

        if ((nearest - playerPos).sqrMagnitude >= 0.5f * 0.5f)
            return;

        var playerVel = this.map.player.velocity;
        if(this.map.player.state == Player.State.InsideMinecart)
        {
            playerVel = this.map.player.GetMinecart().velocity;
        }

        Vector2 velocity = new Vector2(
            Mathf.Clamp( playerVel.x * 0.8f, -0.4f, 0.4f), 
            Mathf.Clamp( playerVel.y * 0.8f, -0.4f, 0.4f)
            );

        this.midpoint = nearest;
        this.midpointPrevious = this.midpoint - velocity;

        //make player bounce
        var perp = Vector2.Perpendicular(lineVector);
        this.map.player.position = nearest;
        this.map.player.timeOfFlyingAsBallWhenFiredFromMineCannon = MineCannon.MaximumTimeOfFlyingAsBall;
        if(this.map.player.state == Player.State.InsideMinecart)
        {
            this.map.player.GetMinecart().velocity = Vector2.Reflect(playerVel, (onRightSide ? perp.normalized : -perp.normalized));
            Audio.instance.PlaySfx(Assets.instance.sfxEnvRubberReboundBounceHeavy, position: this.position, options: new Audio.Options(1, false, 0));
        }
        else
        {
            this.map.player.velocity = Vector2.Reflect(playerVel, (onRightSide ? perp.normalized : -perp.normalized));
            Audio.instance.PlaySfx(Assets.instance.sfxEnvRubberReboundBounceLight, position: this.position, options: new Audio.Options(1, false, 0));
        }
        this.ignoreTime = 10;       
    }

    private float WhichSideOfLinePointIs(Vector2 a1, Vector2 a2, Vector2 p)
    {
        return (a2.x - a1.x) * (p.y - a1.y) - (a2.y - a1.y) * (p.x - a1.x);
    }

    private void AdvanceRopeBouncing()
    {
        Vector2 midpointHome = Vector2.Lerp(
            this.point1, this.point2, this.midpointDistanceAlongLine / this.length
        );

        Vector2 nextMidpoint;
        Vector2 nextPoint2Position;

        void Push(ref Vector2 previous, Vector2 acceleration) =>
            previous -= acceleration;
        void Damp(ref Vector2 previous, Vector2 current, float factor) =>
            previous = Vector2.Lerp(previous, current, factor);

        Vector2 target = midpointHome;
        Push(ref this.midpointPrevious, (target - this.midpoint) * 0.1f);
        Damp(ref this.midpointPrevious, this.midpoint, 0.05f);        

        nextMidpoint = this.midpoint + this.midpoint - this.midpointPrevious;
        nextPoint2Position = this.point2;
        this.midpointPrevious = this.midpoint;
         
        this.midpoint = nextMidpoint;
        this.point2 = nextPoint2Position;   
    }

    private void UpdateDisplay()
    {
        this.pin1.transform.position = this.point1;
        this.attach1.transform.position = this.point1;
        this.pin2.transform.position = this.point2;
        this.attach2.transform.position = this.point2;

        Vector2[] path = new [] { this.point1, this.midpoint, this.point2 };
        this.pathRenderer.Draw(
            this.rebounderSprite,
            path,
            false,
            "Items (Front)",
            this.length / this.originalLength
        );

        {
            var vector = this.midpoint - this.point1;
            if (vector.sqrMagnitude < 0.1f * 0.1f)
                vector = this.point2 - this.point1;
            var angleDegrees = Mathf.Atan2(vector.y, vector.x) * 180 / Mathf.PI;
            this.attach1.localRotation = Quaternion.Euler(0, 0, angleDegrees + 90);

            vector = this.midpoint - this.point2;
            if (vector.sqrMagnitude < 0.1f * 0.1f)
                vector = this.point1 - this.point2;
            angleDegrees = Mathf.Atan2(vector.y, vector.x) * 180 / Mathf.PI;
            if (this.point2IsFixed == true)
            {
                this.attach2.localRotation = Quaternion.Euler(0, 0, angleDegrees + 90);
            }
            else
            {
                this.end.position = this.point2;
                this.end.localRotation = Quaternion.Euler(0, 0, angleDegrees - 90);
            }
        }

        for (var n = 0; n < this.knots.Length; n += 1)
        {
            Vector2 pt1, pt2;
            float distance;

            if (this.knots[n].distanceAlong < this.midpointDistanceAlongLine)
            {
                pt1 = this.point1;
                pt2 = this.midpoint;
                distance = this.knots[n].distanceAlong;
            }
            else
            {
                pt1 = this.midpoint;
                pt2 = this.point2;
                distance =
                    this.knots[n].distanceAlong - this.midpointDistanceAlongLine;
            }

            Vector2 delta = pt2 - pt1;
            float angleDegrees = Mathf.Atan2(delta.y, delta.x) * 180 / Mathf.PI;

            this.knots[n].transform.position = pt1 + (delta.normalized * distance);
            this.knots[n].transform.rotation = Quaternion.Euler(0, 0, angleDegrees + 90);
        }
    }

    private Vector2? SegmentToSegmentIntersection(
        Vector2 a1,
        Vector2 a2,
        Vector2 b1,
        Vector2 b2
    )
    {
        var d = (a1.x - a2.x) * (b1.y - b2.y) - (a1.y - a2.y) * (b1.x - b2.x);
        if(d == 0) return null;

        var t = ((a1.x - b1.x) * (b1.y - b2.y) - (a1.y - b1.y) * (b1.x - b2.x))/d;
        var u = ((a1.x - b1.x) * (a1.y - a2.y) - (a1.y - b1.y) * (a1.x - a2.x))/d;

        if(t >= 0 && t <= 1 && u >= 0 && u <= 1)
        {
            return new Vector2(a1.x + t * (a2.x - a1.x), a1.y + t * (a2.y - a1.y));
        }
        return null;
    }
}
