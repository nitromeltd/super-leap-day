using System;
using System.Collections;
using UnityEngine;
public class Referee : Item
{
    public static Referee instance;

    public Animated.Animation idleAnimation;
    public Animated.Animation flyAnimation;
    public Animated.Animation turnAnimation;
    public Animated.Animation blowWhistleAnimation;
    public Animated.Animation flagWaveAnimation;
    public Animated.Animation chatAnimation;
    public Animated.Animation arrowAnimation;
    public Animated.Animation bubbleAnimation;
    public Animated.Animation bubblePopAnimation;

    private Camera mainCamera;
    private Vector3 startEntryPoint;
    private Vector3 startTargetPoint;

    private bool facingRight;

    // breathe movement
    private bool breatheMovement = true;
    private float breatheTimer;
    private float BreathePeriod = .3f;
    private float BreatheAmplitude = .5f;
    private Vector2 breathePos;

    private float MoveToTargetSpeed = 4f;

    [NonSerialized] public bool playWhistle;
    [NonSerialized] public bool moveAroundGold = false;
    private bool exit = false;
    private bool startRight;
    private bool arrowMove; 

    [NonSerialized] public Vector3 goldTrophyPosition;
    private Vector3 leavePosition;

    private float rotationalRadius = 4.6f;
    private float rotationalSpeed = 2f;
    private bool turnAround = false;
    private bool isRotating = false;
    private bool waveFlag = false;

    public enum State
    {
        idle,
        Move,
        Rotate,
        HoldingBubble
    }
    private State state;

    public static Referee Create(Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.referee, chunk.transform);
        obj.name = "Referee";
        var item = obj.GetComponent<Referee>();
        item.Init(chunk);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.chunk = chunk;

        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, false, false)
        };

        this.animated.PlayAndLoop(this.idleAnimation);

        this.mainCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        if (GameCamera.IsLandscape() == true)
        {
            this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(-0.1f, 0.7f, 0));
        }
        else
        {
            this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(-0.3f, 0.7f, 0));
        }

        this.facingRight = true;
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.transform.position = this.position = startEntryPoint;
        this.startRight = UnityEngine.Random.Range(0.0f, 1.0f) >= 0.5 ? true : false;
        this.playWhistle = false;
        this.arrowMove = false;

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        instance = this;
    }

    public override void Advance()
    {
        this.breatheTimer += Time.deltaTime;
        float theta = this.breatheTimer / BreathePeriod;
        float distance = BreatheAmplitude * Mathf.Sin(theta);
        if (this.arrowMove == true)
        {
            this.breathePos = this.breatheMovement ? Vector2.right * distance : Vector2.zero;
        }
        else
        {
            this.breathePos = this.breatheMovement ? Vector2.up * distance : Vector2.zero;
        }

        SwitchToNearestChunk();
        switch (this.state)
        {
            case (State.Move):
                MoveTarget();
                TrailParticles();
                if (this.map.player.alive == false)
                {
                    DestroyObject();
                }
                break;
            case (State.idle):
                PlayWhistle();
                break;
            case (State.Rotate):
                TrailParticles();
                break;
            case (State.HoldingBubble):
                CheckBubbleTouched();
                DropBubble();
                if (this.map.player.alive == false)
                {
                    DestroyObject();
                }
                break;
        }

        this.transform.position = this.position + breathePos;

        if (this.moveAroundGold)
        {
            this.moveAroundGold = false;
            MoveAroundGold();
        }

        if(instance != this)
        {
            DestroyObject();
        }
    }

    private void MoveTarget()
    {
        this.position = Vector2.MoveTowards(
            this.position, this.startTargetPoint,
            this.MoveToTargetSpeed * Time.deltaTime
        );

        if (Vector2.Distance(this.position, startTargetPoint) < .01f)
        {
            if(this.turnAround == true)
            {
                this.turnAround = false;
                this.animated.PlayOnce(this.turnAnimation, () =>
                {
                    this.facingRight = !this.facingRight;
                    this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
                    this.isRotating = false;
                    if (this.waveFlag == true)
                    {
                        PlayWinAnimation();
                    }
                    else
                    {
                        this.animated.PlayAndLoop(this.idleAnimation);
                    }
                });
                this.state = State.idle;
            }
            else
            {
                if(this.arrowMove == true)
                {
                    this.state = State.idle;
                    this.playWhistle = false;
                    this.animated.PlayOnce(this.turnAnimation, () =>
                    {
                        if (this.facingRight == true)
                        {
                            this.startEntryPoint = this.transform.position;
                            this.startTargetPoint = this.transform.position - new Vector3(5, 0, 0);
                        }
                        else
                        {
                            this.startEntryPoint = this.transform.position;
                            this.startTargetPoint = this.transform.position + new Vector3(5, 0, 0);
                        }

                        this.arrowMove = false;
                        this.MoveToTargetSpeed = 5f;
                        this.animated.PlayAndLoop(this.flyAnimation);
                        this.facingRight = !this.facingRight;
                        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
                        this.transform.position = this.position = this.startEntryPoint;
                        this.BreatheAmplitude = .9f;
                        this.state = State.Move;
                        this.exit = true;
                    });
                    return;
                }

                if (this.exit == true)
                {
                    this.exit = false;
                    DestroyObject();
                }
                else
                {
                    this.animated.PlayAndLoop(this.idleAnimation);
                }
                this.state = State.idle;
            }
        }
    }

    public void PlayWhistle()
    {
        if (this.playWhistle == true)
        {
            this.playWhistle = false;
            this.animated.PlayOnce(this.blowWhistleAnimation, () =>
            {
                this.animated.PlayAndLoop(this.idleAnimation);
                StartCoroutine(LeaveCoroutine());
            });
            this.animated.OnFrame(9, () =>
            {
                Audio.instance.PlaySfx(Assets.instance.sfxRefWhistle);
            });
        }
    }

    public void ComeIn()
    {
        this.facingRight = true;

        if (GameCamera.IsLandscape() == true)
        {
            this.startEntryPoint = this.leavePosition = mainCamera.ViewportToWorldPoint(new Vector3(-0.1f, 0.42f, 0));
            this.startTargetPoint =  mainCamera.ViewportToWorldPoint(new Vector3(0.37f, 0.42f, 0));
        }
        else
        {
            this.MoveToTargetSpeed = 4;
            this.startEntryPoint = this.leavePosition = mainCamera.ViewportToWorldPoint(new Vector3(-0.3f, 0.35f, 0));
            this.startTargetPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.22f, 0.35f, 0));
        }

        this.transform.position = this.position = startEntryPoint;
        this.playWhistle = false;
        this.facingRight = true;
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.state = State.Move;
    }

    private IEnumerator LeaveCoroutine()
    {
        yield return new WaitForSeconds(.8f);
        if (this.startRight == true)
        {
            this.animated.PlayOnce(this.turnAnimation, () =>
            {
                this.animated.PlayAndLoop(this.flyAnimation);
                if (GameCamera.IsLandscape() == true)
                {
                    Vector3 cameraOutPoition = mainCamera.ViewportToWorldPoint(new Vector3(-0.1f, 0.42f, 0));
                    this.startTargetPoint = new Vector3(cameraOutPoition.x, leavePosition.y, 0);
                    this.facingRight = false;
                }
                else
                {
                    Vector3 cameraOutPoition = mainCamera.ViewportToWorldPoint(new Vector3(-0.3f, 0.35f, 0));
                    this.startTargetPoint = new Vector3(cameraOutPoition.x, leavePosition.y, 0);
                    this.facingRight = false;
                }
                this.startRight = !startRight;
                this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
                this.state = State.Move;
                this.exit = true;
            });
        }
        else
        {
            this.animated.PlayAndLoop(this.flyAnimation);
            if (GameCamera.IsLandscape() == true)
            {
                Vector3 cameraOutPoition = mainCamera.ViewportToWorldPoint(new Vector3(1.1f, 0.45f, 0));
                this.startTargetPoint = new Vector3(cameraOutPoition.x, leavePosition.y, 0);
                this.facingRight = true;
            }
            else
            {
                Vector3 cameraOutPoition = mainCamera.ViewportToWorldPoint(new Vector3(1.3f, 0.35f, 0));
                this.startTargetPoint = new Vector3(cameraOutPoition.x, leavePosition.y, 0);
                this.facingRight = true;
            }
            this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
            this.startRight = !startRight;
            this.state = State.Move;
            this.exit = true;
        }
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
    }

    public void MoveArrow()
    {
        if (GameCamera.IsLandscape() == true)
        {
            if (this.startRight == true)
            {
                this.startTargetPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.6f, 1.1f, 0));
                this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.6f, -0.1f, 0));
                this.facingRight = false;
            }
            else
            {
                this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.4f, -0.1f, 0));
                this.startTargetPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.4f, 1.1f, 0));
                this.facingRight = true;
            }
        }
        else
        {
            if (this.startRight == true)
            {
                this.startTargetPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.8f, 1.1f, 0));
                this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.8f, -0.1f, 0));
                this.facingRight = false;
            }
            else
            {
                this.startEntryPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.2f, -0.1f, 0));
                this.startTargetPoint = mainCamera.ViewportToWorldPoint(new Vector3(0.2f, 1.1f, 0));
                this.facingRight = true;
            }
            this.MoveToTargetSpeed = 6;
        }

        this.arrowMove = true;
        this.MoveToTargetSpeed = 3f;
        this.animated.PlayAndLoop(this.arrowAnimation);
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.transform.position = this.position = this.startEntryPoint;
        this.BreatheAmplitude = .9f;
        this.state = State.Move;
        this.exit = true;
    }

    public void MoveBubble()
    {
        if (this.map.player.facingRight == false)
        {
            this.startRight = true;
            this.facingRight = false;
            
            this.transform.position = this.position = this.startEntryPoint = new Vector3(this.map.currentCheckpoint.position.x + 1.5f, this.map.currentCheckpoint.position.y + 3, 0);

            this.hitboxes = new[] {
                new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, false, false),
                new Hitbox(-.5f, 1.1f, 1f, 2.5f, this.rotation, Hitbox.NonSolid, false, false)
            };
        }
        else
        {
            this.startRight = false;
            this.facingRight = true;

            this.transform.position = this.position = this.startEntryPoint = new Vector3(this.map.currentCheckpoint.position.x - 1.5f, this.map.currentCheckpoint.position.y + 3, 0);

            this.hitboxes = new[] {
                new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, false, false),
                new Hitbox(-1.1f, .5f, 1f, 2.5f, this.rotation, Hitbox.NonSolid, false, false)
            };
        }

        this.transform.Find("star").gameObject.SetActive(true);
        this.animated.PlayAndLoop(this.bubbleAnimation);
        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.BreatheAmplitude = .5f;
        this.state = State.HoldingBubble;
        this.exit = true;


        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position.Add(0,.5f),
            this.transform.parent
        );
        p.transform.localScale = Vector3.one * 1.4f;
        p.spriteRenderer.sortingLayerName = "Items";
        p.spriteRenderer.sortingOrder = 101;
    }

    public void CheckBubbleTouched()
    {
        var thisRect = this.hitboxes[1].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
        {
            this.transform.Find("star").gameObject.SetActive(false);

            Particle p;
            if (this.facingRight == true)
            {
                this.startEntryPoint = this.transform.position;
                this.startTargetPoint = this.transform.position + new Vector3(15, 0, 0);
                p = Particle.CreateAndPlayOnce(
                    this.bubblePopAnimation,
                    this.position.Add(-0.3f, 3f),
                    this.transform.parent
                 );
            }
            else
            {
                this.startEntryPoint = this.transform.position;
                this.startTargetPoint = this.transform.position - new Vector3(15, 0, 0);
                p = Particle.CreateAndPlayOnce(
                    this.bubblePopAnimation,
                    this.position.Add(0.3f, 3f),
                    this.transform.parent
                );
            }
            p.animated.OnFrame(3, delegate
            {
                this.map.player.invincibility.Activate(activatedByBubble: true, activatedByReferee: true);
            });

            this.arrowMove = false;
            this.MoveToTargetSpeed = 3.5f;
            this.animated.PlayAndLoop(this.flyAnimation);
            this.transform.position = this.position = this.startEntryPoint;
            this.BreatheAmplitude = .9f;
            this.state = State.Move;
            this.exit = true;
        }
    }

    public void DropBubble()
    {
        if (this.map.player.position.y > this.position.y + 5)
        {
            this.transform.Find("star").gameObject.SetActive(false);

            if (this.facingRight == true)
            {
                BubblePickup.Create(this.position + new Vector2(-0.3f, 3f), this.chunk, PowerupPickup.Type.BubbleInvincibility, true);
                this.startEntryPoint = this.transform.position;
                this.startTargetPoint = this.transform.position + new Vector3(15, 0, 0);
            }
            else
            {
                BubblePickup.Create(this.position + new Vector2(0.3f, 3f), this.chunk, PowerupPickup.Type.BubbleInvincibility, true);
                this.startEntryPoint = this.transform.position;
                this.startTargetPoint = this.transform.position - new Vector3(15, 0, 0);
            }

            this.MoveToTargetSpeed = 3.5f;
            this.animated.PlayAndLoop(this.flyAnimation);
            this.transform.position = this.position = this.startEntryPoint;

            this.state = State.Move;
            this.exit = true;
        }
    }
    public void MoveAroundGold()
    {
        if (this.startRight == true)
        {
            this.startEntryPoint = goldTrophyPosition + new Vector3(8, -2, 0);
            this.startTargetPoint = goldTrophyPosition + new Vector3(5f, -2, 0);
            this.MoveToTargetSpeed = 4f;
            this.facingRight = false;
        }
        else
        {
            this.startEntryPoint = goldTrophyPosition + new Vector3(-8, -2, 0);
            this.startTargetPoint = goldTrophyPosition + new Vector3(-5f, -2, 0);
            this.MoveToTargetSpeed = 4f;
            this.facingRight = true;
        }

        this.animated.PlayAndLoop(this.flyAnimation);

        this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        this.transform.position = this.position = startEntryPoint;
        this.BreatheAmplitude = .7f;
        this.BreathePeriod = .3f;
        this.state = State.Move;
        this.exit = false;

        if(this.gameObject.activeInHierarchy == true)
        {
            StartCoroutine(RotateCoroutine());
        }
    }

    IEnumerator RotateCoroutine()
    {
        yield return new WaitForSeconds(2f);

        this.isRotating = true;
        this.state = State.Rotate;
        this.animated.PlayAndLoop(this.flyAnimation);
        this.gameObject.GetComponent<SpriteRenderer>().sortingLayerName = "Items (Front)";
        this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 98;
        int framenumber = 0;
        float angle;

        if (this.facingRight == true)
        {
            angle = Mathf.PI * 1.2f;
        }
        else
        {
            angle = 0;
        }

        float currrentTime = 0;
        while (currrentTime < 2)
        {
            if (((framenumber / 60.0f) * this.rotationalSpeed) >= Mathf.PI || this.isRotating == false)
            {
                break;
            }
            framenumber++;
            Vector2 targetPosition;
            targetPosition.x = this.goldTrophyPosition.x + Mathf.Cos(angle + ((framenumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.y = this.goldTrophyPosition.y + Mathf.Sin(angle + ((framenumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            this.position = Vector3.MoveTowards(this.position, targetPosition, this.MoveToTargetSpeed * 1.5f * Time.deltaTime);
            yield return null;
        }

        this.animated.PlayOnce(this.turnAnimation, () =>
        {
            this.animated.PlayAndLoop(this.flyAnimation);
            this.facingRight = !this.facingRight;
            this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
        });

        currrentTime = 0;
        while (currrentTime < 2)
        {
            if (((framenumber / 60.0f) * this.rotationalSpeed) >= 1.85 *Mathf.PI || this.isRotating == false)
            {
                break;
            }
            framenumber++;
            Vector2 targetPosition;
            targetPosition.x = this.goldTrophyPosition.x + Mathf.Cos(angle + ((framenumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.y = this.goldTrophyPosition.y + Mathf.Sin(angle + ((framenumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            this.position = Vector3.MoveTowards(this.position, targetPosition, this.MoveToTargetSpeed * 1.5f * Time.deltaTime);
            yield return null;
        }

        this.MoveToTargetSpeed *= 1.5f;
        
        if (this.startRight == true)
        {
            this.startEntryPoint = this.position;
            this.startTargetPoint = goldTrophyPosition + new Vector3(5f, -2, 0);
            this.MoveToTargetSpeed = 5f;
            if(this.isRotating == false && this.facingRight == false)
            {
                this.animated.PlayOnce(this.turnAnimation, () =>
                {
                    this.animated.PlayAndLoop(this.idleAnimation);
                    this.facingRight = !this.facingRight;
                    this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
                });
            }
        }
        else
        {
            this.startEntryPoint = this.position;
            this.startTargetPoint = this.goldTrophyPosition + new Vector3(-5f, -2, 0);
            this.MoveToTargetSpeed = 5f;
            if (this.isRotating == false && this.facingRight == true)
            {
                this.animated.PlayOnce(this.turnAnimation, () =>
                {
                    this.animated.PlayAndLoop(this.idleAnimation);
                    this.facingRight = !this.facingRight;
                    this.transform.localScale = new Vector3(this.facingRight ? -1 : 1, 1, 1);
                });
            }
        }

        this.BreatheAmplitude = .7f;
        this.state = State.Move;
        this.turnAround = true;
    }

    public void PlayWinAnimation()
    {
        if(this.isRotating == false)
        {
            this.animated.PlayAndLoop(this.flagWaveAnimation);
        }
        else
        {
            this.waveFlag = true;
        }
    }

    private void TrailParticles()
    {
        bool createSparkleParticle = this.map.frameNumber % 5 == 0;

        if (createSparkleParticle == true)
        {
            float x, y;

            if (this.arrowMove == true)
            {
                x = (this.facingRight == true) ? this.position.x - .5f : this.position.x + .5f;
                y = this.position.y - 1.2f;

            }
            else
            {
                x = (this.facingRight == true) ? this.position.x - 1 : this.position.x + 1;
                y = this.position.y - 1.2f;
            }

            Particle p = Particle.CreateAndPlayOnce(
                    this.map.player.animationDustParticle,
                    new Vector2(
                        x + UnityEngine.Random.Range(-0.1f, 0.1f),
                        y + UnityEngine.Random.Range(-0.4f, 0.4f) - 0.5f
                    ) + breathePos,
                    this.transform.parent
                );
            float s = UnityEngine.Random.Range(1.2f, 1.5f);
            p.transform.localScale = new Vector3(s, s, 1);
            p.spriteRenderer.sortingLayerName = "Items";
            p.spriteRenderer.sortingOrder = 99;
            if(this.map.theme == Theme.WindySkies)
            {
                p.spriteRenderer.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
            }
        }
    }

    public void DestroyObject()
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyDeath2Animation,
            this.position.Add(0, .5f),
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Items";
        p.spriteRenderer.sortingOrder = 101;
        p.transform.localScale = Vector3.one * 1.4f;
        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }
}
