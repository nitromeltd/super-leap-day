using UnityEngine;
using System.Linq;

public class RockButton : Item
{
    public SpriteRenderer buttonSpriteRenderer;
    private bool pressed;
    private bool pressedThisFrame;

    public bool Pressed { get => pressed; }
    public bool PressedThisFrame { get => pressedThisFrame; }

    public override void Init(Def def)
    {
        base.Init(def);

        if (this.def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1, 1, 1);
            this.hitboxes = new [] {
                new Hitbox(-0.5f, 0.5f, -1, 1, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(0.5f, 0.9f, -0.7f, 0.7f, this.rotation, Hitbox.NonSolid)
            };
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-0.5f, 0.5f, -1, 1, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(-0.9f, -0.5f, -0.7f, 0.7f, this.rotation, Hitbox.NonSolid)
            };
        }

        this.pressed = false;
        this.pressedThisFrame = false;

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        var pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if (this.pressed == true)
        {
            this.buttonSpriteRenderer.transform.localPosition = new Vector2(-0.46f, 0);
            this.pressedThisFrame = (pressedLastFrame == false);
        }
        else
        {
            this.buttonSpriteRenderer.transform.localPosition = new Vector2(-0.8f, 0);
            this.pressedThisFrame = false;
        }

        if (pressedThisFrame == true)
        {
            this.map.lavaline.MakeSolidTemporarily(5 * 60);
        }
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[1].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        var hitboxRect = this.hitboxes[1].InWorldSpace();
        Vector2[] SensorsWhenHorizontal() => new []
        {
            new Vector2(hitboxRect.xMin,     hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.xMax,     hitboxRect.center.y)
        };
        Vector2[] SensorsWhenVertical() => new []
        {
            new Vector2(hitboxRect.center.x, hitboxRect.yMin),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.yMax)
        };

        var sensors =
            (this.rotation == 0 || this.rotation == 180) ?
            SensorsWhenVertical() :
            SensorsWhenHorizontal();

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }
}
