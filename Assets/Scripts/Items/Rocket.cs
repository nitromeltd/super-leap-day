
using System.Collections.Generic;
using UnityEngine;

public class Rocket : Item
{
    public enum State { Waiting, Flying, Destroyed }

    public Sprite spriteDot;
    public SpriteRenderer[] thrusters;
    public Animated.Animation animationExplode;
    public Sprite spriteDustParticle;

    private State state;
    private int ignoreTime;
    private int timeSincePlayerJumpedOut = 0;
    private int destroyedTime = 0;

    private NitromeEditor.Path path;
    private Vector2 offset;
    private float distanceToStartAlongPath;
    private float distanceAlongPath;
    private float speed;
    private bool isInMidairAtStartPoint;

    private List<(Vector2 position, SpriteRenderer sr)> dots;

    public override void Init(Def def)
    {
        base.Init(def);

        this.state = State.Waiting;
        this.ignoreTime = 0;
        // this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        
        this.grabInfo = new GrabInfo(this, GrabType.Ceiling, new Vector2(0, -1));
        this.grabInfo.autoReleaseDistance = 0.75f;

        this.path = def.startChunk.pathLayer.NearestPathTo(this.position);
        this.distanceToStartAlongPath = this.distanceAlongPath =
            this.path.NearestPointTo(this.position)?.distanceAlongPath ?? 0;

        {
            var pt = this.path.PointAtDistanceAlongPath(this.distanceAlongPath);

            this.transform.position = this.position = pt.position;

            var angleDegrees = Mathf.Atan2(pt.tangent.y, pt.tangent.x) * 180 / Mathf.PI;
            this.rotation = Mathf.RoundToInt(angleDegrees - 90);
            this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);

            var raycast = new Raycast(this.map, this.position);
            var backwards = -pt.tangent;
            var result = raycast.Arbitrary(this.position, backwards, 2);
            this.isInMidairAtStartPoint = (result.anything == false);
        }

        this.dots = new List<(Vector2, SpriteRenderer)>();
        for (float f = 0; f < this.path.length; f += 1)
        {
            var pt = this.path.PointAtDistanceAlongPath(f);
            var go = new GameObject("Dot");
            go.transform.SetParent(this.chunk.transform);
            go.transform.position = pt.position;
            var sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = this.spriteDot;
            sr.sortingLayerName = "Moving Platform Dots";
            go.layer = this.map.Layer();
            this.dots.Add((pt.position, sr));
        }

        foreach (var thruster in this.thrusters)
        {
            thruster.gameObject.SetActive(false);
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Reset()
    {
        this.distanceAlongPath = this.distanceToStartAlongPath;
        var pt = this.path.PointAtDistanceAlongPath(this.distanceAlongPath);

        this.transform.position = this.position = pt.position;

        var angleDegrees = Mathf.Atan2(pt.tangent.y, pt.tangent.x) * 180 / Mathf.PI;
        this.rotation = Mathf.RoundToInt(angleDegrees - 90);
        this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);

        Audio.instance.StopSfx(Assets.instance.gravityGalaxy.sfxRocketFlying);
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            foreach (var (position, sr) in this.dots)
            {
                sr.transform.position = position + this.group.Offset();
            }
        }

        switch (this.state)
        {
            case State.Waiting:
                AdvanceWaiting();
                break;

            case State.Flying:
                AdvanceFlying();
                break;

            case State.Destroyed:
                AdvanceDestroyed();
                break;
        }
        
        this.transform.position = this.position;
        this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
    }

    private void AdvanceWaiting()
    {
        if (this.group != null)
            this.position = this.group.FollowerPosition(this);

        foreach (var thruster in this.thrusters)
        {
            thruster.gameObject.SetActive(false);
        }

        if (this.isInMidairAtStartPoint == true)
        {
            var pt =
                this.group != null ?
                this.group.FollowerPosition(this) :
                this.path.PointAtDistanceAlongPath(this.distanceAlongPath).position;

            var speed = 0.010f + (((this.def.tx + this.def.ty) % 8) * 0.001f);
            var theta = this.map.frameNumber * 0.015f;
            theta += this.def.tx;
            theta += this.def.ty * 0.2f;
            pt.x += Mathf.Sin(theta) * 0.25f;

            speed = 0.013f + (((this.def.tx + this.def.ty + 4) % 8) * 0.001f);
            theta = this.map.frameNumber * 0.019f;
            theta += this.def.tx;
            theta += this.def.ty * 0.2f;
            pt.y += Mathf.Sin(theta) * 0.25f;

            this.position = pt;
        }

        var player = this.map.player;
        if (player.grabbingOntoItem != null) return;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var alongDirection = new Vector2(
            Mathf.Cos((this.rotation + 90) * Mathf.PI / 180),
            Mathf.Sin((this.rotation + 90) * Mathf.PI / 180)
        );
        var playerAlong = Vector2.Dot(player.position - this.position, alongDirection);
        playerAlong = Mathf.Clamp(playerAlong, -0.7f, 0.7f);
        var nearestPointToPlayer = this.position + (alongDirection * playerAlong);

        if ((player.position - nearestPointToPlayer).sqrMagnitude < 1.3f * 1.3f)
        {
            player.EnterRocket(this);
            this.ignoreTime = 15;
            this.state = State.Flying;
            this.offset = this.group?.Offset() ?? Vector2.zero;
            Audio.instance.PlaySfx(Assets.instance.gravityGalaxy.sfxRocketFlying);

            foreach (var thruster in this.thrusters)
            {
                thruster.gameObject.SetActive(true);
            }
        }
    }

    private void AdvanceFlying()
    {
        if (RaycastForward().anything == true ||
            this.timeSincePlayerJumpedOut >= 75)
        {
            Audio.instance.StopSfx(Assets.instance.gravityGalaxy.sfxRocketFlying);
            Audio.instance.PlaySfx(Assets.instance.sfxEnvExplosion);

            this.state = State.Destroyed;
            this.destroyedTime = 0;
            this.gameObject.SetActive(false);
            CreateExplosionParticles();

            if (this.map.player.state == Player.State.InsideRocket &&
                this.map.player.InsideRocket == this)
            {
                this.map.player.ExitRocket();
                this.map.player.Hit(this.position);
            }
            return;
        }

        var adjustmentFactor = Util.AdjustmentFactorForCompressedArcs(
            this.path, this.distanceAlongPath
        );

        this.speed = Util.Slide(this.speed, 0.2f, 0.003f);
        this.distanceAlongPath += speed * adjustmentFactor;
        if (this.distanceAlongPath > this.path.length)
            this.distanceAlongPath = this.path.length;

        var point = this.path.PointAtDistanceAlongPath(this.distanceAlongPath);
        this.position = point.position + this.offset;
        this.rotation = Mathf.RoundToInt(
            (Mathf.Atan2(point.tangent.y, point.tangent.x) * 180 / Mathf.PI) - 90
        );

        if (this.distanceAlongPath > this.path.length)
        {
            this.position += point.tangent * (this.distanceAlongPath - this.path.length);
        }

        if (this.map.player.state == Player.State.InsideRocket &&
            this.map.player.InsideRocket == this)
        {
            if (point.tangent.x > 0)
                this.map.player.facingRight = true;
            else if (point.tangent.x < 0)
                this.map.player.facingRight = false;

            this.map.player.velocity = point.tangent * this.speed;
        }
        else
        {
            this.timeSincePlayerJumpedOut += 1;
        }

        this.grabInfo.grabPositionRelativeToItem = point.tangent * -1f;
    }

    private void CreateExplosionParticles()
    {
        Particle.CreateAndPlayOnce(
            Assets.instance.explosionAnimation,
            this.position,
            this.transform.parent
        );

        for (int i = 0; i < 30; i++)
        {
            var angleRadians = Random.Range(0, Mathf.PI * 2);
            Vector2 direction = new Vector2(
                Mathf.Cos(angleRadians),
                Mathf.Sin(angleRadians)
            );
            var particlePosition = this.position + (direction * Random.Range(0.8f, 0.9f));
            var particleVelocity = direction * Random.Range(0.02f, 0.03f);

            Particle trailParticle = Particle.CreateAndPlayOnce(
                Assets.instance.genericDustParticleAnimation,
                particlePosition,
                this.transform.parent
            );

            float s = Random.Range(0.8f, 1.4f);
            trailParticle.transform.localScale = new Vector3(s, s, 1);
            // trailParticle.ScaleOut(this.scaleOutCurve);
            trailParticle.animated.playbackSpeed = Random.Range(0.3f, 0.5f);
            trailParticle.velocity = particleVelocity;
            trailParticle.spriteRenderer.sortingLayerName = "Player Trail (BL)";
            // trailParticle.spriteRenderer.sortingOrder = -(1 + sortingOrderOffset + (this.map.frameNumber % 32));
        }
    }

    private void AdvanceDestroyed()
    {
        this.destroyedTime += 1;
        if (this.destroyedTime > 60)
        {
            this.state = State.Waiting;
            this.distanceAlongPath = this.distanceToStartAlongPath;
            var pt = this.path.PointAtDistanceAlongPath(this.distanceAlongPath);

            this.offset = this.group?.Offset() ?? Vector2.zero;
            this.position = pt.position + this.offset;
            this.rotation = Mathf.RoundToInt(
                (Mathf.Atan2(pt.tangent.y, pt.tangent.x) * 180 / Mathf.PI) - 90
            );
            this.timeSincePlayerJumpedOut = 0;

            this.gameObject.SetActive(true);

            for (var n = 0; n < 20; n += 1)
            {
                var theta = Random.Range(0, Mathf.PI * 2);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateAndPlayOnce(
                    Assets.instance.genericDustParticleAnimation,
                    this.position.Add(cos_theta, sin_theta),
                    this.transform.parent
                );
                float speed = Random.Range(0.01f, 0.04f);
                p.velocity = new Vector2(
                    cos_theta * speed,
                    sin_theta * speed
                );
                p.transform.localScale = Vector3.one * 1.5f;
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.animated.playbackSpeed = Random.Range(0.3f, 0.6f);
            }
        }
    }

    private Raycast.CombinedResults RaycastForward()
    {
        var pt = this.path.PointAtDistanceAlongPath(this.distanceAlongPath);
        var forward = pt.tangent;
        var sideways = new Vector2(forward.y, -forward.x);
        var raycast = new Raycast(this.map, this.position, ignoreTileTopOnlyFlag: true);

        var front = this.position + (forward * 0.75f);
        var r1 = raycast.Arbitrary(front + (sideways * -0.75f), forward, this.speed);
        var r2 = raycast.Arbitrary(front,                       forward, this.speed);
        var r3 = raycast.Arbitrary(front + (sideways *  0.75f), forward, this.speed);
        return Raycast.CombineClosest(r1, r2, r3);
    }

    public void AdvancePlayer()
    {
        this.map.player.position = this.position;
    }

    public void FirePlayer()
    {
        this.map.player.ExitRocket();
        this.map.player.velocity =
            this.path.PointAtDistanceAlongPath(this.distanceAlongPath).tangent *
            this.speed;
    }
}
