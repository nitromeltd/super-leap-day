
using UnityEngine;

public class Rosette : Item
{
    public Sprite spriteNormal;
    public Sprite spriteBronze;
    public Sprite spriteSilver;
    public Sprite spriteGold;

    public override void Init(Def def)
    {
        base.Init(def);

        switch (this.chunk.checkpointNumber)
        {
            case 10: this.spriteRenderer.sprite = this.spriteBronze; break;
            case  5: this.spriteRenderer.sprite = this.spriteSilver; break;
            case  0: this.spriteRenderer.sprite = this.spriteGold;   break;
            default: this.spriteRenderer.sprite = this.spriteNormal; break;
        }

        var textField = this.GetComponentInChildren<TMPro.TextMeshPro>();
        var star = this.transform.Find("star").gameObject;

        if (this.chunk.checkpointNumber == 0)
        {
            star.SetActive(true);
            textField.gameObject.SetActive(false);
        }
        else
        {
            star.SetActive(false);
            textField.text = this.chunk.checkpointNumber.ToString();
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
    }
}
