
using UnityEngine;

public class RosetteBanner : Item
{
    public Sprite spriteEndNormal;
    public Sprite spriteEndBronze;
    public Sprite spriteEndSilver;
    public Sprite spriteEndGold;
    public Sprite spriteMiddleNormal;
    public Sprite spriteMiddleBronze;
    public Sprite spriteMiddleSilver;
    public Sprite spriteMiddleGold;
    public Sprite spriteShadowNormal;
    public Sprite spriteShadowBronze;
    public Sprite spriteShadowSilver;
    public Sprite spriteShadowGold;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);

        if (def.tile.tile.Contains("end"))
        {
            switch (this.chunk.checkpointNumber)
            {
                case 10: this.spriteRenderer.sprite = this.spriteEndBronze; break;
                case  5: this.spriteRenderer.sprite = this.spriteEndSilver; break;
                case  0: this.spriteRenderer.sprite = this.spriteEndGold;   break;
                default: this.spriteRenderer.sprite = this.spriteEndNormal; break;
            }
        }
        else if (def.tile.tile.Contains("middle"))
        {
            switch (this.chunk.checkpointNumber)
            {
                case 10: this.spriteRenderer.sprite = this.spriteMiddleBronze; break;
                case  5: this.spriteRenderer.sprite = this.spriteMiddleSilver; break;
                case  0: this.spriteRenderer.sprite = this.spriteMiddleGold;   break;
                default: this.spriteRenderer.sprite = this.spriteMiddleNormal; break;
            }
        }
        else if (def.tile.tile.Contains("shadow"))
        {
            switch (this.chunk.checkpointNumber)
            {
                case 10: this.spriteRenderer.sprite = this.spriteShadowBronze; break;
                case  5: this.spriteRenderer.sprite = this.spriteShadowSilver; break;
                case  0: this.spriteRenderer.sprite = this.spriteShadowGold;   break;
                default: this.spriteRenderer.sprite = this.spriteShadowNormal; break;
            }
        }

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);
    }
}
