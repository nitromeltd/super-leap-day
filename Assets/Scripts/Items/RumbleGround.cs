using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using System.Linq;
using Rnd = UnityEngine.Random;

public class RumbleGround : Item
{
    private Path path;
    private float timeAtStart;

    public bool goingDown;
    public bool goingBack;
    private Vector2 startingPosition;
    private Vector2 originalPosition;
    private string endBehaviour = "stop";
    private float currentTimeOnPath;
    private const float MaxSpeedOnPath = 1f;

    private Vector3 positionLastFrame;
    private bool isPlayingSfx = false;
    public bool playerCrossedChunk;

    private List<Item> itemsToTrigger = new List<Item>();
    private List<Item> incomingItems = new List<Item>();

    private float totalTravelTime = 2 * 60;
    private float travelTimePerFrame = 1;

    private RumbleGroundBlock[,] grid;
    private List<RumbleGroundBlock> rumbleBlocks;
    private int gridOriginX;
    private int gridOriginY;
    private bool autotileApplied;

    public Vector2 StartingPosition => this.startingPosition;

    public override void Init(Def def)
    {
        base.Init(def);
        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 2);
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));
        this.startingPosition = this.originalPosition = this.position;
        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        this.playerCrossedChunk = false;        
        if (def.tile.properties?.ContainsKey("travel_time_in_seconds") == true)
            this.totalTravelTime = def.tile.properties["travel_time_in_seconds"].f * 60;

        if(this.path != null)
            this.travelTimePerFrame = this.path.duration / this.totalTravelTime;

        if (def.tile.properties?.ContainsKey("end_behaviour") == true)
        {
            this.endBehaviour = def.tile.properties["end_behaviour"].s;
        }
    }

    private void DebugConnections(List<Path> itemToDebug)
    {
        int i = 0;
        foreach(var p in itemToDebug)
        {
            int j = 0;
            foreach(var e in p.edges)
            {
                Debug.Log($"{i}.{j}:{e.from.Position}=>{e.to.Position}");
                j++;
            }
            i++;
        }
    }

    private void AdvanceRumbleGroundBlocks()
    {
        if(this.grid == null)
        {
            this.rumbleBlocks = new List<RumbleGroundBlock>();

            foreach(var f in this.group.followers)
            {
                if(f.Key.item is RumbleGroundBlock)
                {
                    rumbleBlocks.Add(f.Key.item as RumbleGroundBlock);
                }
            }

            SetupAutotiling();
        }
        else
        {
            if (Time.frameCount % 3 == 1 || this.autotileApplied == false)
            {
                UpdateAutotiling();
                this.autotileApplied = true;
            }
        }

        foreach(var f in this.group.followers)
        {
            f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
            f.Key.item.transform.position = f.Key.item.position;
        }
    }

    public bool IsItemInGrid(int mapX, int mapY)
    {
        if(this.grid == null) return false;

        var delta = this.originalPosition - this.startingPosition;
        int tx = this.gridOriginX + Mathf.RoundToInt(delta.x);
        int ty = this.gridOriginY + Mathf.RoundToInt(delta.y);

        int x = mapX - tx;
        int y = mapY - ty;


        if(x < 0 || x >= this.grid.GetLength(0))
            return false;
        if(y < 0 || y >= this.grid.GetLength(1))
            return false;
        
        return this.grid[x, y] != null;
    }
   

    private bool IsItemInGridAndSurroundingArea(int x, int y)
    {
        if( x < 0 || x >= this.grid.GetLength(0)
            || y < 0 || y >= this.grid.GetLength(1) )
        {
            var delta = this.originalPosition - this.startingPosition;
            int dx = this.gridOriginX + x + Mathf.RoundToInt(delta.x);
            int dy = this.gridOriginY + y + Mathf.RoundToInt(delta.y);

            foreach(RumbleGround rg  in this.chunk.subsetOfItemsOfTypeRumbleGround)
            {
                if(rg == this) continue;
                if(rg.IsItemInGrid(dx,dy))
                    return true;
            }
            return false;
        }

        return this.grid[x, y] != null;
    }

    private void SetupAutotiling()
    {       
        if(this.rumbleBlocks.Count <= 0)
        {
            Debug.Log("Rumble Ground Block group is missing blocks!");
            return;
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(this.rumbleBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(this.rumbleBlocks.Min(t => t.position.y));
        
        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(this.rumbleBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(this.rumbleBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new RumbleGroundBlock[sizeX, sizeY];

        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;

        gridOriginX = minX;
        gridOriginY = minY;

        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in this.rumbleBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGridAndSurroundingArea(x, y + 1);
                bool down = IsItemInGridAndSurroundingArea(x, y - 1);
                bool left = IsItemInGridAndSurroundingArea(x - 1, y);
                bool right = IsItemInGridAndSurroundingArea(x + 1, y);

                bool upLeft = IsItemInGridAndSurroundingArea(x - 1, y + 1);
                bool upRight = IsItemInGridAndSurroundingArea(x + 1, y + 1);
                bool downLeft = IsItemInGridAndSurroundingArea(x - 1, y - 1);
                bool downRight = IsItemInGridAndSurroundingArea(x + 1, y - 1);

                // Debug.Log($"({x},{y}): u:{up},d:{down},l:{left},r:{right},ul:{upLeft},ur:{upRight},dl:{downLeft},dr:{downRight}");

                this.grid[x, y].myNeighbours = new RumbleGroundBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }
    }

    private void UpdateAutotiling()
    {
        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGridAndSurroundingArea(x, y + 1);
                bool down = IsItemInGridAndSurroundingArea(x, y - 1);
                bool left = IsItemInGridAndSurroundingArea(x - 1, y);
                bool right = IsItemInGridAndSurroundingArea(x + 1, y);

                bool upLeft = IsItemInGridAndSurroundingArea(x - 1, y + 1);
                bool upRight = IsItemInGridAndSurroundingArea(x + 1, y + 1);
                bool downLeft = IsItemInGridAndSurroundingArea(x - 1, y - 1);
                bool downRight = IsItemInGridAndSurroundingArea(x + 1, y - 1);

                // Debug.Log($"({x},{y}): u:{up},d:{down},l:{left},r:{right},ul:{upLeft},ur:{upRight},dl:{downLeft},dr:{downRight}");

                this.grid[x, y].myNeighbours = new RumbleGroundBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }

        // choose autotile sprite
        foreach(var t in this.rumbleBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    override public void Advance()
    {
        AdvanceRumbleGroundBlocks();
        if(this.goingBack == true)
        {
            if(this.path != null)
            {
                if(this.currentTimeOnPath >= 0)
                {
                    var mult = this.endBehaviour == "go_back_when_triggered_again" ? 1 : 2;
                    this.currentTimeOnPath = this.currentTimeOnPath - this.travelTimePerFrame * mult;
                    if(this.currentTimeOnPath <= 0)
                    {
                        this.currentTimeOnPath = 0;
                        this.transform.position = this.position = this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.goingBack = this.goingDown = false;
                    }
                    else
                    {
                        this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                    }
                }
            }
 
            PlaySfx();
            return;
        }

        if (this.map.player.alive != true || (this.playerCrossedChunk == true && this.map.player.position.y < this.transform.position.y))
        {
            if (this.path != null)
            {
                this.goingBack = true;
            }
            this.playerCrossedChunk = false;
            return;
        }

        if(this.goingDown)
        {
            if(this.path != null)
            {
                if(this.currentTimeOnPath < this.path.duration)
                {
                    this.currentTimeOnPath = this.currentTimeOnPath + this.travelTimePerFrame;
                    if(this.currentTimeOnPath >= this.path.duration)
                    {
                        this.currentTimeOnPath = this.path.duration;
                        this.transform.position = this.position = this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                    }
                    else
                    {
                        this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                    }
                }
            }
        }
        PlaySfx();

        if(this.map.NearestChunkTo(this.map.player.position) == this.map.chunks[this.map.chunks.IndexOf(chunk) + 1])
        {
            this.playerCrossedChunk = true;
        }
    }

    private void PlaySfx()
    {
        if (this.positionLastFrame != this.transform.position)
        {
            if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvUnstableGround) == false)
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnvUnstableGround, position: this.transform.position, options: new Audio.Options(0.6f, false, 1));
            }
            Audio.instance.ChangeSfxPosition(Assets.instance.sfxEnvUnstableGround, this.transform.position);
            this.isPlayingSfx = true;
        }
        else if (this.isPlayingSfx == true)
        {
            Audio.instance.StopSfx(Assets.instance.sfxEnvUnstableGround);
            this.isPlayingSfx = false;
        }
        this.positionLastFrame = this.transform.position;
    }

    public bool CanRumbleGroundGoBack()
    {
        return (this.endBehaviour == "go_back_when_triggered_again");
    }

    public void TriggerGround()
    {
        if (this.goingDown == false && this.goingBack == false)
        {
            this.goingDown = true;
            this.originalPosition = this.position;
        }
        else if(this.endBehaviour == "go_back_when_triggered_again" && this.currentTimeOnPath == this.path.duration && this.goingBack == false)
        {
            this.goingBack = true;
        }
    }

    private float TimeForFrame(int frameNumber) =>
        this.timeAtStart + (frameNumber / 60.0f);

    public override void Reset()
    {

    }
}
