
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System.Linq;

public class RumbleGroundBlock : Item
{
    [Header("Sprites")]
    public Sprite[] sprites;
    public Neighbours myNeighbours;

    private int size;
    protected static int Size = Tile.Size;
    protected static float HalfSize = Size / 2f;
    public SpriteRenderer backSpriteRenderer;

    private Sprite lastSprite = null;
    private int generateDustTime = 0;
    private bool spriteSet = false;
    private bool isFlipped = false;
    [System.Serializable]
    public struct Neighbours
    {
        public bool top;
        public bool bottom;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool topLeft;
        public bool topRight;
        public bool bottomLeft;
        public bool bottomRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool topLeft, bool topRight, bool bottomLeft, bool bottomRight)
        {
            this.top = up;
            this.bottom = down;
            this.left = left;
            this.right = right;
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }
        
        public bool IsEqual(Neighbours n, bool useCorners = false) 
        {
            bool nonCornersEqual = 
                this.top == n.top &&
                this.bottom == n.bottom &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual = 
                this.topLeft == n.topLeft &&
                this.topRight == n.topRight &&
                this.bottomLeft == n.bottomLeft &&
                this.bottomRight == n.bottomRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);
        this.size = Tile.Size;
        this.isFlipped = def.tile.flip;
        this.transform.localScale = new Vector3(this.isFlipped ? -1 : 1, 1, 1);
        if (this.isFlipped == false)
        {
            this.hitboxes = new[] {
                new Hitbox(0, Size, 0, Size, this.rotation, Hitbox.Solid, wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
            };
        }
        else
        {
            backSpriteRenderer.transform.localPosition = new Vector3(1, 0, 0);
            backSpriteRenderer.transform.localScale = new Vector3(-1, 1, 1);

            this.hitboxes = new[] {
                new Hitbox(-Size, 0, 0, Size, this.rotation, Hitbox.Solid, wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
            };
        }

        // group
        int xMin = this.chunk.xMin - this.chunk.boundaryMarkers.bottomLeft.x;
        int yMin = this.chunk.yMin - this.chunk.boundaryMarkers.bottomLeft.y;

        int tx = def.tx - xMin;
        int ty = def.ty - yMin;
        
        GroupLayer.Group g = this.chunk.groupLayer.groupForCell[tx, ty];
        //always attach as follower
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;

        // collisions
        this.onCollisionFromAbove = delegate { OnCollision(false, false, false); };
        this.onCollisionFromBelow = delegate { OnCollision(false, false, true ); };
        this.onCollisionFromLeft  = delegate { OnCollision(true,  false, false); };
        this.onCollisionFromRight = delegate { OnCollision(false, true,  false); };
    }

    override public void Advance()
    {
        float randDist = 0.05f;
        
        if(Time.frameCount % 5 == 4 && this.spriteSet == true)
        if(this.spriteRenderer.sprite != this.lastSprite || this.generateDustTime > 0)
        {
            if (this.generateDustTime > 0)
            {
                this.generateDustTime--;
            }
            else
            {
                this.generateDustTime = 5;
            }
            var tiles = Assets.instance.tombstoneHill.tileTheme.dirtTiles;
            Vector2 pos = Vector2.zero;
            this.lastSprite = this.spriteRenderer.sprite;

            if(tiles.left.Contains<Sprite>(this.spriteRenderer.sprite) == true)
            {
                pos = new Vector2(this.transform.position.x + Random.Range(-randDist, randDist),
                    this.transform.position.y + Random.Range(0, 1f));
            }
            else if(tiles.right.Contains<Sprite>(this.spriteRenderer.sprite) == true)
            {
                pos = new Vector2(this.transform.position.x + 1f + Random.Range(-randDist, randDist),
                    this.transform.position.y + Random.Range(0, 1f));
            }
            else if(tiles.top.Contains<Sprite>(this.spriteRenderer.sprite) == true)
            {
                pos = new Vector2(this.transform.position.x + Random.Range(0, 1f),
                    this.transform.position.y + 1f + Random.Range(-randDist, randDist));
            }
            else if(tiles.bottom.Contains<Sprite>(this.spriteRenderer.sprite) == true)
            {
                pos = new Vector2(this.transform.position.x + Random.Range(0, 1f),
                    this.transform.position.y + Random.Range(-randDist, randDist));
            }
            else
            {
                pos = new Vector2(this.transform.position.x + Random.Range(0, 1f),
                    this.transform.position.y + Random.Range(0, 1f));
            }
            
            Particle.CreateDust(
                this.chunk.transform, 1, 1, pos, velocity: new Vector2(0, -0.03f), variationX: 0, variationY: 0,
                sortingLayerName: "Items (Front)"
            );
        }
    }

    private void OnCollision(bool left, bool right, bool below)
    {
        
    }

    public void NotifyPlayerJumpedAway()
    {

    }

    public void ApplyAutotiling()
    {
        void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

        Sprite Select(Sprite[] sprites)
        {
            return sprites[Mathf.Abs((this.def.tx + this.def.ty) % sprites.Length)];
        }

        Sprite SelectInterior(Tileset tileset, int mapTx, int mapTy)
        {
            int cx = mapTx % tileset.interiorGridColumns;
            int cy = mapTy % tileset.interiorGridRows;
            if (cx < 0) cx += tileset.interiorGridColumns;
            if (cy < 0) cy += tileset.interiorGridRows;
            cy = (tileset.interiorGridRows - 1) - cy;
            return tileset.interior[
                cx + (cy * tileset.interiorGridColumns)
            ];
        }
        var tiles = Assets.instance.tombstoneHill.tileTheme.dirtTiles;

        bool l, r, t, b, tl, tr, bl, br;

        if(this.isFlipped)
        {
            l = myNeighbours.right;
            r = myNeighbours.left;
            t = myNeighbours.top;
            b = myNeighbours.bottom;
            tl = myNeighbours.topRight;
            tr = myNeighbours.topLeft;
            bl = myNeighbours.bottomRight;
            br = myNeighbours.bottomLeft;
        }
        else
        {
            l = myNeighbours.left;
            r = myNeighbours.right;
            t = myNeighbours.top;
            b = myNeighbours.bottom;
            tl = myNeighbours.topLeft;
            tr = myNeighbours.topRight;
            bl = myNeighbours.bottomLeft;
            br = myNeighbours.bottomRight;
        }

        bool allSides = l && r && t && b;
        bool allCorners = tl && tr && bl && br;

        // if (this.debug) Debug.Log($"t:{t} b:{b} l:{l} r:{r} tl:{tl} tr:{tr} bl:{bl} br:{br}");

        if(allSides == true && allCorners == true) SetSprite(null);
        else if( t == false && b == false && l == false && r == false) SetSprite(Select(tiles.singleSquare));
        else if( t == false && b == false && l == false && r == true && tl == false && bl == false) SetSprite(tiles.singleRowLeft);
        else if( t == false && b == false && l == true && r == true && (tl == true || bl == true)) SetSprite(tiles.singleRowLeft);
        else if( t == false && b == false && l == true && r == false ) SetSprite(tiles.singleRowRight);
        else if( t == false && b == false && l == true && r == true && (tr == true || br == true)) SetSprite(tiles.singleRowRight);
        else if( t == false && b == false && l == true && r == true && allCorners == false) SetSprite(Select(tiles.singleRowMiddle));

        else if( l == false && r == false && t == false && b == true && tl == false && tr == false) SetSprite(tiles.pillar1WideTop);
        else if( l == false && r == false && t == true && b == true && (tl == true || tr == true)) SetSprite(tiles.pillar1WideTop);
        else if( l == false && r == false && t == true && b == false ) SetSprite(tiles.pillar1WideBottom);
        else if( l == false && r == false && t == true && b == true && (bl == true || br == true)) SetSprite(tiles.pillar1WideBottom);
        else if( l == false && r == false && t == true && b == true && allCorners == false) SetSprite(Select(tiles.pillar1WideMiddle));

        else if( l == false && r == true && t == true && b == true && tr == false) SetSprite(tiles.topLeft);
        else if( l == true && r == true && t == false && b == true && bl == false) SetSprite(tiles.topLeft);
        else if( l == true && r == false && t == true && b == true && tl == false) SetSprite(tiles.topRight);
        else if( l == true && r == true && t == false && b == true && br == false) SetSprite(tiles.topRight);
        else if( l == true && r == false && t == true && b == true && tl == false) SetSprite(tiles.bottomRight);
        else if( l == true && r == true && t == true && b == false && tr == false) SetSprite(tiles.bottomRight);

        else if( l == true && r == false && t == false && b == true ) SetSprite(tiles.topRight);
        else if( l == false && r == true && t == false && b == true ) SetSprite(tiles.topLeft);
        else if( l == false && r == true && t == true && b == false ) SetSprite(tiles.bottomLeft);
        else if( l == true && r == false && t == true && b == false ) SetSprite(tiles.bottomRight);
        
        else if( allSides == true && tl == false && tr == false && bl == true && br == false) SetSprite(tiles.topRight);            
        else if( allSides == true && tl == false && tr == false && bl == false && br == true) SetSprite(tiles.topLeft);
        else if( bl == false && br == false && tl == true && tr == false) SetSprite(tiles.bottomRight);
        else if( bl == false && br == false && tl == false && tr == true) SetSprite(tiles.bottomLeft);

        else if( allSides == true && tl == false && tr == false && bl == true && br == true) SetSprite(Select(tiles.top));
        else if( allSides == true && tr == false && br == false && tl == true && bl == true) SetSprite(Select(tiles.right));
        else if( allSides == true && tl == false && bl == false && tr == true && br == true) SetSprite(Select(tiles.left));
        else if( allSides == true && bl == false && br == false && tl == true && tr == true) SetSprite(Select(tiles.bottom));            

        else if( l == false && r == true && t == true && b == true ) SetSprite(Select(tiles.left));
        else if( l == true && r == false && t == true && b == true ) SetSprite(Select(tiles.right));
        else if( l == true && r == true && t == false && b == true ) SetSprite(Select(tiles.top));
        else if( l == true && r == true && t == true && b == false ) SetSprite(Select(tiles.bottom));

        else if( l == false && r == true && t == false && b == true && tl == false ) SetSprite(tiles.topLeft);
        else if( l == true && r == false && t == false && b == true && tl == false ) SetSprite(tiles.topRight);
        else if( l == false && r == true && t == true && b == false && bl == false ) SetSprite(tiles.bottomLeft);
        else if( l == true && r == false && t == true && b == false && bl == false ) SetSprite(tiles.bottomRight);

        else if( l == true && r == true && t == true && b == true && tl == false ) SetSprite(tiles.insideTopLeft);
        else if( l == true && r == true && t == true && b == true && tr == false ) SetSprite(tiles.insideTopRight);
        else if( l == true && r == true && t == true && b == true && bl == false ) SetSprite(tiles.insideBottomLeft);
        else if( l == true && r == true && t == true && b == true && br == false ) SetSprite(tiles.insideBottomRight);

        this.backSpriteRenderer.sprite = SelectInterior(tiles, this.def.tx, this.def.ty);
        if (this.spriteSet == false)
        {
            this.lastSprite = spriteRenderer.sprite;
        }
        this.spriteSet = true;
    }

    public override void Reset()
    {

    }
}
