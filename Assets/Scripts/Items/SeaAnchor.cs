using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class SeaAnchor : Item
{
    public Transform grabTransform;
    public Transform seaAnchorTransform;
    public Transform linkParentTransform;
    public Transform sourceTransform;
    public Sprite linkSprite;

    [Header("Animations")]
    public Animated anchorAnimated;
    public Animated.Animation animationAnchorIdle;
    public Animated.Animation animationAnchorShine;
    public Animated grabAnimated;
    public Animated.Animation animationGrabIdle;
    public Animated.Animation animationGrabShine;

    public enum State
    {
        DetectPlayerBeneath,
        DropDown,
        ReachFloor,
        ReturnUpwards
    }

    private State state;
    private Vector2 velocity;
    private Vector2 originAnchorPosition;
    private Vector2 shakeOffset;
    private bool grounded;
    private bool insideWater;
    private int startDetectingPlayerTimer;
    private int fallingDelayTimer;
    private int stayOnFloorTimer;
    private int ignoreGrabTime = 0;
    private GrabType grabType;
    private List<Transform> links = new List<Transform>();
    private int shineTimer;
    private SpriteRenderer grabSpriteRenderer;
    private Transform warningTransform;

    private const int StartDetectingPlayerTime = 60;
    private const int FallingDelayTime = 60;
    private const int StayOnFloorTime = 60;
    private const int ShineTime = 60 * 5;
    private const float DetectMaxDistance = 30f;
    private const float DownwardsAccelerationAir = 0.01f;
    private const float DownwardsAccelerationWater = DownwardsAccelerationAir * WaterResistance;
    private const float UpwardsAccelerationAir = 0.002f;
    private const float UpwardsAccelerationWater = UpwardsAccelerationAir * WaterResistance;
    private const float MaxVerticalSpeed = 0.50f;
    private const float GrabSize = 0.40f;
    private const float WaterResistance = 0.50f;
    private const float LinkDistance = 0.7f;
    private static Vector2 AnchorDeadlyOffset = new Vector2(0f, -0.65f);
    private static Vector2 AnchorSolidOffset = new Vector2(0f, 0.3f);
    private static Vector2 AnchorSolidHitboxSize = new Vector2(0.90f, 0.3f);
    private static Vector2 AnchorDeadlyHitboxSize = new Vector2(0.65f, 0.40f);

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.originAnchorPosition = this.seaAnchorTransform.position;
        this.grabType =  GrabType.Floor;
        this.grabSpriteRenderer = this.seaAnchorTransform.Find("Grab Sprite").GetComponent<SpriteRenderer>();
        this.warningTransform = this.transform.Find("Warning").transform;
        Vector2 anchorDeadlyHitboxPosition =
            (Vector2)this.seaAnchorTransform.localPosition + AnchorDeadlyOffset;
        Hitbox anchorDeadly = new Hitbox(
            anchorDeadlyHitboxPosition.x - AnchorDeadlyHitboxSize.x,
            anchorDeadlyHitboxPosition.x + AnchorDeadlyHitboxSize.x,
            anchorDeadlyHitboxPosition.y - AnchorDeadlyHitboxSize.y,
            anchorDeadlyHitboxPosition.y + AnchorDeadlyHitboxSize.y,
            this.rotation,
            new Hitbox.SolidData(false, false, true, true));
        
        Vector2 anchorSolidHitboxPosition =
            (Vector2)this.seaAnchorTransform.localPosition + AnchorSolidOffset;

        Hitbox anchorSolid = new Hitbox(
            anchorSolidHitboxPosition.x - AnchorSolidHitboxSize.x,
            anchorSolidHitboxPosition.x + AnchorSolidHitboxSize.x,
            anchorSolidHitboxPosition.y - AnchorSolidHitboxSize.y,
            anchorSolidHitboxPosition.y + AnchorSolidHitboxSize.y,
            this.rotation,
            Hitbox.Solid);

        this.hitboxes = new [] { anchorDeadly, anchorSolid };

        PlayIdle();

        ChangeState(State.DetectPlayerBeneath);
        this.shakeOffset = Vector2.zero;
    }

    private void PlayIdle()
    {
        this.anchorAnimated.PlayAndHoldLastFrame(this.animationAnchorIdle);
        this.grabAnimated.PlayAndHoldLastFrame(this.animationGrabIdle);
        this.shineTimer = ShineTime;
    }

    private void PlayShine()
    {
        this.anchorAnimated.PlayOnce(this.animationAnchorShine, PlayIdle);
        this.grabAnimated.PlayOnce(this.animationGrabShine);
    }
    
    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        
        if(this.shineTimer > 0)
        {
            this.shineTimer -= 1;

            if(this.shineTimer == 0)
            {
                PlayShine();
            }
        }

        AdvanceWater();

        switch (state)
        {
            case State.DetectPlayerBeneath:
                AdvanceDetectPlayerBeneath();
                break;

            case State.DropDown:
                AdvanceDropDown();
                break;

            case State.ReachFloor:
                AdvanceReachFloor();
                break;

            case State.ReturnUpwards:
                AdvanceReturnUpwards();
                break;
        }

        if(state != State.DetectPlayerBeneath && this.warningTransform.gameObject.activeInHierarchy)
        {
            this.warningTransform.gameObject.SetActive(false);
        }

        Integrate();
        
        // check collision with player
        var player = this.map.player;
        var playerRectangle = player.Rectangle();
        
        if (player.ShouldCollideWithItems() == true &&
            this.hitboxes[0].InWorldSpace().Overlaps(playerRectangle) && this.state == State.DropDown)
        {
            player.Hit(this.position);
        }
        
        AdvanceHanging();
        this.grabInfo = new GrabInfo(this, this.grabType, this.grabTransform.localPosition);

        // create links to origin position
        {
            float distanceToOrigin =
                Vector2.Distance(this.linkParentTransform.position, this.sourceTransform.position);

            int numberOfLinks = Mathf.FloorToInt(distanceToOrigin / LinkDistance) + 1;

            if(numberOfLinks > this.links.Count)
            {
                for (int i = 0; i < numberOfLinks; i++)
                {
                    if(i >= this.links.Count)
                    {
                        GameObject linkGO = new GameObject($"Link {i}");
                        linkGO.transform.SetParent(this.linkParentTransform);
                        linkGO.transform.localPosition = Vector2.up * LinkDistance * i;

                        SpriteRenderer sr = linkGO.AddComponent<SpriteRenderer>();
                        sr.sprite = this.linkSprite;
                        sr.sortingLayerName = "Items";
                        sr.sortingOrder = 1;

                        this.links.Add(linkGO.transform);
                    }
                }
            }

            for (int i = 0; i < this.links.Count; i++)
            {
                this.links[i].gameObject.layer = this.map.Layer();
                this.links[i].gameObject.SetActive(i < numberOfLinks);
            }
        }

        this.velocity.y = Mathf.Clamp(this.velocity.y, -MaxVerticalSpeed, MaxVerticalSpeed);

        this.position += this.velocity;
        this.seaAnchorTransform.position = this.position + this.shakeOffset;
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity *= 0.70f;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
        }
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreGrabTime = 30;
            }
            return;
        }
        if (this.ignoreGrabTime > 0)
        {
            this.ignoreGrabTime -= 1;
            return;
        }

        var delta = player.position - (Vector2)this.grabTransform.position;
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreGrabTime = 15;
        }
    }

    private void AdvanceDetectPlayerBeneath()
    {
        this.startDetectingPlayerTimer += 1;

        if(this.startDetectingPlayerTimer >= StartDetectingPlayerTime && IsPlayerBeneath() && this.fallingDelayTimer == 0)
        {
            this.fallingDelayTimer++;
        }

        if(this.fallingDelayTimer > 0)
        {
            var cameraRectangle = this.map.gameCamera.VisibleRectangle();
            if (this.position.y > cameraRectangle.yMax + 1)
            {
                this.warningTransform.gameObject.SetActive((this.fallingDelayTimer % 20) > 10);
                this.warningTransform.position = new Vector2(this.position.x, cameraRectangle.yMax - 1);
            }
            else
            {
                this.warningTransform.gameObject.SetActive(false);
            }

            this.fallingDelayTimer++;
            float progress = 0.5f + 0.5f* Mathf.Sin(8f*this.fallingDelayTimer / FallingDelayTime * Mathf.PI + 1.5f * Mathf.PI);
            this.seaAnchorTransform.GetComponent<SpriteRenderer>().color = new Color(1, 1 - progress, 1 - progress,1);
            this.grabSpriteRenderer.color = new Color(1, 1 - progress, 1 - progress,1);

            if (Time.frameCount % 3 == 1)
            {
                this.shakeOffset = new Vector2(Rnd.Range(-0.04f, 0.04f), Rnd.Range(-0.03f, 0.03f));
            }

            if (this.fallingDelayTimer >= FallingDelayTime)
            {
                this.startDetectingPlayerTimer = this.fallingDelayTimer = 0;
                this.shakeOffset = Vector2.zero;
                ChangeState(State.DropDown);
                this.seaAnchorTransform.GetComponent<SpriteRenderer>().color = Color.white;
                this.grabSpriteRenderer.color = Color.white;
            }
        }
        else
        {
            if (this.warningTransform.gameObject.activeInHierarchy == true)
            {
                this.warningTransform.gameObject.SetActive(false);
            }
        }
    }

    private bool IsPlayerBeneath()
    {
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        var rFloor = raycast.Vertical(this.position, false, DetectMaxDistance);

        if(rFloor.anything == true)
        {
            float maxDistance = rFloor.distance;

            var furthestPoint = this.position + (Vector2.down * maxDistance);
            var ray = Vector2.down * maxDistance;
            
            var along = Vector2.Dot(
                this.map.player.position - this.position, ray
            ) / ray.sqrMagnitude;
            along = Mathf.Clamp01(along);
            
            var nearestPointAlongRayToPlayer =
                Vector2.Lerp(this.position, furthestPoint, along);

            return
                (nearestPointAlongRayToPlayer - this.map.player.position).magnitude < 1;
        }
        
        return false;
    }

    private void AdvanceDropDown()
    {
        //float targetVerticalVelocity = this.insideWater == true ? -0.20f : -0.30f;
        //this.velocity.y = Util.Slide(this.velocity.y, targetVerticalVelocity, 0.01f);
        float verticalAcceleration = this.insideWater == true ?
            -DownwardsAccelerationWater : -DownwardsAccelerationAir;
        this.velocity.y += verticalAcceleration;

        // destroy any breakable item in the way
        {
            Vector2 rectOrigin = new Vector2(
                this.hitboxes[0].worldXMin,
                this.hitboxes[0].worldYMin + this.velocity.y
            );
            Vector2 rectSize = new Vector2(
                this.hitboxes[0].InWorldSpace().width,
                this.velocity.y
            );

            Rect damageRect = new Rect(rectOrigin, rectSize);
            damageRect = damageRect.Inflate(Vector2.right * 0.50f);

            this.map.Damage(
                damageRect,
                new Enemy.KillInfo { itemDeliveringKill = this }
            );
        }

        if(this.grounded == true)
        {
            ChangeState(State.ReachFloor);
        }
    }

    private void AdvanceReachFloor()
    {
        this.stayOnFloorTimer += 1;

        if(this.stayOnFloorTimer >= StayOnFloorTime)
        {
            ChangeState(State.ReturnUpwards);
        }
    }

    private void AdvanceReturnUpwards()
    {
        //float targetVerticalVelocity = this.insideWater == true ? 0.20f : 0.30f;
        //this.velocity.y = Util.Slide(this.velocity.y, targetVerticalVelocity, 0.01f);
        float verticalAcceleration = this.insideWater == true ?
            UpwardsAccelerationWater : UpwardsAccelerationAir;
        this.velocity.y += verticalAcceleration;

        float distanceToOriginPos = Vector2.Distance(
            this.seaAnchorTransform.position, this.originAnchorPosition
        );

        if(this.velocity.y > distanceToOriginPos)
        {
            ChangeState(State.DetectPlayerBeneath);
        }
    }
    
    private void ChangeState(State newState)
    {
        this.state = newState;

        switch (state)
        {
            case State.DetectPlayerBeneath:
                EnterDetectPlayerBeneath();
                break;

            case State.DropDown:
                EnterDropDown();
                break;

            case State.ReachFloor:
                EnterReachFloor();
                break;

            case State.ReturnUpwards:
                EnterReturnUpwards();
                break;
        }
    }

    private void EnterDetectPlayerBeneath()
    {
        this.velocity = Vector2.zero;
        this.position = this.originAnchorPosition;
        this.startDetectingPlayerTimer = this.fallingDelayTimer = 0;
    }

    private void EnterDropDown()
    {
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvAnchorStart,
            position: this.position
        );
        // nothing yet
    }

    private void EnterReachFloor()
    {
        this.velocity = Vector2.zero;
        this.stayOnFloorTimer = 0;
        
        this.map.ScreenShakeAtPosition(this.position, new Vector2(0f, -0.5f));

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvAnchorLand,
            position: this.position
        );
    }

    private void EnterReturnUpwards()
    {
        // nothing yet
    }
    
    private void Integrate()
    {
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this);
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -AnchorDeadlyHitboxSize.y), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, AnchorDeadlyHitboxSize.y), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -AnchorDeadlyHitboxSize.y), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, AnchorDeadlyHitboxSize.y), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {

                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            this.grounded = false;

            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-AnchorDeadlyHitboxSize.x, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(AnchorDeadlyHitboxSize.x, 0), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            if((this.map.waterline?.currentLevel > this.position.y + 1.5f)&& Time.frameCount % 5 == 1)
            {
                GenerateBubbles();
            }

            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-AnchorDeadlyHitboxSize.x, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(AnchorDeadlyHitboxSize.x, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.grounded = true;

                Vector2 hitParticlesPosition = this.position + new Vector2(0, -1);
                if (this.map.waterline?.currentLevel < hitParticlesPosition.y)
                {
                    CreateHitParticles(hitParticlesPosition);
                }
            }
            else
            {
                this.position.y += this.velocity.y;
                this.grounded = false;
            }
        }
    }

    private void CreateHitParticles(Vector2 effectPosition)
    {
        var up = new Vector2(0, 1f);
        var forward = new Vector2(1f, 0);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateAndPlayOnce(
                this.map.player.animationDustLandParticle,
                effectPosition +
                    new Vector2(Rnd.Range(-1.25f, 1.25f), 0),
                this.transform.parent
            );
            p.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            float speed = Rnd.Range(0.3f, 0.7f) / 16f;
            p.velocity = new Vector2(Rnd.Range(-0.01f, 0.01f) + this.velocity.x, this.velocity.y + Rnd.Range(0, 100) > 50 ? speed : -speed);
            p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + Rnd.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +

                    new Vector2(Rnd.Range(-0.8f,0.8f), Rnd.Range(-1.5f, 1f)),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = Rnd.Range(0.04f, 0.08f);
                p.velocity = new Vector2(Rnd.Range(-0.01f,0.01f), Rnd.Range(-0.01f, 0.01f));
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
            }
        }
    
}
