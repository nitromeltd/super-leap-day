using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaMine : Item
{
    public Transform seaMineTransform;
    public Transform linkParentTransform;
    public Sprite linkSprite;

    public Animated animatedMine;
    public Animated.Animation mineIdleAnimation;
    public Animated.Animation mineExplosionAnimation;

    private SeaMineSource source;
    [HideInInspector] public Vector2 velocity;
    private bool insideWater;
    private bool stayOnSurface;
    private bool exploded;
    private float chainLength;
    private bool chained;
    private List<Transform> links = new List<Transform>();
    [HideInInspector] public WaterCurrent insideWaterCurrent;
    [HideInInspector] public WaterCurrent.TrackingData waterCurrentTrackingData;

    private const float GravityAcceleration = 0.006f;
    private const float WaterBuoyancy = 0.0025f;
    private const float LinkDistance = 0.7f;
    private static Vector2 HitboxSize = new Vector2(0.9f, 0.9f);

    private bool IsInsideWaterCurrent => this.insideWaterCurrent != null;
    private bool IsCloseEnoughToSurface =>
        Mathf.Abs(this.position.y - this.map.waterline.currentLevel) < 0.50f;
    private float AbsoluteVerticalVelocity => Mathf.Abs(this.velocity.y);
    private bool IsVerticalVelocityEnoughToSettle =>
        AbsoluteVerticalVelocity >= 0f && AbsoluteVerticalVelocity < 0.025f;

    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(
                -HitboxSize.x, HitboxSize.x, -HitboxSize.y, HitboxSize.y,
                this.rotation, Hitbox.NonSolid
            )
        };
        
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 1f);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                this.source = this.chunk.NearestItemTo<SeaMineSource>(otherNode.Position, 1f);

                if(this.source != null)
                {
                    this.source.ChainMine(this);
                    this.chainLength = path.length;
                    this.chained = true;
                }
            }
        }

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;

        this.animatedMine.PlayOnce(this.mineIdleAnimation);

        RefreshHitboxes();
    }
    
    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        if(this.exploded == true) return;

        if(IsInsideWaterCurrent == true)
        {
            AdvanceInsideWaterCurrent();
            return;
        }

        AdvanceWater();

        Waterline waterline = this.map.waterline;

        // horizontal drag
        this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.005f);

        if(this.insideWater == true)
        {
            if(this.stayOnSurface == true)
            {
                if(waterline.IsMoving == true)
                {
                    // sync with waterline velocity
                    this.velocity.y = waterline.Velocity().y;
                }
                else
                {
                    this.velocity.y = 0f;
                    this.position.y = waterline.currentLevel;
                }
            }
            else
            {
                this.velocity += Vector2.up * WaterBuoyancy;
            }
        }
        else
        {
            this.velocity += Vector2.down * GravityAcceleration;
        }

        if(this.source != null && this.chained == true)
        {
            float distanceToSource = Vector2.Distance(this.position, this.source.position);
            
            // create links to source
            int numberOfLinks = Mathf.FloorToInt(distanceToSource / LinkDistance) + 1;
            Vector2 directionToSource = (this.source.position - this.position).normalized;
            if(distanceToSource > LinkDistance && numberOfLinks > this.links.Count)
            {
                for (int i = 0; i < numberOfLinks; i++)
                {
                    if(i >= this.links.Count)
                    {
                        GameObject linkGO = new GameObject($"Link {i}");
                        linkGO.transform.SetParent(this.linkParentTransform);
                        SpriteRenderer sr = linkGO.AddComponent<SpriteRenderer>();
                        sr.sprite = this.linkSprite;
                        sr.sortingLayerName = "Items";
                        sr.sortingOrder = 1;

                        this.links.Add(linkGO.transform);
                    }
                }
            }

            for (int i = 0; i < this.links.Count; i++)
            {
                this.links[i].transform.localPosition = directionToSource * LinkDistance * i;
                this.links[i].gameObject.SetActive(i < numberOfLinks);
                this.links[i].gameObject.layer = this.map.Layer();
            }
        }
        
        Integrate();
        CheckForContact();
        CheckForPlayer();

        // if the mine moves far enough from the waterline, it wont be on the surface anymore
        if(this.stayOnSurface == true)
        {
            if(IsCloseEnoughToSurface == false)
            {
                this.stayOnSurface = false;
            }
        }
        else
        {
            if(IsCloseEnoughToSurface == true && IsVerticalVelocityEnoughToSettle == true)
            {
                this.stayOnSurface = true;
            }
        }
        
        
        this.seaMineTransform.position = this.position;
    }

    private void AdvanceInsideWaterCurrent()
    {
        this.insideWaterCurrent.AdvanceSeaMine(this);
        Integrate();
        CheckForContact();
        CheckForPlayer();

        this.seaMineTransform.position = this.position;
    }

    private void CheckForContact()
    {
        Rect contactRect = this.hitboxes[0].InWorldSpace().Inflate(0.10f);
        List<Item> currentItems = new List<Item>(this.chunk.items);

        foreach(var item in currentItems)
        {
            if(item is Enemy ||
                item is TNTBlock ||
                item is BreakableBlock ||
                item is RandomBox)
            {
                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(contactRect))
                    {
                        Explode();
                    }
                }
            }
        }
    }
    
    private void CheckForPlayer()
    {
        var player = this.map.player;
        var playerRectangle = player.Rectangle();
        
        if (player.ShouldCollideWithItems() == true &&
            this.hitboxes[0].InWorldSpace().Overlaps(playerRectangle))
        {
            Explode();
        }
    }

    private void AdvanceWater()
    {
        Waterline waterline = this.map.waterline;

        if(this.insideWater == false &&
            waterline.currentLevel - waterline.VelocityThreshold > this.position.y)
        {
            // enter water
            this.insideWater = true;
            this.velocity *= 0.30f;
        }

        if(this.insideWater == true &&
            waterline.currentLevel + waterline.VelocityThreshold < this.position.y)
        {
            // exit water
            this.insideWater = false;
            this.velocity *= 0.30f;
        }
    }

    private void Integrate()
    {
        var raycast = new Raycast(this.map, this.position, ignoreItem1: this, isCastByEnemy: true);
        var rect = this.hitboxes[0];

        if (this.velocity.x > 0)
        {
            var start = this.position;
            var maxDistance = this.velocity.x + rect.xMax;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), true, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.x + rect.xMax)
            {
                this.position.x += result.distance - rect.xMax;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else if (this.velocity.x < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.x - rect.xMin;
            var result = Raycast.CombineClosest(
                raycast.Horizontal(start + new Vector2(0, -HitboxSize.y), false, maxDistance),
                raycast.Horizontal(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Horizontal(start + new Vector2(0, HitboxSize.y), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.x - rect.xMin)
            {

                this.position.x += -result.distance - rect.xMin;
                this.velocity.x = 0;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y > 0)
        {
            float distanceToSource = this.source ?
                Vector2.Distance(this.position + this.velocity, this.source.position) : -1f;

            var start = this.position;
            var maxDistance = this.velocity.y + rect.yMax;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), true, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), true, maxDistance)
            );
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else if(result.End().y > this.chunk.yMax ||
                (this.chained == true && distanceToSource > this.chainLength))
            {
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-HitboxSize.x, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(HitboxSize.x, 0), false, maxDistance)
            );
            float distanceToSource = this.source ?
                Vector2.Distance(this.position + this.velocity, this.source.position) : -1f;

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
            }
            else if(this.chained == true && distanceToSource > this.chainLength)
            {
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void Explode()
    {
        if(this.exploded == true) return;
        this.exploded = true;

        this.animatedMine.PlayOnce(this.mineExplosionAnimation);

        Rect explosionRect = this.hitboxes[0].InWorldSpace().Inflate(1f);
        
        this.map.Damage(
            explosionRect,
            new Enemy.KillInfo { itemDeliveringKill = this },
            affectPlayer: true
        );
        
        HideChain();

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSeaMine,
            position: this.position
        );
    }

    public void Unchain()
    {
        if(this.chained == false) return;

        this.chained = false;
        HideChain();
    }

    private void HideChain()
    {
        for (int i = 0; i < this.links.Count; i++)
        {
            this.links[i].gameObject.SetActive(false);
        }
    }
    
    public void EnterWaterCurrent(WaterCurrent waterCurrent)
    {
        if (this.exploded == true) return;

        this.insideWaterCurrent = waterCurrent;
        this.stayOnSurface = false;
        Unchain();
    }

    public void NotifyExitWaterCurrent()
    {
        this.insideWaterCurrent = null;
    }
}
