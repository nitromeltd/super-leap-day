using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeaMineSource : Item
{
    private SeaMine mine;
    private Button[] buttons;

    public override void Init(Def def)
    {
        base.Init(def);

        var buttons = new List<Button>();
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                var button = this.chunk.NearestItemTo<Button>(otherNode.Position);
                buttons.Add(button);
            }
        }
        this.buttons = buttons.ToArray();
    }

    public override void Advance()
    {
        var pressed = false;
        foreach (var button in this.buttons)
        {
            if (button?.PressedThisFrame == true)
            {
                pressed = true;
            }
        }

        if (pressed == true)
        {
            ReleaseMine();
        }
    }

    public void ChainMine(SeaMine mine)
    {
        this.mine = mine;
    }

    public void ReleaseMine()
    {
        if(this.mine != null)
        {
            this.mine.Unchain();
            this.mine = null;
        }
    }
    
}
