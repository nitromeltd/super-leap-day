
using UnityEngine;

public class SequenceMover : Item
{
    private NitromeEditor.Path path;
    private bool playerInContactWithTiles = false;
    private int nodeIndex;
    private float distanceAlongPath;
    private bool moving = false;

    public override void Init(Def def)
    {
        base.Init(def);

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));
        this.path = this.chunk.pathLayer.NearestPathTo(this.position);

        this.hitboxes = new Hitbox[] {
            new Hitbox(-1, 1, -1, 1, this.rotation, Hitbox.Solid, true, true)
        };
    }

    public override void Advance()
    {
        var tileGrid = this.group?.followerTileGrid;
        var contact = false;

        foreach (var r in this.map.player.ProbesThisFrame)
        {
            if ((tileGrid != null && r.tile?.tileGrid == tileGrid) || r.item == this)
                contact = true;
        }

        if (contact == false &&
            this.playerInContactWithTiles == true &&
            this.moving == false)
        {
            this.nodeIndex += 1;
            this.nodeIndex %= this.path.nodes.Length;
        }

        this.playerInContactWithTiles = contact;

        var targetDistance = this.path.PointAtNodeIndex(this.nodeIndex).distanceAlongPath;
        if (this.path.closed == true && this.distanceAlongPath > targetDistance)
        {
            this.distanceAlongPath += 0.25f;
            if (this.distanceAlongPath > this.path.length)
                this.distanceAlongPath = 0;
        }
        else
        {
            this.distanceAlongPath =
                Util.Slide(this.distanceAlongPath, targetDistance, 0.25f);
        }
        this.moving = (this.distanceAlongPath != targetDistance);
        this.position =
            this.path.PointAtDistanceAlongPath(this.distanceAlongPath).position;
        this.transform.position = this.position;
    }
}
