using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using Rnd = UnityEngine.Random;

public class ShootingSun : Enemy
{
    private NitromeEditor.Path path;
    private Vector2 velocity;
    private Vector2 surfaceVelocity;
    private bool notMoving;
    public Animated.Animation animationIdle;
    public Animated.Animation animationAttack;
    public Animated.Animation animationMove;
    public Animated.Animation animationHandIdle;
    public Animated.Animation animationHandReady;
    public Animated.Animation animationHandAttack;
    public Animated.Animation animationHandMove;
    private int attackTime;
    protected float offsetThroughPath;
    const int MaxAttackTime = 240;
    const float SunSpeed = 0.5f / 16;
    public Animated handLeft;
    public Animated handRight;
    private float tweenCounter = 0;
    private bool facingRight = false;
    public Transform glow;
    private static int sortingOrderNumber = 0;
    private bool hideHands = false;
    private int frame = 0;
    private Vector2 lastPosition;

    public override void Init(Def def)
    {
        base.Init(def);

        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 0.50f);
        this.velocity = SunSpeed * new Vector2(1, 1).normalized;
        this.lastPosition = this.position;
        if(this.path != null)
        {
            this.offsetThroughPath = TimeNearestToPoint(this.path, this.position);
            if (this.path.closed == false)
                this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
        else
        {
            var velNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 0.50f);

            if(velNode != null)
            {
                NitromeEditor.Path.Edge firstEdge = velNode.path.edges[0];
                this.velocity = SunSpeed * firstEdge.direction.normalized;
            }
            else
            {
                this.velocity = Vector2.zero;
                this.notMoving = true;
            }
        }

        this.hitboxes = new [] {
                new Hitbox(-1.35f, 1.35f, -1.35f, 1.35f, this.rotation, Hitbox.NonSolid)
            }; 
        
        this.animated.PlayAndLoop(this.animationIdle);
        this.attackTime = MaxAttackTime;
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        Audio.instance.PlaySfxLoop(
            Assets.instance.sfxWindyShootingSunBurns,
            position: this.position
        );

        this.spriteRenderer.sortingOrder = ShootingSun.sortingOrderNumber * 3;
        this.handLeft.gameObject.GetComponent<SpriteRenderer>().sortingOrder = ShootingSun.sortingOrderNumber * 3 + 1;
        this.handRight.gameObject.GetComponent<SpriteRenderer>().sortingOrder = ShootingSun.sortingOrderNumber * 3 + 1;
        this.glow.GetComponent<SpriteRenderer>().sortingOrder = ShootingSun.sortingOrderNumber * 3 - 1;
        ShootingSun.sortingOrderNumber = (ShootingSun.sortingOrderNumber + 1) % 3;
    }

    private static float TimeNearestToPoint(NitromeEditor.Path path, Vector2 pt)
    {
        var edgeIndex = path.NearestEdgeIndexTo(pt).Value;
        var edge = path.edges[edgeIndex];

        var edgeDelta = edge.to.Position - edge.from.Position;
        var through =
            Vector2.Dot(edgeDelta, pt - edge.from.Position) / edgeDelta.sqrMagnitude;
        through = Mathf.Clamp01(through);

        float result = 0.0f;
        for (var n = 0; n < edgeIndex; n += 1)
        {
            result += path.edges[n].distance / path.edges[n].speedPixelsPerSecond;
        }
        result += through * (edge.distance / edge.speedPixelsPerSecond);
        return result;
    }

    public override void Advance()
    {
        this.transform.localScale = new Vector3(this.facingRight == true ? -1 : 1, 1, 1);
        SetCorrectGlowPosition();
        this.hideHands = false;
        if(this.path != null)
        {
            this.position = this.path.PointAtTime(
                (this.frame / 60.0f) + this.offsetThroughPath
            ).position;

            
            if (this.animated.currentAnimation != this.animationAttack)
            {
                var diff = (this.position - this.lastPosition).normalized;
                if (Mathf.Abs(Vector2.Dot(diff, Vector2.right)) > 0.5f)
                {
                    this.animated.PlayAndLoop(this.animationMove);
                    this.hideHands = true;
                }
                else
                {
                    this.animated.PlayAndLoop(this.animationIdle);
                    this.hideHands = false;
                }
            }
            
            this.facingRight = (this.position.x - this.lastPosition.x) > 0;
        }
        else
        {
            AdvanceMovement();
            if (this.notMoving == false)
            {
                this.position += this.velocity;
            }
            
        }
        this.transform.position = this.position;
        AdvanceShooting();
        SetCorrectHands();
        DetectIfPlayerShouldDie();
        if (this.animated.currentAnimation != this.animationAttack)
        {
            this.frame++;
        }
        this.lastPosition = this.position;
    }

    private void SetCorrectHands()
    {
        if(this.hideHands == true)
        {
            this.handLeft.PlayAndLoop(this.animationHandMove);
            this.handRight.PlayAndLoop(this.animationHandMove);
            return;
        }

        var leftPos = this.handLeft.transform.localPosition;
        var rightPos = this.handRight.transform.localPosition;
        if(this.animated.currentAnimation == this.animationAttack)
        {
            if(this.animated.frameNumber > 5)
            {
                this.tweenCounter = Util.Slide(this.tweenCounter, 1f, 0.25f);    
            }

            if (this.animated.frameNumber > 1 && this.tweenCounter < 1f)
            {
                this.handLeft.PlayAndLoop(this.animationHandReady);
                this.handRight.PlayAndLoop(this.animationHandReady);
            }
            
            if(this.animated.frameNumber > 7 && this.tweenCounter == 1)
            {
                this.handLeft.PlayAndLoop(this.animationHandAttack);
                this.handRight.PlayAndLoop(this.animationHandAttack);
            }
            leftPos.y = rightPos.y = -1.65f + 0.4f * this.tweenCounter;
            this.handLeft.transform.localPosition = leftPos;
            this.handRight.transform.localPosition = rightPos;
        }
        else
        {
            this.tweenCounter = Util.Slide(this.tweenCounter, 0f, 0.25f);
            if (this.tweenCounter == 0)
            {
                this.handLeft.PlayAndLoop(this.animationHandIdle);
                this.handRight.PlayAndLoop(this.animationHandIdle);
            }
            leftPos.y = rightPos.y = -1.65f + 0.4f * this.tweenCounter;
            this.handLeft.transform.localPosition = leftPos;
            this.handRight.transform.localPosition = rightPos;
        }
    }

    private void SetCorrectGlowPosition()
    {
        if(this.animated.currentAnimation == this.animationAttack && (this.animated.frameNumber >= 10))
        {
            this.glow.localPosition = new Vector3(-0.24f, -0.14f, 0);
        }
        else
        {
            this.glow.localPosition = Vector3.zero;
        }
    }

    private void AdvanceShooting()
    {
        if(this.attackTime > 0)
        {
            this.attackTime--;
        }
        else
        {
            ShootAtPlayerIfPossible();
        }

        if(this.animated.currentAnimation == this.animationAttack)
        {
            Vector2 v = this.map.player.position - this.position;
            if(v.x > 0) this.facingRight = true;
            if(v.x < 0) this.facingRight = false;
        }
    }

    private void DetectIfPlayerShouldDie()
    {
        if (this.deathAnimationPlaying == true)
            return;

        var player = this.map.player;
        var playerRectangle = player.Rectangle();
        var ownRectangle = this.hitboxes[0].InWorldSpace();

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return;

        // collision
        if (player.invincibility.isActive == true)
        {
            Hit(new KillInfo {
                playerDeliveringKill = this.map.player,
                freezeTime = true
            });
            Audio.instance.PlaySfx(deathAnimation.GetDeathSFX(), position: this.position);
            return;
        }
        else
        {
            this.map.player.Hit(this.position);
        }
    }

    private void ShootAtPlayerIfPossible()
    {
        Vector2 v = this.map.player.position - this.position;
        if(v.sqrMagnitude > 10 * 10)
            return;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var r = raycast.Arbitrary(
                this.position,
                v.normalized,
                v.magnitude
            );

        // Debug.DrawLine(this.position, this.map.player.position, Color.green, 0);

        if (r.anything == false)
        {
            this.attackTime = MaxAttackTime;
            this.tweenCounter = 0;
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyShootingSunInhales,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
            this.animated.PlayOnce(this.animationAttack, () =>
                {
                    this.animated.PlayAndLoop(this.animationIdle);
                    SetCorrectGlowPosition();
                    
                });
            this.animated.OnFrame(10, () =>
                {
                    v = this.map.player.position - this.position;
                    var ray = ShootingSunRay.Create(this.position + 1.0f * v.normalized, this.chunk, Vector2.SignedAngle(Vector2.right, v.normalized), null);
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxWindyShootingSunFires,
                        position: this.position,
                        options: new Audio.Options(1, false, 0)
                    );
                });
        }
    }

    private void AdvanceMovement()
    {
        Vector2 frameVelocity = this.velocity;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0,  6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add( 6 / 16f, 0), false, 0.75f)
        );

        if (this.velocity.x > 0)
        {
            if(right.anything == true)
            {
                this.velocity.x = -SunSpeed;   
                if(right.tiles.Length > 0 && right.tiles[0] != null)
                {
                    this.surfaceVelocity = right.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if(left.anything == true)
            {
                this.velocity.x = SunSpeed;                
                if(left.tiles.Length > 0 && left.tiles[0] != null)
                {
                    surfaceVelocity = left.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }

        if (this.velocity.y > 0)
        {
            if(up.anything == true)
            {
                this.velocity.y = -SunSpeed;                
                if(up.tiles.Length > 0 && up.tiles[0] != null)
                {
                    surfaceVelocity = up.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
        else
        {
            if(down.anything == true)
            {
                this.velocity.y = SunSpeed;
                if(down.tiles.Length > 0 && down.tiles[0] != null)
                {
                    surfaceVelocity = down.tiles[0].tileGrid.effectiveVelocity;
                }
            }
        }
    }
}
