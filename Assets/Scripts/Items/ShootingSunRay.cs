using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using Rnd = UnityEngine.Random;

public class ShootingSunRay : Enemy
{
    private float directionAngle;
    private bool destroyed;
    public Animated.Animation animationIdle;
    public Animated.Animation animationDisappear;

    public static ShootingSunRay Create(Vector2 position, Chunk chunk, float angle, Action actionOnExplode)
    {
        var obj = Instantiate(
            Assets.instance.windySkies.shootingSunRay, chunk.transform
        );
        obj.name = "Spawned Sun Ray";
        var item = obj.GetComponent<ShootingSunRay>();
        item.Init(position, chunk, angle, actionOnExplode);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, float angle = 0, Action actionOnExplode = null)
    {
        base.Init(new Def(chunk));
        this.transform.position = this.position = position;
        this.chunk = chunk;
        this.hitboxes = new [] {
                new Hitbox(-0.1f, 0.1f, -0.1f, 0.1f, this.rotation, Hitbox.NonSolid)
            };
        this.whenHitFromAbove = HitFromAboveBehaviour.HurtPlayer;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.directionAngle = angle;
        this.transform.rotation = Quaternion.Euler(0, 0, this.directionAngle);
        this.animated.PlayAndLoop(this.animationIdle);
    } 

    public override void Advance()
    {
        if(this.destroyed)
            return;

        Vector2 direction = Quaternion.Euler(0, 0, this.directionAngle) * Vector2.right;
        EnemyHitDetectionAgainstPlayer(this.hitboxes[0].InWorldSpace());
        float vel = 1f / 16;
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var r = raycast.Arbitrary(
                this.position,
                direction.normalized,
                vel
            );

        if (r.anything == true)
        {
            vel = r.distance;
            this.destroyed = true;

            Particle.CreateAndPlayOnce(
            Assets.instance.explosionAnimation,
            this.position,
            this.transform.parent
            );
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyShootingSunFires,
                position: this.position,
                options: new Audio.Options(1, false, 0)
            );
            Destroy(this.gameObject);
            this.chunk.items.Remove(this);
        }

        this.position += vel * direction;
        this.transform.position = this.position;
    }
}
