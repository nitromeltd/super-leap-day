using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeGhostGrave : Item
{
    private SnakeGhost activeSnakeGhost;
    public bool snakeGhostSpawned;
    private bool lidOpen;
    private bool activatedByShrine = false;
    public int snakeLength = 50;
    public float snakeSpeed = 0.15f;
    public float snakeTurningSpeed = 3f;
    public Animated effectAnimated;
    public Animated lidAnimated;
    public SpriteRenderer effectSpriteRenderer;

    public Animated.Animation animationOpen;
    public Animated.Animation animationClose;
    public Animated.Animation animationIdle;

    public Animated.Animation animationGlow;

    private float currentEffectAlpha = 1f;
    private float targetEffectAlpha = 1f;

    public override void Init(Def def)
    {
        base.Init(def);
        SetClosedHitbox();
        snakeGhostSpawned = false;

        if (def.tile.properties?.ContainsKey("snake_length") == true)
        {
            this.snakeLength = def.tile.properties["snake_length"].i;
            if(this.snakeLength < 10)
            {
                this.snakeLength = 10;
            }
        }

        if (def.tile.properties?.ContainsKey("snake_speed") == true)
        {
            this.snakeSpeed = def.tile.properties["snake_speed"].f;
            this.snakeSpeed = Mathf.Clamp(this.snakeSpeed, 0.1f, 0.2f);
        }

        if (def.tile.properties?.ContainsKey("snake_turning_speed") == true)
        {
            this.snakeTurningSpeed = def.tile.properties["snake_turning_speed"].f;
            this.snakeTurningSpeed = Mathf.Clamp(this.snakeTurningSpeed, 1f, 5f);
        }

        this.lidAnimated.PlayOnce(this.animationIdle);
        this.effectAnimated.PlayAndLoop(this.animationGlow);
        this.currentEffectAlpha = this.targetEffectAlpha = 1f;
        this.lidOpen = false;
    }

    private void SetClosedHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-1.65f, 1.65f, -1.2f, 1f, this.rotation, Hitbox.Solid)
        };
        this.lidOpen = false;
    }

    private void SetOpenHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-1.65f, 1.65f, -1.2f, 0.0f, this.rotation, Hitbox.Solid)
        };
        this.lidOpen = true;
    }

    public override void Advance()
    {
// #if UNITY_EDITOR        
//         if(Input.GetKeyDown(KeyCode.A))
//         {
//             if(this.activeSnakeGhost == null)
//             {
//                 this.activeSnakeGhost = SnakeGhost.Create(this, this.position, this.chunk);
//             }
//         }
// #endif
        if (this.activatedByShrine == false)
        {
            SpawnGhostIfNeeded();
        }
 
        float changeSpeed = 0.025f;
        if(this.currentEffectAlpha < this.targetEffectAlpha)
        {
            this.currentEffectAlpha += changeSpeed;
            if(this.currentEffectAlpha > this.targetEffectAlpha)
            {
                this.currentEffectAlpha = this.targetEffectAlpha;
            }
        }

        if(this.currentEffectAlpha > this.targetEffectAlpha)
        {
            this.currentEffectAlpha -= changeSpeed;
            if(this.currentEffectAlpha < this.targetEffectAlpha)
            {
                this.currentEffectAlpha = this.targetEffectAlpha;
            }
        }

        if(this.currentEffectAlpha > this.targetEffectAlpha)
        {
            Particle.CreateDust(
            this.chunk.transform, 1, 1, this.transform.position + new Vector3(0, 0.1f, 0), velocity: new Vector2(0, -0.03f), variationX: 2, variationY: 0.05f,
                sortingLayerName: "Player Dust (AL)"
            );
        }

        this.effectSpriteRenderer.color = new Color(1f, 1f, 1f, this.currentEffectAlpha);
    }

    private void SpawnGhostIfNeeded()
    {
        if(this.map.NearestChunkTo(this.position) == this.map.NearestChunkTo(this.map.player.position) && this.activeSnakeGhost == null && this.snakeGhostSpawned == false)
        {
            if (this.lidAnimated.currentAnimation != this.animationOpen)
            {
                this.lidAnimated.PlayOnce(this.animationOpen);
                this.lidAnimated.OnFrame(10, SpawnGhost);
                this.targetEffectAlpha = 0f;
                SetOpenHitbox();

                Audio.instance.PlaySfx(
                    Assets.instance.tombstoneHill.sfxGhostTombRise, null, this.position
                );
            }
        }
    }

    public void CloseLid()
    {
        if (this.lidAnimated.currentAnimation != this.animationClose)
        {
            this.lidAnimated.PlayOnce(this.animationClose);
            this.targetEffectAlpha = 1f;
            SetClosedHitbox();
            this.activeSnakeGhost = null;
            this.snakeGhostSpawned = false;
        }
    }

    private void SpawnGhost()
    {
        this.activeSnakeGhost = SnakeGhost.Create(this, this.position, this.chunk);
        this.snakeGhostSpawned = true;
    }

    public void ClearSnakeGhost()
    {
        this.activeSnakeGhost = null;
    }

    public void Hide()
    {
        this.activatedByShrine = true;
    }

    public void Spawn()
    {
        if(this.activeSnakeGhost == null)
        {
            this.activeSnakeGhost = SnakeGhost.Create(this, this.position, this.chunk);
        }
    }

    public Vector2 GravePosition()
    {
        return this.position;
    }

    public override void Reset()
    {
        if(this.activeSnakeGhost != null)
        {
            this.activeSnakeGhost.Kill(new Enemy.KillInfo{isBecauseLevelIsResetting = false, itemDeliveringKill = this});
        }

        if (this.lidOpen == true)
            CloseLid();

        this.snakeGhostSpawned = false;
    }
}
