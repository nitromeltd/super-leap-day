using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SnakeGhostTrap : Item
{
    private bool trapActivated = false;
    private int howManyGhostsToTrap = 0;
    private int deactivationTime = 0;
    public Animated.Animation idleAnimation;
    public Animated.Animation buttonPressedAnimation;
    public Animated.Animation suckingAnimation;
    public Animated.Animation suckingEffectAnimation;
    public Animated.Animation idleEffectAnimation;
    private bool isTriggeringLoopSfx = false;

    public Animated effectAnimated;

    public override void Init(Def def)
    {
        base.Init(def);
        this.hitboxes = new [] {
            new Hitbox(-1.25f, 1.25f, -2f, 1.25f, this.rotation, Hitbox.Solid)
        };

        this.onCollisionFromAbove = (_) => { CollisionFromAbove(); };
    }

    private void CollisionFromAbove()
    {
        ActivateTrap();
    }

    public override void Advance()
    {
        if(this.deactivationTime > 0)
        {
            this.deactivationTime--;
            if(this.deactivationTime == 0)
            {
                this.animated.PlayAndLoop(this.idleAnimation);
                this.effectAnimated.PlayAndLoop(this.idleEffectAnimation);
                this.trapActivated = false;
            }
        }

        var shouldTriggerLoopSfx =
            (this.animated.currentAnimation == this.suckingAnimation &&
            this.animated.frameNumber == 0);
        if (shouldTriggerLoopSfx == true && this.isTriggeringLoopSfx == false)
        {
            Audio.instance.PlaySfx(
                Assets.instance.tombstoneHill.sfxGhostTrapLoop, null, this.position
            );
        }
        this.isTriggeringLoopSfx = shouldTriggerLoopSfx;

// #if UNITY_EDITOR
//         if(Input.GetKeyDown(KeyCode.P))
//         {
//             ActivateTrap();
//             Debug.Break();
//         }
// #endif
    }

    public void ActivateTrap()
    {
        if(this.trapActivated == false)
        {
            this.trapActivated = true;

            this.animated.PlayOnce(this.buttonPressedAnimation, () =>
            {
                this.animated.PlayAndLoop(this.suckingAnimation);
                this.effectAnimated.PlayAndLoop(this.suckingEffectAnimation);
            });

            Audio.instance.PlaySfx(
                Assets.instance.tombstoneHill.sfxGhostTrapGhostSuck, null, this.position
            );

            var snakeGhosts = new List<SnakeGhost>();
            snakeGhosts.AddRange(this.chunk.items.OfType<SnakeGhost>());

            this.howManyGhostsToTrap = 0;
            foreach(var sg in snakeGhosts)
            {
                if(sg.TrapGhost(this) == true)
                {
                    this.howManyGhostsToTrap++;
                }
            }

            if(this.howManyGhostsToTrap == 0)
            {
                this.deactivationTime = 120;
            }
        }
    }

    public void ClearTrap()
    {
        this.trapActivated = false;
        this.howManyGhostsToTrap--;
        if(this.howManyGhostsToTrap == 0)
        {
            this.animated.PlayAndLoop(this.idleAnimation);
            this.effectAnimated.PlayAndLoop(this.idleEffectAnimation);
        }
    }

    public Vector2 TrapPosition()
    {
        return this.position;
    }
}
