﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowBlock : Item
{
    public enum Origin
    {
        Editor,
        Enemy,
        PowerupTornado
    }

    public Animated.Animation appearAnimation;
    public Animated.Animation[] idleAnimations;
    public Animated.Animation meltAnimation;
    public Animated.Animation meltFastAnimation;

    public Animated animatedBG;
    public Animated.Animation[] bgIdleAnimations;
    public Animated.Animation bgMeltAnimation;
    public Animated.Animation bgMeltFastAnimation;
    public Animated.Animation unmeltAnimation;
    public Animated.Animation bgUnmeltAnimation;

    private Origin origin;
    private int spriteIndex;
    private bool melted;
    private int sparkleTime;
    private bool hitboxSolid;
    private int checkTemperatureTimer;
    public SpinBlock controlledBySpinBlock;
    public static float BlockSize = 1f;
    public static int CheckTemperatureTime = 60;
    
    public override void Init(Def def)
    {
        base.Init(def);

        Init(this.transform.position, def.startChunk);

        this.origin = Origin.Editor;
        this.sparkleTime = 0;
        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        NotifyChangeTemperature(this.map.currentTemperature);
    }

    public static SnowBlock Create(Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.snowBlock, chunk.transform);
        obj.name = "Spawned Snow Block (" + position.x + "," + position.y+")";
        var item = obj.GetComponent<SnowBlock>();
        item.Init(new Def(chunk));
        item.Init(position, chunk);
        item.origin = Origin.Enemy;
        item.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        this.transform.position = this.position = position;
        this.chunk = chunk;
        ToggleHitbox(true);
        
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromLeft  = OnCollision;
        this.onCollisionFromRight = OnCollision;

        // CheckForSpike();

        this.melted = false;

        this.spriteIndex = Random.Range(0, idleAnimations.Length);

        this.sparkleTime = 25;

        this.animated.PlayOnce(appearAnimation, () =>
        {
            this.animated.PlayOnce(idleAnimations[spriteIndex]);
            this.animatedBG.PlayOnce(bgIdleAnimations[spriteIndex]);
        });
    }

    public override void Advance()
    {
        base.Advance();
        
        if (this.group != null && this.controlledBySpinBlock == null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if(this.checkTemperatureTimer > 0)
        {
            this.checkTemperatureTimer -= 1;

            if(this.checkTemperatureTimer == 0)
            {
                NotifyChangeTemperature(this.map.currentTemperature);
            }
        }

        if(this.sparkleTime > 0)
        {
            this.sparkleTime -= 1;

            if(this.sparkleTime % 2 == 0)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-.5f, .5f),
                        Random.Range(-.5f, .5f)
                    ),
                    this.transform.parent
                );
                float sparkleSpeed = 0.05f;
                sparkle.velocity = new Vector2(Random.Range(sparkleSpeed * -1, sparkleSpeed), Random.Range(sparkleSpeed * -1, sparkleSpeed));
                sparkle.spriteRenderer.sortingLayerName = "Items (Front)";
                sparkle.spriteRenderer.sortingOrder = 10;
            }
        }
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(this.map.currentTemperature == Map.Temperature.Cold &&
            this.origin == Origin.Editor)
        {
            Unmelt();
        }

        if(this.map.currentTemperature == Map.Temperature.Hot)
        {
            Melt();
        }
    }

    private void OnCollision(Collision collision)
    {
        Melt();
    }

    public void Melt(bool immediate = false)
    {
        if(this.melted == true) return;
            this.melted = true;

        if(immediate == true)
        {
            this.animated.PlayOnce(meltFastAnimation);
            this.animatedBG.PlayOnce(bgMeltFastAnimation);

            ToggleHitbox(false);
        }
        else
        {
            this.animated.PlayOnce(meltAnimation);
            this.animatedBG.PlayOnce(bgMeltAnimation);

            this.animated.OnFrame(9, () => {
                ToggleHitbox(false);
            });
        }

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSnowMelt,
            options: new Audio.Options(1f, true, 0.2f),
            position: this.position
        );
    }

    public void Unmelt()
    {
        if(this.melted == false) return;
        this.melted = false;

        this.animated.PlayOnce(unmeltAnimation);
        this.animatedBG.PlayOnce(bgUnmeltAnimation);

        this.animated.OnFrame(9, () => {
            ToggleHitbox(true);
        });

        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSnowSpawn,
            position: this.position
        );
    }

    private void ToggleHitbox(bool solid)
    {
        float halfSize = BlockSize / 2f;
        Hitbox.SolidData solidData = solid == true ? Hitbox.Solid : Hitbox.NonSolid;
        this.hitboxSolid = solid;
        
        this.hitboxes = new [] {
            new Hitbox(
                -halfSize, halfSize, -halfSize, halfSize,
                this.rotation, solidData, solid, solid
            )
        };
    }

    public override void Reset()
    {
        NotifyChangeTemperature(this.map.currentTemperature);
        CheckForSpike();
    }

    public bool IsSnowActive()
    {
        if(melted == true && this.hitboxSolid == false)
        {
            return false;
        }

        return true;
    }

    private void CheckForSpike()
    {
        SpikeTile spikeTile = this.chunk.NearestItemTo<SpikeTile>(
            this.position, 0.50f
        );
        spikeTile?.AssignNewSnowBlock(this);
    }

    public void ForceUnmelt()
    {
        this.checkTemperatureTimer = CheckTemperatureTime;
        Unmelt();
    }

    public void OnDestroy()
    {
        this.controlledBySpinBlock?.DetachItem(this as Item);
    }
}
