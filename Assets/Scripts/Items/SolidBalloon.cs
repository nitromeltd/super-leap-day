using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolidBalloon : Item
{
    public Animated animatedFan;
    public Animated.Animation animationIdle;
    public bool isBalloon;
    public override void Init(Def def)
    {
        base.Init(def);

        var chainNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 0.60f);
        if(chainNode != null)
        {            
            var edge = chainNode.path.edges[0];
            float length = Mathf.FloorToInt((edge.to.Position - edge.from.Position).magnitude);

            for (int i = 0; i < length; i++)
            {
                GameObject chainLink = Instantiate(Assets.instance.windySkies?.solidBalloonChain);
                chainLink.transform.parent = this.gameObject.transform;
                chainLink.transform.localPosition = new Vector3(0f, -i, 0);
            }

            if(this.isBalloon == true)
            {
                this.hitboxes = new[] {
                new Hitbox(-0.5f, 0.5f, -length, 0.5f, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(-1f, 1f, 0.5f, 4.0f, this.rotation, Hitbox.Solid, false, false),
                };
            }
            else
            {
                this.hitboxes = new[] {
                new Hitbox(-0.5f, 0.5f, -length, 0.5f, this.rotation, Hitbox.Solid, true, true),
                new Hitbox(-1f, 1f, 0.0f, 1f, this.rotation, Hitbox.Solid, false, false),
                };
            }
        }

        if(animatedFan != null)
            animatedFan.PlayAndLoop(animationIdle);
    }

    public override void Advance()
    {
        var player = this.map.player;
        if (player is PlayerGoop)
        {
            var thisRect = this.hitboxes[1].InWorldSpace();
            if ((player.state == Player.State.GoopBallSpinning || player.state == Player.State.GoopBallTurningInPlace) && thisRect.Overlaps(player.Rectangle()))
            {
                player.state = Player.State.Normal;
            }
        }
    }
}
