using UnityEngine;

public class SpaceBooster : Item
{
    private Transform door1;
    private Transform door2;

    private Vector2 home;
    private Vector2 velocity;

    private int timeSinceFire = 0;
    private float visibleAngleRadians = 0;
    private float doorsOpenAmount = 0;
    private bool isOpen = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.door1 = this.transform.Find("door1");
        this.door2 = this.transform.Find("door2");
        this.home = this.position;
        this.velocity = Vector2.zero;
    }

    public override void Advance()
    {
        base.Advance();

        var player = this.map.player;

        if (this.timeSinceFire > 0)
        {
            this.timeSinceFire -= 1;

            if (this.timeSinceFire < 1)
            {
                if (player.state == Player.State.SpringFromPushPlatform)
                    player.state = Player.State.Normal;
            }

            Particle.CreateDust(this.chunk.transform, 1, 2, player.position);
        }
        else if (player.InsideSpaceBooster == this)
        {
            var targetAngleRadians = Mathf.PI;

            this.visibleAngleRadians = Util.WrapAngleMinus180To180(
                visibleAngleRadians - targetAngleRadians
            ) + targetAngleRadians;
            this.visibleAngleRadians = Util.Slide(
                this.visibleAngleRadians, targetAngleRadians, 0.25f
            );

            this.doorsOpenAmount = Util.Slide(
                this.doorsOpenAmount, 0, 0.1f
            );
        }
        else
        {
            var toPlayer = player.position - this.position;

            float targetAngleRadians = 0;
            bool following = (toPlayer.sqrMagnitude < 8 * 8);

            this.visibleAngleRadians = Util.WrapAngleMinus180To180(
                visibleAngleRadians - targetAngleRadians
            ) + targetAngleRadians;
            this.visibleAngleRadians = Util.Slide(
                this.visibleAngleRadians, targetAngleRadians, 0.2f
            );

            this.doorsOpenAmount = Util.Slide(
                this.doorsOpenAmount, following ? 1 : 0, 0.1f
            );

            if (following)
            {
                if (!isOpen)
                {
                    this.isOpen = true;
                    Audio.instance.PlaySfx(
                        Assets.instance.sfxEnvSpaceboosterOpen,
                        position: this.position
                    );
                }
            }
            else
            {
                this.isOpen = false;
            }
        }

        this.transform.localRotation = Quaternion.Euler(
            0, 0, this.visibleAngleRadians * 180 / Mathf.PI
        );
        this.door1.transform.localRotation = Quaternion.Euler(
            0, 0, this.doorsOpenAmount * 65
        );
        this.door2.transform.localRotation = Quaternion.Euler(
            0, 0, this.doorsOpenAmount * -65
        );

        if (
            this.timeSinceFire == 0 &&
            (player.state == Player.State.Normal || player.state == Player.State.Jump) &&
            player.groundState.onGround == false
        )
        {
            if ((player.position - this.position).sqrMagnitude < 2 * 2)
            {
                player.EnterSpaceBooster(this);
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvSpaceboosterEnter,
                    position: this.position
                );
            }
            // else if ((player.position - this.position).sqrMagnitude < 4 * 4)
            // {
            //     float distance = (player.position - this.position).magnitude;
            //     float intensity = 1 - ((distance - 2) / 2);
            //     player.velocity *= (1 - intensity);
            //     player.velocity += (this.position - player.position) * 0.05f;
            // }
        }

        var target = this.home;
        target.x += Mathf.Sin(this.map.frameNumber * 0.04f) * 0.4f;
        target.y += Mathf.Cos(this.map.frameNumber * 0.034f) * 0.4f;
        target += new Vector2(0.2f, 0.2f) * Mathf.Sin(this.map.frameNumber * 0.032f);
        target += new Vector2(0.2f, -0.2f) * Mathf.Sin(this.map.frameNumber * 0.023f);
        this.velocity *= 0.95f;
        this.velocity += (target - this.position) * 0.01f;
        this.position += this.velocity;
        this.transform.position = this.position;
    }

    public void AdvancePlayer()
    {
        var player = this.map.player;
        player.position = this.position;
        player.velocity = this.velocity;
    }

    public void FirePlayer()
    {
        if (Mathf.Abs(this.visibleAngleRadians) < Mathf.PI * 0.95f) return;

        this.map.player.ExitSpaceBooster();
        this.map.player.velocity = new Vector2(0, 0.6f);
        this.map.player.state = Player.State.SpringFromPushPlatform;
        this.timeSinceFire = 25;
        this.doorsOpenAmount = 1;
        this.velocity = new Vector2(0, -0.5f);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSpaceboosterShoot,
            position: this.position
        );
    }
}
