
using UnityEngine;

public class Spear : Item
{
    public static Spear Create(Vector2 position, int rotation, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.rainyRuins.spear, chunk.transform);
        obj.name = "Spawned Arrow";
        var arrow = obj.GetComponent<Spear>();
        arrow.Init(position, rotation, chunk);
        chunk.items.Add(arrow);
        return arrow;
    }

    public Animated.Animation animationStatic;
    public Animated.Animation animationFire;

    public Vector2 velocity;
    public Vector2 direction;
    public SpearSpawner spawner;
    public bool flying;
    public bool stuck;
    public int stuckTime;
    private TileGrid stuckInTileGrid;

    private bool playerStandingOnTop = false;
    private bool isStuck = false;

    public override void Init(Def def) => throw new System.NotImplementedException();

    public void Init(Vector2 position, int rotation, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.rotation = rotation;
        this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        this.chunk = chunk;

        this.animated.PlayAndLoop(this.animationFire);

        var hitbox = new Hitbox(-2, 2, 0, 0, rotation: 0, Hitbox.SolidOnTop);
        if (this.rotation == 0 || this.rotation == 180)
            this.hitboxes = new [] { hitbox };
        else
            this.hitboxes = new Hitbox[0];

        this.flying = true;
        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        this.onCollisionFromAbove = (c) =>
        {
            if (c.otherPlayer != null)
                this.playerStandingOnTop = true;
        };
    }

    public override void Advance()
    {
        if (this.flying == true)
        {
            var raycast = new Raycast(
                this.map,
                this.position,
                ignoreItem1: this.spawner,
                ignoreItemType: typeof(Spear)
            );
            var r = raycast.Arbitrary(
                this.position + (this.direction * 2.2f),
                this.direction,
                this.velocity.magnitude
            );
            if (r.anything == true)
            {
                this.velocity = this.direction * r.distance;
                this.stuck = true;
                this.stuckInTileGrid = r.tile?.tileGrid;
                this.flying = false;
                
                this.animated.PlayOnce(this.animationStatic);
            }
            this.position += this.velocity;
            this.transform.position = this.position;

            if (r.anything == false)
                CheckIfHitPlayer();
        }
        else if (this.stuck == true)
        {
            var raycast = new Raycast(
                this.map,
                this.position,
                ignoreItem1: this.spawner,
                ignoreItemType: typeof(Spear)
            );
            var r = raycast.Arbitrary(
                this.position + (this.direction * 2.2f),
                this.direction,
                0.5f
            );

            var playerNoLongerOnTop =
                this.playerStandingOnTop == true &&
                this.map.player.groundState.onGround == false;

            this.position += this.stuckInTileGrid?.effectiveVelocity ?? Vector2.zero;
            this.transform.position = this.position;

            this.velocity = Vector2.zero;
            this.stuckTime += 1;
            if (this.stuckTime > 200 ||
                playerNoLongerOnTop == true ||
                r.anything == false)
            {
                this.stuck = false;
                this.hitboxes = new Hitbox[0];
            }

            if (!this.isStuck)
            {
                this.isStuck = true;
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvSpearImpact,
                    position: this.position
                );
            }            
        }
        else
        {
            this.velocity += new Vector2(0, -1 / 16f);
            this.position += this.velocity;
            this.transform.position = this.position;

            Rect cameraRectangle =
                this.map.gameCamera.VisibleRectangle().Inflate(4);

            if (cameraRectangle.Contains(this.position) == false)
            {
                this.chunk.items.Remove(this);
                Destroy(this.gameObject);
            }
        }
    }

    private void CheckIfHitPlayer()
    {
        var tip = this.position + (this.direction * 1);
        if (this.map.player.ShouldCollideWithItems() == true &&
            (this.map.player.position - tip).sqrMagnitude < 1 * 1)
        {
            this.map.player.Hit(tip);
        }
    }
}
