
using UnityEngine;

public class SpearSpawner : Item
{
    public Animated.Animation animationStatic;
    public Animated.Animation animationFire;

    private Vector2 direction;
    private Vector2 scanFrom;
    private float scanDistance;

    private int timeSinceFired = 100;
    private Spear spearFired = null;

    private bool wasJustFired = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.direction = Vector2.right;

        if (def.tile.flip == true)
        {
            if (this.rotation == 0)
                this.rotation = 180;
            else if (this.rotation == 90)
                this.rotation = 270;
            else if (this.rotation == 180)
                this.rotation = 0;
            else if (this.rotation == 270)
                this.rotation = 90;
            this.transform.localRotation = Quaternion.Euler(0, 0, this.rotation);
        }
        
        if (this.rotation == 180)
        {
            // if the spear is facing left, we flip the sprite vertically (right-side up)
            this.transform.localScale = new Vector3(1f, -1f, 1f);
        }

        switch (this.rotation)
        {
            case   0: this.direction = Vector2.right; break;
            case  90: this.direction = Vector2.up;    break;
            case 180: this.direction = Vector2.left;  break;
            case 270: this.direction = Vector2.down;  break;
        }
        this.scanFrom = this.position + (this.direction * 0.55f);

        var raycast = new Raycast(this.map, this.scanFrom);
        this.scanDistance =
            raycast.Arbitrary(this.scanFrom, this.direction, 400).distance;
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        var playerPosition = this.map.player.position;
        var perpendicular = new Vector2(this.direction.y, -this.direction.x);
        var distancePerpendicular =
            Vector2.Dot(perpendicular, playerPosition - this.scanFrom);
        var distanceAlong =
            Vector2.Dot(this.direction, playerPosition - this.scanFrom);

        var isPlayerInSameChunk =
            this.map.NearestChunkTo(playerPosition) == this.chunk;

        if (Mathf.Abs(distancePerpendicular) < 1 &&
            distanceAlong > 0 &&
            distanceAlong < this.scanDistance &&
            isPlayerInSameChunk == true)
        {
            if (this.timeSinceFired > 60 && this.spearFired == null)
            {
                PlayFireAnimation();
            }
        }
        else
        {
            this.timeSinceFired += 1;
        }
    }

    private void PlayFireAnimation()
    {
        this.animated.PlayOnce(this.animationFire, delegate
        {
            this.animated.PlayOnce(this.animationStatic);
        });
        this.animated.OnFrame(18, delegate
        {
            var velocity = new Vector2();
            if (this.rotation == 0)   velocity.x = 0.5f;
            if (this.rotation == 90)  velocity.y = 0.5f;
            if (this.rotation == 180) velocity.x = -0.5f;
            if (this.rotation == 270) velocity.y = -0.5f;

            var spearPosition = this.position + (3.5f * this.direction);

            var spear = Spear.Create(spearPosition, this.rotation, this.chunk);
            spear.spawner = this;
            spear.velocity = velocity;
            spear.direction = velocity.normalized;

            this.timeSinceFired = 0;
            this.spearFired = spear;
            this.wasJustFired = false;
        });
        if (!this.wasJustFired)
        {
            this.wasJustFired = true;

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvSpearActivate,
                position: this.position
            );
        }
    }
}
