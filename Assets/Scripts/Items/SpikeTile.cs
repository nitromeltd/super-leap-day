
using UnityEngine;
using System.Linq;
using System.Collections.Generic;
using System;

public class SpikeTile : Item
{
    public enum Direction { Left, Right, Top, Bottom, Horz, Vert, Junct, Single };
    public Direction direction;
    public SpinBlock controlledBySpinBlock;
    public Sprite[] spritesLeft;
    public Sprite[] spritesRight;
    public Sprite[] spritesTop;
    public Sprite[] spritesBottom;
    public Sprite[] spritesHorz;
    public Sprite[] spritesVert;
    public Sprite[] spritesJunct;
    public Sprite[] spritesSingle;

    public Animated.Animation animationShineLeft;
    public Animated.Animation animationShineRight;
    public Animated.Animation animationShineTop;
    public Animated.Animation animationShineBottom;
    public Animated.Animation animationShineHorz;
    public Animated.Animation animationShineVert;
    public Animated.Animation animationShineJunct;
    public Animated.Animation animationShineSingle;
    
    public SnowBlock snowBlock;

    private SpriteRenderer shineSpriteRenderer;
    private Animated shineAnimated;

    // Hitbox dimensions for ¨Normal¨ Spike (Right)
    private const float HitboxLeft = 0.45f;
    private const float HitboxRight = 0.20f;
    private const float HitboxUp = 0.25f;
    private const float HitboxDown = 0.25f;

    // Hitbox dimensions for Horizontal Spike
    private const float HitboxHorzHeight = 0.25f;
    private const float HitboxHorzWidth = 0.40f;

    // Hitbox dimensions for Junction Spike
    private const float HitboxJunc = 0.35f;

    // Hitbox dimensions for Single Spike
    private const float HitboxSingle = 0.25f;

    public override void Init(Def def)
    {
        base.Init(def);

        {
            var shine = this.transform.Find("Shine");
            this.shineSpriteRenderer = shine.GetComponent<SpriteRenderer>();
            this.shineAnimated       = shine.GetComponent<Animated>();
        }

        this.transform.localRotation = Quaternion.identity;
        this.transform.localScale = new Vector3(1, 1, 1);
        SetHitbox(this.def.tile.flip);
        Sprite[] choices = SpritesForDirection(this.direction);
        this.spriteRenderer.sprite = choices[UnityEngine.Random.Range(0, choices.Length)];
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void SetHitbox(bool flipped)
    {
        float left = flipped ? -HitboxRight : -HitboxLeft;
        float right = flipped ? HitboxLeft : HitboxRight;
        float down = -HitboxDown;
        float up = HitboxUp;

        if (def.tile.tile.Contains("right") && flipped == false)
        {
            if (this.rotation == 0) this.direction = Direction.Right;
            if (this.rotation == 90) this.direction = Direction.Top;
            if (this.rotation == 180) this.direction = Direction.Left;
            if (this.rotation == 270) this.direction = Direction.Bottom;
        }
        else if (def.tile.tile.Contains("right") && flipped == true)
        {
            if (this.rotation == 0) this.direction = Direction.Left;
            if (this.rotation == 90) this.direction = Direction.Bottom;
            if (this.rotation == 180) this.direction = Direction.Right;
            if (this.rotation == 270) this.direction = Direction.Top;
        }
        else if (def.tile.tile.Contains("horz"))
        {
            if (this.rotation == 0 || this.rotation == 180)
                this.direction = Direction.Horz;
            else
                this.direction = Direction.Vert;
                
            up = HitboxHorzHeight;
            down = -HitboxHorzHeight;
            left = -HitboxHorzWidth;
            right = HitboxHorzWidth;
        }
        else if (def.tile.tile.Contains("junct"))
        {
            this.direction = Direction.Junct;
            right = up = HitboxJunc;
            left = down = -HitboxJunc;
        }
        else if (def.tile.tile.Contains("single"))
        {
            this.direction = Direction.Single;
            right = up = HitboxSingle;
            left = down = -HitboxSingle;
        }

        this.hitboxes = new [] {
            new Hitbox(left, right, down, up, this.rotation, Hitbox.NonSolid)
        };
    }

    private Sprite[] SpritesForDirection(Direction d)
    {
        switch (d)
        {
            case Direction.Left:   return this.spritesLeft;
            case Direction.Right:  return this.spritesRight;
            case Direction.Top:    return this.spritesTop;
            case Direction.Bottom: return this.spritesBottom;
            case Direction.Horz:   return this.spritesHorz;
            case Direction.Vert:   return this.spritesVert;
            case Direction.Junct:  return this.spritesJunct;
            case Direction.Single: return this.spritesSingle;
        }
        return this.spritesJunct;
    }

    private Animated.Animation ShineAnimationForDirection(Direction d)
    {
        switch (d)
        {
            case Direction.Left:   return this.animationShineLeft;
            case Direction.Right:  return this.animationShineRight;
            case Direction.Top:    return this.animationShineTop;
            case Direction.Bottom: return this.animationShineBottom;
            case Direction.Horz:   return this.animationShineHorz;
            case Direction.Vert:   return this.animationShineVert;
            case Direction.Junct:  return this.animationShineJunct;
            case Direction.Single:  return this.animationShineSingle;
        }
        return this.animationShineJunct;
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if(IsCoveredWithSnow() == true && this.map.theme == Theme.SunkenIsland)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            foreach (var beachBall in this.chunk.subsetOfItemsOfTypeBeachBall)
            {
                if (beachBall.hitboxes[0].InWorldSpace().Overlaps(thisRect) && beachBall.IsPopped() == false)
                {
                    beachBall.Pop(this);
                    if(this.snowBlock != null)
                    {
                        this.snowBlock.Melt();
                    }
                }
            }

            foreach (var raft in this.chunk.subsetOfItemsOfTypeRaft)
            {
                if (raft.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                {
                    if(this.snowBlock != null)
                    {
                        this.snowBlock.Melt();
                    }
                }
            }
        }

        if(IsDeadly() == true)
        {
            var player = this.map.player;
            var thisRect = this.hitboxes[0].InWorldSpace();
            if (player.ShouldCollideWithItems() && thisRect.Overlaps(player.Rectangle()))
            {
                this.map.player.Hit(this.position);
            }
            else
            {
                foreach (var enemy in this.chunk.subsetOfItemsOfTypeWalkingEnemy)
                {
                    if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                        enemy.Kill(new Enemy.KillInfo { itemDeliveringKill = this });
                }
                
                if(this.map.theme == Theme.SunkenIsland)
                {
                    foreach (var beachBall in this.chunk.subsetOfItemsOfTypeBeachBall)
                    {
                        if (beachBall.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                            beachBall.Pop(this);
                    }
                }

                if (this.map.theme == Theme.TreasureMines)
                {
                    foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
                    {
                        if (minecart.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                        {
                            minecart.DestroyMinecartAndEjectPlayer();
                        }
                    }
                }
            }
        }
        

        var freq = 200;
        var offsetX = freq - ((this.def.tx * 4) % freq);
        var offsetY = ((this.def.ty * 4) % freq);
        if ((this.map.frameNumber + offsetX + offsetY) % freq == 0)
        {
            var a = ShineAnimationForDirection(this.direction);
            this.shineSpriteRenderer.enabled = true;
            this.shineAnimated.PlayOnce(a, delegate
            {
                this.shineSpriteRenderer.enabled = false;
            });
        }
    }

    private bool IsDeadly()
    {
        if(IsCoveredWithSnow() == true)
        {
            return false;
        }

        return true;
    }

    void DebugSpikeTiles()
    {
        // Debug.Log("============================");
        // List<SpikeTile> spikes = new List<SpikeTile>();
        // spikes.AddRange(this.chunk.items.OfType<SpikeTile>());

        // foreach(var s in spikes)
        // {
        //     var sname = "----";
        //     if(s.snowBlock)
        //     {
        //         sname = s.snowBlock.name;
        //     }
        //     Debug.Log($"{s}({s.position.x},{s.position.y} - {s.IsCoveredWithSnow()},{sname})");
        // }
        // Debug.Log("============================");
    }


    public void CoverWithSnow()
    {
        if(this.snowBlock == null)
        {
            Vector2 pos;
            if (this.controlledBySpinBlock != null)
            {
                pos = new Vector2(this.ItemDef.tx + 0.5f, this.ItemDef.ty + 0.5f);
            }
            else
            {
                pos = this.position;
            }

            this.snowBlock = SnowBlock.Create(pos, this.chunk);
            this.snowBlock.AttachToGroup(this.chunk.groupLayer.RegisterFollower(this.snowBlock, this.ItemDef.tx, this.ItemDef.ty));

            if (this.controlledBySpinBlock != null)
            {
                this.snowBlock.controlledBySpinBlock = this.controlledBySpinBlock;
                this.controlledBySpinBlock.AddToAffectedItems(this.snowBlock, true);
            }
        }
        else
        {
            this.snowBlock.ForceUnmelt();
        }
    }

    public void AssignNewSnowBlock(SnowBlock snowBlock)
    {
        if(this.snowBlock != null && this.snowBlock != snowBlock)
        {
            this.snowBlock.Melt();
        }
        this.snowBlock = snowBlock;
    }

    public bool IsCoveredWithSnow()
    {
        if(this.snowBlock != null)
        {
            return this.snowBlock.IsSnowActive();
        }
        
        return false;
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox(this.def.tile.flip);
    }
}
