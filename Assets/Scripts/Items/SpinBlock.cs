using UnityEngine;
using NitromeEditor;
using System.Collections.Generic;
using System.Linq;

public class SpinBlock : Item
{
    public AnimationCurve spinAnimationCurve;
    public Animated.Animation cannonShootAnimation;
    public Animated[] cannons;

    private Path path;
    private float timeAtStart;
    private Vector2 initialPosition;
    private Transform pin;
    private List<AffectedItemDef> affectedItems;
    private bool waitingToShoot = false;

    private int startingRotation;
    private float timePerQuadrant = 1.5f;
    private static readonly float TimeRotating = 0.3f;
    private bool rotateCCW = true;
    private float lastAngle = 0;

    private struct AffectedItemDef
    {
        public Item item;
        public Vector2 position;
        public int initialRotation;
        public int currentRotation;
        public bool shouldRotate;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.startingRotation = def.tile.rotation;

        if (def.tile.flip == true && this.cannons.Length == 1)
        {
            this.startingRotation = (this.startingRotation + 180) % 360;
        }

        if (def.tile.properties?.ContainsKey("rotation_direction") == true)
            this.rotateCCW = (def.tile.properties["rotation_direction"].s == "CCW");
        else
            this.rotateCCW = true;

        if (def.tile.flip == true)
            this.rotateCCW = !this.rotateCCW;

        if (def.tile.properties?.ContainsKey("rotation_delay") == true)
            this.timePerQuadrant = def.tile.properties["rotation_delay"].f;
        else
            this.timePerQuadrant = 1.5f;
        this.timePerQuadrant = Mathf.Clamp(this.timePerQuadrant, 1.5f, 10f);

        AttachToGroup(this.chunk.groupLayer.GetGroupForCell(def.tx, def.ty));
        this.initialPosition = this.position;
        this.pin = this.transform.Find("Pin");

        this.hitboxes = new [] {
            new Hitbox(-1.5f, 1.5f, -1.5f, 1.5f, this.rotation, Hitbox.Solid, true, true)
        };
        
        this.affectedItems = new List<AffectedItemDef>();
        RescanAffectedItems();

        this.path = this.chunk.pathLayer.NearestPathTo(this.position, 2);
        if (this.path != null)
        {
            this.timeAtStart = this.path.NearestPointTo(this.position)?.timeSeconds ?? 0;

            // DrawPathDots();
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
        // DebugAffecteditems();
    }

    private void RescanAffectedItems()
    {
        if (this.group == null)
            return;

        foreach(var f in this.group.followers)
        {
            var affItem = f.Key.item;
            AffectedItemDef found = this.affectedItems.FirstOrDefault(x => x.item == affItem);

            if (found.item == null)
            {
                if (affItem != this)
                {
                    bool rotateItem = true;

                    if (affItem is Pickup)
                    {
                        rotateItem = false;
                        (affItem as Pickup).controlledBySpinBlock = this;
                    }

                    if (affItem is SpikeTile)
                    {
                        (affItem as SpikeTile).controlledBySpinBlock = this;
                    }

                    if (affItem is AutoCannon)
                    {
                        (affItem as AutoCannon).controlledBySpinBlock = this;
                    }

                    if (affItem is MetalBlock)
                    {
                        (affItem as MetalBlock).controlledBySpinBlock = this;
                    }

                    this.affectedItems.Add(
                        new AffectedItemDef
                        {
                            item = affItem,
                            position = affItem.position - this.initialPosition,
                            initialRotation = affItem.rotation,
                            currentRotation = affItem.rotation,
                            shouldRotate = rotateItem,
                        }
                    );
                }
            }
        }
    }

    public void AddToAffectedItems(Item affItem, bool rotateItem)
    {
        AffectedItemDef found = this.affectedItems.FirstOrDefault(x => x.item == affItem);

        if (found.item == null)
        {
            this.affectedItems.Add(
                new AffectedItemDef
                {
                    item = affItem,
                    position = affItem.position - this.initialPosition,
                    initialRotation = affItem.rotation,
                    currentRotation = affItem.rotation,
                    shouldRotate = rotateItem,
                }
            );
        }
    }

    public Vector2 RotatePointAroundPivot(Vector2 pointToRotate, Vector2 pivotPoint, float angleDegrees)
    {
        float sin = Mathf.Sin(angleDegrees * Mathf.Deg2Rad);
        float cos = Mathf.Cos(angleDegrees * Mathf.Deg2Rad);

        Vector2 translatedPoint = pointToRotate - pivotPoint;
        float x = translatedPoint.x * cos - translatedPoint.y * sin;
        float y = translatedPoint.x * sin + translatedPoint.y * cos;

        Vector2 rotatedPoint = new Vector2(x,y);
        return (rotatedPoint + pivotPoint);
    }

    public void UpdateHitBoxesOfAffectedItems(int currentSpinBlockRotation)
    {
        for (int i = 0; i < this.affectedItems.Count; i++)
        {
            var p = this.affectedItems[i];
            if (p.shouldRotate == true)
            {
                p.currentRotation = (int)Util.WrapAngle0To360(
                    p.initialRotation + currentSpinBlockRotation - this.startingRotation
                );
                p.item.UpdateHitboxesForSpinBlock(p.currentRotation);
                this.affectedItems[i] = p;
            }
            
            if(p.item is AutoCannon)
            {
                (p.item as AutoCannon).DelayedShot(20);
            }
        }
    }

    private void DebugAffecteditems()
    {
        // int i = 0;
        // foreach(var a in this.affectedItems)
        // {
        //     Debug.Log($"{i++}: {a.item.name}, r:{a.item.rotation}");
        // }
    }

    public override void Advance()
    {
        float percentageRotating = TimeRotating / timePerQuadrant;
        float percentageStill = 1.0f - percentageRotating;

        var throughQuadrant = (this.map.frameNumber / (timePerQuadrant * 60.0f)) % 4.0f;
        var quadrant = Mathf.FloorToInt(throughQuadrant);
        throughQuadrant -= quadrant;

        if (this.rotateCCW == false)
            quadrant = 4 - quadrant;
        quadrant += this.startingRotation / 90;
        quadrant %= 4;

        this.hitboxes[0].wallSlideAllowedOnLeft = (throughQuadrant < percentageStill);
        this.hitboxes[0].wallSlideAllowedOnRight = (throughQuadrant < percentageStill);

        if (this.cannons.Length > 0)
        {
            if (throughQuadrant < percentageRotating)
            {
                this.waitingToShoot = true;
            }
            else if (throughQuadrant >= percentageRotating && this.waitingToShoot == true)
            {
                this.waitingToShoot = false;
                foreach (var cannon in cannons)
                {
                    cannon.PlayOnce(this.cannonShootAnimation);
                }

                Vector2 dir;
                dir.x = Mathf.Cos(quadrant * Mathf.PI / 2);
                dir.y = Mathf.Sin(quadrant * Mathf.PI / 2);

                var bullet = AutoCannonBullet.Create(
                    this.position + (dir * 26 / 16f),
                    this.chunk
                );
                bullet.velocity = dir * 0.125f;
                bullet.fromSpinBlock = this;

                if (this.cannons.Length == 2)
                {
                    bullet = AutoCannonBullet.Create(
                        this.position - (dir * 26 / 16f),
                        this.chunk
                    );
                    bullet.velocity = dir * -0.125f;
                    bullet.fromSpinBlock = this;
                }
            }
        }

        {
            var angle = quadrant * 90.0f;
            if (throughQuadrant > percentageStill)
            {
                float s = (throughQuadrant - percentageStill) / percentageRotating;
                angle += spinAnimationCurve.Evaluate(s) * (this.rotateCCW ? 90 : -90);

                if(this.lastAngle < 90 && spinAnimationCurve.Evaluate(s) * 90.0f >= 90)
                {
                    UpdateHitBoxesOfAffectedItems(
                        (quadrant + (this.rotateCCW ? 1 : -1)) * 90
                    );
                }
            }
            this.lastAngle = angle - quadrant * 90f;
            this.transform.localRotation = Quaternion.Euler(0, 0, angle);

            if (this.path != null)
            {
                var time = TimeForFrame(this.map.frameNumber) % this.path.duration;

                this.position = this.path.PointAtTime(time).position;
                this.transform.position = this.position;
            }

            foreach(var p in this.affectedItems)
            {
                p.item.position = p.item.transform.position =
                    this.position + RotatePointAroundPivot(
                        p.position, Vector2.zero, angle - this.startingRotation
                    );

                if (p.shouldRotate == true)
                {
                    if (p.item is SpikeTile)
                    {
                        p.item.transform.localRotation = Quaternion.Euler(
                            0, 0, angle - this.startingRotation
                        );
                    }
                    else
                    {
                        p.item.transform.localRotation = Quaternion.Euler(
                            0, 0, p.initialRotation + angle
                        );
                    }

                }
            }
        }

        if (this.pin != null)
        {
            var angle = this.transform.rotation.eulerAngles.z;
            this.pin.localRotation = Quaternion.Euler(0, 0, -angle);
        }

        if (Mathf.FloorToInt((this.map.frameNumber + 19) / (timePerQuadrant * 60)) >
            Mathf.FloorToInt((this.map.frameNumber + 18) / (timePerQuadrant * 60)))
        {
            Audio.instance.PlaySfx(
                Assets.instance.moltenFortress.sfxSpinBlockTurn, null, this.position
            );
        }
    }

    public void DetachItem(Item itemToDetach)
    {
        for (int i = this.affectedItems.Count - 1; i >= 0; i--)
        {
            if(this.affectedItems[i].item == itemToDetach)
            {
                this.affectedItems.RemoveAt(i);
            }
        }
    }

    private float TimeForFrame(int frameNumber)
    {
        return this.timeAtStart + (frameNumber / 60.0f);
    }

    public override void Reset()
    {
        RescanAffectedItems();
    }
}
