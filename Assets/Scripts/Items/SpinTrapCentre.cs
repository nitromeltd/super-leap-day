
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class SpinTrapCentre : Item
{
    public Transform spinningBit;

    private Item[] items;
    private Dictionary<Item, Vector2> relativePositions;
    private float tileSpeed;
    private float currentSpeed;
    private float targetSpeed;
    private float angleRadians;

    private GameObject hotWheel;
    private GameObject coldWheel;
    public override void Init(Def def)
    {
        base.Init(def);

        float flipValue = def.tile.flip ? -1f : 1f;
        this.tileSpeed = this.targetSpeed = this.currentSpeed = def.tile.properties["speed"].f * flipValue;

        if (this.chunk.connectionLayer.NearestPathTo(this.position, 1) != null)
        {
            // limit to parts or fruit on any touching path
            Item[] fruit = this.chunk.items.OfType<Fruit>().ToArray();
            Item[] parts = this.chunk.items.OfType<SpinTrapPart>().ToArray();
            this.items = fruit.Concat(parts).Where(item =>
            {
                var path =
                    this.chunk.connectionLayer.NearestPathTo(item.position, 1);
                if (path == null) return false;
                var node = path.NearestNodeTo(this.position);
                return (node.Position - this.position).sqrMagnitude < 1 * 1;
            }).ToArray();
        }
        else
        {
            // limit to the parts nearest to this centre
            var centres = this.chunk.items.OfType<SpinTrapCentre>().ToArray();
            SpinTrapCentre NearestCentre(Vector2 pt) =>
                centres.OrderBy(c => (pt - c.position).sqrMagnitude).First();
            this.items = this.chunk.items
                .OfType<SpinTrapPart>()
                .Where(p => NearestCentre(p.position) == this)
                .ToArray();
        }

        this.relativePositions = new Dictionary<Item, Vector2>();
        foreach (var item in this.items)
        {
            this.relativePositions[item] = item.position - this.position;
            (item as Fruit)?.NotifyControlledByOtherItem();
        }

        this.hotWheel = this.transform.Find("Back Hot").gameObject;
        this.coldWheel = this.transform.Find("Back Cold").gameObject;
        NotifyChangeTemperature(this.map.currentTemperature);
    }

    public override void Advance()
    {
        this.currentSpeed = Util.Slide(this.currentSpeed, this.targetSpeed, 0.01f);

        /*var player = this.map.player;
        if ((player.position - this.position).sqrMagnitude < 1.5f * 1.5f)
            player.Hit(this.position);*/

        this.angleRadians += 0.01f * this.currentSpeed;
        var c = Mathf.Cos(angleRadians);
        var s = Mathf.Sin(angleRadians);
        foreach (var item in this.items)
        {
            var relative = this.relativePositions[item];
            var rx = (c * relative.x) + (s * relative.y);
            var ry = (c * relative.y) - (s * relative.x);
            item.position = this.position + new Vector2(rx, ry);
            item.transform.position = item.position;
        }

        this.spinningBit.localRotation =
            Quaternion.Euler(-Vector3.forward * this.angleRadians * Mathf.Rad2Deg);
    }
    
    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        if(newTemperature == Map.Temperature.Hot)
        {
            this.targetSpeed = this.tileSpeed;
            this.hotWheel.SetActive(true);
            this.coldWheel.SetActive(false);
        }
        else if(newTemperature == Map.Temperature.Cold)
        {
            this.targetSpeed = 0f;
            this.hotWheel.SetActive(false);
            this.coldWheel.SetActive(true);
        }
    }
}
