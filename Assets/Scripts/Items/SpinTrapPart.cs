
using UnityEngine;

public class SpinTrapPart : Item
{
    [Header("Flame")]
    public GameObject flameGO;
    public GameObject glowTransform;
    public Animated animatedFlameBall;
    public Animated.Animation flameBallAnimation;

    [Header("Snow")]
    public GameObject snowGO;
    public Animated animatedSnowBall;
    public Animated.Animation snowIdleAnimation;
    public Animated.Animation snowMeltAnimation;
    public Sprite[] snowParticles;

    private Sprite GetRandomSnowParticle
        => this.snowParticles[Random.Range(0, this.snowParticles.Length)];

    public enum State
    {
        Flame,
        Snow
    }

    private State currentState;
    private bool melted;
    private float currentRotation;
    private float breatheTimer;
    private float sparkleTime;

    private const float Size = 0.50f;
    private const float FireBallRotationSpeed = 4.50f;
    private const float BreathePeriod = 0.50f;
    private const float BreatheAmplitude = 0.10f;
    private const float BounceStrength = 0.25f;
    private const float BounceWallslidingStrengthX = 0.125f;

    public override void Init(Def def)
    {
        base.Init(def);

        float halfSize = Size / 2f;

        this.hitboxes = new [] {
            new Hitbox(-halfSize, halfSize, -halfSize, halfSize, this.rotation, Hitbox.NonSolid)
        };

        this.currentState = temperature == Map.Temperature.Hot ?
            State.Flame : State.Snow;

        NotifyChangeTemperature(temperature);
    }

    public override void NotifyChangeTemperature(Map.Temperature newTemperature)
    {
        bool isTemperatureHot = (newTemperature == Map.Temperature.Hot);
        this.flameGO.SetActive(isTemperatureHot);
        this.snowGO.SetActive(!isTemperatureHot);

        if(isTemperatureHot)
        {
            this.currentState = State.Flame;
            this.animatedFlameBall.PlayAndLoop(this.flameBallAnimation);

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvSpinwheelHot,
                position: this.position
            );
        }
        else if(newTemperature == Map.Temperature.Cold)
        {
            this.currentState = State.Snow;
            this.animatedSnowBall.PlayAndLoop(this.snowIdleAnimation);
            this.currentRotation = 0f;
            this.sparkleTime = 12;

            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvSpinwheelCold,
                position: this.position
            );
        }

        this.melted = false;
    }

    public override void Advance()
    {
        switch(this.currentState)
        {
            case State.Flame:
                AdvanceFlame();
            break;

            case State.Snow:
                AdvanceSnow();
            break;
        }

        this.transform.rotation =
            Quaternion.Euler(Vector3.forward * this.currentRotation);
    }

    private void AdvanceFlame()
    {
        {
            this.breatheTimer += Time.deltaTime;
            float theta = this.breatheTimer / BreathePeriod;
            float distance = BreatheAmplitude * Mathf.Sin(theta);
            
            this.glowTransform.transform.localScale = 
                (Vector3.one + Vector3.one * BreatheAmplitude) + (Vector3.one * distance);
        }
        
        // spawn flame particle
        if(this.map.frameNumber % 4 == 0)
        {
            Vector2 particleSpawnPos = this.position + (Random.insideUnitCircle * 0.5f)
                + Vector2.up * 0.25f;
            Animated.Animation particleAnimation = UnityEngine.Random.value > 0.5f ?
                Assets.instance.flameAnimationA : Assets.instance.flameAnimationB;

            Particle flameParticle = Particle.CreateAndPlayOnce(
                particleAnimation,
                particleSpawnPos,
                this.transform.parent
            );

            flameParticle.spriteRenderer.sortingLayerName = "Items";
            flameParticle.spriteRenderer.sortingOrder = 100;
            flameParticle.acceleration = Vector2.up * 0.005f;
        }

        var player = this.map.player;
        if (this.hitboxes[0].InWorldSpace().Overlaps(player.Rectangle()) && player.ShouldCollideWithItems())
        {
            player.Hit(this.position);
        }

        this.currentRotation += FireBallRotationSpeed;
    }

    private void AdvanceSnow()
    {
        // spawn sparkles
        if(this.sparkleTime > 0)
        {
            this.sparkleTime -= 1;

            if(this.sparkleTime % 2 == 0)
            {
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.position.Add(
                        Random.Range(-.5f, .5f),
                        Random.Range(-.5f, .5f)
                    ),
                    this.transform.parent
                );
                float sparkleSpeed = 0.05f;
                sparkle.velocity = new Vector2(Random.Range(sparkleSpeed * -1, sparkleSpeed), Random.Range(sparkleSpeed * -1, sparkleSpeed));
                sparkle.spriteRenderer.sortingLayerName = "Items (Front)";
                sparkle.spriteRenderer.sortingOrder = 10;
            }
        }

        var player = this.map.player;
        var playerRectangle = player.Rectangle();
        var ownRectangle = this.hitboxes[0].InWorldSpace();

        if (this.hitboxes[0].InWorldSpace().Overlaps(player.Rectangle())
            && this.melted == false && player.ShouldCollideWithItems())
        {
            Melt();
            
            var playerAbove =
                playerRectangle.yMin > ownRectangle.yMax - 0.1f ||
                (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.1f;


            if(playerAbove == true)
            {
                BounceOffSnowBall();
            }
            else
            {
                //var playerToTheRight = player.position.x > this.position.x;
                var playerToTheRight = playerRectangle.xMin > ownRectangle.xMax - 0.1f ||
                    (playerRectangle.xMin - player.velocity.x) > ownRectangle.xMax + 0.1f;
                
                var playerBelow =
                    playerRectangle.yMax < ownRectangle.yMin + 0.1f ||
                    (playerRectangle.yMax + player.velocity.y) < ownRectangle.yMin + 0.1f;

                float newPlayerX = player.velocity.x * -0.5f;

                // player to the right of the item, and moving to the left
                if(playerToTheRight == true && player.velocity.x < 0f ||  
                    // OR player to the left of the item, and moving to the right
                    playerToTheRight == false && player.velocity.x > 0f)    
                {
                    player.velocity.x = newPlayerX;
                }

                if(player.position.y < this.position.y && player.velocity.y > 0f)
                {
                    player.velocity.y = 0f;
                }

                if(player.groundState.onGround == true)
                {
                    player.groundspeed = -player.groundspeed;
                }
            }
        }
    }

    private void Melt()
    {
        if(this.melted == true) return;
        this.melted = true;

        this.animatedSnowBall.PlayOnce(this.snowMeltAnimation);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvSpinwheelJump,
            position: this.position
        );

        float xVariation = 0.25f;
        float yVariation = 0.50f;
        Vector2 originPosition = this.position;

        for (int i = 0; i < 3; i++)
        {
            originPosition += new Vector2(
                UnityEngine.Random.Range(-xVariation, xVariation),
                UnityEngine.Random.Range(-yVariation, yVariation)
            );

            var p = Particle.CreateWithSprite(
                GetRandomSnowParticle,
                60 * 2,
                originPosition,
                this.transform.parent
            );
            p.velocity.x = UnityEngine.Random.Range(-0.05f, 0.05f);
            p.velocity.y = UnityEngine.Random.Range(0.1f, 0.25f);

            float verticalAccelaration = -0.017f;
            p.acceleration = new Vector2(0f, verticalAccelaration);
        }
    }

    private void BounceOffSnowBall()
    {
        Player player = this.map.player;

        if (player.position.y > this.position.y && player.velocity.y < 0)
        {
            // attack from above
            if (player.state == Player.State.WallSlide)
            {
                player.velocity.y = Mathf.Abs(player.velocity.y);
                if (player.velocity.y < BounceStrength)
                    player.velocity.y = BounceStrength;

                player.state = Player.State.Normal;
                player.velocity.x = (player.facingRight ? -1 : 1)
                    * BounceWallslidingStrengthX;
                player.facingRight = player.velocity.x > 0;
            }
            else
            {
                player.velocity.y *= -1;
                if (player.velocity.y < BounceStrength)
                    player.velocity.y = BounceStrength;
            }
        }
        else
        {
            // attack from below
            player.velocity.y -= Util.Sign(player.velocity.y * 0.75f);
        }

        player.groundState = Player.GroundState.Airborne(player);
    }

    public override void Reset()
    {
        NotifyChangeTemperature(this.map.currentTemperature);
    }
}
