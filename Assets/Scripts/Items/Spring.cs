
using UnityEngine;
using System.Linq;

public class Spring : Item
{
    public Animated.Animation animationStatic;
    public Animated.Animation animationBounce;

    private bool diagonal;
    private bool red;

    private int correctionTime;
    private Vector2 correction;

    private int disableBounceTime = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        this.animated.PlayOnce(this.animationStatic);

        this.diagonal = def.tile.tile.Contains("2");
        this.red = def.tile.tile.Contains("red");

        if (def.tile.flip == true)
        {
            if (this.diagonal == true)
            {
                if (this.rotation == 0)
                    this.rotation = 90;
                else if (this.rotation == 90)
                    this.rotation = 180;
                else if (this.rotation == 180)
                    this.rotation = 270;
                else if (this.rotation == 270)
                    this.rotation = 0;
            }
            this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        }

        SetHitbox();
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void SetHitbox()
    {
        if (this.diagonal)
        {
            this.hitboxes = new [] {
                new Hitbox(0, 20 / 16f, 0, 20 / 16f, this.rotation, Hitbox.NonSolid)
            };
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(-1, 1, 0, 1, this.rotation, Hitbox.NonSolid)
            };
        }
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if (this.disableBounceTime > 0)
            this.disableBounceTime -= 1;

        var player = this.map.player;
        var thisRect = this.hitboxes[0].InWorldSpace();
        var collision =
            player.ShouldCollideWithItems() &&
            player.Rectangle().Overlaps(thisRect);

        if (this.disableBounceTime == 0 && collision == true)
            Bounce();

        if (this.correctionTime > 0)
        {
            this.correctionTime -= 1;
            if (this.correctionTime < 1)
            {
                if (player.state == Player.State.Spring)
                    player.velocity += this.correction;

                this.correction = new Vector2();
            }
        }

        foreach (var enemy in this.chunk.subsetOfItemsOfTypeWalkingEnemy)
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.velocity = VelocityAfterSpringing(enemy.velocity);
            }
        }
        foreach (var enemy in this.chunk.items.OfType<Spikey>())
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.BounceFromSpring(VelocityAfterSpringing(enemy.Velocity));
            }
        }
        foreach (var enemy in this.chunk.items.OfType<TurtleCannon>())
        {
            if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                enemy.BounceFromSpring(VelocityAfterSpringing(enemy.Velocity));
            }
        }
        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
            {
                PlayBounceAnimation();
                boulder.velocity = VelocityAfterSpringing(boulder.velocity);
            }
        }
        foreach (Minecart minecart in this.chunk.subsetOfItemsOfTypeMinecart)
        {
            if (minecart.hitboxes[0].InWorldSpace().Overlaps(thisRect))
            {
                PlayBounceAnimation();
                minecart.BounceFromSpring(VelocityAfterSpringing(minecart.velocity));
            }
        }
    }

    public void PlayBounceAnimation()
    {
        AudioClip sfxSpring = this.red ?
            Assets.instance.sfxSpringRed : Assets.instance.sfxSpringYellow;
        Audio.instance.PlaySfx(sfxSpring, position: this.transform.position);
        this.animated.PlayOnce(this.animationBounce, delegate
        {
            this.animated.PlayOnce(this.animationStatic);
        });
    }

    public Vector2 VelocityAfterSpringing(Vector2 before)
    {
        if (this.diagonal == true)
        {
            float sideways = this.red ? 8/16f  : 6/16f;
            float up       = this.red ? 13/16f : 10/16f;
            float down     = this.red ? 10/16f : 8/16f;

            if (this.chunk.gravityDirection == null)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2( sideways,  sideways);
                    case 90:   return new Vector2(-sideways,  sideways);
                    case 180:  return new Vector2(-sideways, -sideways);
                    case 270:  return new Vector2( sideways, -sideways);
                }
            }
            else if (this.chunk.gravityDirection == Player.Orientation.Normal)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2( sideways,  up);
                    case 90:   return new Vector2(-sideways,  up);
                    case 180:  return new Vector2(-sideways, -down);
                    case 270:  return new Vector2( sideways, -down);
                }
            }
            else if (this.chunk.gravityDirection == Player.Orientation.RightWall)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2( down,  sideways);
                    case 90:   return new Vector2(-up,    sideways);
                    case 180:  return new Vector2(-up,   -sideways);
                    case 270:  return new Vector2( down, -sideways);
                }
            }
            else if (this.chunk.gravityDirection == Player.Orientation.Ceiling)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2( sideways,  down);
                    case 90:   return new Vector2(-sideways,  down);
                    case 180:  return new Vector2(-sideways, -up);
                    case 270:  return new Vector2( sideways, -up);
                }
            }
            else if (this.chunk.gravityDirection == Player.Orientation.LeftWall)
            {
                switch (this.rotation)
                {
                    case 0:    return new Vector2( up,    sideways);
                    case 90:   return new Vector2(-down,  sideways);
                    case 180:  return new Vector2(-down, -sideways);
                    case 270:  return new Vector2( up,   -sideways);
                }
            }
        }
        else
        {
            var isDirectlyAgainstGravity = false;
            if (this.chunk.gravityDirection != null)
            {
                switch (this.chunk.gravityDirection)
                {
                    case Player.Orientation.Normal:
                        isDirectlyAgainstGravity = (this.rotation == 0);
                        break;
                    case Player.Orientation.LeftWall:
                        isDirectlyAgainstGravity = (this.rotation == 90);
                        break;
                    case Player.Orientation.Ceiling:
                        isDirectlyAgainstGravity = (this.rotation == 180);
                        break;
                    case Player.Orientation.RightWall:
                        isDirectlyAgainstGravity = (this.rotation == 270);
                        break;
                }
            }
            
            float strength;
            if (this.red == true)
                strength = (isDirectlyAgainstGravity ? 16/16f : 9/16f);
            else
                strength = (isDirectlyAgainstGravity ? 11/16f : 6/16f);
            
            switch (this.rotation)
            {
                case 0:    return new Vector2(before.x,  strength);
                case 90:   return new Vector2(-strength, before.y);
                case 180:  return new Vector2(before.x,  -strength);
                case 270:  return new Vector2( strength, before.y);
            }
        }

        return before;
    }

    private void Bounce()
    {
        bool IsValidCollisionForPlayer(Raycast.Result r)
        {
            if(r.anything == true)
            {
                if(r.item != null)
                {
                    return r.item.solidToPlayer;
                }

                return true;
            }

            return false;
        }
        this.disableBounceTime = 10;

        PlayBounceAnimation();

        var player = this.map.player;
        player.CancelLateJumpTime();
        player.ResetDoubleJump();

        if(player.state == Player.State.InsideBox)
        {
            player.GetBox().velocity = VelocityAfterSpringing(player.GetBox().velocity);
            return;
        }
        else
        {
            player.velocity = VelocityAfterSpringing(player.velocity);
        }

        var resolvedByWallRunning = false;
        var canSnapPlayerToLeft =
            (player.position.x < this.position.x + 0.5f) || (player.velocity.x < 0);
        var canSnapPlayerToRight =
            (player.position.x > this.position.x - 0.5f) || (player.velocity.x > 0);

        if (this.diagonal == false && this.rotation == 0)
        {
            Raycast r = new Raycast(this.map, this.position, ignoreItem1: this);
            if (canSnapPlayerToLeft == true)
            {
                var left = r.Horizontal(this.position.Add(0, 1.5f), false, 18 / 16f);
                if (IsValidCollisionForPlayer(left))
                {
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Player.Orientation.LeftWall, 270, false, false
                    );
                    player.groundspeed = this.red ? -8 / 16f : -5 / 16f;
                    player.position.x = this.position.x;
                    if (player.position.y < this.position.y + 1)
                        player.position.y = this.position.y + 1;
                    resolvedByWallRunning = true;
                }
            }

            if (canSnapPlayerToRight == true)
            {
                var right = r.Horizontal(this.position.Add(0, 1.5f), true, 18 / 16f);
                if (IsValidCollisionForPlayer(right))
                {
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Player.Orientation.RightWall, 90, false, false
                    );
                    player.groundspeed = this.red ? 8 / 16f : 5 / 16f;
                    player.position.x = this.position.x;
                    if (player.position.y < this.position.y + 1)
                        player.position.y = this.position.y + 1;
                    resolvedByWallRunning = true;
                }
            }
        }
        else if (this.diagonal == false && this.rotation == 180)
        {
            Raycast r = new Raycast(this.map, this.position, ignoreItem1: this);
            if (canSnapPlayerToLeft == true)
            {
                var left = r.Horizontal(this.position.Add(0, -0.5f), false, 18 / 16f);
                if (IsValidCollisionForPlayer(left))
                {
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Player.Orientation.LeftWall, 270, false, false
                    );
                    player.groundspeed = this.red ? 5 / 16f : 4 / 16f;
                    player.position.x = this.position.x;
                    if (player.position.y > this.position.y - 1)
                        player.position.y = this.position.y - 1;
                    resolvedByWallRunning = true;
                }
            }

            if (canSnapPlayerToRight == true)
            {
                var right = r.Horizontal(this.position.Add(0, -0.5f), true, 18 / 16f);
                if (IsValidCollisionForPlayer(right))
                {
                    player.state = Player.State.Normal;
                    player.groundState = new Player.GroundState(
                        true, Player.Orientation.RightWall, 90, false, false
                    );
                    player.groundspeed = this.red ? -5 / 16f : -4 / 16f;
                    player.position.x = this.position.x;
                    if (player.position.y > this.position.y - 1)
                        player.position.y = this.position.y - 1;
                    resolvedByWallRunning = true;
                }
            }
        }

        if (resolvedByWallRunning == false)
        {
            var offset = player.position - (Vector2)this.transform.position;

            if (player.state == Player.State.WallSlide ||
                player.HasWallOnSide(player.facingRight, 0.5f))
            {
                player.facingRight = !player.facingRight;
                player.velocity.x =
                    Mathf.Abs(player.velocity.x) * (player.facingRight ? 1 : -1);
            }

            if (this.def.tile.rotation != 180 || this.diagonal == true)
            {
                player.state = Player.State.Spring;
            }
            player.groundState = Player.GroundState.Airborne(player);
            player.position += player.velocity * 0.3f;

            {
                const int Duration = 10;

                Vector2 parallel = player.velocity.normalized;
                Vector2 perpendicular = new Vector2(parallel.y, -parallel.x);
                // float perpendicularAmount = Vector2.Dot(-offset, perpendicular);
                // float ideal = Util.Slide(perpendicularAmount, 0, 5);
                // player.position += (ideal - perpendicularAmount) * perpendicular;
                Vector2 correction = perpendicular * Vector2.Dot(-offset, perpendicular);

                player.velocity += correction / Duration;
                this.correction = -correction / Duration;
                this.correctionTime = Duration;
            }
        }
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox();
    }
}
