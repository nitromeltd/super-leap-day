﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartBarrier : Item
{
    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-7, 7, 0, 1, this.rotation, Hitbox.SolidOnTop)
        };
    }
}
