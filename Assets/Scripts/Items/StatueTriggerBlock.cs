
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System.Linq;

public class StatueTriggerBlock : Item
{
    private StatueTriggerBlock[,] grid;
    private List<StatueTriggerBlock> triggerBlocks;
    protected static int Size = Tile.Size;
    private bool isFlipped = false;
    private Sprite lastSprite = null;
    private bool spriteSet = false;
    public StatueTriggerBlock leader;
    public Neighbours myNeighbours;
    private NitromeEditor.Path path;
    private NitromeEditor.Path.Node nearestNode;
    private Vector2 pathOffset;
    private float timeAtStart;
    private int gridOriginX;
    private int gridOriginY;
    private Vector2 startingPosition;
    private Vector2 originalPosition;
    public SpriteRenderer backSpriteRenderer;
    public bool activated;
    private bool currentlyActivated;
    private int activationTimer;

    [System.Serializable]
    public struct Neighbours
    {
        public bool top;
        public bool bottom;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool topLeft;
        public bool topRight;
        public bool bottomLeft;
        public bool bottomRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool topLeft, bool topRight, bool bottomLeft, bool bottomRight)
        {
            this.top = up;
            this.bottom = down;
            this.left = left;
            this.right = right;
            this.topLeft = topLeft;
            this.topRight = topRight;
            this.bottomLeft = bottomLeft;
            this.bottomRight = bottomRight;
        }

        public bool IsEqual(Neighbours n, bool useCorners = false)
        {
            bool nonCornersEqual =
                this.top == n.top &&
                this.bottom == n.bottom &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual =
                this.topLeft == n.topLeft &&
                this.topRight == n.topRight &&
                this.bottomLeft == n.bottomLeft &&
                this.bottomRight == n.bottomRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);
        this.isFlipped = def.tile.flip;
        this.transform.localScale = new Vector3(this.isFlipped ? -1 : 1, 1, 1);
        this.startingPosition = this.originalPosition = this.position;
        if (this.isFlipped == false)
        {
            this.hitboxes = new[] {
                new Hitbox(0, Size, 0, Size, this.rotation, Hitbox.Solid, wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
            };
        }
        else
        {
            this.hitboxes = new[] {
                new Hitbox(-Size, 0, 0, Size, this.rotation, Hitbox.Solid, wallSlideAllowedOnLeft: true, wallSlideAllowedOnRight: true)
            };
        }

        this.nearestNode = this.chunk.pathLayer.NearestNodeTo(
            new Vector2(
                this.transform.position.x,
                this.transform.position.y
            ),
            Size
        );
        this.path = this.nearestNode?.path;

        // group
        int xMin = this.chunk.xMin - this.chunk.boundaryMarkers.bottomLeft.x;
        int yMin = this.chunk.yMin - this.chunk.boundaryMarkers.bottomLeft.y;

        int tx = def.tx - xMin;
        int ty = def.ty - yMin;
        
        GroupLayer.Group g = this.chunk.groupLayer.groupForCell[tx, ty];

        if(g.leader == null)
        {
            this.leader = this;
            InitLeader();
            SetupPath(this);
        }
        else
        {
            if(!(g.leader is StatueTriggerBlock))
            {
                this.leader = this;
            }
            AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        }
    }

    override public void Advance()
    {
        CheckCollisionWithPlayer();
        if(this.leader == this)
        {
            AdvanceLeader();
        }

        if(this.leader != null && this.leader.activated != this.currentlyActivated)
        {
            this.currentlyActivated = this.leader.activated;
            ApplyAutotiling();
        }
    }

    public void ApplyAutotiling()
    {
        void SetSprite(Sprite sprite)
        {
            spriteRenderer.sprite = sprite;
        }

        Sprite Select(Sprite[] sprites)
        {
            return sprites[Mathf.Abs((this.def.tx + this.def.ty) % sprites.Length)];
        }

        Sprite SelectInterior(Tileset tileset, int mapTx, int mapTy)
        {
            int cx = mapTx % tileset.interiorGridColumns;
            int cy = mapTy % tileset.interiorGridRows;
            if (cx < 0) cx += tileset.interiorGridColumns;
            if (cy < 0) cy += tileset.interiorGridRows;
            cy = (tileset.interiorGridRows - 1) - cy;
            return tileset.interior[
                cx + (cy * tileset.interiorGridColumns)
            ];
        }
        this.currentlyActivated = this.leader.activated;
        var tiles = (this.currentlyActivated ? Assets.instance.tombstoneHill.activeHaunteStatueBlocks 
                                    : Assets.instance.tombstoneHill.inactiveHaunteStatueBlocks);

        bool l, r, t, b, tl, tr, bl, br;

        if(this.isFlipped)
        {
            l = myNeighbours.right;
            r = myNeighbours.left;
            t = myNeighbours.top;
            b = myNeighbours.bottom;
            tl = myNeighbours.topRight;
            tr = myNeighbours.topLeft;
            bl = myNeighbours.bottomRight;
            br = myNeighbours.bottomLeft;
        }
        else
        {
            l = myNeighbours.left;
            r = myNeighbours.right;
            t = myNeighbours.top;
            b = myNeighbours.bottom;
            tl = myNeighbours.topLeft;
            tr = myNeighbours.topRight;
            bl = myNeighbours.bottomLeft;
            br = myNeighbours.bottomRight;
        }

        bool allSides = l && r && t && b;
        bool allCorners = tl && tr && bl && br;

        // if (this.debug) Debug.Log($"t:{t} b:{b} l:{l} r:{r} tl:{tl} tr:{tr} bl:{bl} br:{br}");

        if(allSides == true && allCorners == true) SetSprite(null);
        else if( t == false && b == false && l == false && r == false) SetSprite(Select(tiles.singleSquare));
        else if( t == false && b == false && l == false && r == true && tl == false && bl == false) SetSprite(tiles.singleRowLeft);
        else if( t == false && b == false && l == true && r == true && (tl == true || bl == true)) SetSprite(tiles.singleRowLeft);
        else if( t == false && b == false && l == true && r == false ) SetSprite(tiles.singleRowRight);
        else if( t == false && b == false && l == true && r == true && (tr == true || br == true)) SetSprite(tiles.singleRowRight);
        else if( t == false && b == false && l == true && r == true && allCorners == false) SetSprite(Select(tiles.singleRowMiddle));

        else if( l == false && r == false && t == false && b == true && tl == false && tr == false) SetSprite(tiles.pillar1WideTop);
        else if( l == false && r == false && t == true && b == true && (tl == true || tr == true)) SetSprite(tiles.pillar1WideTop);
        else if( l == false && r == false && t == true && b == false ) SetSprite(tiles.pillar1WideBottom);
        else if( l == false && r == false && t == true && b == true && (bl == true || br == true)) SetSprite(tiles.pillar1WideBottom);
        else if( l == false && r == false && t == true && b == true && allCorners == false) SetSprite(Select(tiles.pillar1WideMiddle));

        else if( l == false && r == true && t == true && b == true && tr == false) SetSprite(tiles.topLeft);
        else if( l == true && r == true && t == false && b == true && bl == false) SetSprite(tiles.topLeft);
        else if( l == true && r == false && t == true && b == true && tl == false) SetSprite(tiles.topRight);
        else if( l == true && r == true && t == false && b == true && br == false) SetSprite(tiles.topRight);
        else if( l == true && r == false && t == true && b == true && tl == false) SetSprite(tiles.bottomRight);
        else if( l == true && r == true && t == true && b == false && tr == false) SetSprite(tiles.bottomRight);

        else if( l == true && r == false && t == false && b == true ) SetSprite(tiles.topRight);
        else if( l == false && r == true && t == false && b == true ) SetSprite(tiles.topLeft);
        else if( l == false && r == true && t == true && b == false ) SetSprite(tiles.bottomLeft);
        else if( l == true && r == false && t == true && b == false ) SetSprite(tiles.bottomRight);

        else if( allSides == true && tl == false && tr == false && bl == true && br == false) SetSprite(tiles.topRight);
        else if( allSides == true && tl == false && tr == false && bl == false && br == true) SetSprite(tiles.topLeft);
        else if( bl == false && br == false && tl == true && tr == false) SetSprite(tiles.bottomRight);
        else if( bl == false && br == false && tl == false && tr == true) SetSprite(tiles.bottomLeft);

        else if( allSides == true && tl == false && tr == false && bl == true && br == true) SetSprite(Select(tiles.top));
        else if( allSides == true && tr == false && br == false && tl == true && bl == true) SetSprite(Select(tiles.right));
        else if( allSides == true && tl == false && bl == false && tr == true && br == true) SetSprite(Select(tiles.left));
        else if( allSides == true && bl == false && br == false && tl == true && tr == true) SetSprite(Select(tiles.bottom));

        else if( l == false && r == true && t == true && b == true ) SetSprite(Select(tiles.left));
        else if( l == true && r == false && t == true && b == true ) SetSprite(Select(tiles.right));
        else if( l == true && r == true && t == false && b == true ) SetSprite(Select(tiles.top));
        else if( l == true && r == true && t == true && b == false ) SetSprite(Select(tiles.bottom));

        else if( l == false && r == true && t == false && b == true && tl == false ) SetSprite(tiles.topLeft);
        else if( l == true && r == false && t == false && b == true && tl == false ) SetSprite(tiles.topRight);
        else if( l == false && r == true && t == true && b == false && bl == false ) SetSprite(tiles.bottomLeft);
        else if( l == true && r == false && t == true && b == false && bl == false ) SetSprite(tiles.bottomRight);

        else if( l == true && r == true && t == true && b == true && tl == false ) SetSprite(tiles.insideTopLeft);
        else if( l == true && r == true && t == true && b == true && tr == false ) SetSprite(tiles.insideTopRight);
        else if( l == true && r == true && t == true && b == true && bl == false ) SetSprite(tiles.insideBottomLeft);
        else if( l == true && r == true && t == true && b == true && br == false ) SetSprite(tiles.insideBottomRight);

        this.backSpriteRenderer.sprite = SelectInterior(Assets.instance.tombstoneHill.inactiveHaunteStatueBlocks, this.def.tx, this.def.ty);
        if (this.spriteSet == false)
        {
            this.lastSprite = spriteRenderer.sprite;
        }
        this.spriteSet = true;
    }

    private void InitLeader()
    {
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        if(this.group != null)
        {
            SortingGroup sg = this.group.container.GetComponent<SortingGroup>();

            if(sg != null)
            {
                sg.sortingLayerName = "Player (AL)";
                sg.sortingOrder = 1;
            }
        }
    }

    private void SetupPath(StatueTriggerBlock triggerBlock)
    {
        this.path = triggerBlock.path;

        if(this.path != null)
        {
            Vector2 localPathOffset = triggerBlock.position - triggerBlock.nearestNode.Position;
            Vector2 localPosition = triggerBlock.position - localPathOffset;
            this.timeAtStart = this.path.NearestPointTo(triggerBlock.position)?.timeSeconds ?? 0;
            this.pathOffset = this.position - localPosition;
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
    }

    private Vector2 GroupBottomLeft = new Vector2(Mathf.Infinity, Mathf.Infinity);
    private Vector2 GroupTopRight = new Vector2(Mathf.NegativeInfinity, Mathf.NegativeInfinity);
    private void AdvanceLeader()
    {
        if(this.grid == null)
        {
            this.triggerBlocks = new List<StatueTriggerBlock>();

            Item triggerBlockLeader = this.group.leader;
            if (triggerBlockLeader is StatueTriggerBlock)
            {
                triggerBlocks.Add(this.group.leader as StatueTriggerBlock);
            }

            foreach(var f in this.group.followers)
            {
                if(f.Key.item is StatueTriggerBlock)
                {
                    StatueTriggerBlock triggerBlockFollower = f.Key.item as StatueTriggerBlock;
                    triggerBlockFollower.leader = this;
                    if (triggerBlockFollower != null)
                    {
                        triggerBlocks.Add(triggerBlockFollower);
                    }
                }
            }

            SetupAutotiling();

            if(this.path == null)
            {
                FindPathInFollowers();
            }
        }

        if (this.group.leader is StatueTriggerBlock)
        {
            if (this.path != null)
            {
                float TimeForFrame(int frameNumber) =>
                    this.timeAtStart + (frameNumber / 60.0f);

                var time = TimeForFrame(this.map.frameNumber) % this.path.duration;
                this.position = this.path.PointAtTime(time).position + this.pathOffset;
            }
            this.transform.position = this.position;
            foreach (var f in this.group.followers)
            {
                if (f.Key.item is StatueTriggerBlock == false) continue;

                f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
                f.Key.item.transform.position = f.Key.item.position;
            }
        }
    }

    private void SetupAutotiling()
    {
        if(this.triggerBlocks.Count <= 0)
        {
            Debug.Log("Rumble Ground Block group is missing blocks!");
            return;
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(this.triggerBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(this.triggerBlocks.Min(t => t.position.y));

        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(this.triggerBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(this.triggerBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new StatueTriggerBlock[sizeX, sizeY];

        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;

        gridOriginX = minX;
        gridOriginY = minY;

        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in this.triggerBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGridAndSurroundingArea(x, y + 1);
                bool down = IsItemInGridAndSurroundingArea(x, y - 1);
                bool left = IsItemInGridAndSurroundingArea(x - 1, y);
                bool right = IsItemInGridAndSurroundingArea(x + 1, y);

                bool upLeft = IsItemInGridAndSurroundingArea(x - 1, y + 1);
                bool upRight = IsItemInGridAndSurroundingArea(x + 1, y + 1);
                bool downLeft = IsItemInGridAndSurroundingArea(x - 1, y - 1);
                bool downRight = IsItemInGridAndSurroundingArea(x + 1, y - 1);

                // Debug.Log($"({x},{y}): u:{up},d:{down},l:{left},r:{right},ul:{upLeft},ur:{upRight},dl:{downLeft},dr:{downRight}");

                this.grid[x, y].myNeighbours = new StatueTriggerBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }
        foreach(var t in this.triggerBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    public bool IsItemInGrid(int mapX, int mapY)
    {
        if(this.grid == null) return false;

        var delta = this.originalPosition - this.startingPosition;
        int tx = this.gridOriginX + Mathf.RoundToInt(delta.x);
        int ty = this.gridOriginY + Mathf.RoundToInt(delta.y);

        int x = mapX - tx;
        int y = mapY - ty;


        if(x < 0 || x >= this.grid.GetLength(0))
            return false;
        if(y < 0 || y >= this.grid.GetLength(1))
            return false;

        return this.grid[x, y] != null;
    }

    private bool IsItemInGridAndSurroundingArea(int x, int y)
    {
        if( x < 0 || x >= this.grid.GetLength(0)
            || y < 0 || y >= this.grid.GetLength(1) )
        {
            var delta = this.originalPosition - this.startingPosition;
            int dx = this.gridOriginX + x + Mathf.RoundToInt(delta.x);
            int dy = this.gridOriginY + y + Mathf.RoundToInt(delta.y);

            foreach(RumbleGround rg  in this.chunk.subsetOfItemsOfTypeRumbleGround)
            {
                if(rg == this) continue;
                if(rg.IsItemInGrid(dx,dy))
                    return true;
            }
            return false;
        }

        return this.grid[x, y] != null;
    }

    private void FindPathInFollowers()
    {
        foreach(var triggerBlock in this.triggerBlocks)
        {
            if(triggerBlock.path == null) continue;

            SetupPath(triggerBlock);
        }
    }

    private void CheckCollisionWithPlayer()
    {
        bool ItemIsStatueTriggerBlockLeader(Item item)
        {
            if(item is StatueTriggerBlock && (item as StatueTriggerBlock).GetLeader() == item)
            {
                return true;
            }

            return false;
        }

        var ownRectangle = this.hitboxes[0].InWorldSpace().Inflate(0.15f);
        var playerRectangle = this.map.player.Rectangle();
        if (ownRectangle.Overlaps(playerRectangle) == true)
        {
            foreach(HauntedStatue hs in this.chunk.subsetOfItemsOfTypeHauntedStatue)
            {
                hs.TriggerStatue();
            }

            if (this.leader.activated == false)
            {
                List<StatueTriggerBlock> subset = new List<StatueTriggerBlock>();
                subset.AddRange(this.chunk.items.OfType<StatueTriggerBlock>().Where(ItemIsStatueTriggerBlockLeader));

                foreach (var l in subset)
                {
                    l.SetActivated(true);
                }

                Audio.instance.PlaySfx(
                    Assets.instance.tombstoneHill.sfxMagicGround, null, this.position
                );
            }

            ApplyAutotiling();
        }
    }

    public void SetActivated(bool value)
    {
        this.activated = value;
    }

    public StatueTriggerBlock GetLeader()
    {
        return this.leader;
    }
}