
using UnityEngine;
using UnityEngine.Rendering;
using System.Collections.Generic;
using System.Linq;

public class StickyBlock : Item
{
    [Header("Sprites")]
    public Sprite[] sprites;
    public Autotile[] autotiles;
    public Neighbours myNeighbours;
    public GameObject texturePrefab;
    
    [Header("Connector")]
    public Animated.Animation[] connectorAnimationOutlineBottom;
    public Animated.Animation[] connectorAnimationFillBottom;
    public Animated.Animation[] connectorAnimationOutlineSide;
    public Animated.Animation[] connectorAnimationFillSide;
    public Animated.Animation[] connectorAnimationOutlineTop;
    public Animated.Animation[] connectorAnimationFillTop;
    
    [Header("Particles")]
    public Sprite[] stickyDebris;

    private Animated connectorOutline;
    private Animated connectorFill;
    private int size;
    private StickyBlock leader;

    // leader variables
    private NitromeEditor.Path path;
    private NitromeEditor.Path.Node nearestNode;
    private Vector2 pathOffset;
    private float timeAtStart;
    private StickyBlock[,] grid;
    private bool printError;
    private List<StickyBlock> stickyBlocks;
    private SpriteRenderer textureRenderer;

    protected static int Size = Tile.Size;
    protected static float HalfSize = Size / 2f;

    public bool IsLeader => this.leader == this;

    [System.Serializable]
    public struct Autotile
    {
        public Sprite[] sprites;
        public bool useCorners;
        public Neighbours neighbours;
    }

    [System.Serializable]
    public struct Neighbours
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool upLeft;
        public bool upRight;
        public bool downLeft;
        public bool downRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool upLeft, bool upRight, bool downLeft, bool downRight)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
            this.upLeft = upLeft;
            this.upRight = upRight;
            this.downLeft = downLeft;
            this.downRight = downRight;
        }
        
        public bool IsEqual(Neighbours n, bool useCorners = false) 
        {
            bool nonCornersEqual = 
                this.up == n.up &&
                this.down == n.down &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual = 
                this.upLeft == n.upLeft &&
                this.upRight == n.upRight &&
                this.downLeft == n.downLeft &&
                this.downRight == n.downRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public class StuckData
    {
        public StickyBlock stickyBlock;
        public Vector2 relativePositionToBlock;
        public bool belowBlock;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.size = Tile.Size;

        this.hitboxes = new [] {
            new Hitbox(-HalfSize, HalfSize, -HalfSize, HalfSize, this.rotation, Hitbox.Solid)
        };
        this.connectorOutline =
            this.transform.Find("Connector Outline").GetComponent<Animated>();
        this.connectorFill =
            this.transform.Find("Connector Fill").GetComponent<Animated>();

        // path
        this.nearestNode = this.chunk.pathLayer.NearestNodeTo(
            new Vector2(
                this.transform.position.x,
                this.transform.position.y
            ),
            Size
        ); 
        this.path = this.nearestNode?.path;

        // group
        int xMin = this.chunk.xMin - this.chunk.boundaryMarkers.bottomLeft.x;
        int yMin = this.chunk.yMin - this.chunk.boundaryMarkers.bottomLeft.y;

        int tx = def.tx - xMin;
        int ty = def.ty - yMin;
        
        GroupLayer.Group g = this.chunk.groupLayer.groupForCell[tx, ty];

        if(g.leader == null)
        {
            this.leader = this;
            InitLeader();
            SetupPath(this);
        }
        else
        {
            if(!(g.leader is StickyBlock))
            {
                this.leader = this;
            }
            AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        }

        // collisions
        this.onCollisionFromAbove = delegate { OnCollision(false, false, false); };
        this.onCollisionFromBelow = delegate { OnCollision(false, false, true ); };
        this.onCollisionFromLeft  = delegate { OnCollision(true,  false, false); };
        this.onCollisionFromRight = delegate { OnCollision(false, true,  false); };
    }

    private void InitLeader()
    {
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        if(this.group != null)
        {
            SortingGroup sg = this.group.container.GetComponent<SortingGroup>();

            if(sg != null)
            {
                sg.sortingLayerName = "Player (AL)";
                sg.sortingOrder = 1;
            }
        }
    }

    override public void Advance()
    {
        if(IsLeader == true)
        {
            AdvanceLeader();
        }
    }

    private Vector2 GroupBottomLeft = new Vector2(Mathf.Infinity, Mathf.Infinity);
    private Vector2 GroupTopRight = new Vector2(Mathf.NegativeInfinity, Mathf.NegativeInfinity);

    private void AdvanceLeader()
    {
        if(this.grid == null)
        {
            void CheckGroupBounds(Item stickyBlock)
            {
                if(stickyBlock.position.x < GroupBottomLeft.x)
                {
                    GroupBottomLeft.x = stickyBlock.position.x;
                }

                if(stickyBlock.position.y < GroupBottomLeft.y)
                {
                    GroupBottomLeft.y = stickyBlock.position.y;
                }

                if(stickyBlock.position.x > GroupTopRight.x)
                {
                    GroupTopRight.x = stickyBlock.position.x;
                }

                if(stickyBlock.position.y > GroupTopRight.y)
                {
                    GroupTopRight.y = stickyBlock.position.y;
                }
            }

            this.stickyBlocks = new List<StickyBlock>();

            Item stickyBlockLeader = this.group.leader;
            if (stickyBlockLeader is StickyBlock)
            {
                stickyBlocks.Add(this.group.leader as StickyBlock);
                CheckGroupBounds(stickyBlockLeader);
            }

            foreach(var f in this.group.followers)
            {
                if(f.Key.item is StickyBlock)
                {
                    StickyBlock stickyBlockFollower = f.Key.item as StickyBlock;
                    if (stickyBlockFollower != null)
                    {
                        stickyBlocks.Add(stickyBlockFollower);
                    }
                    CheckGroupBounds(stickyBlockFollower as Item);
                }
            }

            SetupAutotiling();
            
            if(this.path == null)
            {
                FindPathInFollowers();
            }
        }

        if(this.textureRenderer == null)
        {
            GameObject textureGO = Instantiate(this.texturePrefab);
            this.textureRenderer = textureGO.GetComponent<SpriteRenderer>();

            textureGO.transform.SetParent(this.transform);
            textureGO.layer = this.map.Layer();

            int width = Mathf.RoundToInt(GroupTopRight.x - GroupBottomLeft.x) + 1;
            int height = Mathf.RoundToInt(GroupTopRight.y - GroupBottomLeft.y) + 1;

            Vector2 size = new Vector2(width, height);
            this.textureRenderer.size = size;

            Vector2 center = (GroupBottomLeft - Vector2.one * 0.50f) + (size / 2f);
            textureGO.transform.position = center;
        }

        if (this.group.leader is StickyBlock)
        {
            if (this.path != null)
            {
                float TimeForFrame(int frameNumber) =>
                    this.timeAtStart + (frameNumber / 60.0f);

                var time = TimeForFrame(this.map.frameNumber) % this.path.duration;
                this.position = this.path.PointAtTime(time).position + this.pathOffset;
            }

            this.transform.position = this.position;

            foreach (var f in this.group.followers)
            {
                if (f.Key.item is StickyBlock == false) continue;

                f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
                f.Key.item.transform.position = f.Key.item.position;
            }
        }
    }

    private void OnCollision(bool left, bool right, bool below)
    {
        if(this.map.frameNumber < this.map.player.lastStickyFrame + 2) return;

        if (this.map.player.state != Player.State.StuckToSticky)
        {
            Animated.Animation[] GetAnimationOutline()
            {
                float hDelta = this.map.player.position.x - this.position.x;
                float vDelta = this.map.player.position.y - this.position.y;

                float hDeltaAbs = Mathf.Abs(hDelta);
                float vDeltaAbs = Mathf.Abs(vDelta);

                if(hDeltaAbs > vDeltaAbs)
                {
                    this.connectorOutline.transform.localScale = new Vector3(
                        hDelta < 0f ? 1f : -1f, 1f, 1f
                    );

                    return this.connectorAnimationOutlineSide;
                }
                else
                {
                    return vDelta > 0f ?
                        this.connectorAnimationOutlineTop : this.connectorAnimationOutlineBottom;
                }
            }
            
            Animated.Animation[] GetAnimationFill()
            {
                float hDelta = this.map.player.position.x - this.position.x;
                float vDelta = this.map.player.position.y - this.position.y;

                float hDeltaAbs = Mathf.Abs(hDelta);
                float vDeltaAbs = Mathf.Abs(vDelta);

                if(hDeltaAbs > vDeltaAbs)
                {
                    this.connectorFill.transform.localScale = new Vector3(
                        hDelta < 0f ? 1f : -1f, 1f, 1f
                    );

                    return this.connectorAnimationFillSide;
                }
                else
                {
                    return vDelta > 0f ?
                        this.connectorAnimationFillTop : this.connectorAnimationFillBottom;
                }
            }

            this.map.player.StickToSticky(this, left, right, below);

            var pxLeft = this.position.x - HalfSize;
            var pyBtm  = this.position.y - HalfSize;
            var pos = this.map.player.position;
            pos.x = Mathf.Clamp(pos.x, pxLeft, pxLeft + this.size);
            pos.y = Mathf.Clamp(pos.y, pyBtm,  pyBtm + this.size);

            this.connectorFill.transform.position =
                this.connectorOutline.transform.position = pos;

            Animated.Animation[] outlines = GetAnimationOutline();
            Animated.Animation[] fills = GetAnimationFill();
            int randomIndex = Random.Range(0, outlines.Length);
            this.connectorOutline.PlayOnce(outlines[randomIndex]);
            this.connectorFill.PlayOnce(fills[randomIndex]);

            this.connectorOutline.gameObject.SetActive(true);
            this.connectorFill.gameObject.SetActive(true);

            for (var n = 0; n < 4; n += 1)
            {
                var particle = Particle.CreateWithSprite(
                    this.stickyDebris[Random.Range(0, this.stickyDebris.Length)],
                    100,
                    pos,
                    this.transform.parent
                );
                particle.velocity = new Vector2(
                    UnityEngine.Random.Range(-1, 1),
                    UnityEngine.Random.Range(4, 8)
                ) / 16;
                particle.acceleration = new Vector2(0, -0.6f) / 16;
            }
        }
    }

    public void NotifyPlayerJumpedAway()
    {
        this.connectorOutline.gameObject.SetActive(false);
        this.connectorFill.gameObject.SetActive(false);
    }
    
    public void ApplyAutotiling()
    {
        foreach(StickyBlock.Autotile at in autotiles)
        {
            if(myNeighbours.IsEqual(at.neighbours, at.useCorners))
            {
                spriteRenderer.sprite =
                    at.sprites[Random.Range(0, at.sprites.Length)];
            }
        }
    }
    
    private void SetupAutotiling()
    {
        bool IsItemInGrid(int x, int y)
        {
            if(x < 0 || x >= this.grid.GetLength(0)) return false;
            if(y < 0 || y >= this.grid.GetLength(1)) return false;

            return this.grid[x, y] != null;
        }

        if(this.stickyBlocks.Count <= 0)
        {
            if(this.printError == false)
            {
                this.printError = true;
                this.chunk.ReportError(this.position, "Sticky Block group is missing blocks!");
            }

            return;
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(this.stickyBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(this.stickyBlocks.Min(t => t.position.y));
        
        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(this.stickyBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(this.stickyBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new StickyBlock[sizeX, sizeY];

        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;
        
        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in this.stickyBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGrid(x, y + 1);
                bool down = IsItemInGrid(x, y - 1);
                bool left = IsItemInGrid(x - 1, y);
                bool right = IsItemInGrid(x + 1, y);

                bool upLeft = IsItemInGrid(x - 1, y + 1);
                bool upRight = IsItemInGrid(x + 1, y + 1);
                bool downLeft = IsItemInGrid(x - 1, y - 1);
                bool downRight = IsItemInGrid(x + 1, y - 1);

                this.grid[x, y].myNeighbours = new StickyBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }

        // choose autotile sprite
        foreach(var t in this.stickyBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    private void FindPathInFollowers()
    {
        foreach(var stickyBlock in this.stickyBlocks)
        {
            if(stickyBlock.path == null) continue;

            SetupPath(stickyBlock);
        }
    }

    private void SetupPath(StickyBlock stickyBlock)
    {
        this.path = stickyBlock.path;

        if(this.path != null)
        {
            Vector2 localPathOffset =
                stickyBlock.position - stickyBlock.nearestNode.Position;
            Vector2 localPosition = stickyBlock.position - localPathOffset;

            this.timeAtStart = this.path.NearestPointTo(stickyBlock.position)?.timeSeconds ?? 0;
            this.pathOffset = this.position - localPosition;
            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
    }
}
