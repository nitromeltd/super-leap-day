﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SwingingAxe : Item
{
    public Animated.Animation axeTurnRight;
    public Animated.Animation axeTurnLeft;

    public Transform anchor;
    public Transform firstLink;
    public Transform axe;

    public AnimationCurve swingCurve;

    private Vector2 anchorPosition;
    private float angleRadiansStart;
    private float angleRadiansEnd;
    private float distance;

    private Transform[] links;

    private Rect axeRect;

    private Animated axeAnimated;
    private bool shouldTurnLeft = false;

    private const float SWING_SPEED = 150;
    private const float ANCHOR_POINT_SIZE = .5f;
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty);

        var node = this.chunk.connectionLayer.NearestNodeTo(this.position);
        var index = Array.IndexOf(node.path.nodes, node);
        var otherIndex = (index == 0) ? 1 : 0;
        var otherNode = node.path.nodes[otherIndex];
        this.anchorPosition = otherNode.Position;

        var delta = this.position - this.anchorPosition;
        this.angleRadiansStart = Mathf.Atan2(delta.y, delta.x);
        this.angleRadiansEnd = Mathf.PI - this.angleRadiansStart;
        this.distance = delta.magnitude;

        while (this.angleRadiansEnd < this.angleRadiansStart - Mathf.PI)
            this.angleRadiansEnd += Mathf.PI * 2;
        while (this.angleRadiansEnd > this.angleRadiansStart + Mathf.PI)
            this.angleRadiansEnd -= Mathf.PI * 2;

        var linkCount = 1 + Mathf.FloorToInt(this.distance / 1.5f);
        this.links = new Transform[linkCount];

        this.links[0] = this.firstLink;
        this.axeAnimated = axe.GetComponent<Animated>();

        for (var n = 1; n < linkCount; n += 1)
        {
            this.links[n] = Instantiate(
                this.links[0].gameObject, this.transform).transform;
        }

        Hitbox anchorSolid = new Hitbox(
            anchorPosition.x - ANCHOR_POINT_SIZE,
            anchorPosition.x + ANCHOR_POINT_SIZE,
            anchorPosition.y - ANCHOR_POINT_SIZE,
            anchorPosition.y + ANCHOR_POINT_SIZE,
            this.rotation,
            Hitbox.Solid,
            true, true,
            positionType: Hitbox.PositionType.Absolute);

        // axe collision bounds
        float x = 0f;
        float y = -.25f;
        float width = 2.4f;
        float height = 1f;

        Hitbox axeSolid = new Hitbox(x - (width/2f), x + (width/2f), 
            y - (height/2f), y + (height/2f), this.rotation, Hitbox.NonSolid);
        this.axeRect = new Rect(x, y, width, height);

        this.hitboxes = new [] { axeSolid, anchorSolid };
    }

    public override void Advance()
    {
        // rotation movement
        var theta = (this.map.frameNumber % SWING_SPEED) * (Util.Tau / SWING_SPEED);
        var s = 0.5f + 0.5f * Mathf.Cos(theta);
        var angle = Mathf.Lerp(
            this.angleRadiansStart,
            this.angleRadiansEnd,
            this.swingCurve.Evaluate(s)
        );

        // trigger animations at curve's end points
        if(this.shouldTurnLeft && s < .15f)
        {
            this.shouldTurnLeft = false;
            this.axeAnimated.PlayOnce(this.axeTurnLeft);
            Audio.instance.PlaySfx(Assets.instance.sfxSwingingAxe2, position: this.position);
        }

        if(!shouldTurnLeft && s > .85f)
        {
            this.shouldTurnLeft = true;
            this.axeAnimated.PlayOnce(this.axeTurnRight);
            Audio.instance.PlaySfx(Assets.instance.sfxSwingingAxe1, position: this.position);
        }

        // rotate axe sprite
        var angleDeg = angle * Mathf.Rad2Deg;
        this.axe.rotation = Quaternion.Euler(Vector3.forward * (angleDeg + 90f));

        // update positions
        var p = new Vector2(
            this.anchorPosition.x + (this.distance * Mathf.Cos(angle)),
            this.anchorPosition.y + (this.distance * Mathf.Sin(angle))
        );
        this.anchor.position = this.anchorPosition;
        this.axe.position = p;
        this.position = p;

        Vector2 top    = this.anchorPosition;
        Vector2 bottom = this.position;
        float divisor = this.links.Length + 2.2f;

        for (var n = 0; n < this.links.Length; n += 1)
        {
            var along = (float)(n + 1.5f) / (float)divisor;
            var linkPos = Vector2.Lerp(top, bottom, along);
            this.links[n].position = linkPos;
        }

        // check collision with player
        var player = this.map.player;
        var playerRectangle = player.Rectangle();
        
        if (player.ShouldCollideWithItems() == true &&
            this.hitboxes[0].InWorldSpace().Overlaps(playerRectangle))
        {
            player.Hit(this.position);
        }
    }

}
