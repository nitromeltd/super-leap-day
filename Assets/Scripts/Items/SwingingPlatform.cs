using System.Linq;
using UnityEngine;

public class SwingingPlatform : Item
{
    public int tilesWide;
    public int tilesHigh;
    public Sprite spriteChainLink;

    private SwingingPlatformAnchor anchor;
    private Vector2 startRelativeToAnchor;
    private float angleRadiansStart;
    private float angleRadiansEnd;
    private float distance;
    private float speed;
    private SpriteRenderer[] links;

    public override void Init(Def def)
    {
        base.Init(def);

        var dx = this.tilesWide / 2f;
        var dy = this.tilesHigh / 2f;
        this.hitboxes = new []
        {
            new Hitbox(-dx, dx, -dy, dy, 0, Hitbox.Solid, true, true)
        };

        FindConnection();
        if (this.anchor == null) return;

        this.angleRadiansStart = Mathf.Atan2(
            this.startRelativeToAnchor.y, this.startRelativeToAnchor.x
        );
        this.angleRadiansEnd = Mathf.PI - this.angleRadiansStart;
        this.distance = this.startRelativeToAnchor.magnitude;

        while (this.angleRadiansEnd < this.angleRadiansStart - Mathf.PI)
            this.angleRadiansEnd += Mathf.PI * 2;
        while (this.angleRadiansEnd > this.angleRadiansStart + Mathf.PI)
            this.angleRadiansEnd -= Mathf.PI * 2;

        var count = Mathf.CeilToInt(this.distance * 1.2f);
        this.links = new SpriteRenderer[count];

        for (int n = 0; n < count; n += 1)
        {
            var obj = new GameObject($"Link {n + 1}");
            obj.transform.SetParent(this.transform, false);

            this.links[n] = obj.AddComponent<SpriteRenderer>();
            this.links[n].sprite = this.spriteChainLink;
            this.links[n].sortingLayerName = "Tiles - Back 2";
            this.links[n].sortingOrder = -1;
        }

        if (this.def.tile.properties?.ContainsKey("speed") == true)
            this.speed = this.def.tile.properties["speed"].f;
        else
            this.speed = 1;
    }

    private void FindConnection()
    {
        var connNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 1);
        if (connNode == null) return;

        var otherNode = connNode.path.nodes.First(n => n != connNode);
        if (otherNode == null) return;

        this.anchor =
            this.chunk.NearestItemTo<SwingingPlatformAnchor>(otherNode.Position);
        this.startRelativeToAnchor = this.position - this.anchor.position;
    }

    public override void Advance()
    {
        if (this.anchor == null) return;

        var theta = this.map.frameNumber * this.speed * Mathf.PI * 2 / 480f;
        var s = 0.5f + 0.5f * Mathf.Cos(theta);
        var angleRadians = Mathf.Lerp(
            this.angleRadiansStart,
            this.angleRadiansEnd,
            s
        );
        var direction = new Vector2(
            Mathf.Cos(angleRadians),
            Mathf.Sin(angleRadians)
        );

        this.transform.position = this.position =
            this.anchor.position + (this.distance * direction);

        for (int n = 0; n < this.links.Length; n += 1)
        {
            var linkDistance = (n + 1) * this.distance / (this.links.Length + 1);
            this.links[n].transform.position =
                this.anchor.position + (linkDistance * direction);
        }
    }
}
