﻿using UnityEngine;
using System.Linq;

public class TNTBlock : Item
{
    public Animated.Animation animationNormal;
    public Animated.Animation animationBreak;
    public Animated.Animation animationDebrisLeft;
    public Animated.Animation animationDebrisRight;

    public Animated.Animation animationExplosionParticle;

    public int blockSize;
    
    private bool destroyed;
    private int? timeToDestroyNeighbours;
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.destroyed = false;
        this.timeToDestroyNeighbours = null;

        MakeSolid();

        this.onCollisionFromAbove = (_) => {
            if (this.map.player.gravity.Orientation() == Player.Orientation.Ceiling)
                Break();
        };
        this.onCollisionFromRight = (_) => {
            if (this.map.player.gravity.Orientation() == Player.Orientation.RightWall)
                Break();
        };
        this.onCollisionFromBelow = (_) => {
            if (this.map.player.gravity.Orientation() == Player.Orientation.Normal)
                Break();
        };
        this.onCollisionFromLeft = (_) => {
            if (this.map.player.gravity.Orientation() == Player.Orientation.LeftWall)
                Break();
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void MakeSolid()
    {
        if(this.destroyed == true)
        {
            this.hitboxes = new [] {
                new Hitbox(
                    this.blockSize * -0.5f,
                    this.blockSize * 0.5f,
                    this.blockSize * -0.5f,
                    this.blockSize * 0.5f,
                    this.rotation,
                    Hitbox.NonSolid,
                    true,
                    true
                )
            };
        }
        else
        {
            this.hitboxes = new [] {
                new Hitbox(
                    this.blockSize * -0.5f,
                    this.blockSize * 0.5f,
                    this.blockSize * -0.5f,
                    this.blockSize * 0.5f,
                    this.rotation,
                    Hitbox.Solid,
                    true,
                    true
                )
            };
        }
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        if (this.timeToDestroyNeighbours.HasValue &&
            this.timeToDestroyNeighbours > 0)
        {
            this.timeToDestroyNeighbours -= 1;
            if (this.timeToDestroyNeighbours == 0)
            {
                this.map.Damage(
                    this.hitboxes[0].InWorldSpace().Inflate(.9f),
                    new Enemy.KillInfo { itemDeliveringKill = this }
                );
            }
        }
    }
    
    public void Break()
    {
        if (this.destroyed == true) return;

        var center = this.hitboxes[0].InWorldSpace().center;

        this.destroyed = true;
        MakeSolid();

        Vector2 leftPosition  = this.position.Add(this.blockSize * -0.25f, 0);
        Vector2 rightPosition = this.position.Add(this.blockSize *  0.25f, 0);

        var left = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisLeft, 100, leftPosition, this.transform.parent
        );
        left.velocity     = new Vector2(-1f, 7f) / 16f;
        left.acceleration = new Vector2(0f, -0.5f) / 16f;

        var right = Particle.CreatePlayAndHoldLastFrame(
            this.animationDebrisRight, 100, rightPosition, this.transform.parent
        );
        right.velocity     = new Vector2(1f, 7f) / 16f;
        right.acceleration = new Vector2(0f, -0.5f) / 16f;

        //Particle.CreateDust(this.transform.parent, 5, 7, center, .5f, .5f);
        Particle explosionParticle = Particle.CreateAndPlayOnce(
            animationExplosionParticle, center, this.transform.parent
        );

        if(blockSize == 1)
        {
            explosionParticle.transform.localScale = Vector3.one * 0.5f;
        }

        // create black frame
        Particle.CreatePlayAndHoldLastFrame(this.animationBreak, 5,
            this.position, this.transform.parent);

        this.spriteRenderer.enabled = false;

        this.timeToDestroyNeighbours = 8;

        Audio.instance.PlaySfx(Assets.instance.sfxTNTExplosion, position: this.position);

        this.map.ScreenShakeAtPosition(this.position, Vector2.one * .2f * blockSize);
    }

    public override void Reset()
    {
        if (this.destroyed == true)
        {
            this.destroyed = false;
            this.timeToDestroyNeighbours = null;

            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(this.animationNormal);
            
            Particle.CreateAndPlayOnce(
                Assets.instance.enemyRespawnAnimation,
                this.hitboxes[0].InWorldSpace().center,
                this.transform.parent
            );

            MakeSolid();
        }
    }
}
