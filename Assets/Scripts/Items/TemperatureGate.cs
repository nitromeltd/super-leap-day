﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TemperatureGate : Item
{
    public AnimationCurve moveCurve;
    public Map.Temperature targetTemperature;
    public SpriteRenderer leftBitRenderer;
    public SpriteRenderer rightBitRenderer;
    public Sprite bitSprite;
    private int ignoreTime;
    private bool grabbed;
    private int grabTime;
    private bool switchActive;
    private float initialPositionY;
    private float finalPositionY;
    public TemperatureGate gateLeader;
    public Vector2 relativePosition;
    public TemperatureGateEnd parentGateEnd; //extra

    private const int SwitchTime = 60;

    private bool isGrabbing = false;

    public override void Init(Def def)
    {
        base.Init(def);

        // hitbox
        this.hitboxes = new[] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };
        this.solidToPlayer = false;

        // init variables
        this.ignoreTime = 0;
        this.grabbed = false;
        this.grabTime = 0;
        this.switchActive = false;
        this.initialPositionY = this.transform.position.y;
        this.finalPositionY = this.transform.position.y - 1f;

        this.grabInfo = new GrabInfo(this, GrabType.Ceiling);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.gateLeader == this)
        {
            UpdateLeader();
        }
        else
        {
            UpdateFollower();
        }

        Player player = this.map.player;

        // Force un-grab when gate disappears
        if (gateLeader.switchActive == true && player.grabbingOntoItem == this)
        {
            player.grabbingOntoItem = null;
            player.ResetDoubleJump();
        }
    }

    private void UpdateFollower()
    {
        AdvanceHanging();
        this.transform.position = this.position =
            this.gateLeader.position + this.relativePosition;
    }

    private void UpdateLeader()
    {
        AdvanceHanging();
        AdvanceGrabbed();
        this.transform.position = this.position;
    }

    public bool IsSwitchActive()
    {
        return gateLeader.switchActive;
    }

    public bool IsCurrentTemperatureCorrect()
    {
        return gateLeader.targetTemperature == this.map.currentTemperature;
    }

    public void SetAlpha(float alpha)
    {
        // this.spriteRenderer.color = new Color(
        //     this.spriteRenderer.color.r,
        //     this.spriteRenderer.color.r,
        //     this.spriteRenderer.color.b,
        //     alpha
        // );
    }

    private void RefreshVisuals()
    {
        // fade gate away when switch is active
        // float colorAlpha = gateLeader.switchActive == true ? 0.0f : 1f;
        // this.spriteRenderer.color = new Color(
        //     this.spriteRenderer.color.r,
        //     this.spriteRenderer.color.r,
        //     this.spriteRenderer.color.b,
        //     colorAlpha
        // );
    }

    private void AdvanceHanging()
    {
        var player = this.map.player;

        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null) return;

        this.grabbed = false;

        if (gateLeader.switchActive == true) return;

        if (gateLeader.ignoreTime > 0)
        {
            gateLeader.ignoreTime -= 1;
            return;
        }

        var delta = player.position - this.position;
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            player.ResetDoubleJump();
            player.velocity = Vector2.zero;
            gateLeader.ignoreTime = 15;
            gateLeader.grabbed = true;
            if (!switchActive)
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvPullswitch,
                    position: this.position
                );
                this.isGrabbing = true;
            }
        }
    }

    private void AdvanceGrabbed()
    {
        float timeT = this.moveCurve.Evaluate(Mathf.InverseLerp(0, SwitchTime, this.grabTime));
        float positionY = Mathf.Lerp(this.initialPositionY, this.finalPositionY, timeT);

        gateLeader.position =
            new Vector2(gateLeader.position.x, positionY);

        bool shouldSwitchBeActive =
            gateLeader.targetTemperature == this.map.currentTemperature;

        if (this.grabbed == false)
        {
            gateLeader.grabTime += shouldSwitchBeActive ? 10 : -10;
            gateLeader.switchActive = shouldSwitchBeActive;

            if (gateLeader.grabTime > SwitchTime)
            {
                gateLeader.grabTime = SwitchTime;
                parentGateEnd.DisappearGate();
            }

            if (gateLeader.grabTime < 0)
            {
                gateLeader.grabTime = 0;

                parentGateEnd.AppearGate();
            }

            if (this.isGrabbing)
            {
                this.isGrabbing = false;
                Audio.instance.StopSfx(Assets.instance.sfxEnvPullswitch);
            }
        }
        else
        {
            if (gateLeader.grabTime < SwitchTime)
            {
                gateLeader.grabTime += 1;
            }


            if (gateLeader.grabTime >= SwitchTime)
            {
                Activate();
            }
        }
    }

    private void Activate()
    {
        gateLeader.switchActive = true;
        this.map.ChangeTemperature(gateLeader.targetTemperature);
        // do something here
        this.parentGateEnd.SetCorrectGateVisualState();

        this.isGrabbing = false;
        if (this.targetTemperature == Map.Temperature.Cold)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPullswitchCold,
                position: this.position
            );
        }
        else
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPullswitchHot,
                position: this.position
            );
        }
    }

    public bool IsBeingActivatedByPlayer()
    {
        return gateLeader.grabbed == true && gateLeader.switchActive == false;
    }

    public bool ProgressRight(bool appear = true)
    {
        if (appear)
        {
            if (leftBitRenderer.sprite == null)
            {
                leftBitRenderer.sprite = bitSprite;
                rightBitRenderer.sprite = null;
                return false;
            }
            else if (rightBitRenderer.sprite == null)
            {
                leftBitRenderer.sprite = bitSprite;
                rightBitRenderer.sprite = bitSprite;
                return true;
            }
        }
        else
        {
            if (leftBitRenderer.sprite != null)
            {
                leftBitRenderer.sprite = null;
                rightBitRenderer.sprite = bitSprite;
                return false;
            }
            else if (rightBitRenderer.sprite != null)
            {
                leftBitRenderer.sprite = null;
                rightBitRenderer.sprite = null;
                return true;
            }
        }

        return true;
    }

    public bool ProgressLeft(bool appear = true)
    {
        if (appear)
        {
            if (rightBitRenderer.sprite == null)
            {
                leftBitRenderer.sprite = null;
                rightBitRenderer.sprite = bitSprite;
                return false;
            }
            else if (leftBitRenderer.sprite == null)
            {
                leftBitRenderer.sprite = bitSprite;
                rightBitRenderer.sprite = bitSprite;
                return true;
            }
        }
        else
        {
            if (rightBitRenderer.sprite != null)
            {
                leftBitRenderer.sprite = bitSprite;
                rightBitRenderer.sprite = null;
                return false;
            }
            else if (leftBitRenderer.sprite != null)
            {
                leftBitRenderer.sprite = null;
                rightBitRenderer.sprite = null;
                return true;
            }
        }

        return true;
    }
}
