﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureGateEnd : Item
{
    public Map.Temperature targetTemperature;
    public Transform edgeBitTransform;

    private List<TemperatureGate> gateBits = new List<TemperatureGate>();
    public TemperatureGate gateLeader;
    public TemperatureGateEnd otherGateEnd; //if not null means we are gate end leader

    private enum State
    {
        Idle,
        Appear,
        Disappear,
        Appeared,
        Disappeared
    }

    private State state;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        FindPieces();
        state = State.Idle;

        if (this.gateLeader != null)
        {
            if (this.gateLeader.IsSwitchActive())
            {
                DisappearGate();
            }
            else
            {
                AppearGate();
            }
        }
    }

    private void FindPieces()
    {
        if(this.otherGateEnd != null) return;

        for (int tx = this.def.tx; tx <= this.chunk.xMax; tx++)
        {
            var gate = this.chunk.ItemAt<TemperatureGate>(tx, this.def.ty);
            var gateEnd = this.chunk.ItemAt<TemperatureGateEnd>(tx, this.def.ty);
            if(gateEnd == this) continue;

            if(gate != null)
            {
                this.gateBits.Add(gate);
                gate.gateLeader = this.gateLeader = this.gateBits[0];
                gate.targetTemperature = targetTemperature;
                gate.relativePosition = gate.position - this.gateBits[0].position;
                gate.parentGateEnd = this;
            }

            if(gateEnd != null)
            {
                this.otherGateEnd = gateEnd;
                gateEnd.otherGateEnd = this;
                break;
            }
        }
    }

    public override void Advance()
    {
        if (this.gateLeader != null)
        {
            this.edgeBitTransform.position = new Vector2(
                this.edgeBitTransform.position.x, this.gateLeader.position.y);
        }

        if(this.otherGateEnd.gateLeader != null)
        {
            this.edgeBitTransform.position = new Vector2(
                this.edgeBitTransform.position.x, this.otherGateEnd.gateLeader.position.y);
        }

        // process states

        switch(state)
        {
            case State.Appear:
                AdvanceAppear();
                break;

            case State.Disappear:
                AdvanceDisappear();
                break;

            default:
                break;
        }

 #if UNITY_EDITOR
    // if(Input.GetKeyDown(KeyCode.P))
    // {
    //     DisappearGate();
    // }

    // if(Input.GetKeyDown(KeyCode.O))
    // {
    //     AppearGate();
    // }
 #endif       
    }

    private int leftIndex, rightIndex;

    private void AdvanceAppear()
    {
        bool finishedBit = gateBits[leftIndex].ProgressRight();
        if(leftIndex != rightIndex)
        {
            gateBits[rightIndex].ProgressLeft();
        }

        if(finishedBit)
        {
            leftIndex++;
            rightIndex--;

            if(leftIndex > rightIndex)
            {
                //finished
                state = State.Appeared;
            }
        }
    }

    private void AdvanceDisappear()
    {
        bool finishedBit = gateBits[leftIndex].ProgressLeft(false);
        if(leftIndex != rightIndex)
        {
            gateBits[rightIndex].ProgressRight(false);
        }

        if (finishedBit)
        {
            leftIndex--;
            rightIndex++;

            if (leftIndex < 0 || rightIndex >= gateBits.Count)
            {
                //finished
                state = State.Disappeared;
            }
        }
    }

    public void SetCorrectGateVisualState()
    {
        if(this.gateLeader != null)
        {
            if(this.gateLeader.IsSwitchActive())
            {
                DisappearGate();
            }
            else
            {
                AppearGate();
            }
        }
    }

    public void DisappearGate()
    {
        if(gateBits.Count == 0 || state != State.Appeared)
        {
            return;
        }

        if(gateBits.Count % 2 == 1)
        {
            leftIndex = rightIndex = Mathf.FloorToInt(gateBits.Count / 2);
        }
        else
        {
            leftIndex =  Mathf.FloorToInt(gateBits.Count / 2) - 1;
            rightIndex = leftIndex + 1;
        }

        state = State.Disappear; 
    }

    public void AppearGate()
    {
        if(gateBits.Count == 0 || (state != State.Disappeared && state != State.Idle))
        {
            return;
        }

        leftIndex = 0;
        rightIndex = gateBits.Count - 1;
        state = State.Appear;
    }
}
