﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TemperatureSwitch : Item
{
    public Transform grabTransform;
    public Map.Temperature targetTemperature;
    public AnimationCurve moveCurve;

    private int ignoreTime = 0;
    private bool grabbed = false;
    private int grabTime = 0;
    private bool switchActive = false;
    private GrabType grabType;

    private const float GrabSize = 0.40f;
    private const int SwitchTime = 60;

    private bool isGrabbing = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        this.rotation = def.tile.flip ? 0 : 270;
        this.grabType =  def.tile.flip ? GrabType.LeftWall : GrabType.RightWall;

        this.hitboxes = new Hitbox[0];
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private Vector2 GetGrabRelativePosition()
    {
        float x = this.grabTransform.localPosition.x * this.transform.localScale.x;
        return new Vector2(x, this.grabTransform.localPosition.y);
    }
    
    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        AdvanceHanging();
        AdvanceGrabbed();
        
        this.grabInfo = new GrabInfo(this, this.grabType, GetGrabRelativePosition());
    }
  
    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null) return;

        this.grabbed = false;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - SensorPosition();
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreTime = 15;
            this.grabbed = true;
            if (!switchActive)
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvPullswitch,
                    position: this.position
                );
                this.isGrabbing = true;
            }
            
        }
    }

    private void AdvanceGrabbed()
    {
        float yGrabPosition = Mathf.Lerp(0f, -1f,
            this.moveCurve.Evaluate(Mathf.InverseLerp(0, SwitchTime, this.grabTime)));

        this.grabTransform.localPosition =
            new Vector2(this.grabTransform.localPosition.x, yGrabPosition);

        bool shouldSwitchBeActive =
            this.targetTemperature == this.map.currentTemperature;

        if(this.grabbed == false)
        {
            this.grabTime += shouldSwitchBeActive ? 10 : -10;
            this.switchActive = shouldSwitchBeActive;

            if(this.grabTime > SwitchTime)  this.grabTime = SwitchTime;
            if(this.grabTime < 0)           this.grabTime = 0;
            if (this.isGrabbing)
            {
                this.isGrabbing = false;
                Audio.instance.StopSfx(Assets.instance.sfxEnvPullswitch);
            }
            
        }
        else
        {
            if(this.grabTime < SwitchTime)
            {
                this.grabTime += 1;
            }

            if(this.grabTime >= SwitchTime)
            {
                ActivateSwitch();
            }
        }
    }
    
    public Vector2 SensorPosition()
    {
        return this.position + GetGrabRelativePosition();
    }

    private void ActivateSwitch()
    {
        if(this.switchActive == true) return;

        this.switchActive = true;
        this.grabTime = SwitchTime;

        this.map.ChangeTemperature(this.targetTemperature);

        this.isGrabbing = false;
        if (this.targetTemperature == Map.Temperature.Cold)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPullswitchCold,
                position: this.position
            );
        }
        else
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvPullswitchHot,
                position: this.position
            );
        }
    }
}
