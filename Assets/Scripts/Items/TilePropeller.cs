using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TilePropeller : Item
{
    public Animated.Animation animationSingleFanIdle;
    public Animated.Animation animationDoubleFanIdle;
    public Animated.Animation animationSingleFanRunning;
    public Animated.Animation animationDoubleFanRunning;

    [Header("Trails")]
    public GameObject trailPrefab;
    public AnimationCurve trailCurve;
    
    private bool on = true;
    private bool checkedNeighbours = false;
    private bool isDoubleFan = false;
    public List<Dandelion> dandelions = new List<Dandelion>();

    private Animated.Animation AnimationFanIdle => this.isDoubleFan ?
        this.animationDoubleFanIdle : this.animationSingleFanIdle;
    private Animated.Animation AnimationFanRunning => this.isDoubleFan ?
        this.animationDoubleFanRunning : this.animationSingleFanRunning;

    public static Vector2[] Directions = new Vector2[]
    {
        Vector2.up,
        Vector2.left,
        Vector2.down,
        Vector2.right,
    };

    public Vector2 CurrentDirection =>
        this.rotation switch
        {
            0   => Directions[0],
            90  => Directions[1],
            180 => Directions[2],
            270 => Directions[3],
            _   => Directions[0]
        };

    private Vector2 NeighbourDirection =>
        this.rotation switch
        {
            0   => Vector2.right,
            90  => Vector2.up,
            180 => Vector2.right,
            270 => Vector2.up,
            _   => Vector2.right
        };

    private bool IsNeighbourLeader =>
        this.rotation switch
        {
            0   => true,
            90  => true,
            180 => false,
            270 => false,
            _   => true
        };
        
    private static Vector2 HitboxPos = new Vector2(0f, 0f);
    private static Vector2 HitboxSize = new Vector2(0.50f, 0.50f);

    private const float MaxDistance = 100f;

    public bool hasLeader = false;
    public bool sfxPlayed = false;

    public override void Init(Def def)
    {
        base.Init(def);

        //this.tilePropertyBlock = new MaterialPropertyBlock();

        this.hitboxes = new Hitbox[]
        {
            new Hitbox(
                HitboxPos.x - HitboxSize.x,
                HitboxPos.x + HitboxSize.x,
                HitboxPos.y - HitboxSize.y,
                HitboxPos.y + HitboxSize.y,
                0, Hitbox.Solid,
                wallSlideAllowedOnLeft: true,
                wallSlideAllowedOnRight: true)
        };
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        RefreshAnimation();

        this.dandelions = this.chunk.items.OfType<Dandelion>().ToList();
    }

    public void NotifyOfNewDandelion(Dandelion dandelion)
    {
        this.dandelions.Add(dandelion);

        List<Dandelion> currentDandelions = new List<Dandelion>(this.dandelions);

        foreach(var d in currentDandelions)
        {
            if(d == null || d.IsDead == true)
            {
                this.dandelions.Remove(d);
            }
        }
    }

    public void HideForNeighbour()
    {
        this.checkedNeighbours = true;
        this.spriteRenderer.enabled = false;
        RefreshAnimation();
    }

    public void JoinWithNeighbour()
    {
        this.checkedNeighbours = true;
        this.isDoubleFan = true;
        RefreshAnimation();
    }
    
    public override void Advance()
    {
        if(this.checkedNeighbours == false)
        {
            Raycast raycast = new Raycast(
                this.map,
                this.position,
                ignoreItem1: this
            );

            var neighbour = raycast.Arbitrary(this.position, NeighbourDirection, 1f);
            if(neighbour.item is TilePropeller)
            {
                TilePropeller tileNeighbour = neighbour.item as TilePropeller;

                if(tileNeighbour.CurrentDirection == CurrentDirection)
                {
                    if(IsNeighbourLeader == true)
                    {
                        tileNeighbour.HideForNeighbour();
                        JoinWithNeighbour();
                    }
                    else
                    {
                        HideForNeighbour();
                        tileNeighbour.JoinWithNeighbour();
                    }
                }
            }

            this.checkedNeighbours = true;
        }

        if(this.on == true)
        {
            CheckForPlayer();
        }

        if(this.sfxPlayed == false)
        {
            StartSfx();
        }
    }

    private void CheckForPlayer()
    {
        float DistanceToChunkLimit(Vector2 startPos)
        {
            if(CurrentDirection == Vector2.up)
            {
                return (this.chunk.yMax + 1) - startPos.y;   
            }
            else if(CurrentDirection == Vector2.left)
            {
                return startPos.x - (this.chunk.xMin + 1);   
            }
            else if(CurrentDirection == Vector2.down)
            {
                return startPos.y - (this.chunk.yMin + 1);   
            }
            else if(CurrentDirection == Vector2.right)
            {
                return (this.chunk.xMax + 1) - startPos.x;   
            }

            return Mathf.Infinity;
        }

        Rect PushRect(Vector2 origin, float distance) =>
            this.rotation switch
            {
                0   => new Rect(origin.x, origin.y, 1f, distance),
                90  => new Rect(origin.x - distance + 1f, origin.y, distance, 1f),
                180 => new Rect(origin.x, origin.y - distance + 1f, 1f, distance),
                270 => new Rect(origin.x, origin.y, distance, 1f),
                _   => new Rect(origin.x, origin.y, 1f, distance)
            };

        Vector2 raycastStartPosition = this.position + (CurrentDirection * 0.50f);

        Raycast raycast = new Raycast(
            this.map,
            raycastStartPosition,
            ignoreChunksOutsideRange: 20f,
            ignoreItem1: this,
            ignoreTileTopOnlyFlag: true
        );
            
        var detectSolid = raycast.Arbitrary(
            raycastStartPosition, CurrentDirection, MaxDistance
        );
        
        float distanceToSolid = detectSolid.distance;

        float distanceToChunkLimit = DistanceToChunkLimit(raycastStartPosition);

        if(distanceToSolid > distanceToChunkLimit)
        {
            distanceToSolid = distanceToChunkLimit;
        }

        ManageTrails(distanceToSolid);

        Vector2 rectangleOrigin = raycastStartPosition - new Vector2(0.50f, 0.50f);
        Rect pushRect = PushRect(rectangleOrigin, distanceToSolid);
        
        Player player = Map.instance.player;
        Rect playerRect = player.Rectangle();

        if(playerRect.Overlaps(pushRect) &&
            player.ShouldCollideWithItems() &&
            CanInteractWithPlayer())
        {
            PushPlayer();
        }

        foreach (var dandelion in this.dandelions)
        {
            Rect dandelionRect = dandelion.hitboxes[0].InWorldSpace();

            if(dandelionRect.Overlaps(pushRect))
            {
                dandelion.PushFromPropeller(CurrentDirection);
            }
        }
    }

    private bool CanInteractWithPlayer()
    {
        Player player = Map.instance.player;

        if(player.state == Player.State.Spring) return false;
        if(player.state == Player.State.SpringFromPushPlatform) return false;

        return true;
    }

    private void PushPlayer()
    {
        Map.instance.player.PushFromPropeller(CurrentDirection);
    }
    
    public void ToggleOn(bool on)
    {
        this.on = on;
        RefreshAnimation();
    }

    private void RefreshAnimation()
    {
        Animated.Animation animationFan = this.on ? AnimationFanRunning : AnimationFanIdle;
        this.animated.PlayAndLoop(animationFan);
    }

    [HideInInspector] public float trailAmplitude = 0.45f;
    [HideInInspector] public float trailPeriod = 3.50f;
    [HideInInspector] public float trailSpeed = 0.25f;

    private List<WindTrail> activeTrails = new List<WindTrail>();
    private List<WindTrail> trailPool = new List<WindTrail>();
    private const int PoolSize = 3;

    private class WindTrail
    {
        public GameObject go;
        private TilePropeller propeller;
        private int timer;
        private int resetTimer;
        private Vector3 position;
        private float distanceToSolid;

        private float amplitude;
        private float period;
        private float speed;

        private const int ResetTime = 24;

        public WindTrail(GameObject go, TilePropeller propeller)
        {
            this.go = go;
            this.propeller = propeller;
            this.timer = 0;
        }

        public void Init(float distanceToSolid)
        {
            Vector2 pos = this.propeller.position + (Random.insideUnitCircle * 0.50f);
            this.go.transform.position = this.position = pos;
            this.distanceToSolid = distanceToSolid + 0.50f;

            this.amplitude = Random.Range(propeller.trailAmplitude - 0.10f, propeller.trailAmplitude + 0.10f);
            this.period = Random.Range(propeller.trailPeriod - 0.50f, propeller.trailPeriod + 0.50f);
            this.speed = Random.Range(propeller.trailSpeed - 0.05f, propeller.trailSpeed + 0.05f);
            
            this.timer = Random.Range(-30, 0);
        }

        public void Advance()
        {
            float distanceToOrigin =
                Vector2.Distance(this.go.transform.position, this.propeller.position);

            if(distanceToOrigin < distanceToSolid)
            {
                Move();
            }
            else
            {
                Reset();
            }
        }

        private void Move()
        {
            this.timer += 1;

            if(this.timer >= 0)
            {
                float theta = this.timer / this.period;
                float distance = this.amplitude * Mathf.Sin(theta);

                float t = 100f;
                float v = (this.timer % t) / t;
                float speed = this.propeller.trailCurve.Evaluate(v) * this.speed;
                Vector3 vMove = this.propeller.CurrentDirection * speed;
                Vector3 hMove = this.propeller.transform.right * distance;

                this.position += vMove;

                this.go.transform.position = this.position + hMove;
            }
        }
        
        private void Reset()
        {
            this.resetTimer += 1;

            if(this.resetTimer >= ResetTime)
            {
                this.timer = 0;
                this.resetTimer = 0;
                this.go.transform.position = this.position = this.propeller.position;

                this.propeller.FinishWindTrail(this);
            }
        }
    }

    private void InitTrailPool()
    {
        for (int i = 0; i < PoolSize; i++)
        {
            GameObject trailGO =
                Instantiate(this.trailPrefab, this.position, Quaternion.identity);
            trailGO.transform.SetParent(this.transform);
            trailGO.layer = this.gameObject.layer;

            WindTrail windTrail = new WindTrail(trailGO, this);
            AddWindTrail(windTrail);
        }
    }

    private void AddWindTrail(WindTrail windTrail)
    {
        this.trailPool.Add(windTrail);
        windTrail.go.SetActive(false);
    }

    private void RemoveWindTrail(WindTrail windTrail)
    {
        this.trailPool.Remove(windTrail);
        windTrail.go.SetActive(true);
    }

    private WindTrail GetWindTrail()
    {
        WindTrail chosenWindTrail = this.trailPool[0];
        RemoveWindTrail(chosenWindTrail);
        return chosenWindTrail;
    }

    private void FinishWindTrail(WindTrail windTrail)
    {
        this.activeTrails.Remove(windTrail);
        AddWindTrail(windTrail);
    }

    private void ManageTrails(float distanceToSolid)
    {
        if(this.trailPool == null || this.trailPool.Count <= 0)
        {
            InitTrailPool();
        }

        if(this.map.frameNumber % 30 == 0)
        {
            WindTrail windTrail = GetWindTrail();
            windTrail.Init(distanceToSolid);
            this.activeTrails.Add(windTrail);
        }

        for (int i = this.activeTrails.Count - 1 - 1; i >= 0 ; i--)
        {
            this.activeTrails[i].Advance();
        }
    }

    private void StartSfx()
    {
        if (this.sfxPlayed == false && this.hasLeader == false)
        {
            Audio.instance.PlaySfxLoop(Assets.instance.sfxWindyTilePropellerOn, this.position);
        }
        this.sfxPlayed = true;
        foreach (var tilePropeller in this.chunk.items.OfType<TilePropeller>())
        {
            tilePropeller.sfxPlayed = true;
        }
    }
}
