using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePropellerLeader : Item
{
    private List<TilePropeller> tilePropellers =  new List<TilePropeller>();
    private bool on = true;
    private bool initialOn = true;
    private Button[] buttons;
    
    public override void Init(Def def)
    {
        base.Init(def);
        
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        if(this.group != null)
        {
            foreach(var f in this.group.followers)
            {
                if(f.Key.item is TilePropeller)
                {
                    this.tilePropellers.Add(f.Key.item as TilePropeller);
                    (f.Key.item as TilePropeller).hasLeader = true;
                }
            }
        }
        
        var buttons = new List<Button>();
        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                var button = this.chunk.NearestItemTo<Button>(otherNode.Position);
                buttons.Add(button);
            }
        }
        this.buttons = buttons.ToArray();

        this.initialOn = this.on = (def.tile.properties?.ContainsKey("on") == true) ?
            def.tile.properties["on"].b : false;

        ToggleOn(this.on);
    }
    
    public override void Advance()
    {
        base.Advance();
        
        var pressed = false;
        foreach (var button in this.buttons)
        {
            if (button.PressedThisFrame == true)
                pressed = true;
        }

        if (pressed == true)
        {
            ButtonPressed();
        }
    }

    public override void Reset()
    {
        base.Reset();

        ToggleOn(this.initialOn);
    }
    
    private void ButtonPressed()
    {
        ToggleOn(!this.on);
    }

    private void ToggleOn(bool on)
    {
        this.on = on;
        
        foreach(var tp in this.tilePropellers)
        {
            tp.ToggleOn(this.on);
        }

        if (on)
        {
            Audio.instance.PlaySfxLoop(Assets.instance.sfxWindyTilePropellerOn, this.position);
        }
        else
        {
            Audio.instance.StopSfxLoop(Assets.instance.sfxWindyTilePropellerOn, this.position);
        }
    }
}
