
using UnityEngine;
using System.Linq;

public class TimerBlock : Item
{
    public TimerButton.Color color;
    public GameObject graphicsGO;

    [Header("Background")]
    public Transform bgTransform;
    
    [Header("Border")]
    public Animated.Animation appearAnimation;
    public Animated.Animation shownAnimation;
    public Animated.Animation disappearAnimation;
    public Animated.Animation hideAnimation;

    [Header("Top")]
    public Animated topAnimated;
    public Animated.Animation topActiveAnimation;

    [Header("Shade")]
    public Animated shadeAnimated;
    public Animated.Animation shadeIdleAnimation;
    public Animated.Animation shadeActiveAnimation;

    private bool isActive = false;
    private int activeTimer = 0;
    private int shadeTime = 0;

    public const int ActiveTotalTime = 8 * 60;
    // allow some extra active time after the animation is finished
    public const int ExtraActiveTime = 12; 
    private static Vector2 TopInitialPos = new Vector2(-0.5f, -0.474f);
    private static Vector2 TopTargetPos = new Vector2(-0.5f, -1.074f);
    private static Vector2 BackgroundInitialScale = new Vector2(1f, 0.9f);
    private static Vector2 BackgroundTargetScale = new Vector2(1f, 0.2f);

    public override void Init(Def def)
    {
        base.Init(def);

        this.graphicsGO.SetActive(false);
        this.activeTimer = 0;

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        ResetGraphics();

        float shadeActiveAnimTime =
            (float)this.shadeActiveAnimation.sprites.Length / (float)this.shadeActiveAnimation.fps;
        this.shadeTime = Mathf.RoundToInt(shadeActiveAnimTime * 60) + ExtraActiveTime;
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if(this.activeTimer > 0)
        {
            this.activeTimer -= 1;

            float timeWithoutShade = Mathf.InverseLerp(ActiveTotalTime, this.shadeTime, this.activeTimer);

            if(timeWithoutShade == 1f && this.shadeAnimated.currentAnimation == this.shadeIdleAnimation)
            {
                this.shadeAnimated.PlayOnce(this.shadeActiveAnimation);

                this.topAnimated.gameObject.SetActive(false);
                this.bgTransform.gameObject.SetActive(false);
            }
            else
            {
                this.topAnimated.transform.localPosition =
                    Vector2.Lerp(TopInitialPos, TopTargetPos, timeWithoutShade);

                this.bgTransform.transform.localScale =
                    Vector3.Lerp(BackgroundInitialScale, BackgroundTargetScale, timeWithoutShade);
            }

            if(this.activeTimer <= 0)
            {
                this.animated.PlayOnce(this.disappearAnimation, Deactivate);
            }
        }
    }

    public void Activate()
    {
        bool wasActive = this.isActive;
        this.isActive = true;

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };
        
        if(wasActive == true)
        {
            this.animated.PlayOnce(this.shownAnimation);
            this.graphicsGO.SetActive(true);
            this.topAnimated.gameObject.SetActive(true);
            this.bgTransform.gameObject.SetActive(true);

            this.activeTimer = ActiveTotalTime;
        }
        else
        {
            this.animated.PlayOnce(this.appearAnimation, () =>
            {
                this.animated.PlayOnce(this.shownAnimation);
                this.graphicsGO.SetActive(true);
                this.activeTimer = ActiveTotalTime;
            });
        }
    }

    private void Deactivate()
    {
        if (this.isActive == false) return;
        this.isActive = false;
        this.activeTimer = 0;

        this.graphicsGO.SetActive(false);
        this.hitboxes = new Hitbox[0];
        
        ResetGraphics();
    }

    private void ResetGraphics()
    {
        this.animated.PlayOnce(this.hideAnimation);
        this.topAnimated.PlayAndLoop(this.topActiveAnimation);
        this.shadeAnimated.PlayAndLoop(this.shadeIdleAnimation);

        this.topAnimated.transform.localPosition = TopInitialPos;
        this.bgTransform.transform.localScale = BackgroundInitialScale;
        
        this.topAnimated.gameObject.SetActive(true);
        this.bgTransform.gameObject.SetActive(true);
    }

    public override void Reset()
    {
        base.Reset();

        if(this.isActive == true)
        {
            this.animated.PlayOnce(this.disappearAnimation, Deactivate);
        }
        else
        {
            Deactivate();
        }
    }
}
