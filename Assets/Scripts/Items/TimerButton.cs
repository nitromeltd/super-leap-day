
using UnityEngine;
using System.Linq;

public class TimerButton : Item
{
    public enum Color
    {
        PURPLE,
        GREEN
    }

    public enum Orientation
    {
        TOP,
        FLOOR
    }
    
    public Color color;
    public Orientation orientation;

    public Animated.Animation idleAnimation;
    public Animated.Animation pressAnimation;
    public Animated.Animation unpressAnimation;

    private bool pressed;

    private const int PressAgainTime = 30;

    public int coinsCollected = 0;
    public int totalCoins;
    public bool isCoinActive = false;
    public int coinActiveTimer = 0;
    public const int ActiveTotalTime = 8 * 60;
    public bool newCoinCollected = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid)
        };

        this.animated.PlayOnce(idleAnimation);
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    override public void Advance()
    {
        base.Advance();

        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        var pressedLastFrame = this.pressed;
        this.pressed = IsBeingPressed();

        if(pressedLastFrame == false && this.pressed == true)
        {
            Press();
        }

        if(pressedLastFrame == true && this.pressed == false)
        {
            Unpress();
        }

        if (this.isCoinActive == true)
        {
            this.coinActiveTimer -= 1;
            if (this.coinActiveTimer <= 0)
            {
                this.isCoinActive = false;
            }
            if(this.newCoinCollected == true)
            {
                foreach (var timerCoin in this.chunk.items.OfType<TimerCoin>())
                {
                    if (timerCoin.color == this.color && timerCoin.collected == true && timerCoin.goldCoinCollected == false)
                    {
                        timerCoin.ShowAnimation();
                    }
                }
                this.newCoinCollected = false;
            }
        }
    }

    private bool IsBeingPressed()
    {
        var thisRect = this.hitboxes[0].InWorldSpace();
        if (this.map.player.Rectangle().Overlaps(thisRect))
            return true;

        foreach (var item in this.chunk.items.Where(Chunk.CanItemPressButton))
        {
            foreach (var h in item.hitboxes)
            {
                if (h.InWorldSpace().Overlaps(thisRect))
                    return true;
            }
        }

        foreach (var boulder in this.chunk.subsetOfItemsOfTypeAppleBoulder)
        {
            if (boulder.IsOverlapping(this))
                return true;
        }

        var hitboxRect = this.hitboxes[0].InWorldSpace();
        Vector2[] SensorsWhenHorizontal() => new []
        {
            new Vector2(hitboxRect.xMin,     hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.xMax,     hitboxRect.center.y)
        };
        Vector2[] SensorsWhenVertical() => new []
        {
            new Vector2(hitboxRect.center.x, hitboxRect.yMin),
            new Vector2(hitboxRect.center.x, hitboxRect.center.y),
            new Vector2(hitboxRect.center.x, hitboxRect.yMax)
        };

        var sensors =
            (this.rotation == 0 || this.rotation == 180) ?
            SensorsWhenHorizontal() :
            SensorsWhenVertical();

        foreach (var sensor in sensors)
        {
            if (this.chunk.TileAt(sensor, Layer.A) != null)
                return true;
        }

        return false;
    }

    public void Press()
    {
        // Debug.LogFormat("{0}, PRESS TimerButton: {1}, Frame Number: {2}",
        //     this.gameObject.GetInstanceID(),
        //     this.gameObject,
        //     this.map.frameNumber
        // );

        this.animated.PlayOnce(this.pressAnimation);

        foreach (var timerBlock in this.chunk.items.OfType<TimerBlock>())
        {
            if(timerBlock.color == this.color)
            {
                timerBlock.Activate();
            }
        }

        foreach (var timerSpike in this.chunk.items.OfType<TimerSpike>())
        {
            if(timerSpike.color == this.color)
            {
                timerSpike.Activate();
            }
        }

        this.totalCoins = 0;
        foreach (var timerCoin in this.chunk.items.OfType<TimerCoin>().ToList())
        {
            if (timerCoin.color == this.color && timerCoin.goldCoinCollected == false && timerCoin.state != TimerCoin.State.BecomingGoldCoin)
            {
                if (timerCoin.parentButtons[0].chunk != this.chunk)
                {
                    timerCoin.PopOnDeath();
                }
                else
                {
                    timerCoin.Activate();
                    this.isCoinActive = true;
                    this.totalCoins++;
                }
            }
        }
        if(this.isCoinActive == true)
        {
            this.coinActiveTimer = ActiveTotalTime;
        }

        Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);
    }

    private void Unpress()
    {
        // Debug.LogFormat("{0}, UNPRESS TimerButton: {1}, Frame Number: {2}",
        //     this.gameObject.GetInstanceID(),
        //     this.gameObject,
        //     this.map.frameNumber
        // );

        this.animated.PlayOnce(this.unpressAnimation);
        Audio.instance.PlaySfx(Assets.instance.sfxButtonPress, position: this.position);
    }
}
