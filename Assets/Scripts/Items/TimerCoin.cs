using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

public class TimerCoin : Pickup
{
    public TimerButton.Color color;

    public Animated.Animation appearAnimation;
    public Animated.Animation stillAnimation;
    public Animated.Animation disappearAnimation;
    public Animated.Animation shownAnimation;
    public Animated.Animation hideAnimation;
    public Animated.Animation hitParticleAnimation;

    [NonSerialized] public bool isActive = false;
    private int activeTimer = 0;
    private const int ActiveTotalTime = 8 * 60;

    private int coinNumber;
    private int totalCoins;
    private Vector2 originalPosition;

    //[NonSerialized] 
    public bool goldCoinCollected = false;
    private bool isRotating;
    public static Vector2 lastPlayerPosition;

    private Chunk originalChunk;
    private Chunk updatedChunk = null;
    private Transform originalParent;
    private bool hitWall = false;
    private bool becomeActive = true;
    private bool hasDetached;

    private float rotationalSpeed = 2;
    private float rotationalRadius = 2.25f;
    private float height = 0;

    private float sparkleOffset;

    private int frameNumber;
    private int lastFrameNumber;

    private bool hasPopped = false;
    private bool onMovingPlatform = false;
    [NonSerialized] public List<TimerButton> parentButtons = new List<TimerButton>();


    public State state;
    public enum State
    {
        Unactive,
        Active,
        Collected,
        BecomingGoldCoin,
        BecameGoldCoin,
        Popping
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.activeTimer = 0;

        this.hitboxes = new[] {
            new Hitbox(-0.25f, 0.25f, -0.40f, 0.25f, this.rotation, Hitbox.NonSolid)
        };

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;

        this.state = State.Unactive;
        this.animated.PlayAndLoop(this.hideAnimation);

        this.originalPosition = this.transform.position;
        if(this.transform.parent.GetComponent<Chunk>() != null)
        {
            this.originalChunk = this.transform.parent.GetComponent<Chunk>();
            this.onMovingPlatform = false;
        }
        else
        {
            this.originalChunk = this.transform.parent.parent.GetComponent<Chunk>();
            this.onMovingPlatform = true;
        }
        this.originalParent = this.transform.parent;

        this.isRotating = false;
        this.hasDetached = false;
        this.rotationalSpeed = 2;
        this.rotationalRadius = 2.25f;
        this.height = 0;

        this.sparkleOffset = 0.75f;

        this.becomeActive = false;

        foreach (var timerButton in this.chunk.items.OfType<TimerButton>().ToList())
        {
            if (timerButton.color == this.color)
            {
                this.parentButtons.Add(timerButton);
            }
        }
    }

    override public void Advance()
    {
        switch (this.state)
        {
            case (State.Unactive):
                break;
            case (State.Active):
                base.Advance();
                CheckCollect();
                CreateSparkleParticles();
                break;
            case (State.Collected):
                if (this.isRotating && this.coinNumber != 0)
                {
                    MoveAroundPlayer();
                }
                if (this.parentButtons[0].coinsCollected == 8 && !this.goldCoinCollected)
                {
                    this.goldCoinCollected = true;
                    this.state = State.BecomingGoldCoin;
                    StartCoroutine(CreateCoinCoroutine());
                }
                break;
            case (State.BecomingGoldCoin):
                if (this.isRotating && this.coinNumber != 0)
                {
                    MoveAroundPlayer();
                }
                break;
            case (State.BecameGoldCoin):
                break;
            case (State.Popping):
                break;
        }

        if (this.activeTimer > 0)
        {
            this.activeTimer -= 1;

            if (this.map.player.alive != true && this.state != State.BecomingGoldCoin)
            {
                if (this.collected)
                {
                    PopOnDeath();
                }
                else
                {
                    Deactivate();
                }
            }

            if (this.activeTimer <= 0 && this.state != State.BecomingGoldCoin && this.state != State.Popping)
            {
                Deactivate();
            }

            if(this.onMovingPlatform == true)
            {
                if (this.map.NearestChunkTo(this.map.player.position) != this.transform.parent.parent.GetComponent<Chunk>())
                {
                    this.transform.SetParent(this.map.NearestChunkTo(this.map.player.position).transform);
                    this.map.NearestChunkTo(this.map.player.position).items.Add(this);
                    if (this.updatedChunk != null)
                    {
                        this.updatedChunk.items.Remove(this);
                    }
                    else
                    {
                        if (this.originalChunk != null)
                        {
                            this.originalChunk.items.Remove(this);
                        }
                    }
                    this.updatedChunk = this.map.NearestChunkTo(this.map.player.position);
                }
            }
            else
            {
                if (this.map.NearestChunkTo(this.map.player.position) != this.transform.parent.GetComponent<Chunk>())
                {
                    this.transform.SetParent(this.map.NearestChunkTo(this.map.player.position).transform);
                    this.map.NearestChunkTo(this.map.player.position).items.Add(this);
                    if (this.updatedChunk != null)
                    {
                        this.updatedChunk.items.Remove(this);
                    }
                    else
                    {
                        if (this.originalChunk != null)
                        {
                            this.originalChunk.items.Remove(this);
                        }
                    }
                    this.updatedChunk = this.map.NearestChunkTo(this.map.player.position);
                }
            }
        }

    }

    public override void Collect()
    {
        if (this.collected == true) return;

        foreach (var timerButton in parentButtons.ToList())
        {
            timerButton.coinsCollected++;
            this.coinNumber = timerButton.coinsCollected;
            this.totalCoins = timerButton.coinsCollected;
        }
        this.collected = true;
        Audio.instance.PlaySfx(Assets.instance.sfxCoin, position: this.position, options: new Audio.Options(0.9f, true, 0));
        this.state = State.Collected;

        this.animated.PlayOnce(this.appearAnimation, () =>
        {
            this.animated.PlayAndLoop(this.shownAnimation);
            this.parentButtons[0].newCoinCollected = true;
        });
    }

    public override void CheckCollect()
    {
        if (this.disallowCollectTime > 0)
        {
            this.disallowCollectTime -= 1;
        }
        else if (this.collected == false && this.canPlayerCollect == true && this.isActive)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            var playerRect = this.map.player.Rectangle();

            if (thisRect.Overlaps(playerRect))
                Collect();
        }
    }

    public void Activate()
    {
        if (this.goldCoinCollected == true || this.state == State.BecameGoldCoin || this.state == State.BecomingGoldCoin)
        {
            return;
        }

        bool wasActive = this.isActive;

        if (this.state == State.Popping)
        {
            this.becomeActive = true;
            return;
        }

        this.isActive = true;
        if (this.becomeActive == true)
        {
            this.activeTimer = parentButtons[0].coinActiveTimer;
            this.becomeActive = false;
            wasActive = false;
        }
        else
        {
            this.activeTimer = ActiveTotalTime;
        }

        if (wasActive)
        {
            this.activeTimer = ActiveTotalTime;
            this.animated.PlayAndLoop(this.shownAnimation);
        }
        else
        {
            this.animated.PlayOnce(this.appearAnimation, () =>
            {
                this.animated.PlayAndLoop(this.shownAnimation);
                this.activeTimer = ActiveTotalTime;
            });
            this.state = State.Active;
            this.movingFreely = false;
            this.isRotating = true;
        }
    }

    public void Deactivate()
    {
        if (this.isActive == false) return;
        this.isActive = false;
        this.activeTimer = 0;

        if (this.collected && !this.goldCoinCollected)
        {
            StartCoroutine(PopWait());
            this.state = State.Popping;
        }
        else if (!this.collected)
        {
            this.animated.PlayOnce(this.disappearAnimation, () =>
            {
                this.animated.PlayAndLoop(this.hideAnimation);
                this.position = originalPosition;
                this.transform.position = originalPosition;
                this.transform.rotation = Quaternion.Euler(0, 0, 0);
                this.movingFreely = false;
                this.state = State.Unactive;
                foreach (var timerButton in parentButtons.ToList())
                {
                    timerButton.coinsCollected = 0;
                    this.coinNumber = 0;
                    this.totalCoins = 0;
                    timerButton.coinActiveTimer = 0;
                    timerButton.isCoinActive = false;
                }
            });
        }

        this.height = 0;
        this.isRotating = false;
        if (this.updatedChunk != null)
        {
            this.transform.SetParent(originalParent);
            this.updatedChunk.items.Remove(this);
            this.originalChunk.items.Add(this);
            this.updatedChunk = null;
        }
        this.collected = false;
    }

    private IEnumerator PopWait()
    {
        this.hasPopped = false;
        this.hitWall = false;
        this.animated.PlayOnce(this.appearAnimation, () =>
        {
            this.animated.PlayOnce(this.shownAnimation);
        });

        float currentTime = 0;
        while (currentTime < 2)
        {
            currentTime += Time.deltaTime;
            PopMove();
            if (this.hitWall)
            {
                yield break;
            }
            yield return null;
        }
        if (!this.hasPopped)
        {
            this.Pop();
        }
    }

    private void PopMove()
    {
        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: false, ignoreTileTopOnlyFlag: true);

        var right = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), true, 0.75f),
            raycast.Horizontal(this.position.Add(0, 6 / 16f), true, 0.75f)
        );
        var left = Raycast.CombineClosest(
            raycast.Horizontal(this.position.Add(0, -6 / 16f), false, 0.75f),
            raycast.Horizontal(this.position.Add(0, 6 / 16f), false, 0.75f)
        );

        var up = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), true, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), true, 0.75f)
        );
        var down = Raycast.CombineClosest(
            raycast.Vertical(this.position.Add(-6 / 16f, 0), false, 0.75f),
            raycast.Vertical(this.position.Add(6 / 16f, 0), false, 0.75f)
        );

        if (this.velocity.x > 0)
        {
            if (right.anything == true)
            {
                this.hitWall = true;
            }
        }
        else
        {
            if (left.anything == true)
            {
                this.hitWall = true;
            }
        }

        if (this.velocity.y > 0)
        {
            if (up.anything == true)
            {
                this.hitWall = true;
            }
        }
        else
        {
            if (down.anything == true)
            {
                this.hitWall = true;
            }
        }
        this.velocity = (new Vector2(this.transform.position.x, this.transform.position.y) - (lastPlayerPosition)).normalized / 4;
        this.transform.position += new Vector3(this.velocity.x, this.velocity.y, 0);
        this.position = this.transform.position;
        if (this.hitWall)
        {
            this.Pop();
            this.hasPopped = true;
        }
    }

    public void PopOnDeath()
    {
        //StartCoroutine(RandomPop());
        this.isActive = false;
        this.activeTimer = 0;
        foreach (var timerButton in this.parentButtons.ToList())
        {
            timerButton.coinsCollected = 0;
            this.coinNumber = 0;
            this.totalCoins = 0;
        }

        this.height = 0;
        this.isRotating = false;
        if (this.updatedChunk != null)
        {
            this.transform.SetParent(originalChunk.transform);
            this.updatedChunk.items.Remove(this);
            this.originalChunk.items.Add(this);
            this.updatedChunk = null;
        }
        this.collected = false;
        Pop();
    }

    private IEnumerator RandomPop()
    {
        this.state = State.Popping;
        float waitTime = UnityEngine.Random.Range(0f, .5f);
        yield return new WaitForSeconds(waitTime);
        Pop();
    }

    private void Pop()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxFruit, position: this.position);
        this.animated.PlayOnce(this.disappearAnimation, () =>
        {
            this.position = originalPosition;
            this.transform.position = originalPosition;
            this.hitWall = false;
            this.hasPopped = false;
            this.state = State.Unactive;
            this.isRotating = false;
            this.transform.rotation = Quaternion.Euler(0, 0, 0);
            if (this.becomeActive == true)
            {
                Activate();
            }
            else
            {
                this.animated.PlayAndLoop(this.hideAnimation);
            }
        });
    }

    private void MoveAroundPlayer()
    {
        if (parentButtons[0].coinsCollected == 0)
            return;
        float angle = (coinNumber - 1) * Mathf.PI * 2 / parentButtons[0].coinsCollected;
        Vector3 playerPosition = this.map.player.position;
        Vector3 targetPosition = playerPosition;
        if (this.hasDetached == false)
        {
            targetPosition.x = playerPosition.x + Mathf.Cos((angle + (this.map.frameNumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.y = playerPosition.y + Mathf.Sin((angle + (this.map.frameNumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.z = 0;
            this.transform.position = targetPosition;
            this.position = this.transform.position;
            {
                var rotationVector = this.transform.rotation.eulerAngles;
                rotationVector.z = (-90) + ((this.coinNumber - 1) * 180 * 2 / this.parentButtons[0].coinsCollected) + ((this.map.frameNumber / 60.0f) * this.rotationalSpeed * Mathf.Rad2Deg);
                this.transform.rotation = Quaternion.Euler(rotationVector);
            }
            this.frameNumber = this.map.frameNumber;
            if (this.coinNumber == 1)
            {
                lastPlayerPosition = playerPosition;
            }
        }
        else
        {
            targetPosition.x = lastPlayerPosition.x + Mathf.Cos((angle + (this.frameNumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.y = lastPlayerPosition.y + this.height + Mathf.Sin((angle + (this.frameNumber / 60.0f) * this.rotationalSpeed)) * this.rotationalRadius;
            targetPosition.z = 0;
            this.transform.position = Vector3.MoveTowards(this.transform.position, targetPosition, 1f);
            this.position = this.transform.position;
            {
                var rotationVector = this.transform.rotation.eulerAngles;
                rotationVector.z = (-90) + ((this.coinNumber - 1) * 180 * 2 / this.parentButtons[0].coinsCollected) + ((this.frameNumber / 60.0f) * this.rotationalSpeed * Mathf.Rad2Deg);
                this.transform.rotation = Quaternion.Euler(rotationVector);
            }
        }
    }

    private IEnumerator CreateCoinCoroutine()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxFanfareFruit);

        float currentTime = 0;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            this.rotationalRadius = Mathf.Lerp(2.25f, 2.75f, currentTime / 1);
            yield return null;
        }
        this.rotationalRadius = 2.75f;

        this.hasDetached = true;
        currentTime = 0;
        while (currentTime <= 1.3)
        {
            currentTime += Time.deltaTime;

            if (currentTime <= 1)
            {
                this.height = Mathf.Lerp(0, 4f, currentTime / 1);
                this.rotationalRadius = Mathf.Lerp(2.75f, 2f, currentTime / 1);
                this.frameNumber++;
            }
            else
            {
                this.height = 4;
                this.rotationalRadius = Mathf.Lerp(2f, 2.5f, (currentTime - 1) / .3f);
            }
            yield return null;
        }

        this.rotationalRadius = 2.5f;

        yield return new WaitForSeconds(.5f);

        currentTime = 0;
        this.isRotating = false;
        bool hasSparkled = false;
        Particle hitParticle;
        while (currentTime <= 1f)
        {
            currentTime += Time.deltaTime;
            this.transform.position = Vector2.MoveTowards(this.transform.position, GetTargetPosition(), .4f);
            this.position = this.transform.position;
            if (Vector2.Distance(this.transform.position, GetTargetPosition()) < .2f && !hasSparkled)
            {
                hasSparkled = true;
                Audio.instance.PlaySfx(Assets.instance.sfxEnvSpacedoorClose);
                this.animated.PlayAndLoop(this.hideAnimation);

                if (this.coinNumber == 1)
                {
                    Coin coin = null;
                    hitParticle = Particle.CreateAndPlayOnce(
                        hitParticleAnimation,
                        GetTargetPosition(),
                        this.transform.parent
                    );
                    hitParticle.spriteRenderer.sortingLayerName = "Items (Front)";
                    hitParticle.spriteRenderer.sortingOrder = 5;
                    hitParticle.animated.OnFrame(1, () =>
                    {
                        coin = Coin.Create(GetTargetPosition() + new Vector3(0, 0.2f, 0), this.chunk, true);
                        Audio.instance.PlaySfx(Assets.instance.sfxCoin, options: new Audio.Options(0.9f, false, 0));
                    });
                    hitParticle.animated.OnFrame(4, () =>
                    {
                        coin.movingFreely = true;
                    });
                }
                break;
            }
            yield return null;
        }
        this.transform.rotation = Quaternion.Euler(0, 0, 0);
        this.transform.position = originalPosition;
        this.movingFreely = false;
        this.state = State.BecameGoldCoin;
    }

    private Vector3 GetTargetPosition()
    {
        Vector3 targetPosition;
        targetPosition.x = lastPlayerPosition.x;
        targetPosition.y = lastPlayerPosition.y + height;
        targetPosition.z = 0;
        return targetPosition;
    }

    private void CreateSparkleParticles()
    {
        bool createSparkleParticle = this.map.frameNumber % 7 == 0;

        if (createSparkleParticle == true)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.coinSparkleAnimation,
                this.position.Add(
                    UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset),
                    UnityEngine.Random.Range(-this.sparkleOffset, this.sparkleOffset)
                ),
                this.transform.parent
            );
            sparkle.spriteRenderer.sortingLayerName = "Items";
            sparkle.spriteRenderer.sortingOrder = -1;
        }
    }

    public void ShowAnimation()
    {
        this.animated.PlayOnce(this.stillAnimation, () =>
        {
            this.animated.PlayAndLoop(this.shownAnimation);
        });
    }

    public override void Reset()
    {
        base.Reset();
    }
}