using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

public class TimerPickup : Item
{
    private float hitboxSize;
    public AnimationCurve speedCurve;

    private float breatheTimer;
    private const float BreathePeriod = .5f;
    private const float BreatheAmplitude = .3f;

    private Vector3 initialPos;
    public SpriteRenderer sr;

    public override void Init(Def def)
    {
        base.Init(def);
        this.hitboxSize = 0.5f;
        this.hitboxes = new[] {
            new Hitbox(-this.hitboxSize, this.hitboxSize, -this.hitboxSize, this.hitboxSize, this.rotation, Hitbox.NonSolid)
        };
    }

    public static TimerPickup Create(Vector2 position, Chunk chunk, Vector3 targetPos, float hoverTime)
    {
        var obj = Instantiate(Assets.instance.timerPickup, chunk.transform);
        obj.name = "Timer Pickup";
        var item = obj.GetComponent<TimerPickup>();
        targetPos = IngameUI.instance.PositionForUICameraFromWorldPosition(chunk.map, targetPos);
        item.Init(position, chunk, targetPos, hoverTime);
        chunk.items.Add(item);
        IngameUI.instance.MoveTransformToUILayer(item.map, item.transform);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, Vector3 targetPos, float hoverTime)
    {
        Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        StartCoroutine(MoveAtStart(targetPos, hoverTime));
    }

    private IEnumerator MoveAtStart(Vector3 targetPos, float hoverTime)
    {
        float timer = 0f;
        float minMoveSpeed = 5f * Time.deltaTime;
        float maxMoveSpeed = 8f * Time.deltaTime;
        float reachMaxSpeedTime = 0.50f;
        bool moving = true;
        while (moving == true)
        {
            if (IngameUI.instance.pauseMenu.isOpen == false)
            {
                timer += Time.deltaTime;

                moving = false;

                float t = Mathf.InverseLerp(0f, reachMaxSpeedTime, timer);
                float currentMoveSpeed =
                    Mathf.Lerp(minMoveSpeed, maxMoveSpeed, this.speedCurve.Evaluate(t));

                Vector3 currentPosition = this.transform.position;

                Vector3 heading = targetPos - currentPosition;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                if (distance > currentMoveSpeed)
                {
                    this.transform.position += direction * currentMoveSpeed;
                    moving = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        StartCoroutine(Hover(hoverTime));
    }

    private IEnumerator Hover(float hoverTime)
    {
        this.initialPos = this.transform.position;
        this.breatheTimer = 0 ;
        while(breatheTimer < hoverTime)
        {
            this.breatheTimer += Time.deltaTime;

            float theta = this.breatheTimer / BreathePeriod;
            float distance = BreatheAmplitude * Mathf.Sin(theta);

            this.transform.position = this.initialPos + this.transform.up * distance;

            yield return null;
        }

        StartCoroutine(MoveToFinish());
    }

    private IEnumerator MoveToFinish()
    {
        Transform powerup = this.transform;

        float timer = 0f;
        float minMoveSpeed = 5f * Time.deltaTime;
        float maxMoveSpeed = 50f * Time.deltaTime;
        float reachMaxSpeedTime = 0.50f;

        bool moving = true;
        while (moving == true)
        {
            if (IngameUI.instance.pauseMenu.isOpen == false)
            {
                timer += Time.deltaTime;
                moving = false;
                RectTransform targetTransform = IngameUI.instance.TimerCoinImageRectTransform(this.map);
                float t = Mathf.InverseLerp(0f, reachMaxSpeedTime, timer);
                float currentMoveSpeed =
                    Mathf.Lerp(minMoveSpeed, maxMoveSpeed, this.speedCurve.Evaluate(t));

                Vector2 currentPosition = powerup.position;

                Vector2 targetWorldPosition = new Vector3(0, 0.2f, 0) + targetTransform.position;
                Vector3 heading = targetWorldPosition - currentPosition;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                if (distance > currentMoveSpeed)
                {
                    powerup.position += direction * currentMoveSpeed;
                    moving = true;
                }
            }

            yield return new WaitForEndOfFrame();
        }

        this.map.timerCollected += 10;
        IngameUI.instance.ShowTimerCollected(this.map, 10);

        this.chunk.items.Remove(this);
        Destroy(this.gameObject);
    }

    public void SetLayerOrder(int setNumber)
    {
        this.sr.sortingOrder = setNumber;
    }
}

