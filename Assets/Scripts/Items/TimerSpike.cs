﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TimerSpike : Item
{
    public TimerButton.Color color;
    public GameObject graphicsGO;

    [Header("Background")]
    public Transform bgTransform;
    
    [Header("Border")]
    public Animated.Animation appearAnimation;
    public Animated.Animation shownAnimation;
    public Animated.Animation disappearAnimation;
    public Animated.Animation hideAnimation;
    public Animated.Animation flushAnimation;

    [Header("Top")]
    public Animated topAnimated;
    public Animated.Animation topActiveAnimation;

    [Header("Shade")]
    public Transform shadeTransform;

    private bool isActive = false;
    private int activeTimer = 0;
    private int shadeTime = 0;

    private static Vector2 TopInitialPos = new Vector2(-0.495f, -1f);
    private static Vector2 TopTargetPos = new Vector2(-0.495f, -1.6f);
    private static Vector2 BackgroundInitialScale = new Vector2(1.25f, 0.9f);
    private static Vector2 BackgroundTargetScale = new Vector2(1.25f, 0.3f);
    
    public override void Init(Def def)
    {
        base.Init(def);

        this.graphicsGO.SetActive(false);
        this.activeTimer = 0;
        
        this.hitboxes = new Hitbox[0];

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        ResetGraphics();

        float flushActiveAnimTime =
            (float)this.flushAnimation.sprites.Length / (float)this.flushAnimation.fps;
        this.shadeTime = Mathf.RoundToInt(flushActiveAnimTime * 60) + TimerBlock.ExtraActiveTime;
    }

    private void SetupHitboxes()
    {
        this.hitboxes = new [] {
            new Hitbox(-0.7f, 0.7f, -0.7f, 0.7f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        if (this.group != null)
        {
            this.transform.position = this.position = this.group.FollowerPosition(this);
        }

        if(this.activeTimer > 0)
        {
            this.activeTimer -= 1;

            float timeWithoutShade = Mathf.InverseLerp(TimerBlock.ActiveTotalTime, this.shadeTime, this.activeTimer);

            if(timeWithoutShade == 1f && this.shadeTransform.gameObject.activeSelf == true)
            {
                this.animated.PlayOnce(this.flushAnimation);

                this.shadeTransform.gameObject.SetActive(false);
                this.topAnimated.gameObject.SetActive(false);
                this.bgTransform.gameObject.SetActive(false);
            }
            else
            {
                this.topAnimated.transform.localPosition =
                    Vector2.Lerp(TopInitialPos, TopTargetPos, timeWithoutShade);

                this.bgTransform.transform.localScale =
                    Vector3.Lerp(BackgroundInitialScale, BackgroundTargetScale, timeWithoutShade);
            }

            if(this.activeTimer <= 0)
            {
                this.animated.PlayOnce(this.disappearAnimation, Deactivate);
            }
        }

        if(this.isActive == true && this.hitboxes.Length > 0)
        {
            var thisRect = this.hitboxes[0].InWorldSpace();
            if (thisRect.Overlaps(this.map.player.Rectangle()))
            {
                this.map.player.Hit(this.position);
            }
            else
            {
                foreach (var enemy in this.chunk.subsetOfItemsOfTypeWalkingEnemy)
                {
                    if (enemy.hitboxes[0].InWorldSpace().Overlaps(thisRect))
                        enemy.Kill(new Enemy.KillInfo { itemDeliveringKill = this });
                }
            }
        }
    }

    public void Activate()
    {
        bool wasActive = this.isActive;
        this.isActive = true;

        SetupHitboxes();
        
        if(wasActive == true)
        {
            this.animated.PlayOnce(this.shownAnimation);
            this.graphicsGO.SetActive(true);
            this.shadeTransform.gameObject.SetActive(true);
            this.topAnimated.gameObject.SetActive(true);
            this.bgTransform.gameObject.SetActive(true);

            this.activeTimer = TimerBlock.ActiveTotalTime;
        }
        else
        {
            this.animated.PlayOnce(this.appearAnimation, () =>
            {
                this.animated.PlayOnce(this.shownAnimation);
                this.graphicsGO.SetActive(true);
                this.activeTimer = TimerBlock.ActiveTotalTime;
            });
        }
    }

    private void Deactivate()
    {
        if (this.isActive == false) return;
        this.isActive = false;
        this.activeTimer = 0;

        this.graphicsGO.SetActive(false);
        this.hitboxes = new Hitbox[0];
        
        ResetGraphics();
    }

    private void ResetGraphics()
    {
        this.animated.PlayOnce(this.hideAnimation);
        this.topAnimated.PlayAndLoop(this.topActiveAnimation);

        this.topAnimated.transform.localPosition = TopInitialPos;
        this.bgTransform.transform.localScale = BackgroundInitialScale;
        
        this.shadeTransform.gameObject.SetActive(true);
        this.topAnimated.gameObject.SetActive(true);
        this.bgTransform.gameObject.SetActive(true);
    }

    public override void Reset()
    {
        base.Reset();

        if(this.isActive == true)
        {
            this.animated.PlayOnce(this.disappearAnimation, Deactivate);
        }
        else
        {
            Deactivate();
        }

    }
}
