
using UnityEngine;
using System.Linq;

public class TogglePlatform : Item
{
    public Sprite activeSprite;
    public Sprite inactiveSprite;

    private Button[] buttons;

    private bool wideTopOnly;
    private bool active;

    public override void Init(Def def)
    {
        base.Init(def);

        this.wideTopOnly = def.tile.tile.Contains("3x1");
        SetActive(def.tile.tile.EndsWith("_a"));

        Button attachedButton = null;
        var nearestNode = this.chunk.connectionLayer.NearestNodeTo(this.position, 4);
        if (nearestNode != null)
        {
            var otherNode = nearestNode.Previous() ?? nearestNode.Next();
            attachedButton = this.chunk.NearestItemTo<Button>(otherNode.Position);
        }
        this.buttons = attachedButton != null ?
            new [] { attachedButton } :
            this.chunk.items.OfType<Button>().ToArray();

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        foreach (var btn in this.buttons)
        {
            if (btn.PressedThisFrame == true)
                SetActive(!this.active);
        }
    }

    private void SetActive(bool active)
    {
        this.active = active;

        if (this.active == true)
        {
            this.spriteRenderer.sprite = this.activeSprite;
            this.hitboxes = new [] { PlatformHitbox() };
        }
        else
        {
            this.spriteRenderer.sprite = this.inactiveSprite;
            this.hitboxes = new Hitbox[0];
        }
    }

    private Hitbox PlatformHitbox() =>
        this.wideTopOnly == true ?
        new Hitbox(0, Tile.Size * 3, 0, Tile.Size, this.rotation, Hitbox.SolidOnTop) :
        new Hitbox(0, Tile.Size    , 0, Tile.Size, this.rotation, Hitbox.Solid, true, true);
}
