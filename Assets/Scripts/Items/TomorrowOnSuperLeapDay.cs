using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TomorrowOnSuperLeapDay : Item
{
    public ThemesAsset themesAsset;

    public Animated kid;
    public Animated cameraMan;
    public Animated newsAnchor;
    public Animated.Animation animationCameramanIdle;
    public Animated.Animation animationKidIdle;
    public Animated.Animation animationNewsAnchorSwitchCards;
    public Animated.Animation animationNewsAnchorTalk;
    public Animated.Animation animationSunkenIslandBubbles;
    public Animated.Animation animationWindySkiesPropeller;
    public Animated.Animation animationWindySkiesParachute;

    public Transform BgConflictCanyon;
    public Transform BgCapitalHighway;
    public Transform BgRainyRuins;
    public Transform BgGravityGalaxy;
    public Transform BgHotNCold;
    public Transform BgSunkenIsland;
    public Transform BgTreasureMines;
    public Transform BgWindySkies;
    public Transform BgTombstoneHill;
    public Transform BgMoltenFortress;

    public SpriteRenderer blank;

    public GameObject themeText;
    public Renderer tomorrowText;

    private Tweener tweener;

    private Theme themeTomorrow;

    private Vector2 textTomStart = new Vector2(0, 3.5f);
    private Vector2 textTomEnd = new Vector2(0, -1f);

    private Vector2 textThemeStart = new Vector2(0.1f, 6.69f);
    private Vector2 textThemeEnd = new Vector2(0.1f, -1f);

    private float anchorTimer = 0;
    private float panelTimer = 0;
    private float moveTime = 0;

    private bool TomTextHidden = false;
    private bool themeTextHidden = false;
    private bool panelVisible = false;
    private Color bgColor;

    private bool resetMovement = false;
    private bool isMoving = true;

    private const float TotalTime = 6f;

    public Transform BackgroundTransform(Theme theme) => theme switch
    {
        Theme.CapitalHighway    => this.BgCapitalHighway,
        Theme.ConflictCanyon    => this.BgConflictCanyon,
        Theme.GravityGalaxy     => this.BgGravityGalaxy,
        Theme.HotNColdSprings   => this.BgHotNCold,
        Theme.SunkenIsland      => this.BgSunkenIsland,
        Theme.TreasureMines     => this.BgTreasureMines,
        Theme.WindySkies        => this.BgWindySkies,
        Theme.TombstoneHill     => this.BgTombstoneHill,
        Theme.MoltenFortress    => this.BgMoltenFortress,
        _                       => this.BgRainyRuins
    };

    public override void Init(Def def)
    {
        base.Init(def);

        this.kid.PlayAndLoop(this.animationKidIdle);
        this.cameraMan.PlayAndLoop(this.animationCameramanIdle);
        this.newsAnchor.PlayAndLoop(this.animationNewsAnchorTalk);

        this.blank = this.transform.Find("Blank").GetComponent<SpriteRenderer>();

        var theme = this.themesAsset.ThemeForDate(Game.selectedDate.AddDays(1));
        this.themeTomorrow = theme.theme;

        if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateTombstoneHillLevel)
            this.themeTomorrow = Theme.TombstoneHill;
        else if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateMoltenFortressLevel)
            this.themeTomorrow = Theme.MoltenFortress;
        else if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateGravityGalaxyLevel)
            this.themeTomorrow = Theme.GravityGalaxy;

        this.themeText.GetComponent<LocalizeText>().SetText(theme.richText.ToUpper());
        this.blank.color = ThemesAsset.CircleColor(this.themeTomorrow);
        this.bgColor = ThemesAsset.CircleColor(this.themeTomorrow);
        if(themeTomorrow == Theme.TreasureMines)
        {
            this.bgColor = new Color(0.9101f, 0.7109f, 0.1289f, 1);
        }

        foreach (var otherTheme in (Theme[])System.Enum.GetValues(typeof(Theme)))
        {
            BackgroundTransform(otherTheme).gameObject.SetActive(false);
        }

        BackgroundTransform(this.themeTomorrow).gameObject.SetActive(true);

        ResetBG();

        this.panelTimer = 0;
        this.anchorTimer = 0;

        this.themeText.gameObject.SetActive(false);

        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
        {
            this.tomorrowText.transform.GetChild(0).gameObject.SetActive(false);
            this.themeText.transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    public override void Advance()
    {
        base.Advance();

        this.anchorTimer += 1 / 60f;

        if(this.anchorTimer > 4f)
        {
            this.anchorTimer = 0;
            this.newsAnchor.PlayOnce(this.animationNewsAnchorSwitchCards, () =>
            {
                this.newsAnchor.PlayAndLoop(this.animationNewsAnchorTalk);
            });
        }

        this.tweener.Advance(1 / 60f);

        if(this.isMoving == true)
        {
            if(this.panelTimer < TotalTime && this.resetMovement == false)
            {
                this.panelTimer += 1 / 60f;
            }
            else
            {
                this.resetMovement = false;
                this.TomTextHidden = false;
                this.themeTextHidden = true;

                this.panelTimer = 0;
                this.tomorrowText.gameObject.SetActive(true);

                Color c = this.tomorrowText.GetComponent<TextMeshPro>().color;
                this.tomorrowText.GetComponent<TextMeshPro>().color = new Color(c.r, c.g, c.b, 0);
                StartCoroutine(FadeAlpha(this.tomorrowText.GetComponent<TextMeshPro>(), 1, .5f));

                this.panelVisible = false;
                this.themeText.gameObject.SetActive(false);
            }

            if(this.panelTimer < TotalTime / 2)
            {
                this.tomorrowText.transform.parent.localPosition = Vector2.Lerp(textTomStart, textTomEnd, (panelTimer * 2) / (TotalTime));
                if(this.panelTimer + 1f > TotalTime / 2 && this.TomTextHidden == false)
                {
                    this.TomTextHidden = true;
                    StartCoroutine(FadeAlpha(this.tomorrowText.GetComponent<TextMeshPro>(), 0, .5f));
                }
            }
            else if(panelTimer < TotalTime - 1f)
            {
                this.tomorrowText.gameObject.SetActive(false);
                this.themeText.gameObject.SetActive(true);

                this.themeText.transform.parent.localPosition = Vector2.Lerp(this.textThemeStart, this.textThemeEnd, (this.panelTimer - (TotalTime / 2)) / TotalTime);
                if(this.themeTextHidden == true && this.panelTimer + 1f < TotalTime)
                {
                    this.themeTextHidden = false;
                    Color c = this.themeText.GetComponent<TextMeshPro>().color;
                    StartCoroutine(FadeAlpha(this.themeText.GetComponent<TextMeshPro>(), 1, .5f));
                }

                if(this.panelVisible == false && this.panelTimer + 1.5f > TotalTime)
                {
                    StartCoroutine(ShowPanel());
                    this.panelVisible = true;
                }

                if(this.panelTimer + 1f > TotalTime && this.themeTextHidden == false)
                {
                    this.themeTextHidden = true;
                    Color c = this.themeText.GetComponent<TextMeshPro>().color;
                }
            }
            else
            {
                if(this.panelVisible == true)
                {
                    this.panelVisible = false;
                    StartCoroutine(HidePanel());
                    StartCoroutine(FadeAlpha(themeText.GetComponent<TextMeshPro>(), 0, .7f));
                }
            }
            this.moveTime = 0;
        }
        else
        {
            this.moveTime += 1 / 60f;
            if(moveTime > 1.5f)
            {
                this.moveTime = 0;
                Color textColor = themeText.GetComponent<TextMeshPro>().color;
                this.themeText.GetComponent<TextMeshPro>().color = new Color(textColor.r, textColor.g, textColor.b, 0);
                Color colorEnd = new Color(bgColor.r, bgColor.g, bgColor.b, 0);
                this.blank.color = colorEnd;
                this.resetMovement = true;
                this.isMoving = true;
                this.blank.gameObject.SetActive(false);
                this.themeText.gameObject.SetActive(false);
            }
        }
    }

    private IEnumerator FadeAlpha(TextMeshPro text, float alpha, float time)
    {
        float timer = 0;
        Color colorStart = text.color;
        Color colorFinal = new Color(colorStart.r, colorStart.g, colorStart.b, alpha);

        text.color = colorStart;
        while(timer < time)
        {
            timer += Time.deltaTime;
            text.color = Color.Lerp(colorStart, colorFinal, timer / time);
            yield return null;
        }
    }

    private IEnumerator ShowPanel()
    {
        this.blank.gameObject.SetActive(true);
        float timer = 0;
        Color colorEnd = bgColor;
        Color colorStart = new Color(colorEnd.r, colorEnd.g, colorEnd.b, 0);
        while(timer < .3f)
        {
            timer += Time.deltaTime;
            this.blank.color = Color.Lerp(colorStart, colorEnd, timer / .3f);
            yield return null;
        }
    }

    private IEnumerator HidePanel()
    {
        float timer = 0;
        Color colorStart = bgColor;
        Color colorEnd = new Color(colorStart.r, colorStart.g, colorStart.b, 0);

        this.isMoving = false;
        ResetBG();
        yield return new WaitForSeconds(.5f);
        this.panelTimer = 0;
        StartCoroutine(FadeAlpha(themeText.GetComponent<TextMeshPro>(), 0, .3f));

        while(timer < .3f)
        {
            timer += Time.deltaTime;
            this.blank.color = Color.Lerp(colorStart, colorEnd, timer / .3f);
            yield return null;
        }

        this.blank.color = colorEnd;
        this.resetMovement = true;
        this.isMoving = true;
        this.blank.gameObject.SetActive(false);
        this.themeText.gameObject.SetActive(false);
    }


    private void ResetBG()
    {
        this.tweener = new Tweener();
        var container = BackgroundTransform(this.themeTomorrow);

        switch(this.themeTomorrow)
        {
            case Theme.ConflictCanyon:
                container.Find("BG").localPosition = new Vector2(0, 4);
                container.Find("bridge").localPosition = new Vector2(0, 3.38f);
                container.Find("mountain1").localPosition = new Vector2(2.2f, 2.28f);
                container.Find("mountain2").localPosition = new Vector2(-2.85f, 2.28f);
                container.Find("cactus").localPosition = new Vector2(2.93f, 2.24f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, -.5f), TotalTime);
                this.tweener.MoveLocal(container.Find("bridge"), new Vector2(0, -1f), TotalTime);
                this.tweener.MoveLocal(container.Find("mountain1"), new Vector2(2.2f, .2f), TotalTime);
                this.tweener.MoveLocal(container.Find("mountain2"), new Vector2(-2.85f, 0), TotalTime);
                this.tweener.MoveLocal(container.Find("cactus"), new Vector2(2.93f, -3.5f), TotalTime);
                break;

            case Theme.CapitalHighway:
                container.Find("BG").localPosition = new Vector2(0, -2);
                container.Find("highway").localPosition = new Vector2(0, -1.88f);
                container.Find("platform").localPosition = new Vector2(0, -4.21f);
                container.Find("sign").localPosition = new Vector2(-3, -1.6f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, -4f), TotalTime);
                this.tweener.MoveLocal(container.Find("highway"), new Vector2(0, -5f), TotalTime);
                this.tweener.MoveLocal(container.Find("platform"), new Vector2(0, -7.67f), TotalTime);
                this.tweener.MoveLocal(container.Find("sign"), new Vector2(-3, -6.84f), TotalTime);
                break;

            case Theme.RainyRuins:
                container.Find("BG").localPosition = new Vector2(0, 4f);
                container.Find("column").localPosition = new Vector2(0, 2f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, 2), TotalTime);
                this.tweener.MoveLocal(container.Find("column"), new Vector2(0, -2.5f), TotalTime);
                break;

            case Theme.GravityGalaxy:
                container.Find("BG").localPosition = new Vector2(0, 2f);
                container.Find("asteroids").localPosition = new Vector2(0, 1f);
                container.Find("yolk").localPosition = new Vector2(1.39f, 4.21f);
                container.Find("planet").localPosition = new Vector2(3.32f, 3.52f);
                container.Find("station").localPosition = new Vector2(-3, 4);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, 1.5f), TotalTime);
                this.tweener.MoveLocal(container.Find("asteroids"), new Vector2(0, 0f), TotalTime);
                this.tweener.MoveLocal(container.Find("yolk"), new Vector2(1.39f, .5f), TotalTime);
                this.tweener.MoveLocal(container.Find("planet"), new Vector2(3.32f, 2f), TotalTime);
                this.tweener.MoveLocal(container.Find("station"), new Vector2(-3, -2), TotalTime);
                break;

            case Theme.HotNColdSprings:
                container.Find("BG").localPosition = new Vector2(0, 3.35f);
                container.Find("mountain").localPosition = new Vector2(0, 1.1f);
                container.Find("pools").localPosition = new Vector2(0, 1.48f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, 2.25f), TotalTime);
                this.tweener.MoveLocal(container.Find("mountain"), new Vector2(0, -1.06f), TotalTime);
                this.tweener.MoveLocal(container.Find("pools"), new Vector2(0, -2.08f), TotalTime);
                break;

            case Theme.SunkenIsland:
                container.Find("BG").localPosition = new Vector2(0, -2.35f);
                container.Find("cloud").localPosition = new Vector2(3.53f, 1.68f);
                container.Find("mountain").localPosition = new Vector2(0, 4.04f);
                container.Find("bridge").localPosition = new Vector2(0, 2.18f);
                container.Find("water").localPosition = new Vector2(0, -9.3f);
                container.Find("water/bubbles").GetComponent<Animated>().PlayAndLoop(this.animationSunkenIslandBubbles);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0, 5.74f), TotalTime);
                this.tweener.MoveLocal(container.Find("cloud"), new Vector2(3.53f, 4.62f), TotalTime);
                this.tweener.MoveLocal(container.Find("mountain"), new Vector2(0, 5.93f), TotalTime);
                this.tweener.MoveLocal(container.Find("bridge"), new Vector2(0, -2.58f), TotalTime);
                this.tweener.MoveLocal(container.Find("water"), new Vector2(-14.31f, -5.2f), TotalTime);
                break;

            case Theme.TreasureMines:
                container.Find("BG").localPosition = new Vector2(2.25f, 0.225f);
                container.Find("BG1").localPosition = new Vector2(3.13f, 2.78f);
                container.Find("BG2").localPosition = new Vector2(3.13f, 1.9f);
                container.Find("Rail").localPosition = new Vector2(3.48f, 0.09f);
                container.Find("Mull").localPosition = new Vector2(-9.62f, 2.16f);
                container.Find("Yolk").localPosition = new Vector2(-6.01f, 1.66f);
                container.Find("Glow").localPosition = new Vector2(-0.16f, -1.69f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0.5f, 0.225f), TotalTime);
                this.tweener.MoveLocal(container.Find("BG1"), new Vector2(0f, 2.78f), TotalTime);
                this.tweener.MoveLocal(container.Find("BG2"), new Vector2(-1.79f, 1.9f), TotalTime);
                this.tweener.MoveLocal(container.Find("Rail"), new Vector2(-6.3f, 0.09f), TotalTime);
                this.tweener.MoveLocal(container.Find("Mull"), new Vector2(6.06f, 2.16f), TotalTime);
                this.tweener.MoveLocal(container.Find("Yolk"), new Vector2(9.67f, 1.66f), TotalTime);
                break;

            case Theme.WindySkies:
                container.Find("BG").localPosition = new Vector2(0.75f, 2.15f);
                container.Find("BG1").localPosition = new Vector2(1.49f, 3.61f);
                container.Find("cloud").localPosition = new Vector2(1.25f, 1.99f);
                container.Find("balloon1").localPosition = new Vector2(-3.59f, 3.75f);
                container.Find("balloon1/propeller").GetComponent<Animated>().PlayAndLoop(this.animationWindySkiesPropeller);
                container.Find("balloon2").localPosition = new Vector2(3.51f, 4.2f);
                container.Find("balloon2/propeller").GetComponent<Animated>().PlayAndLoop(this.animationWindySkiesPropeller);
                container.Find("yolk").localPosition = new Vector2(-2.57f, 4.99f);
                container.Find("yolk/parachute").GetComponent<Animated>().PlayAndLoop(this.animationWindySkiesParachute);
                container.Find("gradient").localPosition = new Vector2(0, -4f);
                this.tweener.MoveLocal(container.Find("BG"), new Vector2(0.75f, 2.87f), TotalTime);
                this.tweener.MoveLocal(container.Find("BG1"), new Vector2(-1.26f, 6.56f), TotalTime);
                this.tweener.MoveLocal(container.Find("cloud"), new Vector2(-6.65f, 4.59f), TotalTime);
                this.tweener.MoveLocal(container.Find("balloon1"), new Vector2(-3.59f, 5.2f), TotalTime);
                this.tweener.MoveLocal(container.Find("balloon2"), new Vector2(3.51f, 6.47f), TotalTime);
                this.tweener.MoveLocal(container.Find("yolk"), new Vector2(3.25f, 3.06f), TotalTime);
                this.tweener.MoveLocal(container.Find("gradient"), new Vector2(0f, 5f), TotalTime);
                break;

            case Theme.TombstoneHill:
                container.Find("fg").localPosition = new Vector2(-11.5f, 0);
                container.Find("cloud 1").localPosition = new Vector2(-13, 0.5f);
                container.Find("bg 1").localPosition = new Vector2(-13, 0.5f);
                container.Find("cloud 2").localPosition = new Vector2(-11, 0.5f);
                container.Find("bg 2").localPosition = new Vector2(-18, 0.5f);
                this.tweener.MoveLocal(container.Find("fg"), new Vector2(-11.5f + 6, 0), TotalTime);
                this.tweener.MoveLocal(container.Find("cloud 1"), new Vector2(-13f + 5, 0.5f), TotalTime);
                this.tweener.MoveLocal(container.Find("bg 1"), new Vector2(-13f + 4, 0.5f), TotalTime);
                this.tweener.MoveLocal(container.Find("cloud 2"), new Vector2(-11f + 3, 0.5f), TotalTime);
                this.tweener.MoveLocal(container.Find("bg 2"), new Vector2(-18f + 2, 0.5f), TotalTime);
                break;

            case Theme.MoltenFortress:
                container.Find("wall left").localPosition = new Vector2(-4.6f, -0.4f);
                container.Find("wall right").localPosition = new Vector2(+4.7f, -0.4f);
                container.Find("lavafalls").localPosition = new Vector2(0.16f, 5);
                container.Find("center column").localPosition = new Vector2(0, 0.2f);
                container.Find("player").localPosition = new Vector2(0, 3.5f);
                this.tweener.MoveLocal(container.Find("wall left"), new Vector2(-4.6f, 37), TotalTime);
                this.tweener.MoveLocal(container.Find("wall right"), new Vector2(+4.7f, 37), TotalTime);
                this.tweener.MoveLocal(container.Find("lavafalls"), new Vector2(0, -43), TotalTime);
                this.tweener.MoveLocal(container.Find("center column"), new Vector2(0, 13), TotalTime);
                this.tweener.MoveLocal(container.Find("player"), new Vector2(0, -4.8f), TotalTime);
                break;
        }
    }
}
