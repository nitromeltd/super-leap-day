﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchPlatformBlock : Item
{
    public Autotile[] autotiles;
    public Neighbours myNeighbours;

    private TouchPlatformCog leaderCog;

    public Vector2 EffectiveVelocity => this.leaderCog.EffectiveVelocity;

    [System.Serializable]
    public struct Autotile
    {
        public Sprite[] sprites;
        public bool useCorners;
        public Neighbours neighbours;
    }

    [System.Serializable]
    public struct Neighbours
    {
        public bool up;
        public bool down;
        public bool left;
        public bool right;

        [Header("Corners")]
        public bool upLeft;
        public bool upRight;
        public bool downLeft;
        public bool downRight;

        public Neighbours(bool up, bool down, bool left, bool right,
            bool upLeft, bool upRight, bool downLeft, bool downRight)
        {
            this.up = up;
            this.down = down;
            this.left = left;
            this.right = right;
            this.upLeft = upLeft;
            this.upRight = upRight;
            this.downLeft = downLeft;
            this.downRight = downRight;
        }
        
        public bool IsEqual(Neighbours n, bool useCorners = false) 
        {
            bool nonCornersEqual = 
                this.up == n.up &&
                this.down == n.down &&
                this.left == n.left &&
                this.right == n.right;

            bool cornersEqual = 
                this.upLeft == n.upLeft &&
                this.upRight == n.upRight &&
                this.downLeft == n.downLeft &&
                this.downRight == n.downRight;

            return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
        }
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.hitboxes = new [] {
            new Hitbox(-0.5f, 0.5f, -0.5f, 0.5f, this.rotation, Hitbox.Solid, true, true)
        };
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        
        this.onCollisionFromAbove = OnCollision;
        this.onCollisionFromBelow = OnCollision;
        this.onCollisionFromLeft  = OnCollision;
        this.onCollisionFromRight = OnCollision;
    }

    public override void Advance()
    {
        if(this.leaderCog == null)
        {
            this.leaderCog = this.group.leader as TouchPlatformCog;
        }
    }
    
    public void OnCollision(Collision collision)
    {
        this.leaderCog.OnCollision(collision);
    }

    public void ApplyAutotiling()
    {
        foreach(TouchPlatformBlock.Autotile at in autotiles)
        {
            if(myNeighbours.IsEqual(at.neighbours, at.useCorners))
            {
                spriteRenderer.sprite =
                    at.sprites[Random.Range(0, at.sprites.Length)];
            }
        }
    }
}
