﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using NitromeEditor;

public class TouchPlatformCog : Item
{
    public Transform cogTransform;

    private NitromeEditor.Path path;
    private bool isMoving;
    private int nodeIndex;
    private float distanceAlongPath;
    private float currentSpeed;
    private TouchPlatformBlock[,] grid;
    private int triggerCooldown;
    private bool printError;
    private Vector2 effectiveVelocity = Vector2.zero;

    private const int TriggerCooldownTime = 15;
    private const float Speed = 0.1f / 16f;

    private bool previousPlayerInContact;
    private bool currentPlayerInContact;

    private bool playedHitSfx = false;

    public Vector2 EffectiveVelocity => this.effectiveVelocity;

    public override void Init(Def def)
    {
        base.Init(def);

        var node = this.chunk.pathLayer.NearestNodeTo(this.position, 0.25f);

        if(node != null)
        {
            this.path = node.path;
        }

        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));

        this.isMoving = false;
        this.nodeIndex = 0;
        this.distanceAlongPath = 0f;
        this.currentSpeed = 0f;
        this.triggerCooldown = 0;
        
        if (this.path != null)
        {
            float totalDistance = 0f;

            for (int i = 0; i < this.path.edges.Length; i++)
            {
                var edge = this.path.edges[i];

                float edgeMidDistance = totalDistance + (edge.distance / 2f);
                Vector2 edgeMidPoint = path.PointAtDistanceAlongPath(edgeMidDistance).position;
                
                GameObject laneGO = Instantiate(
                    Assets.instance.capitalHighway.touchPlatformPathLane,
                    edgeMidPoint,
                    Quaternion.identity,
                    this.transform.parent
                ).gameObject;
                laneGO.name = "touch platform path lane";

                laneGO.transform.localRotation =
                    Quaternion.LookRotation(Vector3.forward, edge.direction);
                laneGO.transform.localScale = new Vector3(1f, 1f * edge.distance, 1f);

                totalDistance += edge.distance;

                void DrawLaneStop(Vector2 stopPosition)
                {
                    GameObject laneEndGO = Instantiate(
                        Assets.instance.capitalHighway.touchPlatformPathLaneStop,
                        stopPosition,
                        Quaternion.identity,
                        this.transform.parent
                    ).gameObject;
                    laneGO.name = "touch platform path lane stop";
                }

                void DrawLaneEnd(Vector2 endPosition, bool flipped)
                {
                    GameObject laneEndGO = Instantiate(
                        Assets.instance.capitalHighway.touchPlatformPathLaneEnd,
                        endPosition,
                        Quaternion.identity,
                        this.transform.parent
                    ).gameObject;
                    laneGO.name = "touch platform path lane end";

                    laneEndGO.transform.localRotation =
                        Quaternion.LookRotation(Vector3.forward, edge.direction);
                    laneEndGO.transform.localScale = new Vector3(1f, flipped ? -1f : 1f, 1f);
                }

                Vector2 toNodePosition = path.PointAtDistanceAlongPath(totalDistance).position;

                if(this.path.closed == true)
                {
                    DrawLaneStop(toNodePosition);
                }
                else
                {
                    if(i < this.path.edges.Length - 1)
                    {
                        DrawLaneStop(toNodePosition);
                    }

                    if(i == 0)
                    {
                        DrawLaneEnd(path.PointAtDistanceAlongPath(0f).position, false);
                    }

                    if(i == this.path.edges.Length - 1)
                    {
                        DrawLaneEnd(toNodePosition, true);
                    }
                }
            }

            this.path = Util.MakeClosedPathByPingPonging(this.path);
        }
    }

    public override void Advance()
    {
        //first items advance, then eventually collisions take place
        if(previousPlayerInContact && !currentPlayerInContact)
        {
            TriggerMovement();
        }
        previousPlayerInContact = currentPlayerInContact;
        currentPlayerInContact = false;

        if(this.grid == null)
        {
            SetupAutotiling();
        }

        if(this.triggerCooldown > 0)
        {
            this.triggerCooldown -= 1;
        }

        Move();

        float rot = this.currentSpeed * 50f;
        this.cogTransform.Rotate(Vector3.forward * rot);
        
        var lastPosition = this.position;
        this.position =
            this.path.PointAtDistanceAlongPath(this.distanceAlongPath).position;
        this.transform.position = this.position;
        this.effectiveVelocity = this.position - lastPosition;

        foreach(var f in this.group.followers)
        {
            f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
            f.Key.item.transform.position = f.Key.item.position;
        }
    }

    private void Move()
    {
        if (this.isMoving == false)
        {
            return;
        }

        var targetDistance = this.path.PointAtNodeIndex(this.nodeIndex).distanceAlongPath;
        if (this.path.closed == true && this.distanceAlongPath > targetDistance)
        {
            this.currentSpeed += Speed;
            this.distanceAlongPath += this.currentSpeed;

            if (this.distanceAlongPath > this.path.length)
            {
                this.distanceAlongPath = 0f;
                this.currentSpeed *= -0.2f;
            }
        }
        else
        {
            this.currentSpeed += Speed;
            this.distanceAlongPath += this.currentSpeed;

            if(this.distanceAlongPath > targetDistance)
            {
                this.distanceAlongPath = targetDistance;

                if((Mathf.Abs(this.currentSpeed) < Speed))
                {
                    this.currentSpeed = 0f;
                }
                else
                {
                    this.currentSpeed *= -0.2f;
                }
            }
        }

        if (this.distanceAlongPath == targetDistance && this.playedHitSfx == false)
        {
            this.playedHitSfx = true;
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvMovingBlockImpactSmall,
                position: this.position
            );
        }

        if (Mathf.Abs(this.currentSpeed) < 0.001f &&
            this.distanceAlongPath == targetDistance)
        {
            Audio.instance.PlaySfx(
                Assets.instance.sfxEnvMovingBlockImpactSmall,
                position: this.position
            );
            this.isMoving = false;
            this.currentSpeed = 0f;
            this.triggerCooldown = TriggerCooldownTime;
        }
    }

    private void SetupAutotiling()
    {
        List<TouchPlatformBlock> touchBlocks = new List<TouchPlatformBlock>();
        foreach(var f in this.group.followers)
        {
            if(f.Key.item is TouchPlatformBlock)
            {
                touchBlocks.Add(f.Key.item as TouchPlatformBlock);
            }
        }

        if(touchBlocks.Count <= 0)
        {
            if(this.printError == false)
            {
                this.printError = true;
                this.chunk.ReportError(this.position, "Touch Platform cog is missing blocks!");
            }

            return;
        }

        // get smallest coordernates (e.g. x: -1, y: -3)
        int minX = Mathf.FloorToInt(touchBlocks.Min(t => t.position.x));
        int minY = Mathf.FloorToInt(touchBlocks.Min(t => t.position.y));
        
        // get largest coordenates (e.g. x: 8, y: 5)
        int maxX = Mathf.FloorToInt(touchBlocks.Max(t => t.position.x));
        int maxY = Mathf.FloorToInt(touchBlocks.Max(t => t.position.y));

        // get total array size (e.g. x: 10, y: 9)
        int sizeX = (maxX - minX) + 1;
        int sizeY = (maxY - minY) + 1;
        this.grid = new TouchPlatformBlock[sizeX, sizeY];

        // get origin offset (e.g. x: +1, y: +3)
        // we want the bottom left tile to be (0, 0) in our new grid
        int originX = -minX;
        int originY = -minY;
        
        // set the new grid following the offset (e.g. [0, -1] => [1, 2])
        foreach(var t in touchBlocks)
        {
            int x = Mathf.FloorToInt(t.position.x);
            int y = Mathf.FloorToInt(t.position.y);

            int gridX = x + originX;
            int gridY = y + originY;
            this.grid[gridX, gridY] = t;
        }

        // set autotiling values for the grid
        for(int x = 0; x < this.grid.GetLength(0); x++)
        {
            for (int y = 0; y < this.grid.GetLength(1); y++)
            {
                if(this.grid[x, y] == null) continue;

                bool up = IsItemInGrid(x, y + 1);
                bool down = IsItemInGrid(x, y - 1);
                bool left = IsItemInGrid(x - 1, y);
                bool right = IsItemInGrid(x + 1, y);

                bool upLeft = IsItemInGrid(x - 1, y + 1);
                bool upRight = IsItemInGrid(x + 1, y + 1);
                bool downLeft = IsItemInGrid(x - 1, y - 1);
                bool downRight = IsItemInGrid(x + 1, y - 1);

                this.grid[x, y].myNeighbours = new TouchPlatformBlock.Neighbours(
                    up, down, left, right, upLeft, upRight, downLeft, downRight
                );
            }
        }

        // choose autotile sprite
        foreach(var t in touchBlocks)
        {
            t.ApplyAutotiling();
        }
    }

    public bool IsItemInGrid(int x, int y)
    {
        if(x < 0 || x >= this.grid.GetLength(0)) return false;
        if(y < 0 || y >= this.grid.GetLength(1)) return false;

        return this.grid[x, y] != null;
    }
    
    public void OnCollision(Collision collision)
    {
        if(this.isMoving == true) return;
        if(this.triggerCooldown > 0) return;
        currentPlayerInContact = true;
    }

    private void TriggerMovement()
    {
        this.nodeIndex += 1;
        this.nodeIndex %= this.path.nodes.Length;

        this.isMoving = true;
        this.playedHitSfx = false;
    }



    public override void Reset()
    {
        this.isMoving = false;
        this.nodeIndex = 0;
        this.distanceAlongPath = 0f;
        this.currentSpeed = 0f;
        this.triggerCooldown = 0;

        this.position =
            this.path.PointAtDistanceAlongPath(0f).position;
        this.transform.position = this.position;

        foreach(var f in this.group.followers)
        {
            f.Key.item.position = this.group.leader.position + f.Value.relativePosition;
            f.Key.item.transform.position = f.Key.item.position;
        }
    }
}
