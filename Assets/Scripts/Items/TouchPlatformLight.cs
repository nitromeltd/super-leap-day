﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchPlatformLight : Item
{
    public Animated.Animation alertAnimation;
    public Animated.Animation idleAnimation;

    private Vector2 lastPosition;
    private bool hasMoved = false;
    public override void Init(Def def)
    {
        base.Init(def);
        
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
        this.animated.PlayOnce(idleAnimation);
        this.lastPosition = this.position;
        this.hasMoved = false;
    }

    public override void Advance()
    {
        if(this.position != this.lastPosition)
        {
            this.animated.PlayAndLoop(alertAnimation);
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnemyConeChase))
            {
                Audio.instance.PlaySfxLoop(Assets.instance.sfxEnemyConeChase);
            }
            this.hasMoved = true;
        }
        else
        {
            this.animated.PlayOnce(idleAnimation);
            if(this.hasMoved == true)
            {
                Audio.instance.StopSfxLoop(Assets.instance.sfxEnemyConeChase);
                this.hasMoved = false;
            }
        }

        this.lastPosition = this.position;
    }

}
