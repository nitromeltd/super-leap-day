﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrafficCone : Item
{
    public Animated.Animation animationSpin;

    private bool tossed;

    public override void Init(Def def)
    {
        base.Init(def);

        this.tossed = false;

        this.hitboxes = new [] {
            new Hitbox(-0.4f, 0.4f, -0.35f, 0.35f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        base.Advance();

        Player player = this.map.player;

        if(player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace()))
        {
            Toss();
        }
    }

    public void Toss()
    {
        if(this.tossed == true) return;
        
        Player player = this.map.player;

        this.tossed = true;
        this.spriteRenderer.enabled = false;
        
        var deathParticle = Particle.CreatePlayAndLoop(
            this.animationSpin,
            100,
            this.transform.position,
            this.transform.parent
        );

        float direction = player.facingRight ? 1f : -1f;

        float hVel = Mathf.Lerp(0.05f, 0.20f,
            Mathf.InverseLerp(0.10f, 0.25f, player.velocity.x));
        float vVel = Mathf.Lerp(0.25f, 0.40f,
            Mathf.InverseLerp(0f, 1f, player.velocity.y));

        hVel *= direction;

        float gravity = -0.02f;

        deathParticle.velocity     = new Vector2(hVel, vVel);
        deathParticle.acceleration = new Vector2(0f, gravity);

        deathParticle.transform.localScale = new Vector3(-direction, 1f, 1f);
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvConeBump,
            position: this.position
        );
    }

    public override void Reset()
    {
        base.Reset();

        this.tossed = false;
        this.spriteRenderer.enabled = true;
    }

}
