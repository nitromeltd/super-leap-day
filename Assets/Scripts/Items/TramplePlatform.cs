﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TramplePlatform : Item
{
    public Animated.Animation animationStatic;
    public Animated.Animation animationUp;
    public Animated.Animation animationDown;
    public Animated.Animation animationTrampledStatic;
    public Animated.Animation animationTrampledUp;
    public Animated.Animation animationTrampledDown;
    
    private bool canBounce;
    private bool trampled;
    private int correctionTime;
    private Vector2 correction;
    [HideInInspector] public bool collideWithPlayer;

    public const float VerticalForce = 1f;
    public const float HorizontalForce = 0.50f;

    private bool IsThisClosestTramplePlatform()
    {
        var player = this.map.player;

        float closestSqDistance = Mathf.Infinity;
        TramplePlatform closestTramplePlatform = null;

        var candidates = new List<TramplePlatform>();
        foreach (var tp in this.chunk.items.OfType<TramplePlatform>())
        {
            if (tp.hitboxes[0].InWorldSpace().Overlaps(player.Rectangle()) == false)
                continue;

            float currentSqDistance = (player.position - tp.position).sqrMagnitude;
            if (currentSqDistance >= closestSqDistance)
                continue;

            closestSqDistance = currentSqDistance;
            closestTramplePlatform = tp;
        }

        return closestTramplePlatform == this;
    }

    public static bool IsOnTopOfAnyTrample(Player player)
    {
        var chunk = player.map.NearestChunkTo(player.position);

        var candidates = chunk.items
            .OfType<TramplePlatform>()
            .Where(c => c.collideWithPlayer == true)
            .ToArray();

        return candidates.Length > 0;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        if (def.tile.flip == true)
        {
            this.transform.rotation = Quaternion.Euler(0, 0, this.rotation);
        }

        this.canBounce = true;
        this.trampled = false;
        this.collideWithPlayer = false;
        this.animated.PlayOnce(this.animationStatic);

        SetHitbox();
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));
    }

    private void SetHitbox()
    {
        this.hitboxes = new [] {
            new Hitbox(-1f, 1f, 0, 1f, this.rotation, Hitbox.NonSolid)
        };
    }

    public override void Advance()
    {
        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        var player = this.map.player;
        var thisRect = this.hitboxes[0].InWorldSpace();
        this.collideWithPlayer =
            player.ShouldCollideWithItems() &&
            player.Rectangle().Overlaps(thisRect);

        if (this.correctionTime > 0)
        {
            this.correctionTime -= 1;
            if (this.correctionTime < 1)
            {
                if (player.state == Player.State.SpringFromPushPlatform)
                    player.velocity += this.correction;

                this.correction = new Vector2();
            }
        }

        if(this.canBounce == true)
        {
            if(this.trampled == false && collideWithPlayer == true)
            {
                this.trampled = true;

                this.animated.PlayOnce(this.animationTrampledDown, () =>
                {
                    this.animated.PlayOnce(this.animationTrampledStatic);
                });
            }

            if(this.trampled == true && collideWithPlayer == false)
            {
                this.trampled = false;

                this.animated.PlayOnce(this.animationTrampledUp, () =>
                {
                    this.animated.PlayOnce(this.animationStatic);
                });
            }

            if (collideWithPlayer == true &&
                GameInput.Player(this.map).pressedJump == true)
            {
                Bounce();
            }
        }
    }

    public void PlayBounceAnimation()
    {
        Audio.instance.PlaySfx(
            Assets.instance.sfxEnvNoGrabBlockOut,
            position: this.position
        );
        this.animated.PlayOnce(this.animationUp, delegate
        {
            this.animated.PlayOnce(this.animationDown, delegate
            {
                this.canBounce = true;
            });
        });
    }
    
    public Vector2 VelocityAfterSpringing(Vector2 before)
    {
        switch (this.rotation)
        {
            case 0:    return new Vector2(0f, VerticalForce);
            case 90:   return new Vector2(-HorizontalForce, 0f);
            case 180:  return new Vector2(0f, -VerticalForce);
            case 270:  return new Vector2(HorizontalForce, 0f);
        }

        return before;
    }

    private void Bounce()
    {
        if(this.canBounce == false) return;

        var player = this.map.player;
        if(player.state == Player.State.SpringFromPushPlatform) return;
        if(player.ShouldCollideWithItems() == false) return;
        if(IsThisClosestTramplePlatform() == false) return; 

        this.canBounce = false;
        PlayBounceAnimation();

        player.CancelLateJumpTime();
        player.ResetDoubleJump();
        player.velocity = VelocityAfterSpringing(player.velocity);

        var offset = player.position - (Vector2)this.transform.position;

        if (player.state == Player.State.WallSlide ||
            player.HasWallOnSide(player.facingRight, 0.5f))
        {
            player.facingRight = !player.facingRight;
            player.velocity.x =
                Mathf.Abs(player.velocity.x) * (player.facingRight ? 1 : -1);
        }
        
        player.state = Player.State.SpringFromPushPlatform;
        player.groundState = Player.GroundState.Airborne(player);
        player.position += player.velocity * 0.3f;

        {
            const int Duration = 3;

            Vector2 parallel = player.velocity.normalized;
            Vector2 perpendicular = new Vector2(parallel.y, -parallel.x);
            Vector2 correction = perpendicular * Vector2.Dot(-offset, perpendicular);

            player.velocity += correction / Duration;
            this.correction = -correction / Duration;
            this.correctionTime = Duration;
        }
    }

    public override void UpdateHitboxesForSpinBlock(int newRotation)
    {
        this.rotation = newRotation;
        SetHitbox();
    }
}
