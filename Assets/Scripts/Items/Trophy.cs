﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;
using TMPro;

public class Trophy : Item
{
    public enum TrophyType
    {
        BRONZE,
        SILVER,
        GOLD
    }

    public TrophyType trophyType;

    public Animated.Animation animationHide;
    public Animated.Animation animationDrop;
    public Animated.Animation animationWobble;
    public Animated.Animation animationShine;
    public Animated.Animation animationFire;
    public Animated.Animation animationBlank;
    public Animated.Animation animationSwing;
    
    [Header("Plaque")]
    //public Animated plaqueAnimated;
    public Animated.Animation animationPlaqueIdle;
    public Animated.Animation animationPlaqueShine;

    [Header("Blender")]
    public Animated.Animation animationBlender;
    public Animated.Animation animationBlenderJuice;
    public Animated.Animation animationBlenderJuiceIdle;
    public Animated.Animation animationBlenderJuiceSplash;
    public Animated.Animation animationStarEffect;
    public GameObject fruitPrefab;

    [Header("Arrow")]
    public Animated arrowAnimated;
    public Animated.Animation animationArrowBlank;
    public Animated.Animation animationArrowMove;

    [Header("Effects")]
    public Animated appearAnim;
    public Animated.Animation animationAppear;
    
    public AnimationCurve flashFadeIn;
    public AnimationCurve flashFadeOut;

    public Transform whiteLinesParent;
    public Transform[] whiteLines;

    [Header("Hop")]
    public float hopForce;

    private Vector2 velocity;
    private bool checkForPlayerNear = true;
    private bool checkForPlayerEnter = true;
    private bool canDrop = false;
    private bool onGround = false;
    private bool canShine = false;
    private bool haveLid = false;
    private int shineTimer = 0;
    private bool rotateWhiteLines = false;
    private bool isFiring = false;
    private bool isSwinging = false;
    [NonSerialized] public bool hasCreatedRefree = false;
    [NonSerialized] public bool inEntranceLift = false;
    //private bool hasBeenOnGroundOnce = false;

    private TrophyGoldLid lid;
    private Hitbox[] rectanglesWithTop;

    private Referee referee;

    private GameObject trophyBase;
    private Animated juice;
    private Animated blade;
    private Transform bgTransform;
    private Transform starLine;
    private TextMeshPro fruitCounterText;
    private static Vector2 TopInitialPos = new Vector2(0f, -1.3f);
    private static Vector2 TopFinalPos = new Vector2(0f, 0.7f);
    private static Vector2 BackgroundInitialScale = new Vector2(6.26f, 0f);
    private static Vector2 BackgroundFinalScale = new Vector2(6.26f, 1.8f);
    private bool fruitsMoving = false;

    private AudioClip TrophyAppearSFX() =>
        this.trophyType == TrophyType.BRONZE ? Assets.instance.sfxSparkleBronzeTrophyAppear :
        (this.trophyType == TrophyType.SILVER ? Assets.instance.sfxSparkleSilverTrophyAppear :
        Assets.instance.sfxSparkleGoldTrophyAppear);

    private AudioClip TrophyFanfare() =>
    this.trophyType == TrophyType.BRONZE ? Assets.instance.sfxFanfareBronzeSliver :
    (this.trophyType == TrophyType.SILVER ? Assets.instance.sfxFanfareBronzeSliver :
    Assets.instance.sfxFanfareGold);

    private AudioClip TrophyWobbleSFX() =>
        this.trophyType == TrophyType.BRONZE ? Assets.instance.sfxWobbleBronzeTrophy : Assets.instance.sfxWobbleSilverTrophy;

    private AudioClip TrophyJumpSFX() =>
        this.trophyType == TrophyType.BRONZE ? Assets.instance.sfxJumpBronzeTrophy : Assets.instance.sfxJumpSilverTrophy;

    private const float GRAVITY_FORCE = 0.02f;
    private const int SHINE_TIME = 30 * 5;
    private static Vector2 PLAYER_CHUNK_OFFSET = Vector2.up * -.25f;
    private static Vector2 DROP_POS_OFFSET = Vector2.up * 6f;
    private const int SPARKLES_AMOUNT = 10;
    
    public override void Init(Def def)
    {
        base.Init(def);

        if(this.trophyType == TrophyType.GOLD)
        {
            this.trophyBase = this.transform.Find("Trophy Base").gameObject;
            this.juice = this.trophyBase.transform.Find("graphics/juice").GetComponent<Animated>();
            this.blade = this.trophyBase.transform.Find("blade").GetComponent<Animated>();
            this.fruitCounterText = this.trophyBase.transform.Find("Text").GetComponent<TextMeshPro>();
            this.bgTransform = this.trophyBase.transform.Find("graphics/BG full").GetComponent<Transform>();
            this.starLine = this.trophyBase.transform.Find("star_line").GetComponent<Transform>();
            this.juice.gameObject.SetActive(false);
            this.bgTransform.gameObject.SetActive(false);
            this.fruitCounterText.text = 0.ToString() + "%";
            this.position += Vector2.up * 4.15f;
            this.transform.position = this.position;
            this.trophyBase.transform.SetParent(this.transform.parent);
            // change bottom blender parent to avoid moving it when moving the gold cup
        }

        switch(trophyType)
        {
            case TrophyType.BRONZE:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1f, 1.25f, 0, 3f, this.rotation, Hitbox.NonSolid)
                };
            }
            break;

            case TrophyType.SILVER:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1f, 1.25f, 0, 3f, this.rotation, Hitbox.NonSolid)
                };
            }
            break;

            case TrophyType.GOLD:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1f, 1.25f, 0f, 3f, this.rotation, Hitbox.NonSolid),
                    new Hitbox(-3f, 3f, 3f, 4.75f, this.rotation, Hitbox.Solid, true, true),

                    // walls
                    new Hitbox(-3f, -2.5f, 3f, 8.75f, this.rotation, Hitbox.Solid, true, true),
                    new Hitbox(2.5f, 3f, 3f, 8.75f, this.rotation, Hitbox.Solid, true, true),

                    // sorting order trigger
                    new Hitbox(-2.25f, 2.25f, 3.5f, 10f, this.rotation, Hitbox.NonSolid),

                    // enter player trigger
                    new Hitbox(-2.7f, 2.7f, 3.5f, 5f, this.rotation, Hitbox.NonSolid),

                    // plaque & base solid collider
                    new Hitbox(
                        this.trophyBase.transform.position.x - 2.8f,
                        this.trophyBase.transform.position.x + 2.8f,
                        this.trophyBase.transform.position.y - 2f,
                        this.trophyBase.transform.position.y + 2.1f,
                        this.rotation, Hitbox.Solid, true, true,
                        positionType: Hitbox.PositionType.Absolute)
                };

                this.rectanglesWithTop = new [] {
                    new Hitbox(-1f, 1.25f, 0f, 3f, this.rotation, Hitbox.NonSolid),
                    new Hitbox(-3f, 3f, 3f, 4.75f, this.rotation, Hitbox.Solid, true, true),
                    
                    // walls
                    new Hitbox(-3f, -2.5f, 3f, 8.75f, this.rotation, Hitbox.Solid, true, true),
                    new Hitbox(2.5f, 3f, 3f, 8.75f, this.rotation, Hitbox.Solid, true, true),

                    // sorting order trigger
                    new Hitbox(-2.25f, 2.25f, 3.5f, 10f, this.rotation, Hitbox.NonSolid),
                    
                    // enter player trigger
                    new Hitbox(-2.7f, 2.7f, 3.5f, 5f, this.rotation, Hitbox.NonSolid),
                    
                    // plaque & base solid collider
                    new Hitbox(
                        this.trophyBase.transform.position.x - 2.8f,
                        this.trophyBase.transform.position.x + 2.8f,
                        this.trophyBase.transform.position.y - 2f,
                        this.trophyBase.transform.position.y + 2.1f,
                        this.rotation, Hitbox.Solid, true, true,
                        positionType: Hitbox.PositionType.Absolute),

                    // top solid collider (for lid)
                    new Hitbox(-3f, 3f, 7f, 8.75f, this.rotation, Hitbox.Solid, true, true)

                };
            }
            break;
        }

        this.animated.PlayAndHoldLastFrame(this.animationHide);

        if(this.arrowAnimated == true)
        {
            this.arrowAnimated.PlayAndHoldLastFrame(this.animationArrowBlank);
        }
    }

    public override void Advance()
    {
        CheckPlayerNear();
        DetectPlayerEnter();
        
        var raycast = new Raycast(this.map, this.position);

        if(this.canDrop == true)
        {
            MoveVertically(raycast);
        }

        this.transform.position = this.position;

        if(this.onGround == true)
        {
            Player player = this.map.player;

            bool playerColliding = 
                player.Rectangle().Overlaps(this.hitboxes[0].InWorldSpace());

            bool playerJumpingUpwards =
                player.groundState.onGround == false && player.velocity.y > 0f;

            if (playerColliding == true)
            {
                bool swingLeft = this.map.player.transform.position.x > this.position.x;

                if(this.isSwinging == false)
                {
                    Swing(swingLeft);
                }

                if(playerJumpingUpwards == true && this.hopForce > 0f)
                {
                    if(this.isSwinging == true)
                    {
                        this.isSwinging = false;
                    }
                    
                    this.velocity = Vector2.up * this.hopForce;
                    this.animated.PlayAndHoldLastFrame(this.animationDrop);
                    this.spriteRenderer.flipX = (swingLeft == true);
                    Audio.instance.PlaySfx(TrophyJumpSFX(), position: this.position);
                }
            }
        }
        
        if(this.canShine == true &&
            this.isFiring == false &&
            this.isSwinging == false &&
            this.onGround == true)
        {
            if(this.shineTimer > 0)
            {
                this.shineTimer--;
            }
            else
            {
                this.shineTimer = SHINE_TIME;
                this.canShine = false;

                this.animated.PlayOnce(this.animationShine, () => {
                    this.canShine = true;
                });

                if(this.haveLid == true)
                {
                    this.lid.PlayShine();
                }
            }
        }

        if(this.rotateWhiteLines == true)
        {
            this.whiteLinesParent.Rotate(Vector3.forward * 100f * Time.deltaTime);
        }
        
        if(Multiplayer.IsMultiplayerGame() && this.trophyType != TrophyType.GOLD)
        {
            // Create Flying Chest with next to Bronze & Silver Trophies in multiplayer games
            Chest chest = this.chunk.AnyItem<Chest>();
            bool hasPowerupChest = false;

            if(chest != null && chest.prize == Chest.Prize.POWERUP)
            {
                hasPowerupChest = true;
            }

            if(hasPowerupChest == false)
            {
                Vector2 chestPosition = this.position.Add(-4f, 4f);
                Chest.CreateFlyingChestWithPowerup(chestPosition, this.chunk);
            }
        }
    }

    private void CheckPlayerNear()
    {
        if(this.checkForPlayerNear == false) return;
        
        Player player = this.map.player;
        Chunk currentPlayerChunk = this.map.NearestChunkTo(player.position);

        bool isPlayerInsideChunk = currentPlayerChunk == this.chunk;
        Vector2 playerPos = this.map.player.position + PLAYER_CHUNK_OFFSET;

        if(isPlayerInsideChunk == false) return;

        if(playerPos.y > this.position.y)
        {
            if(inEntranceLift == true)
            {
                return;
            }
            Appear();

            if(this.trophyType != TrophyType.GOLD)
            {
                CheckSkippedCheckpoints();

                this.map.SaveDataOfAttempt(this.chunk.checkpointNumber.Value);
                this.map.SaveNewRecordIfBetter(this.trophyType);
            }
            this.map.currentCheckpoint = this.transform;
            this.map.checkpointTimer = 0;
        }
    }

    private void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];
        this.velocity.y -= GRAVITY_FORCE;
        
        bool wasAirborne = (this.onGround == false);
        this.onGround = false;

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.375f, 0), false, maxDistance)
            );

            bool ValidCollision(Raycast.CombinedResults results)
            {
                if(results.anything == true)
                {
                    if(results.tiles.Length > 0)
                    {
                        return true;
                    }

                    if(results.items.Length > 0)
                    {
                        if(results.items[0] == this)
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            if (result.anything == true && ValidCollision(result) &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.onGround = true;

                if(wasAirborne == true)
                {
                    HitGround();
                }
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void Appear()
    {
        this.map.player.SetCheckpointPosition(
            this.position + new Vector2(0, Player.PivotDistanceAboveFeet)
        );

        this.map.SaveCoins();

        // increase trophy vertical starting position so it can drop to the ground
        this.position += DROP_POS_OFFSET;

        this.checkForPlayerNear = false;
        StartCoroutine(_Appear());

        if(this.trophyType == TrophyType.BRONZE || this.trophyType == TrophyType.SILVER)
        {
            ResetChecpointEvolution();
        }
    }

    private IEnumerator _Appear()
    {
        //Audio.instance.PlaySfx(Assets.instance.sfxSparkleTrophyAppear);
        Audio.instance.PlaySfx(TrophyAppearSFX(), position: this.position);

        Audio.instance.PlaySfx(
            TrophyFanfare(),
            position: this.position,
            options: new Audio.Options(1, false, 0)
        );

        this.map.DecreaseMusicVolume();
        if(this.trophyType == TrophyType.GOLD)
        {
            StartCoroutine(IncreaseVolumeCoroutine(4));
        }
        else
        {
            StartCoroutine(IncreaseVolumeCoroutine(2.5f));
        }

        if (this.trophyType != TrophyType.GOLD && this.hasCreatedRefree == false)
        {
            this.hasCreatedRefree = true;
            referee = Referee.Create(this.map.NearestChunkTo(this.map.player.position));
            referee.MoveArrow();
        }
        else if (this.trophyType == TrophyType.GOLD && this.hasCreatedRefree == false)
        {
            this.hasCreatedRefree = true;
            referee = Referee.Create(this.map.NearestChunkTo(this.map.player.position));
            referee.moveAroundGold = true;
            referee.goldTrophyPosition = this.transform.position + new Vector3(0, 7, 0);
        }
        // sparkle particles effect
        for (int i = 0; i < SPARKLES_AMOUNT; i++)
        {
            yield return new WaitForSeconds(0.05f);

            Vector2 sparklePos = (Vector2)this.appearAnim.transform.position +
                UnityEngine.Random.insideUnitCircle * 1.5f;
                
            var p = Particle.CreateAndPlayOnce(
                    Assets.instance.trophySparkleAnimation,
                    sparklePos,
                    this.map.transform
                );
        }
        yield return new WaitForSeconds(.1f);

        // circle effect
        this.appearAnim.PlayAndHoldLastFrame(this.animationAppear);
        
        float animationAppearTime = (float)this.animationAppear.sprites.Length
            / (float)this.animationAppear.fps;
        yield return new WaitForSeconds(animationAppearTime - .2f);

        // white line effect
        this.rotateWhiteLines = true;
        yield return StartCoroutine(_WhiteLinesScaleEffect(.1f, 1f));
        yield return new WaitForSeconds(.05f);
        yield return StartCoroutine(_WhiteLinesScaleEffect(.1f, 0f));
        this.rotateWhiteLines = false;

        // flash effect
        IngameUI.instance.FlashFadeIn(.2f, this.flashFadeIn);
        yield return new WaitForSeconds(.3f);

        this.appearAnim.PlayOnce(animationBlank);

        // trophy appear
        this.canDrop = true;
        this.animated.PlayAndHoldLastFrame(this.animationDrop);
        IngameUI.instance.FlashFadeOut(.1f, flashFadeOut);

        yield return new WaitForSeconds(1f);

        StartCoroutine(_GenerateReward());
    }

    private IEnumerator _WhiteLinesScaleEffect(float targetTime, float targetScaleValue)
    {
        float initialTime = 0f;
        Vector3 initialScale = this.whiteLines[0].localScale;
        Vector3 targetScale =
            	new Vector3(initialScale.x, targetScaleValue, initialScale.z);

        if(targetScale.x > 0f || targetScale.y > 0f)
        {
            ToggleWhiteLines(true);
        }

        while(initialTime < targetTime)
        {
            initialTime += Time.deltaTime;

            for (int i = 0; i < this.whiteLines.Length; i++)
            {
                this.whiteLines[i].localScale =
                    Vector3.Lerp(initialScale, targetScale, initialTime/targetTime);
            }

            yield return null;
        }
        
        if(targetScale.x <= 0f || targetScale.y <= 0f)
        {
            ToggleWhiteLines(false);
        }
    }

    private void ToggleWhiteLines(bool activate)
    {
        for (int i = 0; i < this.whiteLines.Length; i++)
        {
            this.whiteLines[i].gameObject.SetActive(activate);
        }
    }

    private void HitGround()
    {
        switch (this.trophyType)
        {
            case TrophyType.SILVER:
            {
                this.map.ScreenShakeAtPosition(
                    this.transform.position, Vector2.up * .4f);
            }
            break;

            case TrophyType.GOLD:
            {
                this.canDrop = false;
                this.map.ScreenShakeAtPosition(
                    this.transform.position, Vector2.up * .8f);
            }
            break;
        }

        //Audio.PlaySfxAtPosition(Assets.instance.sfxTrophyLand, this.position);
        
        this.animated.PlayOnce(this.animationWobble, () => 
        {
            this.canShine = true;
            this.shineTimer = SHINE_TIME;
            this.spriteRenderer.flipX = false;

            switch(this.trophyType)
            {
                case TrophyType.BRONZE:
                {
                        
                    /*if(this.hasBeenOnGroundOnce == false)
                        Audio.PlaySfx(Assets.instance.sfxReachNormalTrophy);*/
                }
                break;

                case TrophyType.SILVER:
                {
                    /*if(this.hasBeenOnGroundOnce == false)
                        Audio.PlaySfx(Assets.instance.sfxReachNormalTrophy);*/
                }
                break;

                case TrophyType.GOLD:
                {
                    /*if(this.hasBeenOnGroundOnce == false)
                        Audio.PlaySfx(Assets.instance.sfxReachGoldTrophy);*/

                    if(this.checkForPlayerEnter == true)
                    {
                        this.arrowAnimated.PlayAndLoop(this.animationArrowMove);
                    }
                }
                break;
            }

            //this.hasBeenOnGroundOnce = true;
        });
    }
 
    private void DetectPlayerEnter()
    {
        if(this.trophyType != TrophyType.GOLD) return;
        if (this.checkForPlayerEnter == false) return;

        if(this.hitboxes[4].InWorldSpace().Overlaps(this.map.player.Rectangle()))
        {
            this.spriteRenderer.sortingLayerName = "Items (Front)";
        }
        
        if(IsHittingAbove(this.hitboxes[5].InWorldSpace()))
        {
            // enforce changing the sorting layer (avoid corner-cases)
            this.spriteRenderer.sortingLayerName = "Items (Front)";

            PlayerEnter();
        }
    }
    
    protected bool IsHittingAbove(Rect ownRectangle)
    {
        if(this.trophyType != TrophyType.GOLD) return false;
        
        var player = this.map.player;
        var playerRectangle = player.Rectangle();

        if (ownRectangle.Overlaps(playerRectangle) == false)
            return false;

        return (playerRectangle.yMin > ownRectangle.yMax - 0.5f ||
            (playerRectangle.yMin - player.velocity.y) > ownRectangle.yMax - 0.5f);
    }

    public IEnumerator _FinishWithoutPlayer()
    {
        this.arrowAnimated.gameObject.SetActive(false);

        yield return new WaitForSeconds(2f);

        SaveEndOfLevelData();
        
        this.hitboxes = this.rectanglesWithTop;
        StartCoroutine(this.map.gameCamera.LookAtTrophy(this.transform));
        
        yield return new WaitForSeconds(1f);

        StartBlender();
    }

    public void PlayerEnter()
    {
        if(this.trophyType != TrophyType.GOLD) return;

        SaveEndOfLevelData();

        this.arrowAnimated.PlayAndHoldLastFrame(this.animationArrowBlank);
        this.hitboxes = this.rectanglesWithTop;

        var mp = Game.instance.multiplayerLastPlayerCutoff;
        mp.AddDelegateWhenAllPlayersFinished(() => { StartCoroutine(_FirePlayer()); });
        mp.NotifyLocalPlayerHasFinished(this.map.mapNumber);

        GameCenterMultiplayer.NotifyEnterGoldTrophy();
    }

    private void SaveEndOfLevelData()
    {
        // timer stops here (player's state is now State.Finished)
        this.map.player.EnterGoldCup(this.transform);

        // skipped checkpoints
        CheckSkippedCheckpoints();
        
        // saving and reporting time to leaderboard
        float timeInSeconds = (float)this.map.frameNumber / 60f;
        this.map.SaveDataOfAttempt(0);
        this.map.SaveNewRecordIfBetter(Trophy.TrophyType.GOLD);

        // final time
        Leaderboard.ReportUserTime(timeInSeconds, Game.selectedDate);

        // total deaths
        Achievements.CheckDeathsAchievement(this.map.deaths);

        // best trophy
        SaveData.Trophy currentTrophy = (SaveData.Trophy)this.trophyType + 1;

        if(this.map.ShouldAwardFruitCup())
        {
            currentTrophy = SaveData.Trophy.Fruit;
        }

        Achievements.CheckTrophyAchievement(currentTrophy);

        // completed days
        int completedDays = SaveData.GetNumberOfCompletedDays();
        Achievements.CheckDaysAchievement(completedDays);

        this.checkForPlayerEnter = false;
    }

    private IEnumerator _FirePlayer()
    {
        yield return new WaitForSeconds(0.75f);
        FirePlayer();
    }

    private void FirePlayer()
    {
        if(this.trophyType != TrophyType.GOLD) return;

        this.isFiring = true;

        this.animated.PlayOnce(this.animationFire, () => {
            StartBlender();
        });

        this.animated.OnFrame(14, () => {

            Audio.instance.PlaySfx(Assets.instance.sfxGoldTrophyShot, position: this.position);
            this.map.player.GoldCupFire();

            var p = Particle.CreateAndPlayOnce(
                    Assets.instance.goldTrophyShotAnimation,
                    this.transform.position + Vector3.up * .5f,
                    this.transform
                );

            p.spriteRenderer.sortingLayerName = "Items";
            p.spriteRenderer.sortingOrder = -1;

            referee.PlayWinAnimation();
        });

        StartCoroutine(this.map.gameCamera.LookAtTrophy(this.transform));
    }

    private void StartBlender()
    {
        StartCoroutine(_StartBlender());
    }

    private IEnumerator _StartBlender()
    {
        this.blade.PlayAndLoop(this.animationBlender);

        yield return new WaitForSeconds(.5f);

        float collectedFruits = this.map.fruitCollected;
        float totalFruits = this.map.TotalFruitAtStart();
        float percentFruitsCollected = (collectedFruits / totalFruits) * 100;
        percentFruitsCollected = (percentFruitsCollected > 100) ? 100 : percentFruitsCollected;

        this.fruitsMoving = true;

        if((Mathf.Floor(percentFruitsCollected)) != 0)
        {
            StartCoroutine(MoveFruits((int)(Mathf.Ceil(50 * percentFruitsCollected / 100)), this.fruitCounterText.transform));
        }
        else
        {
            StartCoroutine(MoveFruits(0, this.fruitCounterText.transform));
        }

        if (GameCamera.IsLandscape() == true)
        {
            yield return new WaitForSeconds(.9f);
        }
        else
        {
            yield return new WaitForSeconds(1.4f);
        }

        Vector2 backgroundTargetScale = new Vector2(0, 0);
        Vector2 topTargetPos = new Vector2(0, 0);

        if (percentFruitsCollected != 0)
        {
            this.juice.gameObject.SetActive(true);
            this.bgTransform.gameObject.SetActive(true);
            this.juice.PlayAndLoop(animationBlenderJuice);
            backgroundTargetScale = new Vector2(BackgroundFinalScale.x, BackgroundInitialScale.y + (BackgroundFinalScale.y - BackgroundInitialScale.y) * (Mathf.Ceil(percentFruitsCollected) / 100));
            topTargetPos = new Vector2(TopFinalPos.x, TopInitialPos.y + (TopFinalPos.y - TopInitialPos.y) * (Mathf.Ceil(percentFruitsCollected) / 100));
        }

        float timer = 0;
        float totalTime = 1 + 3.5f * (percentFruitsCollected / 100);

        while ((this.fruitsMoving == true || timer <= totalTime) && (percentFruitsCollected != 0))
        {
            timer += Time.deltaTime;

            if (timer <= totalTime)
            {
                float fruits = (timer / totalTime) * percentFruitsCollected;
                this.fruitCounterText.text = (Mathf.Floor(fruits)).ToString() + "%";
                this.bgTransform.localScale = Vector2.Lerp(BackgroundInitialScale, backgroundTargetScale, timer / totalTime);
                this.juice.transform.localPosition = Vector2.Lerp(TopInitialPos, topTargetPos, timer / totalTime);
            }
            else
            {
                this.fruitCounterText.text = ((int)(Mathf.Floor(percentFruitsCollected))).ToString() + "%";
            }
            yield return null;
        }

        this.juice.PlayAndLoop(animationBlenderJuiceIdle);

        yield return new WaitForSeconds(0.5f);

        bool enoughFruitsForFruitCup = percentFruitsCollected >= 80.0;
        bool isMultiplayer = Multiplayer.IsMultiplayerGame();
        bool isPlayerWinner = Multiplayer.IsPlayerWinning(this.map.player); //todo: change this to the correct 'Player'
        
        if (isMultiplayer == true || enoughFruitsForFruitCup == true)
        {
            float waitTime = isMultiplayer == true ? 6f : 3.5f;

            Audio.instance.PlaySfx(Assets.instance.sfxPowerupSlot);
            Particle star = Particle.CreateAndPlayOnce(
                animationStarEffect,
                starLine.position + new Vector3( 0, 0.15f, 0),
                this.transform.parent
            );
            star.spriteRenderer.sortingLayerName = "Items (Front)";
            star.spriteRenderer.sortingOrder = 5;
            CreateLid(isMultiplayer, isPlayerWinner);

            yield return new WaitForSeconds(waitTime);
        }
        else
        {
            StartCoroutine(XCoroutine());
            yield return new WaitForSeconds(1.5f);
        }

        if(IngameUI.instance.winMenu.isOpen == false)
        {
            IngameUI.instance.winMenu.Open();
        }
    }

    private IEnumerator MoveFruits(int fruitNumber, Transform targetTransform)
    {
        Transform[] fruit = new Transform[fruitNumber];
        Vector3[] randomOffset = new Vector3[fruitNumber];
        Vector3 startPosition = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 1.05f,0)) + new Vector3( 0, 0, 20);

        for (int i = 0; i < fruitNumber; i++)
        {
            fruit[i] = Instantiate(
                this.fruitPrefab,
                startPosition,
                Quaternion.identity
            ).GetComponent<Transform>();

            fruit[i].GetComponent<SpriteRenderer>().sprite = IngameUI.instance.uiFruit.fruitSprites[i % IngameUI.instance.uiFruit.fruitSprites.Length];

            fruit[i].transform.SetParent(this.transform.parent);
            randomOffset[i] = UnityEngine.Random.insideUnitCircle * .5f;
        }

        bool moveFruits = true;

        float timer = 0f;
        float delay = 0.10f;

        bool hasSplashed = false;
        while (moveFruits == true)
        {
            timer += Time.deltaTime;
            moveFruits = false;

            for (int i = 0; i < fruit.Length; i++)
            {
                if (timer < delay * i) continue;

                float currentMoveSpeed = 20 * Time.deltaTime;

                Vector2 currentPosition = fruit[i].position;

                Vector2 targetWorldPosition = targetTransform.position + new Vector3(0,0f,0) + randomOffset[i];
                Vector3 heading = targetWorldPosition - currentPosition;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                if (distance > currentMoveSpeed)
                {
                    fruit[i].position += direction * currentMoveSpeed;
                    moveFruits = true;
                }
                else
                {
                    Audio.instance.PlaySfx(Assets.instance.sfxFruit, position: this.position, options: new Audio.Options(0.7f, true, 0.02f));
                    if (hasSplashed == false)
                    {
                        hasSplashed = true;
                        Particle splash1 = Particle.CreateAndPlayOnce(
                            animationBlenderJuiceSplash,
                            juice.transform.position + new Vector3(1.5f, 1.75f, 0),
                            this.transform.parent
                        );
                        splash1.spriteRenderer.sortingLayerName = "Items";
                        splash1.spriteRenderer.sortingOrder = 0;

                        Particle splash2 = Particle.CreateAndPlayOnce(
                            animationBlenderJuiceSplash,
                            juice.transform.position + new Vector3(-1.5f, 1.75f, 0),
                            this.transform.parent
                        );
                        splash2.transform.localScale = new Vector3(-1, 1, 1);
                        splash2.spriteRenderer.sortingLayerName = "Items";
                        splash2.spriteRenderer.sortingOrder = 0;
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }

        this.fruitsMoving = false;
    }

    private IEnumerator XCoroutine()
    {
        GameObject x = this.starLine.Find("X").gameObject;

        x.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        x.SetActive(false);

        yield return new WaitForSeconds(0.2f);

        x.SetActive(true);

        yield return new WaitForSeconds(0.2f);

        x.SetActive(false);

        yield return new WaitForSeconds(0.2f);

        x.SetActive(true);
    }

    private void CreateLid(bool isMultiplayer, bool IsPlayerWinner)
    {
        StartCoroutine(_CreateLid(isMultiplayer, IsPlayerWinner));
    }

    private IEnumerator _CreateLid(bool isMultiplayer, bool IsPlayerWinner)
    {
        yield return new WaitForSeconds(1f);

        if(isMultiplayer == true)
        {
            if(IsPlayerWinner == true)
            {
                this.lid = TrophyGoldLidMultiplayerWinner.Create(
                    this,
                    this.transform.position + Vector3.up * 26f,
                    this.chunk
                );
            }
            else
            {
                this.lid = TrophyGoldLidMultiplayerLoser.Create(
                    this,
                    this.transform.position + Vector3.up * 26f,
                    this.chunk
                );
            }
        }
        else
        {
            this.lid = TrophyGoldLidBasic.Create(
                this,
                this.transform.position + Vector3.up * 26f,
                this.chunk
            );
        }

        Audio.instance.PlaySfx(Assets.instance.sfxGoldTrophyLidDrop);
    }

    public void ObtainLid()
    {
        this.haveLid = true;
        this.isFiring = false;
        Audio.instance.PlaySfx(
            Assets.instance.sfxFanfareFruit, 
            position: this.position, 
            options: new Audio.Options(1, false, 0)
        );
        this.map.DecreaseMusicVolume();
    }

    private void Swing(bool swingLeft)
    {
        if(this.animationSwing.sprites.Length <= 0) return;
        
        this.isSwinging = true;

        this.spriteRenderer.flipX = (swingLeft == false);

        this.animated.PlayOnce(this.animationSwing, () => 
        {
            this.isSwinging = false;
            this.canShine = true;
            this.spriteRenderer.flipX = false;
        });

        Audio.instance.PlaySfx(TrophyWobbleSFX(), position: this.position);
    }

    private IEnumerator IncreaseVolumeCoroutine(float duration)
    {
        yield return new WaitForSeconds(duration);
        this.map.IncreaseMusicVolume();
    }

    private void ResetChecpointEvolution()
    {
        List<Checkpoint> checkpoints = this.map.AllItems().OfType<Checkpoint>().ToList();
        int x = checkpoints.IndexOf(this.map.LastCheckpointFromPosition(this.position).items.OfType<Checkpoint>().ToArray()[0]);
        if (this.map.lastCheckpoint != null)
        {
            x = checkpoints.IndexOf(this.map.lastCheckpoint);
        }
        else
        {
            x = checkpoints.IndexOf(this.map.LastCheckpointFromPosition(this.position).items.OfType<Checkpoint>().ToArray()[0]);
        }

        for (int n = 0; n <= 5; n++)
        {
            x++;
            if (x < checkpoints.Count)
            {
                checkpoints[x].single.gameObject.SetActive(false);
                checkpoints[x].pieces.gameObject.SetActive(true);
                checkpoints[x].currentEvolution = n;
                Sprite sprite = checkpoints[x]?.animationBulb.sprites[0];
                switch (n)
                {
                    case 0:
                        sprite = checkpoints[x]?.animationBulb.sprites[0];
                        break;
                    case 1:
                        sprite = checkpoints[x]?.animationEvolve1.sprites[0];
                        break;
                    case 2:
                        sprite = checkpoints[x]?.animationEvolve2.sprites[0];
                        break;
                    case 3:
                        sprite = checkpoints[x]?.animationEvolve3.sprites[0];
                        break;
                    case 4:
                        sprite = checkpoints[x]?.animationEvolve4.sprites[0];
                        break;
                    case 5:
                        sprite = checkpoints[x]?.animationEvolve5.sprites[0];
                        break;
                }
                checkpoints[x].bulb.GetComponent<SpriteRenderer>().sprite = sprite;
            }
        }
    }

    private IEnumerator _GenerateReward()
    {
        yield return new WaitForSeconds(1);
        GenerateReward();
    }

    private void GenerateReward()
    {
        Vector3 bulbPosition;

        if (this.trophyType == TrophyType.GOLD)
        {
            bulbPosition = (Vector3)this.transform.TransformPoint(Vector2.zero) + new Vector3(0, 10);

        }
        else if (this.trophyType == TrophyType.SILVER)
        {
            bulbPosition = (Vector3)this.transform.TransformPoint(Vector2.zero) + new Vector3(0, 6);
        }
        else
        {
            bulbPosition = (Vector3)this.transform.TransformPoint(Vector2.zero) + new Vector3(0, 4);
        }

        float rewardNumber = 2;
        float delay = 1;
        TimerPickup t;
        int currentEvolution;
        if (this.map.lastCheckpoint != null)
        {
            currentEvolution = this.map.lastCheckpoint.currentEvolution + 1;
            if (this.map.lastCheckpoint.active == true)
            {
                return;
            }
        }
        else
        {
            return;
        }

        var p = Particle.CreateAndPlayOnce(
           Assets.instance.enemyRespawnerActivateAnimation, bulbPosition, this.transform
        );
        p.transform.localScale *= 1.5f;
        p.spriteRenderer.sortingLayerName = "Pickups";
        p.spriteRenderer.sortingOrder = 20;

        switch (currentEvolution)
        {
            case 1:
                rewardNumber = 2;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(0, 2, 0), delay);
                break;

            case 2:
                rewardNumber = 4;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                break;

            case 3:
                rewardNumber = 8;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3((-2f), 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(1f, 2 - 0.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3((2f), 0.75f, 0), delay);
                t.SetLayerOrder(15);
                break;

            case 4:
                rewardNumber = 12;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-2.65f, 0.36f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-2f, 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(2f, 0.75f, 0), delay);
                t.SetLayerOrder(16);
                delay += 0.3f;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(2.65f, 0.36f, 0), delay);
                t.SetLayerOrder(15);
                break;

            case 5:
                rewardNumber = 15;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-2.65f, 0.34f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-2f, 0.75f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(20);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(-1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(19);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(0, 2, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(18);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(1f, 1.5f, 0), delay);
                delay += 0.3f;
                t.SetLayerOrder(17);
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(2f, 0.75f, 0), delay);
                t.SetLayerOrder(16);
                delay += 0.3f;
                t = TimerPickup.Create(bulbPosition, this.chunk, bulbPosition + new Vector3(2.65f, 0.34f, 0), delay);
                t.SetLayerOrder(15);
                break;
        }

        float hVelocity = UnityEngine.Random.Range(0.1f, 0.5f);
        float vVelocity = UnityEngine.Random.Range(0.1f, 0.3f);
        if (this.trophyType == TrophyType.GOLD)
        {
            hVelocity += 0.2f;
            vVelocity += 0.2f;
        }

        float start = -1f;
        for (int i = 1; i <= rewardNumber - 1; i++)
        {
            Coin coin = Coin.Create(bulbPosition, this.chunk);
            coin.movingFreely = true;
            coin.velocity = new Vector2(hVelocity * start, vVelocity);
            coin.GetComponent<SpriteRenderer>().sortingLayerName = "Items";
            coin.GetComponent<SpriteRenderer>().sortingOrder = -1;
            start += (2 / (rewardNumber - 1));
        }

        start = -1f;
        for (int i = 1; i <= rewardNumber; i++)
        {
            Coin coin = Coin.CreateSilver(bulbPosition, this.chunk);
            coin.movingFreely = true;
            coin.velocity = new Vector2(hVelocity * start, vVelocity);
            coin.GetComponent<SpriteRenderer>().sortingLayerName = "Items";
            coin.GetComponent<SpriteRenderer>().sortingOrder = -1;
            start += (2 / (rewardNumber - 1));
        }
    }

    private void CheckSkippedCheckpoints()
    {
        int checkpointNumber = this.chunk.checkpointNumber.Value;
        int checkpointsSkipped = SaveData.GetNumberOfCheckpointsSkipped(checkpointNumber);
        Achievements.CheckCheckpointAchievement(checkpointsSkipped);
    }
}
