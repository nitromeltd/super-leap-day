﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class TrophyGoldLid : Item
{
    public Animated.Animation animationLidIdle;
    public Animated.Animation animationLidDrop;
    public Animated.Animation animationLidShine;
    public Animated.Animation animationLidWobble;

    [NonSerialized] public Trophy goldTrophy;

    protected Vector2 velocity;
    protected bool isDropping = true;
    
    protected const float GravityForce = 0.02f;

    public virtual void Init(Trophy trophy, Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.goldTrophy = trophy;
        this.position = position;
        this.transform.position = position;
        this.hitboxes = new [] {
            new Hitbox(-2f, 2f, .6f, 1.6f, this.rotation, Hitbox.NonSolid)
        };
        
        this.animated.PlayAndHoldLastFrame(this.animationLidDrop);
    }
    
    public override void Advance()
    {
        if(this.isDropping == true)
        {
            this.velocity.y -= GravityForce;

            var raycast = new Raycast(this.map, this.position);
            MoveVertically(raycast);
        }
        
        this.transform.position = this.position;
    }
    
    private void MoveVertically(Raycast raycast)
    {
        var rect = this.hitboxes[0];

        if (this.velocity.y < 0)
        {
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start + new Vector2(-0.375f, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2(      0, 0), false, maxDistance),
                raycast.Vertical(start + new Vector2( 0.375f, 0), false, maxDistance)
            );

            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin)
            {
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                CollideTrophy();
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }
    
    protected virtual void CollideTrophy()
    {
        this.isDropping =  false;

        Audio.instance.PlaySfx(Assets.instance.sfxTrophyLand, position: this.position);
        this.animated.PlayOnce(this.animationLidWobble, () =>
        {
            this.animated.PlayAndHoldLastFrame(this.animationLidIdle);
            goldTrophy.ObtainLid();
        });
    }

    public virtual void PlayShine() { }
}
