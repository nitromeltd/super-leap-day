using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrophyGoldLidBasic : TrophyGoldLid
{
    public static TrophyGoldLidBasic Create(Trophy trophy, Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.trophyGoldLid, chunk.transform);
        obj.name = "Trophy Gold Lid Basic";
        var item = obj.GetComponent<TrophyGoldLidBasic>();
        item.Init(trophy, position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public override void PlayShine()
    {
        this.animated.PlayOnce(this.animationLidShine, () =>
        {
            this.animated.PlayAndHoldLastFrame(this.animationLidIdle);
        });
    }
}
