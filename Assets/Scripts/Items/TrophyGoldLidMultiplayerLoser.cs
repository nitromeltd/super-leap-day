using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrophyGoldLidMultiplayerLoser : TrophyGoldLid
{
    [Header("Multiplayer Loser")]
    public Transform[] fliesSpawnPoints;
    public Transform[] fliesMovePoints;
    public Transform meetPoint;

    public static TrophyGoldLidMultiplayerLoser Create(
        Trophy trophy, Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.trophyGoldLidMultiplayerLoser, chunk.transform);
        obj.name = "Trophy Gold Lid Multiplayer Loser";
        var item = obj.GetComponent<TrophyGoldLidMultiplayerLoser>();
        item.Init(trophy, position, chunk);
        chunk.items.Add(item);
        return item;
    }
    
    protected override void CollideTrophy()
    {
        this.isDropping =  false;

        Audio.instance.PlaySfx(Assets.instance.sfxTrophyLand, position: this.position);
        this.animated.PlayOnce(this.animationLidWobble, () =>
        {
            this.animated.PlayOnce(this.animationLidIdle);
            SpawnFlies();
            goldTrophy.ObtainLid();
        });

        this.animated.OnFrame(9, () =>
        {
            this.spriteRenderer.sortingLayerName = "Items (Front)";
            this.spriteRenderer.sortingOrder = 2;
        });
    }

    private void SpawnFlies()
    {
        for (int i = 0; i < this.fliesSpawnPoints.Length; i++)
        {
            Transform spawnPoint = this.fliesSpawnPoints[i];
            Transform movePoint = this.fliesMovePoints[i];
            bool facingRight = spawnPoint.position.x > this.position.x;

            FlyMultiplayerLoser.Data flyData =
                new FlyMultiplayerLoser.Data(spawnPoint, movePoint, meetPoint, facingRight);
            FlyMultiplayerLoser.Create(spawnPoint.position, this.chunk, flyData);
        }
    }
}
