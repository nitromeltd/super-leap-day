using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrophyGoldLidMultiplayerWinner : TrophyGoldLid
{
    [Header("Multiplayer Winner")]
    public Animated.Animation animationLidGrow;
    public GameObject parentGO;
    public Transform flagFabricTransform;
    public SpriteRenderer playerIconSpriteRenderer;

    [Header("Multiplayer Winner - Player Icons")]
    public Sprite yolkIcon;
    public Sprite pufferIcon;
    public Sprite sproutIcon;
    public Sprite kingIcon;
    public Sprite goopIcon;
    public Sprite golfbotIcon;

    [Header("Multiplayer Winner - Confetti")]
    public Sprite confettiSprite;

    private bool showFlagFabric = false;
    private int flagFabricTime = 0;
    private bool spawnConfetti = false;

    private const int FlagFabricDelay = 30;
    
    private static Color[] ConfettiColors = new Color[]
    {
        new Color32(255, 138, 0, 255)// orange
        ,new Color32(122, 212, 42, 255)// green
        ,new Color32(254, 247, 52, 255)// yellow
        ,new Color32(204, 30, 161, 255)// magenta
        ,new Color32(251, 4, 4, 255)// red
        ,new Color32(136, 242, 244, 255)// cyan
        ,new Color32(119, 1, 244, 255)// violet
    };

    private Color RandomConfettiColor =>
        ConfettiColors[UnityEngine.Random.Range(0, ConfettiColors.Length)];

    private Sprite CharacterIcon(Character character) => character switch
    {
        Character.Yolk      => this.yolkIcon,
        Character.Puffer    => this.pufferIcon,
        Character.Sprout    => this.sproutIcon,
        Character.King      => this.kingIcon,
        Character.Goop      => this.goopIcon,
        _                   => throw new NotImplementedException()
    };

    public static TrophyGoldLidMultiplayerWinner Create(Trophy trophy, Vector2 position, Chunk chunk)
    {
        var obj = Instantiate(Assets.instance.trophyGoldLidMultiplayerWinner, chunk.transform);
        obj.name = "Trophy Gold Lid Multiplayer Winner";
        var item = obj.GetComponent<TrophyGoldLidMultiplayerWinner>();
        item.Init(trophy, position, chunk);
        chunk.items.Add(item);
        return item;
    }

    public override void Init(Trophy trophy, Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));
        base.Init(trophy, position, chunk);

        this.playerIconSpriteRenderer.sprite =
            CharacterIcon(this.map.player.Character());
    }
    
    public override void Advance()
    {
        if(this.showFlagFabric == true)
        {
            if(this.flagFabricTime < FlagFabricDelay)
            {
                this.flagFabricTime += 1;
            }
            else
            {
                float fabricPosX = this.flagFabricTransform.localPosition.x;
                fabricPosX = Util.Slide(fabricPosX, 0f, 0.10f);

                this.flagFabricTransform.localPosition = new Vector3(
                    fabricPosX, this.flagFabricTransform.localPosition.y
                );

                if(Mathf.Approximately(fabricPosX, 0f))
                {
                    this.showFlagFabric = false;
                    goldTrophy.ObtainLid();
                }
            }

        }

        if(this.spawnConfetti == true)
        {
            SpawnConfetti();
        }
        
        base.Advance();
    }

    protected override void CollideTrophy()
    {
        this.isDropping =  false;
        this.spawnConfetti = true;

        Audio.instance.PlaySfx(Assets.instance.sfxTrophyLand, position: this.position);
        this.animated.PlayOnce(this.animationLidWobble, () =>
        {
            this.animated.PlayOnce(this.animationLidGrow, () =>
            {
                this.parentGO.SetActive(true);
                this.animated.PlayAndHoldLastFrame(this.animationLidIdle);
                this.showFlagFabric = true;
            });

        });
    }

    private void SpawnConfetti()
    {
        if(Map.instance.frameNumber % 8 == 0)
        {
            SpawnSingleConfetti();
        }
    }    


    private void SpawnSingleConfetti()
    {
        float halfWidth = Map.instance.gameCamera.VisibleRectangle().width / 2f;
        float posX = Map.instance.gameCamera.VisibleRectangle().center.x +
            UnityEngine.Random.Range(-halfWidth, halfWidth);
        float posY = Map.instance.gameCamera.VisibleRectangle().yMax;

        Particle p = Particle.CreateWithSprite(
            confettiSprite, 60 * 10, new Vector2(posX, posY), this.transform
        );

        p.spriteRenderer.color = RandomConfettiColor;

        float scaleX = 2.5f + UnityEngine.Random.Range(-0.50f, 0.50f);
        float scaleY = 2.5f + UnityEngine.Random.Range(-0.50f, 0.50f);

        p.transform.localScale = new Vector3(scaleX, scaleY, 1f);
        
        float fallingSpeed = 0.06f + UnityEngine.Random.Range(-0.01f, 0.01f);
        p.velocity = Vector2.down * fallingSpeed;

        float spinX = (2.5f + UnityEngine.Random.Range(-1f, 1f)) * UnityEngine.Random.value > 0.50f ? 1f : -1f;
        float spinY = (5f + UnityEngine.Random.Range(-1f, 1f)) * UnityEngine.Random.value > 0.50f ? 1f : -1f;
        float spinZ = (3.5f + UnityEngine.Random.Range(-1f, 1f)) * UnityEngine.Random.value > 0.50f ? 1f : -1f;
        p.spin3d = new Vector3(spinX, 5f, 3.5f);
    }
}
