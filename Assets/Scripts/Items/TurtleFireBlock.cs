﻿using System;
using UnityEngine;

public class TurtleFireBlock : Item
{
    public Animated.Animation animationRed;

    public Animated orangeAnimated;
    public Animated.Animation animationOrange;

    public Animated yellowAnimated;
    public Animated.Animation animationYellow;

    private bool isActive;
    [NonSerialized] public int lifeTimer;
    public static float BlockSize = 0.40f;

    public static TurtleFireBlock Create(Vector2 position, Chunk chunk, float rotation)
    {
        var obj = Instantiate(
            Assets.instance.hotNColdSprings.turtleFireBlock, chunk.transform
        );
        obj.name = "Spawned Turtle Fire Block";
        var item = obj.GetComponent<TurtleFireBlock>();
        item.Init(position, chunk, rotation);
        chunk.items.Add(item);
        return item;
    }

    public void Init(Vector2 position, Chunk chunk, float rotation)
    {
        base.Init(new Def(chunk));

        this.chunk = chunk;
        this.transform.position = this.position = position;
        this.transform.rotation = Quaternion.Euler(0, 0, rotation);

        this.whenChunkResets = ChunkResetBehaviour.DestroyGameobject;
        
        float hitboxHalfSize = BlockSize / 2f;
        this.hitboxes = new [] {
            new Hitbox(
                -hitboxHalfSize, hitboxHalfSize,
                -hitboxHalfSize, hitboxHalfSize,
                this.rotation,
                Hitbox.NonSolid)
        };

        this.isActive = true;
        
        this.animated.PlayAndLoop(this.animationRed);
        this.orangeAnimated.PlayAndLoop(this.animationOrange);
        this.yellowAnimated.PlayAndLoop(this.animationYellow);
    }

    public override void Advance()
    {
        base.Advance();

        if(this.isActive == true)
        {
            // kill player hwne overlapping
            var player = this.map.player;
            var thisRect = this.hitboxes[0].InWorldSpace();
            if (player.ShouldCollideWithItems() && thisRect.Overlaps(player.Rectangle()))
            {
                this.map.player.Hit(this.position);
            }

            // spawn flame particle
            if(this.lifeTimer % 15 == 0)
            {
                Vector2 flamePartSpawnCenter = this.position.Add(0f, 1f);
                Vector2 vert1 = flamePartSpawnCenter.Add(0f, 0.5f);
                Vector2 vert2 = flamePartSpawnCenter.Add(-1f, -0.35f);
                Vector2 vert3 = flamePartSpawnCenter.Add(1f, -0.35f);

                Vector2 particleSpawnPos =
                    this.position.Add(0, 0.50f) + (UnityEngine.Random.insideUnitCircle * BlockSize);
                Animated.Animation particleAnimation = UnityEngine.Random.value > 0.5f ?
                    Assets.instance.flameAnimationA : Assets.instance.flameAnimationB;

                Particle flameParticle = Particle.CreateAndPlayOnce(
                    particleAnimation,
                    particleSpawnPos,
                    this.transform
                );

                flameParticle.spriteRenderer.sortingLayerName = "Items (Front)";
                flameParticle.spriteRenderer.sortingOrder = 20;
                flameParticle.acceleration = Vector2.up * 0.005f;
            }

            // manage remaining lifetime
            this.lifeTimer -= 1;

            if(this.lifeTimer <= 0)
            {
                Disappear();
            }
        }
    }

    private void Disappear()
    {
        this.isActive = false;
        this.gameObject.SetActive(false);
    }
}
