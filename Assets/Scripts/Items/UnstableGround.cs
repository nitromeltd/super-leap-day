using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NitromeEditor;
using Rnd = UnityEngine.Random;

public class UnstableGround : Item
{
    private Path path;
    private float timeAtStart;

    public bool goingDown;
    public bool goingBack;
    private Vector2 startingPosition;
    private Vector2 originalPosition;

    private float currentTimeOnPath;
    private const float MaxSpeedOnPath = 1f;

    public Animated.Animation[] crystalAnimations;
    public Sprite[] purpleCrystals;

    private List<Crystal> debris = new List<Crystal>();
    private bool debrisVisible;
    private Vector3 positionLastFrame;
    private bool isPlayingSfx = false;
    public bool playerCrossedChunk;

    public struct Crystal
    {
        public SpriteRenderer spriteRenderer;
        public Sprite stuck;
        public Sprite fall;
    }

    public override void Init(Def def)
    {
        base.Init(def);
        this.path = this.chunk.pathLayer.NearestPathTo(this.position);
        AttachToGroup(this.chunk.groupLayer.RegisterLeader(this, def.tx, def.ty));
        this.startingPosition = this.position;
        SetupDebris();
        this.debrisVisible = true;
        this.playerCrossedChunk = false;
    }

    private void AddDustParticlesToWall()
    {
        var tile = group.followerTileGrid.tiles[Rnd.Range(0, group.followerTileGrid.tiles.Length)];
        if (tile == null || UnityEngine.Random.value>0.3f) return;
        Vector2 pos = new Vector2(
                                tile.spriteRenderer.gameObject.transform.position.x + 0.5f,
                                tile.spriteRenderer.gameObject.transform.position.y
                                );
        Particle.CreateDust(
            this.chunk.transform, 1, 1, pos, velocity: new Vector2(0, -0.03f),
            sortingLayerName: "Items (Front)"
        );
        
        Particle.CreateDust(
            this.chunk.transform, 1, 2, pos, velocity: new Vector2(0, -0.01f),
            sortingLayerName: "Items (Front)"
        );

        if (UnityEngine.Random.value < 0.5f && goingBack == false)
        {
            foreach (var t in group.followerTileGrid.tiles)
            {
                if (Rnd.value > 0.1f) continue;
                if (t == null) continue;
                if (t.mapTx == group.followerTileGrid.xMax ||
                    t.mapTy == group.followerTileGrid.yMax ||
                    t.mapTx == group.followerTileGrid.xMin ||
                    t.mapTy == group.followerTileGrid.yMin) continue;

                int random = Rnd.Range(0, crystalAnimations.Length);
                Sprite randomCrystal = purpleCrystals[random];

                Particle p = Particle.CreateWithSprite(
                    randomCrystal,
                    300,
                    new Vector3(t.spriteRenderer.gameObject.transform.position.x + 0.5f, t.spriteRenderer.gameObject.transform.position.y),
                    this.group.container.transform
                );
                p.velocity.y = Rnd.Range(-0.1f, -0.2f);
                p.acceleration.y = Rnd.Range(-0.01f, -0.03f);
            }
        }
    }

    override public void Advance()
    {
        if(this.goingBack == true)
        {
            if(this.path != null)
            {
                if(this.currentTimeOnPath >= 0)
                {
                    this.currentTimeOnPath = this.currentTimeOnPath - 2*MaxSpeedOnPath / 60.0f;
                    if(this.currentTimeOnPath <= 0)
                    {
                        this.currentTimeOnPath = 0;
                        this.transform.position = this.position = this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.goingBack = this.goingDown = false;
                    }
                    else
                    {
                        this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                        AddDustParticlesToWall();
                    }
                }
            }
            else
            {
                if (this.originalPosition.y < this.startingPosition.y)
                {
                    this.originalPosition -= new Vector2(0, -0.01f);
                    this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                    AddDustParticlesToWall();

                    if(this.originalPosition.y >= this.startingPosition.y)
                    {
                        this.originalPosition = this.position = this.startingPosition;
                    }
                }
            }
            PlaySfx();
            return;
        }

        if (this.map.player.alive != true || (this.playerCrossedChunk == true && this.map.player.position.y < this.transform.position.y))
        {
            this.goingBack = true;
            this.playerCrossedChunk = false;
            return;
        }

        if(this.goingDown)
        {
            if(this.path != null)
            {
                if(this.currentTimeOnPath < this.path.duration)
                {
                    this.currentTimeOnPath = this.currentTimeOnPath + MaxSpeedOnPath / 60.0f;
                    if(this.currentTimeOnPath >= this.path.duration)
                    {
                        this.currentTimeOnPath = this.path.duration;
                        this.transform.position = this.position = this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                    }
                    else
                    {
                        this.originalPosition = this.path.PointAtTime(this.currentTimeOnPath).position;
                        this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                        AddDustParticlesToWall();
                    }
                }
            }
            else
            {
                if (this.originalPosition.y > GetChunkMinXY().y)
                {
                    this.originalPosition += new Vector2(0, -0.01f);
                    this.transform.position = this.position = this.originalPosition + new Vector2(Rnd.Range(-0.15f, 0.15f), Rnd.Range(-0.05f, 0.05f));
                    AddDustParticlesToWall();
                }
            }
        }
        else
        {
            var player = this.map.player;

            var raycast = new Raycast(
                this.map, player.position, layer: Layer.A, isCastByPlayer: true
            );

            if(player.InsideMinecart != null)
            {
                foreach (var tile in group.followerTileGrid.tiles)
                {
                    if(player.InsideMinecart.lastTouchedTiles.Contains(tile))
                    {
                        this.goingDown = true;
                        this.originalPosition = this.position;
                        DropStuckDebris();
                        return;
                    }
                }
            }
            else
            {
                CheckFloor(raycast);
                CheckFront(raycast);
                CheckCeiling(raycast);
            }
        }
        PlaySfx();

        if(this.map.NearestChunkTo(this.map.player.position) == this.map.chunks[this.map.chunks.IndexOf(chunk) + 2])
        {
            this.playerCrossedChunk = true;
        }
    }

    private void PlaySfx()
    {
        if (this.positionLastFrame != this.transform.position)
        {
            if (Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvUnstableGround) == false)
            {
                Audio.instance.PlaySfx(Assets.instance.sfxEnvUnstableGround, position: this.transform.position, options: new Audio.Options(0.6f, false, 1));
            }
            Audio.instance.ChangeSfxPosition(Assets.instance.sfxEnvUnstableGround, this.transform.position);
            this.isPlayingSfx = true;
        }
        else if (this.isPlayingSfx == true)
        {
            Audio.instance.StopSfx(Assets.instance.sfxEnvUnstableGround);
            this.isPlayingSfx = false;
        }
        this.positionLastFrame = this.transform.position;
    }

    private void SetupDebris()
    {
        foreach (var tile in group.followerTileGrid.tiles)
        {
            if (Rnd.value > 0.25f) continue;
            if (tile == null) continue;
            var gameObject = new GameObject();
            gameObject.name = $"debris ({tile.mapTx}, {tile.mapTy})";
            gameObject.transform.position = new Vector3(
                tile.mapTx,
                tile.mapTy
            );
            gameObject.transform.parent = this.group.container.transform;

            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = "Tiles - Solid";
            spriteRenderer.sortingOrder = 10;

            int random = Rnd.Range(0, crystalAnimations.Length);
            Animated.Animation randomCrystal = crystalAnimations[random];
            spriteRenderer.sprite = randomCrystal.sprites[0];
            Crystal c = new Crystal
            {
                spriteRenderer = spriteRenderer,
                stuck = randomCrystal.sprites[0],
                fall = randomCrystal.sprites[1]
            };
            debris.Add(c);
        }
    }

    private void DropStuckDebris()
    {
        if (this.debrisVisible == false) return;
        this.debrisVisible = false;
        foreach (var crystal in debris)
        {
            crystal.spriteRenderer.enabled = false;
            Particle p = Particle.CreateWithSprite(
                crystal.fall,
                300,
                new Vector3(crystal.spriteRenderer.gameObject.transform.position.x + .5f, crystal.spriteRenderer.gameObject.transform.position.y),
                this.group.container.transform
            );
            p.velocity.y = Rnd.Range(-0.1f, -0.2f);
            p.acceleration.y = Rnd.Range(-0.01f, -0.03f);
        }
    }

    private void CheckFloor(Raycast raycast)
    {
        var player = this.map.player;
        float ABSensorLength = 21 / 16f;
        float ABCDSensorOffsetX = 8 / 16f;

        var a = Probe(raycast, player.position, -ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
        var b = Probe(raycast, player.position, +ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);

        if((a.anything && a.tile != null)||(b.anything && b.tile != null))
        {
            foreach (var tile in group.followerTileGrid.tiles)
            {
                if ((a.tile != null && a.tile == tile) ||
                    (b.tile != null && b.tile == tile))
                {
                    this.goingDown = true;
                    this.originalPosition = this.position;
                    DropStuckDebris();
                }
            }
        }
    }

    private void CheckCeiling(Raycast raycast)
    {
        var player = this.map.player;
        float ABCDSensorOffsetX = 8 / 16f;
        float CDSensorLength    = 13 / 16f;
        var c = Probe(raycast, player.position, -ABCDSensorOffsetX, 0, 0, +1, CDSensorLength);
        var d = Probe(raycast, player.position, +ABCDSensorOffsetX, 0, 0, +1, CDSensorLength);         

        if((c.anything && c.tile != null) || (d.anything && d.tile != null))
        {
            foreach (var tile in group.followerTileGrid.tiles)
            {
                if ((c.tile != null && c.tile == tile) ||
                    (d.tile != null && d.tile == tile))
                {
                    this.goingDown = true;
                    this.originalPosition = this.position;
                    DropStuckDebris();
                }
            }
        }
    }

    private void CheckFront(Raycast raycast)
    {
        var player = this.map.player;
        float EFSensorLength    = 10 / 16f;
        var fLo     = Probe(raycast, player.position, 0,  -8 / 16f, +1, 0, EFSensorLength);
        var fMid    = Probe(raycast, player.position, 0,  +4 / 16f, +1, 0, EFSensorLength);
        var fHi     = Probe(raycast, player.position, 0, +12 / 16f, +1, 0, EFSensorLength);
        var f = Raycast.CombineClosest(fLo, fMid, fHi);

        if (player.state == Player.State.WallSlide)
        {
            var slideProbe = Probe(
                raycast, player.position, 0, 0, player.facingRight ? 1 : -1, 0, EFSensorLength + 5 / 16f
            );

            if (slideProbe.anything && slideProbe.tile != null)
            {
                foreach (var tile in group.followerTileGrid.tiles)
                {
                    if(slideProbe.tile == tile)
                    {
                        this.goingDown = true;
                        this.originalPosition = this.position;
                        DropStuckDebris();
                        return;
                    }
                }
            }
        }

        if (f.anything)
        {
            foreach (var tile in group.followerTileGrid.tiles)
            {
                foreach(var t in f.tiles)
                {
                    if(t == tile)
                    {
                        this.goingDown = true;
                        this.originalPosition = this.position;
                        DropStuckDebris();
                    }
                }
            }
        }
    }

    private (float x, float y) GetChunkMinXY()
    {
        return (this.chunk.xMin - this.chunk.boundaryMarkers.bottomLeft.x,
                this.chunk.yMin - this.chunk.boundaryMarkers.bottomLeft.y
                );
    }

    protected Raycast.Result Probe(
        Raycast raycast,
        Vector2 start,
        float relativeForward,
        float relativeUp,
        float directionForward,
        float directionUp,
        float length
    )
    {
        Vector2 forward = new Vector2( 1,  0);
        Vector2 up      = new Vector2( 0,  1);

        start.x += forward.x * relativeForward + up.x * relativeUp;
        start.y += forward.y * relativeForward + up.y * relativeUp;

        Vector2 direction;
        direction.x = forward.x * directionForward + up.x * directionUp;
        direction.y = forward.y * directionForward + up.y * directionUp;

        var result = raycast.Arbitrary(start, direction, length);
        
        return result;
    }

    private float TimeForFrame(int frameNumber) =>
        this.timeAtStart + (frameNumber / 60.0f);
}