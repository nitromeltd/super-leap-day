using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Rnd = UnityEngine.Random;

public class WaterCurrent : Item
{
    public enum Direction
    {
        Left, Right, Up, Down
    }
    
    public enum DirectionsType
    {
        None,
        Corner,
        Straight
    }

    public enum Speed
    {
        Slow,
        Fast
    }

    public DirectionsType directionsType;
    public Speed speed;
    public AnimationCurve fadeOutCurveForBubbleParticles;
    public AnimationCurve scaleOutCurveForBubbleParticles;

    private Direction[] connections;
    private WaterCurrent currentNextToExit = null;
    [HideInInspector] public PlayerBlocker playerBlocker;
    private bool interactuableLastFrame = false;

    private List<SeaMine> seaMines = new List<SeaMine>();
    private List<BeachBall> beachBalls = new List<BeachBall>();

    public class TrackingData
    {
        public Player player;
        public SeaMine seaMine;
        public BeachBall beachBall;
        public float distanceAlongPath;

        public Vector2 Position
        {
            get {
                if (player != null) return player.position;
                if (seaMine != null) return seaMine.position;
                if (beachBall != null) return beachBall.position;
                return player.position;
            }
            set {
                if (player != null) player.position = value;
                if (seaMine != null) seaMine.position = value;
                if (beachBall != null) beachBall.position = value;
            }
        }

        public Vector2 Velocity
        {
            get {
                if (player != null) return player.velocity;
                if (seaMine != null) return seaMine.velocity;
                if (beachBall != null) return beachBall.velocity;
                return player.velocity;
            }
            set {
                if (player != null) player.velocity = value;
                if (seaMine != null) seaMine.velocity = value;
                if (beachBall != null) beachBall.velocity = value;
            }
        }

        public SpriteRenderer SpriteRenderer
        {
            get {
                if (player != null)
                    return player.GetComponent<SpriteRenderer>();
                else if(seaMine != null)
                    return seaMine.GetComponent<SpriteRenderer>();
                else
                    return player.GetComponent<SpriteRenderer>();
            }
        }
    }
    private TrackingData trackingPlayer = null;

    private static int DisableCurrentEntryUntilFrameNumber = 0;
    private static int TimeInsideCurrent = 0;
    private static readonly float PlayerMoveSpeed = 0.20f;
    private static readonly float SeaMineMoveSpeed = 0.10f;
    private static readonly float BeachBallMoveSpeed = 0.134f;
    private static readonly float FastMultiplier = 1.35f;
    private static Vector2 JumpForce = new Vector2(0.15f, 0.35f);
    public static bool IsCurrentOn = false;

    private bool IsFast => this.speed == Speed.Fast;
    private float CurrentPlayerMoveSpeed => IsFast ?
        PlayerMoveSpeed * FastMultiplier : PlayerMoveSpeed;
    private float CurrentSeaMineMoveSpeed => IsFast ?
        SeaMineMoveSpeed * FastMultiplier : SeaMineMoveSpeed;
    private float CurrentBeachBallMoveSpeed => IsFast ?
        BeachBallMoveSpeed * FastMultiplier : BeachBallMoveSpeed;
    private float ExitSpeed => IsFast ? 0.1f : 0.1f;

    private bool IsCorner =>
        this.directionsType == DirectionsType.Corner;

    private Vector2 Center => this.position;

    private Direction EnterDirection => this.connections[0];
    private Direction ExitDirection => this.connections[1];
    public bool IsInteractuable => this.position.y < this.map.waterline?.currentLevel; 
    public Rect EntryRect => this.hitboxes[0].InWorldSpace().Inflate(-0.50f);
    
    private bool ShouldWaitForPlayer()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        return playerChunk.index < this.chunk.index && this.chunk.isHorizontal == true;
    }

    public override void Init(Def def)
    {
        base.Init(def);

        this.spriteRenderer.enabled = false;

        this.trackingPlayer = new TrackingData();
        this.trackingPlayer.player = this.map.player;

        switch(this.directionsType)
        {
            case DirectionsType.Straight:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -0.5f, 0.5f, this.rotation, Hitbox.NonSolid, true, true)
                };
                if (def.tile.rotation == 0)
                    this.connections = new [] { Direction.Down, Direction.Up };
                else if (def.tile.rotation == 90)
                    this.connections = new [] { Direction.Right, Direction.Left };
                else if (def.tile.rotation == 180)
                    this.connections = new [] { Direction.Up, Direction.Down };
                else if (def.tile.rotation == 270)
                    this.connections = new [] { Direction.Left, Direction.Right };
            }
            break;

            case DirectionsType.Corner:
            {
                this.hitboxes = new [] {
                    new Hitbox(-1, 1, -1f, 1f, this.rotation, Hitbox.NonSolid, true, true)
                };

                this.rotation = def.tile.rotation;
                if (def.tile.flip == true)
                {
                    this.transform.localScale = new Vector3(-1f, 1f, 1f);
                    
                    if (this.rotation == 0)
                        this.connections = new [] { Direction.Left, Direction.Up };
                    else if (this.rotation == 90)
                        this.connections = new [] { Direction.Down, Direction.Left };
                    else if (this.rotation == 180)
                        this.connections = new [] { Direction.Right, Direction.Down };
                    else if (this.rotation == 270)
                        this.connections = new [] { Direction.Up, Direction.Right };
                }
                else
                {
                    if (this.rotation == 0)
                        this.connections = new [] { Direction.Right, Direction.Up };
                    else if (this.rotation == 90)
                        this.connections = new [] { Direction.Up, Direction.Left };
                    else if (this.rotation == 180)
                        this.connections = new [] { Direction.Left, Direction.Down };
                    else if (this.rotation == 270)
                        this.connections = new [] { Direction.Down, Direction.Right };
                }
            }
            break;
        }

        this.currentNextToExit = this.chunk.NearestItemTo<WaterCurrent>(
            this.position + DirectionToVector2(ExitDirection), 1.5f
        );

        if(this.currentNextToExit == this)
        {
            this.currentNextToExit = null;
        }
        
        this.playerBlocker = this.chunk.NearestItemTo<PlayerBlocker>(
            this.position, 1f
        );

        DisableCurrentEntryUntilFrameNumber = 0;
        TimeInsideCurrent = 0;
    }

    public override void NotifyNewPlayerCreatedForMultiplayer(Player replacementPlayer)
    {
        this.trackingPlayer.player = replacementPlayer;
    }

    public Vector2 DirectionToVector2(Direction direction, float magnitude = 1)
    {
        switch (direction)
        {
            case Direction.Left:  return magnitude * Vector2.left;
            case Direction.Right: return magnitude * Vector2.right;
            case Direction.Down:  return magnitude * Vector2.down;
            case Direction.Up:    return magnitude * Vector2.up;
        }
        return default;
    }

    public Direction Vector2ToDirection(Vector2 vector)
    {
        if (Mathf.Abs(vector.x) > Mathf.Abs(vector.y))
            return vector.x > 0 ? Direction.Right : Direction.Left;
        else
            return vector.y > 0 ? Direction.Up : Direction.Down;
    }

    public Direction Invert(Direction direction)
    {
        switch (direction)
        {
            case Direction.Left:  return Direction.Right;
            case Direction.Right: return Direction.Left;
            case Direction.Down:  return Direction.Up;
            case Direction.Up:    return Direction.Down;
        }
        return default;
    }

    private bool IsThisCurrentExit()
    {
        if(this.currentNextToExit != null)
        {
            return Invert(this.currentNextToExit.EnterDirection) != ExitDirection;
        }
        else
        {
            return true;
        }
    }

    public float GetThingMoveSpeed(TrackingData thing)
    {
        if(thing.seaMine != null)
        {
            return CurrentSeaMineMoveSpeed;
        }

        if(thing.beachBall != null)
        {
            return CurrentBeachBallMoveSpeed;
        }

        return CurrentPlayerMoveSpeed;
    }

    public override void Advance()
    {
        if(ShouldWaitForPlayer()) return;
        
        if(IsInteractuable == true)
        {
            if(this.interactuableLastFrame == false)
            {
                seaMines = this.chunk.subsetOfItemsOfTypeSeaMine;
                beachBalls = this.chunk.subsetOfItemsOfTypeBeachBall;
            }

            TestForPlayerEntry();

            foreach (var e in seaMines)
                TestForSeaMineEntry(e);

            foreach (var e in beachBalls)
                TestForBeachBallEntry(e);

            if(IsCurrentOn == false)
            {
                IsCurrentOn = true;
                if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxEnvCurrentLoop))
                {
                    Audio.instance.PlaySfxLoop(
                        Assets.instance.sfxEnvCurrentLoop,
                        position: this.position
                    );
                }
            }
        }
        else
        {
            if (IsCurrentOn == true)
            {
                IsCurrentOn = false;
                Audio.instance.StopSfxLoop(
                    Assets.instance.sfxEnvCurrentLoop
                );
            }
        }

        float distanceToPlayer = Vector2.Distance(this.map.player.position, this.position);

        if(this.map.frameNumber % 25 == 0 &&
            IsInteractuable == true &&
            distanceToPlayer < 20f)
        {
            Vector2 startPosition = this.position +
                (Vector2)this.transform.up * -0.50f +
                (Vector2)this.transform.right * Random.Range(-1f, 1f);

            int lifetime = this.speed == Speed.Fast ? 35 : 50;

            Particle p = Particle.CreateWithSprite(
                Assets.instance.sunkenIsland.RandomBubbleParticle,
                lifetime,
                startPosition,
                this.transform
            );

            p.scaleOut.duration = lifetime;
            p.scaleOut.startValue = 1.5f;
            p.scaleOut.enabled = true;
            p.scaleOut.curve = scaleOutCurveForBubbleParticles;

            float localSpeed = Random.Range(0.01f, 0.05f) *
                (this.speed == Speed.Fast ? 3f : 1.5f);
            p.velocity = DirectionToVector2(ExitDirection) * localSpeed;
            p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
            p.FadeOut(this.fadeOutCurveForBubbleParticles);
        }

        this.interactuableLastFrame = IsInteractuable;
    }

    private bool IsPlayerStateValidForEntry()
    {
        var player = this.map.player;
        
        if(player.state == Player.State.Normal) return true;
        if(player.state == Player.State.Jump) return true;
        if(player.state == Player.State.Spring) return true;
        if(player.state == Player.State.SpringFromPushPlatform) return true;
        if(player.state == Player.State.InsideWater) return true;
        if(player is PlayerGoop && player.state == Player.State.GoopBallSpinning) return true;

        return false;
    }

    private void TestForPlayerEntry()
    {
        var player = this.map.player;
        var data = this.trackingPlayer;

        if (this.map.frameNumber < DisableCurrentEntryUntilFrameNumber)
            return;
        if (IsPlayerStateValidForEntry() == false)
            return;
        if(player.grabbingOntoItem != null)
            return;
        if(player.alive == false)
            return;

        if (player.Rectangle().Overlaps(EntryRect))
        {
            player.EnterWaterCurrent(this);
            player.velocity = DirectionToVector2(ExitDirection, CurrentPlayerMoveSpeed);
            TimeInsideCurrent = 0;
        }
    }

    private void TestForSeaMineEntry(SeaMine seaMine)
    {
        if (seaMine.insideWaterCurrent != null)
            return;

        var data = seaMine.waterCurrentTrackingData;
        if (data == null)
            data = seaMine.waterCurrentTrackingData = new TrackingData { seaMine = seaMine };
        if (this.map.frameNumber < DisableCurrentEntryUntilFrameNumber)
            return;

        if (seaMine.hitboxes[0].InWorldSpace().Overlaps(EntryRect))
        {
            seaMine.EnterWaterCurrent(this);
            seaMine.velocity = DirectionToVector2(ExitDirection, CurrentSeaMineMoveSpeed);
            TimeInsideCurrent = 0;
        }
    }
    private void TestForBeachBallEntry(BeachBall beachBall)
    {
        if (beachBall.insideWaterCurrent != null)
            return;

        var data = beachBall.waterCurrentTrackingData;
        if (data == null)
            data = beachBall.waterCurrentTrackingData = new TrackingData { beachBall = beachBall };
        if (this.map.frameNumber < DisableCurrentEntryUntilFrameNumber)
            return;

        if (beachBall.HitboxPlayerRect.Overlaps(EntryRect))
        {
            beachBall.EnterWaterCurrent(this);
            beachBall.velocity = DirectionToVector2(ExitDirection, CurrentBeachBallMoveSpeed);
            TimeInsideCurrent = 0;
        }
    }
    
    public void AdvancePlayer()
    {
        this.trackingPlayer.player = this.map.player;
        AdvanceThing(this.trackingPlayer);
    }

    public void AdvanceSeaMine(SeaMine seaMine) =>
        AdvanceThing(seaMine.waterCurrentTrackingData);

    public void AdvanceBeachBall(BeachBall beachBall) =>
        AdvanceThing(beachBall.waterCurrentTrackingData);

    private void AdvanceThing(TrackingData thing)
    {
        TimeInsideCurrent += 1;
        
        Direction forward = Vector2ToDirection(thing.Velocity);
        Direction backward = Invert(forward);

        var forwardVector = DirectionToVector2(forward);
        var forwardAmount = Vector2.Dot(thing.Position - Center, forwardVector);
        forwardAmount += GetThingMoveSpeed(thing);

        if (forwardAmount > 0 && this.connections.Contains(forward) == false)
        {
            forward = this.connections.Where(c => c != backward).FirstOrDefault();
            forwardVector = DirectionToVector2(forward);
        }

        Vector2 targetPosition = Center + (forwardVector * forwardAmount);
        Vector2 currentPosition = thing.Position;
        currentPosition.x = TimeInsideCurrent < 5 ?
            Util.Slide(currentPosition.x, targetPosition.x, 0.15f) : targetPosition.x;
        currentPosition.y = TimeInsideCurrent < 5 ?
            Util.Slide(currentPosition.y, targetPosition.y, 0.15f) : targetPosition.y;

        thing.Position = currentPosition;
        thing.Velocity = forwardVector * GetThingMoveSpeed(thing);

        float GetCurrentForward()
        {
            if(IsThisCurrentExit() && ExitDirection == forward)
            {
                return 1f;
            }

            if(IsCorner == true)
            {
                return 1f;
            }

            return 0.50f;
        }

        if(IsInteractuable == false)
        {
            ThingExitWaterCurrent(thing, forwardVector);
        }
        else if (forwardAmount > GetCurrentForward())
        {
            if (IsThisCurrentExit() && ExitDirection == forward)
            {
                ThingExitWaterCurrent(thing, forwardVector);
            }
            else
            {
                ThingTransitionToNextPipePiece(thing, forwardVector);
            }
        }
        
        if (thing.player != null)
        {
            this.map.player.transform.position = this.map.player.position;
        }
        
        if (thing.seaMine == true)
        {
            thing.seaMine.transform.position = thing.seaMine.position;
        }
        
        if (thing.beachBall == true)
        {
            thing.beachBall.transform.position = thing.beachBall.position;
        }
    }

    private void ThingTransitionToNextPipePiece(
        TrackingData thing, Vector2 forwardVector
    )
    {
        var nextCurrentPiece = this.chunk.NearestItemTo<WaterCurrent>(
            thing.Position, 2
        );
        
        if(nextCurrentPiece != null &&
            thing.player != null &&
            nextCurrentPiece.playerBlocker != null)
        {
            ThingExitWaterCurrent(thing, -forwardVector);
            return;
        }

        if(nextCurrentPiece != null)
        {
            nextCurrentPiece.ThingEnterWaterCurrent(thing);
            
            if (thing.player != null)
            {
                this.map.player.EnterWaterCurrent(nextCurrentPiece);
            }
            
            if (thing.seaMine != null)
            {
                thing.seaMine.EnterWaterCurrent(nextCurrentPiece);
            }
            
            if (thing.beachBall != null)
            {
                thing.beachBall.EnterWaterCurrent(nextCurrentPiece);
            }
        }
    }

    private void ThingEnterWaterCurrent(TrackingData thing)
    {
        if(thing.player != null)
        {
            Player player = this.map.player;

            if(player.facingRight == true && ExitDirection == Direction.Left)
            {
                player.facingRight = false;
            }

            if(player.facingRight == false && ExitDirection == Direction.Right)
            {
                player.facingRight = true;
            }
        }
    }

    public void ThingExitWaterCurrent(TrackingData thing, Vector2 forwardVector)
    {
        thing.Velocity = GetExitVelocity(thing, forwardVector);
        thing.Position = Center + (forwardVector * 1.5f);

        if(thing.player != null)
        {
            var player = this.map.player;
            player.NotifyExitWaterCurrent();
            DisableCurrentEntryUntilFrameNumber = this.map.frameNumber + 10;
        }
        else if(thing.seaMine != null)
        {
            thing.seaMine.NotifyExitWaterCurrent();
            DisableCurrentEntryUntilFrameNumber = this.map.frameNumber + 10;
        }
        else if(thing.beachBall != null)
        {
            thing.beachBall.NotifyExitWaterCurrent();
            DisableCurrentEntryUntilFrameNumber = this.map.frameNumber + 10;
        }
    }

    private Vector2 GetExitVelocity(TrackingData thing, Vector2 forwardVector)
    {
        Vector2 exitVelocity = thing.Velocity;

        if(thing.player != null)
        {
            if (forwardVector.y < 0)
            {
                return forwardVector * ExitSpeed * 3f;
            }
            else
            {
                return forwardVector * ExitSpeed;
            }
        }
        else if(thing.seaMine != null)
        {
            if (forwardVector.y < 0)
            {
                return forwardVector * ExitSpeed * 1.50f;
            }
            else
            {
                return forwardVector * ExitSpeed;
            }
        }
        else if(thing.beachBall != null)
        {
            if (forwardVector.y < 0)
            {
                return forwardVector * ExitSpeed * 1.50f;
            }
            else
            {
                return forwardVector * ExitSpeed;
            }
        }

        return exitVelocity;
    }

    public void PlayerJumped()
    {
        Player player = this.map.player;

        Vector2 playerVelocity = Vector2.zero;
        Direction direction = ExitDirection;

        switch(direction)
        {
            case Direction.Left:
            case Direction.Right:
                playerVelocity = new Vector2(JumpForce.x * (player.facingRight ? 1f : -1f), JumpForce.y);
            break;

            case Direction.Up:
            case Direction.Down:
                playerVelocity = new Vector2(JumpForce.y * (player.facingRight ? 1f : -1f), -JumpForce.x);
            break;
        }

        if(CanPlayerJump(playerVelocity))
        {
            ThingExitWaterCurrent(this.trackingPlayer, Vector2.up);
            player.velocity = playerVelocity;
        }
    }

    private bool CanPlayerJump(Vector2 playerVelocity)
    {
        Vector2 jumpDirection = playerVelocity.normalized;

        var raycast = new Raycast(
            this.map, this.position, isCastByEnemy: true, ignoreTileTopOnlyFlag: true);

        var r = raycast.Arbitrary(this.position, jumpDirection, 1.50f);

        return r.anything == false;
    }
}
