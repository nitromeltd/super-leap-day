using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterRaiserTrigger : Item
{
    public Transform grabTransform;
    public AnimationCurve moveCurve;

    private int ignoreTime = 0;
    private bool grabbed = false;
    private int grabTime = 0;
    private bool alreadyTriggered = false;
    private GrabType grabType;

    private const float GrabSize = 0.40f;
    private const int SwitchTime = 60;

    private WaterMarker connectedMarker;
    private bool wasPlayerInThisChunkLastFrame = false;

    public override void Init(Def def)
    {
        base.Init(def);

        this.transform.localScale = new Vector3(def.tile.flip ? -1 : 1, 1, 1);
        this.rotation = def.tile.flip ? 0 : 270;
        this.grabType =  def.tile.flip ? GrabType.LeftWall : GrabType.RightWall;

        this.hitboxes = new Hitbox[0];
        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        foreach (var path in this.chunk.connectionLayer.paths)
        {
            var nearestConnectionNode = path.NearestNodeTo(this.position, 2);
            if (nearestConnectionNode != null)
            {
                var otherNode =
                    nearestConnectionNode.Previous() ??
                    nearestConnectionNode.Next();
                this.connectedMarker =
                    this.chunk.NearestItemTo<WaterMarker>(otherNode.Position);
                this.connectedMarker.Connect(this);
            }
        }
    }

    private Vector2 GetGrabRelativePosition()
    {
        float x = this.grabTransform.localPosition.x * this.transform.localScale.x;
        return new Vector2(x, this.grabTransform.localPosition.y);
    }
    
    public override void Advance()
    {
        CheckForPlayerExit();

        if (this.group != null)
            this.transform.position = this.position = this.group.FollowerPosition(this);

        AdvanceHanging();
        AdvanceGrabbed();
        
        this.grabInfo = new GrabInfo(this, this.grabType, GetGrabRelativePosition());
    }

    private void CheckForPlayerExit()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.map.player.position);
        bool isPlayerInThisChunk = playerChunk == this.chunk;

        if(wasPlayerInThisChunkLastFrame == true && isPlayerInThisChunk == false)
        {
            // player exiting chunk
            Reset();
        }

        this.wasPlayerInThisChunkLastFrame = isPlayerInThisChunk;
    }

    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null) return;

        this.grabbed = false;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - SensorPosition();
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreTime = 15;
            this.grabbed = true;

            if (alreadyTriggered == false)
            {
                Audio.instance.PlaySfx(
                    Assets.instance.sfxEnvPullswitch,
                    position: this.position
                );
            }
            
        }
    }

    public override void Reset()
    {
        this.grabTransform.localPosition =
            new Vector2(this.grabTransform.localPosition.x, 0f);
        this.grabbed = false;
        this.grabTime = 0;
        this.ignoreTime = 0;
        this.alreadyTriggered = false;
    }

    public Vector2 SensorPosition()
    {
        return this.position + GetGrabRelativePosition();
    }

    private void AdvanceGrabbed()
    {
        float yGrabPosition = Mathf.Lerp(0f, -1f,
            this.moveCurve.Evaluate(Mathf.InverseLerp(0, SwitchTime, this.grabTime)));

        this.grabTransform.localPosition =
            new Vector2(this.grabTransform.localPosition.x, yGrabPosition);

        if(this.grabbed == true)
        {
            if(this.grabTime < SwitchTime)
            {
                this.grabTime += 1;
            }

            if(this.grabTime >= SwitchTime)
            {
                ActivateSwitch();
            }
        }
    }
    
    private void ActivateSwitch()
    {
        if(this.connectedMarker == null) return;
        if(this.alreadyTriggered == true) return;

        this.alreadyTriggered = true;
        this.grabTime = SwitchTime;

        this.connectedMarker.RaiseWater();
    }

}
