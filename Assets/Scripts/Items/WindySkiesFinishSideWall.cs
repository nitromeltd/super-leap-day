using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindySkiesFinishSideWall : Item
{
    public override void Init(Def def)
    {
        base.Init(def);

        if(def.tile.flip == true)
            this.transform.localScale = new Vector3(-1, 1, 1);
    }
}
