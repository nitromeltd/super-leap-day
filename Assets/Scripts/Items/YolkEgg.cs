using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class YolkEgg : Item
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationShake;
    public Animated.Animation animationExploding;
    public Animated.Animation animationExplosion;

    private float bounciness;
    private int hopTimer = 0;
    private float smoothVelocityH = 0f;
    private float rotationCurrentZ = 0;
    private float rotationTargetZ = 0;
    private float rotationSmoothZ = 0;
    private bool isBreaking = false;
    private bool isPlayerOnTop = false;
    private bool isExploding = false;
    private bool explosion = false;
    private int explodeTimer = 0;

    private const float HorizontalMomentum = 0.1f;
    private const float BreakEggFallingSpeed = -0.55f;
    private const float EggTiltAmount = 15f;
    private const float HitboxSize = 1f;
    private const int HopCooldownTime = 40;
    private const int ExplodeTime = 90;
    private static Vector2 HopForce = new Vector2(.05f, .2f);

    // gravity
    private Gravity gravity;
    private Vector2 velocity;
    private Tile onGroundTile = null;
    private Item onGroundItem = null;
    private bool IsOnGround => this.onGroundTile != null || this.onGroundItem is StartBarrier;
    private static readonly float FreeFloatingMaxSpeed = 0.31f;
    private static readonly float FreeFloatingMinSpeed = 0.0625f;
    protected static readonly float GravityAcceleration = 0.015f;

    public struct Gravity
    {
        public YolkEgg yolkEgg;
        public Player.Orientation? gravityDirection;

        public Player.Orientation Orientation() =>
            this.gravityDirection ?? Player.Orientation.Normal;
        public bool IsZeroGravity() => gravityDirection == null;

        private static Vector2[] Directions =
            new [] { Vector2.down, Vector2.right, Vector2.up, Vector2.left };

        public Vector2 Down()  => Directions[(int)Orientation()];
        public Vector2 Right() => Directions[((int)Orientation() + 1) % 4];
        public Vector2 Up()    => Directions[((int)Orientation() + 2) % 4];
        public Vector2 Left()  => Directions[((int)Orientation() + 3) % 4];

        public int OrientationAngle()
        {
            if(IsZeroGravity() == true) return 0;

            switch(Orientation())
            {
                case Player.Orientation.Normal:
                default:
                    return 0;

                case Player.Orientation.LeftWall:
                    return 270;

                case Player.Orientation.RightWall:
                    return 90;

                case Player.Orientation.Ceiling:
                    return 180;
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => Orientation() switch
            {
                Player.Orientation.Normal    => yolkEgg.velocity,
                Player.Orientation.RightWall => new Vector2(yolkEgg.velocity.y, -yolkEgg.velocity.x),
                Player.Orientation.Ceiling   => new Vector2(-yolkEgg.velocity.x, -yolkEgg.velocity.y),
                _                            => new Vector2(-yolkEgg.velocity.y, yolkEgg.velocity.x)
            };
            set {
                switch (Orientation())
                {
                    case Player.Orientation.Normal:    yolkEgg.velocity = value; break;
                    case Player.Orientation.RightWall: yolkEgg.velocity = new Vector2(-value.y, value.x); break;
                    case Player.Orientation.Ceiling:   yolkEgg.velocity = new Vector2(-value.x, -value.y); break;
                    default:                           yolkEgg.velocity = new Vector2(value.y, -value.x); break;
                }
            }
        }
    }

    public void Init(Vector2 position, Chunk chunk)
    {
        base.Init(new Def(chunk));

        this.position = position;
        this.transform.position = position;
        this.chunk = chunk;
        
        this.hitboxes = new Hitbox[]
        {
            new Hitbox(-HitboxSize, HitboxSize, -HitboxSize, HitboxSize, 0, Hitbox.NonSolid)
        };

        this.whenChunkResets = ChunkResetBehaviour.PersistAndCallReset;
        this.gravity = new Gravity {
                yolkEgg = this,
                gravityDirection = this.map.player.gravity.Orientation()
        };

        float orientationAngle = this.gravity.OrientationAngle();
        float facingEggTilt = this.map.player.facingRight ? -EggTiltAmount : EggTiltAmount;
        this.rotationCurrentZ = this.rotationTargetZ = orientationAngle + facingEggTilt;
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotationCurrentZ);

        this.bounciness = -0.1f;
        this.hopTimer = HopCooldownTime;
        this.explodeTimer = 0;
        this.isPlayerOnTop = true;
        
        this.animated.PlayAndLoop(this.animationIdle);
    }

    public override void Advance()
    {
        if(this.gameObject.activeSelf == false) return;

        UpdateGravity();

        CheckExplosion();
        CheckHop();

        AdvancePhysics();

        this.rotationCurrentZ = Mathf.SmoothDampAngle(
            this.rotationCurrentZ, this.rotationTargetZ, ref rotationSmoothZ, .15f);

        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotationCurrentZ);
        this.transform.position = this.position;
    }

    private void CheckExplosion()
    {
        PlayerYolk playerYolk = this.map.player as PlayerYolk;

        if(playerYolk.power == PlayerYolk.Power.LayEgg
            && this.isPlayerOnTop == false)
        {
            return;
        }

        if(this.isExploding == true) return;

        this.explodeTimer += 1;

        if(this.explodeTimer >= ExplodeTime)
        {
            TriggerExplosionSequence();
        }
    }

    private void TriggerExplosionSequence()
    {
        this.explodeTimer = 0;
        this.isExploding = true;
        this.animated.PlayOnce(this.animationExploding, ExecuteExplosion);
        (this.map.player as PlayerYolk).RemoveCheckpointEgg(this);
    }

    private void ExecuteExplosion()
    {
        this.isExploding = false;
        this.explosion = true;
        this.animated.PlayOnce(this.animationExplosion);

        this.animated.OnFrame(4, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxEnvExplosion);
            Break();

            this.map.Damage(
                this.hitboxes[0].InWorldSpace().Inflate(1.5f),
                new Enemy.KillInfo { itemDeliveringKill = this },
                affectPlayer: true
            );
        });
    }

    private void CheckHop()
    {
        if(this.explosion == true) return;

        var player = this.map.player as PlayerYolk;

        if(player.alive == false ||
            player.state == Player.State.Respawning ||
            player.state == Player.State.YolkLayingEgg)
        {
            return;
        }

        if(this.hopTimer > 0)
        {
            this.hopTimer -= 1;
            return;
        }
        
        if(this.hopTimer < (HopCooldownTime - 5))
        {
            rotationTargetZ = 0f;
        }

        var delta = player.position - this.position;

        float hopDirection = player.facingRight ? 1 : -1;

        if (Mathf.Abs(delta.x) < 1.2 && Mathf.Abs(delta.y) < 1.1)
        {
            Hop(hopDirection);
        }
    }

    private void UpdateGravity()
    {
        Player.Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        var intendedGravity = SampleAt(this.position);
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        if (this.gravity.gravityDirection == Player.Orientation.Ceiling &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Normal) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Player.Orientation.Ceiling)
                intendedGravity = Player.Orientation.Ceiling;
        }
        else if (this.gravity.gravityDirection == Player.Orientation.Normal &&
            (intendedGravity == null || intendedGravity == Player.Orientation.Ceiling) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Player.Orientation.Normal)
                intendedGravity = Player.Orientation.Normal;
        }

        if (this.gravity.gravityDirection != intendedGravity)
        {
            this.gravity.gravityDirection = intendedGravity;
        }
    }

    private void AdvancePhysics()
    {
        if(this.explosion == true) return;

        AdvanceGravity();

        var raycast = new Raycast(this.map, this.position);
        var rect = this.hitboxes[0];
        var start = this.position;

        if (this.velocity.x < 0)
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMin, 0),
                false,
                -this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x -= r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }
        else
        {
            var r = raycast.Horizontal(
                this.position + new Vector2(rect.xMax, 0),
                true,
                this.velocity.x
            );
            if (r.anything == true)
            {
                this.position.x += r.distance;
                this.velocity.x *= this.bounciness;
            }
            else
            {
                this.position.x += this.velocity.x;
            }
        }

        if (this.velocity.y < 0)
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMin),
                false,
                -this.velocity.y
            );
            if (r.anything == true)
            {
                if(this.velocity.y < BreakEggFallingSpeed)
                {
                    Break();
                }

                this.position.y -= r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else
        {
            var r = raycast.Vertical(
                this.position + new Vector2(0, rect.yMax),
                true,
                this.velocity.y
            );
            if (r.anything == true)
            {
                this.position.y += r.distance;
                this.velocity.y *= this.bounciness;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }

        var rFloor = new Raycast(this.map, this.position).Arbitrary(
            this.position + this.gravity.Down() * HitboxSize,
            this.gravity.Down(),
            0.10f
        );

        this.onGroundTile = rFloor.tile;
        this.onGroundItem = rFloor.item;

        // apply drag on floor
        if(IsOnGround == true)
        {
            float newVelocityOnGravityX = Mathf.SmoothDamp(
                this.gravity.VelocityInGravityReferenceFrame.x, 0f, ref smoothVelocityH, HorizontalMomentum
            );

            this.gravity.VelocityInGravityReferenceFrame =  new Vector2(
                newVelocityOnGravityX,
                this.gravity.VelocityInGravityReferenceFrame.y
            );
        }

        if(this.onGroundTile != null)
        {
            Vector2 surfaceVelocity = this.onGroundTile.tileGrid.effectiveVelocity;
            this.position.x += surfaceVelocity.x;
            this.position.y += surfaceVelocity.y;
        }
    }

    private void AdvanceGravity()
    {
        var gravityDirectionVector = this.gravity.Down();

        if (this.gravity.IsZeroGravity() == true)
        {
            var speed = this.velocity.magnitude;
            if (speed > FreeFloatingMaxSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMaxSpeed, 0.01f);
                this.velocity.Normalize();
                this.velocity *= speed;
            }
            else if (speed < FreeFloatingMinSpeed)
            {
                speed = Util.Slide(speed, FreeFloatingMinSpeed, 0.001f);
                if (this.velocity.sqrMagnitude == 0)
                    this.velocity = this.gravity.Down();
                else
                    this.velocity.Normalize();
                this.velocity *= speed;
            }

            this.rotationTargetZ = Util.WrapAngle0To360(this.rotationTargetZ + 0.5f);
        }
        else
        {
            this.velocity += GravityAcceleration * gravityDirectionVector;
            
            if(this.isPlayerOnTop == false)
            {
                if(IsOnGround == true && this.hopTimer == 0)
                {
                    float GetGroundAngleDegrees()
                    {
                        if(this.onGroundTile != null)
                        {
                            return this.onGroundTile.SurfaceAngleDegrees(this.position, this.gravity.Down());
                        }

                        return 0f;
                    }

                    this.rotationTargetZ = GetGroundAngleDegrees();
                }
                
                if(this.gravity.IsZeroGravity())
                {
                    this.rotationTargetZ = this.gravity.OrientationAngle();
                }
            }
        }
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }

    public void Shake()
    {
        this.animated.PlayAndLoop(this.animationShake);
    }

    public void Hop(float direction)
    {
        // smack particle effect
        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyHitPowAnimation,
            this.position.Add(direction * -0.25f, 0.50f),
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Enemies";
        p.spriteRenderer.sortingOrder = -1;

        this.map.timeFrozenByEnemyDeath = true;

        StartCoroutine(_HopResumeTime(direction));
        Audio.instance.PlaySfx(Assets.instance.sfxPlayerJump[0]);
    }

    private IEnumerator _HopResumeTime(float direction)
    {
        yield return new WaitForSecondsRealtime(0.1f);
        this.map.timeFrozenByEnemyDeath = false;

        this.hopTimer = HopCooldownTime;
        this.gravity.VelocityInGravityReferenceFrame = new Vector2(HopForce.x * direction, HopForce.y);
        this.rotationTargetZ += 15f * direction;
    }

    public void Break()
    {
        if(this.gameObject.activeSelf == false) return;
        if(this.isBreaking == true) return;

        this.isBreaking = true;
        (this.map.player as PlayerYolk).RemoveCheckpointEgg(this);

        Audio.instance.PlaySfx(Assets.instance.sfxPlayerEggBreak);

        Vector2 fragmentPosition = this.position.Add(0, -0.50f);

        PlayerYolk playerYolk = this.map.player as PlayerYolk;

        var p = Particle.CreateWithSprite(
            playerYolk.eggFragments[0], 100, fragmentPosition, this.transform.parent
        );
        p.velocity = new Vector2(
            -1.5f + Rnd.Range(-0.5f, 0.5f),
            5     + Rnd.Range(-0.5f, 0.5f)
        ) / 16;
        p.acceleration = new Vector2(0, -0.5f) / 16;

        p = Particle.CreateWithSprite(
            playerYolk.eggFragments[1], 100, fragmentPosition, this.transform.parent
        );
        p.velocity = new Vector2(
            1.5f + Rnd.Range(-0.5f, 0.5f),
            6    + Rnd.Range(-0.5f, 0.5f)
        ) / 16;
        p.acceleration = new Vector2(0, -0.5f) / 16;

        for (var n = 0; n < 5; n += 1)
        {
            var angle = Rnd.Range(0f, Util.Tau);
            var speed = Rnd.Range(1.5f, 2.5f) / 16;
            p = Particle.CreateAndPlayOnce(
                playerYolk.animationEggYolkParticle, this.position, this.transform.parent
            );
            p.velocity = new Vector2(
                Mathf.Cos(angle) * speed,
                Mathf.Sin(angle) * speed
            );
            p.acceleration = p.velocity * -0.03f / 16;
        }

        Hide();
    }

    public Vector2 GetCheckpointPos()
    {
        return this.position;
    }

    public void SeparateFromPlayer()
    {
        this.isPlayerOnTop = false;
        this.rotationTargetZ = this.gravity.OrientationAngle();
    }
}