using UnityEngine;
using System;
using System.Collections.Generic;
using NE = NitromeEditor;

public class Zipline : Item
{
    [NonSerialized] public NitromeEditor.Path path;
    public Sprite lineSprite;
    public SpriteRenderer pointSpriteRenderer;

    override public void Init(Def def)
    {
        base.Init(def);

        this.path = PathStartingNearestTo(this.chunk.pathLayer, this.position, 2);

        var points = new List<Vector2>();
        for (float along = 0; along < this.path.length; along += 0.5f)
        {
            points.Add(this.path.PointAtDistanceAlongPath(along).position);
        }
        if (this.path.closed == false)
        {
            points.Add(this.path.PointAtDistanceAlongPath(this.path.length).position);
        }

        this.GetComponent<PathRenderer>().Draw(
            this.lineSprite, points.ToArray(), this.path.closed, "Spikes"
        );

        this.pointSpriteRenderer.transform.position =
            this.path.PointAtDistanceAlongPath(0).position;

        for (int n = 1; n < this.path.nodes.Length; n += 1)
        {
            var copy = Instantiate(this.pointSpriteRenderer, this.transform);
            copy.transform.position = this.path.PointAtDistanceAlongPath(
                this.path.nodes[n].distanceAlongPath
            ).position;
        }
    }

    NE.Path PathStartingNearestTo(NE.PathLayer layer, Vector2 pt, float maxDistance)
    {
        var maxSqDistance = maxDistance * maxDistance;
        NE.Path result = null;

        foreach (var path in layer.paths)
        {
            var dx = pt.x - path.nodes[0].x;
            var dy = pt.y - path.nodes[0].y;
            var sqDist = (dx * dx) + (dy * dy);
            if (sqDist >= maxSqDistance) continue;

            maxSqDistance = sqDist;
            result = path;
        }

        return result;
    }
}
