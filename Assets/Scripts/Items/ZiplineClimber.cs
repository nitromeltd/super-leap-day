
using UnityEngine;
using System;
using System.Linq;

public class ZiplineClimber : Item
{
    public SpriteRenderer cog;

    [NonSerialized] public Zipline zipline;
    [NonSerialized] public float distanceThrough = 0;
    [NonSerialized] public float speed = 0;
    [NonSerialized] public int ignoreTime = 0;

    public override void Init(Def def)
    {
        base.Init(def);

        var nearestZipline = this.chunk.NearestItemTo<Zipline>(this.position, 1);
        if (nearestZipline != null)
        {
            this.zipline = nearestZipline;
        }
        else
        {
            var nearestPath = def.startChunk.pathLayer.NearestPathTo(this.position, 2);

            foreach (var zipline in this.chunk.items.OfType<Zipline>())
            {
                if (zipline.path == nearestPath)
                    this.zipline = zipline;
            }
        }

        if (this.zipline == null) return;

        this.position = this.zipline.path.nodes[0].Position;
        this.grabInfo = new GrabInfo(this, GrabType.Ceiling, new Vector2(0, -1.5f));
    }

    public override void Advance()
    {
        if (this.zipline == null) return;

        if (this.map.player.grabbingOntoItem == this)
            this.speed = Util.Slide(this.speed, 2 / 16f, 0.2f / 16f);
        else
            this.speed = Util.Slide(this.speed, -3 / 16f, 0.1f / 16f);

        this.distanceThrough += this.speed;
        if (this.distanceThrough < 0)
        {
            this.distanceThrough = 0;
            if      (this.speed < 1) this.speed *= -0.5f;
            else if (this.speed < 0) this.speed = 0;
        }
        if (this.distanceThrough > this.zipline.path.length)
        {
            this.distanceThrough = this.zipline.path.length;
            if      (this.speed > 1) this.speed *= -0.5f;
            else if (this.speed > 0) this.speed = 0;
        }

        var p = this.zipline.path.PointAtDistanceAlongPath(this.distanceThrough).position;
        this.position = p;
        this.transform.position = p;

        this.cog.transform.localRotation = Quaternion.Euler(
            0, 0, this.distanceThrough * -60
        );

        AdvanceHanging();
    }

    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.grabbingOntoItem != null) return;

        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var thisPosition = this.position + new Vector2(0, -2);
        var delta = player.position - thisPosition;
        if (Mathf.Abs(delta.x) < 1 &&
            Mathf.Abs(delta.y) < 1 &&
            player.ShouldCollideWithItems())
        {
            player.grabbingOntoItem = this;
            this.ignoreTime = 15;
        }
    }
}
