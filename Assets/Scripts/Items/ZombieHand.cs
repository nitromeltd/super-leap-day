using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieHand : Item
{
    public Animated.Animation animationIdle;
    public Animated.Animation animationIdleFlavor;
    public Animated.Animation animationDrop;
    public Animated.Animation animationSpawn;
    public Animated.Animation animationHide;
    public Sprite dropSprite;

    private int ignoreTime = 0;
    private int grabTime = 0;
    private Vector3 smoothVelocityScale;
    private Vector2 shakePosition;
    private bool hidden = false;
    private int respawnTime = 0;
    private int flavorTime = 0;

    private static Vector2 ShakeAmount = Vector2.one * 0.03f;
    private const float ShakeSpeed = 0.80f;
    private const int TotalDropTime = 1 * 60;
    private const int RespawnTimeAfterDrop = 1 * 60;
    private const int FlavorTotalTime = 5 * 60;
    
    public override void Init(Def def)
    {
        base.Init(def);

        GrabType grabHookType = GrabType.None;
        Vector2 grabHookPosRelativeToItem = Vector2.zero;

        // define GrabType base on rotation and flip
        if(def.tile.flip == true)
        {
            this.transform.localScale = new Vector3(-1f, 1f, 1f);

            if (this.rotation == 0)
            {
                grabHookType = GrabType.RightWall;
            }
            else if (this.rotation == 90)
            {
                grabHookType = GrabType.Ceiling;
                grabHookPosRelativeToItem = Vector2.up * -0.2f;
            }
            else if (this.rotation == 180)
            {
                grabHookType = GrabType.LeftWall;
                this.spriteRenderer.flipY = true;
            }
            else if (this.rotation == 270)
            {
                grabHookType = GrabType.Floor;
                this.position.y += 0.15f;
            }
        }
        else
        {
            if (this.rotation == 0)
            {
                grabHookType = GrabType.LeftWall;
            }
            else if (this.rotation == 90)
            {
                grabHookType = GrabType.Floor;
                this.position.y += 0.15f;
            }
            else if (this.rotation == 180)
            {
                grabHookType = GrabType.RightWall;
                this.spriteRenderer.flipY = true;
            }
            else if (this.rotation == 270)
            {
                grabHookType = GrabType.Ceiling;
                grabHookPosRelativeToItem = Vector2.up * -0.2f;
            }
        }
        

        this.grabInfo = new GrabInfo(this, grabHookType, grabHookPosRelativeToItem);

        this.hitboxes = new [] {
            new Hitbox(
                grabHookPosRelativeToItem.x - 0.4f,
                grabHookPosRelativeToItem.x + 0.4f,
                grabHookPosRelativeToItem.y - 0.4f,
                grabHookPosRelativeToItem.y + 0.4f,
                this.rotation,
                Hitbox.NonSolid
            )
        };

        AttachToGroup(this.chunk.groupLayer.RegisterFollower(this, def.tx, def.ty));

        this.animated.PlayOnce(this.animationIdle);
    }
    
    public override void Advance()
    {
        if(this.respawnTime > 0)
        {
            this.respawnTime -= 1;

            if(this.respawnTime <= 0)
            {
                this.respawnTime = 0;
                Spawn();
            }
        }

        bool isVisible = this.hidden == false && this.respawnTime == 0;

        if(isVisible == true)
        {
            AdvanceHanging();

            if(this.map.player.grabbingOntoItem == this)
            {
                this.flavorTime = 0;
                this.grabTime += 1;
                Shake();
            }
            else
            {
                this.grabTime = 0;
                this.shakePosition = Vector2.zero;

                if(this.flavorTime < FlavorTotalTime)
                {
                    this.flavorTime += 1;

                    if(this.flavorTime == FlavorTotalTime)
                    {
                        this.flavorTime = 0;

                        this.animated.PlayOnce(this.animationIdleFlavor,
                            () => { this.animated.PlayOnce(this.animationIdle); }
                        );
                    }
                }
            }

            if(this.grabTime > TotalDropTime)
            {
                Drop();
            }
        }

        if (this.group != null)
        {
            this.transform.position = this.position =
                this.group.FollowerPosition(this) + this.shakePosition;
        }
        else
        {
            this.transform.position = this.position + this.shakePosition;
        }
    }

    private void AdvanceHanging()
    {
        var player = this.map.player;
        if (player.ShouldCollideWithItems() == false) return;
        if (player.grabbingOntoItem != null)
        {
            if (player.grabbingOntoItem == this)
            {
                this.ignoreTime = 30;
            }
            return;
        }
        if (this.ignoreTime > 0)
        {
            this.ignoreTime -= 1;
            return;
        }

        var delta = player.position - SensorPosition();
        if (Mathf.Abs(delta.x) < 1 && Mathf.Abs(delta.y) < 1)
        {
            player.GrabItem(this);
            this.ignoreTime = 15;
            this.animated.PlayOnce(this.animationIdle);
        }
    }

    public Vector2 SensorPosition()
    {
        switch(this.grabInfo.grabType)
        {
            case Item.GrabType.Floor:
                return this.position + (Vector2.up * 1f);

            case Item.GrabType.RightWall:
            case Item.GrabType.LeftWall:
            case Item.GrabType.Ceiling:
            default:
                return this.position;

        }
    }

    private void Shake()
    {
        Vector2 shake = 
            Mathf.Sin(Map.instance.frameNumber * ShakeSpeed) * ShakeAmount;

        float shakeX = Random.Range(0.50f, 1f) * shake.x;
        float shakeY = Random.Range(0.50f, 1f) * shake.y;
        this.shakePosition = new Vector2(shakeX, shakeY);
    }

    private void Drop()
    {
        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxZombieHandBreak, null, this.position
        );

        this.respawnTime = RespawnTimeAfterDrop;
        this.animated.PlayAndHoldLastFrame(this.animationDrop);

        {
            var p = Particle.CreateWithSprite(
                this.dropSprite, 100, this.position, this.transform.parent
            );

            p.transform.localScale = this.transform.localScale;
            p.transform.localRotation = this.transform.localRotation;

            float directionValue =
                (this.map.player.position.x < this.position.x) ? 1f : -1f;

            if(def.tile.flip == true)
            {
                directionValue *= 1f;
            }

            float averageVelocityX = -0.05f * directionValue;

            p.Throw(new Vector2(averageVelocityX, 0.15f), 0.025f, 0.05f, new Vector2(0, -0.025f));
            p.spin = UnityEngine.Random.Range(2.0f, 3.0f) * directionValue;

            p.spriteRenderer.flipY = this.spriteRenderer.flipY;
        }
        
        if(this.map.player.grabbingOntoItem == this)
        {
            this.map.player.grabbingOntoItem = null;

            if(this.map.player.groundState.onGround == false)
            {
                this.map.player.state = Player.State.DropVertically;
                this.map.player.velocity = Vector2.zero;
            }
        }
    }
    
    // use this method first if Zombie hand spawning should be controlled by other element
    public void Hide()
    {
        this.animated.PlayOnce(this.animationHide);
        this.hidden = true;
    }

    // use this method to make Zombie hand appear (controlled by other element)
    [ContextMenu("Spawn")]
    public void Spawn()
    {
        this.flavorTime = 0;
        
        Audio.instance.PlaySfx(
            Assets.instance.tombstoneHill.sfxZombieHandAppear, null, this.position
        );
        this.animated.PlayOnce(this.animationSpawn,
            () => { this.hidden = false; }
        );
    }
}
