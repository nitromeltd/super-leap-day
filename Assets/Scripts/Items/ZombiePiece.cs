using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombiePiece : Enemy
{
    public enum Type
    {
        Head,
        LeftArm,
        RightArm,
        Torso,
        LeftLeg,
        RightLeg
    }

    private Type pieceType;
    private bool visible = false;
    private bool movingBackToZombie = false;
    private Vector2 velocity;
    private bool facingRight;
    private float turnSensorForward = 0;
    private bool onGround = false;
    private float groundSensorSpacing = 0.375f;
    private Vector3 currentVelocityPosition;
    private float currentVelocityRotation;
    private Vector2 shakePosition = Vector2.zero;
    private int extraShakeValue;

    private Hitbox HeadHitbox => new Hitbox(-0.5f, 0.5f, -0.55f, 0.40f, this.rotation, Hitbox.NonSolid);
    private Hitbox ArmHitbox => new Hitbox(-0.5f, 0.5f, -0.15f, 0.10f, this.rotation, Hitbox.NonSolid);
    private Hitbox TorsoHitbox => new Hitbox(-0.5f, 0.5f, -0.45f, 0.35f, this.rotation, Hitbox.NonSolid);
    private Hitbox LegHitbox => new Hitbox(-0.5f, 0.5f, -0.15f, 0.10f, this.rotation, Hitbox.NonSolid);

    private static Vector2 ShakeAmount = Vector2.one * 0.03f;
    private const float ShakeSpeed = 0.80f;
    private const float Gravity = 0.03125f;

    public static ZombiePiece Create(Vector2 position, Chunk chunk, Type type, SpriteRenderer spriteRenderer)
    {
        var obj = Instantiate(Assets.instance.tombstoneHill.zombiePiece, chunk.transform);
        obj.name = $"Zombie {type}";

        ZombiePiece zombiePiece = obj.GetComponent<ZombiePiece>();
        zombiePiece.Init(position, chunk, type, spriteRenderer);
        chunk.items.Add(zombiePiece);
        return zombiePiece;
    }

    public void Init(Vector2 position, Chunk chunk, Type type, SpriteRenderer spriteRenderer)
    {
        base.Init(new Def(chunk));

        this.transform.position = this.position = position;
        this.chunk = chunk;
        this.spriteRenderer.sprite = spriteRenderer.sprite;
        this.spriteRenderer.sortingOrder = spriteRenderer.sortingOrder;
        this.extraShakeValue = Random.Range(0, 30);
        
        this.pieceType = type;
        
        Hitbox PieceHitbox()
        {
            switch(this.pieceType)
            {
                case Type.Head:
                default:
                    return HeadHitbox;

                case Type.LeftArm:
                case Type.RightArm:
                    return ArmHitbox;

                case Type.Torso:
                    return TorsoHitbox;

                case Type.LeftLeg:
                case Type.RightLeg:
                    return LegHitbox;
            }
        }

        this.hitboxes = new Hitbox[] { PieceHitbox() };
    }
    
    public override void Advance()
    {
        if(this.visible == true && this.movingBackToZombie == false)
        {
            this.velocity.y -= Gravity;

            float horizontalDrag = this.onGround ? 0.01f : 0.0025f;
            this.velocity.x = Util.Slide(this.velocity.x, 0f, horizontalDrag);

            var raycast = new Raycast(this.map, this.position, isCastByEnemy: true);
            MoveHorizontally(raycast);
            MoveVertically(raycast);

            this.transform.position = this.position + this.shakePosition;
        }
    }

    private void MoveHorizontally(Raycast raycast)
    {
        float forwardAmount = Mathf.Abs(velocity.x);
        if (forwardAmount == 0)
            return;

        this.facingRight = velocity.x > 0;

        var forward = SenseHorizontal(
            raycast, this.facingRight, forwardAmount, out float correctionX
        );

        this.position.x += this.facingRight ? forwardAmount : -forwardAmount;
        
        if(forward.anything == true)
        {
            this.position.x += correctionX;
            this.velocity.x *= -1;
            this.facingRight = (this.velocity.x > 0);
        }
    }

    protected Raycast.CombinedResults SenseHorizontal(
        Raycast raycast, bool right, float distance, out float correctionOffsetX
    )
    {
        var rect = this.hitboxes[0];
        var high = this.position.Add(0, rect.yMax - 0.1f);
        var mid  = this.position.Add(0, (rect.yMin + rect.yMax) * 0.5f);
        var low  = this.position.Add(0, rect.yMin + 0.1f);

        var maxDistance = right ?
            rect.xMax + turnSensorForward + distance :
            -rect.xMin + turnSensorForward + distance;

        var rHigh = raycast.Horizontal(high, right, maxDistance);
        var rMid  = raycast.Horizontal(mid,  right, maxDistance);
        var rLow  = raycast.Horizontal(low,  right, maxDistance);

        if (right)
        {
            if (rHigh.surfaceAngleDegrees <= 45) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees < 90) rMid.anything = false;
            if (rLow.surfaceAngleDegrees < 90) rLow.anything = false;
        }
        else
        {
            if (rHigh.surfaceAngleDegrees >= 315) rHigh.anything = false;
            if (rMid.surfaceAngleDegrees > 270) rMid.anything = false;
            if (rLow.surfaceAngleDegrees > 270) rLow.anything = false;
        }

        var result = Raycast.CombineClosest(rLow, rMid, rHigh);

        correctionOffsetX = right ?
            result.distance - maxDistance :
            maxDistance - result.distance;
        return result;
    }

    private void MoveVertically(Raycast raycast)
    {
        this.onGround = false;

        var rect = this.hitboxes[0];

        if (this.velocity.y > 0)
        {
            var maxDistance = this.velocity.y + rect.yMax;
            var result = raycast.Vertical(this.position, true, maxDistance);
            if (result.anything == true &&
                result.distance < this.velocity.y + rect.yMax)
            {
                this.position.y += result.distance - rect.yMax;
                this.velocity.y = 0;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
        else if (this.velocity.y < 0)
        {
            var tolerance = 0.1f;
            var start = this.position;
            var maxDistance = -this.velocity.y - rect.yMin + tolerance;
            var dx = this.groundSensorSpacing;
            var result = Raycast.CombineClosest(
                raycast.Vertical(start.Add(-dx, 0), false, maxDistance),
                raycast.Vertical(start.Add(  0, 0), false, maxDistance),
                raycast.Vertical(start.Add( dx, 0), false, maxDistance)
            );
            if (result.anything == true &&
                result.distance < -this.velocity.y - rect.yMin + tolerance)
            {
                Vector2 surfaceVelocity = Vector2.zero;
                if (result.tiles.Length > 0)
                    surfaceVelocity = result.tiles[0].tileGrid.effectiveVelocity;

                this.position.x += surfaceVelocity.x;
                this.position.y += surfaceVelocity.y;
                this.position.y += -result.distance - rect.yMin;
                this.velocity.y = 0;
                this.onGround = true;
            }
            else
            {
                this.position.y += this.velocity.y;
            }
        }
    }

    public void Shake()
    {
        int shakeValue = Map.instance.frameNumber + this.extraShakeValue;
        Vector2 shake = 
            Mathf.Sin(shakeValue * ShakeSpeed) * ShakeAmount;

        float shakeX = Random.Range(0.50f, 1f) * shake.x;
        float shakeY = Random.Range(0.50f, 1f) * shake.y;
        this.shakePosition = new Vector2(shakeX, shakeY);
    }

    public void MoveBackTo(Vector2 targetPosition, float targetRotation)
    {
        this.movingBackToZombie = true;
        this.shakePosition = Vector2.zero;

        this.position = Vector3.SmoothDamp(this.position, targetPosition, ref currentVelocityPosition, 0.10f);
        this.transform.position = this.position;

        this.rotation = Mathf.RoundToInt(Mathf.SmoothDamp(this.rotation, targetRotation, ref currentVelocityRotation, 0.10f));
        this.transform.rotation = Quaternion.Euler(Vector3.forward * this.rotation);
    }

    public void Hide()
    {
        this.movingBackToZombie = false;
        this.visible = false;
    }

    public void Throw(Vector2 force)
    {
        this.velocity = force;
        this.visible = true;
    }
}
