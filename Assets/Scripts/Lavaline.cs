using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Lavaline
{
    private Map map;
    public float currentLevel;
    public float targetLevel;

    [HideInInspector] public Transform lavaTransform;
    private float currentLavaRiseSpeed;
    private LoopPath loopPath;
    public Heightmap heightmap;
    private MeshRenderer lavaMeshRenderer;
    private MeshFilter lavaMeshFilter;
    private MeshRenderer lavaTopRowMeshRenderer;
    private MeshFilter lavaTopRowMeshFilter;
    private Mesh lavaMesh;
    private Mesh lavaTopRowMesh;
    private Chunk lastFramePlayerChunk;

    private bool isSolid = false;
    private int transitionTime = 1000;
    private int? timeRemainingSolid = null;

    public const float DefaultRiseSpeed = 0.15f;
    public const float LevelThreshold = 0.05f;
    public const int GracePeriodStillSolid = 50;

    public int transparentCount = 0;

    public class Bubble
    {
        public bool isBig;
        public SpriteRenderer spriteRenderer;
        public Animated animated;
        public Vector2 velocity;
        public bool isAtSurface;
        public bool isSurfacePopAnimationPlaying;
    }
    public Bubble[] bubbles;

    public bool IsMoving => this.targetLevel != this.currentLevel;
    public bool IsSolid => this.isSolid;
    public bool IsSolidIncludingGracePeriod =>
        this.isSolid == true || this.transitionTime < GracePeriodStillSolid;
    public float VelocityThreshold => Mathf.Abs(Velocity().y) + LevelThreshold;  
    private bool IsLoop => this.loopPath != null && this.loopPath?.loopPoints != null;     

    public Vector2 Velocity()
    {
        if(IsMoving == true)
        {
            return this.currentLavaRiseSpeed *
                (this.currentLevel > this.targetLevel ? Vector2.down : Vector2.up); 
        }

        return Vector2.zero;
    }

    public class LoopPath
    {
        public LoopPoint[] loopPoints;
        public int currentIndex;
        public float delayTimer;
        public bool controlByExternalItem;
        public bool closedLoop;
        private bool movingForward;

        public LoopPoint CurrentTargetPoint => this.loopPoints[currentIndex];
        public bool HasReachTarget(float currentLevel) =>
            CurrentTargetPoint.targetLevel == currentLevel;

        public LoopPath(LoopPoint[] loopPoints, bool controlByExternalItem, bool closedLoop)
        {
            this.loopPoints = loopPoints;
            this.currentIndex = 0;
            this.delayTimer = 0;
            this.controlByExternalItem = controlByExternalItem;
            this.closedLoop = closedLoop;
            this.movingForward = true;
        }

        public void GoToNextPoint()
        {
            this.currentIndex = NextIndex();
            this.delayTimer = 0;
        }

        private int NextIndex()
        {
            if(this.closedLoop == false)
            {
                if(this.movingForward == true)
                {
                    if(this.currentIndex + 1 >= this.loopPoints.Length)
                    {
                        this.movingForward = false;
                        return this.currentIndex - 1;
                    }
                    else
                    {
                        return this.currentIndex + 1;
                    }
                } 
                else
                {
                    if(this.currentIndex - 1 < 0)
                    {
                        this.movingForward = true;
                        return this.currentIndex + 1;
                    }
                    else
                    {
                        return this.currentIndex - 1;
                    }
                }
            }

            return (this.currentIndex + 1 >= this.loopPoints.Length) ?
                0 : this.currentIndex + 1;
        }

        public bool WaitForDelay()
        {
            return this.delayTimer < CurrentTargetPoint.delayTime;
        }
    }

    public struct LoopPoint
    {
        public float targetLevel;
        public float riseSpeed;
        public float delayTime;

        public LoopPoint(float targetLevel, float riseSpeed, float delayTime)
        {
            this.targetLevel = targetLevel;
            this.riseSpeed = riseSpeed;
            this.delayTime = delayTime;
        }
    }

    public Lavaline(Map map)
    {
        this.map = map;

        Chunk playerCheckpointChunk =
            this.map.NearestChunkTo(map.player.checkpointPosition);
        this.currentLevel = this.targetLevel = playerCheckpointChunk.yMin;
        
        this.currentLavaRiseSpeed = DefaultRiseSpeed;
        this.loopPath = null;

        this.lavaTransform = GameObject.Instantiate(Assets.instance.moltenFortress.lava).transform;
        this.lavaTransform.parent = map.transform;

        this.lavaMeshRenderer = this.lavaTransform.GetComponent<MeshRenderer>();
        this.lavaMeshFilter = this.lavaTransform.GetComponent<MeshFilter>();
        this.lavaTopRowMeshRenderer =
            this.lavaTransform.Find("Top Row").GetComponent<MeshRenderer>();
        this.lavaTopRowMeshFilter =
            this.lavaTransform.Find("Top Row").GetComponent<MeshFilter>();
        this.lavaMesh = new Mesh();
        this.lavaTopRowMesh = new Mesh();
        this.lavaTopRowMeshRenderer.sortingOrder += 1;

        this.heightmap = new Heightmap(this);
        this.heightmap.SetBoundaries(-1, 17);
        this.heightmap.Advance();
        this.heightmap.DrawTo(this.lavaMeshFilter, this.lavaMesh);
        
        SetLavaColor();

        this.bubbles = new Bubble[10];
        for (int n = 0; n < this.bubbles.Length; n += 1)
        {
            var bubble = this.bubbles[n] = new Bubble();
            var gameObject = new GameObject("Bubble");
            gameObject.transform.SetParent(this.lavaMeshRenderer.transform, false);
            bubble.spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            bubble.spriteRenderer.sprite =
                Assets.instance.moltenFortress.spriteBigBubbleSubmerged;
            bubble.spriteRenderer.sortingOrder = 1;
            bubble.animated = gameObject.AddComponent<Animated>();
            StartBubble(bubble);
        }
    }

    public void ChangeLevel(float newTargetLevel, float riseSpeed, bool evenIfSolid)
    {
        if(IsSolid == true && evenIfSolid == false) return;
        if(this.loopPath == null && this.targetLevel == newTargetLevel) return;

        this.targetLevel = newTargetLevel;
        this.currentLavaRiseSpeed = riseSpeed;
        this.loopPath = null;

        CheckIfCurrentLevelIsTooFar();
    }

    public void LoopLevel(
        List<LavaMarker> loopMarkers,
        bool controlByExternalItem = false,
        bool closedLoop = true,
        bool forceFirstNode = false)
    {
        if(IsSolid == true) return;

        LoopPoint[] loopPoints = new LoopPoint[loopMarkers.Count];
        
        for (int i = 0; i < loopMarkers.Count; i++)
        {
            loopPoints[i] = loopMarkers[i].GenerateLoopPoint();
        }

        if(this.loopPath != null &&
            IsSameLoop(this.loopPath.loopPoints, loopPoints) &&
            forceFirstNode == false)
        {
            MoveToNextLoopPoint();
            return;
        }

        this.loopPath = new LoopPath(loopPoints,
            controlByExternalItem: controlByExternalItem, closedLoop: closedLoop);
        this.targetLevel = this.loopPath.CurrentTargetPoint.targetLevel;
        this.currentLavaRiseSpeed = this.loopPath.CurrentTargetPoint.riseSpeed;
        
        CheckIfCurrentLevelIsTooFar();
    }

    private void CheckIfCurrentLevelIsTooFar()
    {
        float yMinCamera = this.map.gameCamera.VisibleRectangle().yMin;
        float cameraLevel = yMinCamera - 5;

        // If current level is lower than the camera bottom (with an extra margin),
        // we made the current level jump to that height immediately
        if(this.targetLevel > this.currentLevel && cameraLevel > this.currentLevel)
        {
            this.currentLevel = cameraLevel;
        }
    }

    private bool IsSameLoop(LoopPoint[] lp1, LoopPoint[] lp2)
    {
        if(lp1.Length != lp2.Length) return false;

        for (int i = 0; i < lp1.Length; i++)
        {
            if(lp1[i].targetLevel != lp2[i].targetLevel)
            {
                return false;
            }
        }

        return true;
    }

    private void MoveToNextLoopPoint()
    {
        if(this.loopPath == null) return;

        this.loopPath.GoToNextPoint();
        this.targetLevel = this.loopPath.CurrentTargetPoint.targetLevel;
        this.currentLavaRiseSpeed =
            this.loopPath.CurrentTargetPoint.riseSpeed;
    }

    public void Advance()
    {
        if (this.timeRemainingSolid != null)
        {
            this.timeRemainingSolid -= 1;
            if (this.timeRemainingSolid < 1)
            {
                ChangeSolid(false);
                this.timeRemainingSolid = null;
            }
        }

        this.transitionTime += 1;

        AdvancePlayer();
        SetLavaColor();

        foreach (var bubble in this.bubbles)
            AdvanceBubble(bubble);

        if (this.isSolid == false && this.transitionTime >= GracePeriodStillSolid)
        {
            if (this.IsLoop == true &&
                this.loopPath.controlByExternalItem == false)
            {
                if(this.loopPath.HasReachTarget(this.currentLevel))
                {
                    if(this.loopPath.WaitForDelay())
                    {
                        this.loopPath.delayTimer += 1f / 60f;
                    }
                    else
                    {
                        MoveToNextLoopPoint();
                    }
                }
            }

            this.currentLevel = Util.Slide(
                this.currentLevel, this.targetLevel, this.currentLavaRiseSpeed
            );
        }

        var activeChunks = this.map.chunks.Where(
            c =>
                c.gameObject.activeSelf == true
        ).ToArray();

        var cameraRectangle = this.map.gameCamera.VisibleRectangle();

        this.heightmap.SetBoundaries(cameraRectangle.xMin - 1, cameraRectangle.xMax + 1);
        this.lavaTransform.position =
            new Vector2(this.heightmap.xMin, this.currentLevel);

        this.heightmap.Advance();
        this.heightmap.DrawTo(this.lavaMeshFilter, this.lavaMesh);
        this.heightmap.DrawTopRowTo(this.lavaTopRowMeshFilter, this.lavaTopRowMesh);

        /*if(IsMoving == true && !Audio.instance.IsSfxPlaying(Assets.instance.sfxWaterRise) && this.targetLevel > this.currentLevel)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxWaterRise, position: new Vector2(lavaTransform.position.x, currentLevel), options: new Audio.Options(1, false, 0));
            Audio.instance.ChangeSfxPitch(Assets.instance.sfxWaterRise, currentLevel / targetLevel);
        }
        else if (IsMoving == true)
        {
            Audio.instance.ChangeSfxPitch(Assets.instance.sfxWaterRise, currentLevel / targetLevel);
            Audio.instance.ChangeSfxPosition(Assets.instance.sfxWaterRise, new Vector2(lavaTransform.position.x, currentLevel));
        }
        else if(IsMoving == false && Audio.instance.IsSfxPlaying(Assets.instance.sfxWaterRise))
        {
            Audio.instance.StopSfx(Assets.instance.sfxWaterRise);
        }*/
    }

    private void AdvancePlayer()
    {
        Player player = this.map.player;

        if(player.alive == false) 
        {
            this.lastFramePlayerChunk = null;
            return;
        }
        
        if(player.state == Player.State.Respawning) return;
        if(player.state == Player.State.InsideLift) return;
        if(player.state == Player.State.Finished) return;

        Chunk currentPlayerChunk = this.map.NearestChunkTo(player.position);
        bool enteringChunk = currentPlayerChunk != lastFramePlayerChunk;

        int currentChunkIndex = currentPlayerChunk != null ? currentPlayerChunk.index : -1;
        int lastFrameChunkIndex = lastFramePlayerChunk != null ? lastFramePlayerChunk.index : -1;
        bool movingForward = currentChunkIndex > lastFrameChunkIndex;

        // In certain situations we want to control the lava level ourselves to avoid issues
        if(enteringChunk == true && movingForward == true)
        {
            bool autoTriggerLavaMarker = false;

            foreach(var wm in currentPlayerChunk.items.OfType<LavaMarker>())
            {
                if(wm.AutoTrigger == true)
                {
                    wm.RaiseLava(forceFirstNode: true);
                    autoTriggerLavaMarker = true;
                }
            }

            if(autoTriggerLavaMarker == false)
            {
                // Everytime we enter a new checkpoint or a chunk with a trophy,
                // the lava will rise to the bottom of the chunk
                bool riseLevelForCheckpointChunk =
                    currentLevel < currentPlayerChunk.yMin &&
                    (currentPlayerChunk.AnyItem<Checkpoint>() || currentPlayerChunk.AnyItem<Trophy>());

                // If we enter a chunk for the first time and the lava level is already
                // covering it, we will lower it down to the bottom of the current chunk
                // Practical example of what this is solving: we have two horizontal chunks,
                // one after the other, and the first one increases the lava level to the top,
                // while the second one is assuming the lava is not covering it when the player enters
                bool lowerTheLevelForCurrentChunk = currentLevel > currentPlayerChunk.yMin;
                
                if (riseLevelForCheckpointChunk == true)
                {
                    ChangeLevel(currentPlayerChunk.yMin - 3, 0.02f, true);
                }
                else if (lowerTheLevelForCurrentChunk == true)
                {
                    ChangeLevel(currentPlayerChunk.yMin - 3, 0.20f, true);
                }
                else
                {
                    this.loopPath = null;
                }
            }
        }

        this.lastFramePlayerChunk = currentPlayerChunk;
    }

    public void ResetToCheckpoint()
    {
        Chunk playerCheckpointChunk =
            this.map.NearestChunkTo(this.map.player.checkpointPosition);
        this.currentLevel = this.targetLevel = playerCheckpointChunk.yMin;
        this.loopPath = null;

        ChangeSolid(false);
        this.transitionTime = 1000;
    }

    public bool IsDeadly()
    {
        return true;
    }

    public void SetLavaColor()
    {
        this.lavaMeshRenderer.material.SetFloat("_LavaCenterX", this.map.gameCamera?.transform.position.x ?? 8);
        this.lavaMeshRenderer.material.SetFloat("_LavaTopY", this.currentLevel);
        this.lavaMeshRenderer.material.SetFloat("_TransitionTime", this.transitionTime);
        this.lavaMeshRenderer.material.SetFloat("_IsTransitioningToRock", this.isSolid ? 1 : 0);

        this.lavaTopRowMeshRenderer.material.SetFloat("_TransitionTime", this.transitionTime);
        this.lavaTopRowMeshRenderer.material.SetFloat("_IsTransitioningToRock", this.isSolid ? 1 : 0);
    }

    public void ToggleSolid()
    {
        ChangeSolid(!this.isSolid);
    }

    private void ChangeSolid(bool solid)
    {
        this.isSolid = solid;
        this.transitionTime = 0;
    }

    public void MakeSolidTemporarily(int frames)
    {
        if (this.isSolid == false)
            ChangeSolid(true);

        this.timeRemainingSolid = frames;
    }

    private void StartBubble(Bubble bubble)
    {
        var cameraRect = this.map.gameCamera?.VisibleRectangle() ?? new Rect(0, 16, 0, 0);
        bubble.isBig = Util.RandomChoice(true, false);
        bubble.spriteRenderer.sprite =
            bubble.isBig ?
            Assets.instance.moltenFortress.spriteBigBubbleSubmerged :
            Assets.instance.moltenFortress.spriteSmallBubbleSubmerged;
        bubble.spriteRenderer.transform.position = new Vector3(
            Random.Range(cameraRect.xMin, cameraRect.xMax),
            this.currentLevel - 10,
            0
        );
        bubble.spriteRenderer.sortingOrder = 1;
        bubble.velocity = new Vector2(0, Random.Range(0.03f, 0.06f));
    }

    private void AdvanceBubble(Bubble bubble)
    {
        Sprite YellowSprite() =>
            bubble.isBig ?
            Assets.instance.moltenFortress.spriteBigBubbleSurface :
            Assets.instance.moltenFortress.spriteSmallBubbleSurface;
        Animated.Animation PopAnimation() =>
            bubble.isBig ?
            Assets.instance.moltenFortress.animationBigBubblePop :
            Assets.instance.moltenFortress.animationSmallBubblePop;

        Vector2 position = bubble.spriteRenderer.transform.position;
        float alpha = bubble.spriteRenderer.color.a;

        if (this.isSolid == true)
        {
            bubble.velocity *= 0.85f;
            bubble.velocity = Vector2.MoveTowards(bubble.velocity, Vector2.zero, 0.001f);
        }
        else
        {
            bubble.velocity.x += Random.Range(-0.002f, 0.002f);
            bubble.velocity.y += Random.Range(-0.002f, 0.002f);
            if (bubble.velocity.y < 0.01f)
                bubble.velocity.y = 0.01f;
        }

        position += bubble.velocity;

        var surfaceHeight =
            this.currentLevel + this.heightmap.DisplacementAtX(position.x);
        var distanceBelowSurface = surfaceHeight - position.y;

        if (bubble.isAtSurface == false)
        {
            if (distanceBelowSurface < 1.2f)
            {
                alpha = Util.Slide(alpha, 0, 0.1f);
                if (alpha <= 0)
                {
                    bubble.isAtSurface = true;
                    bubble.spriteRenderer.sprite = YellowSprite();
                    bubble.spriteRenderer.sortingOrder = -1;
                    alpha = 1;
                }
            }
            else if (this.isSolid == true)
                alpha = Util.Slide(alpha, 0, 0.1f);
            else if (this.isSolid == false && this.transitionTime > GracePeriodStillSolid)
                alpha = Util.Slide(alpha, 1, 0.05f);
        }
        else
        {
            if (bubble.isSurfacePopAnimationPlaying == false && distanceBelowSurface < 0)
            {
                bubble.isSurfacePopAnimationPlaying = true;
                bubble.animated.PlayOnce(PopAnimation(), () =>
                {
                    bubble.isAtSurface = false;
                    bubble.isSurfacePopAnimationPlaying = false;
                    StartBubble(bubble);
                });
            }
            else if (bubble.isSurfacePopAnimationPlaying == true)
            {
                position.y = surfaceHeight;
            }
        }

        bubble.spriteRenderer.transform.position = position;
        bubble.spriteRenderer.color = new Color(1, 1, 1, alpha);
    }

    public class Heightmap
    {
        public Map map;
        public Lavaline lavaline;
        public const float Spacing = 0.25f;
        public float xMin;
        public float xMax;
        public Point[] points;

        private List<Vector3> meshVertices = new();
        private List<Color>   meshColors   = new();
        private List<Vector2> meshUVs      = new();
        private List<int>     meshIndices  = new();
        private float[] newValues; // temporary, but a member to avoid reallocating

        public struct Point
        {
            public float displacement;
            public float displacementLastFrame;
        }

        public Heightmap(Lavaline lavaline)
        {
            this.map = lavaline.map;
            this.lavaline = lavaline;
        }

        public void SetBoundaries(float xMin, float xMax)
        {
            float oldXMin = this.xMin;
            float oldXMax = this.xMax;
            xMin = Mathf.FloorToInt(xMin / Spacing) * Spacing;
            xMax = Mathf.CeilToInt(xMax / Spacing) * Spacing;

            if (xMin == oldXMin && xMax == oldXMax)
                return;

            var oldPoints = this.points;
            int count = 1 + Mathf.CeilToInt((xMax - xMin) / Spacing);
            if (this.points == null || this.points.Length < count)
                this.points = new Point[count];
            this.xMin = xMin;
            this.xMax = xMax;

            if (oldPoints != null)
            {
                for (int n = 0; n < this.points.Length; n += 1)
                {
                    float x = xMin + (n * Spacing);
                    int indexFromOld = Mathf.RoundToInt((x - oldXMin) / Spacing);
                    indexFromOld = Mathf.Clamp(indexFromOld, 0, oldPoints.Length - 1);
                    this.points[n] = oldPoints[indexFromOld];
                }
            }
            else
            {
                for (int n = 0; n < this.points.Length; n += 1)
                {
                    this.points[n] = default;
                }
            }
        }

        public float DisplacementAtX(float x)
        {
            float indexFloat = (x - this.xMin) / Spacing;
            int indexOnLeft = Mathf.FloorToInt(indexFloat);
            if (indexOnLeft < 0)
                return this.points[0].displacement;
            if (indexOnLeft >= this.points.Length - 1)
                return this.points[this.points.Length - 1].displacement;

            float toNextIndex = indexFloat - indexOnLeft;
            return Mathf.Lerp(
                this.points[indexOnLeft].displacement,
                this.points[indexOnLeft + 1].displacement,
                indexFloat - indexOnLeft
            );
        }

        public void Advance()
        {
            if (this.lavaline.isSolid == true ||
                this.lavaline.transitionTime < GracePeriodStillSolid)
            {
                for (int n = 0; n < this.points.Length; n += 1)
                {
                    var last = this.points[n].displacement;
                    var speed = this.points[n].displacement - this.points[n].displacementLastFrame;
                    this.points[n].displacement += speed * 0.5f;
                    this.points[n].displacement *= 0.9f;
                    this.points[n].displacement = Util.Slide(this.points[n].displacement, 0, 0.002f);
                    this.points[n].displacementLastFrame = last;
                }
                return;
            }

            if (this.newValues == null || this.newValues.Length < this.points.Length)
                this.newValues = new float[this.points.Length];
            int frame = this.map.frameNumber;

            for (int n = 0; n < this.points.Length; n += 1)
            {
                int nMinus1 = Mathf.Clamp(n - 1, 0, this.points.Length - 1);
                int nPlus1  = Mathf.Clamp(n + 1, 0, this.points.Length - 1);

                float surroundingValues = (
                    this.points[nMinus1].displacement +
                    this.points[nPlus1].displacement
                ) * .5f;
                float speed =
                    this.points[n].displacement -
                    this.points[n].displacementLastFrame;

                this.newValues[n] = surroundingValues;
                this.newValues[n] += speed * .97f;
                this.newValues[n] -= (this.points[n].displacement * .05f);

                if (this.lavaline.currentLevel != this.lavaline.targetLevel)
                {
                    this.newValues[n] += 0.004f * Mathf.Sin((frame * 0.3f) + (n * 0.06f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.2f) + (n * 0.10f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.12f) + (n * 0.15f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.06f) + (n * 0.09f));
                }
                else
                {
                    this.newValues[n] += 0.006f * Mathf.Sin((frame * 0.12f) + (n * 0.15f));
                    this.newValues[n] += 0.006f * Mathf.Sin((frame * 0.06f) + (n * 0.09f));
                }
            }

            void PushLava(Rect rectangle, float velocityY)
            {
                if (rectangle.yMin > this.lavaline.currentLevel) return;
                if (rectangle.yMax < this.lavaline.currentLevel) return;

                var first = Mathf.RoundToInt((rectangle.xMin - 2 - this.xMin) / Spacing);
                if (first >= this.points.Length) return;

                var last = Mathf.RoundToInt((rectangle.xMax + 2 - this.xMin) / Spacing);
                if (last < 0) return;

                var centerX = (rectangle.xMin + rectangle.xMax) * 0.5f;
                var centerIndex = Mathf.RoundToInt((centerX - this.xMin) / Spacing);
                int halfwidth = centerIndex - first;
                first = Mathf.Clamp(first, 0, this.points.Length - 1);
                last  = Mathf.Clamp(last, 0, this.points.Length - 1);
                for (int n = first; n <= last; n += 1)
                {
                    float centralness =
                        1.0f - ((float)Mathf.Abs(n - centerIndex) / halfwidth);
                    this.newValues[n] += velocityY * 0.14f * centralness;
                }
            }

            PushLava(this.map.player.Rectangle(), this.map.player.velocity.y);

            var chunk = this.map.NearestChunkTo(new Vector2(
                this.map.player.position.x,
                this.lavaline.currentLevel
            ));
            foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
            {
                var w = enemy as WalkingEnemy;
                if (w != null)
                    PushLava(w.hitboxes[0].InWorldSpace(), w.velocity.y);
            }
 
            for (int n = 0; n < this.points.Length; n += 1)
            {
                this.points[n].displacementLastFrame = this.points[n].displacement;
                this.points[n].displacement = this.newValues[n];
            }
        }

        
        public void DrawTo(MeshFilter meshFilter, Mesh mesh)
        {
            this.meshVertices.Clear();
            this.meshColors  .Clear();
            this.meshUVs     .Clear();
            this.meshIndices .Clear();

            var baseY = (this.map.gameCamera?.VisibleRectangle().yMin ?? 0) - 5;
            var baseYRelative = baseY - meshFilter.transform.position.y;

            for (int n = 0; n < this.points.Length - 1; n += 1)
            {
                float x1 = ((n + 0) * Spacing) + this.xMin;
                float x2 = ((n + 1) * Spacing) + this.xMin;

                int firstVertex = this.meshVertices.Count;
                var a = new Vector2(n * Spacing, this.points[n].displacement);
                var b = new Vector2((n + 1) * Spacing, this.points[n + 1].displacement);
                this.meshVertices.Add(a);
                this.meshVertices.Add(b);
                {
                    float movementOffset = this.map.frameNumber * 0.1f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.4f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.4f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                {
                    float movementOffset = this.map.frameNumber * -0.1f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.72f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.72f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                {
                    float movementOffset = this.map.frameNumber * 0.05f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.56f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.56f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                this.meshVertices.Add(new Vector2(a.x, baseYRelative));
                this.meshVertices.Add(new Vector2(b.x, baseYRelative));
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshUVs.Add(new Vector2(0, 1));
                this.meshUVs.Add(new Vector2(1, 1));
                this.meshUVs.Add(new Vector2(0, 1 - 0.0625f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.0625f));
                this.meshUVs.Add(new Vector2(0, 1 - 0.125f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.125f));
                this.meshUVs.Add(new Vector2(0, 1 - 0.1875f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.1875f));
                this.meshUVs.Add(new Vector2(0, 0));
                this.meshUVs.Add(new Vector2(1, 0));

                for (int offset = 0; offset < 8; offset += 2)
                {
                    this.meshIndices.Add(firstVertex + offset + 0);
                    this.meshIndices.Add(firstVertex + offset + 2);
                    this.meshIndices.Add(firstVertex + offset + 1);
                    this.meshIndices.Add(firstVertex + offset + 1);
                    this.meshIndices.Add(firstVertex + offset + 2);
                    this.meshIndices.Add(firstVertex + offset + 3);
                }
            }

            mesh.Clear();
            mesh.SetVertices(this.meshVertices);
            mesh.SetColors(this.meshColors);
            mesh.SetUVs(0, this.meshUVs);
            mesh.SetIndices(this.meshIndices, MeshTopology.Triangles, 0);
            meshFilter.mesh = mesh;
        }

        public void DrawTopRowTo(MeshFilter meshFilter, Mesh mesh)
        {
            this.meshVertices.Clear();
            this.meshColors  .Clear();
            this.meshUVs     .Clear();
            this.meshIndices .Clear();

            for (int n = 0; n < this.points.Length - 1; n += 1)
            {
                float x1 = ((n + 0) * Spacing) + this.xMin;
                float x2 = ((n + 1) * Spacing) + this.xMin;

                int firstVertex = this.meshVertices.Count;
                var a = new Vector2(n * Spacing, 0);
                var b = new Vector2((n + 1) * Spacing, 0);
                this.meshVertices.Add(a);
                this.meshVertices.Add(b);
                this.meshVertices.Add(a.Add(0, -1.5f));
                this.meshVertices.Add(b.Add(0, -1.5f));
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshUVs.Add(new Vector2((a.x + this.xMin) * 70f / 560f, 1));
                this.meshUVs.Add(new Vector2((b.x + this.xMin) * 70f / 560f, 1));
                this.meshUVs.Add(new Vector2((a.x + this.xMin) * 70f / 560f, 35f / 140f));
                this.meshUVs.Add(new Vector2((b.x + this.xMin) * 70f / 560f, 35f / 140f));
                this.meshIndices.Add(firstVertex + 0);
                this.meshIndices.Add(firstVertex + 2);
                this.meshIndices.Add(firstVertex + 1);
                this.meshIndices.Add(firstVertex + 1);
                this.meshIndices.Add(firstVertex + 2);
                this.meshIndices.Add(firstVertex + 3);
            }

            mesh.Clear();
            mesh.SetVertices(this.meshVertices);
            mesh.SetColors(this.meshColors);
            mesh.SetUVs(0, this.meshUVs);
            mesh.SetIndices(this.meshIndices, MeshTopology.Triangles, 0);
            meshFilter.mesh = mesh;
        }
    }
}
