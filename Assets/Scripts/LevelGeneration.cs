using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using MarkerType = Chunk.MarkerType;
using ChunkData = ChunkLibrary.ChunkData;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class LevelGeneration
{
    public static ChunkLibrary chunkLibrary;
    public static DayLibrary dayLibrary;

    public enum RowType
    {
        Normal                   = 0,
        DedicatedCheckpoint      = 1,
        DedicatedBonusLift       = 2,
        DedicatedChest           = 3,
        Start                    = 10,
        BronzeCup                = 11,
        SilverCup                = 12,
        End                      = 13,
        Connector                = 20,
        SideTunnel               = 30,
        HorizontalStepDown       = 40,
        HorizontalBreathingPoint = 41,
        Airlock                  = 50
    }

    public class Row
    {
        public ChunkData chunk;
        public RowType type;
        public bool flipHorizontally;
        public bool fillAreaOfInterestWithCheckpoint;
        public bool fillAreaOfInterestWithBonusLift;
    };

    private static Dictionary<(MarkerType, MarkerType), Row> Connectors =
        new Dictionary<(MarkerType, MarkerType), Row> {
            [ (MarkerType.Left, MarkerType.Right) ] = new Row {
                chunk = GetChunkData("Technical/connector_right_to_left"),
                type = RowType.Connector,
                flipHorizontally = true
            },
            [ (MarkerType.Left, MarkerType.Center) ] = new Row {
                chunk = GetChunkData("Technical/connector_right_to_center"),
                type = RowType.Connector,
                flipHorizontally = true
            },
            [ (MarkerType.Center, MarkerType.Left) ] = new Row {
                chunk = GetChunkData("Technical/connector_center_to_left"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Center, MarkerType.Right) ] = new Row {
                chunk = GetChunkData("Technical/connector_center_to_left"),
                type = RowType.Connector,
                flipHorizontally = true
            },
            [ (MarkerType.Right, MarkerType.Left) ] = new Row {
                chunk = GetChunkData("Technical/connector_right_to_left"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Right, MarkerType.Center) ] = new Row {
                chunk = GetChunkData("Technical/connector_right_to_center"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Left, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_left_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Center, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_center_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Right, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_right_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wide, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_wide_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wall, MarkerType.Left) ] = new Row {
                chunk = GetChunkData("Technical/connector_wall_to_left"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wall, MarkerType.Center) ] = new Row {
                chunk = GetChunkData("Technical/connector_wall_to_center"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wall, MarkerType.Right) ] = new Row {
                chunk = GetChunkData("Technical/connector_wall_to_right"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wall, MarkerType.Wide) ] = new Row {
                chunk = GetChunkData("Technical/connector_wall_to_wide"),
                type = RowType.Connector,
                flipHorizontally = false
            }
        };

    private static Dictionary<(MarkerType, MarkerType), Row> ConnectorsForWindySkies =
        new Dictionary<(MarkerType, MarkerType), Row> {
            [ (MarkerType.Center, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_windyskies_openceiling_center_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wall, MarkerType.Center) ] = new Row {
                chunk = GetChunkData("Technical/connector_windyskies_openceiling_wall_to_center"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Left, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_windyskies_openceiling_left_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Right, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_windyskies_openceiling_right_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
            [ (MarkerType.Wide, MarkerType.Wall) ] = new Row {
                chunk = GetChunkData("Technical/connector_windyskies_openceiling_wide_to_wall"),
                type = RowType.Connector,
                flipHorizontally = false
            },
        };

    private static Row ShorterStart = new Row {
        chunk = GetChunkData("Technical/shorter_start"),
        type = RowType.Start
    };
    private static Row MultiplayerSideRoom = new Row {
        chunk = GetChunkData("Technical/multiplayer_side_room"),
        type = RowType.Start
    };
    private static Row MultiplayerSideRoomWindySkies = new Row {
        chunk = GetChunkData("Technical/multiplayer_side_room_windy_skies"),
        type = RowType.Start
    };
    private static Row SideTunnel = new Row {
        chunk = GetChunkData("Technical/side_tunnel"),
        type = RowType.SideTunnel
    };
    private static Row SideTunnelVeryWide = new Row {
        chunk = GetChunkData("Technical/side_tunnel_very_wide"),
        type = RowType.SideTunnel
    };
    private static Row SideTunnelForWindySkies = new Row {
        chunk = GetChunkData("Technical/side_tunnel_for_windy_skies"),
        type = RowType.SideTunnel
    };
    private static Row HorizontalStepDown = new Row {
        chunk = GetChunkData("Technical/horizontal_step_down"),
        type = RowType.HorizontalStepDown
    };
    private static Row HorizontalStepDownForWindySkies = new Row {
        chunk = GetChunkData("Technical/horizontal_step_down_for_windy_skies"),
        type = RowType.HorizontalStepDown
    };
    private static Row HorizontalBreathingPoint = new Row {
        chunk = GetChunkData("Technical/horizontal_breathing_point"),
        type = RowType.HorizontalBreathingPoint
    };
    private static Row HorizontalBreathingPointForWindySkies = new Row {
        chunk = GetChunkData("Technical/horizontal_breathing_point_for_windy_skies"),
        type = RowType.HorizontalBreathingPoint
    };
    private static Row ForcePlayerLongJumpIntoNextChunk = new Row {
        chunk = GetChunkData("Technical/force_player_long_jump_into_next_chunk")
    };
    private static Row Airlock = new Row {
        chunk = GetChunkData("Technical/airlock"),
        type = RowType.Airlock
    };
    private static Row AirlockHorizontal = new Row {
        chunk = GetChunkData("Technical/airlock_horizontal"),
        type = RowType.Airlock
    };

    private static Row[] MinecartStoppers = new []
    {
        new Row {
            chunk = GetChunkData("Technical/minecart_stopper_1"),
            type = RowType.HorizontalBreathingPoint
        },
        new Row {
            chunk = GetChunkData("Technical/minecart_stopper_2"),
            type = RowType.HorizontalBreathingPoint
        },
        new Row {
            chunk = GetChunkData("Technical/minecart_stopper_3"),
            type = RowType.HorizontalBreathingPoint
        },
        new Row {
            chunk = GetChunkData("Technical/minecart_stopper_4"),
            type = RowType.HorizontalBreathingPoint
        }
    };
    private static Row MinecartStopperWithCheckpoint = new Row {
        chunk = GetChunkData("Technical/minecart_stopper_with_checkpoint"),
        type = RowType.DedicatedCheckpoint
    };

    private static Dictionary<MarkerType, Row> ForceSide =
        new Dictionary<MarkerType, Row> {
            [ MarkerType.Left ] = new Row {
                chunk = GetChunkData("Technical/force_player_left"),
                flipHorizontally = false
            },
            [ MarkerType.Center ] = new Row {
                chunk = GetChunkData("Technical/force_player_center"),
                flipHorizontally = false
            },
            [ MarkerType.Right ] = new Row {
                chunk = GetChunkData("Technical/force_player_left"),
                flipHorizontally = true
            }
        };

    private static Dictionary<MarkerType, Row> TestExits =
        new Dictionary<MarkerType, Row> {
            [ MarkerType.Left ] = new Row {
                chunk = GetChunkData("Technical/test_exit_left"),
                type = RowType.End,
                flipHorizontally = false
            },
            [ MarkerType.Center ] = new Row {
                chunk = GetChunkData("Technical/test_exit_center"),
                type = RowType.End,
                flipHorizontally = false
            },
            [ MarkerType.Right ] = new Row {
                chunk = GetChunkData("Technical/test_exit_left"),
                type = RowType.End,
                flipHorizontally = true
            }
        };

    private static MarkerType Flip(MarkerType type)
    {
        if (type == MarkerType.Left) return MarkerType.Right;
        if (type == MarkerType.Right) return MarkerType.Left;
        return type;
    }

    private static Row FlipRow(Row row) => new Row
    {
        chunk                            = row.chunk,
        type                             = row.type,
        flipHorizontally                 = !row.flipHorizontally,
        fillAreaOfInterestWithBonusLift  = row.fillAreaOfInterestWithBonusLift,
        fillAreaOfInterestWithCheckpoint = row.fillAreaOfInterestWithCheckpoint
    };

    public static void Init()
    {
        chunkLibrary ??= Resources.Load<ChunkLibrary>("Chunk Library");
        dayLibrary ??= Resources.Load<DayLibrary>("Day Library");
    }

    private static (
        (List<ChunkData> generic, List<ChunkData> themed) tutorial,
        (List<ChunkData> generic, List<ChunkData> themed) easy,
        (List<ChunkData> generic, List<ChunkData> themed) medium,
        (List<ChunkData> generic, List<ChunkData> themed) hard
    ) ChunksForTheme(Theme theme)
    {
        #if UNITY_EDITOR
        {
            if (Application.isPlaying == false && chunkLibrary == null)
            {
                chunkLibrary = AssetDatabase.LoadAssetAtPath<ChunkLibrary>(
                    "Assets/Resources/Chunk Library.asset"
                );
            }
        }       
        #endif

        List<ChunkData> GetDataForChunks(Theme? theme, ChunkDifficulty difficulty) =>
            chunkLibrary.GetChunks(theme, difficulty).ToList();

        var genericTutorial = GetDataForChunks(null, ChunkDifficulty.Tutorial);
        var genericEasy     = GetDataForChunks(null, ChunkDifficulty.Easy);
        var genericMedium   = GetDataForChunks(null, ChunkDifficulty.Medium);
        var genericHard     = GetDataForChunks(null, ChunkDifficulty.Hard);

        var themedTutorial = GetDataForChunks(theme, ChunkDifficulty.Tutorial);
        var themedEasy     = GetDataForChunks(theme, ChunkDifficulty.Easy);
        var themedMedium   = GetDataForChunks(theme, ChunkDifficulty.Medium);
        var themedHard     = GetDataForChunks(theme, ChunkDifficulty.Hard);

        return (
            (genericTutorial, themedTutorial),
            (genericEasy, themedEasy),
            (genericMedium, themedMedium),
            (genericHard, themedHard)
        );
    }

    public static Theme ThemeForDate(DateTime date, bool ignoreBakedDays = false)
    {
        if (date == Game.DateOfOpeningTutorial)
            return Theme.OpeningTutorial;

        if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateGravityGalaxyLevel)
            return Theme.GravityGalaxy;
        if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateTombstoneHillLevel)
            return Theme.TombstoneHill;
        if (DebugPanel.selectedMode == DebugPanel.Mode.GenerateMoltenFortressLevel)
            return Theme.MoltenFortress;

        if (dayLibrary == null)
            LevelGeneration.Init();

        if (ignoreBakedDays == false &&
            DebugPanel.selectedMode != DebugPanel.Mode.AlgorithmRegenerate)
        {
            var match = dayLibrary.days.Where(
                d => (d.day, d.month, d.year) == (date.Day, date.Month, date.Year)
            ).FirstOrDefault();
            if (match != null)
                return match.theme;
        }

        TimeSpan span = date.Date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int days = (int)span.TotalDays;
        return dayLibrary.orderOfThemes[days % dayLibrary.orderOfThemes.Length];
    }

    public static (Theme, Row[]) FetchOrGenerate(DateTime date)
    {
        var match = dayLibrary.days.Where(
            d => (d.day, d.month, d.year) == (date.Day, date.Month, date.Year)
        ).FirstOrDefault();

        if (match != null)
        {
            TimeSpan span =
                date.Date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            int days = (int)span.TotalDays;
            var random = new System.Random(days);

            var rowsWithoutConnectors = match.rows.Select(r => new Row
            {
                chunk = GetChunkData(r.filename),
                type = r.type,
                fillAreaOfInterestWithCheckpoint = r.fillAreaOfInterestWithCheckpoint,
                fillAreaOfInterestWithBonusLift = r.fillAreaOfInterestWithBonusLift
            }).ToArray();

            if (SaveData.IsAnyMinigamePurchased() == false)
                rowsWithoutConnectors = PruneBonusLift(rowsWithoutConnectors);

            rowsWithoutConnectors = PostprocessResult(rowsWithoutConnectors);
            rowsWithoutConnectors = LevelPatching.ApplyPatches(
                date, match.theme, rowsWithoutConnectors
            );

            var rows = RowsWithAddedConnectorsAndFlipped(
                random, rowsWithoutConnectors, match.theme, date
            );
            return (match.theme, rows);
        }
        else
        {
            return Generate(date);
        }
    }

    private static Row[] PruneBonusLift(Row[] rows)
    {
        rows = rows.ToArray();

        foreach (var r in rows)
        {
            if (r.type == RowType.DedicatedBonusLift)
            {
                r.type = RowType.DedicatedChest;
                r.chunk = GetChunkData("Technical/chest");
            };
            r.fillAreaOfInterestWithBonusLift = false;
        }

        return rows;
    }

    private static Row[] PostprocessResult(Row[] rows)
    {
        if (rows[1].chunk.filename.Contains("multiplayer") == false)
        {
            var multiplayerSideRoom = new ChunkData {
                filename = "Technical/multiplayer_side_room",
                entryType = MarkerType.Wide,
                exitType = MarkerType.Wide
            };
            var rowList = rows.ToList();
            rowList.Insert(1, new Row { chunk = multiplayerSideRoom });
            rows = rowList.ToArray();
        }

        if (rows[2].chunk.filename.Contains("padding") == false)
        {
            var padding = new ChunkData {
                filename = "Technical/multiplayer_side_room_padding_on_top",
                entryType = MarkerType.Wide,
                exitType = MarkerType.Wide
            };
            var rowList = rows.ToList();
            rowList.Insert(2, new Row { chunk = padding });
            rows = rowList.ToArray();
        }

        if (rows.Any(r => r.chunk.filename == "Technical/coming_soon_billboard"))
        {
            // to hide the 'coming soon' billboard, use this code. the 'coming soon'
            // chunk is always baked into the sequence, so that later when we need it,
            // days we bake at the time i'm writing this will have the billboard
            // suddenly appear, retroactively.

            var index = Array.FindIndex(
                rows, r => r.chunk.filename == "Technical/coming_soon_billboard"
            );
            var rowList = rows.ToList();
            rowList.RemoveAt(index);
            rows = rowList.ToArray();
        }

        return rows;
    }

    public static (Theme, Row[]) GenerateWithoutConnectors(
        DateTime date,
        Theme? forcedTheme = null,
        (System.Random random, int attempts)? reattemptInfo = null
    )
    {
        Theme theme = forcedTheme ?? ThemeForDate(date, true);

        TimeSpan span = date.Date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int days = (int)span.TotalDays;
        var random = reattemptInfo?.random ?? new System.Random(days);

        var (tutorialChunks, easyChunks, mediumChunks, hardChunks) =
            ChunksForTheme(theme);
        var unusedCheckpointChunks = chunkLibrary.checkpoint.ToList();
        var unusedChestChunks = chunkLibrary.chest.ToList();
        var unusedBlenderChunks = chunkLibrary.blenderVendingMachine.ToList();

        var start = new ChunkData {
            filename = "start", exitType = MarkerType.Wide
        };
        var startWindySkies = new ChunkData {
            filename = "start_windy_skies", exitType = MarkerType.Wide
        };
        var multiplayerSideRoom = new ChunkData {
            filename = "Technical/multiplayer_side_room",
            entryType = MarkerType.Center,
            exitType = MarkerType.Center
        };
        var multiplayerWindySkiesSideRoom = new ChunkData {
            filename = "Technical/multiplayer_side_room_windy_skies",
            entryType = MarkerType.Center,
            exitType = MarkerType.Center
        };
        var bronze = new ChunkData {
            filename = "cup_bronze",
            entryType = MarkerType.Center,
            exitType = MarkerType.Center
        };
        var silver = new ChunkData {
            filename = "cup_silver",
            entryType = MarkerType.Center,
            exitType = MarkerType.Center
        };
        var gold = new ChunkData {
            filename = "ending", exitType = MarkerType.Wide
        };
        var goldWindySkies = new ChunkData {
            filename = "ending_windy_skies", exitType = MarkerType.Wide
        };
        var horizontalCheckpoint = new ChunkData {
            filename = "Technical/horizontal_checkpoint",
            entryType = MarkerType.Wall,
            exitType = MarkerType.Wall,
            isHorizontal = true
        };
        var horizontalCheckpointForWindySkies = new ChunkData {
            filename = "Technical/horizontal_checkpoint_for_windy_skies",
            entryType = MarkerType.Wall,
            exitType = MarkerType.Wall,
            isHorizontal = true
        };
        var bonusLift = new ChunkData {
            filename = "Technical/bonus_lift",
            entryType = MarkerType.Left,
            exitType = MarkerType.Left
        };
        var chest = new ChunkData {
            filename = "Technical/chest",
            entryType = MarkerType.Left,
            exitType = MarkerType.Left
        };
        var tomorrowOnSuperLeapDay = new ChunkData {
            filename = "Technical/tomorrow_on_super_leap_day",
            entryType = MarkerType.Wide,
            exitType = MarkerType.Center
        };
        var comingSoonBillboard = new ChunkData {
            filename = "Technical/coming_soon_billboard",
            entryType = MarkerType.Wide,
            exitType = MarkerType.Wide
        };

        bool[] SequenceOfHorizontalForTreasureMines(int numberOfChunks)
        {
            var result = new bool[numberOfChunks];

            for (int n = 0; n < numberOfChunks; n += 1)
            {
                result[n] = (random.Next() % 100 < 85);
            }

            return result;
        }

        bool[] SequenceOfHorizontal(int numberOfChunks)
        {
            if (theme == Theme.TreasureMines)
                return SequenceOfHorizontalForTreasureMines(numberOfChunks);

            var result = new bool[numberOfChunks];
            var value = false;
            var horizontalLength = 0;

            for (int n = 0; n < numberOfChunks; n += 1)
            {
                if (random.Next() % 6 == 0)
                    value = !value;

                if (value == true)
                    horizontalLength += 1;
                else
                    horizontalLength = 0;

                if (horizontalLength > 4)
                    value = false;

                result[n] = value;
            }

            return result;
        }

        ChunkData RandomChunkTakenFromList(List<ChunkData> candidates, bool horizontal)
        {
            if (candidates.Count == 0) return null;

            for (int n = 0; n < 10; n += 1)
            {
                var index = random.Next() % candidates.Count;
                var chunk = candidates[index];
                if (chunk.isHorizontal == horizontal)
                {
                    candidates.Remove(chunk);
                    return chunk;
                }
            }

            {
                var index = random.Next() % candidates.Count;
                var chunk = candidates[index];
                candidates.Remove(chunk);
                return chunk;
            }
        }

        ChunkData RandomChunk(
            (List<ChunkData> generic, List<ChunkData> themed) candidates,
            bool horizontal
        )
        {
            if (candidates.generic.Count == 0 && candidates.themed.Count == 0)
                Debug.LogError("Out of choices for level generation!");

            if (candidates.themed.Count == 0)
                return RandomChunkTakenFromList(candidates.generic, horizontal);
            else if (candidates.generic.Count == 0)
                return RandomChunkTakenFromList(candidates.themed, horizontal);
            else if (random.Next() % 100 < 30)
                return RandomChunkTakenFromList(candidates.generic, horizontal);
            else
                return RandomChunkTakenFromList(candidates.themed, horizontal);
        }

        ChunkData[] MaybeTwoRandomChunks(
            (List<ChunkData> generic, List<ChunkData> themed) candidatesFor1,
            (List<ChunkData> generic, List<ChunkData> themed) candidatesFor2,
            bool horizontal1,
            bool horizontal2
        )
        {
            if (theme == Theme.ConflictCanyon ||
                theme == Theme.GravityGalaxy ||
                theme == Theme.SunkenIsland)
            {
                // themed chunks in these themes are considered a bit harder, so the
                // quirk here is if we pick one, then we are going to have just
                // one here, even though there would normally be two in this spot
                // according to the template.

                if (random.Next() % 2 == 0)
                {
                    // two generic chunks
                    var first = RandomChunkTakenFromList(
                        candidatesFor1.generic, horizontal1
                    );
                    var second = RandomChunkTakenFromList(
                        candidatesFor2.generic, horizontal2
                    );
                    if (first != null && second != null)
                        return new [] { first, second };
                }
                else
                {
                    // one themed chunk
                    var chunk = RandomChunkTakenFromList(
                        candidatesFor1.themed, horizontal1
                    );
                    if (chunk != null)
                        return new [] { chunk };
                }
            }

            // its either a different theme, or we failed to select because there
            // weren't enough chunks in the candidates.
            {
                // two random chunks of any kind
                var first = RandomChunk(candidatesFor1, horizontal1);
                var second = RandomChunk(candidatesFor2, horizontal2);
                return new [] { first, second };
            }
        }

        var selection = new List<Row>();
        void AddChunks(ChunkData[] chunks)
        {
            // the 'chunks' array contains nulls to represent where checkpoints need to
            // be added (either as their own dedicated chunk or in areas of interest).

            for (int n = 0; n < chunks.Length; n += 1)
            {
                if (chunks[n] != null)
                {
                    selection.Add(new Row { chunk = chunks[n] });
                    continue;
                }

                if (selection.Last().chunk.containsAreaOfInterest == true)
                {
                    selection.Last().fillAreaOfInterestWithCheckpoint = true;
                }
                else if (
                    n > 0 &&
                    n < chunks.Length - 1 &&
                    chunks[n - 1].isHorizontal == true &&
                    chunks[n + 1].isHorizontal == true
                )
                {
                    if (theme == Theme.TreasureMines)
                    {
                        selection.Add(MinecartStopperWithCheckpoint);
                    }
                    else if (theme == Theme.WindySkies)
                    {
                        selection.Add(new Row {
                            chunk = horizontalCheckpointForWindySkies,
                            type = RowType.DedicatedCheckpoint
                        });
                    }
                    else
                    {
                        selection.Add(new Row {
                            chunk = horizontalCheckpoint,
                            type = RowType.DedicatedCheckpoint
                        });
                    }
                }
                else
                {
                    int checkpointIndex = random.Next() % unusedCheckpointChunks.Count;
                    var checkpointChunk = unusedCheckpointChunks[checkpointIndex];
                    unusedCheckpointChunks.RemoveAt(checkpointIndex);
                    selection.Add(new Row {
                        chunk = checkpointChunk,
                        type = RowType.DedicatedCheckpoint
                    });
                }
            }
        }

        selection.Add(new Row { chunk = (theme == Theme.WindySkies ? startWindySkies : start), type = RowType.Start });
        selection.Add(new Row { chunk = (theme == Theme.WindySkies ? multiplayerWindySkiesSideRoom : multiplayerSideRoom), type = RowType.Normal });
        {
            bool[] horz1 = SequenceOfHorizontal(9);

            var chunks = new List<ChunkData>();
            chunks.Add(RandomChunk(tutorialChunks, horz1[0]));
            chunks.Add(null /* 15 */);
            chunks.Add(RandomChunk(tutorialChunks, horz1[1]));
            chunks.Add(tomorrowOnSuperLeapDay);
            chunks.Add(null /* 14 */);
            chunks.Add(RandomChunk(easyChunks,     horz1[2]));
            chunks.Add(null /* 13 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(easyChunks, tutorialChunks, horz1[3], horz1[4])
            );
            chunks.Add(null /* 12 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(easyChunks, easyChunks, horz1[5], horz1[6])
            );
            chunks.Add(null /* 11 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(easyChunks, easyChunks, horz1[7], horz1[8])
            );

            AddChunks(chunks.ToArray());
        }
        selection.Add(new Row { chunk = bronze, type = RowType.BronzeCup }); // 10
        {
            bool[] horz2 = SequenceOfHorizontal(7);

            var chunks = new List<ChunkData>();
            chunks.Add(RandomChunk(mediumChunks,    horz2[0]));
            chunks.Add(null /* 9 */);
            chunks.Add(comingSoonBillboard); /* don't remove this -- use PostprocessResult instead */
            chunks.Add(RandomChunk(mediumChunks,    horz2[1]));
            chunks.Add(null /* 8 */);
            chunks.Add(RandomChunk(mediumChunks,    horz2[2]));
            chunks.Add(null /* 7 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(mediumChunks, easyChunks, horz2[3], horz2[4])
            );
            chunks.Add(null /* 6 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(mediumChunks, easyChunks, horz2[5], horz2[6])
            );

            AddChunks(chunks.ToArray());
        }
        selection.Add(new Row { chunk = silver, type = RowType.SilverCup }); // 5
        {
            bool[] horz3 = SequenceOfHorizontal(8);

            var chunks = new List<ChunkData>();
            chunks.AddRange(
                MaybeTwoRandomChunks(mediumChunks, mediumChunks, horz3[0], horz3[1])
            );
            chunks.Add(null /* 4 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(mediumChunks, mediumChunks, horz3[2], horz3[3])
            );
            chunks.Add(null /* 3 */);
            chunks.AddRange(
                MaybeTwoRandomChunks(mediumChunks, mediumChunks, horz3[4], horz3[5])
            );
            chunks.Add(null /* 2 */);
            chunks.Add(RandomChunk(hardChunks, horz3[6]));
            chunks.Add(null /* 1 */);
            chunks.Add(RandomChunk(hardChunks, horz3[7]));

            AddChunks(chunks.ToArray());
        }
        selection.Add(new Row { chunk = (theme == Theme.WindySkies ? goldWindySkies : gold), type = RowType.End }); // *

        int IndexToRandomlyInsertChunk(int minimum, int maximum)
        {
            int index = random.Next(minimum, maximum + 1);

            bool IsSpecialChunk(Row r) =>
                r.chunk == bronze || r.chunk == silver || r.chunk == gold || r.chunk == goldWindySkies ||
                r.chunk == bonusLift || r.chunk == chest ||
                r.fillAreaOfInterestWithBonusLift == true;

            for (int n = 0; n < 3; n += 1)
            {
                var before = selection[index];
                var after = selection[index + 1];

                // try to keep added chunks away from the trophies and each other
                if (IsSpecialChunk(before)) continue;
                if (IsSpecialChunk(after)) continue;

                // and for now, keep them out of horizontal spaces as well
                if (before.chunk.isHorizontal == true && after.chunk.isHorizontal == true)
                    continue;

                return index;
            }

            index = random.Next(minimum, maximum + 1);
            return index;
        }

        {
            int indexBronze = selection.FindIndex(r => r.chunk == bronze);
            int indexSilver = selection.FindIndex(r => r.chunk == silver);
            int indexGold   = selection.FindIndex(r => r.chunk == (theme == Theme.WindySkies ? goldWindySkies : gold));
            int indexChest1 = IndexToRandomlyInsertChunk(2, indexBronze);
            int indexChest2 = IndexToRandomlyInsertChunk(indexBronze + 1, indexSilver);
            int indexChest3 = IndexToRandomlyInsertChunk(indexSilver + 1, indexGold - 3);
            var chestChunk1 = RandomChunkTakenFromList(unusedChestChunks, false);
            var chestChunk2 = RandomChunkTakenFromList(unusedChestChunks, false);
            var chestChunk3 = RandomChunkTakenFromList(unusedChestChunks, false);
            selection.Insert(
                indexChest3,
                new Row { chunk = chestChunk3, type = RowType.DedicatedChest }
            );
            selection.Insert(
                indexChest2,
                new Row { chunk = chestChunk2, type = RowType.DedicatedChest }
            );
            selection.Insert(
                indexChest1,
                new Row { chunk = chestChunk1, type = RowType.DedicatedChest }
            );
        }

        {
            int indexBronze = selection.FindIndex(r => r.chunk == bronze);
            int indexSilver = selection.FindIndex(r => r.chunk == silver);
            int indexGold   = selection.FindIndex(r => r.chunk == (theme == Theme.WindySkies ? goldWindySkies : gold));
            int halfwayToBronze = indexBronze / 2;
            int halfwayToSilver = indexSilver / 2;
            int indexBlender1 = IndexToRandomlyInsertChunk(halfwayToBronze, indexBronze);
            int indexBlender2 = IndexToRandomlyInsertChunk(halfwayToSilver, indexSilver);
            int indexBlender3 = IndexToRandomlyInsertChunk(indexSilver + 1, indexGold - 3);
            var blenderChunk1 = RandomChunkTakenFromList(unusedBlenderChunks, false);
            var blenderChunk2 = RandomChunkTakenFromList(unusedBlenderChunks, false);
            var blenderChunk3 = RandomChunkTakenFromList(unusedBlenderChunks, false);
            selection.Insert(
                indexBlender3,
                new Row { chunk = blenderChunk1, type = RowType.Normal }
            );
            selection.Insert(
                indexBlender2,
                new Row { chunk = blenderChunk2, type = RowType.Normal }
            );
            selection.Insert(
                indexBlender1,
                new Row { chunk = blenderChunk3, type = RowType.Normal }
            );
        }

        {
            void AddBonusLift(int index)
            {
                if (selection[index].chunk.containsAreaOfInterest == true &&
                    selection[index].fillAreaOfInterestWithCheckpoint == false)
                {
                    selection[index].fillAreaOfInterestWithBonusLift = true;
                }
                else
                {
                    selection.Insert(index + 1, new Row
                    {
                        chunk = bonusLift,
                        type = RowType.DedicatedBonusLift
                    });
                }
            }

            int count = selection.Count;
            int index1 = IndexToRandomlyInsertChunk(7, (count * 4) / 10);
            int index2 = IndexToRandomlyInsertChunk((count * 7) / 10, count - 4);
            AddBonusLift(index2);
            AddBonusLift(index1);
        }

        bool ShouldRejectAndReattempt()
        {
            if (theme == Theme.WindySkies)
            {
                // windy skies levels should have a parachute level
                // before the bronze trophy
                var indexOfBronze =
                    selection.FindIndex(s => s.chunk.filename == bronze.filename);
                var chunksBeforeBronze = selection.Take(indexOfBronze);
                var anyParachuteLevelsBeforeBronze =
                    chunksBeforeBronze.Any(s => s.chunk.filename.Contains("parachute"));
                if (anyParachuteLevelsBeforeBronze == false)
                {
                    return true;
                }
            }

            if (theme == Theme.GravityGalaxy)
            {
                // gravity galaxy levels need to start vertical for a little bit,
                // so the outside boundary doesn't glitch around the multiplayer room
                if (selection[0].chunk.isHorizontal == true ||
                    selection[2].chunk.isHorizontal == true)
                {
                    return true;
                }
            }

            return false;
        }

        if ((reattemptInfo?.attempts ?? 0) < 10 && ShouldRejectAndReattempt())
        {
            return GenerateWithoutConnectors(
                date, forcedTheme, (random, (reattemptInfo?.attempts ?? 0) + 1)
            );
        }

        return (theme, selection.ToArray());
    }

    public static (Theme, Row[]) Generate(DateTime date)
    {
        if (date == Game.DateOfOpeningTutorial)
            return (Theme.OpeningTutorial, OpeningTutorial());

        var (theme, rowsWithoutConnectors) = GenerateWithoutConnectors(date);

        if (SaveData.IsAnyMinigamePurchased() == false)
            rowsWithoutConnectors = PruneBonusLift(rowsWithoutConnectors);

        rowsWithoutConnectors = PostprocessResult(rowsWithoutConnectors);
        rowsWithoutConnectors = LevelPatching.ApplyPatches(
            date, theme, rowsWithoutConnectors
        );

        TimeSpan span = date.Date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int days = (int)span.TotalDays;
        var random = new System.Random(days);

        var rows = RowsWithAddedConnectorsAndFlipped(
            random, rowsWithoutConnectors, theme, date
        );
        return (theme, rows);
    }

    public static Row[] GenerateWithForcedTheme(DateTime date, Theme theme)
    {
        var (_, rowsWithoutConnectors) = GenerateWithoutConnectors(date, theme);

        if (SaveData.IsAnyMinigamePurchased() == false)
            rowsWithoutConnectors = PruneBonusLift(rowsWithoutConnectors);

        rowsWithoutConnectors = PostprocessResult(rowsWithoutConnectors);
        rowsWithoutConnectors = LevelPatching.ApplyPatches(
            date, theme, rowsWithoutConnectors
        );

        TimeSpan span = date.Date - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        int days = (int)span.TotalDays;
        var random = new System.Random(days);

        var rows = RowsWithAddedConnectorsAndFlipped(
            random, rowsWithoutConnectors, theme, date
        );
        return rows;
    }

    private static Row[] OpeningTutorial()
    {
        var chunk1 = new ChunkData { filename = "opening_tutorial_part_1" };
        var chunk2 = new ChunkData { filename = "opening_tutorial_part_2" };
        var chunk3 = new ChunkData { filename = "opening_tutorial_part_3" };
        var chunk4 = new ChunkData { filename = "opening_tutorial_part_4" };
        var chunk5 = new ChunkData { filename = "opening_tutorial_part_5" };
        var chunk6 = new ChunkData { filename = "opening_tutorial_part_6" };

        return new Row[]
        {
            new Row { chunk = chunk1 },
            new Row { chunk = chunk2 },
            new Row { chunk = chunk3, type = RowType.DedicatedCheckpoint },
            new Row { chunk = chunk4 },
            new Row { chunk = chunk5 },
            new Row { chunk = chunk6 }
        };
    }

    private static Row[] RowsWithAddedConnectorsAndFlipped(
        System.Random random,
        Row[] rows,
        Theme theme,
        DateTime date
    )
    {
        var Left  = MarkerType.Left;
        var Right = MarkerType.Right;

        Row GetConnector(MarkerType from, MarkerType to)
        {
            var key = (from, to);
            if (theme == Theme.WindySkies && ConnectorsForWindySkies.TryGetValue(key, out var value))
                return value;
            else if (Connectors.TryGetValue(key, out var value2))
                return value2;
            else
                return null;
        }

        Row GetSideTunnel() =>
            (theme == Theme.WindySkies) ? SideTunnelForWindySkies : SideTunnel;

        Row GetHorizontalStepDown() =>
            (theme == Theme.WindySkies) ? HorizontalStepDownForWindySkies : HorizontalStepDown;

        Row GetMinecartStopper() =>
            MinecartStoppers[random.Next() % MinecartStoppers.Length];

        Row GetAirlock(Row aroundRow)
        {
            if (aroundRow.chunk.isHorizontal == false)
                return Airlock;

            return
                aroundRow.flipHorizontally == true ?
                FlipRow(AirlockHorizontal) :
                AirlockHorizontal;
        }

        var result = new List<Row>();
        result.Add(rows[0]);

        var last = rows[0];
        var lastExitType = MarkerType.Wide;

        var lastHorizontalChunkIndex = -100;
        var lastHorizontalChunkWasFlipped = false;

        for (int n = 1; n < rows.Length; n += 1)
        {
            var next = rows[n];
            var flip = random.Next() % 2 == 1;

            if (next.chunk.isHorizontal == true && theme == Theme.TreasureMines)
                flip = false;
            else if (next.chunk.filename == MultiplayerSideRoom.chunk.filename)
                flip = false;
            else if (next.chunk.filename == MultiplayerSideRoomWindySkies.chunk.filename)
                flip = false;
            else if (next.chunk.isHorizontal == true && n <= lastHorizontalChunkIndex + 3)
                flip = lastHorizontalChunkWasFlipped;
            else if (lastExitType == Right && next.chunk.entryType == Left)
                flip = true;
            else if (lastExitType == Left && next.chunk.entryType == Right)
                flip = true;
            else if (lastExitType == Left && next.chunk.entryType == Left)
                flip = false;
            else if (lastExitType == Right && next.chunk.entryType == Right)
                flip = false;

            if (theme == Theme.GravityGalaxy && last.chunk.isZeroGravity == true)
            {
                result.Add(GetAirlock(last));

                if (last.chunk.isHorizontal == true && next.chunk.isHorizontal == false)
                {
                    var stepDown = GetHorizontalStepDown();
                    if (last.flipHorizontally)
                        stepDown = FlipRow(stepDown);
                    result.Add(stepDown);
                }
            }

            var connectorEntry = lastExitType;
            var connectorExit = flip ? Flip(next.chunk.entryType) : next.chunk.entryType;

            if (last.chunk.isHorizontal == false && next.chunk.isHorizontal == true)
            {
                // starting a horizontal section
                if (flip == true)
                {
                    connectorEntry = Flip(connectorEntry);
                    connectorExit = Flip(connectorExit);
                    result.Add(FlipRow(GetConnector(connectorEntry, connectorExit)));
                    result.Add(FlipRow(GetSideTunnel()));
                    result.Add(FlipRow(GetHorizontalStepDown()));
                }
                else if (n <= 3)
                {
                    // turning right too early risks having the horizontal chunk drop
                    // lower and intersecting with the multiplayer room chunk. the
                    // longer side tunnel gets the chunk far enough away to avoid this.
                    result.Add(GetConnector(connectorEntry, connectorExit));
                    result.Add(SideTunnelVeryWide);
                    result.Add(GetHorizontalStepDown());
                }
                else
                {
                    result.Add(GetConnector(connectorEntry, connectorExit));
                    result.Add(GetSideTunnel());
                    result.Add(GetHorizontalStepDown());
                }
            }
            else if (last.chunk.isHorizontal == true && next.chunk.isHorizontal == false)
            {
                // ending a horizontal section
                if (last.flipHorizontally == true)
                {
                    connectorEntry = Flip(connectorEntry);
                    connectorExit = Flip(connectorExit);
                    result.Add(FlipRow(GetSideTunnel()));
                    result.Add(FlipRow(GetConnector(connectorEntry, connectorExit)));
                }
                else
                {
                    result.Add(GetSideTunnel());
                    result.Add(GetConnector(connectorEntry, connectorExit));
                }
            }
            else if (last.chunk.isHorizontal == true && next.chunk.isHorizontal == true)
            {
                // continuing a horizontal section
                if (last.type != RowType.DedicatedCheckpoint &&
                    next.type != RowType.DedicatedCheckpoint)
                {
                    if (theme == Theme.TreasureMines && flip == true)
                        result.Add(FlipRow(GetMinecartStopper()));
                    else if (theme == Theme.TreasureMines && flip == false)
                        result.Add(GetMinecartStopper());
                    else if (theme == Theme.WindySkies && flip == true)
                        result.Add(FlipRow(HorizontalBreathingPointForWindySkies));
                    else if (theme == Theme.WindySkies)
                        result.Add(HorizontalBreathingPointForWindySkies);
                    else if (flip == true)
                        result.Add(FlipRow(HorizontalBreathingPoint));
                    else
                        result.Add(HorizontalBreathingPoint);
                }
            }
            else
            {
                var connector = GetConnector(connectorEntry, connectorExit);
                if (connector != null)
                    result.Add(connector);
            }

            var nextWithCorrectFlip = new Row {
                chunk = next.chunk,
                type = next.type,
                flipHorizontally = flip,
                fillAreaOfInterestWithBonusLift = next.fillAreaOfInterestWithBonusLift,
                fillAreaOfInterestWithCheckpoint = next.fillAreaOfInterestWithCheckpoint
            };

            if (theme == Theme.GravityGalaxy && next.chunk.isZeroGravity == true)
            {
                if (last.chunk.exitType is MarkerType.Left or MarkerType.Right &&
                    next.chunk.entryType == MarkerType.Wide)
                {
                    // this would normally result in a unusually positioned airlock
                    // for the outside chunk. it's technically possible to test this
                    // will work for the outside chunk, but it's not worth it.
                    // force the airlock to be central.
                    result.Add(Connectors[(last.chunk.exitType, MarkerType.Center)]);
                }

                result.Add(GetAirlock(nextWithCorrectFlip));
            }

            result.Add(nextWithCorrectFlip);
            lastExitType = flip ? Flip(next.chunk.exitType) : next.chunk.exitType;
            last = nextWithCorrectFlip;

            if (next.chunk.isHorizontal == true)
            {
                lastHorizontalChunkIndex = n;
                lastHorizontalChunkWasFlipped = flip;
            }
        }

        return result.ToArray();
    }

    public static Row[] GenerateTestArena(NitromeEditor.Level testLevel, Theme theme)
    {
        var data = GetChunkData("test level", testLevel);

        var Left   = MarkerType.Left;
        var Center = MarkerType.Center;
        var Right  = MarkerType.Right;
        var Wall   = MarkerType.Wall;
        var Wide   = MarkerType.Wide;

        var testChunkData = new ChunkData {
            filename = "level from editor",
            level = testLevel
        };
        ChunkLibrary.FillChunkData(testChunkData, testLevel);

        var testLevelRow = new Row {
            chunk = testChunkData
        };
        var testLevelFlippedRow = new Row {
            chunk = testChunkData,
            flipHorizontally = true
        };
        var vertical15RowsEmpty = new Row {
            chunk = GetChunkData("Technical/vertical_15rows_empty")
        };
        var verticalSeparator = new Row {
            chunk = GetChunkData("Technical/vertical_separator")
        };
        var airlock = new Row {
            chunk = GetChunkData("Technical/airlock")
        };
        var airlockHorizontal = new Row {
            chunk = GetChunkData("Technical/airlock_horizontal")
        };
        var airlockHorizontalFlipped = new Row {
            chunk = GetChunkData("Technical/airlock_horizontal"),
            flipHorizontally = true
        };

        var list = new List<Row>();

        if (data.isZeroGravity == true && data.entryType == Wall && data.exitType == Wall)
        {
            /* zero-gravity; horizontal */

            list.Add(ShorterStart);
            list.Add(Connectors[(Center, Wall)]);
            list.Add(SideTunnel);
            list.Add(airlockHorizontal);
            list.Add(testLevelRow);
            list.Add(airlockHorizontal);
            list.Add(SideTunnel);
            list.Add(Connectors[(Wall, Center)]);
            list.Add(verticalSeparator);
            list.Add(FlipRow(Connectors[(Center, Wall)]));
            list.Add(FlipRow(SideTunnel));
            list.Add(airlockHorizontalFlipped);
            list.Add(testLevelFlippedRow);
            list.Add(airlockHorizontalFlipped);
            list.Add(FlipRow(SideTunnel));
            list.Add(FlipRow(Connectors[(Wall, Center)]));
            list.Add(TestExits[Center]);
        }
        else if (data.isZeroGravity == true)
        {
            /* zero-gravity; vertical */

            list.Add(ShorterStart);
            list.Add(ForcePlayerLongJumpIntoNextChunk);
            list.Add(airlock);
            list.Add(testLevelRow);
            list.Add(airlock);
            list.Add(vertical15RowsEmpty);
            list.Add(airlock);
            list.Add(testLevelFlippedRow);
            list.Add(airlock);
            list.Add(TestExits[Center]);
        }
        else if (data.entryType == Wide && data.exitType == Wide)
        {
            /* vertical, wide exit both top and bottom */

            list.Add(ShorterStart);
            list.Add(ForcePlayerLongJumpIntoNextChunk);
            list.Add(testLevelRow);
            list.Add(testLevelRow);
            list.Add(ForceSide[Left]);
            list.Add(testLevelRow);
            list.Add(Connectors[(Right, Left)]);
            list.Add(testLevelFlippedRow);
            list.Add(Connectors[(Right, Center)]);
            list.Add(testLevelRow);
            list.Add(TestExits[Center]);
        }
        else if (data.entryType == Wide)
        {
            /* vertical, wide at bottom, left/center/right at top */

            list.Add(ShorterStart);
            list.Add(ForcePlayerLongJumpIntoNextChunk);
            list.Add(testLevelRow);
            if (data.exitType != Center)
                list.Add(Connectors[(data.exitType, Center)]);
            list.Add(testLevelRow);
            if (data.exitType != Left)
                list.Add(Connectors[(data.exitType, Left)]);
            list.Add(testLevelFlippedRow);
            if (Flip(data.exitType) != Left)
                list.Add(Connectors[(Flip(data.exitType), Left)]);
            list.Add(testLevelRow);
            list.Add(TestExits[data.exitType]);
        }
        else if (data.exitType == Wide)
        {
            /* vertical, left/center/right at bottom, wide at top */

            list.Add(ShorterStart);
            if (data.entryType == Wall)
                list.Add(Connectors[(Center, Wall)]);
            else
                list.Add(ForceSide[data.entryType]);  
            list.Add(testLevelRow);
            if (Left != Flip(data.entryType))
                list.Add(Connectors[(Left, Flip(data.entryType))]);
            list.Add(testLevelFlippedRow);
            if (Right != data.entryType)
                list.Add(Connectors[(Right, data.entryType)]);
            list.Add(testLevelRow);
            list.Add(TestExits[Center]);
        }
        else if (data.entryType == Wall && data.entryType == Wall)
        {
            /* horizontal */

            Row GetConnector(MarkerType from, MarkerType to)
            {
                var key = (from, to);
                if (theme == Theme.WindySkies && ConnectorsForWindySkies.TryGetValue(key, out var value))
                    return value;
                else if (Connectors.TryGetValue(key, out var value2))
                    return value2;
                else
                    return null;
            }
            Row GetSideTunnel() =>
                (theme == Theme.WindySkies) ? SideTunnelForWindySkies : SideTunnel;

            list.Add(ShorterStart);
            list.Add(GetConnector(Center, Wall));
            list.Add(GetSideTunnel());
            list.Add(testLevelRow);
            list.Add(GetSideTunnel());
            list.Add(GetConnector(Wall, Center));
            list.Add(verticalSeparator);
            list.Add(FlipRow(GetConnector(Center, Wall)));
            list.Add(FlipRow(GetSideTunnel()));
            list.Add(testLevelFlippedRow);
            list.Add(FlipRow(GetSideTunnel()));
            list.Add(FlipRow(GetConnector(Wall, Center)));
            list.Add(TestExits[Center]);
        }
        else
        {
            /* vertical, left/right/middle at bottom, left/right/middle at top */

            list.Add(ShorterStart);
            list.Add(ForceSide[data.entryType]);
            list.Add(testLevelRow);
            if (data.exitType != Flip(data.entryType))
                list.Add(Connectors[(data.exitType, Flip(data.entryType))]);
            list.Add(testLevelFlippedRow);
            list.Add(TestExits[Flip(data.exitType)]);
        }

        return list.ToArray();
    }

    public static Row[] GenerateTestArena(NitromeEditor.Level[] testLevels, Theme theme)
    {
        var random = new System.Random();
        var start = new ChunkData {
            filename = "start", exitType = MarkerType.Wide
        };

        var testLevelChunkDatas = testLevels.Select(
            (level, n) => GetChunkData($"level #{n + 1} from editor", level)
        ).ToArray();

        if (DebugPanel.shuffleEnabled == true)
        {
            Util.Shuffle(random, testLevelChunkDatas);
        }

        var unusedCheckpointChunks = chunkLibrary.checkpoint.ToList();
        var selection = new List<Row>();
        selection.Add(new Row { chunk = start });

        for (int n = 0; n < testLevelChunkDatas.Length; n += 1)
        {
            var chunk = testLevelChunkDatas[n];
            if (n % 2 == 0)
            {
                selection.Add(new Row { chunk = chunk });
            }
            else if (chunk.containsAreaOfInterest == true)
            {
                selection.Add(new Row {
                    chunk = chunk,
                    fillAreaOfInterestWithCheckpoint = true
                });
            }
            else if (unusedCheckpointChunks.Count > 0)
            {
                selection.Add(new Row { chunk = chunk });

                int index = random.Next() % unusedCheckpointChunks.Count;
                var checkpoint = unusedCheckpointChunks[index];
                unusedCheckpointChunks.RemoveAt(index);
                selection.Add(new Row { chunk = checkpoint });
            }
        }
        selection.Add(TestExits[MarkerType.Center]);

        return RowsWithAddedConnectorsAndFlipped(
            random, selection.ToArray(), theme, DateTime.Now.Date
        );
    }

    public static Row[] GenerateTestArena(ChunkLibrary.ReviewPack reviewPack, Theme theme)
    {
        var random = new System.Random();
        var start = new ChunkData {
            filename = "start", exitType = MarkerType.Wide
        };

        // this should match the logic of MaybeTwoRandomChunks, above
        var checkpointEveryChunk =
            theme is Theme.ConflictCanyon or Theme.GravityGalaxy or Theme.SunkenIsland;

        var selection = new List<Row>();
        selection.Add(new Row { chunk = start });

        var unusedCheckpointChunks = chunkLibrary.checkpoint.ToList();
        for (int n = 0; n < reviewPack.chunks.Length; n += 1)
        {
            var chunk = GetChunkData(reviewPack.chunks[n].filename);
            if (n % 2 == 0 && checkpointEveryChunk == false)
            {
                selection.Add(new Row { chunk = chunk });
            }
            else if (chunk.containsAreaOfInterest == true)
            {
                selection.Add(new Row {
                    chunk = chunk,
                    fillAreaOfInterestWithCheckpoint = true
                });
            }
            else if (unusedCheckpointChunks.Count > 0)
            {
                selection.Add(new Row { chunk = chunk });

                int index = random.Next() % unusedCheckpointChunks.Count;
                var checkpoint = unusedCheckpointChunks[index];
                unusedCheckpointChunks.RemoveAt(index);
                selection.Add(new Row { chunk = checkpoint });
            }
        }
        selection.Add(TestExits[MarkerType.Center]);

        return RowsWithAddedConnectorsAndFlipped(
            random, selection.ToArray(), theme, DateTime.Now.Date
        );
    }

    public static Row[] GenerateTestArenaFromLevelTestingServer(Theme theme)
    {
        if (LevelTestingServer.receivedLevel != null)
        {
            return GenerateTestArena(LevelTestingServer.receivedLevel, theme);
        }
        else if (LevelTestingServer.receivedLevels != null)
        {
            var levels = LevelTestingServer.receivedLevels.ToArray();
            if (DebugPanel.shuffleEnabled == true)
                Util.Shuffle(new System.Random(), levels);

            return GenerateTestArena(LevelTestingServer.receivedLevels, theme);
        }
        else
        {
            return new [] { new Row { chunk = GetChunkData("Technical/nothing") } };
        }
    }

    private static ChunkData GetChunkData(string filename)
    {
        var resource = Resources.Load<TextAsset>("Levels/" + filename);
        var bytes = resource.bytes;
        var level = NitromeEditor.Level.UnserializeFromBytes(bytes);
        return GetChunkData(filename, level);
    }

    private static ChunkData GetChunkData(string filename, NitromeEditor.Level level)
    {
        var result = new ChunkData
        {
            filename = filename,
            level = level
        };
        ChunkLibrary.FillChunkData(result, level);
        return result;
    }
}
