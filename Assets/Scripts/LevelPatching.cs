using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public class LevelPatching
{
    /*
        Level-patches should be a json file in this format:

        {
            "patches": [
                < list of patches >
            ]
        }

        where a patch is the following:

        {
            [optional] "date": [ 2024, 12, 31 ],
            "forbid": < md5 of chunk name >,
            "seed": < integer used to generate replacement chunk >
        }

        If the date is omitted then the chunk will just be banned across
        the entire game. "Chunk name" is relative to Levels, using forward-slashes,
        which means it should exactly match the chunk's name in the Level library.
    */

    public class Patch
    {
        public DateTime? date;
        public string forbiddenChunkName;
        public int seed;
    }
    public static Patch[] patches;

    public static string Url = "https://cfg.nitrome.com/dev/superleapday/patch.json";

    private static bool initialized = false;

    public static void Init()
    {
        if (initialized == true)
            return;

        initialized = true;

        // load from save
        {
            var json = SaveData.GetLevelPatch();
            if (json is not (null or ""))
                patches = PatchesFromJson(json);
        }

        // then from server, and overwrite copy in save if it works
        var req = UnityWebRequest.Get(Url);
        req.SendWebRequest().completed += (op) =>
        {
            if (req.result != UnityWebRequest.Result.Success)
            {
                Debug.LogWarning(
                    $"Unable to retrive level-patch from nitrome.com ({req.result})."
                );
                return;
            }

            var json = req.downloadHandler.text;
            // Debug.Log($"Level patch retrieved from nitrome.com...\n{json}");

            patches = PatchesFromJson(json);
            if (patches != null)
                SaveData.SetLevelPatch(json);
            else
                patches = new Patch[0];
        };

        // {
        //     // some sample code to create a patch file.
        //     var patch = new Patch
        //     {
        //         date = new DateTime(2024, 12, 25),
        //         forbiddenChunkName =
        //             "Set 2/Generic/Owen/Tutorial/owen_hgeneric_tutorial_pipe_1",
        //             // "Set 1/Generic/Alvaro/Hard/alvaro_generic_hard_blob_1"
        //         seed = 1
        //     };
        //     Debug.Log("patch:\n" + JsonFromPatches(patch));
        // }
    }

    private static Dictionary<string, string> ChunkNamesByHash()
    {
        var result = new Dictionary<string, string>();

        var chunkLibrary = Resources.Load<ChunkLibrary>("Chunk Library");
        foreach (var field in chunkLibrary.GetType().GetFields())
        {
            if (field.FieldType != typeof(ChunkLibrary.ChunkData[]))
                continue;

            var list = (ChunkLibrary.ChunkData[])field.GetValue(chunkLibrary);
            foreach (var chunkData in list)
            {
                result[MD5(chunkData.filename)] = chunkData.filename;
            }
        }

        return result;
    }

    private static string MD5(string key)
    {
        using (var md5 = System.Security.Cryptography.MD5.Create())
        {
            var bytes = UTF8Encoding.UTF8.GetBytes(key);
            var hash = md5.ComputeHash(bytes);
            var result = new System.Text.StringBuilder();
            for (int i = 0; i < hash.Length; i += 1)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }
    }

    private static (bool, Patch) MustReplaceRow(
        DateTime date, LevelGeneration.Row row
    )
    {
        foreach (var patch in patches)
        {
            if (patch.forbiddenChunkName != row.chunk.filename) continue;
            if (patch.date != null && patch.date.Value.Date != date.Date) continue;
            return (true, patch);
        }
        return (false, null);
    }

    private static ChunkLibrary.ChunkData[] ViableReplacementCandidates(
        ChunkLibrary.ChunkData chunkToReplace,
        ChunkLibrary chunkLibrary,
        LevelGeneration.Row[] rowsOfOriginal
    )
    {
        var filenameComponents = chunkToReplace.filename.Replace('\\', '/').Split('/');
        if (filenameComponents.Length < 4)
            return null;

        Theme? theme;
        switch (filenameComponents[1])
        {
            case "Generic":     theme = null;             break;
            case "Rainy Ruins": theme = Theme.RainyRuins; break;
            default: return null;
        };

        ChunkDifficulty difficulty;
        switch (filenameComponents[3])
        {
            case "Tutorial":  difficulty = ChunkDifficulty.Tutorial;  break;
            case "Easy":      difficulty = ChunkDifficulty.Easy;      break;
            case "Medium":    difficulty = ChunkDifficulty.Medium;    break;
            case "Hard":      difficulty = ChunkDifficulty.Hard;      break;
            default: return null;
        };

        var candidates = chunkLibrary.GetChunks(theme, difficulty).ToList();

        foreach (var original in rowsOfOriginal)
        {
            candidates.Remove(original.chunk);
        }

        return candidates.ToArray();
    }

    public static LevelGeneration.Row[] ApplyPatches(
        DateTime date, Theme theme, LevelGeneration.Row[] rows
    )
    {
        if (patches == null || patches.Length == 0)
            return rows;

        var result = new List<LevelGeneration.Row>();
        var chunkLibrary = Resources.Load<ChunkLibrary>("Chunk Library");

        foreach (var oldRow in rows)
        {
            var (mustReplaceRow, rule) = MustReplaceRow(date, oldRow);
            if (mustReplaceRow == false)
            {
                result.Add(oldRow);
                continue;
            }

            var candidates = ViableReplacementCandidates(
                oldRow.chunk, chunkLibrary, rows
            );
            if (candidates == null || candidates.Length == 0)
            {
                result.Add(oldRow);
                continue;
            }

            var rnd = new System.Random(rule.seed);
            var choice = rnd.Next() % candidates.Length;
            var newRow = new LevelGeneration.Row
            {
                chunk = candidates[choice],
                flipHorizontally = oldRow.flipHorizontally,
                type = oldRow.type
            };
            result.Add(newRow);
            Debug.Log(
                $"Replacing '{oldRow.chunk.filename} '" +
                $"with '{newRow.chunk.filename}"
            );

            if (oldRow.fillAreaOfInterestWithCheckpoint == true)
            {
                if (newRow.chunk.containsAreaOfInterest == true)
                    newRow.fillAreaOfInterestWithCheckpoint = true;
                else
                    result.Add(SubstituteCheckpoint());
            }
            else if (oldRow.fillAreaOfInterestWithBonusLift == true)
            {
                if (newRow.chunk.containsAreaOfInterest == true)
                    newRow.fillAreaOfInterestWithBonusLift = true;
                else
                    result.Add(SubstituteBonusLift());
            }
        }

        return result.ToArray();
    }

    private static LevelGeneration.Row SubstituteCheckpoint()
    {
        var filename = "Checkpoint/Mat/mat_checkpoint_30";
        var resource = Resources.Load<TextAsset>("Levels/" + filename);
        var bytes = resource.bytes;
        var level = NitromeEditor.Level.UnserializeFromBytes(bytes);

        var result = new ChunkLibrary.ChunkData
        {
            filename = filename,
            level = level
        };
        ChunkLibrary.FillChunkData(result, level);

        return new LevelGeneration.Row
        {
            chunk = result,
            type = LevelGeneration.RowType.DedicatedCheckpoint
        };
    }

    private static LevelGeneration.Row SubstituteBonusLift()
    {
        var filename = "Technical/bonus_lift";
        var resource = Resources.Load<TextAsset>("Levels/" + filename);
        var bytes = resource.bytes;
        var level = NitromeEditor.Level.UnserializeFromBytes(bytes);

        var result = new ChunkLibrary.ChunkData
        {
            filename = filename,
            level = level
        };
        ChunkLibrary.FillChunkData(result, level);

        return new LevelGeneration.Row
        {
            chunk = result,
            type = LevelGeneration.RowType.DedicatedBonusLift
        };
    }
    
    public static Patch[] PatchesFromJson(string json)
    {
        var chunkNamesByHash = ChunkNamesByHash();

        try
        {
            var patches = new List<Patch>();

            var jsonObject = JObject.Parse(json);
            var jsonPatches = (JArray)jsonObject["patches"];
            if (jsonPatches == null)
                return new Patch[0];

            foreach (var jsonPatch in jsonPatches)
            {
                var patch = new Patch();

                if (jsonPatch.Contains("date"))
                {
                    var jsonDate = (JArray)jsonPatch["date"];
                    patch.date = new DateTime(
                        (int)jsonDate[0], // year
                        (int)jsonDate[1], // month
                        (int)jsonDate[2]  // day
                    );
                }

                string forbiddenChunkHash = (string)jsonPatch["forbid"];
                patch.forbiddenChunkName = chunkNamesByHash[forbiddenChunkHash];
                patch.seed = (int)jsonPatch["seed"];
                patches.Add(patch);
            }

            return patches.ToArray();
        }
        catch
        {
            Debug.LogWarning("Parse error reading patch file.");
            return null;
        }
    }

    public static string JsonFromPatches(params Patch[] patches)
    {
        static JObject ObjectForPatch(Patch patch)
        {
            JObject result = new();
            if (patch.date != null)
            {
                result["date"] = new JArray(
                    patch.date.Value.Year,
                    patch.date.Value.Month,
                    patch.date.Value.Day
                );
            }
            result["forbid"] = MD5(patch.forbiddenChunkName);
            result["seed"] = patch.seed;
            return result;
        }

        var file = JObject.FromObject(new
        {
            patches = new JArray(
                patches.Select(p => ObjectForPatch(p))
            )
        });
        return file.ToString();
    }
}
