
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Threading.Tasks;

public class LevelTestingServer : MonoBehaviour
{
    public Text IpAddressField;

    public static NitromeEditor.Level receivedLevel;
    public static NitromeEditor.Level[] receivedLevels;
    private static string localIPAddress = null;
    private static TcpListener listener = null;

    public const int PortNumber = 12345;

    private void Start()
    {
        FillIPAddressField();
        StartListening();
    }

    private void StartListening()
    {
        if (listener != null) return;

        try
        {
            #if !(UNITY_SWITCH && !UNITY_EDITOR)
            listener = new TcpListener(IPAddress.Parse("0.0.0.0"), PortNumber);
            listener.Start();
            #endif
        }
        catch (SocketException)
        {
            listener?.Stop();
            listener = null;
            throw;
        }
    }

    private void StopListening()
    {
        if (listener != null)
        {
            listener?.Stop();
            listener = null;
        }
    }

    private async void FillIPAddressField()
    {
        var ip = await LocalIPAddress();

        if (this.IpAddressField != null)
        {
            this.IpAddressField.text = ip;
        }
    }

    public async Task<string> LocalIPAddress()
    {
        if (localIPAddress != null)
        {
            return localIPAddress;
        }

        IPHostEntry host;
        try
        {
            host = await Dns.GetHostEntryAsync(Dns.GetHostName());
        }
        catch (SocketException)
        {
            return "";
        }

        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork &&
                ip.ToString().StartsWith("192.168."))
            {
                localIPAddress = ip.ToString();
                return localIPAddress;
            }
        }

        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIPAddress = ip.ToString();
                return localIPAddress;
            }
        }

        return "";
    }

    private void OnApplicationPause(bool paused)
    {
        if (paused == true)
            StopListening();
        else if (paused == false)
            StartListening();
    }

    private void OnApplicationQuit()
    {
        StopListening();
    }

    private void OnDestroy()
    {
        StopListening();
    }

    private void Update()
    {       
#if (UNITY_EDITOR && true)
        // "solution" for Mac not being able to test chunks remotely - enable this code and press key L ingame to 
        // load "Levels/chunk_test" remote server way

        if(Input.GetKeyDown(KeyCode.L))
        {
            Debug.Log("Loading 'Levels/chunk_test'");
             var resource = Resources.Load<TextAsset>("Levels/chunk_test");
            var bytes = resource.bytes;
            receivedLevel =
                    NitromeEditor.Level.UnserializeFromBytes(bytes);
            DebugPanel.EnsureEditorMode();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            return;
        }
#endif

        int ReadInt(NetworkStream stream)
        {
            var buffer = new byte[4];
            stream.Read(buffer, 0, 4);
            return BitConverter.ToInt32(buffer, 0);
        }

        if (listener != null && listener.Pending() == true)
        {
            receivedLevel = null;
            receivedLevels = null;

            var client = listener.AcceptTcpClient();
            var stream = client.GetStream();

            var type = stream.ReadByte();
            if (type == 0)
            {
                var remaining = ReadInt(stream);
                var received = new List<byte>();

                while (remaining > 0)
                {
                    byte[] bytes = new byte[1024];
                    int length = stream.Read(bytes, 0, bytes.Length);
                    if (length == 0) break;
                    received.AddRange(bytes.Take(length));
                    remaining -= length;
                }

                receivedLevel =
                    NitromeEditor.Level.UnserializeFromBytes(received.ToArray());
            }
            else if (type == 10)
            {
                var levelCount = ReadInt(stream);
                var levels = new List<NitromeEditor.Level>();

                for (var l = 0; l < levelCount; l += 1)
                {
                    var remaining = ReadInt(stream);
                    var received = new List<byte>();

                    while (remaining > 0)
                    {
                        var maxBytesToRead = System.Math.Min(1024, remaining);
                        byte[] bytes = new byte[maxBytesToRead];
                        int length = stream.Read(bytes, 0, bytes.Length);
                        if (length == 0) break;
                        received.AddRange(bytes.Take(length));
                        remaining -= length;
                    }

                    levels.Add(
                        NitromeEditor.Level.UnserializeFromBytes(received.ToArray())
                    );
                }

                receivedLevels = levels.ToArray();
            }

            client.Close();

            DebugPanel.EnsureEditorMode();

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}
