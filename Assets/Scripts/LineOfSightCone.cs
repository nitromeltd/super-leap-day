using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class LineOfSightCone
{
    public class Result
    {
        public class Slice
        {
            public Vector2 fromPosition;
            public Vector2 toPosition;
            public Vector2 insideFromPosition;
            public Vector2 insideToPosition;
            public bool isStartOfSubdivision;
            public bool isEndOfSubdivision;
        }

        public Vector2 start;
        public Slice[] slices;
        public (Vector2 outer, Vector2 inner)[] border;
    }

    public class ShadowMap1D
    {
        /*
            ShadowMap1D natively places points in an (across, out) coordinate system
                across = 0..1 from one side of the cone to the other
                out    = distance projected along the central axis of the cone

            The purpose of this class is to take a bunch of random edges in that space,
            and starting along the (0..1) 'across' axis, if we look down the 'out' axis,
            which edges are visible (i.e. frontmost)? and where?
        */

        public class Edge
        {
            public float across1, out1;
            public float across2, out2;
            public Vector2 pt1RelativeToStart, pt2RelativeToStart;
            public Chunk edgeIsMarkerIntoChunk;
            public int indexByDepth;
            public bool active;
        }

        public class Region
        {
            public float fromAcross;
            public float toAcross;
            public Edge edge;
        }

        public Vector2 startVector;
        public Vector2 centerVector;
        public Vector2 endVector;
        public List<Edge> edges;

        public ShadowMap1D(Vector2 startVector, Vector2 centerVector, Vector2 endVector)
        {
            this.startVector = startVector;
            this.centerVector = centerVector;
            this.endVector = endVector;
            this.edges = new List<Edge>();
        }

        public void AddEdge(
            float across1,
            float out1,
            float across2,
            float out2,
            Vector2 pt1RelativeToStart,
            Vector2 pt2RelativeToStart,
            Chunk edgeIsMarkerIntoChunk
        )
        {
            this.edges.Add(new Edge
            {
                across1 = across1,
                across2 = across2,
                out1 = out1,
                out2 = out2,
                pt1RelativeToStart = pt1RelativeToStart,
                pt2RelativeToStart = pt2RelativeToStart,
                edgeIsMarkerIntoChunk = edgeIsMarkerIntoChunk
            });
        }

        public Region[] CalculateRegions()
        {
            var edgesByAcross1 = this.edges.OrderBy(l => l.across1).ToArray();
            var edgesByAcross2 = this.edges.OrderBy(l => l.across2).ToArray();

            foreach (var edge in edgesByAcross1)
            {
                edge.active = false;
            }

            Edge currentEdge = null;

            var result = new List<Region>();
            int index1 = 0;
            int index2 = 0;

            while (index1 < edgesByAcross1.Length || index2 < edgesByAcross2.Length)
            {
                float currentAcross;
                float nextEdgeStart = 1;
                float nextEdgeEnd = 1;

                if (index1 < edgesByAcross1.Length)
                    nextEdgeStart = edgesByAcross1[index1].across1;
                if (index2 < edgesByAcross2.Length)
                    nextEdgeEnd = edgesByAcross2[index2].across2;

                if (nextEdgeStart < nextEdgeEnd)
                {
                    // the next event of note is another edge starting.
                    // if it's topmost, this will mark the start of a new region.

                    var edge = edgesByAcross1[index1];
                    index1 += 1;
                    edge.active = true;
                    currentAcross = edge.across1;

                    if (currentEdge == null)
                    {
                        currentEdge = edge;
                    }
                    else
                    {
                        var currentVector = Vector2.Lerp(
                            this.startVector, this.endVector, currentAcross
                        );
                        var intersection = Util.LineIntersection(
                            currentEdge.pt1RelativeToStart,
                            currentEdge.pt2RelativeToStart,
                            Vector2.zero,
                            currentVector
                        ) ?? Vector2.zero;
                        var currentEdgeDepth = Vector2.Dot(centerVector, intersection);

                        if (edge.out1 < currentEdgeDepth - 0.01f)
                        {
                            currentEdge = edge;
                        }
                        else if (edge.out1 < currentEdgeDepth + 0.01f)
                        {
                            float currentEdgeGradient =
                                (currentEdge.out2 - currentEdge.out1) /
                                (currentEdge.across2 - currentEdge.across1);
                            float newEdgeGradient =
                                (edge.out2 - edge.out1) /
                                (edge.across2 - edge.across1);

                            if (newEdgeGradient < currentEdgeGradient)
                                currentEdge = edge;
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
                else
                {
                    // the next event of note is an edge ending.
                    // if that was the topmost edge, then we move out to the next
                    // active edge, and this will also mark the start of a new region.

                    var edge = edgesByAcross2[index2];
                    index2 += 1;
                    edge.active = false;

                    if (currentEdge == edge)
                    {
                        currentEdge = null;

                        float depth = float.PositiveInfinity;
                        foreach (var e in edgesByAcross1)
                        {
                            if (e.active == false) continue;
                            var throughCurrent =
                                (edge.across2 - e.across1) / (e.across2 - e.across1);
                            var eDepth = Mathf.Lerp(e.out1, e.out2, throughCurrent);
                            if (eDepth < depth)
                            {
                                depth = eDepth;
                                currentEdge = e;
                            }
                        }
                    }

                    currentAcross = edge.across2;
                }

                if (currentEdge == null)
                    continue;

                if (result.Count == 0)
                {
                    // our first region
                    result.Add(new Region
                    {
                        fromAcross = currentEdge.across1,
                        edge = currentEdge
                    });
                }
                else if (result[result.Count - 1].edge == currentEdge)
                {
                    // the same edge as last time is still topmost
                    result[result.Count - 1].toAcross = currentAcross;
                }
                else if (result[result.Count - 1].fromAcross + 1e-4 >= currentAcross)
                {
                    // the last edge was topmost for very thin width.
                    // we discard it and start the new edge in its place
                    result[result.Count - 1].edge = currentEdge;
                }
                else
                {
                    // a new edge is topmost, so we have a new region in the result
                    result[result.Count - 1].toAcross = currentAcross;
                    result.Add(new Region
                    {
                        fromAcross = currentAcross,
                        edge = currentEdge
                    });
                }
            }

            result[result.Count - 1].toAcross = 1;

            if (result.Count > 1 && result[result.Count - 1].fromAcross >= 0.9999f)
            {
                result.RemoveAt(result.Count - 1);
                result[result.Count - 1].toAcross = 1;
            }

            return result.ToArray();
        }
    }

    public class EdgesForTileGrid
    {
        public class Edge
        {
            public Vector2 from, to;
        }

        private Dictionary<TileGrid, Edge[]> edgesForTileGrid =
            new Dictionary<TileGrid, Edge[]>();

        public Edge[] GetEdgesForTileGrid(TileGrid tileGrid)
        {
            if (edgesForTileGrid.TryGetValue(tileGrid, out var value))
                return value;

            var edges = new List<Edge>();
            var tiles = new HashSet<Tile>();

            for (int ty = tileGrid.yMin; ty <= tileGrid.yMax; ty += 1)
            {
                var gridTy = ty - tileGrid.yMin;

                for (int tx = tileGrid.xMin; tx <= tileGrid.xMax; tx += 1)
                {
                    var gridTx = tx - tileGrid.xMin;

                    var t = tileGrid.GetTile(gridTx, gridTy);
                    if (t == null) continue;
                    if (tiles.Contains(t)) continue;
                    tiles.Add(t);

                    var bottomLeft = new Vector2(t.mapTx, t.mapTy);
                    var boundary = BoundaryForTileShape(t.spec.shape);
                    for (int n = 0; n < boundary.Length; n += 1)
                    {
                        var a = boundary[n];
                        var b = boundary[(n + 1) % boundary.Length];

                        edges.Add(new Edge
                        {
                            from = bottomLeft + a,
                            to = bottomLeft + b
                        });
                    }
                }
            }

            // remove pairs of edges in the same place with opposite orientation.
            // this is an interior edge between two tiles --
            // we don't care about either side.
            for (int a = 0; a < edges.Count; a += 1)
            {
                var edge = edges[a];
                var b = edges.FindIndex(
                    a + 1, e => e.from == edge.to && e.to == edge.from
                );
                if (b == -1)
                    continue;

                edges.RemoveAt(b);
                edges.RemoveAt(a);
                a -= 1;
            }

            // join pairs of lines which are coplanar and next to one another
            for (int a = 0; a < edges.Count; a += 1)
            {
                var edge = edges[a];
                var forward = edge.to - edge.from;
                var sideways = new Vector2(forward.y, -forward.x);
                var b = edges.FindIndex(
                    e =>
                        e.from == edge.to && 
                        Vector2.Dot(e.to - e.from, forward) > 0 &&
                        Vector2.Dot(e.to - e.from, sideways) == 0
                );
                if (b != -1)
                {
                    edges[a].to = edges[b].to;
                    edges.RemoveAt(b);
                    if (b < a)
                        a -= 1;
                    a -= 1;
                }
            }

            var result = edges.ToArray();
            edgesForTileGrid[tileGrid] = result;
            return result;
        }

        private static Vector2[] ShapeOfSquareTile = new [] {
            new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)
        };

        private static Vector2[] ShapeOfSlope22TL = new [] {
            new Vector2(0, 0), new Vector2(0, 1), new Vector2(2, 1)
        };
        private static Vector2[] ShapeOfSlope22TR = new [] {
            new Vector2(0, 1), new Vector2(2, 1), new Vector2(2, 0)
        };
        private static Vector2[] ShapeOfSlope22BL = new [] {
            new Vector2(0, 0), new Vector2(0, 1), new Vector2(2, 0)
        };
        private static Vector2[] ShapeOfSlope22BR = new [] {
            new Vector2(0, 0), new Vector2(2, 1), new Vector2(2, 0)
        };

        private static Vector2[] ShapeOfSlope45TL = new [] {
            new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1)
        };
        private static Vector2[] ShapeOfSlope45TR = new [] {
            new Vector2(0, 1), new Vector2(1, 1), new Vector2(1, 0)
        };
        private static Vector2[] ShapeOfSlope45BL = new [] {
            new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 0)
        };
        private static Vector2[] ShapeOfSlope45BR = new [] {
            new Vector2(0, 0), new Vector2(1, 1), new Vector2(1, 0)
        };

        private static Vector2[] ShapeOfTopLine = new [] {
            new Vector2(0, 0.5f), new Vector2(0, 1),
            new Vector2(1, 1), new Vector2(1, 0.5f)
        };

        private static Vector2[] ShapeOfCurveBL3Plus1 = new [] {
            new Vector2(0, 0),
            new Vector2(0, 4),
            new Vector2(1, 4),
            new Vector2(4 - 2.942f, 4 - 0.585f),
            new Vector2(4 - 2.772f, 4 - 1.148f),
            new Vector2(4 - 2.494f, 4 - 1.667f),
            new Vector2(4 - 2.121f, 4 - 2.121f),
            new Vector2(4 - 1.667f, 4 - 2.494f),
            new Vector2(4 - 1.148f, 4 - 2.772f),
            new Vector2(4 - 0.585f, 4 - 2.942f),
            new Vector2(4, 1),
            new Vector2(4, 0),
        };
        private static Vector2[] ShapeOfCurveBR3Plus1 = new [] {
            new Vector2(0, 0),
            new Vector2(0, 1),
            new Vector2(0.585f, 4 - 2.942f),
            new Vector2(1.148f, 4 - 2.772f),
            new Vector2(1.667f, 4 - 2.494f),
            new Vector2(2.121f, 4 - 2.121f),
            new Vector2(2.494f, 4 - 1.667f),
            new Vector2(2.772f, 4 - 1.148f),
            new Vector2(2.942f, 4 - 0.585f),
            new Vector2(3, 4),
            new Vector2(4, 4),
            new Vector2(4, 0),
        };
        private static Vector2[] ShapeOfCurveTL3Plus1 = new [] {
            new Vector2(0, 0),
            new Vector2(0, 4),
            new Vector2(4, 4),
            new Vector2(4, 3),
            new Vector2(4 - 0.585f, 2.942f),
            new Vector2(4 - 1.148f, 2.772f),
            new Vector2(4 - 1.667f, 2.494f),
            new Vector2(4 - 2.121f, 2.121f),
            new Vector2(4 - 2.494f, 1.667f),
            new Vector2(4 - 2.772f, 1.148f),
            new Vector2(4 - 2.942f, 0.585f),
            new Vector2(1, 0),
        };
        private static Vector2[] ShapeOfCurveTR3Plus1 = new [] {
            new Vector2(0, 3),
            new Vector2(0, 4),
            new Vector2(4, 4),
            new Vector2(4, 0),
            new Vector2(3, 0),
            new Vector2(2.942f, 0.585f),
            new Vector2(2.772f, 1.148f),
            new Vector2(2.494f, 1.667f),
            new Vector2(2.121f, 2.121f),
            new Vector2(1.667f, 2.494f),
            new Vector2(1.148f, 2.772f),
            new Vector2(0.585f, 2.942f),
        };

        private static Vector2[] ShapeOfCurveOutBL3 = new [] {
            new Vector2(0, 3),
            new Vector2(3, 3),
            new Vector2(3, 0),
            new Vector2(3 - 0.585f, 3 - 2.942f),
            new Vector2(3 - 1.148f, 3 - 2.772f),
            new Vector2(3 - 1.667f, 3 - 2.494f),
            new Vector2(3 - 2.121f, 3 - 2.121f),
            new Vector2(3 - 2.494f, 3 - 1.667f),
            new Vector2(3 - 2.772f, 3 - 1.148f),
            new Vector2(3 - 2.942f, 3 - 0.585f),
        };
        private static Vector2[] ShapeOfCurveOutBR3 = new [] {
            new Vector2(0, 0),
            new Vector2(0, 3),
            new Vector2(3, 3),
            new Vector2(2.942f, 3 - 0.585f),
            new Vector2(2.772f, 3 - 1.148f),
            new Vector2(2.494f, 3 - 1.667f),
            new Vector2(2.121f, 3 - 2.121f),
            new Vector2(1.667f, 3 - 2.494f),
            new Vector2(1.148f, 3 - 2.772f),
            new Vector2(0.585f, 3 - 2.942f),
        };
        private static Vector2[] ShapeOfCurveOutTL3 = new [] {
            new Vector2(3, 3),
            new Vector2(3, 0),
            new Vector2(0, 0),
            new Vector2(3 - 2.942f, 0.585f),
            new Vector2(3 - 2.772f, 1.148f),
            new Vector2(3 - 2.494f, 1.667f),
            new Vector2(3 - 2.121f, 2.121f),
            new Vector2(3 - 1.667f, 2.494f),
            new Vector2(3 - 1.148f, 2.772f),
            new Vector2(3 - 0.585f, 2.942f),
        };
        private static Vector2[] ShapeOfCurveOutTR3 = new [] {
            new Vector2(3, 0),
            new Vector2(0, 0),
            new Vector2(0, 3),
            new Vector2(0.585f, 2.942f),
            new Vector2(1.148f, 2.772f),
            new Vector2(1.667f, 2.494f),
            new Vector2(2.121f, 2.121f),
            new Vector2(2.494f, 1.667f),
            new Vector2(2.772f, 1.148f),
            new Vector2(2.942f, 0.585f),
        };

        private static Vector2[] BoundaryForTileShape(Tile.Shape shape) => shape switch
        {
            Tile.Shape.Slope22TL => ShapeOfSlope22TL,
            Tile.Shape.Slope22TR => ShapeOfSlope22TR,
            Tile.Shape.Slope22BL => ShapeOfSlope22BL,
            Tile.Shape.Slope22BR => ShapeOfSlope22BR,

            Tile.Shape.Slope45TL => ShapeOfSlope45TL,
            Tile.Shape.Slope45TR => ShapeOfSlope45TR,
            Tile.Shape.Slope45BL => ShapeOfSlope45BL,
            Tile.Shape.Slope45BR => ShapeOfSlope45BR,

            Tile.Shape.TopLine => ShapeOfTopLine,

            Tile.Shape.CurveBL3Plus1 => ShapeOfCurveBL3Plus1,
            Tile.Shape.CurveBR3Plus1 => ShapeOfCurveBR3Plus1,
            Tile.Shape.CurveTL3Plus1 => ShapeOfCurveTL3Plus1,
            Tile.Shape.CurveTR3Plus1 => ShapeOfCurveTR3Plus1,

            Tile.Shape.CurveOutBL3 => ShapeOfCurveOutBL3,
            Tile.Shape.CurveOutBR3 => ShapeOfCurveOutBR3,
            Tile.Shape.CurveOutTL3 => ShapeOfCurveOutTL3,
            Tile.Shape.CurveOutTR3 => ShapeOfCurveOutTR3,

            _ => ShapeOfSquareTile
        };
    }

    private EdgesForTileGrid cachedEdgesForTileGrid = new EdgesForTileGrid();

    public Result Calculate(
        Map map,
        Vector2 startPosition,
        float startAngleDegrees,
        float endAngleDegrees,
        float distance,
        float insideNotchDistance
    )
    {
        if (map.IsSolidAtPoint(startPosition) == true)
        {
            return new Result
            {
                start = startPosition,
                border = new (Vector2, Vector2)[0],
                slices = new Result.Slice[0]
            };
        }

        float angleDifference = Util.WrapAngle0To360(endAngleDegrees - startAngleDegrees);
        float coneEdgeGradient = Mathf.Tan((angleDifference * 0.5f) * Mathf.PI / 180);
        float centerAngleDegrees =
            startAngleDegrees + (Util.WrapAngleMinus180To180(angleDifference) * 0.5f);

        var startVector = new Vector2(
            Mathf.Cos(startAngleDegrees * Mathf.PI / 180),
            Mathf.Sin(startAngleDegrees * Mathf.PI / 180)
        );
        var endVector = new Vector2(
            Mathf.Cos(endAngleDegrees * Mathf.PI / 180),
            Mathf.Sin(endAngleDegrees * Mathf.PI / 180)
        );
        var startVectorNormal = new Vector2(startVector.y, -startVector.x);
        var endVectorNormal = new Vector2(endVector.y, -endVector.x);

        var centerDirection = new Vector2(
            Mathf.Cos(centerAngleDegrees * Mathf.PI / 180),
            Mathf.Sin(centerAngleDegrees * Mathf.PI / 180)
        );
        var acrossDirection = new Vector2(-centerDirection.y, centerDirection.x);

        var chunk = map.NearestChunkTo(startPosition);
        var result = CalculateForChunk(
            map:                map,
            chunk:              chunk,
            startPosition:      startPosition,
            startAngleDegrees:  startAngleDegrees,
            centerAngleDegrees: centerAngleDegrees,
            endAngleDegrees:    endAngleDegrees,
            centerDirection:    centerDirection,
            acrossDirection:    acrossDirection,
            startVector:        startVector,
            endVector:          endVector,
            startVectorNormal:  startVectorNormal,
            endVectorNormal:    endVectorNormal,
            coneEdgeGradient:   coneEdgeGradient,
            distance:           distance,
            recursions:         0
        );
        Subdivide(result, distance);
        ConstrainAndAddOutline(result, distance, insideNotchDistance);
        return result;
    }

    private Result CalculateForChunk(
        Map map,
        Chunk chunk,
        Vector2 startPosition,
        float startAngleDegrees,
        float centerAngleDegrees,
        float endAngleDegrees,
        Vector2 centerDirection,
        Vector2 acrossDirection,
        Vector2 startVector,
        Vector2 endVector,
        Vector2 startVectorNormal,
        Vector2 endVectorNormal,
        float coneEdgeGradient,
        float distance,
        int recursions
    )
    {
        var shadowMap = new ShadowMap1D(startVector, centerDirection, endVector);

        void ConsiderEdge(
            Vector2 aRelativeToStart,
            Vector2 bRelativeToStart,
            Chunk edgeIsMarkerIntoChunk = null
        )
        {
            if (Vector2.Dot(aRelativeToStart, startVectorNormal) > 0 &&
                Vector2.Dot(bRelativeToStart, startVectorNormal) > 0)
            {
                return;
            }
            if (Vector2.Dot(aRelativeToStart, endVectorNormal) < 0 &&
                Vector2.Dot(bRelativeToStart, endVectorNormal) < 0)
            {
                return;
            }

            {
                var lineVector = (bRelativeToStart - aRelativeToStart);
                var lineTangent = new Vector2(lineVector.y, -lineVector.x);
                if (Vector2.Dot(lineTangent, startVector) > 0 &&
                    Vector2.Dot(lineTangent, endVector) > 0 &&
                    Vector2.Dot(lineTangent, -aRelativeToStart) > 0)
                {
                    return;
                }
            }

            var aOut = Vector2.Dot(aRelativeToStart, centerDirection);
            var bOut = Vector2.Dot(bRelativeToStart, centerDirection);
            if (aOut < 0 && bOut < 0) return;

            if (aOut < 0)
            {
                var i = Util.LineIntersection(
                    aRelativeToStart, bRelativeToStart, Vector2.zero, acrossDirection
                );
                aRelativeToStart = i ?? aRelativeToStart;
                aOut = 0;
            }
            else if (bOut < 0)
            {
                var i = Util.LineIntersection(
                    aRelativeToStart, bRelativeToStart, Vector2.zero, acrossDirection
                );
                bRelativeToStart = i ?? bRelativeToStart;
                bOut = 0;
            }

            var aConeWidth = aOut * coneEdgeGradient * 2;
            var bConeWidth = bOut * coneEdgeGradient * 2;
            if (aConeWidth < 0.1f) aConeWidth = 0.1f;
            if (bConeWidth < 0.1f) bConeWidth = 0.1f;
            var aPositionAcross = Vector2.Dot(aRelativeToStart, acrossDirection);
            var bPositionAcross = Vector2.Dot(bRelativeToStart, acrossDirection);
            var aAcross = (aPositionAcross / aConeWidth) + 0.5f;
            var bAcross = (bPositionAcross / bConeWidth) + 0.5f;

            {
                float centerAcross = (0 - aAcross) / (bAcross - aAcross);
                float centerOut = Mathf.Lerp(aOut, bOut, centerAcross);
                if (centerOut < 0) return;
            }

            if (bAcross < aAcross) return;
            if (bAcross <= 0 || aAcross >= 1) return;

            Vector2 aPosition = aRelativeToStart;
            Vector2 bPosition = bRelativeToStart;

            if (aAcross < 0)
            {
                float s = (0 - aAcross) / (bAcross - aAcross);
                aPosition = Util.LineIntersection(
                    aRelativeToStart, bRelativeToStart, Vector2.zero, startVector
                ) ?? aRelativeToStart;
                aOut = Vector2.Dot(aPosition, centerDirection);
                aAcross = 0;
            }
            if (bAcross > 1)
            {
                float s = (1 - aAcross) / (bAcross - aAcross);
                bPosition = Util.LineIntersection(
                    aRelativeToStart, bRelativeToStart, Vector2.zero, endVector
                ) ?? bRelativeToStart;
                bOut = Vector2.Dot(bPosition, centerDirection);
                bAcross = 1;
            }

            shadowMap.AddEdge(
                aAcross, aOut, bAcross, bOut, aPosition, bPosition, edgeIsMarkerIntoChunk
            );
        }

        foreach (var tileGrid in chunk.solidTilesA)
        {
            var edges = this.cachedEdgesForTileGrid.GetEdgesForTileGrid(tileGrid);

            foreach (var edge in edges)
            {
                var a = edge.from + tileGrid.offset - startPosition;
                var b = edge.to   + tileGrid.offset - startPosition;
                ConsiderEdge(a, b);
            }
        }

        foreach (var item in chunk.subsetOfItemsRaycastCanHit)
        {
            if (item.solidToEnemy == false) continue;

            foreach (var hitbox in item.hitboxes)
            {
                var rect = hitbox.InWorldSpace();
                var bl = new Vector2(rect.min.x, rect.min.y) - startPosition;
                var br = new Vector2(rect.max.x, rect.min.y) - startPosition;
                var tl = new Vector2(rect.min.x, rect.max.y) - startPosition;
                var tr = new Vector2(rect.max.x, rect.max.y) - startPosition;
                if (hitbox.solidXMin == true) ConsiderEdge(bl, tl);
                if (hitbox.solidYMax == true) ConsiderEdge(tl, tr);
                if (hitbox.solidXMax == true) ConsiderEdge(tr, br);
                if (hitbox.solidYMin == true) ConsiderEdge(br, bl);
            }
        }

        if (chunk.index > 0 &&
            chunk.entry.type != Chunk.MarkerType.Wall)
        {
            var xCenter = chunk.entry.mapLocation.x - startPosition.x;
            var y       = chunk.entry.mapLocation.y - startPosition.y;
            ConsiderEdge(
                new Vector2(xCenter - 7, y),
                new Vector2(xCenter + 7, y),
                map.chunks[chunk.index - 1]
            );
        }

        if (chunk.index < map.chunks.Count - 1 && 
            chunk.exit.type != Chunk.MarkerType.Wall)
        {
            var xCenter = chunk.exit.mapLocation.x     - startPosition.x;
            var y       = chunk.exit.mapLocation.y + 1 - startPosition.y;
            ConsiderEdge(
                new Vector2(xCenter + 7, y),
                new Vector2(xCenter - 7, y),
                map.chunks[chunk.index + 1]
            );
        }

        var shadowMapRegions = shadowMap.CalculateRegions();
        var resultSlices = new List<Result.Slice>();

        for (int n = 0; n < shadowMapRegions.Length; n += 1)
        {
            var region = shadowMapRegions[n];
            var slice = new Result.Slice();

            if (region.fromAcross <= region.edge.across1)
            {
                slice.fromPosition = startPosition + region.edge.pt1RelativeToStart;
            }
            else
            {
                Vector2 relativePointCuttingOffRegion =
                    n == 0 ?
                    startVector :
                    shadowMapRegions[n - 1].edge.pt2RelativeToStart;
                var ptRelative = Util.LineIntersection(
                    region.edge.pt1RelativeToStart,
                    region.edge.pt2RelativeToStart,
                    Vector2.zero,
                    relativePointCuttingOffRegion
                ) ?? region.edge.pt1RelativeToStart;
                slice.fromPosition = startPosition + ptRelative;
            }

            if (region.toAcross >= region.edge.across2)
            {
                slice.toPosition = startPosition + region.edge.pt2RelativeToStart;
            }
            else
            {
                Vector2 relativePointCuttingOffRegion =
                    n == shadowMapRegions.Length - 1 ?
                    endVector :
                    shadowMapRegions[n + 1].edge.pt1RelativeToStart;
                var ptRelative = Util.LineIntersection(
                    region.edge.pt1RelativeToStart,
                    region.edge.pt2RelativeToStart,
                    Vector2.zero,
                    relativePointCuttingOffRegion
                ) ?? region.edge.pt2RelativeToStart;
                slice.toPosition = startPosition + ptRelative;
            }

            bool DoesConeEscapeIntoNextChunk() => Util.LineCircleIntersection(
                region.edge.pt1RelativeToStart,
                region.edge.pt2RelativeToStart,
                Vector2.zero,
                distance
            ) != null;

            if (region.edge.edgeIsMarkerIntoChunk != null &&
                DoesConeEscapeIntoNextChunk() == true &&
                recursions < 3)
            {
                var newStartAngleDegrees = Mathf.Atan2(
                    slice.fromPosition.y - startPosition.y,
                    slice.fromPosition.x - startPosition.x
                ) * 180 / Mathf.PI;

                var newEndAngleDegrees = Mathf.Atan2(
                    slice.toPosition.y - startPosition.y,
                    slice.toPosition.x - startPosition.x
                ) * 180 / Mathf.PI;
                
                float newAngleDifference =
                    Util.WrapAngle0To360(newEndAngleDegrees - newStartAngleDegrees);
                float newConeEdgeGradient =
                    Mathf.Tan((newAngleDifference * 0.5f) * Mathf.PI / 180);
                float newCenterAngleDegrees =
                    newStartAngleDegrees +
                    (Util.WrapAngleMinus180To180(newAngleDifference) * 0.5f);

                var newStartVector = new Vector2(
                    Mathf.Cos(newStartAngleDegrees * Mathf.PI / 180),
                    Mathf.Sin(newStartAngleDegrees * Mathf.PI / 180)
                );
                var newEndVector = new Vector2(
                    Mathf.Cos(newEndAngleDegrees * Mathf.PI / 180),
                    Mathf.Sin(newEndAngleDegrees * Mathf.PI / 180)
                );
                var newStartVectorNormal =
                    new Vector2(newStartVector.y, -newStartVector.x);
                var newEndVectorNormal =
                    new Vector2(newEndVector.y, -newEndVector.x);

                var newCenterDirection = new Vector2(
                    Mathf.Cos(newCenterAngleDegrees * Mathf.PI / 180),
                    Mathf.Sin(newCenterAngleDegrees * Mathf.PI / 180)
                );
                var newAcrossDirection =
                    new Vector2(-newCenterDirection.y, newCenterDirection.x);

                var resultForAdjacentChunk = CalculateForChunk(
                    map:                 map,
                    chunk:               region.edge.edgeIsMarkerIntoChunk,
                    startPosition:       startPosition,
                    startAngleDegrees:   newStartAngleDegrees,
                    centerAngleDegrees:  newCenterAngleDegrees,
                    endAngleDegrees:     newEndAngleDegrees,
                    centerDirection:     newCenterDirection,
                    acrossDirection:     newAcrossDirection,
                    startVector:         newStartVector,
                    endVector:           newEndVector,
                    startVectorNormal:   newStartVectorNormal,
                    endVectorNormal:     newEndVectorNormal,
                    coneEdgeGradient:    newConeEdgeGradient,
                    distance:            distance,
                    recursions:          recursions + 1
                );
                resultSlices.AddRange(resultForAdjacentChunk.slices);
            }
            else
            {
                resultSlices.Add(slice);
            }
        }

        return new Result
        {
            start = startPosition,
            slices = resultSlices.ToArray()
        };
    }

    private void Subdivide(Result result, float distance)
    {
        var slices = new List<Result.Slice>();

        void AddSlices(Vector2 from, Vector2 to)
        {
            var fromAngle = Mathf.Atan2(
                from.y - result.start.y, from.x - result.start.x
            ) * 180 / Mathf.PI;
            var toAngle = Mathf.Atan2(
                to.y - result.start.y, to.x - result.start.x
            ) * 180 / Mathf.PI;
            var subdivisions = Mathf.CeilToInt(
                Util.WrapAngle0To360(toAngle - fromAngle) / 5
            );
            for (int n = 0; n < subdivisions; n += 1)
            {
                var along1 = n / (float)subdivisions;
                var along2 = (n + 1) / (float)subdivisions;
                slices.Add(new Result.Slice
                {
                    fromPosition = Vector2.Lerp(from, to, along1),
                    toPosition = Vector2.Lerp(from, to, along2),
                    isStartOfSubdivision = (n == 0),
                    isEndOfSubdivision = (n == subdivisions - 1)
                });
            }
        }

        foreach (var slice in result.slices)
        {
            var startsOutside =
                (slice.fromPosition - result.start).sqrMagnitude >=
                distance * distance;
            var endsOutside =
                (slice.toPosition - result.start).sqrMagnitude >=
                distance * distance;

            if (startsOutside != endsOutside)
            {
                var i = Util.LineCircleIntersection(
                    slice.fromPosition, slice.toPosition, result.start, distance
                );
                if (i != null)
                {
                    var aToB = slice.toPosition - slice.fromPosition;
                    var aToI1 = i.Value.Item1 - slice.fromPosition;
                    var aToI2 = i.Value.Item2 - slice.fromPosition;

                    var along1 = Vector2.Dot(aToI1, aToB) / aToB.sqrMagnitude;
                    var along2 = Vector2.Dot(aToI2, aToB) / aToB.sqrMagnitude;
                    if (along1 >= 0 && along1 <= 1)
                    {
                        AddSlices(slice.fromPosition, i.Value.Item1);
                        AddSlices(i.Value.Item1, slice.toPosition);
                    }
                    else if (along2 >= 0 && along2 <= 1)
                    {
                        AddSlices(slice.fromPosition, i.Value.Item2);
                        AddSlices(i.Value.Item2, slice.toPosition);
                    }
                    continue;
                }
            }
            AddSlices(slice.fromPosition, slice.toPosition);
        }

        result.slices = slices.ToArray();
    }

    private void ConstrainAndAddOutline(
        Result result, float distance, float insideNotchDistance
    )
    {
        const float BorderWidth = 0.1f;

        var polygon = new List<Vector2>();

        for (int n = 0; n < result.slices.Length; n += 1)
        {
            var from = result.slices[n].fromPosition - result.start;
            var to   = result.slices[n].toPosition   - result.start;
            if (from.sqrMagnitude >= distance * distance)
                from = from.normalized * distance;
            if (to.sqrMagnitude >= distance * distance)
                to = to.normalized * distance;
            from += result.start;
            to   += result.start;

            result.slices[n].fromPosition = from;
            result.slices[n].toPosition = to;

            if (result.slices[n].isStartOfSubdivision == true &&
                (n == 0 || (from - polygon.Last()).sqrMagnitude >= 0.01f * 0.01f))
            {
                polygon.Add(from);
            }

            polygon.Add(to);
        }

        for (int n = result.slices.Length - 1; n >= 0; n -= 1)
        {
            var from = result.slices[n].fromPosition - result.start;
            var to   = result.slices[n].toPosition   - result.start;
            if (from.sqrMagnitude >= insideNotchDistance * insideNotchDistance)
                from = from.normalized * insideNotchDistance;
            if (to.sqrMagnitude >= insideNotchDistance * insideNotchDistance)
                to = to.normalized * insideNotchDistance;
            from += result.start;
            to   += result.start;

            result.slices[n].insideFromPosition = from;
            result.slices[n].insideToPosition = to;

            polygon.Add(to);
            if (n == 0)
                polygon.Add(from);
        }

        var edgeDirections = new Vector2[polygon.Count];

        for (int n = 0; n < polygon.Count; n += 1)
        {
            edgeDirections[n] = (
                polygon[(n + 1) % polygon.Count] - polygon[n]
            ).normalized;
        }

        result.border = new (Vector2 outer, Vector2 inner)[polygon.Count];

        for (int n = 0; n < polygon.Count; n += 1)
        {
            var nMinus1 = (n + polygon.Count - 1) % polygon.Count;
            var nPlus1 = (n + 1) % polygon.Count;
            var edgeDirection1 = new Vector2(
                -edgeDirections[nMinus1].y, edgeDirections[nMinus1].x
            );
            var edgeDirection2 = new Vector2(
                -edgeDirections[n].y, edgeDirections[n].x
            );

            var intersection = Util.LineIntersection(
                polygon[nMinus1] + (edgeDirection1 * BorderWidth),
                polygon[n]       + (edgeDirection1 * BorderWidth),
                polygon[n]       + (edgeDirection2 * BorderWidth),
                polygon[nPlus1]  + (edgeDirection2 * BorderWidth)
            );
            result.border[n].outer = polygon[n];
            if (intersection != null)
            {
                result.border[n].inner = intersection.Value;

                Vector2 toInner = (result.border[n].inner - result.border[n].outer);
                if (toInner.sqrMagnitude >= 3 * 3)
                {
                    result.border[n].inner =
                        result.border[n].outer + (toInner.normalized * 3);
                }
            }
            else
            {
                result.border[n].inner = polygon[n] + edgeDirection1 * BorderWidth;
            }
        }
    }
}
