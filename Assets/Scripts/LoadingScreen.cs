
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.Video;
using UnityEngine.UI;
using Spine.Unity;
#if UNITY_STANDALONE_WIN
using Steamworks;
#elif UNITY_STANDALONE_OSX && !UNITY_EDITOR
using Apple.Arcade;
#endif

public class LoadingScreen : MonoBehaviour
{

    public AudioClip nitromeSplashSfx;
    public VideoClip videoClip16x9_1920_1366_1334_1136;
    public VideoClip videoClip16x9_1920_1366_1334_1136Portrait;
    public VideoClip videoClip16x9_2560_2208;
    public VideoClip videoClip16x9_2560_2208Portrait;
    public VideoClip videoClip16x9_4096_3840;
    public VideoClip videoClip16x9_4096_3840Portrait;
    public VideoClip videoClip16x9_5120_and_higher;
    public VideoClip videoClip16x9_5120_and_higherPortrait;
    public VideoClip videoClip16x9; //default
    public VideoClip videoClip16x9Portrait; //default

    public VideoClip videoClip4x3;
    public VideoClip videoClip4x3Portrait;
    public VideoClip videoClip19_5x9;
    public VideoClip videoClip19_5x9Portrait;

    public VideoClip videoClip16x10_1440_1280;
    public VideoClip videoClip16x10_1440_1280Portrait;
    public VideoClip videoClip16x10_2880_2560_2304;
    public VideoClip videoClip16x10_2880_2560_2304Portrait;
    public VideoClip videoClip16x10;
    public VideoClip videoClip16x10Portrait;
    
    public VideoClip videoClip4_3x3;
    public VideoClip videoClip4_3x3Portrait;

    private Image nitromeImageSplash;
    private Image blackBackground;
    private Image blackOverlay;
    private Image radialBarParent;
    private Image radialBarFill;
    private SkeletonGraphic spineUIAnimation;
    private GameObject noInternetPanel;
    private GameObject whatsNewPanel;
    private PlushToyAd plushToyAd;
    private bool skipSequence;

    #if UNITY_STANDALONE_OSX
    [DllImport("LD2ExternalOSX")]
    private static extern void CloudSaving_Start();
    #elif UNITY_IOS || UNITY_TVOS
    [DllImport("__Internal")]
    private static extern void CloudSaving_Start();
    #endif

    public void Start()
    {   
        Game.InitScene();

        this.nitromeImageSplash = this.transform.Find("NitromeImageSplash").GetComponent<Image>();
        this.blackBackground = this.transform.Find("BlackBackground").GetComponent<Image>();
        this.blackOverlay = this.transform.Find("BlackOverlay").GetComponent<Image>();
        this.radialBarFill = this.transform.Find("RadialBarFill").GetComponent<Image>();
        this.radialBarParent = this.transform.Find("RadialBar").GetComponent<Image>();
        this.spineUIAnimation = this.transform.Find("SpineUIAnimation").GetComponent<SkeletonGraphic>();
        this.noInternetPanel = this.transform.Find("NoInternetPanel").gameObject;
        this.whatsNewPanel = this.transform.Find("WhatsNewPanel").gameObject;
        this.plushToyAd = this.transform.Find("PlushToyAd").gameObject.GetComponent<PlushToyAd>();
        this.noInternetPanel.SetActive(false);

        // var originalImageSize = this.nitromeImageSplash.rectTransform.sizeDelta;
        Vector2 originalImageSize = new Vector2(1400, 1390);

        var canvasSize = this.gameObject.GetComponent<RectTransform>().sizeDelta;

        if (GameCamera.IsLandscape())
        {
            float newHeight = canvasSize.y;
            float scaleRatio = newHeight / originalImageSize.y;
            Vector2 newAnchoredPosition = new Vector2(
                -(this.spineUIAnimation.rectTransform.sizeDelta.x * scaleRatio)/2,
                 (this.spineUIAnimation.rectTransform.sizeDelta.y * scaleRatio)/2            
            );
            this.spineUIAnimation.rectTransform.localScale = new Vector3(scaleRatio, scaleRatio, 1);
            this.spineUIAnimation.rectTransform.anchoredPosition = newAnchoredPosition;
        }
        else
        {
            float newWidth = canvasSize.x;
            float scaleRatio = newWidth / originalImageSize.x;
            Vector2 newAnchoredPosition = new Vector2(
                -(this.spineUIAnimation.rectTransform.sizeDelta.x * scaleRatio)/2,
                 (this.spineUIAnimation.rectTransform.sizeDelta.y * scaleRatio)/2            
            );
            this.spineUIAnimation.rectTransform.localScale = new Vector3(scaleRatio, scaleRatio, 1);
            this.spineUIAnimation.rectTransform.anchoredPosition = newAnchoredPosition;        
        }

        StartCoroutine(Sequence());
    }

    private (int width, int height) GetResolution()
    {
        if (GameCamera.IsLandscape())
        {
            return ((int)(Screen.width * Camera.main.rect.width / Camera.main.rect.height),
                Screen.height);
        }
        else
            return (Screen.width, Screen.height);
    }

    private float GetAspectRatio()
    {
        var r = GetResolution();
        return (float)r.width / (float)r.height;
    }

    private VideoClip CorrectVideoClip()
    {
        var resolution = GetResolution();
        Debug.Log("res:" + resolution);
        var videoClipToPlay = resolution switch
        {
            // 16x9 landscape
            (1136, 640) => videoClip16x9_1920_1366_1334_1136,
            (1334, 750) => videoClip16x9_1920_1366_1334_1136,
            (2208, 1242) => videoClip16x9_2560_2208,
            (1366, 768) => videoClip16x9_1920_1366_1334_1136,
            (1920, 1080) => videoClip16x9_1920_1366_1334_1136,
            (4096, 2304) => videoClip16x9_4096_3840,
            (5120, 2880) => videoClip16x9_5120_and_higher,
            (2560, 1440) => videoClip16x9_2560_2208,

            // 16x9 portrait
            (640, 1136) => videoClip16x9_1920_1366_1334_1136Portrait,
            (750, 1334) => videoClip16x9_1920_1366_1334_1136Portrait,
            (1242, 2208) => videoClip16x9_2560_2208Portrait,
            (768, 1366) => videoClip16x9_1920_1366_1334_1136Portrait,
            (1080, 1920) => videoClip16x9_1920_1366_1334_1136Portrait,
            (2304, 4096) => videoClip16x9_4096_3840Portrait,
            (2880, 5120) => videoClip16x9_5120_and_higherPortrait,
            (1440, 2560) => videoClip16x9_2560_2208Portrait,
        
            // 4x3 landscape
            (2048, 1536) => videoClip4x3,
            (2732, 2048) => videoClip4x3,
            (2224, 1668) => videoClip4x3,

            // 4x3 portrait
            (1536, 2048) => videoClip4x3Portrait,
            (2048, 2732) => videoClip4x3Portrait,
            (1668, 2224) => videoClip4x3Portrait,

            // 19.5x9 landscape
            (2436, 1125) => videoClip19_5x9,
            (2688, 1242) => videoClip19_5x9,
            (1792, 828) => videoClip19_5x9,

            // 19.5x9 portrait
            (1125, 2436) => videoClip19_5x9Portrait,
            (1242, 2688) => videoClip19_5x9Portrait,
            (828, 1792) => videoClip19_5x9Portrait,

            // 16x10 landscape
            (2304, 1440) => videoClip16x10_2880_2560_2304,
            (1440, 900) => videoClip16x10_1440_1280,
            (1280, 800) => videoClip16x10_1440_1280,
            (2560, 1600) => videoClip16x10_2880_2560_2304,
            (2880, 1800) => videoClip16x10_2880_2560_2304,

            // 16x10 portrait
            (1440, 2304) => videoClip16x10_2880_2560_2304Portrait, 
            (900, 1440) => videoClip16x10_1440_1280Portrait,
            (800, 1280) => videoClip16x10_1440_1280Portrait,
            (1600, 2560) => videoClip16x10_2880_2560_2304Portrait,
            (1800, 2880) => videoClip16x10_2880_2560_2304Portrait,

            // 4.3x3 landscape
            (2388, 1668) => videoClip4_3x3,

            // 4.3x3 portrait
            (1668, 2388) => videoClip4_3x3Portrait,
            _ => null
        };
        if (videoClipToPlay != null)
            return videoClipToPlay;

        float actualAspectRatio = GetAspectRatio();
        VideoClip ClosestMatch((float aspect, VideoClip clip)[] candidates) =>
            candidates.MinBy(c => Mathf.Abs(c.aspect - actualAspectRatio)).clip;

        if (resolution.width > resolution.height)
        {
            return ClosestMatch(new [] {
                (16f / 9f,  videoClip16x9_5120_and_higher),
                (16f / 10f, videoClip16x10_2880_2560_2304),
            });
        }
        else
        {
            return ClosestMatch(new [] {
                (3f / 4f,    videoClip4x3Portrait),
                (3f / 4.3f,  videoClip4_3x3Portrait),
                (10f / 16f,  videoClip16x10_2880_2560_2304Portrait),
                (9f / 16f,   videoClip16x9_2560_2208Portrait),
                (9f / 19.5f, videoClip19_5x9Portrait)
            });
        }
    }

    private IEnumerator Sequence()
    {
        var appleVideoSplashPlayer =
            this.transform.Find("AppleVideoSplash").GetComponent<VideoPlayer>();
        this.whatsNewPanel.SetActive(false);

        #if !(UNITY_SWITCH || UNITY_STANDALONE_WIN)
        var video = CorrectVideoClip();
        if (video != null)
        {
            bool prepared = false;
            bool finished = false;
            
            appleVideoSplashPlayer.clip = video;
            appleVideoSplashPlayer.prepareCompleted += (_) => { prepared = true; };
            appleVideoSplashPlayer.errorReceived += (_, __) => { finished = true; };
            appleVideoSplashPlayer.loopPointReached += (_) => { finished = true; };

            appleVideoSplashPlayer.Prepare();
            while (prepared == false && finished == false && this.skipSequence == false)
            {
                yield return null;
            }

            appleVideoSplashPlayer.Play();
            while (finished == false && this.skipSequence == false)
            {
                yield return null;
            }
        }
        #endif
        
        appleVideoSplashPlayer.gameObject.SetActive(false);

        {
            var tweener = new Tweener();
            tweener.Alpha(this.blackOverlay, 0, 0.35f, Easing.QuadEaseOut);
            tweener.Callback(0.0f, () =>
            {
                this.spineUIAnimation.freeze = false;
            });
            tweener.Callback(1.2f, () =>
            {
                Audio.instance.PlaySfx(this.nitromeSplashSfx, options: new Audio.Options(0.5f, false, 0));
            });
            tweener.Alpha(this.blackOverlay, 0, 0.01f, null, 1.85f);   //1.85

            while (!tweener.IsDone() && this.skipSequence == false)
            {
                tweener.Advance(Time.deltaTime);
                yield return null;
            }
        }

        {
            var tweener2 = new Tweener();
            tweener2.Alpha(this.blackOverlay, 1, 0.25f, null, 0);   //1.85
            tweener2.Alpha(this.radialBarParent, 1, 0.25f, null, 0.25f);     //2.1
            tweener2.Alpha(this.radialBarFill, 1, 0.25f, null, 0.25f);    //2.1        

            while (!tweener2.IsDone())
            {
                tweener2.Advance(Time.deltaTime);
                yield return null;
            }
        }
        
        if(this.skipSequence == false)
            yield return new WaitForSeconds(0.8f); //delay to make sure sound is not cut

        DontDestroyOnLoad(this);
        GameCenter.Init();

        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            this.noInternetPanel.SetActive(true);
            var overlay = this.noInternetPanel.transform.Find("BlackOverlay").GetComponent<Image>();
            var tweener = new Tweener();
            var alpha = tweener.Alpha(overlay, 0f, 0.2f);
            yield return tweener.Run();

            while((GameInput.pressedConfirm || Input.GetMouseButtonDown(0)) == false)
                yield return null;
            
            tweener = new Tweener();
            alpha = tweener.Alpha(overlay, 1f, 0.2f);
            yield return tweener.Run();

            this.noInternetPanel.SetActive(false);
        }

        if(SaveData.IsGameFirstRun() == false && SaveData.HasSeenWhatsNew() == false)
        {
            this.whatsNewPanel.SetActive(true);
            var overlay = this.whatsNewPanel.transform.Find("BlackOverlay").GetComponent<Image>();
            var tweener = new Tweener();
            var alpha = tweener.Alpha(overlay, 0f, 0.2f);
            yield return tweener.Run();

            while((GameInput.pressedConfirm || Input.GetMouseButtonDown(0)) == false)
                yield return null;

            tweener = new Tweener();
            alpha = tweener.Alpha(overlay, 1f, 0.2f);
            yield return tweener.Run();

            this.whatsNewPanel.SetActive(false);
        }
        else if(this.plushToyAd.CanShow() == true)
        {
            this.plushToyAd.gameObject.SetActive(true);
            var overlay = this.plushToyAd.gameObject.transform.Find("BlackOverlay").GetComponent<Image>();
            var tweener = new Tweener();
            var alpha = tweener.Alpha(overlay, 0f, 0.2f);
            yield return tweener.Run();

            while(this.plushToyAd.canContinue == false)
                yield return null;

            tweener = new Tweener();
            alpha = tweener.Alpha(overlay, 1f, 0.2f);
            yield return tweener.Run();

            this.plushToyAd.gameObject.SetActive(false);
        }

        SaveData.SetSeenWhatsNew(true);

        var load = SceneManager.LoadSceneAsync("Title", LoadSceneMode.Single);
        load.allowSceneActivation = false;
        while (load.progress < 0.9f)
        {
            this.radialBarFill.fillAmount = Util.Slide(
                this.radialBarFill.fillAmount, load.progress, 0.05f
            );
            yield return null;
        }

        load.allowSceneActivation = true;
        while (true)
        {
            if (load.isDone == true)
                break;

            this.radialBarFill.fillAmount = Util.Slide(
                this.radialBarFill.fillAmount, load.progress, 0.05f
            );

            yield return null;
        }

        yield return null;
        this.radialBarFill.fillAmount = 1;

        {
            var tweener = new Tweener();
            tweener.Alpha(this.radialBarParent, 0, 0.25f);
            tweener.Alpha(this.radialBarFill, 0, 0.25f);
            yield return tweener.Run();
        }

        spineUIAnimation.gameObject.SetActive(false);
        {
            var tweener = new Tweener();
            tweener.Alpha(this.blackOverlay, 0, 0.35f);
            yield return tweener.Run();
        }

        if (SaveData.IsGameFirstRun() == true)
        {
            SaveData.SetGameFirstRun(false);
        }
        Destroy(this.gameObject);
    }

    private void Update()
    {
        GameInput.Advance();
        if (GameInput.pressedConfirm == true ||
            Input.GetMouseButtonDown(0) == true)
            {
                if (SaveData.IsGameFirstRun() == false)
                {
                    this.skipSequence = true;
                }
            }
    }
}
