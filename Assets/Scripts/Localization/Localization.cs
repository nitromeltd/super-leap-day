using UnityEngine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Text;
using Steamworks;

#if UNITY_EDITOR
using UnityEditor;
#endif

// Go to http://www.unicode.org/charts/ to find missing characters in fonts

public class Localization
{
    public static bool isReady = false;
    public static LanguageId activeLanguage;
    public static Dictionary<string, string> stringTable;
    public static string[] keys;
    public static string[] pagedKeys;
    public static TMPro.TMP_FontAsset font;
    public static Dictionary<Material, Material> materialsForFont;

    private static LocalizationData cachedLocalizationData;
    private static Dictionary<LanguageId, Dictionary<string, string>> stringTableForLanguage;

    public enum LanguageId
    {
        EnglishUS, EnglishUK, French, FrenchCanadian, Italian, German,
        Spanish, ChineseSimplified, ChineseTraditional, Japanese,
        Korean, Russian, Turkish, Arabic, Portuguese, Dutch
    }

    public static void Init(LanguageId? languageId = null)
    {
        if (cachedLocalizationData == null || stringTableForLanguage == null)
        {
            if (Application.isPlaying)
            {
                cachedLocalizationData = Resources.Load<LocalizationData>("LocalizationData");
            }
            else
            {
    #if UNITY_EDITOR
                cachedLocalizationData = AssetDatabase.LoadAssetAtPath<LocalizationData>(
                    "Assets/Resources/LocalizationData.asset"
                );
    #endif
            }

            var parsedCSV = ParseCSV(cachedLocalizationData.csv.text);

            string ColumnName(LanguageId l) => l switch
            {
                LanguageId.Arabic              => "Arabic",
                LanguageId.Portuguese          => "Brazilian Portuguese",
                LanguageId.ChineseSimplified   => "Simplified Chinese",
                LanguageId.ChineseTraditional  => "Traditional Chinese",
                LanguageId.Dutch               => "Dutch",
                LanguageId.EnglishUK           => "English",
                LanguageId.EnglishUS           => "English",
                LanguageId.French              => "French",
                LanguageId.FrenchCanadian      => "Canadian French",
                LanguageId.German              => "German",
                LanguageId.Italian             => "Italian",
                LanguageId.Japanese            => "Japanese",
                LanguageId.Korean              => "Korean",
                LanguageId.Russian             => "Russian",
                LanguageId.Spanish             => "Spanish",
                LanguageId.Turkish             => "Turkish",
                _                              => "English"
            };
            var columnForLanguage = new Dictionary<LanguageId, int>();
            var headers = parsedCSV[0];
            foreach (LanguageId l in Enum.GetValues(typeof(LanguageId)))
            {
                columnForLanguage[l] = Array.IndexOf(headers, ColumnName(l));
            }

            stringTableForLanguage = new();
            foreach (LanguageId l in Enum.GetValues(typeof(LanguageId)))
                stringTableForLanguage[l] = new();
    
            var pageList = new List<string>();

            for (int rowIndex = 1; rowIndex < parsedCSV.Length; rowIndex += 1)
            {
                var row = parsedCSV[rowIndex];
                var key = row[0];
                var keyUppercase = key.ToUpper();

                if (stringTableForLanguage[0].ContainsKey(keyUppercase))
                    Debug.LogWarning($"Duplicate translation key in CSV: {key} row: {rowIndex}");

                foreach (LanguageId l in Enum.GetValues(typeof(LanguageId)))
                {
                    var value = row[columnForLanguage[l]];
                    stringTableForLanguage[l][keyUppercase] = value;
                }

                var page = row[1];
                if (key.Contains('<'))
                    key = RemoveRichText(key);
                pageList.Add($"{page}/{(key.Length > 100 ? key.Substring(0, 100) + "..." : key)}");
            }

            keys = stringTableForLanguage[LanguageId.EnglishUS].Keys.ToArray();
            pagedKeys = pageList.ToArray();
        }

        activeLanguage =
            languageId ??
            SaveData.GetSelectedLanguage() ??
            DefaultLanguageAccordingToSteam() ??
            DefaultLanguageAccordingToSystem();
        stringTable = stringTableForLanguage[activeLanguage];

        font = activeLanguage switch
        {
            LanguageId.Arabic             => cachedLocalizationData.fontForArabic,
            LanguageId.ChineseSimplified  => cachedLocalizationData.fontForChineseSimplified,
            LanguageId.ChineseTraditional => cachedLocalizationData.fontForChineseTraditional,
            LanguageId.Russian            => cachedLocalizationData.fontForRussian,
            LanguageId.Korean             => cachedLocalizationData.fontForKorean,
            LanguageId.Japanese           => cachedLocalizationData.fontForJapanese,
            _                             => cachedLocalizationData.fontLeapDay,
        };

        materialsForFont = new Dictionary<Material, Material>();
        isReady = true;
    }

    public static LanguageId? DefaultLanguageAccordingToSteam()
    {
        #if (UNITY_EDITOR || UNITY_STANDALONE_WIN) && STEAMWORKS_NET

        return SteamApps.GetCurrentGameLanguage() switch
        {
            "english"    => LanguageId.EnglishUS,
            "french"     =>
                CultureInfo.CurrentCulture.Name == "fr-CA" ?
                LanguageId.FrenchCanadian :
                LanguageId.French,
            "italian"    => LanguageId.Italian,
            "german"     => LanguageId.German,
            "spanish"    => LanguageId.Spanish,
            "schinese"   => LanguageId.ChineseSimplified,
            "tchinese"   => LanguageId.ChineseTraditional,
            "japanese"   => LanguageId.Japanese,
            "koreana"    => LanguageId.Korean,
            "russian"    => LanguageId.Russian,
            "turkish"    => LanguageId.Turkish,
            "arabic"     => LanguageId.Arabic,
            "portuguese" => LanguageId.Portuguese,
            "dutch"      => LanguageId.Dutch,
            _            => null
        };

        #else

        return null;

        #endif
    }

    public static LanguageId DefaultLanguageAccordingToSystem()
    {
        if (Application.systemLanguage == SystemLanguage.French &&
            CultureInfo.CurrentCulture.Name == "fr-CA")
        {
            return LanguageId.FrenchCanadian;
        }

        return Application.systemLanguage switch
        {
            SystemLanguage.French             => LanguageId.French,
            SystemLanguage.German             => LanguageId.German,
            SystemLanguage.Italian            => LanguageId.Italian,
            SystemLanguage.Spanish            => LanguageId.Spanish,
            SystemLanguage.ChineseSimplified  => LanguageId.ChineseSimplified,
            SystemLanguage.ChineseTraditional => LanguageId.ChineseTraditional,
            SystemLanguage.Japanese           => LanguageId.Japanese,
            SystemLanguage.Korean             => LanguageId.Korean,
            SystemLanguage.Russian            => LanguageId.Russian,
            SystemLanguage.Turkish            => LanguageId.Turkish,
            SystemLanguage.Arabic             => LanguageId.Arabic,
            SystemLanguage.Portuguese         => LanguageId.Portuguese,
            SystemLanguage.Dutch              => LanguageId.Dutch,
            _                                 => LanguageId.EnglishUS,
        };
    }

    public static string GetText(string key, params string[] substitutions)
    {
        if (isReady == false)
            Init();

        if (stringTable.TryGetValue(key, out var value))
            key = value;
        if (substitutions != null && substitutions.Length > 0)
            key = string.Format(key, GetStrings(substitutions));
        if(activeLanguage == LanguageId.Arabic)
            key = FormatRichText(key, ArabicSupport.ArabicFixer.Fix);
        if(activeLanguage == LanguageId.Turkish)
            key = FormatRichText(key, FixTurkish);

        return key;
    }

    public static string GetTextWithoutFixes(string key, params string[] substitutions)
    {
        /* The function ArabicFixer.Fix reverses the text so that RTL text can be
           displayed in TextMeshPro, which is permanently entrenched in LTR thinking.
           
           However, you obviously wouldn't want to reverse the text twice.
           This might happen if you nest translations inside one another using
           substitutions, e.g.
                GetText("DAY_MONTH", 15, GetText("July")).
           would first fix and then un-fix the word "July" in Arabic.
           Using this function can avoid that situation, i.e.
                GetText("DAY_MONTH", 15, GetTextWithoutFixes("July")) would work. */

        if (isReady == false)
            Init();

        if (stringTable.TryGetValue(key, out var value))
            key = value;
        if (substitutions != null && substitutions.Length > 0)
            key = string.Format(key, GetStrings(substitutions));

        return key;
    }

    // If automatically translating substitutions becomes an issue / redundant, remove this method
    public static string[] GetStrings(string[] keys)
    {
        var translated = new string[keys.Length];
        for(int n = 0; n < keys.Length; n += 1)
        {
            if(stringTable.TryGetValue(keys[n], out var value))
                translated[n] = value;
            else
                translated[n] = keys[n];
        }
        return translated;
    }

    public static string GetTextForDayWithOrdinal(DateTime date)
    {
        return Localization.GetText("DAY_" + date.Day);
    }

    public static string GetTextForMonthName(DateTime dateTime)
    {
        var englishMonth = dateTime.Month switch
        {
            1 => "January",
            2 => "February",
            3 => "March",
            4 => "April",
            5 => "May",
            6 => "June",
            7 => "July",
            8 => "August",
            9 => "September",
            10 => "October",
            11 => "November",
            12 => "December",
            _ => ""
        };
        return Localization.GetText(englishMonth);
    }

    public static TMPro.TMP_FontAsset GetFontForLanguage(LanguageId language)
    {
        return language switch
        {
            LanguageId.Arabic             => cachedLocalizationData.fontForArabic,
            LanguageId.ChineseSimplified  => cachedLocalizationData.fontForChineseSimplified,
            LanguageId.ChineseTraditional => cachedLocalizationData.fontForChineseTraditional,
            LanguageId.Russian            => cachedLocalizationData.fontForRussian,
            LanguageId.Korean             => cachedLocalizationData.fontForKorean,
            LanguageId.Japanese           => cachedLocalizationData.fontForJapanese,
            _                             => cachedLocalizationData.fontLeapDay,
        };
    }

    public static void SetTextFont(TMPro.TMP_Text text)
    {
        if (isReady == false)
            Init();

        if (font == text.font)
            return;

        // korean is too bunched
        if(activeLanguage == LanguageId.Korean)
            text.characterSpacing += 5;

        // arabic needs to join
        if(activeLanguage == LanguageId.Arabic)
            text.characterSpacing = 0;

        if (materialsForFont.ContainsKey(text.fontSharedMaterial))
        {
            var originalMaterial = text.fontSharedMaterial;
            text.font = font;
            text.fontSharedMaterial = materialsForFont[originalMaterial];

            if(activeLanguage == LanguageId.Arabic)
                GetLayeredText(text);
            return;
        }

        var source = text.fontSharedMaterial;
        var target = new Material(font.material);
        target.SetColor(TMPro.ShaderUtilities.ID_FaceColor, source.GetColor(TMPro.ShaderUtilities.ID_FaceColor));
        target.SetFloat(TMPro.ShaderUtilities.ID_FaceDilate, source.GetFloat(TMPro.ShaderUtilities.ID_FaceDilate));
        target.SetColor(TMPro.ShaderUtilities.ID_OutlineColor, source.GetColor(TMPro.ShaderUtilities.ID_OutlineColor));
        target.SetFloat(TMPro.ShaderUtilities.ID_OutlineWidth, source.GetFloat(TMPro.ShaderUtilities.ID_OutlineWidth));
        target.SetFloat(TMPro.ShaderUtilities.ID_UnderlayOffsetY, source.GetFloat(TMPro.ShaderUtilities.ID_UnderlayOffsetY));
        target.SetFloat(TMPro.ShaderUtilities.ID_UnderlayDilate, source.GetFloat(TMPro.ShaderUtilities.ID_UnderlayDilate));
        target.SetColor(TMPro.ShaderUtilities.ID_UnderlayColor, source.GetColor(TMPro.ShaderUtilities.ID_UnderlayColor));

        // outlines on Asian fonts are too large
        if
        (
            activeLanguage == LanguageId.ChineseSimplified ||
            activeLanguage == LanguageId.ChineseTraditional ||
            activeLanguage == LanguageId.Korean ||
            activeLanguage == LanguageId.Japanese
        )
        {
            target.SetFloat(TMPro.ShaderUtilities.ID_FaceDilate, source.GetFloat(TMPro.ShaderUtilities.ID_FaceDilate) / 2);
            target.SetFloat(TMPro.ShaderUtilities.ID_OutlineWidth, source.GetFloat(TMPro.ShaderUtilities.ID_OutlineWidth) / 2);
        }
        // and too small for Arabic
        else if(activeLanguage == LanguageId.Arabic)
        {
            target.SetFloat(TMPro.ShaderUtilities.ID_FaceDilate, source.GetFloat(TMPro.ShaderUtilities.ID_FaceDilate) * 3);
            target.SetFloat(TMPro.ShaderUtilities.ID_OutlineWidth, source.GetFloat(TMPro.ShaderUtilities.ID_OutlineWidth) * 3);
        }

        if(source.IsKeywordEnabled(TMPro.ShaderUtilities.Keyword_Underlay))
            target.EnableKeyword(TMPro.ShaderUtilities.Keyword_Underlay);
        else
            target.DisableKeyword(TMPro.ShaderUtilities.Keyword_Underlay);
        if(source.IsKeywordEnabled(TMPro.ShaderUtilities.Keyword_Outline))
            target.EnableKeyword(TMPro.ShaderUtilities.Keyword_Outline);
        else
            target.DisableKeyword(TMPro.ShaderUtilities.Keyword_Outline);
        text.font = font;
        text.fontSharedMaterial = target;
        materialsForFont[source] = target;

        if(activeLanguage == LanguageId.Arabic)
            GetLayeredText(text);
    }

    // For Arabic joined-text we need 2 layers of text to manage the outline
    // TextMeshPro copies the material
    public static void GetLayeredText(TMPro.TMP_Text text)
    {
        if(text.fontSharedMaterial.GetFloat(TMPro.ShaderUtilities.ID_OutlineWidth) == 0) return;

        var obj = new GameObject($"{text.name} face");
        obj.transform.parent = text.transform;
        obj.transform.localPosition = Vector3.zero;
        obj.transform.localRotation = Quaternion.identity;
        TMPro.TMP_Text copy = (TMPro.TMP_Text)obj.AddComponent(text.GetType());
        copy.rectTransform.anchorMin = Vector2.zero;
        copy.rectTransform.anchorMax = Vector2.one;
        copy.rectTransform.offsetMin = Vector2.zero;
        copy.rectTransform.offsetMax = Vector2.zero;
        copy.text = text.text;
        copy.font = text.font;
        copy.color = text.color;
        copy.horizontalAlignment = text.horizontalAlignment;
        copy.verticalAlignment = text.verticalAlignment;
        copy.lineSpacing = text.lineSpacing;
        copy.characterSpacing = text.characterSpacing;
        copy.fontSize = text.fontSize;
        copy.fontSizeMax = text.fontSizeMax;
        copy.fontSizeMin = text.fontSizeMin;
        copy.raycastTarget = text.raycastTarget;
        copy.enableAutoSizing = text.enableAutoSizing;
        copy.transform.localScale = Vector3.one;

        var meshRenderer = text.GetComponent<MeshRenderer>();
        if(meshRenderer != null)
        {
            var meshRendererCopy = copy.GetComponent<MeshRenderer>();
            meshRendererCopy.sortingLayerName = meshRenderer.sortingLayerName;
            meshRendererCopy.sortingOrder = meshRenderer.sortingOrder + 1;
        }

        var circleText = text.GetComponent<CircleText>();
        if(circleText != null)
        {
            var circleTextCopy = copy.gameObject.AddComponent<CircleText>();
            circleTextCopy.angle = circleText.angle;
            circleTextCopy.autoSize = circleText.autoSize;
            circleTextCopy.widthMax = circleText.widthMax;
            circleTextCopy.radius = circleText.radius;
            circleText.child = circleTextCopy;
        }

        // the replacement font material becomes a key for the face material
        if(materialsForFont.ContainsKey(text.fontSharedMaterial))
        {
            copy.fontSharedMaterial = materialsForFont[text.fontSharedMaterial];
        }
        else
        {
            copy.fontSharedMaterial = new Material(text.fontSharedMaterial);
            copy.fontSharedMaterial.SetFloat(TMPro.ShaderUtilities.ID_OutlineWidth, 0);
            copy.fontSharedMaterial.SetFloat(TMPro.ShaderUtilities.ID_FaceDilate, 0);
            copy.fontSharedMaterial.DisableKeyword(TMPro.ShaderUtilities.Keyword_Underlay);
            materialsForFont[text.fontSharedMaterial] = copy.fontSharedMaterial;
        }
    }

    private static string[][] ParseCSV(string content)
    {
        var result = new List<string[]>();
        var currentRow = new List<string>();
        var currentItem = new StringBuilder();
        bool insideQuotes = false;

        for (int n = 0; n < content.Length; n += 1)
        {
            char ch = content[n];

            if (ch == '\"')
            {
                if (n < content.Length - 1 && content[n + 1] == '\"')
                {
                    currentItem.Append("\"");
                    n += 1;
                }
                else
                {
                    insideQuotes = !insideQuotes;
                }
            }
            else if (ch == ',' && insideQuotes == false)
            {
                currentRow.Add(currentItem.ToString());
                currentItem.Clear();
            }
            else if (ch == '\n' && insideQuotes == false)
            {
                currentRow.Add(currentItem.ToString());
                result.Add(currentRow.ToArray());
                currentRow = new List<string>();
                currentItem.Clear();
            }
            else if (ch == '\r')
            {
                // ignore
            }
            else
            {
                currentItem.Append(content[n]);
            }
        }

        currentRow.Add(currentItem.ToString());
        result.Add(currentRow.ToArray());

        return result.ToArray();
    }

    public static void UpdateAllText()
    {
        var objs = SceneManager.GetActiveScene().GetRootGameObjects();
        var list = new List<LocalizeText>();
        foreach(var o in objs)
            list.AddRange(o.GetComponentsInChildren<LocalizeText>());

        foreach(var lt in list)
            lt.UpdateText();
    }

    public static bool IsLanguageWithOwnFont(LanguageId languageId)
    {
        return languageId switch
        {
            LanguageId.Arabic               => true,
            LanguageId.ChineseSimplified    => true,
            LanguageId.ChineseTraditional   => true,
            LanguageId.Japanese             => true,
            LanguageId.Korean               => true,
            LanguageId.Russian              => true,
            _ => false
        };
    }
    
    public static string FixTurkish(string str)
    {
        return str.ToUpper(new CultureInfo("tr-TR", false));
    }

    // Arabic and Turkish modifications break rich text
    public static string FormatRichText(string input, System.Func<string, string> func)
    {
        // func doesn't get called unless we have rich text
        if (input.Contains('<') == false)
            return func(input);

        var result = "";
        int start = 0;
        while (start < input.Length)
        {
            if (input[start] == '<')
            {
                // next thing is a tag <>
                int end = input.IndexOf('>', start);
                result += input.Substring(start, end + 1 - start);
                start = end + 1;
            }
            else if (input[start] == ' ')
            {
                // spaces after tag, before text
                result += ' ';
                start += 1;
            }
            else
            {
                // some text. don't include spaces at the end of the run
                int next = input.IndexOf('<', start);
                if (next == -1)
                    next = input.Length;
                while (next > start && input[next - 1] == ' ')
                    next -= 1;
                result += func(input.Substring(start, next - start));
                start = next;
            }
        }

        return result;
    }

    // format for pagination
    public static string RemoveRichText(string input)
    {
        bool inside = false;
        string inner = "";
        string output = "";
        for(int i = 0; i < input.Length; i++)
        {
            var c = input[i];
            switch(c)
            {
                case '<':
                    inside = false;
                    output += inner;
                    inner = "";
                    break;
                case '>':
                    inside = true;
                    break;
                case '\n':
                    output += ' ';
                    break;
                default:
                    if(inside == true)
                        inner += c;
                    break;
            }
        }
        return output;
    }

    /*
        We shouldn't really be using most of these calendars, but unless they're
        mentioned, like so, a compiler bug seems to be stripping these classes out of
        the executable on iOS, which then leads to an ArgumentOutOfRangeException
        when calling DateTime.ToString() or TimeSpan.ToString() when the current
        culture is e.g. ar-SA (Saudi Arabia) or th-TH (Thailand).
        It doesn't happen if you pass InvariantCulture, and that's quite likely
        to be what we want, but this is here anyway just in case.
    */
    private static void PreventLinkerFromStrippingCommonLocalizationReferences()
    {
        _ = new System.Globalization.ChineseLunisolarCalendar();
        _ = new System.Globalization.GregorianCalendar();
        _ = new System.Globalization.HebrewCalendar();
        _ = new System.Globalization.HijriCalendar();
        _ = new System.Globalization.JapaneseCalendar();
        _ = new System.Globalization.JapaneseLunisolarCalendar();
        _ = new System.Globalization.JulianCalendar();
        _ = new System.Globalization.KoreanCalendar();
        _ = new System.Globalization.KoreanLunisolarCalendar();
        _ = new System.Globalization.PersianCalendar();
        _ = new System.Globalization.TaiwanCalendar();
        _ = new System.Globalization.TaiwanLunisolarCalendar();
        _ = new System.Globalization.ThaiBuddhistCalendar();
        _ = new System.Globalization.UmAlQuraCalendar();
    }
}
