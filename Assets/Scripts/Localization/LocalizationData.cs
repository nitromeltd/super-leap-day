using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Localization Data")]
public class LocalizationData : ScriptableObject
{
    public TextAsset csv;
    public TMPro.TMP_FontAsset fontLeapDay;
    public TMPro.TMP_FontAsset fontForArabic;
    public TMPro.TMP_FontAsset fontForChineseSimplified;
    public TMPro.TMP_FontAsset fontForChineseTraditional;
    public TMPro.TMP_FontAsset fontForJapanese;
    public TMPro.TMP_FontAsset fontForKorean;
    public TMPro.TMP_FontAsset fontForRussian;

    [HideInInspector] public Localization.LanguageId testLanguage;
}
