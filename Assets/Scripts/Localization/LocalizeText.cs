using UnityEngine;

public class LocalizeText : MonoBehaviour
{
    private TMPro.TMP_Text text;
    public string key;
    private string[] substitutions;

    private void Awake()
    {
        if (this.text == null)
        {
            this.text = GetComponent<TMPro.TMP_Text>();
            if (string.IsNullOrWhiteSpace(this.key) == true)
                this.key = this.text.text.ToUpper();
            UpdateText();
        }
    }

    public void UpdateText()
    {
        // remove layered text
        if(this.transform.childCount > 0)
        {
            var txts = GetComponentsInChildren<TMPro.TMP_Text>();
            if(txts.Length > 1)
                Destroy(txts[1].gameObject);
        }
        if (this.key != null)
        {
            // key == null means that SetTextRaw() was called last, so no updating should occur.
            this.text.text = Localization.GetText(this.key, substitutions);
            Localization.SetTextFont(this.text);
        }
    }

    public void SetText(string key, params string[] substitutions)
    {
        if (this.text == null)
            Awake();

        this.key = key.ToUpper();
        this.substitutions = substitutions;
        this.text.text = Localization.GetText(key.ToUpper(), substitutions);

        // set layer text
        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
        {
            var texts = GetComponentsInChildren<TMPro.TMP_Text>();
            if(texts.Length > 1)
            {
                texts[1].text = this.text.text;
            }
        }
    }

    public void SetTextRaw(string str)
    {
        if (this.text == null)
            Awake();

        this.key = null;
        this.substitutions = null;
        this.text.text = str;

        // set layer text
        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
        {
            var texts = GetComponentsInChildren<TMPro.TMP_Text>();
            if(texts.Length > 1)
            {
                texts[1].text = str;
            }
        }
    }

    public void SetTextRawForceLanguage(string str, Localization.LanguageId language)
    {
        if (this.text == null)
            Awake();

        this.key = null;
        this.substitutions = null;

        if (language == Localization.LanguageId.Arabic)
            str = Localization.FormatRichText(str, ArabicSupport.ArabicFixer.Fix);
        if (language == Localization.LanguageId.Turkish)
            str = Localization.FormatRichText(str, Localization.FixTurkish);

        this.text.text = str;
        this.text.font = Localization.GetFontForLanguage(language);
    }

    public void SetAlpha(float a)
    {
        if (this.text == null)
            Awake();

        this.text.alpha = a;

        // set layer text
        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
        {
            var texts = GetComponentsInChildren<TMPro.TMP_Text>();
            if(texts.Length > 1)
            {
                texts[1].alpha = a;
            }
        }
    }

}
