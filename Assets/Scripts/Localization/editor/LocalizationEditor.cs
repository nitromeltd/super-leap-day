using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Linq;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(LocalizationData))]
public class LocalizationEditor : Editor
{
    private SerializedProperty testLanguage;

    void OnEnable()
    {
        testLanguage = serializedObject.FindProperty("testLanguage");
    }

    public override void OnInspectorGUI()
    {
        if(Localization.isReady == false)
            Localization.Init();

        DrawDefaultInspector();


        GUILayout.Space(5);
        EditorGUILayout.LabelField($"Total translation keys: {Localization.keys.Length}", EditorStyles.boldLabel);
        if(GUILayout.Button("Update Keys"))
        {
            Localization.Init();
        }

        GUILayout.Space(5);
        EditorGUILayout.LabelField("Preview Language", EditorStyles.boldLabel);
        EditorGUILayout.PropertyField(testLanguage);
        if(GUILayout.Button("Change LocalizeText (Only in Play Mode)"))
        {
            if(Application.isPlaying == true)
            {
                Localization.Init((target as LocalizationData).testLanguage);

                var list = FindObjectsOfType<LocalizeText>();
                foreach(var lt in list)
                    lt.UpdateText();
            }
        }
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Deployment Tools", EditorStyles.boldLabel);

        var stage = StageUtility.GetCurrentStage();

        GUILayout.Space(5);
        if(GUILayout.Button("Add LocalizeText to all TextMeshPros"))
        {
            var list = stage.FindComponentsOfType<TMPro.TMP_Text>();
            AddLocalizedTextToTextMeshPros(list);
        }
        if(GUILayout.Button("Add LocalizeText to selected TextMeshPros"))
        {
            var list = new List<TMPro.TMP_Text>();
            foreach(var obj in Selection.objects)
            {
                if(obj is GameObject)
                {
                    var txt = (obj as GameObject).GetComponent<TMPro.TMP_Text>();
                    if(txt != null) list.Add(txt);
                }
                else if(obj is TMPro.TMP_Text)
                    list.Add(obj as TMPro.TMP_Text);
            }
            AddLocalizedTextToTextMeshPros(list.ToArray());
        }
        if(GUILayout.Button("Select all TextMeshPros"))
        {
            Selection.objects = stage.FindComponentsOfType<TMPro.TMP_Text>().Select(t => t.gameObject).ToArray();
        }
        if(GUILayout.Button("Select all TextMeshPros w/o LocalizeText"))
        {
            Selection.objects = stage.FindComponentsOfType<TMPro.TMP_Text>().
                Where((t) => { return t.GetComponent<LocalizeText>() == null; }).
                Select(t => t.gameObject).ToArray();
        }
        if(GUILayout.Button("Select null-key LocalizeTexts"))
        {
            Selection.objects =
                Array.FindAll(stage.FindComponentsOfType<LocalizeText>(), (lt) => { return string.IsNullOrWhiteSpace(lt.key); })
                .Select(t => t.gameObject).ToArray();
        }
        GUILayout.Space(5);
        EditorGUILayout.LabelField("Font Tools", EditorStyles.boldLabel);

        // We have switched to using a dynamic atlas for almost all foreign fonts
        // TODO: remove old commented code when we are confident there are no problems, 31/08/2021
        if(GUILayout.Button("Update unique char files for fonts"))
        {
            foreach(var id in new[]{
                Localization.LanguageId.Arabic,
                Localization.LanguageId.ChineseSimplified,
                Localization.LanguageId.ChineseTraditional,
                Localization.LanguageId.Japanese,
                Localization.LanguageId.Korean,
                Localization.LanguageId.Russian
            })
            {
                var uniqueChars = GetUniqueChars(id);
                var path = Application.dataPath + $"/Font/{id}/{id}_chars.txt";
                System.IO.File.WriteAllText(path, uniqueChars);
            }
            Debug.LogWarning("Update any non-dynamic SDFs in Fonts/.");
            AssetDatabase.Refresh();
        }


        // testLanguage won't update unless we call this
        serializedObject.ApplyModifiedProperties();
    }

    string GetLanguageChars(Localization.LanguageId languageId)
    {
        string Chars(int start, int end)
        {
            var str = "";
            for(int n = start; n <= end; n += 1)
                str += Convert.ToChar(n);
            return str;
        }
        return languageId switch
        {
            Localization.LanguageId.Russian => Chars(0x0400, 0x04FF) + Chars(0x0500, 0x052F) + Chars(0x2DE0, 0x2DFF) + Chars(0xA640, 0xA69F) + Chars(0x1C80, 0x1C8F),

            // full char sets for exotic unicode
            // Using dynamic fonts for now

            // Localization.LanguageId.Arabic => Chars(0x0600, 0x06FF) + Chars(0x0750, 0x077F) + Chars(0x08A0, 0x08FF) + Chars(0xFB50, 0xFDFF) + Chars(0xFE70, 0xFEFF),
            //Localization.LanguageId.Japanese => Chars(0x3040, 0x309F) + Chars(0x31F0, 0x31FF) + Chars(0x30A0, 0x30FF) + Chars(0x31F0, 0x31FF),// + Chars(0x4E00, 0x9FCC),
            //Localization.LanguageId.Korean => Chars(0xAC00, 0xD7AF) + Chars(0x1100, 0x11FF) + Chars(0xA960, 0xA97F) + Chars(0xD7B0, 0xD7FF) + Chars(0x3130, 0x318F) + Chars(0xFF00, 0xFFEF) + Chars(0x4E00, 0x9FFF),
            //Localization.LanguageId.ChineseSimplified => Chars(0x2E80, 0x2FD5) + Chars(0x3190, 0x319f) + Chars(0x3400, 0x4DBF) + Chars(0x4E00, 0x9FCC) + Chars(0xF900, 0xFAAD) + Chars(0x4E00, 0x9FFF),

            _ => ""
        };
    }


    string GetUniqueChars(Localization.LanguageId languageId)
    {
        Localization.Init(languageId);
        var allText = string.Join("", Localization.stringTable.Values.ToArray());
        if(languageId == Localization.LanguageId.Arabic)
        {
            foreach(var str in Localization.stringTable.Values.ToArray())
                allText += ArabicSupport.ArabicFixer.Fix(str);
        }
        allText += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890_-�:., ";
        allText += GetLanguageChars(languageId);
        var dict = new Dictionary<char, bool>();
        for(int i = 0; i < allText.Length; i++)
        {
            var s = allText[i];
            switch(s)
            {
                case '\n': continue;
                case '\r': continue;
                case '\t': continue;
                case '\u200f': continue; // right-to-left mark (no glyph)
            }
            dict[s] = true;
        }
        return string.Join("", dict.Keys.OrderBy(c => c).ToArray());
    }

    void AddLocalizedTextToTextMeshPros(TMPro.TMP_Text[] list)
    {
        int added = 0, updated = 0;
        Debug.Log("<color=yellow>Adding LocalizeText...</color>");
        foreach(var t in list)
        {
            // do they already have one?
            var lt = t.GetComponent<LocalizeText>();
            if(lt == null)
            {
                added++;
                lt = t.gameObject.AddComponent<LocalizeText>();
                EditorUtility.SetDirty(t.gameObject);
                if(Localization.keys.Contains(t.text.ToUpper()))
                    lt.key = t.text.ToUpper();
                Debug.Log($"+ {t.name} {lt.key}");
            }
            else if(lt.key.ToUpper() != lt.key)
            {
                updated++;
                lt.key = lt.key.ToUpper();
                Debug.Log($"~ {t.name} {lt.key}");
            }
            else if(string.IsNullOrWhiteSpace(lt.key) && Localization.keys.Contains(t.text.ToUpper()))
            {
                updated++;
                lt.key = t.text.ToUpper();
                Debug.Log($"~ {t.name} {lt.key}");
            }
            else
                Debug.Log($"{t.name} {lt.key}");
        }
        Debug.Log($"<color=cyan>{list.Length} Text Components, {added} added, {updated} updated</color>");
    }
}

