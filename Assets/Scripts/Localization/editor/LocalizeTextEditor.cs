using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(LocalizeText), true)]
public class LocalizeTextEditor : Editor
{
    public SerializedProperty key;

    void OnEnable()
    {
        this.key = this.serializedObject.FindProperty("key");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (Localization.isReady == false)
            Localization.Init();
        
        int currentIndex = Array.IndexOf(Localization.keys, this.key.stringValue);
        int newIndex = EditorGUILayout.Popup("Key", currentIndex, Localization.pagedKeys);
        if (newIndex >= 0)
        {
            this.key.stringValue = Localization.keys[newIndex];
            this.serializedObject.ApplyModifiedProperties();
        }
    }
}
