
using UnityEngine;
using Unity.Profiling;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Map : MonoBehaviour
{
    public static Map instance;
    public static DetailsToPersist detailsToPersist;

    [NonSerialized] public int mapNumber; // 1-based; only >1 in multiplayer games
    [NonSerialized] public int frameNumber;
    [NonSerialized] public Theme theme;
    [NonSerialized] public Background background;
    [NonSerialized] public OutsideTexture outsideTexture;
    [NonSerialized] public List<Chunk> chunks;
    [NonSerialized] public GameCamera gameCamera;
    [NonSerialized] public Player player;
    [NonSerialized] public List<GhostPlayer> ghostPlayers;
    [NonSerialized] public bool timeFrozenByEnemyDeath;
    [NonSerialized] public int fruitCollected;
    [NonSerialized] public int fruitCurrent;
    [NonSerialized] public int coinsCollected;
    [NonSerialized] public int silverCoinsCollected;
    [NonSerialized] public int timerCollected;
    [NonSerialized] public CoinsWhenLastSaved coinsWhenLastSaved;
    [NonSerialized] public int deaths;
    [NonSerialized] public bool finished;
    [NonSerialized] public int finishFrameNumber;
    [NonSerialized] public PowerupPickup.Type currentPowerupType;
    [NonSerialized] public Temperature currentTemperature;
    [NonSerialized] public TemperatureAnimation temperatureAnimation;
    [NonSerialized] public Chunk furthestChunkReached;
    [NonSerialized] public DesertAlertState desertAlertState;
    private Temperature temperatureLastFrame;
    [NonSerialized] public bool isInvincible = false;
    [NonSerialized] public EntranceLift entranceLift;
    [NonSerialized] public Referee referee;
    [NonSerialized] public Transform currentCheckpoint;
    [NonSerialized] public Checkpoint lastCheckpoint;
    [NonSerialized] public float checkpointTimer = 0;
    [NonSerialized] public int checkpointDeaths = 0;
    [NonSerialized] public LineOfSightCone lineOfSightCone;
    private bool isComingFromMinigame;
    public int xMin;
    public int xMax;
    public int yMin;
    public int yMax;
    public Waterline waterline;
    public Lavaline lavaline;

    private Chunk[][] chunksByTy;
    private int initialChunks;
    private ProfilerMarker markerMapAdvance = new ProfilerMarker("Map.Advance");
    private ProfilerMarker markerChunkAdvance = new ProfilerMarker("Chunk.Advance");
    private ProfilerMarker markerPlayerAdvance = new ProfilerMarker("Player.Advance");
    private ProfilerMarker markerCameraAdvance = new ProfilerMarker("GameCamera.Advance");
    private ProfilerMarker markerBackgroundAdvance = new ProfilerMarker("Background.Advance");
    private ProfilerMarker markerOutsideTextureAdvance = new ProfilerMarker("OutsideTexture.Advance");
    private ProfilerMarker markerMapDisableDistantChunks = new ProfilerMarker("Map.DisableDistantChunks");

    public struct CoinsWhenLastSaved
    {
        public int coinsInSaveData;
        public int coinsCollectedInMap;
    }

    // only used by the IceAndFire theme
    public enum Temperature
    {
        None,
        Hot,
        Cold
    }

    public struct DesertAlertState
    {
        public Map map;
        public const int DefaultAlarmDurationInFrames = 599;
        public const float MaxAlarmDistance = 20;
        public int alertTimerInFrames;
        public Vector2 alertInitialPosition;
        public bool IsGloballyActive()
        {
            return alertTimerInFrames > 0; 
        }
        public bool IsActive(Vector2 enemyPosition) 
        { 
            if ((enemyPosition - alertInitialPosition).sqrMagnitude > MaxAlarmDistance * MaxAlarmDistance)
                return false;

            return alertTimerInFrames > 0; 
        }
        public void DecreaseAlertTimer() 
        { 
            if (alertTimerInFrames > 0) 
                alertTimerInFrames--; 
        }
        public void ActivateAlert(Vector2 alertPosition, int durationInFrames = DefaultAlarmDurationInFrames) 
        {
            if(!this.map.player.alive)  return;
            
            alertInitialPosition = alertPosition; 
            alertTimerInFrames = durationInFrames;
            IngameUI.instance.ActivateOrRestartGlobalAlertCountdown(this.map);
        }
        public void ResetAlert() { alertTimerInFrames = 0; }
        public void SetAlertTimer(int durationInFrames = DefaultAlarmDurationInFrames)
        {
            alertTimerInFrames = durationInFrames;
        }
    }

    public class DetailsToPersist
    {
        public DateTime returnDate;
        public int returnChunkIndex;
        public Vector2 playerCheckpointPosition;
        public XY bonusLiftPositionInMap;
        public bool forceNoEntranceLift;
        public int coins;
        public int silverCoins;
        public int prizeCoins;
        public int fruitCollected;
        public int fruitCurrent;
        public int timerCollected;
        public PowerupPickup.Type minigamePrizePowerup;
        public PowerupPickup.Type slottedPowerup;
        public PowerupPickup.Type[] activePowerups = new PowerupPickup.Type[0];
        public int yellowPetHearts;
        public int tornadoPetHearts;
        public int shields;
        public int swordShields;
    }

    public int Layer() => this.mapNumber;

    public void NotifyAssetsLoaded(int mapNumber, Theme theme, LevelGeneration.Row[] rows)
    {
        this.mapNumber = mapNumber;
        this.theme = theme;
        this.desertAlertState.map = this;

        this.coinsWhenLastSaved.coinsInSaveData = SaveData.GetCoins();

        this.chunks = new List<Chunk>();
        XY nextEntry = new XY(8, 0);
        int nextCheckpointNumber = 15;

        for (var n = 0; n < rows.Length; n += 1)
        {
            var row = rows[n];

            var obj = new GameObject();
            obj.transform.parent = this.transform;

            var chunkName = row.chunk.filename ?? "no name";
            chunkName = chunkName.Replace("Technical/", "");
 
            var chunk = obj.AddComponent<Chunk>();
            chunk.InitEssentials(
                this,
                n,
                chunkName,
                row.chunk.level,
                row.type,
                row.flipHorizontally,
                row.fillAreaOfInterestWithCheckpoint,
                nextEntry,
                nextCheckpointNumber
            );
            this.chunks.Add(chunk);

            if (chunk.checkpointNumber != null)
                nextCheckpointNumber -= 1;

            nextEntry = chunk.exit.mapLocation;
            if (chunk.exit.type == Chunk.MarkerType.Wall)
            {
                if (chunk.isFlippedHorizontally == true)
                    nextEntry.x -= 1;
                else
                    nextEntry.x += 1;
            }
            else
            {
                nextEntry.y += 1;
            }
        }

        {
            var lastChunkToInitSynchronously = 2;
            var attempt = SaveData.GetLatestAttemptForDate(Game.selectedDate);
            if (attempt.checkpointNumber != 0)
            {
                foreach (var chunk in this.chunks)
                {
                    if (chunk.checkpointNumber == attempt.checkpointNumber)
                        lastChunkToInitSynchronously = chunk.index + 3;
                }
            }

            if (lastChunkToInitSynchronously >= this.chunks.Count)
                lastChunkToInitSynchronously = this.chunks.Count - 1;

            // Debug.Log($"On start, init chunks #1 -> #{lastChunkToInitSynchronously+1}");
            for (int n = 0; n <= lastChunkToInitSynchronously; n += 1)
            {
                this.chunks[n].InitDetails(
                    rows[n].fillAreaOfInterestWithCheckpoint,
                    rows[n].fillAreaOfInterestWithBonusLift
                );
            }
        }

        this.xMin = this.chunks.Select(c => c.xMin).Min();
        this.xMax = this.chunks.Select(c => c.xMax).Max();
        this.yMin = this.chunks.Select(c => c.yMin).Min();
        this.yMax = this.chunks.Select(c => c.yMax).Max();

        FindChunksByTy();

        if (this.theme == Theme.HotNColdSprings)
        {
            this.currentTemperature = Temperature.Cold;
            this.temperatureLastFrame = Temperature.Cold;
        }
        else if (this.theme == Theme.ConflictCanyon)
        {
            this.lineOfSightCone = new LineOfSightCone();
        }

        foreach (var chunk in this.chunks)
        {
            if (chunk.hasFinishedInitDetails == false) continue;

            chunk.InitItems();
        }

        foreach (var chunk in this.chunks)
        {
            if (chunk.hasFinishedInitDetails == false ||
                chunk.Next()?.hasFinishedInitDetails == false)
            {
                continue;
            }

            chunk.InitAutotile();
            chunk.InitSurfaceDecoration();
        }

        if (this.theme == Theme.HotNColdSprings)
        {
            this.temperatureAnimation = new TemperatureAnimation(
                this, this.currentTemperature
            );
        }

        if (this.player == null)
            Debug.LogError("No player in level!");
        this.player.transform.SetAsLastSibling();

        {
            Background bg = this.theme switch
            {
                Theme.RainyRuins      => Assets.instance.rainyRuins.background,
                Theme.CapitalHighway  => Assets.instance.capitalHighway.background,
                Theme.ConflictCanyon  => Assets.instance.conflictCanyon.background,
                Theme.HotNColdSprings => Assets.instance.hotNColdSprings.background,
                Theme.GravityGalaxy   => Assets.instance.gravityGalaxy.background,
                Theme.SunkenIsland    => Assets.instance.sunkenIsland.background,
                Theme.TreasureMines   => Assets.instance.treasureMines.background,
                Theme.WindySkies      => Assets.instance.windySkies.background,
                Theme.TombstoneHill   => Assets.instance.tombstoneHill.background,
                Theme.MoltenFortress  => Assets.instance.moltenFortress.background,
                Theme.OpeningTutorial => Assets.instance.openingTutorial.background,
                Theme.VRTrainingRoom  => Assets.instance.vrTrainingRoom.background,
                _                     => null
            };
            if (bg != null)
                this.background = Instantiate(bg, this.transform);
        }

        this.outsideTexture = OutsideTexture.Make(this, this.chunks[0].TileTheme());

        ResumeFromSavedData();
        SaveNewRecordIfBetter(null);

        this.isComingFromMinigame = false;
        if (detailsToPersist != null)
        {
            var details = detailsToPersist;
            if (details.returnDate.Date == Game.selectedDate)
            {
                var chunk = this.chunks[details.returnChunkIndex];
                var lift = chunk.NearestItemTo<BonusLift>(new Vector2
                (
                    details.bonusLiftPositionInMap.x,
                    details.bonusLiftPositionInMap.y
                ));
                if (lift != null)
                {
                    this.player.position = lift.position;
                    lift.ComeBackFromMinigame();
                    this.isComingFromMinigame = true;
                    lift.detailsToPersist = detailsToPersist;
                }
                else if (details.forceNoEntranceLift == true)
                {
                    this.player.position = details.playerCheckpointPosition;
                    this.isComingFromMinigame = true;
                }

                this.player.checkpointPosition = details.playerCheckpointPosition;
                this.coinsCollected            = details.coins;
                this.silverCoinsCollected      = details.silverCoins;
                this.fruitCollected            = details.fruitCollected;
                this.fruitCurrent              = details.fruitCurrent;
                this.timerCollected            = details.timerCollected;

                this.coinsWhenLastSaved = new CoinsWhenLastSaved
                {
                    coinsInSaveData = SaveData.GetCoins(),
                    coinsCollectedInMap = details.coins
                };

                if(details.slottedPowerup != PowerupPickup.Type.None)
                {
                    CollectPowerup(details.slottedPowerup);
                }

                foreach (var activePowerup in details.activePowerups)
                {
                    switch (activePowerup)
                    {
                        case PowerupPickup.Type.PetYellow:
                            StartCoroutine(this.player.petYellow.DelayedActivation());
                            break;

                        case PowerupPickup.Type.PetTornado:
                            StartCoroutine(this.player.petTornado.DelayedActivation());
                            break;

                        case PowerupPickup.Type.Shield:
                            this.player.shield.Activate();
                            break;

                        case PowerupPickup.Type.InfiniteJump:
                            this.player.infiniteJump.Activate();
                            break;

                        case PowerupPickup.Type.MidasTouch:
                            this.player.midasTouch.Activate();
                            break;

                        case PowerupPickup.Type.SwordAndShield:
                            StartCoroutine(this.player.swordAndShield.DelayedActivation());
                            break;
                    }
                }
            }
            detailsToPersist = null;
        }
        
        if (this.theme == Theme.SunkenIsland)
        {
            this.waterline = new Waterline(this);
        }
        else if(this.theme == Theme.MoltenFortress)
        {
            this.lavaline = new Lavaline(this);
        }

        this.gameCamera = FindObjectOfType<GameCamera>();
        if (this != Game.instance.map1)
        {
            var before = this.gameCamera;
            this.gameCamera = Instantiate(this.gameCamera);
            Destroy(this.gameCamera.GetComponent<AudioListener>());
            this.gameCamera.transform.SetSiblingIndex(
                before.transform.GetSiblingIndex() + 1
            );
        }
        this.gameCamera.Init(this);
        this.background?.Init(this);
        if (this.mapNumber == 1)
            IngameUI.instance.Init();

        EntranceLiftSetup();

        Util.SetLayerRecursively(this.transform, Layer(), true);

        Advance(true);
        if (this.mapNumber != 1)
            this.frameNumber = Game.instance.map1.frameNumber;

        StartMusic();

        initialChunks = chunks.Count - 1;

        StartCoroutine(FinishLoadingRemainingChunks(rows));
    }

    private IEnumerator FinishLoadingRemainingChunks(LevelGeneration.Row[] rows)
    {
        yield return null;

        /* Do autotiling stuff one chunk behind other work, so we won't accidentally
           create incorrect top-edges because the chunk above isn't loaded yet... */

        for (int n = 0; n < this.chunks.Count; n += 1)
        {
            if (this.chunks[n].hasFinishedInitItems == true)
                continue;

            float TimeToSpendLoadingPerFrame()
            {
                if (n <= 0) return 1.0f;
                bool isChunkRequiredVerySoon =
                    this.gameCamera.VisibleRectangle().yMax >=
                    this.chunks[n - 1].yMin - 10;
                return isChunkRequiredVerySoon ? 1.0f : 0.001f;
            }

            var coroutine = this.chunks[n].InitDetailsCoroutine(
                rows[n].fillAreaOfInterestWithCheckpoint,
                rows[n].fillAreaOfInterestWithBonusLift
            );
            while (Util.CoroutineRunForTime(coroutine, TimeToSpendLoadingPerFrame()))
                yield return null;

            coroutine = this.chunks[n].InitItemsCoroutine();
            while (Util.CoroutineRunForTime(coroutine, TimeToSpendLoadingPerFrame()))
                yield return null;

            coroutine = this.chunks[n - 1].InitAutotileCoroutine();
            while (Util.CoroutineRunForTime(coroutine, TimeToSpendLoadingPerFrame()))
                yield return null;

            coroutine = this.chunks[n - 1].InitSurfaceDecorationCoroutine();
            while (Util.CoroutineRunForTime(coroutine, TimeToSpendLoadingPerFrame()))
                yield return null;

            Util.SetLayerRecursively(this.chunks[n - 1].transform, Layer(), true);
        }

        {
            int n = this.chunks.Count - 1;

            var coroutine = this.chunks[n].InitAutotileCoroutine();
            while (Util.CoroutineRunForTime(coroutine, 0.001f))
                yield return null;

            coroutine = this.chunks[n].InitSurfaceDecorationCoroutine();
            while (Util.CoroutineRunForTime(coroutine, 0.001f))
                yield return null;

            Util.SetLayerRecursively(this.chunks[n].transform, Layer(), true);
        }
    }

    NitromeEditor.Level LoadChunkByName(string chunkName)
    {
        if (chunkName.ToLowerInvariant().EndsWith(".json"))
        {
            chunkName = chunkName.Substring(0, chunkName.Length - 5);
            var r = Resources.Load<TextAsset>("Levels/" + chunkName);
            if (r == null)
                Debug.LogError($"Failed to load chunk {chunkName} (json)!");
            return NitromeEditor.Level.UnserializeFromJson(r.text);
        }
        else
        {
            var r = Resources.Load<TextAsset>("Levels/" + chunkName);
            if (r == null)
                Debug.LogError($"Failed to load chunk {chunkName} (bytes)!");
            return NitromeEditor.Level.UnserializeFromBytes(r.bytes);
        }
    }

    public void CreateGhostPlayers()
    {
        if (this.ghostPlayers != null)
        {
            foreach (var ghostPlayer in this.ghostPlayers)
                Destroy(ghostPlayer.gameObject);
        }

        this.ghostPlayers = new List<GhostPlayer>();

        foreach (var otherMap in Game.instance.maps)
        {
            if (otherMap == this) continue;

            this.ghostPlayers.Add(
                GhostPlayer.Make(this, otherMap.player)
            );
        }
    }

    public void StartMusic(bool isLoadingScreenClosing = false)
    {
        if (isLoadingScreenClosing == true || FindObjectOfType<LoadingScreen>() == null)
        {
            if (this.theme == Theme.HotNColdSprings)
            {
                var assets = Assets.instance.hotNColdSprings;
                if (currentTemperature == Temperature.Cold)
                    Audio.instance.PlayMusicWithVariations(assets.musicCold, assets.musicHot);
                else
                    Audio.instance.PlayMusicWithVariations(assets.musicHot, assets.musicCold);
            }
            else
            {
                Audio.instance.PlayMusic(MusicForNormalGameplay(), stopAllSfx: false);
            }
        }
    }

    private void FindChunksByTy()
    {
        this.chunksByTy = new Chunk[this.yMax + 1][];

        for (int ty = 0; ty <= this.yMax; ty += 1)
        {
            var chunksHere = new List<Chunk>();
            foreach (var chunk in this.chunks)
            {
                if (chunk.yMax < ty && ty > 0) continue;
                if (chunk.yMin > ty) continue;
                chunksHere.Add(chunk);
            }
            this.chunksByTy[ty] = chunksHere.ToArray();
        }
    }

    public void ResetItems()
    {
        StartCoroutine("ResetCoroutine");
    }

    private IEnumerator ResetCoroutine()
    {
        /* The current logic of this, is that Enemies will be killed and respawned,
           so if they die, just Destroy() them, it doesn't matter.
           But other Items will remain unchanged on death, so if you Destroy them they'll
           stay gone. If you need them to come back later, then use trickery to hide them
           and show them back again when Item.Reset() is called.
           This is inconsistent, but if I end up pooling items then it doesn't matter
           as it's all going to change anyway. */

        var checkpointChunk = NearestChunkTo(this.player.checkpointPosition);
        var chunksToReset = this.chunks.Where(
            c =>
                c.index >= checkpointChunk.index &&
                c.index <= this.furthestChunkReached.index
        ).ToArray();

        foreach (var c in chunksToReset)
            c.KillThingsPreparingForReset();

        if(this.theme == Theme.SunkenIsland)
        {
            this.waterline.ResetToCheckpoint();
        }
        else if(this.theme == Theme.MoltenFortress)
        {
            this.lavaline.ResetToCheckpoint();
        }

        yield return new WaitForSeconds(1.0f);

        foreach (var c in chunksToReset)
            c.ResetAndRecreateItems();
    }

    public void Advance(bool isLastAdvanceInFrame = true)
    {
        this.markerMapAdvance.Begin();

        this.frameNumber += 1;
        FPSCounter.timerAdvance.Start();

        DisableDistantChunks();

        this.temperatureAnimation?.Advance();
        NearestChunkTo(this.player.position).AdvanceItemsBySubsteps();
        FPSCounter.timerItems.Start();
        foreach (var chunk in this.chunks)
        {
            if (chunk.HasLoaded == true && chunk.gameObject.activeSelf == true)
            {
                this.markerChunkAdvance.Begin();
                chunk.AdvanceItems();
                this.markerChunkAdvance.End();
            }
        }
        FPSCounter.timerItems.Stop();

        FPSCounter.timerPlayer.Start();
        this.markerPlayerAdvance.Begin();
        this.player.Advance();
        this.markerPlayerAdvance.End();
        FPSCounter.timerPlayer.Stop();

        this.markerCameraAdvance.Begin();
        this.gameCamera.Advance();
        this.markerCameraAdvance.End();

        if (isLastAdvanceInFrame == true)
        {
            this.markerBackgroundAdvance.Begin();
            this.background?.Advance();
            this.markerBackgroundAdvance.End();

            this.markerOutsideTextureAdvance.Begin();
            this.outsideTexture.Advance();
            this.markerOutsideTextureAdvance.End();
        }

        DisableDistantChunks();

        TileGrid.MoveFoliageWithPlayer(this);

        Chunk playerChunk = NearestChunkTo(this.player.position);
        if(this.furthestChunkReached == null ||
            playerChunk.index > this.furthestChunkReached.index)
        {
            this.furthestChunkReached = playerChunk;
        }
        desertAlertState.DecreaseAlertTimer();
        FPSCounter.timerAdvance.Stop();

        // var secs = Time.realtimeSinceStartup - time;
        // var ms = Mathf.Round(secs * 100000) / 100;
        // var field = IngameUI.instance.debugText;
        // field.text = ms + " ms";

        // Raycast.TestRaycast();

        if (DebugPanel.hitboxesEnabled == true)
            DebugDrawing.Draw(this, false, true);
        else
            DebugDrawing.Clear();

        switch(this.theme)
        {
            case Theme.HotNColdSprings:
            {
                if (currentTemperature != temperatureLastFrame && !this.isInvincible)
                {
                    ChangeHotnColdMusic();
                }
            }
            break;

            case Theme.SunkenIsland:
            {
                this.waterline.Advance();
            }
            break;

            case Theme.MoltenFortress:
            {
                this.lavaline.Advance();
            }
            break;
        }
        InvinciblityMusic();
        temperatureLastFrame = currentTemperature;
        checkpointTimer++;

        // #if UNITY_EDITOR
        // {
        //     if (this.frameNumber % 60 == 0)
        //         CheckForThingsOnIncorrectLayer();
        // }
        // #endif

        this.markerMapAdvance.End();
    }

    private void CheckForThingsOnIncorrectLayer()
    {
        var intendedLayer = Layer();

        void Check(Transform parent)
        {
            var chunk = parent.GetComponent<Chunk>();
            if (chunk != null && chunk.HasLoaded == false)
                return;

            if (parent.gameObject.layer != intendedLayer)
            {
                var path = parent.FullPath();
                Debug.LogWarning(
                    $"Incorrect layer: {path} " +
                    $"({parent.gameObject.layer} instead of {intendedLayer})"
                );
                return;
            }

            foreach (Transform child in parent)
            {
                Check(child);
            }
        }

        Check(this.transform);
    }

    public void ChangeHotnColdMusic()
    {
        if (currentTemperature == Temperature.Hot)
        {
            StartCoroutine(
                Audio.instance.SwitchToVariationCoroutine(
                    Assets.instance.hotNColdSprings.musicCold,
                    Assets.instance.hotNColdSprings.musicHot,
                    1
                )
            );

        }
        else
        {
            StartCoroutine(
                Audio.instance.SwitchToVariationCoroutine(
                    Assets.instance.hotNColdSprings.musicHot,
                    Assets.instance.hotNColdSprings.musicCold,
                    1
                )
            );
        }
    }

    AudioClip MusicForNormalGameplay() => this.theme switch
    {
        Theme.RainyRuins      => Assets.instance.rainyRuins.music,
        Theme.CapitalHighway  => Assets.instance.capitalHighway.music,
        Theme.ConflictCanyon  => Assets.instance.conflictCanyon.music,
        Theme.HotNColdSprings =>
            this.currentTemperature == Temperature.Cold ?
                Assets.instance.hotNColdSprings.musicCold :
                Assets.instance.hotNColdSprings.musicHot,
        Theme.OpeningTutorial => Assets.instance.openingTutorial.music,
        Theme.GravityGalaxy   => Assets.instance.gravityGalaxy.music,
        Theme.SunkenIsland    => Assets.instance.sunkenIsland.music,
        Theme.TreasureMines   => Assets.instance.treasureMines.music,
        Theme.WindySkies      => Assets.instance.windySkies.music,
        Theme.TombstoneHill   => Assets.instance.tombstoneHill.music,
        Theme.MoltenFortress  => Assets.instance.moltenFortress.music,
        _                     => Assets.instance.rainyRuins?.music
    };

    public void InvinciblityMusic()
    {
        if (this.player.invincibility.isActive && !this.isInvincible)
        {
            this.isInvincible = true;
            if(this.player.invincibility.activatedByReferee == false)
            {
                Audio.instance.PlaySfx(Assets.instance.musicInvincible, position: null, options: new Audio.Options(1, false, 0));
            }
            else
            {
                Audio.instance.PlaySfx(Assets.instance.musicInvincibleExtended, position: null, options: new Audio.Options(1, false, 0));
            }
            AudioClip music = MusicForNormalGameplay();
            StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 1, audioClip: music));
        }
        else if (!this.player.invincibility.isActive && this.isInvincible)
        {
            this.isInvincible = false;
            Audio.instance.StopSfx(Assets.instance.musicInvincible);
            Audio.instance.StopSfx(Assets.instance.musicInvincibleExtended);
            AudioClip music = MusicForNormalGameplay();
            StartCoroutine(Audio.instance.FadeMusic(setTo: .5f, duration: 1, startFrom: 0, audioClip: music));
        }
    }

    public void DecreaseMusicVolume()
    {
        if(this.isInvincible == false)
        {
            AudioClip music = MusicForNormalGameplay();
            StartCoroutine(Audio.instance.FadeMusic(setTo: 0.25f, duration: 0.1f, audioClip: music));
        }
        else
        {
            StartCoroutine(Audio.instance.FadeMusic(setTo: 0.25f, duration: 0.1f, audioClip: Assets.instance.musicInvincible));
        }
    }

    public void IncreaseMusicVolume()
    {
        if (this.isInvincible == false)
        {
            AudioClip music = MusicForNormalGameplay();
            StartCoroutine(Audio.instance.FadeMusic(setTo: 1f, duration: 1f, audioClip: music));
        }
        else
        {
            if(this.player.invincibility.activatedByReferee == false)
                StartCoroutine(Audio.instance.FadeMusic(setTo: 1f, duration: 1f, audioClip: Assets.instance.musicInvincible));
            else
                StartCoroutine(Audio.instance.FadeMusic(setTo: 1f, duration: 1f, audioClip: Assets.instance.musicInvincibleExtended));
        }
    }

    public void PlayWinMusic()
    {
        if (this.isInvincible == false)
        {
            AudioClip music = MusicForNormalGameplay();
            StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 0.3f, audioClip: music));
        }
        else
        {
            if (this.player.invincibility.activatedByReferee == false)
                StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 0.3f, audioClip: Assets.instance.musicInvincible));
            else
                StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 0.3f, audioClip: Assets.instance.musicInvincibleExtended));
        }

        Audio.instance.PlayMusic(Assets.instance.musicWinMusicLoop);
    }

    public Item[] AllItems() =>
        this.chunks.SelectMany(c => c.items).ToArray();

    public Tile TileAt(int tx, int ty, Layer layer) =>
        ChunkAt(tx, ty)?.TileAt(tx, ty, layer);

    public NitromeEditor.TileLayer.Tile? TriggerAt(int tx, int ty) =>
        ChunkAt(tx, ty)?.TriggerAt(tx, ty);

    public Item ItemAt(int tx, int ty) =>
        ChunkAt(tx, ty)?.ItemAt(tx, ty);

    public T ItemAt<T>(int tx, int ty) where T : Item =>
        ChunkAt(tx, ty)?.ItemAt<T>(tx, ty);

    public Portal PortalAt(int tx, int ty) =>
        ChunkAt(tx, ty)?.PortalAt(tx, ty);

    public T AnyItem<T>() where T : Item
    {
        foreach (var chunk in this.chunks)
        {
            var result = chunk.AnyItem<T>();
            if (result != null)
                return result;
        }
        return null;
    }

    public Chunk[] ChunksAtTy(int ty)
    {
        if (ty < 0) 
            return this.chunksByTy[0];
        else if (ty > this.yMax)
            return this.chunksByTy[this.yMax];
        else
            return this.chunksByTy[ty];
    }

    public Chunk ChunkAt(int tx, int ty)
    {
        Chunk[] candidates;
        if (ty < 0)
            candidates = this.chunksByTy[0];
        else if (ty > this.yMax)
            candidates = this.chunksByTy[this.yMax];
        else
            candidates = this.chunksByTy[ty];

        foreach (var chunk in candidates)
        {
            if (tx < chunk.xMin || tx > chunk.xMax) continue;
            if (ty < chunk.yMin || ty > chunk.yMax) continue;
            return chunk;
        }
        return null;
    }

    public bool IsSolidAtPoint(Vector2 pt)
    {
        var tx = Mathf.FloorToInt(pt.x / Tile.Size);
        var ty = Mathf.FloorToInt(pt.y / Tile.Size);
        var t = TileAt(tx, ty, global::Layer.A);
        if (t == null) return false;

        var tilePx = pt.x - (t.mapTx * Tile.Size);
        var tilePy = pt.y - (t.mapTy * Tile.Size);
        return t.IsSolidAtPoint(tilePx, tilePy);
    }

    public Chunk NearestChunkTo(
        Vector2 position,
        float maxDistance = float.PositiveInfinity
    )
    {
        Chunk result = ChunkAt(
            Mathf.FloorToInt(position.x),
            Mathf.FloorToInt(position.y)
        );
        if (result != null) return result;

        float resultSqDistance = maxDistance * maxDistance;

        foreach (var chunk in this.chunks)
        {
            Vector2 nearest = position;
            nearest.x = Mathf.Clamp(nearest.x, chunk.xMin, chunk.xMax + 1);
            nearest.y = Mathf.Clamp(nearest.y, chunk.yMin, chunk.yMax + 1);

            float sqDistance = (position - nearest).sqrMagnitude;
            if (sqDistance < resultSqDistance)
            {
                result = chunk;
                resultSqDistance = sqDistance;
            }
        }

        return result;
    }

    public IEnumerable<Chunk> ChunksWithinDistance(Vector2 position, float maxDistance)
    {
        foreach (var chunk in chunks)
        {
            Vector2 nearest;
            nearest.x = Mathf.Clamp(position.x, chunk.xMin, chunk.xMax + 1);
            nearest.y = Mathf.Clamp(position.y, chunk.yMin, chunk.yMax + 1);

            Vector2 delta = position - nearest;
            if (delta.sqrMagnitude <= maxDistance * maxDistance)
                yield return chunk;
        }
        yield break;
    }

    public void DelayedCall(float time, Action action)
    {
        StartCoroutine(DelayedCallImpl(time, action));
    }

    private IEnumerator DelayedCallImpl(float time, Action action)
    {
        yield return new WaitForSeconds(time);
        action();
    }

    public float ManhattanDistanceCameraToPoint(Vector2 pt)
    {
        var cameraPosition = this.gameCamera.transform.position;
        var cameraHalfwidth = Camera.main.orthographicSize * Camera.main.aspect;
        var cameraHalfheight = Camera.main.orthographicSize;

        var closestOnScreen = new Vector2(
            Mathf.Clamp(
                pt.x,
                cameraPosition.x - cameraHalfwidth,
                cameraPosition.x + cameraHalfwidth
            ),
            Mathf.Clamp(
                pt.y,
                cameraPosition.y - cameraHalfheight,
                cameraPosition.y + cameraHalfheight
            )
        );

        return Mathf.Abs(closestOnScreen.x - pt.x) + Mathf.Abs(closestOnScreen.y - pt.y);
    }

    public void ScreenShakeAtPosition(Vector2 position, Vector2 vector)
    {
        float dist = ManhattanDistanceCameraToPoint(position);
        if (dist == 0)
        {
            this.gameCamera.ScreenShake(vector);
        }
        else if (dist < 8)
        {
            var multiplier = 1 - (dist / 8);
            this.gameCamera.ScreenShake(vector * multiplier);
        }
    }

    private void DisableDistantChunks()
    {
        this.markerMapDisableDistantChunks.Begin();

        var cameraRect = this.gameCamera.VisibleRectangle();
        var tolerance = 20;

        for (var n = 0; n < this.chunks.Count; n += 1)
        {
            var chunk = this.chunks[n];
            
            var active = true;
            if (chunk.yMax * Tile.Size < cameraRect.yMin - tolerance) active = false;
            if (chunk.yMin * Tile.Size > cameraRect.yMax + tolerance) active = false;
            if (chunk.xMax * Tile.Size < cameraRect.xMin - tolerance) active = false;
            if (chunk.xMin * Tile.Size > cameraRect.xMax + tolerance) active = false;
            if (chunk.timeToRemainActive > 0) active = true;
            if (chunk.HasLoaded == false && chunk.isTomorrowOnSuperLeapDay == true)
                active = true;

            if (chunk.gameObject.activeSelf != active)
                chunk.gameObject.SetActive(active);
        }

        this.markerMapDisableDistantChunks.End();
    }

    public void DamageEnemy(Rect rect, Enemy.KillInfo killInfo)
    {
        foreach (var chunk in this.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (chunk.HasLoaded == false) continue;

            foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
            {
                if (enemy == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < enemy.hitboxes.Length; hi += 1)
                {
                    var rectEnemy = enemy.hitboxes[hi].InWorldSpace();

                    if (rectEnemy.Overlaps(rect))
                        enemy.Hit(killInfo);
                }
            }

            // Now that we need the BreakableBlock affecting a non-enemy Item,
            // the name of this method doesnt make much sense anymore
            // TODO: Implement a smarter way of setting which items will
            // get affected by which other items (collision matrix?)
            foreach (var item in chunk.subsetOfItemsOfTypeTrafficCone)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Toss();
                }
            }
        }
    }

    public void Damage(Rect rect, Enemy.KillInfo killInfo, bool affectPlayer = false)
    {
        if (affectPlayer == true && this.player.Rectangle().Overlaps(rect))
        {
            this.player.Hit(killInfo.itemDeliveringKill.position);
        }

        foreach (var chunk in this.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (chunk.HasLoaded == false) continue;

            foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
            {
                if (enemy == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < enemy.hitboxes.Length; hi += 1)
                {
                    var rectEnemy = enemy.hitboxes[hi].InWorldSpace();

                    if (rectEnemy.Overlaps(rect))
                        enemy.Hit(killInfo);
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeTNTBlock)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Break();
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeBreakableBlock)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Break(false);
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeRandomBox)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Break();
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeChest)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                    {
                        if ((item as Chest).isLocked == false)
                        {
                            item.Open();
                        }
                    }
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeTrafficCone)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Toss();
                }
            }

            foreach (var item in chunk.subsetOfItemsOfTypeSnowBlock)
            {
                if (item == killInfo.itemDeliveringKill) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Melt(immediate: true);
                }
            }
        }
    }

    public void Melt(Rect rect, Item melterItem)
    {
        foreach (var chunk in this.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (chunk.HasLoaded == false) continue;

            foreach (var item in chunk.subsetOfItemsOfTypeSnowBlock)
            {
                if (item == melterItem) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Melt(immediate: true);
                }
            }
        }
    }

    public void Disintegrate(Rect rect, Item disintegraterItem)
    {
        foreach (var chunk in this.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (chunk.HasLoaded == false) continue;

            foreach (var item in chunk.subsetOfItemsOfTypeAshBlock)
            {
                if (item == disintegraterItem) continue;

                for (var hi = 0; hi < item.hitboxes.Length; hi += 1)
                {
                    var rectItem = item.hitboxes[hi].InWorldSpace();

                    if (rectItem.Overlaps(rect))
                        item.Disintegrate();
                }
            }
        }
    }

    public void BreakBreakableItems(Rect rect)
    {
        foreach (var chunk in this.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (chunk.HasLoaded == false) continue;

            foreach (var item in chunk.subsetOfItemsOfTypeTNTBlock)
            {
                if (item.hitboxes[0].InWorldSpace().Overlaps(rect))
                    item.Break();
            }

            foreach (var item in chunk.subsetOfItemsOfTypeBreakableBlock)
            {
                if (item.hitboxes[0].InWorldSpace().Overlaps(rect))
                    item.Break(false);
            }

            foreach (var item in chunk.subsetOfItemsOfTypeRandomBox)
            {
                if (item.hitboxes[0].InWorldSpace().Overlaps(rect))
                    item.Break();
            }
        }
    }

    public Pickup.LuckyDrop RandomlySelectLuckyPickup()
    {
        Pickup.LuckyDrop selectedPickup = Pickup.DefaultDrop;

        // powerup heart
        if(CanGivePowerupHeart() == true && UnityEngine.Random.value < 0.2f)
        {
            selectedPickup = Pickup.LuckyDrop.PowerupHeart;
        }

        // rare drop
        if (UnityEngine.Random.value < 0.1f)
        {
            selectedPickup = Pickup.GetRandomRareDrop();
        }

        if(SaveData.IsInfiniteJumpPurchased() == true && SaveData.RecievedInfiniteJump() == false)
        {
            SaveData.SetRecievedInfiniteJump(true);
            selectedPickup = Pickup.LuckyDrop.PowerupInfiniteJump;
        }
        else if (SaveData.IsSwordAndShieldPurchased() == true && SaveData.RecievedSwordAndShield() == false)
        {
            SaveData.SetRecievedSwordAndShield(true);
            selectedPickup = Pickup.LuckyDrop.PowerupSwordAndShield;
        }
        else if (SaveData.IsMidasTouchPurchased() == true && SaveData.RecievedMidasTouch() == false)
        {
            SaveData.SetRecievedMidasTouch(true);
            selectedPickup = Pickup.LuckyDrop.PowerupMidasTouch;
        }

        return selectedPickup;
    }

    public void DropLuckyPickup(Pickup.LuckyDrop pickupDrop, Vector2 position, Chunk chunk)
    {
        Pickup pickup = null;

        switch (pickupDrop) 
        {
            case Pickup.LuckyDrop.SilverCoin:
                pickup = Coin.CreateSilver(position, chunk);
                break;

            case Pickup.LuckyDrop.GoldCoin:
                pickup = Coin.Create(position, chunk);
                break;

            case Pickup.LuckyDrop.PowerupHeart:
                pickup = PowerupHeart.Create(position, chunk);
                break;

            case Pickup.LuckyDrop.PowerupInvincibility:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.Invincibility);
                break;

            case Pickup.LuckyDrop.PowerupShield:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.Shield);
                break;

            case Pickup.LuckyDrop.PowerupYellowPet:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.PetYellow);
                break;

            case Pickup.LuckyDrop.PowerupTornadoPet:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.PetTornado);
                break;

            case Pickup.LuckyDrop.PowerupInfiniteJump:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.InfiniteJump);
                break;

            case Pickup.LuckyDrop.PowerupMidasTouch:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MidasTouch);
                break;

            case Pickup.LuckyDrop.PowerupSwordAndShield:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.SwordAndShield);
                break;

            case Pickup.LuckyDrop.PowerupMultiplayerBaseball:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MultiplayerBaseball);
                break;

            case Pickup.LuckyDrop.PowerupMultiplayerInk:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MultiplayerInk);
                break;

            case Pickup.LuckyDrop.PowerupMultiplayerMines:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MultiplayerMines);
                break;

            case Pickup.LuckyDrop.PowerupMultiplayerMinesTap:
                pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MultiplayerMinesTap);
                break;

            case Pickup.LuckyDrop.BubblePowerupInvincibility:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleInvincibility);
                break;

            case Pickup.LuckyDrop.BubblePowerupShield:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleShield);
                break;

            case Pickup.LuckyDrop.BubblePowerupYellowPet:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubblePetYellow);
                break;

            case Pickup.LuckyDrop.BubblePowerupTornadoPet:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubblePetTornado);
                break;

            case Pickup.LuckyDrop.BubblePowerupInfiniteJump:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleInfiniteJump);
                break;

            case Pickup.LuckyDrop.BubblePowerupMidasTouch:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleMidasTouch);
                break;

            case Pickup.LuckyDrop.BubblePowerupSwordAndShield:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleSwordAndShield);
                break;

            case Pickup.LuckyDrop.BubblePowerupMultiplayerBaseball:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleMultiplayerBaseball);
                break;

            case Pickup.LuckyDrop.BubblePowerupMultiplayerInk:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleMultiplayerInk);
                break;

            case Pickup.LuckyDrop.BubblePowerupMultiplayerMines:
                pickup = BubblePickup.Create(position, chunk, PowerupPickup.Type.BubbleMultiplayerMines);
                break;

            case Pickup.LuckyDrop.Key:
                pickup = GateKey.Create(position, chunk);
                break;
        }

        pickup.SpawnLuckyPickup();
    }

    public void RandomlyDropLuckyPickup(Vector2 position, Chunk chunk)
    {
        Pickup pickup = null;

        if (SaveData.IsInfiniteJumpPurchased() == true && SaveData.RecievedInfiniteJump() == false)
        {
            SaveData.SetRecievedInfiniteJump(true);
            pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.InfiniteJump);
        }
        else if (SaveData.IsSwordAndShieldPurchased() == true && SaveData.RecievedSwordAndShield() == false)
        {
            SaveData.SetRecievedSwordAndShield(true);
            pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.SwordAndShield);
        }
        else if (SaveData.IsMidasTouchPurchased() == true && SaveData.RecievedMidasTouch() == false)
        {
            SaveData.SetRecievedMidasTouch(true);
            pickup = PowerupPickup.Create(position, chunk, PowerupPickup.Type.MidasTouch);
        }
        else if (UnityEngine.Random.value > 0.1f)
        {
            if(CanGivePowerupHeart() == true && UnityEngine.Random.value < 0.2f)
            {
                pickup = PowerupHeart.Create(position, chunk);
            }
            else
            {
                pickup = Coin.CreateSilver(position, chunk);
            }
        }
        else
        {
            switch (UnityEngine.Random.Range(0, 3))
            {
                case 0: pickup = Coin.Create(position, chunk); break;
                case 1: pickup = PowerupPickup.CreateRandomPowerup(position, chunk); break;
                case 2: pickup = BubblePickup.CreateRandomPowerup(position, chunk); break;
                //case 3:  pickup = GateKey.Create(position, chunk);                break;
            }
        }

        pickup.SpawnLuckyPickup();
    }

    public bool CanGivePowerupHeart()
    {
        if (player.petTornado.needsHeart == false && 
            player.petYellow.needsHeart == false &&
            player.infiniteJump.needsHeart == false &&
            player.midasTouch.needsHeart == false)
        {
            return false;
        }

        if(player.infiniteJump.needsHeart == true &&
            player.infiniteJump.activatedByReferee == true)
        {
            return false;
        }

        return true;
    }

    public void AddFruit(int fruitAdded)
    {
        this.fruitCollected += fruitAdded;
        this.fruitCurrent += fruitAdded;
    }

    public void RemoveFruit(int fruitRemoved)
    {
        this.fruitCurrent -= fruitRemoved;

        if(this.fruitCurrent < 0)
        {
            this.fruitCurrent = 0;
        }
    }

    public void AddSilverCoin()
    {
        this.silverCoinsCollected += 1;
        CheckForSilverCoinEffect();
    }

    public void CheckForSilverCoinEffect()
    {
        var silverCoinIndicator =
            IngameUI.instance.CurrentLayout().Player(this).silverCoins;

        if (this.silverCoinsCollected >= 10 &&
            silverCoinIndicator.IsPlayingSilverCoinEffect == false)
        {
            this.silverCoinsCollected -= 10;
            StartCoroutine(silverCoinIndicator.SilverCoinEffect(this));
        }
    }

    public void CollectPowerup(PowerupPickup.Type powerupType)
    {
        this.currentPowerupType = powerupType;

        switch(this.currentPowerupType)
        {
            case PowerupPickup.Type.MultiplayerMinesTap:
                this.player.minesTap.Collect(numberOfMines: 5);
                break;
        }
    }

    public void TriggerPowerup()
    {
        if(this.player.alive == false) return;

        switch(this.currentPowerupType)
        {
            case PowerupPickup.Type.Invincibility:
                this.player.invincibility.Activate();
                break;

            case PowerupPickup.Type.PetYellow:
                this.player.petYellow.Activate();
                break;

            case PowerupPickup.Type.PetTornado:
                this.player.petTornado.Activate();
                break;

            case PowerupPickup.Type.Shield:
                this.player.shield.Activate();
                break;

            case PowerupPickup.Type.InfiniteJump:
                this.player.infiniteJump.Activate();
                break;

            case PowerupPickup.Type.MidasTouch:
                this.player.midasTouch.Activate();
                break;

            case PowerupPickup.Type.SwordAndShield:
                this.player.swordAndShield.Activate();
                break;

            case PowerupPickup.Type.MultiplayerBaseball:
                this.player.baseball.Activate();
                break;

            case PowerupPickup.Type.MultiplayerInk:
                this.player.ink.Activate();
                break;

            case PowerupPickup.Type.MultiplayerMines:
                this.player.mines.Activate(numberOfMines: 5);
                break;

            case PowerupPickup.Type.MultiplayerMinesTap:
                this.player.minesTap.Activate();
                break;
        }

        if (this.currentPowerupType != PowerupPickup.Type.None)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxPowerupActivate);
        }

        switch (this.currentPowerupType)
        {
            default:
            {
                this.currentPowerupType = PowerupPickup.Type.None;
            }
            break;

            case PowerupPickup.Type.MultiplayerMinesTap:
            {
                if(this.player.minesTap.HasMinesLeft == false)
                {
                    this.currentPowerupType = PowerupPickup.Type.None;
                }
            }
            break;
        }

    }

    public void ChangeTemperature(Temperature newTemperature)
    {
        if (newTemperature == this.currentTemperature)
            return;

        Temperature oldTemperature = this.currentTemperature;
        this.currentTemperature = newTemperature;
        this.temperatureAnimation?.Go(oldTemperature, newTemperature);
    }

    private void ResumeFromSavedData()
    {
        if (this.theme == Theme.OpeningTutorial)
            return;

        var attempt = SaveData.GetLatestAttemptForDate(Game.selectedDate);
        if (attempt.checkpointNumber == 0)
        {
            return;
        }

        this.deaths = attempt.deaths;
        this.frameNumber = Mathf.RoundToInt(attempt.timeInSeconds * 60);

        Chunk chunk = this.chunks.Where(
            c => c.checkpointNumber == attempt.checkpointNumber
        ).FirstOrDefault();
        if (chunk == null) return;

        var checkpoint = chunk.items.OfType<Checkpoint>().FirstOrDefault();
        if (checkpoint != null)
        {
            player.position = player.checkpointPosition = checkpoint.position.Add(0, 1);
        }

        var trophy = chunk.items.OfType<Trophy>().FirstOrDefault();
        if (trophy != null)
        {
            player.position = player.checkpointPosition = trophy.position.Add(0, 1);
        }

        this.fruitCollected       = attempt.fruitCollected;
        this.fruitCurrent         = attempt.fruitCurrent;
        this.coinsCollected       = attempt.coins;
        this.silverCoinsCollected = attempt.silverCoins;
        this.timerCollected       = attempt.timerCollected;

        this.coinsWhenLastSaved = new CoinsWhenLastSaved
        {
            coinsCollectedInMap = this.coinsCollected,
            coinsInSaveData = SaveData.GetCoins()
        };
    }

    public void SaveCoins()
    {
        int coinsCollectedBeforeLevel =
            this.coinsWhenLastSaved.coinsInSaveData -
            this.coinsWhenLastSaved.coinsCollectedInMap;

        this.coinsWhenLastSaved = new CoinsWhenLastSaved
        {
            coinsCollectedInMap = this.coinsCollected,
            coinsInSaveData = coinsCollectedBeforeLevel + this.coinsCollected
        };

        SaveData.SetCoins(this.coinsWhenLastSaved.coinsInSaveData);
    }

    private void EntranceLiftSetup()
    {
        if (this.isComingFromMinigame == true)
            return;

        if (this.mapNumber > 1)
            return;

        var attempt = SaveData.GetLatestAttemptForDate(Game.selectedDate);
        if (attempt.checkpointNumber == 0 || this.theme == Theme.OpeningTutorial)
        {
            entranceLift = EntranceLift.Create(player.position - new Vector2(0, 0.9375f), NearestChunkTo(this.player.position));
            entranceLift.RefreshTargetPosition();
            referee = Referee.Create(NearestChunkTo(this.player.position));
            referee.ComeIn();
            return;
        }

        Chunk chunk = this.chunks.Where(
            c => c.checkpointNumber == attempt.checkpointNumber
        ).FirstOrDefault();
        if (chunk == null) return;

        var checkpoint = chunk.items.OfType<Checkpoint>().FirstOrDefault();
        if (checkpoint != null)
        {
            checkpoint.Activate(true);
            if(this.theme != Theme.OpeningTutorial)
            {
                entranceLift = EntranceLift.Create(player.position - new Vector2(0, 0.9375f), NearestChunkTo(this.player.position));
                entranceLift.checkpoint = checkpoint;
                entranceLift.transform.position = entranceLift.position = checkpoint.position.Add(0, 1) + new Vector2(0, -0.9375f);
                entranceLift.RefreshTargetPosition();
            }
            else
            {
                entranceLift = EntranceLift.Create(player.position - new Vector2(0, 0.9375f), NearestChunkTo(this.player.position));
                entranceLift.checkpoint = checkpoint;
                entranceLift.transform.position = entranceLift.position = checkpoint.position.Add(0, 1) + new Vector2(0, -0.9375f);
                player.position = checkpoint.position.Add(0, 1);
                entranceLift.RefreshTargetPosition();
            }
            
            this.currentCheckpoint = checkpoint.transform;
        }

        var trophy = chunk.items.OfType<Trophy>().FirstOrDefault();
        if (trophy != null)
        { 
            trophy.inEntranceLift = true;
            entranceLift = EntranceLift.Create(player.position - new Vector2(0, 0.9375f), NearestChunkTo(this.player.position));
            entranceLift.trophy = trophy;
            entranceLift.atTrophy = true;
            entranceLift.transform.position = entranceLift.position = trophy.position.Add(0, 1) + new Vector2(0, -0.9375f);
            entranceLift.RefreshTargetPosition();

            trophy.hasCreatedRefree = true;
            referee = Referee.Create(NearestChunkTo(this.player.position));
            referee.MoveArrow();

            this.currentCheckpoint = trophy.transform;
        }
    }

    public void CheckBubbleSpawn()
    {
        if(((this.checkpointTimer/ 60 > 180 && this.checkpointDeaths > 5) || this.checkpointTimer / 60 > 600) && this.currentCheckpoint != null)
        {
            this.referee = Referee.Create(NearestChunkTo(this.player.position));
            this.referee.MoveBubble();
        }
    }

    public bool ShouldAwardFruitCup()
    {
        return ((float)this.fruitCollected / TotalFruitAtStart()) >= 0.8f;
    }

    public int TotalFruitAtStart()
    {
        int result = 0;
        foreach (var chunk in this.chunks)
        {
            result += chunk.totalFruitOnStart;
        }
        return result;
    }

    public void SaveDataOfAttempt(int? setCheckpointNumberReached)
    {
        if (setCheckpointNumberReached == 0)
        {
            SaveData.ClearLatestAttemptForDate(Game.selectedDate);
        }
        else
        {
            var attempt = SaveData.GetLatestAttemptForDate(Game.selectedDate);

            if (setCheckpointNumberReached.HasValue == true)
                attempt.checkpointNumber       = setCheckpointNumberReached.Value;

            attempt.timeInSeconds  = this.frameNumber / 60f;
            attempt.deaths         = this.deaths;
            attempt.fruitCollected = this.fruitCollected;
            attempt.fruitCurrent   = this.fruitCurrent;
            attempt.coins          = this.coinsCollected;
            attempt.silverCoins    = this.silverCoinsCollected;
            attempt.timerCollected = this.timerCollected;

            SaveData.SetLatestAttemptForDate(Game.selectedDate, attempt);
        }
    }

    public void SaveNewRecordIfBetter(Trophy.TrophyType? trophyType)
    {
        var trophyAwarded = trophyType switch
        {
            Trophy.TrophyType.BRONZE => SaveData.Trophy.Bronze,
            Trophy.TrophyType.SILVER => SaveData.Trophy.Silver,
            Trophy.TrophyType.GOLD   => SaveData.Trophy.Gold,
            _                        => SaveData.Trophy.None
        };
        if (trophyType == Trophy.TrophyType.GOLD && ShouldAwardFruitCup() == true)
        {
            trophyAwarded = SaveData.Trophy.Fruit;
        }

        var oldRecord = SaveData.GetRecordForDate(Game.selectedDate);
        var newRecord = new SaveData.RecordForDate
        {
            hasPlayedAtAll = true,
            trophy         = trophyAwarded,
            timeInSeconds  = this.frameNumber / 60f,
            deaths         = this.deaths
        };
        var record = SaveData.RecordForDate.Combine(oldRecord, newRecord);
        if (record.hasPlayedAtAll != oldRecord.hasPlayedAtAll ||
            record.trophy         != oldRecord.trophy         ||
            record.timeInSeconds  != oldRecord.timeInSeconds  ||
            record.deaths         != oldRecord.deaths)
        {
            SaveData.SetRecordForDate(Game.selectedDate, record);
        }
    }

    public Chunk NextCheckpointFromPosition(Vector2 position)
    {
        Chunk currentChunk = NearestChunkTo(position);

        Chunk checkpointChunk = this.chunks.Where(
            c => c.checkpointNumber != null && c.index > currentChunk.index
        ).FirstOrDefault();

        return checkpointChunk;
    }

    public Chunk LastCheckpointFromPosition(Vector2 position)
    {
        Chunk currentChunk = NearestChunkTo(position);

        Chunk checkpointChunk = this.chunks.Where(
            c => c.checkpointNumber != null && c.index < currentChunk.index
        ).FirstOrDefault();

        return checkpointChunk;
    }

    public void InsertChunkBeforeEnding(Chunk chunkToInsert)
    {
        /*var obj = new GameObject();
        obj.transform.parent = this.transform;
        int insertIndex = this.chunks.Count - 1;

        var chunkName = chunkToInsert.name ?? "no name";
        chunkName = chunkName.Replace("Technical/", "");

        XY nextEntry = this.chunks[insertIndex - 1].exit.mapLocation;
        if (this.chunks[insertIndex - 1].exit.type == Chunk.MarkerType.Wall)
        {
            if (this.chunks[insertIndex - 1].isFlippedHorizontally == true)
                nextEntry.x -= 1;
            else
                nextEntry.x += 1;
        }
        else
        {
            nextEntry.y += 1;
        }

        var chunk = obj.AddComponent<Chunk>();
        chunk.Init(
            this,
            insertIndex,
            chunkName,
            chunkToInsert.levelData,
            false,
            false,
            false,
            nextEntry,
            1
        );

        XY updatedEntry = chunk.exit.mapLocation;
        if (chunk.exit.type == Chunk.MarkerType.Wall)
        {
            if (chunk.isFlippedHorizontally == true)
                updatedEntry.x -= 1;
            else
                updatedEntry.x += 1;
        }
        else
        {
            updatedEntry.y += 1;
        }

        this.chunks[insertIndex].index = this.chunks.Count;
        this.chunks[insertIndex].UpdateEndingChunk(updatedEntry);

        this.xMin = this.chunks.Select(c => c.xMin).Min();
        this.xMax = this.chunks.Select(c => c.xMax).Max();
        this.yMin = this.chunks.Select(c => c.yMin).Min();
        this.yMax = this.chunks.Select(c => c.yMax).Max();

        FindChunksByTy();
        this.chunks.Insert(insertIndex, chunk);
        chunk.InitItems();
        chunk.InitAutotile();*/
    }

    public void RemoveInsertedChunks()
    {
        XY entry = this.chunks[initialChunks - 1].exit.mapLocation;
        if (this.chunks[initialChunks - 1].exit.type == Chunk.MarkerType.Wall)
        {
            if (this.chunks[initialChunks - 1].isFlippedHorizontally == true)
                entry.x -= 1;
            else
                entry.x += 1;
        }
        else
        {
            entry.y += 1;
        }
        this.chunks[chunks.Count - 1].UpdateEndingChunk(entry);

        for (int i = this.initialChunks; i < this.chunks.Count-1; i++)
        {
            this.chunks[i].KillThingsPreparingForReset();
            this.chunks.Remove(chunks[i]);
        }
        this.chunks[chunks.Count - 1].index = this.initialChunks;
    }
}
