using System.Linq;
using UnityEngine;

public static class Multiplayer
{
    public enum Style
    {
        None,
        SplitScreen,
        GameKit
    }

    public static bool IsMultiplayerGame()
    {
        return Game.instance?.maps?.Length > 1 || GameCenterMultiplayer.IsActive == true;
    }

    public static Style StyleOfMultiplayerGame()
    {
        if (Game.instance.maps.Length > 1)
            return Style.SplitScreen;
        else if (GameCenterMultiplayer.IsActive == true)
            return Style.GameKit;
        else
            return Style.None;
    }

    public static Style StyleOnThisPlatform()
    {
        #if UNITY_IOS
            return Style.GameKit;
        #else
            return Style.SplitScreen;
        #endif
    }

    public static bool IsMultiplayerSplitScreenGame()
    {
        return (IsMultiplayerGame() && StyleOfMultiplayerGame() == Style.SplitScreen);
    } 

    public static bool IsPlayerWinning(Player player)
    {
        bool result = false;

        // if any player has finished, then this function should return true if
        // the player in question is the one who reached the gold trophy first.

        if (StyleOfMultiplayerGame() == Style.GameKit)
        {
            if (GameCenterMultiplayer.ThisPlayerReachedTheGoldTrophyFirst == true)
                return true;
            if (GameCenterMultiplayer.HasAnyRemotePlayerReachedTheGoldTrophy() == true)
                return false;
        }
        else
        {
            int quickestTime = int.MaxValue;
            bool anyFinished = false;

            foreach (var map in Game.instance.maps)
            {
                if (map.finished == false) continue;
                if (map.finishFrameNumber >= quickestTime) continue;
                quickestTime = map.finishFrameNumber;
                anyFinished = true;
                result = (player == map.player);
            }

            if (anyFinished == true)
                return result;
        }

        // if nobody's finished yet, then this function should return true if
        // the player in question is the highest in the level at the moment.

        var bestProgress = float.NegativeInfinity;

        foreach (var map in Game.instance.maps)
        {
            var progress = ProgressOfPlayer(map, map.player.position);
            if (progress >= bestProgress)
            {
                bestProgress = progress;
                result = (map.player == player);
            }
        }

        if (StyleOfMultiplayerGame() == Style.GameKit)
        {
            foreach (var ghost in Game.instance.map1.ghostPlayers)
            {
                var progress = ProgressOfPlayer(
                    Game.instance.map1, ghost.transform.position
                );
                if (progress > bestProgress)
                    return false;
            }
        }

        return result;
    }

    private static float ProgressOfPlayer(Map map, Vector2 playerPosition)
    {
        Chunk chunk = map.NearestChunkTo(playerPosition);
        float progress = chunk.index;

        if (chunk.isHorizontal == true && chunk.isFlippedHorizontally == false)
            progress += (playerPosition.x - chunk.xMin) / (chunk.xMax + 1 - chunk.xMin);
        else if (chunk.isHorizontal == true && chunk.isFlippedHorizontally == true)
            progress += (playerPosition.x - (chunk.xMax + 1)) / (chunk.xMin - chunk.xMax);
        else
            progress += (playerPosition.y - chunk.yMin) / (chunk.yMax + 1 - chunk.yMin);

        return progress;
    }

    public static Map BestTargetForWeaponInSplitScreenGame(Map excludingThisMap)
    {
        Map bestMap = null;
        var bestProgress = 0f;

        foreach (var map in Game.instance.maps)
        {
            if (map == excludingThisMap)
                continue;

            var progress = ProgressOfPlayer(map, map.player.position);
            if (progress <= bestProgress) continue;

            bestMap = map;
            bestProgress = progress;
        }

        return bestMap;
    }
}
