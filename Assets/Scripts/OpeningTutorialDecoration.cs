using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpeningTutorialDecoration : Item
{
    public bool changeSign = false;

    public Sprite signTouch;
    public Sprite signGamePad;
    public Sprite signKeyboard;
    public Sprite signRemote;
    public override void Init(Def def)
    {
        base.Init(def);

        if(this.changeSign == true)
        {
            if(GameInput.mostRecentInputType == GameInput.InputType.TouchScreen)
            {
                this.GetComponent<SpriteRenderer>().sprite = signTouch;
            }
            else if(
                GameInput.mostRecentInputType == GameInput.InputType.GamepadXbox ||
                GameInput.mostRecentInputType == GameInput.InputType.GamepadPlaystation ||
                GameInput.mostRecentInputType == GameInput.InputType.GamepadMfi
            )
            {
                this.GetComponent<SpriteRenderer>().sprite = signGamePad;
            }
            else if (
                GameInput.mostRecentInputType == GameInput.InputType.Keyboard ||
                GameInput.mostRecentInputType == GameInput.InputType.Mouse
            )
            {
                this.GetComponent<SpriteRenderer>().sprite = signKeyboard;
            }
            else if (GameInput.mostRecentInputType == GameInput.InputType.SiriRemote)
            {
                this.GetComponent<SpriteRenderer>().sprite = signRemote;
            }
        }
    }
}
