using System;
using System.Linq;
using UnityEngine;
using MarkerType = Chunk.MarkerType;

public class OutsideBoundaryForGravityGalaxy : MonoBehaviour
{
    private struct LimitsOfEdgesInVerticalSection
    {
        public int bottomLeft, bottomRight, topLeft, topRight;
    }

    private enum Corner { BL, BR, TL, TR }

    [Serializable]
    public class Decoration
    {
        public Sprite sprite;
        public int reachLeft;
        public int reachRight;
    }

    [NonSerialized] public TileGridMesh tileGridMesh;

    public Sprite spriteTile;
    public Sprite spriteTileJoinLeft;
    public Sprite spriteTileJoinRight;
    public Sprite spriteTileSpacer;
    public Sprite[] spritesTileWithDamage;
    public Sprite[] spritesTileWithTech;
    public Sprite spriteTileToSpacerLeft;
    public Sprite spriteTileToSpacerRight;

    public Sprite spriteOuterCornerBL;
    public Sprite spriteOuterCornerBR;
    public Sprite spriteOuterCornerTL;
    public Sprite spriteOuterCornerTR;
    public Sprite spriteInnerCornerBL;
    public Sprite spriteInnerCornerBR;
    public Sprite spriteInnerCornerTL;
    public Sprite spriteInnerCornerTR;
    public Sprite spriteAirlockJoinHorizontalBL;
    public Sprite spriteAirlockJoinHorizontalBR;
    public Sprite spriteAirlockJoinHorizontalTL;
    public Sprite spriteAirlockJoinHorizontalTR;
    public Sprite spriteAirlockJoinVerticalBL;
    public Sprite spriteAirlockJoinVerticalBR;
    public Sprite spriteAirlockJoinVerticalTL;
    public Sprite spriteAirlockJoinVerticalTR;

    public Sprite spriteTexture;

    public Decoration[] decoration;

    private Chunk chunk;
    private Item dummyItemForHitboxes;
    private System.Random rnd;

    public void Init(Chunk chunk)
    {
        if (chunk.initialGravityDirection == null) return;

        this.chunk = chunk;
        this.rnd = new System.Random(Game.selectedDate.DayOfYear);
        this.gameObject.name = "Outside Boundary for Gravity Galaxy";

        this.tileGridMesh = this.gameObject.AddComponent<TileGridMesh>();
        this.transform.position = new Vector2(chunk.xMin, chunk.yMin);
        var entry = chunk.entry.type;
        var exit = chunk.exit.type;

        if (chunk.isAirlock == true && chunk.isHorizontal == true)
        {
            var (bottom, top) = HorizontalTopAndBottom(chunk);

            var isLeavingSpace = chunk.Next().initialGravityDirection == null;
            var isFlipped = chunk.isFlippedHorizontally;

            if ((isFlipped, isLeavingSpace) is (false, true) or (true, false))
            {
                // airlock on left hand side of horizontal space section
                AddHCorner(
                    chunk.xMin, bottom - 3, this.spriteAirlockJoinVerticalTR, Corner.TR
                );
                AddHCorner(
                    chunk.xMin, top, this.spriteAirlockJoinVerticalBR, Corner.BR
                );
                AddPeg(chunk.xMax + 2, bottom - 1f, 270, false);
                AddPeg(chunk.xMax + 2, top + 2f, 270, true);
            }
            else
            {
                // airlock on right hand side of horizontal space section
                AddHCorner(
                    chunk.xMin - 1, bottom - 3, this.spriteAirlockJoinVerticalBL, Corner.TL
                );
                AddHCorner(
                    chunk.xMin - 1, top, this.spriteAirlockJoinVerticalTL, Corner.BL
                );
                AddPeg(chunk.xMin - 1, bottom - 1f, 90, true);
                AddPeg(chunk.xMin - 1, top + 2f, 90, false);
            }

            var airlock = chunk.AnyItem<AirlockAndCannon>();
            airlock.ExtendUpwards(top - 1);
            airlock.ExtendDownwards(bottom + 1);
        }
        else if (
            chunk.isAirlock == true && 
            chunk.Next().initialGravityDirection == null
        )
        {
            // airlock in vertical section heading into space
            AddVCorner(
                chunk.xMin - 3, chunk.yMin, this.spriteAirlockJoinHorizontalTR, Corner.TR
            );
            AddVCorner(
                chunk.xMax, chunk.yMin, this.spriteAirlockJoinHorizontalTL, Corner.TL
            );
            AddPeg(chunk.xMin - 1, chunk.yMax + 2, 0, true);
            AddPeg(chunk.xMax + 2, chunk.yMax + 2, 0, false);
        }
        else if (chunk.isAirlock == true)
        {
            // airlock in vertical section leaving space
            AddVCorner(
                chunk.xMin - 3, chunk.yMin - 1, this.spriteAirlockJoinHorizontalBR, Corner.BR
            );
            AddVCorner(
                chunk.xMax, chunk.yMin - 1, this.spriteAirlockJoinHorizontalBL, Corner.BL
            );
            AddPeg(chunk.xMin - 1, chunk.yMin - 1, 180, false);
            AddPeg(chunk.xMax + 2, chunk.yMin - 1, 180, true);
        }
        else if (entry != MarkerType.Wall && exit == MarkerType.Wall)
        {
            // vertical to horizontal
            var (_, top) = HorizontalTopAndBottom(chunk);

            if (chunk.isFlippedHorizontally == false)
            {
                AddLeft(chunk.xMin - 1, chunk.yMin, top);
                AddCorner(chunk.xMin - 3, top + 1, this.spriteOuterCornerTL);
            }
            else
            {
                AddRight(chunk.xMax + 1, chunk.yMin, top);
                AddCorner(chunk.xMax + 1, top + 1, this.spriteOuterCornerTR);
            }

            AddTop(chunk.xMin, chunk.xMax, top + 1);
            FillUnusedSpaceAboveHorizontalChunk(chunk, top);
        }
        else if (entry == MarkerType.Wall && exit != MarkerType.Wall)
        {
            // horizontal to vertical
            var (bottom, _) = HorizontalTopAndBottom(chunk);

            if (chunk.isFlippedHorizontally == false)
            {
                AddRight(chunk.xMax + 1, bottom, chunk.yMax);
                AddCorner(chunk.xMax + 1, bottom - 3, this.spriteOuterCornerBR);
            }
            else
            {
                AddLeft(chunk.xMin - 1, bottom, chunk.yMax);
                AddCorner(chunk.xMin - 3, bottom - 3, this.spriteOuterCornerBL);
            }

            AddBottom(chunk.xMin, chunk.xMax, bottom - 1);
            FillUnusedSpaceBelowHorizontalChunk(chunk, bottom);
        }
        else if (chunk.isHorizontal == false)
        {
            var limits = FindLimitsOfEdgesInVerticalSection(chunk);

            if (chunk.Next()?.isMultiplayerSideRoom == true)
            {
                limits.topRight -= 3;
                AddLeft(chunk.xMin - 1, chunk.yMax - 2, chunk.yMax);
                AddCorner(chunk.xMax + 1, chunk.yMax - 2, this.spriteInnerCornerTR);
                AddBottom(chunk.xMax + 4, chunk.Next().xMax, chunk.yMax);
                AddCorner(chunk.Next().xMax + 1, chunk.yMax - 2, this.spriteOuterCornerBR);
            }
            else if (chunk.Previous()?.isMultiplayerSideRoom == true)
            {
                limits.bottomRight += 3;
                AddLeft(chunk.xMin - 1, chunk.yMin, chunk.yMin + 2);
                AddCorner(chunk.xMax + 1, chunk.yMin, this.spriteInnerCornerBL);
                AddTop(chunk.xMax + 4, chunk.Previous().xMax, chunk.yMin);
                AddCorner(chunk.Previous().xMax + 1, chunk.yMin, this.spriteOuterCornerTR);
            }

            if (limits.bottomLeft < limits.bottomRight)
                AddLeft(chunk.xMin - 1, limits.bottomLeft, limits.bottomRight - 1);
            else if (limits.bottomLeft > limits.bottomRight)
                AddRight(chunk.xMax + 1, limits.bottomRight, limits.bottomLeft - 1);

            AddLeftAndRightTogether(
                chunk.xMin - 1, chunk.xMax + 1,
                Mathf.Max(limits.bottomLeft, limits.bottomRight),
                Mathf.Min(limits.topLeft, limits.topRight)
            );

            if (limits.topLeft > limits.topRight)
                AddLeft(chunk.xMin - 1, limits.topRight + 1, limits.topLeft);
            else if (limits.topLeft < limits.topRight)
                AddRight(chunk.xMax + 1, limits.topLeft + 1, limits.topRight);

            if (chunk.index == 0)
            {
                AddCorner(chunk.xMin - 3, chunk.yMin - 3, this.spriteOuterCornerBL);
                AddCorner(chunk.xMax + 1, chunk.yMin - 3, this.spriteOuterCornerBR);
                AddBottom(chunk.xMin, chunk.xMax, chunk.yMin - 1);
            }
        }
        else
        {
            var (bottom, top) = HorizontalTopAndBottom(chunk);

            var (bl, br) = (chunk.xMin, chunk.xMax);
            var (tl, tr) = (chunk.xMin, chunk.xMax);

            if (chunk.Previous().entry.type != MarkerType.Wall)
            {
                // horizontal run just started
                if (chunk.isFlippedHorizontally == false)
                {
                    bl += 3;
                    AddCorner(chunk.xMin, bottom - 3, this.spriteInnerCornerTR);
                }
                else
                {
                    br -= 3;
                    AddCorner(chunk.xMax - 2, bottom - 3, this.spriteInnerCornerTL);
                }
            }

            if (chunk.Next().exit.type != MarkerType.Wall)
            {
                // horizontal run is about to end
                if (chunk.isFlippedHorizontally == false)
                {
                    tr -= 3;
                    AddCorner(chunk.xMax - 2, top + 1, this.spriteInnerCornerBR);
                }
                else
                {
                    tl += 3;
                    AddCorner(chunk.xMin, top + 1, this.spriteInnerCornerBL);
                }
            }

            if (tl < bl)
                AddTop(tl, bl - 1, top + 1);
            else if (tl > bl)
                AddBottom(bl, tl - 1, bottom - 1);

            AddTopAndBottomTogether(
                Mathf.Max(tl, bl), Mathf.Min(tr, br), bottom - 1, top + 1
            );

            if (tr > br)
                AddTop(br + 1, tr, top + 1);
            else if (tr < br)
                AddBottom(tr + 1, br, bottom - 1);

            FillUnusedSpaceAboveHorizontalChunk(chunk, top);
            FillUnusedSpaceBelowHorizontalChunk(chunk, bottom);
        }

        this.tileGridMesh.FinishCreating(this.spriteTile.texture, "Tiles - Solid");
    }

    private void AddPeg(float x, float y, int rotation, bool right)
    {
        var go = Instantiate(
            Assets.instance.gravityGalaxy.airlockPeg, this.chunk.transform
        );
        var peg = go.GetComponent<AirlockPeg>();

        var tx = Mathf.FloorToInt(x);
        var ty = Mathf.FloorToInt(y);
        var tile = new NitromeEditor.TileLayer.Tile
        {
            offsetX = x - tx,
            offsetY = y - ty,
            rotation = rotation
        };
        peg.Init(new Item.Def(tx, ty, tile, this.chunk));
        peg.SetDirection(right);
        this.chunk.items.Add(peg);
    }

    private void AddLeft(int mapTx, int mapTyBottom, int mapTyTop)
    {
        AddStraightLine(
            new Vector2Int(mapTx, mapTyBottom), Vector2Int.up, 1 + mapTyTop - mapTyBottom
        );
        AddHitbox(mapTx - 2, mapTyBottom, 3, 1 + mapTyTop - mapTyBottom);
    }

    private void AddRight(int mapTx, int mapTyBottom, int mapTyTop)
    {
        AddStraightLine(
            new Vector2Int(mapTx, mapTyTop), Vector2Int.down, 1 + mapTyTop - mapTyBottom
        );
        AddHitbox(mapTx, mapTyBottom, 3, 1 + mapTyTop - mapTyBottom);
    }

    private void AddTop(int mapTxLeft, int mapTxRight, int mapTy)
    {
        AddStraightLine(
            new Vector2Int(mapTxLeft, mapTy), Vector2Int.right, 1 + mapTxRight - mapTxLeft
        );
        AddHitbox(mapTxLeft, mapTy, 1 + mapTxRight - mapTxLeft, 3);
    }

    private void AddBottom(int mapTxLeft, int mapTxRight, int mapTy)
    {
        AddStraightLine(
            new Vector2Int(mapTxRight, mapTy), Vector2Int.left, 1 + mapTxRight - mapTxLeft
        );
        AddHitbox(mapTxLeft, mapTy - 2, 1 + mapTxRight - mapTxLeft, 3);
    }

    private void AddLeftAndRightTogether(
        int mapTxLeft, int mapTxRight, int mapTyBottom, int mapTyTop
    )
    {
        AddStraightLine(
            new Vector2Int(mapTxLeft, mapTyBottom),
            Vector2Int.up,
            1 + mapTyTop - mapTyBottom,
            (new Vector2Int(mapTxRight, mapTyBottom), true, false)
        );
        AddHitbox(mapTxLeft - 2, mapTyBottom, 3, 1 + mapTyTop - mapTyBottom);
        AddHitbox(mapTxRight,    mapTyBottom, 3, 1 + mapTyTop - mapTyBottom);
    }

    private void AddTopAndBottomTogether(
        int mapTxLeft, int mapTxRight, int mapTyBottom, int mapTyTop
    )
    {
        AddStraightLine(
            new Vector2Int(mapTxLeft, mapTyTop),
            Vector2Int.right,
            1 + mapTxRight - mapTxLeft,
            (new Vector2Int(mapTxLeft, mapTyBottom), false, true)
        );
        AddHitbox(mapTxLeft, mapTyTop, 1 + mapTxRight - mapTxLeft, 3);
        AddHitbox(mapTxLeft, mapTyBottom - 2, 1 + mapTxRight - mapTxLeft, 3);
    }

    private void AddStraightLine(
        Vector2Int start, Vector2Int direction, int length,
        (Vector2Int otherStart, bool flipHorizontal, bool flipVertical)? mirror = null
    )
    {
        int rotation = (direction.x, direction.y) switch
        {
            ( 0,  1) => 90,
            (-1,  0) => 180,
            ( 0, -1) => 270,
            _        => 0
        };
        var rotationMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, rotation));

        var mirrorMatrix = Matrix4x4.identity;
        if (mirror?.flipHorizontal == true)
        {
            var centerX = Mathf.Lerp(
                start.x + 0.5f, mirror.Value.otherStart.x + 0.5f, 0.5f
            ) - this.chunk.xMin;
            mirrorMatrix =
                Matrix4x4.Translate(new Vector3(centerX, 0, 0)) *
                Matrix4x4.Scale(new Vector3(-1, 1, 1)) *
                Matrix4x4.Translate(new Vector3(-centerX, 0, 0));
        }
        if (mirror?.flipVertical == true)
        {
            var centerY = Mathf.Lerp(
                start.y + 0.5f, mirror.Value.otherStart.y + 0.5f, 0.5f
            ) - this.chunk.yMin;
            mirrorMatrix =
                Matrix4x4.Translate(new Vector3(0, centerY, 0)) *
                Matrix4x4.Scale(new Vector3(1, -1, 1)) *
                Matrix4x4.Translate(new Vector3(0, -centerY, 0));
        }

        if (rotation == 90) start.x += 1;
        if (rotation == 180) start += Vector2Int.one;
        if (rotation == 270) start.y += 1;
        start.x -= this.chunk.xMin;
        start.y -= this.chunk.yMin;

        T RandomChoice<T>(System.Random rnd, T[] items) =>
            items[rnd.Next() % items.Length];

        bool inSpacer = false;
        int distanceInSpacer = 0;
        int startOfNormal = 0;

        for (int n = 0; n < length; n += 1)
        {
            var sprite = this.spriteTile;
            if (inSpacer == false)
            {
                var r = this.rnd.NextDouble();
                if (r > 0.9f && n < length - 20)
                {
                    sprite = this.spriteTileToSpacerRight;
                    inSpacer = true;
                    distanceInSpacer = 0;
                    Decorate(startOfNormal, n - startOfNormal);
                }
                else
                {
                    if (r > 0.9f)
                        sprite = RandomChoice(this.rnd, this.spritesTileWithDamage);
                    else if (r > 0.8f)
                        sprite = RandomChoice(this.rnd, this.spritesTileWithTech);
                }
            }
            else
            {
                distanceInSpacer += 1;

                var r = this.rnd.NextDouble();
                if ((distanceInSpacer > 6 && r > 0.9f) || n > length - 4)
                {
                    sprite = this.spriteTileToSpacerLeft;
                    inSpacer = false;
                    startOfNormal = n + 3;
                }
                else
                {
                    sprite = this.spriteTileSpacer;
                }
            }

            if (n == length - 1 && sprite == this.spritesTileWithTech[0])
                sprite = this.spriteTile;

            var position = (Vector2)(start + (direction * n));
            this.tileGridMesh.AddSprite(
                Matrix4x4.Translate(position) * rotationMatrix, sprite, Color.white
            );

            if (mirror != null)
            {
                this.tileGridMesh.AddSprite(
                    mirrorMatrix * Matrix4x4.Translate(position) * rotationMatrix,
                    sprite, Color.white
                );
            }

            if (sprite == this.spritesTileWithTech[0])
                n += 1;
            else if (sprite == this.spriteTileToSpacerLeft)
                n += 2;
            else if (sprite == this.spriteTileToSpacerRight)
                n += 2;
        }

        Decorate(startOfNormal, length - startOfNormal);

        void Decorate(int startAlong, int distanceAlong)
        {
            var startPoint = rotation switch
            {
                90  => ((Vector2)start).Add(-3, 0),
                180 => ((Vector2)start).Add(0, -3),
                270 => ((Vector2)start).Add(3, 0),
                _   => ((Vector2)start).Add(0, 3)
            };
            startPoint += (startAlong * direction);
            DecorateAlongLine(
                startPoint, direction, distanceAlong, rotation, mirrorMatrix
            );
        }
    }

    private void DecorateAlongLine(
        Vector2 start, Vector2 direction, float lineLength, int angleDegrees,
        Matrix4x4? matrixForMirroredVersion = null
    )
    {
        var rotationMatrix = Matrix4x4.Rotate(Quaternion.Euler(0, 0, angleDegrees));
        var along = 0f;

        while (along < lineLength)
        {
            var d = this.decoration[this.rnd.Next() % this.decoration.Length];
            if (along + d.reachLeft + d.reachRight + 1 > lineLength)
                break;

            along += d.reachLeft + 0.5f;

            if (this.rnd.NextDouble() < 0.3f)
            {
                var pos = start + (direction * along);
                var matrix = Matrix4x4.Translate(pos) * rotationMatrix;
                this.tileGridMesh.AddSprite(matrix, d.sprite, Color.white);

                if (matrixForMirroredVersion != null)
                {
                    matrix = matrixForMirroredVersion.Value * matrix;
                    this.tileGridMesh.AddSprite(matrix, d.sprite, Color.white);
                }
            }

            along += d.reachRight + 0.5f;
        }
    }

    private void AddHCorner(int mapTx, int mapTy, Sprite sprite, Corner cornerCut)
    {
        this.tileGridMesh.AddSprite(
            mapTx - this.chunk.xMin, mapTy - this.chunk.yMin, sprite, Color.white
        );

        var (corner, normal) = cornerCut switch
        {
            Corner.TR => (new Vector2(2.25f, 4), new Vector2(-1, -1).normalized),
            Corner.BR => (new Vector2(2.25f, 0), new Vector2(-1, +1).normalized),
            Corner.TL => (new Vector2(0.75f, 4), new Vector2(+1, -1).normalized),
            _         => (new Vector2(0.75f, 0), new Vector2(+1, +1).normalized)
        };

        var hitbox = AddHitbox(mapTx, mapTy, 3, 4);
        hitbox.customShape = new AirlockCornerHitboxShape
        {
            slicePositionRelative = new Vector2(
                mapTx + corner.x - this.chunk.xMin,
                mapTy + corner.y - this.chunk.yMin
            ),
            sliceDirection = normal
        };
    }

    private void AddVCorner(int mapTx, int mapTy, Sprite sprite, Corner cornerCut)
    {
        this.tileGridMesh.AddSprite(
            mapTx - this.chunk.xMin, mapTy - this.chunk.yMin, sprite, Color.white
        );

        var (corner, normal) = cornerCut switch
        {
            Corner.TR => (new Vector2(4, 2.25f), new Vector2(-1, -1).normalized),
            Corner.BR => (new Vector2(4, 0.75f), new Vector2(-1, +1).normalized),
            Corner.TL => (new Vector2(0, 2.25f), new Vector2(+1, -1).normalized),
            _         => (new Vector2(0, 0.75f), new Vector2(+1, +1).normalized)
        };

        var hitbox = AddHitbox(mapTx, mapTy, 4, 3);
        hitbox.customShape = new AirlockCornerHitboxShape
        {
            slicePositionRelative = new Vector2(
                mapTx + corner.x - this.chunk.xMin,
                mapTy + corner.y - this.chunk.yMin
            ),
            sliceDirection = normal
        };
    }

    private void AddCorner(int mapTx, int mapTy, Sprite sprite)
    {
        this.tileGridMesh.AddSprite(
            mapTx - this.chunk.xMin, mapTy - this.chunk.yMin, sprite, Color.white
        );
        AddHitbox(mapTx, mapTy, 3, 3);
    }

    private Hitbox AddHitbox(int mapTx, int mapTy, int width, int height)
    {
        var newHitbox = new Hitbox(
            mapTx - this.chunk.xMin,
            mapTx + width - this.chunk.xMin,
            mapTy - this.chunk.yMin,
            mapTy + height - this.chunk.yMin,
            0, Hitbox.Solid, false, false
        );

        if (this.dummyItemForHitboxes == null)
        {
            this.dummyItemForHitboxes = this.gameObject.AddComponent<Item>();
            this.dummyItemForHitboxes.Init(
                new Item.Def(this.chunk.xMin, this.chunk.yMin, default, this.chunk)
            );
            this.dummyItemForHitboxes.hitboxes = new [] { newHitbox };
            this.chunk.items.Add(this.dummyItemForHitboxes);
        }
        else
        {
            this.dummyItemForHitboxes.hitboxes =
                this.dummyItemForHitboxes.hitboxes.Append(newHitbox).ToArray();
        }

        return newHitbox;
    }

    private Sprite SpriteForInterior(int tx, int ty)
    {
        tx %= 4;
        ty %= 4;
        if (tx < 0) tx += 4;
        if (ty < 0) ty += 4;

        var rect = this.spriteTexture.textureRect;
        var r = new Rect(
            Mathf.Lerp(rect.xMin, rect.xMax, tx / 4f),
            Mathf.Lerp(rect.yMin, rect.yMax, ty / 4f),
            rect.width / 4f,
            rect.height / 4f
        );
        return Sprite.Create(
            this.spriteTexture.texture, r, Vector2.zero, this.spriteTexture.pixelsPerUnit
        );
    }

    private void FillUnusedSpaceBelowHorizontalChunk(Chunk chunk, int horizontalRunBottom)
    {
        for (int tx = chunk.xMin; tx <= chunk.xMax; tx += 1)
        {
            for (int ty = horizontalRunBottom; ty < chunk.yMin; ty += 1)
            {
                this.tileGridMesh.AddSprite(
                    tx - this.chunk.xMin, ty - this.chunk.yMin,
                    SpriteForInterior(tx, ty), Color.white
                );
            }
        }
    }

    private void FillUnusedSpaceAboveHorizontalChunk(Chunk chunk, int horizontalRunTop)
    {
        for (int tx = chunk.xMin; tx <= chunk.xMax; tx += 1)
        {
            for (int ty = chunk.yMax + 1; ty <= horizontalRunTop; ty += 1)
            {
                this.tileGridMesh.AddSprite(
                    tx - this.chunk.xMin, ty - this.chunk.yMin,
                    SpriteForInterior(tx, ty), Color.white
                );
            }
        }
    }

    private LimitsOfEdgesInVerticalSection FindLimitsOfEdgesInVerticalSection(Chunk chunk)
    {
        var result = new LimitsOfEdgesInVerticalSection
        {
            bottomLeft  = chunk.yMin,
            bottomRight = chunk.yMin,
            topLeft     = chunk.yMax,
            topRight    = chunk.yMax
        };

        for (int n = chunk.index - 1; n >= 0; n -= 1)
        {
            var c = chunk.map.chunks[n];
            if (c.isHorizontal == true)
            {
                var horizontalTop = HorizontalTopAndBottom(c).top + 4;
                if (c.isFlippedHorizontally == true)
                    result.bottomRight = Mathf.Max(result.bottomRight, horizontalTop);
                else
                    result.bottomLeft = Mathf.Max(result.bottomLeft, horizontalTop);
                break;
            }
        }

        for (int n = chunk.index + 1; n < chunk.map.chunks.Count; n += 1)
        {
            var c = chunk.map.chunks[n];
            if (c.isHorizontal == true)
            {
                var horizontalBottom = HorizontalTopAndBottom(c).bottom - 4;
                if (c.isFlippedHorizontally == true)
                    result.topLeft = Mathf.Min(result.topLeft, horizontalBottom);
                else
                    result.topRight = Mathf.Min(result.topRight, horizontalBottom);
                break;
            }
        }

        return result;
    }

    private (int bottom, int top) HorizontalTopAndBottom(Chunk chunk)
    {
        var bottom = chunk.yMin;
        var top = chunk.yMax;

        bool IsInHorizontalRun(Chunk c) =>
            (c.entry.type == MarkerType.Wall || c.exit.type == MarkerType.Wall)
            && (c.initialGravityDirection != null);

        for (int n = chunk.index - 1; n >= 0; n -= 1)
        {
            var prevChunk = chunk.map.chunks[n];
            if (IsInHorizontalRun(prevChunk) == false) break;
            if (prevChunk.yMin < bottom) bottom = prevChunk.yMin;
            if (prevChunk.yMax > top)    top = prevChunk.yMax;
        }

        for (int n = chunk.index + 1; n < chunk.map.chunks.Count; n += 1)
        {
            var nextChunk = chunk.map.chunks[n];
            if (IsInHorizontalRun(nextChunk) == false) break;
            if (nextChunk.yMin < bottom) bottom = nextChunk.yMin;
            if (nextChunk.yMax > top)    top = nextChunk.yMax;
        }

        return (bottom, top);
    }
}

public class AirlockCornerHitboxShape : Hitbox.CustomShape
{
    public Vector2 slicePositionRelative;
    public Vector2 sliceDirection;

    public override bool IsSolidAtPoint(Vector2 relativePosition)
    {
        return Vector2.Dot(
            relativePosition - this.slicePositionRelative,
            this.sliceDirection
        ) > 0;
    }

    public override float SurfaceAngleDegrees(
        Vector2 relativePosition, Vector2 incomingRayDirection
    )
    {
        var angleRadians = Mathf.Atan2(incomingRayDirection.y, incomingRayDirection.x);
        var angleDegrees = angleRadians * 180 / Mathf.PI;
        return angleDegrees + 90;
    }
}
