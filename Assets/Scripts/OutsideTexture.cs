
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class OutsideTexture : MonoBehaviour
{
    private Map map;
    private Sprite sprite;
    private int spriteSize;
    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;
    private Mesh mesh;
    private (int txMin, int txMax, int tyMin, int tyMax) lastCameraBounds =
        (-1000, 1000, -1000, 1000);
    
    private List<Vector3> meshVertices = new();
    private List<Vector2> meshUVs      = new();
    private List<Color>   meshColours  = new();
    private List<int>     meshIndices  = new();

    public static OutsideTexture Make(Map map, TileTheme tileTheme)
    {
        var go = new GameObject("Outside Texture");
        go.transform.parent = map.transform;
        var ot = go.AddComponent<OutsideTexture>();
        ot.Init(map, tileTheme);
        return ot;
    }

    public void Init(Map map, TileTheme tileTheme)
    {
        this.map = map;

        if (tileTheme.outsideTexture == null)
            return;

        this.sprite = tileTheme.outsideTexture;
        this.spriteSize = tileTheme.outsideTextureSize;

        this.meshRenderer = this.gameObject.AddComponent<MeshRenderer>();
        this.meshRenderer.material = new Material(
            map.theme == Theme.HotNColdSprings ?
                Assets.instance.tileTemperatureChangeMaterial :
                Assets.instance.spritesDefaultMaterial
        );
        TemperatureAnimation.SetupTemperatureChangeMaterial(this.meshRenderer.material);
        this.meshRenderer.material.mainTexture = this.sprite.texture;
        this.meshRenderer.sortingLayerName = "Tiles - Solid";
        
        this.mesh = new Mesh();

        this.meshFilter = this.gameObject.AddComponent<MeshFilter>();
        this.meshFilter.mesh = mesh;
    }

    public void Advance()
    {
        if (this.sprite == null)
            return;

        RefreshMesh();

        this.map.temperatureAnimation?.AddTemperatureMaterialProperties(
            this.meshRenderer
        );
    }

    private void RefreshMesh()
    {
        if(this.map.theme == Theme.WindySkies) return;

        var cameraRect = this.map.gameCamera.VisibleRectangle();

        int size = this.spriteSize;
        var txMin = Mathf.FloorToInt(cameraRect.xMin - 1);
        var txMax = Mathf.FloorToInt(cameraRect.xMax + 2);
        var tyMin = Mathf.FloorToInt(cameraRect.yMin - 1);
        var tyMax = Mathf.FloorToInt(cameraRect.yMax + 2);
        var cameraBounds = (txMin, txMax, tyMin, tyMax);

        if (this.lastCameraBounds == cameraBounds)
            return;
        this.lastCameraBounds = cameraBounds;

        var xSet = new HashSet<int> { txMin, txMax };
        var ySet = new HashSet<int> { tyMin, tyMax };

        for (int x = txMin - Util.Modulo(txMin, size); x <= txMax; x += size)
            xSet.Add(x);

        for (int y = tyMin - Util.Modulo(tyMin, size); y <= tyMax; y += size)
            ySet.Add(y);

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;

            int x = chunk.xMin;
            if (x >= txMin && x <= txMax) xSet.Add(x);

            x = chunk.xMax + 1;
            if (x >= txMin && x <= txMax) xSet.Add(x);

            int y = chunk.yMin;
            if (y >= tyMin && y <= tyMax) ySet.Add(y);

            y = chunk.yMax + 1;
            if (y >= tyMin && y <= tyMax) ySet.Add(y);
        }

        var xs = xSet.OrderBy(x => x).ToArray();
        var ys = ySet.OrderBy(x => x).ToArray();

        var chunkHere = new bool[xs.Length - 1, ys.Length - 1];
        for (int y = 0; y < ys.Length - 1; y += 1)
            for (int x = 0; x < xs.Length - 1; x += 1)
                chunkHere[x, y] = false;

        int XSection(float x)
        {
            for (int n = 1; n < xs.Length - 1; n += 1)
                if (xs[n] > x) return n - 1;
            return xs.Length - 2;
        }
        int YSection(float y)
        {
            for (int n = 1; n < ys.Length - 1; n += 1)
                if (ys[n] > y) return n - 1;
            return ys.Length - 2;
        }

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.xMax + 1 <= txMin) continue;
            if (chunk.xMin     >= txMax) continue;
            if (chunk.yMax + 1 <= tyMin) continue;
            if (chunk.yMin     >= tyMax) continue;

            int fromXSection = XSection(chunk.xMin + 0.5f);
            int toXSection   = XSection(chunk.xMax + 0.5f);
            int fromYSection = YSection(chunk.yMin + 0.5f);
            int toYSection   = YSection(chunk.yMax + 0.5f);

            for (int y = fromYSection; y <= toYSection; y += 1)
                for (int x = fromXSection; x <= toXSection; x += 1)
                    chunkHere[x, y] = true;
        }

        var theColour =
            this.map.theme == Theme.HotNColdSprings ?
            TemperatureAnimation.DrawingColorForNormalTiles :
            Color.white;

        this.meshVertices.Clear();
        this.meshUVs.Clear();
        this.meshColours.Clear();
        this.meshIndices.Clear();

        for (int y = 0; y < ys.Length - 1; y += 1)
        {
            for (int x = 0; x < xs.Length - 1; x += 1)
            {
                if (chunkHere[x, y] == true) continue;

                float u1 = ((float)xs[x    ] / (float)size) % 1.0f;
                float u2 = ((float)xs[x + 1] / (float)size) % 1.0f;
                float v1 = ((float)ys[y    ] / (float)size) % 1.0f;
                float v2 = ((float)ys[y + 1] / (float)size) % 1.0f;

                if (u1 < 0) u1 += 1;
                if (u2 < 0) u2 += 1;
                if (v1 < 0) v1 += 1;
                if (v2 < 0) v2 += 1;

                if (u2 == 0) u2 = 1;
                if (v2 == 0) v2 = 1;

                u1 = Mathf.Lerp(this.sprite.uv[0].x, this.sprite.uv[1].x, u1);
                u2 = Mathf.Lerp(this.sprite.uv[0].x, this.sprite.uv[1].x, u2);
                v1 = Mathf.Lerp(this.sprite.uv[0].y, this.sprite.uv[1].y, v1);
                v2 = Mathf.Lerp(this.sprite.uv[0].y, this.sprite.uv[1].y, v2);

                int firstVertex = this.meshVertices.Count;
                this.meshVertices.Add(new Vector3(xs[x    ], ys[y    ], 0));
                this.meshVertices.Add(new Vector3(xs[x + 1], ys[y    ], 0));
                this.meshVertices.Add(new Vector3(xs[x    ], ys[y + 1], 0));
                this.meshVertices.Add(new Vector3(xs[x + 1], ys[y + 1], 0));
                this.meshUVs.Add(new Vector2(u1, v1));
                this.meshUVs.Add(new Vector2(u2, v1));
                this.meshUVs.Add(new Vector2(u1, v2));
                this.meshUVs.Add(new Vector2(u2, v2));
                this.meshColours.Add(theColour);
                this.meshColours.Add(theColour);
                this.meshColours.Add(theColour);
                this.meshColours.Add(theColour);
                this.meshIndices.Add(firstVertex + 0);
                this.meshIndices.Add(firstVertex + 1);
                this.meshIndices.Add(firstVertex + 2);
                this.meshIndices.Add(firstVertex + 2);
                this.meshIndices.Add(firstVertex + 1);
                this.meshIndices.Add(firstVertex + 3);
            }
        }

        mesh.name = $"Mesh generated by OutsideTexture";
        mesh.Clear();
        mesh.SetVertices(this.meshVertices);
        mesh.SetColors(this.meshColours);
        mesh.SetUVs(0, this.meshUVs);
        mesh.SetTriangles(this.meshIndices, 0);
        mesh.MarkModified();

        this.meshFilter.mesh = this.mesh;
    }
}
