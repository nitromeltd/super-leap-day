
using UnityEngine;
using System;
using System.Collections.Generic;

public class Particle : MonoBehaviour
{
    public static List<Particle> pooled = new List<Particle>();
    public static List<Particle> active = new List<Particle>();

    public SpriteRenderer spriteRenderer;
    public Animated animated;

    public Vector2 velocity;
    public Vector2 acceleration;
    public float spin;
    public Vector3 spin3d;
    public float scale;
    public int frameNumber;
    public int? lifetime;
    public Fadeout fadeout;
    public Scaleout scaleOut;
    public Action onLifetimeOver;
    public bool canBeSucked;
    public bool controlledByPlayer;
    public float? maximumY;
    public float? minimumY;

    public struct Fadeout
    {
        public bool enabled;
        public int duration;
        public Color color;
        public AnimationCurve curve;
    }

    public struct Scaleout
    {
        public bool enabled;
        public int duration;
        public float? startValue;
        public AnimationCurve curve;
    }

    private static Particle NewInstance(Vector3 position, Transform parent)
    {
        GameObject gameObject = null;
        Particle result = null;

        if (pooled.Count > 0)
        {
            result = pooled[0];
            pooled.RemoveAt(0);
        }
        if (result != null)
        {
            gameObject = result.gameObject;
            gameObject.transform.localRotation = Quaternion.identity;
            gameObject.transform.localScale = Vector2.one;
            result.spriteRenderer.enabled = true;
            result.spriteRenderer.sortingLayerName = null;
            result.spriteRenderer.sortingOrder = 0;
            result.spriteRenderer.material = minigame.TileTexture.SpritesDefaultMaterial ?? Assets.instance.spritesDefaultMaterial;
            result.spriteRenderer.color = Color.white;
            result.velocity = Vector2.zero;
            result.acceleration = Vector2.zero;
            result.spin = 0;
            result.scale = 0;
            result.frameNumber = 0;
            result.lifetime = null;
            result.onLifetimeOver = null;
            result.fadeout = default(Fadeout);
            result.scaleOut = default(Scaleout);
            result.gameObject.layer = 0;// Default
            result.canBeSucked = false;
            result.controlledByPlayer = false;
            result.maximumY = null;
            result.minimumY = null;

            gameObject.SetActive(true);
        }
        else
        {
            // pool was empty, or pooled particle was destroyed (by scene change?)
            gameObject = new GameObject();
            result                = gameObject.AddComponent<Particle>();
            result.spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            result.animated       = gameObject.AddComponent<Animated>();
        }

        // TODO: make this set localPosition, not position, and change all
        // callers from this.transform.parent to Map.instance.transform
        // (since chunks have an offset)

        active.Add(result);
        gameObject.name = "Particle";
        gameObject.transform.parent = parent;
        gameObject.transform.position = position;
        
        var map = parent?.GetComponentInParent<Map>();
        if (map != null)
            gameObject.layer = map.Layer();
        else if (Game.instance?.map1 != null)
        {
            Debug.Log("Particle needs to be inside a map transform in order to have its layer set correctly.");
        }
        return result;
    }

    public static Particle CreatePlayAndLoop(
        Animated.Animation animation,
        int lifetime,
        Vector3 position,
        Transform parent
    )
    {
        var result = Particle.NewInstance(position, parent);
        result.gameObject.name = $"Particle [{animation.sprites[0].name}]";
        result.animated.PlayAndLoop(animation);
        result.lifetime = lifetime;
        return result;
    }

    public static Particle CreatePlayAndHoldLastFrame(
        Animated.Animation animation,
        int lifetime,
        Vector3 position,
        Transform parent
    )
    {
        var result = Particle.NewInstance(position, parent);
        result.gameObject.name = $"Particle [{animation.sprites[0].name}]";
        result.animated.PlayAndHoldLastFrame(animation);
        result.lifetime = lifetime;
        return result;
    }

    public static Particle CreateAndPlayOnce(
        Animated.Animation animation,
        Vector3 position,
        Transform parent
    )
    {
        var result = Particle.NewInstance(position, parent);
        result.gameObject.name = $"Particle [{animation.sprites[0].name}]";
        result.animated.PlayOnce(animation, delegate
        {
            result.StopAndReturnToPool();
        });
        return result;
    }

    public static Particle CreateWithSprite(
        Sprite sprite,
        int lifetime,
        Vector3 position,
        Transform parent
    )
    {
        var result = Particle.NewInstance(position, parent);
        result.gameObject.name = $"Particle [{sprite.name}]";
        result.spriteRenderer.sprite = sprite;
        result.animated.Stop();
        result.lifetime = lifetime;
        return result;
    }

    public void Throw(
        Vector2 averageVelocity,
        float velocityVariationX,
        float velocityVariationY,
        Vector2 acceleration
    )
    {
        var deltaVx = UnityEngine.Random.Range(-velocityVariationX, velocityVariationX);
        var deltaVy = UnityEngine.Random.Range(-velocityVariationY, velocityVariationY);
        this.velocity.x = averageVelocity.x + deltaVx;
        this.velocity.y = averageVelocity.y + deltaVy;
        this.acceleration = acceleration;
    }

    public static void CreateDust(
        Transform parent,
        int particleCountMin,
        int particleCountMax,
        Vector2 center,
        float variationX         = 3 / 16f,
        float variationY         = 3 / 16f,
        Vector2 velocity         = default,
        float velocityVariationX = 0.1f / 16f,
        float velocityVariationY = 0.1f / 16f,
        bool smaller             = false,
        string sortingLayerName  = null
    )
    {
        var count = UnityEngine.Random.Range(particleCountMin, particleCountMax + 1);
        for (var n = 0; n < count; n += 1)
        {
            float scale = UnityEngine.Random.Range(0.5f, smaller ? 0.7f : 1.0f);

            var p = Particle.CreateAndPlayOnce(
                Assets.instance.genericDustParticleAnimation,
                new Vector2(
                    center.x + UnityEngine.Random.Range(-variationX, variationX),
                    center.y + UnityEngine.Random.Range(-variationY, variationY)
                ),
                parent
            );
            p.transform.localScale = new Vector3(scale, scale, 1);
            p.velocity = velocity.Add(
                UnityEngine.Random.Range(-velocityVariationX, velocityVariationX),
                UnityEngine.Random.Range(-velocityVariationY, velocityVariationY)
            );
            p.animated.playbackSpeed = UnityEngine.Random.Range(0.9f, 1.1f);
            if (sortingLayerName != null)
                p.spriteRenderer.sortingLayerName = sortingLayerName;
            else
                p.spriteRenderer.sortingLayerName = "Player Dust (AL)";
        }
    }

    public static void AdvanceAll()
    {
        for (var n = active.Count - 1; n >= 0; n -= 1)
        {
            if (active[n] == null)
            {
                active.RemoveAt(n);
                continue;
            }

            active[n].Advance();
        }
    }

    public void Advance()
    {
        if(this.controlledByPlayer == true) return;

        this.velocity += this.acceleration;
        this.transform.position += (Vector3)this.velocity;
        if (this.spin != 0)
            this.transform.localRotation *= Quaternion.Euler(0, 0, this.spin);
        if (this.spin3d != Vector3.zero)
            this.transform.localRotation *= Quaternion.Euler(this.spin3d);
        if (this.scale != 0)
            this.transform.localScale += Vector3.one * scale;

        if(maximumY.HasValue == true && this.transform.position.y > maximumY.Value)
        {
            StopAndReturnToPool();
        }

        if(minimumY.HasValue == true && this.transform.position.y < minimumY.Value)
        {
            StopAndReturnToPool();
        }

        this.frameNumber += 1;
        if (this.lifetime.HasValue)
        {
            if (this.frameNumber >= this.lifetime.Value)
            {
                this.onLifetimeOver?.Invoke();
                this.onLifetimeOver = null;
                active.Remove(this);
                StopAndReturnToPool();
            }

            if (this.fadeout.enabled &&
                this.frameNumber > this.lifetime - this.fadeout.duration)
            {
                int startFrame = this.lifetime.Value - this.fadeout.duration;
                float s =
                    (float)(this.frameNumber - startFrame) /
                    (float)this.fadeout.duration;

                Color color = this.fadeout.color;
                color.a *= this.fadeout.curve.Evaluate(s);
                this.spriteRenderer.color = color;
            }

            if (this.scaleOut.enabled == true &&
                this.frameNumber > this.lifetime - this.scaleOut.duration)
            {
                int startFrame = this.lifetime.Value - this.scaleOut.duration;
                float s =
                    (float)(this.frameNumber - startFrame) /
                    (float)this.scaleOut.duration;

                if (this.scaleOut.startValue == null)
                    this.scaleOut.startValue = this.transform.localScale.x;
                float scale =
                    this.scaleOut.curve.Evaluate(s) *
                    this.scaleOut.startValue.Value;

                this.transform.localScale = new Vector3(scale, scale, 1);
            }
        }
    }

    public void StopAndReturnToPool()
    {
        this.gameObject.SetActive(false);
        this.transform.parent = null;
        active.Remove(this);
        pooled.Add(this);
    }

    public void FadeOut(AnimationCurve curve = null)
    {
        if (this.lifetime.HasValue == false)
        {
            Debug.LogError(
                "Fading out particles with indeterminate lifetime is unsupported."
            );
        }

        this.fadeout = new Fadeout
        {
            enabled = true,
            curve = curve ?? AnimationCurve.Linear(0f, 1f, 1f, 0f),
            duration = this.lifetime ?? 15,
            color = this.spriteRenderer.color
        };
    }

    public void FadeOutAtEnd(int fadeOutDuration, AnimationCurve curve = null)
    {
        if (this.lifetime.HasValue == false)
        {
            Debug.LogError(
                "Fading out particles with indeterminate lifetime is unsupported."
            );
        }

        this.fadeout = new Fadeout
        {
            enabled = true,
            curve = curve ?? AnimationCurve.Linear(0f, 1f, 1f, 0f),
            duration = fadeOutDuration,
            color = this.spriteRenderer.color
        };
    }

    public void ScaleOut(AnimationCurve curve = null)
    {
        if (this.lifetime.HasValue == false)
        {
            Debug.LogError(
                "Scaling out particles with indeterminate lifetime is unsupported."
            );
        }

        this.scaleOut = new Scaleout
        {
            enabled = true,
            curve = curve ?? AnimationCurve.Linear(0f, 1f, 1f, 0f),
            duration = this.lifetime ?? 15
        };
    }

    public void ScaleOutAtEnd(int scaleOutDuration, AnimationCurve curve = null)
    {
        if (this.lifetime.HasValue == false)
        {
            Debug.LogError(
                "Scaling out particles with indeterminate lifetime is unsupported."
            );
        }

        this.scaleOut = new Scaleout
        {
            enabled = true,
            curve = curve ?? AnimationCurve.Linear(0f, 1f, 1f, 0f),
            duration = scaleOutDuration
        };
    }
}
