
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class PathRenderer : MonoBehaviour
{
    // sprite should be horizontal. pixels-per-unit will be respected.

    private Mesh mesh;

    public void Draw(
        Sprite sprite,
        Vector2[] positions,
        bool loopPath,
        string sortingLayerName,
        float spriteStretchFactor = 1
    )
    {
        float thickness = sprite.rect.height / sprite.pixelsPerUnit;
        float lengthOfSprite =
            spriteStretchFactor * sprite.rect.width / sprite.pixelsPerUnit;

        for (int n = 0; n < positions.Length; n += 1)
            positions[n] = this.transform.InverseTransformPoint(positions[n]);

        Vector2[] vertexDirections;
        float[] vertexScales;
        Vector2 EdgeDirection(int n) => (positions[n + 1] - positions[n]).normalized;

        if (loopPath)
        {
            vertexDirections = new Vector2[positions.Length];
            vertexScales = new float[positions.Length];
            for (var n = 0; n < positions.Length; n += 1)
            {
                var a = EdgeDirection(n);
                var b = EdgeDirection((n + positions.Length - 1) % positions.Length);
                vertexDirections[n] = (a + b).normalized;
                vertexScales[n] = CornerScale(a, b);
            }
        }
        else
        {
            vertexDirections = new Vector2[positions.Length];
            vertexScales = new float[positions.Length];
            for (var n = 0; n < positions.Length; n += 1)
            {
                Vector2 a = (n == 0) ?
                    EdgeDirection(0) :
                    EdgeDirection(n - 1);
                Vector2 b = (n == positions.Length - 1) ?
                    EdgeDirection(n - 1) :
                    EdgeDirection(n);
                vertexDirections[n] = (a + b).normalized;
                vertexScales[n] = CornerScale(a, b);
            }
        }

        var edgeCount = positions.Length + (loopPath ? 0 : -1);
        var uMin = sprite.uv.Select(uv => uv.x).Min();
        var uMax = sprite.uv.Select(uv => uv.x).Max();
        var vMin = sprite.uv.Select(uv => uv.y).Min();
        var vMax = sprite.uv.Select(uv => uv.y).Max();

        var vertices = new List<Vector3>();
        var colours  = new List<Color>();
        var uvs      = new List<Vector2>();
        var indices  = new List<int>();

        float distanceAlongSprite = 0;

        for (var e = 0; e < edgeCount; e += 1)
        {
            float distanceAlongEdge = 0;

            var fromIndex = e;
            var toIndex = (e + 1) % positions.Length;
            var from = positions[fromIndex];
            var to = positions[toIndex];
            var edgeLength = (to - from).magnitude;
            if (edgeLength < 0.01f) continue;

            while (distanceAlongEdge < edgeLength - 0.001f)
            {
                float pieceLength = lengthOfSprite - distanceAlongSprite;
                if (pieceLength > edgeLength - distanceAlongEdge)
                    pieceLength = edgeLength - distanceAlongEdge;

                var s = distanceAlongEdge / edgeLength;
                var position = Vector2.Lerp(from, to, s);
                var parallel = Vector2.Lerp(
                    vertexDirections[fromIndex], vertexDirections[toIndex], s
                ).normalized;
                var perpendicular =
                    new Vector2(parallel.y, -parallel.x) * thickness / 2;
                var u = Mathf.Lerp(uMin, uMax, distanceAlongSprite / lengthOfSprite);
                var width = Mathf.Lerp(
                    vertexScales[fromIndex], vertexScales[toIndex], s
                );
                int firstIndex = vertices.Count;

                vertices.Add(position - (perpendicular * width));
                vertices.Add(position + (perpendicular * width));
                colours.Add(Color.white);
                colours.Add(Color.white);
                uvs.Add(new Vector2(u, vMin));
                uvs.Add(new Vector2(u, vMax));

                distanceAlongEdge += pieceLength;
                distanceAlongSprite += pieceLength;

                s = distanceAlongEdge / edgeLength;
                position = Vector2.Lerp(from, to, s);
                parallel = Vector2.Lerp(
                    vertexDirections[fromIndex], vertexDirections[toIndex], s
                ).normalized;
                perpendicular = new Vector2(parallel.y, -parallel.x) * thickness / 2;
                u = Mathf.Lerp(uMin, uMax, distanceAlongSprite / lengthOfSprite);
                width = Mathf.Lerp(
                    vertexScales[fromIndex], vertexScales[toIndex], s
                );

                vertices.Add(position - (perpendicular * width));
                vertices.Add(position + (perpendicular * width));
                colours.Add(Color.white);
                colours.Add(Color.white);
                uvs.Add(new Vector2(u, vMin));
                uvs.Add(new Vector2(u, vMax));

                indices.Add(firstIndex + 0);
                indices.Add(firstIndex + 2);
                indices.Add(firstIndex + 1);
                indices.Add(firstIndex + 1);
                indices.Add(firstIndex + 2);
                indices.Add(firstIndex + 3);

                if (distanceAlongSprite >= lengthOfSprite - 0.001f)
                    distanceAlongSprite = 0;
            }
        }

        if (this.mesh == null)
            this.mesh = new Mesh();
        mesh.name = $"Mesh generated by PathRenderer ({gameObject.name})";
        mesh.Clear();
        mesh.SetVertices(vertices);
        mesh.SetColors(colours);
        mesh.SetUVs(0, uvs);
        mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
        mesh.MarkModified();

        var meshFilter =
            this.gameObject.GetComponent<MeshFilter>() ??
            this.gameObject.AddComponent<MeshFilter>();
        meshFilter.mesh = mesh;

        var meshRenderer =
            this.gameObject.GetComponent<MeshRenderer>() ??
            this.gameObject.AddComponent<MeshRenderer>();
        meshRenderer.material.mainTexture = sprite.texture;
        meshRenderer.sortingLayerName = sortingLayerName;
    }

    private Vector2[] VertexDirections(NitromeEditor.Path path)
    {
        var result = new Vector2[path.nodes.Length];

        if (path.closed)
        {
            for (var n = 0; n < result.Length; n += 1)
            {
                var a = path.edges[n].direction;
                var b = path.edges[(n + result.Length - 1) % result.Length].direction;
                result[n] = (a + b).normalized;
            }
        }
        else
        {
            result[0] = path.edges[0].direction;
            result[result.Length - 1] = path.edges[path.edges.Length - 1].direction;

            for (var n = 1; n < result.Length - 1; n += 1)
            {
                var a = path.edges[n - 1].direction;
                var b = path.edges[n].direction;
                result[n] = (a + b).normalized;
            }
        }

        return result;
    }

    private float CornerScale(Vector2 a, Vector2 b)
    {
        // i don't think this is quite right yet

        float dot = Vector2.Dot(a.normalized, b.normalized);
        if (dot > 1) dot = 1;
        if (dot < -1) dot = -1;
        float angleRadians = Mathf.Acos(dot);

        if (dot >= 0)
        {
            float s = Mathf.Sin(angleRadians / 2);
            return Mathf.Sqrt(1 + s * s);
        }
        else
        {
            float multiplier = Mathf.Tan(angleRadians / 2);
            if (multiplier > 2) multiplier = 2;
            if (multiplier < -2) multiplier = -2;
            return multiplier;
        }
    }

    private Vector2[] VertexDirections(Vector2[] edgeDirections)
    {
        var result = new Vector2[edgeDirections.Length + 1];
        result[0] = edgeDirections[0];
        result[result.Length - 1] = edgeDirections[edgeDirections.Length - 1];

        for (var n = 1; n < result.Length - 1; n += 1)
        {
            result[n] = (edgeDirections[n - 1] + edgeDirections[n]).normalized;
        }

        return result;
    }
}
