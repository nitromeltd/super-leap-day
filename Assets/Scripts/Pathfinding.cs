using UnityEngine;
using System;
using System.Collections.Generic;

// will be in an invalid state until Refresh()'d for the first time

public class Pathfinding
{
    private Stopwatch timer = new();
    private Chunk chunk;
    public Chunk Chunk { get => chunk; }

    private int lastRefreshFrameNumber = int.MinValue;
    private int columns;
    private int rows;
    private bool[,] filled;
    private int[,] values;

    private Stack<(int, int)> stack = new Stack<(int, int)>();

    public Pathfinding(Chunk chunk)
    {
        this.chunk = chunk;
    }

    public void Refresh()
    {
        if (this.chunk.map.frameNumber == this.lastRefreshFrameNumber)
            return;

        this.lastRefreshFrameNumber = this.chunk.map.frameNumber;

        if (this.values == null)
        {
            this.columns = 1 + chunk.xMax - chunk.xMin;
            this.rows = 1 + chunk.yMax - chunk.yMin;
            this.filled = new bool[this.columns, this.rows];
            this.values = new int[this.columns, this.rows];
        }

        for (int x = 0; x < this.columns; x += 1)
        {
            for (int y = 0; y < this.rows; y += 1)
            {
                this.filled[x, y] = false;
                this.values[x, y] = 1_000_000;
            }
        }

        Vector2 playerRelative =
            this.chunk.map.player.position.Add(-chunk.xMin, -chunk.yMin);

        if (playerRelative.x < 0 || playerRelative.x >= this.columns) return;
        if (playerRelative.y < 0 || playerRelative.y >= this.rows) return;

        int mapTx, mapTy;
        int gridTx, gridTy;
        int offsetX, offsetY;
        Tile tile;
        foreach (var tileGrid in this.chunk.solidTilesA)
        {
            offsetX = (int)(tileGrid.offset.x);
            offsetY = (int)(tileGrid.offset.y);

            for (gridTx = 0; gridTx < tileGrid.columns; gridTx += 1)
            {
                for (gridTy = 0; gridTy < tileGrid.rows; gridTy += 1)
                {
                    mapTx = gridTx + tileGrid.xMin + offsetX;
                    mapTy = gridTy + tileGrid.yMin + offsetY;
                    if (mapTx < this.chunk.xMin) continue;
                    if (mapTx > this.chunk.xMax) continue;
                    if (mapTy < this.chunk.yMin) continue;
                    if (mapTy > this.chunk.yMax) continue;

                    tile = tileGrid.GetTile(
                        gridTx, gridTy
                    );

                    if (tile != null)
                    {
                        // this.filled[mapTx - this.chunk.xMin, mapTy - this.chunk.yMin] = true;
                        FillUsed(mapTx - this.chunk.xMin, mapTy - this.chunk.yMin);
                    }
                }
            }
        }

        foreach (var item in this.chunk.items)
        {
            if (item is BreakableBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    //we need actual position instead of index position as we need to calculate tiles based on exact hitbox
                    var hitbox = item.hitboxes[0];
                    for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                    {
                        for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                        {
                            int ix = Mathf.FloorToInt(px);
                            int iy = Mathf.FloorToInt(py);
                            FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                        }
                    }
                }
            }
            else if (item is TNTBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    //we need actual position instead of index position as we need to calculate tiles based on exact hitbox
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
            else if (item is RandomBox)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                    {
                        for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                        {
                            int ix = Mathf.FloorToInt(px);
                            int iy = Mathf.FloorToInt(py);
                            FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                        }
                    }
                }
            }
            else if (item is MetalBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                    {
                        for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                        {
                            int ix = Mathf.FloorToInt(px);
                            int iy = Mathf.FloorToInt(py);
                            FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                        }
                    }
                }
            }
            else if (item is ABBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
            else if (item is GateBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
            else if (item is CCTVGateBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
            else if (item is EnemyBlocker)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
            else if (item is StatueTriggerBlock)
            {
                if (item.hitboxes.Length > 0)
                {
                    var hitbox = item.hitboxes[0];
                    if (hitbox.solidXMin && hitbox.solidXMax && hitbox.solidYMin && hitbox.solidYMax)
                    {
                        for (float px = item.position.x + hitbox.xMin; px < item.position.x + hitbox.xMax; px += 1)
                        {
                            for (float py = item.position.y + hitbox.yMin; py < item.position.y + hitbox.yMax; py += 1)
                            {
                                int ix = Mathf.FloorToInt(px);
                                int iy = Mathf.FloorToInt(py);
                                FillUsed(ix - this.chunk.xMin, iy - this.chunk.yMin);
                            }
                        }
                    }
                }
            }
        }

        timer.Reset();
        timer.Start();
 
        this.values[
            Mathf.FloorToInt(playerRelative.x),
            Mathf.FloorToInt(playerRelative.y)
        ] = 0;

        bool anythingChanged = true;
        while (anythingChanged == true)
        {
            anythingChanged = false;

            for (int y = 0; y < this.rows; y += 1)
            {
                // rightwards
                for (int x = 1; x < this.columns; x += 1)
                {
                    if (this.filled[x, y] == true) continue;

                    int newValue = this.values[x - 1, y] + 1;
                    if (this.values[x, y] > newValue)
                    {
                        this.values[x, y] = newValue;
                        anythingChanged = true;
                    }
                }

                // leftwards
                for (int x = this.columns - 2; x >= 0; x -= 1)
                {
                    if (this.filled[x, y] == true) continue;

                    int newValue = this.values[x + 1, y] + 1;
                    if (this.values[x, y] > newValue)
                    {
                        this.values[x, y] = newValue;
                        anythingChanged = true;
                    }
                }
            }

            for (int x = 0; x < this.columns; x += 1)
            {
                // upwards
                for (int y = 1; y < this.rows; y += 1)
                {
                    if (this.filled[x, y] == true) continue;

                    int newValue = this.values[x, y - 1] + 1;
                    if (this.values[x, y] > newValue)
                    {
                        this.values[x, y] = newValue;
                        anythingChanged = true;
                    }
                }

                // downwards
                for (int y = this.rows - 2; y >= 0; y -= 1)
                {
                    if (this.filled[x, y] == true) continue;

                    int newValue = this.values[x, y + 1] + 1;
                    if (this.values[x, y] > newValue)
                    {
                        this.values[x, y] = newValue;
                        anythingChanged = true;
                    }
                }
            }
        }

        timer.Stop();
        // Debug.Log("Timer:" + timer.elapsed.ToString("F5"));
}

    private void FillUsed(int x, int y)
    {
        this.filled[x, y] = true;
        if (x > 0) this.filled[x - 1, y] = true;
        if (x < this.columns - 1) this.filled[x + 1, y] = true;
        if (y > 0) this.filled[x, y - 1] = true;
        if (y < this.rows - 1) this.filled[x, y + 1] = true;
    }


    public Vector2? NextPointToPlayer(Vector2 startPoint)
    {
        var tx = Mathf.FloorToInt(startPoint.x) - this.chunk.xMin;
        var ty = Mathf.FloorToInt(startPoint.y) - this.chunk.yMin;

        if (tx < 0 || tx >= this.columns) return null;
        if (ty < 0 || ty >= this.rows) return null;

        (int x, int y)? result = null;
        int bestValue = 1_000_000;

        if (ty > 0 && this.values[tx, ty - 1] < bestValue)
        {
            result = (tx, ty - 1);
            bestValue = this.values[tx, ty - 1];
        }
        if (ty < this.rows - 1 && this.values[tx, ty + 1] < bestValue)
        {
            result = (tx, ty + 1);
            bestValue = this.values[tx, ty + 1];
        }
        if (tx > 0 && this.values[tx - 1, ty] < bestValue)
        {
            result = (tx - 1, ty);
            bestValue = this.values[tx - 1, ty];
        }
        if (tx < this.columns - 1 && this.values[tx + 1, ty] < bestValue)
        {
            result = (tx + 1, ty);
            bestValue = this.values[tx + 1, ty];
        }


        if (result.HasValue == true)
            return new Vector2(result.Value.x + 0.5f, result.Value.y + 0.5f);
        else
            return null;
    }
}