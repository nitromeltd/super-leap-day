
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rnd = UnityEngine.Random;

public class Player : MonoBehaviour
{
    // http://info.sonicretro.org/Sonic_Physics_Guide

    [NonSerialized] public SpriteRenderer spriteRenderer;
    [NonSerialized] public SpriteRenderer backArmSpriteRenderer;
    [NonSerialized] public SpriteRenderer frontArmSpriteRenderer;
    [NonSerialized] public Animated animated;

    [NonSerialized] public Map map;
    [NonSerialized] public Vector2 position;
    [NonSerialized] public Vector2 velocity;
    [NonSerialized] public State state;
    [NonSerialized] public float groundspeed;
    [NonSerialized] public GroundState groundState;
    [NonSerialized] public Gravity gravity;
    [NonSerialized] public bool facingRight;
    [NonSerialized] public readonly bool autoRunning = true;
    [NonSerialized] public bool alive = true;
    [NonSerialized] public Layer layer = Layer.A;
    [NonSerialized] public Vector2 checkpointPosition;
    [NonSerialized] public Item grabbingOntoItem;
    [NonSerialized] public List<GateKey> keys = new List<GateKey>();
    public PlayerSquashAndStretch squashAndStretch;
    [NonSerialized] public bool firedFromPipeCannon = false;
    [NonSerialized] public int portalsEnteredInARowWhileAirborne;

    [Header("Power-ups")]
    public PlayerInvincibility invincibility;
    public PlayerPetYellow petYellow;
    public PlayerPetTornado petTornado;
    public PlayerShield shield;
    public PlayerInfiniteJump infiniteJump;
    public PlayerMidasTouch midasTouch;
    public PlayerSwordAndShield swordAndShield;
    public PlayerMultiplayerBaseball baseball;
    public PlayerMultiplayerInk ink;
    public PlayerMultiplayerMines mines;
    public PlayerMultiplayerMinesTap minesTap;
    
    [Header("Parachute")]
    public PlayerParachute parachute;

    [Header("Movement states")]
    public Animated.Animation animationStanding;
    public Animated.Animation animationRunning;
    public Animated.Animation animationRunningPanicked;
    public Animated.Animation animationRunningTooFast;
    public Animated.Animation animationJumping;
    public Animated.Animation animationJumpingDown;
    public Animated.Animation animationDoubleJumping;
    public Animated.Animation animationStompFrame;
    public Animated.Animation animationAfterStomp;
    public Animated.Animation animationWallSliding;
    public Animated.Animation animationIceSliding;
    public Animated.Animation animationPoleSliding;
    public Animated.Animation animationPoleSlidingBackArm;
    public Animated.Animation animationPoleSlidingTwist;
    public Animated.Animation animationSpring;
    public Animated.Animation animationHanging;
    public Animated.Animation animationBall;
    public Sprite animationHangingFrontArm;
    public Animated.Animation animationHit;
    public Animated.Animation animationDead;
    public Animated.Animation animationRespawn;
    public Animated.Animation animationGrabSide;
    public Sprite animationGrabSideFrontArm;
    public Animated.Animation animationGrabStand;
    public Sprite animationGrabStandFrontArm;
    public Sprite animationBlankSprite;
    [Header("Sunken Island-only animations")]
    public Animated.Animation animationHoldingBreath;
    public Animated.Animation animationSwimmingSurface;
    public Animated.Animation animationSwimmingCeiling;
    [Header("Air Theme-only animations")]
    public Animated.Animation animationWithParachuteOpened;
    public Animated.Animation animationWithParachuteClosed;

    [Header("Transitions")]
    public Animated.Animation animationTransitionWallBounce;
    public Animated.Animation animationTransitionWallBouncePanicked;
    public Animated.Animation animationTransitionRunToWallSlide;
    public Animated.Animation animationTransitionWallSlideToRun;
    public Animated.Animation animationTransitionWallSlideToDrop;
    public Animated.Animation animationTransitionJumpToJumpDown;
    public Animated.Animation animationTransitionSwimToSwimUp;
    [Header("Dust, particles, effects")]
    public Animated.Animation animationWallSlideDust;
    public Animated.Animation animationDustParticle;
    public Animated.Animation animationDustJump;
    public Animated.Animation animationDustJumpFromWallSlide;
    public Animated.Animation animationDustLandParticle;
    public Animated.Animation animationRunTooFastSweat;

    protected Animated.Animation transitionAnimation;
    private Vector2 velocityLastFrame;
    private int forceWalkLeftTime = 0;
    private int forceWalkRightTime = 0;
    private CoyoteTime coyoteTime;
    private int jumpIfAbleTime = 0;
    protected int jumpsSinceOnGround = 0;
    private int deadTime = 0;
    private int wallGrabDisableTime = 0;
    private int wallBounceTime = 0;
    private int timeSinceJump = 0;
    private int timeSinceDustParticle = 0;
    private int dustParticleTrailRemainingTime = 0;
    private bool bouncedOffEnemy = false;
    private int disableSlideOnBambooTime = 0;
    protected bool runPanicking = false;
    private TurningInPlaceDetails turningInPlaceDetails;
    private StickyBlock.StuckData stuckToStickyBlock = null;
    private Pipe insidePipe = null;
    private WaterCurrent insideWaterCurrent = null;
    private AirCurrent insideAirCurrent = null;
    private Box insideBox = null;
    private HidingPlace insideHidingPlace = null;
    private Minecart insideMinecart = null;
    private MineCannon insideMineCannon = null;
    private Bamboo onBamboo = null;
    private SpaceBooster insideSpaceBooster = null;
    private CityCannon insideCityCannon = null;
    private EntranceLift insideEntranceLift = null;
    private BonusLift insideBonusLift = null;
    private AirlockAndCannon insideAirlockCannon = null;
    private OpeningTutorialLift insideOpeningTutorialLift = null;
    private Rocket insideRocket = null;
    private List<Raycast.Result> probesThisFrame = new List<Raycast.Result>();
    private int finishMoveUpwardsTime = 0;
    private bool firstJump = true;
    private int grabItemSmoothTime = 0;
    private bool wasSweatingLastFrame = false;
    private int respawnFrameNumber = 0;
    private Vector2 lastElectricShockPosition;
    private int airborneTime;
    private int wallRunningTime;
    public int timeOfFlyingAsBallWhenFiredFromMineCannon = 0;
    public int noGravityTimeWhenBoostFromFlyingRing = 0;
    public int noGravityTimeWhenBoostFromAirlock = 0;
    private int jumpsInsideWater = 0;
    private bool settleOnWaterSurface = false;
    private int wallBounceInsideWaterTime = 0;
    private bool[] propellerPushActive = new bool[TilePropeller.Directions.Length];
    public Vector2 VelocityLastFrame { get => this.velocityLastFrame; }
    public int TimeSinceJump { get => timeSinceJump; }
    public bool CanAttachToBamboo { get => this.disableSlideOnBambooTime == 0; }
    public Raycast.Result[] ProbesThisFrame { get => probesThisFrame.ToArray(); }
    public Bamboo OnBamboo { get => this.onBamboo; }
    public SpaceBooster InsideSpaceBooster { get => this.insideSpaceBooster; }
    public AirlockAndCannon InsideAirlockCannon { get => this.insideAirlockCannon; }
    public CityCannon InsideCityCannon { get => this.insideCityCannon; }
    public Minecart InsideMinecart { get => this.insideMinecart; }
    public EntranceLift InsideEntranceLift { get => this.insideEntranceLift; set => this.insideEntranceLift = value; } 
    public BonusLift InsideBonusLift { get => this.insideBonusLift; set => this.insideBonusLift = value; } 
    public OpeningTutorialLift InsideOpeningTutorialLift { get => this.insideOpeningTutorialLift; set => this.insideOpeningTutorialLift = value; }
    public Rocket InsideRocket { get => this.insideRocket; }
    public bool IsInsideWater => this.position.y < this.map.waterline?.currentLevel;
    public bool IsCompletelyInsideWater => this.position.y + PivotDistanceAboveFeet < this.map.waterline?.currentLevel;
    public bool IsCompletelyOutsideWater => this.position.y - PivotDistanceAboveFeet > this.map.waterline?.currentLevel;
    public bool IsOnWaterSurface =>
        this.map.waterline?.currentLevel < this.position.y + WaterSurfaceRange &&
        this.map.waterline?.currentLevel > this.position.y - WaterSurfaceRange;
    public bool IsWaterCoveringFeet => IsInsideWater == true && IsCompletelyInsideWater == false;
    private Vector2 JumpStrengthInsideWater => new Vector2(0.15f * (this.facingRight ? 1f : -1f), -0.35f);
    private float TargetSurfacePositionY => this.map.waterline.currentLevel - 0.20f;
    public int parachuteToggleTimer = 0;
    public GravityAtmosphere underInfluenceFromGravityAtmosphere = null;
    public int lastStickyFrame = 0;
    public int lastFrameInsideAirCurrent = -1000;
    public int lastFrameEnteringAirCurrent = -1000;
    public int lastFrameInsideLevelBoundaries = -1000;
    public bool isInsideLava =>
        (this.position.y + LavaDetectionPointY) < this.map.lavaline?.currentLevel;

    protected static readonly float ABCDSensorOffsetX = 8 / 16f;
    protected static readonly float ABSensorLength    = 21 / 16f;  // floor
    private static readonly float CDSensorLength    = 13 / 16f;  // ceiling
    private static readonly float EFSensorLength    = 10 / 16f;  // walls

    public static readonly float PivotDistanceAboveFeet = 15 / 16f;
    public static readonly float HangDistance           = 1;
    public static readonly int   JumpFromGripGroundTime = 25;
    public static readonly float LavaDetectionPointY    = -0.3f;

    protected static readonly float JumpStrength                   = 8.45f / 16f;
    protected static readonly float SecondJumpStrength             = 7f / 16f;
    private static readonly float BopStrength                    = 8.45f / 16f;
    private static readonly float BopWallslidingStrengthX        = 2f / 16f;
    private static readonly float WallJumpStrengthX              = 2.3f / 16f;
    private static readonly float WallJumpStrengthY              = 8.58f / 16f;
    protected static readonly float GravityAcceleration            = 0.4095f / 16f;
    private static readonly float GravityAfterPushPlatform       = 0.3f / 16f;
    private static readonly float AccelerationGround             = 0.05625f / 16f;
    private static readonly float DecelerationGround             = 0.5f / 16f;
    private static readonly float FrictionGround                 = 0.046875f / 16f;
    protected static readonly float AccelerationGroundAutorun      = 0.1f / 16f;
    protected static readonly float DecelerationGroundAutorun      = 0.01f / 16f;
    private static readonly float SlopeAccelerationStrength      = 0.0875f / 16f;
    private static readonly float AccelerationMidair             = 0.09375f / 16f;
    protected static readonly float RunSpeed                       = 2.7f / 16f;
    private static readonly float RunTooFastSpeed                = 5 / 16f;
    protected static readonly float TopSpeed                       = 6 / 16f;
    private static readonly float JumpHorizontalSpeed            = 3.0f / 16f;
    private static readonly float MinimumSpeedToLoop             = 2.5f / 16f;
    private static readonly float WallSlideBecomesRunSpeed       = -3.5f / 16f;
    private static readonly float WallSlideStartMaxSpeed         = 0.7f / 16f;
    private static readonly float WallSlideStartMinSpeed         = -1.4f / 16f;
    private static readonly float WallSlideMinSpeed              = -5.0f / 16f;
    private static readonly float AccelerationGroundOnIce        = 0.02f / 16f;
    private static readonly float SlopeAccelerationStrengthOnIce = 0.22f / 16f;
    private static readonly float DecelerationSlidingOnIce       = 0.02f / 16f;
    private static readonly float WallBounceStrengthSlidingOnIce = 0.5f / 16f;
    private static readonly float IceWallSlideStartMaxSpeed      = 4 / 16f;
    private static readonly float IceWallSlideStartMinSpeed      = -1.25f / 16f;
    private static readonly float IceWallSlideGravity            = -0.05f / 16f;
    private static readonly float CrushDistanceFloorCeiling      = 25 / 16f;
    private static readonly float CrushDistanceWalls             = 20 / 16f;
    private static readonly float AccelerationOnGripGround       = 0.02f / 16f;
    private static readonly float RunSpeedOnGripGround           = 3.5f / 16f;
    private static readonly float MaxSpeedOnGripGround           = 0.40f;
    private static readonly float FreeFloatingJumpStrength       = 4.5f / 16f;
    private static readonly float FreeFloatingJumpMaxSpeed       = 5f / 16f;
    protected static readonly float FreeFloatingSecondJumpStrength = 6.5f / 16f;
    private static readonly float FreeFloatingMinSpeed           = 1f / 16f;
    private static readonly float WaterBuoyancy                  = 0.015f;
    private static readonly float WaterSlowFactor                = 1.5f;
    private static readonly float MinLandSpeedInsideWater        = -0.10f;
    private static readonly float MinHorizontalSpeedInsideWater  = 0.13f;
    private static readonly float MaxHorizontalSpeedInsideWater  = 0.15f;
    private static readonly float MinVerticalSpeedInsideWater    = -0.50f;
    private static readonly float MaxVerticalSpeedInsideWater    = 0.75f;
    private static readonly float WaterSurfaceRange              = 0.50f;
    private static readonly float WaterSurfaceSettleSpeed        = 0.05f;
    private static readonly float MaxHorizontalSpeedInsideCloud  = 1.5f / 16;
    private static readonly float MaxHorizontalParachuteOpenedSpeed = 0.45f;
    private static readonly float MaxHorizontalParachuteClosedSpeed = 0.35f;
    private static readonly float MaxVelocityBoostFlyingRing     = 1f;

    private static readonly int   AllowJumpAfterContactTime = 8;  // coyote time
    private static readonly int   AllowJumpAfterInputTime   = 8;
    private static readonly int   SlideOnBambooCooldown     = 8;
    private static readonly int   WallBounceTime            = 6;
    private static readonly int   WallBounceInsideWaterTime = 6;
    private static readonly int   ParachuteToggleCooldown   = 10;

    public enum State
    {
        Normal,
        Jump,
        WallSlide,
        DropVertically,
        SlidingOnIce,
        TurningInPlace,
        Spring,
        SpringFromPushPlatform,
        Geyser,
        Hit,
        Respawning,
        StuckToSticky,
        InsidePipe,
        InsideWaterCurrent,
        EnteringLocker,
        InsideBox,
        InsideMinecart,
        InsideMineCannon,
        FireFromMineCannon,
        InsideSpaceBooster,
        AttractedToAirlockCannon,
        InsideAirlockCannon,
        InsideCityCannon,
        FireFromCityCannon,
        SlidingOnBamboo,
        RunningOnGripGround,
        InsideLift,
        InsideWater,
        UsingParachuteOpened,
        UsingParachuteClosed,
        BoostFlyingRing,
        InsideAirCurrent,
        SproutSucking,
        KingStompDrop,
        KingHurt,
        YolkLayingEgg,
        GoopBallSpinning,
        GoopBallTurningInPlace,
        InsideRocket,
        Finished
    }

    public enum Orientation
    {
        Normal    = 0,
        RightWall = 1, // the wall on the right-hand-side of the map
        Ceiling   = 2,
        LeftWall  = 3  // the wall on the left-hand-side of the map
    }

    public struct ValidOrientations
    {
        public Orientation a;
        public Orientation? b;
    }

    // These are consolidated so that I remember that I have to set all of them at once
    public struct GroundState
    {
        public static GroundState Airborne(Player player) => new GroundState(
            false,
            player.gravity.Orientation(),
            player.gravity.FloorAngle(),
            false,
            false
        );

        public GroundState(
            bool onGround,
            Orientation orientation,
            float angleDegrees,
            bool groundIsIce,
            bool groundIsShallowSlope
        )
        {
            this.onGround = onGround;
            this.groundIsIce = groundIsIce;
            this.groundIsShallowSlope = groundIsShallowSlope;
            this.orientation = orientation;
            this.angleDegrees = angleDegrees;
        }

        public bool onGround;
        public bool groundIsIce;
        public bool groundIsShallowSlope;
        public Orientation orientation;
        public float angleDegrees;
    }

    public struct Gravity
    {
        // remember, this isn't the same as player.groundState.orientation
        // this only changes in the space theme when the gravity gets flipped.
        public Player player;
        public Vector2? gravityDirection;

        public Orientation Orientation()
        {
            if (this.gravityDirection?.y < -0.707f)
                return Player.Orientation.Normal;
            else if (this.gravityDirection?.x > 0.707f)
                return Player.Orientation.RightWall;
            else if (this.gravityDirection?.y > 0.707f)
                return Player.Orientation.Ceiling;
            else if (this.gravityDirection != null)
                return Player.Orientation.LeftWall;
            else
                return this.player.groundState.orientation;
        }
        public bool IsZeroGravity() => gravityDirection == null;

        public Vector2 Down() =>
            this.gravityDirection ?? this.player.groundState.orientation switch
            {
                Player.Orientation.RightWall => Vector2.right,
                Player.Orientation.Ceiling   => Vector2.up,
                Player.Orientation.LeftWall  => Vector2.left,
                _                            => Vector2.down,
            };

        public Vector2 Right() { var down = Down(); return new Vector2(-down.y,  down.x); }
        public Vector2 Up()    { var down = Down(); return new Vector2(-down.x, -down.y); }
        public Vector2 Left()  { var down = Down(); return new Vector2( down.y, -down.x); }

        public int FloorAngle()
        {
            var right = Right();
            var angleDegrees = Mathf.Atan2(right.y, right.x) * 180 / Mathf.PI;
            var angleDegreesAsInt = Mathf.FloorToInt(angleDegrees);
            angleDegreesAsInt %= 360;
            if (angleDegreesAsInt < 0)
                angleDegreesAsInt += 360;
            return angleDegreesAsInt;
        }
        public int RightWallAngle() => (FloorAngle() + 90) % 360;
        public int CeilingAngle()   => (FloorAngle() + 180) % 360;
        public int LeftWallAngle()  => (FloorAngle() + 270) % 360;

        public float VelocityRight
        {
            get => Vector2.Dot(this.player.velocity, Right());
            set {
                var right = Right();
                var across = new Vector2(-right.y, right.x);
                var speedRight = Vector2.Dot(right, this.player.velocity);
                var speedAcross = Vector2.Dot(across, this.player.velocity);
                speedRight = value;
                this.player.velocity = (right * speedRight) + (across * speedAcross);
            }
        }

        public float VelocityUp
        {
            get => Vector2.Dot(this.player.velocity, Up());
            set {
                var up = Up();
                var across = new Vector2(-up.y, up.x);
                var speedUp = Vector2.Dot(up, this.player.velocity);
                var speedAcross = Vector2.Dot(across, this.player.velocity);
                speedUp = value;
                this.player.velocity = (up * speedUp) + (across * speedAcross);
            }
        }

        public Vector2 VelocityInGravityReferenceFrame
        {
            get => new Vector2(
                Vector2.Dot(this.player.velocity, Right()),
                Vector2.Dot(this.player.velocity, Up())
            );
            set {
                this.player.velocity = (Right() * value.x) + (Up() * value.y);
            }
        }
    }

    private struct CoyoteTime
    {
        public int time;
        public float lastSurfaceAngleDegrees;
    }

    public struct TurningInPlaceDetails
    {
        public float targetAngleDegrees;
        public Vector2 currentCenter;
        public Vector2 idealCenter;
    };

    public Character Character()
    {
        if (this is PlayerPuffer) return global::Character.Puffer;
        if (this is PlayerSprout) return global::Character.Sprout;
        if (this is PlayerGoop  ) return global::Character.Goop  ;
        if (this is PlayerKing  ) return global::Character.King  ;
        return global::Character.Yolk;
    }

    public void Init(Map map, int x, int y)
    {
        this.map            = map;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.animated       = GetComponent<Animated>();

        this.backArmSpriteRenderer =
            this.transform.Find("Back Arm Sprite").GetComponent<SpriteRenderer>();

        this.frontArmSpriteRenderer =
            this.transform.Find("Front Arm Sprite").GetComponent<SpriteRenderer>();

        this.gravity = new Gravity { player = this, gravityDirection = Vector2.down };

        this.position    = new Vector2(x, y + PivotDistanceAboveFeet);
        this.velocity    = Vector2.zero;
        this.state       = State.Normal;
        this.groundspeed = 0;
        this.groundState = new GroundState(true, Orientation.Normal, 0, false, false);
        this.facingRight = true;

        this.checkpointPosition = this.position;

        this.squashAndStretch.Init(this);
        this.invincibility.Init(this);
        this.petYellow.Init(this);
        this.petTornado.Init(this);
        this.shield.Init(this);
        this.infiniteJump.Init(this);
        this.midasTouch.Init(this);
        this.swordAndShield.Init(this);
        this.baseball.Init(this);
        this.ink.Init(this);
        this.mines.Init(this);
        this.minesTap.Init(this, numberOfMines: 5);
        this.parachute.Init(this);

        LoadSavedCharacterUpgradeLevel();
    }

    public void LoadSavedCharacterUpgradeLevel()
    {
        int upgradeLevel = SaveData.GetCharacterUpgradeLevelCurrentlySelected(
            SaveData.GetSelectedCharacter()
        );
        ChangeCharacterUpgradeLevel(upgradeLevel);
    }

    public virtual void ChangeCharacterUpgradeLevel(int upgradeLevel) { }

    protected Vector2 Forward()
    {
        switch (this.groundState.orientation)
        {
            case Orientation.Normal:     return new Vector2( 1,  0);
            case Orientation.RightWall:  return new Vector2( 0,  1);
            case Orientation.Ceiling:    return new Vector2(-1,  0);
            case Orientation.LeftWall:   return new Vector2( 0, -1);
        }
        return new Vector2(1, 0);
    }

    private Vector2 Up()
    {
        switch (this.groundState.orientation)
        {
            case Orientation.Normal:     return new Vector2( 0,  1);
            case Orientation.RightWall:  return new Vector2(-1,  0);
            case Orientation.Ceiling:    return new Vector2( 0, -1);
            case Orientation.LeftWall:   return new Vector2( 1,  0);
        }
        return new Vector2(0, 1);
    }

    protected Raycast.Result Probe(
        Raycast raycast,
        float relativeForward,
        float relativeUp,
        float directionForward,
        float directionUp,
        float length
    )
    {
        Vector2 forward = Forward();
        Vector2 up      = Up();

        Vector2 start = this.position;
        start.x += forward.x * relativeForward + up.x * relativeUp;
        start.y += forward.y * relativeForward + up.y * relativeUp;

        Vector2 direction;
        direction.x = forward.x * directionForward + up.x * directionUp;
        direction.y = forward.y * directionForward + up.y * directionUp;

        var result = raycast.Arbitrary(start, direction, length);
        this.probesThisFrame.Add(result);
        return result;
    }

    public void Advance()
    {
        // Debug.Log(string.Format(
        //     "[{0}]   " +
        //     "<color=grey>pos</color> <color=white><b>({1}, {2})</b></color> " +
        //     "<color=grey>vel</color> <color=white><b>({3}, {4})</b></color> " +
        //     "<color=grey>grav</color> <color=white><b>{5}</b></color> " +
        //     "<color=grey>gs</color> <color=white><b>({6}/{7}° ... {8})</b></color> " +
        //     "<color=grey>|</color> <color=white><b>{9} {10} {11}</b></color>",
        //     this.map.frameNumber,
        //     this.position.x.ToString("0.0000"),
        //     this.position.y.ToString("0.0000"),
        //     this.velocity.x.ToString("0.0000"),
        //     this.velocity.y.ToString("0.0000"),
        //     this.gravity.gravityDirection.HasValue ?
        //         this.gravity.gravityDirection.ToString() :
        //         "None",
        //     this.groundState.onGround,
        //     this.groundState.angleDegrees,
        //     this.groundspeed.ToString("0.0000"),
        //     this.facingRight ? "→" : "←",
        //     this.layer,
        //     this.state
        // ));

        this.probesThisFrame.Clear();
        this.velocityLastFrame = this.velocity;

        if (this.alive == false)
        {
            this.velocity = Vector2.zero;
            UpdateSprite();

            this.deadTime += 1;
            if (this.deadTime > 80)
                ResetToCheckpoint(); // sets this.alive = true
        }

        if (this.alive == true)
        {
            if (this.state == State.Respawning)
            {
                UpdateSprite();

                if(this.map.frameNumber == this.respawnFrameNumber + 10)
                {
                    Audio.instance.PlaySfx(Assets.instance.sfxPlayerRespawn);
                }

                if(this is PlayerYolk)
                {
                    (this as PlayerYolk).AdvanceRespawning();
                }
            }
            else if (this.state == State.InsidePipe)
            {
                this.insidePipe.AdvancePlayer();
                UpdateSprite();
                AdvancePowerups();
            }
            else if(this.state == State.InsideBox)
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );
                
                if (this.alive == true)
                {
                    HandleJump(raycast);
                }
                UpdateSprite();
                AdvancePowerups();
            }
            else if(this.state == State.InsideMinecart)
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );
                
                if (this.alive == true)
                {
                    HandleJump(raycast);
                }
                UpdateSprite();
                AdvancePowerups();
            }
            else if(this.state == State.InsideMineCannon)
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );
                
                if (this.alive == true)
                {
                    HandleJump(raycast);
                }
                UpdateSprite();
            }
            else if(this.state == State.EnteringLocker)
            {
                UpdateSprite();
            }
            else if(this.state == State.InsideLift)
            {
                UpdateSprite();
            }
            else if (this.state == State.InsideWaterCurrent)
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );
                
                this.insideWaterCurrent?.AdvancePlayer();
                if (this.alive == true)
                {
                    HandleJump(raycast);
                }
                UpdateSprite();
                AdvancePowerups();
            }
            else if (this.state == State.InsideAirCurrent)
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );
                
                this.insideAirCurrent?.AdvancePlayer();
                if (this.alive == true)
                {
                    HandleJump(raycast);
                }
                UpdateSprite();
                AdvancePowerups();
            }
            else if (this.state == State.Finished)
            {
                // move player upwards while playing 'spring' animation
                this.position.y += this.velocity.y;
                UpdateSprite();

                // stop player after moving upwards for enough time
                if(finishMoveUpwardsTime > 0)
                {
                    finishMoveUpwardsTime--;

                    if(finishMoveUpwardsTime <= 0)
                    {
                        finishMoveUpwardsTime = 0;
                        this.velocity = Vector2.zero;
                        //IngameUI.instance.winMenu.Open();
                    }
                }
            }
            else
            {
                var raycast = new Raycast(
                    this.map, this.position, layer: this.layer, isCastByPlayer: true
                );

                UpdateGravity();
                UpdateWater();
                UpdateLava();
                HandleControlsAndIntegrate(raycast);
                HandleCrush(raycast);
                if (this.alive == true)
                {
                    if(this is PlayerSprout)
                    {
                        (this as PlayerSprout).HandleSuckingInput();
                    }
                    else if(this is PlayerKing)
                    {
                        (this as PlayerKing).HandleStompDropInput();
                    }
                    else if(this is PlayerYolk)
                    {
                        (this as PlayerYolk).HandleLayingEggInput();
                    }
                    else if(this is PlayerGoop)
                    {
                        (this as PlayerGoop).HandleBallSpinningInput();
                    }

                    HandleEFWallSensors(raycast);
                    HandleCDCeilingSensors(raycast);
                    HandleABFloorSensors(raycast);
                    HandleJump(raycast);
                    DropOffLoopIfNotFastEnough();
                    HandleTriggers();
                }
                UpdateSprite();
                CreateParticles();
                AdvancePowerups();
            }
        }
    }

    public virtual bool ShouldPerformSubstepping() =>
        this.alive == true &&
        this.state != State.Respawning &&
        this.state != State.InsidePipe &&
        this.state != State.EnteringLocker &&
        this.state != State.InsideBox &&
        this.state != State.InsideMinecart &&
        this.state != State.InsideMineCannon &&
        this.state != State.InsideLift &&
        this.state != State.GoopBallSpinning &&
        this.state != State.Finished;

    public bool ShouldCollideWithItems() =>
        this.alive == true &&
        this.state != State.Respawning &&
        this.state != State.InsideMinecart &&
        this.state != State.InsidePipe;

    public bool ShouldCollideWithEnemies() =>
        this.alive == true &&
        this.state != State.Respawning &&
        this.state != State.InsidePipe;

    public void CheckAllSensors()
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        var raycast = new Raycast(
            this.map, this.position, layer: this.layer, isCastByPlayer: true
        );

        HandleCrush(raycast);
        if (this.alive == false) return;

        HandleEFWallSensors(raycast);
        HandleCDCeilingSensors(raycast);
        HandleABFloorSensors(raycast);
    }

    private void UpdateGravity()
    {
        Orientation? SampleAt(Vector2 mapPosition)
        {
            var chunk = this.map.NearestChunkTo(mapPosition);
            var gravityArea = chunk.GravityAreaAt(
                Mathf.FloorToInt(mapPosition.x),
                Mathf.FloorToInt(mapPosition.y)
            );
            if (gravityArea != null)
                return gravityArea.gravityDirection;
            else
                return chunk.gravityDirection;
        }

        Vector2? intendedGravity = SampleAt(this.position) switch
        {
            Orientation.Normal    => Vector2.down,
            Orientation.RightWall => Vector2.right,
            Orientation.Ceiling   => Vector2.up,
            Orientation.LeftWall  => Vector2.left,
            _                     => null
        };
        float padding = (intendedGravity != null) ? 1 : 0.45f;

        // if two sequential chunks' gravity points towards each other, you can get
        // caught on the seam between them. to avoid this, we can hold the previous
        // gravity just a little bit longer to get us away from the old chunk

        // if we are dropping into a zero-g chunk, then the amount of padding we give
        // to the higher chunk's influence of gravity decides how fast the player falls
        // back into the lower chunk after poking out of the top of it

        if (this.gravity.Orientation() == Orientation.Ceiling &&
            (intendedGravity == null || intendedGravity == Vector2.down) &&
            this.velocity.y > 0)
        {
            var sampleBelow = SampleAt(this.position.Add(0, -padding));
            if (sampleBelow == Orientation.Ceiling)
                intendedGravity = Vector2.up;
        }
        else if (this.gravity.Orientation() == Orientation.Normal &&
            (intendedGravity == null || intendedGravity == Vector2.up) &&
            this.velocity.y < 0)
        {
            var sampleAbove = SampleAt(this.position.Add(0, padding));
            if (sampleAbove == Orientation.Normal)
                intendedGravity = Vector2.down;
        }

        // now ... atmospheres require their own special handling.

        if (this.state == State.Jump /*&& this.jumpsSinceOnGround == 1*/)
        {
            var newAtmosphere = GravityAtmosphere.Nearest(
                this.map, this.position, 1.5f, this.underInfluenceFromGravityAtmosphere
            );
            if (newAtmosphere != null)
                this.underInfluenceFromGravityAtmosphere = newAtmosphere.atmosphere;
        }
        else if (this.underInfluenceFromGravityAtmosphere == null)
        {
            // we don't have one -- if we're near one, grab it
            var newAtmosphere = GravityAtmosphere.Nearest(this.map, this.position, 1.5f);
            if (newAtmosphere != null)
                this.underInfluenceFromGravityAtmosphere = newAtmosphere.atmosphere;
        }
        else
        {
            // let go of the one we're attached to if we get far enough away from it
            var atmosphereNearestPointToFeet =
                this.underInfluenceFromGravityAtmosphere.NearestPoint(FeetPosition())
                    .positionOfNearestPoint;
            if ((FeetPosition() - atmosphereNearestPointToFeet).sqrMagnitude > 3 * 3)
                this.underInfluenceFromGravityAtmosphere = null;
        }

        if (this.underInfluenceFromGravityAtmosphere != null)
        {
            var current = this.underInfluenceFromGravityAtmosphere;
            var nearestOnCurrent = current.NearestPoint(this.position);

            bool IsNearAngle(float targetAngleDegrees) =>
                Mathf.Approximately(Util.WrapAngleMinus180To180(
                    this.groundState.angleDegrees - targetAngleDegrees
                ), 0);

            if (nearestOnCurrent.amountThroughPath == 0)
            {
                var previous = current.ConnectedAtmosphereBackward();
                if (previous != null)
                {
                    if (this.state == State.Normal &&
                        this.groundState.onGround == true &&
                        IsNearAngle(previous.rotation) == false &&
                        Vector2.Dot(this.velocity, nearestOnCurrent.forward) < 0 &&
                        GravityAtmosphere.NeedsTurningInPlace(previous, current))
                    {
                        this.state = State.TurningInPlace;
                        this.turningInPlaceDetails = new TurningInPlaceDetails
                        {
                            currentCenter = nearestOnCurrent.positionOfNearestPoint,
                            idealCenter   = nearestOnCurrent.positionOfNearestPoint,
                            targetAngleDegrees = previous.rotation
                        };
                    }

                    var nearestOnPrevious = previous.NearestPoint(this.position);
                    if (nearestOnPrevious.squareDistanceToNearestPoint <
                        nearestOnCurrent.squareDistanceToNearestPoint)
                    {
                        this.underInfluenceFromGravityAtmosphere = previous;
                    }
                }
            }
            else if (nearestOnCurrent.amountThroughPath == 1)
            {
                var next = current.ConnectedAtmosphereForward();
                if (next != null)
                {
                    if (this.state == State.Normal &&
                        this.groundState.onGround == true &&
                        IsNearAngle(next.rotation) == false &&
                        Vector2.Dot(this.velocity, nearestOnCurrent.forward) > 0 &&
                        GravityAtmosphere.NeedsTurningInPlace(current, next))
                    {
                        this.state = State.TurningInPlace;
                        this.turningInPlaceDetails = new TurningInPlaceDetails
                        {
                            currentCenter = nearestOnCurrent.positionOfNearestPoint,
                            idealCenter   = nearestOnCurrent.positionOfNearestPoint,
                            targetAngleDegrees = next.rotation
                        };
                    }

                    var nearestOnNext = next.NearestPoint(this.position);
                    if (nearestOnNext.squareDistanceToNearestPoint <
                        nearestOnCurrent.squareDistanceToNearestPoint)
                    {
                        this.underInfluenceFromGravityAtmosphere = next;
                    }
                }
            }

            intendedGravity =
                this.underInfluenceFromGravityAtmosphere.NearestPoint(this.position).down;
        }

        // if (this.gravity.Orientation() != intendedGravity)
        {
            var oldDirection = this.gravity.gravityDirection;

            this.gravity.gravityDirection = intendedGravity;

            if (oldDirection != this.gravity.gravityDirection)
            {
                if (intendedGravity != null && this.state != State.Normal)
                {
                    // if we're wall-sliding, since the gravity has changed,
                    // we definitely need to detach from the ground
                    // and let the player reattach. if state is something else
                    // entirely, we'll also detach, just in case
                    this.groundState = GroundState.Airborne(this);
                    if (this.state == State.WallSlide)
                        this.state = State.Normal;
                }
            }
        }
    }
    private const float BreathePeriod = 0.2f;
    private const float BreatheAmplitude = 0.15f;
    private float breatheTimer = 0f;

    private void UpdateWater()
    {
        void PlaySplashWater()
        {
            if(Mathf.Abs(this.velocity.y) > 0.125f)
            {
                Particle p = Particle.CreateAndPlayOnce(
                    Assets.instance.sunkenIsland.waterSplash,
                    new Vector2(this.position.x, this.map.waterline.currentLevel - 0.25f),
                    this.map.waterline.waterTransform
                );
            }
        }

        void GenerateBubbles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            var up = Up();
            var forward = Forward();
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + Rnd.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomBubbleParticle,
                    60,
                    this.position +
                        (up      * -PivotDistanceAboveFeet) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                float speed = Rnd.Range(0.04f, 0.08f);
                p.velocity =
                    forward * cos_theta * speed +
                    -up      * sin_theta * speed;
                p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
                p.maximumY = this.map.waterline.currentLevel;
            }
        }

        void GenerateWaterParticles()
        {
            float absVelocityV = Mathf.Abs(this.velocity.y);

            var up = Up();
            var forward = Forward();
            for (var n = 0; n < 10; n += 1)
            {
                var theta =
                    (0f * Mathf.PI / 180) + Rnd.Range(0, Mathf.PI);
                var cos_theta = Mathf.Cos(theta);
                var sin_theta = Mathf.Sin(theta);
                var p = Particle.CreateWithSprite(
                    Assets.instance.sunkenIsland.RandomWaterParticle,
                    40,
                    new Vector2(this.position.x, this.map.waterline.currentLevel) +
                        (forward * cos_theta) +
                        (up      * sin_theta),
                    this.transform.parent
                );
                p.transform.localScale = new Vector3(1f, 1f, 1);
                float hspeed = Rnd.Range(0.03f, 0.06f);
                float vspeed =
                    0.15f + (0.15f * Mathf.Clamp(Mathf.Abs(this.velocity.y), 0f, 1f));
                p.velocity =
                    forward * cos_theta * hspeed +
                    up      * sin_theta * vspeed;
                p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForWaterParticles);
                p.acceleration = Vector2.down * 0.01f;
                p.minimumY = this.map.waterline.currentLevel;
            }
        }

        if(this.map.theme != Theme.SunkenIsland) return;

        if(IsInsideWater == true && this.map.waterline.IsDeadly())
        {
            Hit(this.position);
        }

        if(this.grabbingOntoItem != null) return;

        if(IsOnWaterSurface == true)
        {
            this.breatheTimer += Time.deltaTime;
            float theta = this.breatheTimer / BreathePeriod;
            float breatheDistance = BreatheAmplitude * Mathf.Sin(theta);

            this.velocity.y = Util.Slide(this.velocity.y, 0f, 0.005f);
            float targetSurfacePositionY = TargetSurfacePositionY + breatheDistance;
            this.position.y = Util.Slide(this.position.y, targetSurfacePositionY, 0.01f);
        }
        else
        {
            this.wallBounceInsideWaterTime = 0;
        }

        if(this.state == State.InsideWater && IsInsideWater == false)
        {
            // exit water
            PlaySplashWater();
            Audio.instance.PlaySfx(Assets.instance.sfxWaterOut);

            GenerateWaterParticles();

            this.state = State.Jump;
            this.jumpsSinceOnGround = 1;
            this.jumpsInsideWater = 0;
            this.wallBounceInsideWaterTime = 0;
        }

        if(this.state != State.InsideWater && IsInsideWater == true)
        {
            // enter water
            if(Mathf.Abs(this.velocity.y) < WaterSurfaceSettleSpeed)
            {
                if(this.map.waterline?.IsMoving == true)
                {
                    // nudge to avoid getting stuck on floor when water rises
                    this.velocity.y = -0.05f;
                }
                else if(this.state == State.Jump)
                {
                    // helps settling when on water surface
                    this.velocity.y = 0f;
                    this.settleOnWaterSurface = true;
                }
            }

            PlaySplashWater();
            Audio.instance.PlaySfx(Assets.instance.sfxWaterIn);

            GenerateBubbles();
            GenerateWaterParticles();
            
            this.state = State.InsideWater;

            this.velocity *= 0.85f;

            this.groundState = GroundState.Airborne(this);

            // limit maximum speed
            if(this.velocity.y < MinVerticalSpeedInsideWater)
            {
                this.velocity.y = MinVerticalSpeedInsideWater;
            }
            
            this.jumpsSinceOnGround = 1;
            this.jumpsInsideWater = 0;
            this.wallBounceInsideWaterTime = 0;
        }
    }

    private void UpdateLava()
    {
        if(this.map.theme != Theme.MoltenFortress) return;
        
        if(isInsideLava == true && this.map.lavaline.IsDeadly())
        {
            Hit(this.position);
        }
    }
    private void HandleControlsAndIntegrate(Raycast raycast)
    {
        var forward = Forward();
        // var up      = Up();

        if (this.forceWalkLeftTime   > 0) this.forceWalkLeftTime   -= 1;
        if (this.forceWalkRightTime  > 0) this.forceWalkRightTime  -= 1;

        if (this.disableSlideOnBambooTime > 0)
            this.disableSlideOnBambooTime -= 1;
        if (this.noGravityTimeWhenBoostFromAirlock > 0)
            this.noGravityTimeWhenBoostFromAirlock -= 1;

        // if we're straining over a little bump, this will force us back for a bit
        if (this.groundState.onGround &&
            this.groundState.orientation == Orientation.RightWall &&
            this.groundspeed < 2.0f &&
            this.forceWalkLeftTime == 0)
        {
            this.forceWalkLeftTime = 15;
        }
        else if (this.groundState.onGround &&
            this.groundState.orientation == Orientation.LeftWall &&
            this.groundspeed > -2.0f &&
            this.forceWalkRightTime == 0)
        {
            this.forceWalkRightTime = 15;
        }

        if (GameInput.Player(this).pressedPowerup == true)
        {
            this.map.TriggerPowerup();
        }

        if(this.groundState.onGround == false && this.state != State.WallSlide)
        {
            this.airborneTime += 1;
        }
        else
        {
            this.airborneTime = 0;
            this.portalsEnteredInARowWhileAirborne = 0;
        }

        if(this.groundState.orientation != Orientation.Normal)
        {
            this.wallRunningTime += 1;
        }
        else
        {
            this.wallRunningTime = 0;
        }

        if(this.parachute.IsAvailable == true &&
            this.parachute.visuals.IsVisible == true &&
            this.state != State.UsingParachuteOpened &&
            this.state != State.UsingParachuteClosed)
        {
            this.parachute.Close();
        }

        if(this.parachuteToggleTimer > 0)
        {
            this.parachuteToggleTimer -= 1;
        }

        for (int i = 0; i < this.propellerPushActive.Length; i++)
        {
            if(this.propellerPushActive[i] == true)
            {
                Vector2 pushDirection = TilePropeller.Directions[i];

                switch (this.state)
                {
                    case State.UsingParachuteOpened:
                    {
                        this.velocity += pushDirection * 0.015f;
                    }
                    break;

                    case State.WallSlide:
                    {
                        this.velocity += pushDirection * 0.0025f;
                    }
                    break;

                    default:
                    {
                        this.velocity += pushDirection * 0.01f;
                    }
                    break;
                }
            }
        }
        this.propellerPushActive = new bool[TilePropeller.Directions.Length];

        DieIfFallenOutOfChunk();

        if (this.grabbingOntoItem != null)
        {
            var target = this.grabbingOntoItem.grabInfo.GetGrabPositionInWorldSpace();
            var autoReleaseDistance = this.grabbingOntoItem.grabInfo.autoReleaseDistance;
            var itemGrabType = ItemGrabTypeInGravity(
                this.grabbingOntoItem.grabInfo.grabType
            );
            if (itemGrabType == Item.GrabType.Ceiling)
                target += this.gravity.Down() * HangDistance;

            if (this.grabItemSmoothTime > 0)
            {
                target = Vector2.Lerp(
                    this.position, target, 1.0f / (float)this.grabItemSmoothTime
                );
                this.grabItemSmoothTime -= 1;
            }
            var tVel = this.velocity;
            var tFacingRight = this.facingRight;
            this.velocity = target - this.position;

            for (var n = 0; n < 10; n += 1)
            {
                this.position += this.velocity / 10;
                this.state = State.Normal;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
                HandleTriggers();
            }

            if (itemGrabType == Item.GrabType.Ceiling
                || itemGrabType == Item.GrabType.Floor)
            {
                this.velocity = tVel;
                this.facingRight = tFacingRight;
            }

            if ((this.position - target).magnitude > autoReleaseDistance)
            {
                this.grabbingOntoItem = null;

                if (this.groundState.onGround == false)
                    this.state = State.DropVertically;
            }
        }
        else if (this.state == State.StuckToSticky)
        {
            var target =
                this.stuckToStickyBlock.stickyBlock.position +
                this.stuckToStickyBlock.relativePositionToBlock;
            if ((this.position - target).sqrMagnitude > 25f / 16f)
            {
                // we've moved too far
                this.state = State.Normal;
                JumpAwayFromSticky();
            }
            else
            {
                this.velocity = target - this.position;
                for (var n = 0; n < 5; n += 1)
                {
                    this.position += this.velocity / 5;

                    HandleCrush(raycast);
                    if (this.alive == false) break;

                    HandleEFWallSensors(raycast);
                    HandleCDCeilingSensors(raycast);
                    HandleABFloorSensors(raycast);
                    HandleTriggers();
                }
            }
        }
        else if (this.state == State.SlidingOnBamboo)
        {
            if (this.onBamboo?.twisty == true ?
                Rnd.Range(0, 3) == 0 :
                Rnd.Range(0, 10) == 0)
            {
                Particle.CreateDust(
                    this.transform.parent, 1, 1, this.position,
                    sortingLayerName: "Spikes"
                );
            }

            var lastPosition = this.position;
            var lastBamboo = this.onBamboo;
            var offset = lastBamboo.group?.Offset() ?? Vector2.zero;

            if (this.onBamboo != null)
                this.velocity.x = this.onBamboo.position.x - this.position.x;
            else
                this.velocity.x = 0;
            this.velocity.y -= 0.1f / 16;
            this.position += this.velocity;
            this.jumpsSinceOnGround = 0;
            this.coyoteTime = new CoyoteTime
            {
                time = AllowJumpAfterContactTime,
                lastSurfaceAngleDegrees = this.gravity.FloorAngle()
            };

            this.onBamboo = Bamboo.BambooAt(
                this.map,
                this.position.x - offset.x,
                Mathf.FloorToInt((this.position.y - offset.y) / Tile.Size)
            );
            if (this.onBamboo == null)
            {
                this.state = State.DropVertically;
                this.velocity.x = 0;
                this.disableSlideOnBambooTime = SlideOnBambooCooldown;
            }
            else if (
                lastBamboo != null &&
                lastBamboo.twistyTop == true &&
                (
                    (this.position.y >= lastBamboo.transform.position.y - 0.5f) !=
                    (lastPosition.y >= lastBamboo.transform.position.y - 0.5f)
                )
            )
            {
                this.facingRight = !this.facingRight;
                Audio.instance.PlaySfx(Assets.instance.sfxBambooTurn);
            }
            else if (lastBamboo != null && this.onBamboo != null)
            {
                if (this.onBamboo.ItemDef.ty == lastBamboo.ItemDef.ty + 1 &&
                    lastBamboo.twisty == true &&
                    lastBamboo.twistyTop == false &&
                    this.onBamboo.twistyTop == true)
                {
                    this.facingRight = !this.facingRight;
                }
                else if (this.onBamboo.ItemDef.ty == lastBamboo.ItemDef.ty - 1 &&
                    lastBamboo.twisty == true &&
                    lastBamboo.twistyTop == false &&
                    this.onBamboo.twistyTop == true)
                {
                    this.facingRight = !this.facingRight;
                }
            }
        }
        else if (this.state == State.InsideSpaceBooster)
        {
            this.insideSpaceBooster.AdvancePlayer();
        }
        else if (this.state == State.AttractedToAirlockCannon)
        {
        }
        else if (this.state == State.InsideAirlockCannon)
        {
            this.insideAirlockCannon.AdvancePlayer();
        }
        else if (this.state == State.InsideCityCannon)
        {
            this.insideCityCannon.AdvancePlayer();
        }
        else if (this.state == State.FireFromCityCannon)
        {
            this.velocity += GravityAcceleration * this.gravity.Down();
            this.position += this.velocity;
        }
        else if (this.state == State.FireFromMineCannon)
        {
            if(this.timeOfFlyingAsBallWhenFiredFromMineCannon > MineCannon.MaximumTimeOfFlyingAsBall - MineCannon.MaximumTimeOfFlyingWithoutGravity)
            {
                this.position += this.velocity;
            }
            else
            {
                this.velocity += GravityAcceleration * this.gravity.Down();
                this.position += this.velocity;
            }

            if(this.timeOfFlyingAsBallWhenFiredFromMineCannon > 0)
            {
                this.timeOfFlyingAsBallWhenFiredFromMineCannon--;
                if(this.timeOfFlyingAsBallWhenFiredFromMineCannon == 0)
                {
                    this.state = Player.State.Normal;
                }
            }

            Particle.CreateDust(this.transform.parent, 1, 2, this.position);
        }
        else if (this.state == State.BoostFlyingRing)
        {
            this.velocity.x = Mathf.Clamp(this.velocity.x,
                -MaxVelocityBoostFlyingRing, MaxVelocityBoostFlyingRing);
            this.velocity.y = Mathf.Clamp(this.velocity.y,
                -MaxVelocityBoostFlyingRing, MaxVelocityBoostFlyingRing);

            if(this.noGravityTimeWhenBoostFromFlyingRing > 0)
            {
                this.noGravityTimeWhenBoostFromFlyingRing -= 1;
            }
            else
            {
                this.velocity += GravityAcceleration * this.gravity.Down();
            }

            for (var n = 0; n < 5; n += 1)
            {
                this.position += this.velocity / 5;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
                HandleTriggers();
            }

            if(this.velocity.y < 0f)
            {
                this.state = Player.State.Normal;
                this.noGravityTimeWhenBoostFromFlyingRing = 0;
            }
        }
        else if (this.state == State.InsideBox)
        {
            this.insideBox?.AdvancePlayer();
            this.insideHidingPlace?.AdvancePlayer();
        }
        else if (this.state == State.InsideRocket)
        {
            this.insideRocket.AdvancePlayer();
        }
        else if (this is PlayerSprout && this.state == State.SproutSucking)
        {
            (this as PlayerSprout).HandleSuckingState();
        }
        else if (this is PlayerKing && this.state == State.KingStompDrop)
        {
            (this as PlayerKing).HandleStompDropState(raycast);
        }
        else if (this is PlayerKing && this.state == State.KingHurt)
        {
            (this as PlayerKing).HandleKingHurt();
        }
        else if (this is PlayerYolk && this.state == State.YolkLayingEgg)
        {
            (this as PlayerYolk).HandleLayingEggState(raycast);
        }
        else if (this is PlayerGoop && this.state == State.GoopBallSpinning)
        {
            (this as PlayerGoop).HandleBallSpinningState(raycast);
        }
        else if (this.state == State.TurningInPlace)
        {
            // this state assumes we are in a free-floating chunk without gravity

            var target = this.turningInPlaceDetails.targetAngleDegrees;
            while (target < this.groundState.angleDegrees - 180) target += 360;
            while (target > this.groundState.angleDegrees + 180) target -= 360;

            var newAngleDegrees = Util.WrapAngle0To360(Util.Slide(
                this.groundState.angleDegrees, target, 7
            ));
            var offsetAngleRadians = (newAngleDegrees + 90) * Mathf.PI / 180;

            this.turningInPlaceDetails.currentCenter = Vector3.Lerp(
                this.turningInPlaceDetails.currentCenter,
                this.turningInPlaceDetails.idealCenter,
                0.5f
            );
            this.position = this.turningInPlaceDetails.currentCenter.Add(
                Mathf.Cos(offsetAngleRadians) * PivotDistanceAboveFeet,
                Mathf.Sin(offsetAngleRadians) * PivotDistanceAboveFeet
            );

            var o = ValidOrientationsForAngle(newAngleDegrees);
            Orientation orientation =
                (o.b.HasValue == true && this.groundState.orientation == o.b) ?
                o.b.Value :
                o.a;

            this.groundState = new GroundState(
                true, orientation, newAngleDegrees, false, false
            );
            this.velocity = Forward() * groundspeed;

            if (AngleDifferenceAbsolute(this.groundState.angleDegrees, target) < 1)
            {
                this.state = State.Normal;
            }
        }
        else if (this is PlayerGoop && this.state == State.GoopBallTurningInPlace)
        {
            // this state assumes we are in a free-floating chunk without gravity

            var target = this.turningInPlaceDetails.targetAngleDegrees;
            while (target < this.groundState.angleDegrees - 180) target += 360;
            while (target > this.groundState.angleDegrees + 180) target -= 360;

            var newAngleDegrees = Util.WrapAngle0To360(Util.Slide(
                this.groundState.angleDegrees, target, 7
            ));
            var offsetAngleRadians = (newAngleDegrees + 90) * Mathf.PI / 180;

            this.turningInPlaceDetails.currentCenter = Vector3.Lerp(
                this.turningInPlaceDetails.currentCenter,
                this.turningInPlaceDetails.idealCenter,
                0.5f
            );
            this.position = this.turningInPlaceDetails.currentCenter.Add(
                Mathf.Cos(offsetAngleRadians) * PivotDistanceAboveFeet,
                Mathf.Sin(offsetAngleRadians) * PivotDistanceAboveFeet
            );

            var o = ValidOrientationsForAngle(newAngleDegrees);
            Orientation orientation =
                (o.b.HasValue == true && this.groundState.orientation == o.b) ?
                o.b.Value :
                o.a;

            this.groundState = new GroundState(
                true, orientation, newAngleDegrees, false, false
            );
            this.velocity = Forward() * groundspeed;

            if (AngleDifferenceAbsolute(this.groundState.angleDegrees, target) < 1)
            {
                this.state = State.GoopBallSpinning;
            }
        }
        else if (this.groundState.onGround && this.wallBounceTime > 0)
        {
            this.wallBounceTime -= 1;
        }
        else if (this.groundState.onGround && this.state == State.RunningOnGripGround)
        {
            var targetSpeed = RunSpeedOnGripGround * (this.facingRight ? 1 : -1);
            this.groundspeed = Util.Slide(
                this.groundspeed, targetSpeed, AccelerationOnGripGround
            );
            this.groundspeed = Mathf.Clamp(
                this.groundspeed, -MaxSpeedOnGripGround, MaxSpeedOnGripGround
            );

            this.velocity = forward * this.groundspeed;

            for (var n = 0; n < 5; n += 1)
            {
                this.position += this.velocity / 5;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
                HandleTriggers();
            }

            if (this.groundspeed < 0) this.facingRight = false;
            if (this.groundspeed > 0) this.facingRight = true;
            
            var gripGround = GripGroundUnderneathPlayer(raycast);
            if (gripGround.Length == 0)
                this.state = State.Normal;
        }
        else if (this.groundState.onGround == true && this.autoRunning == true)
        {
            var gripGround = GripGroundUnderneathPlayer(raycast);
            if (gripGround.Length > 0)
            {
                this.state = State.RunningOnGripGround;
                this.lastElectricShockPosition = this.position;
            }

            if (this.state == State.SlidingOnIce)
            {
                var groundAngleRelativeToGravity = Util.WrapAngleMinus180To180(
                    this.groundState.angleDegrees - this.gravity.FloorAngle()
                );
                if (groundAngleRelativeToGravity == 0)
                {
                    this.groundspeed = Util.Slide(
                        this.groundspeed, 0, DecelerationSlidingOnIce
                    );
                    if (this.groundspeed == 0)
                        this.state = State.Normal;
                }
                else
                {
                    this.groundspeed += SlopeAccelerationStrengthOnIce *
                        -Mathf.Sin(groundAngleRelativeToGravity * Mathf.PI / 180f);
                }

                if (this.groundState.groundIsIce == false)
                    this.state = State.Normal;
            }
            else if (
                this.groundState.angleDegrees == this.gravity.FloorAngle() ||
                (
                    this.groundState.groundIsShallowSlope == true &&
                    this.groundState.groundIsIce == false
                ) ||
                this.gravity.IsZeroGravity() == true ||
                this.underInfluenceFromGravityAtmosphere != null
            )
            {
                float force;
                if (this.groundState.groundIsIce == true)
                {
                    force = AccelerationGroundOnIce;
                }
                else
                {
                    force = Mathf.Abs(this.groundspeed) > RunSpeed ?
                        DecelerationGroundAutorun :
                        AccelerationGroundAutorun;
                }
                var target = this.facingRight ? RunSpeed : -RunSpeed;

                if(IsWaterCoveringFeet == true)
                {
                    target = this.facingRight ? RunSpeed / 1.50f : -RunSpeed / 1.50f;
                }

                this.groundspeed = Util.Slide(this.groundspeed, target, force);
            }
            else
            {
                var groundAngleRelativeToGravity = Util.WrapAngleMinus180To180(
                    this.groundState.angleDegrees - this.gravity.FloorAngle()
                );
                var acceleration = SlopeAccelerationStrength *
                    -Mathf.Sin(groundAngleRelativeToGravity * Mathf.PI / 180f);
                this.groundspeed += acceleration;

                if (this.state == State.Normal && this.groundState.groundIsIce == true)
                {
                    if (this.groundspeed < 0 &&
                        this.facingRight == false &&
                        acceleration < 0)
                    {
                        this.state = State.SlidingOnIce;
                    }
                    else if (this.groundspeed > 0 &&
                        this.facingRight == true &&
                        acceleration > 0)
                    {
                        this.state = State.SlidingOnIce;
                    }
                }
            }

            if (this.gravity.IsZeroGravity() == true)
            {
                float theoreticalFloor = this.gravity.FloorAngle();
                float actualFloor = this.groundState.angleDegrees;
                float difference =
                    Util.WrapAngleMinus180To180(actualFloor - theoreticalFloor);
                float factor = Mathf.Cos(difference * Mathf.PI / 180);
                this.velocity = forward * this.groundspeed * factor;
            }
            else
            {
                this.velocity = forward * this.groundspeed;
                if(this.map.theme == Theme.WindySkies && CloudBlock.IsPlayerInsideClouds(this) == true)
                {
                    this.velocity.x = Mathf.Clamp(this.velocity.x, -MaxHorizontalSpeedInsideCloud, MaxHorizontalSpeedInsideCloud);
                }
            }
            for (var n = 0; n < 5; n += 1)
            {
                this.position += this.velocity / 5;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
                HandleTriggers();
            }

            if (this.groundspeed < 0) this.facingRight = false;
            if (this.groundspeed > 0) this.facingRight = true;
        }
        else if (this.groundState.onGround == true)
        {
            var groundAngleRelativeToGravity = Util.WrapAngleMinus180To180(
                this.groundState.angleDegrees - this.gravity.FloorAngle()
            );
            this.groundspeed -= SlopeAccelerationStrength *
                Mathf.Sin(groundAngleRelativeToGravity * Mathf.PI / 180f);

            if (this.forceWalkLeftTime > 0)
                this.groundspeed -= AccelerationGround;
            else if (this.forceWalkRightTime > 0)
                this.groundspeed += AccelerationGround;
            else if (GameInput.Player(this).holdingLeft && this.groundspeed > 0)
                this.groundspeed -= DecelerationGround;
            else if (GameInput.Player(this).holdingRight && this.groundspeed < 0)
                this.groundspeed += DecelerationGround;
            else if (GameInput.Player(this).holdingLeft)
                this.groundspeed -= AccelerationGround;
            else if (GameInput.Player(this).holdingRight)
                this.groundspeed += AccelerationGround;
            else
                this.groundspeed = Util.Slide(this.groundspeed, 0, FrictionGround);

            this.groundspeed = Mathf.Clamp(this.groundspeed, -TopSpeed, TopSpeed);
            this.velocity = forward * this.groundspeed;
            this.position += this.velocity;

            if (this.groundspeed < 0) this.facingRight = false;
            if (this.groundspeed > 0) this.facingRight = true;
        }
        else if (this.state == State.WallSlide)
        {
            Raycast.Result wallSlideRaycast = Probe(
                raycast, 0, 0, this.facingRight ? 1 : -1, 0, EFSensorLength + 2
            );
            if (wallSlideRaycast.IsIce() == true)
            {
                this.gravity.VelocityUp += IceWallSlideGravity;
            }
            else
            {
                const float slow = -1.5f / 16;
                const float fast = -3.0f / 16;
                float accelerationIfSlow = 0.4095f * -0.15f / 16;
                float accelerationIfFast = 0.4095f * -0.25f / 16;
                float s = Mathf.Clamp01(
                    (this.gravity.VelocityUp - slow) / (fast - slow)
                );
                float accel = Mathf.Lerp(accelerationIfSlow, accelerationIfFast, s);
                this.gravity.VelocityUp += accel;

                if (this.gravity.VelocityUp < WallSlideMinSpeed)
                    this.gravity.VelocityUp = WallSlideMinSpeed;
            }

            if (this.gravity.VelocityUp < WallSlideBecomesRunSpeed &&
                wallSlideRaycast.IsIce() == false)
            {
                // look for ground below us and don't run if it's there.
                // the outside sensor is trying to spot where we're about to run
                // into a very narrow column (e.g. 1.5 tiles wide)
                // where running will get you crushed.
                var outsideSensor =
                    this.position.Add(this.facingRight ? -0.85f : 0.85f, 0);
                var floorBelow = Raycast.CombineClosest(
                    raycast.Vertical(this.position, false, 6),
                    raycast.Vertical(outsideSensor, false, 6)
                );
                var isFloorClose =
                    floorBelow.anything == true &&
                    floorBelow.surfaceAngleDegrees == 0;

                // we'll also check whether we are *already* in such a column
                // a second way and forgo the change if so
                var left = raycast.Horizontal(
                    this.position.Add(0, 0),
                    false,
                    CrushDistanceFloorCeiling
                );
                var right = raycast.Horizontal(
                    this.position.Add(0, 0),
                    true,
                    CrushDistanceFloorCeiling
                );
                bool willBeCrushedIfRunning =
                    left.anything == true &&
                    right.anything == true &&
                    left.distance + right.distance < CrushDistanceFloorCeiling;

                // if all of those things are good, we'll start running
                if (this.facingRight == true &&
                    isFloorClose == false &&
                    willBeCrushedIfRunning == false)
                {
                    this.groundState =
                        new GroundState(true, Orientation.RightWall, 90, false, false);
                    this.state       = State.Normal;
                    this.groundspeed = this.velocity.y;
                    this.facingRight = false;
                    this.PlayTransitionAnimation(this.animationTransitionWallSlideToRun);
                }
                else if (this.facingRight == false &&
                    isFloorClose == false &&
                    willBeCrushedIfRunning == false)
                {
                    this.groundState =
                        new GroundState(true, Orientation.LeftWall, 270, false, false);
                    this.state       = State.Normal;
                    this.groundspeed = -this.velocity.y;
                    this.facingRight = true;
                    this.PlayTransitionAnimation(this.animationTransitionWallSlideToRun);
                }
            }

            this.position += this.velocity;
        }
        else if (this.state == State.SpringFromPushPlatform)
        {
            if(this.velocity.x == 0f && this.velocity.y > -PushPlatform.VerticalForce)
                this.velocity.y -= GravityAfterPushPlatform;

            for (int n = 0; n < 10; n += 1)
            {
                this.position += this.velocity / 10;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleABFloorSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleEFWallSensors(raycast);
            }
        }
        else if (this.state == State.Geyser)
        {
            this.velocity.y += Geyser.ImpulseForce;
            this.velocity.y =
                Mathf.Clamp(this.velocity.y, Mathf.NegativeInfinity, Geyser.MaxPlayerVerticalSpeed);

            this.position += this.velocity;
            
            HandleABFloorSensors(raycast);
        }
        else if (this.state == State.InsideWater && this.wallBounceInsideWaterTime > 0)
        {
            this.wallBounceInsideWaterTime -= 1;
        }
        else if(this.state == State.InsideWater)
        {    
            var c = Probe(raycast, -ABCDSensorOffsetX, 0, 0, +1, 1f);
            var d = Probe(raycast, +ABCDSensorOffsetX, 0, 0, +1, 1f);
            var sensor = Raycast.CombineClosest(c, d);

            if(this.settleOnWaterSurface == false && sensor.anything == false)
            {
                // if the player is moving vertically, apply water buoyancy force
                this.velocity += WaterBuoyancy * Vector2.up;

                if(this.map.frameNumber % 4 == 0 && this.velocity.y > 0f)
                {
                    Particle p = Particle.CreateWithSprite(
                        Assets.instance.sunkenIsland.RandomBubbleParticle,
                        60,
                        this.position + (Rnd.insideUnitCircle * 1.15f),
                        this.map.transform);

                    p.FadeOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
                    p.transform.localScale = Vector3.one * 1.5f;
                }
            }
            else if(Mathf.Abs(this.velocity.y) > WaterSurfaceSettleSpeed ||
                this.map.waterline?.IsMoving == true)
            {
                this.settleOnWaterSurface = false;
            }

            if(this.settleOnWaterSurface == true)
            {
                if(this.map.frameNumber % 8 == 0)
                {
                    for (var n = 0; n < 2; n += 1)
                    {
                        // bubbles
                        var p2 = Particle.CreateWithSprite(
                            Assets.instance.sunkenIsland.RandomBubbleParticle,
                            60,
                            this.position.Add(
                                Rnd.Range(0f, 0.5f) * (this.facingRight ? -1f : 1f), -0.65f
                            ),
                            this.transform.parent
                        );
                        p2.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                        float speed = Rnd.Range(0.025f, 0.1f);
                        p2.velocity = (this.velocity * Vector2.left) * speed;
                        p2.acceleration = Vector2.down * Rnd.Range(0, 0.001f);
                        p2.spriteRenderer.sortingLayerName = SortingLayerForDust();
                        p2.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);

                        // water
                        var p3 = Particle.CreateWithSprite(
                            Assets.instance.sunkenIsland.RandomWaterParticle,
                            60,
                            new Vector2(
                                this.position.Add(
                                    0.95f * (this.facingRight ? -1f : 1f), 0f).x,
                                this.map.waterline.currentLevel + 0.01f),
                            this.transform.parent
                        );
                        p3.transform.localScale = new Vector3(0.75f, 0.75f, 1f);
                        float speedH = Rnd.Range(0.025f, 0.1f);
                        float speedV = Rnd.Range(0.075f, 0.175f);
                        p3.velocity = new Vector2(this.velocity.x * speedH, speedV);
                        p3.acceleration = Vector2.down * 0.01f;
                        p3.spriteRenderer.sortingLayerName = SortingLayerForDust();
                        p3.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForWaterParticles);
                        p3.minimumY = this.map.waterline.currentLevel;
                    }
                }
            }
            
            if(this.map.waterline?.IsMoving == false &&
                this.settleOnWaterSurface == false &&
                IsOnWaterSurface == true &&
                Mathf.Abs(this.velocity.y) < 0.01f)
            {
                // helps settling when on water surface
                this.velocity.y = 0f;
                this.settleOnWaterSurface = true;
            }

            // the player is emerging after a jump, reduce horizontal speed to 0
            if(this.jumpsInsideWater > 0 && this.velocity.y > -0.10 || sensor.anything == true)
            {
                this.velocity.x = Util.Slide(this.velocity.x, 0f, 0.003f);
            }
            // if the player is not emerging, enforce horizontal speed minimum value
            else if(Mathf.Abs(this.velocity.x) < MinHorizontalSpeedInsideWater)
            {
                this.velocity.x = MinHorizontalSpeedInsideWater * (this.facingRight ? 1f : -1f);
            }

            // the player has surpassed its maximum horizontal speed
            // (e.g. interacting with a spring underwater)
            // so we need to reduce it back to the limit
            if(Mathf.Abs(this.velocity.x) > MaxHorizontalSpeedInsideWater)
            {
                this.velocity.x = Util.Slide(
                    this.velocity.x,
                    MaxHorizontalSpeedInsideWater * (this.facingRight ? 1f : -1f),
                    0.01f
                );
            }

            if(this.velocity.y > MaxVerticalSpeedInsideWater)
            {
                this.velocity.y = MaxVerticalSpeedInsideWater;
            }

            for (var n = 0; n < 5; n += 1)
            {
                this.position += this.velocity / 5 / WaterSlowFactor;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
            }
            
            if (this.velocity.x < 0) this.facingRight = false;
            if (this.velocity.x > 0) this.facingRight = true;

            this.bouncedOffEnemy = false;
        }
        else if(this.state == State.UsingParachuteOpened)
        {
            if(this.parachute.opened == true)
            {
                this.velocity.y = Util.Slide(this.velocity.y, -0.05f, 0.01f);
            }
            else
            {
                this.velocity.y -= GravityAcceleration * 0.50f;
            }
        
            this.velocity.x = Mathf.Clamp(
                this.velocity.x,
                -MaxHorizontalParachuteOpenedSpeed,
                MaxHorizontalParachuteOpenedSpeed
            );

            this.position += this.velocity;
        }
        else if(this.state == State.UsingParachuteClosed)
        {
            this.velocity.y -= GravityAcceleration;

            this.velocity.x = Mathf.Clamp(
                this.velocity.x,
                -MaxHorizontalParachuteClosedSpeed,
                MaxHorizontalParachuteClosedSpeed
            );

            this.position += this.velocity;
        }
        else
        {
            if (this.state == State.DropVertically &&
                this.gravity.gravityDirection == null)
            {
                this.state = State.Normal;
            }

            if (this.autoRunning == true)
            {
                float? targetXSpeed = null;
                if (this.state == State.DropVertically)
                    targetXSpeed = 0;
                else if (this.gravity.IsZeroGravity() == true)
                    {}
                else if (this.gravity.VelocityRight > JumpHorizontalSpeed)
                    targetXSpeed = JumpHorizontalSpeed;
                else if (this.gravity.VelocityRight < -JumpHorizontalSpeed)
                    targetXSpeed = -JumpHorizontalSpeed;
                else if (this.facingRight == true && this.gravity.VelocityRight < WallJumpStrengthX)
                    targetXSpeed = WallJumpStrengthX;
                else if (this.facingRight == false && this.gravity.VelocityRight > -WallJumpStrengthX)
                    targetXSpeed = -WallJumpStrengthX;

                if (targetXSpeed != null)
                {
                    this.gravity.VelocityRight = Util.Slide(
                        this.gravity.VelocityRight,
                        targetXSpeed.Value,
                        0.05f
                    );
                }
            }
            else
            {
                if (this.velocity.y > 0 && this.velocity.y < 4)
                {
                    if (Mathf.Abs(this.velocity.x) >= 0.125f)
                        this.velocity.x *= 0.96875f;
                }

                if (GameInput.Player(this).holdingRight)
                    this.velocity.x += AccelerationMidair;
                else if (GameInput.Player(this).holdingLeft)
                    this.velocity.x -= AccelerationMidair;
            }

            var gravityDirectionVector = this.gravity.Down();

            if (this.underInfluenceFromGravityAtmosphere != null)
            {
                gravityDirectionVector =
                    this.underInfluenceFromGravityAtmosphere
                        .NearestPoint(this.position).down;
            }

            SafeguardForEndlessPortalDrop();
            
            if (this.gravity.IsZeroGravity() == true)
            {
                var speed = this.velocity.magnitude;
                if (speed > FreeFloatingJumpMaxSpeed)
                {
                    speed = Util.Slide(speed, FreeFloatingJumpMaxSpeed, 0.01f);
                    this.velocity.Normalize();
                    this.velocity *= speed;
                }
                else if (speed < FreeFloatingMinSpeed)
                {
                    speed = Util.Slide(speed, FreeFloatingMinSpeed, 0.001f);
                    if (this.velocity.sqrMagnitude == 0)
                        this.velocity = this.gravity.Down();
                    else
                        this.velocity.Normalize();
                    this.velocity *= speed;
                }

                this.groundState.angleDegrees = Util.WrapAngle0To360(
                    this.groundState.angleDegrees + (this.facingRight ? 0.5f : -0.5f)
                );
            }
            else
            {
                if(this is PlayerPuffer)
                {
                    (this as PlayerPuffer).HandlePufferAirborneVelocity(gravityDirectionVector);
                }
                else
                {
                    if (this.noGravityTimeWhenBoostFromAirlock == 0)
                        this.velocity += GravityAcceleration * gravityDirectionVector;
                }

                if(this.map.theme == Theme.WindySkies && CloudBlock.IsPlayerInsideClouds(this) == true)
                {
                    this.velocity.x = Mathf.Clamp(this.velocity.x, -MaxHorizontalSpeedInsideCloud, MaxHorizontalSpeedInsideCloud);
                    if (this.velocity.y < 0)
                    {
                        this.velocity.y *= 0.5f;
                        this.bouncedOffEnemy = false;
                        ResetDoubleJump();
                    }
                }
            }
            
            for (var n = 0; n < 5; n += 1)
            {
                this.position += this.velocity / 5;

                HandleCrush(raycast);
                if (this.alive == false) break;

                HandleEFWallSensors(raycast);
                HandleCDCeilingSensors(raycast);
                HandleABFloorSensors(raycast);
            }

            if (this.gravity.IsZeroGravity() == false)
            {
                var forwardVector = this.gravity.Right();
                var velocityForwardComponent = Vector2.Dot(forwardVector, this.velocity);
                if (velocityForwardComponent < -0.01f) this.facingRight = false;
                if (velocityForwardComponent > 0.01f) this.facingRight = true;
            }
            
            if(this.jumpsSinceOnGround >= 2 &&
                this.state == State.Jump)
            {
                this.parachute.Toggle();
            }
        }        
    }

    protected void DieIfFallenOutOfChunk()
    {
        if (this.map.theme != Theme.WindySkies && this.map.theme != Theme.GravityGalaxy)
            return;

        if (this.map.theme == Theme.WindySkies)
        {
            // global rules for open ceiling chunks and bottomless pits
            Chunk currentChunk = this.map.NearestChunkTo(this.position);
            Rect cameraRect = this.map.gameCamera.VisibleRectangle();

            if (currentChunk.isHorizontal == true ||
                currentChunk.isTransitionalTunnel == true ||
                currentChunk.entry.type == Chunk.MarkerType.Wall ||
                currentChunk.exit.type == Chunk.MarkerType.Wall)
            {
                // die when falling on a bottomless pit
                if (this.position.y < cameraRect.min.y)
                {
                    Die();
                }

                // get pushed downwards when over the top of an horizontal chunk
                if (this.position.y + 1f > cameraRect.max.y)
                {
                    this.velocity.y = Util.Slide(this.velocity.y, -0.15f, 0.08f);

                    if (this.position.x < currentChunk.xMin)
                        this.velocity.x = Util.Slide(this.velocity.x, 0.15f, 0.04f);

                    if (this.position.x > currentChunk.xMax)
                        this.velocity.x = Util.Slide(this.velocity.x, -0.15f, 0.04f);

                    if (this is PlayerGoop && this.state == State.GoopBallSpinning)
                        (this as PlayerGoop).CancelSpinning();
                }

            }

            if (currentChunk.isHorizontal == false)
            {
                if (this.position.x - 1 < cameraRect.min.x ||
                    this.position.x + 1 > cameraRect.max.x)
                {
                    if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                        (this as PlayerGoop).CancelSpinning();
                }
            }
        }
        else if (this.map.theme == Theme.GravityGalaxy)
        {
            Chunk currentChunk = this.map.gameCamera.currentChunk;
            Rect allowedRect = this.map.gameCamera.VisibleRectangle();

            allowedRect.xMin = Mathf.Min(allowedRect.xMin, currentChunk.xMin);
            allowedRect.xMax = Mathf.Max(allowedRect.xMax, currentChunk.xMax + 1);
            allowedRect.yMin = Mathf.Min(allowedRect.yMin, currentChunk.yMin);
            allowedRect.yMax = Mathf.Max(allowedRect.yMax, currentChunk.yMax + 1);

            if (currentChunk.gravityDirection != null ||
                allowedRect.Contains(this.position) == true)
            {
                this.lastFrameInsideLevelBoundaries = this.map.frameNumber;
            }
            else
            {
                if (this.map.frameNumber > this.lastFrameInsideLevelBoundaries + 60)
                    Die();
            }
        }
    }

    protected void HandleEFWallSensors(Raycast raycast)
    {
        var forward = Forward();
        var eLo     = Probe(raycast, 0,  -8 / 16f, -1, 0, EFSensorLength);
        var eMid    = Probe(raycast, 0,  +4 / 16f, -1, 0, EFSensorLength);
        var eHi     = Probe(raycast, 0, +12 / 16f, -1, 0, EFSensorLength);
        var fLo     = Probe(raycast, 0,  -8 / 16f, +1, 0, EFSensorLength);
        var fMid    = Probe(raycast, 0,  +4 / 16f, +1, 0, EFSensorLength);
        var fHi     = Probe(raycast, 0, +12 / 16f, +1, 0, EFSensorLength);

        if (this.groundState.orientation == Orientation.Normal)
        {
            if (eHi.surfaceAngleDegrees < 270) eHi.anything = false;
            if (fHi.surfaceAngleDegrees > 90) fHi.anything = false;
        }

        if (eLo.anything == true && (eLo.tile, eLo.item) == (fLo.tile, fLo.item))
            eLo.anything = fLo.anything = false;
        if (eMid.anything == true && (eMid.tile, eMid.item) == (fMid.tile, fMid.item))
            eMid.anything = fMid.anything = false;
        if (eHi.anything == true && (eHi.tile, eHi.item) == (fHi.tile, fHi.item))
            eHi.anything = fHi.anything = false;

        var e = Raycast.CombineClosest(eLo, eMid, eHi);
        var f = Raycast.CombineClosest(fLo, fMid, fHi);

        if (e.anything)
        {
            position += forward * (EFSensorLength - e.distance);
            if (this.groundState.onGround == true)
            {
                if (this.groundspeed < 0)
                {
                    float relativeAngle = Util.WrapAngleMinus180To180(
                        e.surfaceAngleDegrees - this.groundState.angleDegrees
                    );

                    if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                    {
                        this.groundState = (this as PlayerGoop).GetMovingLeftGroundState();
                    }
                    else if (
                        this.gravity.IsZeroGravity() == true &&
                        relativeAngle >= -60 &&
                        relativeAngle < 0
                    )
                    {
                        this.groundState = new GroundState
                        {
                            onGround = true,
                            orientation =
                                ValidOrientationsForAngle(e.surfaceAngleDegrees).a,
                            angleDegrees = e.surfaceAngleDegrees,
                            groundIsIce = e.IsIce(),
                            groundIsShallowSlope = false
                        };
                    }
                    else if (this.autoRunning == true &&
                        this.groundState.orientation == this.gravity.Orientation() &&
                        this.groundState.groundIsIce == false)
                    {
                        this.facingRight = true;

                        this.groundspeed *= -1;
                        this.groundspeed = Mathf.Clamp(
                            this.groundspeed, 0.1f / 16f, RunSpeed
                        );

                        this.wallBounceTime = WallBounceTime;
                        this.PlayTransitionAnimation(
                            this.runPanicking ?
                            this.animationTransitionWallBouncePanicked :
                            this.animationTransitionWallBounce
                        );
                    }
                    else if (
                        this.autoRunning == true &&
                        this.groundState.orientation == this.gravity.Orientation() &&
                        this.groundState.groundIsIce == true
                    )
                    {
                        this.facingRight = true;
                        this.groundspeed *= -0.5f;
                        if (this.groundspeed > WallBounceStrengthSlidingOnIce)
                            this.groundspeed = WallBounceStrengthSlidingOnIce;
                    }
                    else if (this.groundState.orientation != this.gravity.Orientation())
                    {
                        this.groundState = GroundState.Airborne(this);
                    }
                    else
                    {
                        this.groundspeed = 0;
                    }
                }
            }
            else if (
                this.gravity.IsZeroGravity() == true ||
                this.underInfluenceFromGravityAtmosphere != null
            )
            {
                var o = ValidOrientationsForAngle(e.surfaceAngleDegrees);
                Land(raycast, e.IsIce(), false, o.a, e.surfaceAngleDegrees);
            }
            else
            {
                if (this.gravity.VelocityRight < -2 / 16f)
                    this.squashAndStretch.Squash(PlayerSquashAndStretch.WallSide.OnLeft);

                if (this.gravity.VelocityRight < 0)
                    this.gravity.VelocityRight = 0;
            }
            NotifyContact(e);
        }
        else if (f.anything)
        {
            position -= forward * (EFSensorLength - f.distance);
            if (this.groundState.onGround == true)
            {
                if (this.groundspeed > 0)
                {
                    float relativeAngle = Util.WrapAngleMinus180To180(
                        f.surfaceAngleDegrees - this.groundState.angleDegrees
                    );

                    if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                    {
                        this.groundState = (this as PlayerGoop).GetMovingRightGroundState();
                    }
                    else if (
                        this.gravity.IsZeroGravity() == true &&
                        relativeAngle > 0 &&
                        relativeAngle <= 60
                    )
                    {
                        this.groundState = new GroundState
                        {
                            onGround = true,
                            orientation =
                                ValidOrientationsForAngle(f.surfaceAngleDegrees).a,
                            angleDegrees = f.surfaceAngleDegrees,
                            groundIsIce = f.IsIce(),
                            groundIsShallowSlope = false
                        };
                    }
                    else if (this.autoRunning == true &&
                        this.groundState.orientation == this.gravity.Orientation() &&
                        this.groundState.groundIsIce == false)
                    {
                        this.facingRight = false;

                        this.groundspeed *= -1;
                        this.groundspeed = Mathf.Clamp(
                            this.groundspeed, -RunSpeed, -0.1f / 16f
                        );

                        this.wallBounceTime = WallBounceTime;
                        this.PlayTransitionAnimation(
                            this.runPanicking ?
                            this.animationTransitionWallBouncePanicked :
                            this.animationTransitionWallBounce
                        );
                    }
                    else if (
                        this.autoRunning == true &&
                        this.groundState.orientation == this.gravity.Orientation() &&
                        this.groundState.groundIsIce == true
                    )
                    {
                        this.facingRight = false;
                        this.groundspeed *= -0.5f;
                        if (this.groundspeed < -WallBounceStrengthSlidingOnIce)
                            this.groundspeed = -WallBounceStrengthSlidingOnIce;
                    }
                    else if (this.groundState.orientation != this.gravity.Orientation())
                    {
                        this.groundState = GroundState.Airborne(this);
                    }
                    else
                    {
                        this.groundspeed = 0;
                    }
                }
            }
            else if (
                this.gravity.IsZeroGravity() == true ||
                this.underInfluenceFromGravityAtmosphere != null
            )
            {
                var o = ValidOrientationsForAngle(f.surfaceAngleDegrees);
                Land(raycast, f.IsIce(), false, o.a, f.surfaceAngleDegrees);
            }
            else
            {
                if (this.gravity.VelocityRight > 2 / 16f)
                    this.squashAndStretch.Squash(PlayerSquashAndStretch.WallSide.OnRight);

                if (this.gravity.VelocityRight > 0)
                    this.gravity.VelocityRight = 0;
            }
            NotifyContact(f);
        }

        if (this.wallGrabDisableTime > 0)
            this.wallGrabDisableTime -= 1;

        if (this.state != State.WallSlide &&
            this.state != State.InsideAirlockCannon &&
            this.groundState.onGround == false &&
            this.grabbingOntoItem == null &&
            this.wallGrabDisableTime == 0 &&
            this.gravity.IsZeroGravity() == false)
        {
            var e2 = Probe(raycast, 0, 0, -1, 0, EFSensorLength + 3 / 16f);
            var f2 = Probe(raycast, 0, 0, +1, 0, EFSensorLength + 3 / 16f);

            void ClampVelocityYForStartOfWallSlide(Raycast.Result sensorResult)
            {
                var min = WallSlideStartMinSpeed;
                var max = WallSlideStartMaxSpeed;

                if (sensorResult.IsIce() == true)
                {
                    min = IceWallSlideStartMinSpeed;
                    max = IceWallSlideStartMaxSpeed;
                }

                this.gravity.VelocityUp = Mathf.Clamp(
                    this.gravity.VelocityUp, min, max
                );
            }

            if (this.bouncedOffEnemy == true && this.gravity.VelocityUp > 0)
            {
                // don't grab walls
                e2.anything = false;
                f2.anything = false;
            }

            if(this is PlayerKing &&
                (this as PlayerKing).CanWallSlide() == false)
            {
                // don't grab walls to allow more leniency on King's stomp ability
                e2.anything = false;
                f2.anything = false;
            }

            if(this.state == State.InsideCityCannon)
            {
                e2.anything = false;
                f2.anything = false;
            }

            if (this.gravity.VelocityRight <= 0 &&
                e2.anything == true &&
                e2.surfaceAngleDegrees == this.gravity.LeftWallAngle())
            {
                if(e2.wallSlideAllowed == true)
                {
                    if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                    {
                        this.groundState = (this as PlayerGoop).GetMovingLeftGroundState();
                    }
                    else if (e2.item is GripGround)
                    {
                        Land(raycast, false, false, Orientation.LeftWall, 270);
                        this.state = State.RunningOnGripGround;
                        this.lastElectricShockPosition = this.position;
                        this.groundspeed = Mathf.Clamp(
                            this.groundspeed, -RunSpeedOnGripGround, RunSpeedOnGripGround
                        );
                    }
                    else if (this.state == State.InsideWater)
                    {
                        if(IsOnWaterSurface == true)
                        {
                            this.wallBounceInsideWaterTime = WallBounceInsideWaterTime;
                        }

                        this.velocity.x = Mathf.Abs(this.velocity.x) * 1f;
                        this.facingRight = true;
                    }
                    else if(CloudBlock.IsPlayerInsideClouds(this))
                    {
                        this.velocity.x = Mathf.Abs(this.velocity.x) * 1f;
                        this.facingRight = true;
                        this.groundspeed *= -1;
                        this.state = State.Normal;
                    }
                    else
                    {
                        this.state = State.WallSlide;
                        this.facingRight = false;
                        ClampVelocityYForStartOfWallSlide(e2);
                        if (this.gravity.VelocityRight < 0)
                            this.gravity.VelocityRight = 0;
                    }
                }
                else
                {
                    if (this.state == State.InsideWater)
                    {
                        if(IsOnWaterSurface == true)
                        {
                            this.wallBounceInsideWaterTime = WallBounceInsideWaterTime;
                        }

                        this.velocity.x = Mathf.Abs(this.velocity.x) * 1f;
                        this.facingRight = true;
                    }
                }
            }
            else if (this.gravity.VelocityRight >= 0 &&
                f2.anything == true &&
                f2.surfaceAngleDegrees == this.gravity.RightWallAngle())
            {
                if(f2.wallSlideAllowed == true)
                {
                    if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                    {
                        this.groundState = (this as PlayerGoop).GetMovingRightGroundState();
                    }
                    else if (f2.item is GripGround)
                    {
                        Land(raycast, false, false, Orientation.RightWall, 90);
                        this.state = State.RunningOnGripGround;
                        this.lastElectricShockPosition = this.position;
                        this.groundspeed = Mathf.Clamp(
                            this.groundspeed, -RunSpeedOnGripGround, RunSpeedOnGripGround
                        );
                    }
                    else if (this.state == State.InsideWater)
                    {
                        if(IsOnWaterSurface == true)
                        {
                            this.wallBounceInsideWaterTime = WallBounceInsideWaterTime;
                        }

                        this.velocity.x = Mathf.Abs(this.velocity.x) * -1f;
                        this.facingRight = false;
                    }
                    else if(CloudBlock.IsPlayerInsideClouds(this))
                    {
                        this.velocity.x = Mathf.Abs(this.velocity.x) * -1f;
                        this.facingRight = false;
                        this.groundspeed *= -1;
                        this.state = State.Normal;
                    }
                    else
                    {
                        this.state = State.WallSlide;
                        this.facingRight = true;
                        ClampVelocityYForStartOfWallSlide(f2);                                                  
                        if (this.gravity.VelocityRight > 0)
                            this.gravity.VelocityRight = 0;
                    }
                }
                else
                {
                    if (this.state == State.InsideWater)
                    {
                        if(IsOnWaterSurface == true)
                        {
                            this.wallBounceInsideWaterTime = WallBounceInsideWaterTime;
                        }

                        this.velocity.x = Mathf.Abs(this.velocity.x) * -1f;
                        this.facingRight = false;
                    }
                }
            }
        }

        if (this.state == State.WallSlide)
        {
            var slideProbe = Probe(
                raycast, 0, 0, this.facingRight ? 1 : -1, 0, EFSensorLength + 5 / 16f
            );
            var angleRelativeToGravity = Util.WrapAngleMinus180To180(
                slideProbe.surfaceAngleDegrees - this.gravity.FloorAngle()
            );
            var minAngle = this.facingRight ? 90  : -135;
            var maxAngle = this.facingRight ? 135 : 90;

            if (this.groundState.onGround == true)
            {
                this.state = State.Normal;
                this.PlayTransitionAnimation(this.animationTransitionWallSlideToRun);
            }
            else if (slideProbe.anything == false ||
                slideProbe.wallSlideAllowed == false ||
                angleRelativeToGravity < minAngle ||
                angleRelativeToGravity > maxAngle)
            {
                var lowerProbe = Probe(
                    raycast, 0, -0.75f,
                    this.facingRight ? 1 : -1, 0, EFSensorLength + 5 / 16f
                );

                this.state = State.DropVertically;
                this.PlayTransitionAnimation(this.animationTransitionWallSlideToDrop);
                this.facingRight = !this.facingRight;

                // this little delay can stop us getting stuck at the top of a conveyor
                if (lowerProbe.item is Conveyor)
                    this.wallGrabDisableTime = 10;
            }
            else
            {
                var correctionAmount =
                    (slideProbe.distance - (EFSensorLength + 1 / 16f)) *
                    (this.facingRight ? 1 : -1);
                this.position += this.gravity.Right() * correctionAmount;
                this.groundspeed =
                    this.gravity.VelocityUp * (this.facingRight ? 1 : -1);
                NotifyContact(Raycast.CombineClosest(slideProbe));
            }
        }
    }

    protected void HandleCDCeilingSensors(Raycast raycast)
    {
        var c = Probe(raycast, -ABCDSensorOffsetX, 0, 0, +1, CDSensorLength);
        var d = Probe(raycast, +ABCDSensorOffsetX, 0, 0, +1, CDSensorLength);
        var sensor = Raycast.CombineClosest(c, d);
        var velocityBefore = this.velocity;

        {
            var fastestDownward =
                FastestContactMovingInDirection(sensor, -Up());
            var playerRelativeToContact = this.velocity - fastestDownward;
            if (Vector2.Dot(playerRelativeToContact, Up()) < 0)
                return;
        }

        if (this.state == State.InsideCityCannon ||
            this.state == State.InsideMineCannon ||
            this.state == State.InsideAirlockCannon)
        {
            c.anything = false;
            d.anything = false;
        }

        if (c.anything || d.anything)
        {
            var sensorAngleRelativeToGravity = Util.WrapAngle0To360(
                sensor.surfaceAngleDegrees - this.gravity.FloorAngle()
            );

            if (sensorAngleRelativeToGravity == 180 && this.gravity.VelocityUp > 2)
                this.squashAndStretch.Squash(PlayerSquashAndStretch.WallSide.Above);

            position -= Up() * ((CDSensorLength + (1 / 16f)) - sensor.distance);

            var surfaceVelocity = Vector2.zero;
            if (sensor.tiles.Length > 0)
                surfaceVelocity = sensor.tiles[0].tileGrid.effectiveVelocity;

            var tangent = new Vector2(
                Mathf.Cos(sensor.surfaceAngleDegrees * Mathf.PI / 180),
                Mathf.Sin(sensor.surfaceAngleDegrees * Mathf.PI / 180)
            );
            var jumpingWithTileCurveDirection =
                (
                    sensorAngleRelativeToGravity > 90 &&
                    sensorAngleRelativeToGravity < 180 &&
                    this.gravity.VelocityRight < 0
                ) ||
                (
                    sensorAngleRelativeToGravity > 180 &&
                    sensorAngleRelativeToGravity < 270 &&
                    this.gravity.VelocityRight > 0
                );

            if (Mathf.Abs(sensorAngleRelativeToGravity - 180) >= 15 &&
                timeSinceJump < 30 &&
                jumpingWithTileCurveDirection == true
            )
            {
                // just continue upwards. collide with the curve tile, but don't
                // reorient to run along it. just skid past it.

                // this is a fix for weird behaviour where you jump off a curve and then
                // immediately hit the next curve tile going up and around the ceiling.
                // all of the jump velocity becomes groundspeed, making pressing jump
                // act more like a boost. basically it feels weird and wrong.
            }
            else if (
                (
                    this.gravity.IsZeroGravity() == true ||
                    this.underInfluenceFromGravityAtmosphere != null ||
                    sensorAngleRelativeToGravity < 15 || // hitting the floor
                    sensorAngleRelativeToGravity > 345   // with your head?
                ) &&
                this.groundState.onGround == false)
            {
                var o = ValidOrientationsForAngle(sensor.surfaceAngleDegrees);
                Land(raycast, sensor.IsIce(), false, o.a, sensor.surfaceAngleDegrees);

                this.groundState = new GroundState(
                    true, o.a, sensor.surfaceAngleDegrees, sensor.IsIce(), false
                );
                this.state = State.Normal;

                var forwardSpeed = Vector2.Dot(this.velocity, tangent);
                if (forwardSpeed > 0) this.facingRight = true;
                if (forwardSpeed < 0) this.facingRight = false;
            }
            else
            {
                var surfaceSpeedY =
                    Vector2.Dot(surfaceVelocity, this.gravity.Up());
                this.gravity.VelocityUp = surfaceSpeedY;
                // this.velocity.y = surfaceVelocity.y;
            }

            NotifyContact(sensor);

            var hitboxWasDestroyed = false;

            foreach (var item in sensor.items)
            {
                item.onCollisionWithPlayersHead?.Invoke();
                if (this.gravity.gravityDirection == null && item.hitboxes.Length == 0)
                    hitboxWasDestroyed = true;
            }

            if (this.gravity.gravityDirection == null && hitboxWasDestroyed == true)
            {
                // if we hit an item and it self-destructed, then bounce away from it
                this.groundState = Player.GroundState.Airborne(this);

                var surfaceTangent = new Vector2(
                    Mathf.Cos(sensor.surfaceAngleDegrees * Mathf.PI / 180),
                    Mathf.Sin(sensor.surfaceAngleDegrees * Mathf.PI / 180)
                );
                var surfaceNormal = new Vector2(surfaceTangent.y, -surfaceTangent.x);
                var withTangent = Vector2.Dot(velocityBefore, surfaceTangent);
                var withNormal  = Vector2.Dot(velocityBefore, surfaceNormal);
                withNormal *= -1;
                this.velocity =
                    (surfaceNormal * withNormal) + (surfaceTangent * withTangent);
            }
        }
    }

    protected void HandleABFloorSensors(Raycast raycast)
    {
        if (this.state == State.TurningInPlace) return;
        if (this.state == State.StuckToSticky) return;
        if (this is PlayerGoop && this.state == State.GoopBallTurningInPlace) return;

        var up = Up();
        var a  = Probe(raycast, -ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
        var b  = Probe(raycast, +ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);

        if (this.state == State.InsideCityCannon ||
            this.state == State.InsideAirlockCannon)
        {
            a.anything = false;
            b.anything = false;
        }
        if (this.state == State.AttractedToAirlockCannon)
        {
            if (this.map.NearestChunkTo(a.End()).isAirlock == true)
                a.anything = true;
            if (this.map.NearestChunkTo(b.End()).isAirlock == true)
                b.anything = true;
        }

        if (this.map.theme == Theme.MoltenFortress &&
            this.map.lavaline.IsSolidIncludingGracePeriod == true)
        {
            if(a.End().y < this.map.lavaline.currentLevel)
            {
                a.anything = true;
                a.distance = a.start.y - this.map.lavaline.currentLevel;
            }

            if(b.End().y < this.map.lavaline.currentLevel)
            {
                b.anything = true;
                b.distance = b.start.y - this.map.lavaline.currentLevel;
            }
        }

        if (this.velocity.y > 0 && this.grabbingOntoItem == null)
        {
            if (a.anything && a.tile?.spec.topOnly == true)
                a.anything = false;
            if (b.anything && b.tile?.spec.topOnly == true)
                b.anything = false;
        }

        var sensor = Raycast.CombineClosest(a, b);
        if (this.groundState.onGround == false)
        {
            var fastestUpward =
                FastestContactMovingInDirection(sensor, this.gravity.Up());
            var playerRelativeToContact = this.velocity - fastestUpward;
            if (Vector2.Dot(playerRelativeToContact, this.gravity.Up()) > 0)
                return;
        }

        if (a.anything || b.anything)
            position += up * (PivotDistanceAboveFeet - (sensor.distance + 0.5f));

        if (a.anything && a.tile != null)
        {
            if (this.groundState.onGround == true ||
                this.gravity.IsZeroGravity() == false)
            {
                var difference = Mathf.Abs(Util.WrapAngleMinus180To180(
                    a.surfaceAngleDegrees - this.groundState.angleDegrees
                ));
                if (difference >= 90)
                    a.anything = false;
            }

            var surfaceRelativeToGravity = Util.WrapAngle0To360(
                a.surfaceAngleDegrees - this.gravity.FloorAngle()
            );
            if (surfaceRelativeToGravity > 90 &&
                surfaceRelativeToGravity < 270 &&
                a.tile.IsOutsideCurve() == true &&
                this.gravity.IsZeroGravity() == false)
                a.anything = false;
        }
        if (b.anything && b.tile != null)
        {
            if (this.groundState.onGround == true ||
                this.gravity.IsZeroGravity() == false)
            {
                var difference = Mathf.Abs(Util.WrapAngleMinus180To180(
                    b.surfaceAngleDegrees - this.groundState.angleDegrees
                ));
                if (difference >= 90)
                    b.anything = false;
            }

            var surfaceRelativeToGravity = Util.WrapAngle0To360(
                b.surfaceAngleDegrees - this.gravity.FloorAngle()
            );
            if (surfaceRelativeToGravity > 90 &&
                surfaceRelativeToGravity < 270 &&
                b.tile.IsOutsideCurve() == true &&
                this.gravity.IsZeroGravity() == false)
                b.anything = false;
        }

        sensor = Raycast.CombineClosest(a, b);

        float oldAngle = this.groundState.angleDegrees;
        float newAngle = sensor.surfaceAngleDegrees; // fallback
        bool isShallowSlope = false;

        if (a.anything || b.anything)
        {
            var sensorTile = sensor.tiles.Length > 0 ? sensor.tiles[0] : null;
            if (sensorTile != null)
            {
                // better information for newAngle, since the sensor
                // isn't aligned with player centerpoint
                newAngle = sensorTile.SurfaceAngleDegrees(
                    position - sensorTile.Position(), -up
                );

                // reject this better information if it disagrees too much
                // with what the sensors gave us. this can happen when walking over
                // onto curve tiles, and believing the better info leads to bad things
                if (AngleDifferenceAbsolute(sensor.surfaceAngleDegrees, newAngle) > 80)
                    newAngle = sensor.surfaceAngleDegrees;

                // player treats 22.5° slopes as flat
                if (sensorTile.spec.shape == Tile.Shape.Slope22BL ||
                    sensorTile.spec.shape == Tile.Shape.Slope22BR)
                {
                    isShallowSlope = true;
                }
            }
        }

        var o = ValidOrientationsForAngle(newAngle);
        var speedIntoSurface = SpeedIntoSurface(sensor);
        PlayerGoop playerGoop = this as PlayerGoop;

        if ((a.anything == true || b.anything == true) &&
            this.groundState.onGround == true &&
            AngleDifferenceAbsolute(newAngle, oldAngle) <= 60)
        {
            float angleDifference =
                Util.WrapAngleMinus180To180(newAngle - oldAngle);
            bool isGroundMovingAway =
                (this.facingRight == true && angleDifference < -15) ||
                (this.facingRight == false && angleDifference > 15);

            if (this.gravity.IsZeroGravity() == true &&
                isGroundMovingAway == true &&
                this.underInfluenceFromGravityAtmosphere == null)
            {
                var downToFeet = new Vector2(
                    Mathf.Sin(oldAngle * Mathf.PI / 180) * PivotDistanceAboveFeet,
                    -Mathf.Cos(oldAngle * Mathf.PI / 180) * PivotDistanceAboveFeet
                );
                var forward = new Vector2(
                    Mathf.Cos(newAngle * Mathf.PI / 180) * PivotDistanceAboveFeet,
                    Mathf.Sin(newAngle * Mathf.PI / 180) * PivotDistanceAboveFeet
                );

                if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                {
                    this.state = State.GoopBallTurningInPlace;
                }
                else
                {
                    this.state = State.TurningInPlace;
                }

                this.turningInPlaceDetails.currentCenter = this.position + downToFeet;
                this.turningInPlaceDetails.idealCenter =
                    sensor.End() + (forward * 0.25f * (this.facingRight ? 1 : -1));
                this.turningInPlaceDetails.targetAngleDegrees = newAngle;
            }
            else
            {
                var bestOrientation = o.a;
                if (o.b.HasValue == true && this.groundState.orientation == o.b.Value)
                    bestOrientation = o.b.Value;
                this.groundState = new GroundState(
                    true, bestOrientation, newAngle, sensor.IsIce(), isShallowSlope
                );
            }

            NotifyContact(sensor);

            if(this is PlayerGoop && playerGoop.turningCornerGroundState.HasValue == true)
            {
                playerGoop.turningCornerGroundState = null;
            }
        }
        else if ((a.anything == true || b.anything == true) &&
            this.groundState.onGround == false &&
            AngleDifferenceAbsolute(this.groundState.angleDegrees, this.gravity.FloorAngle()) < 90 &&
            speedIntoSurface > -1 / 16f)
        {
            var newOrientation = o.a;
            if (o.b != null && o.b == this.gravity.Orientation())
                newOrientation = o.b.Value;

            Land(raycast, sensor.IsIce(), isShallowSlope, newOrientation, newAngle);
            NotifyContact(sensor);

            if(this is PlayerGoop && playerGoop.turningCornerGroundState.HasValue == true)
            {
                playerGoop.turningCornerGroundState = null;
            }
        }
        else if (
            this.groundState.onGround == true &&
            this.gravity.IsZeroGravity() == true
        )
        {
            var forward = this.gravity.Right() * (this.facingRight ? 1 : -1);

            var raycastStart =
                this.position +
                (this.gravity.Down() * (PivotDistanceAboveFeet - 0.25f)) +
                (forward * 9 / 16f);
            var raycastEnd =
                this.position +
                (this.gravity.Down() * (PivotDistanceAboveFeet + 0.5f)) +
                (forward * -9 / 16f);
            var raycastDelta = raycastEnd - raycastStart;
            var downRaycast = raycast.Arbitrary(
                raycastStart, raycastDelta.normalized, raycastDelta.magnitude + 0.25f
            );

            bool shouldTurnInPlace = downRaycast.anything;

            if (downRaycast.item is Portal or AirlockAndCannon)
                shouldTurnInPlace = false;

            if (shouldTurnInPlace == true)
            {
                var downToFeet = (this.gravity.Down() * PivotDistanceAboveFeet);
                var a1 = this.position + (forward * 1) + downToFeet;
                var a2 = this.position - (forward * 1) + downToFeet;
                var b1 = downRaycast.End();
                var b2 = b1 + (this.gravity.Down());
                var corner =
                    Util.LineIntersection(a1, a2, b1, b2) ?? downRaycast.End();

                if(this is PlayerGoop && this.state == State.GoopBallSpinning)
                {
                    this.state = State.GoopBallTurningInPlace;
                }
                else
                {
                    this.state = State.TurningInPlace;
                }

                this.turningInPlaceDetails.currentCenter = this.position + downToFeet;
                this.turningInPlaceDetails.idealCenter = corner;
                this.turningInPlaceDetails.targetAngleDegrees =
                    downRaycast.surfaceAngleDegrees;
            }
            else
            {
                this.groundState = GroundState.Airborne(this);
            }
        }
        else if (this.groundState.onGround == true)
        {
            if(this is PlayerGoop && this.state == State.GoopBallSpinning)
            {
                playerGoop.BallSpinningCornerUpwardsImpulse();

                if(playerGoop.CanTurnCorners())
                {
                    playerGoop.EnterBallSpinningTurnCorner();

                    if(playerGoop.turningCornerGroundState.HasValue == true)
                    {
                        this.groundState = playerGoop.turningCornerGroundState.Value;
                        return;
                    }
                }
            }

            var groundAngleRelativeToGravity = Util.WrapAngleMinus180To180(
                this.groundState.angleDegrees - this.gravity.FloorAngle()
            );
            if (groundAngleRelativeToGravity == 90)
            {
                this.state = State.DropVertically;
                this.PlayTransitionAnimation(this.animationTransitionWallSlideToDrop);
                this.position += this.gravity.Right() * 3 / 16f;
            }
            else if (groundAngleRelativeToGravity == -90)
            {
                this.state = State.DropVertically;
                this.PlayTransitionAnimation(this.animationTransitionWallSlideToDrop);
                this.position += this.gravity.Left() * 3 / 16f;
            }

            this.groundState = GroundState.Airborne(this);
        }
    }

    private void Land(
        Raycast raycast,
        bool groundIsIce,
        bool groundIsShallowSlope,
        Orientation targetOrientation,
        float surfaceAngleDegrees
    )
    {
        bool shouldChangeStateToNormal = true;

        if(this.state == State.InsideWater)
        {
            if(this.velocity.y < MinLandSpeedInsideWater)
            {
                this.velocity.y = MinLandSpeedInsideWater;
            }
            return;
        }
        if (this.state == State.InsideRocket) return;

        if(this is PlayerKing &&
            this.state == State.KingStompDrop &&
            (this as PlayerKing).isDropping == true)
        {
            (this as PlayerKing).StompDropLand();
            shouldChangeStateToNormal = false;
        }

        if(this is PlayerGoop)
        {
            if(this.state == State.GoopBallSpinning)
            {
                shouldChangeStateToNormal = false;
            }
            else
            {
                (this as PlayerGoop).TriggerSplat();
            }
        }

        if(this is PlayerYolk)
        {
            if(this.bouncedOffEnemy == true ||
                this.airborneTime > 90 ||
                this.wallRunningTime > 60)
            {
                (this as PlayerYolk).TriggerPanic();
            }   
        }

        {
            var speedDown = Vector2.Dot(this.velocity, this.gravity.Down());
            var speedRight = Vector2.Dot(this.velocity, this.gravity.Right());
            if (speedDown > 0.28f)
                speedDown = 0.28f;
            this.velocity =
                (speedDown * this.gravity.Down()) + (speedRight * this.gravity.Right());
        }

        var tangent = new Vector2(
            Mathf.Cos(surfaceAngleDegrees * Mathf.PI / 180),
            Mathf.Sin(surfaceAngleDegrees * Mathf.PI / 180)
        );
        var normal       = new Vector2(-tangent.y, tangent.x);
        var speedInto    = Vector2.Dot(this.velocity, -normal);
        this.groundspeed = Vector2.Dot(this.velocity, tangent);

        if(this.grabbingOntoItem == null)
        {
            if (this.groundspeed > 0) this.facingRight = true;
            if (this.groundspeed < 0) this.facingRight = false;
        }

        if (this.gravity.IsZeroGravity() == true ||
            this.underInfluenceFromGravityAtmosphere != null)
        {
            var direction = Util.Sign(this.groundspeed);
            if (direction == 0)
                direction = (this.facingRight ? 1 : -1);
            var magnitude = Mathf.Abs(this.groundspeed);
            magnitude = Mathf.Clamp(magnitude, RunSpeed * 0.2f, RunSpeed * 1.2f);
            this.groundspeed = magnitude * direction;
        }

        if (speedInto > 5 / 16f)
        {
            speedInto = 5 / 16f;
            this.velocity = (speedInto * -normal) + (this.groundspeed * tangent);
        }

        if(shouldChangeStateToNormal == true)
        {
            this.state = State.Normal;
        }
        
        this.groundState = new GroundState(
            true,
            targetOrientation,
            surfaceAngleDegrees,
            groundIsIce,
            groundIsShallowSlope
        );

        if(this.parachute.ActiveTime > 10)
        {
            this.parachute.Exit();
        }

        if (HasWallOnSide(raycast, this.facingRight, 6 / 16f))
            this.facingRight = !this.facingRight;

        if (speedInto > 2 / 16f)
        {
            for (var n = 0; n < 10; n += 1)
            {
                float horizontalSpeed =
                    ((n % 2 == 0) ? 1 : -1) * Rnd.Range(0.4f, 2.3f) / 16;
                float verticalSpeed = Rnd.Range(-0.2f, 0.2f) / 16;

                var p = Particle.CreateAndPlayOnce(
                    this.animationDustLandParticle,
                    this.position - (normal * PivotDistanceAboveFeet),
                    this.transform.parent
                );
                p.velocity = (tangent * horizontalSpeed) + (normal * verticalSpeed);
                p.acceleration = p.velocity * -0.05f / 16;
                p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                p.spriteRenderer.sortingLayerName = SortingLayerForDust();
            }

            this.squashAndStretch.Squash(PlayerSquashAndStretch.WallSide.Below);
        }
    }

    private Vector2 FastestContactMovingInDirection(
        Raycast.CombinedResults raycastResults, Vector2 direction
    )
    {
        if (raycastResults.anything == false) return Vector2.zero;

        var vectors = new List<Vector2>();
        foreach (var t in raycastResults.tiles)
        {
            vectors.Add(t.tileGrid.effectiveVelocity);
        }
        foreach (var i in raycastResults.items)
        {
            if (i is BrainBox brainBox)
                vectors.Add(brainBox.Velocity);
            else if (i is TouchPlatformBlock block)
                vectors.Add(block.EffectiveVelocity);
            else
                vectors.Add(Vector2.zero);
        }

        return vectors.MaxBy(v => Vector2.Dot(v, direction));
    }

    private ValidOrientations ValidOrientationsForAngle(float angle)
    {
        if (angle ==  45) return new ValidOrientations { a = Orientation.Normal,    b = Orientation.RightWall };
        if (angle == 135) return new ValidOrientations { a = Orientation.RightWall, b = Orientation.Ceiling   };
        if (angle == 225) return new ValidOrientations { a = Orientation.Ceiling,   b = Orientation.LeftWall  };
        if (angle == 315) return new ValidOrientations { a = Orientation.LeftWall,  b = Orientation.Normal    };

        if      (angle >  45 && angle <= 135) return new ValidOrientations { a = Orientation.RightWall };
        else if (angle > 135 && angle <= 225) return new ValidOrientations { a = Orientation.Ceiling };
        else if (angle > 225 && angle <= 315) return new ValidOrientations { a = Orientation.LeftWall };

        return new ValidOrientations { a = Orientation.Normal };
    }

    private float SpeedIntoSurface(Raycast.CombinedResults sensor)
    {
        var surfaceVelocity = Vector2.zero;
        if (sensor.tiles.Length > 0)
            surfaceVelocity = sensor.tiles[0].tileGrid.effectiveVelocity;
        var surfaceNormal = new Vector2(
            Mathf.Cos((sensor.surfaceAngleDegrees + 90) * Mathf.PI / 180),
            Mathf.Sin((sensor.surfaceAngleDegrees + 90) * Mathf.PI / 180)
        );
        return -Vector2.Dot(surfaceNormal, this.velocity - surfaceVelocity);
    }

    private GripGround[] GripGroundUnderneathPlayer(Raycast raycast)
    {
        var r1 = Probe(raycast, -0.5f, 0, 0, -1, PivotDistanceAboveFeet + 0.5f);
        var r2 = Probe(raycast,     0, 0, 0, -1, PivotDistanceAboveFeet + 0.5f);
        var r3 = Probe(raycast,  0.5f, 0, 0, -1, PivotDistanceAboveFeet + 0.5f);
        var r = Raycast.CombineClosest(r1, r2, r3);
        return r.items.OfType<GripGround>().ToArray();
    }

    private void HandleJump(Raycast raycast)
    {
        this.timeSinceJump += 1;

        {
            var grabAndSwingEnemy = GrabAndSwingEnemy.NearestToPlayer(this);
            if (grabAndSwingEnemy != null)
            {
                return;
            }

            bool onTopOfTramplePlatform = TramplePlatform.IsOnTopOfAnyTrample(this);
            if(onTopOfTramplePlatform == true)
            {
                return;
            }

            bool IsGrabOntoUnskippableItem()
            {
                if(this.grabbingOntoItem != null &&
                    this.grabbingOntoItem is TemperatureGate)
                {
                    TemperatureGate temperatureBarrier =
                        this.grabbingOntoItem as TemperatureGate;

                    return temperatureBarrier.IsBeingActivatedByPlayer();
                }

                if (this.grabbingOntoItem is MultiplayerHeadset)
                    return true;

                return false;
            }

            if(IsGrabOntoUnskippableItem())
            {
                return;
            }

            if(IsOverTopOfHorizonalChunk())
            {
                return;
            }
        }

        if (this.groundState.onGround == true)
        {
            this.coyoteTime.time = AllowJumpAfterContactTime;
            this.coyoteTime.lastSurfaceAngleDegrees = this.groundState.angleDegrees;
            this.bouncedOffEnemy = false;
            this.firedFromPipeCannon = false;
        }
        else if (
            this.state == State.WallSlide ||
            this.state == State.StuckToSticky ||
            this.grabbingOntoItem != null
        )
        {
            this.coyoteTime.time = AllowJumpAfterContactTime;
            this.coyoteTime.lastSurfaceAngleDegrees = this.gravity.FloorAngle();
            this.bouncedOffEnemy = false;
            this.firedFromPipeCannon = false;
        }
        else if (this.coyoteTime.time > 0)
        {
            this.coyoteTime.time -= 1;
        }

        if (this.jumpIfAbleTime > 0)
            this.jumpIfAbleTime -= 1;

        if (this.coyoteTime.time > 0)
        {
            ResetDoubleJump();
        }

        var jumpsPermitted = 2;

        if (this.infiniteJump.isActive == true)
        {
            jumpsPermitted = int.MaxValue;
        }

        if (this is PlayerPuffer)
        {
            (this as PlayerPuffer).HandlePufferJump(jumpsPermitted);
        }

        bool jump =
            GameInput.Player(this).pressedJump == true ||
            this.jumpIfAbleTime > 0;

        if (jump == true)
        {
            HapticFeedback.JumpVibrate();
            if (this.state == State.InsideSpaceBooster)
            {
                this.insideSpaceBooster.FirePlayer();
                return;
            }
            if (this.state == State.InsideAirlockCannon)
            {
                this.insideAirlockCannon.FirePlayer();
                return;
            }
            if (this.state == State.InsideCityCannon)
            {
                this.insideCityCannon.FirePlayer();
                return;
            }
            else if (this.state == State.InsideBox)
            {
                this.insideBox?.PlayerJumped();
                this.insideHidingPlace?.PlayerJumped();
                return;
            }
            else if (this.state == State.InsideMinecart)
            {
                this.insideMinecart?.PlayerJumped();
                return;
            }
            else if (this.state == State.InsideMineCannon)
            {
                this.insideMineCannon?.PlayerJumped();
                return;
            }
            else if (this.state == State.EnteringLocker)
            {
                return;
            }
            else if (this.state == State.InsideWater)
            {

                Audio.instance.PlaySfx(audioClip: Assets.instance.sfxWaterSwim, position: this.position);

                if (IsOnWaterSurface == false)
                {
                    this.velocity = JumpStrengthInsideWater;
                    this.jumpsInsideWater += 1;

                    PlayTransitionAnimation(this.animationTransitionJumpToJumpDown);
                    
                    var up = Up();
                    var forward = Forward();

                    for (var n = 0; n < 3; n += 1)
                    {
                        var p = Particle.CreateWithSprite(
                            Assets.instance.sunkenIsland.RandomBubbleParticle,
                            60,
                            this.position.Add(
                                Rnd.Range(-0.5f, 0.5f), Rnd.Range(-0.5f, 0.5f)
                            ),
                            this.transform.parent
                        );
                        p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                        p.velocity = this.velocity * Rnd.Range(0.1f, 0.3f) / 16f;
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                        p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
                    }

                    // half-circle above ground
                    for (var n = 0; n < 10; n += 1)
                    {
                        var theta =
                            (0f * Mathf.PI / 180) + Rnd.Range(0, Mathf.PI);
                        var cos_theta = Mathf.Cos(theta);
                        var sin_theta = Mathf.Sin(theta);
                        var p = Particle.CreateWithSprite(
                            Assets.instance.sunkenIsland.RandomBubbleParticle,
                            60,
                            this.position +
                                (up      * -PivotDistanceAboveFeet) +
                                (forward * cos_theta) +
                                (up      * sin_theta),
                            this.transform.parent
                        );
                        p.transform.localScale = new Vector3(1.5f, 1.5f, 1);
                        float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                        p.velocity =
                            forward * cos_theta * speed +
                            up      * sin_theta * speed;
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                        p.ScaleOut(Assets.instance.sunkenIsland.scaleOutCurveForBubbleParticles);
                    }

                    return;
                }
            }
            else if(this.state == State.InsideWaterCurrent)
            {
                this.insideWaterCurrent?.PlayerJumped();
                return;
            }
            else if(this.state == State.UsingParachuteOpened)
            {
                if(GameInput.Player(this).pressedJump == true &&
                    this.parachuteToggleTimer <= 0)
                {
                    this.parachute.Toggle();
                    this.parachuteToggleTimer = ParachuteToggleCooldown;
                    return;
                }
            }
            else if(this.state == State.UsingParachuteClosed)
            {
                if(GameInput.Player(this).pressedJump == true &&
                    this.parachuteToggleTimer <= 0)
                {
                    this.parachute.Toggle();
                    this.parachuteToggleTimer = ParachuteToggleCooldown;
                    return;
                }
            }
            else if(this.state == State.InsideAirCurrent)
            {
                this.insideAirCurrent?.PlayerJumped();
                return;
            }
            else if (this.state == State.InsideRocket)
            {
                this.insideRocket?.FirePlayer();
            }
            
            if (this.infiniteJump.isActive == true)
            {
                Audio.instance.PlaySfx(Assets.instance.sfxPowerupWings);
                if (UnityEngine.Random.value > 0.5)
                {
                    infiniteJump.wings.CreateFeatherParticles(UnityEngine.Random.Range(2, 4));
                }
            }
        }

        if (this.jumpsSinceOnGround < jumpsPermitted && jump == true)
        {
            if (this.coyoteTime.time > 0 || this.infiniteJump.isActive == true)
            {
                var angleDegrees =
                    this.coyoteTime.lastSurfaceAngleDegrees - this.gravity.FloorAngle();
                var gs = this.groundspeed;
                if (this.state == State.WallSlide)
                {
                    angleDegrees = this.facingRight ? 90 : 270;
                    gs = 0.0f;
                }
                else if (
                    this.grabbingOntoItem != null ||
                    this.state == State.StuckToSticky
                )
                {
                    angleDegrees = 0;
                    gs = (this.facingRight ? 1 : -1) * JumpHorizontalSpeed;
                }

                if (angleDegrees == 0 && Mathf.Abs(gs) < JumpHorizontalSpeed)
                {
                    gs = (this.facingRight ? 1 : -1) * JumpHorizontalSpeed;
                }

                float s = Mathf.Sin(angleDegrees * Mathf.PI / 180);
                float c = Mathf.Cos(angleDegrees * Mathf.PI / 180);
                // old wall-sliding
                // this.velocity = new Vector2(this.facingRight ? -3 : 3, 6);

                var gsy = s * gs;

                Item.GrabType? grabbingOntoItemType = null;
                if (this.grabbingOntoItem != null)
                {
                    grabbingOntoItemType = ItemGrabTypeInGravity(
                        this.grabbingOntoItem.grabInfo.grabType
                    );
                }

                if (grabbingOntoItemType == Item.GrabType.LeftWall ||
                    grabbingOntoItemType == Item.GrabType.RightWall)
                {
                    this.gravity.VelocityRight = 0;
                    this.gravity.VelocityUp = JumpStrength;
                    this.facingRight = grabbingOntoItemType == Item.GrabType.RightWall;
                    this.wallGrabDisableTime = 90;
                }
                else if (this.gravity.IsZeroGravity() == true &&
                    this.underInfluenceFromGravityAtmosphere == null)
                {
                    float direction = (this.facingRight ? 1 : -1);
                    Vector2 tangent = new Vector2(c * JumpStrength, s * JumpStrength);
                    Vector2 normal  = new Vector2(-tangent.y, tangent.x);
                    this.gravity.VelocityInGravityReferenceFrame =
                        (tangent * FreeFloatingJumpStrength * direction) +
                        (normal  * FreeFloatingJumpStrength);
                    this.facingRight = (this.gravity.VelocityRight > 0);

                    var vo = ValidOrientationsForAngle(this.groundState.angleDegrees);
                    if (vo.b != null)
                    {
                        Vector2 UpForOrientation(Orientation o)
                        {
                            switch (o)
                            {
                                case Orientation.Normal:    return Vector2.up;
                                case Orientation.RightWall: return Vector2.left;
                                case Orientation.Ceiling:   return Vector2.down;
                                case Orientation.LeftWall:  return Vector2.right;
                                default:                    return Vector2.up;
                            }
                        }
                        Vector2 aUp = UpForOrientation(vo.a);
                        Vector2 bUp = UpForOrientation(vo.b.Value);
                        float velocityUpA = Vector2.Dot(aUp, this.velocity);
                        float velocityUpB = Vector2.Dot(bUp, this.velocity);
                        if (velocityUpA > velocityUpB)
                            this.groundState.orientation = vo.a;
                        else
                            this.groundState.orientation = vo.b.Value;
                    }
                }
                else 
                {
                    this.gravity.VelocityInGravityReferenceFrame = new Vector2(
                        (-s * WallJumpStrengthX) + (c * gs),
                        Mathf.LerpUnclamped(WallJumpStrengthY, JumpStrength, c)
                    );
                    if (gsy > this.gravity.VelocityUp)
                    {
                        var factor = gsy / this.gravity.VelocityUp;
                        this.gravity.VelocityUp = gsy;
                        this.gravity.VelocityRight *= factor;
                    }
                    if (this.state == State.StuckToSticky &&
                        this.stuckToStickyBlock?.belowBlock == true)
                    {
                        this.gravity.VelocityUp = -1 / 16f;
                    }
                }

                if (angleDegrees == 90 || angleDegrees == 270)
                {
                    var dx = (angleDegrees == 270 ? -0.6875f : 0.6875f);
                    var a = Particle.CreateAndPlayOnce(
                        this.animationDustJumpFromWallSlide,
                        this.position + (this.gravity.Right() * dx),
                        this.transform.parent
                    );
                    a.transform.localRotation =
                        Quaternion.Euler(0, 0, this.gravity.FloorAngle());
                    a.transform.localScale =
                        new Vector3(angleDegrees == 270 ? 1 : -1, 1, 1);
                    a.spriteRenderer.sortingLayerName = SortingLayerForDust();
                }
                else
                {
                    var a = Particle.CreateAndPlayOnce(
                        this.animationDustJump,
                        this.position + (this.gravity.Down() * 0.875f),
                        this.transform.parent
                    );
                    var speedNow = Vector2.Dot(new Vector2(c, s), this.velocity);
                    a.transform.localScale = new Vector3(speedNow >= 0 ? 1 : -1, 1, 1);
                    a.transform.localRotation = Quaternion.AngleAxis(
                        this.groundState.angleDegrees + (speedNow >= 0 ? 15 : -15),
                        Vector3.forward
                    );
                    a.spriteRenderer.sortingLayerName = SortingLayerForDust();
                }

                if (this.grabbingOntoItem != null || this.onBamboo != null)
                {
                    var up = Up();
                    var forward = Forward();

                    // particles moving with player velocity
                    for (var n = 0; n < 3; n += 1)
                    {
                        var p = Particle.CreateAndPlayOnce(
                            this.animationDustLandParticle,
                            this.position.Add(
                                Rnd.Range(-0.5f, 0.5f), Rnd.Range(-0.5f, 0.5f)
                            ),
                            this.transform.parent
                        );
                        p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                        p.velocity = this.velocity * Rnd.Range(0.1f, 0.3f);
                        p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                    }
                    // a circle
                    for (var n = 0; n < 10; n += 1)
                    {
                        var theta = Rnd.Range(0, Mathf.PI * 2);
                        var cos_theta = Mathf.Cos(theta);
                        var sin_theta = Mathf.Sin(theta);
                        var p = Particle.CreateAndPlayOnce(
                            this.animationDustLandParticle,
                            this.position +
                                (up      * -PivotDistanceAboveFeet) +
                                (forward * cos_theta) +
                                (up      * sin_theta),
                            this.transform.parent
                        );
                        float speed = Rnd.Range(0.3f / 16f, 0.7f / 16f);
                        p.velocity =
                            forward * cos_theta * speed +
                            up      * sin_theta * speed;
                        p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                    }
                }
                else
                {
                    var up = Up();
                    var forward = Forward();

                    // particles moving with player velocity
                    for (var n = 0; n < 3; n += 1)
                    {
                        var p = Particle.CreateAndPlayOnce(
                            this.animationDustLandParticle,
                            this.position.Add(
                                Rnd.Range(-0.5f, 0.5f), Rnd.Range(-0.5f, 0.5f)
                            ),
                            this.transform.parent
                        );
                        p.transform.localScale = new Vector3(0.8f, 0.8f, 1);
                        p.velocity = this.velocity * Rnd.Range(0.1f, 0.3f) / 16f;
                        p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                    }
                    // half-circle above ground
                    for (var n = 0; n < 10; n += 1)
                    {
                        var theta =
                            (angleDegrees * Mathf.PI / 180) + Rnd.Range(0, Mathf.PI);
                        var cos_theta = Mathf.Cos(theta);
                        var sin_theta = Mathf.Sin(theta);
                        var p = Particle.CreateAndPlayOnce(
                            this.animationDustLandParticle,
                            this.position +
                                (up      * -PivotDistanceAboveFeet) +
                                (forward * cos_theta) +
                                (up      * sin_theta),
                            this.transform.parent
                        );
                        float speed = Rnd.Range(0.3f, 0.7f) / 16f;
                        p.velocity =
                            forward * cos_theta * speed +
                            up      * sin_theta * speed;
                        p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                    }
                    // a few particles moving along the ground plane
                    for (var n = 0; n < 2; n += 1)
                    {
                        var theta =
                            (angleDegrees * Mathf.PI / 180) + ((n % 2) * Mathf.PI);
                        var cos_theta = Mathf.Cos(theta);
                        var sin_theta = Mathf.Sin(theta);
                        var p = Particle.CreateAndPlayOnce(
                            this.animationDustLandParticle,
                            this.position +
                                up      * -PivotDistanceAboveFeet +
                                forward * cos_theta +
                                up      * sin_theta,
                            this.transform.parent
                        );
                        float speed = Rnd.Range(0.7f, 1.4f) / 16f;
                        p.velocity =
                            forward * cos_theta * speed +
                            up      * sin_theta * speed;
                        p.animated.playbackSpeed = Rnd.Range(0.8f, 1.2f);
                        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
                    }
                }

                Audio.instance.PlayRandomSfx(audioClips: Assets.instance.sfxPlayerJump);
                Audio.instance.PlayRandomSfx(audioClips: Assets.instance.sfxPlayerWallJump);
                if (this is PlayerYolk)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxYolkVoice);
                }
                else if(this is PlayerPuffer)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxPuffer);
                }
                else if(this is PlayerSprout)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxSprout);
                }
                else if(this is PlayerKing)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxKing);
                }
                else if(this is PlayerGoop)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxGoop);
                }

                if (firstJump)
                {
                    firstJump = false;
                    //Audio.PlaySfx(Assets.instance.sfxStartGong);
                    if(this.map.referee != null)
                    {
                        this.map.referee.playWhistle = true;
                    }
                }
            }
            else
            {
                if (this.gravity.IsZeroGravity() == true)
                {
                    var velocity = this.gravity.VelocityInGravityReferenceFrame;
                    velocity.Normalize();
                    if (velocity.sqrMagnitude == 0)
                    {
                        velocity = new Vector2(
                            Mathf.Sin(this.groundState.angleDegrees * 180 / Mathf.PI),
                            -Mathf.Cos(this.groundState.angleDegrees * 180 / Mathf.PI)
                        );
                    }
                    velocity *= FreeFloatingSecondJumpStrength;
                    this.gravity.VelocityInGravityReferenceFrame = velocity;

                    // logic of coyote-time / double-jump is different in zero-g chunks.
                    // when falling off ground, once coyote time is up, you have
                    // lost your first jump and the next jump acts like a double-jump.
                    this.jumpsSinceOnGround = jumpsPermitted;
                }
                else
                {
                    // if (this.underInfluenceFromGravityAtmosphere != null)
                    // {
                    //     this.underInfluenceFromGravityAtmosphere = null;
                    //     UpdateGravity();
                    // }

                    this.gravity.VelocityUp = SecondJumpStrength;

                    if (Mathf.Abs(this.gravity.VelocityRight) < JumpHorizontalSpeed)
                    {
                        this.gravity.VelocityRight =
                            (this.facingRight ? 1 : -1) * JumpHorizontalSpeed;
                    }
                }

                if(IsInsideWater == false)
                {
                    var a = Particle.CreateAndPlayOnce(
                        Assets.instance.doubleJumpAnimation,
                        this.position + new Vector2(0, -14 / 16f),
                        this.transform.parent
                    );
                    a.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);
                    a.spriteRenderer.sortingLayerName = SortingLayerForDust();
                }

                this.dustParticleTrailRemainingTime = 25;
                Audio.instance.PlayRandomSfx(audioClips: Assets.instance.sfxPlayerDoubleJump);

                if (this is PlayerYolk)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxYolkVoice);
                }
                else if (this is PlayerPuffer)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxPuffer);
                }
                else if (this is PlayerSprout)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxSprout);
                }
                else if (this is PlayerKing)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxKing);
                }
                else if (this is PlayerGoop)
                {
                    Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxGoop);
                }
            }

            if (this.state == State.SlidingOnBamboo)
                this.disableSlideOnBambooTime = SlideOnBambooCooldown;

            this.groundState = GroundState.Airborne(this);
            this.state = State.Jump;
            this.grabbingOntoItem = null;
            JumpAwayFromSticky();
            this.bouncedOffEnemy = false;
            this.firedFromPipeCannon = false;

            if (this.gravity.VelocityRight > 0) this.facingRight = true;
            if (this.gravity.VelocityRight < 0) this.facingRight = false;

            this.coyoteTime.time = 0;
            this.jumpIfAbleTime = 0;
            this.jumpsSinceOnGround += 1;
            this.timeSinceJump = 0;
        }
        else if (GameInput.Player(this).pressedJump)
        {
            this.jumpIfAbleTime = AllowJumpAfterInputTime;
        }
    }

    private void DropOffLoopIfNotFastEnough()
    {
        if (this.groundState.onGround == false) return;
        if (this.groundState.orientation == this.gravity.Orientation()) return;
        if (this.state == State.RunningOnGripGround) return;
        if (this is PlayerGoop && this.state == State.GoopBallSpinning) return;

        var groundAngleRelativeToGravity = Util.WrapAngleMinus180To180(
            this.groundState.angleDegrees - this.gravity.FloorAngle()
        );
        if (groundAngleRelativeToGravity > -90 && groundAngleRelativeToGravity < 90)
            return;
        if (groundAngleRelativeToGravity == -90 && this.groundspeed > 0) return;
        if (groundAngleRelativeToGravity == +90 && this.groundspeed < 0) return;

        if (Mathf.Abs(this.groundspeed) >= MinimumSpeedToLoop) return;

        var raycast = new Raycast(
            this.map, this.position, layer: this.layer, isCastByPlayer: true
        );

        // this is just a quality of life check. if the wall starts to
        // curve back to flat ground, very close to where we are contacting it,
        // then it's probably not worth falling off.
        if (groundAngleRelativeToGravity == 90)
        {
            var toRightBelow = Probe(raycast, 0, -10 / 16f, 1, 0, 20 / 16f);
            if (toRightBelow.anything && toRightBelow.surfaceAngleDegrees < 90)
                return;
        }
        else if (groundAngleRelativeToGravity == -90)
        {
            var toLeftBelow = Probe(raycast, 0, -10 / 16f, -1, 0, 20 / 16f);
            if (toLeftBelow.anything && toLeftBelow.surfaceAngleDegrees > 270)
                return;
        }

        if (groundAngleRelativeToGravity == 90)
        {
            var probe = Probe(raycast, 0, 0, 0, -1, ABSensorLength);
            if (probe.anything)
            {
                this.state = State.WallSlide;
                this.facingRight = true;
                this.position += this.gravity.Right() * 3 / 16f;
                this.groundState = GroundState.Airborne(this);
                this.PlayTransitionAnimation(this.animationTransitionRunToWallSlide);
                return;
            }
        }
        else if (groundAngleRelativeToGravity == -90)
        {
            var probe = Probe(raycast, 0, 0, 0, -1, ABSensorLength);
            if (probe.anything)
            {
                this.state = State.WallSlide;
                this.facingRight = false;
                this.position += this.gravity.Left() * 3 / 16f;
                this.groundState = GroundState.Airborne(this);
                this.PlayTransitionAnimation(this.animationTransitionRunToWallSlide);
                return;
            }
        }

        this.groundState = GroundState.Airborne(this);
        this.state = State.Normal;
    }

    protected void HandleTriggers()
    {
        var centerTX = Mathf.FloorToInt(this.position.x / Tile.Size);
        var centerTY = Mathf.FloorToInt(this.position.y / Tile.Size);
        var bestDistance = Mathf.Infinity;

        for (var x = centerTX - 1; x <= centerTX + 1; x += 1)
        {
            for (var y = centerTY - 1; y <= centerTY + 1; y += 1)
            {
                var dx = Mathf.Abs(((x + 0.5f) * Tile.Size) - this.position.x);
                var dy = Mathf.Abs(((y + 0.5f) * Tile.Size) - this.position.y);
                var distance = dx + dy;
                if (distance >= bestDistance) continue;

                var trigger = this.map.TriggerAt(x, y);
                if (trigger == null) continue;

                if (trigger.Value.tile == "layer_a_low")
                {
                    this.layer = Layer.A;
                    bestDistance = distance;
                }
                else if (trigger.Value.tile == "layer_b_low")
                {
                    this.layer = Layer.B;
                    bestDistance = distance;
                }
            }
        }
    }

    protected void HandleCrush(Raycast raycast)
    {
        if(this.state == State.InsideCityCannon) return;
        if(this.state == State.InsideMinecart) return;

        var chunk = this.map.NearestChunkTo(this.position);
        if (chunk.index == this.map.chunks.Count - 1)
        {
            // If we're being crushed in the last chunk, its a trophy hitbox appearing.
            // Killing the player with a trophy is bad form. Let's dodge it instead.
            if (WillBeCrushed(raycast) == true)
                MoveToAvoidBeingCrushed(raycast);
            return;
        }

        var up   = Probe(raycast, 0, 0, 0, +1, CDSensorLength);
        var down = Probe(raycast, 0, 0, 0, -1, ABSensorLength);
        if (up.anything &&
            down.anything &&
            up.distance + down.distance < CrushDistanceFloorCeiling)
        {
            Die();
            return;
        }

        var left  = Probe(raycast, 0, 0, -1, 0, EFSensorLength + (5 / 16f));
        var right = Probe(raycast, 0, 0, +1, 0, EFSensorLength + (5 / 16f));
        if (left.anything &&
            right.anything &&
            left.distance + right.distance < CrushDistanceWalls)
        {
            Die();
            return;
        }
    }

    public bool WillBeCrushed(Raycast raycast)
    {
        if(this.state == State.InsideCityCannon) return false;
        if(this.state == State.InsideMinecart) return false;
        
        var up   = Probe(raycast, 0, 0, 0, +1, CDSensorLength);
        var down = Probe(raycast, 0, 0, 0, -1, ABSensorLength);
        if (up.anything &&
            down.anything &&
            up.distance + down.distance < CrushDistanceFloorCeiling)
        {
            return true;
        }
        
        var left  = Probe(raycast, 0, 0, -1, 0, EFSensorLength + (5 / 16f));
        var right = Probe(raycast, 0, 0, +1, 0, EFSensorLength + (5 / 16f));
        if (left.anything &&
            right.anything &&
            left.distance + right.distance < CrushDistanceWalls)
        {
            return true;
        }

        return false;
    }

    private void MoveToAvoidBeingCrushed(Raycast raycast)
    {
        var before = this.position;
        for (float range = 1; range <= 2; range += 1)
        {
            for (float angle = 0; angle < Mathf.PI * 2; angle += Mathf.PI * 0.25f)
            {
                this.position = before.Add(
                    Mathf.Cos(angle) * range,
                    Mathf.Sin(angle) * range
                );
                if (WillBeCrushed(raycast) == false) return;
            }
        }
        this.position = before;
    }

    private Item.GrabType ItemGrabTypeInGravity(Item.GrabType typeInNormalGravity)
    {
        if (this.gravity.Orientation() == Orientation.RightWall)
        {
            switch (typeInNormalGravity)
            {
                case Item.GrabType.RightWall: return Item.GrabType.Floor;
                case Item.GrabType.Ceiling:   return Item.GrabType.RightWall;
                case Item.GrabType.LeftWall:  return Item.GrabType.Ceiling;
                case Item.GrabType.Floor:     return Item.GrabType.LeftWall;
            }
        }
        else if (this.gravity.Orientation() == Orientation.Ceiling)
        {
            switch (typeInNormalGravity)
            {
                case Item.GrabType.Ceiling:   return Item.GrabType.Floor;
                case Item.GrabType.LeftWall:  return Item.GrabType.RightWall;
                case Item.GrabType.Floor:     return Item.GrabType.Ceiling;
                case Item.GrabType.RightWall: return Item.GrabType.LeftWall;
            }
        }
        else if (this.gravity.Orientation() == Orientation.LeftWall)
        {
            switch (typeInNormalGravity)
            {
                case Item.GrabType.LeftWall:  return Item.GrabType.Floor;
                case Item.GrabType.Floor:     return Item.GrabType.RightWall;
                case Item.GrabType.RightWall: return Item.GrabType.Ceiling;
                case Item.GrabType.Ceiling:   return Item.GrabType.LeftWall;
            }
        }
        return typeInNormalGravity;
    }
    
    protected virtual void UpdateSprite()
    {
        if (this.transitionAnimation == this.animationTransitionWallBounce ||
            this.transitionAnimation == this.animationTransitionWallSlideToRun ||
            this.transitionAnimation == this.animationTransitionRunToWallSlide ||
            this.transitionAnimation == this.animationTransitionJumpToJumpDown ||
            this.transitionAnimation == this.animationTransitionSwimToSwimUp)
        {
            if (this.state != State.Normal &&
                this.state != State.WallSlide)
            {
                this.transitionAnimation = null;
            }
        }
        if (this.transitionAnimation == this.animationTransitionJumpToJumpDown &&
            (this.state != State.Normal || this.groundState.onGround == true
                || this.grabbingOntoItem != null
                || (this.map.theme == Theme.WindySkies && CloudBlock.IsPlayerInsideClouds(this) == true)                
                ))
        {
            this.transitionAnimation = null;
        }
        if (this.transitionAnimation == this.animationTransitionWallSlideToDrop &&
            this.state != State.DropVertically)
        {
            this.transitionAnimation = null;
        }

        this.transform.position = this.position;

        {
            float angleDegrees = this.groundState.angleDegrees;
            if (this.groundState.onGround == true &&
                this.groundState.groundIsShallowSlope == true)
            {
                angleDegrees = this.gravity.FloorAngle();
            }
            this.transform.rotation = Quaternion.Euler(0, 0, angleDegrees);
        }
        this.transform.localScale = new Vector3(this.facingRight ? 1 : -1, 1, 1);

        var speed = this.groundState.onGround ? this.groundspeed : this.velocity.x;
        speed = Mathf.Abs(speed);
        var tooFast = speed >= RunTooFastSpeed;

        var drawBambooArm = false;
        var behindBamboo = false;

        if (this.alive == false)
        {
            if (this.animated.currentAnimation != this.animationDead)
            {
                this.animated.PlayAndHoldLastFrame(this.animationDead);
            }
        }
        else if (this.grabbingOntoItem is MultiplayerHeadset)
        {
            this.animated.Stop();
            this.spriteRenderer.sprite = animationBlankSprite;
        }
        else if (this.state == State.Finished)
            this.animated.PlayAndLoop(this.animationSpring);
        else if (this is PlayerYolk && (this as PlayerYolk).insideEgg == true)
            this.animated.PlayAndHoldLastFrame((this as PlayerYolk).animationInsideEgg);
        else if (this.state == State.InsideSpaceBooster)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.InsideAirlockCannon)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.InsideCityCannon)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.FireFromCityCannon)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.FireFromMineCannon)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.EnteringLocker)
            this.animated.PlayAndLoop(this.animationStanding);
        else if (this.state == State.InsideMinecart)
        {
            if(this.insideMinecart != null)
            {
                if (this.insideMinecart.ShouldPlayerDisappearWhileMinecartFlip())
                {
                    this.animated.Stop();
                    this.spriteRenderer.sprite = animationBlankSprite;
                }
                else
                {
                    if(this.insideMinecart.IsMinecartPlayingJumpAnim())
                    {
                        this.animated.PlayAndLoop(this.animationJumping);
                    }
                    else
                    {
                        this.animated.PlayAndLoop(this.animationStanding);
                    }
                }
            }
            else
            {
                this.animated.PlayAndLoop(this.animationStanding);
            }
        }
        else if (this.state == State.InsideMineCannon)
        {
            this.animated.Stop();
            this.spriteRenderer.sprite = animationBlankSprite;
        }
        else if (this.state == State.InsideBox)
        {
            this.animated.Stop();
            this.spriteRenderer.sprite = animationBlankSprite;
        }
        else if (this.state == State.InsideWaterCurrent)
        {
            this.animated.PlayAndLoop(this.animationAfterStomp);
        }
        else if (this.state == State.InsideAirCurrent)
        {
            this.animated.PlayAndLoop(this.animationAfterStomp);
        }
        else if (this.state == State.Respawning)
        { }
        else if (this.transitionAnimation != null)
        { }
        else if (this.grabbingOntoItem != null)
        {
            if (this.grabItemSmoothTime == 0)
            {
                switch (ItemGrabTypeInGravity(this.grabbingOntoItem.grabInfo.grabType))
                {
                    case Item.GrabType.Floor:
                        this.animated.PlayAndLoop(this.animationGrabStand);
                        break;

                    case Item.GrabType.RightWall:
                    case Item.GrabType.LeftWall:
                        this.animated.PlayAndLoop(this.animationGrabSide);
                        break;

                    case Item.GrabType.Ceiling:
                    default:
                        this.animated.PlayAndLoop(this.animationHanging);
                        break;
                }
            }
            else
            {
                // allow whatever animation is currently playing to continue
                // (player will briefly seem to be still in the previous state)
            }
        }
        else if (this.firedFromPipeCannon == true)
        {
            this.animated.PlayAndLoop(this.animationBall);
        }
        else if (this.bouncedOffEnemy == true)
        {
            if (this.animated.currentAnimation != this.animationStompFrame)
                this.animated.PlayAndLoop(this.animationAfterStomp);
        }
        else if (this.state == State.WallSlide)
        {
            PlayWallSlide();
        }
        else if (this.state == State.Jump && this.jumpsSinceOnGround >= 2 && this.infiniteJump.isActive == false)
        {
            if (this is PlayerPuffer)
            {
                (this as PlayerPuffer).UpdateBodyInflateSprites();
            }
            else
            {
                this.animated.PlayAndLoop(this.animationDoubleJumping);
            }

            if (this is PlayerGoop && !Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopSpin))
            {
                Audio.instance.PlaySfx(Assets.instance.sfxGoopSpin, options: new Audio.Options(0.5f, false, 0));
            }
        }
        else if (
            this.state == State.Jump ||
            (this.gravity.IsZeroGravity() && this.groundState.onGround == false)
        )
        {
            if (this.velocity.y < 0.2f && this.animationJumpingDown.sprites.Length > 0)
            {
                if (this.animated.currentAnimation == this.animationJumping ||
                    this.animated.currentAnimation ==
                        this.animationTransitionJumpToJumpDown)
                {
                    PlayTransitionAnimation(this.animationTransitionJumpToJumpDown);
                }
                else
                {
                    this.animated.PlayAndLoop(this.animationJumpingDown);
                }
            }
            else
                this.animated.PlayAndLoop(this.animationJumping);

            if (this is PlayerGoop)
            {
                (this as PlayerGoop).StopWallSlideSfx();
            }
        }
        else if (this.state == State.InsideWater)
        {
            if (this.velocity.y < 0.2f && this.animationJumpingDown.sprites.Length > 0)
            {
                if (Mathf.Approximately(this.velocity.y, 0f))
                {
                    if (IsOnWaterSurface == true)
                    {
                        this.animated.PlayAndLoop(this.animationSwimmingSurface);
                    }
                    else
                    {
                        this.animated.PlayAndLoop(this.animationSwimmingCeiling);
                    }
                }
                else if (this.animated.currentAnimation == this.animationJumping ||
                    this.animated.currentAnimation == this.animationTransitionJumpToJumpDown)
                {
                    PlayTransitionAnimation(this.animationTransitionJumpToJumpDown);
                }
                else
                {
                    this.animated.PlayAndLoop(this.animationJumpingDown);
                }
            }
            else
            {
                if (this.animated.currentAnimation == this.animationJumpingDown ||
                    this.animated.currentAnimation == this.animationTransitionJumpToJumpDown)
                {
                    PlayTransitionAnimation(this.animationTransitionSwimToSwimUp);
                }
                else
                {
                    this.animated.PlayAndLoop(this.animationHoldingBreath);
                }
            }
        }
        else if (this.state == State.DropVertically)
        {
            this.animated.PlayAndLoop(this.animationJumpingDown);
            if (this is PlayerGoop)
            {
                (this as PlayerGoop).StopWallSlideSfx();
            }
        }
        else if (this.state == State.SlidingOnIce)
        {
            this.animated.PlayAndLoop(this.animationIceSliding);
        }
        else if (this.state == State.SlidingOnBamboo)
        {
            this.groundspeed = 0;

            if (this.onBamboo != null &&
                this.onBamboo.twisty == true &&
                this.onBamboo.twistyTop == false &&
                (
                    this.onBamboo.twistyBottom == false ||
                    this.position.y >= this.onBamboo.position.y
                )
            )
            {
                this.animated.Stop();
                var dy = this.position.y - this.onBamboo.position.y;
                var through = (dy - (-0.5f)) / (0.5f - (-0.5f));
                var frame = Mathf.FloorToInt(
                    through * this.animationPoleSlidingTwist.sprites.Length
                );
                behindBamboo = (frame >= 4);
                this.spriteRenderer.sprite =
                    this.animationPoleSlidingTwist.sprites[frame];

                if (this is PlayerPuffer)
                {
                    (this as PlayerPuffer).UpdatePoleSlidingTwistSprite(frame);
                }
                else if (this is PlayerKing)
                {
                    (this as PlayerKing).UpdatePoleSlidingTwistSprite(frame);
                }
            }
            else
            {
                if (this.animated.currentAnimation != this.animationPoleSliding)
                {
                    this.animated.PlayAndLoop(this.animationPoleSliding);
                    this.animated.frameNumber = Util.RandomChoice(0, 14);
                }
                drawBambooArm = true;
            }

            if (infiniteJump.isActive == true)
            {
                infiniteJump.wings.behindBamboo = behindBamboo;
            }
        }
        else if (this.state == State.Spring)
            this.animated.PlayAndLoop(this.animationSpring);
        else if (this.state == State.SpringFromPushPlatform)
            this.animated.PlayAndLoop(this.animationSpring);
        else if (this.state == State.Geyser)
            this.animated.PlayAndLoop(this.animationSpring);
        else if (this.state == State.Hit)
            this.animated.PlayAndLoop(this.animationHit);
        else if (this.state == State.StuckToSticky)
        {
            this.animated.Stop();
            this.spriteRenderer.sprite = this.animationJumping.sprites[0];
        }
        else if (this.state == State.InsidePipe)
            this.animated.PlayAndLoop(this.animationBall);
        else if (this.state == State.InsideLift)
        {
            this.animated.PlayAndLoop(this.animationStanding);
        }
        else if (this.state == State.UsingParachuteOpened)
        {
            this.animated.PlayAndLoop(this.animationWithParachuteOpened);
        }
        else if (this.state == State.UsingParachuteClosed)
        {
            this.animated.PlayAndLoop(this.animationWithParachuteClosed);
        }
        else if (this.state == State.BoostFlyingRing)
        {
            this.animated.PlayAndLoop(this.animationSpring);
        }
        else if (this.state == State.InsideRocket)
        {
            this.animated.PlayAndLoop(this.animationBall);
        }
        else if (this is PlayerSprout && this.state == State.SproutSucking)
        {
            this.animated.PlayAndLoop((this as PlayerSprout).animationSuck);
        }
        else if (this is PlayerKing && this.state == State.KingStompDrop)
        {
            (this as PlayerKing).UpdateStompDropSprite();
        }
        else if (this is PlayerKing && this.state == State.KingHurt)
        {
            // handled by child class
        }
        else if (this is PlayerYolk && this.state == State.YolkLayingEgg)
        {
            // handled by child class
        }
        else if (this is PlayerGoop && this.state == State.GoopBallSpinning)
        {
            this.animated.PlayAndLoop((this as PlayerGoop).animationWallRoll);
        }
        else if (this is PlayerGoop && this.state == State.GoopBallTurningInPlace)
        {
            this.animated.PlayAndLoop((this as PlayerGoop).animationWallRoll);
        }
        else if (this.groundspeed == 0)
        {
            PlayStanding();
        }
        else if (tooFast == true)
            this.animated.PlayAndLoop(this.animationRunningTooFast);
        else
        {
            PlayRunning();
        }

        if (this.alive == false || ((this is PlayerYolk) && (this as PlayerYolk).insideEgg == true))
        {
            this.spriteRenderer.sortingLayerName = "Player When Dead";
            this.spriteRenderer.sortingOrder = 0;
        }
        else if (behindBamboo == true || this.insidePipe != null)
        {
            this.spriteRenderer.sortingLayerName = "Spikes";
            this.spriteRenderer.sortingOrder = -10;
        }
        else if (this.layer == Layer.B || this.grabbingOntoItem != null)
        {
            this.spriteRenderer.sortingLayerName = "Player (BL)";
            this.spriteRenderer.sortingOrder = 0;
        }
        else if (state == State.InsideLift)
        {
            this.spriteRenderer.sortingLayerName = "Items";
            this.spriteRenderer.sortingOrder = 9;

            if (insideBonusLift != null)
            {
                if(insideBonusLift.group != null)
                {
                    this.spriteRenderer.sortingLayerName = " Tiles - Layer B";
                    this.spriteRenderer.sortingOrder = 5;
                }
            }
            else if(insideEntranceLift != null)
            {
                if(insideEntranceLift.checkpoint != null)
                {
                    this.spriteRenderer.sortingLayerName = "Player Dust (AL)";
                    this.spriteRenderer.sortingOrder = 9;
                }
            }
        }
        else if(state == State.InsideMineCannon || state == State.InsideRocket)
        {
            this.spriteRenderer.sortingLayerName = "Player (BL)";
            this.spriteRenderer.sortingOrder = 0;
        }
        else
        {
            this.spriteRenderer.sortingLayerName = "Player (AL)";
            this.spriteRenderer.sortingOrder = 0;
        }

        if (drawBambooArm == true)
        {
            this.backArmSpriteRenderer.gameObject.SetActive(true);
            this.backArmSpriteRenderer.sprite =
                this.animationPoleSlidingBackArm.sprites[
                    (this.map.frameNumber / 2) %
                    this.animationPoleSlidingBackArm.sprites.Length
                ];
        }
        else
        {
            this.backArmSpriteRenderer.gameObject.SetActive(false);
        }
        
        if (this.alive == true &&
            this.grabbingOntoItem != null &&
            !(this.grabbingOntoItem is MultiplayerHeadset) &&
            this.grabItemSmoothTime == 0)
        {
            this.frontArmSpriteRenderer.gameObject.SetActive(true);

            switch (ItemGrabTypeInGravity(this.grabbingOntoItem.grabInfo.grabType))
            {
                case Item.GrabType.Floor:
                    this.frontArmSpriteRenderer.sprite = this.animationGrabStandFrontArm;
                    break;
                    
                case Item.GrabType.LeftWall:
                case Item.GrabType.RightWall:
                    this.frontArmSpriteRenderer.sprite = this.animationGrabSideFrontArm;
                    break;

                case Item.GrabType.Ceiling:
                    this.frontArmSpriteRenderer.sprite = this.animationHangingFrontArm;
                    break;
            }
        }
        else
        {
            this.frontArmSpriteRenderer.gameObject.SetActive(false);
            this.frontArmSpriteRenderer.sprite = null;
        }

        if (this.squashAndStretch.active == true)
            this.squashAndStretch.Advance();
            
        if(this is PlayerPuffer)
        {
            (this as PlayerPuffer).UpdateHeadSprite();
        } 
        else if(this is PlayerKing)
        {
            (this as PlayerKing).UpdateHatSprite();
        }
    }

    protected virtual void PlayStanding()
    {
        this.animated.PlayAndLoop(this.animationStanding);
    }

    protected virtual void PlayRunning()
    {
        this.animated.PlayAndLoop(this.animationRunning);
    }

    protected virtual void PlayWallSlide()
    {
        this.animated.PlayAndLoop(this.animationWallSliding);
    }

    private string SortingLayerForDust() =>
        (this.layer == Layer.A) ? "Player Dust (AL)" : "Player Dust (BL)";

    private void CreateParticles()
    {
        if (this.state == State.WallSlide &&
            this.map.frameNumber % 12 == 0 &&
            this.gravity.VelocityUp < -0.5f / 16)
        {
            var sx = this.facingRight ? -1 : 1;
            var dx = -sx * (EFSensorLength + 1 / 16f);
            var a = Particle.CreateAndPlayOnce(
                this.animationWallSlideDust,
                this.position
                    + (this.gravity.Right() * dx)
                    + (this.gravity.Up() * 10 / 16f),
                this.map.transform
            );
            a.spriteRenderer.sortingLayerName = SortingLayerForDust();
            a.transform.localRotation =
                Quaternion.Euler(0, 0, this.groundState.angleDegrees);
            a.transform.localScale = new Vector3(sx, 1, 1);
        }

        if (this.timeSinceDustParticle <= 0)
        {
            var max = 4;
            if (this.velocity.sqrMagnitude > 0.2f * 0.2f)
                max = 2;
            this.timeSinceDustParticle = Rnd.Range(1, max);

            if (this.state == State.Normal &&
                this.groundState.onGround == true &&
                Mathf.Abs(this.groundspeed) > 0.125f &&
                this.grabbingOntoItem?.grabInfo.grabType != Item.GrabType.Floor)
            {
                CreateDustParticle(0, -PivotDistanceAboveFeet);
            }
            else if (this.state == State.WallSlide)
            {
                CreateDustParticle(EFSensorLength * (this.facingRight ? 1 : -1), 0);
            }
        }
        else
        {
            this.timeSinceDustParticle -= 1;
        }

        if (this.groundState.onGround ||
            this.state == State.WallSlide)
        {
            this.dustParticleTrailRemainingTime = 0;
        }

        if (this.dustParticleTrailRemainingTime > 0)
        {
            this.dustParticleTrailRemainingTime -= 1;

            if (this.map.frameNumber % 2 == 0)
                CreateDustParticle(0, 0, true);
        }

        bool IsSweating()
        {
            if(this is PlayerYolk == false)
                return false;
                
            if (this.grabbingOntoItem is MultiplayerHeadset)
                return false;
            else if (this.grabbingOntoItem != null)
                return true;

            if (this.groundState.onGround == true)
            {
                if (Mathf.Abs(this.groundspeed) >= RunTooFastSpeed)
                    return true;
                if (Mathf.Abs(this.groundspeed) >= RunSpeed && this.runPanicking == true)
                    return true;
            }

            return false;
        }

        if (IsSweating() == true && this.map.frameNumber % 10 == 0)
        {
            var center = this.position;
            if (this.grabbingOntoItem?.grabInfo.grabType == Item.GrabType.Ceiling)
                center += new Vector2(this.facingRight ? -0.8f : 0.8f, 0);

            var up = Up();
            var forward = Forward();
            var ahead = forward * (this.facingRight ? 1 : -1);
            var p = Particle.CreateAndPlayOnce(
                this.animationRunTooFastSweat,
                center +
                    (up * Rnd.Range(1.25f, 1.625f)) +
                    (ahead * Rnd.Range(0, 1)),
                this.transform.parent
            );

            if (this.map.frameNumber % 20 == 0)
            {
                p.Throw(
                    ((ahead * 1.3f) + (forward * this.groundspeed * 1.0f) + (up * 1.6f))
                        / 16,
                    0.5f / 16,
                    0.1f / 16,
                    ((ahead + up) * -0.1f) / 16
                );
            }
            else
            {
                p.Throw(
                    ((ahead * 0) + (forward * this.groundspeed * 0.5f) + (up * 1.6f))
                        / 16,
                    1f / 16,
                    0.1f / 16,
                    ((ahead + up) * -0.1f) / 16
                );
            }
        }

        this.wasSweatingLastFrame = IsSweating();

        if (this.state == State.RunningOnGripGround)
        {
            if((this.position - this.lastElectricShockPosition).sqrMagnitude > 2f*2f)
            {
                this.lastElectricShockPosition = this.position;
                CreateElectricShockParticle(0, -PivotDistanceAboveFeet + 1.0f);
            }
        }
    }

    private Particle CreateElectricShockParticle(
        float relativeForward,
        float relativeUp,
        bool smallOnly = false
    )
    {
        Vector2 forward = Forward();
        Vector2 up      = Up();

        float x = this.position.x;
        float y = this.position.y;
        x += (int)forward.x * relativeForward + (int)up.x * relativeUp;
        y += (int)forward.y * relativeForward + (int)up.y * relativeUp;
        Vector2 pos = new Vector2(x, y);
       
        Particle p = Particle.CreateAndPlayOnce        
        (
            Assets.instance.electricShockParticlesAnimation,
            pos,
            this.map.transform
        );
        
        p.transform.localScale = new Vector3(this.facingRight? 1: -1, 1, 1);
        p.transform.localRotation = Quaternion.Euler(0, 0, this.groundState.angleDegrees);
        
        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
        return p;
    }

    private Particle CreateDustParticle(
        float relativeForward,
        float relativeUp,
        bool smallOnly = false
    )
    {
        Vector2 forward = Forward();
        Vector2 up      = Up();

        float x = this.position.x;
        float y = this.position.y;
        x += (int)forward.x * relativeForward + (int)up.x * relativeUp;
        y += (int)forward.y * relativeForward + (int)up.y * relativeUp;

        Particle p = Particle.CreateAndPlayOnce(
            this.animationDustParticle,
            new Vector2(
                x + Rnd.Range(-0.2f, 0.2f),
                y + Rnd.Range(-0.2f, 0.2f)
            ),
            this.transform.parent
        );
        float s = Rnd.Range(0.5f, 1.0f);
        p.transform.localScale = new Vector3(s, s, 1);

        p.velocity = new Vector2(
            Rnd.Range(-0.1f, 0.1f),
            Rnd.Range(0, 0.2f)
        ) / 16;
        p.spriteRenderer.sortingLayerName = SortingLayerForDust();
        return p;
    }

    private void AdvancePowerups()
    {
        this.invincibility.Advance();
        this.shield.Advance();
        this.swordAndShield.Advance();
        this.infiniteJump.Advance();
        this.midasTouch.Advance();
        this.ink.Advance();
        this.mines.Advance();
        this.minesTap.Advance();
    }

    private float AngleDifferenceAbsolute(float angle1Degrees, float angle2Degrees) =>
        Mathf.Abs(Util.WrapAngleMinus180To180(angle2Degrees - angle1Degrees));

    public Vector2 FeetPosition()
    {
        return this.position + (Up() * -PivotDistanceAboveFeet);
    }

    public Rect Rectangle()
    {
        if (this.alive == false)
            return Rect.zero;
        else
            return new Rect(
                this.position.x - EFSensorLength,
                this.position.y - CDSensorLength,
                EFSensorLength + EFSensorLength,
                (15 / 16f) + CDSensorLength
            );
    }

    private void NotifyContact(Raycast.CombinedResults probeResult)
    {
        int probeAngle = 0;
        if (probeResult.direction.x > 0) probeAngle = 90;
        if (probeResult.direction.y > 0) probeAngle = 180;
        if (probeResult.direction.x < 0) probeAngle = 270;

        foreach (var item in probeResult.items)
        {
            int angleToReport;

            if (item.collisionsFollowRotation == true)
            {
                // for something like a button, it's convenient to always
                // report collisions as "from above" even if the button is on its side
                angleToReport = probeAngle - item.rotation;
                if (angleToReport < 0) angleToReport += 360;
            }
            else
            {
                // something like a conveyor where the actual rotation
                // in world space is more important
                angleToReport = probeAngle;
            }

            var c = new Item.Collision { otherPlayer = this };
            switch (angleToReport)
            {
                case   0:  item.onCollisionFromAbove?.Invoke(c);  break;
                case  90:  item.onCollisionFromLeft ?.Invoke(c);  break;
                case 180:  item.onCollisionFromBelow?.Invoke(c);  break;
                case 270:  item.onCollisionFromRight?.Invoke(c);  break;
            }
        }
    }

    public void Hit(Vector2 hazardPosition)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;
        if (this.state == State.InsideBox) return;
        if (this.invincibility.isActive == true) return;

        if (this.shield.IsInvulnerable() == true) return;
        if (this.shield.isActive == true)
        {
            this.shield.RemoveShield();
            return;
        }

        if (this.swordAndShield.IsInvulnerable() == true) return;
        if (this.swordAndShield.isActive == true)
        {
            this.swordAndShield.RemoveShield();
            return;
        }

        if(this.state == State.InsideMinecart)
        {
            this.insideMinecart?.PlayerDied();
        }

        if (this is PlayerKing)
        {
            if((this as PlayerKing).isHatActive == true)
            {
                (this as PlayerKing).HitWithHatOn();
            }

            if((this as PlayerKing).IsInvulnerable()) return;
        }

        Die();
    }

    public virtual void Die()
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.alive = false;
        this.deadTime = 0;
        this.state = State.Normal;
        this.groundState = new GroundState(true, Orientation.Normal, 0, false, false);
        this.grabbingOntoItem = null;
        this.onBamboo = null;
        this.insidePipe = null;
        this.insideBox = null;
        this.insideHidingPlace = null;
        JumpAwayFromSticky();
        EndPowerups();
        this.parachute.Exit();
        ClearAllKeys();
        this.map.desertAlertState.ResetAlert();
        this.map.deaths += 1;
        this.map.SaveDataOfAttempt(null);

        DeathFX();

        Audio.instance.PlaySfx(Assets.instance.sfxPlayerDeath);
        HapticFeedback.DeathVibrate();
    }

    protected virtual void DeathFX()
    {
        PlayerDeathParticles.Death(this);
    }

    private void EndPowerups()
    {
        this.invincibility.End();
        this.petYellow.End();
        this.petTornado.End();
        this.shield.End();
        this.swordAndShield.End();
        this.infiniteJump.End();
        this.midasTouch.End();
        this.mines.End();
        this.minesTap.End();
    }

    public void ResetToCheckpoint()
    {
        this.position = this.GetCheckpointPosition().Add(0, 6);
        this.velocity = Vector2.zero;
        this.state = State.Respawning;
        this.facingRight = (Rnd.Range(0, 2) == 1);
        this.groundspeed = this.facingRight ? RunSpeed : -RunSpeed;
        this.alive = true;
        this.layer = Layer.A;
        this.spriteRenderer.enabled = false;
        this.transitionAnimation = null;
        this.respawnFrameNumber = this.map.frameNumber;

        RespawnFX();
        
        this.map.ResetItems();
        this.map.CheckBubbleSpawn();
        this.map.checkpointDeaths++;
        StartCoroutine(RespawnPowerups());
    }

    protected virtual void RespawnFX() { }

    private IEnumerator RespawnPowerups()
    {
        yield return new WaitForSeconds(1);
        this.petTornado.Respawn();
        this.petYellow.Respawn();
        this.midasTouch.Respawn();
        yield return new WaitForSeconds(.75f);
        this.infiniteJump.Respawn();
    }

    public void PlayStompFrame()
    {
        this.animated.PlayOnce(this.animationStompFrame, delegate
        {
            this.animated.PlayAndLoop(this.animationAfterStomp);

            if(this is PlayerPuffer)
            {
                (this as PlayerPuffer).UpdateHeadSprite();
            }
        });
    }

    public virtual void ResetDoubleJump()
    {
        this.jumpsSinceOnGround = 0;
    }

    public void CancelLateJumpTime()
    {
        this.coyoteTime.time = 0;
    }

    public void BounceOffHitEnemy(Vector2 enemyPosition, bool enemyHasSolidTop = false)
    {
        if(this is PlayerKing && this.state == State.KingStompDrop) return;
        
        if(this is PlayerGoop)
        {
            (this as PlayerGoop).CreateBlobLand(8);
        }
        
        if (this.position.y > enemyPosition.y && this.velocityLastFrame.y < 0)
        {
            // attack from above
            if (this.state == State.WallSlide)
            {
                this.velocity.y = Mathf.Abs(this.velocity.y);
                if (this.velocity.y < BopStrength)
                    this.velocity.y = BopStrength;

                this.state = State.Normal;
                this.velocity.x = (this.facingRight ? -1 : 1) * BopWallslidingStrengthX;
                this.facingRight = this.velocity.x > 0;
            }
            else if (this.state == State.SlidingOnBamboo)
            {
                this.velocity.y *= -1;
                if (this.velocity.y < BopStrength)
                    this.velocity.y = BopStrength;

                this.state = State.Normal;
                this.onBamboo = null;
                this.disableSlideOnBambooTime = SlideOnBambooCooldown;
                this.velocity.x = (this.facingRight ? 1 : -1) * BopWallslidingStrengthX;
            }
            else
            {
                this.velocity.y *= -1;
                if (this.velocity.y < BopStrength)
                    this.velocity.y = BopStrength;
            }
        }
        else
        {
            // attack from below
            this.velocity.y -= Util.Sign(this.velocity.y);
        }

        if (enemyHasSolidTop == true)
        {
            // we need to push the player upwards,
            // so he doesn't magnetize to the top edge of the enemy
            this.position.y += 0.3f;
        }

        this.groundState = GroundState.Airborne(this);
        this.bouncedOffEnemy = true;
    }
    
    public void BounceOffHitItem(Vector2 itemPosition, bool itemHasSolidTop = false)
    {
        if(this is PlayerKing && this.state == State.KingStompDrop) return;
        
        if(this is PlayerGoop)
        {
            (this as PlayerGoop).CreateBlobLand(8);
        }

        Vector2 newVelocity = this.gravity.VelocityInGravityReferenceFrame;

        bool IsPlayerOnTopOfItem()
        {
            switch(this.gravity.Orientation())
            {
                case Orientation.Normal:
                default:
                    return this.position.y > itemPosition.y;

                case Orientation.LeftWall:
                    return this.position.x > itemPosition.x;

                case Orientation.RightWall:
                    return this.position.x < itemPosition.x;

                case Orientation.Ceiling:
                    return this.position.y < itemPosition.y;
            }
        }
        
        if (IsPlayerOnTopOfItem() && this.gravity.VelocityInGravityReferenceFrame.y < 0)
        {
            // attack from above
            if (this.state == State.WallSlide)
            {
                newVelocity.y = Mathf.Abs(newVelocity.y);
                if (newVelocity.y < BopStrength)
                    newVelocity.y = BopStrength;

                this.state = State.Normal;
                newVelocity.x = (this.facingRight ? -1 : 1) * BopWallslidingStrengthX;
                this.facingRight = newVelocity.x > 0;
            }
            else if (this.state == State.SlidingOnBamboo)
            {
                newVelocity.y *= -1;
                if (newVelocity.y < BopStrength)
                    newVelocity.y = BopStrength;

                this.state = State.Normal;
                this.onBamboo = null;
                this.disableSlideOnBambooTime = SlideOnBambooCooldown;
                newVelocity.x = (this.facingRight ? 1 : -1) * BopWallslidingStrengthX;
            }
            else
            {
                newVelocity.y *= -1;
                if (newVelocity.y < BopStrength)
                    newVelocity.y = BopStrength;
            }
        }
        else
        {
            // attack from below
            newVelocity.y -= Util.Sign(newVelocity.y);
        }

        if (itemHasSolidTop == true)
        {
            // we need to push the player upwards,
            // so he doesn't magnetize to the top edge of the enemy
            this.position += this.gravity.Up() * 0.3f;
        }

        this.groundState = GroundState.Airborne(this);
        this.bouncedOffEnemy = true;
        this.gravity.VelocityInGravityReferenceFrame = newVelocity;
    }

    public void TriggerBounce()
    {
        this.velocity.y = BopStrength;
    }

    public bool HasWallOnSide(bool rightHandSide, float maxDistance = 0) => HasWallOnSide(
        new Raycast(this.map, this.position, layer: Layer.A, isCastByPlayer: true),
        rightHandSide,
        maxDistance
    );

    private bool HasWallOnSide(Raycast raycast, bool rightHandSide, float maxDistance = 0)
    {
        if (rightHandSide)
            return Probe(raycast, 0, 0, +1, 0, EFSensorLength + maxDistance).anything;
        else
            return Probe(raycast, 0, 0, -1, 0, EFSensorLength + maxDistance).anything;
    }

    public void StickToSticky(StickyBlock stickyBlock, bool left, bool right, bool below)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.StuckToSticky;
        this.stuckToStickyBlock = new StickyBlock.StuckData
        {
            stickyBlock = stickyBlock,
            relativePositionToBlock = this.position - stickyBlock.position,
            belowBlock = below
        };
        if (left) this.facingRight = false;
        if (right) this.facingRight = true;
        ResetDoubleJump();
    }

    private void JumpAwayFromSticky()
    {
        this.stuckToStickyBlock?.stickyBlock.NotifyPlayerJumpedAway();
        this.stuckToStickyBlock = null;
        this.lastStickyFrame = this.map.frameNumber;
    }

    public void GoInsidePipe(Pipe pipe)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsidePipe;
        this.insidePipe = pipe;
        this.bouncedOffEnemy = false;
        ResetDoubleJump();

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();

        if(this.parachute.IsAvailable == true)
        {
            this.parachute.Exit();
        }
    }

    public void NotifyExitedPipe()
    {
        this.insidePipe = null;
        if (this.state == State.InsidePipe)
            this.state = State.Normal;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public void EnterWaterCurrent(WaterCurrent waterCurrent)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideWaterCurrent;
        this.insideWaterCurrent = waterCurrent;
        this.bouncedOffEnemy = false;
        ResetDoubleJump();
    }
    
    public void NotifyExitWaterCurrent()
    {
        this.insideWaterCurrent = null;

        if (this.state == State.InsideWaterCurrent)
        {
            this.state = IsInsideWater ? State.InsideWater : State.Normal;
        }
    }

    public void EnterAirCurrent(AirCurrent airCurrent)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        if(this.parachute.IsAvailable == true)
        {
            this.parachute.Exit();
        }

        this.state = State.InsideAirCurrent;
        this.insideAirCurrent = airCurrent;

        this.bouncedOffEnemy = false;
        this.groundspeed = 0;
        this.groundState = new GroundState(false, Orientation.Normal, 0f, false, false);
        ResetDoubleJump();
        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxWindyAirCurrentBlowing))
        {
            Audio.instance.PlaySfx(Assets.instance.sfxWindyAirCurrentBlowing, position: this.position, options: new Audio.Options(1, false, 0));
        }
    }

    public void NotifyExitAirCurrent()
    {
        this.insideAirCurrent = null;
        this.state = State.Jump;
        Audio.instance.ChangeSfxVolume(Assets.instance.sfxWindyAirCurrentBlowing, 0, duration:0.3f);
    }

    public void GrabBamboo(Bamboo bamboo)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        if (this.state != State.SlidingOnBamboo)
        {
            this.squashAndStretch.Squash(
                this.velocity.x > 0 ?
                PlayerSquashAndStretch.WallSide.OnRight :
                PlayerSquashAndStretch.WallSide.OnLeft
            );
        }

        this.state = State.SlidingOnBamboo;
        this.onBamboo = bamboo;
        this.bouncedOffEnemy = false;
        this.velocity.x = 0;
        this.velocity.y = Mathf.Clamp(this.velocity.y, -1 / 16f, 1 / 16f);
        this.position.x = bamboo.position.x;

        Audio.instance.PlaySfx(Assets.instance.sfxPlayerGrab);
    }

    public void EnterSpaceBooster(SpaceBooster spaceBooster)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideSpaceBooster;
        this.insideSpaceBooster = spaceBooster;
        this.position = spaceBooster.position;
        ResetDoubleJump();

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void ExitSpaceBooster()
    {
        this.state = State.Normal;
        this.insideSpaceBooster = null;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public void EnterAirlockCannon(AirlockAndCannon airlockAndCannon)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideAirlockCannon;
        this.insideAirlockCannon = airlockAndCannon;
        ResetDoubleJump();

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();
 
        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public void ExitAirlockCannon()
    {
        this.state = State.Normal;
        this.insideAirlockCannon = null;
        this.noGravityTimeWhenBoostFromAirlock = 5;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public void EnterCityCannon(CityCannon cityCannon)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideCityCannon;
        this.insideCityCannon = cityCannon;
        this.position = cityCannon.position;
        ResetDoubleJump();

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void ExitCityCannon()
    {
        this.state = State.Normal;
        this.insideCityCannon = null;
        this.jumpsSinceOnGround = 1;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public void EnterLocker(Vector2 snapPosition)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;
        this.state = State.EnteringLocker;
        this.position = snapPosition;

        if(this is PlayerKing)
        {
            (this as PlayerKing).LoseHatOnCollision();
        }

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void EnterBox(Box box)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideBox;
        this.insideBox = box;
        this.position = box.position;
        this.groundState = GroundState.Airborne(this);
        ResetDoubleJump();
        
        if(this is PlayerKing)
        {
            (this as PlayerKing).LoseHatOnCollision();
        }

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void ExitBox()
    {
        this.groundState = GroundState.Airborne(this);
        this.insideBox.ExitBoxCleanup();
        this.state = State.Normal;
        this.insideBox = null;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public Box GetBox()
    {
        return this.insideBox;
    }

    public void EnterHidingPlace(HidingPlace hidingPlace)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideBox;
        this.insideHidingPlace = hidingPlace;
        this.position = hidingPlace.position;
        this.groundState = GroundState.Airborne(this);
        ResetDoubleJump();
        
        if(this is PlayerKing)
        {
            (this as PlayerKing).LoseHatOnCollision();
        }

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void ExitHidingPlace()
    {
        this.groundState = GroundState.Airborne(this);
        this.insideHidingPlace.ExitHidingPlaceCleanup();
        this.state = State.Normal;
        this.insideHidingPlace = null;

        if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public HidingPlace GetHidingPlace()
    {
        return this.insideHidingPlace;
    }

    public void EnterMineCannon(MineCannon mineCannon)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideMineCannon;
        this.insideMineCannon = mineCannon;
        this.position = mineCannon.position;
        this.groundState = new GroundState(true, Orientation.Normal, 0, false, false);
        ResetDoubleJump();

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void ExitMineCannon()
    {
        this.groundState = GroundState.Airborne(this);
        this.insideMineCannon.ExitMineCannonCleanup();
        this.state = State.Normal;
        this.insideMineCannon = null;

         if (this.shield.isActive == true)
            this.shield.ShowShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.ShowSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Show();
    }

    public MineCannon GetMineCannon()
    {
        return this.insideMineCannon;
    }

    public void EnterMinecart(Minecart minecart)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideMinecart;
        this.insideMinecart = minecart;
        this.position = minecart.position;
        this.groundState = new GroundState(true, Orientation.Normal, 0, false, false);
        this.jumpsSinceOnGround = 2;
    }

    public void ExitMinecart()
    {
        this.groundState = GroundState.Airborne(this);
        this.state = State.Jump;
        this.insideMinecart = null;
        this.jumpsSinceOnGround = 2;
        this.coyoteTime.time = 0;
    }

    public Minecart GetMinecart()
    {
        return this.insideMinecart;
    }

    public void EnterRocket(Rocket rocket)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.state = State.InsideRocket;
        this.insideRocket = rocket;
        this.position = rocket.position;
    }

    public void ExitRocket()
    {
        this.groundState = GroundState.Airborne(this);
        this.state = State.Jump;
        this.jumpsSinceOnGround = 1;
        this.coyoteTime.time = 0;
    }

    public void GrabItem(Item item)
    {
        if (this.alive == false) return;
        if (this.state == State.Respawning) return;

        this.grabbingOntoItem = item;

        Vector2 initialGrabPos =
            this.grabbingOntoItem.grabInfo.GetGrabPositionInWorldSpace();

        this.state = State.Normal;
        this.groundState = new GroundState(
            this.groundState.onGround,
            this.groundState.orientation,
            this.gravity.FloorAngle(),
            this.groundState.groundIsIce,
            this.groundState.groundIsShallowSlope
        );

        // force direction when grabbing onto a wall-type grab-hook from opposite side
        var itemGrabType = ItemGrabTypeInGravity(this.grabbingOntoItem.grabInfo.grabType);
        if (itemGrabType == Item.GrabType.LeftWall)
            this.facingRight = false;
        else if (itemGrabType == Item.GrabType.RightWall)
            this.facingRight = true;

        // smooth player snapping onto grab items along a certain amount of time
        if (itemGrabType == Item.GrabType.Ceiling)
        {
            // 0 = smoothing disabled
            this.grabItemSmoothTime = 0;
        }
        else
        {
            this.grabItemSmoothTime = 5;
        }

        Audio.instance.PlaySfx(Assets.instance.sfxPlayerGrab);
        if (this is PlayerYolk)
        {
            Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxYolkVoice);
        }
        else if (this is PlayerPuffer)
        {
            Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxPuffer);
        }
        else if (this is PlayerSprout)
        {
            Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxSprout);
        }
        else if (this is PlayerKing)
        {
            Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxKing);
        }
        else if (this is PlayerGoop)
        {
            Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxGoop);
        }

    }

    public void EnterGoldCup(Transform goldCupTransform)
    {
        this.state = State.Finished;
        this.map.finished = true;
        this.map.finishFrameNumber = this.map.frameNumber;
        this.velocity = Vector2.zero;
        this.position.x = goldCupTransform.position.x;

        if (this.shield.isActive == true)
            this.shield.HideShieldEffect();

        if (this.swordAndShield.isActive == true)
            this.swordAndShield.HideSwordAndShieldEffect();

        if (this.midasTouch.isActive == true)
            this.midasTouch.playerMidasTouchEffect.Hide();
    }

    public void GoldCupFire()
    {
        this.velocity = Vector2.up * .75f;
        finishMoveUpwardsTime = 300;
    }

    public void AddKey(GateKey key)
    {
        keys.Add(key);
    }

    private void UseKey(GateKey key)
    {
        keys.Remove(key);
    }

    private void ClearAllKeys()
    {
        foreach(GateKey key in keys)
        {
            key.Hide();
        }

        keys.Clear();
    }

    public Vector2 KeyFollowPosition(GateKey key)
    {
        int keyIndex = keys.IndexOf(key);

        if(keyIndex > 0)
        {
            return keys[keyIndex - 1].position +
                (Vector2.right * -.5f * (this.facingRight ? 1f : -1f));
        }
        else
        {
            return this.position + (Vector2.right * -1f * (this.facingRight ? 1f : -1f));
        }
    }

    public bool HaveAvailableKey()
    {
        return keys.Count > 0;
    }

    public bool OpenGateWithKey(GateBlock gate)
    {
        if(HaveAvailableKey() == false) return false;

        GateKey lastKey = keys[keys.Count - 1];
        UseKey(lastKey);

        lastKey.MoveToGate(gate);
        
        return true;
    }

    public void OpenChestWithKey(Chest chest)
    {
        if (HaveAvailableKey() == false) return;

        GateKey lastKey = keys[keys.Count - 1];
        UseKey(lastKey);

        lastKey.MoveToChest(chest);
    }

    public Vector2 PetYellowFollowPosition(int index)
    {
        if (index > 0)
        {
            return this.petYellow.yellowPets[index - 1].position +
                (Vector2.right * -1f * (this.map.player.facingRight ? 1f : -1f));
        }
        else
        {
            return this.position + (new Vector2(-1.5f * (this.facingRight ? 1f : -1f), 1.25f));
        }
    }

    public Vector2 PetTornadoFollowPosition(int index)
    {
        if (index > 0)
        {
            return this.petTornado.tornadoPets[index - 1].position +
                (Vector2.right * -1f * (this.map.player.facingRight ? 1f : -1f));
        }
        else
        {
            return this.position + (new Vector2(-2.5f * (this.facingRight ? 1f : -1f), .5f));
        }
    }
    
    public virtual void SetCheckpointPosition(Vector2 position)
    {
        this.checkpointPosition = position;
    }

    public virtual Vector2 GetCheckpointPosition()
    {
        return this.checkpointPosition;
    }

    protected void SafeguardForEndlessPortalDrop(float nudgeMultiplier = 1f)
    {
        if (this.gravity.IsZeroGravity() == false) return;
        
        if(this.portalsEnteredInARowWhileAirborne > 7)
        {
            float nudgeForce = 0.30f * nudgeMultiplier;
            float absVelocityH = Mathf.Abs(this.velocity.x);
            float absVelocityV = Mathf.Abs(this.velocity.y);

            // at this point, the player is probably stuck between portals
            // we apply a nudge to one direction 
            if(absVelocityH > absVelocityV)
            {
                this.velocity += Vector2.up * nudgeForce;
            }

            if(absVelocityV > absVelocityH)
            {
                this.velocity += Vector2.right * nudgeForce;
            }

            this.portalsEnteredInARowWhileAirborne = 0;
        }
    }

    public void PushFromPropeller(Vector2 direction)
    {
        int index = Array.IndexOf(TilePropeller.Directions, direction);
        this.propellerPushActive[index] = true;
    }

    protected void PlayTransitionAnimation(Animated.Animation animation)
    {
        this.transitionAnimation = animation;

        if (this.animated.currentAnimation != animation)
        {
            this.animated.PlayOnce(animation, delegate
            {
                this.transitionAnimation = null;
                this.UpdateSprite();
            });
        }
    }

    private bool IsOverTopOfHorizonalChunk()
    {
        Chunk currentChunk = this.map.NearestChunkTo(this.position);
        Rect cameraRect = this.map.gameCamera.VisibleRectangle();

        if(currentChunk.isHorizontal == true || currentChunk.isTransitionalTunnel == true ||
            currentChunk.entry.type == Chunk.MarkerType.Wall || currentChunk.exit.type == Chunk.MarkerType.Wall)
        {
            if(this.position.y + 1f > cameraRect.max.y)
            {
                return true;
            }
        }

        return false;
    }
}
