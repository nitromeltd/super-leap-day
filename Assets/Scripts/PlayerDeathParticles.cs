
using UnityEngine;

public class PlayerDeathParticles
{
    public static void Death(Player player)
    {
        var deathLocation = player.position;
        var particle = Assets.instance.playerDeathParticle;
        var parent = player.transform.parent;
        var color = new Color(1.0f, 0xf8 / 255.0f, 0x34 / 255.0f);
        var speed = 7 / 16f;

        player.map.DelayedCall(0.48f, delegate
        {
            var d = Particle.CreateWithSprite(particle, 60, deathLocation, parent);
            d.spriteRenderer.color = color;
            d.velocity = new Vector2(speed, speed);

            d = Particle.CreateWithSprite(particle, 60, deathLocation, parent);
            d.transform.rotation = Quaternion.Euler(0, 0, 90);
            d.spriteRenderer.color = color;
            d.velocity = new Vector2(-speed, speed);

            d = Particle.CreateWithSprite(particle, 60, deathLocation, parent);
            d.spriteRenderer.color = color;
            d.velocity = new Vector2(-speed, -speed);

            d = Particle.CreateWithSprite(particle, 60, deathLocation, parent);
            d.transform.rotation = Quaternion.Euler(0, 0, 90);
            d.spriteRenderer.color = color;
            d.velocity = new Vector2(speed, -speed);
        });
    }

    public static void Life(
        Player player,
        Color particleColor,
        Vector2 respawnLocation,
        System.Action onComplete
    )
    {
        var particle = Assets.instance.playerDeathParticle;
        var parent = player.transform.parent;
        int lifetime = 20;
        var speed = 8 / 16f;
        var delta = speed * -lifetime;

        var d = Particle.CreateWithSprite(
            particle, lifetime, respawnLocation + new Vector2(delta, delta), parent
        );
        d.spriteRenderer.color = particleColor;
        d.velocity = new Vector2(speed, speed);
        d.onLifetimeOver = onComplete;

        d = Particle.CreateWithSprite(
            particle, lifetime, respawnLocation + new Vector2(-delta, delta), parent
        );
        d.transform.rotation = Quaternion.Euler(0, 0, 90);
        d.spriteRenderer.color = particleColor;
        d.velocity = new Vector2(-speed, speed);

        d = Particle.CreateWithSprite(
            particle, lifetime, respawnLocation + new Vector2(-delta, -delta), parent
        );
        d.spriteRenderer.color = particleColor;
        d.velocity = new Vector2(-speed, -speed);

        d = Particle.CreateWithSprite(
            particle, lifetime, respawnLocation + new Vector2(delta, -delta), parent
        );
        d.transform.rotation = Quaternion.Euler(0, 0, 90);
        d.spriteRenderer.color = particleColor;
        d.velocity = new Vector2(speed, -speed);
    }
}
