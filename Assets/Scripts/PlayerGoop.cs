using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGoop : Player
{
    [Header("Character: Goop")]
    public Sprite respawnDebrisUpSprite;
    public Sprite respawnDebrisDownSprite;

    public Animated.Animation animationSplat;
    public Animated.Animation animationWallSlideSplat;
    public Animated.Animation animationWallRoll;
    public Sprite[] blobParticles;
    public Sprite[] smallBlobParticles;

    public enum Power
    {
        ClimbWalls,
        ClimbWallsNoDrag,
        ClimbWallsAndCeiling,
        ClimbAnyShape
    }

    private Power power = Power.ClimbWalls;
    public int ballPressedTimer = 0;
    public GroundState? turningCornerGroundState = null;
    private int runBlobTime = 24;
    
    private const int BallPressedTotalTime = 10;
    private const int BallPressedTotalTimeWithParachute = 20;
    public const float BallMovementSpeed = 0.32f;

    private int CurrentBallPressedTotalTime =>
        (this.parachute.IsAvailable == true) ?
        BallPressedTotalTimeWithParachute : BallPressedTotalTime;

    public override void ChangeCharacterUpgradeLevel(int upgradeLevel)
    {
        switch(upgradeLevel)
        {
            case 1:
                this.power = Power.ClimbWalls;
            break;

            case 2:
                this.power = Power.ClimbWallsNoDrag;
            break;

            case 3:
                this.power = Power.ClimbWallsAndCeiling;
            break;

            case 4:
                this.power = Power.ClimbAnyShape;
            break;
        }
    }

    public bool CanEnterBallSpinningState()
    {
        if(IsInsideWater == true) return false;

        return this.state != State.GoopBallSpinning &&
            this.state != State.GoopBallTurningInPlace &&
            this.state != State.Respawning &&
            this.state != State.Geyser &&
            this.state != State.StuckToSticky &&
            this.state != State.InsidePipe &&
            this.state != State.EnteringLocker &&
            this.state != State.InsideBox &&
            this.state != State.InsideSpaceBooster &&
            this.state != State.InsideCityCannon &&
            this.state != State.FireFromCityCannon &&
            this.state != State.RunningOnGripGround &&
            this.state != State.InsideLift &&
            this.state != State.InsideWater;
    }
    
    public void HandleBallSpinningInput()
    {
        if(GameInput.Player(this).releasedJump == true)
        {
            if(this.state == State.GoopBallSpinning)
            {
                this.state = State.Normal;
            }

            this.ballPressedTimer = 0;
            this.turningCornerGroundState = null;
        }

        if(GameInput.Player(this).holdingJump == true &&
            CanEnterBallSpinningState())
        {
            if(this.ballPressedTimer < CurrentBallPressedTotalTime)
            {
                this.ballPressedTimer += 1;
            }
            else
            {
                this.ballPressedTimer = 0;
                this.state = State.GoopBallSpinning;
            }
        }

        if(this.map.frameNumber % this.runBlobTime == 0 &&
            (this.state == State.Normal || this.state == State.WallSlide))
        {
            CreateContinuousBlob();
        }

        if(this.map.frameNumber % 4 == 0 && this.state == State.GoopBallSpinning)
        {
            CreateBallSpinningBlob();
        }

        if ((this.state != State.Jump && this.state != State.GoopBallSpinning) && Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopSpin))
        {
            Audio.instance.StopSfx(Assets.instance.sfxGoopSpin);
        }
    }

    public void HandleBallSpinningState(Raycast raycast)
    {
        Vector2 forward = Forward();

        float force = Mathf.Abs(this.groundspeed) > PlayerGoop.BallMovementSpeed ?
                DecelerationGroundAutorun :
                AccelerationGroundAutorun;

        var target = this.facingRight ?
            PlayerGoop.BallMovementSpeed :
            -PlayerGoop.BallMovementSpeed;

        if(this.power == Power.ClimbWalls) // apply drag
        {
            if(this.groundState.orientation != Orientation.Normal)
            {
                force = 0.0025f;
                target = 0f;
            }
        }

        this.groundspeed = Util.Slide(this.groundspeed, target, force);

        if(this.groundState.onGround == true)
        {
            this.velocity = forward * this.groundspeed;
        }
        else
        {
            this.velocity += GravityAcceleration *  this.gravity.Down();
        }

        for (var n = 0; n < 5; n += 1)
        {
            this.position += this.velocity / 5;

            HandleCrush(raycast);
            if (this.alive == false) break;

            HandleEFWallSensors(raycast);
            HandleCDCeilingSensors(raycast);
            HandleABFloorSensors(raycast);
            HandleTriggers();
        }

        if(this.groundState.onGround == true)
        {
            if (this.groundspeed < 0) this.facingRight = false;
            if (this.groundspeed > 0) this.facingRight = true;
        }
        else
        {
            var velocityForwardComponent = Vector2.Dot(forward, this.velocity);
            if (velocityForwardComponent < 0) this.facingRight = false;
            if (velocityForwardComponent > 0) this.facingRight = true;
        }

        StopWallSlideSfx();
    }
    
    public override void Die()
    {
        base.Die();

        this.ballPressedTimer = 0;
    }

    protected override void DeathFX()
    {
        base.DeathFX();

        this.map.DelayedCall(0.40f, CreateBlobDeath);
        Audio.instance.PlaySfx(Assets.instance.sfxGoopDeath);
    }

    protected override void RespawnFX()
    {
        this.position = this.position.Add(0f, -6f);

        PlayerDeathParticles.Life(
            this,
            Color.white,
            this.position.Add(0f, 0.50f), 
            delegate
        {
            this.spriteRenderer.enabled = true;
            this.animated.PlayOnce(this.animationRespawn, delegate
            {
                this.state = State.Normal;
            });

            this.animated.OnFrame(15, () =>
            {
                CreateRespawnDebris();
                CreateBlobRespawn();
            });
        });
        Audio.instance.PlaySfx(Assets.instance.sfxGoopRespawn);
    }

    private void CreateRespawnDebris()
    {
        Vector2 upPosition  = this.position.Add(0f, 0.50f);
        Vector2 downPosition = this.position.Add(0f, 0f);

        var up = Particle.CreateWithSprite(
            this.respawnDebrisUpSprite, 100, upPosition, this.transform.parent
        );
        up.velocity     = new Vector2(-1f, 7f) / 16f;
        up.acceleration = new Vector2(0f, -0.5f) / 16f;

        var down = Particle.CreateWithSprite(
            this.respawnDebrisDownSprite, 100, downPosition, this.transform.parent
        );
        down.velocity     = new Vector2(1f, 7f) / 16f;
        down.acceleration = new Vector2(0f, -0.5f) / 16f;
    }

    public void TriggerSplat()
    {
        if(this.state == State.GoopBallSpinning) return;

        this.animated.PlayOnce(this.animationSplat);
        CreateBlobLand();
        Audio.instance.PlaySfx(Assets.instance.sfxGoopLand);

        StopWallSlideSfx();
    }
    
    protected override void PlayStanding()
    {
        if(this.animated.currentAnimation != this.animationSplat)
        {
            this.animated.PlayAndLoop(this.animationStanding);
        }
    }

    protected override void PlayRunning()
    {
        if(this.animated.currentAnimation != this.animationSplat)
        {
            this.animated.PlayAndLoop(this.animationRunning);
        }
        this.animated.OnFrame(2, delegate
        {
            if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopStep[0]) && !Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopStep[1]))
                Audio.instance.PlayRandomSfx(options: new Audio.Options(0.5f, false, 0), null, Assets.instance.sfxGoopStep);
        });
        this.animated.OnFrame(9, delegate
        {
            if(!Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopStep[0]) && !Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopStep[1]))
                Audio.instance.PlayRandomSfx(options: new Audio.Options(0.5f, false, 0), null, Assets.instance.sfxGoopStep);
        });
    }

    protected override void PlayWallSlide()
    {
        bool enteringWallSlide =
            this.animated.currentAnimation != this.animationWallSlideSplat &&
            this.animated.currentAnimation != this.animationWallSliding;

        if(this.state == State.GoopBallSpinning)
        {
            enteringWallSlide = false;
        }
        
        if(enteringWallSlide == true)
        {
            this.animated.PlayOnce(this.animationWallSlideSplat, () =>
            {
                this.animated.PlayAndLoop(this.animationWallSliding);
            });

            CreateBlobWall();
        }
        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopWallslide))
        {
            Audio.instance.PlaySfx(Assets.instance.sfxGoopWallslide, options: new Audio.Options(0.5f, false, 0));
        }
    }

    public void StopWallSlideSfx()
    {
        Audio.instance.StopSfx(Assets.instance.sfxGoopWallslide);
    }

    public void StopSpinSfx()
    {
        Audio.instance.StopSfx(Assets.instance.sfxGoopSpin);
    }
    
    private void CreateContinuousBlob()
    {
        float xValue = 0.75f * (this.state == State.WallSlide ? 1f : -1f);
        float x = this.position.x + xValue * (this.facingRight ? 1f : -1f);
        float y = this.position.y;

        float xVariation = 0.25f;
        float yVariation = 0.50f;
        Vector2 originPosition = new Vector2(x, y);
        originPosition += new Vector2(
            UnityEngine.Random.Range(-xVariation, xVariation),
            UnityEngine.Random.Range(-yVariation, yVariation)
        );

        var p = Particle.CreateWithSprite(
            GetRandomSmallBlobParticle(),
            60 * 2,
            originPosition,
            this.transform.parent
        );
        p.velocity.x = UnityEngine.Random.Range(0.02f, 0.05f) * (this.facingRight ? 1f : -1f);
        p.velocity.y = UnityEngine.Random.Range(0.15f, 0.25f);

        float verticalAccelaration = -0.027f;
        p.acceleration = new Vector2(0f, verticalAccelaration);

        this.runBlobTime = UnityEngine.Random.Range(16, 30);

        if(this.midasTouch.isActive == true)
        {
            p.spriteRenderer.material = Assets.instance.spriteGoldTint;
        }
    }

    private void CreateBallSpinningBlob()
    {
        float x = this.position.x - 0.75f * (this.facingRight ? 1f : -1f);
        float y = this.position.y - 0.50f;

        Vector2 originPosition = new Vector2(x, y);

        var p = Particle.CreateWithSprite(
            GetRandomSmallBlobParticle(),
            60 * 2,
            originPosition,
            this.transform.parent
        );
        p.velocity.x = UnityEngine.Random.Range(0.02f, 0.05f) * (this.facingRight ? 1f : -1f);
        p.velocity.y = UnityEngine.Random.Range(0.15f, 0.25f);

        float verticalAccelaration = -0.027f;
        p.acceleration = new Vector2(0f, verticalAccelaration);

        if (this.midasTouch.isActive == true)
        {
            p.spriteRenderer.material = Assets.instance.spriteGoldTint;
        }

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxGoopSpin))
        {
            Audio.instance.PlaySfx(Assets.instance.sfxGoopSpin, options: new Audio.Options(0.5f, false, 0));
        }
    }

    public void CreateBlobLand(int blobAmount = 10)
    {
        float x = this.position.x + 0.50f * (this.facingRight ? 1f : -1f);
        float y = this.position.y - 1.50f;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < blobAmount; n += 1)
        {
            var p = Particle.CreateWithSprite(
                GetRandomBlobParticle(),
                60 * 2,
                originPosition,
                this.transform.parent
            );
            p.velocity.x =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(0.03f, 0.20f));
            p.velocity.x += -0.05f * (this.facingRight ? 1f : -1f);
            p.velocity.y = UnityEngine.Random.Range(0.17f, 0.30f);

            float verticalAccelaration = -0.027f;
            p.acceleration = new Vector2(0f, verticalAccelaration);

            if (this.midasTouch.isActive == true)
            {
                p.spriteRenderer.material = Assets.instance.spriteGoldTint;
            }
        }
    }

    private void CreateBlobWall()
    {
        float x = this.position.x;
        float y = this.position.y;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateWithSprite(
                GetRandomBlobParticle(),
                60 * 2,
                originPosition,
                this.transform.parent
            );
            p.velocity.y = UnityEngine.Random.Range(0.03f, 0.30f);
            p.velocity.x = UnityEngine.Random.Range(0.03f, 0.20f)
                * (this.facingRight ? -1f : 1f);

            float verticalAccelaration = -0.027f;
            p.acceleration = new Vector2(0f, verticalAccelaration);

            if (this.midasTouch.isActive == true)
            {
                p.spriteRenderer.material = Assets.instance.spriteGoldTint;
            }
        }
    }
    
    private void CreateBlobDeath()
    {
        float x = this.position.x;
        float y = this.position.y;

        float circleRadius = 1f;
        int particlesAmount = 16;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < particlesAmount; n += 1)
        {
            float currentAngle = (n * Mathf.PI * 2) / particlesAmount;
            Vector2 offset =
                new Vector2(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle)) * circleRadius;
            Vector2 spawnPos = originPosition + offset;

            var p = Particle.CreateWithSprite(
                GetRandomBlobParticle(),
                60 * 2,
                spawnPos,
                this.transform.parent
            );
            p.velocity.x =
                (((spawnPos.x > originPosition.x) ? 1 : -1) *
                UnityEngine.Random.Range(0.03f, 0.20f));
            p.velocity.y = UnityEngine.Random.Range(0.17f, 0.30f);

            float verticalAccelaration = -0.027f;
            p.acceleration = new Vector2(0f, verticalAccelaration);
            if (this.midasTouch.isActive == true)
            {
                p.spriteRenderer.material = Assets.instance.spriteGoldTint;
            }
        }
    }
    
    private void CreateBlobRespawn()
    {
        float x = this.position.x + 0.50f * (this.facingRight ? 1f : -1f);
        float y = this.position.y + 0.50f;

        Vector2 originPosition = new Vector2(x, y);

        for (var n = 0; n < 10; n += 1)
        {
            var p = Particle.CreateWithSprite(
                GetRandomBlobParticle(),
                60 * 2,
                originPosition,
                this.transform.parent
            );
            p.velocity.x =
                (((n % 2 == 0) ? 1 : -1) *
                UnityEngine.Random.Range(0.03f, 0.20f));
            p.velocity.x += -0.05f * (this.facingRight ? 1f : -1f);
            p.velocity.y = UnityEngine.Random.Range(0.30f, 0.60f);

            float verticalAccelaration = -0.027f;
            p.acceleration = new Vector2(0f, verticalAccelaration);
            if (this.midasTouch.isActive == true)
            {
                p.spriteRenderer.material = Assets.instance.spriteGoldTint;
            }
        }
    }

    private Sprite GetRandomBlobParticle()
    {
        return this.blobParticles[Random.Range(0, this.blobParticles.Length)];
    }

    private Sprite GetRandomSmallBlobParticle()
    {
        return this.smallBlobParticles[Random.Range(0, this.smallBlobParticles.Length)];
    }

    public GroundState GetMovingLeftGroundState()
    {
        float angleDegrees = 0f;
        Orientation orientation = Orientation.Normal;

        if((int)this.power < (int)Power.ClimbWallsAndCeiling &&
            this.groundState.orientation == Orientation.LeftWall)
        {
            return new GroundState(
                true,
                this.groundState.orientation,
                this.groundState.angleDegrees,
                false,
                false
            );
        }
        
        switch(this.groundState.orientation)
        {
            case Orientation.Normal:
                angleDegrees = 270f;
                orientation = Orientation.LeftWall;
            break;

            case Orientation.LeftWall:
                angleDegrees = 180f;
                orientation = Orientation.Ceiling;
            break;

            case Orientation.Ceiling:
                angleDegrees = 90f;
                orientation = Orientation.RightWall;
            break;

            case Orientation.RightWall:
                angleDegrees = 0f;
                orientation = Orientation.Normal;
            break;

        }

        return new GroundState(
            true,
            orientation,
            angleDegrees,
            false,
            false
        );
    }

    public GroundState GetMovingRightGroundState()
    {
        float angleDegrees = 0f;
        Orientation orientation = Orientation.Normal;
        
        if((int)this.power < (int)Power.ClimbWallsAndCeiling &&
            this.groundState.orientation == Orientation.RightWall)
        {
            return new GroundState(
                true,
                this.groundState.orientation,
                this.groundState.angleDegrees,
                false,
                false
            );
        }

        switch(this.groundState.orientation)
        {
            case Orientation.Normal:
                angleDegrees = 90f;
                orientation = Orientation.RightWall;
            break;

            case Orientation.RightWall:
                angleDegrees = 180f;
                orientation = Orientation.Ceiling;
            break;

            case Orientation.Ceiling:
                angleDegrees = 270f;
                orientation = Orientation.LeftWall;
            break;

            case Orientation.LeftWall:
                angleDegrees = 0f;
                orientation = Orientation.Normal;
            break;
        }

        return new GroundState(
            true,
            orientation,
            angleDegrees,
            false,
            false
        );
    }

    public void BallSpinningCornerUpwardsImpulse()
    {
        switch(this.gravity.Orientation())
        {
            case Orientation.Normal:
            {
                if((this.groundState.orientation == Orientation.RightWall ||
                        this.groundState.orientation == Orientation.LeftWall) &&
                    this.velocity.y > 0f)
                {
                    this.groundState = new GroundState(
                        false,
                        Orientation.Normal,
                        0f,
                        false,
                        false
                    );

                    this.velocity.x = RunSpeed * (this.facingRight ? 1f : -1f);
                    this.velocity.y *= 1.20f;
                }
            }
            break;

            case Orientation.LeftWall:
            {
                if((this.groundState.orientation == Orientation.Normal ||
                        this.groundState.orientation == Orientation.Ceiling) &&
                    this.velocity.x > 0f)
                {
                    this.groundState = new GroundState(
                        false,
                        Orientation.LeftWall,
                        270f,
                        false,
                        false
                    );

                    this.velocity.x *= 1.20f;
                    this.velocity.y = RunSpeed * (this.facingRight ? -1f : 1f);
                }
            }
            break;

            case Orientation.Ceiling:
            {
                if((this.groundState.orientation == Orientation.RightWall ||
                        this.groundState.orientation == Orientation.LeftWall) &&
                    this.velocity.y < 0f)
                {
                    this.groundState = new GroundState(
                        false,
                        Orientation.Ceiling,
                        180f,
                        false,
                        false
                    );

                    this.velocity.x = RunSpeed * (this.facingRight ? -1f : 1f);
                    this.velocity.y *= 1.20f;
                }
            }
            break;

            case Orientation.RightWall:
            {
                if((this.groundState.orientation == Orientation.Normal ||
                        this.groundState.orientation == Orientation.Ceiling) &&
                    this.velocity.x < 0f)
                {
                    this.groundState = new GroundState(
                        false,
                        Orientation.RightWall,
                        90f,
                        false,
                        false
                    );

                    this.velocity.x *= 1.20f;
                    this.velocity.y = RunSpeed * (this.facingRight ? 1f : -1f);
                }
            }
            break;
        }
    }

    public bool CanTurnCorners()
    {
        return this.power == Power.ClimbAnyShape;
    }

    public void EnterBallSpinningTurnCorner()
    {
        if(this.gravity.Orientation() == Orientation.Normal && this.groundState.orientation == Orientation.Ceiling ||
           this.gravity.Orientation() == Orientation.Ceiling && this.groundState.orientation == Orientation.Normal)
        {
            this.turningCornerGroundState = this.velocity.x > 0f ?
                new GroundState(true, Orientation.LeftWall, 270f, false, false) :
                new GroundState(true, Orientation.RightWall, 90f, false, false);
        }

        if(this.gravity.Orientation() == Orientation.RightWall && this.groundState.orientation == Orientation.LeftWall ||
            this.gravity.Orientation() == Orientation.LeftWall && this.groundState.orientation == Orientation.RightWall)
        {
            this.turningCornerGroundState = this.velocity.y > 0f ?
                new GroundState(true, Orientation.Normal, 0f, false, false) :
                new GroundState(true, Orientation.Ceiling, 180f, false, false);
        }
    }

    public void CancelSpinning()
    {
        this.state = State.Normal;
        this.velocity.y = 0f;
    }
}
