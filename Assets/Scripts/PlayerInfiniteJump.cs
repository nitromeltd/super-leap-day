using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerInfiniteJump
{
    [NonSerialized] public bool isActive = false;
    private bool wasActive = false;

    public GameObject wingPrefab;
    [NonSerialized] public PlayerWings wings;

    private Player player;

    private Particle uiParticle;

    [NonSerialized] public int hearts;
    [NonSerialized] public bool needsHeart = false;

    [NonSerialized] public bool activatedByReferee = false;

    public void Init(Player player)
    {
        this.player = player;
        this.hearts = 0;
    }

    public void Advance()
    {
        if (this.wings != null)
        {
            this.wings.Advance();
        }

        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool activatedByBubble = false, bool activatedByReferee = false)
    {
        if (this.isActive == true)
            return;

        this.activatedByReferee = activatedByReferee;
        this.isActive = true;
        this.wasActive = true;

        if (hearts > 0)
        {
            this.hearts--;
        }

        if(this.hearts < 3)
        {
            this.needsHeart = true;
        }

        wings = GameObject.Instantiate(
                this.wingPrefab,
                this.player.transform.position,
                this.player.transform.rotation
            ).GetComponent<PlayerWings>();

        wings.transform.parent = this.player.transform;

        wings.Play(this.player);

        if (this.player.midasTouch.isActive == true)
        {
            wings.GetComponent<SpriteRenderer>().material = Assets.instance.spriteGoldTint;
        }

        if (activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),
                this.player.map.transform
            );
            
            this.uiParticle.transform.localScale = Vector3.one * 0.35f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }
    }

    public void End()
    {
        this.activatedByReferee = false;
        if (this.isActive == true)
        {
            this.isActive = false;
            this.wasActive = true;
        }
        else
        {
            this.wasActive = false;
        }

        if (this.wings != null)
        {
            this.wings.Stop();
            this.wings = null;
        }
    }

    public void Respawn()
    {
        if (this.wasActive == true && this.hearts > 0)
        {
            Activate();
            wings.LoseHeart();
        }
        else
        {
            this.wasActive = false;
            this.needsHeart = false;
        }
    }

    public void AddHearts()
    {
        if (this.hearts < 3)
        {
            this.hearts++;
            wings.GainHeart();
            if (this.hearts == 3)
            {
                this.needsHeart = false;
            }
        }
    }

    public IEnumerator DelayedActivation()
    {
        yield return 0;
        Activate();
    }
}
