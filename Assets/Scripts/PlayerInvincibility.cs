﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerInvincibility
{
    public AnimationCurve afterImageCurve;
    public Color[] afterImageColors;
    [NonSerialized] public bool isActive = false;

    private Player player;
    private float timer = 0f;
    // blinking effect
    private int blinkingTime = 0;
    private Particle blinkingParticle;
    private bool blinkingEffect = false;
    private float blinkDurationTimer = 0f;
    // after image effect
    private int afterImageColorIndex = 0;
    private int afterImageSortingOrder = -1000;
    private Vector2 lastPosition;
    private Particle uiParticle;
    [NonSerialized] public bool activatedByReferee = false;
    private int activateAgain = 0;

    private class AfterImage
    {
        public Particle particle;
        public Color color;
    };
    private List<AfterImage> afterImageParticles = new List<AfterImage>();

    private static readonly float   HueEffectSpeed          = 0.05f;
    private static readonly float   AfterImageMaxDistance   = 0.7f;
    private static readonly float   BlinkDurationTime       = 0.05f;
    private static readonly int     TotalTime               = 60 * 20;
    private static readonly int     TotalExtendedTime        = 60 * 34;
    private static readonly int     StartBlinkingTime       = 60 * 3;
    private static readonly int     AfterImageLifetime      = 60;
    private static readonly string  HueEffectParam          = "_HueValue";
    private static readonly string  BlinkingParticleName    = "Invincibility Blinking Particle";

    public void Init(Player player)
    {
        this.player = player;
    }

    public void Advance()
    {
        if (!this.isActive)
        {
            if (this.activateAgain > 0 && this.player.map.isInvincible == false)
            {
                this.activateAgain--;
                Activate();
            }
            return;
        }

        if (this.timer > 0)
        {
            this.timer -= 1;
        }

        if(DebugPanel.debugInvincibilityCheat)
            return;

        if (this.timer <= 0 || (!Audio.instance.IsSfxPlaying(Assets.instance.musicInvincible) && activatedByReferee == false && this.timer < 60 * 19) || 
            (!Audio.instance.IsSfxPlaying(Assets.instance.musicInvincibleExtended) && activatedByReferee == true && this.timer < 60 * 19))
        {
            End();
        }

        // rainbow hue effect
        float hueValue = Mathf.PingPong(this.timer * HueEffectSpeed, 1f);
        this.player.spriteRenderer.material.SetFloat(HueEffectParam, hueValue);
        this.player.backArmSpriteRenderer.material.SetFloat(HueEffectParam, hueValue);
        this.player.frontArmSpriteRenderer.material.SetFloat(HueEffectParam, hueValue);

        if(this.player is PlayerPuffer)
        {
            (this.player as PlayerPuffer).headSpriteRenderer.material.SetFloat(HueEffectParam, hueValue);
        }

        // the particle will stay on top of player sprite to emulate a blinking effect
        this.blinkingParticle.transform.position = this.player.transform.position;
        this.blinkingParticle.transform.localScale = this.player.transform.localScale;
        this.blinkingParticle.transform.localRotation = this.player.transform.localRotation;
        this.blinkingParticle.spriteRenderer.sprite = this.player.spriteRenderer.sprite;

        // control when to trigger the blink
        if(this.timer < StartBlinkingTime)
        {
            this.blinkingTime = this.blinkingTime > 0 ?
                this.blinkingTime - 1 : 0;

            if(this.blinkingTime <= 0)
            {
                this.blinkingTime = 
                    Mathf.RoundToInt(Mathf.Lerp(15, 5,
                        (StartBlinkingTime - this.timer) / StartBlinkingTime
                ));

                this.blinkingEffect = true;
            }
        }

        // control how long the blink should last
        if(this.blinkingEffect == true)
        {
            this.blinkDurationTimer += Time.deltaTime;

            if(this.blinkDurationTimer > BlinkDurationTime)
            {
                this.blinkingEffect = false;
                this.blinkDurationTimer = 0f;
            }
        }

        // after image effect (trail)
        if(Vector2.Distance(this.player.position, this.lastPosition)
            > AfterImageMaxDistance)
        {
            this.lastPosition = this.player.position;

            Particle aiParticle = Particle.CreateWithSprite(
                this.player.spriteRenderer.sprite,
                AfterImageLifetime,
                this.player.position,
                this.player.map.transform
            );
            AfterImage afterImage = new AfterImage
            {
                particle = aiParticle,
                color = this.afterImageColors[this.afterImageColorIndex]
            };

            this.afterImageParticles.Add(afterImage);

            aiParticle.onLifetimeOver = delegate
            {
                if(aiParticle)
                {
                    this.afterImageParticles.Remove(afterImage);
                }
            };

            // increase sorting order to always render last after image on top
            this.afterImageSortingOrder++;

            // reset sorting order after reaching player's sprite sorting value
            if(this.afterImageSortingOrder >= player.spriteRenderer.sortingOrder)
            {
                this.afterImageSortingOrder = -1000;
            }

            aiParticle.spriteRenderer.material = Assets.instance.spritesFullColorMaterial;
            aiParticle.spriteRenderer.color = afterImage.color;

            this.afterImageColorIndex =
                this.afterImageColorIndex < this.afterImageColors.Length - 1 ?
                this.afterImageColorIndex + 1 : 0;

            aiParticle.spriteRenderer.sortingLayerName = "Player (BL)";
            aiParticle.spriteRenderer.sortingOrder = this.afterImageSortingOrder;
            aiParticle.transform.localScale = this.player.transform.localScale;
            aiParticle.transform.localRotation = this.player.transform.localRotation;

            aiParticle.FadeOut(this.afterImageCurve);

            // sparkle particles
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.playerInvincibleSparkleAnimation,
                this.player.position.Add(
                    UnityEngine.Random.Range(-.5f, -.1f) * (this.player.facingRight ? 1f : -1f),
                    UnityEngine.Random.Range(-0.85f, 0.85f)
                ),
                this.player.transform.parent
            );
        }

        // apply blinking effect to sprites (full white color)
        for (int i = 0; i < this.afterImageParticles.Count; i++)
        {
            if (this.afterImageParticles == null) continue;

            this.afterImageParticles[i].particle.fadeout.color =
                this.blinkingEffect == true ?
                Color.white :
                this.afterImageParticles[i].color;
        }

        this.blinkingParticle.spriteRenderer.enabled = this.blinkingEffect;

        // visual effect: powerup sprite moving from UI box to player
        if(this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if(Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool? activatedByBubble = false, bool? activatedByReferee = false)
    {
        if(this.isActive == true)
        {
            this.activateAgain++;
            return;
        }

        this.isActive = true;

        this.activatedByReferee = activatedByReferee.Value;
        if (this.activatedByReferee == true)
        {
            this.timer = TotalExtendedTime;
            this.blinkingParticle = Particle.CreateWithSprite(
                Assets.instance.emptySprite,
                TotalExtendedTime,
                this.player.transform.position,
                this.player.transform.parent);
            this.player.infiniteJump.Activate(activatedByBubble: true, activatedByReferee: true);
        }
        else
        {
            this.timer = TotalTime;
            this.blinkingParticle = Particle.CreateWithSprite(
                Assets.instance.emptySprite,
                TotalTime,
                this.player.transform.position,
                this.player.transform.parent);
        }

        this.blinkingTime = 0;
        this.blinkingParticle.spriteRenderer.color = new Color(1f, 1f, 1f, 0.75f);
        this.blinkingParticle.spriteRenderer.material
            = Assets.instance.spritesFullColorMaterial;
        this.blinkingParticle.spriteRenderer.enabled = false;
        this.blinkingParticle.gameObject.name = BlinkingParticleName;

        if(activatedByBubble == false)
        {
            var pos = (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f);

            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                pos,
                this.player.map.transform
            );

            this.uiParticle.transform.localScale = Vector3.one * 0.65f;
            this.uiParticle.spin = 2.50f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }
        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxPowerupInvincibility))
        {
            Audio.instance.PlaySfxLoop(Assets.instance.sfxPowerupInvincibility);
        }
    }

    public void End()
    {
        this.isActive = false;
        this.timer = 0f;
        this.blinkingTime = 0;
        this.blinkDurationTimer = 0f;
        
        this.player.spriteRenderer.material.SetFloat(HueEffectParam, 0f);
        this.player.backArmSpriteRenderer.material.SetFloat(HueEffectParam, 0f);
        this.player.frontArmSpriteRenderer.material.SetFloat(HueEffectParam, 0f);
        
        if(this.player is PlayerPuffer)
        {
            (this.player as PlayerPuffer).headSpriteRenderer.material.SetFloat(HueEffectParam, 0f);
        }
        Audio.instance.StopSfxLoop(Assets.instance.sfxPowerupInvincibility);

        if (this.activatedByReferee == true)
        {
            this.activatedByReferee = false;
            if(this.player.infiniteJump.activatedByReferee == true)
            {
                this.player.infiniteJump.End();
            }
        }
    }
}
