using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerKing : Player
{
    [Header("Character: King")]
    public Animated.Animation animationStompDropPrep;
    public Animated.Animation animationStompDropLand;
    public Animated.Animation animationStompDropDust;

    public GameObject kingHatPrefab;
    public Animated hatAnimated;
    public SpriteRenderer hatSpriteRenderer;
    public Sprite hatDeathSprite;

    [Header("Hat Animations")]
    public Animated.Animation animationHatBlank;
    public Animated.Animation animationHatStanding;
    public Animated.Animation animationHatRunning;
    public Animated.Animation animationHatRunningPanicked;
    public Animated.Animation animationHatRunningTooFast;
    public Animated.Animation animationHatJumping;
    public Animated.Animation animationHatJumpingDown;
    public Animated.Animation animationHatDoubleJumping;
    public Animated.Animation animationHatStompFrame;
    public Animated.Animation animationHatAfterStomp;
    public Animated.Animation animationHatWallSliding;
    public Animated.Animation animationHatIceSliding;
    public Animated.Animation animationHatPoleSliding;
    public Animated.Animation animationHatPoleSlidingTwist;
    public Animated.Animation animationHatSpring;
    public Animated.Animation animationHatHanging;
    public Animated.Animation animationHatGrabSide;
    public Animated.Animation animationHatGrabStand;
    public Animated.Animation animationHatSwimmingSurface;
    public Animated.Animation animationHatSwimmingCeiling;
    public Animated.Animation animationHatHoldingBreath;
    public Animated.Animation animationHatWithParachuteOpened;
    public Animated.Animation animationHatWithParachuteClosed;

    public Animated.Animation animationHatTransitionWallBounce;
    public Animated.Animation animationHatTransitionWallBouncePanicked;
    public Animated.Animation animationHatTransitionRunToWallSlide;
    public Animated.Animation animationHatTransitionWallSlideToRun;
    public Animated.Animation animationHatTransitionWallSlideToDrop;
    public Animated.Animation animationHatTransitionJumpToJumpDown;
    public Animated.Animation animationHatTransitionSwimToSwimUp;

    public Animated.Animation animationHatStompDropLand;

    [Header("Hurt")]
    public Animated.Animation animationKingHurt;
    public Animated.Animation animationHurtEffect;

    [Header("Death & Respawn")]
    public Animated.Animation animationKingRespawn;
    public Animated.Animation particleKingRespawn;

    public enum StompPower
    {
        OnlyStop,
        AffectEnemies,
        AffectEnemiesAndItems
    }

    public enum HatRespawnMode
    {
        None,
        OnCheckpoints,
        OnFruits,
        OnStompAttack
    }

    private Dictionary<Animated.Animation, Animated.Animation> hatAnimationsDictionary;
    private Dictionary<Animated.Animation, Animated.Animation> hatTransitionsDictionary;
    private StompPower stompPower = StompPower.AffectEnemiesAndItems;
    private HatRespawnMode hatRespawnMode = HatRespawnMode.None;

    public Vector2 HeadPosition => this.position + (this.gravity.Up() * 1.75f);

    [HideInInspector] public bool isHatActive = true;
    private KingHat kingHat = null;
    [HideInInspector] public bool isDropping = false;
    private bool respawnSpin = false;
    private float respawnAngleDegrees = 0f;
    private int stompHoldTimer = 0;
    private int stompAttackTimer = 0;
    private int invulnerableTimer = 0;
    private float stompShortRange = 0f;
    private float stompLongRange = 0f;
    private bool spawnHatOnStomp = false;
    private Vector2 spawnHatStompPosition;
    private int spawnHatStompTimer = 0;
    private bool canCreateFirstHat = true;

    public bool IsPreparingStomp => this.stompHoldTimer > 0;

    private static readonly int StompDropHoldTotalTime = 18;
    private static readonly int StompDropHoldTotalTimeWithParachute = 30;
    private static readonly int StompDropAttackTime = 10;
    private static readonly float StompDropAttackAccel = -0.05f;

    private int CurrentStompDropHoldTotalTime =>
        (this.parachute.IsAvailable == true) ?
        StompDropHoldTotalTimeWithParachute : StompDropHoldTotalTime;

    private const int InvulnerableAfterHitTime = 90;

    public override void ChangeCharacterUpgradeLevel(int upgradeLevel)
    {
        switch (upgradeLevel)
        {
            case 1:
                this.stompPower = StompPower.OnlyStop;
                this.hatRespawnMode = HatRespawnMode.None;
                this.stompShortRange = 4f;
                this.stompLongRange = 8f;
                break;

            case 2:
                this.stompPower = StompPower.AffectEnemies;
                this.hatRespawnMode = HatRespawnMode.OnCheckpoints;
                this.stompShortRange = 6f;
                this.stompLongRange = 10f;
                break;

            case 3:
                this.stompPower = StompPower.AffectEnemiesAndItems;
                this.hatRespawnMode = HatRespawnMode.OnFruits;
                this.stompShortRange = 8f;
                this.stompLongRange = 12f;
                break;

            case 4:
                this.stompPower = StompPower.AffectEnemiesAndItems;
                this.hatRespawnMode = HatRespawnMode.OnStompAttack;
                this.stompShortRange = 10f;
                this.stompLongRange = 14f;
                break;
        }
    }

    public void HandleStompDropInput()
    {
        if (IsInvulnerable() == true)
        {
            this.invulnerableTimer -= 1;

            // invulnerable blinking effect
            if (this.map.frameNumber % 2 == 0)
            {
                this.spriteRenderer.enabled = !this.spriteRenderer.enabled;
            }

            if (this.invulnerableTimer <= 0)
            {
                this.spriteRenderer.enabled = true;
            }
        }

        if (GameInput.Player(this).holdingJump == true &&
            this.state != State.KingStompDrop &&
            this.groundState.onGround == false &&
            this.jumpsSinceOnGround > 1)
        {
            this.stompHoldTimer += 1;
        }

        if (this.stompHoldTimer >= CurrentStompDropHoldTotalTime &&
            this.state != State.KingStompDrop)
        {
            this.state = State.KingStompDrop;
            this.stompHoldTimer = 0;
            this.velocity = Vector2.zero;
        }

        if (GameInput.Player(this).releasedJump == true)
        {
            this.stompHoldTimer = 0;
        }

        switch(this.hatRespawnMode)
        {
            case HatRespawnMode.OnCheckpoints:
                RespawnHatOnCheckpoint();
            break;

            case HatRespawnMode.OnFruits:
            case HatRespawnMode.OnStompAttack:
                RespawnHatOnCheckpoint();
                RespawnHatOnFruits();
            break;
        }

        if(this.spawnHatOnStomp == true && this.isHatActive == false)
        {
            this.spawnHatStompTimer += 1;

            if(this.map.frameNumber % 2 == 0)
            {
                float sparkleRadius = 1;
                var sparkle = Particle.CreateAndPlayOnce(
                    Assets.instance.coinSparkleAnimation,
                    this.spawnHatStompPosition + (UnityEngine.Random.insideUnitCircle * sparkleRadius),
                    this.transform.parent
                );

                bool front = Random.value > 0.75f;
                sparkle.spriteRenderer.sortingLayerName = front ? "Items (Front)" : "Items";
                sparkle.spriteRenderer.sortingOrder = front ? 11 : 1;
            }

            if(this.spawnHatStompTimer >= 60)
            {
                // spawn hat after stomping attack
                this.spawnHatOnStomp = false;
                this.spawnHatStompTimer = 0;
                this.kingHat.position = this.spawnHatStompPosition;
                this.kingHat.ThrowOnStompDrop();
                this.kingHat.SwitchToNearestChunk();
            }
        }
    }

    private void RespawnHatOnCheckpoint()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.position);

        if (this.kingHat != null && 
            this.kingHat.CanGoToCheckpoint() &&
            Mathf.Abs(playerChunk.index - this.kingHat.chunk.index) > 1)
        {
            Chunk nextCheckpointChunk = this.map.NextCheckpointFromPosition(this.position);

            if (nextCheckpointChunk != null &&
                nextCheckpointChunk.gameObject.activeSelf == true)
            {
                Checkpoint checkpoint = nextCheckpointChunk.AnyItem<Checkpoint>();

                if (checkpoint != null && checkpoint.IsKingHatActive == false)
                {
                    this.kingHat.PlaceOnCheckpoint(checkpoint);
                }
            }
        }
    }

    private void RespawnHatOnFruits()
    {
        Fruit GetNextFruit()
        {
            Chunk currentChunk = this.map.NearestChunkTo(position);

            foreach(var c in this.map.chunks)
            {
                if(c.index <= currentChunk.index) continue;
                if(c.gameObject.activeSelf == false) continue;

                List<Fruit> fruits = new List<Fruit>();

                foreach(var i in c.items)
                {
                    if(i is Fruit)
                    {
                        fruits.Add(i as Fruit);
                    }
                }

                if(fruits.Count > 0)
                {
                    return fruits[Random.Range(0, fruits.Count)];
                }
            }

            return null;
        }

        Chunk playerChunk = this.map.NearestChunkTo(this.position);

        if (this.kingHat != null && 
            this.kingHat.CanReplaceFruit() &&
            Mathf.Abs(playerChunk.index - this.kingHat.chunk.index) > 1)
        {
            Fruit nextFruit = GetNextFruit();

            if (nextFruit != null)
            {
                this.kingHat.ReplaceFruit(nextFruit);
            }
        }
    }

    public void HandleStompDropState(Raycast raycast)
    {
        // on Air
        if (this.groundState.onGround == false)
        {
            this.gravity.VelocityInGravityReferenceFrame = new Vector2(
                0f,
                this.gravity.VelocityInGravityReferenceFrame.y
            );

            SafeguardForEndlessPortalDrop(nudgeMultiplier: 5f);

            // ATTACK
            if (this.stompAttackTimer < StompDropAttackTime &&
                this.isDropping == false)
            {
                this.stompAttackTimer += 1;
            }

            if (this.stompAttackTimer >= StompDropAttackTime)
            {
                this.isDropping = true;
                this.stompAttackTimer = 0;
                Audio.instance.PlaySfx(Assets.instance.sfxKingGroundslamDrop);
                Audio.instance.PlaySfxSometimes(0.35f, audioClips: Assets.instance.sfxKingGroundslamVoice);

                CheckForHatSpawnOnStompDrop();
                LoseHatOnStompDrop();
            }

            // DROP
            if (this.isDropping == true)
            {
                this.gravity.VelocityInGravityReferenceFrame += Vector2.up * StompDropAttackAccel;
                
                float stompTopSpeed = TopSpeed * 3f;
                if(Mathf.Abs(this.gravity.VelocityUp) > stompTopSpeed)
                {
                    this.gravity.VelocityUp = stompTopSpeed * Mathf.Sign(this.gravity.VelocityUp);
                }
            }

            // CANCEL
            if (GameInput.Player(this).releasedJump == true)
            {
                this.state = State.DropVertically;
                this.stompAttackTimer = 0;
                this.isDropping = false;
            }

        }
        // on Floor
        else
        {
            var a = Probe(raycast, -ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
            var b = Probe(raycast, +ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
            var sensor = Raycast.CombineClosest(a, b);

            this.velocity = Vector2.zero;

            if (sensor.tiles.Length > 0)
                this.velocity = sensor.tiles[0].tileGrid.effectiveVelocity;

            if (GameInput.Player(this).releasedJump == true)
            {
                this.state = State.Normal;
                this.stompAttackTimer = 0;
                this.isDropping = false;
            }
        }

        this.position += this.velocity;
    }

    public void HandleKingHurt()
    {
        // do nothing
    }

    public void StompDropLand()
    {
        this.isDropping = false;
        this.animated.PlayAndHoldLastFrame(this.animationStompDropLand);

        // Screenshake
        float screenShakeForce = Mathf.Lerp(0.5f, 1.5f,
            Mathf.InverseLerp(-.5f, -1.5f, this.velocity.y));
        this.map.ScreenShakeAtPosition(this.position, Vector2.down * 0.75f);

        // Hit-stun pause
        StartCoroutine(_StoneAttackTimeFrozen());

        // Smack effect particle
        var p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyHitPowAnimation,
            this.position.Add(0f, -0.50f),
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "Player (BL)";
        p.spriteRenderer.sortingOrder = -1;
        p.scale = .05f;

        // Dust particles
        float dustHorizontal = 2f;
        float dustVertical = -0.90f;

        var dustLeft = Particle.CreateAndPlayOnce(
            this.animationStompDropDust,
            this.position.Add(-dustHorizontal, dustVertical),
            this.transform.parent
        );
        dustLeft.spriteRenderer.sortingLayerName = "Player (BL)";
        dustLeft.spriteRenderer.sortingOrder = -1;
        dustLeft.transform.localScale = new Vector3(-1f, 1f, 1f);

        var dustRight = Particle.CreateAndPlayOnce(
            this.animationStompDropDust,
            this.position.Add(dustHorizontal, dustVertical),
            this.transform.parent
        );
        dustRight.spriteRenderer.sortingLayerName = "Player (BL)";
        dustRight.spriteRenderer.sortingOrder = -1;

        Audio.instance.PlaySfx(Assets.instance.sfxKingGroundslamLand);

        List<Item> affectedItems = new List<Item>();
        IEnumerable<Chunk> chunks = this.map.ChunksWithinDistance(this.position, this.stompLongRange);

        foreach (var c in chunks)
        {
            foreach (var item in c.items)
            {
                if (item.gameObject.activeSelf == false) continue;

                if (item is Enemy)
                {
                    if (Vector2.Distance(item.position, this.position) < this.stompLongRange)
                    {
                        affectedItems.Add(item);
                    }
                }

                if ((item is Chest) == true ||
                    (item is ChestMimic) == true ||
                    (item is BreakableBlock) == true ||
                    (item is RandomBox) == true)
                {
                    if (Vector2.Distance(item.position, this.position) < this.stompShortRange)
                    {
                        affectedItems.Add(item);
                    }
                }

            }

            // apply attack effect on items
            foreach (var item in affectedItems)
            {
                switch (this.stompPower)
                {
                    case StompPower.AffectEnemies:
                        {
                            if (item is Enemy)
                            {
                                (item as Enemy).Kill(new Enemy.KillInfo
                                {
                                    isBecauseLevelIsResetting = false
                                });
                            }
                        }
                        break;

                    case StompPower.AffectEnemiesAndItems:
                        {
                            if (item is Enemy)
                            {
                                (item as Enemy).Kill(new Enemy.KillInfo
                                {
                                    isBecauseLevelIsResetting = false
                                });
                            }
                            else if (item is Chest)
                            {
                                if((item as Chest).isLocked == false && (item as Chest).isConnectedToLightBlock == false)
                                {
                                    (item as Chest).Open();
                                }
                            }
                            else if (item is ChestMimic)
                            {
                                (item as ChestMimic).Open();
                            }
                            else if (item is BreakableBlock)
                            {
                                (item as BreakableBlock).Break(true);
                            }
                            else if (item is RandomBox)
                            {
                                (item as RandomBox).Break();
                            }
                        }
                        break;
                }
            }
        }
    }

    private IEnumerator _StoneAttackTimeFrozen()
    {
        this.map.timeFrozenByEnemyDeath = true;
        yield return new WaitForSecondsRealtime(0.10f);
        this.map.timeFrozenByEnemyDeath = false;
    }

    public void UpdateStompDropSprite()
    {
        if (this.isDropping == true)
        {
            this.animated.PlayAndHoldLastFrame(this.animationStompDropPrep);
        }
    }

    public void UpdatePoleSlidingTwistSprite(int frame)
    {
        this.hatAnimated.Stop();
        this.hatSpriteRenderer.sprite =
            this.animationHatPoleSlidingTwist.sprites[frame];
    }

    private void InitializeHatAnimationsDictionary()
    {
        this.hatAnimationsDictionary = new Dictionary<Animated.Animation, Animated.Animation>()
        {
            { this.animationStanding, this.animationHatStanding },
            { this.animationRunning, this.animationHatRunning },
            { this.animationRunningPanicked, this.animationHatRunningPanicked },
            { this.animationRunningTooFast, this.animationHatRunningTooFast },
            { this.animationJumping, this.animationHatJumping },
            { this.animationJumpingDown, this.animationHatJumpingDown },
            { this.animationDoubleJumping, this.animationHatDoubleJumping },
            { this.animationStompFrame, this.animationHatStompFrame },
            { this.animationAfterStomp, this.animationHatAfterStomp },
            { this.animationWallSliding, this.animationHatWallSliding },
            { this.animationIceSliding, this.animationHatIceSliding },
            { this.animationPoleSliding, this.animationHatPoleSliding },
            { this.animationSpring, this.animationHatSpring },
            { this.animationHanging, this.animationHatHanging },
            { this.animationGrabSide, this.animationHatGrabSide },
            { this.animationGrabStand, this.animationHatGrabStand },
            { this.animationSwimmingSurface, this.animationHatSwimmingSurface },
            { this.animationSwimmingCeiling, this.animationHatSwimmingCeiling },
            { this.animationHoldingBreath, this.animationHatHoldingBreath },
            { this.animationWithParachuteOpened, this.animationHatWithParachuteOpened },
            { this.animationWithParachuteClosed, this.animationHatWithParachuteClosed },
            //{ this.animationStompDropPrep, this.animationHatStompDropPrep },
            //{ this.animationStompDropLand, this.animationHatStompDropLand },
        };

        this.hatTransitionsDictionary = new Dictionary<Animated.Animation, Animated.Animation>()
        {
            { this.animationTransitionWallBounce, this.animationHatTransitionWallBounce },
            { this.animationTransitionWallBouncePanicked, this.animationHatTransitionWallBouncePanicked },
            { this.animationTransitionRunToWallSlide, this.animationHatTransitionRunToWallSlide },
            { this.animationTransitionWallSlideToRun, this.animationHatTransitionWallSlideToRun },
            { this.animationTransitionWallSlideToDrop, this.animationHatTransitionWallSlideToDrop },
            { this.animationTransitionJumpToJumpDown, this.animationHatTransitionJumpToJumpDown },
            { this.animationTransitionSwimToSwimUp, this.animationHatTransitionSwimToSwimUp },
        };
    }

    private Animated.Animation GetHatAnimation(Animated.Animation animation)
    {
        if (this.hatAnimationsDictionary.ContainsKey(animation) == false)
        {
            return this.animationHatBlank;
        }

        return this.hatAnimationsDictionary[animation];
    }

    private Animated.Animation GetHatTransitionAnimation(Animated.Animation animation)
    {
        if (this.hatTransitionsDictionary.ContainsKey(animation) == false)
        {
            return animation;
        }

        return this.hatTransitionsDictionary[animation];
    }

    public void UpdateHatSprite()
    {
        if (this.hatAnimationsDictionary == null)
        {
            InitializeHatAnimationsDictionary();
        }

        if (this.canCreateFirstHat == true && this.kingHat == null)
        {
            this.canCreateFirstHat = false;
            CreateKingHat();
        }

        if (this.respawnSpin == true)
        {
            float targetAngleDegrees = -360f;

            this.respawnAngleDegrees = Util.Slide(this.respawnAngleDegrees, targetAngleDegrees, 25);
            this.transform.rotation = Quaternion.Euler(0, 0, this.respawnAngleDegrees);

            if (this.respawnAngleDegrees == targetAngleDegrees)
            {
                this.respawnSpin = false;
                RespawnAnimationEnd();
            }
        }

        this.hatSpriteRenderer.enabled = this.isHatActive;

        if (this.isHatActive == false) return;

        this.hatSpriteRenderer.sortingLayerName = this.spriteRenderer.sortingLayerName;
        this.hatSpriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;


        if(this.state == State.InsideMineCannon)
        {
            this.hatAnimated.Stop();
            this.hatSpriteRenderer.sprite = null;
        }
        else if (this.animated.currentAnimation == this.animationStompDropLand)
        {
            Animated.Animation hatAnimation = this.isHatActive ?
                this.animationHatStompDropLand : this.animationHatBlank;
            this.hatAnimated.PlayAndLoop(hatAnimation);

            this.hatSpriteRenderer.sortingLayerName = "Player (Front)";
            this.hatSpriteRenderer.sortingOrder = 1;
        }
        else if (this.transitionAnimation != null)
        {
            Animated.Animation bodyTransitionAnimation = this.transitionAnimation;
            Animated.Animation hatTransitionAnimation =
                GetHatTransitionAnimation(this.transitionAnimation);
            PlayHatTransitionAnimation(hatTransitionAnimation);
        }
        else if (this.animated.currentAnimation != null)
        {
            Animated.Animation bodyAnimation = this.animated.currentAnimation;
            Animated.Animation hatAnimation = GetHatAnimation(bodyAnimation);
            this.hatAnimated.PlayAndLoop(hatAnimation);
        }
    }

    private void DeactivateCurrentKingHat()
    {
        if(this.kingHat == null) return;

        this.kingHat.Deactivate();
        this.kingHat = null;
    }

    private void CreateKingHat()
    {
        var playerChunk = this.map.NearestChunkTo(this.position);

        {
            var obj = Instantiate(this.kingHatPrefab, playerChunk.transform);
            obj.name = "King Hat";
            this.kingHat = obj.GetComponent<KingHat>();
            this.kingHat.Init(position, playerChunk);
            playerChunk.items.Add(this.kingHat);
        }

        this.kingHat.gameObject.SetActive(false);
    }

    private void PlayHatTransitionAnimation(Animated.Animation animation)
    {
        if (this.hatAnimated.currentAnimation != animation)
        {
            this.hatAnimated.PlayOnce(animation, delegate
            {
                this.UpdateHatSprite();
            });
        }
    }

    private void CheckForHatSpawnOnStompDrop()
    {
        if(this.hatRespawnMode != HatRespawnMode.OnStompAttack) return;
        if (this.isHatActive == true) return;

        this.spawnHatOnStomp = true;
        this.spawnHatStompPosition = this.position;
        this.spawnHatStompTimer = 0;
    }

    public void LoseHatOnStompDrop()
    {
        if (this.isHatActive == false) return;

        this.isHatActive = false;
        this.kingHat.ThrowOnStompDrop();
    }

    public void LoseHatOnCollision()
    {
        if (this.isHatActive == false) return;
        if (this.state == State.Finished) return;

        this.isHatActive = false;
        this.kingHat.Throw(this.velocity);
        Audio.instance.PlaySfx(Assets.instance.sfxKingHatLose);
    }

    public void HitWithHatOn()
    {
        this.isHatActive = false;
        Vector2 hitVelocity = new Vector2(-this.velocity.x, 0.15f);
        this.kingHat.Throw(hitVelocity);
        this.invulnerableTimer = InvulnerableAfterHitTime;
        this.state = State.KingHurt;

        this.animated.PlayOnce(this.animationKingHurt, () =>
        {
            this.state = State.Normal;
        });

        Particle p = Particle.CreateAndPlayOnce(
            this.animationHurtEffect,
            this.position,
            this.transform.parent
        );
        p.spriteRenderer.sortingLayerID = this.spriteRenderer.sortingLayerID;
        p.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;
    }

    public void RecoverHat()
    {
        if (this.isHatActive == true) return;

        this.isHatActive = true;
    }

    public override void Die()
    {
        base.Die();

        this.stompHoldTimer = 0;
        this.stompAttackTimer = 0;
        this.isDropping = false;
    }

    protected override void DeathFX()
    {
        if (this.isHatActive == true)
        {
            Particle p = Particle.CreateWithSprite(
                this.hatDeathSprite, 60 * 2, HeadPosition, this.transform.parent
            );
            p.Throw(new Vector2(0f, 0.4f), 0.15f, 0.1f, new Vector2(0, -0.025f));

            p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
            if (UnityEngine.Random.Range(0, 2) == 0)
            {
                p.spin *= -1;
            }

            p.transform.localScale = new Vector3(this.transform.localScale.x, 1f, 1f);
        }
        Audio.instance.PlaySfx(Assets.instance.sfxKingDeath);
    }

    protected override void RespawnFX()
    {
        this.spriteRenderer.enabled = true;
        this.position = this.GetCheckpointPosition().Add(0f, 1.4125f);

        PlayerDeathParticles.Life(
            this,
            Color.white,
            this.position,
            RespawnAnimationStart
        );

        Audio.instance.PlaySfx(Assets.instance.sfxKingRespawn);
    }

    private void RespawnAnimationStart()
    {
        this.animated.PlayOnce(this.animationRespawn, delegate
        {
            this.respawnAngleDegrees = 0f;
            this.respawnSpin = true;
        });
    }

    private void RespawnAnimationEnd()
    {
        this.animated.PlayOnce(this.animationKingRespawn, delegate
        {
            DeactivateCurrentKingHat();
            this.state = State.Normal;
            this.isHatActive = true;
            CreateKingHat();
        });

        Particle p = Particle.CreateAndPlayOnce(
            this.particleKingRespawn,
            this.position,
            this.transform.parent
        );

        p.spriteRenderer.sortingLayerName = "Player (BL)";
        p.spriteRenderer.sortingOrder = 0;
    }

    public bool IsInvulnerable()
    {
        return this.invulnerableTimer > 0;
    }

    public bool CanWallSlide()
    {
        if(IsPreparingStomp == true && GameInput.Player(this).holdingJump)
        {
            return false;
        }

        if(this.state == State.KingStompDrop)
        {
            return false;
        }

        return true;
    }

    public bool CanRecoverHat()
    {
        if(this.state == State.Geyser) return false;
        if(this.state == State.Respawning) return false;
        if(this.state == State.InsidePipe) return false;
        if(this.state == State.EnteringLocker) return false;
        if(this.state == State.InsideBox) return false;
        if(this.state == State.InsideSpaceBooster) return false;
        if(this.state == State.InsideCityCannon) return false;
        if(this.state == State.InsideLift) return false;
        if(this.state == State.InsideSpaceBooster) return false;
        if(this.state == State.InsideSpaceBooster) return false;

        return true;
    }

}
