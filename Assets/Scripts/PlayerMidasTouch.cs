using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerMidasTouch
{
    [NonSerialized] public bool isActive = false;
    private bool wasActive = false;

    public GameObject midasTouchPrefab;

    private Player player;

    private Particle uiParticle;
    [NonSerialized] public PlayerMidasTouchEffect playerMidasTouchEffect;

    [NonSerialized] public int hearts = 0;
    [NonSerialized] public bool needsHeart = false;

    public void Init(Player player)
    {
        this.player = player;
        this.hearts = 0;
    }

    public void Advance()
    {
        if (this.playerMidasTouchEffect != null)
        {
            this.playerMidasTouchEffect.Advance();
        }

        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool activatedByBubble = false)
    {
        if (this.isActive == true)
            return;

        this.isActive = true;
        this.wasActive = true;

        if (hearts > 0)
        {
            this.hearts--;
        }

        if (this.hearts < 3)
        {
            this.needsHeart = true;
        }
        this.player.spriteRenderer.material = Assets.instance.spriteGoldTint;
        this.player.backArmSpriteRenderer.material = Assets.instance.spriteGoldTint;
        this.player.frontArmSpriteRenderer.material = Assets.instance.spriteGoldTint;

        if (this.player is PlayerPuffer)
        {
            (this.player as PlayerPuffer).headSpriteRenderer.material = Assets.instance.spriteGoldTint;
        }
        else if(this.player is PlayerKing)
        {
            (this.player as PlayerKing).hatAnimated.GetComponent<SpriteRenderer>().material = Assets.instance.spriteGoldTint;
        }

        if(this.player.infiniteJump.isActive == true)
        {
            this.player.infiniteJump.wings.spriteRenderer.color = new Color(244, 214, 57);
        }

        //this.player.spriteRenderer.color = new Color(225, 215, 0);
        if (this.playerMidasTouchEffect == null)
        {
            this.playerMidasTouchEffect = GameObject.Instantiate(
                this.midasTouchPrefab,
                this.player.transform.position,
                this.player.transform.rotation
            ).GetComponent<PlayerMidasTouchEffect>();

            this.playerMidasTouchEffect.Init(this.player.map);
        }

        if (activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),
                this.player.map.transform
            );

            this.uiParticle.transform.localScale = Vector3.one * 0.35f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }

        Audio.instance.PlaySfx(Assets.instance.sfxPowerupMidasStart);
    }

    public void End()
    {
        if(this.isActive == true)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxPowerupMidasEnd);
            this.isActive = false;

            this.player.spriteRenderer.material = Assets.instance.spritePlayerInvincibility;
            this.player.backArmSpriteRenderer.material = Assets.instance.spritePlayerInvincibility;
            this.player.frontArmSpriteRenderer.material = Assets.instance.spritePlayerInvincibility;
            if (this.player is PlayerPuffer)
            {
                (this.player as PlayerPuffer).headSpriteRenderer.material = Assets.instance.spritePlayerInvincibility;
            }
            else if (this.player is PlayerKing)
            {
                (this.player as PlayerKing).hatAnimated.GetComponent<SpriteRenderer>().material = Assets.instance.spritesDefaultMaterial;
            }

            this.isActive = false;
            this.wasActive = true;
        }
        else
        {
            this.wasActive = false;
        }

        if (this.playerMidasTouchEffect != null)
        {
            this.playerMidasTouchEffect.Stop();
            this.playerMidasTouchEffect = null;
        }
    }


    public void Respawn()
    {
        if (this.wasActive == true && this.hearts > 0)
        {
            Activate();
            //wings.LoseHeart();
        }
        else
        {
            this.wasActive = false;
            this.needsHeart = false;
        }
    }

    public void AddHearts()
    {
        if (this.hearts < 3)
        {
            this.hearts++;
            //wings.GainHeart();
            if (this.hearts == 3)
            {
                this.needsHeart = false;
            }
        }
    }

    public IEnumerator DelayedActivation()
    {
        yield return 0;
        Activate();
    }
}


