using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMidasTouchEffect : MonoBehaviour
{
    private Map map;

    public Animated.Animation animationInsideSpark;

    // Hearts
    public Animated.Animation heartSolidAnimation;
    public Animated.Animation heartDisappearAnimation;
    public Animated.Animation heartAppearAnimation;
    private Transform heartsParent;
    private Transform heart1;
    private Transform heart3;
    private Transform heart2;

    [NonSerialized] public bool hidden = false;

    public void Init(Map map)
    {
        this.map = map;

        heartsParent = this.transform.Find("Hearts").transform;
        heart1 = this.transform.Find("Hearts/1").transform;
        heart2 = this.transform.Find("Hearts/2").transform;
        heart3 = this.transform.Find("Hearts/3").transform;
        heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);

        if (this.map.player.infiniteJump.hearts == 3)
        {

        }
        else if (this.map.player.infiniteJump.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
        }
        else if (this.map.player.infiniteJump.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
        }
        else
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(false);
        }

        heartsParent.gameObject.SetActive(false);
    }

    public void Advance()
    {
        if (this.hidden == true)
            return;

        this.transform.position = GetTargetPosition();   

        if (this.map.frameNumber % 5 == 0)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                Assets.instance.playerInvincibleSparkleAnimation,
                (Vector2)this.transform.position + UnityEngine.Random.insideUnitCircle * 1.6f,
                this.map.player.transform.parent
            );
            sparkle.spriteRenderer.color = new Color(243, 203, 0);
        }

        if (this.map.frameNumber % 6 == 0)
        {
            var spark = Particle.CreateAndPlayOnce(
                this.animationInsideSpark,
                (Vector2)this.transform.position + UnityEngine.Random.insideUnitCircle * 1.6f,
                this.map.player.transform.parent
            );
            spark.gameObject.name = "Shield Effect Spark Particle";
            spark.spriteRenderer.sortingLayerName = "Player (AL)";
            spark.spriteRenderer.sortingOrder = -1;
            spark.spriteRenderer.color = new Color(243, 203, 0);
        }

        this.heartsParent.transform.position = this.map.player.position + new Vector2(0, 2.1f);
    }

    public void Stop()
    {
        this.gameObject.SetActive(false);
    }

    private Vector2 GetTargetPosition()
    {
        return this.map.player.transform.position + (Vector3.up * .05f);
    }

    public void LoseHeart()
    {
        StartCoroutine(_LoseHeart());
    }

    public IEnumerator _LoseHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.map.player.midasTouch.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.map.player.midasTouch.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.map.player.midasTouch.hearts == 0)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.5f);

        Vector3 originalPosition;
        if (this.map.player.midasTouch.hearts == 2)
        {
            heart2.gameObject.SetActive(true);
            heart1.gameObject.SetActive(true);
            originalPosition = heart3.transform.localPosition;
            heart3.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart3.transform.localPosition = originalPosition;
                heart3.gameObject.SetActive(false);
            });
        }
        else if (this.map.player.midasTouch.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            originalPosition = heart2.transform.localPosition;
            heart2.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart2.transform.localPosition = originalPosition;
                heart2.gameObject.SetActive(false);
            });
        }
        else if (this.map.player.midasTouch.hearts == 0)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            originalPosition = heart1.transform.localPosition;
            heart1.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart1.transform.localPosition = originalPosition;
                heart1.gameObject.SetActive(false);
            });
        }
        float timer = 0;
        while (timer < .5f)
        {
            timer += Time.deltaTime;
            if (this.map.player.midasTouch.hearts == 2)
            {
                heart3.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.map.player.midasTouch.hearts == 1)
            {
                heart2.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.map.player.midasTouch.hearts == 0)
            {
                heart1.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            yield return null;
        }

        yield return new WaitForSeconds(.5f);

        heartsParent.gameObject.SetActive(false);
    }

    public void GainHeart()
    {
        StartCoroutine(_GainHeart());
    }

    public IEnumerator _GainHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.map.player.midasTouch.hearts == 3)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.map.player.midasTouch.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.map.player.midasTouch.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.25f);

        if (this.map.player.midasTouch.hearts == 3)
        {
            heart3.gameObject.SetActive(true);
            heart3.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.map.player.midasTouch.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.map.player.midasTouch.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
        }

        yield return new WaitForSeconds(1f);
        heartsParent.gameObject.SetActive(false);
    }

    public void Hide()
    {
        this.hidden = true;
        heartsParent.gameObject.SetActive(false);
    }

    public void Show()
    {
        this.hidden = false;
    }
}
