using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerMultiplayerInk
{

    [NonSerialized] public bool isActive = false;

    private Player player;
    private Particle uiParticle;

    public void Init(Player player)
    {
        this.player = player;
    }

    public void Advance()
    {
        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool activatedByBubble = false)
    {
        //if(Multiplayer.IsMultiplayerGame() == false) return;
        isActive = true;

        Vector2 position = this.player.position.Add(0, 2f);
        Chunk chunk = this.player.map.NearestChunkTo(this.player.position);

        if(Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.SplitScreen)
        {
            PetPainter.Create(position, chunk);
        }
        else //if(Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.GameKit)
        {
            PetPainter.CreateP1(position, chunk);
        }

        if (activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),    
                this.player.map.transform
            );

            this.uiParticle.transform.localScale = Vector3.one * 0.65f;
            this.uiParticle.spin = 2.50f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }
    }

}

