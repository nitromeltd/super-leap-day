using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerMultiplayerMines
{
    [NonSerialized] public bool isActive = false;
    [NonSerialized] public int numberOfMines;

    private Player player;
    private Particle uiParticle;
    private float timer = 0;
    private int mines = 0;

    private const float Delay = 1.50f;

    public void Init(Player player)
    {
        this.player = player;
        this.numberOfMines = 0;
    }

    public void Advance()
    {
        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }

        if(this.isActive)
        {
            this.timer += Time.deltaTime;

            if(this.timer > Delay && this.mines < this.numberOfMines)
            {
                this.mines++;
                GenerateMines();
                this.timer = 0;

                if(this.mines == this.numberOfMines)
                {
                    this.isActive = false;
                }
            }
        }
    }

    public void Activate(bool activatedByBubble = false, int numberOfMines = 5)
    {
        //if(Multiplayer.IsMultiplayerGame() == false) return;

        this.isActive = true;
        this.mines = 0;
        this.timer = Delay;
        this.numberOfMines += numberOfMines;

        if (activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),
                this.player.map.transform
            );

            this.uiParticle.transform.localScale = Vector3.one * 0.65f;
            this.uiParticle.spin = 2.50f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }
    }

    private void GenerateMines()
    {
        Vector2 position = this.player.position;
        Chunk chunk = this.player.map.NearestChunkTo(this.player.position);
        
        PetMine.Create(position, chunk);
        
        if(Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.GameKit)
        {
            GameCenterMultiplayer.SendMultiplayerPowerup(
                PowerupPickup.Type.MultiplayerMines, position, chunk
            );
        }
    }

    public void End()
    {
        this.isActive = false;
    }
}

