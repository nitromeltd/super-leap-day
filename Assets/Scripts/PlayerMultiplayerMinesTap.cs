using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerMultiplayerMinesTap
{
    [NonSerialized] public bool isActive = false;

    private Player player;
    private Particle uiParticle;
    private int mines = 0;

    public int MinesAmount => this.mines;
    public bool HasMinesLeft => this.mines > 0;

    public void Init(Player player, int numberOfMines)
    {
        this.player = player;
        this.mines = 0;
    }

    public void Advance()
    {
        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = this.player.position;

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Collect(int numberOfMines)
    {
        this.mines = numberOfMines;
    }

    public void Activate()
    {
        //if(Multiplayer.IsMultiplayerGame() == false) return;

        this.isActive = true;

        GenerateMines();
        this.mines -= 1;

        if(this.mines <= 0)
        {
            this.mines = 0;
            End();
        }
    }

    private void GenerateMines()
    {
        Vector2 position = this.player.position;
        Chunk chunk = this.player.map.NearestChunkTo(this.player.position);
        
        PetMine.Create(position, chunk);
        
        if(Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.GameKit)
        {
            GameCenterMultiplayer.SendMultiplayerPowerup(
                PowerupPickup.Type.MultiplayerMines, position, chunk
            );
        }
    }

    public void End()
    {
        this.isActive = false;
    }
}
