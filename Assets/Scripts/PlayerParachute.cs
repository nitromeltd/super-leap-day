using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerParachute
{
    public GameObject parachuteVisualsPrefab;
    
    [HideInInspector] public PlayerParachuteVisuals visuals;
    [HideInInspector] public Player player;
    private Parachute parachuteItem = null;
    private int parachuteToggleFrame = 0;
    [HideInInspector] public bool opening = false;
    [HideInInspector] public bool opened = false;
    private int enterFrame = 0;
    
    public static readonly float ParachuteSpeed = 0.09f;
    
    public bool IsAvailable => this.parachuteItem != null;
    public bool IsVisible => this.opening == true || this.opened == true;
    public bool ShouldZoomOut => IsAvailable == true && this.parachuteItem.zoomOut == true;
    public int ActiveTime => IsAvailable ? Map.instance.frameNumber - enterFrame : -1;

    public void Init(Player player)
    {
        if(player.map.theme != Theme.WindySkies) return;

        this.player = player;
        
        this.visuals = GameObject.Instantiate(
            this.parachuteVisualsPrefab,
            this.player.transform.position,
            this.player.transform.rotation
        ).GetComponent<PlayerParachuteVisuals>();

        this.visuals.transform.SetParent(this.player.transform);
        this.visuals.Init(this);
    }

    public void Enter(Parachute parachuteItem)
    {
        this.parachuteItem = parachuteItem;
        this.visuals.EnterBackpackMode();
        this.enterFrame = Map.instance.frameNumber;
    }

    public void Exit()
    {
        if(IsAvailable == false) return;

        this.parachuteItem = null;
        this.visuals.ExitBackpackMode();
        this.enterFrame = 0;

        Close();
    }

    public void Toggle()
    {
        if(IsAvailable == false) return;
        
        if(this.player.map.frameNumber - this.parachuteToggleFrame < 5) return;
        this.parachuteToggleFrame = Map.instance.frameNumber;

        if(this.player.state == Player.State.UsingParachuteOpened)
        {
            this.player.state = Player.State.UsingParachuteClosed;
            this.player.velocity.y = 0f;

            Close();
        }
        else
        {
            this.player.state = Player.State.UsingParachuteOpened;
            this.player.velocity.y = Mathf.Clamp(this.player.velocity.y, 0.20f, Mathf.Infinity);
            this.player.velocity.x = PlayerParachute.ParachuteSpeed * (this.player.facingRight ? 1f : -1f);
            this.visuals.OpenParachute();
            this.opening = true;
        }
    }

    public void Opened()
    {
        if(IsAvailable == false) return;

        this.opening = false;
        this.opened = true;

        this.player.velocity.y *= 0.25f;
    }

    public void Close()
    {
        if(IsAvailable == false) return;
        
        this.opening = false;
        this.opened = false;

        this.visuals.CloseParachute();
    }

    public void ShowAll()
    {
        this.visuals.gameObject.SetActive(true);
    }

    public void HideAll()
    {
        this.visuals.gameObject.SetActive(false);
    }
}
