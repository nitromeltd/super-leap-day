using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParachuteVisuals : MonoBehaviour
{
    [Header("Backpack")]
    public SpriteRenderer backpackSpriteRenderer;
    public Sprite backpackSprite;
    public Animated.Animation animationSpinningBackpack;

    [Header("Parachute")]
    public Animated parachuteAnimated;
    public Transform parachuteTransform;
    public Animated.Animation animationEmptyParachute;
    public Animated.Animation animationOpenParachute;
    public Animated.Animation animationCloseParachute;
    public Animated.Animation animationIdleParachute;
    public Animated.Animation animationHideParachute;
    public Sprite spriteHideParachute;
    public AnimationCurve hideParachuteFadeCurve;
    public Transform targetParachutePoint;

    [Header("Ropes")]
    public FrameGroup[] allFrameGroups;
    public Transform ropeParentTransform;
    public GameObject[] ropeGOs;

    private List<Rope> ropes = new List<Rope>();
    private PlayerParachute parachute;
    private Animated.Animation lastFramePlayerAnimation;

    public FrameGroup CurrentFrameGroup => this.allFrameGroups[CurrentFrameNumber];
    public int CurrentFrameNumber =>
        (this.parachuteAnimated.currentAnimation == this.animationIdleParachute) ?
        this.parachuteAnimated.frameNumber : 0;

    private bool[] BackpackInFront = new bool[]
    {
        false,
        false,
        false,
        false,
        false,
        true,
        true,
        false,
        false,
    };

    [System.Serializable] public class FrameGroup
    {
        public Transform[] points;
    }

    [System.Serializable] public class Rope
    {
        private MeshFilter meshFilter;
        private MeshRenderer meshRenderer;
        public Transform initialPoint;
        public Vector3 targetEndPosition;
        public PlayerParachuteVisuals visuals;
        public int index;

        public Vector3[] vertices;
        private Vector3[] normals;
        private Vector2[] uvs;

        private const int Length = 4;

        private const int SizeX = 1;
        private const int SizeY = 1;

        public Transform CurrentEndPoint => this.visuals.CurrentFrameGroup.points[this.index];

        public Rope(GameObject ropeGameObject, Transform initialPoint, PlayerParachuteVisuals visuals, int index)
        {
            this.meshFilter = ropeGameObject.GetComponent<MeshFilter>();
            this.meshRenderer = ropeGameObject.GetComponent<MeshRenderer>();
            this.initialPoint = initialPoint;
            this.index = index;
            this.targetEndPosition = this.initialPoint.position;
            this.visuals = visuals;

            this.meshRenderer.sortingLayerName = "Player (BL)";
            this.meshRenderer.sortingOrder = -2;

            // init
            this.vertices = new Vector3[Length];
            this.normals = new Vector3[Length];
            this.uvs = new Vector2[Length];

            int uv1 = 0;
            int uv2 = 1;
            for(int i = 0; i < SizeY + 1; i++)
            {
                float y = 1f - ((float)i / (float)SizeY);
                uvs[uv1 + i] = new Vector2(0, y);
                uvs[uv2 + i] = new Vector2(1, y);
                
                uv1++;
                uv2++;
            }
        }

        private const float Width = 0.10f;

        public void Advance(bool show, float speed)
        {
            Vector3 currentTargetEndPos = show ? CurrentEndPoint.position : this.initialPoint.position;

            this.targetEndPosition.x = Util.Slide(this.targetEndPosition.x, currentTargetEndPos.x, speed);
            this.targetEndPosition.y = Util.Slide(this.targetEndPosition.y, currentTargetEndPos.y, speed);

            if(this.visuals.parachute.player.alive == false || this.visuals.parachute.player.state == Player.State.Respawning)
            {
                this.targetEndPosition = this.initialPoint.position;
            }

            this.vertices = new Vector3[]
            {
                (this.targetEndPosition + (Vector3.left * (Width / 2f))) - this.initialPoint.position,
                (this.targetEndPosition + (Vector3.right * (Width / 2f))) - this.initialPoint.position,
                (this.initialPoint.position + (Vector3.left * (Width / 2f))) - this.initialPoint.position,
                (this.initialPoint.position + (Vector3.right * (Width / 2f))) - this.initialPoint.position
            };
            
            meshFilter.mesh.vertices = this.vertices;

            int[] triangles = new int[SizeX * SizeY * 6];
            for (int ti = 0, vi = 0, y = 0; y < SizeY; y++, vi++) {
                for (int x = 0; x < SizeX; x++, ti += 6, vi++) {
                    triangles[ti] = vi;
                    triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                    triangles[ti + 4] = triangles[ti + 1] = vi + SizeX + 1;
                    triangles[ti + 5] = vi + SizeX + 2;
                }
            }
            meshFilter.mesh.triangles = triangles;
        
            meshFilter.mesh.normals = this.normals;
            meshFilter.mesh.uv = this.uvs;
        }
    }

    public bool IsVisible =>
        this.parachuteAnimated.currentAnimation != this.animationEmptyParachute;

    public bool IsOpen =>
        this.parachuteAnimated.currentAnimation == this.animationOpenParachute ||
        this.parachuteAnimated.currentAnimation == this.animationIdleParachute;

    private void Start()
    {
        for (int i = 0; i < this.allFrameGroups[0].points.Length; i++)
        {
            Rope r = new Rope(
                this.ropeGOs[i], this.ropeParentTransform, this, i
            );

            this.ropes.Add(r);
        }
    }

    private void Update()
    {
        this.ropeParentTransform.gameObject.SetActive(this.parachute.IsAvailable && this.parachute.IsVisible);

        bool canShowRopes =
            (this.parachuteAnimated.currentAnimation == this.animationOpenParachute &&
            this.parachuteAnimated.frameNumber > 3) ||
            (this.parachuteAnimated.currentAnimation == this.animationCloseParachute &&
            this.parachuteAnimated.frameNumber < 5) ||
            this.parachuteAnimated.currentAnimation == this.animationIdleParachute;

        foreach(var r in this.ropes)
        {
            bool ropeShouldAdvanceFast =
                this.parachute.IsAvailable == true &&
                this.parachute.IsVisible == true;
            float speed = ropeShouldAdvanceFast == true ? 0.75f : 2f;

            r.Advance(canShowRopes, speed);
        }

        Vector3 parachutePosition = this.parachuteTransform.position;
        Vector3 targetParachutePosition = this.parachute.opening == true ?
            this.targetParachutePoint.position + Vector3.up * 1f : this.targetParachutePoint.position;

        parachutePosition.x = Util.Slide(parachutePosition.x, targetParachutePosition.x, 0.1f);
        parachutePosition.y = Util.Slide(parachutePosition.y, targetParachutePosition.y, 0.1f);

        this.parachuteTransform.position = parachutePosition;

        float ropeScaleX = this.parachute.player.transform.localScale.x * -1f;
        this.ropeParentTransform.localScale = new Vector3(ropeScaleX, 1f, 1f);

        bool ShouldBeFlipped()
        {
            if(this.parachute.player.state == Player.State.WallSlide)
            {
                return true;
            }

            return false;
        }

        if(ShouldBeFlipped())
        {
            this.backpackSpriteRenderer.transform.localPosition =
                new Vector3(BackpackSpritePos.x, BackpackSpritePos.y);
            this.backpackSpriteRenderer.transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            this.backpackSpriteRenderer.transform.localPosition =
                new Vector3(-BackpackSpritePos.x, BackpackSpritePos.y);
            this.backpackSpriteRenderer.transform.localScale = new Vector3(1f, 1f, 1f);
        }

        if(this.parachute.player.animated.currentAnimation ==
            this.parachute.player.animationSpring && this.parachute.IsAvailable)
        {
            int index = this.parachute.player.animated.frameNumber;

            this.backpackSpriteRenderer.sprite =
                this.animationSpinningBackpack.sprites[index];

            this.backpackSpriteRenderer.sortingLayerName =
                BackpackInFront[index] ? "Items (Front)" : "Player (BL)";
        }

        if(this.lastFramePlayerAnimation != this.parachute.player.animated.currentAnimation &&
            this.lastFramePlayerAnimation == this.parachute.player.animationSpring)
        {
            this.backpackSpriteRenderer.sprite = this.parachute.IsAvailable ?
                this.backpackSprite : Assets.instance.emptySprite;
            this.backpackSpriteRenderer.sortingLayerName = "Player (BL)";
        }

        this.lastFramePlayerAnimation =
            this.parachute.player.animated.currentAnimation;
    }

    private static Vector3 BackpackSpritePos = new Vector3(0.439f, 0.083f);

    public void Init(PlayerParachute parachute)
    {
        this.parachute = parachute;
    }

    public void EnterBackpackMode()
    {
        this.backpackSpriteRenderer.sprite = this.backpackSprite;
        HideParachute();
        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyParachuteUnequip,
            options: new Audio.Options(1, false, 0)
        );
    }

    public void ExitBackpackMode()
    {
        bool isOpen = IsOpen;

        this.backpackSpriteRenderer.sprite = Assets.instance.emptySprite;
        HideParachute();
        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyParachuteEquip,
            options: new Audio.Options(1, false, 0)
        );
        
        // throw backpack particle
        var p = Particle.CreateWithSprite(
            this.backpackSprite,
            100,
            this.backpackSpriteRenderer.transform.position,
            this.parachute.player.transform.parent
        );

        float xValue = this.parachute.player.facingRight ? 1f : -1f;
        p.transform.localScale = new Vector3(xValue, 1f, 1f);
        p.Throw(new Vector2(0.1f * -xValue, 0.2f), 0f, 0.1f, new Vector2(0, -0.025f));

        p.spin = UnityEngine.Random.Range(2.0f, 3.0f);
        p.spin *= xValue;

        if(isOpen == true)
        {
            // hide parachute particle
            Vector2 parachutePos =
                this.parachuteAnimated.transform.position + new Vector3(0f, 0.12f);

            var parachuteParticle1 = Particle.CreateAndPlayOnce(
                this.animationHideParachute,
                parachutePos,
                this.parachute.player.transform.parent
            );
            
            parachuteParticle1.transform.localScale = new Vector3(xValue, 1f, 1f);

            parachuteParticle1.animated.onComplete  = delegate
                {
                    parachuteParticle1.StopAndReturnToPool();

                    var parachuteParticle2 = Particle.CreateWithSprite(
                        this.spriteHideParachute,
                        20,
                        parachutePos,
                        this.parachute.player.transform.parent
                    );

                    parachuteParticle2.Throw(new Vector2(0f, 0.25f), 0f, 0f, new Vector2(0, -0.02f));
                    parachuteParticle2.FadeOut(this.hideParachuteFadeCurve);
                };
        }
    }

    public void OpenParachute()
    {
        this.parachuteAnimated.PlayOnce(this.animationOpenParachute, () =>
        {
            this.parachute.Opened();

            this.parachuteTransform.position += Vector3.up * 1f;
            this.parachuteAnimated.PlayAndLoop(this.animationIdleParachute);
        });
        Audio.instance.PlaySfx(
            Assets.instance.sfxWindyParachuteOpen,
            options: new Audio.Options(1, false, 0)
        );
    }

    public void CloseParachute()
    {
        if(this.parachuteAnimated.currentAnimation == this.animationIdleParachute ||
            this.parachuteAnimated.currentAnimation == this.animationOpenParachute)
        {
            this.parachuteAnimated.PlayAndHoldLastFrame(this.animationCloseParachute);
            Audio.instance.PlaySfx(
                Assets.instance.sfxWindyParachuteClose,
                options: new Audio.Options(1, false, 0)
            );
        }
    }

    public void HideParachute()
    {
        this.parachuteAnimated.PlayAndHoldLastFrame(this.animationEmptyParachute);
    }
}
