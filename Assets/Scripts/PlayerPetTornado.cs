﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerPetTornado
{
    [NonSerialized] public bool isActive = false;
    private bool wasActive = false;

    private Player player;
    [NonSerialized] public List<PetTornado> tornadoPets = new List<PetTornado>();

    [NonSerialized] public int hearts = 0;

    [NonSerialized] public bool needsHeart = false;
    public void Init(Player player)
    {
        this.player = player;
    }

    public void Activate(bool? activatedByBubble = false, Transform bubbleTransform = null)
    {
        if (this.isActive)
        {
            if (tornadoPets.Count > 5)
            {
                return;
            }
        }

        this.isActive = true;
        this.wasActive = true;
        this.needsHeart = true;
        PetTornado petTornado;
        if (activatedByBubble == false)
        {
            petTornado = PetTornado.Create(
                IngameUI.instance.PositionForWorldFromUICameraPosition(this.player.map, IngameUI.instance.PowerUpImageRectTransform(this.player.map).position),
                this.player.map.NearestChunkTo(this.player.position)
            );
        }
        else
        {
            petTornado = PetTornado.Create(
                bubbleTransform.position,
                this.player.map.NearestChunkTo(this.player.position),
                activatedByBubble: true
            );
        }

        tornadoPets.Add(petTornado);
        Audio.instance.PlaySfx(Assets.instance.sfxPowerupTornado);
    }

    public void End()
    {
        if (this.isActive == true)
        {
            this.isActive = false;
            this.wasActive = true;
        }
        else
        {
            this.wasActive = false;
        }
    }

    public void Respawn()
    {
        if (this.wasActive == true && hearts > 0)
        {
            this.isActive = true;
            tornadoPets[0] = PetTornado.Create(
                this.player.PetTornadoFollowPosition(0),
                this.player.map.NearestChunkTo(this.player.position)
            );
            this.needsHeart = true;
            this.player.petTornado.hearts--;
            tornadoPets[0].LoseHeart();
        }
        else
        {
            this.wasActive = false;
            this.needsHeart = false;
        }
    }

    public void AddHearts()
    {
        if (this.hearts < 3)
        {
            this.hearts++;
            tornadoPets[0].GainHeart();
            if (this.hearts == 3)
            {
                this.needsHeart = false;
            }
        }
    }

    public IEnumerator DelayedActivation()
    {
        yield return 0;
        Activate();
    }
}
