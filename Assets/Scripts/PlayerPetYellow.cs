﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerPetYellow
{
    [NonSerialized] public bool isActive = false;
    private bool wasActive = false;
    private Player player;
    [NonSerialized] public List<PetYellow> yellowPets = new List<PetYellow>();

    [NonSerialized] public int hearts = 0;
    [NonSerialized] public bool needsHeart = false;

    public void Init(Player player)
    {
        this.player = player;
    }

    public void Activate(bool? activatedByBubble = false, Transform bubbleTransform = null)
    {
        if (this.isActive)
        {
            if(yellowPets.Count > 5)
            {
                return;
            }
        }

        this.isActive = true;
        this.wasActive = true;
        this.needsHeart = true;
        PetYellow petYellow;
        if (activatedByBubble == false)
        {
            petYellow = PetYellow.Create(
                IngameUI.instance.PositionForWorldFromUICameraPosition(this.player.map, IngameUI.instance.PowerUpImageRectTransform(this.player.map).position),
                this.player.map.NearestChunkTo(this.player.position)
            );
        }
        else
        {
            petYellow = PetYellow.Create(
                bubbleTransform.position,
                this.player.map.NearestChunkTo(this.player.position),
                activatedByBubble: true
            );
        }

        yellowPets.Add(petYellow);
    }

    public void End()
    {
        if(this.isActive == true)
        {
            this.isActive = false;
            this.wasActive = true;
        }
        else
        {
            this.wasActive = false;
        }
    }

    public void Respawn()
    {
        if(this.wasActive == true && this.hearts > 0)
        {
            this.isActive = true;
            PetYellow petYellow = PetYellow.Create(
                this.player.PetYellowFollowPosition(0),
                this.player.map.NearestChunkTo(this.player.position)
            );
            yellowPets.Add(petYellow);
            this.needsHeart = true;
            this.hearts--;
            petYellow.LoseHeart();
        }
        else
        {
            this.wasActive = false;
            this.needsHeart = false;
        }
    }

    public void IgnoreEnemy(Enemy enemy)
    {
        if (this.isActive == false) return;

        for (int i = yellowPets.Count; i > 0; i--)
        {
            yellowPets[i - 1].AddEnemyToIgnoreList(enemy);
        }
    }

    public void AddHearts()
    {
        if(this.hearts < 3)
        {
            this.hearts++;
            yellowPets[0].GainHeart();
            if(this.hearts == 3)
            {
                this.needsHeart = false;
            }
        }
    }

    public IEnumerator DelayedActivation()
    {
        yield return 0;
        Activate();
    }
}
