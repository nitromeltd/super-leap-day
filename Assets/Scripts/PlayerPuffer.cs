using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPuffer : Player
{
    [Header("Character: Puffer")]
    public Animated headAnimated;
    public SpriteRenderer headSpriteRenderer;
    
    [Header("Head Animations")]
    public Animated.Animation animationHeadBlank;
    //public Animated.Animation animationHeadStanding;
    public Animated.Animation animationHeadRunning;
    public Animated.Animation animationHeadRunningPanicked;
    public Animated.Animation animationHeadRunningTooFast;
    public Animated.Animation animationHeadJumping;
    public Animated.Animation animationHeadJumpingDown;
    public Animated.Animation animationHeadStompFrame;
    public Animated.Animation animationHeadAfterStomp;
    public Animated.Animation animationHeadWallSliding;
    public Animated.Animation animationHeadIceSliding;
    public Animated.Animation animationHeadPoleSliding;
    public Animated.Animation animationHeadPoleSlidingTwist;
    public Animated.Animation animationHeadSpring;
    public Animated.Animation animationHeadHanging;
    public Animated.Animation animationHeadGrabSide;
    public Animated.Animation animationHeadGrabStand;
    public Animated.Animation animationHeadSwimmingCeiling;

    public Animated.Animation animationHeadTransitionWallBounce;
    public Animated.Animation animationHeadTransitionWallBouncePanicked;
    public Animated.Animation animationHeadTransitionRunToWallSlide;
    public Animated.Animation animationHeadTransitionWallSlideToRun;
    public Animated.Animation animationHeadTransitionWallSlideToDrop;
    public Animated.Animation animationHeadTransitionJumpToJumpDown;
    public Animated.Animation animationHeadTransitionSwimToSwimUp;
    
    [Header("Head Animations - Inflate 1")]
    public Animated.Animation animationBodyInflate1JumpUp;
    public Animated.Animation animationBodyInflate1JumpDown;
    public Animated.Animation animationBodyInflate1JumpHold;
    public Animated.Animation animationHeadInflate1;
    
    [Header("Head Animations - Inflate 2")]
    public Animated.Animation animationBodyInflate2JumpUp;
    public Animated.Animation animationBodyInflate2JumpDown;
    public Animated.Animation animationBodyInflate2JumpHold;
    public Animated.Animation animationHeadInflate2;
    
    [Header("Head Animations - Inflate 3")]
    public Animated.Animation animationBodyInflate3JumpUp;
    public Animated.Animation animationBodyInflate3JumpDown;
    public Animated.Animation animationBodyInflate3JumpHold;
    public Animated.Animation animationHeadInflate3;
    
    [Header("Head Animations - Inflate 4")]
    public Animated.Animation animationBodyInflate4JumpUp;
    public Animated.Animation animationBodyInflate4JumpDown;
    public Animated.Animation animationBodyInflate4JumpHold;
    public Animated.Animation animationHeadInflate4;
    
    [Header("Death & Respawn")]
    public Animated.Animation animationHeadDead;
    public Animated.Animation animationBodyShoes;
    public Animated.Animation animationEffectHeadPop;
    public Animated.Animation animationDeflatedHead;

    public enum GlidingMode
    {
        LoseHeight, // gradually moves downwards
        MaintainHeight, // keeps same height
        GainHeight // gradually moves upwards
    }

    private Dictionary<Animated.Animation, Animated.Animation> headAnimationsDictionary;
    private Dictionary<Animated.Animation, Animated.Animation> headTransitionsDictionary;
    public int balloonJumpsSinceOnGround = 0;
    public int currrentHeadInflateLevel = 0;
    public int balloonHeadTimer = 0;
    private GlidingMode glidingMode = GlidingMode.LoseHeight;

    private Particle deflatedHeadParticle;
    private Vector2 deflatedHeadInitialPosition;
    private Vector3 deflatedHeadVelocity;
    private float deflatedHeadTimer;
    private Vector3 deflatedHeadLastPosition;
    private int balloonJumpMax;

    private static readonly float BalloonGlidingGravityLoseHeight = -0.0005f;
    private static readonly float BalloonGlidingVelocityGainHeight = 0.01f;
    private static readonly float BalloonGlidingHSpeed = 0.075f;
    private static readonly int BallonHeadDeflateTime = 30;
    
    public override void ChangeCharacterUpgradeLevel(int upgradeLevel)
    {
        this.balloonJumpMax = upgradeLevel;
    }

    public void HandlePufferAirborneVelocity(Vector2 gravityDirectionVector)
    {
        if(IsGliding())
        {
            this.gravity.VelocityInGravityReferenceFrame = new Vector2(this.gravity.VelocityInGravityReferenceFrame.x, GetCurrentGlidingVelocity());

            if (this.gravity.VelocityInGravityReferenceFrame.x > BalloonGlidingHSpeed)
            {
                this.gravity.VelocityInGravityReferenceFrame = new Vector2(BalloonGlidingHSpeed, this.gravity.VelocityInGravityReferenceFrame.y);
            }
            else if (this.gravity.VelocityInGravityReferenceFrame.x < -BalloonGlidingHSpeed)
            {
                this.gravity.VelocityInGravityReferenceFrame = new Vector2(-BalloonGlidingHSpeed, this.gravity.VelocityInGravityReferenceFrame.y);
            }
        }
        else
        {
            if (this.noGravityTimeWhenBoostFromAirlock == 0)
                this.velocity += GravityAcceleration * gravityDirectionVector;
        }
    }

    private bool IsGliding()
    {
        return this.gravity.VelocityInGravityReferenceFrame.y <= BalloonGlidingVelocityGainHeight &&
            GameInput.Player(this).holdingJump == true &&
            this.jumpsSinceOnGround >= 2;
    }

    private float GetCurrentGlidingVelocity()
    {
        switch(this.glidingMode)
        {
            case GlidingMode.LoseHeight:
            default:
                return this.gravity.VelocityInGravityReferenceFrame.y + BalloonGlidingGravityLoseHeight;

            case GlidingMode.MaintainHeight:
                return 0f;

            case GlidingMode.GainHeight:
                return BalloonGlidingVelocityGainHeight;
        }
    }

    public void HandlePufferJump(int jumpsPermitted)
    {
        if(this.parachute.IsAvailable == true) return;

        switch(this.balloonJumpsSinceOnGround)
        {
            case 2:
            default:
                this.glidingMode = GlidingMode.LoseHeight;
            break;

            case 3:
                this.glidingMode = GlidingMode.MaintainHeight;
            break;

            case 4:
                this.glidingMode = GlidingMode.GainHeight;
            break;
        }

        if((GameInput.Player(this).holdingJump == false &&
            this.balloonHeadTimer > 0) || IsInsideWater == true)
        {
            this.balloonHeadTimer -= 1;
        }

        // balloon jump
        if(GameInput.Player(this).pressedJump == true &&
            this.jumpsSinceOnGround >= jumpsPermitted - 1 &&
            this.balloonJumpsSinceOnGround < this.balloonJumpMax &&
            IsInsideWater == false)
        {
            this.balloonJumpsSinceOnGround += 1;

            if (this.gravity.IsZeroGravity() == true)
            {
                var velocity = this.gravity.VelocityInGravityReferenceFrame;
                velocity.Normalize();
                if (velocity.sqrMagnitude == 0)
                {
                    velocity = new Vector2(
                        Mathf.Sin(this.groundState.angleDegrees * 180 / Mathf.PI),
                        -Mathf.Cos(this.groundState.angleDegrees * 180 / Mathf.PI)
                    );
                }
                velocity *= FreeFloatingSecondJumpStrength;
                this.gravity.VelocityInGravityReferenceFrame = velocity;
            }
            else
            {
                this.gravity.VelocityUp = SecondJumpStrength;
            }

            this.balloonHeadTimer = BallonHeadDeflateTime;
            Audio.instance.PlayRandomSfx(options: null, position: this.position, Assets.instance.sfxPufferInflate);
        }

        // start gliding on first jump
        if(IsGliding() == false &&
            this.gravity.VelocityInGravityReferenceFrame.y <= BalloonGlidingVelocityGainHeight &&
            GameInput.Player(this).holdingJump == true &&
            this.jumpsSinceOnGround == 1 &&
            this.balloonJumpsSinceOnGround == 0 &&
            IsValidStateForGliding())
        {
            this.jumpsSinceOnGround = 2;
            this.balloonJumpsSinceOnGround = 1;
            this.balloonHeadTimer = BallonHeadDeflateTime;
        }

        float deflateValue = Mathf.InverseLerp(BallonHeadDeflateTime, 0, this.balloonHeadTimer);

        if(deflateValue == 1f && this.balloonJumpsSinceOnGround > 0)
        {
            this.balloonHeadTimer = BallonHeadDeflateTime;
            this.balloonJumpsSinceOnGround -= 1;
        }
    }

    private bool IsValidStateForGliding()
    {
        if(this.state == State.FireFromCityCannon) return false;
        if(this.state == State.InsideWater) return false;
        if(this.state == State.InsideWaterCurrent) return false;

        return true;
    }

    private void InitializeHeadAnimationsDictionary()
    {
        this.headAnimationsDictionary = new Dictionary<Animated.Animation, Animated.Animation>()
        {
            //{ this.animationStanding, this.animationHeadStanding },
            { this.animationRunning, this.animationHeadRunning },
            { this.animationRunningPanicked, this.animationHeadRunningPanicked },
            { this.animationRunningTooFast, this.animationHeadRunningTooFast },
            { this.animationStompFrame, this.animationHeadStompFrame },
            { this.animationAfterStomp, this.animationHeadAfterStomp },
            { this.animationWallSliding, this.animationHeadWallSliding },
            { this.animationIceSliding, this.animationHeadIceSliding },
            { this.animationPoleSliding, this.animationHeadPoleSliding },
            { this.animationSpring, this.animationHeadSpring },
            { this.animationHanging, this.animationHeadHanging },
            { this.animationGrabSide, this.animationHeadGrabSide },
            { this.animationGrabStand, this.animationHeadGrabStand },
            { this.animationSwimmingCeiling, this.animationHeadSwimmingCeiling },

            // First Jump (Normal jump, not inflated)
            { this.animationJumping, this.animationHeadJumping },
            { this.animationJumpingDown, this.animationHeadJumpingDown },

            // Second Jump (1st Inflate Phase, character power lvl. 1)
            //{ this.animationDoubleJumping, this.animationHeadInflate1 },
            { this.animationBodyInflate1JumpDown, this.animationHeadInflate1 },
            { this.animationBodyInflate1JumpUp, this.animationHeadInflate1 },
            { this.animationBodyInflate1JumpHold, this.animationHeadInflate1 },

            // Third Jump (2nd Inflate Phase, character power lvl. 2)
            { this.animationBodyInflate2JumpDown, this.animationHeadInflate2 },
            { this.animationBodyInflate2JumpUp, this.animationHeadInflate2 },
            { this.animationBodyInflate2JumpHold, this.animationHeadInflate2 },

            // Fourth Jump (3rd Inflate Phase, character power lvl. 3)
            { this.animationBodyInflate3JumpDown, this.animationHeadInflate3 },
            { this.animationBodyInflate3JumpUp, this.animationHeadInflate3 },
            { this.animationBodyInflate3JumpHold, this.animationHeadInflate3 },
            
            // Fifth Jump (4th and Final Inflate Phase, character power lvl. 4) 
            { this.animationBodyInflate4JumpDown, this.animationHeadInflate4 },
            { this.animationBodyInflate4JumpUp, this.animationHeadInflate4 },
            { this.animationBodyInflate4JumpHold, this.animationHeadInflate4 },

        };

        this.headTransitionsDictionary = new Dictionary<Animated.Animation, Animated.Animation>()
        {
            { this.animationTransitionWallBounce, this.animationHeadTransitionWallBounce },
            { this.animationTransitionWallBouncePanicked, this.animationHeadTransitionWallBouncePanicked },
            { this.animationTransitionRunToWallSlide, this.animationHeadTransitionRunToWallSlide },
            { this.animationTransitionWallSlideToRun, this.animationHeadTransitionWallSlideToRun },
            { this.animationTransitionWallSlideToDrop, this.animationHeadTransitionWallSlideToDrop },
            { this.animationTransitionJumpToJumpDown, this.animationHeadTransitionJumpToJumpDown },
            { this.animationTransitionSwimToSwimUp, this.animationHeadTransitionSwimToSwimUp },
        };
    }

    private Animated.Animation GetHeadAnimation(Animated.Animation animation)
    {
        if(this.headAnimationsDictionary.ContainsKey(animation) == false)
        {
            return this.animationHeadBlank;
        }

        return this.headAnimationsDictionary[animation];
    }
    
    private Animated.Animation GetHeadTransitionAnimation(Animated.Animation animation)
    {
        if(this.headTransitionsDictionary.ContainsKey(animation) == false)
        {
            return animation;
        }

        return this.headTransitionsDictionary[animation];
    }

    public void UpdateBodyInflateSprites()
    {
        Animated.Animation inflateJumpDown =  IsGliding() ?
            this.animationBodyInflate1JumpHold : this.animationBodyInflate1JumpDown;
        Animated.Animation inflateJumpUp = this.animationBodyInflate1JumpUp;

        if(this.currrentHeadInflateLevel == 2)
        {
            inflateJumpDown = IsGliding() ?
                this.animationBodyInflate2JumpHold : this.animationBodyInflate2JumpDown;
            inflateJumpUp = this.animationBodyInflate2JumpUp;
        }
        else if(this.currrentHeadInflateLevel == 3)
        {
            inflateJumpDown = IsGliding() ?
                this.animationBodyInflate3JumpHold : this.animationBodyInflate3JumpDown;
            inflateJumpUp = this.animationBodyInflate3JumpUp;
        }
        else if(this.currrentHeadInflateLevel == 4)
        {
            inflateJumpDown = IsGliding() ?
                this.animationBodyInflate4JumpHold :  this.animationBodyInflate4JumpDown;
            inflateJumpUp = this.animationBodyInflate4JumpUp;
        }
        
        if (this.gravity.VelocityInGravityReferenceFrame.y < 0.1f)
        {
            this.animated.PlayAndLoop(inflateJumpDown);
        }
        else
        {
            this.animated.PlayAndLoop(inflateJumpUp);
        }
    }

    private void UpdateHeadInflateSprites()
    {
        Animated.Animation headInflate =  this.animationHeadInflate1;

        if(this.currrentHeadInflateLevel == 2)
        {
            headInflate =  this.animationHeadInflate2;
        }
        else if(this.currrentHeadInflateLevel == 3)
        {
            headInflate =  this.animationHeadInflate3;
        }
        else if(this.currrentHeadInflateLevel == 4)
        {
            headInflate =  this.animationHeadInflate4;
        }
        
        this.headAnimated.PlayAndLoop(headInflate);
    }

    private void SpawnDeflatedHead()
    {
        float facingValue = this.facingRight ? 1f : -1f;
        this.deflatedHeadInitialPosition = this.deflatedHeadLastPosition =
            this.position.Add(0.30f * facingValue, 1.10f);

        this.deflatedHeadParticle = Particle.CreatePlayAndLoop(
            this.animationDeflatedHead,
            180,
            this.deflatedHeadInitialPosition,
            this.transform.parent
        );

        this.deflatedHeadParticle.transform.localScale =
            new Vector3(facingValue, 1f, 1f);

        this.deflatedHeadParticle.velocity = Vector2.up * 0.05f;

        // reset particle variables
        this.deflatedHeadTimer = 0f;
        this.deflatedHeadVelocity = Vector2.zero;
    }

    public void UpdateHeadSprite()
    {
        if(this.headAnimationsDictionary == null)
        {
            InitializeHeadAnimationsDictionary();
        }

        HeadPosition();
        HeadScale();

        if(this.deflatedHeadParticle != null &&
            this.deflatedHeadParticle.gameObject.activeSelf == true &&
            this.deflatedHeadParticle.animated.currentAnimation == this.animationDeflatedHead)
        {
            ControlDeflatedHead();
        }

        if (infiniteJump.isActive == true)
        {
            this.currrentHeadInflateLevel = 0;
        }

        if (ShouldHeadBeInvisible())
        {
            this.headAnimated.PlayAndLoop(this.animationHeadBlank);
        }
        else if(this.animated.currentAnimation == this.animationDead)
        {
            if (this.headAnimated.currentAnimation != this.animationHeadDead)
            {
                this.headAnimated.PlayAndHoldLastFrame(this.animationHeadDead);
                this.headAnimated.OnFrame(8, SpawnDeflatedHead);

                Particle popParticle = Particle.CreateAndPlayOnce(
                    this.animationEffectHeadPop,
                    this.position.Add(-0.15f * (this.facingRight ? 1f : -1f), 0.60f),
                    this.transform.parent
                );
            }
        }
        else if(this.currrentHeadInflateLevel > 0)
        {
            UpdateHeadInflateSprites();
        }
        else if(this.transitionAnimation != null)
        {
            Animated.Animation bodyTransitionAnimation = this.transitionAnimation;
            Animated.Animation headTransitionAnimation =
                GetHeadTransitionAnimation(this.transitionAnimation);
            PlayHeadTransitionAnimation(headTransitionAnimation);
        }
        else if(this.animated.currentAnimation != null)
        {
            Animated.Animation bodyAnimation = this.animated.currentAnimation;
            Animated.Animation headAnimation = GetHeadAnimation(bodyAnimation);
            this.headAnimated.PlayAndLoop(headAnimation);
        }
        else if (this.state == State.StuckToSticky)
        {
            this.headAnimated.Stop();
            this.headSpriteRenderer.sprite = this.animationHeadJumping.sprites[0];
        }

        this.headSpriteRenderer.sortingLayerName = this.spriteRenderer.sortingLayerName;
        this.headSpriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder + 1;
    }

    private bool ShouldHeadBeInvisible()
    {
        return
            this.state == State.InsidePipe ||
            this.state == State.InsideBox ||
            this.state == State.InsideSpaceBooster ||
            this.state == State.InsideCityCannon ||
            this.state == State.FireFromCityCannon ||
            this.state == State.InsideLift ||
            this.state == State.UsingParachuteClosed ||
            this.state == State.UsingParachuteOpened ||
            this.grabbingOntoItem is MultiplayerHeadset;
    }

    private void ControlDeflatedHead()
    {
        float period = 7;
        float amplitude = 2f + (4f * Mathf.InverseLerp(60f, 0f, this.deflatedHeadTimer));

        float theta = this.deflatedHeadTimer / period;
        float distance = amplitude * Mathf.Sin(theta);
        this.deflatedHeadTimer += 1;

        distance *= this.facingRight ? 1f : -1f;

        float hPosition = this.deflatedHeadInitialPosition.x + distance;
        float vAcceleration = 0.01f; //0.005f;

        this.deflatedHeadVelocity += new Vector3(0f, vAcceleration);
        this.deflatedHeadParticle.transform.position += this.deflatedHeadVelocity;
        this.deflatedHeadParticle.transform.position = new Vector2(
            hPosition, this.deflatedHeadParticle.transform.position.y
        );

        Vector2 deflatedHeadDirection =
            (this.deflatedHeadParticle.transform.position - this.deflatedHeadLastPosition).normalized;
        this.deflatedHeadLastPosition = this.deflatedHeadParticle.transform.position;

        this.deflatedHeadParticle.transform.localRotation =
            Quaternion.LookRotation(Vector3.forward, deflatedHeadDirection);

        if(this.map.frameNumber % 2 == 0)
        {
            float x = this.deflatedHeadParticle.transform.position.x;
            float y = this.deflatedHeadParticle.transform.position.y;

            int lifetime = UnityEngine.Random.Range(25, 35);

            Particle p = Particle.CreatePlayAndHoldLastFrame(
                this.map.player.animationDustParticle,
                lifetime,
                new Vector2(
                    x + UnityEngine.Random.Range(-0.2f, 0.2f),
                    y + UnityEngine.Random.Range(-0.2f, 0.2f)
                ),
                this.transform.parent
            );
            float s = UnityEngine.Random.Range(0.75f, 1f);
            p.transform.localScale = new Vector3(s, s, 1);

            p.spriteRenderer.sortingLayerName = "Enemies";
            p.spriteRenderer.sortingOrder = -2;
        }
    }

    public void UpdatePoleSlidingTwistSprite(int frame)
    {
        this.headAnimated.Stop();
        this.headSpriteRenderer.sprite =
            this.animationHeadPoleSlidingTwist.sprites[frame];
    }

    private void PlayHeadTransitionAnimation(Animated.Animation animation)
    {
        if (this.headAnimated.currentAnimation != animation)
        {
            this.headAnimated.PlayOnce(animation, delegate
            {
                this.UpdateHeadSprite();
            });
        }
    }

    private void HeadPosition()
    {
        Vector2 headLocalPosition = Vector2.zero;

        if(this.headAnimated.currentAnimation == this.animationHeadInflate1)
        {
            if(this.animated.currentAnimation == this.animationWallSliding)
            {
                headLocalPosition = new Vector2(0.068f, 0.538f);
            }
            else if (this.animated.currentAnimation == this.animationRunning)
            {
                headLocalPosition = new Vector2(0f, 1f);
            }
        }
        else if(this.headAnimated.currentAnimation == this.animationHeadInflate2)
        {
            if(this.animated.currentAnimation == this.animationWallSliding)
            {
                headLocalPosition = new Vector2(-0.017f, 0.538f);
            }
            else if (this.animated.currentAnimation == this.animationRunning)
            {
                headLocalPosition = new Vector2(0f, 1f);
            }
        }
        else if(this.headAnimated.currentAnimation == this.animationHeadInflate3)
        {
            if(this.animated.currentAnimation == this.animationWallSliding)
            {
                headLocalPosition = new Vector2(0.092f, 0.671f);
            }
            else if (this.animated.currentAnimation == this.animationRunning)
            {
                headLocalPosition = new Vector2(0f, 1f);
            }
        }
        else if(this.headAnimated.currentAnimation == this.animationHeadInflate4)
        {
            if(this.animated.currentAnimation == this.animationWallSliding)
            {
                headLocalPosition = new Vector2(0.069f, 0.766f);
            }
            else if (this.animated.currentAnimation == this.animationRunning)
            {
                headLocalPosition = new Vector2(0f, 1f);
            }
        }

        this.headAnimated.transform.localPosition = headLocalPosition;
    }

    private void HeadScale()
    {
        float targetHeadScale = 1f;
        float currentHeadScale = this.headAnimated.transform.localScale.x;

        if(this.currrentHeadInflateLevel < this.balloonJumpsSinceOnGround)
        {
            targetHeadScale = 1.1f;

            if(currentHeadScale == targetHeadScale)
            {
                this.currrentHeadInflateLevel += 1;
                targetHeadScale = currentHeadScale = 1f;
            }
        }
        else if(this.currrentHeadInflateLevel > this.balloonJumpsSinceOnGround)
        {
            targetHeadScale = 0.9f;

            if(currentHeadScale == targetHeadScale)
            {
                this.currrentHeadInflateLevel -= 1;
                targetHeadScale = currentHeadScale = 1f;
            }
        }

        float slideSpeed = this.jumpsSinceOnGround >= 2 ? 0.02f : 0.085f;
        currentHeadScale = Util.Slide(currentHeadScale, targetHeadScale, slideSpeed);
        this.headAnimated.transform.localPosition = Vector3.zero;
        this.headAnimated.transform.localScale = Vector3.one * currentHeadScale;
    }

    public override void ResetDoubleJump()
    {
        base.ResetDoubleJump();
        this.balloonJumpsSinceOnGround = 0;
    }

    public override void Die()
    {
        base.Die();

        this.balloonJumpsSinceOnGround = 0;
        this.currrentHeadInflateLevel = 0;
        this.balloonHeadTimer = 0;
        this.headAnimated.transform.localScale = Vector3.one;
    }

    protected override void DeathFX()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxPufferDeath);
    }

    protected override void RespawnFX()
    {
        this.spriteRenderer.enabled = true;
        this.position = this.GetCheckpointPosition().Add(0f, 0.7625f);
        this.animated.PlayAndHoldLastFrame(this.animationBodyShoes);
        
        PlayerDeathParticles.Life(
            this,
            new Color(42 / 255.0f, 136 / 255.0f, 233 / 255.0f),
            this.position,
            RespawnFromShoes
        );

        Audio.instance.PlaySfx(Assets.instance.sfxPufferRespawn);
    }

    private void RespawnFromShoes()
    {
        this.animated.PlayOnce(this.animationRespawn, delegate
        {
            this.state = State.Normal;
        });
    }
}
