﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerShield
{
    public GameObject shieldEffectPrefab;

    [NonSerialized] public bool isActive = false;

    private Player player;
    private PlayerShieldEffect playerShieldEffect;

    [NonSerialized] public int currentActiveShields = 0;
    private int invulnerableTimer = 0;
    private Particle uiParticle;

    private const int DefaultShieldsNumber = 3;
    private const int MaxShieldsNumber = 9;
    private const int InvulnerableAfterHitTime = 90;

    public void Init(Player player)
    {
        this.player = player;
    }
    
    public void Advance()
    {
        if(IsInvulnerable() == true)
        {
            this.invulnerableTimer -= 1;

            // invulnerable blinking effect
            if(this.player.map.frameNumber % 2 == 0)
            {
                this.player.spriteRenderer.enabled = !this.player.spriteRenderer.enabled;
            }
            
            if(this.invulnerableTimer <= 0)
            {
                this.player.spriteRenderer.enabled = true;
            }
        }

        if(this.playerShieldEffect != null)
        {
            this.playerShieldEffect.Advance();
        }

        // visual effect: powerup sprite moving from UI box to player
        if(this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if(Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool activatedByBubble = false)
    {
        if(this.currentActiveShields >= MaxShieldsNumber) return;

        int shieldNumber = DefaultShieldsNumber;
        if(this.currentActiveShields + shieldNumber > MaxShieldsNumber)
        {
            shieldNumber = MaxShieldsNumber - this.currentActiveShields;
        }

        this.isActive = true;

        if(this.playerShieldEffect == null)
        {
            this.playerShieldEffect = GameObject.Instantiate(
                this.shieldEffectPrefab,
                this.player.transform.position,
                this.player.transform.rotation,
                this.player.map.transform
            ).GetComponent<PlayerShieldEffect>();
        }

        this.playerShieldEffect.Play(this.player);
        
        this.currentActiveShields += shieldNumber;
        this.playerShieldEffect.AddShields(shieldNumber);
        
        

        if(activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),
                this.player.map.transform
            );

            this.uiParticle.transform.localScale = Vector3.one * 0.35f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxPowerupShieldLoop))
        {
            Audio.instance.PlaySfxLoop(Assets.instance.sfxPowerupShieldLoop);
        }
    }

    public void AddShield()
    {
        this.currentActiveShields += 1;
        this.playerShieldEffect.AddShield();
    }

    public void RemoveShield()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxPowerupShieldBreak);

        this.player.map.ScreenShakeAtPosition(
            this.player.position,
            Vector2.one * .5f
        );

        this.currentActiveShields -= 1;
        this.playerShieldEffect.RemoveShield();

        if(this.currentActiveShields <= 0)
        {
            End();
        }

        this.invulnerableTimer = InvulnerableAfterHitTime;
    }

    public void End()
    {
        Audio.instance.StopSfxLoop(Assets.instance.sfxPowerupShieldLoop);

        this.isActive = false;
        
        if(this.playerShieldEffect != null)
        {
            this.playerShieldEffect.Stop();
        }

        this.currentActiveShields = 0;
    }

    public bool IsInvulnerable()
    {
        return this.invulnerableTimer > 0;
    }

    public void HideShieldEffect()
    {
        playerShieldEffect.hidden = true;
        playerShieldEffect.innerBallAnimated.gameObject.SetActive(false);
        playerShieldEffect.electricBlueAnimated.gameObject.SetActive(false);
        playerShieldEffect.electricWhiteAnimated.gameObject.SetActive(false);

        for(int i = 0; i < playerShieldEffect.currentShields.Count; i++)
        {
            playerShieldEffect.currentShields[i].gameObject.SetActive(false);
        }
    }

    public void ShowShieldEffect()
    {
        playerShieldEffect.hidden = false;
        playerShieldEffect.innerBallAnimated.gameObject.SetActive(true);
        playerShieldEffect.electricBlueAnimated.gameObject.SetActive(true);
        playerShieldEffect.electricWhiteAnimated.gameObject.SetActive(true);

        for (int i = 0; i < playerShieldEffect.currentShields.Count; i++)
        {
            playerShieldEffect.currentShields[i].gameObject.SetActive(true);
        }
    }
}
