﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShieldEffect : MonoBehaviour
{
    private Player player;

    public GameObject effectParentGO;

    public Animated innerBallAnimated;
    public Animated electricBlueAnimated;
    public Animated electricWhiteAnimated;

    public Animated.Animation animationInnerBall;
    public Animated.Animation animationElectricBlue;
    public Animated.Animation animationElectricWhite;
    public Animated.Animation animationInnerBallHit;
    public Animated.Animation animationElectricBlueHit;
    public Animated.Animation animationElectricWhiteHit;

    public Animated.Animation animationInsideSpark;

    public GameObject shieldPrefab;

    public List<PlayerShieldSprite> currentShields = new List<PlayerShieldSprite>();
    private List<PlayerShieldSprite> destroyingShields = new List<PlayerShieldSprite>();

    private float timer;
    private float angle;

    public const float Period = .25f;
    public const float Amplitude = 2.5f;

    private const float RotateSpeed = 20f;

    public bool hidden = false;

    private bool ShouldAdvance()
    {
        if(currentShields.Count > 0) return true;
        if(destroyingShields.Count > 0) return true;

        return false;
    }

    public void Advance()
    {
        if(ShouldAdvance() == false)
        {
            if(this.effectParentGO.activeSelf == true &&
                this.innerBallAnimated.currentAnimation != this.animationInnerBallHit)
            {
                this.effectParentGO.SetActive(false);
            }

            return;
        }
        
        this.transform.position = GetTargetPosition();

        this.timer += Time.deltaTime;

        if(this.player.swordAndShield.isActive == false)
        {
            for (int i = 0; i < this.currentShields.Count; i++)
            {
                float extraTime = ((Period * 6f) / this.currentShields.Count) * i;
                float finalTimer = timer + extraTime;

                this.currentShields[i].Move(this.transform.position, finalTimer, 90f);
            }
        }
        else
        {
            for (int i = 0; i < this.currentShields.Count; i++)
            {
                float extraTime = ((Period * 6f) / (this.currentShields.Count + this.player.swordAndShield.playerShieldEffect.currentShields.Count + this.player.swordAndShield.petSwords.Count)) * i;
                float finalTimer = this.player.swordAndShield.playerShieldEffect.timer + extraTime;

                this.currentShields[i].Move(this.transform.position, finalTimer, 90f);
            }
        }
        

        for (int i = this.destroyingShields.Count - 1; i >= 0 ; i--)
        {
            this.destroyingShields[i].Break(this.transform.position + Vector3.up * 2f);

            if(this.destroyingShields[i].destroyed == true)
            {
                this.destroyingShields.Remove(this.destroyingShields[i]);
            }
        }

        if(this.player.map.frameNumber % 6 == 0 && this.hidden == false)
        {
            var sparkle = Particle.CreateAndPlayOnce(
                this.animationInsideSpark,
                (Vector2)this.transform.position + Random.insideUnitCircle * 1.6f,
                //this.transform
                this.player.map.transform
            );
            sparkle.gameObject.name = "Shield Effect Spark Particle";
            sparkle.spriteRenderer.sortingLayerName = "Player (AL)";
            sparkle.spriteRenderer.sortingOrder = -1;
        }
    }

    private Vector2 GetTargetPosition()
    {
        return this.player.transform.position + (Vector3.up * .05f);
    }

    public void Play(Player player)
    {
        this.player = player;

        this.effectParentGO.SetActive(true);

        this.innerBallAnimated.PlayAndLoop(this.animationInnerBall);
        this.electricBlueAnimated.PlayAndLoop(this.animationElectricBlue);
        this.electricWhiteAnimated.PlayAndLoop(this.animationElectricWhite);

        this.gameObject.layer = this.player.map.Layer();
        this.effectParentGO.gameObject.layer = this.player.map.Layer();
        this.innerBallAnimated.gameObject.layer = this.player.map.Layer();
        this.electricBlueAnimated.gameObject.layer = this.player.map.Layer();
        this.electricWhiteAnimated.gameObject.layer = this.player.map.Layer();    
    }

    public void Stop()
    {
        RemoveAllShields();

        // When player dies while still having shields left (e.g. getting squashed),
        // we handle the remaining graphics and animations with this:
        if(this.player.alive == false)
        {
            this.effectParentGO.SetActive(false);

            foreach(PlayerShieldSprite shield in this.destroyingShields)
            {
                shield.BreakWithoutMoving();
            }

            this.destroyingShields.Clear();
        }
    }

    public void AddShields(int number)
    {
        for (int i = 0; i < number; i++)
        {
            AddShield();
        }
    }

    public void AddShield()
    {
        PlayerShieldSprite shieldSprite = Instantiate(this.shieldPrefab).GetComponent<PlayerShieldSprite>();
        shieldSprite.transform.position = GetTargetPosition() + (Vector2.up * 2f);
        shieldSprite.gameObject.layer = this.player.map.Layer();
        this.currentShields.Add(shieldSprite);
    }

    private void RemoveAllShields()
    {
        int numberShield = this.currentShields.Count;

        for (int i = 0; i < numberShield; i++)
        {
            RemoveShield(i == numberShield - 1);
        }
    }

    public void RemoveShield(bool playAnimation = true)
    {
        PlayerShieldSprite lastShieldSprite = currentShields[currentShields.Count - 1];
        lastShieldSprite.SetupBreak();
        lastShieldSprite.transform.SetParent(this.transform);

        this.currentShields.Remove(lastShieldSprite);
        this.destroyingShields.Add(lastShieldSprite);

        if(playAnimation == true)
        {
            this.innerBallAnimated.PlayOnce(this.animationInnerBallHit, () =>
            {
                this.innerBallAnimated.PlayAndLoop(this.animationInnerBall);
            });
            this.electricBlueAnimated.PlayOnce(this.animationElectricBlueHit, () =>
            {
                this.electricBlueAnimated.PlayAndLoop(this.animationElectricBlue);
            });
            this.electricWhiteAnimated.PlayOnce(this.animationElectricWhiteHit, () =>
            {
                this.electricWhiteAnimated.PlayAndLoop(this.animationElectricWhite);
            });
        }
    }
}
