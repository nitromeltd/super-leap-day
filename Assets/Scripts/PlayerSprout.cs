using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSprout : Player
{
    [Header("Character: Sprout")]
    public Animated.Animation animationSuck;
    public Animated.Animation animationSwallow;
    public Animated.Animation animationPot;

    [Header("Death & Respawn")]
    public Animated.Animation particleHeadSpin;
    public Sprite particleDeathPetal;
    public AnimationCurve petalFadeOut;
    public Sprite spritePotDebrisLeft;
    public Sprite spritePotDebrisRight;
    
    [Header("Backpack")]
    public Animated.Animation animationRunningArm;

    public enum SuckingPower
    {
        SucksOnlyPickups, // only sucks pickups (fruits, coins, etc)
        WobbleBlocksWithPickups, // wobble blocks with hidden pick ups
        SucksItemsFromBlocks, // sucks items from within blocks
        SucksChestsAndDestroyBlocks // pull pick ups from blocks and chests and destroy blocks.
    }

    private SuckingPower suckingPower = SuckingPower.SucksOnlyPickups;

    private float currentShortRange = 0; // for everything else
    private float currentLongRange = 0; //  only for pickups
    private int suckingHoldTimer = 0;
    private float suckingShortRange = 0f;
    private float suckingLongRange = 0f;
    private List<Item> suckingItems = new List<Item>();
    private List<SuckingParticle> suckingParticles = new List<SuckingParticle>();

    private const int SuckingHoldMinimumTime = 15;
    private const int SuckingHoldFallTime = 60;

    public class SuckingParticle
    {
        public PlayerSprout player;
        public Particle particle;
        public bool suckActive;
        private Vector2 smoothSuckVel;
        private float smoothSuckTime;

        public SuckingParticle(Particle particle, PlayerSprout player)
        {
            this.particle = particle;
            this.player = player;
            this.suckActive = false;
            this.smoothSuckVel = Vector2.zero;
            this.smoothSuckTime = 0f;
        }

        public void Enter()
        {
            if(this.suckActive == true) return;

            this.suckActive = true;
            this.smoothSuckTime = UnityEngine.Random.Range(0.15f, 0.20f);
            this.particle.controlledByPlayer = true;

            this.particle.spriteRenderer.sortingLayerName = "Player (AL)";
            this.particle.spriteRenderer.sortingOrder = -1;
        }

        public void Exit()
        {
            this.suckActive = false;
            this.particle.controlledByPlayer = false;
            this.particle.velocity = Vector2.down * 0.05f;
            this.particle.acceleration = Vector2.down * 0.01f;
        }

        public void Stop()
        {
            Exit();
            this.particle.StopAndReturnToPool();
        }

        public void Update()
        {
            if(this.suckActive == false) return;

            this.particle.transform.position = Vector2.SmoothDamp(
                this.particle.transform.position,
                this.player.position,
                ref smoothSuckVel,
                this.smoothSuckTime
            );
        }
    }

    private bool IsStateValidForSucking()
    {
        return this.state == State.Jump ||
            this.state == State.DropVertically ||
            this.state == State.Spring ||
            this.state == State.SpringFromPushPlatform ||
            this.state == State.SproutSucking;
    }

    public override void ChangeCharacterUpgradeLevel(int upgradeLevel)
    {
        switch(upgradeLevel)
        {
            case 1:
                this.suckingPower = SuckingPower.SucksOnlyPickups;
                this.suckingShortRange = 4f;
                this.suckingLongRange = 8f;
            break;

            case 2:
                this.suckingPower = SuckingPower.WobbleBlocksWithPickups;
                this.suckingShortRange = 6f;
                this.suckingLongRange = 10f;
            break;

            case 3:
                this.suckingPower = SuckingPower.SucksItemsFromBlocks;
                this.suckingShortRange = 8f;
                this.suckingLongRange = 12f;
            break;

            case 4:
                this.suckingPower = SuckingPower.SucksChestsAndDestroyBlocks;
                this.suckingShortRange = 10f;
                this.suckingLongRange = 14f;
            break;
        }
    }

    public void HandleSuckingInput()
    {
        if(GameInput.Player(this).holdingJump == true &&
            this.groundState.onGround == false &&
            IsStateValidForSucking())
        {
            this.suckingHoldTimer += 1;

            if(this.suckingHoldTimer >= SuckingHoldMinimumTime && this.state != State.SproutSucking)
            {
                this.state = State.SproutSucking;
                Audio.instance.PlayRandomSfx(options: null, position: this.position, Assets.instance.sfxSproutSuction);       
            }
        }
        else 
        {
            this.suckingHoldTimer = 0;
            this.currentShortRange = this.currentLongRange = 0f;

            if(this.state == State.SproutSucking)
            {
                if(this.groundState.onGround == true)
                {
                    this.state = State.Normal;
                }
                else
                {
                    this.state = State.Jump;

                    // avoid Sprout getting stuck in zero gravity after cancelling their sucking ability
                    if(this.gravity.IsZeroGravity() && this.jumpsSinceOnGround > 1)
                    {
                        this.gravity.VelocityInGravityReferenceFrame -= Vector2.up * (JumpStrength / 5);
                    }
                }
            }
            
            // exit suck for all items still on sucking list
            foreach (var item in this.suckingItems)
            {
                if (item is Pickup)
                {
                    (item as Pickup).ExitSuck();
                }

                if (item is RandomBox)
                {
                    (item as RandomBox).ExitSuck();
                }

                if (item is BreakableBlock)
                {
                    (item as BreakableBlock).ExitSuck();
                }

                if (item is Chest)
                {
                    (item as Chest).ExitSuck();
                }
            }

            foreach(var sp in this.suckingParticles)
            {
                sp.Exit();
            }

            this.suckingItems.Clear();
            this.suckingParticles.Clear();

            Audio.instance.StopSfx(Assets.instance.sfxSproutSuction[0]);
            Audio.instance.StopSfx(Assets.instance.sfxSproutSuction[1]);
            Audio.instance.StopSfx(Assets.instance.sfxSproutSuction[2]);
        }
    }

    public void HandleSuckingState()
    {
        SafeguardForEndlessPortalDrop();

        this.currentShortRange = Util.Slide(this.currentShortRange, this.suckingShortRange, 0.20f);
        this.currentLongRange = Util.Slide(this.currentLongRange, this.suckingLongRange, 0.20f);

        IEnumerable<Chunk> chunks = this.map.ChunksWithinDistance(this.position, this.suckingLongRange);

        foreach(var c in chunks)
        {
            foreach (var item in c.items)
            {
                if(this.suckingItems.Contains(item)) continue;

                if ((item is Pickup) == true)
                {
                    if (Vector2.Distance(this.position, item.position) <= this.currentLongRange)
                    {
                        this.suckingItems.Add(item);
                    }
                }

                if ((item is RandomBox) == true||
                    (item is BreakableBlock) == true||
                    (item is Chest) == true)
                {
                    if (Vector2.Distance(this.position, item.position) <= this.currentShortRange)
                    {
                        this.suckingItems.Add(item);
                    }
                }
            }

            foreach (var item in this.suckingItems)
            {
                bool itemOutOfShortRange =
                    Vector2.Distance(this.position, item.position) > this.currentShortRange;
                bool itemOutOfLongRange =
                    Vector2.Distance(this.position, item.position) > this.currentLongRange;

                if (item is Pickup)
                {
                    if (itemOutOfLongRange == true ||
                        (item as Pickup).collected == true)
                    {
                        (item as Pickup).ExitSuck();
                        continue;
                    }

                    if((item is ABCoin))
                    {
                        if((item as ABCoin).isSolid)
                        {
                            (item as Pickup).EnterSuck();
                        }
                    }
                    else if (item is TimerCoin)
                    {
                        if ((item as TimerCoin).isActive && !(item as TimerCoin).collected)
                        {
                            (item as TimerCoin).EnterSuck();
                        }
                    }
                    else
                    {
                        (item as Pickup).EnterSuck();
                    }

                }

                if (item is RandomBox)
                {
                    if (itemOutOfShortRange == true ||
                        (item as RandomBox).destroyed == true)
                    {
                        (item as RandomBox).ExitSuck();
                        continue;
                    }

                    switch(this.suckingPower)
                    {
                        case SuckingPower.WobbleBlocksWithPickups:
                            (item as RandomBox).ShakeIfRarePickup();
                        break;

                        case SuckingPower.SucksItemsFromBlocks:
                            (item as RandomBox).ExtractPickup();
                        break;

                        case SuckingPower.SucksChestsAndDestroyBlocks:
                            (item as RandomBox).Break();
                        break;
                    }
                }

                if (item is BreakableBlock)
                {
                    if (itemOutOfShortRange == true ||
                        (item as BreakableBlock).destroyed == true)
                    {
                        (item as BreakableBlock).ExitSuck();
                        continue;
                    }

                    switch(this.suckingPower)
                    {
                        case SuckingPower.SucksChestsAndDestroyBlocks:
                            (item as BreakableBlock).Break(brokenByPlayer: false);
                        break;
                    }
                }

                if (item is Chest)
                {
                    if (itemOutOfShortRange == true ||
                        (item as Chest).Opened == true)
                    {
                        (item as Chest).ExitSuck();
                        continue;
                    }

                    switch(this.suckingPower)
                    {
                        case SuckingPower.WobbleBlocksWithPickups:
                        case SuckingPower.SucksItemsFromBlocks:
                            (item as Chest).EnterSuck();
                        break;

                        case SuckingPower.SucksChestsAndDestroyBlocks:
                            if ((item as Chest).CanBeOpened() && (item as Chest).isConnectedToLightBlock == false)
                            {
                                (item as Chest).Open();
                            }
                        break;
                    }
                }
            }
        }

        foreach(var p in Particle.active)
        {
            float distanceToPlayer =
                Vector2.Distance(this.position, p.transform.position);

            if(distanceToPlayer < this.currentLongRange &&
                p.canBeSucked == true &&
                p.controlledByPlayer == false)
            {
                SuckingParticle sp = new SuckingParticle(p, this);
                this.suckingParticles.Add(sp);
                sp.Enter();
            }
        }

        foreach(var sp in this.suckingParticles)
        {
            if(sp.particle.gameObject.activeSelf == false) continue;

            float distanceToPlayer =
                Vector2.Distance(this.position, sp.particle.transform.position);
                
            if(distanceToPlayer < 1f)
            {
                sp.Stop();
                continue;
            }

            sp.Update();
        }

        if(this.suckingHoldTimer < SuckingHoldFallTime)
        {
            this.gravity.VelocityInGravityReferenceFrame = new Vector2(
                Util.Slide(this.gravity.VelocityInGravityReferenceFrame.x, 0f, 0.03f),
                Util.Slide(this.gravity.VelocityInGravityReferenceFrame.y, 0f, 0.03f)
            );
        }
        else
        {
            this.gravity.VelocityInGravityReferenceFrame -= Vector2.up * 0.005f;

            if(this.gravity.VelocityUp > TopSpeed)
            {
                this.gravity.VelocityUp = TopSpeed;
            }
        }

        this.position += this.velocity;
    }

    protected override void DeathFX()
    {
        this.map.DelayedCall(0.20f, delegate
        {
            Vector2 headPosition = this.position.Add(0, 1f);
            Particle headSpin = Particle.CreatePlayAndLoop(
                this.particleHeadSpin,
                60 * 5,
                headPosition,
                this.transform.parent
            );

            headSpin.velocity = Vector2.up * 0.10f;
            headSpin.acceleration = Vector2.down * 0.005f;
            headSpin.spriteRenderer.sortingLayerName = "Items (Front)";
            headSpin.spriteRenderer.sortingOrder = 1;

            Vector2 GetRandomPetalVelocity()
            {
                return new Vector2(
                    Random.Range(0.01f, 0.03f),
                    Random.Range(0.005f, 0.03f)
                );
            }

            int numberPetals = 4;
            Vector2 savedPetalVelocity = Vector2.zero;

            for(int i = 0; i < numberPetals; i++)
            {
                float direction = i % 2 == 0 ? 1f : -1f;
                Vector2 velocity = Vector2.zero;

                if(i % 2 == 0)
                {
                    velocity = savedPetalVelocity = GetRandomPetalVelocity();
                }
                else
                {
                    velocity = savedPetalVelocity;
                }

                Particle petalParticle = Particle.CreateWithSprite(
                    this.particleDeathPetal,
                    60,
                    headPosition,
                    this.transform.parent
                );

                petalParticle.velocity = new Vector2(velocity.x * direction, -velocity.y);
                petalParticle.FadeOut(this.petalFadeOut);
                petalParticle.transform.localScale = new Vector3(-direction, 1f, 1f);
                petalParticle.spriteRenderer.sortingLayerName = "Items (Front)";
                petalParticle.spriteRenderer.sortingOrder = 2;
            }
        });

        Audio.instance.PlaySfx(Assets.instance.sfxSproutDeath);
    }

    protected override void RespawnFX()
    {
        this.spriteRenderer.enabled = true;
        this.position = this.GetCheckpointPosition().Add(0f, 0.4125f);
        this.animated.PlayAndHoldLastFrame(this.animationPot);
        
        PlayerDeathParticles.Life(
            this,
            Color.white,
            this.position.Add(0f, -0.60f),
            RespawnFromPot
        );

        Audio.instance.PlaySfx(Assets.instance.sfxSproutRespawn);
    }

    private void RespawnFromPot()
    {
        this.animated.PlayOnce(this.animationRespawn, delegate
        {
            this.state = State.Normal;
        });

        this.animated.OnFrame(11, () => { // pot particles
            Vector2 leftPosition  = this.position.Add(-0.25f, -0.60f);
            Vector2 rightPosition = this.position.Add(0.25f, -0.60f);

            var left = Particle.CreateWithSprite(
                this.spritePotDebrisLeft, 100, leftPosition, this.transform.parent
            );
            left.velocity     = new Vector2(-1f, 7f) / 16f;
            left.acceleration = new Vector2(0f, -0.5f) / 16f;

            var right = Particle.CreateWithSprite(
                this.spritePotDebrisRight, 100, rightPosition, this.transform.parent
            );
            right.velocity     = new Vector2(1f, 7f) / 16f;
            right.acceleration = new Vector2(0f, -0.5f) / 16f;
        });
    }

    protected override void UpdateSprite()
    {
        base.UpdateSprite();
        DrawBackArmForRunningWithBackpack();
    }

    private void DrawBackArmForRunningWithBackpack()
    {
        bool isRunning = this.animated.currentAnimation == this.animationRunning;
        
        this.backArmSpriteRenderer.gameObject.SetActive(isRunning);
        this.backArmSpriteRenderer.sprite =
            this.animationRunningArm.sprites[this.animated.frameNumber];
    }

}
