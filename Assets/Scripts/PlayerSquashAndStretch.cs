
using UnityEngine;
using System;

[Serializable]
public class PlayerSquashAndStretch
{
    public enum WallSide
    {
        Above,
        Below,
        OnLeft,
        OnRight
    }

    private Player player;
    public AnimationCurve animationCurve;
    [NonSerialized] public WallSide side;
    [NonSerialized] public float time;
    [NonSerialized] public bool active = false;

    public void Init(Player player)
    {
        this.player = player;
    }

    public void Squash(WallSide side)
    {
        this.side = side;
        this.time = 0;
        this.active = true;
    }

    public void Advance()
    {
        if (this.active == false)
            return;

        this.time += 1 / 60f;
        if (this.time >= 0.2f)
        {
            this.active = false;
            return;
        }

        float scale = this.animationCurve.Evaluate(this.time / 0.2f);
        float other = 1 / scale;
        other = Mathf.Lerp(other, 1, 0.5f);

        switch (this.side)
        {
            case WallSide.Below:
                this.player.transform.localScale = new Vector3(
                    this.player.transform.localScale.x * other,
                    this.player.transform.localScale.y * scale,
                    1
                );
                this.player.transform.localPosition =
                    this.player.position.Add(0, -1.0f * (1 - scale));
                break;

            case WallSide.Above:
                this.player.transform.localScale = new Vector3(
                    this.player.transform.localScale.x * other,
                    this.player.transform.localScale.y * scale,
                    1
                );
                this.player.transform.localPosition =
                    this.player.position.Add(0, 1.0f * (1 - scale));
                break;

            case WallSide.OnLeft:
                this.player.transform.localScale = new Vector3(
                    this.player.transform.localScale.x * scale,
                    this.player.transform.localScale.y * other,
                    1
                );
                this.player.transform.localPosition =
                    this.player.position.Add(-0.75f * (1 - scale), 0);
                break;

            case WallSide.OnRight:
                this.player.transform.localScale = new Vector3(
                    this.player.transform.localScale.x * scale,
                    this.player.transform.localScale.y * other,
                    1
                );
                this.player.transform.localPosition =
                    this.player.position.Add(0.75f * (1 - scale), 0);
                break;
        }
    }
}
