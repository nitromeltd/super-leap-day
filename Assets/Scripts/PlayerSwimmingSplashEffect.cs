using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwimmingSplashEffect : MonoBehaviour
{
    private Map map;

    public Animated frontSplash;
    public Animated backSplash;

    public Animated.Animation animationSwimmingSplash;
    public Animated.Animation animationEmpty;

    private int splashDelay;

    public void Init(Map map)
    {
        this.map = map;
        frontSplash.PlayAndLoop(animationSwimmingSplash);
        backSplash.PlayAndLoop(animationSwimmingSplash);
        this.splashDelay = 30;
        this.gameObject.layer = this.map.Layer();
        this.frontSplash.gameObject.layer = this.map.Layer();
        this.backSplash.gameObject.layer = this.map.Layer();
    }

    void Update()
    {
        this.transform.position = GetTargetPosition();
        this.transform.localScale = new Vector3(IsPlayerFacingRight()? -1: 1, 1, 1);

        if(this.map.player is PlayerKing)
        {
            frontSplash.transform.localPosition = new Vector3(2f, 0.2f, 0);
            backSplash.transform.localPosition = new Vector3(1.6f, 0.2f, 0);
        }
        else
        {
            frontSplash.transform.localPosition = new Vector3(1.7f, 0.2f, 0);
            backSplash.transform.localPosition = new Vector3(1.3f, 0.2f, 0);
        }

        if(this.map.player.IsOnWaterSurface == false)
        {
            frontSplash.PlayAndLoop(animationEmpty);
            backSplash.PlayAndLoop(animationEmpty);
            splashDelay = 10;
        }
        else
        {
            if(this.splashDelay > 0)
            {
                this.splashDelay--;
            }
            else
            {
                frontSplash.PlayAndLoop(animationSwimmingSplash);
                backSplash.PlayAndLoop(animationSwimmingSplash);
            }
        }
    }

    private Vector2 GetTargetPosition()
    {
        return this.map.player.transform.position;
    }
    private bool IsPlayerFacingRight()
    {
        return this.map.player.facingRight;
    }
}
