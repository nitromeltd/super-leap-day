using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class PlayerSwordAndShield
{
    public GameObject shieldEffectPrefab;
    [NonSerialized] public PetSword petSword;
    [NonSerialized] public List<PetSword> petSwords = new List<PetSword>();

    [NonSerialized] public bool isActive = false;

    private Player player;
    [NonSerialized] public PlayerSwordAndShieldEffect playerShieldEffect;

    [NonSerialized] public int currentActiveShields = 0;
    private int invulnerableTimer = 0;
    private Particle uiParticle;

    private const int DefaultShieldsNumber = 1;
    private const int MaxShieldsNumber = 3;
    private const int InvulnerableAfterHitTime = 90;

    public void Init(Player player)
    {
        this.player = player;
    }

    public void Advance()
    {
        if (IsInvulnerable() == true)
        {
            this.invulnerableTimer -= 1;

            // invulnerable blinking effect
            if (this.player.map.frameNumber % 2 == 0)
            {
                this.player.spriteRenderer.enabled = !this.player.spriteRenderer.enabled;
            }

            if (this.invulnerableTimer <= 0)
            {
                this.player.spriteRenderer.enabled = true;
            }
        }

        if (this.playerShieldEffect != null)
        {
            this.playerShieldEffect.Advance();
        }

        // visual effect: powerup sprite moving from UI box to player
        if (this.uiParticle != null)
        {
            Vector3 uiParticlePosition = this.uiParticle.transform.position;
            Vector3 playerPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(this.player.map, this.player.position);

            this.uiParticle.transform.position = Vector3.MoveTowards(
                uiParticlePosition, playerPosition, 0.75f
            );

            if (Vector3.Distance(uiParticlePosition, playerPosition) < 0.01f)
            {
                this.uiParticle = null;
            }
        }
    }

    public void Activate(bool activatedByBubble = false)
    {
        if (this.currentActiveShields >= MaxShieldsNumber) return;

        int shieldNumber = DefaultShieldsNumber;
        if (this.currentActiveShields + shieldNumber > MaxShieldsNumber)
        {
            shieldNumber = MaxShieldsNumber - this.currentActiveShields;
        }

        this.isActive = true;

        if (this.playerShieldEffect == null)
        {
            this.playerShieldEffect = GameObject.Instantiate(
                this.shieldEffectPrefab,
                this.player.transform.position,
                this.player.transform.rotation,
                this.player.map.transform
            ).GetComponent<PlayerSwordAndShieldEffect>();
        }
        this.playerShieldEffect.gameObject.layer = this.player.map.Layer();
        this.playerShieldEffect.Play(this.player);

        this.currentActiveShields += shieldNumber;
        this.playerShieldEffect.AddShields(shieldNumber);

        PetSword petSword = PetSword.Create(
                this.player.position,
                this.player.map.NearestChunkTo(this.player.position)
            );

        petSwords.Add(petSword);

        if (activatedByBubble == false)
        {
            this.uiParticle = Particle.CreateWithSprite(
                IngameUI.instance.PowerUpImage(this.player.map).sprite,
                18,
                (Vector2)(IngameUI.instance.PowerUpImageRectTransform(this.player.map)).position + new Vector2(0, 0.2f),
                this.player.map.transform
            );
            
            this.uiParticle.transform.localScale = Vector3.one * 0.35f;
            Util.SetLayerRecursively(this.uiParticle.transform, LayerMask.NameToLayer("UI"), true);
        }

        if (!Audio.instance.IsSfxPlaying(Assets.instance.sfxPowerupShieldLoop))
        {
            Audio.instance.PlaySfxLoop(Assets.instance.sfxPowerupShieldLoop);
        }
    }

    public void AddShield()
    {
        this.currentActiveShields += 1;
        this.playerShieldEffect.AddShield();
    }

    public void RemoveShield()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxPowerupShieldBreak);

        this.player.map.ScreenShakeAtPosition(
            this.player.position,
            Vector2.one * .5f
        );

        this.currentActiveShields -= 1;
        this.playerShieldEffect.RemoveShield();

        petSwords[petSwords.Count - 1].DestroyPet();
        petSwords.RemoveAt(petSwords.Count - 1);

        if (this.currentActiveShields <= 0)
        {
            End();
        }

        this.invulnerableTimer = InvulnerableAfterHitTime;
    }

    public void End()
    {
        Audio.instance.StopSfxLoop(Assets.instance.sfxPowerupShieldLoop);

        this.isActive = false;

        if (this.playerShieldEffect != null)
        {
            this.playerShieldEffect.Stop();
        }

        for(int i = petSwords.Count; i > 0; i--)
        {
            petSwords[i - 1].DestroyPet();
            petSwords.RemoveAt(i - 1);
        }
    }

    public bool IsInvulnerable()
    {
        return this.invulnerableTimer > 0;
    }

    public void HideSwordAndShieldEffect()
    {
        playerShieldEffect.hidden = true;
        for (int i = 0; i < playerShieldEffect.currentShields.Count; i++)
        {
            playerShieldEffect.currentShields[i].gameObject.SetActive(false);
        }
        for (int i = petSwords.Count; i > 0; i--)
        {
            petSwords[i - 1].Hide();
        }
    }

    public void ShowSwordAndShieldEffect()
    {
        playerShieldEffect.hidden = false;

        for (int i = 0; i < playerShieldEffect.currentShields.Count; i++)
        {
            playerShieldEffect.currentShields[i].gameObject.SetActive(true);
        }

        for (int i = petSwords.Count; i > 0; i--)
        {
            petSwords[i - 1].Show();
        }
    }

    public IEnumerator DelayedActivation()
    {
        yield return 0;
        Activate();
    }
}

