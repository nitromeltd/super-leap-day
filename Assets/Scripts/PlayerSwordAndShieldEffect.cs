using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSwordAndShieldEffect : MonoBehaviour
{
    private Player player;

    public Animated.Animation animationInsideSpark;

    public GameObject shieldPrefab;

    public List<PlayerSwordAndShieldSprite> currentShields = new List<PlayerSwordAndShieldSprite>();
    private List<PlayerSwordAndShieldSprite> destroyingShields = new List<PlayerSwordAndShieldSprite>();

    public float timer;
    private float angle;

    public const float Period = .25f;
    public const float Amplitude = 2.5f;

    private const float RotateSpeed = 20f;

    public bool hidden = false;

    private bool ShouldAdvance()
    {
        if (currentShields.Count > 0) return true;
        if (destroyingShields.Count > 0) return true;

        return false;
    }

    public void Advance()
    {
        this.transform.position = GetTargetPosition();

        this.timer += Time.deltaTime;

        if(this.player.shield.isActive == false)
        {
            for (int i = 0; i < this.currentShields.Count; i++)
            {
                float extraTime = ((Period * 6f) / (this.currentShields.Count + this.player.swordAndShield.petSwords.Count)) * i;
                float finalTimer = timer + extraTime;

                this.currentShields[i].Move(this.transform.position, finalTimer, 90f);
            }
        }
        else
        {
            for (int i = 0; i < this.currentShields.Count; i++)
            {
                int shields = this.player.shield.currentActiveShields;
                float extraTime = ((Period * 6f) / (this.currentShields.Count + shields + this.player.swordAndShield.petSwords.Count)) * (i + shields);
                float finalTimer = timer + extraTime;

                this.currentShields[i].Move(this.transform.position, finalTimer, 90f);
            }
        }
        

        for (int i = this.destroyingShields.Count - 1; i >= 0; i--)
        {
            this.destroyingShields[i].Break(this.transform.position + Vector3.up * 2f);

            if (this.destroyingShields[i].destroyed == true)
            {
                this.destroyingShields.Remove(this.destroyingShields[i]);
            }
        }
    }

    private Vector2 GetTargetPosition()
    {
        return this.player.transform.position + (Vector3.up * .05f);
    }

    public void Play(Player player)
    {
        this.player = player;
    }

    public void Stop()
    {
        RemoveAllShields();

        // When player dies while still having shields left (e.g. getting squashed),
        // we handle the remaining graphics and animations with this:
        if (this.player.alive == false)
        {
            foreach (PlayerSwordAndShieldSprite shield in this.destroyingShields)
            {
                shield.BreakWithoutMoving();
            }

            this.destroyingShields.Clear();
        }
    }

    public void AddShields(int number)
    {
        for (int i = 0; i < number; i++)
        {
            AddShield();
        }
    }

    public void AddShield()
    {
        PlayerSwordAndShieldSprite shieldSprite = Instantiate(this.shieldPrefab).GetComponent<PlayerSwordAndShieldSprite>();
        shieldSprite.Init(this.player);
        shieldSprite.transform.position = GetTargetPosition() + (Vector2.up * 2f);

        this.currentShields.Add(shieldSprite);
    }

    private void RemoveAllShields()
    {
        int numberShield = this.currentShields.Count;

        for (int i = 0; i < numberShield; i++)
        {
            RemoveShield(i == numberShield - 1);
        }
    }

    public void RemoveShield(bool playAnimation = true)
    {
        PlayerSwordAndShieldSprite lastShieldSprite = currentShields[currentShields.Count - 1];
        lastShieldSprite.SetupBreak();
        lastShieldSprite.transform.SetParent(this.transform);

        this.currentShields.Remove(lastShieldSprite);
        this.destroyingShields.Add(lastShieldSprite);
    }
}
