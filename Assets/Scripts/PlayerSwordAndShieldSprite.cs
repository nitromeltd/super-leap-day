using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSwordAndShieldSprite : MonoBehaviour
{
    private Player player;

    public Sprite shieldFull;
    public Sprite[] shieldFrontSprite;
    public Sprite[] shieldBackSprite;
    public Sprite shieldDebrisLeft;
    public Sprite shieldDebrisRight;

    public Animated.Animation animationShieldDestroy;

    private bool spriteInFront;
    private float ySmoothVelocity;
    private Vector2 breakSmoothVelocity;
    [HideInInspector] public bool destroyed;

    private Animated animated;
    private SpriteRenderer spriteRenderer;

    public void Init(Player player)
    {
        this.player = player;
        this.animated = GetComponent<Animated>();
        this.spriteRenderer = GetComponent<SpriteRenderer>();

        this.spriteInFront = false;
        this.ySmoothVelocity = 0f;
        this.breakSmoothVelocity = Vector2.zero;
        this.destroyed = false;

        this.gameObject.layer = this.player.map.Layer();
    }

    public void Move(Vector3 center, float timer, float angle)
    {
        // POSITION
        float theta = timer / PlayerShieldEffect.Period;
        float distance = PlayerShieldEffect.Amplitude * Mathf.Sin(theta);

        Vector2 targetPos = center +
            ((Quaternion.AngleAxis(-angle, Vector3.forward) * Vector3.up) * distance);

        float ySmoothTargetPos = Mathf.SmoothDamp(
            this.transform.position.y,
            targetPos.y,
            ref ySmoothVelocity,
            .05f
        );

        Vector2 smoothTargetPos = new Vector2(targetPos.x, ySmoothTargetPos);

        bool shouldUseSmoothing =
            Vector2.Distance(this.transform.position, targetPos) > 0.1f;

        this.transform.position = shouldUseSmoothing ? smoothTargetPos : targetPos;


        // SPRITE ORDER
        if (this.spriteInFront == true && distance > PlayerShieldEffect.Amplitude - 0.1f)
        {
            this.spriteInFront = false;
        }

        if (this.spriteInFront == false && distance < -PlayerShieldEffect.Amplitude + 0.1f)
        {
            this.spriteInFront = true;
        }

        this.spriteRenderer.sortingLayerName = this.spriteInFront == true ? "Player (AL)" : "Player (BL)";
        this.spriteRenderer.sortingOrder = this.spriteInFront == true ? 10 : -10;


        // SPRITE ANIMATION
        float horizontalValue = Mathf.InverseLerp(
            center.x - PlayerShieldEffect.Amplitude,
            center.x + PlayerShieldEffect.Amplitude,
            targetPos.x
        );

        Sprite currentSprite = this.spriteInFront == true ?
            shieldFrontSprite[Mathf.RoundToInt(
                Mathf.Lerp(0, shieldFrontSprite.Length - 1, horizontalValue))] :
            shieldBackSprite[Mathf.RoundToInt(
                Mathf.Lerp(0, shieldBackSprite.Length - 1, horizontalValue))];

        this.spriteRenderer.sprite = currentSprite;
    }

    public void SetupBreak()
    {
        this.spriteRenderer.sprite = this.shieldFull;
        this.spriteRenderer.sortingOrder = 10;
    }

    public void Break(Vector3 destroyTargetPos)
    {
        if (this.destroyed == true) return;

        this.transform.position = Vector2.SmoothDamp(
            this.transform.position,
            destroyTargetPos,
            ref breakSmoothVelocity,
            .1f
        );


        if (Vector2.Distance(this.transform.position, destroyTargetPos) < 0.1f &&
            this.animated.currentAnimation != this.animationShieldDestroy)
        {
            this.animated.PlayOnce(this.animationShieldDestroy, ShowBreakParticles);
        }
    }

    public void BreakWithoutMoving()
    {
        this.animated.PlayOnce(this.animationShieldDestroy, ShowBreakParticles);
    }

    private void ShowBreakParticles()
    {
        var left = Particle.CreateWithSprite(
            this.shieldDebrisLeft, 100, this.transform.position, this.transform.parent
        );
        left.velocity = new Vector2(-1f, 7f) / 16f;
        left.acceleration = new Vector2(0f, -0.5f) / 16f;

        var right = Particle.CreateWithSprite(
            this.shieldDebrisRight, 100, this.transform.position, this.transform.parent
        );
        right.velocity = new Vector2(1f, 7f) / 16f;
        right.acceleration = new Vector2(0f, -0.5f) / 16f;

        this.destroyed = true;
        if(this.player.swordAndShield.isActive == false)
            this.player.swordAndShield.playerShieldEffect = null;
    }
}
