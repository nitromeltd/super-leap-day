using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWings : MonoBehaviour
{
    public Animated wings;

    public Animated.Animation animationWingFlap;
    public Animated.Animation animationWingExpand;
    public Animated.Animation animationWingShrink;
    public Animated.Animation animationWingRotation;
    public Animated.Animation animationWingIdleRun;
    public Animated.Animation animationWingSprint;
    public Animated.Animation animationWingDrop;

    public Animated.Animation animationFeatherParticles;

    private Player player;
    private Player.State playerStateLastFrame;

    private enum State
    {
        Idle,
        Run,
        Flap,
        Expand,
        Shrink,
        Rotate,
        Sprint,
        Drop,
        WallSlide,
        OnBamboo,
        IceSlide,
        GrabbingHook,
        GrabbingHookSide,
        GrabbingHookBottom,
        SproutSucking,
        YolkEgg,
        KingStompDrop,
    }
    private State state;

    private Vector3 offset;

    private float scale = 1;
    [NonSerialized] public SpriteRenderer spriteRenderer;

    public bool behindBamboo = false;
    private bool grabbing = false;
    private bool grabbingLastFrame = false;
    private bool running = false;
    private bool runningLastFrame = false;

    // Hearts
    public Animated.Animation heartSolidAnimation;
    public Animated.Animation heartDisappearAnimation;
    public Animated.Animation heartAppearAnimation;
    private Transform heartsParent;
    private Transform heart1;
    private Transform heart3;
    private Transform heart2;

    public void Init(Player player)
    {
        this.state = State.Idle;
        this.player = player;
        playerStateLastFrame = player.state;

        if (this.player is PlayerYolk)
        {
            offset = new Vector3(0, 0, 0);
        }
        else if(this.player is PlayerSprout)
        {
            offset = new Vector3(0.2f, 0, 0);
        }
        else if (this.player is PlayerPuffer)
        {
            offset = new Vector3(-0.2f, 0, 0);
        }
        else if (this.player is PlayerKing)
        {
            offset = new Vector3(-0.4f, 0.3f, 0);
            this.transform.localScale = new Vector3(1.2f, 1.2f, 0);
            scale = 1.2f;
        }
        else if (this.player is PlayerGoop)
        {
            offset = new Vector3(0.2f, 0.4f, 0);
        }
        else
        {
            offset = new Vector3(0, 0, 0);
        }

        CreateFeatherParticles(4);
        spriteRenderer = this.GetComponent<SpriteRenderer>();

        CheckState();

        if (this.player.midasTouch.isActive == true)
        {
            this.spriteRenderer.color = new Color(244, 214, 57);
        }

        heartsParent = this.transform.Find("Hearts").transform;
        heart1 = this.transform.Find("Hearts/1").transform;
        heart2 = this.transform.Find("Hearts/2").transform;
        heart3 = this.transform.Find("Hearts/3").transform;
        heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);

        if (this.player.infiniteJump.hearts == 3)
        {

        }
        else if (this.player.infiniteJump.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
        }
        else if (this.player.infiniteJump.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
        }
        else
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(false);
        }

        this.gameObject.layer = this.player.map.Layer();
        this.heartsParent.gameObject.layer = this.player.map.Layer();
        this.heart1.gameObject.layer = this.player.map.Layer();
        this.heart2.gameObject.layer = this.player.map.Layer();
        this.heart3.gameObject.layer = this.player.map.Layer();
        
        heartsParent.gameObject.SetActive(false);
    }

    public void Advance()
    {
        grabbing = player.grabbingOntoItem != null;
        running = player.animated.currentAnimation == player.animationRunningTooFast;
        CheckState();

        if (this.player.state == Player.State.Finished ||
            this.player.state == Player.State.InsideBox ||
            this.player.state == Player.State.InsideCityCannon ||
            this.player.state == Player.State.InsideLift ||
            this.player.state == Player.State.InsidePipe ||
            this.player.state == Player.State.InsideSpaceBooster ||
            this.player.state == Player.State.FireFromCityCannon ||
            this.player.state == Player.State.GoopBallSpinning ||
            this.player.state == Player.State.GoopBallTurningInPlace ||
            this.player.state == Player.State.KingHurt ||
            this.behindBamboo == true)

        {
            HideWings();
            heartsParent.gameObject.SetActive(false);
        }
        else
        {
            ShowWings();
        }

        if(player is PlayerGoop)
        {
            if (player.animated.currentAnimation == (player as PlayerGoop).animationSplat ||
            player.animated.currentAnimation == (player as PlayerGoop).animationWallSlideSplat)
            {
                HideWings();
                heartsParent.gameObject.SetActive(false);
            }
        }

        if (this.state == State.Flap || this.state == State.WallSlide)
        {
            this.transform.localScale = new Vector3(-scale, scale, 1);
        }
        else
        {
            this.transform.localScale = new Vector3(scale, scale, 1);
        }

        this.transform.localPosition = GetTargetPosition();

        if(this.state == State.Rotate)
        {
            if(wings.frameNumber >= 3)
            {
                this.spriteRenderer.sortingLayerName = "Default";
            }
            else
            {
                this.spriteRenderer.sortingLayerName = "Player (BL)";
                this.spriteRenderer.sortingOrder = 0;
            }
        }
        else
        {
            this.spriteRenderer.sortingLayerName = "Player (BL)";
            this.spriteRenderer.sortingOrder = 0;
        }

        playerStateLastFrame = player.state;
        grabbingLastFrame = grabbing;
        runningLastFrame = running;
    }

    private void CheckState()
    {
        if (playerStateLastFrame != player.state || grabbing != grabbingLastFrame || running != runningLastFrame)
        {
            if (player.state == Player.State.Jump)
            {
                this.wings.PlayOnce(this.animationWingExpand, () =>
                {
                    this.wings.PlayAndLoop(this.animationWingFlap);
                    this.state = State.Flap;
                });
                this.state = State.Expand;
            }
            else if (player.state == Player.State.WallSlide)
            {
                this.wings.PlayAndLoop(this.animationWingIdleRun);
                this.state = State.WallSlide;
            }
            else if (player.state == Player.State.DropVertically)
            {
                this.wings.PlayAndLoop(this.animationWingDrop);
                this.state = State.Drop;
            }
            else if (player.state == Player.State.TurningInPlace)
            {
                this.wings.PlayAndLoop(this.animationWingIdleRun);
                this.state = State.Idle;
            }
            else if (player.state == Player.State.SlidingOnIce)
            {
                this.wings.PlayAndLoop(this.animationWingIdleRun);
                this.state = State.IceSlide;
            }
            else if (player.state == Player.State.SlidingOnBamboo)
            {
                this.wings.PlayOnce(this.animationWingShrink, () =>
                {
                    this.wings.PlayAndLoop(this.animationWingIdleRun);
                });
                this.state = State.OnBamboo;
            }
            else if (player.state == Player.State.SproutSucking)
            {
                this.wings.PlayOnce(this.animationWingShrink, () =>
                {
                    this.wings.PlayAndLoop(this.animationWingIdleRun);
                });
                this.state = State.SproutSucking;
            }
            else if (player.state == Player.State.YolkLayingEgg)
            {
                this.wings.PlayOnce(this.animationWingShrink, () =>
                {
                    this.wings.PlayAndLoop(this.animationWingIdleRun);
                });
                this.state = State.YolkEgg;
            }
            else if (player.state == Player.State.KingStompDrop)
            {
                this.wings.PlayAndLoop(this.animationWingDrop);

                this.state = State.KingStompDrop;
            }
            else if (player.state == Player.State.Spring ||
                player.state == Player.State.SpringFromPushPlatform ||
                player.state == Player.State.Geyser)
            {
                this.wings.PlayAndLoop(this.animationWingRotation);
                this.state = State.Rotate;
            }
            else
            {
                if (player.animated.currentAnimation == player.animationRunningTooFast)
                {
                    this.wings.PlayAndLoop(this.animationWingSprint);
                    this.state = State.Run;
                }
                else if (player.grabbingOntoItem != null)
                {
                    if (player.grabbingOntoItem.grabInfo.grabType == Item.GrabType.LeftWall ||
                        player.grabbingOntoItem.grabInfo.grabType == Item.GrabType.RightWall)
                    {
                        this.state = State.GrabbingHookSide;
                    }
                    else if (player.grabbingOntoItem.grabInfo.grabType == Item.GrabType.Floor)
                    {
                        this.state = State.GrabbingHookBottom;
                    }
                    else
                    {
                        this.state = State.GrabbingHook;
                    }

                    this.wings.PlayOnce(this.animationWingShrink, () =>
                    {
                        this.wings.PlayAndLoop(this.animationWingIdleRun);
                    });
                }
                else if (playerStateLastFrame != Player.State.TurningInPlace)
                {
                    this.wings.PlayOnce(this.animationWingShrink, () =>
                    {
                        this.wings.PlayAndLoop(this.animationWingIdleRun);
                        this.state = State.Idle;
                    });
                    this.state = State.Shrink;
                }
            }
        }
    }

    private Vector2 GetTargetPosition()
    {
        this.transform.localRotation = Quaternion.Euler(0, 0, 0);

        if (this.state == State.Flap)
        {
            if (this.player is PlayerGoop)
            {
                if(player.animated.currentAnimation == player.animationJumping)
                {
                    return new Vector3(-1, 0.52f, 0);
                }
                else if(player.animated.currentAnimation == player.animationJumpingDown)
                {
                    if(player.animated.frameNumber == 0)
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -180);
                        return new Vector3(1.36f, 0.35f, 0);
                    }
                    else
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -180);
                        return new Vector3(1.36f, 0.35f, 0);
                    }
                }
                else if(player.animated.currentAnimation == (player as PlayerGoop).animationTransitionJumpToJumpDown)
                {
                    if (player.animated.frameNumber == 0)
                    {
                        return new Vector3(-0.72f, 0.6f, 0);
                    }
                    else if(player.animated.frameNumber == 1)
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -30);
                        return new Vector3(-0.38f, 1.35f, 0);
                    }
                    else if (player.animated.frameNumber == 2)
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -60);
                        return new Vector3(0f, 1.69f, 0);
                    }
                    else if (player.animated.frameNumber == 3)
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -95);
                        return new Vector3(0.58f, 1.54f, 0);
                    }
                    else
                    {
                        this.transform.localRotation = Quaternion.Euler(0, 0, -115);
                        return new Vector3(1f, 0.85f, 0);
                    }
                }
            }

            return new Vector3(-0.8f, 0, 0) + offset;
        }
        else if (this.state == State.Drop)
        {
            if(player is PlayerGoop)
            {
                if (player.animated.currentAnimation == (player as PlayerGoop).animationTransitionJumpToJumpDown)
                {
                    if (player.animated.frameNumber == 0)
                    {
                        return new Vector3(-0.29f, 1.07f, 0);
                    }
                    else if (player.animated.frameNumber == 1)
                    {
                        return new Vector3(0f, 1.67f, 0);
                    }
                    else if (player.animated.frameNumber == 2)
                    {
                        return new Vector3(0.1f, 1.8f, 0);
                    }
                    else if (player.animated.frameNumber == 3)
                    {
                        return new Vector3(0.58f, 1.54f, 0);
                    }
                    else
                    {
                        return new Vector3(0.94f, 0.55f, 0);
                    }
                }
            }

            return new Vector3(-.5f, 0.5f, 0) + offset;
        }
        else if (this.state == State.Expand || this.state == State.Shrink)
        {
            return new Vector3(-.5f, 0.2f, 0) + offset;
        }
        else if (this.state == State.WallSlide)
        {
            if (player as PlayerKing)
            {
                return new Vector3(0.75f, 0.25f, 0);
            }
            return new Vector3(1f, 0.4f, 0) - offset;
        }
        else if (this.state == State.OnBamboo)
        {
            if(player as PlayerPuffer)
            {
                return new Vector3(-1.2f, -0.5f, 0) + offset ;
            }
            else if (player as PlayerKing)
            {
                return new Vector3(-2.1f, -0.2f, 0);
            }
            else if (player as PlayerSprout)
            {
                return new Vector3(-1.35f, -0.54f, 0);
            }
            else if (player as PlayerGoop)
            {
                return new Vector3(-1.2f, 0f, 0);
            }
            else
            {
                return new Vector3(-1f, -0.5f, 0);
            }
        }
        else if (this.state == State.SproutSucking)
        {
            return new Vector3(-0.75f, -0.3f, 0) + offset;
        }
        else if (this.state == State.KingStompDrop)
        {
            return new Vector3(-0.19f, 1.4f, 0);
        }
        else if (this.state == State.YolkEgg)
        {
            if(player.animated.currentAnimation == (player as PlayerYolk).animationLayEggStart)
            {
                if (player.animated.frameNumber <= 7)
                {
                    return new Vector3(-0.83f, 0.11f, 0);
                }
                else if(player.animated.frameNumber == 8)
                {
                    return new Vector3(-0.75f, 0.7f, 0);
                }
                else if (player.animated.frameNumber == 9)
                {
                    return new Vector3(-0.73f, 1.24f, 0);
                }
                else if (player.animated.frameNumber == 10 ||
                    player.animated.frameNumber == 11)
                {
                    return new Vector3(-0.62f, 1.48f, 0);
                }
                else
                {
                    return new Vector3(-0.53f, 1.48f, 0);
                }
            }
            else if (player.animated.currentAnimation == (player as PlayerYolk).animationLayEggEnd)
            {
                if (player.animated.frameNumber == 0)
                {
                    return new Vector3(-0.26f, 1.35f, 0);
                }
                else if (player.animated.frameNumber == 1)
                {
                    return new Vector3(-0.05f, 1.1f, 0);
                }
                else
                {
                    return new Vector3(0.34f, 0.68f, 0);
                }
            }
            else
            {
                return new Vector3(-0.35f, 1.62f, 0);
            }
        }
        else if (this.state == State.Rotate)
        {
            if (player is PlayerGoop)
            {
                return new Vector3(0, .15f, 0);
            }
            return new Vector3(0, -0.4f, 0);
        }
        else if (this.state == State.IceSlide)
        {
            if(player is PlayerPuffer)
            {
                this.transform.localRotation = Quaternion.Euler(0, 0, 45);
                return new Vector3(-0.4f, -0.95f, 0);
            }
            else if (player is PlayerGoop)
            {
                return new Vector3(-0.55f, -0.34f, 0);
            }
            else if(player is PlayerKing)
            {
                this.transform.localRotation = Quaternion.Euler(0, 0, 22);
                return new Vector3(-0.95f, -0.55f, 0);
            }
            else if (player is PlayerSprout)
            {
                this.transform.localRotation = Quaternion.Euler(0, 0, 20);
                return new Vector3(-0.55f, -0.7f, 0);
            }
            return new Vector3(-0.7f, -0.34f, 0);
        }
        else if (this.state == State.GrabbingHook)
        {
            if (player is PlayerKing)
            {
                return new Vector3(-1.6f, -0.7f, 0);
            }
            else if (player is PlayerSprout)
            {
                return new Vector3(-0.73f, -0.13f, 0);
            }
            return new Vector3(-0.94f, -0.13f, 0);
        }
        else if (this.state == State.GrabbingHookSide)
        {
            if (player is PlayerKing)
            {
                return new Vector3(-1.7f, 0f, 0);
            }
            else if (player is PlayerSprout)
            {
                return new Vector3(-0.72f, -0.89f, 0);
            }
            else if (player is PlayerGoop)
            {
                return new Vector3(-1.3f, -0.56f, 0);
            }
            return new Vector3(-0.8f, 0.2f, 0);
        }
        else if (this.state == State.GrabbingHookBottom)
        {
            if (player is PlayerKing)
            {
                return new Vector3(-1.7f, .5f, 0);
            }
            else if (player is PlayerSprout)
            {
                return new Vector3(-0.9f, 0, 0);
            }
            else if (player is PlayerGoop)
            {
                return new Vector3(-1f, 0.2f, 0);
            }
            return new Vector3(-0.45f, 0.2f, 0);
        }
        else if (this.state == State.Run)
        {
            if(player is PlayerGoop)
            {
                return new Vector3(0.09f, .75f, 0);
            }
            else if (player is PlayerYolk)
            {
                return new Vector3(-.8f, 0.1f, 0) + offset;
            }
            else if (player is PlayerSprout)
            {
                return new Vector3(-.6f, 0.1f, 0);
            }
            return new Vector3(-0.5f, .1f, 0) + offset;
        }
        else
        {
            return new Vector3(-0.8f, .1f, 0) + offset;
        }
    }

    public void CreateFeatherParticles( int count)
    {
        for (var n = 0; n < count; n += 1)
        {
            Vector3 position = this.transform.position + new Vector3(UnityEngine.Random.Range(-0.25f, 0.25f), UnityEngine.Random.Range(-0.25f, 0.25f),1);
            var p = Particle.CreatePlayAndLoop(
                this.animationFeatherParticles,
                100,
                position,
                player.transform.parent
            );
            p.animated.frameNumber = UnityEngine.Random.Range(0, 18);
            p.velocity = new Vector2(
                UnityEngine.Random.Range(-0.3f, 0.3f) / 16,
                UnityEngine.Random.Range(-0.6f, -0.2f) / 16
            );
            p.acceleration = new Vector2(UnityEngine.Random.Range(-0.01f, 0.01f), -0.01f) / 16;
            p.spriteRenderer.sortingLayerName = "Player (AL)";
            p.spriteRenderer.sortingOrder = 100;
            
            if(this.player.midasTouch.isActive == true)
            {
                p.spriteRenderer.color = new Color(244, 214, 57);
            }
        }
    }

    public void Play(Player player)
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnAnimation,
            this.transform.position,
            this.transform
        );
        p.transform.localScale = Vector3.one * 0.4f;
        p.spriteRenderer.sortingLayerName = "Player (BL)";
        p.spriteRenderer.sortingOrder = 1;
        Init(player);

        this.wings.PlayAndLoop(this.animationWingIdleRun);
    }

    public void Stop()
    {
        DestroyWings();
    }

    public void HideWings()
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
    }

    public void ShowWings()
    {
        this.GetComponent<SpriteRenderer>().enabled = true;
    }

    public void LoseHeart()
    {
        StartCoroutine(_LoseHeart());
    }

    public IEnumerator _LoseHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.player.infiniteJump.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.player.infiniteJump.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.player.infiniteJump.hearts == 0)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.5f);

        Vector3 originalPosition;
        if (this.player.infiniteJump.hearts == 2)
        {
            heart2.gameObject.SetActive(true);
            heart1.gameObject.SetActive(true);
            originalPosition = heart3.transform.localPosition;
            heart3.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart3.transform.localPosition = originalPosition;
                heart3.gameObject.SetActive(false);
            });
        }
        else if (this.player.infiniteJump.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            originalPosition = heart2.transform.localPosition;
            heart2.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart2.transform.localPosition = originalPosition;
                heart2.gameObject.SetActive(false);
            });
        }
        else if (this.player.infiniteJump.hearts == 0)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            originalPosition = heart1.transform.localPosition;
            heart1.GetComponent<Animated>().PlayOnce(heartDisappearAnimation, () => {
                heart1.transform.localPosition = originalPosition;
                heart1.gameObject.SetActive(false);
            });
        }
        float timer = 0;
        while (timer < .5f)
        {
            timer += Time.deltaTime;
            if (this.player.infiniteJump.hearts == 2)
            {
                heart3.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.player.infiniteJump.hearts == 1)
            {
                heart2.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            else if (this.player.infiniteJump.hearts == 0)
            {
                heart1.transform.localPosition += new Vector3(0, 0.025f, 0);
            }
            yield return null;
        }

        yield return new WaitForSeconds(.5f);

        heartsParent.gameObject.SetActive(false);
    }

    public void GainHeart()
    {
        StartCoroutine(_GainHeart());
    }

    public IEnumerator _GainHeart()
    {
        heartsParent.gameObject.SetActive(true);
        if (this.player.infiniteJump.hearts == 3)
        {
            heart1.transform.localPosition = new Vector3(-0.5f, 0, 0);
            heart2.transform.localPosition = new Vector3(0, 0, 0);
            heart3.transform.localPosition = new Vector3(0.5f, 0, 0);
        }
        else if (this.player.infiniteJump.hearts == 2)
        {
            heart1.transform.localPosition = new Vector3(-0.25f, 0, 0);
            heart2.transform.localPosition = new Vector3(0.25f, 0, 0);
        }
        else if (this.player.infiniteJump.hearts == 1)
        {
            heart1.transform.localPosition = new Vector3(0f, 0, 0);
        }

        yield return new WaitForSeconds(.25f);

        if (this.player.infiniteJump.hearts == 3)
        {
            heart3.gameObject.SetActive(true);
            heart3.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart3.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.player.infiniteJump.hearts == 2)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(true);
            heart2.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart2.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
        }
        else if (this.player.infiniteJump.hearts == 1)
        {
            heart3.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart1.gameObject.SetActive(true);
            heart1.GetComponent<Animated>().PlayOnce(heartAppearAnimation, () => {
                heart1.GetComponent<Animated>().PlayOnce(heartSolidAnimation);
            });
        }

        yield return new WaitForSeconds(1f);
        heartsParent.gameObject.SetActive(false);
    }

    public void DestroyWings()
    {
        Particle p = Particle.CreateAndPlayOnce(
            Assets.instance.enemyRespawnAnimation,
            this.transform.position,
            this.transform
        );
        p.transform.localScale = Vector3.one * 0.4f;
        p.spriteRenderer.sortingLayerName = "Player (BL)";
        p.spriteRenderer.sortingOrder = 1;
        Destroy(this.gameObject);
    }
}
