using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rnd = UnityEngine.Random;

public class PlayerYolk : Player
{
    [Header("Character: Yolk")]
    public GameObject yolkEggPrefab;
    public GameObject yolkFlyingEggPrefab;

    public Animated.Animation animationLandToIdle;
    public Animated.Animation animationStandingToLayEgg;
    public Animated.Animation animationLayEggStart;
    public Animated.Animation animationLayEggIdle;
    public Animated.Animation animationLayEggEnd;
    public Animated.Animation animationLayEggShaking;

    [Header("Death & Respawn")]
    public Animated.Animation animationInsideEgg;
    public Sprite[] eggFragments;
    public Animated.Animation animationEggYolkParticle;

    public enum Power
    {
        Nothing,
        Stop,
        LayEgg,
        EggOnDeath
    }

    [HideInInspector] public bool insideEgg = false;
    private List<YolkEgg> layEggs = new List<YolkEgg>();
    private YolkEgg latestEgg = null;
    private FlyingYolkEgg flyingYolkEgg = null;
    private int stopHoldTimer = 0;
    private int timeSinceRespawn = 0;
    private int runPanicTime = 0;

    [HideInInspector] public Power power = Power.LayEgg;

    private static readonly int StopHoldTime = 15;
    private static readonly int MaxNumberEggCheckpoints = 1;
    private static readonly int MinTimeToCreateRespawnEgg = 10;
    private static readonly float FallingEggMaxSpeed = 0.75f;
    private static readonly float DefaultDistanceAboveCheckpoint = 6f;
    private static readonly float EggPositionOffsetX = 0.25f;

    public override void ChangeCharacterUpgradeLevel(int upgradeLevel)
    {
        int powerIndex = upgradeLevel - 1;
        this.power = (Power)powerIndex;
    }

    public void AdvanceRespawning()
    {
        if(IsFlyingEggActive() == false) return;

        this.position = this.flyingYolkEgg.position;
        
        if(this.flyingYolkEgg.canRespawnPlayer == true &&
            GameInput.Player(this).releasedJump == true)
        {
             var raycast = new Raycast(
                this.map, this.position, layer: this.layer, isCastByPlayer: true
            );

            if(WillBeCrushed(raycast) == false)
            {
                RespawnFromFlyingEgg();
            }
        }
    }

    public void HandleLayingEggInput()
    {
        this.timeSinceRespawn += 1;

        if(this.state == State.YolkLayingEgg)
        {
            this.stopHoldTimer = 0;

            if(GameInput.Player(this).releasedJump == true)
            {
                ExitLayingEggState();
            }
            else if(this.latestEgg != null &&
                Vector2.Distance(this.position, this.latestEgg.position) > 0.70f)
            {
                ExitLayingEggState();
            }
        }
        else
        {
            if(GameInput.Player(this).holdingJump == true &&
                this.groundState.onGround == false)
            {
                this.stopHoldTimer += 1;
            }

            if(GameInput.Player(this).releasedJump == true)
            {
                this.stopHoldTimer = 0;
            }

            if(this.stopHoldTimer >= StopHoldTime &&
                this.groundState.onGround == true &&
                this.jumpsSinceOnGround > 0 &&
                CanLayEgg())
            {
                switch(this.power)
                {
                    case Power.Stop:
                    {
                        EnterStop();
                    }
                    break;

                    case Power.LayEgg:
                    case Power.EggOnDeath:
                    {
                        EnterLayEgg();
                    }
                    break;
                }
            }
        }
    }

    private void ExitLayingEggState()
    {
        switch(this.power)
        {
            case Power.Stop:
            {
                ExitStop();
            }
            break;

            case Power.LayEgg:
            case Power.EggOnDeath:
            {
                ExitLayEgg();
            }
            break;
        }
    }

    private bool CanLayEgg()
    {
        Chunk playerChunk = this.map.NearestChunkTo(this.position);

        if(playerChunk.isTransitionalTunnel == true)
        {
            return false;
        }

        if(IsInsideWater == true)
        {
            return false;
        }

        return true;
    }

    private bool IsFlyingEggActive()
    {
        return this.flyingYolkEgg != null;
    }

    private void EnterLayEgg()
    {
        this.state = State.YolkLayingEgg;

        Audio.instance.PlaySfx(Assets.instance.sfxYolkEggInit);

        this.animated.PlayOnce(this.animationLayEggStart, () =>
        {
            if(this.state != State.YolkLayingEgg) return;

            this.animated.PlayAndLoop(this.animationLayEggShaking);
            CreateYolkEgg();

            if(this.gravity.IsZeroGravity() == true)
            {
                ExitLayingEggState();
            }
        });
        this.animated.OnFrame(9, () =>
        {
            Audio.instance.PlaySfx(Assets.instance.sfxYolkEggGrow);
        });
    }

    private void ExitLayEgg()
    {
        if(this.groundState.onGround == true)
        {
            this.state = State.Normal;
        }
        else
        {
            this.state = State.Jump;
        }

        if(this.animated.currentAnimation == this.animationLayEggShaking)
        {
            this.position = this.position + ((Vector2)this.transform.up * 1.50f);
        }

        if(IsCheckpointEggActive())
        {
            GetCurrentEgg().SeparateFromPlayer();
        }

        this.latestEgg = null;
    }

    private void EnterStop()
    {
        this.state = State.YolkLayingEgg;

        this.animated.PlayOnce(this.animationLandToIdle, () => 
        {
            this.animated.PlayAndLoop(this.animationStanding);
        });
    }

    private void ExitStop()
    {
        this.state = State.Normal;
    }

    public void HandleLayingEggState(Raycast raycast)
    {
        var a = Probe(raycast, -ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
        var b = Probe(raycast, +ABCDSensorOffsetX, -0.5f, 0, -1, ABSensorLength - 0.5f);
        var sensor = Raycast.CombineClosest(a, b);

        this.velocity = Vector2.zero;

        if (sensor.tiles.Length > 0)
            this.velocity = sensor.tiles[0].tileGrid.effectiveVelocity;
            
        this.position += this.velocity;
    }

    public override void Die()
    {
        base.Die();

        this.stopHoldTimer = 0;
    }

    protected override void RespawnFX()
    {
        if(CanRespawnFromFlyingEgg())
        {
            this.position = this.transform.position;
            Chunk playerChunk = this.map.NearestChunkTo(this.position);

            {
                var obj = Instantiate(this.yolkFlyingEggPrefab, playerChunk.transform);
                obj.name = "Spawned Yolk Respawning Egg";
                this.flyingYolkEgg = obj.GetComponent<FlyingYolkEgg>();
                this.flyingYolkEgg.Init(position, playerChunk);
                playerChunk.items.Add(this.flyingYolkEgg);
            }
        }
        else
        {
            this.position = IsCheckpointEggActive() ? 
                GetCheckpointPosition() : this.position;

            PlayerDeathParticles.Life(
                this,
                new Color(150 / 255.0f, 226 / 255.0f, 44 / 255.0f),
                this.position, 
                delegate
            {
                if(IsCheckpointEggActive() == true)
                {
                    OpenCheckpointEgg();
                }
                else
                {
                    StartCoroutine(RespawnFromEggAnimation(
                        initialPosition: GetCheckpointPosition(),
                        checkForGround: false)
                    );
                }
            });
        }

        this.timeSinceRespawn = 0;
    }

    private bool CanRespawnFromFlyingEgg()
    {
        if(this.power != Power.EggOnDeath)
        {
            return false;
        }

        if(this.timeSinceRespawn < MinTimeToCreateRespawnEgg)
        {
            return false;
        }

        return true;
    }

    private void RespawnFromFlyingEgg()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxPlayerRespawn);
        Audio.instance.StopSfxLoop(Assets.instance.sfxYolkEggRespawnSparkle);
        Audio.instance.StopSfxLoop(Assets.instance.sfxYolkEggRespawnFlap);
        this.position = this.flyingYolkEgg.position;
        this.flyingYolkEgg.Hide();
        this.flyingYolkEgg = null;
        StartCoroutine(RespawnFromEggAnimation(initialPosition: this.position, checkForGround: true));
    }
    
    protected IEnumerator RespawnFromEggAnimation(Vector2 initialPosition, bool checkForGround)
    {
        {
            float extraHeight = 1f;
            var raycast = new Raycast(this.map, initialPosition, isCastByPlayer: true, stopAtPortals: true);
            var result = raycast.Vertical(initialPosition, false, 20f);
            float resultDistance = result.distance;
            Vector2 resultEnd = result.End();

            // stop egg from clipping through horizontal chunks
            Chunk playerChunk = this.map.NearestChunkTo(this.position);
            if(checkForGround == true && playerChunk.isHorizontal == true)
            {
                if(resultEnd.y < playerChunk.yMin)
                {
                    resultDistance -= (playerChunk.yMin - resultEnd.y);
                    resultEnd.y = playerChunk.yMin;
                }
            }

            Vector2 endPosition = checkForGround ?
                resultEnd + (Vector2.up * extraHeight) : initialPosition;

            float above = checkForGround ? (resultDistance - extraHeight) : DefaultDistanceAboveCheckpoint;
            float speed = 0;

            this.spriteRenderer.enabled = true;
            this.insideEgg = true;
            this.position = endPosition.Add(0, above);
            this.velocity = Vector2.zero;
            yield return new WaitForEndOfFrame();

            for (int n = 0; n < 8; n += 1)
                yield return new WaitForEndOfFrame();

            while (above > 0)
            {
                speed -= 0.5f / 16f;
                above += speed;

                if (above < 0) above = 0;

                if(Mathf.Abs(speed) > FallingEggMaxSpeed)
                {
                    speed = FallingEggMaxSpeed * Mathf.Sign(speed);
                }

                this.position = endPosition.Add(0, above);
                yield return new WaitForEndOfFrame();
            }
        }

        this.insideEgg = false;
        this.animated.PlayOnce(this.animationRespawn, delegate
        {
            this.state = State.Normal;
        });

        var p = Particle.CreateWithSprite(
            this.eggFragments[0], 100, this.position, this.transform.parent
        );
        p.velocity = new Vector2(
            -1.5f + Rnd.Range(-0.5f, 0.5f),
            5     + Rnd.Range(-0.5f, 0.5f)
        ) / 16;
        p.acceleration = new Vector2(0, -0.5f) / 16;

        p = Particle.CreateWithSprite(
            this.eggFragments[1], 100, this.position, this.transform.parent
        );
        p.velocity = new Vector2(
            1.5f + Rnd.Range(-0.5f, 0.5f),
            6    + Rnd.Range(-0.5f, 0.5f)
        ) / 16;
        p.acceleration = new Vector2(0, -0.5f) / 16;

        for (var n = 0; n < 5; n += 1)
        {
            var angle = Rnd.Range(0f, Util.Tau);
            var speed = Rnd.Range(1.5f, 2.5f) / 16;
            p = Particle.CreateAndPlayOnce(
                this.animationEggYolkParticle, this.position, this.transform.parent
            );
            p.velocity = new Vector2(
                Mathf.Cos(angle) * speed,
                Mathf.Sin(angle) * speed
            );
            p.acceleration = p.velocity * -0.03f / 16;
        }
    }

    private void CreateYolkEgg()
    {
        Audio.instance.PlaySfx(Assets.instance.sfxYolkEggBomb);

        if (this.layEggs.Count >= MaxNumberEggCheckpoints)
        {
            YolkEgg oldEgg = this.layEggs[0];
            oldEgg.Break();
        }

        Chunk playerChunk = this.map.NearestChunkTo(this.position);
        Vector2 finalEggPosition = this.position + 
            (Forward() * (this.facingRight ? -EggPositionOffsetX : EggPositionOffsetX));

        var obj = Instantiate(this.yolkEggPrefab, playerChunk.transform);
        obj.name = "Spawned Yolk Egg";
        this.latestEgg = obj.GetComponent<YolkEgg>();
        this.latestEgg.Init(finalEggPosition, playerChunk);
        playerChunk.items.Add(this.latestEgg);

        this.layEggs.Add(this.latestEgg);
    }

    private YolkEgg GetCurrentEgg()
    {
        if(this.layEggs.Count <= 0) return null;

        return this.layEggs[this.layEggs.Count - 1];
    }

    private void SetupCheckpointEgg()
    {
        this.position = GetCheckpointPosition();
        this.spriteRenderer.enabled = true;
        this.GetCurrentEgg().Shake();
    }

    private void OpenCheckpointEgg()
    {
        this.spriteRenderer.enabled = true;

        GetCurrentEgg().Break();

        this.animated.PlayOnce(this.animationRespawn, delegate
        {
            this.state = State.Normal;
        });        
    }

    public override void SetCheckpointPosition(Vector2 position)
    {
        base.SetCheckpointPosition(position);

        for (int i = this.layEggs.Count - 1; i >= 0; i--)
        {
            this.layEggs[i].Break();
        }
    }
    
    public override Vector2 GetCheckpointPosition()
    {
        if(IsCheckpointEggActive())
        {
            return GetCurrentEgg().GetCheckpointPos();
        }

        return base.GetCheckpointPosition();
    }

    private bool IsCheckpointEggActive()
    {
        if(this.power == Power.Nothing) return false;

        return GetCurrentEgg() != null;
    }

    public void RemoveCheckpointEgg(YolkEgg egg)
    {
        this.layEggs.Remove(egg);
    }

    protected override void PlayRunning()
    {
        this.runPanicTime += 1;

        if(this.runPanicking == true && this.runPanicTime > 80)
        {
            this.runPanicking = false;
            this.runPanicTime = 0;
        }

        if (this.runPanicking == true)
            this.animated.PlayAndLoop(this.animationRunningPanicked);
        else
            this.animated.PlayAndLoop(this.animationRunning);
    }

    public void TriggerPanic()
    {
        if (this.runPanicTime < 80) return;

        this.runPanicking = !this.runPanicking;
        this.runPanicTime = 0;
        
        Audio.instance.PlayRandomSfx(audioClips: Assets.instance.sfxYolkPanic);
    }

    protected override void PlayWallSlide()
    {
        bool enteringWallSlide =
            this.animated.currentAnimation != this.animationWallSliding;
        this.animated.PlayAndLoop(this.animationWallSliding);

        if(enteringWallSlide == true)
        {
            this.animated.frameNumber = Util.RandomChoice(0, 12, 24, 36);
        }
    }
}
