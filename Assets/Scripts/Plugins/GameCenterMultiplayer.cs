using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

/* If you ever see Il2CppExceptionWrapper in Xcode, what that means is that
   the Objective-C++ layer called a callback into the C# world and then the
   C# code threw an exception and didn't catch it. */

public class GameCenterMultiplayer
{
    public class Msg {}
    public class MsgConnectionStart : Msg
    {
        public DateTime levelDate;
        public int matchId;
        public int playerNumberOfReceivingDevice;
    }
    public class MsgConnectionWelcomeBack : Msg
    {
        public DateTime levelDate;
        public int matchId;
    }
    public class MsgCharacterSelectionConfirmed : Msg
    {
    }
    public class MsgHereIAm : Msg
    {
        public Character? character;
        public float x, y;
        public float xScale, rotation;
        public GhostPlayer.SpriteData[] sprites;
    }
    public class MsgIntentionallyLeaving : Msg
    {
    }
    public class MsgMultiplayerPowerup : Msg
    {
        public PowerupPickup.Type type;
        public float x, y;
        public int chunkIndex;
    }
    public class MsgEnterGoldTrophy : Msg
    {
    }
    public class MsgTimeLeft : Msg
    {
        public int timeLeftInFrames;
    }

    public enum ConnectionState { Unknown = 0, Connected = 1, Disconnected = 2 }

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void GCMultiplayer_Init(
        CallbackMatchmakerDidFindMatch         callbackMatchmakerDidFindMatch,
        CallbackMatchmakerDidFailWithError     callbackMatchmakerDidFailWithError,
        CallbackMatchmakerPopupCancelled       callbackMatchmakerPopupCancelled,
        CallbackPlayerDidAcceptInvite          callbackPlayerDidAcceptInvite,
        CallbackPlayerDidChangeConnectionState callbackPlayerDidChangeConnectionState,
        CallbackReceivedDataFromPlayer         callbackReceivedDataFromPlayer
    );
    [DllImport("__Internal")]
    private static extern void GCMultiplayer_PresentMatchmaker();
    [DllImport("__Internal")]
    private static extern void GCMultiplayer_SendData(byte[] data, int length, bool reliable);
    [DllImport("__Internal")]
    private static extern void GCMultiplayer_SendDataToPlayer(string toPlayerId, byte[] data, int length, bool reliable);
    [DllImport("__Internal")]
    private static extern void GCMultiplayer_End();
#endif

    public delegate void CallbackMatchmakerDidFindMatch(string thisPlayerId, string remotePlayerIds);
    public delegate void CallbackMatchmakerDidFailWithError();
    public delegate void CallbackMatchmakerPopupCancelled();
    public delegate void CallbackPlayerDidAcceptInvite(string playerId);
    public delegate void CallbackPlayerDidChangeConnectionState(string playerId, ConnectionState state);
    public delegate void CallbackReceivedDataFromPlayer(string playerId, IntPtr data, int length);

    public static bool IsActive => match != null;
    public static int PlayerNumber => match?.thisPlayerNumber ?? 1;
    public static bool ThisPlayerReachedTheGoldTrophyFirst => match?.thisPlayerReachedTheGoldTrophyFirst ?? false;

    private class Match
    {
        public int id;
        public bool isPlayerWhoInitiatedMatch;
        public int thisPlayerNumber; /* 1-based */
        public string thisPlayerId;
        public string[] remotePlayerIds;
        public int numberOfPlayers; /* including this player */
        public bool isDelayingMessagesWhileSceneSwitches;
        public bool hasBegun;
        public bool thisPlayerHasConfirmedCharacter;
        public bool thisPlayerReachedTheGoldTrophyFirst;
        public HashSet<string> remotePlayersWhoHaveConfirmedCharacter = new();
        public HashSet<string> remotePlayersWhoHaveConnectionProblems = new();
        public HashSet<string> remotePlayersWhoHaveLeftIntentionally = new();
        public HashSet<string> remotePlayersWhoHaveEnteredGoldTrophy = new();
        public List<(string fromPlayerId, Msg msg)> messagesReceivedWaitingForProcessing = new();
        public Dictionary<string, Vector2> lastRecordedPositionOfPlayer = new();
    }
    private static Match match;
    private static List<Msg> messagesWaitingForLoopback = new List<Msg>();

    public static void Init()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            GCMultiplayer_Init(
                NotifyMatchmakerDidFindMatch,
                NotifyMatchmakerDidFailWithError,
                NotifyMatchmakerPopupCancelled,
                NotifyPlayerDidAcceptInvite,
                NotifyPlayerDidChangeConnectionState,
                NotifyReceivedDataFromPlayer
            );
        }
        #endif
    }

    public static void PresentMatchmaker()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            match = new Match();
            match.id = UnityEngine.Random.Range(0, 100_000_000);
            match.isPlayerWhoInitiatedMatch = true;
            match.thisPlayerNumber = 1;
            match.messagesReceivedWaitingForProcessing.Clear();
            GCMultiplayer_PresentMatchmaker();
        }
        #endif
    }

    public static void NotifyMatchHasBegun()
    {
        if (match != null)
            match.hasBegun = true;
    }

    public static void NotifyLeavingGameScene()
    {
        if (match?.isDelayingMessagesWhileSceneSwitches == false)
        {
            EndMultiplayerGame();
        }
    }

    public static void EndMultiplayerGame()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            var msg = new MsgIntentionallyLeaving {};
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);

            match = null;
            GCMultiplayer_End();

            Game.instance?.map1?.CreateGhostPlayers();
        }
        #endif
    }
    
    public static void NotifyCharacterSelectionConfirmed()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            var msg = new MsgCharacterSelectionConfirmed {};
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);
        }
        #endif
    }

    public static void SendMultiplayerPowerup(PowerupPickup.Type type, Vector2 position, Chunk chunk)
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            var msg = new MsgMultiplayerPowerup
            {
                type = (PowerupPickup.Type)type,
                x = position.x,
                y = position.y,
                chunkIndex = chunk.index
            };
            
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);
        }
        #endif
    }

    public static bool HasEveryoneElseConfirmedCharacter()
    {
        if (match == null)
            return false;

        return
            match.remotePlayersWhoHaveConfirmedCharacter.Count ==
            match.numberOfPlayers - 1;
    }

    public static bool HasAnyoneLeftIntentionally()
    {
        if (match == null)
            return false;

        return match.remotePlayersWhoHaveLeftIntentionally.Any();
    }

    public static bool HasRemotePlayerReachedTheGoldTrophy(string playerId)
    {
        if (match == null)
            return false;

        return match.remotePlayersWhoHaveEnteredGoldTrophy.Contains(playerId);
    }

    public static bool HasAnyRemotePlayerReachedTheGoldTrophy()
    {
        if (match == null)
            return false;

        return match.remotePlayersWhoHaveEnteredGoldTrophy.Count > 0;
    }

    public static bool HaveAllRemotePlayersReachedTheGoldTrophy()
    {
        if (match == null)
            return false;

        return
            match.remotePlayersWhoHaveEnteredGoldTrophy.Count ==
            match.numberOfPlayers - 1;
    }

    public static string OnlyOneRemotePlayerStillPlaying()
    {
        int count = 0;
        string result = null;

        foreach (var playerId in match.remotePlayerIds)
        {
            if (match.remotePlayersWhoHaveEnteredGoldTrophy.Contains(playerId))
                continue;

            count += 1;
            result = playerId;
        }

        return (count == 1) ? result : null;
    }

    public static void NotifyEnterGoldTrophy()
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            var msg = new MsgEnterGoldTrophy {};
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);

            if (HasAnyRemotePlayerReachedTheGoldTrophy() == false)
                match.thisPlayerReachedTheGoldTrophyFirst = true;
        }
        #endif
    }

    public static void NotifyTimeLeftInFrames(int timeLeftInFrames)
    {
        #if UNITY_IOS && !UNITY_EDITOR
        {
            var msg = new MsgTimeLeft
            {
                timeLeftInFrames = timeLeftInFrames
            };
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);
        }
        #endif
    }

    public static void Advance()
    {
        if (match == null) return;
        if (match.hasBegun == false && match.remotePlayersWhoHaveLeftIntentionally.Any())
            return;

        #if UNITY_IOS && !UNITY_EDITOR
        {
            var player = Game.instance.map1.player;
            var msg = new MsgHereIAm
            {
                character = player.Character(),
                x = player.position.x,
                y = player.position.y,
                xScale = player.transform.localScale.x,
                rotation = player.transform.rotation.eulerAngles.z,
                sprites = GhostPlayer.GetGhostSpriteData(player)
            };

            var headset = player.grabbingOntoItem as MultiplayerHeadset;
            var room = Game.instance.map1.AnyItem<MultiplayerRoom>();
            if (room.HasCharacterBeenSelected() == false)
                msg.character = null;
            else if (headset != null)
                msg.character = room.SelectedCharacter;

            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, false);

            if (match.isDelayingMessagesWhileSceneSwitches == false)
            {
                foreach (var entry in match.messagesReceivedWaitingForProcessing)
                {
                    ProcessReceivedMsg(entry.fromPlayerId, entry.msg);
                }
                match.messagesReceivedWaitingForProcessing.Clear();
            }

            SaveData.SetSavedGamekitMultiplayerState(match.id, player.position);
        }
        #endif
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackMatchmakerDidFindMatch))]
    private static void NotifyMatchmakerDidFindMatch(string thisPlayerId, string remotePlayerIds)
    {
        /* To keep interop complexity small, the "remotePlayerIds" parameter
           is all of the remote player ids concatenated together,
           separated by semicolons. */

        #if UNITY_IOS && !UNITY_EDITOR
        {
            match.thisPlayerId = thisPlayerId;
            match.remotePlayerIds = remotePlayerIds.Split(";");
            match.numberOfPlayers = match.remotePlayerIds.Length + 1;

            if (match?.isPlayerWhoInitiatedMatch == true)
            {
                var room = Game.instance.map1.AnyItem<MultiplayerRoom>();
                var player = Game.instance.map1.player;
                bool isLeftHeadset = player.grabbingOntoItem == room.headsets[0];

                for (int n = 0; n < match.remotePlayerIds.Length; n += 1)
                {
                    var msg = new MsgConnectionStart
                    {
                        levelDate = Game.selectedDate.Date,
                        matchId = match.id,
                        playerNumberOfReceivingDevice = n + 2
                    };
                    var bytes = MsgToBytes(msg);
                    GCMultiplayer_SendDataToPlayer(match.remotePlayerIds[n], bytes, bytes.Length, true);
                }

                room.NotifyWeHaveJoinedGameIOS(1);
            }
        }
        #endif
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackMatchmakerDidFailWithError))]
    private static void NotifyMatchmakerDidFailWithError()
    {
        var room = Game.instance?.map1?.AnyItem<MultiplayerRoom>();
        room?.NotifyMatchmakerPopupWasCancelledIOS();
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackMatchmakerPopupCancelled))]
    private static void NotifyMatchmakerPopupCancelled()
    {
        var room = Game.instance?.map1?.AnyItem<MultiplayerRoom>();
        room?.NotifyMatchmakerPopupWasCancelledIOS();
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackPlayerDidAcceptInvite))]
    private static void NotifyPlayerDidAcceptInvite(string playerId)
    {
        /* we might be player 2, 3, or 4. this will be overwritten in a moment
           when a MsgConnectionStart comes in. */
        match = new Match();
        match.isPlayerWhoInitiatedMatch = false;
        match.thisPlayerNumber = 2;
        match.thisPlayerId = playerId;
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackPlayerDidChangeConnectionState))]
    private static void NotifyPlayerDidChangeConnectionState(string playerId, ConnectionState state)
    {
        #if UNITY_IOS && !UNITY_EDITOR

        if (SceneManager.GetActiveScene().name != "Game") return;
        if (match == null) return;
        if (match.remotePlayerIds.Contains(playerId) == false) return;

        var map = Game.instance.map1;
        if (map == null) return;

        var ghostPlayer = map.ghostPlayers.FirstOrDefault(
            g => g.trackingMultiplayerPlayerId == playerId
        );
        ghostPlayer?.NotifyConnectionStateChanged(state == ConnectionState.Connected);

        if (state == ConnectionState.Connected &&
            match.lastRecordedPositionOfPlayer.ContainsKey(playerId) == true)
        {
            var msg = new MsgConnectionWelcomeBack
            {
                matchId = match.id,
                levelDate = Game.selectedDate.Date
            };
            var bytes = MsgToBytes(msg);
            GCMultiplayer_SendData(bytes, bytes.Length, true);
        }

        if (state == ConnectionState.Connected)
            match.remotePlayersWhoHaveConnectionProblems.Remove(playerId);
        else
            match.remotePlayersWhoHaveConnectionProblems.Add(playerId);

        if (match.numberOfPlayers == 2)
        {
            if (
                state == ConnectionState.Connected ||
                match.hasBegun == false ||
                HaveAllRemotePlayersReachedTheGoldTrophy() == true ||
                Game.instance.map1.finished == true
            )
            {
                IngameUI.instance.multiplayerConnectionProblemsPanel.Hide();
            }
            else
            {
                IngameUI.instance.multiplayerConnectionProblemsPanel.Show();
            }
        }
        else
        {
            /*
                There doesn't seem to be any point in showing the "connection problems"
                popup for 3P/4P games. The whole point is to wait for players to
                reconnect, but they would presumably be doing this via the
                match:shouldReinviteDisconnectedPlayer: mechanism, which is only
                called for 2P matches anyway.

                In a 3P/4P game, we just hide the disconnected players and continue
                on. We still need to know which players are disconnected because we'll
                be adjusting the logic of when the match ends so we don't get softlocked.
            */

            IngameUI.instance.multiplayerConnectionProblemsPanel.Hide();

            if (state != ConnectionState.Connected)
            {
                // if someone has disconnected, we need to recheck whether there's
                // still a game to finish. if only one person is left, the timer starts.

                match.remotePlayerIds =
                    match.remotePlayerIds.Where(id => id != playerId).ToArray();
                match.numberOfPlayers = 1 + match.remotePlayerIds.Count();

                Game.instance.multiplayerLastPlayerCutoff.NotifyRemotePlayerHasFinished();
            }
        }

        #endif
    }

    [AOT.MonoPInvokeCallback(typeof(CallbackReceivedDataFromPlayer))]
    private static void NotifyReceivedDataFromPlayer(string fromPlayerId, IntPtr ptr, int length)
    {
        if (match == null) return;

        byte[] bytes = new byte[length];
        Marshal.Copy(ptr, bytes, 0, length);
        var msg = BytesToMsg(bytes);
        match.messagesReceivedWaitingForProcessing.Add((fromPlayerId, msg));

        if (msg is MsgConnectionStart)
        {
            var m = msg as MsgConnectionStart;

            match.thisPlayerNumber = m.playerNumberOfReceivingDevice;

            if (match.isPlayerWhoInitiatedMatch == false)
            {
                Game.selectedDate = m.levelDate;
                Transition.GoToGame();
                GameCenter.HideAccessPoint();

                var themed = Transition.instance as TransitionThemed;
                if (themed != null)
                {
                    match.isDelayingMessagesWhileSceneSwitches = true;
                    themed.actionWhenSwitchedToGameScene = () => {
                        match.isDelayingMessagesWhileSceneSwitches = false;
                    };
                }
            }
        }
    }

    private static void ProcessReceivedMsg(string fromPlayerId, Msg msg)
    {
        var map = Game.instance?.map1;
        var room = map?.AnyItem<MultiplayerRoom>();
        if (map == null || room == null)
        {
            // this function is only supposed to be called when we are in the game
            // and ready to act on these messages. this check is just in case.
            return;
        }
        if (match.remotePlayerIds.Contains(fromPlayerId) == false)
            return;

        if (msg is MsgConnectionStart)
        {
            var m = msg as MsgConnectionStart;

            match.id = m.matchId;
            match.thisPlayerNumber = m.playerNumberOfReceivingDevice;
            room.NotifyWeHaveJoinedGameIOS(match.thisPlayerNumber);
        }
    
        else if (msg is MsgConnectionWelcomeBack)
        {
            var m = msg as MsgConnectionWelcomeBack;
            var previousInfo = SaveData.GetSavedGamekitMultiplayerState();

            match.id = m.matchId;

            if (m.matchId == previousInfo?.matchId)
            {
                var player = Game.instance.map1.player;
                player.position = previousInfo.Value.position;
                player.velocity = Vector2.zero;
                player.state = Player.State.Normal;
                player.InsideEntranceLift = null;
            }
        }

        else if (msg is MsgHereIAm)
        {
            var m = msg as MsgHereIAm;

            match.lastRecordedPositionOfPlayer[fromPlayerId] = new Vector2(m.x, m.y);

            var ghostPlayer = map.ghostPlayers.FirstOrDefault(
                g => g.trackingMultiplayerPlayerId == fromPlayerId
            );
            if (ghostPlayer == null)
            {
                ghostPlayer = GhostPlayer.Make(map, fromPlayerId);
                map.ghostPlayers.Add(ghostPlayer);
            }
            ghostPlayer.NotifyReceivedMessage(m);
        }

        else if (msg is MsgMultiplayerPowerup)
        {
            var m = msg as MsgMultiplayerPowerup;
            Vector2 position = new Vector2(m.x, m.y);
            
            Chunk chunk = map.chunks[m.chunkIndex];

            switch (m.type)
            {
                case PowerupPickup.Type.MultiplayerBaseball:
                default:
                {
                    PetBaseball.CreateP2(position, chunk);
                }
                break;

                case PowerupPickup.Type.MultiplayerInk:
                {
                    PetPainter.CreateP2(position, chunk);
                }
                break;

                case PowerupPickup.Type.MultiplayerMines:
                {
                    PetMine.Create(position, chunk);
                }
                break;
            }
        }

        else if(msg is MsgEnterGoldTrophy)
        {
            match.remotePlayersWhoHaveEnteredGoldTrophy.Add(fromPlayerId);

            Game.instance.multiplayerLastPlayerCutoff.NotifyRemotePlayerHasFinished();
        }

        else if(msg is MsgTimeLeft)
        {
            var m = msg as MsgTimeLeft;
            Game.instance.multiplayerLastPlayerCutoff
                .NotifyRemotePlayerHasTimeLeftInFrames(m.timeLeftInFrames);
        }

        else if (msg is MsgIntentionallyLeaving)
        {
            match.remotePlayersWhoHaveLeftIntentionally.Add(fromPlayerId);

            if (match.hasBegun == true && match.numberOfPlayers > 2)
            {
                match.numberOfPlayers -= 1;
                match.remotePlayerIds =
                    match.remotePlayerIds.Where(id => id != fromPlayerId).ToArray();

                var ghost =
                    Game.instance.map1.ghostPlayers
                    .Where(g => g.trackingMultiplayerPlayerId == fromPlayerId)
                    .FirstOrDefault();
                if (ghost != null)
                    ghost.NotifyConnectionStateChanged(false);
            }
            else if (match.hasBegun == true)
            {
                EndMultiplayerGame();
            }
        }
        
        else if (msg is MsgCharacterSelectionConfirmed)
        {
            match.remotePlayersWhoHaveConfirmedCharacter.Add(fromPlayerId);
        }

        room.NotifyOtherPlayerIsActiveIOS();
    }

    private static byte[] MsgToBytes(Msg msg)
    {
        var list = new List<byte>();

        void WriteInt(int i)
        {
            unchecked
            {
                list.Add((byte)i);
                list.Add((byte)(i >> 8));
                list.Add((byte)(i >> 16));
                list.Add((byte)(i >> 24));
            }
        }
        void WriteFloat(float f)
        {
            WriteInt((int)(f * 256));
        }
        void WriteString(string s)
        {
            if (s == null)
            {
                WriteInt(0);
                return;
            }
            WriteInt(s.Length);
            for (int n = 0; n < s.Length; n += 1)
                list.Add((byte)s[n]);
        }

        if (msg is MsgConnectionStart)
        {
            var m = msg as MsgConnectionStart;
            WriteInt(1);
            WriteInt(m.matchId);
            WriteInt(m.levelDate.DayOfYear);
            WriteInt(m.levelDate.Year);
            WriteInt(m.playerNumberOfReceivingDevice);
        }
        else if (msg is MsgConnectionWelcomeBack)
        {
            var m = msg as MsgConnectionWelcomeBack;
            WriteInt(2);
            WriteInt(m.matchId);
            WriteInt(m.levelDate.DayOfYear);
            WriteInt(m.levelDate.Year);
        }
        else if (msg is MsgCharacterSelectionConfirmed)
        {
            var m = msg as MsgCharacterSelectionConfirmed;
            WriteInt(3);
        }
        else if (msg is MsgHereIAm)
        {
            var m = msg as MsgHereIAm;
            WriteInt(4);
            WriteInt(m.character.HasValue ? 1 : 0);
            WriteInt((int)(m.character ?? Character.Yolk));
            WriteFloat(m.x);
            WriteFloat(m.y);
            WriteFloat(m.xScale);
            WriteFloat(m.rotation);
            WriteInt(m.sprites.Length);
            for (int n = 0; n < m.sprites.Length; n += 1)
            {
                WriteFloat(m.sprites[n].xRelative);
                WriteFloat(m.sprites[n].yRelative);
                WriteFloat(m.sprites[n].xScale);
                WriteString(m.sprites[n].animationId);
                WriteInt(m.sprites[n].animationFrame);
                WriteInt(m.sprites[n].sortingLayerID);
                WriteInt(m.sprites[n].sortingOrder);
            }
        }
        else if (msg is MsgIntentionallyLeaving)
        {
            WriteInt(5);
        }
        else if(msg is MsgMultiplayerPowerup)
        {
            var m = msg as MsgMultiplayerPowerup;
            WriteInt(6);
            WriteInt((int)m.type);
            WriteFloat((int)m.x);
            WriteFloat((int)m.y);
            WriteInt((int)m.chunkIndex);
        }
        else if(msg is MsgEnterGoldTrophy)
        {
            WriteInt(7);
        }
        else if(msg is MsgTimeLeft)
        {
            var m = msg as MsgTimeLeft;
            WriteInt(8);
            WriteInt((int)m.timeLeftInFrames);
        }
        else
        {
            WriteInt(0);
        }

        return list.ToArray();
    }

    private static Msg BytesToMsg(byte[] bytes)
    {
        int cursor = 0;

        int GetInt()
        {
            int b1 = bytes[cursor];
            int b2 = bytes[cursor + 1];
            int b3 = bytes[cursor + 2];
            int b4 = bytes[cursor + 3];
            cursor += 4;
            return b1 + (b2 << 8) + (b3 << 16) + (b4 << 24);
        }
        float GetFloat()
        {
            return GetInt() / 256.0f;
        }
        string GetString()
        {
            int length = GetInt();
            string result = "";
            for (int n = 0; n < length; n += 1)
            {
                result += (char)bytes[cursor];
                cursor += 1;
            }
            return result;
        }

        int messageType = GetInt();
        if (messageType == 1)
        {
            var m = new MsgConnectionStart {};
            m.matchId = GetInt();

            int dayOfYear = GetInt();
            int year = GetInt();
            m.levelDate = new DateTime(year, 1, 1).AddDays(dayOfYear - 1);

            m.playerNumberOfReceivingDevice = GetInt();

            return m;
        }
        else if (messageType == 2)
        {
            var m = new MsgConnectionWelcomeBack {};
            m.matchId = GetInt();

            int dayOfYear = GetInt();
            int year = GetInt();
            m.levelDate = new DateTime(year, 1, 1).AddDays(dayOfYear - 1);

            return m;
        }
        else if (messageType == 3)
        {
            var m = new MsgCharacterSelectionConfirmed {};
            return m;
        }
        else if (messageType == 4)
        {
            var m = new MsgHereIAm {};

            {
                var hasValue = GetInt() != 0;
                var value = (Character)GetInt();
                m.character = hasValue ? value : (Character?)null;
            }

            m.x = GetFloat();
            m.y = GetFloat();
            m.xScale = GetFloat();
            m.rotation = GetFloat();
            int spriteCount = GetInt();
            m.sprites = new GhostPlayer.SpriteData[spriteCount];

            for (int n = 0; n < m.sprites.Length; n += 1)
            {
                m.sprites[n].xRelative = GetFloat();
                m.sprites[n].yRelative = GetFloat();
                m.sprites[n].xScale = GetFloat();
                m.sprites[n].animationId = GetString();
                m.sprites[n].animationFrame = GetInt();
                m.sprites[n].sortingLayerID = GetInt();
                m.sprites[n].sortingOrder = GetInt();
            }

            return m;
        }
        else if (messageType == 5)
        {
            var m = new MsgIntentionallyLeaving {};
            return m;
        }
        else if(messageType == 6)
        {
            var m = new MsgMultiplayerPowerup {};

            m.type = (PowerupPickup.Type)GetInt();
            m.x = GetFloat();
            m.y = GetFloat();
            m.chunkIndex = GetInt();

            return m;
        }
        else if(messageType == 7)
        {
            var m = new MsgEnterGoldTrophy {};
            return m;
        }
        else if(messageType == 8)
        {
            var m = new MsgTimeLeft {};
            m.timeLeftInFrames = GetInt();
            return m;
        }
        else
        {
            return new Msg {}; // shouldn't happen
        }
    }
}
