
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Raycast
{
    public struct Result
    {
        public Map map;
        public bool anything;
        public Vector2 start;
        public Vector2 direction;
        public float distance;
        public Tile tile;
        public Item item;
        public float surfaceAngleDegrees;
        public bool wallSlideAllowed;

        public Vector2 End() => this.start + (this.distance * this.direction);
        public bool IsIce()
        {
            if (this.map.currentTemperature != Map.Temperature.Cold)
                return false;

            if (this.tile?.ice == true) return true;
            if (this.item is FireSkull) return true;
            return false;
        }
    }

    public struct CombinedResults
    {
        public Map map;
        public bool anything;
        public Vector2 start;
        public Vector2 direction;
        public float distance;
        public TinyArray<Tile> tiles;
        public TinyArray<Item> items;
        public float surfaceAngleDegrees;
        public bool wallSlideAllowed;

        public Vector2 End() => this.start + (this.distance * this.direction);
        public bool IsIce()
        {
            if (this.map.currentTemperature != Map.Temperature.Cold)
                return false;
            if (this.tiles.Length == 0 && this.items.Length == 0)
                return false;

            return (
                this.tiles.All(t => t?.ice == true) &&
                this.items.All(i => i is FireSkull)
            );
        }
    }

    private static float testRaycastAngle;

    private Map map;
    private Item[] relevantItems;
    private Chunk[] relevantChunks;
    private (Chunk, TileGrid)[] relevantTileGrids;
    private Layer layer;
    private bool isCastByPlayer;
    private bool isCastByEnemy;
    private Item ignoreItem1;
    private Item ignoreItem2;
    private System.Type ignoreItemType;
    private System.Type[] ignoreItemTypes;
    private bool ignoreTileTopOnlyFlag; // set this to hit top-only tiles from below
    private bool stopAtPortals; //raycasts will return distance to portal

    public Raycast(
        Map map,
        Vector2 approximateStartPosition,
        float ignoreChunksOutsideRange = 12,
        Layer layer = Layer.A,
        bool isCastByPlayer = false,
        bool isCastByEnemy = false,
        Item ignoreItem1 = null,
        Item ignoreItem2 = null,
        System.Type ignoreItemType = null,
        System.Type[] ignoreItemTypes = null,
        bool ignoreTileTopOnlyFlag = false,
        bool stopAtPortals = false
    )
    {
        this.map = map;
        this.layer = layer;
        this.isCastByPlayer = isCastByPlayer;
        this.isCastByEnemy = isCastByEnemy;
        this.ignoreItem1 = ignoreItem1;
        this.ignoreItem2 = ignoreItem2;
        this.ignoreItemType = ignoreItemType;
        this.ignoreItemTypes = ignoreItemTypes;
        this.ignoreTileTopOnlyFlag = ignoreTileTopOnlyFlag;
        this.stopAtPortals = stopAtPortals;

        this.relevantChunks = ChunksWithinDistance(
            map, approximateStartPosition, ignoreChunksOutsideRange
        ).ToArray();

        var relevantTileGridCount = 0;
        foreach (var chunk in this.relevantChunks)
        {
            foreach (var tileGrid in chunk.TileGridsForLayer(layer))
                relevantTileGridCount += 1;
        }

        this.relevantTileGrids = new (Chunk, TileGrid)[relevantTileGridCount];
        var index = 0;

        foreach (var chunk in this.relevantChunks)
        {
            foreach (var tileGrid in chunk.TileGridsForLayer(layer))
                this.relevantTileGrids[index++] = (chunk, tileGrid);
        }

        FindRelevantItems();
    }

    private static List<Item> findRelevantItemsTempList1 = new();
    private static List<Item> findRelevantItemsTempList2 = new();
    private void FindRelevantItems()
    {
        var items = findRelevantItemsTempList1;
        items.Clear();

        foreach (var chunk in this.relevantChunks)
        {
            foreach (var item in chunk.subsetOfItemsRaycastCanHit)
            {
                if (isCastByPlayer == true && item.solidToPlayer == false) continue;
                if (isCastByEnemy == true && item.solidToEnemy == false) continue;
                if (ignoreItem1 == item) continue;
                if (ignoreItem2 == item) continue;
                if (ignoreItemType != null && item.GetType() == ignoreItemType) continue;

                items.Add(item);
            }
        }

        if (this.ignoreItemTypes != null)
        {
            var itemsBeforeFilter = items;
            items = findRelevantItemsTempList2;
            items.Clear();

            foreach (var item in itemsBeforeFilter)
            {
                if (this.ignoreItemTypes.Contains(item.GetType()) == false)
                    items.Add(item);
            }
        }

        this.relevantItems = items.ToArray();
    }

    private static List<Tile> combineClosestTempListTiles = new();
    private static List<Item> combineClosestTempListItems = new();

    public static CombinedResults CombineClosest(
        Result first,
        Result second = default,
        Result third = default,
        Result fourth = default,
        Result fifth = default
    )
    {
        Result closest = first;
        if (second.anything && (!closest.anything || second.distance < closest.distance))
            closest = second;
        if (third.anything && (!closest.anything || third.distance < closest.distance))
            closest = third;
        if (fourth.anything && (!closest.anything || fourth.distance < closest.distance))
            closest = fourth;
        if (fifth.anything && (!closest.anything || fifth.distance < closest.distance))
            closest = fifth;

        var tiles = combineClosestTempListTiles;
        var items = combineClosestTempListItems;
        tiles.Clear();
        items.Clear();
        var result = new CombinedResults
        {
            map = first.map,
            anything = false,
            start = closest.start,
            direction = closest.direction,
            distance = closest.distance,
            surfaceAngleDegrees = closest.surfaceAngleDegrees,
            wallSlideAllowed = closest.wallSlideAllowed
        };

        if (first.anything == true && first.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (first.tile != null) tiles.Add(first.tile);
            if (first.item != null) items.Add(first.item);
        }
        if (second.anything == true && second.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (second.tile != null) tiles.Add(second.tile);
            if (second.item != null) items.Add(second.item);
        }
        if (third.anything == true && third.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (third.tile != null) tiles.Add(third.tile);
            if (third.item != null) items.Add(third.item);
        }
        if (fourth.anything == true && fourth.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (fourth.tile != null) tiles.Add(fourth.tile);
            if (fourth.item != null) items.Add(fourth.item);
        }
        if (fifth.anything == true && fifth.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (fifth.tile != null) tiles.Add(fifth.tile);
            if (fifth.item != null) items.Add(fifth.item);
        }

        result.tiles.SetValues(tiles);
        result.items.SetValues(items);
        return result;
    }

    public static CombinedResults CombineClosest(Result first, params Result[] more)
    {
        Result closest = first;
        foreach (var r in more)
        {
            if (r.anything == false)
                continue;
            if (first.anything == false || r.distance < closest.distance)
                closest = r;
        }

        var tiles = combineClosestTempListTiles;
        var items = combineClosestTempListItems;
        tiles.Clear();
        items.Clear();
        var result = new CombinedResults
        {
            map = first.map,
            anything = false,
            start = closest.start,
            direction = closest.direction,
            distance = closest.distance,
            surfaceAngleDegrees = closest.surfaceAngleDegrees,
            wallSlideAllowed = closest.wallSlideAllowed
        };

        if (first.anything == true && first.distance <= closest.distance + 1e-3)
        {
            result.anything = true;
            if (first.tile != null) tiles.Add(first.tile);
            if (first.item != null) items.Add(first.item);
        }
        foreach (var ray in more)
        {
            if (ray.anything == true && ray.distance <= closest.distance + 1e-3)
            {
                result.anything = true;
                if (ray.tile != null) tiles.Add(ray.tile);
                if (ray.item != null) items.Add(ray.item);
            }
        }
        result.tiles.SetValues(tiles);
        result.items.SetValues(items);
        return result;
    }

    private static Result Closest(Result a, Result b)
    {
        if (a.anything == false) return b;
        if (b.anything == false) return a;
        return a.distance <= b.distance ? a : b;
    }

    public Result Horizontal(
        Vector2 start,
        bool    right,
        float   maxDistance
    )
    {
        Vector2 direction = new Vector2(right ? 1 : -1, 0);
        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            direction = direction,
            distance = maxDistance
        };

        foreach (var item in this.relevantItems)
        {
            foreach (var hitbox in item.hitboxes)
            {
                if (hitbox.worldYMin > start.y) continue;
                if (hitbox.worldYMax < start.y) continue;

                if (right)
                {
                    if (hitbox.solidXMin == false) continue;
                    if (start.x > hitbox.worldXMax) continue;
                    if (start.x + maxDistance < hitbox.worldXMin) continue;

                    var dist = hitbox.worldXMin - start.x;
                    if (dist < 0) dist = 0;
                    if (dist > result.distance) continue;

                    float surfaceAngleDegrees = 90;

                    if (hitbox.customShape != null)
                    {
                        Vector2 relative = start + (direction * dist) - item.position;
                        if (hitbox.customShape.IsSolidAtPoint(relative) == false)
                        {
                            var distanceInside =
                                hitbox.DistanceToSolidPart(relative, direction);
                            if (distanceInside == null) continue;
                            dist += distanceInside.Value;
                            if (dist > result.distance) continue;
                            surfaceAngleDegrees = hitbox.customShape.SurfaceAngleDegrees(
                                start + (direction * dist) - item.position,
                                direction
                            );
                        }
                    }

                    result = new Result {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = direction,
                        distance            = dist,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = surfaceAngleDegrees,
                        wallSlideAllowed    = hitbox.wallSlideAllowedOnLeft
                    };
                }
                else
                {
                    if (hitbox.solidXMax == false) continue;
                    if (start.x < hitbox.worldXMin) continue;
                    if (start.x - maxDistance > hitbox.worldXMax) continue;

                    var dist = start.x - hitbox.worldXMax;
                    if (dist < 0) dist = 0;
                    if (dist > result.distance) continue;

                    float surfaceAngleDegrees = 270;

                    if (hitbox.customShape != null)
                    {
                        Vector2 relative = start + (direction * dist) - item.position;
                        if (hitbox.customShape.IsSolidAtPoint(relative) == false)
                        {
                            var distanceInside =
                                hitbox.DistanceToSolidPart(relative, direction);
                            if (distanceInside == null) continue;
                            dist += distanceInside.Value;
                            if (dist > result.distance) continue;
                            surfaceAngleDegrees = hitbox.customShape.SurfaceAngleDegrees(
                                start + (direction * dist) - item.position,
                                direction
                            );
                        }
                    }

                    result = new Result {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = direction,
                        distance            = dist,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = surfaceAngleDegrees,
                        wallSlideAllowed    = hitbox.wallSlideAllowedOnRight
                    };
                }
            }
        }

        var gridResult =
            HorizontalRaycastAgainstTiles(start, right, maxDistance);
        result = Closest(result, gridResult);

        if (this.stopAtPortals == false && (result.item as Portal)?.IsRayEnteringPortal(start, direction) == true)
        {
            result.anything = false;
            result.distance = maxDistance;
            result.item = null;
        }

        if (DebugDrawing.enableLoggingRaycasts == true)
            DebugDrawing.RaycastsThisFrame?.Add(result);
        return result;
    }

    public Result Vertical(
        Vector2 start,
        bool    up,
        float   maxDistance
    )
    {
        Vector2 direction = new Vector2(0, up ? 1 : -1);
        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            direction = direction,
            distance = maxDistance
        };

        foreach (var item in this.relevantItems)
        {
            foreach (var hitbox in item.hitboxes)
            {
                if (hitbox.worldXMin > start.x) continue;
                if (hitbox.worldXMax < start.x) continue;

                if (up)
                {
                    if (hitbox.solidYMin == false) continue;
                    if (start.y > hitbox.worldYMax) continue;
                    if (start.y + maxDistance < hitbox.worldYMin) continue;

                    var dist = hitbox.worldYMin - start.y;
                    if (dist < 0) dist = 0;
                    if (dist > result.distance) continue;

                    float surfaceAngleDegrees = 180;

                    if (hitbox.customShape != null)
                    {
                        Vector2 relative = start + (direction * dist) - item.position;
                        if (hitbox.customShape.IsSolidAtPoint(relative) == false)
                        {
                            var distanceInside =
                                hitbox.DistanceToSolidPart(relative, direction);
                            if (distanceInside == null) continue;
                            dist += distanceInside.Value;
                            if (dist > result.distance) continue;
                            surfaceAngleDegrees = hitbox.customShape.SurfaceAngleDegrees(
                                start + (direction * dist) - item.position,
                                direction
                            );
                        }
                    }

                    result = new Result {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = direction,
                        distance            = dist,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = surfaceAngleDegrees,
                        wallSlideAllowed    = false
                    };
                }
                else
                {
                    if (hitbox.solidYMax == false) continue;
                    if (start.y < hitbox.worldYMin) continue;
                    if (start.y - maxDistance > hitbox.worldYMax) continue;

                    var dist = start.y - hitbox.worldYMax;
                    if (dist < 0) dist = 0;
                    if (dist > result.distance) continue;

                    float surfaceAngleDegrees = 0;

                    if (hitbox.customShape != null)
                    {
                        Vector2 relative = start + (direction * dist) - item.position;
                        if (hitbox.customShape.IsSolidAtPoint(relative) == false)
                        {
                            var distanceInside =
                                hitbox.DistanceToSolidPart(relative, direction);
                            if (distanceInside == null) continue;
                            dist += distanceInside.Value;
                            if (dist > result.distance) continue;
                            surfaceAngleDegrees = hitbox.customShape.SurfaceAngleDegrees(
                                start + (direction * dist) - item.position,
                                direction
                            );
                        }
                    }

                    result = new Result {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = direction,
                        distance            = dist,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = surfaceAngleDegrees,
                        wallSlideAllowed    = false
                    };
                }
            }
        }

        var gridResult =
            VerticalRaycastAgainstTiles(start, up, maxDistance);
        result = Closest(result, gridResult);

        if (this.stopAtPortals == false && (result.item as Portal)?.IsRayEnteringPortal(start, direction) == true)
        {
            result.anything = false;
            result.distance = maxDistance;
            result.item = null;
        }

        if (DebugDrawing.enableLoggingRaycasts == true)
            DebugDrawing.RaycastsThisFrame?.Add(result);
        return result;
    }

    public Result Arbitrary(
        Vector2 start,
        Vector2 normalizedDirection,
        float   maxDistance
    )
    {
        if (Mathf.Abs(normalizedDirection.x) < 0.001f)
            return Vertical(start, normalizedDirection.y > 0, maxDistance);
        if (Mathf.Abs(normalizedDirection.y) < 0.001f)
            return Horizontal(start, normalizedDirection.x > 0, maxDistance);

        #if UNITY_EDITOR
            if (Mathf.Approximately(normalizedDirection.sqrMagnitude, 1) == false)
            {
                Debug.LogError("Raycast direction is not normalized!");
            }
        #endif

        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            direction = normalizedDirection,
            distance = maxDistance
        };

        var invNormalizedDirectionX = 1 / normalizedDirection.x;
        var invNormalizedDirectionY = 1 / normalizedDirection.y;

        foreach (var item in this.relevantItems)
        {
            foreach (var hitbox in item.hitboxes)
            {
                float pxEntry, pxExit, pyEntry, pyExit;
                bool solidOnX, solidOnY;

                // when do we enter and exit the x region that the item is in?
                if (normalizedDirection.x > 0)
                {
                    pxEntry  = hitbox.worldXMin - start.x;
                    pxExit   = hitbox.worldXMax - start.x;
                    solidOnX = hitbox.solidXMin;
                }
                else
                {
                    pxEntry  = hitbox.worldXMax - start.x;
                    pxExit   = hitbox.worldXMin - start.x;
                    solidOnX = hitbox.solidXMax;
                }

                // when do we enter and exit the y region that the item is in?
                if (normalizedDirection.y > 0)
                {
                    pyEntry  = hitbox.worldYMin - start.y;
                    pyExit   = hitbox.worldYMax - start.y;
                    solidOnY = hitbox.solidYMin;
                }
                else
                {
                    pyEntry  = hitbox.worldYMax - start.y;
                    pyExit   = hitbox.worldYMin - start.y;
                    solidOnY = hitbox.solidYMax;
                }

                pxEntry *= invNormalizedDirectionX;
                pxExit  *= invNormalizedDirectionX;
                pyEntry *= invNormalizedDirectionY;
                pyExit  *= invNormalizedDirectionY;

                // are we already past the object?
                if (pxEntry < 0 && pyEntry < 0) continue;

                // are we ever overlapping in both x and y simulaneously?
                if (pxEntry > pyExit) continue;
                if (pyEntry > pxExit) continue;

                if (pxEntry > pyEntry)
                {
                    // we hit the left/right edge of the item
                    if (solidOnX == false) continue;
                    if (pxEntry > result.distance) continue;

                    result = new Result
                    {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = normalizedDirection,
                        distance            = pxEntry,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = normalizedDirection.x > 0 ? 90 : 270,
                        wallSlideAllowed    = normalizedDirection.x > 0 ?
                                                  hitbox.wallSlideAllowedOnLeft :
                                                  hitbox.wallSlideAllowedOnRight
                    };
                }
                else
                {
                    // we hit the top/bottom edge of the item
                    if (solidOnY == false) continue;
                    if (pyEntry > result.distance) continue;

                    result = new Result
                    {
                        map                 = this.map,
                        anything            = true,
                        start               = start,
                        direction           = normalizedDirection,
                        distance            = pyEntry,
                        tile                = null,
                        item                = item,
                        surfaceAngleDegrees = normalizedDirection.y > 0 ? 180 : 0,
                        wallSlideAllowed    = normalizedDirection.x > 0 ?
                                                  hitbox.wallSlideAllowedOnLeft :
                                                  hitbox.wallSlideAllowedOnRight
                    };
                }
            }
        }

        var gridResult = ArbitraryRaycastAgainstTiles(
            start, normalizedDirection, maxDistance
        );
        result = Closest(result, gridResult);

        if (this.stopAtPortals == false && (result.item as Portal)?.IsRayEnteringPortal(
            start, normalizedDirection
        ) == true)
        {
            result.anything = false;
            result.distance = maxDistance;
            result.item = null;
        }

        if (DebugDrawing.enableLoggingRaycasts == true)
            DebugDrawing.RaycastsThisFrame?.Add(result);
        return result;
    }

    private Result HorizontalRaycastAgainstTiles(
        Vector2 start,
        bool    right,
        float   length
    )
    {
        var direction = new Vector2(right ? 1 : -1, 0);
        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            direction = direction,
            distance = length
        };

        foreach (var (chunk, tileGrid) in this.relevantTileGrids)
        {
            var startOffset = start - tileGrid.offset;

            var tx = Mathf.FloorToInt(startOffset.x / Tile.Size);
            var ty = Mathf.FloorToInt(startOffset.y / Tile.Size);
            if (ty < tileGrid.yMin) continue;
            if (ty > tileGrid.yMax) continue;

            var px = startOffset.x;
            var py = startOffset.y;

            if (right)
            {
                if (tx < tileGrid.xMin)
                {
                    tx = tileGrid.xMin;
                    px = tx;
                }

                for (;
                    tx <= tileGrid.xMax;
                    tx += 1, px = tx * Tile.Size
                )
                {
                    if (px > startOffset.x + length) break;

                    // var portal = chunk.PortalAt(tx, ty);
                    // if (portal != null) break;

                    var t = tileGrid.GetTileWithoutBoundsCheck(
                        tx - tileGrid.xMin, ty - tileGrid.yMin
                    );
                    if (t == null) continue;
                    if (t.spec.topOnly == true) continue;

                    var pxRelative = px - (t.mapTx * Tile.Size);
                    var pyRelative = py - (t.mapTy * Tile.Size);
                    var relative = new Vector2(pxRelative, pyRelative);
                    var wallSlideAllowed = t.spec.wallSlideEnabled;

                    if (t.IsSolidAtPoint(pxRelative, pyRelative) == false)
                    {
                        var distance = t.DistanceToSolidPart(relative, direction);
                        if (distance == null) continue;
                        px += distance.Value;
                        pxRelative += distance.Value;
                        relative.x += distance.Value;
                        if (px > startOffset.x + length) break;
                        wallSlideAllowed = false;
                    }
                    var r = new Result
                    {
                        map = this.map,
                        anything = true,
                        start = start,
                        distance = px - startOffset.x,
                        direction = direction,
                        tile = t,
                        item = null,
                        surfaceAngleDegrees = t.SurfaceAngleDegrees(relative, direction),
                        wallSlideAllowed = wallSlideAllowed
                    };
                    result = Closest(result, r);
                    break;
                }
            }
            else
            {
                if (tx > tileGrid.xMax)
                {
                    tx = tileGrid.xMax;
                    px = tx + 1;
                }

                for (;
                    tx >= tileGrid.xMin;
                    tx -= 1, px = (tx * Tile.Size) + Tile.Size
                )
                {
                    if (px < startOffset.x - length) break;

                    // var portal = chunk.PortalAt(tx, ty);
                    // if (portal != null) break;

                    var t = tileGrid.GetTileWithoutBoundsCheck(
                        tx - tileGrid.xMin, ty - tileGrid.yMin
                    );
                    if (t == null) continue;
                    if (t.spec.topOnly == true) continue;

                    var pxRelative = px - (t.mapTx * Tile.Size);
                    var pyRelative = py - (t.mapTy * Tile.Size);
                    var relative = new Vector2(pxRelative, pyRelative);
                    var wallSlideAllowed = t.spec.wallSlideEnabled;

                    if (t.IsSolidAtPoint(pxRelative, pyRelative) == false)
                    {
                        var distance = t.DistanceToSolidPart(relative, direction);
                        if (distance == null) continue;
                        px -= distance.Value;
                        pxRelative -= distance.Value;
                        relative.x -= distance.Value;
                        if (px < startOffset.x - length) break;
                        wallSlideAllowed = false;
                    }
                    var r = new Result
                    {
                        map = this.map,
                        anything = true,
                        start = start,
                        distance = startOffset.x - px,
                        direction = direction,
                        tile = t,
                        item = null,
                        surfaceAngleDegrees = t.SurfaceAngleDegrees(relative, direction),
                        wallSlideAllowed = wallSlideAllowed
                    };
                    result = Closest(result, r);
                    break;
                }
            }
        }

        return result;
    }

    private Result VerticalRaycastAgainstTiles(
        Vector2 start,
        bool    up,
        float   length
    )
    {
        var direction = new Vector2(0, up ? 1 : -1);
        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            distance = length,
            direction = direction
        };

        foreach (var (chunk, tileGrid) in this.relevantTileGrids)
        {
            var startOffset = start - tileGrid.offset;

            var tx = Mathf.FloorToInt(startOffset.x / Tile.Size);
            var ty = Mathf.FloorToInt(startOffset.y / Tile.Size);
            if (tx < tileGrid.xMin) continue;
            if (tx > tileGrid.xMax) continue;

            float px = startOffset.x;
            float py = startOffset.y;

            if (up)
            {
                if (ty < tileGrid.yMin)
                {
                    ty = tileGrid.yMin;
                    py = ty;
                }

                for (;
                    ty <= tileGrid.yMax;
                    ty += 1, py = ty * Tile.Size
                )
                {
                    if (py > startOffset.y + length) break;

                    var t = tileGrid.GetTileWithoutBoundsCheck(
                        tx - tileGrid.xMin, ty - tileGrid.yMin
                    );
                    if (t == null) continue;
                    if (t.spec.topOnly == true &&
                        this.ignoreTileTopOnlyFlag == false) continue;

                    var pxRelative = px - (t.mapTx * Tile.Size);
                    var pyRelative = py - (t.mapTy * Tile.Size);
                    var relative = new Vector2(pxRelative, pyRelative);
                    var wallSlideAllowed = t.spec.wallSlideEnabled;

                    if (t.IsSolidAtPoint(pxRelative, pyRelative) == false)
                    {
                        var distance = t.DistanceToSolidPart(relative, direction);
                        if (distance == null) continue;
                        py += distance.Value;
                        pyRelative += distance.Value;
                        relative.y += distance.Value;
                        if (py > startOffset.y + length) break;
                        wallSlideAllowed = false;
                    }
                    var r = new Result
                    {
                        map = this.map,
                        anything = true,
                        start = start,
                        distance = py - startOffset.y,
                        direction = direction,
                        tile = t,
                        item = null,
                        surfaceAngleDegrees = t.SurfaceAngleDegrees(relative, direction),
                        wallSlideAllowed = wallSlideAllowed
                    };
                    result = Closest(result, r);
                    break;
                }
            }
            else
            {
                if (ty > tileGrid.yMax)
                {
                    ty = tileGrid.yMax;
                    py = ty + 1;
                }

                for (;
                    ty >= tileGrid.yMin;
                    ty -= 1, py = (ty * Tile.Size) + Tile.Size
                )
                {
                    if (py < startOffset.y - length) break;

                    var t = tileGrid.GetTileWithoutBoundsCheck(
                        tx - tileGrid.xMin, ty - tileGrid.yMin
                    );
                    if (t == null) continue;

                    var pxRelative = px - (t.mapTx * Tile.Size);
                    var pyRelative = py - (t.mapTy * Tile.Size);
                    var relative = new Vector2(pxRelative, pyRelative);
                    var wallSlideAllowed = t.spec.wallSlideEnabled;

                    if (t.IsSolidAtPoint(pxRelative, pyRelative) == false)
                    {
                        var distance = t.DistanceToSolidPart(relative, direction);
                        if (distance == null) continue;
                        py -= distance.Value;
                        pyRelative -= distance.Value;
                        relative.y -= distance.Value;
                        if (py < startOffset.y - length) break;
                        wallSlideAllowed = false;
                    }
                    var r = new Result
                    {
                        map = this.map,
                        anything = true,
                        start = start,
                        distance = startOffset.y - py,
                        direction = direction,
                        tile = t,
                        item = null,
                        surfaceAngleDegrees = t.SurfaceAngleDegrees(relative, direction),
                        wallSlideAllowed = wallSlideAllowed
                    };
                    result = Closest(result, r);
                    break;
                }
            }
        }

        return result;
    }

    private Result ArbitraryRaycastAgainstTiles(
        Vector2 start,
        Vector2 normalizedDirection,
        float   length
    )
    {
        var result = new Result
        {
            map = this.map,
            anything = false,
            start = start,
            distance = length,
            direction = normalizedDirection
        };

        foreach (var (chunk, tileGrid) in this.relevantTileGrids)
        {
            var startOffset = start - tileGrid.offset;

            var tx = Mathf.FloorToInt(startOffset.x / Tile.Size);
            var ty = Mathf.FloorToInt(startOffset.y / Tile.Size);
            
            {
                var t = tileGrid.GetTile(tx - tileGrid.xMin, ty - tileGrid.yMin);
                if (t != null && t.spec.solid == true)
                {
                    var pxRelative = startOffset.x - (t.mapTx * Tile.Size);
                    var pyRelative = startOffset.y - (t.mapTy * Tile.Size);
                    if (t.IsSolidAtPoint(pxRelative, pyRelative) == true)
                    {
                        var r = new Result
                        {
                            map = this.map,
                            anything = true,
                            start = start,
                            direction = normalizedDirection,
                            distance = 0,
                            tile = t,
                            surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                new Vector2(pxRelative, pyRelative),
                                normalizedDirection
                            )
                        };
                        result = Closest(result, r);
                        continue;
                    }
                    else
                    {
                        var dist = t.DistanceToSolidPart(
                            new Vector2(pxRelative, pyRelative), normalizedDirection
                        );
                        if (dist != null && dist <= length)
                        {
                            var r = new Result
                            {
                                map = this.map,
                                anything = true,
                                start = start,
                                direction = normalizedDirection,
                                distance = dist.Value,
                                tile = t,
                                surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                    new Vector2(pxRelative, pyRelative),
                                    normalizedDirection
                                )
                            };
                            result = Closest(result, r);
                            continue;
                        }
                    }
                }
            }

            float px = startOffset.x - (tx * Tile.Size);
            float py = startOffset.y - (ty * Tile.Size);
            int targetPX = (normalizedDirection.x < 0) ? 0 : Tile.Size;
            int targetPY = (normalizedDirection.y < 0) ? 0 : Tile.Size;
            int dirX = normalizedDirection.x < 0 ? -1 : 1;
            int dirY = normalizedDirection.y < 0 ? -1 : 1;
            float invDirectionX = 1.0f / normalizedDirection.x;
            float invDirectionY = 1.0f / normalizedDirection.y;
            float totalDistance = 0;

            while (tx >= this.map.xMin &&
                tx <= this.map.xMax &&
                ty >= this.map.yMin &&
                ty <= this.map.yMax
            ) {
                var toNextX = Mathf.Abs((px - targetPX) * invDirectionX);
                var toNextY = Mathf.Abs((py - targetPY) * invDirectionY);

                if (toNextX < toNextY)
                {
                    // move horizontally
                    tx += dirX;
                    totalDistance += toNextX;
                    if (totalDistance > length)
                        break;

                    px += normalizedDirection.x * toNextX;
                    py += normalizedDirection.y * toNextX;
                    px -= dirX * Tile.Size;

                    var t = tileGrid.GetTile(tx - tileGrid.xMin, ty - tileGrid.yMin);
                    if (t != null && t.spec.solid == true)
                    {
                        if (t.spec.topOnly == true &&
                            this.ignoreTileTopOnlyFlag == false)
                        {
                            continue;
                        }

                        var pxRelative = px + ((tx - t.mapTx) * Tile.Size);
                        var pyRelative = py + ((ty - t.mapTy) * Tile.Size);
                        if (t.IsSolidAtPoint(pxRelative, pyRelative))
                        {
                            var r = new Result
                            {
                                map = this.map,
                                anything = true,
                                start = start,
                                direction = normalizedDirection,
                                distance = totalDistance,
                                tile = t,
                                surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                    new Vector2(pxRelative, pyRelative),
                                    normalizedDirection
                                )
                            };
                            result = Closest(result, r);
                            break;
                        }
                        else
                        {
                            var dist = t.DistanceToSolidPart(
                                new Vector2(pxRelative, pyRelative), normalizedDirection
                            );
                            if (dist != null && totalDistance + dist <= length)
                            {
                                var r = new Result
                                {
                                    map = this.map,
                                    anything = true,
                                    start = start,
                                    direction = normalizedDirection,
                                    distance = totalDistance + dist.Value,
                                    tile = t,
                                    surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                        new Vector2(pxRelative, pyRelative),
                                        normalizedDirection
                                    )
                                };
                                result = Closest(result, r);
                                break;
                            }
                        }
                    };
                }
                else
                {
                    // move vertically
                    ty += dirY;
                    totalDistance += toNextY;
                    if (totalDistance > length)
                        break;

                    px += normalizedDirection.x * toNextY;
                    py += normalizedDirection.y * toNextY;
                    py -= dirY * Tile.Size;

                    var t = tileGrid.GetTile(tx - tileGrid.xMin, ty - tileGrid.yMin);
                    if (t != null && t.spec.solid == true)
                    {
                        if (t.spec.topOnly == true &&
                            normalizedDirection.y > 0 &&
                            this.ignoreTileTopOnlyFlag == false)
                        {
                            continue;
                        }

                        var pxRelative = px + ((tx - t.mapTx) * Tile.Size);
                        var pyRelative = py + ((ty - t.mapTy) * Tile.Size);
                        if (t.IsSolidAtPoint(pxRelative, pyRelative))
                        {
                            var r = new Result
                            {
                                map = this.map,
                                anything = true,
                                start = start,
                                direction = normalizedDirection,
                                distance = totalDistance,
                                tile = t,
                                surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                    new Vector2(pxRelative, pyRelative),
                                    normalizedDirection
                                )
                            };
                            result = Closest(result, r);
                            break;
                        }
                        else
                        {
                            var dist = t.DistanceToSolidPart(
                                new Vector2(pxRelative, pyRelative), normalizedDirection
                            );
                            if (dist != null && totalDistance + dist <= length)
                            {
                                var r = new Result
                                {
                                    map = this.map,
                                    anything = true,
                                    start = start,
                                    direction = normalizedDirection,
                                    distance = totalDistance + dist.Value,
                                    tile = t,
                                    surfaceAngleDegrees = t.SurfaceAngleDegrees(
                                        new Vector2(pxRelative, pyRelative),
                                        normalizedDirection
                                    )
                                };
                                result = Closest(result, r);
                                break;
                            }
                        }
                    };
                }
            }
        }

        return result;
    }

    private static List<Chunk> ChunksWithinDistance(Map map, Vector2 pt, float distance)
    {
        var result = new List<Chunk>();

        foreach (var chunk in map.chunks)
        {
            var bottom = chunk.yMin * Tile.Size;
            if (pt.y < bottom - distance) continue;

            var top = (chunk.yMax + 1) * Tile.Size;
            if (pt.y > top + distance) continue;

            var left = chunk.xMin * Tile.Size;
            if (pt.x < left - distance) continue;

            var right = (chunk.xMax + 1) * Tile.Size;
            if (pt.x > right + distance) continue;

            result.Add(chunk);
        }

        return result;
    }

    public static void TestRaycast(Map map)
    {
        Vector3 world = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        testRaycastAngle += Util.Tau / 2400;
        if (Input.GetKeyDown(KeyCode.A)) testRaycastAngle -= Util.Tau / 8;
        if (Input.GetKeyDown(KeyCode.D)) testRaycastAngle += Util.Tau / 8;

        Vector2 dir = new Vector2(
            Mathf.Cos(testRaycastAngle),
            Mathf.Sin(testRaycastAngle)
        );
        var raycast = new Raycast(map, world, layer: Layer.A, isCastByPlayer: true);
        raycast.Arbitrary(world, dir, 300); // discard result
    }
}
