using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_IOS 
using UnityEngine.iOS;
#elif UNITY_TVOS
using UnityEngine.tvOS;
#endif 

using System.Runtime.InteropServices;
using AOT;

public static class RequestReview
{
    public static void ShowRequestReview()
    {
#if (UNITY_IOS || UNITY_STANDALONE_OSX) && !UNITY_EDITOR
        requestReview();
        SaveData.SetRatingPopUpDisplayed(true);
#endif
    }

#if (UNITY_IOS || UNITY_STANDALONE_OSX) && !UNITY_EDITOR
#if UNITY_STANDALONE_OSX
        private const string dllSource = "LD2ExternalOSX";
#else
        private const string dllSource = "__Internal";
#endif

    [DllImport(dllSource)]
        private static extern void requestReview();
#endif
}
