﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using LanguageId = Localization.LanguageId;

public static class SaveData
{
    // When adding a key to this class, make sure that:
    //    - DeleteSaveSlot() deletes it
    //    - CloudSaving.m does correct conflict-resolution on it
    //    - If updating CloudSaving.m, the LD2ExternalOSX bundle is updated as well

    private const string DateFormat = "yyyyMMdd";

    private const string WhatsNewKey = "End of the World Update";

    public static int currentSlotNumber
    {
        get { return Backing.GetInt("slotNumber", 1); }
        set { Backing.SetInt("slotNumber", value); }
    }

    public struct RecordForDate
    {
        public bool hasPlayedAtAll;
        public Trophy trophy;
        public float timeInSeconds;
        public int deaths;

        public static RecordForDate Combine(RecordForDate a, RecordForDate b)
        {
            if (a.hasPlayedAtAll == false) return b;
            if (b.hasPlayedAtAll == false) return a;
            if (a.trophy > b.trophy) return a;
            if (b.trophy > a.trophy) return b;
            return new RecordForDate
            {
                hasPlayedAtAll = true,
                trophy = a.trophy,
                timeInSeconds = Mathf.Min(a.timeInSeconds, b.timeInSeconds),
                deaths = Mathf.Min(a.deaths, b.deaths)
            };
        }
    }

    public struct AttemptForDate
    {
        public int checkpointNumber;
        public float timeInSeconds;
        public int deaths;
        public int fruitCurrent;
        public int fruitCollected;
        public int coins;
        public int silverCoins;
        public int timerCollected;
    }

    public enum Trophy
    {
        None, Bronze, Silver, Gold, Fruit
    }

    public enum ShopItem
    {
        YolkUpgradeLevel2,
        YolkUpgradeLevel3,
        YolkUpgradeLevel4,
        Goop,
        GoopUpgradeLevel2,
        GoopUpgradeLevel3,
        GoopUpgradeLevel4,
        Sprout,
        SproutUpgradeLevel2,
        SproutUpgradeLevel3,
        SproutUpgradeLevel4,
        Puffer,
        PufferUpgradeLevel2,
        PufferUpgradeLevel3,
        PufferUpgradeLevel4,
        King,
        KingUpgradeLevel2,
        KingUpgradeLevel3,
        KingUpgradeLevel4,
        PowerupInfiniteJump,
        PowerupMidasTouch,
        PowerupSwordAndShield,
        MinigameGolfBeginner,
        MinigameGolfIntermediate,
        MinigameGolfAdvanced,
        MinigameGolfExpert,
        MinigameRacingBeginner,
        MinigameRacingIntermediate,
        MinigameRacingAdvanced,
        MinigameRacingExpert,
        MinigamePinballBeginner,
        MinigamePinballIntermediate,
        MinigamePinballAdvanced,
        MinigamePinballExpert,
        MinigameFishingBeginner,
        MinigameFishingIntermediate,
        MinigameFishingAdvanced,
        MinigameFishingExpert,
        MinigameRollingBeginner,
        MinigameRollingIntermediate,
        MinigameRollingAdvanced,
        MinigameRollingExpert,
        MinigameAbseilingBeginner,
        MinigameAbseilingIntermediate,
        MinigameAbseilingAdvanced,
        MinigameAbseilingExpert
    }

    public static bool HasCompletedOpeningTutorial()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_completedOpeningTutorial", 0) != 0;
    }

    public static void SetHasCompletedOpeningTutorial(bool completed)
    {
        Backing.SetInt($"slot{currentSlotNumber}_completedOpeningTutorial", completed ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    private static string IdForShopItem(ShopItem item) => item switch
    {
        ShopItem.YolkUpgradeLevel2              => "YolkUpgradeLevel2",
        ShopItem.YolkUpgradeLevel3              => "YolkUpgradeLevel3",
        ShopItem.YolkUpgradeLevel4              => "YolkUpgradeLevel4",
        ShopItem.Goop                           => "Goop",
        ShopItem.GoopUpgradeLevel2              => "GoopUpgradeLevel2",
        ShopItem.GoopUpgradeLevel3              => "GoopUpgradeLevel3",
        ShopItem.GoopUpgradeLevel4              => "GoopUpgradeLevel4",
        ShopItem.Sprout                         => "Sprout",
        ShopItem.SproutUpgradeLevel2            => "SproutUpgradeLevel2",
        ShopItem.SproutUpgradeLevel3            => "SproutUpgradeLevel3",
        ShopItem.SproutUpgradeLevel4            => "SproutUpgradeLevel4",
        ShopItem.Puffer                         => "Puffer",
        ShopItem.PufferUpgradeLevel2            => "PufferUpgradeLevel2",
        ShopItem.PufferUpgradeLevel3            => "PufferUpgradeLevel3",
        ShopItem.PufferUpgradeLevel4            => "PufferUpgradeLevel4",
        ShopItem.King                           => "King",
        ShopItem.KingUpgradeLevel2              => "KingUpgradeLevel2",
        ShopItem.KingUpgradeLevel3              => "KingUpgradeLevel3",
        ShopItem.KingUpgradeLevel4              => "KingUpgradeLevel4",
        ShopItem.PowerupInfiniteJump            => "PowerupInfiniteJump",
        ShopItem.PowerupMidasTouch              => "PowerupMidasTouch",
        ShopItem.PowerupSwordAndShield          => "PowerupSwordAndShield",
        ShopItem.MinigameGolfBeginner           => "MinigameGolfBeginner",
        ShopItem.MinigameGolfIntermediate       => "MinigameGolfIntermediate",
        ShopItem.MinigameGolfAdvanced           => "MinigameGolfAdvanced",
        ShopItem.MinigameGolfExpert             => "MinigameGolfExpert",
        ShopItem.MinigameRacingBeginner         => "MinigameRacingBeginner",
        ShopItem.MinigameRacingIntermediate     => "MinigameRacingIntermediate",
        ShopItem.MinigameRacingAdvanced         => "MinigameRacingAdvanced",
        ShopItem.MinigameRacingExpert           => "MinigameRacingExpert",
        ShopItem.MinigamePinballBeginner        => "MinigamePinballBeginner",
        ShopItem.MinigamePinballIntermediate    => "MinigamePinballIntermediate",
        ShopItem.MinigamePinballAdvanced        => "MinigamePinballAdvanced",
        ShopItem.MinigamePinballExpert          => "MinigamePinballExpert",
        ShopItem.MinigameFishingBeginner        => "MinigameFishingBeginner",
        ShopItem.MinigameFishingIntermediate    => "MinigameFishingIntermediate",
        ShopItem.MinigameFishingAdvanced        => "MinigameFishingAdvanced",
        ShopItem.MinigameFishingExpert          => "MinigameFishingExpert",
        ShopItem.MinigameRollingBeginner        => "MinigameRollingBeginner",
        ShopItem.MinigameRollingIntermediate    => "MinigameRollingIntermediate",
        ShopItem.MinigameRollingAdvanced        => "MinigameRollingAdvanced",
        ShopItem.MinigameRollingExpert          => "MinigameRollingExpert",
        ShopItem.MinigameAbseilingBeginner      => "MinigameAbseilingBeginner",
        ShopItem.MinigameAbseilingIntermediate  => "MinigameAbseilingIntermediate",
        ShopItem.MinigameAbseilingAdvanced      => "MinigameAbseilingAdvanced",
        ShopItem.MinigameAbseilingExpert        => "MinigameAbseilingExpert",
        _                                    => throw new NotImplementedException()
    };

    public static ShopItem ShopItemForMinigameDifficulty(minigame.Minigame.Type type, MinigameDifficulty difficulty) => type switch
    {
        minigame.Minigame.Type.Golf => difficulty switch
        {
            MinigameDifficulty.Beginner     => ShopItem.MinigameGolfBeginner,
            MinigameDifficulty.Intermediate => ShopItem.MinigameGolfIntermediate,
            MinigameDifficulty.Advanced     => ShopItem.MinigameGolfAdvanced,
            MinigameDifficulty.Expert       => ShopItem.MinigameGolfExpert,
            _ => throw new NotImplementedException()
        },
        minigame.Minigame.Type.Pinball => difficulty switch
        {
            MinigameDifficulty.Beginner     => ShopItem.MinigamePinballBeginner,
            MinigameDifficulty.Intermediate => ShopItem.MinigamePinballIntermediate,
            MinigameDifficulty.Advanced     => ShopItem.MinigamePinballAdvanced,
            MinigameDifficulty.Expert       => ShopItem.MinigamePinballExpert,
            _ => throw new NotImplementedException()
        },
        minigame.Minigame.Type.Racing => difficulty switch
        {
            MinigameDifficulty.Beginner     => ShopItem.MinigameRacingBeginner,
            MinigameDifficulty.Intermediate => ShopItem.MinigameRacingIntermediate,
            MinigameDifficulty.Advanced     => ShopItem.MinigameRacingAdvanced,
            MinigameDifficulty.Expert       => ShopItem.MinigameRacingExpert,
            _ => throw new NotImplementedException()
        },
        minigame.Minigame.Type.Fishing => difficulty switch
        {
            MinigameDifficulty.Beginner     => ShopItem.MinigameFishingBeginner,
            MinigameDifficulty.Intermediate => ShopItem.MinigameFishingIntermediate,
            MinigameDifficulty.Advanced     => ShopItem.MinigameFishingAdvanced,
            MinigameDifficulty.Expert       => ShopItem.MinigameFishingExpert,
            _ => throw new NotImplementedException()
        },
        minigame.Minigame.Type.Rolling => difficulty switch
        {
            MinigameDifficulty.Beginner     => ShopItem.MinigameRollingBeginner,
            MinigameDifficulty.Intermediate => ShopItem.MinigameRollingIntermediate,
            MinigameDifficulty.Advanced     => ShopItem.MinigameRollingAdvanced,
            MinigameDifficulty.Expert       => ShopItem.MinigameRollingExpert,
            _ => throw new NotImplementedException()
        },
        _ => throw new NotImplementedException()
    };

    private static SaveDataBacking _backing;
    private static SaveDataBacking Backing
    {
        get
        {
            EnsureValid();
            return _backing;
        }
    }

    public static void EnsureValid()
    {
        if (_backing != null)
            return;

        #if UNITY_SWITCH && !UNITY_EDITOR
            _backing = new SaveDataSwitch();
        #elif UNITY_STANDALONE_WIN || UNITY_EDITOR_WIN
            _backing = new SaveDataToFile();
        #else
            _backing = new SaveDataPlayerPrefs();
        #endif
    }

    public static Character GetSelectedCharacter() =>
        GetSelectedCharacter(currentSlotNumber);

    public static Character GetSelectedCharacter(int slotNumber)
    {
        var str = Backing.GetString($"slot{slotNumber}_selectedCharacter", null);
        return str switch
        {
            "goop" => Character.Goop,
            "king" => Character.King,
            "puffer" => Character.Puffer,
            "sprout" => Character.Sprout,
            _ => Character.Yolk
        };
    }

    public static void SetSelectedCharacter(Character character)
    {
        var str = character switch
        {
            Character.Goop   => "goop",
            Character.King   => "king",
            Character.Puffer => "puffer",
            Character.Sprout => "sprout",
            _                => "yolk"
        };
        Backing.SetString($"slot{currentSlotNumber}_selectedCharacter", str);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static int GetCharacterUpgradeLevel(Character character)
    {
        bool IsPurchased(ShopItem item) =>
            Backing.GetInt($"slot{currentSlotNumber}_purchased" + IdForShopItem(item), 0) != 0;

        switch (character)
        {
            case Character.Yolk:
                if (IsPurchased(ShopItem.YolkUpgradeLevel4)) return 4;
                if (IsPurchased(ShopItem.YolkUpgradeLevel3)) return 3;
                if (IsPurchased(ShopItem.YolkUpgradeLevel2)) return 2;
                return 1;

            case Character.Sprout:
                if (IsPurchased(ShopItem.SproutUpgradeLevel4)) return 4;
                if (IsPurchased(ShopItem.SproutUpgradeLevel3)) return 3;
                if (IsPurchased(ShopItem.SproutUpgradeLevel2)) return 2;
                return 1;

            case Character.Goop:
                if (IsPurchased(ShopItem.GoopUpgradeLevel4)) return 4;
                if (IsPurchased(ShopItem.GoopUpgradeLevel3)) return 3;
                if (IsPurchased(ShopItem.GoopUpgradeLevel2)) return 2;
                return 1;

            case Character.Puffer:
                if (IsPurchased(ShopItem.PufferUpgradeLevel4)) return 4;
                if (IsPurchased(ShopItem.PufferUpgradeLevel3)) return 3;
                if (IsPurchased(ShopItem.PufferUpgradeLevel2)) return 2;
                return 1;

            case Character.King:
                if (IsPurchased(ShopItem.KingUpgradeLevel4)) return 4;
                if (IsPurchased(ShopItem.KingUpgradeLevel3)) return 3;
                if (IsPurchased(ShopItem.KingUpgradeLevel2)) return 2;
                return 1;
        }
        return 1;
    }

    public static int GetCharacterUpgradeLevelCurrentlySelected(Character character)
    {
        return Backing.GetInt($"slot{currentSlotNumber}_{character}_upgrade", GetCharacterUpgradeLevel(character));
    }

    public static void SetCharacterUpgradeLevelCurrentlySelected(Character character, int value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_{character}_upgrade", value);
    }

    public static RecordForDate GetRecordForDate(DateTime date) =>
        GetRecordForDate(currentSlotNumber, date);

    public static RecordForDate GetRecordForDate(int slotNumber, DateTime date)
    {
        RecordForDate Blank() => new RecordForDate {
            hasPlayedAtAll = false, trophy = Trophy.None, timeInSeconds = 0, deaths = 0
        };

        string key = $"slot{slotNumber}_recordFor" + date.ToString("yyyyMM");
        if (Backing.HasKey(key) == false)
            return Blank();

        string monthInfo = Backing.GetString(key, "");
        string[] daysInfo = monthInfo.Split('|');
        int daysInfoIndex = date.Day - 1;
        if (daysInfoIndex < 0 || daysInfoIndex >= daysInfo.Length)
            return Blank();

        string dayInfo = daysInfo[daysInfoIndex];
        string[] elements = dayInfo.Split(' ');
        if (
            elements.Length == 3 &&
            int.TryParse(elements[0], out var trophy) &&
            float.TryParse(elements[1], out var timeInSeconds) &&
            int.TryParse(elements[2], out var deaths)
        )
        {
            return new RecordForDate {
                hasPlayedAtAll = true,
                trophy        = (Trophy)trophy,
                timeInSeconds = timeInSeconds,
                deaths        = deaths
            };
        }
        else
        {
            return Blank();
        }
    }

    public static void SetRecordForDate(DateTime date, RecordForDate record)
    {
        string key = $"slot{currentSlotNumber}_recordFor" + date.ToString("yyyyMM");
        int writeToIndex = date.Day - 1;

        List<string> daysInfo;
        if (Backing.HasKey(key) == true)
            daysInfo = Backing.GetString(key).Split('|').ToList();
        else
            daysInfo = new List<string>();

        while (writeToIndex >= daysInfo.Count)
        {
            daysInfo.Add("");
        }

        if (record.hasPlayedAtAll == true)
        {
            daysInfo[writeToIndex] = string.Format(
                "{0} {1:0.00} {2}", (int)record.trophy, record.timeInSeconds, record.deaths
            );
        }
        else
        {
            daysInfo[writeToIndex] = "";
        }

        string value = string.Join("|", daysInfo);
        Backing.SetString(key, value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static AttemptForDate GetLatestAttemptForDate(DateTime date)
    {
        string key = $"slot{currentSlotNumber}_latestAttempt" + date.ToString(DateFormat);
        string[] split = Backing.GetString(key, "0 0").Split(' ');

        if (
            split.Length >= 3 &&
            int.TryParse(split[0], out var checkpointNumber) &&
            float.TryParse(split[1], out var timeInSeconds) &&
            int.TryParse(split[2], out var deaths)
        )
        {
            var result = new AttemptForDate {
                checkpointNumber = checkpointNumber,
                timeInSeconds = timeInSeconds,
                deaths = deaths
            };
            if (split.Length >= 7)
            {
                result.fruitCurrent   = int.Parse(split[3]);
                result.fruitCollected = int.Parse(split[4]);
                result.coins          = int.Parse(split[5]);
                result.silverCoins    = int.Parse(split[6]);
            }
            return result;
        }
        else
        {
            return new AttemptForDate {
                checkpointNumber = 0,
                timeInSeconds = 0,
                deaths = 0
            };
        }
    }

    public static void SetLatestAttemptForDate(DateTime date, AttemptForDate attempt)
    {
        string key = $"slot{currentSlotNumber}_latestAttempt" + date.ToString(DateFormat);
        string value = string.Format(
            "{0} {1:0.00} {2} {3} {4} {5} {6}",
            attempt.checkpointNumber,
            attempt.timeInSeconds,
            attempt.deaths,
            attempt.fruitCurrent,
            attempt.fruitCollected,
            attempt.coins,
            attempt.silverCoins
        );
        Backing.SetString(key, value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static void ClearLatestAttemptForDate(DateTime date)
    {
        string key = $"slot{currentSlotNumber}_latestAttempt" + date.ToString(DateFormat);
        Backing.DeleteKey(key);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    private static string KeyForMinigameCompleted(
        minigame.Minigame.Type type, MinigameDifficulty difficulty, int levelNumber
    )
    {
        string g = type switch
        {
            minigame.Minigame.Type.Golf      => "Golf",
            minigame.Minigame.Type.Racing    => "Racing",
            minigame.Minigame.Type.Pinball   => "Pinball",
            minigame.Minigame.Type.Fishing   => "Fishing",
            minigame.Minigame.Type.Rolling   => "Rolling",
            minigame.Minigame.Type.Abseiling => "Abseiling",
            _                              => throw new InvalidOperationException()
        };
        string d = difficulty switch
        {
            MinigameDifficulty.Beginner     => "Beginner",
            MinigameDifficulty.Intermediate => "Intermediate",
            MinigameDifficulty.Advanced     => "Advanced",
            MinigameDifficulty.Expert       => "Expert",
            _                               => throw new InvalidOperationException()
        };
        return $"slot{currentSlotNumber}_minigameCompleted{g}{d}{levelNumber}";
    }

    public static bool IsMinigameCompleted(
        minigame.Minigame.Type type,
        MinigameDifficulty difficulty,
        int levelNumberZeroBased
    )
    {
        string key = KeyForMinigameCompleted(type, difficulty, levelNumberZeroBased);
        return Backing.GetInt(key) != 0;
    }

    public static void SetMinigameCompleted(
        minigame.Minigame.Type type,
        MinigameDifficulty difficulty,
        int levelNumberZeroBased,
        bool value
    )
    {
        string key = KeyForMinigameCompleted(type, difficulty, levelNumberZeroBased);
        Backing.SetInt(key, value ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool HasCompletedMinigameDifficulty(
        minigame.Minigame.Type type, MinigameDifficulty difficulty
    ) =>
        IsMinigameCompleted(type, difficulty, 0) &&
        IsMinigameCompleted(type, difficulty, 1) &&
        IsMinigameCompleted(type, difficulty, 2) &&
        IsMinigameCompleted(type, difficulty, 3) &&
        IsMinigameCompleted(type, difficulty, 4);

    public static bool HasCompletedAllMinigameDifficulties(
        minigame.Minigame.Type type
    ) =>
        HasCompletedMinigameDifficulty(type, MinigameDifficulty.Beginner) &&
        HasCompletedMinigameDifficulty(type, MinigameDifficulty.Intermediate) &&
        HasCompletedMinigameDifficulty(type, MinigameDifficulty.Advanced) &&
        HasCompletedMinigameDifficulty(type, MinigameDifficulty.Expert);

    public static bool IsAnyMinigamePurchased()
    {
        return
            IsItemPurchased(ShopItem.MinigameGolfBeginner) ||
            IsItemPurchased(ShopItem.MinigameRacingBeginner) ||
            IsItemPurchased(ShopItem.MinigameFishingBeginner) ||
            IsItemPurchased(ShopItem.MinigameRollingBeginner) ||
            IsItemPurchased(ShopItem.MinigamePinballBeginner) ||
            IsItemPurchased(ShopItem.MinigameAbseilingBeginner);
    }

    public static bool IsMinigamePurchased(minigame.Minigame.Type type) => type switch
    {
        minigame.Minigame.Type.Golf => IsItemPurchased(ShopItem.MinigameGolfBeginner),
        minigame.Minigame.Type.Pinball => IsItemPurchased(ShopItem.MinigamePinballBeginner),
        minigame.Minigame.Type.Racing => IsItemPurchased(ShopItem.MinigameRacingBeginner),
        minigame.Minigame.Type.Fishing => IsItemPurchased(ShopItem.MinigameFishingBeginner),
        minigame.Minigame.Type.Rolling => IsItemPurchased(ShopItem.MinigameRollingBeginner),
        minigame.Minigame.Type.Abseiling => IsItemPurchased(ShopItem.MinigameAbseilingBeginner),
        _ => throw new NotImplementedException()
    };

    public static MinigameDifficulty[] MinigameDifficultiesPurchased(minigame.Minigame.Type type)
    {
        var shopItemsAndDifficulties = type switch
        {
            minigame.Minigame.Type.Golf =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigameGolfBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigameGolfIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigameGolfAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigameGolfExpert
                },
            minigame.Minigame.Type.Racing =>
                new Dictionary<MinigameDifficulty, ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigameRacingBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigameRacingIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigameRacingAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigameRacingExpert
                },
            minigame.Minigame.Type.Pinball =>
                new Dictionary<MinigameDifficulty, ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigamePinballBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigamePinballIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigamePinballAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigamePinballExpert
                },
            minigame.Minigame.Type.Fishing =>
                new Dictionary<MinigameDifficulty, ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigameFishingBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigameFishingIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigameFishingAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigameFishingExpert
                },
            minigame.Minigame.Type.Rolling =>
                new Dictionary<MinigameDifficulty, ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigameRollingBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigameRollingIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigameRollingAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigameRollingExpert
                },
            minigame.Minigame.Type.Abseiling =>
                new Dictionary<MinigameDifficulty, ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = ShopItem.MinigameAbseilingBeginner,
                    [MinigameDifficulty.Intermediate]   = ShopItem.MinigameAbseilingIntermediate,
                    [MinigameDifficulty.Advanced]       = ShopItem.MinigameAbseilingAdvanced,
                    [MinigameDifficulty.Expert]         = ShopItem.MinigameAbseilingExpert
                },
            _ => null
        };

        var permitted =
            shopItemsAndDifficulties
                .Where(kv => SaveData.IsItemPurchased(kv.Value))
                .Select(kv => kv.Key)
                .ToArray();

        if(permitted.Length == 0)
        {
            permitted = new MinigameDifficulty[] {
                MinigameDifficulty.Beginner,
                MinigameDifficulty.Intermediate,
                MinigameDifficulty.Advanced,
                MinigameDifficulty.Expert
            };
        }
        return permitted;
    }

    public static int GetCoins()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_coins", 0);
    }

    public static int GetCoins(int slot)
    {
        return Backing.GetInt($"slot{slot}_coins", 0);
    }

    public static void SetCoins(int value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_coins", value);
        UpdateTimestampAndSave(currentSlotNumber);
    }
    
    private const int FirstCheckpointNumber = 15;

    public static int GetNumberOfCheckpointsDifference(int newCheckpointNumberReached)
    {
        var latestAttempt = SaveData.GetLatestAttemptForDate(Game.selectedDate);
        int latestCheckpointNumberReached = latestAttempt.checkpointNumber;

        if(latestAttempt.checkpointNumber == 0) // No previous checkpoint reached
        {
            // If we don't have a previous reached checkpoint number, we use the first
            // checkpoint number plus one, to help calculate the difference.
            latestCheckpointNumberReached = FirstCheckpointNumber + 1;
        }
        
        return latestCheckpointNumberReached - newCheckpointNumberReached;
    }

    public static int GetNumberOfCheckpointsSkipped(int newCheckpointNumberReached)
    {
        int checkpointNumberDiff = GetNumberOfCheckpointsDifference(newCheckpointNumberReached);

        // The number of checkpoints skipped is equal to the difference between
        // checkpoint numbers minus one. This number should not be less than 0
        // or more than the first checkpoint number in the level.
        return Mathf.Clamp(checkpointNumberDiff - 1, 0, FirstCheckpointNumber);
    }

    public static int GetMaxCheckpointsSkipped()
    {
        // this info was wrongly not attached to a save slot, so for 1.8 we'll
        // migrate it to whatever save slot is active when the key is first read.
        // once we get to 1.9 and it's been a few months, we can delete this and
        // go back to the straightforward version.

        var oldKey = "max_checkpoints_skipped";
        var newKey = $"slot{currentSlotNumber}_maxCheckpointsSkipped";

        if (Backing.HasKey(newKey) == false && Backing.HasKey(oldKey) == true)
        {
            int value = Backing.GetInt(oldKey, 0);
            Backing.SetInt(newKey, value);
            Backing.DeleteKey(oldKey);
            UpdateTimestampAndSave(currentSlotNumber);
        }

        return Backing.GetInt($"slot{currentSlotNumber}_maxCheckpointsSkipped", 0);
    }

    public static void SetMaxCheckpointsSkipped(int value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_maxCheckpointsSkipped", value);
    }

    public static int GetCoinsDonatedToCharity()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_coinsDonatedToCharity", 0);
    }

    public static void SetCoinsDonatedToCharity(int value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_coinsDonatedToCharity", value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static float GetSfxVolume()
    {
        return Backing.GetFloat($"slot{currentSlotNumber}_sfxVolume", 1);
    }

    public static void SetSfxVolume(float value)
    {
        Audio.instance.NotifyVolumeChanged();
        Backing.SetFloat($"slot{currentSlotNumber}_sfxVolume", value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static float GetMusicVolume()
    {
        return Backing.GetFloat($"slot{currentSlotNumber}_musicVolume", 1);
    }

    public static void SetMusicVolume(float value)
    {
        Audio.instance.NotifyVolumeChanged();
        Backing.SetFloat($"slot{currentSlotNumber}_musicVolume", value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool IsHapticFeedbackEnabled()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_hapticFeedbackEnabled", 1) != 0;
    }

    public static void SetHapticFeedbackEnabled(bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_hapticFeedbackEnabled", value ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool IsItemPurchased(ShopItem item)
    {
        return Backing.GetInt($"slot{currentSlotNumber}_purchased" + IdForShopItem(item), 0) != 0;
    }

    public static void SetItemPurchased(ShopItem item, bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_purchased" + IdForShopItem(item), value ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool IsItemNew(ShopItem item)
    {
        return Backing.GetInt($"slot{currentSlotNumber}_new" + IdForShopItem(item), 1) != 0;
    }

    public static void SetItemNew(ShopItem item, bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_new" + IdForShopItem(item), value ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool IsCharacterPurchased(Character character)
    {
        return character switch
        {
            Character.Yolk   => true,
            Character.Sprout => IsItemPurchased(ShopItem.Sprout),
            Character.Goop   => IsItemPurchased(ShopItem.Goop),
            Character.Puffer => IsItemPurchased(ShopItem.Puffer),
            Character.King   => IsItemPurchased(ShopItem.King),
            _                => throw new NotImplementedException()
        };
    }

    public static bool IsInfiniteJumpPurchased()
    {
        return IsItemPurchased(ShopItem.PowerupInfiniteJump);
    }

    public static bool RecievedInfiniteJump()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_recievedInfiniteJump", 0) == 1;
    }

    public static void SetRecievedInfiniteJump(bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_recievedInfiniteJump", value ? 1 : 0);
        Backing.Save();
    }

    public static bool IsSwordAndShieldPurchased()
    {
        return IsItemPurchased(ShopItem.PowerupSwordAndShield);
    }

    public static bool RecievedSwordAndShield()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_recievedSwordAndShield", 0) == 1;
    }

    public static void SetRecievedSwordAndShield(bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_recievedSwordAndShield", value ? 1 : 0);
        Backing.Save();
    }

    public static bool IsMidasTouchPurchased()
    {
        return IsItemPurchased(ShopItem.PowerupMidasTouch);
    }

    public static bool RecievedMidasTouch()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_recievedMidasTouch", 0) == 1;
    }

    public static void SetRecievedMidasTouch(bool value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_recievedMidasTouch", value ? 1 : 0);
        Backing.Save();
    }

    public static (int matchId, Vector2 position)? GetSavedGamekitMultiplayerState()
    {
        int matchId = Backing.GetInt("gamekitMultiplayerMatchId");
        float x = Backing.GetFloat("gamekitMultiplayerX");
        float y = Backing.GetFloat("gamekitMultiplayerY");
        if (matchId != 0)
            return (matchId, new Vector2(x, y));
        else
            return null;
    }

    public static void SetSavedGamekitMultiplayerState(int matchId, Vector2 position)
    {
        Backing.SetInt("gamekitMultiplayerMatchId", matchId);
        Backing.SetFloat("gamekitMultiplayerX", position.x);
        Backing.SetFloat("gamekitMultiplayerY", position.y);
    }

    public static bool IsGameFirstRun()
    {
        return Backing.GetInt("gameFirstRun", 1) == 1;
    }

    public static void SetGameFirstRun(bool value)
    {
        Backing.SetInt("gameFirstRun", value ? 1 : 0);
        Backing.Save();
    }

    public static bool HasSeenWhatsNew()
    {
        return Backing.GetString("whatsNewKey", "") == WhatsNewKey;
    }

    public static void SetSeenWhatsNew(bool value)
    {
        if (value == true)
            Backing.SetString("whatsNewKey", WhatsNewKey);
        else
            Backing.DeleteKey("whatsNewKey");

        Backing.Save();
    }

    public static bool IsDontShowPlushToyAd()
    {
        return Backing.GetInt("dontShowPlushToyAd", 0) == 1;
    }

    public static void SetDontShowPlushToyAd(bool value)
    {
        if(value == true)
            Backing.SetInt("dontShowPlushToyAd", 1);
        else
            Backing.DeleteKey("dontShowPlushToyAd");

        Backing.Save();
    }

    public static bool WasRatingPopUpDisplayed()
    {
        return Backing.GetInt("ratingPopUpDisplayed", 0) == 1;
    }

    public static void SetRatingPopUpDisplayed(bool value)
    {
        Backing.SetInt("ratingPopUpDisplayed", value ? 1 : 0);
        Backing.Save();
    }

    public static Localization.LanguageId? GetSelectedLanguage()
    {
        var code = Backing.GetString($"slot{currentSlotNumber}_selectedLanguage", "");
        return code switch
        {
            "ar"      => LanguageId.Arabic,
            "zh-Hans" => LanguageId.ChineseSimplified,
            "zh-Hant" => LanguageId.ChineseTraditional,
            "nl"      => LanguageId.Dutch,
            "en-GB"   => LanguageId.EnglishUK,
            "en"      => LanguageId.EnglishUS,
            "fr"      => LanguageId.French,
            "fr-CA"   => LanguageId.FrenchCanadian,
            "de"      => LanguageId.German,
            "it"      => LanguageId.Italian,
            "ja"      => LanguageId.Japanese,
            "ko"      => LanguageId.Korean,
            "pt-BR"   => LanguageId.Portuguese,
            "ru"      => LanguageId.Russian,
            "es"      => LanguageId.Spanish,
            "tr"      => LanguageId.Turkish,
            _         => null
        };
    }

    public static void SetSelectedLanguage(Localization.LanguageId languageId)
    {
        #if (UNITY_EDITOR || UNITY_STANDALONE_WIN) && STEAMWORKS_NET
        {
            /* If the language being saved matches the one in Steam's settings,
               then clear this field in the save data, so that future changes
               to either setting will take effect. */

            var d = Localization.DefaultLanguageAccordingToSteam();
            if (d != null && d == languageId)
            {
                Backing.DeleteKey($"slot{currentSlotNumber}_selectedLanguage");
                UpdateTimestampAndSave(currentSlotNumber);
                return;
            }
        }
        #endif

        var code = languageId switch
        {
            LanguageId.Arabic             => "ar",
            LanguageId.ChineseSimplified  => "zh-Hans",
            LanguageId.ChineseTraditional => "zh-Hant",
            LanguageId.Dutch              => "nl",
            LanguageId.EnglishUK          => "en-GB",
            LanguageId.EnglishUS          => "en",
            LanguageId.French             => "fr",
            LanguageId.FrenchCanadian     => "fr-CA",
            LanguageId.German             => "de",
            LanguageId.Italian            => "it",
            LanguageId.Japanese           => "ja",
            LanguageId.Korean             => "ko",
            LanguageId.Portuguese         => "pt-BR",
            LanguageId.Russian            => "ru",
            LanguageId.Spanish            => "es",
            LanguageId.Turkish            => "tr",
            _                             => ""
        };
        Backing.SetString($"slot{currentSlotNumber}_selectedLanguage", code);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static int DateToInt(DateTime dateTime)
    {
        int.TryParse(dateTime.ToString(DateFormat), out int result);
        return result;
    }

    public static string TimeToString(float time)
    {
        if(time < 0f)
        {
            time = 0f;
        }
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        return string.Format("{0:00}.{1:00}", minutes, seconds);
    }

    public static string TimeToStringFractions(float time)
    {
        if(time < 0f)
        {
            time = 0f;
        }
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        int fractionTwoDigits = (int)(time * 100) % 100;
        return string.Format("{0:00}:{1:00}:{2:00}", minutes, seconds, fractionTwoDigits);
    }

    public static string TimeToStringSmallMinutes(float time)
    {
        if(time < 0f)
        {
            time = 0f;
        }
        int minutes = (int)time / 60;
        int seconds = (int)time % 60;
        return string.Format("{0:0}.{1:00}", minutes, seconds);
    }

    public static bool IsDeveloperModeEnabled()
    {
        return Backing.GetInt("devModeEnabled", 1) == 1;
    }

    public static void SetDeveloperModeEnabled(bool value)
    {
        Backing.SetInt("devModeEnabled", value ? 1 : 0);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static int GetFuturePlayOffset()
    {
        return Backing.GetInt($"slot{currentSlotNumber}_futurePlayOffset", 0);
    }

    public static void SetFuturePlayOffset(int value)
    {
        Backing.SetInt($"slot{currentSlotNumber}_futurePlayOffset", value);
        UpdateTimestampAndSave(currentSlotNumber);
    }

    public static bool DoesSlotExist(int slot)
    {
        return Backing.HasKey($"slot{slot}_slotName");
    }
    
    public static string GetSlotName(int slot)
    {
        return Backing.GetString($"slot{slot}_slotName", "");
    }

    public static void SetSlotName(int slot, string name)
    {
        Backing.SetString($"slot{slot}_slotName", name);
    }

    public static (int started, int completed) GetDaysStartedAndCompleted(int slot)
    {
        var date = Game.DateOfOpeningTutorial;
        int started = 0, completed = 0;

        while (date <= Game.DateLastAllowed)
        {
            string key = $"slot{slot}_recordFor" + date.ToString("yyyyMM");

            if (Backing.HasKey(key) == true)
            {
                string monthInfo = Backing.GetString(key, "");
                string[] daysInfo = monthInfo.Split('|');
                int daysInfoIndex = 0;
            
                while (daysInfoIndex < daysInfo.Length)
                {
                    string dayInfo = daysInfo[daysInfoIndex];
                    string[] elements = dayInfo.Split(' ');
                    if(
                        elements.Length == 3 &&
                        int.TryParse(elements[0], out var trophy) &&
                        float.TryParse(elements[1], out var timeInSeconds) &&
                        int.TryParse(elements[2], out var deaths)
                    )
                    {
                        if (trophy >= (int)Trophy.Gold) completed += 1;
                        started += 1;
                    }
                    daysInfoIndex += 1;
                }
            }

            date = CalendarMonth.LastDayOfMonth(date).AddDays(1);
        }
        return (started, completed);
    }

    public static List<RecordForDate> GetAllRecordForDates()
    {
        var list = new List<RecordForDate>();
        var date = Game.DateOfOpeningTutorial;

        while(date <= Game.DateLastAllowed)
        {
            string key = $"slot{currentSlotNumber}_recordFor" + date.ToString("yyyyMM");

            if(Backing.HasKey(key) == true)
            {
                string monthInfo = Backing.GetString(key, "");
                string[] daysInfo = monthInfo.Split('|');
                int daysInfoIndex = 0;

                while(daysInfoIndex < daysInfo.Length)
                {
                    string dayInfo = daysInfo[daysInfoIndex];
                    string[] elements = dayInfo.Split(' ');
                    if(
                        elements.Length == 3 &&
                        int.TryParse(elements[0], out var trophy) &&
                        float.TryParse(elements[1], out var timeInSeconds) &&
                        int.TryParse(elements[2], out var deaths)
                    )
                    {
                        list.Add(new RecordForDate
                        {
                            hasPlayedAtAll = true,
                            trophy = (Trophy)trophy,
                            timeInSeconds = timeInSeconds,
                            deaths = deaths
                        });
                    }
                    daysInfoIndex += 1;
                }
            }
            date = CalendarMonth.LastDayOfMonth(date).AddDays(1);
        }
        return list;
    }

    public static int GetNumberOfCompletedDays()
    {
        int completedDays = 0;
        var records = GetAllRecordForDates();
        
        foreach(var record in records)
        {
            if(record.trophy >= SaveData.Trophy.Gold)
            {
                completedDays += 1;
            }
        }

        return completedDays;
    }

    public static DateTime? GetLastSaveTimestamp(int slot)
    {
        string str = Backing.GetString($"slot{slot}_timestamp");
        if (DateTime.TryParse(str, out var result))
            return result;
        else
            return null;
    }

    public static void UpdateTimestampAndSave(int slot)
    {
        Backing.SetString($"slot{slot}_timestamp", DateTime.Now.ToString("u"));
        Backing.Save();
    }

    public static void DeleteSaveSlot(int slot)
    {
        var list = new List<string>
        {
            "completedOpeningTutorial",
            "selectedCharacter",
            "coinsDonatedToCharity",
            "coins",
            "sfxVolume",
            "musicVolume",
            "hapticFeedbackEnabled",
            "selectedLanguage",
            "slotName",
            "timestamp"
        };

        var items = (ShopItem[])Enum.GetValues(typeof(ShopItem));
        foreach(var shopItem in items)
            list.Add("purchased" + IdForShopItem(shopItem));

        var date = Game.DateOfOpeningTutorial;
        while(date <= Game.DateLastAllowed)
        {
            var str = "recordFor" + date.ToString("yyyyMM");
            if(Backing.HasKey($"slot{slot}_{str}"))
                list.Add(str);
            date = CalendarMonth.LastDayOfMonth(date).AddDays(1);
        }

        date = Game.DateOfOpeningTutorial;
        while(date <= Game.DateLastAllowed)
        {
            var str = "latestAttempt" + date.ToString(DateFormat);
            if(Backing.HasKey($"slot{slot}_{str}"))
                list.Add(str);
            date = date.AddDays(1);
        }

        var types = (minigame.Minigame.Type[])Enum.GetValues(typeof(minigame.Minigame.Type));
        foreach(var type in types)
        {
            var diffs = (MinigameDifficulty[])Enum.GetValues(typeof(MinigameDifficulty));
            foreach(var diff in diffs)
            {
                for(int i = 0; i < 5; i++)
                {
                    var str = "minigameCompleted{type}{diff}{i}";
                    if(Backing.HasKey($"slot{slot}_{str}"))
                        list.Add(str);
                }
            }
        }

        foreach (var k in list)
            Backing.DeleteKey($"slot{slot}_{k}");

        Backing.SetInt($"slot{slot}_recievedInfiniteJump", 0);
        Backing.SetInt($"slot{slot}_recievedSwordAndShield", 0);
        Backing.SetInt($"slot{slot}_recievedMidasTouch", 0);

        Backing.Save();
    }

    public static string GetLevelPatch()
    {
        return Backing.GetString("levelPatch");
    }

    public static void SetLevelPatch(string text)
    {
        Backing.SetString("levelPatch", text);
    }
}
