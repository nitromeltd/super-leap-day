public interface SaveDataBacking
{
    public bool HasKey(string key);
    public void DeleteKey(string key);

    public int GetInt(string key, int defaultValue = 0);
    public bool GetBool(string key, bool defaultValue = false);
    public float GetFloat(string key, float defaultValue = 0f);
    public string GetString(string key, string defaultValue = "");

    public void SetInt(string key, int value);
    public void SetBool(string key, bool value);
    public void SetFloat(string key, float value);
    public void SetString(string key, string value);

    public void Save();
}
