using UnityEngine;

public class SaveDataPlayerPrefs : SaveDataBacking
{
    public bool HasKey(string key) => PlayerPrefs.HasKey(key);
    public void DeleteKey(string key) => PlayerPrefs.DeleteKey(key);

    public int GetInt(string key, int defaultValue)
        => PlayerPrefs.GetInt(key, defaultValue);
    public bool GetBool(string key, bool defaultValue)
        => PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) != 0;
    public float GetFloat(string key, float defaultValue)
        => PlayerPrefs.GetFloat(key, defaultValue);
    public string GetString(string key, string defaultValue)
        => PlayerPrefs.GetString(key, defaultValue);

    public void SetInt(string key, int value)
        => PlayerPrefs.SetInt(key, value);
    public void SetBool(string key, bool value)
        => PlayerPrefs.SetInt(key, value ? 1 : 0);
    public void SetFloat(string key, float value)
        => PlayerPrefs.SetFloat(key, value);
    public void SetString(string key, string value)
        => PlayerPrefs.SetString(key, value);

    public void Save() => PlayerPrefs.Save();
}
