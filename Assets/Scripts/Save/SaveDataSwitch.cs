#if UNITY_SWITCH

using nn.account;
using nn.fs;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/*
    In NMETA file, we need
    
        <StartupUserAccount>Required</StartupUserAccount>
        so that a user account is selected on start-up.

        <UserAccountSaveDataSize> and <UserAccountSaveDataJournalSize>
        to both be greater than zero, so that save-data space is allocated.
*/

public class SaveDataSwitch : SaveDataBacking
{
    private const string mountName = "saveData";
    private const string saveFilename = "save.dat";

    private Uid userId;
    private FileHandle fileHandle;
    private Dictionary<string, object> dictionary;

    public SaveDataSwitch()
    {
        Switch.Initialize();
        Account.GetUserId(ref this.userId, Switch.userHandle);

        nn.fs.SaveData.Ensure(userId);

        var result = nn.fs.SaveData.Mount(mountName, userId);
        if (result.IsSuccess() == false)
        {
            Debug.LogError(
                $"[SaveDataSwitch] nn.fs.SaveData.Mount() failed with result {result}"
            );
            result.abortUnlessSuccess();
        }

        this.dictionary = new();
        Load();
    }

    private void Load()
    {
        this.dictionary.Clear();

        nn.Result result;
        EntryType entryType = default;
        var folder = $"{mountName}:/";
        var fullPath = $"{mountName}:/{saveFilename}";

        result = FileSystem.GetEntryType(ref entryType, folder);
        if (result.IsSuccess() == false)
        {
            Debug.LogError("[SaveDataSwitch] Couldn't access save storage.");
            result.abortUnlessSuccess();
        }

        result = FileSystem.GetEntryType(ref entryType, fullPath);
        if (result.IsSuccess() == false)
        {
            // don't crash in this case, it's fine.
            Debug.LogWarning("[SaveDataSwitch] No save data found, starting anew.");
            return;
        }

        result = File.Open(ref this.fileHandle, fullPath, OpenFileMode.Read);
        if (result.IsSuccess() == false)
        {
            Debug.LogWarning(
                $"[SaveDataSwitch] Loading save-data failed with result {result}"
            );
            result.abortUnlessSuccess();
        }

        long size = default;
        File.GetSize(ref size, this.fileHandle);
        var data = new byte[size];
        File.Read(this.fileHandle, 0, data, size);
        File.Close(this.fileHandle);

        using var memoryStream = new System.IO.MemoryStream(data);
        using var reader = new System.IO.BinaryReader(memoryStream);

        int count = reader.ReadInt32();
        for (int n = 0; n < count; n += 1)
        {
            string key = reader.ReadString();
            int valueType = reader.ReadByte();
            object value = null;
            if (valueType == 0)
                value = reader.ReadInt32();
            else if (valueType == 1)
                value = reader.ReadBoolean();
            else if (valueType == 2)
                value = reader.ReadSingle();
            else if (valueType == 3)
                value = reader.ReadString();
            else
                Debug.LogWarning($"[SaveDataSwitch] Invalid save-data for key {key}");

            this.dictionary[key] = value;
        }

        memoryStream.Close();
    }

    public void Save()
    {
        byte[] data;

        using (var stream = new System.IO.MemoryStream(1024 * 16))
        {
            var writer = new System.IO.BinaryWriter(stream);
            writer.Write(this.dictionary.Count);

            foreach (var (key, value) in this.dictionary)
            {
                writer.Write(key);

                if (value is int)
                {
                    writer.Write((byte)0);
                    writer.Write((int)value);
                }
                else if (value is bool)
                {
                    writer.Write((byte)1);
                    writer.Write((bool)value);
                }
                else if (value is float)
                {
                    writer.Write((byte)2);
                    writer.Write((float)value);
                }
                else if (value is string)
                {
                    writer.Write((byte)3);
                    writer.Write((string)value);
                }
                else
                {
                    // invalid data, but at least we won't lose our place when reading
                    writer.Write((byte)255);
                }
            }

            data = stream.GetBuffer().Take((int)stream.Position).ToArray();
            stream.Close();
        }

        UnityEngine.Switch.Notification.EnterExitRequestHandlingSection();

        var fullPath = $"{mountName}:/{saveFilename}";
        File.Delete(fullPath);
        File.Create(fullPath, data.Length);
        File.Open(ref this.fileHandle, fullPath, OpenFileMode.Write);
        File.Write(this.fileHandle, 0, data, data.LongLength, WriteOption.Flush);
        File.Close(this.fileHandle);
        FileSystem.Commit(mountName);

        UnityEngine.Switch.Notification.LeaveExitRequestHandlingSection();
    }

    public bool HasKey(string key) => this.dictionary.ContainsKey(key);
    public void DeleteKey(string key) => this.dictionary.Remove(key);

    public int GetInt(string key, int defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is int valueAsInt)
            return valueAsInt;
        else
            return defaultValue;
    }

    public bool GetBool(string key, bool defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is bool valueAsBool)
            return valueAsBool;
        else
            return defaultValue;
    }

    public float GetFloat(string key, float defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is float valueAsFloat)
            return valueAsFloat;
        else
            return defaultValue;
    }

    public string GetString(string key, string defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is string valueAsString)
            return valueAsString;
        else
            return defaultValue;
    }

    public void SetInt(string key, int value) => this.dictionary[key] = value;
    public void SetBool(string key, bool value) => this.dictionary[key] = value;
    public void SetFloat(string key, float value) => this.dictionary[key] = value;
    public void SetString(string key, string value) => this.dictionary[key] = value;
}

#endif
