using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

/*
    On Windows, PlayerPrefs stores to the registry, which is no good for cloud saving.
    So, we save to a file, and then we can enable Steam Auto-Cloud on that file.
*/

public class SaveDataToFile : SaveDataBacking
{
    private Dictionary<string, object> dictionary;

    public SaveDataToFile()
    {
        this.dictionary = new();
        Load();
    }

    private string FullPathForSaveData() => Path.Combine(
        Application.persistentDataPath, /* %USERPROFILE%/AppData/LocalLow/Nitrome */
        "save.dat"
    );
    
    private void Load()
    {
        this.dictionary.Clear();

        var fullPath = FullPathForSaveData();
        byte[] data;
        try
        {
            data = File.ReadAllBytes(fullPath);
        }
        catch (Exception)
        {
            Debug.LogWarning("[SaveDataToFile] Unable to load save data, starting anew.");
            return;
        }

        using var memoryStream = new System.IO.MemoryStream(data);
        using var reader = new System.IO.BinaryReader(memoryStream);

        int count = reader.ReadInt32();
        for (int n = 0; n < count; n += 1)
        {
            string key = reader.ReadString();
            int valueType = reader.ReadByte();
            object value = null;
            if (valueType == 0)
                value = reader.ReadInt32();
            else if (valueType == 1)
                value = reader.ReadBoolean();
            else if (valueType == 2)
                value = reader.ReadSingle();
            else if (valueType == 3)
                value = reader.ReadString();
            else
                Debug.LogWarning($"[SaveDataToFile] Invalid save-data for key {key}");

            this.dictionary[key] = value;
        }

        memoryStream.Close();
    }

    public void Save()
    {
        byte[] data;

        using (var stream = new System.IO.MemoryStream(1024 * 16))
        {
            var writer = new System.IO.BinaryWriter(stream);
            writer.Write(this.dictionary.Count);

            foreach (var (key, value) in this.dictionary)
            {
                writer.Write(key);

                if (value is int)
                {
                    writer.Write((byte)0);
                    writer.Write((int)value);
                }
                else if (value is bool)
                {
                    writer.Write((byte)1);
                    writer.Write((bool)value);
                }
                else if (value is float)
                {
                    writer.Write((byte)2);
                    writer.Write((float)value);
                }
                else if (value is string)
                {
                    writer.Write((byte)3);
                    writer.Write((string)value);
                }
                else
                {
                    // invalid data, but at least we won't lose our place when reading
                    writer.Write((byte)255);
                }
            }

            data = stream.GetBuffer().Take((int)stream.Position).ToArray();
            stream.Close();
        }

        var fullPath = FullPathForSaveData();
        try
        {
            File.WriteAllBytes(fullPath, data);
        }
        catch (Exception)
        {
            Debug.LogWarning("[SaveDataToFile] Unable to write save data.");
        }
    }

    public bool HasKey(string key) => this.dictionary.ContainsKey(key);
    public void DeleteKey(string key) => this.dictionary.Remove(key);

    public int GetInt(string key, int defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is int valueAsInt)
            return valueAsInt;
        else
            return defaultValue;
    }

    public bool GetBool(string key, bool defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is bool valueAsBool)
            return valueAsBool;
        else
            return defaultValue;
    }

    public float GetFloat(string key, float defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is float valueAsFloat)
            return valueAsFloat;
        else
            return defaultValue;
    }

    public string GetString(string key, string defaultValue)
    {
        if (this.dictionary.TryGetValue(key, out var value) && value is string valueAsString)
            return valueAsString;
        else
            return defaultValue;
    }

    public void SetInt(string key, int value) => this.dictionary[key] = value;
    public void SetBool(string key, bool value) => this.dictionary[key] = value;
    public void SetFloat(string key, float value) => this.dictionary[key] = value;
    public void SetString(string key, string value) => this.dictionary[key] = value;
}
