using UnityEngine;
using System.Collections.Generic;

public class TemperatureAnimation
{
    public static readonly Color DrawingColorForNormalTiles    = Color.cyan;
    public static readonly Color DrawingColorForSecondaryTiles = Color.red;
    public static readonly Color DrawingColorForBackWall       = Color.magenta;
    public static readonly Color DrawingColorForCloudPlatform  = Color.yellow;
    public static readonly Color DrawingColorForIceBase        = Color.black;
    public static readonly Color DrawingColorForIceCover       = Color.white;

    private Map map;
    private float yPosition;
    private Map.Temperature changingFromTemperature;
    private Map.Temperature changingToTemperature;
    private List<Item> itemsToNotify;
    private Dictionary<Chunk, Map.Temperature> lastReportedTemperatureInChunk;
    private Dictionary<int, Map.Temperature> lastReportedTemperatureInTileRow;

    private Material tileMaterial;
    private MaterialPropertyBlock tileMaterialPropertyBlock;

    public TemperatureAnimation(Map map, Map.Temperature initialTemperature)
    {
        this.map = map;
        this.yPosition = this.map.yMax;
        this.changingFromTemperature = initialTemperature;
        this.changingToTemperature = initialTemperature;
        this.itemsToNotify = new List<Item>();

        this.lastReportedTemperatureInChunk = new Dictionary<Chunk, Map.Temperature>();
        this.lastReportedTemperatureInTileRow = new Dictionary<int, Map.Temperature>();

        foreach (var chunk in this.map.chunks)
        {
            this.lastReportedTemperatureInChunk[chunk] = initialTemperature;
            foreach (var item in chunk.items)
            {
                item.temperature = initialTemperature;
            }
        }

        for (int ty = this.map.yMin; ty <= this.map.yMax; ty += 1)
        {
            this.lastReportedTemperatureInTileRow[ty] = Map.Temperature.None;
        }

        this.tileMaterial = new Material(Assets.instance.tileTemperatureChangeMaterial);
        SetupTemperatureChangeMaterial(this.tileMaterial);
        this.tileMaterialPropertyBlock = new MaterialPropertyBlock();
    }

    public static void SetupTemperatureChangeMaterial(Material material)
    {
        material.SetFloatArray("_ColourBalanceRed", new float[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 1, 6, 14, 18, 23, 28, 31, 35, 40, 43, 46, 50, 52,
            54, 58, 60, 62, 64, 66, 68, 71, 72, 73, 74, 76, 79, 80, 81, 83, 84,
            86, 88, 89, 91, 92, 93, 95, 97, 99, 101, 102, 103, 104, 106, 107, 109,
            110, 111, 112, 115, 116, 117, 118, 120, 121, 122, 123, 124, 126, 127,
            129, 130, 131, 133, 133, 135, 135, 139, 139, 140, 142, 143, 144, 145,
            147, 148, 149, 149, 152, 152, 153, 154, 156, 157, 159, 159, 161, 162,
            163, 165, 167, 168, 169, 170, 170, 173, 174, 175, 176, 176, 178, 179,
            180, 182, 182, 184, 185, 187, 188, 188, 190, 192, 193, 195, 196, 196,
            198, 198, 201, 201, 202, 205, 205, 205, 206, 209, 209, 210, 212, 213,
            214, 215, 218, 219, 219, 221, 222, 224, 225, 225, 229, 230, 232, 232,
            233, 235, 237, 238, 240, 241, 243, 243, 245, 247, 249, 250, 251, 254,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255
        });
        material.SetFloatArray("_ColourBalanceGreen", new float[] {
            0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 1, 1, 2, 2, 1, 3, 4, 4, 3, 4, 5, 6, 6,
            6, 6, 7, 6, 6, 8, 8, 10, 10, 11, 11, 12, 11, 13, 13, 15, 14, 17, 15, 17,
            18, 18, 20, 19, 20, 21, 22, 23, 22, 24, 25, 26, 27, 28, 29, 29, 29, 31,
            31, 33, 33, 34, 34, 36, 37, 38, 39, 39, 39, 41, 41, 43, 43, 45, 46, 47,
            48, 49, 50, 50, 51, 51, 54, 54, 54, 56, 58, 58, 59, 59, 60, 61, 62, 65,
            66, 66, 67, 68, 69, 70, 71, 71, 72, 73, 74, 75, 76, 76, 79, 80, 81, 81,
            83, 84, 85, 86, 87, 89, 89, 90, 91, 93, 93, 95, 95, 96, 97, 98, 100,
            102, 103, 103, 104, 105, 107, 107, 110, 111, 111, 112, 113, 116, 116,
            117, 119, 118, 120, 122, 123, 124, 124, 126, 127, 128, 129, 130, 132,
            132, 133, 136, 136, 138, 139, 141, 141, 143, 144, 145, 147, 147, 149,
            150, 152, 153, 154, 156, 157, 157, 159, 160, 161, 162, 163, 165, 166,
            167, 169, 169, 172, 172, 174, 175, 177, 178, 179, 181, 181, 182, 183,
            185, 186, 188, 188, 189, 191, 192, 195, 195, 197, 198, 199, 200, 202,
            203, 205, 206, 207, 208, 209, 211, 213, 214, 215, 217, 217, 218, 219,
            221, 221, 224, 225, 226, 230, 231, 233, 233, 235, 237, 237, 238, 240,
            242, 242, 245, 246, 247, 250, 251, 253, 253, 255, 255
        });
        material.SetFloatArray("_ColourBalanceBlue", new float[] {
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 8, 11,
            12, 15, 18, 21, 23, 24, 28, 28, 30, 31, 34, 34, 38, 39, 42, 42, 45, 47,
            47, 49, 51, 52, 55, 55, 57, 58, 61, 61, 63, 65, 65, 67, 69, 69, 73, 73,
            74, 76, 76, 78, 79, 80, 82, 83, 84, 86, 87, 89, 90, 92, 93, 93, 95, 96,
            98, 99, 101, 103, 103, 104, 106, 106, 108, 110, 110, 112, 113, 115,
            116, 118, 118, 119, 121, 123, 123, 125, 126, 127, 128, 130, 131, 132,
            134, 134, 136, 138, 139, 141, 141, 143, 145, 145, 147, 148, 149, 151,
            152, 154, 154, 155, 158, 159, 159, 161, 163, 164, 167, 167, 169, 170,
            171, 172, 174, 175, 176, 177, 179, 181, 181, 184, 185, 187, 188, 188,
            191, 192, 194, 195, 197, 198, 199, 200, 203, 204, 205, 207, 208, 210,
            212, 212, 214, 217, 217, 220, 220, 223, 223, 227, 230, 230, 233, 235,
            237, 240, 241, 242, 243, 246, 251, 252, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
            255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255
        });
    }

    public void Go(Map.Temperature oldTemperature, Map.Temperature newTemperature)
    {
        FinishAnimationEarly();

        Rect cameraRect = this.map.gameCamera.VisibleRectangle();
        this.yPosition = Mathf.FloorToInt(cameraRect.yMin);
        this.itemsToNotify = new List<Item>();
        this.changingFromTemperature = oldTemperature;
        this.changingToTemperature = newTemperature;

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.gameObject.activeSelf == false)
                continue;
            if (this.lastReportedTemperatureInChunk[chunk] == newTemperature)
                continue;

            this.itemsToNotify.AddRange(chunk.items);
            this.lastReportedTemperatureInChunk[chunk] = newTemperature;
        }
    }

    public void Advance()
    {
        this.yPosition += 0.6f;

        foreach (var chunk in this.map.chunks)
        {
            if (chunk.gameObject.activeSelf == false) continue;
            if (this.lastReportedTemperatureInChunk[chunk] != this.changingToTemperature)
            {
                this.lastReportedTemperatureInChunk[chunk] = this.changingToTemperature;
                foreach (var item in chunk.items)
                {
                    if (this.itemsToNotify.Contains(item))
                        continue;
                    item.temperature = this.changingToTemperature;
                    item.NotifyChangeTemperature(this.changingToTemperature);
                }
            }
        }

        foreach (var item in this.itemsToNotify.ToArray())
        {
            if (item.position.y > this.yPosition)
                continue;

            item.temperature = this.changingToTemperature;
            item.NotifyChangeTemperature(this.changingToTemperature);
            this.itemsToNotify.Remove(item);
        }

        var cameraRect = this.map.gameCamera.VisibleRectangle();
        var cameraYBtm = Mathf.FloorToInt(cameraRect.yMin - 1);
        var cameraYTop = Mathf.FloorToInt(cameraRect.yMax + 1);

        var spriteRenderers = new List<SpriteRenderer>();
        var meshRenderers = new List<MeshRenderer>();
        for (int ty = cameraYBtm; ty <= cameraYTop; ty += 1)
        {
            if (ty < this.map.yMin) continue;
            if (ty > this.map.yMax) continue;

            var rowTemperature = Map.Temperature.None;
            if (ty < this.yPosition - 3.5f) rowTemperature = this.changingToTemperature;
            if (ty > this.yPosition + 0.5f) rowTemperature = this.changingFromTemperature;

            if (rowTemperature == Map.Temperature.None ||
                rowTemperature != lastReportedTemperatureInTileRow[ty])
            {
                FindRenderersInRow(ty, spriteRenderers, meshRenderers);
                lastReportedTemperatureInTileRow[ty] = rowTemperature;
            }
        }

        float yForCold, yForHot;
        if (this.changingToTemperature == Map.Temperature.Hot)
            (yForCold, yForHot) = (this.yPosition, this.yPosition - 2);
        else
            (yForHot, yForCold) = (this.yPosition, this.yPosition - 2);

        foreach (var sr in spriteRenderers)
        {
            if (this.tileMaterial.mainTexture == null)
                this.tileMaterial.mainTexture = sr.sprite.texture;

            sr.material = this.tileMaterial;
            sr.GetPropertyBlock(this.tileMaterialPropertyBlock);
            this.tileMaterialPropertyBlock.SetFloat("_YForCold", yForCold);
            this.tileMaterialPropertyBlock.SetFloat("_YForHot", yForHot);
            sr.SetPropertyBlock(this.tileMaterialPropertyBlock);
        }
        foreach (var mr in meshRenderers)
        {
            if (this.tileMaterial.mainTexture == null)
                this.tileMaterial.mainTexture = mr.material.mainTexture;

            mr.material = this.tileMaterial;
            mr.GetPropertyBlock(this.tileMaterialPropertyBlock);
            this.tileMaterialPropertyBlock.SetFloat("_YForCold", yForCold);
            this.tileMaterialPropertyBlock.SetFloat("_YForHot", yForHot);
            mr.SetPropertyBlock(this.tileMaterialPropertyBlock);
        }
    }

    public void AddTemperatureMaterialProperties(MeshRenderer mr)
    {
        float yForCold, yForHot;
        if (this.changingToTemperature == Map.Temperature.Hot)
            (yForCold, yForHot) = (this.yPosition, this.yPosition - 2);
        else
            (yForHot, yForCold) = (this.yPosition, this.yPosition - 2);

        mr.GetPropertyBlock(this.tileMaterialPropertyBlock);
        this.tileMaterialPropertyBlock.SetFloat("_YForCold", yForCold);
        this.tileMaterialPropertyBlock.SetFloat("_YForHot", yForHot);
        mr.SetPropertyBlock(this.tileMaterialPropertyBlock);
    }

    private void FindRenderersInRow(
        int ty,
        List<SpriteRenderer> outSpriteRenderers,
        List<MeshRenderer> outMeshRenderers
    )
    {
        var chunks = this.map.ChunksAtTy(ty);

        foreach (var chunk in chunks)
        {
            foreach (var grid in chunk.solidTilesA)
                CheckGrid(grid, false);
            foreach (var grid in chunk.solidTilesB)
                CheckGrid(grid, false);

            CheckGrid(chunk.backTileGrid1, true);
            CheckGrid(chunk.backTileGrid2, true);

            if (chunk.extraSpriteRenderersForTransitionalTunnel != null)
            {
                foreach (var sr in chunk.extraSpriteRenderersForTransitionalTunnel)
                    AddSR(sr, DrawingColorForBackWall);
            }
        }

        void AddSR(SpriteRenderer sr, Color color)
        {
            if (sr == null) return;
            sr.color = color;
            outSpriteRenderers.Add(sr);
        }
        void AddMR(MeshRenderer mr)
        {
            if (mr == null) return;
            outMeshRenderers.Add(mr);
        }
        void CheckGrid(TileGrid grid, bool isBackWall)
        {
            if (grid.tileGridMesh != null)
                AddMR(grid.tileGridMesh.meshRendererForThemedTiles);

            if (grid.yMin > ty) return;
            if (grid.yMax < ty) return;

            for (int tx = grid.xMin; tx <= grid.xMax; tx += 1)
            {
                var t = grid.GetTile(tx - grid.xMin, ty - grid.yMin);
                if (t == null) continue;

                {
                    var color = DrawingColorForNormalTiles;
                    if (t.spec.shape == Tile.Shape.TopLine)
                        color = DrawingColorForCloudPlatform;
                    else if (isBackWall == true)
                        color = DrawingColorForBackWall;
                    else if (t.ice == true)
                        color = DrawingColorForIceBase;
                    else if (t.secondary == true)
                        color = DrawingColorForSecondaryTiles;

                    AddSR(t.spriteRenderer, color);
                    AddSR(t.interior?.GetComponent<SpriteRenderer>(), color);
                    AddMR(t.interior?.GetComponent<MeshRenderer>());
                }

                {
                    var color = DrawingColorForIceCover;
                    AddSR(t.spriteRendererIceCover, color);
                    AddSR(t.interiorIceCover?.GetComponent<SpriteRenderer>(), color);
                    AddMR(t.interiorIceCover?.GetComponent<MeshRenderer>());
                }
            }
        }
    }

    private void FinishAnimationEarly()
    {
        foreach (var item in this.itemsToNotify)
        {
            item.temperature = this.changingToTemperature;
            item.NotifyChangeTemperature(this.changingToTemperature);
        }
        this.itemsToNotify.Clear();
    }
}
