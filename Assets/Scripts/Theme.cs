public enum Theme
{
    RainyRuins,
    CapitalHighway,
    ConflictCanyon,
    HotNColdSprings,
    GravityGalaxy,
    SunkenIsland,
    TreasureMines,
    WindySkies,
    TombstoneHill,
    MoltenFortress,
    VRTrainingRoom = 1000,
    OpeningTutorial = 1001
}

public static class Themes
{
    public static readonly Theme[] all = new Theme[]
    {
        Theme.RainyRuins,
        Theme.CapitalHighway,
        Theme.ConflictCanyon,
        Theme.HotNColdSprings,
        Theme.GravityGalaxy,
        Theme.SunkenIsland,
        Theme.TreasureMines,
        Theme.WindySkies,
        Theme.TombstoneHill,
        Theme.MoltenFortress
    };

    public static string CleanName(this Theme theme) => theme switch
    {
        Theme.RainyRuins      => "Rainy Ruins",
        Theme.CapitalHighway  => "Capital Highway",
        Theme.ConflictCanyon  => "Conflict Canyon",
        Theme.HotNColdSprings => "Hot 'n Cold Springs",
        Theme.GravityGalaxy   => "Gravity Galaxy",
        Theme.SunkenIsland    => "Sunken Island",
        Theme.TreasureMines   => "Treasure Mines",
        Theme.WindySkies      => "Windy Skies",
        Theme.TombstoneHill   => "Tombstone Hill",
        Theme.MoltenFortress  => "Molten Fortress",
        _ => null
    };
}
