using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationAnimatedFan : MonoBehaviour
{
    public Animated animated;

    public Animated.Animation animationIdle;

    void Start()
    {
        this.animated.PlayAndLoop(this.animationIdle);
    }
 
}
