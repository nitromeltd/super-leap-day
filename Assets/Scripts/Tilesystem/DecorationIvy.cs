using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationIvy : MonoBehaviour
{
    public Sprite top;
    public SpriteRenderer first, second, third, forth;
    private void Start()
    {
        switch(Random.Range(1, 4))
        {
            case 1:
                first.sprite = top;
                second.gameObject.SetActive(false);
                third.gameObject.SetActive(false);
                forth.gameObject.SetActive(false);
                break;

            case 2:
                second.sprite = top;
                third.gameObject.SetActive(false);
                forth.gameObject.SetActive(false);
                break;

            case 3:
                third.sprite = top;
                forth.gameObject.SetActive(false);
                break;

            case 4:
                break;
        }
    }
}
