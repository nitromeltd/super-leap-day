using UnityEngine;

public class DecorationLantern : MonoBehaviour
{
    private Map map;
    public Transform glow;

    public void Start()
    {
        this.map = this.transform.GetComponentInParent<Map>();
    }

    public void Update()
    {
        if (this.map == null)
            Start();

        float scale =
            Random.Range(0.97f, 1.0f) *
            (1 + (0.01f * Mathf.Sin(this.map.frameNumber))) *
            (1 + (0.03f * Mathf.Sin(this.map.frameNumber * 0.04f)));
        this.glow.transform.localScale = new Vector3(scale, scale, 1);
    }
}
