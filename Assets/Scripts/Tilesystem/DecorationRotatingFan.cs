using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecorationRotatingFan : MonoBehaviour
{
    private float rotationSpeed;
    void Start()
    {
        this.rotationSpeed = Random.Range(0.75f, 2f) * Mathf.Sign(Random.Range(-1,1));
    }

    void Update()
    {
        this.transform.Rotate(0, 0, rotationSpeed);
    }
}
