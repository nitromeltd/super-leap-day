using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateDecorationElement : MonoBehaviour
{
    public float speed;

    private void Update()
    {
        this.transform.Rotate(Vector3.forward * this.speed * Time.deltaTime);   
    }
}
