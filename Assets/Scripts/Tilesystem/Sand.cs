using UnityEngine;

public class Sand : MonoBehaviour
{
    public static void AdvanceSand(Chunk chunk)
    {
        var player = chunk.map.player;
        var playerFeet = player.position.Add(0, -Player.PivotDistanceAboveFeet - 0.1f);
        if (chunk.map.NearestChunkTo(playerFeet) != chunk)
            return;

        var raycast = new Raycast(chunk.map, player.position);
        var playerLow = player.position.Add(0, -Player.PivotDistanceAboveFeet * 0.5f);
        var result = raycast.Vertical(playerLow, false, 2);
        if (result.tile == null)
            return;

        if (result.tile.sand.first != null)
        {
            var centerX = result.tile.sand.first.transform.position.x + 0.5f;
            if (Mathf.Abs(player.position.x - centerX) <= 0.5f)
                result.tile.sand.first.gameObject.SetActive(false);
        }

        if (result.tile.sand.second != null)
        {
            var centerX = result.tile.sand.second.transform.position.x + 0.5f;
            if (Mathf.Abs(player.position.x - centerX) <= 0.5f)
                result.tile.sand.second.gameObject.SetActive(false);
        }
    }
}
