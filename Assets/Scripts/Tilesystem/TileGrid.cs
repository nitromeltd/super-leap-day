using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public partial class TileGrid
{
    private Map map;
    public Chunk containerChunk;
    public Vector2 offset;
    public Vector2 offsetLastFrame;
    public Vector2 effectiveVelocity;
    public int xMin;
    public int xMax;
    public int yMin;
    public int yMax;
    public int columns;
    public int rows;
    public Tile[] tiles;
    public SpriteRenderer[] tilePatterns;
    public TileGridMesh tileGridMesh;
    public GroupLayer.Group group;

    public static TileGrid CreateSameSizeAs(TileGrid other)
    {
        var result = new TileGrid();
        result.map          = other.map;
        result.xMin         = other.xMin;
        result.xMax         = other.xMax;
        result.yMin         = other.yMin;
        result.yMax         = other.yMax;
        result.columns      = other.columns;
        result.rows         = other.rows;
        result.tiles        = new Tile[other.tiles.Length];
        result.tilePatterns = new SpriteRenderer[other.tilePatterns.Length];
        return result;
    }

    public void InitBlank(int columns, int rows)
    {
        this.columns = columns;
        this.rows = rows;
        this.tiles = new Tile[columns * rows];
    }

    public void Init(
        int tileLayerColumns,
        int tileLayerRows,
        NitromeEditor.TileLayer tileLayer,
        Chunk containerChunk
    )
    {
        var boundaryMarkers = containerChunk.boundaryMarkers;

        this.map            = containerChunk.map;
        this.containerChunk = containerChunk;
        this.xMin           = containerChunk.xMin - boundaryMarkers.bottomLeft.x;
        this.yMin           = containerChunk.yMin - boundaryMarkers.bottomLeft.y;
        this.xMax           = this.xMin + tileLayerColumns - 1;
        this.yMax           = this.yMin + tileLayerRows - 1;
        this.columns        = tileLayerColumns;
        this.rows           = tileLayerRows;
        this.tiles          = new Tile[this.columns * this.rows];
        this.tilePatterns   = new SpriteRenderer[this.columns * this.rows];

        if (tileLayer == null)
            return;

        for (int y = 0; y < rows; y += 1)
        {
            for (int x = 0; x < columns; x += 1)
            {
                int n = x + (y * this.columns);
                var tData = tileLayer.tiles[n];
                if (tData.tile == null) continue;
                if (tData.tile == "") continue;

                int mapTx = x + this.xMin;
                int mapTy = y + this.yMin;

                var tile = new Tile();
                tile.Init(mapTx, mapTy, x, y, tData);
                tile.tileGrid = this;

                // this is just if the tile changes its own tx (can happen if it flips)
                int gridTx = tile.mapTx - this.xMin;
                int gridTy = tile.mapTy - this.yMin;

                for (int dx = 0; dx < tile.spec.spanX; dx += 1)
                {
                    for (int dy = 0; dy < tile.spec.spanY; dy += 1)
                    {
                        if (tile.IncludeTileInTileGridSpace(dx, dy) == false)
                            continue;
                        this.tiles[(gridTx + dx) + ((gridTy + dy) * columns)] = tile;
                    }
                }
            }
        }
    }

    public void ShrinkToFit(
        (int xMin, int xMax, int yMin, int yMax)? minimumSize
    )
    {
        bool any = false;
        int newXMin = 0;
        int newXMax = 0;
        int newYMin = 0;
        int newYMax = 0;

        if (minimumSize != null)
        {
            newXMin = minimumSize.Value.xMin;
            newXMax = minimumSize.Value.xMax;
            newYMin = minimumSize.Value.yMin;
            newYMax = minimumSize.Value.yMax;
            any = true;
        }

        for (int y = this.yMin; y <= this.yMax; y += 1)
        {
            for (int x = this.xMin; x <= this.xMax; x += 1)
            {
                var t = this.GetTile(x - this.xMin, y - this.yMin);
                if (t == null) continue;

                if (any == false)
                {
                    newXMin = newXMax = x;
                    newYMin = newYMax = y;
                }
                else
                {
                    if (x < newXMin) newXMin = x;
                    if (x > newXMax) newXMax = x;
                    if (y < newYMin) newYMin = y;
                    if (y > newYMax) newYMax = y;
                }
                any = true;
            }
        }

        int newColumns = 1 + newXMax - newXMin;
        int newRows    = 1 + newYMax - newYMin;
        var newTiles   = new Tile[newColumns * newRows];
        
        for (int y = newYMin; y <= newYMax; y += 1)
        {
            for (int x = newXMin; x <= newXMax; x += 1)
            {
                var t = this.GetTile(x - this.xMin, y - this.yMin);
                int index = (x - newXMin) + ((y - newYMin) * newColumns);
                newTiles[index] = t;
                if (t != null)
                {
                    t.gridTx = t.mapTx - newXMin;
                    t.gridTy = t.mapTy - newYMin;
                }
            }
        }

        this.xMin    = newXMin;
        this.xMax    = newXMax;
        this.yMin    = newYMin;
        this.yMax    = newYMax;
        this.columns = newColumns;
        this.rows    = newRows;
        this.tiles   = newTiles;
    }

    public IEnumerator AutotileCoroutine(
        TileTheme tileTheme, string sortingLayerName, bool isBackWall
    )
    {
        if (this.containerChunk.map.mapNumber != 1 &&
            TileGridToStealAutotileFrom() is TileGrid tileGridToStealFrom &&
            tileGridToStealFrom.tiles.Length == this.tiles.Length)
        {
            for (int n = 0; n < this.tiles.Length; n += 1)
            {
                var sourceTile = tileGridToStealFrom.tiles[n];
                var tile = this.tiles[n];
                if (tile == null || sourceTile == null) continue;

                tile.sprite = sourceTile.sprite;
                tile.spriteIceCover = sourceTile.spriteIceCover;
                tile.addInteriorFromTileset = sourceTile.addInteriorFromTileset;
                tile.addInteriorFromTilesetIce = sourceTile.addInteriorFromTilesetIce;
            }

            goto skipAutotileWork;
        }

        // first thin tiles need to be found and treated separately from
        // everything else, so that no other tiles try to merge into them
        foreach (var tile in this.tiles)
        {
            if (tile == null) continue;
            if (tile.sprite != null) continue;
            if (tileTheme.iceTilesBase != null && tile.tileName.StartsWith("ICE:"))
            {
                SelectThinTile(tileTheme.iceTilesCover, tile);
                if (tile.sprite != null)
                {
                    tile.spriteIceCover = tile.sprite;
                    tile.addInteriorFromTilesetIce = tile.addInteriorFromTileset;
                    SelectThinTile(tileTheme.iceTilesBase, tile);
                }
            }
            else if (
                tileTheme.gravityGroundTiles != null &&
                tile.tileName.StartsWith("GRAVITY:")
            )
            {
                SelectThinTile(tileTheme.gravityGroundTiles, tile);
            }
            else if (tile.tileName == "BLANK:back" && tileTheme.backTiles != null)
                SelectThinTile(tileTheme.backTiles, tile);
            else if (tile.tileName == "BLANK:dirt" && tileTheme.dirtTiles != null)
            {
                if (tile.tileGrid.group?.leader is UnstableGround)
                {
                    SelectThinTile(tileTheme.dirtTiles2, tile);
                }
                else
                {
                    SelectThinTile(tileTheme.dirtTiles, tile);
                }
            }
            else if (tile.tileName == "BLANK:square" && tileTheme.normalTiles != null)
                SelectThinTile(tileTheme.normalTiles, tile);

            yield return null;
        }

        // then slope tiles; we need to know when skew tiles are selected
        // because the choice of sprites for those tiles affect other tiles around them
        foreach (var tile in this.tiles)
        {
            if (tile == null) continue;
            if (tile.sprite != null) continue;
            if (tileTheme.iceTilesBase != null && tile.tileName.StartsWith("ICE:"))
            {
                SelectSlopeTile(tileTheme.iceTilesCover, tile);
                if (tile.sprite != null)
                {
                    tile.spriteIceCover = tile.sprite;
                    tile.addInteriorFromTilesetIce = tile.addInteriorFromTileset;
                    SelectSlopeTile(tileTheme.iceTilesBase, tile);
                }
            }
            else if (
                tileTheme.gravityGroundTiles != null &&
                tile.tileName.StartsWith("GRAVITY:")
            )
            {
                SelectSlopeTile(tileTheme.gravityGroundTiles, tile);
            }
            else
            {
                SelectSlopeTile(tileTheme.normalTiles, tile);
            }

            yield return null;
        }

        // anything unclaimed gets selected by more standard rules.
        foreach (var tile in this.tiles)
        {
            if (tile == null) continue;
            if (tile.sprite != null) continue;

            int x = tile.gridTx;
            int y = tile.gridTy;

            if (tile.spec == Tile.Spec.TopLineMoving)
            {
                var left  = GetTileForAutotile(tile.gridTx - 1, tile.gridTy);
                var right = GetTileForAutotile(tile.gridTx + 1, tile.gridTy);
                bool leftmost  = left.tile?.spec != Tile.Spec.TopLineMoving;
                bool rightmost = right.tile?.spec != Tile.Spec.TopLineMoving;

                if (leftmost == true && rightmost == false)
                    tile.sprite = tileTheme.movingCloudLeft;
                else if (rightmost == true && leftmost == false)
                    tile.sprite = tileTheme.movingCloudRight;
                else if (leftmost == true && rightmost == true)
                    tile.sprite = tileTheme.movingCloudSingle;
                else
                    tile.sprite = tileTheme.movingCloudCenter;
            }
            else if (tile.spec.shape == Tile.Shape.TopLine)
            {
                var left  = GetTileForAutotile(tile.gridTx - 1, tile.gridTy);
                var right = GetTileForAutotile(tile.gridTx + 1, tile.gridTy);
                bool leftmost  = left.tile?.spec  != Tile.Spec.TopLine;
                bool rightmost = right.tile?.spec != Tile.Spec.TopLine;

                if (leftmost == true && rightmost == false)
                    tile.sprite = tileTheme.cloudLeft;
                else if (rightmost == true && leftmost == false)
                    tile.sprite = tileTheme.cloudRight;
                else
                    tile.sprite = Util.RandomChoice(tileTheme.cloudMiddle);
            }
            else if (tile.tileName == "BLANK:square")
                SelectTile(tileTheme.normalTiles, tile);
            else if (
                tile.spec.curve != null &&
                tile.setType == Tile.SetType.Ice &&
                tileTheme.iceTilesBase != null
            )
            {
                SelectCurveTile(tileTheme.iceTilesCover, tile);
                tile.spriteIceCover = tile.sprite;
                tile.addInteriorFromTilesetIce = tile.addInteriorFromTileset;
                SelectCurveTile(tileTheme.iceTilesBase, tile);
            }
            else if (
                tile.spec.curve != null &&
                tile.setType == Tile.SetType.GravityGround &&
                tileTheme.gravityGroundTiles != null
            )
            {
                SelectCurveTile(tileTheme.gravityGroundTiles, tile);
            }
            else if (tile.spec.curve != null)
                SelectCurveTile(tileTheme.normalTiles, tile);
            else if (tile.tileName == "BLANK:outside")
                SelectOutsideTile(tileTheme.outsideTiles, tile);
            else if (tile.tileName == "BLANK:dirt")
            {
                if (tile.tileGrid.group?.leader is UnstableGround)
                {
                    SelectTile(tileTheme.dirtTiles2, tile);
                }
                else
                {
                    SelectTile(tileTheme.dirtTiles, tile);
                }
            }
            else if (tile.tileName == "BLANK:back" && tileTheme.backTiles != null)
                SelectTile(tileTheme.backTiles, tile);
            else if (tileTheme.iceTilesBase != null && tile.tileName == "ICE:square")
            {
                SelectTile(tileTheme.iceTilesCover, tile);
                tile.spriteIceCover = tile.sprite;
                tile.addInteriorFromTilesetIce = tile.addInteriorFromTileset;
                SelectTile(tileTheme.iceTilesBase, tile);
            }
            else if (
                tileTheme.gravityGroundTiles != null && tile.tileName == "GRAVITY:square"
            )
            {
                SelectTile(tileTheme.gravityGroundTiles, tile);
            }
            else if (tile.tileName == "BLANK:checkerboard")
            {
                var tileset =
                    (tile.mapTx + tile.mapTy) % 2 == 0 ?
                    tileTheme.checkerboardLightTiles :
                    tileTheme.checkerboardDarkTiles;
                SelectTile(tileset, tile);
            }

            // these are just here to support the apple demo (not used in current design)
            else if (tile.tileName == "TEMPLE:pillar_thick_top")
                tile.sprite = tileTheme.normalTiles.pillar2WideTop;
            else if (tile.tileName == "TEMPLE:pillar_thick_bottom")
                tile.sprite = tileTheme.normalTiles.pillar2WideBottom;
            else if (tile.tileName.StartsWith("TEMPLE:pillar_thick_"))
                tile.sprite = Util.RandomChoice(tileTheme.normalTiles.pillar2WideMiddle);
            else if (tile.tileName == "TEMPLE:pillar_top")
                tile.sprite = tileTheme.normalTiles.pillar1WideTop;
            else if (tile.tileName == "TEMPLE:pillar_bottom")
                tile.sprite = tileTheme.normalTiles.pillar1WideBottom;
            else if (tile.tileName.StartsWith("TEMPLE:pillar_"))
                tile.sprite = Util.RandomChoice(tileTheme.normalTiles.pillar1WideMiddle);

            yield return null;
        }

        if (this.containerChunk.map.theme == Theme.WindySkies)
        {
            SwapOutBottomCornerTilesForWindySkies(tileTheme.normalTiles);
            SwapOutBottomCornerTilesForWindySkies(tileTheme.dirtTiles);
        }

        skipAutotileWork:

        Transform tileParent = 
            this.group != null ?
            this.group.container.transform :
            this.containerChunk.transform;

        var listOfTiles = this.tiles
            .Where(t => t != null)
            .OrderBy(t => t.autotileChoseSkew ? 1 : 0) // these need to be drawn on top
            .Distinct().ToArray();

        if (listOfTiles.Length > 0)
        {
            this.tileGridMesh = TileGridMesh.CreateAt(this, tileParent);

            Color ColorForTile(Tile tile)
            {
                if (this.containerChunk.map.theme == Theme.HotNColdSprings)
                {
                    if (tile.spec.shape == Tile.Shape.TopLine)
                        return TemperatureAnimation.DrawingColorForCloudPlatform;
                    else if (isBackWall == true)
                        return TemperatureAnimation.DrawingColorForBackWall;
                    else if (tile.ice == true)
                        return TemperatureAnimation.DrawingColorForIceBase;
                    else if (tile.secondary == true)
                        return TemperatureAnimation.DrawingColorForSecondaryTiles;
                    else
                        return TemperatureAnimation.DrawingColorForNormalTiles;
                }
                else
                {
                    return Color.white;
                }
            }

            foreach (var tile in listOfTiles)
            {
                if (tile.addInteriorFromTileset != null)
                {
                    this.tileGridMesh.AddInterior(
                        tile, tile.addInteriorFromTileset, ColorForTile(tile)
                    );
                    yield return null;
                }
            }
            this.tileGridMesh.MarkEndOfLayer();

            foreach (var tile in listOfTiles)
            {
                if (tile.addInteriorFromTilesetIce != null)
                {
                    var color = TemperatureAnimation.DrawingColorForIceCover;
                    this.tileGridMesh.AddInterior(
                        tile, tile.addInteriorFromTilesetIce, color
                    );
                    yield return null;
                }
            }
            this.tileGridMesh.MarkEndOfLayer();

            foreach (var tile in listOfTiles)
            {
                this.tileGridMesh.AddSprite(tile, tile.sprite, ColorForTile(tile));
                yield return null;
            }
            this.tileGridMesh.MarkEndOfLayer();

            foreach (var tile in listOfTiles)
            {
                if (tile.spriteIceCover != null)
                {
                    var color = TemperatureAnimation.DrawingColorForIceCover;
                    this.tileGridMesh.AddSprite(tile, tile.spriteIceCover, color);
                    yield return null;
                }
            }

            var texture =
                tileTheme.normalTiles.left.Length > 0 ?
                tileTheme.normalTiles.left[0].texture : null;
            this.tileGridMesh.FinishCreating(texture, sortingLayerName);
        }

        // var listOfTiles = this.tiles.Where(t => t != null).Distinct().ToArray();

        // foreach (var tile in listOfTiles)
        // {
        //     GameObject gameObject;
        //     Transform tileParent = 
        //         tile.tileGrid.group != null ?
        //         tile.tileGrid.group.container.transform :
        //         this.containerChunk.transform;

        //     if (tile.tileName == "TEMPLE:bigplant_1")
        //     {
        //         gameObject = GameObject.Instantiate(Assets.instance.templeBigPlant1);
        //         gameObject.name = $"tile ({tile.mapTx}, {tile.mapTy})";
        //         gameObject.transform.position = new Vector3(
        //             tile.mapTx * Tile.Size,
        //             tile.mapTy * Tile.Size
        //         );
        //         gameObject.transform.parent = this.containerChunk.transform;
        //         continue;
        //     }
        //     else if (tile.tileName == "TEMPLE:bigplant_2")
        //     {
        //         gameObject = GameObject.Instantiate(Assets.instance.templeBigPlant2);
        //         gameObject.name = $"tile ({tile.mapTx}, {tile.mapTy})";
        //         gameObject.transform.position = new Vector3(
        //             tile.mapTx * Tile.Size,
        //             tile.mapTy * Tile.Size
        //         );
        //         gameObject.transform.parent = this.containerChunk.transform;
        //         continue;
        //     }

        //     gameObject = new GameObject();
        //     gameObject.name = $"tile ({tile.mapTx}, {tile.mapTy})";
        //     gameObject.transform.position = new Vector3(
        //         tile.mapTx * Tile.Size,
        //         tile.mapTy * Tile.Size
        //     );
        //     gameObject.transform.parent = tileParent;

        //     if (tile.sprite == null &&
        //         tile.shouldAutotile == false &&
        //         tileTheme.normalTiles.singleSquare.Length > 0)
        //     {
        //         tile.sprite = Util.RandomChoice(tileTheme.normalTiles.singleSquare);
        //     }
 
        //     var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
        //     spriteRenderer.sprite = tile.sprite;
        //     spriteRenderer.sortingLayerName = sortingLayerName;
        //     if (tile.autotileChoseSkew == true)
        //         spriteRenderer.sortingOrder = 1;
        //     tile.spriteRenderer = spriteRenderer;

        //     if (tile.spriteIceCover != null)
        //     {
        //         var iceGameObject = new GameObject();
        //         iceGameObject.name = $"ice cover ({tile.mapTx}, {tile.mapTy})";
        //         iceGameObject.transform.position = new Vector3(
        //             tile.mapTx * Tile.Size,
        //             tile.mapTy * Tile.Size
        //         );
        //         iceGameObject.transform.parent = tileParent;

        //         var iceSpriteRenderer = iceGameObject.AddComponent<SpriteRenderer>();
        //         iceSpriteRenderer.sprite = tile.spriteIceCover;
        //         iceSpriteRenderer.sortingLayerName = sortingLayerName;
        //         if (tile.autotileChoseSkew == true)
        //             iceSpriteRenderer.sortingOrder = 4;
        //         else
        //             iceSpriteRenderer.sortingOrder = 3;
        //         tile.spriteRendererIceCover = iceSpriteRenderer;
        //     }

        //     if (tile.addInteriorFromTileset != null)
        //     {
        //         tile.interior = TileTexture.Init(
        //             tileParent,
        //             tile,
        //             tile.addInteriorFromTileset,
        //             this.map.theme,
        //             isIceCover: false,
        //             sortingLayerName,
        //             sortingOrder: -1
        //         );
        //     }
        //     if (tile.addInteriorFromTilesetIce != null)
        //     {
        //         tile.interiorIceCover = TileTexture.Init(
        //             tileParent,
        //             tile,
        //             tile.addInteriorFromTilesetIce,
        //             this.map.theme,
        //             isIceCover: true,
        //             sortingLayerName,
        //             sortingOrder: 2
        //         );
        //     }

        //     yield return null;
        // }

        if (this == this.containerChunk.backTileGrid1 && tileTheme.backTiles != null)
            CreatePillarsForBackLayer(tileTheme.backTiles);
    }

    private TileGrid TileGridToStealAutotileFrom()
    {
        if (this.containerChunk.map.mapNumber == 1)
            return null;

        var correspondingChunk = Game.instance.map1.chunks[this.containerChunk.index];
        if (correspondingChunk.hasFinishedInitAutotile == false)
            return null;

        if (this == this.containerChunk.backTileGrid1)
            return correspondingChunk.backTileGrid1;
        if (this == this.containerChunk.backTileGrid2)
            return correspondingChunk.backTileGrid2;

        for (int n = 0; n < this.containerChunk.solidTilesA.Length; n += 1)
        {
            if (this == this.containerChunk.solidTilesA[n])
                return correspondingChunk.solidTilesA[n];
        }
        for (int n = 0; n < this.containerChunk.solidTilesB.Length; n += 1)
        {
            if (this == this.containerChunk.solidTilesB[n])
                return correspondingChunk.solidTilesB[n];
        }

        return null;
    }

    public SpriteRenderer[] CreateBackWallForExit(
        TileTheme tileTheme, Vector2 exitPosition
    )
    {
        if (tileTheme.backTiles == null) return null;

        if(tileTheme == Assets.instance.windySkies?.tileTheme) return null;

        var result = new List<SpriteRenderer>();

        void AddSpriteRenderer(
            string name, float x, float y, Sprite sprite, int sortingOrder
        )
        {
            var gameObject = new GameObject();
            gameObject.name = name;
            gameObject.transform.parent = this.containerChunk.transform;
            gameObject.transform.position = new Vector2(x, y);

            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.sortingLayerName = "Tiles - Back 2";
            spriteRenderer.sortingOrder = sortingOrder;
            result.Add(spriteRenderer);
        }

        var tilesetBack =
            tileTheme.backTilesForTunnel != null ?
            tileTheme.backTilesForTunnel :
            tileTheme.backTiles;

        for (int y = this.yMin; y <= this.yMax; y += 1)
        {
            var leftName = "Back Wall for Exit (Left Edge)";
            var leftSprite = Util.RandomChoice(tilesetBack.left);
            AddSpriteRenderer(leftName, this.xMin - 0.5f, y, leftSprite, 1);

            var rightName = "Back Wall for Exit (Right Edge)";
            var rightSprite = Util.RandomChoice(tilesetBack.right);
            AddSpriteRenderer(rightName, this.xMax + 0.5f, y, rightSprite, 1);
        }

        int fromX, toX;
        float offsetX;

        if (tilesetBack.interiorUnderneathEdges == true)
            (fromX, toX, offsetX) = (this.xMin, this.xMax, 0);
        else
            (fromX, toX, offsetX) = (this.xMin, this.xMax - 1, 0.5f);

        for (int y = this.yMin; y <= this.yMax; y += 1)
        {
            for (int x = fromX; x <= toX; x += 1)
            {
                var name = "Back Wall For Exit";
                var sprite = tilesetBack.InteriorTile(x, y);
                AddSpriteRenderer(name, x + offsetX, y, sprite, 0);
            }
        }

        return result.ToArray();
    }

    private (Tile tile, Chunk chunk) GetTileForAutotile(int gridTx, int gridTy)
    {
        // the autotile functions need a bit more. we are kind of pretending that
        // there is an infinite grid of filled tiles outside the defined chunks.

        if (gridTx >= 0 && gridTx < this.columns &&
            gridTy >= 0 && gridTy < this.rows)
        {
            return (this.tiles[gridTx + (gridTy * this.columns)], this.containerChunk);
        }

        int mapTx = gridTx + this.xMin;
        int mapTy = gridTy + this.yMin;
        var chunk = this.map.ChunkAt(mapTx, mapTy);
        if (chunk == null)
            return (null, null);
        if (this.containerChunk.solidTilesA[0] != this)
            return (null, chunk);

        var tile = chunk.TileAt(mapTx, mapTy, Layer.A);
        if (tile == null)
            return (null, chunk);

        if (chunk.TileTheme() != this.containerChunk.TileTheme() &&
            tile.tileName != "BLANK:outside")
        {
            return (null, chunk);
        }

        return (tile, chunk);
    }

    private void SelectThinTile(Tileset tileset, Tile tile)
    {
        bool IsFilled((Tile tile, Chunk chunk) locationContents)
        {
            if (locationContents.chunk == null)
                return tileset.flowIntoOutside;
            else
                return IsTileRelevant(locationContents.tile);
        }

        bool IsTileRelevant(Tile t)
        {
            if (t == null) return false;

            // if (tile.setType == Tile.SetType.GravityGround)
            //     return true;

            if (tileset.type == Tileset.TilesetType.Dirt)
                return (t.tileName == "BLANK:dirt");
            else if (tileset.type == Tileset.TilesetType.Outside)
                return (t.tileName == "BLANK:outside");

            if (t.spec.shape == Tile.Shape.TopLine) return false;
            if (t.tileName == "BLANK:dirt") return false;
            if (t.tileName == "BLANK:outside") return false;
            return true;
            // return t.spec.shape == Tile.Shape.Square;
        }

        bool IsSingleColumn(Tile t)
        {
            if (t == null || t.spec.shape != Tile.Shape.Square)
                return false;
            else
                return
                    IsFilled(GetTileForAutotile(t.gridTx - 1, t.gridTy)) == false &&
                    IsFilled(GetTileForAutotile(t.gridTx + 1, t.gridTy)) == false;
        }

        bool IsSingleRow(Tile t)
        {
            if (t == null || t.spec.shape != Tile.Shape.Square)
                return false;
            else
                return
                    IsFilled(GetTileForAutotile(t.gridTx, t.gridTy + 1)) == false &&
                    IsFilled(GetTileForAutotile(t.gridTx, t.gridTy - 1)) == false;
        }

        if (tile.spec.shape != Tile.Shape.Square)
            return;
        if (IsTileRelevant(tile) == false)
            return;

        {
            var l  = GetTileForAutotile(tile.gridTx - 1, tile.gridTy    );
            var r  = GetTileForAutotile(tile.gridTx + 1, tile.gridTy    );
            var b  = GetTileForAutotile(tile.gridTx,     tile.gridTy - 1);
            var t  = GetTileForAutotile(tile.gridTx,     tile.gridTy + 1);
            var tl = GetTileForAutotile(tile.gridTx - 1, tile.gridTy + 1);
            var tr = GetTileForAutotile(tile.gridTx + 1, tile.gridTy + 1);
            var bl = GetTileForAutotile(tile.gridTx - 1, tile.gridTy - 1);
            var br = GetTileForAutotile(tile.gridTx + 1, tile.gridTy - 1);

            if (IsSingleColumn(tile) == true)
            {
                var isTop    = IsFilled(t) == false || IsSingleColumn(t.tile) == false;
                var isBottom = IsFilled(b) == false || IsSingleColumn(b.tile) == false;

                var isShort = false;
                if (isTop == true)
                {
                    var bb = GetTileForAutotile(tile.gridTx, tile.gridTy - 2);
                    if (IsFilled(bb) == false || IsSingleColumn(bb.tile) == false)
                        isShort = true;
                }
                else if (isBottom == true)
                {
                    var tt = GetTileForAutotile(tile.gridTx, tile.gridTy + 2);
                    if (IsFilled(tt) == false || IsSingleColumn(tt.tile) == false)
                        isShort = true;
                }

                if (isShort == true)
                    tile.sprite = Util.RandomChoice(tileset.singleSquare);
                else if (isTop == true && isBottom == true)
                    tile.sprite = Util.RandomChoice(tileset.singleSquare);
                else if (isTop == true && isBottom == false)
                    tile.sprite = tileset.pillar1WideTop;
                else if (isTop == false && isBottom == true)
                    tile.sprite = tileset.pillar1WideBottom;
                else 
                    tile.sprite = Util.RandomChoice(tileset.pillar1WideMiddle);

                tile.autotileChoseThin = true;
            }
            else if (IsSingleRow(tile) == true)
            {
                var isLeft  = IsFilled(l) == false || IsSingleRow(l.tile) == false;
                var isRight = IsFilled(r) == false || IsSingleRow(r.tile) == false;

                var isShort = false;
                if (isLeft == true)
                {
                    var rr = GetTileForAutotile(tile.gridTx + 2, tile.gridTy);
                    if (IsFilled(rr) == false || IsSingleRow(rr.tile) == false)
                        isShort = true;
                }
                else if (isRight == true)
                {
                    var ll = GetTileForAutotile(tile.gridTx - 2, tile.gridTy);
                    if (IsFilled(ll) == false || IsSingleRow(ll.tile) == false)
                        isShort = true;
                }

                if (isShort == true)
                    tile.sprite = Util.RandomChoice(tileset.singleSquare);
                else if (isLeft == true && isRight == true)
                    tile.sprite = Util.RandomChoice(tileset.singleSquare);
                else if (isLeft == true && isRight == false)
                    tile.sprite = tileset.singleRowLeft;
                else if (isLeft == false && isRight == true)
                    tile.sprite = tileset.singleRowRight;
                else
                    tile.sprite = Util.RandomChoice(tileset.singleRowMiddle);

                tile.autotileChoseThin = true;
            }
            else if (
                (IsFilled(tl) == false || IsFilled(t) == false || IsFilled(l) == false) &&
                (IsFilled(tr) == false || IsFilled(t) == false || IsFilled(r) == false) &&
                (IsFilled(bl) == false || IsFilled(b) == false || IsFilled(l) == false) &&
                (IsFilled(br) == false || IsFilled(b) == false || IsFilled(r) == false)
            )
            {
                tile.sprite = Util.RandomChoice(tileset.singleSquare);
                tile.autotileChoseThin = true;
            }
        }
    }

    private void SelectSlopeTile(Tileset tileset, Tile tile)
    {
        bool IsFilled(int dx, int dy)
        {
            var (t, chunk) =
                GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy);
            if (chunk == null) return false;
            if (t == null) return false;

            if (tileset.type == Tileset.TilesetType.Dirt)
                return (t.tileName == "BLANK:dirt");
            else if (tileset.type == Tileset.TilesetType.Outside)
                return (t.tileName == "BLANK:outside");

            if (t.autotileChoseThin == true) return false;
            if (t.spec.shape == Tile.Shape.TopLine) return false;
            if (t.tileName == "BLANK:dirt") return false;
            if (t.tileName == "BLANK:outside") return false;
            return true;
        }

        switch (tile.spec.shape)
        {
            case Tile.Shape.Slope22BR:
            {
                bool solidOnLeft  = IsFilled(-1, -1);
                bool solidOnRight = IsFilled(2, 0);
                bool solidBelow   = IsFilled(0, -1) && IsFilled(1, -1);
                Sprite fallbackSprite = tileset.slope22BottomRight;

                if (solidOnLeft == true && solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoined22BottomRight;
                else if (solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedLeft22BottomRight;
                else if (solidOnLeft == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedRight22BottomRight;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope22BL:
            {
                bool solidOnLeft  = IsFilled(-1, 0);
                bool solidOnRight = IsFilled(2, -1);
                bool solidBelow   = IsFilled(0, -1) && IsFilled(1, -1);
                Sprite fallbackSprite = tileset.slope22BottomLeft;

                if (solidOnLeft == true && solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoined22BottomLeft;
                else if (solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedLeft22BottomLeft;
                else if (solidOnLeft == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedRight22BottomLeft;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope22TL:
            {
                bool solidOnLeft  = IsFilled(-1, 0);
                bool solidOnRight = IsFilled(2, 1);
                bool solidAbove   = IsFilled(0, 1) && IsFilled(1, 1);
                Sprite fallbackSprite = tileset.slope22TopLeft;

                if (solidOnLeft == true && solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoined22TopLeft;
                else if (solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedLeft22TopLeft;
                else if (solidOnLeft == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedRight22TopLeft;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope22TR:
            {
                bool solidOnLeft  = IsFilled(-1, 1);
                bool solidOnRight = IsFilled(2, 0);
                bool solidAbove   = IsFilled(0, 1) && IsFilled(1, 1);
                Sprite fallbackSprite = tileset.slope22TopRight;

                if (solidOnLeft == true && solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoined22TopRight;
                else if (solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedLeft22TopRight;
                else if (solidOnLeft == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedRight22TopRight;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope45BR:
            {
                bool solidOnLeft  = IsFilled(-1, -1);
                bool solidOnRight = IsFilled(1, 0);
                bool solidBelow   = IsFilled(0, -1);
                Sprite fallbackSprite = tileset.slope45BottomRight;
                
                if (solidOnLeft == true && solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoined45BottomRight;
                else if (solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedLeft45BottomRight;
                else if (solidOnLeft == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedRight45BottomRight;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope45BL:
            {
                bool solidOnLeft  = IsFilled(-1, 0);
                bool solidOnRight = IsFilled(1, -1);
                bool solidBelow   = IsFilled(0, -1);
                Sprite fallbackSprite = tileset.slope45BottomLeft;
                
                if (solidOnLeft == true && solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoined45BottomLeft;
                else if (solidOnRight == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedLeft45BottomLeft;
                else if (solidOnLeft == true && solidBelow == true)
                    tile.sprite = tileset.slopeJoinedRight45BottomLeft;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope45TL:
            {
                bool solidOnLeft  = IsFilled(-1, 0);
                bool solidOnRight = IsFilled(1, 1);
                bool solidAbove   = IsFilled(0, 1);
                Sprite fallbackSprite = tileset.slope45TopLeft;
                
                if (solidOnLeft == true && solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoined45TopLeft;
                else if (solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedLeft45TopLeft;
                else if (solidOnLeft == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedRight45TopLeft;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }

            case Tile.Shape.Slope45TR:
            {
                bool solidOnLeft  = IsFilled(-1, 1);
                bool solidOnRight = IsFilled(1, 0);
                bool solidAbove   = IsFilled(0, 1);
                Sprite fallbackSprite = tileset.slope45TopRight;
                
                if (solidOnLeft == true && solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoined45TopRight;
                else if (solidOnRight == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedLeft45TopRight;
                else if (solidOnLeft == true && solidAbove == true)
                    tile.sprite = tileset.slopeJoinedRight45TopRight;
                else
                    tile.sprite = fallbackSprite;

                tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                break;
            }
        }

        if (tile.autotileChoseSkew == true &&
            tileset.interiorUnderneathJoinedSlopes == true)
        {
            tile.addInteriorFromTileset = tileset;
        }
    }

    private void SelectCurveTile(Tileset tileset, Tile tile)
    {
        bool CurveIsJoinable(XY basePosition, XY outwardDirection)
        {
            bool SafeToJoin((Tile tile, Chunk chunk) data) =>
                data.chunk == null ||
                data.tile?.spec.shape == Tile.Shape.Square;

            if (SafeToJoin(GetTileForAutotile(
                tile.gridTx + basePosition.x + (outwardDirection.x * 4),
                tile.gridTy + basePosition.y
            )) == false) return false;

            if (SafeToJoin(GetTileForAutotile(
                tile.gridTx + basePosition.x,
                tile.gridTy + basePosition.y + (outwardDirection.y * 4)
            )) == false) return false;

            for (int dx = -1; dx <= 4; dx += 1)
            {
                if (SafeToJoin(GetTileForAutotile(
                    tile.gridTx + basePosition.x + (outwardDirection.x * dx),
                    tile.gridTy + basePosition.y + (outwardDirection.y * -1)
                )) == false) return false;
            }

            for (int dy = -1; dy <= 4; dy += 1)
            {
                if (SafeToJoin(GetTileForAutotile(
                    tile.gridTx + basePosition.x + (outwardDirection.x * -1),
                    tile.gridTy + basePosition.y + (outwardDirection.y * dy)
                )) == false) return false;
            }

            return true;
        }

        Sprite SelectVariant(
            bool joinable,
            bool outside,
            Sprite spriteJoined,
            Sprite spriteOutside,
            Sprite spriteRegular
        )
        {
            if (outside == true && spriteOutside != null)
                return spriteOutside;
            else if (joinable == true && outside == false && spriteJoined != null)
                return spriteJoined;
            else if (spriteRegular != null)
                return spriteRegular;
            else if (spriteJoined != null)
                return spriteJoined;
            else
                return null;
        }

        switch (tile.spec.shape)
        {
            case Tile.Shape.CurveBL3Plus1: {
                bool joinable = CurveIsJoinable(new XY(0, 0), new XY(1, 1));
                bool outside = (tile.mapTx == this.containerChunk.xMin);
                if (tile.ice == true)
                {
                    var above = GetTileForAutotile(tile.gridTx, tile.gridTy + 4);
                    if (above.tile?.ice == true) outside = false;
                }
                tile.sprite = SelectVariant(
                    joinable,
                    outside,
                    tileset.curve3Plus1BottomLeftJoined,
                    tileset.curve3Plus1BottomLeftWithOutside,
                    tileset.curve3Plus1BottomLeft
                );
                break;
            }
            case Tile.Shape.CurveBR3Plus1: {
                bool joinable = CurveIsJoinable(new XY(3, 0), new XY(-1, 1));
                bool outside = (tile.mapTx == this.containerChunk.xMax - 3);
                if (tile.ice == true)
                {
                    var above = GetTileForAutotile(tile.gridTx + 3, tile.gridTy + 4);
                    if (above.tile?.ice == true) outside = false;
                }
                tile.sprite = SelectVariant(
                    joinable,
                    outside,
                    tileset.curve3Plus1BottomRightJoined,
                    tileset.curve3Plus1BottomRightWithOutside,
                    tileset.curve3Plus1BottomRight
                );
                break;
            }
            case Tile.Shape.CurveTL3Plus1: {
                bool joinable = CurveIsJoinable(new XY(0, 3), new XY(1, -1));
                bool outside = (tile.mapTx == this.containerChunk.xMin);
                if (tile.ice == true)
                {
                    var below = GetTileForAutotile(tile.gridTx, tile.gridTy - 1);
                    if (below.tile?.ice == true) outside = false;
                }
                tile.sprite = SelectVariant(
                    joinable,
                    outside,
                    tileset.curve3Plus1TopLeftJoined,
                    tileset.curve3Plus1TopLeftWithOutside,
                    tileset.curve3Plus1TopLeft
                );
                break;
            }
            case Tile.Shape.CurveTR3Plus1: {
                bool joinable = CurveIsJoinable(new XY(3, 3), new XY(-1, -1));
                bool outside = (tile.mapTx == this.containerChunk.xMax - 3);
                if (tile.ice == true)
                {
                    var below = GetTileForAutotile(tile.gridTx + 3, tile.gridTy - 1);
                    if (below.tile?.ice == true) outside = false;
                }
                tile.sprite = SelectVariant(
                    joinable,
                    outside,
                    tileset.curve3Plus1TopRightJoined,
                    tileset.curve3Plus1TopRightWithOutside,
                    tileset.curve3Plus1TopRight
                );
                break;
            }
            case Tile.Shape.CurveOutBL3: {
                tile.sprite = tileset.curveOut3Plus1BottomLeft;
                break;
            }
            case Tile.Shape.CurveOutBR3: {
                tile.sprite = tileset.curveOut3Plus1BottomRight;
                break;
            }
            case Tile.Shape.CurveOutTL3: {
                tile.sprite = tileset.curveOut3Plus1TopLeft;
                break;
            }
            case Tile.Shape.CurveOutTR3: {
                tile.sprite = tileset.curveOut3Plus1TopRight;
                break;
            }
        }

        if (tileset.interiorUnderneathAllCurves == true && tile.sprite != null)
            tile.addInteriorFromTileset = tileset;
    }

    private void SelectTile(Tileset tileset, Tile tile)
    {
        Tile TileRelative(int dx, int dy) =>
            GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy).tile;

        bool IsFilled(int dx, int dy)
        {
            if (tileset.type == Tileset.TilesetType.Back)
            {
                var st = this.containerChunk.solidTilesA[0].GetTileForAutotile(
                    tile.gridTx + dx, tile.gridTy + dy
                );
                if (st.chunk == null) return tileset.flowIntoOutside;
                if (st.tile != null)
                {
                    if (st.tile.IsEntirelySolidInCell(
                        tile.gridTx + dx - st.tile.mapTx,
                        tile.gridTy + dy - st.tile.mapTy
                    ) == true)
                        return true;
                }

                var item = this.containerChunk.ItemAt(
                    this.containerChunk.xMin + tile.gridTx + dx,
                    this.containerChunk.yMin + tile.gridTy + dy
                );
                if (item is MetalBlock || item is Pipe || item is GripGround)
                    return true;
            }

            var (_t, chunk) =
                GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy);
            if (chunk == null && this.group == null)
                return tileset.flowIntoOutside;

            {
                var item = this.containerChunk.ItemAt<GripGround>(
                    this.containerChunk.xMin + tile.gridTx + dx,
                    this.containerChunk.yMin + tile.gridTy + dy
                );
                if (item != null) return true;
            }

            if (_t == null) return false;
            if (_t.setType != tile.setType) return false;

            if (_t.autotileChoseSkew == true) return true;
            if (_t.autotileChoseThin == true) return false;

            if (_t.spec.shape == Tile.Shape.TopLine) return false;

            if (tileset.flowIntoSlopes == false)
            {
                if (_t.tileName?.StartsWith("BLANK:22") == true) return false;
                if (_t.tileName?.StartsWith("BLANK:45") == true) return false;
            }
            return true;
        }

        Sprite SelectLeftRight(Sprite[] sprites) =>
            (tileset.leftAndRightTilesInOrder == true) ?
                sprites[(tile.gridTx + tile.gridTy) % 4] :
                sprites[UnityEngine.Random.Range(0, sprites.Length)];

        Sprite SelectTopBottom(Sprite[] sprites) =>
            (tileset.topAndBottomTilesInOrder == true) ?
                sprites[(tile.gridTx + tile.gridTy) % 4] :
                sprites[UnityEngine.Random.Range(0, sprites.Length)];

        var l  = IsFilled(-1,  0);
        var r  = IsFilled( 1,  0);
        var b  = IsFilled( 0, -1);
        var t  = IsFilled( 0,  1);
        var tl = IsFilled(-1,  1);
        var tr = IsFilled( 1,  1);
        var bl = IsFilled(-1, -1);
        var br = IsFilled( 1, -1);

        if (TileRelative(0, 1)?.autotileChoseSkew == true)
            l = r = tl = tr = t = true;
        if (TileRelative(0, -1)?.autotileChoseSkew == true)
            l = r = bl = br = b = true;

        var allSides = l && r && t && b;
        var allCorners = tl && tr && bl && br;

        if (allSides == true && allCorners == true)
        {
            tile.addInteriorFromTileset = tileset;
        }

        else if (l == false && r == true && b == true && t == true) tile.sprite = SelectLeftRight(tileset.left);
        else if (l == true && r == false && b == true && t == true) tile.sprite = SelectLeftRight(tileset.right);
        else if (l == true && r == true && b == false && t == true) tile.sprite = SelectTopBottom(tileset.bottom);
        else if (l == true && r == true && b == true && t == false) tile.sprite = SelectTopBottom(tileset.top);

        else if (l == true && bl == true && b == true && r == false && t == false) tile.sprite = tileset.topRight;
        else if (r == true && br == true && b == true && l == false && t == false) tile.sprite = tileset.topLeft;
        else if (l == true && tl == true && t == true && r == false && b == false) tile.sprite = tileset.bottomRight;
        else if (r == true && tr == true && t == true && l == false && b == false) tile.sprite = tileset.bottomLeft;

        else if (allSides == true && tl == false && tr == true && bl == true && br == true) tile.sprite = tileset.insideTopLeft;
        else if (allSides == true && tl == true && tr == false && bl == true && br == true) tile.sprite = tileset.insideTopRight;
        else if (allSides == true && tl == true && tr == true && bl == false && br == true) tile.sprite = tileset.insideBottomLeft;
        else if (allSides == true && tl == true && tr == true && bl == true && br == false) tile.sprite = tileset.insideBottomRight;

        else if (tileset.singleSquare.Length > 0)
            tile.sprite = Util.RandomChoice(tileset.singleSquare);

        if (tileset.hasArtForRow2 == true)
        {
            bool IsSquare(int dx, int dy)
            {
                var t = TileRelative(dx, dy);
                return
                    t?.spec.shape == Tile.Shape.Square &&
                    t.setType == tile.setType;
            }

            bool ShouldColumnContainRow2(int dx) =>
                TileRelative(dx, 2) == null &&
                IsSquare(dx, 1) == true &&
                IsSquare(dx, -1) == true;

            bool ShouldRow2FlowIntoUpwardWallHere(int dx) =>
                IsSquare(dx, 2) == true &&
                IsSquare(dx, 1) == true &&
                IsSquare(dx, -1) == true;

            if (ShouldColumnContainRow2(0) == true)
            {
                var row2Left = ShouldColumnContainRow2(-1);
                var row2Right = ShouldColumnContainRow2(+1);

                if (row2Left == false && row2Right == false)
                    { /* do nothing */ }
                else if (allSides == true && ShouldRow2FlowIntoUpwardWallHere(-1) == true)
                    tile.sprite = SelectTopBottom(tileset.row2Mid);
                else if (allSides == true && ShouldRow2FlowIntoUpwardWallHere(1) == true)
                    tile.sprite = SelectTopBottom(tileset.row2Mid);
                else if (allSides == true && row2Left == false)
                    tile.sprite = tileset.row2LeftInterior;
                else if (allSides == true && row2Right == false)
                    tile.sprite = tileset.row2RightInterior;
                else if (allSides == true)
                    tile.sprite = SelectTopBottom(tileset.row2Mid);
                else if (l == false && r == true)
                    tile.sprite = tileset.row2Left;
                else if (l == true && r == false)
                    tile.sprite = tileset.row2Right;
            }
            else if (ShouldRow2FlowIntoUpwardWallHere(0) == true)
            {
                if (ShouldColumnContainRow2(-1) == true)
                    tile.sprite = tileset.row2RightInterior;
                else if (ShouldColumnContainRow2(1) == true)
                    tile.sprite = tileset.row2LeftInterior;
            }
        }
 
        if ((allSides == false || allCorners == false) &&
            tileset.interiorUnderneathEdges == true)
        {
            tile.addInteriorFromTileset = tileset;
        }
        else if (
            (tileset.left.Contains(tile.sprite) || tileset.right.Contains(tile.sprite))
            && tileset.interiorUnderneathLeftAndRightEdges == true)
        {
            tile.addInteriorFromTileset = tileset;
        }
    }

    private void SelectOutsideTile(Tileset tileset, Tile tile)
    {
        if (tileset == null) return;

        bool IsFilled(int dx, int dy)
        {
            var (_t, chunk) =
                GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy);
            if (chunk == null || chunk.isAirlock == true)
                return tileset.flowIntoOutside;
            else if (_t == null)
                return false;
            else if (_t.tileName == "BLANK:outside")
                return true;
            else if (tile.mapTx + dx == this.containerChunk.xMin)
            {
                if (_t.spec.shape == Tile.Shape.CurveBL3Plus1) return true;
                if (_t.spec.shape == Tile.Shape.CurveTL3Plus1) return true;
            }
            else if (tile.mapTx + dx == this.containerChunk.xMax)
            {
                if (_t.spec.shape == Tile.Shape.CurveBR3Plus1) return true;
                if (_t.spec.shape == Tile.Shape.CurveTR3Plus1) return true;
            }
            return false;
        }

        // Sprite Select(Sprite[] sprites) =>
        //     sprites[UnityEngine.Random.Range(0, sprites.Length)];

        Sprite Select(Sprite[] sprites)
        {
            if (this.containerChunk.map.theme == Theme.TombstoneHill)
            {
                return sprites[(tile.gridTx + tile.gridTy) % 4];
            }
            else
                return sprites[UnityEngine.Random.Range(0, sprites.Length)];
        }


        var l  = IsFilled(-1,  0);
        var r  = IsFilled( 1,  0);
        var b  = IsFilled( 0, -1);
        var t  = IsFilled( 0,  1);
        var tl = IsFilled(-1,  1);
        var tr = IsFilled( 1,  1);
        var bl = IsFilled(-1, -1);
        var br = IsFilled( 1, -1);

        var allSides = l && r && t && b;
        var allCorners = tl && tr && bl && br;

        if (this.containerChunk.map.theme == Theme.WindySkies)
        {            
            /* windy skies behaves a little differently, and different combinations tend to come up */
            if (l == true && r == true && t == false && b == false) tile.sprite = Select(tileset.top);
            else if (l == false && r == false && t == true && b == true) tile.sprite = Select(tileset.left);
            else if (l == true && r == false && t == true && b == false) tile.sprite = tileset.bottomRight;
            else if (l == false && r == true && t == true && b == false) tile.sprite = tileset.bottomLeft;
            else if (l == true && r == false && t == false && b == true) tile.sprite = tileset.topRight;
            else if (l == false && r == true && t == false && b == true) tile.sprite = tileset.topLeft;
            else tile.sprite = Select(tileset.singleSquare);
        }

        else if (allSides == true && allCorners == true) tile.sprite = Select(tileset.left);

        else if (l == false && r == true && b == true && t == true) tile.sprite = Select(tileset.left);
        else if (l == true && r == false && b == true && t == true) tile.sprite = Select(tileset.right);
        else if (l == true && r == true && b == false && t == true) tile.sprite = Select(tileset.bottom);
        else if (l == true && r == true && b == true && t == false) tile.sprite = Select(tileset.top);

        else if (l == true && bl == true && b == true && r == false && t == false) tile.sprite = tileset.topRight;
        else if (r == true && br == true && b == true && l == false && t == false) tile.sprite = tileset.topLeft;
        else if (l == true && tl == true && t == true && r == false && b == false) tile.sprite = tileset.bottomRight;
        else if (r == true && tr == true && t == true && l == false && b == false) tile.sprite = tileset.bottomLeft;

        else if (allSides == true && tl == false && tr == true && bl == true && br == true) tile.sprite = tileset.insideTopLeft;
        else if (allSides == true && tl == true && tr == false && bl == true && br == true) tile.sprite = tileset.insideTopRight;
        else if (allSides == true && tl == true && tr == true && bl == false && br == true) tile.sprite = tileset.insideBottomLeft;
        else if (allSides == true && tl == true && tr == true && bl == true && br == false) tile.sprite = tileset.insideBottomRight;

        else tile.sprite = Select(tileset.left);

        if (tileset.interiorUnderneathEdges == true)
        {
            tile.addInteriorFromTileset = tileset;
        }
    }

    private Tile[] GetConnectedTiles(Tile tile)
    {
        var result = new HashSet<Tile>();
        var toCheck = new List<Tile> { tile };

        while (toCheck.Count() > 0)
        {
            Tile current = toCheck[0];
            toCheck.RemoveAt(0);
            result.Add(current);

            void MaybeAdd(int dx, int dy)
            {
                var next = GetTile(current.gridTx + dx, current.gridTy + dy);
                if (next == null) return;
                if (toCheck.Contains(next)) return;
                if (result.Contains(next)) return;
                toCheck.Add(next);
            }

            MaybeAdd(-1, 0);
            MaybeAdd(+1, 0);
            MaybeAdd(0, -1);
            MaybeAdd(0, +1);
        }

        return result.ToArray();
    }

    private void CreatePillarsForBackLayer(Tileset tileset)
    {
        if (tileset.pillar2WideMiddle.Length == 0)
            return;

        var tiles = new HashSet<Tile>(this.tiles.Where(t => t != null));
        
        while (tiles.Count > 0)
        {
            Tile firstTile = tiles.First();
            Tile[] group = GetConnectedTiles(firstTile);
            foreach (var tile in group)
                tiles.Remove(tile);

            int minTx = group.Select(t => t.gridTx).Min();
            int maxTx = group.Select(t => t.gridTx).Max();
            int width = 1 + maxTx - minTx;
            if (width < 6) continue;

            int spacing = 6;
            int pillarCount = (width - 6) / spacing;
            if (pillarCount < 0) pillarCount = 0;

            int centerTx = (minTx + maxTx) / 2;
            int leftTx = centerTx - (pillarCount * (spacing / 2));
            
            foreach (var tile in group)
            {
                if (tile.gridTx < leftTx) continue;
                
                int pillarNumber = (tile.gridTx - leftTx) / spacing;
                if (tile.gridTx - leftTx != pillarNumber * spacing)
                    continue;

                var above    = GetTile(tile.gridTx, tile.gridTy + 1);
                var below    = GetTile(tile.gridTx, tile.gridTy - 1);
                var isTop    = above == null || group.Contains(above) == false;
                var isBottom = below == null || group.Contains(below) == false;

                Sprite sprite;
                if (isTop == true && isBottom == true)
                    continue;
                else if (isTop == true)
                    sprite = tileset.pillar2WideTop;
                else if (isBottom == true)
                    sprite = tileset.pillar2WideBottom;
                else
                    sprite = Util.RandomChoice(tileset.pillar2WideMiddle);

                var gameObject = new GameObject();
                gameObject.name = $"tile ({tile.mapTx}, {tile.mapTy}) pillar";
                gameObject.transform.position = new Vector3(tile.mapTx, tile.mapTy);
                gameObject.transform.parent = this.containerChunk.transform;

                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = sprite;
                spriteRenderer.sortingLayerName = "Tiles - Back 2";
                spriteRenderer.sortingOrder = 0;
            }
        }
    }

    private void SwapOutBottomCornerTilesForWindySkies(Tileset tileset)
    {
        if (tileset.bottomLeftWithoutRoundedCorner == null) return;
        if (tileset.bottomRightWithoutRoundedCorner == null) return;

        bool ItemExistsAt(int mapTx, int mapTy)
        {
            var item = this.containerChunk.ItemAt(mapTx, mapTy);
            return
                (item != null) &&
                (item is SpikeTile || item is Pipe) &&
                (item.group == this.group);
        }

        bool BackTileExistsAt(int gridTx, int gridTy)
        {
            if (this.group != null) return false;

            var tile = this.containerChunk.backTileGrid1.GetTile(gridTx, gridTy);
            return tile != null;
        }

        foreach (var tile in this.tiles)
        {
            if (tile == null)
                continue;
            if (tile.sprite != tileset.bottomLeft && tile.sprite != tileset.bottomRight)
                continue;

            var useFlatCorners =
                ItemExistsAt(tile.mapTx, tile.mapTy - 1) ||
                ItemExistsAt(tile.mapTx - 1, tile.mapTy) ||
                ItemExistsAt(tile.mapTx + 1, tile.mapTy) ||
                BackTileExistsAt(tile.gridTx, tile.gridTy) ||
                BackTileExistsAt(tile.gridTx, tile.gridTy - 1) ||
                BackTileExistsAt(tile.gridTx - 1, tile.gridTy) ||
                BackTileExistsAt(tile.gridTx + 1, tile.gridTy);
            if (useFlatCorners == false)
                continue;

            if (tile.sprite == tileset.bottomLeft)
                tile.sprite = tileset.bottomLeftWithoutRoundedCorner;
            else if (tile.sprite == tileset.bottomRight)
                tile.sprite = tileset.bottomRightWithoutRoundedCorner;
        }
    }

    public bool IsValidLocation(int gridTx, int gridTy)
    {
        if (gridTx < 0 || gridTx >= columns) return false;
        if (gridTy < 0 || gridTy >= rows)    return false;
        return true;
    }

    public Tile GetTile(int gridTx, int gridTy)
    {
        // note: the tile you get back might have a different tx, ty

        if (gridTx < 0 || gridTx >= columns) return null;
        if (gridTy < 0 || gridTy >= rows)    return null;
        return this.tiles[gridTx + (gridTy * columns)];
    }

    public Tile GetTileWithoutBoundsCheck(int gridTx, int gridTy)
    {
        return this.tiles[gridTx + (gridTy * columns)];
    }

    public Tile GetTileAtPixel(int px, int py)
    {
        var tx = Mathf.FloorToInt((float)px / Tile.Size);
        var ty = Mathf.FloorToInt((float)py / Tile.Size);
        return GetTile(tx, ty);
    }

    public int? GetTileIndex(int tx, int ty)
    {
        if (tx < 0 || tx >= columns) return null;
        if (ty < 0 || ty >= rows   ) return null;
        return tx + (ty * columns);
    }

    public void UpdateTilePositions()
    {
        if (this.tileGridMesh != null)
        {
            this.tileGridMesh.transform.position = new Vector2(
                (this.xMin * Tile.Size) + this.offset.x,
                (this.yMin * Tile.Size) + this.offset.y
            );
        }

        for (var ty = 0; ty < this.rows; ty += 1)
        {
            for (var tx = 0; tx < this.columns; tx += 1)
            {
                var t = this.tiles[tx + (ty * columns)];
                if (t == null) continue;
                if (t.mapTx != tx + this.xMin) continue;
                if (t.mapTy != ty + this.yMin) continue;

                var pos = new Vector2(
                    (t.mapTx * Tile.Size) + this.offset.x,
                    (t.mapTy * Tile.Size) + this.offset.y
                );
                if (t.spriteRenderer != null)
                    t.spriteRenderer.transform.position = pos;
                if (t.interior != null)
                    t.interior.transform.position = pos;
                if (t.backMeshRenderers != null)
                {
                    foreach (var m in t.backMeshRenderers)
                    {
                        if (m != null)
                            m.transform.position = pos;
                    }
                }

                if (t.foliage != null)
                {
                    foreach (var s in t.foliage)
                    {
                        s.spriteRenderer.transform.position = pos + s.offset;
                    }
                }
            }
        }
    }

    public XY? PositionOfSurfaceTileNextAlong(Tile t)
    {
        // assume this tile is the ground.
        // give me the (x,y) of wherever we should look for the next ground tile
        // (the one that would be on the right,
        // but might also be up or down if it's a slope)

        int tx = t.mapTx;
        int ty = t.mapTy;

        switch (t.spec.shape)
        {
            case Tile.Shape.Square: break;
            case Tile.Shape.Large2x1: tx = t.mapTx + 1; break;
            case Tile.Shape.Large1x2: ty = t.mapTy + 1; break;
            case Tile.Shape.TopLine: break;
            case Tile.Shape.Slope45BL: ty = t.mapTy - 1; break;
            case Tile.Shape.Slope45BR: break;
            case Tile.Shape.Slope45TL: break;
            case Tile.Shape.Slope45TR: break;
            case Tile.Shape.Slope22BR: tx = t.mapTx + 1; break;
            case Tile.Shape.Slope22BL: tx = t.mapTx + 1; ty = t.mapTy - 1; break;
            case Tile.Shape.CurveBR3: return null;
            case Tile.Shape.CurveTR3: tx = t.mapTx + 2; break;
            case Tile.Shape.CurveTL3: tx = t.mapTx + 2; break;
            case Tile.Shape.CurveBL3: tx = t.mapTx + 2; break;
            case Tile.Shape.CurveBR3Plus1: return null;
            case Tile.Shape.CurveTR3Plus1: tx = t.mapTx + 3; break;
            case Tile.Shape.CurveTL3Plus1: tx = t.mapTx + 3; break;
            case Tile.Shape.CurveBL3Plus1: tx = t.mapTx + 3; break;
            case Tile.Shape.CurveBR4Plus1: return null;
            case Tile.Shape.CurveTR4Plus1: tx = t.mapTx + 4; break;
            case Tile.Shape.CurveTL4Plus1: tx = t.mapTx + 4; break;
            case Tile.Shape.CurveBL4Plus1: tx = t.mapTx + 4; break;
            case Tile.Shape.CurveBR8Plus1: return null;
            case Tile.Shape.CurveTR8Plus1: tx = t.mapTx + 8; break;
            case Tile.Shape.CurveTL8Plus1: tx = t.mapTx + 8; break;
            case Tile.Shape.CurveBL8Plus1: tx = t.mapTx + 8; break;
            case Tile.Shape.CurveOutBR3: tx = t.mapTx + 2; ty = t.mapTy + 2; break;
            case Tile.Shape.CurveOutTR3: return null;
            case Tile.Shape.CurveOutTL3: tx = t.mapTx + 2; ty = t.mapTy + 2; break;
            case Tile.Shape.CurveOutBL3: tx = t.mapTx + 2; ty = t.mapTy + 2; break;
        }
        tx += 1;

        return new XY(tx, ty);
    }

    public bool AreTileTopsConnected(Tile t1, Tile t2)
    {
        // this technically isn't quite enough because a ground 48_16 tile actually
        // has two "tops" one flat one and one curvy one, and this function
        // can't distinguish between them. but it's close enough.

        if (t1 == t2)
            return true;

        Tile TileAt(int mapTx, int mapTy) => GetTile(
            mapTx - this.xMin,
            mapTy - this.yMin
        );

        if (t1.mapTx > t2.mapTx)
        {
            var temp = t2;
            t2 = t1;
            t1 = temp;
        }
        if (t2.mapTx < t1.mapTx + t1.spec.spanX)
            return false;

        int tx = t1.mapTx;
        int ty = t1.mapTy;

        while (tx < t2.mapTx + t2.spec.spanX)
        {
            var t = TileAt(tx, ty);
            if (t == null) return false;
            if (t == t2) return true;

            // find whichever (tx, ty) this tile ends on the top-right
            var onRight = PositionOfSurfaceTileNextAlong(t);
            if (onRight.HasValue == false)
                return false;

            tx = onRight.Value.x;
            ty = onRight.Value.y;

            var above = TileAt(tx, ty + 1);
            if (above != null)
            {
                if (above.spec.shape == Tile.Shape.Slope22BR ||
                    above.spec.shape == Tile.Shape.Slope45BR)
                {
                    ty += 1;
                }
                else
                {
                    return false;
                }
            }
        }

        return false;
    }
}
