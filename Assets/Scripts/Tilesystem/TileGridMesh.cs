using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileGridMesh : MonoBehaviour
{
    [NonSerialized] public MeshRenderer meshRendererForThemedTiles;
    // ^^ this field is for temperature animation, to avoid applying
    // that animation to checkerboard tiles at the end of the level

    private class MeshData
    {
        public Texture2D texture;
        public List<Vector3> vertices = new();
        public List<Vector2> uvs      = new();
        public List<Color>   colors   = new();
        public List<int>     indices  = new();
    }

    private List<MeshData> meshDatasCurrentLayer = new();
    private List<MeshData> meshDatasPreviousLayer = new();

    public static TileGridMesh CreateAt(TileGrid tileGrid, Transform parent)
    {
        var container = new GameObject();
        container.name = "Tile Grid Mesh";
        container.transform.SetParent(parent, false);
        container.transform.position = new Vector2(tileGrid.xMin, tileGrid.yMin);

        var mesh = container.AddComponent<TileGridMesh>();
        return mesh;
    }

    private MeshData MeshDataForTexture(Texture2D texture)
    {
        for (int n = 0; n < this.meshDatasCurrentLayer.Count; n += 1)
        {
            if (this.meshDatasCurrentLayer[n].texture == texture)
                return this.meshDatasCurrentLayer[n];
        }
        
        var result = new MeshData();
        result.texture = texture;
        this.meshDatasCurrentLayer.Add(result);
        return result;
    }

    public void AddSprite(Tile tile, Sprite sprite, Color color)
    {
        AddSprite(tile.gridTx, tile.gridTy, sprite, color);
    }

    public void AddSprite(int tx, int ty, Sprite sprite, Color color)
    {
        if (sprite == null) return;

        var meshData = MeshDataForTexture(sprite.texture);
        int firstVertex = meshData.vertices.Count;

        foreach (var v in sprite.vertices)
        {
            meshData.vertices.Add(v + new Vector2(tx, ty));
            meshData.colors.Add(color);
        }
        foreach (var uv in sprite.uv)
        {
            meshData.uvs.Add(uv);
        }
        foreach (var i in sprite.triangles)
        {
            meshData.indices.Add(firstVertex + i);
        }
    }

    public void AddSprite(Matrix4x4 matrix, Sprite sprite, Color color)
    {
        if (sprite == null) return;

        var meshData = MeshDataForTexture(sprite.texture);
        int firstVertex = meshData.vertices.Count;

        foreach (var v in sprite.vertices)
        {
            meshData.vertices.Add(matrix.MultiplyPoint3x4(v));
            meshData.colors.Add(color);
        }
        foreach (var uv in sprite.uv)
        {
            meshData.uvs.Add(uv);
        }
        foreach (var i in sprite.triangles)
        {
            meshData.indices.Add(firstVertex + i);
        }
    }

    private static int[] TriangleFan3 = new [] { 0, 1, 2 };
    private static int[] TriangleFan4 = new [] { 0, 1, 2, 0, 2, 3 };
    private static int[] TriangleFan5 = new [] { 0, 1, 2, 0, 2, 3, 0, 3, 4 };
    private static int[] TriangleFan6 = new [] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5 };

    private static int[] TriangleFan(int indexCount)
    {
        if (indexCount == 3) return TriangleFan3;
        if (indexCount == 4) return TriangleFan4;
        if (indexCount == 5) return TriangleFan5;
        if (indexCount == 6) return TriangleFan6;
        return null;
    }

    public void AddInterior(Tile tile, Tileset tileset, Color color)
    {
        TileTexture.FillDictionary();

        for (int dx = 0; dx < tile.spec.spanX; dx += 1)
        {
            for (int dy = 0; dy < tile.spec.spanY; dy += 1)
            {
                var sprite = tileset.InteriorTile(tile.mapTx + dx, tile.mapTy + dy);
                var meshData = MeshDataForTexture(sprite.texture);

                var uvXs = sprite.uv.Select(v => v.x);
                var uvYs = sprite.uv.Select(v => v.y);
                Vector2 min = new Vector2(uvXs.Min(), uvYs.Min());
                Vector2 max = new Vector2(uvXs.Max(), uvYs.Max());
                Vector2 mid = (min + max) * 0.5f;

                var shape = TileTexture.ShapeOfTile(tile.spec.shape, dx, dy);
                if (shape == null) continue;

                int firstIndex = meshData.vertices.Count;
                foreach (var shapeVertex in shape)
                {
                    meshData.vertices.Add(new Vector3(
                        dx + shapeVertex.x + tile.gridTx,
                        dy + shapeVertex.y + tile.gridTy,
                        0
                    ));
                    meshData.uvs.Add(new Vector2(
                        Mathf.Lerp(min.x, max.x, shapeVertex.x),
                        Mathf.Lerp(min.y, max.y, shapeVertex.y)
                    ));
                    meshData.colors.Add(color);
                }
                foreach (var shapeIndex in TriangleFan(shape.Length))
                {
                    meshData.indices.Add(firstIndex + shapeIndex);
                }
            }
        }
    }

    public void MarkEndOfLayer()
    {
        this.meshDatasPreviousLayer.AddRange(this.meshDatasCurrentLayer);
        this.meshDatasCurrentLayer.Clear();
    }

    private void MergeMeshDatas()
    {
        for (int n = 1; n < this.meshDatasPreviousLayer.Count; n += 1)
        {
            var first = this.meshDatasPreviousLayer[n - 1];
            var second = this.meshDatasPreviousLayer[n];
            if (first.texture != second.texture)
                continue;

            int firstVertexCount = first.vertices.Count;
            first.vertices.AddRange(second.vertices);
            first.uvs.AddRange(second.uvs);
            first.colors.AddRange(second.colors);
            for (int i = 0; i < second.indices.Count; i += 1)
            {
                first.indices.Add(second.indices[i] + firstVertexCount);
            }

            this.meshDatasPreviousLayer.RemoveAt(n);
            n -= 1;
        }
    }

    private void AddMeshRendererToGameObject(
        MeshData meshData, GameObject gameObject, string sortingLayerName, bool isThemed
    )
    {
        var mesh       = new Mesh();
        mesh.name      = "Generated tile mesh";
        mesh.vertices  = meshData.vertices.ToArray();
        mesh.uv        = meshData.uvs.ToArray();
        mesh.colors    = meshData.colors.ToArray();
        mesh.triangles = meshData.indices.ToArray();

        var filter = gameObject.AddComponent<MeshFilter>();
        filter.mesh = mesh;

        var mr = gameObject.AddComponent<MeshRenderer>();
        mr.material = new Material(Shader.Find("Sprites/Default"));
        mr.material.mainTexture = meshData.texture;
        mr.sortingLayerName = sortingLayerName;

        if (isThemed == true)
            this.meshRendererForThemedTiles = mr;
    }

    public void FinishCreating(Texture2D textureForThemedTiles, string sortingLayerName)
    {
        if (this.meshDatasCurrentLayer.Count > 0)
            MarkEndOfLayer();

        MergeMeshDatas();

        if (this.meshDatasPreviousLayer.Count == 1)
        {
            AddMeshRendererToGameObject(
                this.meshDatasPreviousLayer[0],
                this.gameObject,
                sortingLayerName,
                this.meshDatasPreviousLayer[0].texture == textureForThemedTiles
            );
        }
        else if (this.meshDatasPreviousLayer.Count > 1)
        {
            for (int n = 0; n < this.meshDatasPreviousLayer.Count; n += 1)
            {
                var child = new GameObject();
                child.name = $"Mesh {n + 1}";
                child.transform.SetParent(this.transform, false);

                AddMeshRendererToGameObject(
                    this.meshDatasPreviousLayer[n],
                    child,
                    sortingLayerName,
                    this.meshDatasPreviousLayer[n].texture == textureForThemedTiles
                );
            }
        }
    }
}
