using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using Rnd = UnityEngine.Random;

public partial class TileGrid
{
    public void AddFoliage(TileTheme tileTheme, bool[,] permittedInTileSpace)
    {
        if (tileTheme.foliageTallAnimated.Length == 0 &&
            tileTheme.foliageShortAnimated.Length == 0)
        {
            return;
        }

        float YAtTop(Tile tile, float alongX)
        {
            if (tile == null) return 1;
            for (float y = tile.spec.spanY; y >= 0; y -= 0.1f)
            {
                if (tile.IsSolidAtPoint(alongX, y)) return y;
            }
            return 0;
        }

        bool IsFilled(int gridTx, int gridTy)
        {
            // a couple of uses of this below, and i'm not sure all of the
            // nuances of them are the same as each other.
            var (t, chunk) = GetTileForAutotile(gridTx, gridTy);
            if (chunk == null) return true;
            if (t == null) return false;
            if (t.spec.shape == Tile.Shape.TopLine) return false;
            return true;
        }

        for (int y = 0; y < this.rows; y += 1)
        {
            for (int x = 0; x < this.columns; x += 1)
            {
                var tile = this.tiles[x + (y * this.columns)];
                if (tile == null) continue;

                if (IsFilled(x, y + 1)) continue;
                if (Rnd.Range(0f, 1f) < 0.7f) continue;
                if (permittedInTileSpace[x, y] == false) continue;

                int tryClusterSize = Rnd.Range(2, 5);

                if (tileTheme == Assets.instance.sunkenIsland?.tileTheme)
                {
                    tryClusterSize = 2;
                }
                else if (tileTheme == Assets.instance.tombstoneHill?.tileTheme &&
                    this.containerChunk.isHorizontal == false)
                {
                    tryClusterSize = 1;
                    if (Rnd.Range(0, 10) != 0) continue;
                }

                int clusterSize = 1;

                for (int n = 1; n < tryClusterSize; n += 1)
                {
                    int leftX = x + (n / 2);
                    int rightX = x + 1 + (n / 2);
                    if (rightX >= this.columns) break;

                    var atRight = GetTileForAutotile(rightX, y);
                    if (atRight.chunk == null) break;
                    if (atRight.tile == null) break;

                    if (IsFilled(rightX, y + 1) == true) break;
                    if (permittedInTileSpace[rightX, y] == false) break;
                    clusterSize = 2 + n;
                }

                int tallPlantIndex = -1;

                var foliage = new List<Tile.Foliage>();
                var actualTile = tile;
                float actualAlongX = (tile.tileGrid.xMin + x) - tile.mapTx;

                for (int n = 0; n < clusterSize; n += 1)
                {
                    if (n == 2 * actualTile.spec.spanX)
                    {
                        var pos = PositionOfSurfaceTileNextAlong(actualTile);
                        if (pos == null) break;
                        actualTile = GetTileForAutotile(
                            pos.Value.x - this.xMin,
                            pos.Value.y - this.yMin
                        ).tile;
                        actualAlongX = 0.5f;
                        if (actualTile == null) break;
                    }
                    else
                    {
                        actualAlongX += 0.5f;
                    }

                    TileTheme.FoliageDefinition[] candidates;
                    if (n == tallPlantIndex)
                        candidates = tileTheme.foliageTallAnimated;
                    else
                        candidates = tileTheme.foliageShortAnimated;

                    float topY = YAtTop(actualTile, actualAlongX);

                    var gameObject = new GameObject();
                    gameObject.name =
                        $"tile ({tile.mapTx}, {tile.mapTy}) foliage";
                    gameObject.transform.position = new Vector3(
                        actualTile.mapTx + actualAlongX,
                        actualTile.mapTy + topY
                    );
                    if (this.group != null)
                        gameObject.transform.parent = this.group.container.transform;
                    else
                        gameObject.transform.parent = this.containerChunk.transform;

                    var choice = Util.RandomChoice(candidates);

                    if(choice.prefab == null)
                    {
                        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                        spriteRenderer.sprite = choice.idleAnimation.sprites[0];
                        if (Rnd.Range(0f, 1f) <= 0.7f)
                        {
                            spriteRenderer.sortingLayerName = "Tiles - Foliage";
                            spriteRenderer.sortingOrder = 0;
                        }
                        else
                        {
                            spriteRenderer.sortingLayerName = "Tiles - Solid";
                            spriteRenderer.sortingOrder = -1;
                        }

                        if (this.map.theme == Theme.TombstoneHill)
                            spriteRenderer.color = new Color(0.8f, 0.8f, 0.8f, 1);

                        var animated = gameObject.AddComponent<Animated>();
                        animated.PlayAndLoop(choice.idleAnimation);
                        animated.playbackSpeed = Rnd.Range(0.9f, 1.1f);

                        foliage.Add(new Tile.Foliage
                        {
                            definition = choice,
                            sprite = spriteRenderer.sprite,
                            spriteRenderer = spriteRenderer,
                            animated = animated,
                            offset = new Vector2(actualAlongX, topY)
                        });
                    }
                    else
                    {
                        if (Rnd.Range(0f, 1f) < 0.7f) continue;
                        var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                        var animated = gameObject.AddComponent<Animated>();
                        GameObject anim = GameObject.Instantiate(choice.prefab);
                        anim.transform.parent = gameObject.transform;
                        anim.transform.localPosition = new Vector3(0, 0, 0);
                        foliage.Add(new Tile.Foliage
                        {
                            definition = choice,
                            sprite = null,
                            spriteRenderer = spriteRenderer,
                            animated = animated,
                            offset = new Vector2(actualAlongX, topY)
                        });
                    }
                }
                tile.foliage = foliage.ToArray();

                x += clusterSize / 2;
            }
        }
    }

    public void AddSand(TileTheme tileTheme)
    {
        var normal = tileTheme.normalTiles;
        var sand = tileTheme.conflictCanyonSand;

        foreach (var tile in this.tiles)
        {
            if (tile == null) continue;
            if (tile.sprite == null) continue;
            if (tile.sand.first != null) continue;

            var pos = new Vector2(tile.mapTx, tile.mapTy);

            if (normal.top.Contains(tile.sprite))
                CreateSand(tile, sand.top[0], pos);
            else if (tile.sprite == normal.topLeft)
                CreateSand(tile, sand.topLeft, pos);
            else if (tile.sprite == normal.topRight)
                CreateSand(tile, sand.topRight, pos);
            else if (tile.sprite == normal.slopeJoined45BottomLeft)
                CreateSand(tile, sand.slopeJoined45BottomLeft, pos);
            else if (tile.sprite == normal.slopeJoined45BottomRight)
                CreateSand(tile, sand.slopeJoined45BottomRight, pos);
            else if (tile.sprite == normal.slopeJoinedLeft45BottomLeft)
                CreateSand(tile, sand.slopeJoinedLeft45BottomLeft, pos);
            else if (tile.sprite == normal.slopeJoinedLeft45BottomRight)
                CreateSand(tile, sand.slopeJoinedLeft45BottomRight, pos);
            else if (tile.sprite == normal.slopeJoinedRight45BottomLeft)
                CreateSand(tile, sand.slopeJoinedRight45BottomLeft, pos);
            else if (tile.sprite == normal.slopeJoinedRight45BottomRight)
                CreateSand(tile, sand.slopeJoinedRight45BottomRight, pos);
            else if (tile.sprite == normal.slopeJoined22BottomLeft)
            {
                CreateSand(tile, sand.slopeJoined22BottomLeft, pos);
                CreateSand(tile, sand.slopeJoined22BottomLeft, pos.Add(1, -0.5f));
            }
            else if (tile.sprite == normal.slopeJoinedLeft22BottomLeft)
            {
                CreateSand(tile, sand.slopeJoinedLeft22BottomLeft, pos);
                CreateSand(tile, sand.slopeJoined22BottomLeft, pos.Add(1, -0.5f));
            }
            else if (tile.sprite == normal.slopeJoinedRight22BottomLeft)
            {
                CreateSand(tile, sand.slopeJoined22BottomLeft, pos);
                CreateSand(tile, sand.slopeJoinedRight22BottomLeft, pos.Add(1, -0.5f));
            }
            else if (tile.sprite == normal.slopeJoined22BottomRight)
            {
                CreateSand(tile, sand.slopeJoined22BottomRight, pos);
                CreateSand(tile, sand.slopeJoined22BottomRight, pos.Add(1, 0.5f));
            }
            else if (tile.sprite == normal.slopeJoinedLeft22BottomRight)
            {
                CreateSand(tile, sand.slopeJoinedLeft22BottomRight, pos);
                CreateSand(tile, sand.slopeJoined22BottomRight, pos.Add(1, 0.5f));
            }
            else if (tile.sprite == normal.slopeJoinedRight22BottomRight)
            {
                CreateSand(tile, sand.slopeJoined22BottomRight, pos);
                CreateSand(tile, sand.slopeJoinedRight22BottomRight, pos.Add(1, 0.5f));
            }
        }

        void CreateSand(Tile tile, Sprite sprite, Vector2 position)
        {
            var go = new GameObject($"Sand ({position.x}, {position.y})");
            go.transform.SetParent(this.containerChunk.transform);
            go.transform.position = position;

            var sr = go.AddComponent<SpriteRenderer>();
            sr.sprite = sprite;
            sr.sortingLayerName = "Tiles - Sand";

            var sand = go.AddComponent<Sand>();
            if (tile.sand.first == null)
                tile.sand.first = sand;
            else
                tile.sand.second = sand;
        }
    }

    public void AddDecorationSpritesToSolid(TileTheme tileTheme)
    {
        var decorationTypes = tileTheme.decorationSprites.Where(
            d => d.style == TileTheme.DecorationStyle.OnSolidTiles
        ).ToArray();
        if (decorationTypes.Length == 0)
            return;

        bool SolidDecorationIsCandidate(XY xy, TileTheme.DecorationDefinition decoration)
        {
            int minTx = xy.x - decoration.reachLeft;
            int maxTx = xy.x + decoration.reachRight;
            int minTy = xy.y - decoration.reachBelow;
            int maxTy = xy.y + decoration.reachAbove;

            if (minTx < 0 || minTx > this.columns) return false;
            if (minTy < 0 || minTy > this.rows)    return false;

            for (int x = minTx; x <= maxTx; x += 1)
            {
                for (int y = minTy; y <= maxTy; y += 1)
                {
                    if (GetTile(x, y)?.decorationSprite != null)
                        return false;
                }
            }

            int startX = -2;
            int startY = -2;
            int endX = 1;
            int endY = 1;

            if (tileTheme == Assets.instance.sunkenIsland?.tileTheme)
            {
                startX = -1;
                startY = -1;
                endX = 0;
                endY = 0;
            }
            for (int dx = startX; dx <= endX; dx += 1)
            {
                for (int dy = startY; dy <= endY; dy += 1)
                {
                    var t = GetTile(xy.x + dx, xy.y + dy);
                    if (t?.spec.shape != Tile.Shape.Square)
                        return false;
                    if (t?.tileName != "BLANK:square")
                        return false;
                    if (decoration.onlyOnInterior == true && t?.interior == null)
                        return false;
                }
            }
            
            return true;
        }

        for (int x = 0; x <= this.columns; x += 1)
        {
            for (int y = 0; y <= this.rows; y += 1)
            {
                var t = GetTile(x, y);
                if (t?.spec.shape != Tile.Shape.Square) continue;
                if (tileTheme == Assets.instance.sunkenIsland?.tileTheme)
                {
                    if (UnityEngine.Random.value > 0.15f ) continue;
                }
                else
                {
                    if (Rnd.Range(0, 3) != 1) continue;
                }

                var decoration = Util.RandomChoice(decorationTypes);
                if (SolidDecorationIsCandidate(new XY(x, y), decoration) == false) continue;

                if(decoration.prefab != null)
                {
                    var gameObject = GameObject.Instantiate(decoration.prefab);
                    gameObject.name = $"tile ({t.mapTx}, {t.mapTy}) decoration";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(t.mapTx, t.mapTy);

                    t.decorationSprite = new Tile.DecorationSprite();
                    t.decorationSprite.sprite = decoration.sprite;
                    t.decorationSprite.offset = new Vector2(0, 0);
                }
                else // use sprite
                {
                    var gameObject = new GameObject();
                    gameObject.name = $"tile ({t.mapTx}, {t.mapTy}) decoration";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(t.mapTx, t.mapTy);

                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = decoration.sprite;
                    spriteRenderer.sortingLayerName = "Tiles - Decorative Sprites";

                    t.decorationSprite = new Tile.DecorationSprite();
                    t.decorationSprite.sprite = spriteRenderer.sprite;
                    t.decorationSprite.offset = new Vector2(0, 0);
                }

                // once per chunk for graffiti (and nothing else uses this code yet)
                if (tileTheme != Assets.instance.sunkenIsland?.tileTheme)
                {
                    return;
                }
            }
        }
    }

    public void AddDecorationSpritesToSecondary(TileTheme tileTheme)
    {
        var decorationTypes = tileTheme.decorationSprites.Where(
            d => d.style == TileTheme.DecorationStyle.OnSecondaryTiles
        ).ToArray();
        if (decorationTypes.Length == 0)
            return;

        bool SecondaryDecorationIsCandidate(XY xy, TileTheme.DecorationDefinition decoration)
        {
            int minTx = xy.x - decoration.reachLeft;
            int maxTx = xy.x + decoration.reachRight;
            int minTy = xy.y - decoration.reachBelow;
            int maxTy = xy.y + decoration.reachAbove;

            if (minTx < 0 || minTx > this.columns) return false;
            if (minTy < 0 || minTy > this.rows) return false;

            for (int x = minTx; x <= maxTx; x += 1)
            {
                for (int y = minTy; y <= maxTy; y += 1)
                {
                    if (GetTile(x, y)?.decorationSprite != null)
                        return false;
                }
            }

            int startX = -1;
            int startY = -1;
            int endX = 0;
            int endY = 0;

            for (int dx = startX; dx <= endX; dx += 1)
            {
                for (int dy = startY; dy <= endY; dy += 1)
                {
                    var t = GetTile(xy.x + dx, xy.y + dy);
                    if (t?.spec.shape != Tile.Shape.Square)
                        return false;
                    if (t?.tileName != "BLANK:dirt")
                        return false;
                    if (decoration.onlyOnInterior == true && t?.interior == null)
                        return false;
                }
            }
            return true;
        }

        for (int x = 0; x <= this.columns; x += 1)
        {
            for (int y = 0; y <= this.rows; y += 1)
            {
                var t = GetTile(x, y);
                if (t?.spec.shape != Tile.Shape.Square) continue;
                if (UnityEngine.Random.value > 0.25f) continue;

                var decoration = Util.RandomChoice(decorationTypes);
                if (SecondaryDecorationIsCandidate(new XY(x, y), decoration) == false) continue;

                if (decoration.prefab != null)
                {
                    var gameObject = GameObject.Instantiate(decoration.prefab);
                    gameObject.name = $"tile ({t.mapTx}, {t.mapTy}) decoration";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(t.mapTx, t.mapTy);

                    t.decorationSprite = new Tile.DecorationSprite();
                    t.decorationSprite.sprite = decoration.sprite;
                    t.decorationSprite.offset = new Vector2(0, 0);
                }
                else // use sprite
                {
                    var gameObject = new GameObject();
                    gameObject.name = $"tile ({t.mapTx}, {t.mapTy}) decoration";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(t.mapTx, t.mapTy);

                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = decoration.sprite;
                    spriteRenderer.sortingLayerName = "Tiles - Decorative Sprites";

                    t.decorationSprite = new Tile.DecorationSprite();
                    t.decorationSprite.sprite = spriteRenderer.sprite;
                    t.decorationSprite.offset = new Vector2(0, 0);
                }
            }
        }
    }

    public void AddPillars(TileTheme tileTheme, bool[,] permitted)
    {
        if (tileTheme.pillar.top == null)
            return;

        (bool, int topGridTy) IsCandidateBaseOfPillar(int gridTx, int gridTy)
        {
            if (gridTx <= 2)                return (false, 0);
            if (gridTx >= this.columns - 1) return (false, 0);
            if (gridTy == 0)                return (false, 0);
            if (gridTy > this.rows - 5)     return (false, 0);

            for (int dx = -2; dx <= 2; dx += 1)
            {
                if (GetTile(gridTx + dx, gridTy - 1)?.spec.shape != Tile.Shape.Square)
                    return (false, 0);
            }

            for (int rowTy = gridTy; rowTy < this.rows; rowTy += 1)
            {
                bool allEmpty = true;
                bool allSquare = true;
                bool forbiddenHere = false;

                for (int dx = -2; dx <= 1; dx += 1)
                {
                    if (permitted[gridTx + dx, rowTy] == false)
                        forbiddenHere = true;

                    var t = GetTile(gridTx + dx, rowTy);
                    if (t == null || t.spec.shape != Tile.Shape.Square)
                        allSquare = false;
                    if (t != null)
                        allEmpty = false;
                }

                if (allEmpty == true && forbiddenHere == false)
                {
                    continue;
                }
                else if (allEmpty == true && forbiddenHere == true)
                {
                    return (false, 0);
                }
                else if (allSquare == true)
                {
                    int height = rowTy - gridTy;
                    if (height > 5)
                        return (true, rowTy - 1);
                    else
                        return (false, 0);
                }
                else
                {
                    return (false, 0);
                }
            }

            return (false, 0); // no top to connect to
        }

        List<(XY bottom, XY top)> PillarsFindCandidates()
        {
            var result = new List<(XY bottom, XY top)>();

            for (int x = 0; x < this.columns; x += 1)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    var (valid, topTy) = IsCandidateBaseOfPillar(x, y);
                    if (valid == true)
                        result.Add((new XY(x, y), new XY(x, topTy)));
                }
            }

            return result;
        }

        int max = Rnd.Range(0, 3);
        for (int n = 0; n < max; n += 1)
        {
            var candidates = PillarsFindCandidates();
            if (candidates.Count == 0)
                break;
            var (bottom, top) = Util.RandomChoice(candidates);

            for (int y = bottom.y; y <= top.y; y += 1)
            {
                int mapTx = bottom.x + this.xMin;
                int mapTy = y        + this.yMin;

                Sprite sprite;
                if (y == top.y)
                {
                    sprite = null;
                }
                else if (y == top.y - 1)
                    sprite = tileTheme.pillar.top;
                else
                    sprite = Util.RandomChoice(tileTheme.pillar.middle);

                if (sprite != null)
                {
                    var gameObject = new GameObject();
                    gameObject.name = $"tile ({mapTx}, {mapTy}) pillar sprite";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(mapTx, mapTy);

                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = sprite;
                    spriteRenderer.sortingLayerName = "Tiles - Surface Decoration";
                }

                for (int dx = -2; dx <= 1; dx += 1)
                {
                    permitted[bottom.x + dx, y] = false;
                }
            }
        }
    }

    public void AddIvy(TileTheme tileTheme, bool[,] permitted)
    {
        if (tileTheme.pillar.top == null)
            return;

        (bool, int topGridTy) IsCandidateBaseOfPillar(int gridTx, int gridTy)
        {
            var back = this.containerChunk.backTileGrid1;
            if (gridTx <= 2) return (false, 0);
            if (gridTx >= this.columns - 1) return (false, 0);
            if (gridTy == 0) return (false, 0);
            if (gridTy > this.rows - 2) return (false, 0);

            for (int dx = -1; dx <= 0; dx += 1)
            {
                if (GetTile(gridTx + dx, gridTy - 1)?.spec.shape != Tile.Shape.Square 
                     && GetTile(gridTx + dx, gridTy - 1)?.spec.shape != Tile.Shape.TopLine)
                    return (false, 0);
                if (back.GetTile(gridTx + dx, gridTy - 1) != null)
                    return (false, 0);
            }

            int availableHeight = 0;
            for (int rowTy = gridTy; rowTy < this.rows; rowTy += 1)
            {
                bool allEmpty = true;
                bool forbiddenHere = false;

                for (int dx = -1; dx <= 0; dx += 1)
                {
                    if (permitted[gridTx + dx, rowTy] == false)
                        forbiddenHere = true;

                    if(back.GetTile(gridTx + dx, rowTy) == null)
                    {
                        allEmpty = false;
                    }

                }

                if (allEmpty == true && forbiddenHere == false)
                {
                    availableHeight++;
                    if(rowTy != this.rows - 1)
                    {
                        continue;
                    }
                    else
                    {
                        if (availableHeight >= 2)
                        {
                            return (true, rowTy - 1);
                        }
                        else
                        {
                            return (false, 0);
                        }
                    }
                }
                else if (allEmpty == true && forbiddenHere == true)
                {
                    if (availableHeight >= 2)
                    {
                        return (true, rowTy - 1);
                    }
                    else
                    {
                        return (false, 0);
                    }
                }
                else
                {
                    if (availableHeight >= 2)
                        return (true, rowTy - 1);
                    else
                        return (false, 0);
                }
            }

            return (false, 0);
        }

        List<(XY bottom, XY top)> IvyFindCandidates()
        {
            var result = new List<(XY bottom, XY top)>();

            for (int x = 0; x < this.columns; x += 1)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    var (valid, topTy) = IsCandidateBaseOfPillar(x, y);

                    if (valid == true)
                    {
                        result.Add((new XY(x, y), new XY(x, topTy)));
                    }
                }
            }

            return result;
        }

        int max = Rnd.Range(3, 5);
        for (int n = 0; n < max; n += 1)
        {
            var candidates = IvyFindCandidates();
            if (candidates.Count == 0)
                break;
            var (bottom, top) = Util.RandomChoice(candidates);

            for (int y = bottom.y; y <= top.y; y += 2)
            {
                int mapTx = bottom.x + this.xMin;
                int mapTy = y + this.yMin;
                bool exitLoop = false;
                Sprite sprite;

                if (top.y - bottom.y > 3)
                {
                    if (y >= top.y - 2)
                    {
                        sprite = tileTheme.pillar.top;
                        exitLoop = true;
                    }
                    else
                    {
                        if (Rnd.Range(0, 3) == 0)
                        {
                            exitLoop = true;
                            sprite = tileTheme.pillar.top;
                        }
                        else
                        {
                            sprite = Util.RandomChoice(tileTheme.pillar.middle);
                        }
                    }
                }
                else
                {
                    sprite = tileTheme.pillar.top;
                    exitLoop = true;
                }

                if (sprite != null)
                {
                    var gameObject = new GameObject();
                    gameObject.name = $"Ivy tile ({mapTx}, {mapTy}) pillar sprite";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(mapTx, mapTy);

                    if(containerChunk.boundaryMarkers.bottomLeft.x != 0 && containerChunk.isHorizontal != true)
                    {
                        gameObject.transform.position = new Vector2(mapTx - containerChunk.boundaryMarkers.bottomLeft.x, mapTy);
                    }

                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = sprite;
                    spriteRenderer.sortingLayerName = "Tiles - Surface Decoration";
                    spriteRenderer.sortingOrder = 1;
                }

                for (int dx = -2; dx <= 1; dx += 1)
                {
                    permitted[bottom.x + dx, y] = false;
                }

                if(exitLoop == true)
                {
                    y = top.y +1;
                }
            }
        }
    }

    public void AddDecorationSpritesToBack(
        TileTheme.DecorationDefinition[] decoration, TileTheme tileTheme, bool[,] permitted
    )
    {
        if (decoration.Length == 0)
            return;

        bool BackDecorationIsCandidate(
        int gridTx, int gridTy, TileTheme.DecorationDefinition decoration, bool[,] permitted
        )
        {
            switch (decoration.usedInChunks)
            {
                case TileTheme.DecorationUsedInChunks.HorizontalOnly:
                    if (this.containerChunk.isHorizontal == false) return false;
                    break;
                case TileTheme.DecorationUsedInChunks.VerticalOnly:
                    if (this.containerChunk.isHorizontal == true) return false;
                    break;
            }

            if (permitted[gridTx, gridTy] == false)             return false;

            int minTx = gridTx - decoration.reachLeft;
            int maxTx = gridTx + decoration.reachRight;
            int minTy = gridTy - decoration.reachBelow;
            int maxTy = gridTy + decoration.reachAbove;
            if (minTx < 0 || maxTx >= this.columns)  return false;
            if (minTy < 0 || maxTy >= this.rows)     return false;

            switch (decoration.style)
            {
                case TileTheme.DecorationStyle.Floor:
                    if (minTy == 0) return false;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, minTy - 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.Ceiling:
                    if (maxTy == this.rows + 1) return false;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, maxTy + 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.FloorThickOnly:
                    if (minTy == 0) return false;
                    if (minTy - 2 < 0) return false;

                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, minTy - 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        
                        t = GetTile(tx, minTy - 2);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;                 

                case TileTheme.DecorationStyle.CeilingThickOnly:
                    if (maxTy == this.rows + 1) return false;
                    if (maxTy + 2 >= this.rows) return false;


                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, maxTy + 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;

                        t = GetTile(tx, maxTy + 2);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.LeftWall:
                    if (minTx == 0) return false;
                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(minTx - 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.RightWall:
                    if (maxTx == this.columns - 1) return false;
                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(maxTx + 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.OnBackTiles:
                    var back = this.containerChunk.backTileGrid1;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        for (int ty = minTy; ty <= maxTy; ty += 1)
                        {
                            if (back.GetTile(tx, ty) == null)
                                return false;
                        }
                    }
                    break;

                case TileTheme.DecorationStyle.LeftWallThickOnly:
                    if (minTx == 0) return false;
                    if  (minTx - 2 < 0) return false;

                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(minTx - 1, ty);

                        if(t == null) return false;
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;

                        t = GetTile(minTx - 2, ty);

                        if(t == null) return false;
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.RightWallThickOnly:
                    if (maxTx == this.columns - 1) return false;
                    if (maxTx + 2 > this.columns - 1) return false;

                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(maxTx + 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;

                        t = GetTile(maxTx + 2, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                    }
                    break;
            }

            for (int tx = minTx; tx <= maxTx; tx += 1)
            {
                for (int ty = minTy; ty <= maxTy; ty += 1)
                {
                    if (permitted[tx, ty] == false)
                        return false;
                }
            }

            return true;
        }

        List<XY> BackDecorationFindCandidates(TileTheme.DecorationDefinition decoration)
        {
            var result = new List<XY>();

            for (int x = 0; x < this.columns; x += 1)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    if (BackDecorationIsCandidate(x, y, decoration, permitted))
                        result.Add(new XY(x, y));
                }
            }

            return result;
        }

        List<(XY, XY)> BackDecorationFindCandidateRanges(TileTheme.DecorationDefinition decoration)
        {
            var result = new List<(XY, XY)>();

            for (int y = 0; y < this.rows; y += 1)
            {
                XY? startOfRange = null;
                for (int x = 0; x < this.columns; x += 1)
                {
                    bool isCandidate = BackDecorationIsCandidate(x, y, decoration, permitted);
                    if (isCandidate == true && startOfRange == null)
                    {
                        startOfRange = new XY(x, y);
                    }
                    else if (isCandidate == false && startOfRange != null)
                    {
                        result.Add((startOfRange.Value, new XY(x - 1, y)));
                        startOfRange = null;
                    }
                }
                if (startOfRange != null)
                {
                    result.Add((startOfRange.Value, new XY(this.columns - 1, y)));
                    startOfRange = null;
                }
            }

            return result;
        }

        int WeightedChoice(int maxExclusive, Span<float> weights)
        {
            // Picks a random number between [0..maxExclusive),
            // weighted by the values in weights[].

            float max = 0;
            for (int n = 0; n < weights.Length; n += 1)
            {
                max += weights[n];
            }

            float value = Rnd.Range(0, max);
            for (int n = 0; n < weights.Length; n += 1)
            {
                if (value < weights[n])
                    return n;
                else
                    value -= weights[n];
            }
            return 0;
        }

        void BackCreateTypes(TileTheme.DecorationDefinition[] decorationStyles, int count)
        {
            if (decorationStyles.Length == 0) return;
            if (count == 0) return;

            Span<float> weights = stackalloc float[decorationStyles.Length];
            for (int n = 0; n < decorationStyles.Length; n += 1)
                weights[n] = 1.0f;

            if (decorationStyles.Length == 0)
                return;

            for (int n = 0; n < count; n += 1)
            {
                var decorationIndex = WeightedChoice(decorationStyles.Length, weights);
                var decoration = decorationStyles[decorationIndex];
                var candidates = BackDecorationFindCandidates(decoration);
                if (candidates.Count == 0)
                    continue;
                var xy = Util.RandomChoice(candidates);
                CreateDecoration(tileTheme, xy, 0, decoration, permitted);
                weights[decorationIndex] *= 0.4f;
            }
        }

        void BackCreateTypesEvenly(
            TileTheme.DecorationDefinition[] decorationStyles, int count
        )
        {
            if (decorationStyles.Length == 0)
                return;

            for (int n = 0; n < count; n += 1)
            {
                var decoration = Util.RandomChoice(decorationStyles);
                var candidates = BackDecorationFindCandidateRanges(decoration);
                if (candidates.Count == 0)
                    continue;
                var (left, right) = Util.RandomChoice(candidates);
                if (right.x - left.x > 2 && Rnd.Range(0.0f, 1.0f) > 0.3f)
                {
                    CreateDecoration(tileTheme, left, 0, decoration, permitted);
                    CreateDecoration(tileTheme, right, 0, decoration, permitted);
                }
                else
                {
                    float x = left.x + ((right.x - left.x) / 2.0f);
                    var xy = new XY(Mathf.FloorToInt(x), left.y);
                    CreateDecoration(tileTheme, xy, x - xy.x, decoration, permitted);
                }
                for (int x = left.x; x <= right.x; x += 1)
                {
                    permitted[x, left.y] = false;
                }
            }
        }

        int ApproxAvailableFloorTiles()
        {
            int result = 0;
            for (int tx = 0; tx < this.columns; tx += 1)
            {
                bool solid = this.GetTile(tx, this.rows - 1) != null;
                for (int ty = this.rows - 2; ty >= 0; ty -= 1)
                {
                    bool solidBelow = solid;
                    solid = this.GetTile(tx, ty) != null;
                    if (solid == false && solidBelow == true)
                        result += 1;
                }
            }
            return result;
        }

        {
            var decorationStyles = decoration.Where(
                d => d.style == TileTheme.DecorationStyle.OnBackTiles
            ).ToArray();

            int count = this.containerChunk.backTileGrid1.tiles.Where(t => t != null).Count();
            if (tileTheme == Assets.instance.sunkenIsland?.tileTheme)
                count = Mathf.RoundToInt(count / 3);
            else
                count = Mathf.RoundToInt(count / 10);

            BackCreateTypes(decorationStyles, count);
        }

        if (tileTheme == Assets.instance.sunkenIsland?.tileTheme && 
            this.containerChunk.isHorizontal == true) return;

        {
            var decorationStyles = decoration.Where(d =>
                d.style == TileTheme.DecorationStyle.Floor
            ).ToArray();

            int count;
            if (tileTheme == Assets.instance.tombstoneHill?.tileTheme)
            {
                count = (
                    front: (decoration == tileTheme.decorationSprites),
                    horz: this.containerChunk.isHorizontal
                ) switch
                {
                    (front: true, horz: false) /* skulls */ =>
                        Mathf.RoundToInt(Rnd.Range(0.3f, 1.0f) * ApproxAvailableFloorTiles()),
                    (front: false, horz: false) /* coffins */ =>
                        Mathf.RoundToInt(Rnd.Range(0.1f, 0.25f) * ApproxAvailableFloorTiles()),

                    (front: true, horz: true) /* tombstones */ =>
                        Mathf.RoundToInt(Rnd.Range(0.15f, 0.35f) * ApproxAvailableFloorTiles()),
                    (front: false, horz: true) /* railings */ =>
                        Mathf.RoundToInt(Rnd.Range(0.3f, 0.8f) * ApproxAvailableFloorTiles()),
                };
            }
            else
                count = Mathf.RoundToInt(Rnd.value * this.rows / 6);

            BackCreateTypes(decorationStyles, count);
        }

        BackCreateTypesEvenly(
            decoration.Where(d =>
                d.style == TileTheme.DecorationStyle.Ceiling
            ).ToArray(),
            Mathf.RoundToInt(Rnd.value * this.rows / 5)
        );

        BackCreateTypes(
            decoration.Where(d =>
                d.style == TileTheme.DecorationStyle.LeftWall ||
                d.style == TileTheme.DecorationStyle.RightWall
            ).ToArray(),
            Mathf.RoundToInt(Rnd.value * this.rows / 20)
        );
    }

    public void AddWindySkiesDecorations(TileTheme tileTheme, bool[,] permitted)
    {
        bool WindySkiesIsCandidate(
                int gridTx, int gridTy, TileTheme.DecorationDefinition decoration, bool[,] permitted
            )
        {
            if (permitted[gridTx, gridTy] == false) return false;

            int minTx = gridTx - decoration.reachLeft;
            int maxTx = gridTx + decoration.reachRight;
            int minTy = gridTy - decoration.reachBelow;
            int maxTy = gridTy + decoration.reachAbove;
            if (minTx < 0 || maxTx >= this.columns) return false;
            if (minTy < 0 || maxTy >= this.rows) return false;

            switch (decoration.style)
            {
                case TileTheme.DecorationStyle.Floor:
                    if (minTy == 0) return false;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, minTy - 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        // if(t?.secondary == true)
                        //     return false;
                    }
                    break;

                case TileTheme.DecorationStyle.Ceiling:
                    if (maxTy == this.rows + 1) return false;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, maxTy + 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.FloorThickOnly:
                    if (minTy == 0) return false;
                    if (minTy - 2 < 0) return false;

                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, minTy - 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;

                        t = GetTile(tx, minTy - 2);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        // if(t?.secondary == true)
                        //     return false;
                    }
                    break;

                case TileTheme.DecorationStyle.CeilingThickOnly:
                    if (maxTy == this.rows + 1) return false;
                    if (maxTy + 2 >= this.rows) return false;

                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        var t = GetTile(tx, maxTy + 1);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;

                        t = GetTile(tx, maxTy + 2);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.LeftWall:
                    if (minTx == 0) return false;
                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(minTx - 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.RightWall:
                    if (maxTx == this.columns - 1) return false;
                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(maxTx + 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.OnBackTiles:
                    var back = this.containerChunk.backTileGrid1;
                    for (int tx = minTx; tx <= maxTx; tx += 1)
                    {
                        for (int ty = minTy; ty <= maxTy; ty += 1)
                        {
                            if (back.GetTile(tx, ty) == null)
                                return false;
                        }
                    }
                    break;

                case TileTheme.DecorationStyle.LeftWallThickOnly:
                    if (minTx == 0) return false;
                    if (minTx - 2 < 0) return false;

                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(minTx - 1, ty);

                        if (t == null) return false;
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;

                        t = GetTile(minTx - 2, ty);

                        if (t == null) return false;
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;

                case TileTheme.DecorationStyle.RightWallThickOnly:
                    if (maxTx == this.columns - 1) return false;
                    if (maxTx + 2 > this.columns - 1) return false;

                    for (int ty = minTy; ty <= maxTy; ty += 1)
                    {
                        var t = GetTile(maxTx + 1, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;

                        t = GetTile(maxTx + 2, ty);
                        if (t?.spec.shape != Tile.Shape.Square)
                            return false;
                        if (t?.secondary == true)
                            return false;
                    }
                    break;
            }

            for (int tx = minTx; tx <= maxTx; tx += 1)
            {
                for (int ty = minTy; ty <= maxTy; ty += 1)
                {
                    if (permitted[tx, ty] == false)
                        return false;
                }
            }
            return true;
        }

        List<XY> WindySkiesFindCandidates(TileTheme.DecorationDefinition decoration)
        {
            var result = new List<XY>();

            for (int x = 0; x < this.columns; x += 1)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    if (WindySkiesIsCandidate(x, y, decoration, permitted))
                        result.Add(new XY(x, y));
                }
            }
            return result;
        }

        List<(XY, XY)> WindySkiesFindCandidateRanges(TileTheme.DecorationDefinition decoration)
        {
            var result = new List<(XY, XY)>();

            for (int y = 0; y < this.rows; y += 1)
            {
                XY? startOfRange = null;
                for (int x = 0; x < this.columns; x += 1)
                {
                    bool isCandidate = WindySkiesIsCandidate(x, y, decoration, permitted);
                    if (isCandidate == true && startOfRange == null)
                    {
                        startOfRange = new XY(x, y);
                    }
                    else if (isCandidate == false && startOfRange != null)
                    {
                        result.Add((startOfRange.Value, new XY(x - 1, y)));
                        startOfRange = null;
                    }
                }
                if (startOfRange != null)
                {
                    result.Add((startOfRange.Value, new XY(this.columns - 1, y)));
                    startOfRange = null;
                }
            }
            return result;
        }

        void WindySkiesCreateTypes(TileTheme.DecorationDefinition[] decorationStyles, int count)
        {
            if (decorationStyles.Length == 0)
                return;

            for (int n = 0; n < count; n += 1)
            {
                var decoration = Util.RandomChoice(decorationStyles);
                var candidates = WindySkiesFindCandidates(decoration);
                if (candidates.Count == 0)
                    continue;
                var xy = Util.RandomChoice(candidates);
                CreateDecoration(tileTheme, xy, 0, decoration, permitted);
            }
        }

        void WindySkiesCreateTypesEvenly(
            TileTheme.DecorationDefinition[] decorationStyles, int count
        )
        {
            if (decorationStyles.Length == 0)
                return;

            for (int n = 0; n < count; n += 1)
            {
                var decoration = Util.RandomChoice(decorationStyles);
                var candidates = WindySkiesFindCandidateRanges(decoration);
                if (candidates.Count == 0)
                    continue;
                var (left, right) = Util.RandomChoice(candidates);
                if (right.x - left.x > 2 && Rnd.Range(0.0f, 1.0f) > 0.3f)
                {
                    CreateDecoration(tileTheme, left, 0, decoration, permitted);
                    CreateDecoration(tileTheme, right, 0, decoration, permitted);
                }
                else
                {
                    float x = left.x + ((right.x - left.x) / 2.0f);
                    var xy = new XY(Mathf.FloorToInt(x), left.y);
                    CreateDecoration(tileTheme, xy, x - xy.x, decoration, permitted);
                }
                for (int x = left.x; x <= right.x; x += 1)
                {
                    permitted[x, left.y] = false;
                }
            }
        }

        List<XY> fans = new List<XY>();
        for (int y = 0; y < this.rows; y += 1)
        {
            for (int x = 0; x < this.columns; x += 1)
            {
                var tile = this.tiles[x + (y * this.columns)];
                if (tile?.addInteriorFromTileset != tileTheme.normalTiles)
                    continue;
                var interior = tile.addInteriorFromTileset.InteriorTile(tile.mapTx, tile.mapTy);
                var tileName = interior.name;

                if (tileName == "interior_18" || tileName == "interior_47")
                {
                    var gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationBigFan);
                    gameObject.name = $"big fan ({x + this.xMin}, {y + this.yMin})";
                    gameObject.transform.position = new Vector2(tile.mapTx + 0.5f, tile.mapTy + 0.5f);
                    gameObject.transform.parent =
                        this.tileGridMesh?.transform ?? tile.interior.gameObject.transform;
                    fans.Add(new XY(x, y));
                }
                if (tileName == "interior_28")
                {
                    var gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationSmallFan);
                    gameObject.name = $"small fan ({x + this.xMin}, {y + this.yMin})";
                    gameObject.transform.position = new Vector2(tile.mapTx + 0.3f, tile.mapTy + 0.71f);
                    gameObject.transform.parent =
                        this.tileGridMesh?.transform ?? tile.interior.gameObject.transform;
                    fans.Add(new XY(x, y));
                }
            }
        }

        List<int> GetAvailableHeightsBelow(int startX, int startY)
        {
            List<int> result = new List<int>();
            int height = 0;
            while (true)
            {
                for (int j = -1; j < 2; j++)
                {
                    var tempTile = GetTile(startX + j, startY - height);
                    if (tempTile == null || tempTile?.spec.shape != Tile.Shape.Square)
                    {

                        if (result.Count == 0)
                        {
                            result.Add(2);
                        }
                        else if (result.Count > 1)
                        {
                            // get rid of last element as it is probably on top of bottom edge
                            result.RemoveAt(result.Count - 1);
                        }
                        return result;
                    }
                }

                var tile = GetTile(startX, startY - height);
                var tileName = tile?.interior?.gameObject?.GetComponent<SpriteRenderer>()?.sprite.name;

                if (height >= 2 && tileName != "interior_18" && tileName != "interior_47" && tileName != "interior_28")
                {
                    result.Add(height);
                }

                height++;
            }
        }

        List<(XY, XY)> WindySkiesFindCandidateRangesWithMinimum(TileTheme.DecorationDefinition decoration, int minimum)
        {
            var result = new List<(XY, XY)>();

            for (int y = 0; y < this.rows; y += 1)
            {
                XY? startOfRange = null;
                for (int x = 0; x < this.columns; x += 1)
                {
                    bool isCandidate = WindySkiesIsCandidate(x, y, decoration, permitted);
                    if (isCandidate == true && startOfRange == null)
                    {
                        startOfRange = new XY(x, y);
                    }
                    else if (isCandidate == false && startOfRange != null)
                    {
                        if (x - 1 - startOfRange.Value.x >= minimum)
                            result.Add((startOfRange.Value, new XY(x - 1, y)));
                        startOfRange = null;
                    }
                }
                if (startOfRange != null)
                {
                    if (this.columns - 1 - startOfRange.Value.x >= minimum)
                        result.Add((startOfRange.Value, new XY(this.columns - 1, y)));
                    startOfRange = null;
                }
            }
            return result;
        }

        void CreateWindySkiesRailingsAndLadders(
            TileTheme tileTheme, TileTheme.DecorationDefinition[] decorationStyles, int count, List<XY> fans, bool[,] permitted
        )
        {
            if (decorationStyles.Length == 0)
                return;

            var tempDecoration = new TileTheme.DecorationDefinition();
            tempDecoration.reachAbove = 0;
            tempDecoration.reachLeft = tempDecoration.reachRight = tempDecoration.reachBelow = 0;
            tempDecoration.style = TileTheme.DecorationStyle.FloorThickOnly;

            //find series of horizontal, single tile candidates
            var candidates = WindySkiesFindCandidateRangesWithMinimum(tempDecoration, 2);

            List<(XY, XY)> rails = new List<(XY, XY)>();
            List<(XY, XY)> windows = new List<(XY, XY)>();
            List<XY> ladders = new List<XY>();

            // find columns where ladders will be placed
            for (int i = 0; i < candidates.Count; i++)
            {
                var (left, right) = candidates[i];
                if (right.x - left.x + 1 >= 5)
                {
                    int ladderX = Mathf.FloorToInt(Rnd.Range(left.x + 1, right.x - 1));

                    if (IsTileSquare(ladderX, left.y - 3) == true && IsTileSquare(ladderX - 1, left.y - 3) == true && IsTileSquare(ladderX + 1, left.y - 3) == true)
                    {
                        rails.Add((new XY(left.x, left.y), new XY(ladderX - 1, left.y)));
                        ladders.Add(new XY(ladderX, left.y));
                        rails.Add((new XY(ladderX + 1, right.y), new XY(right.x, right.y)));

                        // windows
                        if (IsTileSquare(left.x - 1, left.y - 2) == false)
                        {
                            if (ladderX - left.x >= 3)
                            {
                                windows.Add((new XY(left.x + 2, left.y), new XY(ladderX - 1, left.y)));
                            }
                        }
                        else
                        {
                            if (ladderX - left.x >= 1)
                            {
                                windows.Add((new XY(left.x + 0, left.y), new XY(ladderX - 1, left.y)));
                            }
                        }

                        if (IsTileSquare(right.x + 1, left.y - 2) == false)
                        {
                            if (right.x - ladderX >= 3)
                                windows.Add((new XY(ladderX + 1, right.y), new XY(right.x - 2, right.y)));
                        }
                        else
                        {
                            if (right.x - ladderX >= 1)
                                windows.Add((new XY(ladderX + 1, right.y), new XY(right.x - 0, right.y)));
                        }
                    }
                    else
                    {
                        rails.Add((new XY(left.x, left.y), new XY(right.x, left.y)));
                    }
                }
                else
                {
                    rails.Add(candidates[i]);
                }
            }

            //generate ladders
            for (int i = 0; i < ladders.Count; i++)
            {
                var availableHeights = GetAvailableHeightsBelow(ladders[i].x, ladders[i].y - 1);
                int ladderHeight = 1 + availableHeights[Rnd.Range(0, availableHeights.Count)];

                for (int k = 0; k <= ladderHeight; k++)
                {
                    GameObject gameObject;

                    if (k == 0) continue;

                    if (k == 1)
                    {
                        gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationLadderTop);
                    }
                    else if (k == ladderHeight)
                    {
                        gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationLadderBottom);
                    }
                    else
                    {
                        gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationLadderMiddle[Rnd.Range(0, Assets.instance.windySkies.tileDecorationLadderMiddle.Length)]);
                    }
                    int mapTx = ladders[i].x + this.xMin;
                    int mapTy = ladders[i].y + this.yMin - k;

                    gameObject.name = $"tile ({mapTx}, {mapTy}) decoration";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(mapTx + 0.5f, mapTy);
                }
            }

            // generate railings
            for (int i = 0; i < rails.Count; i++)
            {
                var (left, right) = rails[i];

                var delta = right.x - left.x + 1;
                var xy = left;
                int safety = 100;
                while (delta > 0)
                {
                    var randomDecoration = Util.RandomChoice(decorationStyles);
                    if (randomDecoration.reachRight + 1 <= delta)
                    {
                        CreateDecoration(tileTheme, xy, 0, randomDecoration, permitted);
                        xy.x += randomDecoration.reachRight + 1;
                        delta -= randomDecoration.reachRight + 1;
                    }
                    else
                    {
                        if (--safety < 0)
                            break;
                    }
                }
            }

            // generate windows
            for (int i = 0; i < windows.Count; i++)
            {
                var (left, right) = windows[i];
                bool windowsGroupedByTwo = ((right.x - left.x + 1 + 1) % 3 == 0);
                int placeWindow = 2;
                if (Rnd.Range(0, 100) > 50)
                    windowsGroupedByTwo = false;

                for (int k = left.x; k <= right.x; k++)
                {
                    if(k <= 1 || k >= this.columns - 2)
                        continue;

                    if (IsTileSquare(k, left.y - 3) == true && IsTileSquare(k - 1, left.y - 3) == true && IsTileSquare(k + 1, left.y - 3) == true)
                    {
                        if (windowsGroupedByTwo == false ||
                            (windowsGroupedByTwo == true && placeWindow > 0))
                        {
                            var gameObject = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationWindows[Rnd.Range(0, Assets.instance.windySkies.tileDecorationWindows.Length)]);
                            int mapTx = k + this.xMin;
                            int mapTy = left.y + this.yMin - 2;
                            gameObject.name = $"tile ({mapTx}, {mapTy}) decoration";
                            gameObject.transform.parent = this.containerChunk.transform;
                            gameObject.transform.position = new Vector2(mapTx + 0.5f, mapTy);
                        }
                    }
                    if (--placeWindow < 0)
                        placeWindow = 2;
                }
            }
        }

        // small balloons
        var tempPermitted = (bool[,])permitted.Clone();

        WindySkiesCreateTypes(
        tileTheme.decorationSprites.Where(d =>
            d.style == TileTheme.DecorationStyle.Floor
        ).ToArray(),
        Mathf.RoundToInt(Rnd.Range(0.0f, 2.0f) * this.rows / 12)
        );

        permitted = tempPermitted;

        WindySkiesCreateTypes(
        tileTheme.decorationSprites.Where(d =>
            d.style == TileTheme.DecorationStyle.LeftWallThickOnly
        ).ToArray(),
        10);

        WindySkiesCreateTypes(
        tileTheme.decorationSprites.Where(d =>
            d.style == TileTheme.DecorationStyle.RightWallThickOnly
        ).ToArray(),
        10);

        WindySkiesCreateTypesEvenly(
        tileTheme.decorationSprites.Where(d =>
            d.style == TileTheme.DecorationStyle.CeilingThickOnly
        ).ToArray(),
        20
        );

        CreateWindySkiesRailingsAndLadders(
        tileTheme,
        tileTheme.decorationSprites.Where(d =>
            d.style == TileTheme.DecorationStyle.FloorThickOnly
        ).ToArray(),
        20,
        fans,
        permitted
        );

        AddWindySkiesPillars(tileTheme, permitted);
        AddWindySkiesCrossJoiners();
        AddWindySkiesSideDecorations();
        return;
    }

    void CreateDecoration(
        TileTheme tileTheme, XY xy, float xOffset, TileTheme.DecorationDefinition decoration, bool[,] permitted
    )
    {
        int mapTx = xy.x + this.xMin;
        int mapTy = xy.y + this.yMin;

        Vector2 offset;
        if (decoration.style == TileTheme.DecorationStyle.Floor || decoration.style == TileTheme.DecorationStyle.FloorThickOnly)
            offset = new Vector2(0.5f, 0);
        else if (decoration.style == TileTheme.DecorationStyle.Ceiling || decoration.style == TileTheme.DecorationStyle.CeilingThickOnly)
            offset = new Vector2(0.5f, 1);
        else if (decoration.style == TileTheme.DecorationStyle.LeftWall || decoration.style == TileTheme.DecorationStyle.LeftWallThickOnly)
            offset = new Vector2(0, 0.5f);
        else if (decoration.style == TileTheme.DecorationStyle.RightWall || decoration.style == TileTheme.DecorationStyle.RightWallThickOnly)
            offset = new Vector2(1, 0.5f);
        else
            offset = new Vector2(0.5f, 0.5f);

        if (decoration.prefab != null)
        {
            var gameObject = GameObject.Instantiate(decoration.prefab);
            gameObject.name = $"tile ({mapTx}, {mapTy}) decoration";
            gameObject.transform.parent = this.containerChunk.transform;
            gameObject.transform.position =
                new Vector2(mapTx + offset.x + xOffset, mapTy + offset.y);

            if (decoration.flipSpriteHorizontally == true)
                gameObject.transform.localScale = new Vector3(-1, 1, 1);
        }
        else // use sprite
        {
            var gameObject = new GameObject();
            gameObject.name = $"tile ({mapTx}, {mapTy}) decoration - " + decoration.name;
            gameObject.transform.parent = this.containerChunk.transform;
            gameObject.transform.position =
                new Vector2(mapTx + offset.x + xOffset, mapTy + offset.y);

            if (decoration.flipSpriteHorizontally == true)
                gameObject.transform.localScale = new Vector3(-1, 1, 1);

            var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            spriteRenderer.sprite = decoration.sprite;
            spriteRenderer.sortingLayerName = "Tiles - Surface Decoration";
            spriteRenderer.sortingOrder = decoration.sortingOrder;
            if (decoration.inFrontOfFoliage == true)
            {
                spriteRenderer.sortingOrder = 11;
            }
            if (tileTheme == Assets.instance.sunkenIsland?.tileTheme &&
                containerChunk.boundaryMarkers.bottomLeft.x != 0 &&
                decoration.style == TileTheme.DecorationStyle.OnBackTiles
                && containerChunk.isHorizontal != true)
            {
                gameObject.transform.position = new Vector2(mapTx + offset.x + xOffset - containerChunk.boundaryMarkers.bottomLeft.x, mapTy + offset.y);
            }
        }

        int dx, dy;
        for (dx = -decoration.reachLeft; dx <= decoration.reachRight; dx += 1)
        {
            for (dy = -decoration.reachBelow; dy <= decoration.reachAbove; dy += 1)
            {
                permitted[xy.x + dx, xy.y + dy] = false;
            }
        }
    }

    bool IsTileSquare(int gridTx, int gridTy)
    {
        var t = GetTile(gridTx, gridTy);
        return t?.spec.shape == Tile.Shape.Square;
    }    

    // void DebugCandidates(List<(XY, XY)> candidates)
    // {
    //     foreach(var candidate in candidates)
    //     {
    //         Debug.Log($"({candidate.Item1.x},{this.yMin + candidate.Item1.y})=>({candidate.Item2.x},{this.yMin + candidate.Item2.y})");
    //     }
    // }

    public void AddWindySkiesSideDecorations()
    {

        int HowManyTilesUpAreOnOutside(XY start)
        {
            int height = 0;

            while(start.y < this.rows && GetTile(start.x, start.y) != null 
                && GetTile(start.x, start.y).setType == Tile.SetType.Outside
                && this.containerChunk.ItemAt(
                    this.containerChunk.xMin + start.x,
                    this.containerChunk.yMin + start.y
                ) == null
                )
            {
                height++;
                start.y++;
            }
            return height - 1;
        }

        List<(XY, XY)> GetCandidates(XY start)
        {
            List<(XY, XY)> candidates = new List<(XY, XY)>();
            XY currentTile = start;

            do
            {
                int height = HowManyTilesUpAreOnOutside(currentTile);

                if (height > 6)
                {
                    candidates.Add((currentTile, new XY(currentTile.x, currentTile.y + height)));
                    currentTile.y += height;                    
                }
                else
                {
                    currentTile.y++;
                    if(currentTile.y >= this.rows)
                        break;
                }
            } while (true);

            return candidates;
        }

        void CreateLargeCannon(List<(XY bottom, XY top)> candidates, bool flip = false)
        {
            if(candidates.Count == 0)
                return;

            int chosenIndex = Rnd.Range(0, candidates.Count);
            var (bottom, top) = candidates[chosenIndex];
            int column = (flip == true ? this.columns - 1 : 0);

            int height = 5;
            int startY = Rnd.Range(bottom.y, top.y - height);
            int mapTx = column + this.xMin + (flip == true? 1 : 0);
            int mapTy = this.yMin + startY;
            
            GameObject gun = null; 
            gun = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationLargeSideCannon);
            gun.name = "Large Side Gun (" + column + "," + startY +")";
            gun.transform.parent = this.containerChunk.transform;
            gun.transform.localScale = new Vector3(flip == true ? -1 : 1, 1, 1);
            gun.transform.position = new Vector2(mapTx, mapTy);
            candidates.RemoveAt(chosenIndex);

            if(startY - height - bottom.y >= 6)
            {
                candidates.Add((new XY(column, bottom.y), new XY(column, startY - 1 - height)));
            }

            if(top.y - (startY + height) >= 6)
            {
                candidates.Add((new XY(column, startY + height), new XY(column, top.y)));
            }
        }

        void CreateSmallCannon(List<(XY bottom, XY top)> candidates, bool flip = false)
        {
            if(candidates.Count == 0)
                return;
                
            int column = (flip == true ? this.columns - 1: 0);
            int chosenIndex = Rnd.Range(0, candidates.Count);
            var (bottom, top) = candidates[chosenIndex];

            int height = 5;
            int startY = Rnd.Range(bottom.y, top.y - height);
            int mapTx = column + this.xMin + (flip == true? 1 : 0);
            int mapTy = this.yMin + startY;
            
            GameObject gun = null; 
            gun = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationSmallSideCannon);
            gun.name = "Small Side Gun (" + column + "," + startY +")";
            gun.transform.parent = this.containerChunk.transform;
            gun.transform.position = new Vector2(mapTx, mapTy);
            gun.transform.localScale = new Vector3(flip == true ? -1 : 1, 1, 1);
            candidates.RemoveAt(chosenIndex);

            if(startY - height - bottom.y >= 6)
            {
                candidates.Add((new XY(column, bottom.y), new XY(column, startY - 1 - height)));
            }

            if(top.y - (startY + height) >= 6)
            {
                candidates.Add((new XY(column, startY + height), new XY(column, top.y)));
            }
        }

        void CreateMetalTilesPatch(List<(XY bottom, XY top)> candidates)
        {
            if(candidates.Count == 0)
                return;

            int column = candidates[0].bottom.x;
            int chosenIndex = Rnd.Range(0, candidates.Count);
            var (bottom, top) = candidates[chosenIndex];

            int height = Rnd.Range(5,11);
            int startY = Rnd.Range(bottom.y, top.y - height);
            int mapTx = column + this.xMin;
            int mapTy = this.yMin + startY;

            for (int i = 0; i < height; i++)
            {
                GameObject tile = null;

                if(i == 0)
                {
                    tile = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationMetalTileBottom);
                }
                else if(i == height - 1)
                {
                    tile = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationMetalTileTop);
                }
                else
                {
                    tile = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationMetalTileMiddle[Rnd.Range(0, Assets.instance.windySkies.tileDecorationMetalTileMiddle.Length)]);
                }
                
                tile.name = "Metal Patch (" + column + "," + startY +")";
                tile.transform.parent = this.containerChunk.transform;
                tile.transform.position = new Vector2(mapTx, mapTy + i);
            }            
             
            candidates.RemoveAt(chosenIndex);
            if(startY - height - bottom.y >= 6)
            {
                candidates.Add((new XY(column, bottom.y), new XY(column, startY - 1 - height)));
            }

            if(top.y - (startY + height) >= 6)
            {
                candidates.Add((new XY(column, startY + height), new XY(column, top.y)));
            }
        }

        if  (this.rows < 6
            || this.containerChunk.isHorizontal == true
            || this.containerChunk.isTransitionalTunnel == true
            || this.containerChunk.entry.type == Chunk.MarkerType.Wall
            || this.containerChunk.exit.type == Chunk.MarkerType.Wall
            )
            return;


        var candidatesLeft = GetCandidates(new XY(0, 0));
        var candidatesRight = GetCandidates(new XY(this.columns - 1, 0));

        for (int i = 0; i < Mathf.FloorToInt(this.rows / 15); i++)
        {
            if (Rnd.Range(0, 100) > 50)
            {
                if (Rnd.Range(0, 100) > 60)
                    CreateLargeCannon(candidatesLeft);
                else
                    CreateSmallCannon(candidatesLeft);
            }
        }

        for (int i = 0; i < Mathf.FloorToInt(this.rows / 15); i++)
        {
            if (Rnd.Range(0, 100) > 50)
            {
                if (Rnd.Range(0, 100) > 60)
                    CreateLargeCannon(candidatesRight, true);
                else
                    CreateSmallCannon(candidatesRight, true);
            }
        }

        var longerThanTenLeft = candidatesLeft.Where<(XY bottom, XY top)>(x => x.top.y - x.bottom.y > 10).ToList();
        int howMany = longerThanTenLeft.Count;

        for (int i = 0; i < howMany; i++)
        {
            if (Rnd.Range(0, 100) > 30)
                CreateMetalTilesPatch(longerThanTenLeft);
        }

        var longerThanTenRight = candidatesRight.Where<(XY bottom, XY top)>(x => x.top.y - x.bottom.y > 10).ToList();
        int howManyRight = longerThanTenRight.Count;

        for (int i = 0; i < howManyRight; i++)
        {
            if (Rnd.Range(0, 100) > 30)
                CreateMetalTilesPatch(longerThanTenRight);
        }
    }

    public void AddWindySkiesCrossJoiners()
    {
        void CreateJoiner(List<(XY bottom, XY top)> candidates)
        {
            if(candidates.Count == 0)
                return;

            int chosenIndex = Rnd.Range(0, candidates.Count);
            var (bottom, top) = candidates[chosenIndex];

            bool useSmall = (top.y - bottom.y < 16);
            int joinerHeight = (useSmall == true ? 6 : 16);
            int startY = Rnd.Range(bottom.y, top.y);
            int mapTx = 1 + this.xMin;
            int mapTy = this.yMin + startY;
            GameObject joiner = null;
            if (useSmall == true)
            {
                joiner = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationSmallBackJoiner);
            }
            else
            {
                joiner = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationLargeBackJoiner);
            }
            joiner.name = "Joiner (" + 1 + "," + startY +")";
            joiner.transform.parent = this.containerChunk.transform;
            joiner.transform.position = new Vector2(mapTx, mapTy);
            candidates.RemoveAt(chosenIndex);

            if(startY - joinerHeight - bottom.y >= 6)
            {
                candidates.Add((new XY(0, bottom.y), new XY(0, startY - 1 - joinerHeight)));
            }

            if(top.y - (startY + joinerHeight) >= 6)
            {
                candidates.Add((new XY(0, startY + joinerHeight), new XY(0, top.y)));
            }
        }

        // do checks here
        if  (this.rows < 16
            || this.containerChunk.isHorizontal == true
            || this.containerChunk.isTransitionalTunnel == true
            || this.containerChunk.entry.type == Chunk.MarkerType.Wall
            || this.containerChunk.exit.type == Chunk.MarkerType.Wall
            )
            return;

        var candidates = new List<(XY bottom, XY top)>();
        candidates.Add((new XY(0, 0), new XY(0, this.rows - 16)));

        int howMany = Mathf.FloorToInt(this.rows / 20);        
        for (int i = 0; i < howMany; i++)
        {
             CreateJoiner(candidates);
        }
    }

    public void AddWindySkiesPillars(TileTheme tileTheme, bool[,] permitted)
    {
        (bool, int topGridTy) IsCandidateBaseOfPillar(int gridTx, int gridTy)
        {
            if(gridTx < 1 || gridTx >= this.columns - 1 || gridTy <= 1 || gridTy >= this.rows - 1)
                return (false, 0);

            var k = GetTile(gridTx, gridTy - 1);

            if (GetTile(gridTx, gridTy - 1)?.spec.shape != Tile.Shape.Square
                || GetTile(gridTx - 1, gridTy - 1)?.spec.shape != Tile.Shape.Square
                || GetTile(gridTx + 1, gridTy - 1)?.spec.shape != Tile.Shape.Square            
                )
                    return (false, 0);

            for (int rowTy = gridTy; rowTy < this.rows; rowTy += 1)
                {
                    bool[] squareTiles = { true, true, true };
                    bool[] emptyTiles = { false, false, false };
                   
                    {
                        var t = GetTile(gridTx - 1, rowTy);
                        if (t == null || t.spec.shape != Tile.Shape.Square)
                        {
                            squareTiles[0] = false;
                            if (t == null)
                                emptyTiles[0] = true;
                        }
                    }

                    {
                        var t = GetTile(gridTx, rowTy);
                        if (t == null || t.spec.shape != Tile.Shape.Square)
                        {
                            squareTiles[1] = false;
                            if (t == null)
                                emptyTiles[1] = true;
                        }
                    }

                    {
                        var t = GetTile(gridTx + 1, rowTy);
                        if (t == null || t.spec.shape != Tile.Shape.Square)
                        {
                            squareTiles[2] = false;
                            if (t == null)
                                emptyTiles[2] = true;
                        }
                    }
 
                    if(squareTiles[0] != false || squareTiles[2] != false)
                    {
                        return (false, 0);
                    }

                    if(squareTiles[1] == false)
                    {
                        if(emptyTiles[0] == true && emptyTiles[1] == true && emptyTiles[2] == true)
                        {
                            int height = rowTy - gridTy;
                            if (height > 3)
                                return (true, rowTy - 1);
                            else
                                return (false, 0);
                        }
                        else
                        {
                            return (false, 0);
                        }
                    }
                }

            return (false, 0); // no top to connect to
        }

        List<(XY bottom, XY top)> BaseOfPillarFindCandidates()
        {
            var result = new List<(XY bottom, XY top)>();

            for (int x = 0; x < this.columns; x += 1)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    var (valid, topTy) = IsCandidateBaseOfPillar(x, y);
                    if (valid == true)
                        result.Add((new XY(x, y), new XY(x, topTy)));
                }
            }

            return result;
        }
         
        var candidates = BaseOfPillarFindCandidates();
        foreach (var candidate in candidates)
        {
            var (bottom, top) = candidate;

            for (int y = bottom.y; y <= top.y + 1; y += 1)
            {
                int mapTx = bottom.x + this.xMin;
                int mapTy = y + this.yMin;

                Sprite sprite = null;
                GameObject prefab = null;
                Tile t = GetTile(bottom.x, y);
                Vector3 localOffset = Vector3.zero;
                if (y == top.y + 1)
                {
                    prefab = GameObject.Instantiate(Assets.instance.windySkies?.tileDecorationPillarFan);
                    t = GetTile(bottom.x, top.y);
                    localOffset = new Vector3(0, 1, 0);
                    sprite = null;
                }
                else if (y == top.y)
                {
                    
                }
                else if (y == top.y - 1)
                {
                     
                }
                else if (y == bottom.y)
                {
                    sprite = Assets.instance.windySkies?.tileDecorationPillarBase;
                }
                else
                    sprite = Util.RandomChoice(tileTheme.pillar.middle);

                if (sprite != null)
                {
                    var gameObject = new GameObject();
                    gameObject.name = $"tile ({mapTx}, {mapTy}) pillar sprite";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(mapTx, mapTy);

                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = sprite;
                    spriteRenderer.sortingLayerName = "Tiles - Decorative Sprites";
                    spriteRenderer.sortingOrder = 5;

                    if (t != null)
                    {
                        gameObject.transform.parent = t.spriteRenderer.gameObject.transform;
                        gameObject.transform.localPosition = localOffset;
                    }
                }
                else if(prefab != null)
                {
                    prefab.name = $"tile ({mapTx}, {mapTy}) pillar fan";
                    prefab.transform.parent = t.spriteRenderer.gameObject.transform;
                    prefab.transform.localPosition = localOffset;
                }

                for (int dx = -1; dx <= 1; dx += 1)
                {
                    permitted[bottom.x + dx, y] = false;
                }
            }
        }
    }

    public void AddLargeFoliage()
    {
        bool IsCandidate(XY xy)
        {
            for (int dy = 0; dy >= -2; dy -= 1)
            {
                for (int dx = -2; dx <= 1; dx += 1)
                {
                    var t = GetTileForAutotile(xy.x + dx, xy.y + dy);
                    if (t.tile == null) return false;
                    if (t.tile.spec.shape != Tile.Shape.Square) return false;
                }
            }
            for (int dy = 1; dy < 8; dy += 1)
            {
                for (int dx = -2; dx <= 1; dx += 1)
                {
                    var t = GetTileForAutotile(xy.x + dx, xy.y + dy);
                    if (t.tile != null) return false;

                    var i = this.containerChunk.ItemAt(
                        this.xMin + xy.x + dx,
                        this.yMin + xy.y + dy
                    );
                    if (i != null && Chunk.DoesItemBlockDecoration(i) == true)
                        return false;
                }
            }
            return true;
        }

        var candidates = new List<XY>();
        for (int y = 0; y < this.rows; y += 1)
        {
            for (int x = 0; x < this.columns; x += 1)
            {
                var xy = new XY(x, y);
                if (IsCandidate(xy))
                    candidates.Add(xy);
            }
        }

        if (candidates.Count == 0)
            return;

        var choice = Util.RandomChoice(candidates);
        var prefab = Util.RandomChoice(
            Assets.instance.templeBigPlant1,
            Assets.instance.templeBigPlant2
        );
        var gameObject = GameObject.Instantiate(prefab);
        gameObject.name = $"big decoration ({choice.x}, {choice.y})";
        gameObject.transform.position = new Vector3(
            this.xMin + choice.x,
            this.yMin + choice.y + 1
        );
        gameObject.transform.parent = this.containerChunk.transform;
    }

    public void AddSpaceInteriorBackWall(TileTheme tileTheme)
    {
        if (this.containerChunk.gravityDirection == null)
            return;

        var filled = new bool[this.columns, this.rows];

        bool ShouldThereBeEdgeWithPreviousChunk()
        {
            return (this.containerChunk.index - 1 >= 0 
                    && this.map.chunks[this.containerChunk.index - 1].gravityDirection == null);
        }

        bool ShouldThereBeEdgeWithNextChunk()
        {
            return (this.containerChunk.index + 1 < this.map.chunks.Count 
                    && this.map.chunks[this.containerChunk.index + 1].gravityDirection == null);
        }

        void FillColumn(int gridX)
        {
            //it fills whole vertical column with predefined rows
            Sprite Row14() => Util.RandomChoice(tileTheme.spaceInteriorBgRows14);
            Sprite Row3()  => Util.RandomChoice(tileTheme.spaceInteriorBgRows3);
            Sprite Row2()  => Util.RandomChoice(tileTheme.spaceInteriorBgRows2);
            Sprite Row1()  => Util.RandomChoice(tileTheme.spaceInteriorBgRows1);

            int height = 1 + this.containerChunk.yMax - this.containerChunk.yMin;

            if(ShouldThereBeEdgeWithPreviousChunk()) height--;
            if(ShouldThereBeEdgeWithNextChunk()) height--;

            var backWallPieces = new List<(Sprite sprite, int size)>();
            while (height >= 14) { backWallPieces.Add((Row14(), 14)); height -= 14; }
            while (height >= 5)  { backWallPieces.Add((Row3(),  3 )); height -= 3; }
            while (height >= 3)  { backWallPieces.Add((Row2(),  2 )); height -= 2; }
            while (height >= 1)  { backWallPieces.Add((Row1(),  1 )); height -= 1; }
            Util.Shuffle(new System.Random(), backWallPieces);

            int y = ShouldThereBeEdgeWithPreviousChunk() ? 1 : 0;
            
            foreach (var piece in backWallPieces)
            {
                var gameObject = new GameObject();
                gameObject.name = $"back wall piece y={y}";
                gameObject.transform.parent = this.containerChunk.transform;
                gameObject.transform.position = new Vector2(
                    this.containerChunk.xMin + gridX,
                    this.containerChunk.yMin + y
                );
                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = piece.sprite;
                spriteRenderer.sortingLayerName = "Tiles - Back 1";
                spriteRenderer.sortingOrder = -150;
                for (int dx = 0; dx < 14; dx += 1)
                {
                    for (int dy = 0; dy < piece.size; dy += 1)
                    {
                        filled[gridX + dx, y + dy] = true;
                    }
                }

                y += piece.size;
            }
        }

        void FillRow(int gridY)
        {
            Sprite Column14() => Util.RandomChoice(tileTheme.spaceInteriorBgRows14);
            Sprite Column3()  => tileTheme.spaceInteriorBgColumn3;
            Sprite Column2()  => tileTheme.spaceInteriorBgColumn2;
            Sprite Column1()  => tileTheme.spaceInteriorBgColumn1;

            int width = 1 + this.containerChunk.xMax - this.containerChunk.xMin;
            var backWallPieces = new List<(Sprite sprite, int size)>();
            while (width >= 14) { backWallPieces.Add((Column14(), 14)); width -= 14; }
            while (width >= 5)  { backWallPieces.Add((Column3(),  3 )); width -= 3; }
            while (width >= 3)  { backWallPieces.Add((Column2(),  2 )); width -= 2; }
            while (width >= 1)  { backWallPieces.Add((Column1(),  1 )); width -= 1; }
            Util.Shuffle(new System.Random(), backWallPieces);

            int x = 0;
            foreach (var piece in backWallPieces)
            {
                var gameObject = new GameObject();
                gameObject.name = $"back wall piece x={x}";
                gameObject.transform.parent = this.containerChunk.transform;
                gameObject.transform.position = new Vector2(
                    this.containerChunk.xMin + x,
                    this.containerChunk.yMin + gridY
                );
                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = piece.sprite;
                spriteRenderer.sortingLayerName = "Tiles - Back 1";
                spriteRenderer.sortingOrder = -150;
                for (int dx = 0; dx < piece.size; dx += 1)
                {
                    for (int dy = 0; dy < 14; dy += 1)
                    {
                        filled[x + dx, gridY + dy] = true;
                    }
                }

                x += piece.size;
            }
        }

        if (this.containerChunk.isHorizontal == false)
        {
            //vertical
            int totalColumns = (this.columns - 2) / 14;
            int firstColumnX =
                this.xMin + 1 + ((this.columns - 2 - (totalColumns * 14)) / 2);
            for (int c = 0; c < totalColumns; c += 1)
            {
                int x = firstColumnX + (c * 14);
                FillColumn(x - this.xMin);
            }

            if(ShouldThereBeEdgeWithPreviousChunk())
            {
                for (int x = 0; x < this.columns; x += 1)
                {
                    Tileset tileset = tileTheme.spaceInteriorGravityEdgingTiles;

                    var gameObject = new GameObject();
                    gameObject.name = $"back wall piece ({x}, 0)";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(
                        this.containerChunk.xMin + x,
                        this.containerChunk.yMin + 0
                    );
                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = Util.RandomChoice(tileset.bottom);
                    spriteRenderer.sortingLayerName = "Tiles - Back 1";
                    spriteRenderer.sortingOrder = -150;
                    filled[x, 0] = true;
                }
            }

            if(ShouldThereBeEdgeWithNextChunk())
            {
                for (int x = 0; x < this.columns; x += 1)
                {
                    Tileset tileset = tileTheme.spaceInteriorGravityEdgingTiles;

                    var gameObject = new GameObject();
                    gameObject.name = $"back wall piece ({x}, {this.containerChunk.yMax - 1})";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(
                        this.containerChunk.xMin + x,
                        this.containerChunk.yMax
                    );
                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = Util.RandomChoice(tileset.top);
                    spriteRenderer.sortingLayerName = "Tiles - Back 1";
                    spriteRenderer.sortingOrder = -150;
                    filled[x, this.containerChunk.yMax - this.containerChunk.yMin] = true;
                }
            }
        }
        else
        {
            //horizontal
            int totalRows = (this.rows - 2) / 14;
            int firstRowY = this.yMin + 1 + ((this.rows - 2 - (totalRows * 14)) / 2);
            for (int r = 0; r < totalRows; r += 1)
            {
                int y = firstRowY + (r * 14);
                FillRow(y - this.yMin);
            }

            bool enablePreviousEdge = ShouldThereBeEdgeWithPreviousChunk();
            bool enableNextEdge = ShouldThereBeEdgeWithNextChunk();

            if(this.containerChunk.isFlippedHorizontally)
            {
                //swap the edges, chunk is flipped!
                enablePreviousEdge = ShouldThereBeEdgeWithNextChunk();
                enableNextEdge = ShouldThereBeEdgeWithPreviousChunk();
            }

            if(enablePreviousEdge)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    Tileset tileset = tileTheme.spaceInteriorGravityEdgingTiles;

                    var gameObject = new GameObject();
                    gameObject.name = $"back wall piece (0,{y})";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(
                        this.containerChunk.xMin + 0,
                        this.containerChunk.yMin + y
                    );
                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = Util.RandomChoice(tileset.left);
                    spriteRenderer.sortingLayerName = "Tiles - Back 1";
                    spriteRenderer.sortingOrder = -150;
                    filled[0, y] = true;
                }
            }

            if(enableNextEdge)
            {
                for (int y = 0; y < this.rows; y += 1)
                {
                    Tileset tileset = tileTheme.spaceInteriorGravityEdgingTiles;

                    var gameObject = new GameObject();
                    gameObject.name = $"back wall piece ({this.containerChunk.xMax - 1}, {y})";
                    gameObject.transform.parent = this.containerChunk.transform;
                    gameObject.transform.position = new Vector2(
                        this.containerChunk.xMax,
                        this.containerChunk.yMin + y
                    );
                    var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                    spriteRenderer.sprite = Util.RandomChoice(tileset.right);
                    spriteRenderer.sortingLayerName = "Tiles - Back 1";
                    spriteRenderer.sortingOrder = -150;
                    filled[this.containerChunk.xMax - this.containerChunk.xMin, y] = true;
                }
            }
        }



        for (int y = 0; y < this.rows; y += 1)
        {
            for (int x = 0; x < this.columns; x += 1)
            {
                if (filled[x, y] == true) continue;
                
                var t = GetTile(x, y);
                if(t != null && t.IsEntirelySolidInCell(0,0)) continue;

                bool IsFilledRelative(int dx, int dy)
                {
                    if (x + dx < 0 || x + dx >= this.columns) return false;
                    if (y + dy < 0 || y + dy >= this.rows) return false;
                    // if (filled[x, y] == true) return true;
                    // if (GetTile(x, y) != null) return true;
                    return true;
                }

                bool l = IsFilledRelative(-1,  0);
                bool r = IsFilledRelative(+1,  0);
                bool u = IsFilledRelative( 0, +1);
                bool d = IsFilledRelative( 0, -1);

                Tileset tileset = tileTheme.spaceInteriorBackFallbackTiles;
                Sprite sprite;
                if (l == true && r == false && u == true && d == true)
                    sprite = Util.RandomChoice(tileset.right);
                else if (r == false && l == true && u == true && d == true)
                    sprite = Util.RandomChoice(tileset.left);
                else if (l == true && r == true && u == false && d == true)
                    sprite = Util.RandomChoice(tileset.top);
                else if (r == true && l == true && u == true && d == false)
                    sprite = Util.RandomChoice(tileset.bottom);
                else
                    sprite = tileset.InteriorTile(this.xMin + x, this.yMin + y);

                var gameObject = new GameObject();
                gameObject.name = $"back wall piece ({x}, {y})";
                gameObject.transform.parent = this.containerChunk.transform;
                gameObject.transform.position = new Vector2(
                    this.containerChunk.xMin + x,
                    this.containerChunk.yMin + y
                );
                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = sprite;
                spriteRenderer.sortingLayerName = "Tiles - Back 1";
                spriteRenderer.sortingOrder = -150;
            }
        }
    }

    public static void MoveFoliageWithPlayer(Map map)
    {
        if (Mathf.Abs(map.player.velocity.x) < 0.1f) return;

        var playerPos = map.player.position.Add(
            0, -(Player.PivotDistanceAboveFeet + 0.3f)
        );
        int mapTx = Mathf.FloorToInt(map.player.position.x);
        int mapTyMin = Mathf.FloorToInt(
            map.player.position.y - Player.PivotDistanceAboveFeet - 1.5f
        );
        int mapTyMax = Mathf.FloorToInt(
            map.player.position.y - Player.PivotDistanceAboveFeet - 0.3f
        );
        for (int mapTy = mapTyMin; mapTy <= mapTyMax; mapTy += 1)
        {
            var t = map.TileAt(mapTx, mapTy, Layer.A);
            if (t == null) continue;
            if (t.foliage == null) continue;

            foreach (var sd in t.foliage)
            {
                if (sd.definition.prefab != null) continue;
                var anim = map.player.velocity.x > 0 ?
                    sd.definition.brushRightAnimation :
                    sd.definition.brushLeftAnimation;

                if (sd.animated.currentAnimation == anim &&
                    sd.animated.frameNumber < 20)
                    continue;

                sd.animated.PlayOnce(anim, () =>
                {
                    sd.animated.PlayAndLoop(sd.definition.idleAnimation);
                });
            }
        }
    }
}
