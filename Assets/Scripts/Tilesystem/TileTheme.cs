
using UnityEngine;
using System;
using UnityEngine.Serialization;

[CreateAssetMenu(menuName = "Leap Day 2/Tile Theme")]
public class TileTheme : ScriptableObject
{
    [Header("Tile sets")]
    public Tileset normalTiles;
    public Tileset dirtTiles;
    public Tileset outsideTiles;
    public Tileset backTiles;
    public Tileset backTilesForTunnel;
    public Tileset iceTilesBase;
    public Tileset iceTilesCover;
    public Tileset checkerboardLightTiles;
    public Tileset checkerboardDarkTiles;
    public Tileset dirtTiles2;
    public Tileset conflictCanyonSand;
    public Tileset gravityGroundTiles;

    [Header("Cloud platform tiles")]
    public Sprite cloudLeft;
    public Sprite cloudRight;
    public Sprite[] cloudMiddle;

    [Header("Moving cloud platform tiles")]
    public Sprite movingCloudLeft;
    public Sprite movingCloudRight;
    public Sprite movingCloudCenter;
    public Sprite movingCloudSingle;

    [Header("Outside texture")]
    public Sprite outsideTexture;
    public int outsideTextureSize;

    [Serializable]
    public class FoliageDefinition
    {
        public string name;
        public Animated.Animation idleAnimation;
        public Animated.Animation brushLeftAnimation;
        public Animated.Animation brushRightAnimation;
        public GameObject prefab;
    }

    public enum DecorationStyle
    {
        Floor,
        LeftWall,
        RightWall,
        Ceiling,
        OnSolidTiles,
        OnBackTiles,
        OnSecondaryTiles,
        LeftWallThickOnly,
        RightWallThickOnly,
        FloorThickOnly,
        CeilingThickOnly,
    }

    public enum DecorationUsedInChunks
    {
        Any,
        HorizontalOnly,
        VerticalOnly
    }

    [Serializable]
    public class DecorationDefinition
    {
        public string name;
        public Sprite sprite;
        public GameObject prefab;
        public DecorationStyle style;
        public DecorationUsedInChunks usedInChunks;
        public bool flipSpriteHorizontally;
        public int reachLeft;
        public int reachRight;
        public int reachAbove;
        public int reachBelow;
        public bool onlyOnInterior;
        public bool inFrontOfFoliage;
        public int sortingOrder;
    }

    [System.Serializable]
    public class PillarDefinition
    {
        public Sprite top;
        public Sprite[] middle;
    }

    [Header("Surface decoration")]
    public FoliageDefinition[] foliageTallAnimated;
    public FoliageDefinition[] foliageShortAnimated;
    public DecorationDefinition[] decorationSprites;
    public DecorationDefinition[] decorationSpritesPass2;
    public PillarDefinition pillar;

    [Header("Space interior background")]
    public Sprite[] spaceInteriorBgRows1;
    public Sprite[] spaceInteriorBgRows2;
    public Sprite[] spaceInteriorBgRows3;
    public Sprite[] spaceInteriorBgRows14;
    public Sprite spaceInteriorBgColumn1;
    public Sprite spaceInteriorBgColumn2;
    public Sprite spaceInteriorBgColumn3;
    public Tileset spaceInteriorBackFallbackTiles;
    public Tileset spaceInteriorGravityEdgingTiles;

    public void OnEnable()
    {
        void Check(Tileset tileset, string name, Tileset.TilesetType type)
        {
            if (tileset != null && tileset.type != type)
            {
                Debug.LogError(
                    $"{this.name}.{name}: tileset has wrong type " +
                    $"({tileset.type} instead of {type})."
                );
            }
        }
        Check(this.normalTiles, "normalTiles", Tileset.TilesetType.Normal);
        Check(this.dirtTiles, "dirtTiles", Tileset.TilesetType.Dirt);
        Check(this.outsideTiles, "outsideTiles", Tileset.TilesetType.Outside);
        Check(this.backTiles, "backTiles", Tileset.TilesetType.Back);
        Check(this.iceTilesBase, "iceTilesBase", Tileset.TilesetType.IceBase);
        Check(this.iceTilesCover, "iceTilesCover", Tileset.TilesetType.IceCover);
        Check(this.checkerboardLightTiles, "checkerboardLightTiles", Tileset.TilesetType.CheckerboardLight);
        Check(this.checkerboardDarkTiles, "checkerboardDarkTiles", Tileset.TilesetType.CheckerboardDark);
    }
}
