
using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Tileset")]
public class Tileset : ScriptableObject
{
    public enum TilesetType
    {
        Normal,
        Dirt,
        Outside,
        Back,
        IceBase,
        IceCover,
        CheckerboardLight,
        CheckerboardDark
    };
    public enum InteriorStyle
    {
        None,
        Grid,
        Random
    };

    public TilesetType type;
    public bool flowIntoSlopes; // still needed?
    public bool flowIntoOutside = true;

    [Header("Single Square")]
    public Sprite[] singleSquare;

    [Header("Interior")]
    public Sprite[] interior;
    public InteriorStyle interiorStyle;
    public int interiorGridColumns;
    public int interiorGridRows;
    public bool interiorUnderneathEdges; // overrides the following
    public bool interiorUnderneathLeftAndRightEdges;
    public bool interiorUnderneathJoinedSlopes;
    public bool interiorUnderneathAllCurves;

    [Header("Edges")]
    public Sprite[] bottom;
    public Sprite[] left;
    public Sprite[] right;
    public Sprite[] top;
    public Sprite bottomLeft;
    public Sprite bottomRight;
    public Sprite topLeft;
    public Sprite topRight;
    public Sprite insideBottomLeft;
    public Sprite insideBottomRight;
    public Sprite insideTopLeft;
    public Sprite insideTopRight;
    public bool leftAndRightTilesInOrder;
    public bool topAndBottomTilesInOrder;

    [Header("Slopes (individual)")]
    public Sprite slope22BottomLeft;
    public Sprite slope22BottomRight;
    public Sprite slope22TopLeft;
    public Sprite slope22TopRight;
    public Sprite slope45BottomLeft;
    public Sprite slope45BottomRight;
    public Sprite slope45TopLeft;
    public Sprite slope45TopRight;

    [Header("Slopes (joined together)")]
    public Sprite slopeJoined22BottomLeft;
    public Sprite slopeJoined22BottomRight;
    public Sprite slopeJoined22TopLeft;
    public Sprite slopeJoined22TopRight;
    public Sprite slopeJoined45BottomLeft;
    public Sprite slopeJoined45BottomRight;
    public Sprite slopeJoined45TopLeft;
    public Sprite slopeJoined45TopRight;

    [Header("Slopes (joined together; left edge)")]
    public Sprite slopeJoinedLeft22BottomLeft;
    public Sprite slopeJoinedLeft22BottomRight;
    public Sprite slopeJoinedLeft22TopLeft;
    public Sprite slopeJoinedLeft22TopRight;
    public Sprite slopeJoinedLeft45BottomLeft;
    public Sprite slopeJoinedLeft45BottomRight;
    public Sprite slopeJoinedLeft45TopLeft;
    public Sprite slopeJoinedLeft45TopRight;

    [Header("Slopes (joined together; right edge)")]
    public Sprite slopeJoinedRight22BottomLeft;
    public Sprite slopeJoinedRight22BottomRight;
    public Sprite slopeJoinedRight22TopLeft;
    public Sprite slopeJoinedRight22TopRight;
    public Sprite slopeJoinedRight45BottomLeft;
    public Sprite slopeJoinedRight45BottomRight;
    public Sprite slopeJoinedRight45TopLeft;
    public Sprite slopeJoinedRight45TopRight;

    [Header("Standard curves")]
    public Sprite curve3Plus1BottomLeft;
    public Sprite curve3Plus1BottomRight;
    public Sprite curve3Plus1TopLeft;
    public Sprite curve3Plus1TopRight;
    public Sprite curve3Plus1BottomLeftWithOutside;
    public Sprite curve3Plus1BottomRightWithOutside;
    public Sprite curve3Plus1TopLeftWithOutside;
    public Sprite curve3Plus1TopRightWithOutside;
    public Sprite curve3Plus1BottomLeftJoined;
    public Sprite curve3Plus1BottomRightJoined;
    public Sprite curve3Plus1TopLeftJoined;
    public Sprite curve3Plus1TopRightJoined;

    [Header("Out-curves")]
    public Sprite curveOut3Plus1BottomLeft;
    public Sprite curveOut3Plus1BottomRight;
    public Sprite curveOut3Plus1TopLeft;
    public Sprite curveOut3Plus1TopRight;

    [Header("Single row")]
    public Sprite singleRowLeft;
    public Sprite[] singleRowMiddle;
    public Sprite singleRowRight;

    [Header("Pillar 1-wide")]
    public Sprite pillar1WideTop;
    public Sprite[] pillar1WideMiddle;
    public Sprite pillar1WideBottom;

    [Header("Pillar 2-wide")]
    public Sprite pillar2WideTop;
    public Sprite[] pillar2WideMiddle;
    public Sprite pillar2WideBottom;

    [Header("Special-case for Windy Skies")]
    public Sprite bottomLeftWithoutRoundedCorner;
    public Sprite bottomRightWithoutRoundedCorner;

    [Header("Second-row top for Molten Fortress")]
    public bool hasArtForRow2;
    public Sprite row2Left;
    public Sprite row2LeftInterior;
    public Sprite[] row2Mid;
    public Sprite row2Right;
    public Sprite row2RightInterior;

    public Sprite InteriorTile(int tx, int ty)
    {
        switch (this.interiorStyle)
        {
            case InteriorStyle.None:
                return null;
            case InteriorStyle.Grid:
                tx %= this.interiorGridColumns;
                ty %= this.interiorGridRows;
                if (tx < 0) tx += this.interiorGridColumns;
                if (ty < 0) ty += this.interiorGridRows;
                ty = this.interiorGridRows - 1 - ty;
                return this.interior[tx + (ty * this.interiorGridColumns)];
            case InteriorStyle.Random:
                return Util.RandomChoice(this.interior);
        }
        return null;
    }
}
