using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Spine.Unity;

public class TitleScreen : MonoBehaviour
{
    public Image staticImage;
    private bool goingIntoGame;

    public Animated.Animation animationController;
    public Animated.Animation animationControllerShadow;

    public Animated.Animation animationKeyboard;
    public Animated.Animation animationKeyboardShadow;

    public Animated.Animation animationRemote;
    public Animated.Animation animationRemoteShadow;

    public Animated.Animation animationTouch;
    public Animated.Animation animationTouchShadow;

    public GameObject appleTVSymbols;
    public AnimatedUI appleTVRemote;
    public AnimatedUI appleTVRemoteShadow;
    public AnimatedUI appleTVController;
    public AnimatedUI appleTVControllerShadow;

    public GameObject iOSSymbols;
    public AnimatedUI iOSTouch;
    public AnimatedUI iOSTouchShadow;
    public AnimatedUI iOSController;
    public AnimatedUI iOSControllerShadow;

    public GameObject macSymbols;
    public AnimatedUI macKeyboard;
    public AnimatedUI macKeyboardShadow;
    public AnimatedUI macController;
    public AnimatedUI macControllerShadow;
    private GameObject currentSymbols;

    public SkeletonAnimation[] spineAnimations;
    public AudioClip titleMusic;
    public AudioClip mainliftMusic;
    public AudioClip sfxLeaveTitle;
    public AudioClip[] sfxTitle;
    public SpriteRenderer blueBackground;
    public RectTransform safeArea;

    public Image[] imagesToFadeInTo100;
    public Image[] imagesToFadeInTo50;
    public TMPro.TMP_Text textCopyright;

    void Start()
    {
        Game.InitScene();
        LevelGeneration.Init();
        Transition.PrepareThemedTransition();

        if(GameCamera.IsLandscape())
        {
            Camera.main.orthographicSize = 7f;
        }
        else
        {
            Camera.main.orthographicSize = 7f * Screen.height / Screen.width;
        }
        Audio.instance.PlayMusic(this.titleMusic);

        for (int i = 0; i < spineAnimations.Length; i++)
        {
            spineAnimations[i].AnimationState.Complete += (trackEntry) =>
            {
                // Debug.Log("ANIMATION FINISHED:" + trackEntry.TrackTime);
                trackEntry.TrackTime = 263f / 30;// 30fps playback speed, should be ~8.76f;
            };
        }

        this.safeArea.anchorMin = new Vector2(
            Screen.safeArea.min.x / Screen.width,
            Screen.safeArea.min.y / Screen.height
        );
        this.safeArea.anchorMax = new Vector2(
            Screen.safeArea.max.x / Screen.width,
            Screen.safeArea.max.y / Screen.height
        );

        this.textCopyright.alpha = 0f;
        StartCoroutine("BackgroundFix");
        StartCoroutine("FadeInAndShowSymbols");
        StartCoroutine("sfxCoroutine");
    }

    private void ShowSymbols()
    {
        SetCorrectSymbols();
    }

    private void SetCorrectSymbols()
    {
#if UNITY_TVOS        
        {
            this.currentSymbols = this.appleTVSymbols;
            this.appleTVSymbols.SetActive(true);          
        }
#elif (UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN)
        {
            this.currentSymbols = this.macSymbols;
            this.macSymbols.SetActive(true);
        }
#else
        {
            this.currentSymbols = this.iOSSymbols;
            this.iOSSymbols.SetActive(true);
  
        }
#endif
    }

private void SetCorrectSymbolsAnimations()
    {
#if UNITY_TVOS        
        {

            this.appleTVController.PlayAndLoop(this.animationController);
            this.appleTVControllerShadow.PlayAndLoop(this.animationControllerShadow);
            this.appleTVRemote.PlayAndLoop(this.animationRemote);
            this.appleTVRemoteShadow.PlayAndLoop(this.animationRemoteShadow);          
        }
#elif (UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN)
        {

            this.macController.PlayAndLoop(this.animationController);
            this.macControllerShadow.PlayAndLoop(this.animationControllerShadow);
            this.macKeyboard.PlayAndLoop(this.animationKeyboard);
            this.macKeyboardShadow.PlayAndLoop(this.animationKeyboardShadow);
        }
#else
        {

            this.iOSController.PlayAndLoop(this.animationController);
            this.iOSControllerShadow.PlayAndLoop(this.animationControllerShadow);
            this.iOSTouch.PlayAndLoop(this.animationTouch);
            this.iOSTouchShadow.PlayAndLoop(this.animationTouchShadow);  
        }
#endif
    }

    private IEnumerator FadeInAndShowSymbols()
    {
        yield return new WaitForSeconds(7.6f);
        SetCorrectSymbols();
        {
            float fadeInDuration = 0.3f;
            var tweener = new Tweener();
            for (int i = 0; i < imagesToFadeInTo100.Length; i++)
                tweener.Alpha(imagesToFadeInTo100[i], 1, fadeInDuration);
            for (int i = 0; i < imagesToFadeInTo50.Length; i++)
                tweener.Alpha(imagesToFadeInTo50[i], 0.5f, fadeInDuration);
            tweener.Alpha(textCopyright, 1, fadeInDuration);
            yield return tweener.Run();
        }
        SetCorrectSymbolsAnimations();
    }

    private float startTime;
    private IEnumerator BackgroundFix()
    {
        yield return new WaitForSeconds(6.5f);

        this.startTime = Time.timeSinceLevelLoad;
        float fadeDuration = 0.3f;

        while(Time.timeSinceLevelLoad - this.startTime < fadeDuration)
        {
            Color32 startColor = new Color32(19, 38, 172, 255);
            Color32 endColor = new Color32(21, 35, 165, 255);

            Color32 c = Color32.Lerp(startColor, endColor, Mathf.Clamp01((Time.timeSinceLevelLoad - this.startTime)/fadeDuration));
            blueBackground.color = c;
            yield return null;
        }
    }

    private IEnumerator sfxCoroutine()
    {
        Audio.instance.PlaySfx(this.sfxTitle[0]);
        yield return new WaitForSeconds(2f);
        Audio.instance.PlaySfx(this.sfxTitle[1]);
        yield return new WaitForSeconds(4.2f);
        Audio.instance.PlaySfx(this.sfxTitle[2]);
        yield return new WaitForSeconds(1.5f);
        Audio.instance.PlaySfx(this.sfxTitle[3]);
    }

    void Update()
    {
        GameInput.Advance();
        
        if (GameInput.pressedConfirm == true ||
            Input.GetMouseButtonDown(0) == true)
        {
            if(!goingIntoGame)
            {
                goingIntoGame = true;
                Audio.instance.PlaySfx(this.sfxLeaveTitle);
                GameCenter.HideAccessPoint();

                StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 1, audioClip: this.titleMusic));
                Audio.instance.PlayMusic(this.mainliftMusic, loop: true, stopOtherMusic: false);
                StartCoroutine(Audio.instance.FadeMusic(startFrom: 0, setTo: 1, duration: 2, audioClip: this.mainliftMusic));

                StopCoroutine("sfxCoroutine");
                Audio.instance.StopSfx(this.sfxTitle[0]);
                Audio.instance.StopSfx(this.sfxTitle[1]);
                Audio.instance.StopSfx(this.sfxTitle[2]);
                Audio.instance.StopSfx(this.sfxTitle[3]);

                if (SaveData.HasCompletedOpeningTutorial() == false)
                    Game.selectedDate = Game.DateOfOpeningTutorial;

                Transition.GoToGame();
            }
        }
#if(UNITY_STANDALONE_OSX || UNITY_STANDALONE_WIN || UNITY_TVOS)
        else if(!goingIntoGame)
        {
            if(GameInput.pressedMenu == true)
                Application.Quit();
        }    
#endif
        AnimatedUI.AdvanceAll();
    }
}