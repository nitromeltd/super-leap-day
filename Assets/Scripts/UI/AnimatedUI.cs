﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedUI : MonoBehaviour
{
    private static List<AnimatedUI> allRunning = new List<AnimatedUI>();

    private class FrameAction
    {
        public int frameNumber;
        public Action action;
    }

    private Image image;

    [NonSerialized] public Animated.Animation currentAnimation;
    [NonSerialized] public int frameNumber;
    [NonSerialized] public float playbackSpeed = 1;
    [NonSerialized] public bool loop;
    [NonSerialized] public bool holdLastFrame;
    [NonSerialized] public Action onComplete;

    private float timeThroughFrame = 0.0f;
    private bool animationJustStarted = false;
    private bool nextAnimationStarted = false;
    private List<FrameAction> frameActions = new List<FrameAction>();

    private void OnEnable()
    {
        this.image = GetComponent<Image>();
        allRunning.Add(this);
    }
    private void OnDisable()
    {
        allRunning.Remove(this);
    }

    public void PlayAndLoop(Animated.Animation animation)
    {
        if(animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        if(animation != this.currentAnimation)
        {
            this.currentAnimation = animation;
            this.frameNumber = 0;
            this.playbackSpeed = 1;
            this.loop = true;
            this.holdLastFrame = false;
            this.onComplete = null;
            this.timeThroughFrame = 0;
            this.animationJustStarted = true;
            this.frameActions.Clear();

            if(this.image != null)
                this.image.sprite = animation.sprites[0];

            this.nextAnimationStarted = true;
        }
    }

    public void PlayAndHoldLastFrame(Animated.Animation animation)
    {
        if(animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        if(animation != this.currentAnimation)
        {
            this.currentAnimation = animation;
            this.frameNumber = 0;
            this.playbackSpeed = 1;
            this.loop = false;
            this.holdLastFrame = true;
            this.onComplete = null;
            this.timeThroughFrame = 0;
            this.animationJustStarted = true;
            this.frameActions.Clear();

            if(this.image != null)
                this.image.sprite = animation.sprites[0];

            this.nextAnimationStarted = true;
        }
    }

    public void PlayOnce(Animated.Animation animation, Action onComplete = null)
    {
        if(animation.sprites.Length == 0)
        {
            Debug.LogError("Unable to play animation.");
            return;
        }

        if(animation != this.currentAnimation)
        {
            this.currentAnimation = animation;
            this.frameNumber = 0;
            this.playbackSpeed = 1;
            this.loop = false;
            this.holdLastFrame = false;
            this.onComplete = onComplete;
            this.timeThroughFrame = 0;
            this.animationJustStarted = true;
            this.frameActions.Clear();

            if(this.image != null)
                this.image.sprite = animation.sprites[0];

            this.nextAnimationStarted = true;
        }
    }

    public void Stop()
    {
        this.currentAnimation = null;
        this.frameNumber = 0;
        this.playbackSpeed = 1;
        this.loop = false;
        this.holdLastFrame = false;
        this.onComplete = null;
        this.timeThroughFrame = 0;
        this.frameActions.Clear();
        this.nextAnimationStarted = true;
    }

    public void OnFrame(int frameNumberZeroBased, Action action)
    {
        this.frameActions.Add(
            new FrameAction { frameNumber = frameNumberZeroBased, action = action }
        );
    }


    public static void AdvanceAll()
    {
        foreach(var a in allRunning.ToArray())
            a.Advance();
    }

    private void Advance()
    {
        if(this.currentAnimation == null) return;
        if(this.image == null) return;

        if(this.animationJustStarted == true)
        {
            // since Animated stuff is the last thing to Advance,
            // we probably want to hold that timer one frame before starting off
            this.animationJustStarted = false;
        }
        else
        {
            float time = this.currentAnimation.fps / 60.0f;
            this.timeThroughFrame += time * this.playbackSpeed;
        }

        int framesToAdvance = Mathf.FloorToInt(this.timeThroughFrame);
        this.frameNumber += framesToAdvance;
        this.timeThroughFrame -= framesToAdvance;

        if(this.loop)
        {
            this.frameNumber %= this.currentAnimation.sprites.Length;
        }
        else if(this.holdLastFrame)
        {
            if(this.frameNumber > this.currentAnimation.sprites.Length - 1)
                this.frameNumber = this.currentAnimation.sprites.Length - 1;
        }
        else if(this.frameNumber >= this.currentAnimation.sprites.Length)
        {
            this.frameNumber = this.currentAnimation.sprites.Length - 1;
            this.nextAnimationStarted = false;

            if(this.onComplete != null)
            {
                // If calling onComplete() sets this.nextAnimationStarted,
                // it means they started another animation running,
                // so only clear it if that didn't happen
                this.onComplete();

                if(this.nextAnimationStarted == false)
                    this.onComplete = null;
            }

            if(this.nextAnimationStarted == false)
                this.currentAnimation = null;
        }

        if(this.frameActions.Count() > 0)
        {
            foreach(var frameAction in this.frameActions.ToArray())
            {
                if(this.frameNumber >= frameAction.frameNumber)
                {
                    frameAction.action.Invoke();
                    this.frameActions.Remove(frameAction);
                }
            }
        }

        if(this.currentAnimation != null)
            this.image.sprite = this.currentAnimation.sprites[this.frameNumber];
    }
}