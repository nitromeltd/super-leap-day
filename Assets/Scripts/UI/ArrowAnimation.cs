using UnityEngine;

public class ArrowAnimation : MonoBehaviour
{
    public enum Direction { Left, Right, Down, Up };
    public Direction direction;
    public float distance;

    private RectTransform rectTransform;
    private Vector2 center;

    public void Start()
    {
        this.rectTransform = this.GetComponent<RectTransform>();
        this.center = this.rectTransform.anchoredPosition;
    }

    public void Update()
    {
        float delta = this.distance * Mathf.Sin(Time.realtimeSinceStartup * Mathf.PI * 2);

        Vector2 newPosition = this.rectTransform.anchoredPosition;
        switch (this.direction)
        {
            case Direction.Left:  newPosition.x = this.center.x - delta; break;
            case Direction.Right: newPosition.x = this.center.x - delta; break;
            case Direction.Down:  newPosition.y = this.center.y - delta; break;
            case Direction.Up:    newPosition.y = this.center.y - delta; break;
        }
        this.rectTransform.anchoredPosition = newPosition;
    }
}
