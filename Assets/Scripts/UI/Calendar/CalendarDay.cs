﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalendarDay : MonoBehaviour
{
    public CalendarScreen calendarScreen;
    public RectTransform pageSize;

    public DateTime dateTime;

    [Header("Day Info")]
    public LocalizeText monthText;
    public TMP_Text numberText;
    public TMP_Text yearText;

    [Header("Theme")]
    public Image themeBackground;
    public Image themeLogo;
    public LocalizeText themeText;

    [Header("Cabinet")]
    public LocalizeText playerNameText;
    public TMP_Text timeText;
    public TMP_Text placeText;
    public TMP_Text deathsText;
    public Image trophyImage;
    public Image deathsTrophyImage;
    public Sprite deathsTrophySkullHatSprite;
    public Sprite deathsTrophySkullPurpleSprite;
    public Sprite deathsTrophySkullGoldSprite;
    public Sprite deathsTrophySkullFruitSprite;
    public Sprite deathsTrophySkullOnlySprite;
    public Sprite deathsTrophyEmptySprite;
    public Image timerTrophyImage;
    public Sprite timerTrophyGoldSprite;
    public Sprite timerTrophySilverSprite;
    public Sprite timerTrophyBlueSprite;
    public Sprite timerTrophyGreenSprite;
    public Sprite timerTrophyPurpleSprite;
    public Sprite timerTrophyGraySprite;
    public Sprite timerTrophyEmptySprite;
    public GameObject rosette;
    public ScoresScrollRect scoresScrollRect;
    public GameObject messageGO;
    public LocalizeText messageText;
    public GameObject noWifi;
    public Sprite[] trophyCabinetSprites;
    public RectTransform progressBarMarker;
    public RectTransform[] progressBarPoints;

    [Header("Navigation")]
    public UnityEngine.UI.Button playButton;
    public UnityEngine.UI.Button resetButton;
    [Header("Decals")]
    public CalendarDecals decals;

    private Coroutine waitForScrolling;

    public void SetDate(DateTime dateTime)
    {
        this.dateTime = dateTime;

        if(IsActive() == true)
        {
            // day info
            this.gameObject.SetActive(true);
            this.monthText.SetText(Localization.GetTextForMonthName(dateTime));
            int number = dateTime.Day;
            this.numberText.text = number.ToString();
            this.yearText.text = dateTime.Year.ToString();

            // theme
            var theme = this.calendarScreen.themesAsset.ThemeForDate(dateTime);
            this.themeBackground.sprite = theme.calendarBackgroundSprite;
            this.themeLogo.sprite = theme.calendarLogoSprite;
            this.themeText.SetText(theme.richText);

            // cabinet
            this.playerNameText.SetTextRaw(SaveData.GetSlotName(SaveData.currentSlotNumber));
            var record = SaveData.GetRecordForDate(dateTime);
            if(dateTime == Game.DateOfOpeningTutorial)
                this.trophyImage.sprite = this.trophyCabinetSprites[this.trophyCabinetSprites.Length - 1];
            else
                this.trophyImage.sprite = this.trophyCabinetSprites[(int)record.trophy];

            bool isDayCompleted = (int)record.trophy >= (int)SaveData.Trophy.Gold;
            this.timeText.text = isDayCompleted == true ? SaveData.TimeToString(record.timeInSeconds) : SaveData.TimeToString(0);
            this.deathsText.text = record.deaths.ToString();

            // Timer Trophy
            Sprite chosenTimerTrophySprite = this.timerTrophyEmptySprite;
            float timeInSeconds = record.timeInSeconds;
            
            if((int)record.trophy >= (int)SaveData.Trophy.Gold)
            {
                if(timeInSeconds < (5f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophyGoldSprite;
                }
                else if(timeInSeconds < (6f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophySilverSprite;                
                }
                else if(timeInSeconds < (7f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophyBlueSprite;
                }
                else if(timeInSeconds < (10f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophyGreenSprite;
                }
                else if(timeInSeconds < (15f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophyPurpleSprite;
                }
                else if(timeInSeconds < (20f * 60f))
                {
                    chosenTimerTrophySprite = this.timerTrophyGraySprite;
                }
            }
            this.timerTrophyImage.sprite = chosenTimerTrophySprite;

            // Deaths Trophy
            Sprite chosenDeathsTrophySprite = this.deathsTrophyEmptySprite;
            int deaths = record.deaths;

            if((int)record.trophy >= (int)SaveData.Trophy.Gold)
            {
                if(deaths <= 0)
                {
                    chosenDeathsTrophySprite = this.deathsTrophySkullHatSprite;
                }
                else if(deaths <= 1)
                {
                    chosenDeathsTrophySprite = this.deathsTrophySkullPurpleSprite;
                }
                else if(deaths <= 5)
                {
                    chosenDeathsTrophySprite = this.deathsTrophySkullGoldSprite;
                }
                else if(deaths <= 10)
                {
                    chosenDeathsTrophySprite = this.deathsTrophySkullFruitSprite;
                }
                else
                {
                    chosenDeathsTrophySprite = this.deathsTrophySkullOnlySprite;
                }
            }
            this.deathsTrophyImage.sprite = chosenDeathsTrophySprite;

            this.placeText.text = "";
            this.placeText.transform.parent.Find("#").gameObject.SetActive(false);
            this.decals.Randomize(dateTime);
            this.scoresScrollRect.Init();
            this.messageGO.SetActive(true);
            this.noWifi.SetActive(false);
            this.messageText.SetText("");

            var index = SaveData.GetLatestAttemptForDate(dateTime).checkpointNumber;
            if(index == 0 && isDayCompleted == true) index = 16;
            else if(index > 0) index = 16 - index;
            this.progressBarMarker.anchoredPosition = this.progressBarPoints[index].anchoredPosition;

            CheckLeaderboard();
        }
        else
            this.gameObject.SetActive(false);
    }

    public void CheckLeaderboard()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable)
        {
            this.messageGO.SetActive(true);
            this.noWifi.SetActive(true);
            return;
        }

        if (Leaderboard.AreLeaderboardsAvailableOnThisPlatform() == false)
        {
            this.messageGO.SetActive(true);
            this.messageText.SetText("Leaderboards are only available on iOS, macOS and tvOS.");
            return;
        }

        if (GameCenter.IsValidOSVersion() == false)
        {
            this.messageGO.SetActive(true);
            this.messageText.SetText("Leaderboards requires iOS14.");
            return;
        }

        if (Leaderboard.IsLeaderboardDateAvailable(dateTime) == false)
        {
            this.messageGO.SetActive(true);
            this.messageText.SetText("No Data. Leaderboards only available for the last 30 days.");
            return;
        }

        this.messageGO.SetActive(false);

        if(this.waitForScrolling != null)
            StopCoroutine(this.waitForScrolling);

        this.waitForScrolling = StartCoroutine(WaitForScrolling());
    }

    private IEnumerator WaitForScrolling()
    {
        while(
            this.calendarScreen.IsScrolling() == true ||
            this.dateTime != this.calendarScreen.currentDate ||
            this.calendarScreen.IsMonthView() == true
        )
            yield return null;

        this.waitForScrolling = null;

        Leaderboard.FetchLeaderboardData(dateTime, OnLoadLeaderboardDataSuccess);
        //SimulateLeaderboardLoaded();
    }

    // Test asynchronous requests
    private void SimulateLeaderboardLoaded()
    {
        IEnumerator FakeLoadingPause(DateTime dateTime)
        {
            yield return new WaitForSeconds(2f);
            OnLoadLeaderboardDataSuccess(ScoresScrollRect.DummyLeaderboardData(dateTime));
        }
        StartCoroutine(FakeLoadingPause(this.dateTime));
    }

    public void OnLoadLeaderboardDataSuccess(Leaderboard.LeaderboardData leaderboardData)
    {
        // may have changed dateTime since request
        if (this.dateTime.Date != leaderboardData.date.Date)
            return;

        this.scoresScrollRect.OnScoresLoaded(leaderboardData);
        int userScore = leaderboardData.LocalPlayerScore;
        int userRank = leaderboardData.LocalPlayerRank;

        var record = SaveData.GetRecordForDate(dateTime);
        if(userScore != 0 && record.trophy != SaveData.Trophy.None)
        {
            this.placeText.transform.parent.Find("#").gameObject.SetActive(true);
            this.placeText.text = userRank.ToString();
        }
    }

    public void OnClickCancel()
    {
        this.calendarScreen.CancelDay();
        Audio.instance.PlaySfx(this.calendarScreen.sfxForMenu.sfxUIBack);
    }

    public void OnClickPlay()
    {
        Game.selectedDate = this.dateTime;
        Audio.instance.PlaySfx(this.calendarScreen.sfxForMenu.sfxUIConfirm);
        this.calendarScreen.PlayGame();
    }

    public bool IsActive() => IsActive(this.dateTime);

    public static bool IsActive(DateTime dateTime) =>
        dateTime == DateTime.Now.Date ||
        dateTime == Game.DateOfOpeningTutorial ||
        (dateTime >= CalendarScreen.MinimumDate && dateTime <= CalendarScreen.MaximumDate);

}
