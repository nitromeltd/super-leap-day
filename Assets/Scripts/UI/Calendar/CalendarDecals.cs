﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

// Randomly selects art to make pages look unique
public class CalendarDecals : MonoBehaviour
{

    public Image cornerTopLeft;
    public Image cornerTopRight;
    public Image cornerBottomLeft;
    public Image cornerBottomRight;
    public Image scribble;
    public Sprite[] cornerBottomSprites;
    public Sprite[] cornerTopSprites;
    public Sprite[] scribbleSprites;// TODO: Mat said he would provide 12 scribbles, one for each month

    Vector3 scribblePositionDefault;
    void Awake()
    {
        if(scribble != null)
            this.scribblePositionDefault = this.scribble.rectTransform.anchoredPosition;
    }

    public void Randomize(DateTime dateTime)
    {
        UnityEngine.Random.InitState(SaveData.DateToInt(dateTime));

        List<Sprite> list;
        list = cornerTopSprites.ToList();
        PluckRngSprite(this.cornerTopRight, list);
        PluckRngSprite(this.cornerTopLeft, list);
        list = this.cornerBottomSprites.ToList();
        PluckRngSprite(this.cornerBottomRight, list);
        PluckRngSprite(this.cornerBottomLeft, list);

        if(scribble != null)
        {
            // is there a nice big space at the bottom?
            // calculate how many day-cells are blank at the end
            dateTime = dateTime.AddDays(-1);// we init the random month from the 1st of next month to avoid repeating day-decals
            var firstDayOfMonth = dateTime.AddDays(-(dateTime.Day - 1));
            int total = 42;
            total -= (((int)firstDayOfMonth.DayOfWeek + 6) % 7);// week starting Monday
            total -= (DateTime.DaysInMonth(firstDayOfMonth.Year, firstDayOfMonth.Month));
            if(total >= 9)
            {
                this.scribble.sprite = this.scribbleSprites[dateTime.Month % this.scribbleSprites.Length];// 12 scribbles
                var p = scribblePositionDefault + (Vector3)UnityEngine.Random.insideUnitCircle * 10f;
                p.x += UnityEngine.Random.Range(-20, 0);
                this.scribble.rectTransform.anchoredPosition = p;
                this.scribble.transform.localRotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-30f, 30f));
            }
            else
            {
                scribble.enabled = false;
            }
        }
    }

    public static void PluckRngSprite(Image image, List<Sprite> list)
    {
        var i = UnityEngine.Random.Range(0, list.Count);
        var s = list[i];
        image.sprite = s;
        image.enabled = s != null;
        list.RemoveAt(i);
    }

}
