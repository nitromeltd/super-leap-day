﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CalendarMonth : MonoBehaviour
{
    public DateTime firstDay;
    public DateTime lastDay;

    public CalendarScreen calendarScreen;
    public RectTransform pageSize;
    public LocalizeText monthText;
    public TMP_Text[] textWeekdays;
    public TMP_Text yearFooterText;
    public DayButton[] dayButtons;
    public CalendarDecals decals;
    public int weekdayStrLength;

    public Sprite[] trophyStickerSprites;

    public static string[] Weekdays = new string[] { "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY", "SUNDAY" };

    private void Start()
    {
        // create shorthand for weekdays
        for(int i = 0; i < Weekdays.Length; i++)
        {
            var str = Localization.GetText(Weekdays[i]);
            textWeekdays[i].text = str.Substring(0, Math.Min(weekdayStrLength, str.Length));
            Localization.SetTextFont(textWeekdays[i]);
        }
    }

    public void SetDate(DateTime dateTime)
    {
        this.firstDay = FirstDayOfMonth(dateTime);
        this.lastDay = LastDayOfMonth(dateTime);
        if(IsActive() == true)
        {
            this.gameObject.SetActive(true);
            int index = ((int)this.firstDay.DayOfWeek + 6) % 7;// week starting Monday
            for(int i = 0; i < this.dayButtons.Length; i++)
            {
                var db = this.dayButtons[i];
                DateTime date = this.firstDay.AddDays(-index + i);
                db.Init(date, this.firstDay, this.trophyStickerSprites);
            }
            this.monthText.SetText(Localization.GetTextForMonthName(this.firstDay));
            this.yearFooterText.text = dateTime.Year.ToString();
            this.decals.Randomize(this.lastDay.AddDays(1));// don't use same seed as day-pages
        }
        else
            this.gameObject.SetActive(false);
    }

    public DayButton GetDayButton(DateTime dateTime)
    {
        if(dateTime >= this.firstDay && dateTime <= this.lastDay)
        {
            for(int i = 0; i < this.dayButtons.Length; i++)
                if(this.dayButtons[i].dateTime == dateTime)
                    return this.dayButtons[i];
        }
        return null;
    }

    public bool IsActive() =>
        this.lastDay >= CalendarScreen.MinimumDate && this.firstDay <= CalendarScreen.MaximumDate;

    public static bool IsActive(DateTime dateTime) =>
        LastDayOfMonth(dateTime) >= CalendarScreen.MinimumDate && FirstDayOfMonth(dateTime) <= CalendarScreen.MaximumDate;

    public static DateTime FirstDayOfMonth(DateTime dateTime) =>
        dateTime.AddDays(-(dateTime.Day - 1));

    public static DateTime LastDayOfMonth(DateTime dateTime)
    {
        var firstDayOfMonth = FirstDayOfMonth(dateTime);
        return firstDayOfMonth.AddDays(DateTime.DaysInMonth(firstDayOfMonth.Year, firstDayOfMonth.Month) - 1);
    }

}
