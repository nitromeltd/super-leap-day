﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/* Calendar Controller
 * 
 * Device Aspect Ratios:
 * Each device requires a dozen different rules
 * 4k TV (16:9): 1.777778
 * Monitor (16:10): 1.6
 * iPad:  0.75
 * iPad 11ins: 0.6984925
 * iPhone4: 0.6666667
 * iPhone5: 0.5633803
 * iPhoneX: 0.4618227
 */

public class CalendarScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SfxForMenu sfxForMenu;
    public ThemesAsset themesAsset;
    public AudioClip musicMenu;
    public AudioClip musicTransition;

    [Serializable]
    public class Variant
    {
        public RectTransform container;
        public PlayerForMenu player;
        public RectTransform railMonth;
        public RectTransform railDay;
        public RectTransform backgroundContainer;
        public RectTransform groundContainer;
        public UnityEngine.UI.Button buttonLeft;
        public UnityEngine.UI.Button buttonRight;
        public RectTransform confirmDeletePanel;
        public UnityEngine.UI.Button buttonOutsideConfirmDelete;
        public UnityEngine.UI.Button buttonDismissConfirmDelete;
        public Animator liftAnimator;
        public Animated liftButtonAnimated;
        public RectTransform playerContainerWalking;
        public RectTransform playerContainerSitting;
        public RectTransform playerContainerLift;
        public RectTransform buttonExit;
        public SelectionCursor selectionCursor;
    }
    public Variant variantLandscape;
    public Variant variantPortrait;

    [Serializable]
    public class PagePosition
    {
        public float aspectRatio;
        public RectTransform position;
        public RectTransform size;
    }
    public PagePosition pagePositionIPhoneX;
    public PagePosition pagePositionIPhone5;
    public PagePosition pagePositionIPad11ins;
    public PagePosition pagePositionIPad;

    [NonSerialized] public SelectionCursor selectionCursor;

    public Animated.Animation animationLiftArrow;

    public DateTime currentDate;
    private List<CalendarMonth> calendarMonths;
    private List<CalendarDay> calendarDays;
    private DayButton buttonSelectedDay;
    private Coroutine currentPageCoroutine;
    private Coroutine currentRailCoroutine;
    private RectTransform currentRail;
    private bool dragging;
    private float screenWidth;
    private Vector2 mousePosition;
    private Vector2 mouseDownPosition;
    private Vector2 prevMousePosition;
    private float railTargetX;

    private bool ready;// has Start() completed?

    // Edge-case where someone sets their system-date to before the tutorial
    public static DateTime MinimumDate => Game.selectedDate < Game.DateOfOpeningTutorial ? Game.selectedDate : Game.DateOfOpeningTutorial;
    public static DateTime MaximumDate => Game.selectedDate > Game.DateLastAllowed ? Game.selectedDate : Game.DateLastAllowed;


    void Awake()
    {
        Application.targetFrameRate = 60;
        Game.InitScene();
    }

    IEnumerator Start() {

        Canvas.ForceUpdateCanvases();

        this.variantLandscape.container.gameObject.SetActive(false);
        this.variantPortrait.container.gameObject.SetActive(false);

        var v = ActiveVariant();

        this.currentRail = v.railMonth;

        this.selectionCursor = v.selectionCursor;
        v.liftAnimator.enabled = false;
        v.container.gameObject.SetActive(true);
        v.confirmDeletePanel.gameObject.SetActive(false);
        v.buttonOutsideConfirmDelete.gameObject.SetActive(false);

        // variants
        PagePosition pagePosition = null;
        if(GameCamera.IsLandscape() == false)
        {
            var bestDist = float.MaxValue;
            var pagePositions = new[]
            {
                this.pagePositionIPad,
                this.pagePositionIPad11ins,
                this.pagePositionIPhone5,
                this.pagePositionIPhoneX
            };
            var aspectRatio = (float)Screen.width / Screen.height;
            foreach(var profile in pagePositions)
            {
                var dist = Mathf.Abs(aspectRatio - profile.aspectRatio);
                if(dist < bestDist)
                {
                    pagePosition = profile;
                    bestDist = dist;
                }
            }
            // tall ground on iPhoneX only
            if(aspectRatio > 0.56f)
                v.groundContainer.anchoredPosition += Vector2.down * 50f;
            // exit button repositioned on iPad
            if(aspectRatio > 0.69f)
                v.buttonExit.anchoredPosition = new Vector2(294, 296);
        }
        else
        {
            // TODO: Rebuild old landscape assets to match new Canvas scale
            // (write an editor script that will move anchors to fit a local scale of 1,1,1)
            
            // fix for background anchors
            var parentSize = (v.backgroundContainer.parent as RectTransform).rect.size;
            var childSize = parentSize / (Vector2)v.backgroundContainer.localScale;
            var delta = (childSize - parentSize) / 2;
            v.backgroundContainer.offsetMin = -delta;
            v.backgroundContainer.offsetMax = delta;
        }

        // page carousel
        this.calendarMonths = new List<CalendarMonth>();
        this.calendarDays = new List<CalendarDay>();
        this.screenWidth = v.container.rect.width;
        this.currentDate = Game.selectedDate;

        var monthOriginal = v.railMonth.Find("Month").gameObject;
        var dayOriginal = v.railDay.Find("Day").gameObject;
        for(int i = 0; i < 3; i++)
        {
            var monthObj = Instantiate(monthOriginal, this.transform);
            var dayObj = Instantiate(dayOriginal, this.transform);
            var calendarMonth = monthObj.GetComponent<CalendarMonth>();
            var calendarDay = dayObj.GetComponent<CalendarDay>();

            for(int j = 0; j < calendarMonth.dayButtons.Length; j++)
            {
                var dayButton = calendarMonth.dayButtons[j];
                dayButton.button.onClick.AddListener(() =>
                {
                    OnClickDayButton(dayButton);
                });
            }

            monthObj.transform.SetParent(v.railMonth, true);
            dayObj.transform.SetParent(v.railDay, true);
            this.calendarMonths.Add(calendarMonth);
            this.calendarDays.Add(calendarDay);
            calendarMonth.SetDate(this.currentDate.AddMonths(i - 1));
            calendarDay.SetDate(this.currentDate.AddDays(i - 1));
            if(GameCamera.IsLandscape() == false)
            {
                CopyRectTransform(pagePosition.position, (RectTransform)monthObj.transform);
                CopyRectTransform(pagePosition.size, calendarMonth.pageSize);
                CopyRectTransform(pagePosition.position, (RectTransform)dayObj.transform);
                CopyRectTransform(pagePosition.size, calendarDay.pageSize);
            }
            calendarMonth.transform.localPosition += new Vector3((i - 1) * screenWidth, 0);
            calendarDay.transform.localPosition += new Vector3((i - 1) * screenWidth, 0);
        }
        Destroy(monthOriginal);
        Destroy(dayOriginal);

        v.railDay.localPosition += Vector3.up * (this.transform as RectTransform).sizeDelta.y;

        this.railTargetX = this.currentRail.localPosition.x;

        this.buttonSelectedDay = this.calendarMonths[1].GetDayButton(Game.selectedDate);
        this.selectionCursor.Select(this.buttonSelectedDay.button, false);

        // Audio Start() executes after CalendarScreen.Start();
        yield return null;

        Audio.instance.PlayMusic(this.musicMenu);

        // player running in
        var player = v.player;
        var stairsX = player.TargetXForObject(player.stairs);
        var chairX =
            player.TargetXForObject(v.playerContainerSitting);
        player.rectTransform.anchoredPosition = new Vector2(stairsX - 200, 0);

        player.WalkToX(chairX, () =>
        {
            player.rectTransform.SetParent(
                v.playerContainerSitting
            );
            player.Sit();
            Audio.instance.PlaySfx(sfxForMenu.sfxPlayerGrab);
            Audio.instance.PlaySfx(sfxForMenu.sfxPlayerJump);
        });
        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));
        GameCenter.ShowAccessPoint();

        this.ready = true;
    }

    void Update()
    {
        if(this.ready == false)
            return;

        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);
        var v = ActiveVariant();

        if(this.currentPageCoroutine == null && this.currentRailCoroutine == null)
        {
            if(this.selectionCursor.gameObject.activeSelf == false ||
                this.selectionCursor.IsActive == false)
            {
                UpdateRailDrag();
                if(IsMonthView() == true)
                    WrapPages(this.calendarMonths);
                else
                    WrapPages(this.calendarDays);
            }


            if(GameInput.pressedMenu)
            {
                if(IsDayView() == true)
                {
                    if(v.confirmDeletePanel.gameObject.activeInHierarchy == true)
                        OnClickDismissResetDay(true);
                    else
                        CancelDay();
                }
                else if(IsScrolling() == false && v.player.IsWalking() == false)
                {
                    OnClickExitButton();
                }
            }
        }

        var stepRightActive = CanScroll(1);
        var stepLeftActive = CanScroll(-1);

        if(v.buttonLeft.gameObject.activeSelf != stepLeftActive)
            v.buttonLeft.gameObject.SetActive(stepLeftActive);
        if(v.buttonRight.gameObject.activeSelf != stepRightActive)
            v.buttonRight.gameObject.SetActive(stepRightActive);
    }


    public void OnClickDayButton(DayButton dayButton)
    {
        if(IsScrolling() == true) return;

        var v = ActiveVariant();
        v.buttonLeft.gameObject.SetActive(false);
        v.buttonRight.gameObject.SetActive(false);
        this.buttonSelectedDay.SetSelected(false);
        Game.selectedDate = this.currentDate = dayButton.dateTime;
        for(int i = 0; i < 3; i++)
            this.calendarDays[i].SetDate(this.currentDate.AddDays(i - 1));
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));
        this.currentRailCoroutine = StartCoroutine(ScrollToRail(v.railDay));
    }
    
    public void CancelDay()
    {
        if(IsScrolling() == true) return;

        var v = ActiveVariant();
        v.buttonLeft.gameObject.SetActive(false);
        v.buttonRight.gameObject.SetActive(false);

        for(int i = 0; i < 3; i++)
            this.calendarMonths[i].SetDate(this.currentDate.AddMonths(i - 1));

        this.buttonSelectedDay = this.calendarMonths[1].GetDayButton(this.currentDate);
        this.buttonSelectedDay.SetSelected(true);

        this.currentRailCoroutine = StartCoroutine(ScrollToRail(ActiveVariant().railMonth));

        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));
    }

    public void OnClickExitButton()
    {
        var v = ActiveVariant();
        var player = ActiveVariant().player;
        player.Stand();
        player.rectTransform.SetParent(v.playerContainerWalking);
        var stairsX = player.TargetXForObject(player.stairs);
        player.WalkToX(stairsX - 500, () =>
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));
            MainMenuScreen.enterSceneThroughDoor = MainMenuScreen.DoorType.Calendar;
            Transition.GoSimple("Main Menu");
        });
        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));
        player.canPlayStairSfx = true;
        GameCenter.HideAccessPoint();
    }

    public void OnClickPlayButton()
    {
        PlayGame();
    }

    public void PlayGame()
    {
        if(this.ready == false)
            return;

        var v = ActiveVariant();

        // prevent any further input
        LD2Button.DisableAllInteraction();
        this.selectionCursor.gameObject.SetActive(false);
        var player = v.player;
        var liftX =
            player.TargetXForObject(v.playerContainerLift);
        player.Stand();

        IEnumerator PlayTransitionSfx()
        {
            yield return new WaitForSeconds(.8f);
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn, options: new Audio.Options(1f, false, 0f));
        }

        IEnumerator CloseDoorSequence()
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxPlayerJump);

            if(player.Character == Character.Yolk)
                Audio.instance.PlaySfx(sfxForMenu.sfxYolkVoice);
            else if(player.Character == Character.Puffer)
                Audio.instance.PlaySfx(sfxForMenu.sfxPuffer);
            else if(player.Character == Character.Sprout)
                Audio.instance.PlaySfx(sfxForMenu.sfxSprout);
            else if(player.Character == Character.King)
                Audio.instance.PlaySfx(sfxForMenu.sfxKing);
            else if (player.Character == Character.Goop)
                Audio.instance.PlaySfx(sfxForMenu.sfxGoop);

            yield return player.WalkToXCoroutine(liftX);
            
            player.rectTransform.SetParent(
                v.playerContainerLift
            );

            v.liftAnimator.Play("close");

            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator2floorClose, options: new Audio.Options(1f, false, 0f));
            Audio.instance.StopMusic(musicMenu);

            yield return new WaitForSeconds(0.5f);

            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevatorFloorchange, options: new Audio.Options(1f, false, 0f));
            Audio.instance.PlayMusic(musicTransition);
            v.liftButtonAnimated.PlayOnce(this.animationLiftArrow);

            yield return new WaitForSeconds(0.5f);

            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));
            if (DebugPanel.selectedMode != DebugPanel.Mode.AlgorithmRegenerate)
                DebugPanel.selectedMode = DebugPanel.Mode.Algorithm;
            Transition.GoToGame();
            StartCoroutine(PlayTransitionSfx());
        }
        StartCoroutine(CloseDoorSequence());

        v.liftAnimator.enabled = true;
        v.liftAnimator.Play("open");
        Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator, options: new Audio.Options(1f, false, 0f));
        GameCenter.HideAccessPoint();
    }

    private void UpdateRailDrag()
    {
        this.mousePosition = UIMousePosition(this.transform as RectTransform);

        if(Input.GetMouseButtonDown(0) == true)
        {
            this.dragging = false;
            this.prevMousePosition = this.mouseDownPosition = this.mousePosition;
            if(this.currentPageCoroutine != null)
            {
                StopCoroutine(this.currentPageCoroutine);
            }
        }
        else if(Input.GetMouseButton(0) == true)
        {
            if(this.dragging == false && (this.mousePosition - this.mouseDownPosition).magnitude > 1f)
                this.dragging = true;
            this.currentRail.localPosition += Vector3.right * (this.mousePosition.x - this.prevMousePosition.x);
            this.prevMousePosition = this.mousePosition;
        }
        else if(Input.GetMouseButtonUp(0) == true || (this.dragging == true && GameInput.IsMouseOnScreen() == false))
        {
            this.dragging = false;
            var dist = this.railTargetX - this.currentRail.localPosition.x;
            var absDist = Mathf.Abs(dist);
            var steps = 0;

            if(absDist > 80f)
            {
                steps = 1;
                var sign = (int)Mathf.Sign(dist);
                if(absDist >= this.screenWidth)
                    steps = Mathf.RoundToInt(absDist / this.screenWidth);
                steps *= sign;

                if(IsMonthView() == true)
                {
                    while(CalendarMonth.IsActive(this.currentDate.AddMonths(steps)) == false)
                        steps -= sign;
                }
                else
                {
                    while(CalendarDay.IsActive(this.currentDate.AddDays(steps)) == false)
                        steps -= sign;
                }

            }
            this.currentPageCoroutine = StartCoroutine(ScrollToPage(steps));
        }
    }

    private IEnumerator ScrollToPage(int dir)
    {
        this.selectionCursor.gameObject.SetActive(false);
        this.dragging = false;

        this.railTargetX += screenWidth * -dir;
        if(IsMonthView() == true)
            this.currentDate = this.currentDate.AddMonths(dir);
        else
            this.currentDate = Game.selectedDate = this.currentDate.AddDays(dir);

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(
            this.currentRail.localPosition.x, this.railTargetX, 0.25f, Easing.QuadEaseInOut
        );
        if(dir != 0)
            Audio.instance.PlaySfx(sfxForMenu.sfxUICalendarMonthswipe, options: new Audio.Options(1f, false, 0f));

        while(tweener.IsDone() == false)
        {
            if(this.dragging == true)
            {
                this.currentPageCoroutine = null;
                yield break;
            }
            tweener.Advance(Time.deltaTime);
            var p = this.currentRail.localPosition;
            p.x = tween.Value();
            this.currentRail.localPosition = p;

            yield return null;
        }
        this.currentPageCoroutine = null;

        if(dir != 0)
        {
            if(IsMonthView() == true)
                WrapPages(this.calendarMonths);
            else
                WrapPages(this.calendarDays);

            this.selectionCursor.gameObject.SetActive(true);
            if(CanScroll(dir) == true)
            {
                this.selectionCursor.selection.gameObject.SetActive(true);
                this.selectionCursor.Select(this.selectionCursor.selection);
            }
            else
            {
                var v = ActiveVariant();
                var otherButton = CanScroll(1) ? v.buttonRight : v.buttonLeft;
                otherButton.gameObject.SetActive(true);
                this.selectionCursor.Select(otherButton);
            }
        }
    }

    private IEnumerator ScrollToRail(RectTransform nextRail)
    {
        this.selectionCursor.gameObject.SetActive(false);

        var screenHeight = (this.transform as RectTransform).sizeDelta.y;

        var tweener = new Tweener();
        tweener.MoveLocal(this.currentRail, this.currentRail.localPosition + Vector3.up * screenHeight, 0.25f, Easing.QuadEaseInOut);
        tweener.MoveLocal(nextRail, nextRail.localPosition + Vector3.down * screenHeight, 0.25f, Easing.QuadEaseInOut, 0.25f);

        Audio.instance.PlaySfx(sfxForMenu.sfxUICalendarDayswipe, options: new Audio.Options(1f, false, 0f));

        while(tweener.IsDone() == false)
        {
            tweener.Advance(Time.deltaTime);
            yield return null;
        }
        this.currentRail = nextRail;
        this.railTargetX = this.currentRail.localPosition.x;
        this.currentRailCoroutine = null;

        this.selectionCursor.gameObject.SetActive(true);

        if(IsMonthView() == true)
            this.selectionCursor.Select(this.buttonSelectedDay.button, false);
        else
            this.selectionCursor.Select(this.calendarDays[1].playButton, false);
    }

    private void WrapPages<T>(List<T> pages)
    {
        T itemA, itemB;
        Vector3 a, b;

        // check left
        itemA = pages[0];
        itemB = pages[pages.Count - 1];
        a = (itemA as MonoBehaviour).transform.localPosition;
        b = (itemB as MonoBehaviour).transform.localPosition;

        var wrapLeft = (a.x + this.currentRail.localPosition.x) + this.screenWidth * 0.5f;
        if(wrapLeft < -this.screenWidth * 0.5f)
        {
            a.x = b.x + this.screenWidth;
            pages.RemoveAt(0);
            pages.Add(itemA);
            (itemA as MonoBehaviour).transform.localPosition = a;
            if(itemA is CalendarMonth)
                (itemA as CalendarMonth).SetDate((pages[1] as CalendarMonth).firstDay.AddMonths(1));
            else if(itemA is CalendarDay)
                (itemA as CalendarDay).SetDate((pages[1] as CalendarDay).dateTime.AddDays(1));
        }

        // check right
        itemA = pages[pages.Count - 1];
        itemB = pages[0];
        a = (itemA as MonoBehaviour).transform.localPosition;
        b = (itemB as MonoBehaviour).transform.localPosition;

        var wrapRight = (a.x + this.currentRail.localPosition.x) - this.screenWidth * 0.5f;
        if(wrapRight > this.screenWidth)
        {
            a.x = b.x - this.screenWidth;
            pages.RemoveAt(this.calendarMonths.Count - 1);
            pages.Insert(0, itemA);
            (itemA as MonoBehaviour).transform.localPosition = a;
            if(itemA is CalendarMonth)
                (itemA as CalendarMonth).SetDate((pages[1] as CalendarMonth).firstDay.AddMonths(-1));
            else if(itemA is CalendarDay)
                (itemA as CalendarDay).SetDate((pages[1] as CalendarDay).dateTime.AddDays(-1));
        }
    }

    public void OnClickArrow(int dir)
    {
        this.currentPageCoroutine = StartCoroutine(ScrollToPage(dir));
    }

    public void OnClickResetDay()
    {
        var v = ActiveVariant();
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));
        this.selectionCursor.gameObject.SetActive(false);
        v.confirmDeletePanel.gameObject.SetActive(true);
        v.confirmDeletePanel.anchoredPosition = new Vector2(0, (this.transform as RectTransform).sizeDelta.y);

        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(v.confirmDeletePanel, Vector2.zero, 0.3f, Easing.QuadEaseOut);
        tweener.Callback(0.3f, () =>
        {
            this.selectionCursor.Select(v.buttonDismissConfirmDelete);
            this.selectionCursor.ConstrainMovement(v.confirmDeletePanel.gameObject);
            this.selectionCursor.gameObject.SetActive(true);
        });
        v.buttonOutsideConfirmDelete.gameObject.SetActive(true);
        tweener.Alpha(v.buttonOutsideConfirmDelete.image, 0.5f, 0.3f);
        tweener.Callback(0.3f, () =>
        {
            v.buttonOutsideConfirmDelete.interactable = true;
        });
        StartCoroutine(tweener.Run());
    }

    public void OnClickConfirmResetDay()
    {
        var calendarDay = this.calendarDays[1];
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));
        SaveData.ClearLatestAttemptForDate(calendarDay.dateTime);
        calendarDay.SetDate(calendarDay.dateTime);
        OnClickDismissResetDay(false);
    }

    public void OnClickDismissResetDay(bool sfxBack)
    {
        var v = ActiveVariant();
        if(sfxBack)
            Audio.instance.PlaySfx(sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));
        this.selectionCursor.gameObject.SetActive(false);
        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(v.confirmDeletePanel, new Vector2(0, (this.transform as RectTransform).sizeDelta.y), 0.3f, Easing.QuadEaseIn);
        tweener.Callback(0.3f, () =>
        {
            v.confirmDeletePanel.gameObject.SetActive(false);
            this.selectionCursor.RemoveConstraint();
            this.selectionCursor.Select(this.calendarDays[1].resetButton);
            this.selectionCursor.gameObject.SetActive(true);
        });
        v.buttonOutsideConfirmDelete.interactable = false;
        tweener.Alpha(v.buttonOutsideConfirmDelete.image, 0f, 0.3f);
        tweener.Callback(0.3f, () =>
        {
            v.buttonOutsideConfirmDelete.gameObject.SetActive(false);
        });
        StartCoroutine(tweener.Run());
    }

    public bool IsScrolling() =>
        this.currentPageCoroutine != null || this.currentRailCoroutine != null || this.dragging == true;
    
    private Variant ActiveVariant() =>
        GameCamera.IsLandscape() ? this.variantLandscape : this.variantPortrait;

    public bool IsMonthView() =>
        this.currentRail == ActiveVariant().railMonth;

    public bool IsDayView() =>
        this.currentRail == ActiveVariant().railDay;

    public bool CanScroll(int dir) =>
        IsScrolling() == false &&
            (IsMonthView() == true ?
                CalendarMonth.IsActive(this.currentDate.AddMonths(dir)) == true :
                CalendarDay.IsActive(this.currentDate.AddDays(dir)) == true);

    public static void CopyRectTransform(RectTransform source, RectTransform target)
    {
        if(source == null || target == null) return;
        target.anchorMin = source.anchorMin;
        target.anchorMax = source.anchorMax;
        target.anchoredPosition = source.anchoredPosition;
        target.sizeDelta = source.sizeDelta;
    }

    /* Converts mouse position to Unity UI scale */
    public static Vector2 UIMousePosition(RectTransform canvasTransform)
    {
        Vector2 pos;
        if(Input.touchCount > 0)
            pos = Input.GetTouch(0).position;
        else
            pos = Input.mousePosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(canvasTransform, pos, Camera.main, out Vector2 p);
        return p;
    }

}
