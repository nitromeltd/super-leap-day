﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// Month page day-cell
public class DayButton : MonoBehaviour
{
    public DateTime dateTime;
    public TMP_Text numberText;
    public Image trophyImage;
    public UnityEngine.UI.Button button;
    public RectTransform banner;
    [NonSerialized] public bool visible;

    public static Color32 SelectionColor = new Color32(165, 165, 165, 255);

    private Vector3 trophyPositionDefault;

    void Awake()
    {
        this.trophyPositionDefault = this.trophyImage.rectTransform.anchoredPosition;
    }

    public void Init(DateTime dateTime, DateTime firstDayOfMonth, Sprite[] trophySprites)
    {
        this.dateTime = dateTime;
        this.numberText.text = dateTime.Day.ToString();

        if (dateTime.Date.Month == firstDayOfMonth.Date.Month)
        {
            string bannerText =
                (dateTime.Date.Year, dateTime.Date.Month, dateTime.Date.Day) switch
                {
                    (2021,  8, 15) => "v 1.1",
                    (2021, 10, 22) => "v 1.2",
                    (2022,  1, 28) => "v 1.3",
                    (2022,  3, 23) => "v 1.4",
                    (2022,  4,  7) => "v 1.5",
                    (2022,  6, 13) => "v 1.6",
                    (2022,  9,  1) => "v 1.7",
                    (2023,  1, 13) => "v 1.8",
                    _ => null
                };

            banner.gameObject.SetActive(bannerText != null);
            if (bannerText != null)
                banner.GetComponentInChildren<TMP_Text>().text = bannerText;
        }
        else
        {
            banner.gameObject.SetActive(false);
        }

        if(dateTime == Game.DateOfOpeningTutorial)
        {
            this.trophyImage.sprite = trophySprites[trophySprites.Length - 1];
            this.numberText.gameObject.SetActive(false);
            this.visible = true;
            TiltAndDisplaceTrophy();
            if(GameCamera.IsLandscape() == true)
                this.trophyImage.rectTransform.anchoredPosition += Vector2.left * 54f;
            else
                this.trophyImage.rectTransform.anchoredPosition += Vector2.down * 20f;
            return;
        }

        this.numberText.gameObject.SetActive(true);

        var trophy = SaveData.GetRecordForDate(dateTime).trophy;
        this.trophyImage.sprite = trophySprites[(int)trophy];
        SetVisible(dateTime.Month == firstDayOfMonth.Month);
        if(this.visible == true)
        {
            if(CalendarDay.IsActive(dateTime) == true)
            {
                if(trophy > 0) TiltAndDisplaceTrophy();
            } 
            else
            {
                this.numberText.color = SelectionColor;
                this.button.interactable = false;
                this.trophyImage.sprite = trophySprites[0];
            }
        }
    }

    public void TiltAndDisplaceTrophy()
    {
        // tilt and displace
        UnityEngine.Random.InitState(SaveData.DateToInt(dateTime));
        this.trophyImage.rectTransform.anchoredPosition = this.trophyPositionDefault + (Vector3)UnityEngine.Random.insideUnitCircle * 5f;
        this.trophyImage.transform.localRotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(-30f, 30f));
    }

    public void SetVisible(bool value)
    {
        this.visible = value;
        this.numberText.enabled = value;
        this.trophyImage.enabled = value;
        this.button.interactable = value;
        this.numberText.color = Color.black;
        this.button.image.color = Color.white;
    }
    public void SetSelected(bool value)
    {
        if(value == true)
        {
            this.numberText.color = Color.white;
            this.button.image.color = SelectionColor;
        }
        else
        {
            this.numberText.color = Color.black;
            this.button.image.color = Color.white;
        }
    }
}
