﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Manages highscore table in CalendarDay cabinet
public class ScoresScrollRect : MonoBehaviour
{
    public CalendarScreen calendarScreen;
    public ScrollRect scrollRect;
    public RectTransform linesRectTransform;
    public RectTransform contentRectTransform;
    public UnityEngine.UI.Button upButton;
    public UnityEngine.UI.Button downButton;
    public List<LoadedEntry> loadedEntries;
    public List<ScoresScrollRectLine> lines;
    public GameObject scoreLinePrefab;
    public Image loadingImage;

    private List<Coroutine> lineCoroutines = new List<Coroutine>();
    private Coroutine loadingCoroutine;
    private float lineHeight;
    private int lineIndex;
    private float scrollMax = 1f;
    private float scrollMin = 0f;
    private bool scrollable = false;
    private bool scoresLoaded = false;

    private float scrollRectVelocity = 500f;

    static Color HiddenCol = new Color(1, 1, 1, 0);
    static Color[] LineBackgroundCols = new Color[]
    {
        new Color32(131,122,133,255), new Color32(159,159,159,255)
    };

    public void Init()
    {
        this.loadedEntries = new List<LoadedEntry>();
        this.lines = new List<ScoresScrollRectLine>();

        StopLineCoroutines();

        var lineHeightReference = Instantiate(scoreLinePrefab, this.linesRectTransform);
        this.lineHeight = lineHeightReference.GetComponent<RectTransform>().rect.height;
        foreach(Transform childTransform in this.linesRectTransform)
            Destroy(childTransform.gameObject);

        this.scrollRectVelocity = this.scrollRect.viewport.rect.height * 1.5f;

        int total = Mathf.CeilToInt(this.scrollRect.viewport.rect.height / this.lineHeight);
        for(int i = 0; i < total; i++)
            AddLine(i);
        this.upButton.interactable = false;
        this.upButton.image.color = HiddenCol;
        this.downButton.interactable = false;
        this.downButton.image.color = HiddenCol;
        this.loadingImage.color = Color.white;
        this.scoresLoaded = false;
        UpdateContentSize();
    }

    void Update()
    {
        if(this.scrollable == true && this.scoresLoaded == true && this.calendarScreen.IsDayView() == true)
        {
            if(this.scrollRect.verticalNormalizedPosition > this.scrollMax)
            {
                this.upButton.interactable = false;
                this.upButton.image.color = HiddenCol;

                if (this.calendarScreen.selectionCursor.selection == this.upButton)
                    this.calendarScreen.selectionCursor.Select(this.downButton);
            }
            else
            {
                this.upButton.interactable = true;
                this.upButton.image.color = Color.white;
            }
            if(this.scrollRect.verticalNormalizedPosition < this.scrollMin)
            {
                this.downButton.interactable = false;
                this.downButton.image.color = HiddenCol;

                if (this.calendarScreen.selectionCursor.selection == this.downButton)
                    this.calendarScreen.selectionCursor.Select(this.upButton);
            }
            else
            {
                this.downButton.interactable = true;
                this.downButton.image.color = Color.white;
            }
        }
        if(this.scoresLoaded == true)
        {
            var c = this.loadingImage.color;
            if(c.a > 0)
            {
                c.a -= Time.deltaTime / 0.2f;
                this.loadingImage.color = c;
            }
        }
        this.loadingImage.transform.localRotation = Quaternion.Euler(0, 0, this.loadingImage.transform.localRotation.eulerAngles.z + 3f);
    }

    public void UpdateContentSize()
    {
        var contentDefaultSize = this.contentRectTransform.sizeDelta;
        contentDefaultSize.y = this.lineHeight * lines.Count;
        this.contentRectTransform.sizeDelta = contentDefaultSize;
        this.scrollRect.verticalNormalizedPosition = 1f;
        this.scrollMin = 1f / contentDefaultSize.y * this.lineHeight / 3;
        this.scrollMax = 1f - this.scrollMin;
    }

    public void OnScoresLoaded(Leaderboard.LeaderboardData leaderboardData)
    {
        if(leaderboardData.HaveEntries() == false) return;

        this.scoresLoaded = true;

        StopLineCoroutines();

        this.loadedEntries.Clear();
        
        //Debug.Log("--> Loading user scores...");
        foreach(var entry in leaderboardData.Entries)
        {
            //Debug.Log($"Score Id: {entry.PlayerIdentifier} / Score Place: {entry.Rank} / Score Value: {entry.Score}");
            string name = entry.PlayerIdentifier;
            int place = entry.Rank;
            float time = (float)TimeSpan.FromMilliseconds(entry.Score * 10).TotalSeconds;
            this.loadedEntries.Add(new LoadedEntry{ name = name, place = place, timeInSeconds = time });
        }
        this.scrollable = false;
        this.upButton.interactable = false;
        this.upButton.image.color = HiddenCol;
        this.downButton.interactable = false;
        this.downButton.image.color = HiddenCol;

        for(int i = 0; i < this.loadedEntries.Count; i++)
        {
            ScoresScrollRectLine line;
            if(i >= this.lines.Count)
            {
                line = AddLine(i);
                this.scrollable = true;
            }
            else
            {
                line = this.lines[i];
            }
            line.SetEntry(this.loadedEntries[i]);
        }
        
        UpdateContentSize();
        this.lineIndex = 0;
        this.lineCoroutines.Add(StartCoroutine(RevealScoreLine()));
    }

    private void StopLineCoroutines()
    {
        foreach(var c in this.lineCoroutines)
            if(c != null) StopCoroutine(c);
        this.lineCoroutines.Clear();
    }

    ScoresScrollRectLine AddLine(int i)
    {
        var obj = Instantiate(scoreLinePrefab, this.linesRectTransform);
        var line = obj.GetComponent<ScoresScrollRectLine>();
        line.Init();
        this.lines.Add(line);
        line.background.color = LineBackgroundCols[i % 2];
        return line;
    }

    IEnumerator RevealScoreLine()
    {
        if(this.lineIndex > 0)
            yield return new WaitForSeconds(0.1f);

        this.lines[this.lineIndex].Play();
        this.lineIndex++;
        if(this.lineIndex < this.loadedEntries.Count)
            this.lineCoroutines.Add(StartCoroutine(RevealScoreLine()));
    }


    public void OnClickScrollScores(int dir)
    {
        // bug in scrollrect stops velocity being changed when position is at 1f or 0f
        this.scrollRect.verticalNormalizedPosition = Mathf.Clamp(this.scrollRect.verticalNormalizedPosition, 0.0001f, 0.9999f);
        this.scrollRect.velocity = new Vector2(0, this.scrollRectVelocity * dir);
    }

    public class LoadedEntry
    {
        public string name;
        public int place;
        public float timeInSeconds;
    }

    public static Leaderboard.LeaderboardData DummyLeaderboardData(DateTime date)
    {
        var name = date.ToString("yyyyMMdd");
        var entries = new Leaderboard.LeaderboardData.Entry[50];

        for (int i = 0; i < 50; i += 1)
        {
            entries[i] = new Leaderboard.LeaderboardData.Entry
            {
                PlayerIdentifier = name,
                Rank = UnityEngine.Random.Range(1, 100),
                Score = UnityEngine.Random.Range(1, 100)
            };
        }

        return new Leaderboard.LeaderboardData
        {
            Entries = entries,
            date = date,
            LocalPlayerRank = UnityEngine.Random.Range(1, 100),
            LocalPlayerScore = UnityEngine.Random.Range(1, 100),
            Success = true,
            TotalPlayerCount = entries.Length
        };
    }

}
