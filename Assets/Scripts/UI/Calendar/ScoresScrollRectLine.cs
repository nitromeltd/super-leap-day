﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

// The flap-in animation is performed with 2 TMP_Texts and a mask
public class ScoresScrollRectLine : MonoBehaviour
{
    public Animator animator;
    public Image background;
    public Image topFlapImage;
    public Image bottomFlapImage;
    // we transition from front to back with a mask
    public ScoresScrollRectText frontText;
    public ScoresScrollRectText backText;
    public ScoresScrollRect.LoadedEntry entry;

    [System.Serializable]
    public class ScoresScrollRectText
    {
        public TMP_Text placeText;
        public TMP_Text nameText;
        public TMP_Text timeText;
        public RectTransform mask;

        public void SetEntry(ScoresScrollRect.LoadedEntry entry)
        {
            this.placeText.text = entry.place.ToString();
            this.nameText.text = entry.name;
            this.timeText.text = SaveData.TimeToStringFractions(entry.timeInSeconds);
        }

        public void SetBlank()
        {
            this.placeText.text = "";
            this.nameText.text = "";
            this.timeText.text = "";
        }
    }

    public void Init()
    {
        this.animator.enabled = false;
        this.topFlapImage.enabled = false;
        this.bottomFlapImage.enabled = false;
        this.frontText.SetBlank();
        this.backText.SetBlank();
    }

    public void SetEntry(ScoresScrollRect.LoadedEntry entry)
    {
        this.entry = entry;
        this.backText.SetEntry(entry);
    }

    public void Play()
    {
        this.animator.enabled = true;
        this.animator.Play(0);
    }

}
