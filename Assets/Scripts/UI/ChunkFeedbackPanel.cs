using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.IO;
using System.Net;
using System.Net.Mail;

public class ChunkFeedbackPanel : MonoBehaviour
{
    private Chunk chunk;
    public TMPro.TextMeshProUGUI chunkNameText;
    public Dropdown feedbackTypeDropdown;
    public TMPro.TMP_InputField text;
    public TMPro.TextMeshProUGUI statusText;
    public UnityEngine.UI.Button sendButton;
    public Camera screenshotCamera;

    public void Start()
    {
        this.gameObject.SetActive(false);
        this.screenshotCamera.gameObject.SetActive(false);
    }

    public void Open(Chunk chunk)
    {
        this.gameObject.SetActive(true);
        this.sendButton.interactable = true;
        if (this.chunk != chunk)
        {
            this.chunk = chunk;
            this.chunkNameText.text =
                $"Chunk: <color=grey>{chunk.chunkName}</color>";
            this.feedbackTypeDropdown.value = 0;
            this.text.text = "";
            this.statusText.text = "";
        }
    }

    public void OnClickSend()
    {
        IngameUI.instance.StartCoroutine(SendCoroutine());
    }

    private IEnumerator SendCoroutine()
    {
        this.sendButton.interactable = false;

        this.gameObject.SetActive(false);
        Texture2D texture = CaptureScreen();
        this.gameObject.SetActive(true);
        yield return null;

        byte[] data = texture.EncodeToPNG();
        string filename =
            "screenshot " + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".png";
        string fullPathname = Path.Combine(Application.persistentDataPath, filename);
        File.WriteAllBytes(fullPathname, data);

        string from = "ld2.chunk.feedback@gmail.com";
        // string password = "+t%VyUS{st7Z6<jqd2hCpc4C"; // account password
        string password = "gzijixesyyqcqpob"; // app password
        string to = "chrisburtbrown+t30x7mkydwd9qcq0lmxa@boards.trello.com";

        string subject;
        {
            string creator = null;
            var split = this.chunk.chunkName.Split('/');
            if (split.Length == 4)
            {
                creator = split[1] switch
                {
                    "Alvaro" => "@alarts",
                    "Kenny"  => "@heragaa",
                    "Kiavik" => "@kiavik",
                    "Markus" => "@markusheinel",
                    "Mat"    => "@mat182",
                    "Owen"   => "@owenmidgette",
                    _        => null
                };
            }
            string type = this.feedbackTypeDropdown.value switch
            {
                1 => "rejected",
                2 => "difficulty should be 'easy'",
                3 => "difficulty should be 'medium'",
                4 => "difficulty should be 'hard'",
                _ => "issue"
            };

            creator ??= "@mat182";

            subject = $"{creator} {type} {this.chunk.chunkName}";
        }
        string body = this.text.text;

        // Create mail
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(from);
        mail.To.Add(to);
        mail.Subject = subject;
        mail.Body = body;
        Attachment a = new Attachment(fullPathname);
        mail.Attachments.Add(a);

        SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
        smtpServer.Port = 587;
        smtpServer.Credentials =
            new System.Net.NetworkCredential(from, password) as ICredentialsByHost;
        smtpServer.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
            delegate { return true; };

        this.statusText.text = "Sending ...";
        yield return null;

        try
        {
            smtpServer.Send(mail);
            this.statusText.text = "...sent";
            this.gameObject.SetActive(false);
        }
        catch (Exception e)
        {
            this.statusText.text =
                "<color=red>Error:</color>\n" + e.GetBaseException().Message;
            Debug.LogError("Error sending mail: " + e.GetBaseException());
        }

        this.sendButton.interactable = true;
    }

    private Texture2D CaptureScreen()
    {
        this.screenshotCamera.gameObject.SetActive(true);
        this.screenshotCamera.transform.position = Camera.main.transform.position;
		this.screenshotCamera.orthographicSize = Camera.main.orthographicSize;
		this.screenshotCamera.aspect = (float)Screen.width / (float)Screen.height;
		this.screenshotCamera.targetTexture = new RenderTexture(
            1000,
            1000 * Screen.height / Screen.width,
            32,
            UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm
        );

		var oldRenderTexture = RenderTexture.active;
		RenderTexture.active = this.screenshotCamera.targetTexture;

		this.screenshotCamera.Render();

		Texture2D screenShot = new Texture2D(
            RenderTexture.active.width,
            RenderTexture.active.height,
            TextureFormat.RGB24,
            false
        );
		screenShot.ReadPixels(
            new Rect(0, 0, RenderTexture.active.width, RenderTexture.active.height),
            0,
            0
        );
		screenShot.Apply ();

		RenderTexture.active = oldRenderTexture;
		this.screenshotCamera.gameObject.SetActive(false);
		return screenShot;
    }

    public void OnClickCancel()
    {
        this.gameObject.SetActive(false);
    }
}
