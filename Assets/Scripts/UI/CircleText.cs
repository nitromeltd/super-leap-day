﻿using UnityEngine;
using TMPro;
using UnityEngine.Serialization;

[ExecuteInEditMode]
public class CircleText : MonoBehaviour
{
    [FormerlySerializedAsAttribute("m_TextComponent")]
    protected TMP_Text text;

    public bool autoSize;
    public float widthMax;
    [FormerlySerializedAsAttribute("m_radius")]
    public float radius = 10.0f;
    [FormerlySerializedAsAttribute("m_angularOffset")]
    public float angle = -90;

    private float oldRadius = float.MaxValue;
    private float oldAngle = float.MaxValue;

    private bool oldAutoSize;
    private float oldWidthMax;
    private float heightText;

    private float autoSizeRatio = 1f;

    // set text and properties of child
    [HideInInspector] public CircleText child;
    // we need parent bounds because child bounds are smaller, but these are lost when we bend the text
    [HideInInspector] public (float min, float max, float center)? boundsXParent;

    private void Awake()
    {
        this.text = this.gameObject.GetComponent<TMP_Text>();
        this.oldAngle = this.angle;
        this.oldRadius = this.radius;
        if(this.widthMax == 0) this.widthMax = this.text.preferredWidth;
        this.oldWidthMax = this.widthMax;
        this.oldAutoSize = this.autoSize;
    }

    private void OnEnable()
    {
        this.text.OnPreRenderText += UpdateCurve;
        ForceUpdate();
    }

    private void OnDisable()
    {
        this.text.OnPreRenderText -= UpdateCurve;
    }

    private void Update()
    {
        if (Application.isPlaying == false &&
            (this.text.havePropertiesChanged == true || IsParametersChanged() == true))
        {
            ForceUpdate();
        }
    }

    public void ForceUpdate()
    {
        UpdateCurve(this.text.textInfo);
        this.text.ForceMeshUpdate();
    }

    public void UpdateCurve(TMP_TextInfo textInfo)
    {
        if(this.child != null && IsParametersChanged() == true)
            (this.child.radius, this.child.angle, this.child.autoSize, this.child.widthMax) =
                (this.radius, this.angle, this.autoSize, this.widthMax);

        Vector3[] vertices;
        Matrix4x4 matrix;

        int characterCount = textInfo.characterCount;

        // ignore trailing whitespace
        var str = this.text.GetParsedText();
        while(characterCount > 0 && char.IsWhiteSpace(str[characterCount - 1]))
            characterCount--;

        this.heightText = TextHeightNormalized(this.text.font) * this.text.fontSize;

        if(characterCount == 0)
            return;
        if (textInfo.meshInfo[textInfo.characterInfo[0].materialReferenceIndex].vertices == null)
            return;

        // text.bounds is unreliable - reading direct from vertices
        float boundsMinX = textInfo.meshInfo[textInfo.characterInfo[0].materialReferenceIndex].vertices[textInfo.characterInfo[0].vertexIndex + 0].x;
        float boundsMaxX = textInfo.meshInfo[textInfo.characterInfo[characterCount - 1].materialReferenceIndex].vertices[textInfo.characterInfo[characterCount - 1].vertexIndex + 2].x;
        float boundsCenterX = boundsMinX + (boundsMaxX - boundsMinX) / 2;

        // read bounds from parent when text is layered
        if(this.child != null)
            this.child.boundsXParent = (boundsMinX, boundsMaxX, boundsCenterX);
        else if(this.boundsXParent.HasValue == true)
            (boundsMinX, boundsMaxX, boundsCenterX) = (this.boundsXParent.Value.min, this.boundsXParent.Value.max, this.boundsXParent.Value.center);

        float width = boundsMaxX - boundsMinX;

        this.autoSizeRatio = 1f;
        if(this.autoSize == true && width > this.widthMax)
            this.autoSizeRatio = this.widthMax / width;

        for(int i = 0; i < characterCount; i++)
        {
            //skip if it is invisible
            if(textInfo.characterInfo[i].isVisible == false)
                continue;

            int vertexIndex = textInfo.characterInfo[i].vertexIndex;
            int materialIndex = textInfo.characterInfo[i].materialReferenceIndex;
            vertices = textInfo.meshInfo[materialIndex].vertices;

            Vector3 charMidBaselinePos = new Vector2((vertices[vertexIndex + 0].x + vertices[vertexIndex + 2].x) / 2, textInfo.characterInfo[i].baseLine);
            vertices[vertexIndex + 0] += -charMidBaselinePos;
            vertices[vertexIndex + 1] += -charMidBaselinePos;
            vertices[vertexIndex + 2] += -charMidBaselinePos;
            vertices[vertexIndex + 3] += -charMidBaselinePos;
            float xPosNormalized;
            switch(this.text.alignment)
            {
                case TextAlignmentOptions.Right:
                    xPosNormalized = 1f - (boundsMaxX - charMidBaselinePos.x) / (width);
                    break;
                case TextAlignmentOptions.Center:
                    xPosNormalized = 0.5f + (charMidBaselinePos.x - boundsCenterX) / (width);
                    break;
                default:
                    xPosNormalized = (charMidBaselinePos.x - boundsMinX) / (width);
                    break;
            }
            matrix = ComputeTransformationMatrix(xPosNormalized, textInfo, i);
            vertices[vertexIndex + 0] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 0]);
            vertices[vertexIndex + 1] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 1]);
            vertices[vertexIndex + 2] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 2]);
            vertices[vertexIndex + 3] = matrix.MultiplyPoint3x4(vertices[vertexIndex + 3]);
        }
    }

    public bool IsParametersChanged()
    {
        bool value =
            this.radius != this.oldRadius || this.angle != this.oldAngle || this.oldAutoSize != this.autoSize || this.oldWidthMax != this.widthMax;
        (this.oldRadius, this.oldAngle, this.oldAutoSize, this.oldWidthMax) =
            (this.radius, this.angle, this.autoSize, this.widthMax);
        return value;
    }

    protected Matrix4x4 ComputeTransformationMatrix(float xPosNormalized, TMP_TextInfo textInfo, int charIdx)
    {
        // arc degrees is a constant - reverse engineered by calculating the amount of cirumference used
        var arcDegrees = this.text.preferredWidth / (2 * Mathf.PI * this.radius / 360);
        arcDegrees *= this.autoSizeRatio;
        float a;
        switch(this.text.alignment)
        {
            case TextAlignmentOptions.Left:
                a = (xPosNormalized * arcDegrees + this.angle) * Mathf.Deg2Rad;
                break;
            case TextAlignmentOptions.Right:
                a = ((xPosNormalized - 1.0f) * arcDegrees + this.angle) * Mathf.Deg2Rad; break;
            default:
                // centered
                a = ((xPosNormalized - 0.5f) * arcDegrees + this.angle) * Mathf.Deg2Rad; break;
        }
        float x0 = Mathf.Cos(a);
        float y0 = Mathf.Sin(a);

        var lineExtents = textInfo.lineInfo[0].lineExtents;
        float radiusForThisLine = this.radius - lineExtents.max.y * textInfo.characterInfo[charIdx].lineNumber;
        // text is anchored from the bottom, so a negative radius needs to adjust
        if(this.radius < 0)
        {
            radiusForThisLine -= this.heightText;
            radiusForThisLine += (1f - this.autoSizeRatio) * this.heightText * 0.5f;
        }
        else
        {
            radiusForThisLine += (1f - this.autoSizeRatio) * this.heightText * 0.5f;
        }
        Vector2 newMideBaselinePos = new Vector2(x0 * radiusForThisLine, -y0 * radiusForThisLine); //actual new position of the character
        //compute the trasformation matrix: move the points to the just found position, then rotate the character to fit the angle of the curve 
        //(-90 is because the text is already vertical, it is as if it were already rotated 90 degrees)
        return Matrix4x4.TRS(new Vector3(newMideBaselinePos.x, newMideBaselinePos.y, 0), Quaternion.AngleAxis(-Mathf.Atan2(y0, x0) * Mathf.Rad2Deg - 90, Vector3.forward), Vector3.one * this.autoSizeRatio);
    }

    // Return the height of the font from the baseline normalized, minus the ascender (cannot derive this from font asset values)
    public static float TextHeightNormalized(TMPro.TMP_FontAsset font)
    {
        // could derive fron LanguageId, but need to be sure we never have negative radius and mixed font under a LanguageId
        // (eg: numerals look shit in anything but Leap Day SDF)
        return font.name switch
        {
            "Mikhak-Bold SDF" => 0.658f,
            "DroidSansFallback_Chinese SDF" => 0.701f,
            "CP Font SDF" => 0.73f,
            "BMJUA_ttf SDF" => 0.546f,
            "Molot SDF" => 0.69f,
            _ => 0.805f
        };
    }

    // Written for Arabic and its ascender / descender problems
    public static void DisplaceByTextHeight(Object[] list, float lines = 0.5f)
    {
        var circleTexts = new System.Collections.Generic.List<CircleText>();
        foreach(var item in list)
        {
            if(item is GameObject)
            {
                var ct = (item as GameObject).GetComponent<CircleText>();
                if(ct != null) circleTexts.Add(ct);
            }
            else if(item is MonoBehaviour)
            {
                var ct = (item as MonoBehaviour).gameObject.GetComponent<CircleText>();
                if(ct != null) circleTexts.Add(ct);
            }
        }
        foreach(var item in circleTexts)
        {
            var txt = item.GetComponent<TMP_Text>();
            item.radius += txt.fontSize * TextHeightNormalized(txt.font) * lines * Mathf.Sign(item.radius);
        }
    }

    public static void DisplaceByTextHeight(Object obj, float lines = 0.5f)
    {
        DisplaceByTextHeight(new Object[] { obj }, lines);
    }

}
