using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCenterAccessPointSubstitute : MonoBehaviour
{
    public SelectionCursor selectionCursor;

    private UnityEngine.UI.Button button;

    private void Awake()
    {
        this.button = this.GetComponent<UnityEngine.UI.Button>();

        #if UNITY_TVOS
            this.gameObject.SetActive(true);
        #else
            this.gameObject.SetActive(false);
        #endif
    }

    private void LateUpdate()
    {
        #if UNITY_TVOS
            bool isThisButtonSelected = this.selectionCursor.selection == button;
            
            // turn focus on
            if(isThisButtonSelected == true && GameCenter.IsAccessPointFocused() == false)
            {
                GameCenter.FocusAccessPoint(true);
            }

            // turn focus off
            if(isThisButtonSelected == false && GameCenter.IsAccessPointFocused() == true)
            {
                GameCenter.FocusAccessPoint(false);
            }
        #endif
    }
    
    public void OnPressGameCenterAccessPoint()
    {
        GameCenter.TriggerAccessPoint();
    }
}
