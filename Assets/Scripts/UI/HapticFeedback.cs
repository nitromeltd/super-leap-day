using UnityEngine;
using System.Collections;
using System;
#if (UNITY_IOS && !UNITY_EDITOR)
using System.Runtime.InteropServices;
#endif

public class HapticFeedback
{
    public class iOSFeedbackTypeSettings
    {
        public bool SelectionChange = true, ImpactLight = true, ImpactMedium = true, ImpactHeavy = true;
        public bool NotificationSuccess = true, NotificationWarning = true, NotificationFailure = true;

        public bool Notifications
        {
            get
            {
                return NotificationSuccess || NotificationWarning || NotificationFailure;
            }
        }
    }

    /// <summary>
    /// Defines which feedback generators will be used
    /// This prevents the instantiation of feedback generators that are not used.
    /// </summary>
    public static iOSFeedbackTypeSettings usedFeedbackTypes = new iOSFeedbackTypeSettings();

    private static bool feedbackGeneratorsSetUp = false;

    // feedback generators should be initialized first
    public static void Initialize()
    {
        if (!feedbackGeneratorsSetUp)
        {
            for (int i = 0; i < 5; i++)
            {
                if (FeedbackIdSet(i))
                {
                    InstantiateFeedbackGenerator(i);
                }
            }
            feedbackGeneratorsSetUp = true;
        }
    }

    public static void Destroy()
    {
        if (!feedbackGeneratorsSetUp)
            return;

        for (int i = 0; i < 5; i++)
        {
            if (FeedbackIdSet(i))
            {
                ReleaseFeedbackGenerator(i);
            }
        }

        feedbackGeneratorsSetUp = false;
    }

    protected static bool FeedbackIdSet(int id)
    {
        return ((id == 0 && usedFeedbackTypes.SelectionChange)
             || (id == 1 && usedFeedbackTypes.ImpactLight)
             || (id == 2 && usedFeedbackTypes.ImpactMedium)
             || (id == 3 && usedFeedbackTypes.ImpactHeavy)
            || ((id == 4 || id == 5 || id == 6) && usedFeedbackTypes.Notifications));
    }

    // Link to native functions
#if UNITY_IOS && !UNITY_EDITOR
    [DllImport ("__Internal")]
    private static extern void _instantiateFeedbackGenerator(int id);

    [DllImport ("__Internal")]
    private static extern void _prepareFeedbackGenerator(int id);

    [DllImport ("__Internal")]
    private static extern void _triggerFeedbackGenerator(int id, bool advanced);

    [DllImport ("__Internal")]
    private static extern void _releaseFeedbackGenerator(int id);
#else
    // Instantiate placeholders that do nothing for other platforms than iOS
    private static void _instantiateFeedbackGenerator(int id) { }
    private static void _prepareFeedbackGenerator(int id) { }
    private static void _triggerFeedbackGenerator(int id, bool advanced) { }
    private static void _releaseFeedbackGenerator(int id) { }
#endif

    protected static void InstantiateFeedbackGenerator(int id)
    {
        _instantiateFeedbackGenerator(id);
    }

    protected static void PrepareFeedbackGenerator(int id)
    {
        _prepareFeedbackGenerator(id);
    }

    protected static void TriggerFeedbackGenerator(int id, bool advanced)
    {
        _triggerFeedbackGenerator(id, advanced);
    }

    protected static void ReleaseFeedbackGenerator(int id)
    {
        _releaseFeedbackGenerator(id);
    }

    public enum iOSFeedbackType { SelectionChange, ImpactLight, ImpactMedium, ImpactHeavy, Success, Warning, Failure, None };

    public static void Trigger(iOSFeedbackType feedbackType)
    {
        if(CanTrigger() == false)
        {
            return;
        }        
        
#if UNITY_IOS
        if (FeedbackIdSet((int)feedbackType))
            TriggerFeedbackGenerator((int)feedbackType, false);
        else
            Debug.LogError("You cannot trigger a feedback generator without instantiating it first");
#endif
    }

    public static bool IsSupported ()
    {
#if UNITY_IOS
        return (SystemInfo.supportsVibration &&
            SystemInfo.deviceModel.Contains("iPhone") &&
            UnityEngine.iOS.Device.generation >= UnityEngine.iOS.DeviceGeneration.iPhone7);
#else
        return false;
#endif
    }

    private static bool CanTrigger()
    {
#if UNITY_EDITOR
        return false;
#else
        // we can use it if it is enabled AND supported
        return SaveData.IsHapticFeedbackEnabled() && IsSupported(); 
#endif
    }

    public static void DeathVibrate()
    {
        Initialize();
        HapticFeedback.Trigger(HapticFeedback.iOSFeedbackType.ImpactMedium);
    }

    public static void JumpVibrate()
    {
        Initialize();
        HapticFeedback.Trigger(HapticFeedback.iOSFeedbackType.SelectionChange);
    }

    public static void ButtonVibrate()
    {
        Initialize();
        HapticFeedback.Trigger(HapticFeedback.iOSFeedbackType.SelectionChange);
    }
}