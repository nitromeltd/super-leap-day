using UnityEngine;
using UnityEngine.EventSystems;

public class HoldButtonToRepeat : MonoBehaviour,
    IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler
{
    private bool isHolding = false;
    private bool isInside = false;
    private float lastTriggerTime;
    private float nextWaitDuration;

    public void OnPointerDown(PointerEventData eventData)
    {
        this.isHolding = true;
        this.isInside = true;
        this.lastTriggerTime = Time.realtimeSinceStartup;
        this.nextWaitDuration = 0.8f;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        this.isInside = true;
        this.nextWaitDuration = 0.5f;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.isInside = false;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        this.isHolding = false;
    }

    public void Update()
    {
        if (this.isHolding == true &&
            this.isInside == true &&
            Time.realtimeSinceStartup > this.lastTriggerTime + this.nextWaitDuration)
        {
            if (this.TryGetComponent<UnityEngine.UI.Button>(out var button))
                button.onClick.Invoke();

            this.lastTriggerTime += this.nextWaitDuration;
            this.nextWaitDuration = Mathf.Lerp(this.nextWaitDuration, 0.1f, 0.7f);
        }
    }
}
