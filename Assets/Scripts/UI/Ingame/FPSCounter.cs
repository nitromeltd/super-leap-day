using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    private static int framesCounted = 0;
    private static float lastRealtime;
    public static Stopwatch timerAdvance = new();
    public static Stopwatch timerItems   = new();
    public static Stopwatch timerPlayer  = new();

    public Text text;

    public void Advance()
    {
        if (DebugPanel.fpsEnabled == true && this.gameObject.activeInHierarchy == true)
        {
            framesCounted += 1;
            if (framesCounted >= 8)
            {
                framesCounted = 0;

                float elapsed = (Time.realtimeSinceStartup - lastRealtime) / 8;
                lastRealtime = Time.realtimeSinceStartup;

                var ms = (elapsed * 1000).ToString("0.0");
                var fps = Game.instance.Fps;
                this.text.text =
                    $"<size=12>{fps}</size>FPS\n" +
                    $"<size=10>{ms}</size>MS\n" +
                    $"<size=7>ADVANCE</size> <size=8>{timerAdvance.MillisAsString()}</size><size=6>MS</size>\n" +
                    $"<size=7>PLAYER</size> <size=8>{timerPlayer.MillisAsString()}</size><size=6>MS</size>\n" +
                    $"<size=7>ITEMS</size> <size=8>{timerItems.MillisAsString()}</size><size=6>MS</size>";
                this.text.color =
                    (Game.instance.map1.theme == Theme.WindySkies) ?
                    Color.black :
                    Color.white;

                timerAdvance.Reset();
                timerPlayer.Reset();
                timerItems.Reset();
            }
        }
        else if (this.text.text != "")
        {
            this.text.text = "";
        }
    }
}
