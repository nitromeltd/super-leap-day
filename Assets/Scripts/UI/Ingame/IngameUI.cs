
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
using System.Collections.Generic;

public class IngameUI : MonoBehaviour
{
    public static IngameUI instance;

    public Camera UICamera;
    public CanvasScaler canvasScaler;
    public RectTransform safeArea;
    public Image flashImage;
    public MultiplayerConnectionProblemsPanel multiplayerConnectionProblemsPanel;
    public MultiplayerOtherPlayerLeftPanel multiplayerOtherPlayerLeftPanel;

    public IngameUILayout layoutPortrait;
    public IngameUILayout layoutLandscape;
    public IngameUILayout layoutLandscape2P;
    public IngameUILayout layoutLandscape3P;
    public IngameUILayout layoutLandscape4P;
    public PauseMenu pauseMenu;
    public WinMenu winMenu;
    public DebugPanel debugPanel;
    public ChunkFeedbackPanel chunkFeedbackPanel;

    [Header("Power Up Icons")]
    public Sprite starIcon;
    public Sprite petYellowIcon;
    public Sprite petTornadoIcon;
    public Sprite shieldIcon;
    public Sprite wingsIcon;
    public Sprite midasTouchIcon;
    public Sprite swordAndShieldIcon;
    public Sprite baseball;
    public Sprite ink;
    public Sprite[] mines;

    [HideInInspector] public UIFruit uiFruit;

    private float lastRealtime;
    private int framesCounted;
    private Coroutine switchingBetweenLayoutsCoroutine = null;
    private bool waitingForPlayerToFinish = false;
    private Action onMultiplayerFinishCallback;
    private Map mapAhead, mapBehind;

    public bool IsWaitingForPlayerToFinish => this.waitingForPlayerToFinish;

    public AnimationCurve speedCurve;
    public GameObject uiCoinPrefab;
    public Animated.Animation hitAnimation;
    public Animated.Animation smackAnimation;
    public Animated.Animation timerFlashAnimation;

    public GameObject warningSign;

    public IngameUILayout CurrentLayout()
    {
        return (GameCamera.IsLandscape(), Game.instance.maps.Length) switch
        {
            (true, 4)  => this.layoutLandscape4P,
            (true, 3)  => this.layoutLandscape3P,
            (true, 2)  => this.layoutLandscape2P,
            (true, _)  => this.layoutLandscape,
            (false, _) => this.layoutPortrait
        };
    }

    public Image PowerUpImage(Map map) =>
        CurrentLayout().Player(map).powerupBox.slotImage;

    public Vector2 FruitCounterPosition(Map map) =>
        CurrentLayout().Player(map).fruitCounterImageRT.position;

    public float FruitCounterScale(Map map) =>
        CurrentLayout().Player(map).fruitCounterImageRT.parent.transform.localScale.x;

    public RectTransform FruitCounterImageRectTransform(Map map) =>
        CurrentLayout().Player(map).fruitCounterImageRT;

    public RectTransform PowerUpImageRectTransform(Map map) =>
        CurrentLayout().Player(map).powerupBox.slotRT;

    public RectTransform CoinImageRectTransform(Map map) =>
        CurrentLayout().Player(map).coinCounterImageRT;

    public RectTransform TimerCoinImageRectTransform(Map map) =>
        CurrentLayout().Player(map).timerCounter.imageRT;

    public void Init()
    {
        instance = this;
        this.gameObject.SetActive(true);

        this.canvasScaler = this.GetComponent<CanvasScaler>();
        this.uiFruit = this.GetComponent<UIFruit>();

        IngameUILayout useLayout =
            (GameCamera.IsLandscape(), Game.instance.maps.Length) switch
            {
                (true, 4)  => this.layoutLandscape4P,
                (true, 3)  => this.layoutLandscape3P,
                (true, 2)  => this.layoutLandscape2P,
                (true, _)  => this.layoutLandscape,
                (false, _) => this.layoutPortrait
            };

        this.layoutPortrait.gameObject.SetActive(useLayout == this.layoutPortrait);
        this.layoutLandscape.gameObject.SetActive(useLayout == this.layoutLandscape);
        this.layoutLandscape2P.gameObject.SetActive(useLayout == this.layoutLandscape2P);
        this.layoutLandscape3P.gameObject.SetActive(useLayout == this.layoutLandscape3P);
        this.layoutLandscape4P.gameObject.SetActive(useLayout == this.layoutLandscape4P);

        if (GameCamera.IsLandscape())
        {
            this.canvasScaler.referenceResolution = new Vector2(700, 400);
            this.canvasScaler.matchWidthOrHeight = 1;
        }
        else
        {
            this.canvasScaler.referenceResolution = new Vector2(256, 400);
            this.canvasScaler.matchWidthOrHeight = 0;
        }

        this.layoutPortrait.feedbackButton
            .gameObject.SetActive(SaveData.IsDeveloperModeEnabled());

        this.pauseMenu.Init();
        this.winMenu.Init();
        this.debugPanel.Init();
        this.multiplayerConnectionProblemsPanel?.Init();

#if !UNITY_TVOS || UNITY_EDITOR
        this.safeArea.anchorMin = new Vector2(
            Screen.safeArea.min.x / Screen.width,
            Screen.safeArea.min.y / Screen.height
        );
        this.safeArea.anchorMax = new Vector2(
            Screen.safeArea.max.x / Screen.width,
            Screen.safeArea.max.y / Screen.height
        );

#if UNITY_IOS && !UNITY_EDITOR
        if (Screen.height == 2436 ||    // iPhone X, iPhone Xs, iPhone 11 Pro
            Screen.height == 1792 ||    // iPhone 11, iPhone Xr
            Screen.height == 2688 ||    // iPhone 11 Pro Max, iPhone Xs Max
            Screen.height == 2340 ||    // iPhone 12 mini
            Screen.height == 2532 ||    // iPhone 12, iPhone 12 Pro
            Screen.height == 2778)      // iPhone 12 Pro Max
        {
            this.safeArea.anchorMin += new Vector2(0.03f, -0.025f);
            this.safeArea.anchorMax += new Vector2(-0.03f, 0.045f);
        }
#endif
#endif

        this.layoutPortrait.Init();
        this.layoutLandscape.Init();
        this.layoutLandscape2P.Init();
        this.layoutLandscape3P.Init();
        this.layoutLandscape4P.Init();
    }

    public void Advance()
    {
        if(this.UICamera?.orthographicSize != Game.instance.map1.gameCamera.Camera.orthographicSize)
        {
           this.UICamera.orthographicSize = Game.instance.map1.gameCamera.Camera.orthographicSize;
        }

        if (GameInput.DidAnyonePressPause() == true)
            OnClickPause();

        if (this.switchingBetweenLayoutsCoroutine == null)
        {
            IngameUILayout useLayout =
                (GameCamera.IsLandscape(), Game.instance.maps.Length) switch
                {
                    (true, 4)  => this.layoutLandscape4P,
                    (true, 3)  => this.layoutLandscape3P,
                    (true, 2)  => this.layoutLandscape2P,
                    (true, _)  => this.layoutLandscape,
                    (false, _) => this.layoutPortrait
                };

            this.layoutPortrait.gameObject.SetActive(useLayout == this.layoutPortrait);
            this.layoutLandscape.gameObject.SetActive(useLayout == this.layoutLandscape);
            this.layoutLandscape2P.gameObject.SetActive(useLayout == this.layoutLandscape2P);
            this.layoutLandscape3P.gameObject.SetActive(useLayout == this.layoutLandscape3P);
            this.layoutLandscape4P.gameObject.SetActive(useLayout == this.layoutLandscape4P);

            if (GameCamera.IsLandscape())
            {
                this.canvasScaler.referenceResolution = new Vector2(700, 400);
                this.canvasScaler.matchWidthOrHeight = 1;
            }
            else
            {
                this.canvasScaler.referenceResolution = new Vector2(256, 400);
                this.canvasScaler.matchWidthOrHeight = 0;
            }

            if (this.layoutPortrait.gameObject.activeSelf == true)
                this.layoutPortrait.Advance();
            if (this.layoutLandscape.gameObject.activeSelf == true)
                this.layoutLandscape.Advance();
            if (this.layoutLandscape2P.gameObject.activeSelf == true)
                this.layoutLandscape2P.Advance();
            if (this.layoutLandscape3P.gameObject.activeSelf == true)
                this.layoutLandscape3P.Advance();
            if (this.layoutLandscape4P.gameObject.activeSelf == true)
                this.layoutLandscape4P.Advance();
        }
    }

    public void FadeToNewLayout(int newNumberOfPlayers)
    {
        var fromLayout = CurrentLayout();
        var toLayout = newNumberOfPlayers switch
        {
            4 => this.layoutLandscape4P,
            3 => this.layoutLandscape3P,
            2 => this.layoutLandscape2P,
            _ => this.layoutLandscape,
        };

        if (this.switchingBetweenLayoutsCoroutine != null)
        {
            StopCoroutine(this.switchingBetweenLayoutsCoroutine);
        }

        IEnumerator Fade()
        {
            yield return new WaitForSeconds(0.2f);

            if (this.TryGetComponent<CanvasGroup>(out var canvasGroup) == false)
                canvasGroup = this.gameObject.AddComponent<CanvasGroup>();

            while (canvasGroup.alpha > 0)
            {
                canvasGroup.alpha = Util.Slide(canvasGroup.alpha, 0, 0.06f);
                yield return null;
            }

            fromLayout.gameObject.SetActive(false);
            yield return new WaitForSeconds(0.3f);
            toLayout.gameObject.SetActive(true);
            toLayout.Advance();
            toLayout.UpdateControllerIndicators();

            while (canvasGroup.alpha < 1)
            {
                canvasGroup.alpha = Util.Slide(canvasGroup.alpha, 1, 0.06f);
                yield return null;
            }

            Destroy(canvasGroup);
            yield return null;

            this.switchingBetweenLayoutsCoroutine = null;
        }
        this.switchingBetweenLayoutsCoroutine = StartCoroutine(Fade());
    }

    public void OnClickPause()
    {
        if (this.pauseMenu.isOpen == false)
        {
            this.pauseMenu.Open();
        }
    }

    public bool IsGamePaused()
    {
        return
            this.pauseMenu.isOpen == true ||
            (
                this.debugPanel.gameObject.activeSelf == true &&
                this.debugPanel.isExpanded == true
            ) ||
            this.chunkFeedbackPanel.gameObject.activeSelf == true ||
            this.multiplayerConnectionProblemsPanel?.gameObject.activeSelf == true;
    }

    public static bool IsPositionOverUI(Vector2 position)
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = position;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void FlashFadeIn(float flashTime, AnimationCurve flashCurve)
    {
        StartCoroutine(_FlashFade(flashTime, 1f, flashCurve));
    }
    
    public void FlashFadeOut(float flashTime, AnimationCurve flashCurve)
    {
        StartCoroutine(_FlashFade(flashTime, 0f, flashCurve));
    }

    private IEnumerator _FlashFade(
        float targetTime, float targetAlpha, AnimationCurve fadeCurve
    )
    {
        float initialTime = 0f;
        Color initialColor = flashImage.color;
        Color targetColor = new Color(
            flashImage.color.r, flashImage.color.g, flashImage.color.b, targetAlpha
        );

        while(initialTime < targetTime)
        {
            initialTime += Time.deltaTime;
            flashImage.color = Color.Lerp(
                initialColor, targetColor, fadeCurve.Evaluate(initialTime / targetTime)
            );
            yield return null;
        }
    }

    public void FlashFruitCounter(float time, Color color, Map map)
    {
        CurrentLayout().FlashFruitCounter(time, color, map);
    }

    public IEnumerator TintFlashPowerupBox(Map map)
    {
        return CurrentLayout().Player(map).powerupBox.TintFlashPowerupBox();
    }

    public IEnumerator ShakePowerupBox(Map map)
    {
        return CurrentLayout().Player(map).powerupBox.ShakePowerupBox();
    }

    public IEnumerator TintFlashCoinSymbol(Map map)
    {
        return CurrentLayout().TintFlashCoinSymbol(map);
    }

    public IEnumerator ShakeCoinSymbol(Map map)
    {
        return CurrentLayout().ShakeCoinSymbol(map);
    }

    public void ShowTimerCollected(Map map, int value)
    {
        CurrentLayout().Player(map).timerCounter.ShowCollected(value);
    }

    public void ActivateOrRestartGlobalAlertCountdown(Map map)
    {
        CurrentLayout().Player(map).globalAlertCountdown.ActivateOrRestartCounter();
    }

    public RectTransform CreateWarningSign(Vector2 screenPosition, bool top)
    { 
        RectTransform warningSign = Instantiate(
            this.warningSign,
            new Vector3(0, 0, 0),
            Quaternion.identity
        ).GetComponent<RectTransform>();

        var layout = CurrentLayout();

        if (layout == this.layoutPortrait)
        {
            warningSign.transform.localScale = new Vector3(0.5f, 0.5f, 1);
        }

        warningSign.anchorMin = screenPosition;
        warningSign.anchorMax = screenPosition;
        warningSign.transform.SetParent(layout.transform, false);

        RectTransform warningSignArrow;
        if (top == true)
        {
            warningSignArrow = warningSign.transform.Find("Top Arrow").GetComponent<RectTransform>();
        }
        else
        {
            warningSignArrow = warningSign.transform.Find("Bottom Arrow").GetComponent<RectTransform>();
        }
        warningSignArrow.gameObject.SetActive(true);
        StartCoroutine(FlashWarningSign(warningSignArrow));
        return warningSign;
    }

    public IEnumerator FlashWarningSign(RectTransform warningSignArrow)
    {
        yield return new WaitForSeconds(0.6f);
        warningSignArrow?.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        warningSignArrow?.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        warningSignArrow?.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        warningSignArrow?.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.6f);
        warningSignArrow?.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        warningSignArrow?.gameObject.SetActive(true);
    }

    public void HideWarningSign(RectTransform warningSign)
    {
        StartCoroutine(_HideWarningSign(warningSign));
    }

    public IEnumerator _HideWarningSign(RectTransform warningSign)
    {
        if(warningSign.gameObject.activeSelf == true)
        {
            yield return new WaitForSeconds(0.2f);
        }
        warningSign.gameObject.SetActive(false);
    }

    public Vector2 PositionInMapForScreenPosition(RectTransform image)
    {
        Vector2 pos = this.UICamera.ScreenToWorldPoint(image.position);
        return pos;
    }

    public Vector3 PositionForWorldFromUICameraPosition(Map map, Vector3 uiCameraPos)
    {
        // it gives world position related to map from world position related to UI
        var screenPos = this.UICamera.WorldToScreenPoint(uiCameraPos);
        var positionInWorldSpace = map.gameCamera.Camera.ScreenToWorldPoint(screenPos);
        return positionInWorldSpace;
    }

    public Vector3 PositionForUICameraFromWorldPosition(Map map, Vector3 pos)
    {
        // it gives world position related to UI from world position related to map
        var screenPos = map.gameCamera.Camera.WorldToScreenPoint(pos);
        var positionInUICameraSpace = this.UICamera.ScreenToWorldPoint(screenPos);
        return positionInUICameraSpace;
    }

    public Vector3 MoveTransformToUILayer(Map map, Transform target)
    {
        // it moves given transform from world position related to map to world position related to UI + set it's layer to UI
        Util.SetLayerRecursively(target, LayerMask.NameToLayer("UI"), true);
        target.position = PositionForUICameraFromWorldPosition(map, target.position);
        return target.position;
    }
}
