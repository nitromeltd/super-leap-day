using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IngameUILayout : MonoBehaviour
{
    [Serializable]
    public class PlayerSpecificStuff
    {
        public string name;
        public TextMeshProUGUI fruitCounterText;
        public RectTransform fruitCounterImageRT;
        public TextMeshProUGUI coinCounterText;
        public RectTransform coinCounterImageRT;
        public UISilverCoinCounter silverCoins;
        public UIPowerupBox powerupBox;
        public UITimerCounter timerCounter;
        public UIGlobalAlertCountdown globalAlertCountdown;
        public UIControllerIndicator controllerIndicator;
    }

    public UIPauseButton pauseButton;
    public PlayerSpecificStuff[] players;
    public UILevelProgressBar progressBar;
    public UILevelProgressBar progressBarForTrainingStage;
    public GameObject containerForTrainingStageUI;
    public UIThemeAndDate themeAndDate;
    public UIMultiplayerTimer multiplayerTimer;
    public UIFeedbackButton feedbackButton;
    public FPSCounter fpsCounter;

    private bool flashFruitCounterActive = false;
    private Vector2 startAnchoredPositionForProgressBar;
    private Vector2 startAnchoredPositionForThemeAndDate;

    private bool inHorizontal = false;
    private bool changingLayout = false;

    public void Init()
    {
        this.pauseButton.Init();

        if (Game.IsOpeningTutorialSelected == true)
        {
            this.progressBarForTrainingStage.Init();
            this.progressBar.gameObject.SetActive(false);
            this.containerForTrainingStageUI.SetActive(true);
        }
        else
        {
            if (this.progressBarForTrainingStage != null)
                this.progressBarForTrainingStage.gameObject.SetActive(false);

            this.progressBar.Init();
        }

        if (this.themeAndDate != null)
        {
            this.themeAndDate.Init();

            this.startAnchoredPositionForProgressBar =
                this.progressBar.GetComponent<RectTransform>().anchoredPosition;
            this.startAnchoredPositionForThemeAndDate =
                this.themeAndDate.GetComponent<RectTransform>().anchoredPosition;
        }

        for (int n = 0; n < this.players.Length; n += 1)
        {
            var player = this.players[n];

            player.timerCounter.Init();
        }

        if (this.feedbackButton != null)
            this.feedbackButton.Init();

        NotifyUIStyleChanged();
    }

    public void Advance()
    {
        for (int n = 0; n < this.players.Length && n < Game.instance.maps.Length; n += 1)
        {
            var player = this.players[n];
            var map = Game.instance.maps[n];

            UpdateFruitAndCoinCounters(map);
            player.silverCoins.Advance();
            player.powerupBox.Advance();
            player.globalAlertCountdown.Advance();
        }

        this.pauseButton.Advance();

        if (Game.IsOpeningTutorialSelected == true)
            this.progressBarForTrainingStage.Advance();
        else
            this.progressBar.Advance();

        if (this == IngameUI.instance.layoutLandscape)
            CheckAdjustLayoutForHorizontalChunk();

        if (this.multiplayerTimer != null)
            this.multiplayerTimer.Advance();

        this.fpsCounter.Advance();

        if (this.feedbackButton != null)
            this.feedbackButton.Advance();
    }

    public void NotifyUIStyleChanged()
    {
        bool uiVisible = (DebugPanel.uiStyle != DebugPanel.UIStyle.Hidden);

        this.pauseButton.Advance();
        this.progressBar.gameObject.SetActive(
            uiVisible && (Game.IsOpeningTutorialSelected == false)
        );
        this.progressBarForTrainingStage.gameObject.SetActive(
            uiVisible && (Game.IsOpeningTutorialSelected == true)
        );
        if (this.themeAndDate != null)
            this.themeAndDate.gameObject.SetActive(uiVisible);

        foreach (var player in this.players)
        {
            player.powerupBox.Advance();
            player.powerupBox.gameObject.SetActive(uiVisible);
            player.fruitCounterText.gameObject.SetActive(uiVisible);
            player.fruitCounterImageRT.gameObject.SetActive(uiVisible);
            player.coinCounterText.gameObject.SetActive(uiVisible);
            player.coinCounterImageRT.gameObject.SetActive(uiVisible);
            player.silverCoins.gameObject.SetActive(uiVisible);

            if (player.controllerIndicator != null)
                player.controllerIndicator.Refresh();
        }

        if (this.feedbackButton != null)
            this.feedbackButton.Advance();

        if (this == IngameUI.instance.layoutLandscape)
        {
            if (DebugPanel.uiStyle != DebugPanel.UIStyle.Normal)
            {
                this.pauseButton.gameObject.SetActive(false);
                this.players[0].powerupBox.rectTransform.anchoredPosition =
                    new Vector2(-16, -12);
            }
            else if (Application.platform == RuntimePlatform.Switch)
            {
                this.pauseButton.gameObject.SetActive(false);
                this.players[0].powerupBox.rectTransform.anchoredPosition =
                    new Vector2(-25, -12);
            }
            else
            {
                this.pauseButton.gameObject.SetActive(true);
                this.players[0].powerupBox.rectTransform.anchoredPosition =
                    new Vector2(-25, -44);
            }
        }
        else if (
            this == IngameUI.instance.layoutLandscape2P ||
            this == IngameUI.instance.layoutLandscape3P ||
            this == IngameUI.instance.layoutLandscape4P
        )
        {
            this.pauseButton.gameObject.SetActive(
                DebugPanel.uiStyle == DebugPanel.UIStyle.Normal &&
                Application.platform != RuntimePlatform.Switch
            );
        }
        else
        {
            this.pauseButton.gameObject.SetActive(
                DebugPanel.uiStyle != DebugPanel.UIStyle.Hidden
            );
        }
    }

    public PlayerSpecificStuff Player(int playerOneBased) =>
        this.players[playerOneBased - 1];
    public PlayerSpecificStuff Player(Map map) =>
        this.players[map.mapNumber - 1];

    public void UpdateFruitAndCoinCounters(Map map)
    {
        var player = this.players[map.mapNumber - 1];

        var value = map.fruitCurrent.ToString();
        if (player.fruitCounterText.text != value)
            player.fruitCounterText.text = value;

        value = map.coinsCollected.ToString();
        if (player.coinCounterText.text != value)
            player.coinCounterText.text = value;
    }

    public void FlashFruitCounter(float time, Color color, Map map)
    {
        if (this.flashFruitCounterActive == true) return;

        StartCoroutine(_FlashFruitCounter(time, color, map));
    }

    private IEnumerator _FlashFruitCounter(float time, Color flashColor, Map map)
    {
        this.flashFruitCounterActive = true;

        var player = Player(map);
        int loopTimes = Mathf.RoundToInt(time / 0.1f);

        for (int i = 0; i < loopTimes; i++)
        {
            player.fruitCounterText.color = flashColor;
            yield return new WaitForSeconds(0.1f);
            player.fruitCounterText.color = Color.white;
            yield return new WaitForSeconds(0.1f);
        }

        this.flashFruitCounterActive = false;
    }
    
    public System.Collections.IEnumerator TintFlashCoinSymbol(Map map)
    {
        Player(map).coinCounterImageRT.GetComponent<Image>().color = Color.black;
        yield return new WaitForSeconds(0.1f);
        Player(map).coinCounterImageRT.GetComponent<Image>().color = Color.white;
    }

    public System.Collections.IEnumerator ShakeCoinSymbol(Map map)
    {
        var player = Player(map);

        float intensity;
        float shakeDecay = 0.5f;

        if (GameCamera.IsLandscape() == true)
            intensity = 7f;
        else
            intensity = 4f;

        Vector3 originalPosition = player.coinCounterImageRT.anchoredPosition;

        float timer = 0;
        while (timer < 0.3)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                timer += Time.deltaTime;
                player.coinCounterImageRT.anchoredPosition =
                    originalPosition + UnityEngine.Random.insideUnitSphere * intensity;
                intensity -= shakeDecay;
            }
            yield return null;
        }

        player.coinCounterImageRT.anchoredPosition = originalPosition;
    }

    private void CheckAdjustLayoutForHorizontalChunk()
    {
        if (this.gameObject.activeInHierarchy == false)
            return;

        var isChunkHorizontal = Game.instance.map1.gameCamera.currentChunk.isHorizontal;

        if (this.changingLayout == false && this.inHorizontal != isChunkHorizontal)
        {
            this.changingLayout = true;
            StartCoroutine(MoveInHorizontalChunk(isChunkHorizontal));
        }
    }

    private IEnumerator MoveInHorizontalChunk(bool inHorizontal)
    {
        RectTransform progressBar = this.progressBar.GetComponent<RectTransform>();
        RectTransform themeAndDate = this.themeAndDate.GetComponent<RectTransform>();

        var targetLeft =
            this.startAnchoredPositionForProgressBar.Add(inHorizontal ? -165 : 0, 0);
        var targetRight =
            this.startAnchoredPositionForThemeAndDate.Add(inHorizontal ? 200 : 0, 0);
        var ease = Easing.ByAnimationCurve(IngameUI.instance.speedCurve);

        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(progressBar, targetLeft, 0.5f, ease);
        tweener.MoveAnchoredPosition(themeAndDate, targetRight, 0.5f, ease);

        while (tweener.IsDone() == false)
        {
            if (IngameUI.instance.IsGamePaused() == false)
                tweener.Advance(Time.deltaTime);
            
            yield return null;
        }

        this.changingLayout = false;
        this.inHorizontal = inHorizontal;
    }

    public void UpdateControllerIndicators()
    {
        foreach (var player in this.players)
        {
            if (player.controllerIndicator != null)
                player.controllerIndicator.Refresh();
        }
    }
}
