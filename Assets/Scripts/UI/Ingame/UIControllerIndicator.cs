using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIControllerIndicator : MonoBehaviour
{
    public int forPlayerNumber;
    public Image keyboard;
    public Image mouse;
    public Image genericController;
    public Image siriRemote1;
    public Image siriRemote2;
    public Image ps4Controller;
    public Image ps5Controller;
    public Image switchController;
    public Image switchJoycon;
    public Image switchProController;
    public Image xboxController;

    public void Start()
    {
        Refresh();
    }

    public void Refresh()
    {
        var rewiredPlayer = GameInput.Player(this.forPlayerNumber).rewiredPlayer;
        var controller = rewiredPlayer?.controllers.Controllers.FirstOrDefault();
        ShowController(controller);
    }

    public void ShowController(Rewired.Controller controller)
    {
        this.keyboard           .gameObject.SetActive(false);
        this.mouse              .gameObject.SetActive(false);
        this.genericController  .gameObject.SetActive(false);
        this.siriRemote1        .gameObject.SetActive(false);
        this.siriRemote2        .gameObject.SetActive(false);
        this.ps4Controller      .gameObject.SetActive(false);
        this.ps5Controller      .gameObject.SetActive(false);
        this.switchController   .gameObject.SetActive(false);
        this.switchJoycon       .gameObject.SetActive(false);
        this.switchProController.gameObject.SetActive(false);
        this.xboxController     .gameObject.SetActive(false);

        if (controller == null)
            return;
        if (DebugPanel.uiStyle == DebugPanel.UIStyle.Clean)
            return;

        if (controller is Rewired.Keyboard)
            this.keyboard.gameObject.SetActive(true);
        else if (controller is Rewired.Mouse)
            this.mouse.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForPS4)
            this.ps4Controller.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForPS5)
            this.ps5Controller.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForSiriRemote)
            this.siriRemote1.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForSwitchLeft)
            this.switchJoycon.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForSwitchRight)
            this.switchJoycon.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForSwitchDual)
            this.switchController.gameObject.SetActive(true);
        else if (controller.hardwareTypeGuid == GameInput.HardwareGuidForSwitchProController)
            this.switchProController.gameObject.SetActive(true);
        else
            this.genericController.gameObject.SetActive(true);
    }
}
