using UnityEngine;

public class UIFeedbackButton : MonoBehaviour
{
    public void Init()
    {
        Advance();
    }

    public void Advance()
    {
        var shouldBeVisible =
            SaveData.IsDeveloperModeEnabled() &&
            DebugPanel.uiStyle != DebugPanel.UIStyle.Clean &&
            DebugPanel.uiStyle != DebugPanel.UIStyle.Hidden;

        if (shouldBeVisible != this.gameObject.activeSelf)
        {
            this.gameObject.SetActive(shouldBeVisible);
        }
    }

    public void OnClickButton()
    {
        IngameUI.instance.chunkFeedbackPanel.Open(
            Game.instance.map1.NearestChunkTo(Game.instance.map1.player.position)
        );
    }
}
