using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIGlobalAlertCountdown : MonoBehaviour
{
    private const float FadeInOutSpeed = 0.1f;

    public IngameUILayout layout;
    public Animated animatedBackground;
    public Animated animatedOverlay;
    public Image imageBackground;
    public Image imageOverlay;
    public TextMeshProUGUI[] textDigits;

    public Animated.Animation animationAppear;
    public Animated.Animation animationPreDisappear;
    public Animated.Animation animationDisappear;
    public Animated.Animation animationResetCounter;
    public Animated.Animation animationWipe;

    private float targetAlpha = 0;
    private float currentAlpha = 0;
    private bool alertActive = false;
    private int resetDelay = 0;

    public Map TheMap()
    {
        for (int n = 0; n < this.layout.players.Length; n += 1)
        {
            if (this.layout.players[n].globalAlertCountdown == this)
                return Game.instance.maps[n];
        }
        return Game.instance.map1;
    }

    public void ActivateOrRestartCounter()
    {
        if (this.resetDelay > 0)
            return;

        if (this.alertActive == false)
        {
            //activate 
            this.animatedBackground.PlayOnce(
                this.animationAppear, () => { this.targetAlpha = 1; }
            );
            this.alertActive = true;
        }
        else
        {
            //reset counter
            this.animatedBackground.PlayOnce(this.animationResetCounter, () =>
            {
                this.animatedOverlay.PlayOnce(this.animationWipe);
                this.animatedOverlay.OnFrame(10, () =>
                {
                    this.targetAlpha = 1f;
                });
            });
            this.targetAlpha = 0;
            this.currentAlpha = 0;
            AdvanceTransparency();
        }
        this.resetDelay = 120;
    }

    public void Advance()
    {
        var map = TheMap();

        if (this.resetDelay > 0)
            this.resetDelay -= 1;

        if (this.alertActive == false)
        {
            if (map.desertAlertState.IsGloballyActive() == true)
                this.alertActive = true;
        }
        else
        {
            if (map.desertAlertState.IsGloballyActive() == false)
            {
                this.alertActive = false;
                this.animatedBackground.PlayOnce(this.animationPreDisappear);

                if (this.animatedOverlay.currentAnimation == this.animationWipe)
                    this.animatedOverlay.Stop();
                
                this.animatedOverlay.PlayOnce(this.animationWipe, () =>
                {
                    this.animatedBackground.PlayOnce(this.animationDisappear); 
                });
                this.animatedOverlay.OnFrame(5, () => { this.targetAlpha = 0f; });
                
                SetCorrectNumbersColor();                
            }
        }
        AdvanceTransparency();
        AdvanceGlobalAlertText();
    }

    private void AdvanceTransparency()
    {
        this.currentAlpha = Util.Slide(this.currentAlpha, this.targetAlpha, FadeInOutSpeed);

        if (this.textDigits[0].color.a != this.currentAlpha)
            SetCorrectNumbersColor();
    }

    private void SetCorrectNumbersColor()
    {
        var map = TheMap();

        for (int i = 0; i < this.textDigits.Length; i++)
        {
            Color color =
                (map.desertAlertState.IsGloballyActive()) ? Color.red : Color.green;
            color.a = this.currentAlpha;
            this.textDigits[i].color = color;
        }
    }

    private void AdvanceGlobalAlertText()
    {
        var map = TheMap();

        if (map.desertAlertState.IsGloballyActive())
        {
            int secs = Mathf.FloorToInt(
                map.desertAlertState.alertTimerInFrames / 60
            );
            int oneSixties = Mathf.FloorToInt(
                map.desertAlertState.alertTimerInFrames % 60
            );

            var values = new string[4];
            values[0] = Mathf.FloorToInt(secs / 10).ToString("D1");
            values[1] = Mathf.FloorToInt(secs % 10).ToString("D1");
            values[2] = Mathf.FloorToInt(oneSixties / 10).ToString("D1");
            values[3] = Mathf.FloorToInt(oneSixties % 10).ToString("D1");

            for (int i = 0; i < 4; i += 1)
            {
                if (this.textDigits[i].text != values[i])
                    this.textDigits[i].text = values[i];
            }
        }
        else
        {
            for (int i = 0; i < 4; i += 1)
            {
                if (this.textDigits[i].text != "0")
                    this.textDigits[i].text = "0";
            }
        }
    }
}
