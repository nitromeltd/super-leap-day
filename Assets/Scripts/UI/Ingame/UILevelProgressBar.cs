using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;

public class UILevelProgressBar : MonoBehaviour
{
    [Serializable]
    public class MarkersForPlayer
    {
        public UILevelProgressBarBalloon balloonForPlayer;
        public UILevelProgressBarBalloon balloonForCheckpoint;
    }

    public bool isTutorial;
    public bool isHorizontal;
    public RectTransform[] points;
    public TextMeshProUGUI timerText;
    public MarkersForPlayer[] markersForPlayers;
    public Dictionary<GhostPlayer, UILevelProgressBarBalloon> balloonForGhostPlayers = new();

    private Vector2[] correspondingPointsInMap; 

    public void Init()
    {
        var map = Game.instance.map1;

        var firstChunk = map.chunks[0];

        if (this.isTutorial == true)
        {
            var xCenter    = map.chunks[0].CenterX();
            var yMax       = map.chunks.Last().yMax;
            var allItems   = map.AllItems();
            var checkpoint = allItems.OfType<Checkpoint>().FirstOrDefault();
            var trophy     = allItems.OfType<OpeningTutorialTrophy>().FirstOrDefault();

            this.correspondingPointsInMap = new []
            {
                new Vector2(xCenter, 0),
                checkpoint?.position ?? Vector2.zero,
                trophy    ?.position ?? Vector2.zero,
                new Vector2(xCenter, yMax - 15)
            };
        }
        else
        {
            var mapPoints = new List<Vector2>();
            mapPoints.Add(new Vector2(firstChunk.CenterX(), firstChunk.yMin));
            foreach (var chunk in map.chunks)
            {
                if (chunk.checkpointNumber != null)
                    mapPoints.Add(LocationOfCheckpointItem(chunk));
            }
            this.correspondingPointsInMap = mapPoints.ToArray();
        }

        for (int n = 0; n < this.markersForPlayers.Length; n += 1)
        {
            this.markersForPlayers[n].balloonForCheckpoint.Init(this);
            this.markersForPlayers[n].balloonForPlayer.Init(this);
            this.markersForPlayers[n].balloonForPlayer.SetCharacter(
                SaveData.GetSelectedCharacter()
            );
        }
    }

    public void Advance()
    {
        if (this.correspondingPointsInMap == null)
            return;

        void MoveBalloon(
            UILevelProgressBarBalloon balloon, Vector2 targetWorldPosition
        )
        {
            if (balloon?.rectTransform == null) return;

            Vector2 pos = balloon.rectTransform.position;

            if (this.isHorizontal == true)
                pos.x = targetWorldPosition.x;
            else
                pos.y = targetWorldPosition.y;

            balloon.rectTransform.position = new Vector3(
                pos.x,
                pos.y,
                this.transform.position.z
            );
        }

        // players
        for (int n = 0; n < this.markersForPlayers.Length; n += 1)
        {
            if (n >= Game.instance.maps.Length) continue;

            var map = Game.instance.maps[n];
            var player = map.player;
            var target = WorldPositionForMarkerIndicatingMapPosition(player.position);
            MoveBalloon(this.markersForPlayers[n].balloonForPlayer, target);
            this.markersForPlayers[n].balloonForPlayer.SetCharacter(player.Character());

            bool isCheckpointActive = (map.player.GetCheckpointPosition().y > 10f);

            this.markersForPlayers[n].balloonForCheckpoint.gameObject.SetActive(
                isCheckpointActive
            );

            if (isCheckpointActive == true)
            {
                var checkpointPosition = map.player.GetCheckpointPosition();
                var checkpointChunk = map.NearestChunkTo(checkpointPosition);
                target = WorldPositionForMarkerIndicatingCheckpointChunk(checkpointChunk);
                MoveBalloon(this.markersForPlayers[n].balloonForCheckpoint, target);
            }
        }

        // ghost players
        if (Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.GameKit)
        {
            Game.instance.map1.ghostPlayers?.ForEach(gp =>
            {
                if (gp == null) return;

                UILevelProgressBarBalloon balloon;
                if (this.balloonForGhostPlayers.TryGetValue(gp, out balloon) == false)
                {
                    balloon = Instantiate(
                        this.markersForPlayers[0].balloonForPlayer, this.transform
                    );
                    balloon.Init(this, ghostPlayer: gp);
                    balloon.rectTransform.SetAsFirstSibling();
                    this.balloonForGhostPlayers[gp] = balloon;
                }

                balloon.SetCharacter(gp.Character ?? Character.Yolk);

                var target = WorldPositionForMarkerIndicatingMapPosition(
                    gp.transform.position
                );
                MoveBalloon(balloon, target);
            });
        }

        // timer
        var allMapsFinished = true;
        foreach (var map in Game.instance.maps)
        {
            if (map.finished == false)
                allMapsFinished = false;
        }

        if (allMapsFinished == false)
        {
            float levelTimeInSeconds = Game.instance.map1.frameNumber / 60;
            System.TimeSpan duration = System.TimeSpan.FromSeconds(levelTimeInSeconds);
            string timerLevelString = duration.ToString(@"mm\:ss");

            this.timerText.text = timerLevelString;
        }
    }

    private static Vector2 LocationOfCheckpointItem(Chunk chunk)
    {
        var checkpoint = chunk.AnyItem<Checkpoint>();
        if (checkpoint != null) return checkpoint.position;

        var trophy = chunk.AnyItem<Trophy>();
        if (trophy != null) return trophy.position;

        return chunk.Rectangle().center;
    }

    private Vector2 WorldPositionForMarkerIndicatingCheckpointChunk(Chunk checkpointChunk)
    {
        if (this.isTutorial == true)
            return this.points[1].position;

        while (checkpointChunk.checkpointNumber == null && checkpointChunk.index > 0)
        {
            checkpointChunk = Game.instance.map1.chunks[checkpointChunk.index - 1];
        }
        int pointNumber = 16 - (checkpointChunk.checkpointNumber ?? 16);
        pointNumber = Mathf.Clamp(pointNumber, 0, this.points.Length - 1);
        return this.points[pointNumber].position;
    }

    private Vector2 WorldPositionForMarkerIndicatingMapPosition(Vector2 position)
    {
        var bestSqDistance = Mathf.Infinity;
        (int fromIndex, float amountToNext) bestResult = default;

        for (
            int n = 0;
            n < this.points.Length - 1 &&
                n < this.correspondingPointsInMap.Length - 1;
            n += 1
        )
        {
            var a = this.correspondingPointsInMap[n];
            var b = this.correspondingPointsInMap[n + 1];
            var vector = b - a;
            var along = Vector2.Dot(position - a, vector) / vector.sqrMagnitude;

            var nearest = Vector2.Lerp(a, b, along);
            var sqDistance = (position - nearest).sqrMagnitude;
            if (sqDistance < bestSqDistance)
            {
                bestSqDistance = sqDistance;
                bestResult = (fromIndex: n, amountToNext: along);
            }
        }

        var from = this.points[bestResult.fromIndex].position;
        var to   = this.points[bestResult.fromIndex + 1].position;
        return Vector2.Lerp(from, to, bestResult.amountToNext);
    }

    public void NotifyGhostPlayerIsBeingDestroyed(GhostPlayer ghostPlayer)
    {
        if (this.balloonForGhostPlayers.TryGetValue(ghostPlayer, out var balloon) &&
            balloon != null)
        {
            Destroy(balloon.gameObject);
            this.balloonForGhostPlayers.Remove(ghostPlayer);
        }
    }
}
