using UnityEngine;
using System;

public class UILevelProgressBarBalloon : MonoBehaviour
{
    [NonSerialized] public RectTransform rectTransform;
    [NonSerialized] public UILevelProgressBar bar;
    [NonSerialized] public Player trackingPlayer;
    [NonSerialized] public GhostPlayer trackingGhostPlayer;
    [NonSerialized] public Checkpoint trackingCheckpoint;

    private Character? character = null;

    public void Init(
        UILevelProgressBar bar,
        Player player = null,
        GhostPlayer ghostPlayer = null,
        Checkpoint checkpoint = null
    )
    {
        this.rectTransform = GetComponent<RectTransform>();
        this.bar = bar;
        this.trackingPlayer = player;
        this.trackingGhostPlayer = ghostPlayer;
        this.trackingCheckpoint = checkpoint;

        this.transform.SetParent(this.bar.transform, true);
    }

    public void SetCharacter(Character character)
    {
        if (this.character == character)
            return;
        this.character = character;

        var yolk   = this.transform.Find("Progress Bar Player Icon Yolk"  ).gameObject;
        var sprout = this.transform.Find("Progress Bar Player Icon Sprout").gameObject;
        var goop   = this.transform.Find("Progress Bar Player Icon Goop"  ).gameObject;
        var puffer = this.transform.Find("Progress Bar Player Icon Puffer").gameObject;
        var king   = this.transform.Find("Progress Bar Player Icon The X" ).gameObject;
        yolk  .SetActive(character == Character.Yolk);
        sprout.SetActive(character == Character.Sprout);
        goop  .SetActive(character == Character.Goop);
        puffer.SetActive(character == Character.Puffer);
        king  .SetActive(character == Character.King);
    }
}
