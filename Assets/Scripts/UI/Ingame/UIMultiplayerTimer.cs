using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMultiplayerTimer : MonoBehaviour
{
    public Animated timerAnimated;
    public RectTransform timerRectTransform;
    public TextMeshProUGUI timerText;
    public Image timerBlackCircleImage;
    public Image timerWhiteCircleImage;

    [NonSerialized] public int? followingPlayerInMapNumber;
    [NonSerialized] public GhostPlayer followingGhostPlayer;

    public Animated.Animation animationGreenTimer;
    public Animated.Animation animationGoldTimer;
    public Animated.Animation animationRedTimer;
    public Animated.Animation animationRedRingTimer;

    public void ShowOnLastPlayer(int mapNumberOneBased)
    {
        this.followingPlayerInMapNumber = mapNumberOneBased;
    }

    public void ShowOnLastGhostPlayer(GhostPlayer ghostPlayer)
    {
        this.followingGhostPlayer = ghostPlayer;
    }

    public void Hide()
    {
        this.followingPlayerInMapNumber = null;
        this.followingGhostPlayer = null;
    }

    public void Advance()
    {
        if (this.followingGhostPlayer == null && this.followingPlayerInMapNumber == null)
        {
            this.gameObject.SetActive(false);
            return;
        }

        if (Game.instance.multiplayerLastPlayerCutoff.TimeLeftInFrames() < -120)
        {
            this.gameObject.SetActive(false);
            return;
        }

        this.gameObject.SetActive(true);

        Animated.Animation TimerAnimation(int timeLeft)
        {
            if (timeLeft > MultiplayerLastPlayerCutoff.ExtraTimeSeconds)
                return this.animationGreenTimer;
            else if (timeLeft > 5)
                return this.animationGoldTimer;
            else if (timeLeft > 0)
                return this.animationRedTimer;
            else
                return this.animationRedRingTimer;
        }

        if (this.followingGhostPlayer != null)
        {
            var map = Game.instance.map1;
            var ghost = this.followingGhostPlayer;
            Vector2 otherPlayerPosition = this.followingGhostPlayer.transform.position;

            var cameraRect = map.gameCamera.VisibleRectangle().Inflate(-2);
            cameraRect.yMin += 0.8f;
            if (GameCamera.IsLandscape() == false)
            {
                cameraRect.yMin += 2.5f;
                cameraRect.yMax -= 3f;
            }

            Vector2 positionWithinScreenBounds = new Vector2(
                Mathf.Clamp(otherPlayerPosition.x, cameraRect.xMin, cameraRect.xMax),
                Mathf.Clamp(otherPlayerPosition.y, cameraRect.yMin, cameraRect.yMax)
            );

            bool shouldShowTimerOverIndicator =
                (otherPlayerPosition - positionWithinScreenBounds).sqrMagnitude > 3;

            ghost.playerOffscreenIndicator.spriteCircle.enabled =
                shouldShowTimerOverIndicator == false;
            ghost.playerOffscreenIndicator.spriteYolk.enabled =
                shouldShowTimerOverIndicator == false;
            ghost.playerOffscreenIndicator.spritePuffer.enabled =
                shouldShowTimerOverIndicator == false;
            ghost.playerOffscreenIndicator.spriteSprout.enabled =
                shouldShowTimerOverIndicator == false;
            ghost.playerOffscreenIndicator.spriteGoop.enabled =
                shouldShowTimerOverIndicator == false;
            ghost.playerOffscreenIndicator.spriteKing.enabled =
                shouldShowTimerOverIndicator == false;
            
            Vector3 timerPositionOverIndicator =
                ghost.playerOffscreenIndicator.transform.position;
            
            Vector3 timerPositionAbovePlayer =
                IngameUI.instance.PositionForUICameraFromWorldPosition(
                    map, otherPlayerPosition
                );

            Vector2 targetTimerPosition = shouldShowTimerOverIndicator ?
                timerPositionOverIndicator : timerPositionAbovePlayer;

            float extraHeight = Multiplayer.IsMultiplayerSplitScreenGame() ? 3f : 2.95f;

            if (shouldShowTimerOverIndicator == true)
            {
                extraHeight = 0.50f;
            }

            this.transform.position = targetTimerPosition + (Vector2.up * extraHeight);
        }
        else if (this.followingPlayerInMapNumber != null)
        {
            var map = Game.instance.maps[this.followingPlayerInMapNumber.Value - 1];

            Vector2 targetTimerPosition =
                IngameUI.instance.PositionForUICameraFromWorldPosition(
                    map, map.player.position
                );
            
            float extraHeight = Multiplayer.IsMultiplayerSplitScreenGame() ? 3f : 2.95f;
            this.transform.position = targetTimerPosition + (Vector2.up * extraHeight);
        }

        int timeLeftInFrames =
            Game.instance.multiplayerLastPlayerCutoff.TimeLeftInFrames();
        int timeLeftInSeconds = Mathf.RoundToInt(Mathf.Ceil(timeLeftInFrames / 60f));
        if (timeLeftInSeconds < 0)
            timeLeftInSeconds = 0;
        this.timerText.text = timeLeftInSeconds.ToString("D2");

        Animated.Animation targetAnimation = TimerAnimation(timeLeftInSeconds); 

        if (this.timerAnimated.currentAnimation != targetAnimation)
            this.timerAnimated.PlayAndLoop(targetAnimation);
    
        int fillProgress = timeLeftInFrames % 120;
        float fillBlackProgress = Mathf.InverseLerp(60, 120, fillProgress);
        float fillWhiteProgress = Mathf.InverseLerp(0, 60, fillProgress);

        this.timerBlackCircleImage.fillAmount = fillBlackProgress;
        this.timerWhiteCircleImage.fillAmount = fillWhiteProgress;
    }
}
