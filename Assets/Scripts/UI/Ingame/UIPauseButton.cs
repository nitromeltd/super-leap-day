using UnityEngine;
using UnityEngine.UI;

public class UIPauseButton : MonoBehaviour
{
    public Image controlIndicatorAppleTV;
    public Image controlIndicatorKeyboard;
    public bool shouldHideOnSwitch;

    public void Init()
    {
        if (this.shouldHideOnSwitch == true)
        {
            #if UNITY_SWITCH
            this.gameObject.SetActive(false);
            #endif
        }
    }

    public void Advance()
    {
        bool showControlIndicators =
            DebugPanel.uiStyle == DebugPanel.UIStyle.Normal;

        this.controlIndicatorAppleTV?.gameObject.SetActive(
            GameInput.mostRecentInputType == GameInput.InputType.SiriRemote &&
            showControlIndicators == true
        );
        this.controlIndicatorKeyboard?.gameObject.SetActive(
            (
                GameInput.mostRecentInputType == GameInput.InputType.Keyboard ||
                GameInput.mostRecentInputType == GameInput.InputType.Mouse
            ) &&
            showControlIndicators == true
        );
    }
}
