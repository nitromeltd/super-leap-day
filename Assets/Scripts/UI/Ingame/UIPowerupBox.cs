using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIPowerupBox : MonoBehaviour
{
    public IngameUILayout layout;
    public RectTransform rectTransform;

    public Image bgImage;
    public Image slotImage;
    public RectTransform slotRT;

    public Image controlIndicatorAppleTV;
    public Image controlIndicatorXbox;
    public Image controlIndicatorPlaystation;
    public Image controlIndicatorKeyboard;

    public Map TheMap()
    {
        for (int n = 0; n < this.layout.players.Length; n += 1)
        {
            if (this.layout.players[n].powerupBox == this)
                return Game.instance.maps.ElementAtOrDefault(n);
        }
        return Game.instance.map1;
    }

    public void Advance()
    {
        var map = TheMap();

        if (map != null)
        {
            var powerupSprite = map.currentPowerupType switch
            {
                PowerupPickup.Type.Invincibility        => IngameUI.instance.starIcon,
                PowerupPickup.Type.PetYellow            => IngameUI.instance.petYellowIcon,
                PowerupPickup.Type.PetTornado           => IngameUI.instance.petTornadoIcon,
                PowerupPickup.Type.Shield               => IngameUI.instance.shieldIcon,
                PowerupPickup.Type.SwordAndShield       => IngameUI.instance.swordAndShieldIcon,
                PowerupPickup.Type.InfiniteJump         => IngameUI.instance.wingsIcon,
                PowerupPickup.Type.MidasTouch           => IngameUI.instance.midasTouchIcon,
                PowerupPickup.Type.MultiplayerBaseball  => IngameUI.instance.baseball,
                PowerupPickup.Type.MultiplayerInk       => IngameUI.instance.ink,
                PowerupPickup.Type.MultiplayerMinesTap  =>
                    IngameUI.instance.mines[map.player.minesTap.MinesAmount - 1],
                _ => Assets.instance.emptySprite
            };

            bool havePowerup = powerupSprite != Assets.instance.emptySprite;
            float targetScale = havePowerup == true ? 1f : 0f;

            float currentScale = this.slotImage.transform.localScale.x;
            currentScale = Util.Slide(currentScale, targetScale, 0.20f);

            this.slotImage.transform.localScale = Vector3.one * currentScale;
            this.slotImage.sprite = powerupSprite;
        }

        GameInput.InputType? inputType;

        if (Game.instance.maps?.Length > 1)
        {
            var player = GameInput.Player(map).rewiredPlayer;
            var controller = player.controllers.Controllers.FirstOrDefault();
            inputType = GameInput.InputTypeForController(controller);
        }
        else
        {
            inputType = GameInput.mostRecentInputType;
        }

        bool showControlIndicators =
            DebugPanel.uiStyle == DebugPanel.UIStyle.Normal;

        this.controlIndicatorAppleTV.gameObject.SetActive(
            inputType == GameInput.InputType.SiriRemote &&
            showControlIndicators == true
        );
        this.controlIndicatorPlaystation.gameObject.SetActive(
            inputType == GameInput.InputType.GamepadPlaystation &&
            showControlIndicators == true
        );
        this.controlIndicatorXbox.gameObject.SetActive(
            (
                inputType == GameInput.InputType.GamepadXbox ||
                inputType == GameInput.InputType.GamepadMfi
            ) &&
            showControlIndicators == true
        );
        this.controlIndicatorKeyboard.gameObject.SetActive(
            inputType == GameInput.InputType.Keyboard &&
            showControlIndicators == true
        );
    }

    public IEnumerator TintFlashPowerupBox()
    {
        this.slotImage.color = Color.black;
        this.bgImage.color = Color.black;

        yield return new WaitForSeconds(0.1f);

        this.slotImage.color = Color.white;
        this.bgImage.color = Color.white;
    }

    public IEnumerator ShakePowerupBox()
    {
        float intensity;
        float shakeDecay = 0.5f;

        if (GameCamera.IsLandscape() == true)
            intensity = 7f;
        else
            intensity = 4f;

        Vector3 originalPosition = this.rectTransform.anchoredPosition;

        float timer = 0;
        while (timer < 0.3)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                timer += Time.deltaTime;
                this.rectTransform.anchoredPosition =
                    originalPosition + UnityEngine.Random.insideUnitSphere * intensity;
                intensity -= shakeDecay;
            }
            yield return null;
        }

        this.rectTransform.anchoredPosition = originalPosition;
    }

    public void OnClickButton()
    {
        var map = TheMap();

        if (map.currentPowerupType != PowerupPickup.Type.None)
            map.TriggerPowerup();
    }
}
