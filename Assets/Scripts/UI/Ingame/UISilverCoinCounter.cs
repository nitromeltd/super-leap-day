using System.Collections;
using UnityEngine;

public class UISilverCoinCounter : MonoBehaviour
{
    public IngameUILayout layout;
    public RectTransform[] coins;

    private bool isPlayingSilverCoinEffect = false;
    private bool checkCoinEffect = false;

    public bool IsPlayingSilverCoinEffect => this.isPlayingSilverCoinEffect;

    public Map TheMap()
    {
        for (int n = 0; n < this.layout.players.Length; n += 1)
        {
            if (this.layout.players[n].silverCoins == this)
                return Game.instance.maps[n];
        }
        return Game.instance.map1;
    }

    public void Advance()
    {
        if (this.isPlayingSilverCoinEffect == false)
        {
            int amountSilverCoins = TheMap().silverCoinsCollected;
            for (int i = 0; i < this.coins.Length; i += 1)
            {
                bool silverCoinActive = (i < amountSilverCoins);

                this.coins[i].gameObject.SetActive(silverCoinActive);

                float targetScale = silverCoinActive ? 1f : 0f;
                float currentScale = Util.Slide(
                    this.coins[i].localScale.x, targetScale, 0.20f
                );
                this.coins[i].localScale = Vector3.one * currentScale;
            }

            if (this.checkCoinEffect == true)
            {
                this.checkCoinEffect = false;
                TheMap().CheckForSilverCoinEffect();
            }
        }
    }

    public IEnumerator SilverCoinEffect(Map map)
    {
        this.isPlayingSilverCoinEffect = true;

        var player = this.layout.Player(map);
        AnimationCurve speedCurve = IngameUI.instance.speedCurve;

        Vector3 offset;
        float radius;
        if (GameCamera.IsLandscape() == true)
        {
            offset = new Vector3(3, 0, 0);
            radius = 1.6f;
        } 
        else
        {
            offset = new Vector3(2, 0, 0);
            radius = 1.2f;
        }

        Vector2 center = player.coinCounterText.rectTransform.position + offset;
        Vector3[] CoinOriginalPosition = new Vector3[10];
        for (int i = 0; i <= 9; i++)
        {
            CoinOriginalPosition[i] = this.coins[i].localPosition;
            this.coins[i].localScale = new Vector3(1, 1, 1);
            this.coins[i].gameObject.SetActive(true);
        }

        float timer = 0;
        while (timer < 1)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                center = player.coinCounterText.rectTransform.position + offset;
                timer += Time.deltaTime;
                for (int i = 0; i < 10; i++)
                {
                    Vector3 targetPosition = this.coins[i].position;
                    float angle = (i - 1) * Mathf.PI * 2 / 10;
                    targetPosition.x = center.x + Mathf.Cos(angle) * radius;
                    targetPosition.y = center.y + Mathf.Sin(angle) * radius;
                    this.coins[i].position = Vector3.Lerp(
                        this.coins[i].position, targetPosition, speedCurve.Evaluate(timer)
                    );
                }
            }
            
            yield return null;
        }

        timer = 0;
        int frame = 0;

        while (timer <= 1)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                frame++;
                center = player.coinCounterText.rectTransform.position + offset;
                timer += Time.deltaTime;
                for (int i = 0; i < 10; i++)
                {
                    Vector3 targetPosition = this.coins[i].position;
                    float angle = (i - 1) * Mathf.PI * 2 / 10;
                    targetPosition.x = center.x + Mathf.Cos((angle + (frame / 60.0f) * 3)) * radius;
                    targetPosition.y = center.y + Mathf.Sin((angle + (frame / 60.0f) * 3)) * radius;
                    this.coins[i].position = Vector3.MoveTowards(this.coins[i].position, targetPosition, .1f);
                }
            }
            yield return null;
        }

        timer = 0;
        bool hasHit = false;
        Transform particle = null;
        RectTransform coin = null;
        Particle p = null;

        while (timer <= 1f)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                center = player.coinCounterText.rectTransform.position + offset;
                timer += Time.deltaTime;
                frame++;
                if (timer <= .5)
                {
                    for (int i = 0; i < 10; i++)
                    {
                        this.coins[i].position = Vector3.Lerp(
                            this.coins[i].position, center, speedCurve.Evaluate(timer)
                        );
                    }
                }
                if (timer > .4f)
                {
                    if (hasHit == false)
                    {
                        hasHit = true;
                        for (int i = 0; i < 10; i++)
                        {
                            this.coins[i].gameObject.SetActive(false);
                        }

                        coin = Instantiate(
                            IngameUI.instance.uiCoinPrefab,
                            center,
                            Quaternion.identity
                        ).GetComponent<RectTransform>();

                        coin.transform.SetParent(this.layout.transform.parent, false);
                        coin.position = new Vector3(
                            player.coinCounterText.rectTransform.position.x,
                            player.coinCounterImageRT.position.y,
                            -1
                        ) + offset;
                        
                        p = Particle.CreateAndPlayOnce(
                            IngameUI.instance.hitAnimation,
                            IngameUI.instance.PositionForWorldFromUICameraPosition(
                                map, coin.position
                            ),
                            map.player.transform.parent
                        );
                        p.spriteRenderer.sortingLayerName = "Items (Front)";
                        p.spriteRenderer.sortingOrder = 100;
                        particle = p.transform;

                        if (GameCamera.IsLandscape())
                        {
                            coin.transform.localScale = new Vector3(1.75f, 1.75f, 1);
                            p.transform.localScale = new Vector3(1, 1, 1);
                        }
                        else
                        {
                            p.transform.localScale = new Vector3(0.6f, 0.6f, 1);
                        }

                        Audio.instance.PlaySfx(Assets.instance.sfxCoin, options: new Audio.Options(0.9f, false, 0));
                    }
                    else
                    {
                        p.transform.position =
                            IngameUI.instance.PositionForWorldFromUICameraPosition(
                                map, coin.position
                            );
                    }
                }
            }
            yield return null;
        }

        yield return new WaitForSeconds(.1f);
        CreateSparkleParticles(coin, map);
        particle.position =
            IngameUI.instance.PositionForWorldFromUICameraPosition(map, coin.position);

        timer = 0;
        while (timer < 1)
        {
            if (IngameUI.instance.IsGamePaused() == false)
            {
                timer += Time.deltaTime;
                coin.position = Vector3.Lerp(coin.position, player.coinCounterImageRT.position, speedCurve.Evaluate(timer));
                CreateSparkleParticles(coin, map);
                if (Vector3.Distance(coin.position, player.coinCounterImageRT.position) < 0.05)
                {
                    timer = 1;
                    Game.instance.map1.ScreenShakeAtPosition(coin.position, Vector2.one * 0.5f);
                }
                particle.position =
                    IngameUI.instance.PositionForWorldFromUICameraPosition(map, coin.position);
            }
            yield return null;
        }

        this.layout.ShakeCoinSymbol(map);
        Audio.instance.PlaySfx(Assets.instance.sfxCoin);
        Game.instance.map1.coinsCollected += 1;
        coin.gameObject.SetActive(false);

        for (int i = 0; i <= 9; i++)
        {
            this.coins[i].localPosition = CoinOriginalPosition[i];
        }

        this.isPlayingSilverCoinEffect = false;
        this.checkCoinEffect = true;
    }

    private void CreateSparkleParticles(RectTransform rectTransform, Map map)
    {
        float sparkleOffset = 0.5f;

        bool createSparkleParticle = map.frameNumber % 2 == 0;
        if (createSparkleParticle == false)
            return;

        Vector2 pos = IngameUI.instance.PositionForWorldFromUICameraPosition(
            map, rectTransform.position
        );

        var sparkle = Particle.CreateAndPlayOnce(
            Assets.instance.coinSparkleAnimation,
            pos.Add(
                UnityEngine.Random.Range(-sparkleOffset, sparkleOffset),
                UnityEngine.Random.Range(-sparkleOffset, sparkleOffset)
            ),
            map.transform
        );
        sparkle.spriteRenderer.sortingLayerName = "Items (Front)";
        sparkle.spriteRenderer.sortingOrder = 2;
    }
}
