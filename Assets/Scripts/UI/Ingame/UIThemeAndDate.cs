using System;
using System.Collections.Generic;
using UnityEngine;

public class UIThemeAndDate : MonoBehaviour
{
    [Serializable]
    public struct ThemeIcon
    {
        public string name;
        public Theme theme;
        public GameObject icon;
    }

    public LocalizeText dateText;
    public List<ThemeIcon> themeIcons;

    public void Init()
    {
        var date = Game.selectedDate;
        if (Game.IsOpeningTutorialSelected == true)
            date = DateTime.Now.Date;

        this.dateText.SetTextRaw(Localization.GetText
        (
            "DAY_MONTH_YEAR",
            Localization.GetTextForDayWithOrdinal(date),
            Localization.GetTextForMonthName(date).ToUpper(),
            date.Year.ToString()
        ));

        foreach (ThemeIcon themeIcon in themeIcons)
        {
            themeIcon.icon.SetActive(themeIcon.theme == Game.instance.map1.theme);
        }
    }
}
