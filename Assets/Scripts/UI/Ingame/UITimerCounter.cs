using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITimerCounter : MonoBehaviour
{
    public IngameUILayout layout;
    public RectTransform imageRT;
    public Image image;
    public Animated imageAnimated;
    public TextMeshProUGUI text;
    private int showingValue = 0;
    private Vector3 originalScale;

    public Map TheMap()
    {
        for (
            int n = 0;
            n < Game.instance.maps.Length && n < this.layout.players.Length;
            n += 1
        )
        {
            if (this.layout.players[n].timerCounter == this)
                return Game.instance.maps[n];
        }
        return null;
    }

    public void Init()
    {
        this.originalScale = this.transform.localScale;

        var map = TheMap();
        if (map != null && map.timerCollected > 0)
        {
            this.gameObject.SetActive(true);
            this.text.gameObject.SetActive(true);
            this.text.text = "-" + (map.timerCollected).ToString();
            this.showingValue = map.timerCollected;
        }
        else
        {
            this.gameObject.SetActive(false);
            this.showingValue = 0;
        }
    }

    public void ShowCollected(int value)
    {
        if (DebugPanel.uiStyle == DebugPanel.UIStyle.Hidden)
            return;

        this.showingValue += value;

        var map = TheMap() ?? Game.instance.map1;

        this.gameObject.SetActive(true);
        this.text.gameObject.SetActive(true);

        this.imageAnimated.PlayAndHoldLastFrame(IngameUI.instance.timerFlashAnimation);

        Particle p = Particle.CreateAndPlayOnce(
            IngameUI.instance.smackAnimation,
            this.imageRT.position,
            map.player.transform.parent
        );
        p.spriteRenderer.sortingLayerName = "UI";
        p.spriteRenderer.sortingOrder = 100;
        p.transform.localScale = Vector3.one * 1.25f;
        StartCoroutine(TimerCoroutine());
    }

    public IEnumerator TimerCoroutine()
    {
        this.transform.localScale = this.originalScale * 1.1f;
        this.image.sprite = IngameUI.instance.timerFlashAnimation.sprites[0];

        this.gameObject.SetActive(true);
        this.text.text = "-" + this.showingValue.ToString();

        yield return new WaitForSeconds(0.5f);

        this.transform.localScale = this.originalScale;
        this.image.sprite = IngameUI.instance.timerFlashAnimation.sprites[1];

        yield return new WaitForSeconds(0.5f);
    }
}
