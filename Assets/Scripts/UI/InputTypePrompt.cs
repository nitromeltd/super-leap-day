using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputTypePrompt : MonoBehaviour
{
    [System.NonSerialized] public bool isHidden;
    [System.NonSerialized] public float secondsIdle;
    public CanvasGroup canvasGroup;

    public GameObject iPhonePrompt;
    public GameObject iPadPrompt;
    public GameObject OSXPrompt;
    public GameObject TVOSPrompt;

    public Animated.Animation animationController;
    public Animated.Animation animationControllerShadow;
    public Animated.Animation animationKeyboard;
    public Animated.Animation animationKeyboardShadow;
    public Animated.Animation animationRemote;
    public Animated.Animation animationRemoteShadow;
    public Animated.Animation animationTouch;
    public Animated.Animation animationTouchShadow;

    protected GameObject prompt;
    protected GameInput.InputType inputType;
    protected Animated animated;
    protected Animated animatedShadow;


    private void Awake()
    {
        Init();
    }

    protected virtual void Init()
    {
        this.isHidden = true;
        this.canvasGroup.alpha = 0;
        this.inputType = GameInput.mostRecentInputType;
        this.secondsIdle = 0f;

        this.iPadPrompt.SetActive(false);
        this.iPhonePrompt.SetActive(false);
        this.OSXPrompt.SetActive(false);
        this.TVOSPrompt.SetActive(false);
#if UNITY_TVOS
            this.prompt = this.TVOSPrompt;
#elif (UNITY_STANDALONE_OSX || UNITY_WIN)
            this.prompt = this.OSXPrompt;
#else
        if((float)Screen.width / Screen.height > 0.69f)
            this.prompt = this.iPadPrompt;
        else
            this.prompt = this.iPhonePrompt;
#endif
        this.prompt.SetActive(true);
        var animateds = this.prompt.GetComponentsInChildren<Animated>();
        this.animatedShadow = animateds[0];
        this.animated = animateds[1];
    }

    void Update()
    {
        this.secondsIdle += Time.deltaTime;

        if(this.inputType != GameInput.mostRecentInputType)
            UpdateAnimations();

        if (GameInput.pressedConfirm == true ||
            Input.GetMouseButtonDown(0) == true)
        {
            this.isHidden = true;
            this.secondsIdle = 0f;
        }
        if(this.isHidden == false)
        {
            if(this.canvasGroup.alpha < 1f) this.canvasGroup.alpha += 0.1f;
        }
        else
        {
            if(this.canvasGroup.alpha > 0f) this.canvasGroup.alpha -= 0.1f;
        }
    }

    public void Show()
    {
        UpdateAnimations();
        this.isHidden = false;
        this.canvasGroup.alpha = 0;
    }

    protected virtual void UpdateAnimations()
    {
        (Animated.Animation anim, Animated.Animation shadow) anims = GameInput.mostRecentInputType switch
        {
            GameInput.InputType.GamepadXbox => (animationController, animationControllerShadow),
            GameInput.InputType.GamepadPlaystation => (animationController, animationControllerShadow),
            GameInput.InputType.GamepadMfi => (animationController, animationControllerShadow),
            GameInput.InputType.TouchScreen => (animationTouch, animationTouchShadow),
            GameInput.InputType.SiriRemote => (animationRemote, animationRemoteShadow),
            _ => (animationKeyboard, animationKeyboardShadow)
        };
        animated.PlayAndLoop(anims.anim);
        animatedShadow.PlayAndLoop(anims.shadow);
    }
}
