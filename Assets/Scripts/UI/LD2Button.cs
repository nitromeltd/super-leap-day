﻿using System.Collections.Generic;
using UnityEngine;

public class LD2Button : MonoBehaviour
{
    public static List<LD2Button> ld2Buttons = new List<LD2Button>();
    
    public bool haptic;

    private UnityEngine.UI.Button button;

    private void Awake()
    {
        this.button ??= this.GetComponent<UnityEngine.UI.Button>();

        if (this.haptic == true)
        {
            this.button.onClick.AddListener(() =>
            {
                HapticFeedback.ButtonVibrate();
            });
        }
    }

    private void OnEnable()
    {
        ld2Buttons.Add(this);
    }

    private void OnDisable()
    {
        ld2Buttons.Remove(this);
    }

    public static void DisableAllInteraction()
    {
        foreach (var button in ld2Buttons)
        {
            button.button.interactable = false;
        }
    }
}
