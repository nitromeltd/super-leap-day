using UnityEngine;
using UnityEngine.UI;

public class LD2List : MonoBehaviour
{
    public RectTransform content;
    public RectTransform viewport;

    public void ScrollToListItem(Selectable item)
    {
        var itemRT = item.GetComponent<RectTransform>();

        float rowRelativeY = itemRT.anchoredPosition.y + this.content.anchoredPosition.y;

        if (rowRelativeY > -itemRT.rect.height)
        {
            float toY = -itemRT.anchoredPosition.y - itemRT.rect.height;
            this.content.anchoredPosition = new Vector2(0, toY);
        }
        else if (rowRelativeY < -this.viewport.rect.height)
        {
            float toY = -itemRT.anchoredPosition.y - this.viewport.rect.height;
            this.content.anchoredPosition = new Vector2(0, toY);
        }
    }
}
