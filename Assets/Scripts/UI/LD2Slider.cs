using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LD2Slider : MonoBehaviour
{
    public UnityEvent onConfirm;
    public Slider slider;

    private void Awake()
    {
        this.slider = this.GetComponent<Slider>();
    }
}
