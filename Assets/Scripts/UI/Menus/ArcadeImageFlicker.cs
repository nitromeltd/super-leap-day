using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeImageFlicker : MonoBehaviour
{
    public float wavelength;
    private Image image;
    private float offset;

    void Start()
    {
        this.image = GetComponent<Image>();
        this.offset = Random.value * this.wavelength;
    }

    void Update()
    {
        this.image.color =
            new Color(1, 1, 1, 0.5f + minigame.Shake.Wave(Time.timeSinceLevelLoad + offset, wavelength, 0.5f));
    }
}
