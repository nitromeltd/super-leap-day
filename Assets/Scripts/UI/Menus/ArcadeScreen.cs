using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using Type = minigame.Minigame.Type;
using ShopItem = SaveData.ShopItem;

public class ArcadeScreen : MonoBehaviour
{
    public static int pageNumberSelected;

    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    public static bool enterThroughTrophyDoor = false;

    [Serializable]
    public class Variant
    {
        public int referenceWidth;
        public RectTransform container;
        public PlayerForMenu player;
        public KingHatForMenu hat;
        public RectTransform scrollContainer;
        public RectTransform wallpaper;
        public UnityEngine.UI.Button clickOutside;
        public RectTransform leftButton;
        public RectTransform rightButton;
        public ShopScreenVideoDropdown videoDropdown;
        public TMPro.TMP_Text totalCoinsText;
        public Image dropdownGradient;
        [NonSerialized] public Page[] pages;
    }

    public Variant variantLandscape;
    public Variant variantPortraitTall;
    public Variant variantPortraitShort;
    [NonSerialized] public float currentPage;

    public Sprite spriteGolfCabinet;
    public Sprite spritePinballCabinet;
    public Sprite spriteRacingCabinet;
    public Sprite spriteFishingCabinet;
    public Sprite spriteRollingCabinet;
    public Sprite spriteAbseilingCabinet;
    public Sprite spriteSitYolk;
    public Sprite spriteSitPuffer;
    public Sprite spriteSitGoop;
    public Sprite spriteSitSprout;
    public Sprite spriteSitKing;
    public Sprite[] spritesGolfTrophy;
    public Sprite[] spritesPinballTrophy;
    public Sprite[] spritesRacingTrophy;
    public Sprite[] spritesFishingTrophy;
    public Sprite[] spritesRollingTrophy;
    public Sprite[] spritesAbseilingTrophy;
    public Sprite[] spritesMedal;
    public Sprite spriteDoorOpen;
    public Sprite spriteDoorClosed;
    public Sprite spriteBlank;
    public Animated.Animation animationJumpDust;
    public Animated.Animation animationJumpPuff;
    public Animated.Animation animationButtonFlash;

    public class Page
    {
        // Game page
        public int pageNumber;
        public Type? type;
        public bool isPurchased;
        public RectTransform pageContainer;
        public UnityEngine.UI.Button buttonCabinet;
        public Animated animatedButtonFlash;

        // Goto Achievements page
        public Image door;
        public RectTransform maskForPlayer;
    }

    public AudioClip music;

    public enum SnapDirection { Neutral, Up, Down }

    private bool isDragging = false;
    private Vector2 lastMousePosition;
    private Coroutine currentScrollCoroutine;
    private Coroutine playerCoroutine;
    private Queue<Action> queueClickArrow;
    private float scrollSpeed;
    private bool isGoingToMinigame = false;
    private Page minigamePage = null;
    private float offsetDropdownY;


    private IEnumerator Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        var types = new[] {
            Type.Golf,
            Type.Pinball,
            Type.Racing,
            Type.Fishing,
            Type.Rolling,
            Type.Abseiling
        };
        string totalCoinsStr = SaveData.GetCoins().ToString();

        void SetupVariant(Variant v)
        {
            v.container.gameObject.SetActive(true);// LocalizeText won't call Awake() if inactive

            var pageOriginal = v.scrollContainer.Find("Page") as RectTransform;
            var pages = new List<Page>();

            for(int i = 0; i < types.Length + 1; i++)
            {
                Type? type = null;
                if(i < types.Length) type = types[i];
                var container = Instantiate(pageOriginal, v.scrollContainer);
                var page = new Page
                {
                    type = type,
                    pageNumber = i,
                    isPurchased = type.HasValue && SaveData.IsMinigamePurchased(type.Value),
                    pageContainer = container
                };
                pages.Add(page);
            }
            Destroy(pageOriginal.gameObject);

            v.pages = pages.ToArray();

            for(int n = 0; n < v.pages.Length; n += 1)
            {
                var page = v.pages[n];
                var container = page.pageContainer;
                container.gameObject.name = $"Page {page.type}";
                container.SetSiblingIndex(n);
                container.anchoredPosition = new Vector2(n * v.referenceWidth, 0);
                SetupPage(page);
            }

            v.leftButton.gameObject.SetActive(types.Length > 1);
            v.rightButton.gameObject.SetActive(types.Length > 1);

            v.clickOutside.gameObject.SetActive(false);
            v.clickOutside.image.color = Color.clear;

            if(SaveData.GetSelectedCharacter() == Character.Puffer)
                v.hat.transform.SetAsLastSibling();

            v.totalCoinsText.text = totalCoinsStr;
        }

        void SetupPage(Page page)
        {
            var name = page.type switch
            {
                Type.Golf       => "Golf Day",
                Type.Pinball    => "Pinball Day",
                Type.Racing     => "Racing Day",
                Type.Fishing    => "Fishing Day",
                Type.Rolling    => "Rolling Day",
                Type.Abseiling  => "Abseiling Day",
                _ => "???"
            };
            if(page.isPurchased == false)
                name = "???";
            var container = page.pageContainer;
            var trophyRoomDoor = container.Find("Trophy Room Door");
            var gameButton = container.Find("Select Game Button").GetComponent<UnityEngine.UI.Button>();
            var isDoor = page.type == null;

            container.Find("Bg Cabinets 1").gameObject.SetActive(page.pageNumber % 2 == 0 && isDoor == false);
            container.Find("Bg Cabinets 2").gameObject.SetActive(page.pageNumber % 2 == 1 && isDoor == false);
            container.Find("Bg Machines 1").gameObject.SetActive(page.pageNumber % 2 == 0);
            container.Find("Bg Machines 2").gameObject.SetActive(page.pageNumber % 2 == 1);

            container.Find("Locked").gameObject.SetActive(page.isPurchased == false && isDoor == false);
            container.Find("Cabinet").gameObject.SetActive(page.isPurchased == true);
            container.Find("Jump Dust").GetComponent<Image>().sprite = this.spriteBlank;
            for(int i = 0; i <= 4; i++)
                container.Find($"Jump Dust/Puff {i}").GetComponent<Image>().sprite = this.spriteBlank;

            if(isDoor == false)
            {
                Destroy(trophyRoomDoor.gameObject);

                container.Find("Hanging Sign/Game Name").GetComponent<LocalizeText>().SetText(name);
                var awards = container.Find("Hanging Sign/Awards");
                awards.gameObject.SetActive(page.isPurchased == true);
                page.buttonCabinet = gameButton;

                //if(true)
                if(page.isPurchased == true)
                {
                    container.Find("Cabinet").GetComponent<Image>().sprite = page.type.Value switch
                    {
                        Type.Golf => this.spriteGolfCabinet,
                        Type.Pinball => this.spritePinballCabinet,
                        Type.Racing => this.spriteRacingCabinet,
                        Type.Fishing => this.spriteFishingCabinet,
                        Type.Rolling => this.spriteRollingCabinet,
                        Type.Abseiling => this.spriteAbseilingCabinet,
                        _ => null
                    };
                    minigame.Levels.GetTrophies(page.type.Value, out int? trophy, out int medals);
                    awards.Find("Trophy").gameObject.SetActive(trophy.HasValue == true);
                    if(trophy.HasValue == true)
                        awards.Find("Trophy").GetComponent<Image>().sprite = page.type.Value switch
                        {
                            Type.Golf => this.spritesGolfTrophy[trophy.Value],
                            Type.Pinball => this.spritesPinballTrophy[trophy.Value],
                            Type.Racing => this.spritesRacingTrophy[trophy.Value],
                            Type.Fishing => this.spritesFishingTrophy[trophy.Value],
                            Type.Rolling => this.spritesRollingTrophy[trophy.Value],
                            Type.Abseiling => this.spritesAbseilingTrophy[trophy.Value],
                            _ => null
                        };
                    var medalType = trophy.HasValue ? trophy.Value + 1 : 0;
                    var centered = new List<RectTransform>();
                    for(int i = 1; i <= 4; i++)
                    {
                        var medal = awards.Find($"Medal {i}");
                        medal.gameObject.SetActive(i <= medals);
                        if(medals > 0)
                            medal.GetComponent<Image>().sprite = this.spritesMedal[medalType];
                        if(i <= medals) centered.Add(medal as RectTransform);
                    }
                    for(int i = 0; i < centered.Count; i++)
                    {
                        var t = centered[i];
                        t.anchoredPosition +=
                            t.sizeDelta.x * Vector2.left * (centered.Count - 1) * 0.5f +
                            t.sizeDelta.x * Vector2.right * i;
                    }
                    gameButton.onClick.AddListener(() => OnClickGameCabinet(page));
                    container.Find("Cabinet/Player Sit").gameObject.SetActive(false);
                    container.Find("Cabinet/Player Sit").
                        GetComponent<Image>().sprite = SaveData.GetSelectedCharacter() switch
                        {
                            Character.Yolk => this.spriteSitYolk,
                            Character.Goop => this.spriteSitGoop,
                            Character.Sprout => this.spriteSitSprout,
                            Character.Puffer => this.spriteSitPuffer,
                            Character.King => this.spriteSitKing,
                            _ => null
                        };
                    page.animatedButtonFlash = container.Find("Cabinet/Button Flash").GetComponent<Animated>();
                    page.animatedButtonFlash.PlayAndLoop(this.animationButtonFlash);
                }
                else
                {
                    gameButton.interactable = false;
                }
            }
            else
            {
                container.Find("Hanging Sign").gameObject.SetActive(false);
                Destroy(gameButton.gameObject);
                page.door = trophyRoomDoor.GetComponent<Image>();
                page.maskForPlayer =
                    trophyRoomDoor.Find("Mask For Player").GetComponent<RectTransform>();
            }
        }

        SetupVariant(this.variantLandscape);
        SetupVariant(this.variantPortraitShort);
        SetupVariant(this.variantPortraitTall);

        var activeVariant = ActiveVariant();

        if(enterThroughTrophyDoor == true)
            pageNumberSelected = activeVariant.pages.Length - 1;
        else
        {
            while(pageNumberSelected < 0) pageNumberSelected += activeVariant.pages.Length;
            pageNumberSelected %= activeVariant.pages.Length;
        }

        {
            this.currentPage = pageNumberSelected;

            void MovePlayerToStartingDoor(Variant v)
            {
                v.player.rectTransform.anchoredPosition = new Vector2(
                    PlayerXForPage(v, (int)this.currentPage), 0
                );
            }
            MovePlayerToStartingDoor(this.variantLandscape);
            MovePlayerToStartingDoor(this.variantPortraitShort);
            MovePlayerToStartingDoor(this.variantPortraitTall);
        }


        Canvas.ForceUpdateCanvases();
        this.offsetDropdownY = (float)Screen.height / Screen.width * ActiveVariant().referenceWidth;
        var offset = new Vector2(0, this.offsetDropdownY);
        activeVariant.videoDropdown.container.anchoredPosition += offset;
        activeVariant.dropdownGradient.color = new Color(1, 1, 1, 0);

        this.queueClickArrow = new Queue<Action>();
        this.scrollSpeed = 1f;

        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));

        if(enterThroughTrophyDoor == true)
        {
            this.playerCoroutine = StartCoroutine(ExitDoorCoroutine());
        }

        // Unity Canvas is feeding SelectionCursor the wrong values on Start()
        yield return null;

        if(this.currentPage < activeVariant.pages.Length - 1)
            this.selectionCursor.Select(activeVariant.pages[(int)this.currentPage].buttonCabinet, false);
        else
            this.selectionCursor.Select(activeVariant.pages[(int)this.currentPage].
                door.transform.Find("Door Button").GetComponent<UnityEngine.UI.Button>(), false);
    }

    public Variant ActiveVariant()
    {
        if(GameCamera.IsLandscape() == true)
            return this.variantLandscape;
        else if((float)Screen.height / (float)Screen.width > 1.9f)
            return this.variantPortraitTall;
        else
            return this.variantPortraitShort;
    }

    public void OnClickLeft()
    {
        if(this.currentScrollCoroutine != null)
        {
            if(this.queueClickArrow.Count > 0 && this.queueClickArrow.Peek() == OnClickRight)
                this.queueClickArrow.Clear();
            this.queueClickArrow.Enqueue(OnClickLeft);
            this.scrollSpeed = Mathf.Min(this.scrollSpeed + 0.5f, 2f);
            return;
        }

        var variant = ActiveVariant();
        int next = Mathf.FloorToInt(this.currentPage - 0.2f);
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(next));

        variant.player.WalkToX(PlayerXForPage(variant, next));
    }

    public void OnClickRight()
    {
        if(this.currentScrollCoroutine != null)
        {
            if(this.queueClickArrow.Count > 0 && this.queueClickArrow.Peek() == OnClickLeft)
                this.queueClickArrow.Clear();
            this.queueClickArrow.Enqueue(OnClickRight);
            this.scrollSpeed = Mathf.Min(this.scrollSpeed + 0.5f, 2f);
            return;
        }

        var variant = ActiveVariant();
        int next = Mathf.CeilToInt(this.currentPage + 0.2f);
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(next));

        variant.player.WalkToX(PlayerXForPage(variant, next));
    }

    public void OnClickExit()
    {
        MainMenuScreen.enterSceneThroughDoor = MainMenuScreen.DoorType.Arcade;
        Transition.GoSimple("Main Menu");
        var player = ActiveVariant().player;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIScreenwipeIn, options: new Audio.Options(1f, false, 0f));
    }

    public void OnClickGameCabinet(Page page)
    {
        this.minigamePage = page;
        this.playerCoroutine = StartCoroutine(SitAtGameCabinetCoroutine());
    }
    private IEnumerator SitAtGameCabinetCoroutine()
    {
        var variant = ActiveVariant();
        var coins = SaveData.GetCoins();
        var cost = minigame.RetryMenu.CoinCost;

        variant.leftButton.gameObject.SetActive(false);
        variant.rightButton.gameObject.SetActive(false);
        this.minigamePage.buttonCabinet.interactable = false;
        this.selectionCursor.gameObject.SetActive(false);
        this.minigamePage.animatedButtonFlash.Stop();
        this.minigamePage.animatedButtonFlash.GetComponent<Image>().sprite = this.spriteBlank;

        var shopItem = this.minigamePage.type.Value switch
        {
            Type.Golf       => ShopItem.MinigameGolfBeginner,
            Type.Pinball    => ShopItem.MinigamePinballBeginner,
            Type.Racing     => ShopItem.MinigameRacingBeginner,
            Type.Fishing    => ShopItem.MinigameFishingBeginner,
            Type.Rolling    => ShopItem.MinigameRollingBeginner,
            Type.Abseiling  => ShopItem.MinigameAbseilingBeginner,
            _ => (ShopItem)0
        };
        var gameName = this.minigamePage.type.Value switch
        {
            Type.Golf       => "Golf Day",
            Type.Pinball    => "Pinball Day",
            Type.Racing     => "Racing Day",
            Type.Fishing    => "Fishing Day",
            Type.Rolling    => "Rolling Day",
            Type.Abseiling  => "Abseiling Day",
            _ => ""
        };
        variant.videoDropdown.Init(shopItem, gameName, cost);
        var playButton = variant.videoDropdown.container.Find("Play Button").GetComponent<UnityEngine.UI.Button>();
        var cancelButton = variant.videoDropdown.container.Find("Cancel Button").GetComponent<UnityEngine.UI.Button>();
        playButton.interactable = cost <= coins;

        var player = variant.player;
        var hat = variant.hat;

        player.CancelWalkAndStopHere();
        player.rectTransform.SetParent(variant.scrollContainer, true);
        var container = this.minigamePage.pageContainer;

        yield return player.WalkToXCoroutine(container.anchoredPosition.x - 100);

        container.Find("Jump Dust").GetComponent<Animated>().PlayOnce(animationJumpDust);
        for(int i = 0; i <= 4; i++)
            container.Find($"Jump Dust/Puff {i}").GetComponent<Animated>().PlayOnce(animationJumpPuff);
        Audio.instance.PlaySfx(sfxForMenu.sfxPlayerJump);

        player.FaceRight();
        yield return player.JumpToCoroutine(new Vector2(container.anchoredPosition.x + 100, 350));

        player.gameObject.SetActive(false);
        hat.gameObject.SetActive(false);

        container.Find("Cabinet/Player Sit").gameObject.SetActive(true);
        Audio.instance.PlaySfx(this.sfxForMenu.sfxPlayerGrab);

        yield return new WaitForSeconds(0.25f);

        yield return ShowDropdown(variant.videoDropdown.container, true);

        this.selectionCursor.ConstrainMovement(variant.videoDropdown.gameObject);
        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.Select(cost <= coins ? playButton : cancelButton);
        variant.clickOutside.gameObject.SetActive(true);
    }

    private IEnumerator ShowDropdown(RectTransform container, bool value)
    {
        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(
            container,
            container.anchoredPosition + this.offsetDropdownY * (value ? Vector2.down : Vector2.up)
            , 0.25f, Easing.QuadEaseOut);
        tweener.Alpha(ActiveVariant().dropdownGradient, value ? 1f : 0f, 0.25f, Easing.QuadEaseInOut);

        yield return tweener.Run();
    }

    private IEnumerator GetOffGameCabinetCoroutine()
    {
        ActiveVariant().clickOutside.gameObject.SetActive(false);

        this.selectionCursor.gameObject.SetActive(false);

        yield return ShowDropdown(ActiveVariant().videoDropdown.container, false);

        var player = ActiveVariant().player;
        var hat = ActiveVariant().hat;

        var container = this.minigamePage.pageContainer;

        container.Find("Cabinet/Player Sit").gameObject.SetActive(false);
        player.gameObject.SetActive(true);
        player.FaceLeft();
        hat.gameObject.SetActive(hat.headAnimations != null);

        yield return player.JumpToCoroutine(new Vector2(container.anchoredPosition.x - 100, 0));

        player.Idle();
        Audio.instance.PlaySfx(this.sfxForMenu.sfxPlayerFootstep);

        this.playerCoroutine = null;
        this.minigamePage.buttonCabinet.interactable = true;
        this.minigamePage.animatedButtonFlash.PlayAndLoop(this.animationButtonFlash);
        this.selectionCursor.RemoveConstraint();
        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.Select(this.minigamePage.buttonCabinet);
        ActiveVariant().leftButton.gameObject.SetActive(true);
        ActiveVariant().rightButton.gameObject.SetActive(true);
    }

    public void OnClickPlayMinigame()
    {
        if(this.isGoingToMinigame == true)
            return;

        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));

        this.isGoingToMinigame = true;

        SaveData.SetCoins(SaveData.GetCoins() - minigame.RetryMenu.CoinCost);

        Transition.GoToMinigame(this.minigamePage.type.Value, minigame.Minigame.Mode.Arcade);
    }

    public void OnClickCloseDropdown()
    {
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));
        StartCoroutine(GetOffGameCabinetCoroutine());
    }

    public void OnClickTrophyDoor()
    {
        if(this.playerCoroutine != null)
            return;

        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));

        this.playerCoroutine = StartCoroutine(WalkIntoDoorCoroutine());
    }

    private IEnumerator WalkIntoDoorCoroutine()
    {
        var player = ActiveVariant().player;
        var page = ActiveVariant().pages[^1];

        ActiveVariant().leftButton.gameObject.SetActive(false);
        ActiveVariant().rightButton.gameObject.SetActive(false);
        this.selectionCursor.gameObject.SetActive(false);
        page.door.transform.Find("Door Button").GetComponent<UnityEngine.UI.Button>().enabled = false;
        
        player.CancelWalkAndStopHere();
        player.rectTransform.SetParent(ActiveVariant().scrollContainer, true);

        yield return player.WalkToXCoroutine(page.pageContainer.anchoredPosition.x - 200);

        page.door.sprite = this.spriteDoorOpen;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIDoorOpen, options: new Audio.Options(1f, false, 0f));
        player.rectTransform.SetParent(page.maskForPlayer, true);

        // 0 here is the center of the mask area, which is actually a little left
        // of the center of the door
        yield return player.WalkToXCoroutine(900);

        yield return new WaitForSeconds(0.4f);

        Transition.GoSimple("Trophy Room");
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIScreenwipeIn, options: new Audio.Options(1f, false, 0f));
    }

    private IEnumerator ExitDoorCoroutine()
    {
        var player = ActiveVariant().player;
        var kingHat = ActiveVariant().hat;
        var page = ActiveVariant().pages[^1];

        ActiveVariant().leftButton.gameObject.SetActive(false);
        ActiveVariant().rightButton.gameObject.SetActive(false);
        this.selectionCursor.gameObject.SetActive(false);
        page.door.transform.Find("Door Button").GetComponent<UnityEngine.UI.Button>().enabled = false;

        page.door.sprite = this.spriteDoorOpen;

        Audio.instance.PlaySfx(sfxForMenu.sfxUIDoorOpen, options: new Audio.Options(1f, false, 0f));

        kingHat.rectTransform.SetParent(page.maskForPlayer);
        player.rectTransform.SetParent(page.maskForPlayer);
        if(SaveData.GetSelectedCharacter() == Character.Puffer)
            kingHat.transform.SetAsLastSibling();

        var doorX = player.TargetXForObject(page.door.rectTransform);
        player.rectTransform.anchoredPosition = new Vector2(doorX + 900, 0);

        yield return player.WalkToXCoroutine(doorX - 100);

        Audio.instance.PlaySfx(sfxForMenu.sfxUIDoorClose);
        page.door.sprite = this.spriteDoorClosed;
        
        kingHat.rectTransform.SetParent(ActiveVariant().scrollContainer);
        player.rectTransform.SetParent(ActiveVariant().scrollContainer);
        if(SaveData.GetSelectedCharacter() == Character.Puffer)
            kingHat.transform.SetAsLastSibling();
        
        page.door.transform.Find("Door Button").GetComponent<UnityEngine.UI.Button>().enabled = true;
        this.playerCoroutine = null;
        enterThroughTrophyDoor = false;
        ActiveVariant().leftButton.gameObject.SetActive(true);
        ActiveVariant().rightButton.gameObject.SetActive(true);
        this.selectionCursor.gameObject.SetActive(true);
    }

    public float PlayerXForPage(Variant variant, int pageNumber)
    {
        pageNumber %= variant.pages.Length;
        if(pageNumber < 0)
            pageNumber += variant.pages.Length;

        var x = variant.pages[pageNumber].pageContainer.anchoredPosition.x;
        return x - 500;
    }

    private void StartScrollToPageRespectingSnap(SnapDirection snapDirection)
    {
        var variant = ActiveVariant();
        int target = snapDirection switch
        {
            SnapDirection.Up => Mathf.CeilToInt(this.currentPage),
            SnapDirection.Down => Mathf.FloorToInt(this.currentPage),
            _ => Mathf.RoundToInt(this.currentPage)
        };
        this.queueClickArrow.Clear();
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(target));

        variant.player.WalkToX(PlayerXForPage(variant, target));
    }

    public IEnumerator ScrollToPage(int targetPageNumber)
    {
        this.selectionCursor.gameObject.SetActive(false);

        pageNumberSelected = targetPageNumber;

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(
            this.currentPage, targetPageNumber, 0.7f / this.scrollSpeed, Easing.QuadEaseInOut
        );

        while(!tweener.IsDone())
        {
            tweener.Advance(Time.deltaTime);
            this.currentPage = tween.Value();
            yield return null;
        }

        EnsureCurrentPageIsInBounds();
        this.currentScrollCoroutine = null;

        if(this.queueClickArrow.Count > 0)
        {
            this.queueClickArrow.Dequeue()();
            yield break;
        }
        this.scrollSpeed = 1f;

        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.SelectNearestToPosition(false);

    }

    public Page CurrentPage(Variant variant)
    {
        int pageNumber = Mathf.RoundToInt(this.currentPage);
        pageNumber %= variant.pages.Length;
        if(pageNumber < 0)
            pageNumber += variant.pages.Length;
        return variant.pages[pageNumber];
    }

    private bool IsMouseOverDragRegion()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        foreach(var r in results)
        {
            if(r.gameObject.GetComponent<UnityEngine.UI.Button>())
                return false;
        }
        return true;
    }

    public void Update()
    {
        Game.UpdateCameraLetterboxing();
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        if(this.playerCoroutine == null)
            UpdateDragging();

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortraitShort.container.gameObject.SetActive(
            activeVariant == this.variantPortraitShort
        );
        this.variantPortraitTall.container.gameObject.SetActive(
            activeVariant == this.variantPortraitTall
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);

        if(this.currentScrollCoroutine == null &&
            this.playerCoroutine == null &&
            this.isDragging == true)
        {
            var pageIndex = Mathf.RoundToInt(this.currentPage);
            activeVariant.player.WalkToX(PlayerXForPage(activeVariant, pageIndex));
        }

        UpdateScrollContainer();

        if(this.playerCoroutine == null)
        {
            EnsurePlayerIsCloseToCamera();
        }

        if(GameInput.pressedMenu)
        {
            if(Transition.instance == null)
                OnClickExit();
        }

        if(
            GameInput.pressedConfirm &&
            this.currentScrollCoroutine != null &&
            (this.selectionCursor.selection.transform == ActiveVariant().leftButton ||
                this.selectionCursor.selection.transform == ActiveVariant().rightButton)
        )
        {
            (this.selectionCursor.selection as UnityEngine.UI.Button).onClick.Invoke();
        }


    }

    public void UpdateScrollContainer()
    {
        var activeVariant = ActiveVariant();
        var w = activeVariant.wallpaper;
        var referenceWidth = activeVariant.referenceWidth;
        var pageCount = activeVariant.pages.Length;
        var totalWidth = referenceWidth * pageCount;
        var xPosition = -this.currentPage * referenceWidth;

        w.sizeDelta = new Vector2(totalWidth + referenceWidth * 2, w.sizeDelta.y);
        w.pivot = new Vector2(0, w.pivot.y);
        w.anchoredPosition = new Vector2(
            xPosition - referenceWidth, w.anchoredPosition.y
        );

        activeVariant.scrollContainer.anchoredPosition = new Vector2(xPosition, 0);
        RearrangePagesToAssistWrap();
    }

    private void RearrangePagesToAssistWrap()
    {
        var variant = ActiveVariant();
        var totalAmount = variant.referenceWidth * variant.pages.Length;
        var xScroll = this.currentPage * variant.referenceWidth;

        foreach(var page in variant.pages)
        {
            var x = page.pageContainer.anchoredPosition.x;
            var xRelative = x - xScroll;
            if(xRelative > totalAmount / 2)
            {
                xRelative -= totalAmount;
                x = xScroll + xRelative;
                page.pageContainer.anchoredPosition = new Vector2(x, 0);
            }
            else if(xRelative < -totalAmount / 2)
            {
                xRelative += totalAmount;
                x = xScroll + xRelative;
                page.pageContainer.anchoredPosition = new Vector2(x, 0);
            }
        }
    }

    private void EnsurePlayerIsCloseToCamera()
    {
        // if we have scrolled a long way from where the player is,
        // skip them forward so they're pretty close to the camera edge

        var variant = ActiveVariant();
        var playerX = variant.player.rectTransform.anchoredPosition.x;
        var cameraX = variant.referenceWidth * this.currentPage;
        var maxDistance = variant.referenceWidth * 0.5f + 400f;

        if(Mathf.Abs(playerX - cameraX) > maxDistance)
        {
            playerX = Mathf.Clamp(
                playerX, cameraX - maxDistance, cameraX + maxDistance
            );
            variant.player.rectTransform.anchoredPosition =
                new Vector2(playerX, 0);
        }
    }

    private void UpdateDragging()
    {
        if(Input.GetMouseButtonDown(0) == true &&
            IsMouseOverDragRegion() == true &&
            ActiveVariant().pages.Length > 1)
        {
            if(this.currentScrollCoroutine != null)
            {
                StopCoroutine(this.currentScrollCoroutine);
                this.currentScrollCoroutine = null;
            }

            this.isDragging = true;
            this.lastMousePosition = Input.mousePosition;
        }
        else if(this.isDragging == true && Input.GetMouseButton(0) == false)
        {
            var speed = (Input.mousePosition.x - this.lastMousePosition.x);
            SnapDirection snap = 0;
            if(speed > 10) snap = SnapDirection.Down;
            if(speed < -10) snap = SnapDirection.Up;

            this.isDragging = false;
            StartScrollToPageRespectingSnap(snap);
        }
        else if(this.isDragging == true)
        {
            float dx =
                (Input.mousePosition.x - this.lastMousePosition.x) / Screen.width;
            this.currentPage -= dx;

            EnsureCurrentPageIsInBounds();
            this.lastMousePosition = Input.mousePosition;
        }
    }

    private void EnsureCurrentPageIsInBounds()
    {
        var variant = ActiveVariant();
        if(this.currentPage < -0.5f)
        {
            this.currentPage += variant.pages.Length;
            UpdateScrollContainer();
            variant.player.DisplaceX(variant.pages.Length * variant.referenceWidth);
        }
        if(this.currentPage > variant.pages.Length - 0.5f)
        {
            this.currentPage -= variant.pages.Length;
            UpdateScrollContainer();
            variant.player.DisplaceX(variant.pages.Length * -variant.referenceWidth);
        }
    }

}
