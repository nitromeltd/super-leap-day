using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CharacterSelectScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    [Serializable]
    public class Variant
    {
        public int referenceWidth;
        public RectTransform container;
        public PlayerForMenu player;
        public RectTransform scrollContainer;
        public RectTransform wallpaper;
        public RectTransform backstoryDropdown;
        public UnityEngine.UI.Button backstoryCloseButton;
        public RectTransform abilitiesDropdown;
        public UnityEngine.UI.Button abilitiesCloseButton;
        public UnityEngine.UI.Button clickOutside;
        public RectTransform leftButton;
        public RectTransform rightButton;
        [NonSerialized] public Page[] pages;
    }
    public Variant variantLandscape;
    public Variant variantPortraitTall;
    public Variant variantPortraitShort;
    [NonSerialized] public float currentPage;

    public class Page
    {
        public Character character;
        public RectTransform pageContainer;
        public Image door;
        public UnityEngine.UI.Button infoButton;
        public UnityEngine.UI.Button characterButton;
        public UnityEngine.UI.Button abilitiesButton;
        public RectTransform maskForPlayer;

        public Image[] abilityGaugeCells;
        public LocalizeText upgradeText;
    }

    public Sprite spriteDoorClosed;
    public Sprite spriteDoorOpen;

    public Sprite abilityGaugeCellActive;
    public Sprite abilityGaugeCellDisabled;

    public AudioClip music;

    public enum SnapDirection { Neutral, Up, Down }

    private bool isDragging = false;
    private float dragStartPosition;
    private Vector2 lastMousePosition;
    private Coroutine currentScrollCoroutine;
    private Coroutine walkingIntoDoorCoroutine;
    private Queue<Action> queueClickArrow;
    private float scrollSpeed;

    public void Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        var characters = new [] {
            Character.Yolk,
            Character.Puffer,
            Character.Sprout,
            Character.Goop,
            Character.King
        }.Where(c =>
            true || c == Character.Yolk || SaveData.IsCharacterPurchased(c) == true
        ).ToArray();

        void SetupVariant(Variant v)
        {
            v.container.gameObject.SetActive(true);// LocalizeText won't call Awake() if inactive

            var pageOriginal = v.scrollContainer.Find("Page") as RectTransform;
            var pages = new List<Page>();

            foreach (var character in characters)
            {
                var container = Instantiate(pageOriginal, v.scrollContainer);
                var page = new Page { character = character, pageContainer = container };
                pages.Add(page);
            }
            Destroy(pageOriginal.gameObject);

            v.pages = pages.ToArray();

            for (int n = 0; n < v.pages.Length; n += 1)
            {
                var page = v.pages[n];
                var container = page.pageContainer;
                container.gameObject.name = $"Page {page.character.ToString()}";
                container.SetSiblingIndex(n);
                container.anchoredPosition = new Vector2(n * v.referenceWidth, 0);
                SetupPage(page);
            }

            v.leftButton.gameObject.SetActive(characters.Length > 1);
            v.rightButton.gameObject.SetActive(characters.Length > 1);

            v.backstoryDropdown.gameObject.SetActive(false);
            v.abilitiesDropdown.gameObject.SetActive(false);
            v.clickOutside.gameObject.SetActive(false);
            v.clickOutside.interactable = false;
            v.clickOutside.image.color = Color.clear;
        }

        void SetupPage(Page page)
        {
            var locked = SaveData.IsCharacterPurchased(page.character) == false;
            var doorway = page.pageContainer.Find("Doorway");
            page.door = doorway.Find("Door").GetComponent<Image>();
            page.maskForPlayer =
                doorway.Find("Mask For Player").GetComponent<RectTransform>();
            page.infoButton =
                page.pageContainer.Find("Info Button")
                .GetComponent<UnityEngine.UI.Button>();
            page.characterButton =
                page.pageContainer.Find("Doorway/Choose Character Button")
                .GetComponent<UnityEngine.UI.Button>();
            page.abilitiesButton =
                page.pageContainer.Find("Character Info/Ability Level Gauge/Abilities Button")
                .GetComponent<UnityEngine.UI.Button>();
            var characterName = page.pageContainer.Find("Character Info/Name")
                .GetComponent<TMPro.TextMeshProUGUI>();
            foreach(Transform t in page.characterButton.transform)
                t.gameObject.SetActive(false);
            GameObject ButtonImage(string name) =>
                page.characterButton.transform.Find(name).gameObject;
            foreach(Transform t in page.pageContainer.Find("Character Info/Portraits").transform)
                t.gameObject.SetActive(false);
            GameObject ProfileImage(string name) =>
                page.pageContainer.Find($"Character Info/Portraits/{name}").gameObject;
            page.upgradeText = page.pageContainer.Find("Character Info/Ability Level Gauge/Upgrade Text").GetComponent<LocalizeText>();
            var cells = new List<Image>();
            for(int n = 1; n <= 4; n += 1)
                cells.Add(page.pageContainer.Find($"Character Info/Ability Level Gauge/cell{n}").GetComponent<Image>());
            page.abilityGaugeCells = cells.ToArray();

            if(locked == false)
            {
                page.infoButton.onClick.AddListener(() => OnClickCharacterInfo(page));
                page.pageContainer.Find("Toolbox").gameObject.SetActive(false);
                page.characterButton.onClick.AddListener(() => OnClickSwitchCharacter(page));
                page.abilitiesButton.onClick.AddListener(() => OnClickCharacterAbilities(page));
                characterName.text = page.character switch
                {
                    Character.King => "The X",
                    _              => page.character.ToString()
                };
                ButtonImage($"{page.character}").SetActive(true);
                ProfileImage($"{page.character}").SetActive(true);

                void UpdateUpgradeLevel()
                {
                    int max = SaveData.GetCharacterUpgradeLevel(page.character);
                    int current = SaveData.GetCharacterUpgradeLevelCurrentlySelected(page.character);
                    for(int n = 1; n <= 4; n += 1)
                    {
                        page.abilityGaugeCells[n-1].gameObject.SetActive(n <= max);
                        page.abilityGaugeCells[n-1].sprite = n <= current ? this.abilityGaugeCellActive : this.abilityGaugeCellDisabled;
                    }
                    page.upgradeText.SetText("UPGRADE {0}", current.ToString());
                }

                void ChangeUpgradeLevel(int dir)
                {
                    int max = SaveData.GetCharacterUpgradeLevel(page.character);
                    int current = SaveData.GetCharacterUpgradeLevelCurrentlySelected(page.character);
                    SaveData.SetCharacterUpgradeLevelCurrentlySelected(page.character, Mathf.Clamp(current + dir, 1, max));
                    UpdateUpgradeLevel();
                }
                UpdateUpgradeLevel();

                page.pageContainer.Find("Character Info/Ability Level Gauge/Ability Up")
                    .GetComponent<UnityEngine.UI.Button>()
                    .onClick.AddListener(() => { ChangeUpgradeLevel(1); });

                page.pageContainer.Find("Character Info/Ability Level Gauge/Ability Down")
                    .GetComponent<UnityEngine.UI.Button>()
                    .onClick.AddListener(() => { ChangeUpgradeLevel(-1); });
            }
            else
            {
                page.infoButton.interactable = false;
                if(ActiveVariant() != this.variantLandscape)
                    page.pageContainer.Find("Table").gameObject.SetActive(false);
                page.characterButton.interactable = false;
                characterName.text = "? ? ?";
                var deniedButton = page.characterButton.transform.Find("Denied Button").GetComponent<UnityEngine.UI.Button>();
                var image = deniedButton.transform.Find("Image").GetComponent<Image>();
                deniedButton.onClick.AddListener(() => 
                { 
                    Audio.instance.PlaySfx(this.sfxForMenu.sfxUICalendarSwipeDeny);
                    image.color = Color.white;
                    var tweener = new Tweener();
                    tweener.Alpha(image, 0, 0.5f);
                    StartCoroutine(tweener.Run());
                });
                deniedButton.gameObject.SetActive(true);
                ButtonImage("Locked").SetActive(true);
                ProfileImage($"{page.character} Locked").SetActive(true);
                page.pageContainer.Find("Character Info/Ability Level Gauge").gameObject.SetActive(false);
            }
        }

        SetupVariant(this.variantLandscape);
        SetupVariant(this.variantPortraitShort);
        SetupVariant(this.variantPortraitTall);

        {
            this.currentPage = Array.IndexOf(characters, SaveData.GetSelectedCharacter());
            if (this.currentPage < 0)
                this.currentPage = 0;

            void MovePlayerToStartingDoor(Variant v)
            {
                v.player.rectTransform.anchoredPosition = new Vector2(
                    PlayerXForPage(v, (int)this.currentPage), 0
                );
            }
            MovePlayerToStartingDoor(this.variantLandscape);
            MovePlayerToStartingDoor(this.variantPortraitShort);
            MovePlayerToStartingDoor(this.variantPortraitTall);
        }

        this.selectionCursor.Select(
            ActiveVariant().pages[(int)this.currentPage].characterButton, false
        );

        this.queueClickArrow = new Queue<Action>();
        this.scrollSpeed = 1f;

        var player = ActiveVariant().player;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));
    }

    public Variant ActiveVariant()
    {
        if (GameCamera.IsLandscape() == true)
            return this.variantLandscape;
        else if ((float)Screen.height / (float)Screen.width > 1.9f)
            return this.variantPortraitTall;
        else
            return this.variantPortraitShort;
    }

    public void OnClickLeft()
    {
        if (this.currentScrollCoroutine != null)
        {
            if (this.queueClickArrow.Count > 0 && this.queueClickArrow.Peek() == OnClickRight)
                this.queueClickArrow.Clear();
            this.queueClickArrow.Enqueue(OnClickLeft);
            this.scrollSpeed = Mathf.Min(this.scrollSpeed + 0.5f, 2f);
            return;
        }

        var variant = ActiveVariant();
        int next = Mathf.FloorToInt(this.currentPage - 0.2f);
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(next));

        variant.player.WalkToX(PlayerXForPage(variant, next));
    }

    public void OnClickRight()
    {
        if (this.currentScrollCoroutine != null)
        {
            if (this.queueClickArrow.Count > 0 && this.queueClickArrow.Peek() == OnClickLeft)
                this.queueClickArrow.Clear();
            this.queueClickArrow.Enqueue(OnClickRight);
            this.scrollSpeed = Mathf.Min(this.scrollSpeed + 0.5f, 2f);
            return;
        }

        var variant = ActiveVariant();
        int next = Mathf.CeilToInt(this.currentPage + 0.2f);
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(next));

        variant.player.WalkToX(PlayerXForPage(variant, next));
    }

    public void OnClickCharacterInfo(Page page)
    {
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));

        this.selectionCursor.gameObject.SetActive(false);

        var variant = ActiveVariant();
        int height = variant.referenceWidth * Screen.height / Screen.width;
        variant.backstoryDropdown.gameObject.SetActive(true);
        variant.backstoryDropdown.anchoredPosition = new Vector2(0, height);

        variant.backstoryDropdown
            .GetComponent<CharacterSelectScreenBackstoryDropdown>()
            .Init(page.character);

        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(variant.backstoryDropdown, Vector2.zero, 0.3f, Easing.QuadEaseOut);
        tweener.Callback(0.3f, () =>
        {
            this.selectionCursor.Select(variant.backstoryCloseButton);
            this.selectionCursor.ConstrainMovement(variant.backstoryDropdown.gameObject);
            this.selectionCursor.gameObject.SetActive(true);
        });
        FadeInClickOutside(tweener, variant.clickOutside);
        StartCoroutine(tweener.Run());
    }

    public void OnClickDismissCharacterInfo()
    {
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));

        this.selectionCursor.gameObject.SetActive(false);

        var variant = ActiveVariant();
        int height = variant.referenceWidth * Screen.height / Screen.width;

        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(variant.backstoryDropdown, new Vector2(0, height), 0.3f, Easing.QuadEaseIn);
        tweener.Callback(0.3f, () =>
        {
            variant.backstoryDropdown.gameObject.SetActive(false);
            this.selectionCursor.RemoveConstraint();
            this.selectionCursor.Select(CurrentPage(variant).infoButton);
            this.selectionCursor.gameObject.SetActive(true);
        });
        FadeOutClickOutside(tweener, variant.clickOutside);
        StartCoroutine(tweener.Run());
    }

    public void OnClickCharacterAbilities(Page page)
    {
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));

        this.selectionCursor.gameObject.SetActive(false);

        var variant = ActiveVariant();
        int height = variant.referenceWidth * Screen.height / Screen.width;
        variant.abilitiesDropdown.gameObject.SetActive(true);
        variant.abilitiesDropdown.anchoredPosition = new Vector2(0, height);

        variant.abilitiesDropdown
            .GetComponent<CharacterSelectScreenAbilityDropdown>()
            .Init(page.character);
        
        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(variant.abilitiesDropdown, Vector2.zero, 0.3f, Easing.QuadEaseOut);
        tweener.Callback(0.3f, () =>
        {
            this.selectionCursor.Select(variant.abilitiesCloseButton);
            this.selectionCursor.ConstrainMovement(variant.abilitiesDropdown.gameObject);
            this.selectionCursor.gameObject.SetActive(true);
        });
        FadeInClickOutside(tweener, variant.clickOutside);
        StartCoroutine(tweener.Run());
    }

    public void OnClickDismissCharacterAbilities()
    {
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIBack, options: new Audio.Options(1f, false, 0f));

        this.selectionCursor.gameObject.SetActive(false);

        var variant = ActiveVariant();
        int height = variant.referenceWidth * Screen.height / Screen.width;

        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(variant.abilitiesDropdown, new Vector2(0, height), 0.3f, Easing.QuadEaseIn);
        tweener.Callback(0.3f, () =>
        {
            variant.abilitiesDropdown.gameObject.SetActive(false);
            this.selectionCursor.RemoveConstraint();
            this.selectionCursor.Select(CurrentPage(variant).abilitiesButton);
            this.selectionCursor.gameObject.SetActive(true);
        });
        FadeOutClickOutside(tweener, variant.clickOutside);
        StartCoroutine(tweener.Run());
    }

    public void FadeInClickOutside(Tweener tweener, UnityEngine.UI.Button clickOutside)
    {
        clickOutside.gameObject.SetActive(true);
        tweener.Alpha(clickOutside.image, 0.5f, 0.3f);
        tweener.Callback(0.3f, () =>
        {
            clickOutside.interactable = true;
        });
    }

    public void FadeOutClickOutside(Tweener tweener, UnityEngine.UI.Button clickOutside)
    {
        clickOutside.interactable = false;
        tweener.Alpha(clickOutside.image, 0f, 0.3f);
        tweener.Callback(0.3f, () =>
        {
            clickOutside.gameObject.SetActive(false);
        });
    }

    public void OnClickSwitchCharacter(Page page)
    {
        if (this.walkingIntoDoorCoroutine != null)
            return;

        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));

        SaveData.SetSelectedCharacter(page.character);


        ActiveVariant().leftButton.gameObject.SetActive(false);
        ActiveVariant().rightButton.gameObject.SetActive(false);

        this.walkingIntoDoorCoroutine = StartCoroutine(WalkIntoDoorCoroutine(page));
    }

    private IEnumerator WalkIntoDoorCoroutine(CharacterSelectScreen.Page page)
    {
        var player = ActiveVariant().player;

        player.CancelWalkAndStopHere();
        player.rectTransform.SetParent(ActiveVariant().scrollContainer, true);
        var oldParent = this.transform.parent;

        yield return player.WalkToXCoroutine(page.pageContainer.anchoredPosition.x - 200);

        page.door.sprite = this.spriteDoorOpen;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIDoorOpen, options: new Audio.Options(1f, false, 0f));
        player.rectTransform.SetParent(page.maskForPlayer, true);

        // 0 here is the center of the mask area, which is actually a little left
        // of the center of the door
        yield return player.WalkToXCoroutine(900);

        yield return new WaitForSeconds(0.4f);
        player.RefreshSelectedCharacter();

        if (player.Character == Character.Yolk)
        {
            Audio.instance.PlaySfx(this.sfxForMenu.sfxYolkVoice);
        }
        else if (player.Character == Character.Sprout)
        {
            Audio.instance.PlaySfx(this.sfxForMenu.sfxSprout);
        }
        else if (player.Character == Character.Puffer)
        {
            Audio.instance.PlaySfx(this.sfxForMenu.sfxPuffer);
        }
        else if (player.Character == Character.King)
        {
            Audio.instance.PlaySfx(this.sfxForMenu.sfxKing);
        }
        else if (player.Character == Character.Goop)
        {
            Audio.instance.PlaySfx(this.sfxForMenu.sfxGoop);
        }
        yield return player.WalkToXCoroutine(-200);

        page.door.sprite = this.spriteDoorClosed;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIDoorClose, options: new Audio.Options(1f, false, 0f));
        player.rectTransform.SetParent(ActiveVariant().scrollContainer, true);
        player.rectTransform.SetAsLastSibling();

        // player changing containers can be flaky, walk to adjust to new container before finish

        player.WalkToX(PlayerXForPage(ActiveVariant(), Mathf.RoundToInt(this.currentPage)), () =>
        {
            ActiveVariant().leftButton.gameObject.SetActive(true);
            ActiveVariant().rightButton.gameObject.SetActive(true);
            this.walkingIntoDoorCoroutine = null;
        });
    }

    public void OnClickExit()
    {
        MainMenuScreen.enterSceneThroughDoor = MainMenuScreen.DoorType.CharacterSelect;
        Transition.GoSimple("Main Menu");
        var player = ActiveVariant().player;
        Audio.instance.PlaySfx(this.sfxForMenu.sfxUIScreenwipeIn, options: new Audio.Options(1f, false, 0f));
    }

    public void OnClickOutside()
    {
        var variant = ActiveVariant();
        if(variant.backstoryDropdown.gameObject.activeInHierarchy)
            OnClickDismissCharacterInfo();
        else if(variant.abilitiesDropdown.gameObject.activeInHierarchy)
            OnClickDismissCharacterAbilities();
        else return;
    }

    public Page CurrentPage(Variant variant)
    {
        int pageNumber = Mathf.RoundToInt(this.currentPage);
        pageNumber %= variant.pages.Length;
        if (pageNumber < 0)
            pageNumber += variant.pages.Length;
        return variant.pages[pageNumber];
    }

    public float PlayerXForPage(Variant variant, int pageNumber)
    {
        pageNumber %= variant.pages.Length;
        if (pageNumber < 0)
            pageNumber += variant.pages.Length;

        var x = variant.pages[pageNumber].pageContainer.anchoredPosition.x;
        return x - 500;
    }

    private void StartScrollToPageRespectingSnap(SnapDirection snapDirection)
    {
        var variant = ActiveVariant();
        int target = snapDirection switch
        {
            SnapDirection.Up   => Mathf.CeilToInt(this.currentPage),
            SnapDirection.Down => Mathf.FloorToInt(this.currentPage),
            _                  => Mathf.RoundToInt(this.currentPage)
        };
        this.queueClickArrow.Clear();
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(target));

        variant.player.WalkToX(PlayerXForPage(variant, target));
    }

    public IEnumerator ScrollToPage(int targetPageNumber)
    {
        this.selectionCursor.gameObject.SetActive(false);

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(
            this.currentPage, targetPageNumber, 0.7f / this.scrollSpeed, Easing.QuadEaseInOut
        );

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.deltaTime);
            this.currentPage = tween.Value();
            yield return null;
        }

        EnsureCurrentPageIsInBounds();
        this.currentScrollCoroutine = null;

        if (this.queueClickArrow.Count > 0)
        {
            this.queueClickArrow.Dequeue()();
            yield break;
        }
        this.scrollSpeed = 1f;

        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.SelectNearestToPosition(false);

    }

    private bool IsMouseOverDragRegion()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        foreach (var r in results)
        {
            if (r.gameObject.GetComponent<UnityEngine.UI.Button>())
                return false;
        }
        return true;
    }

    public void Update()
    {
        Game.UpdateCameraLetterboxing();
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        if (this.walkingIntoDoorCoroutine == null)
            UpdateDragging();

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortraitShort.container.gameObject.SetActive(
            activeVariant == this.variantPortraitShort
        );
        this.variantPortraitTall.container.gameObject.SetActive(
            activeVariant == this.variantPortraitTall
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);

        if (this.currentScrollCoroutine == null &&
            this.walkingIntoDoorCoroutine == null &&
            this.isDragging == true)
        {
            var pageIndex = Mathf.RoundToInt(this.currentPage);
            activeVariant.player.WalkToX(PlayerXForPage(activeVariant, pageIndex));
        }

        UpdateScrollContainer();

        if (this.walkingIntoDoorCoroutine == null)
        {
            EnsurePlayerIsCloseToCamera();
        }

        if (GameInput.pressedMenu)
        {
            var variant = ActiveVariant();
            if (variant.backstoryDropdown.gameObject.activeInHierarchy)
                OnClickDismissCharacterInfo();
            else if (variant.abilitiesDropdown.gameObject.activeInHierarchy)
                OnClickDismissCharacterAbilities();
            else if (Transition.instance == null)
                OnClickExit();
        }

        if(
            GameInput.pressedConfirm &&
            this.currentScrollCoroutine != null &&
            (this.selectionCursor.selection.transform == ActiveVariant().leftButton ||
                this.selectionCursor.selection.transform == ActiveVariant().rightButton)
        )
        {
            (this.selectionCursor.selection as UnityEngine.UI.Button).onClick.Invoke();
        }


    }

    public void UpdateScrollContainer()
    {
        var activeVariant = ActiveVariant();
        var w = activeVariant.wallpaper;
        var referenceWidth = activeVariant.referenceWidth;
        var pageCount = activeVariant.pages.Length;
        var totalWidth = referenceWidth * pageCount;
        var xPosition = -this.currentPage * referenceWidth;

        w.sizeDelta = new Vector2(totalWidth + referenceWidth * 2, w.sizeDelta.y);
        w.pivot = new Vector2(0, w.pivot.y);
        w.anchoredPosition = new Vector2(
            xPosition - referenceWidth, w.anchoredPosition.y
        );

        activeVariant.scrollContainer.anchoredPosition = new Vector2(xPosition, 0);
        RearrangePagesToAssistWrap();
    }

    private void RearrangePagesToAssistWrap()
    {
        var variant = ActiveVariant();
        var totalAmount = variant.referenceWidth * variant.pages.Length;
        var xScroll = this.currentPage * variant.referenceWidth;

        foreach (var page in variant.pages)
        {
            var x = page.pageContainer.anchoredPosition.x;
            var xRelative = x - xScroll;
            if (xRelative > totalAmount / 2)
            {
                xRelative -= totalAmount;
                x = xScroll + xRelative;
                page.pageContainer.anchoredPosition = new Vector2(x, 0);
            }
            else if (xRelative < -totalAmount / 2)
            {
                xRelative += totalAmount;
                x = xScroll + xRelative;
                page.pageContainer.anchoredPosition = new Vector2(x, 0);
            }
        }
    }

    private void EnsurePlayerIsCloseToCamera()
    {
        // if we have scrolled a long way from where the player is,
        // skip them forward so they're pretty close to the camera edge

        var variant = ActiveVariant();
        var playerX = variant.player.rectTransform.anchoredPosition.x;
        var cameraX = variant.referenceWidth * this.currentPage;
        var maxDistance = variant.referenceWidth * 0.5f + 400f;

        if (Mathf.Abs(playerX - cameraX) > maxDistance)
        {
            playerX = Mathf.Clamp(
                playerX, cameraX - maxDistance, cameraX + maxDistance
            );
            variant.player.rectTransform.anchoredPosition =
                new Vector2(playerX, 0);
        }
    }

    private void UpdateDragging()
    {
        if (Input.GetMouseButtonDown(0) == true &&
            IsMouseOverDragRegion() == true &&
            ActiveVariant().pages.Length > 1)
        {
            if (this.currentScrollCoroutine != null)
            {
                StopCoroutine(this.currentScrollCoroutine);
                this.currentScrollCoroutine = null;
            }

            this.isDragging = true;
            this.dragStartPosition = this.currentPage;
            this.lastMousePosition = Input.mousePosition;
        }
        else if (this.isDragging == true && Input.GetMouseButton(0) == false)
        {
            var speed = (Input.mousePosition.x - this.lastMousePosition.x);
            SnapDirection snap = 0;
            if (speed > 10) snap = SnapDirection.Down;
            if (speed < -10) snap = SnapDirection.Up;

            this.isDragging = false;
            StartScrollToPageRespectingSnap(snap);
        }
        else if (this.isDragging == true)
        {
            float dx =
                (Input.mousePosition.x - this.lastMousePosition.x) / Screen.width;
            this.currentPage -= dx;

            EnsureCurrentPageIsInBounds();
            this.lastMousePosition = Input.mousePosition;
        }
    }

    private void EnsureCurrentPageIsInBounds()
    {
        var variant = ActiveVariant();
        if (this.currentPage < -0.5f)
        {
            this.currentPage += variant.pages.Length;
            UpdateScrollContainer();
            variant.player.DisplaceX(variant.pages.Length * variant.referenceWidth);
        }
        if (this.currentPage > variant.pages.Length - 0.5f)
        {
            this.currentPage -= variant.pages.Length;
            UpdateScrollContainer();
            variant.player.DisplaceX(variant.pages.Length * -variant.referenceWidth);
        }
    }
}
