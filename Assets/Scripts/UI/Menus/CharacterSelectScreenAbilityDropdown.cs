using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;

public class CharacterSelectScreenAbilityDropdown : MonoBehaviour
{
    public VideoClip[] videoClipsForYolk;
    public VideoClip[] videoClipsForSprout;
    public VideoClip[] videoClipsForGoop;
    public VideoClip[] videoClipsForPuffer;
    public VideoClip[] videoClipsForKing;
    
    public VideoPlayer videoPlayer;
    public Image videoBlackOut;
    public LocalizeText text;

    public static string[] AbilityText(Character character) => character switch
    {
        Character.Yolk => new[] {
            "Tap once to jump and tap a second time to double jump.",
            "Press and hold to stop moving.",
            "Press and hold to lay an egg restart point. Holding on too long will make it explode.",
            "Press and hold to lay explosive eggs. Respawn where you died in a flying egg that travels back towards the last checkpoint.",
        },
        Character.Puffer => new[] {
            "Press and hold in the air to glide.",
            "Press and hold in the air to glide. Tap a third time in the air for an extra lift.",
            "Press and hold in the air to glide. Tap a third and fourth time in the air for extra lift.",
            "Press and hold in the air to glide. Tap as many times as you like in the air for an extra lift."
        },
        Character.Sprout => new[] {
            "Press and hold in the air to suck in fruit and coins near you.",
            "Press and hold to suck in an increased range of fruit and coins, and wobble blocks with hidden pick-ups.",
            "Press and hold to suck an increased range of fruit and coins. Suck items from within blocks.",
            "Press and hold to suck the widest range of fruit and coins. Suck items from within blocks and chests."
        },
        Character.Goop => new[] {
            "Press and hold to stick and roll higher up walls.",
            "Press and hold to stick and roll up any height of walls.",
            "Press and hold to stick and roll up walls and along ceilings.",
            "Press and hold to stick and roll up walls and along ceilings and round corners."
        },
        Character.King => new[] {
            "Hold second jump to perform a slam down move. Hold to stay still. Keeping your hat will give you a second hit.",
            "Press and hold second jump to perform a slam down move with a deadly shockwave. Hat appears at checkpoints.",
            "Press and hold second jump to perform a slam down move with bigger range. Hat appears in more places.",
            "Press and hold second jump to perform a slam down move with biggest range. Hat appears in maximum places."
        },
        _ => new[] { "", "", "", "" }
    };

    public void Init(Character character)
    {
        int upgradeIndex = SaveData.GetCharacterUpgradeLevelCurrentlySelected(character) - 1;

        this.videoPlayer.clip = (character switch
        {
            Character.Yolk   => this.videoClipsForYolk,
            Character.Sprout => this.videoClipsForSprout,
            Character.Goop   => this.videoClipsForGoop,
            Character.Puffer => this.videoClipsForPuffer,
            Character.King   => this.videoClipsForKing,
            _                => null
        })[upgradeIndex];

        this.text.SetText(AbilityText(character)[upgradeIndex].ToUpper());
        
        this.videoBlackOut.color = Color.black;
        this.videoPlayer.Prepare();
        StartCoroutine(WaitForVideoPlayer());

    }
    public IEnumerator WaitForVideoPlayer()
    {
        while(this.videoPlayer.isPrepared == false)
            yield return null;

        this.videoPlayer.Play();
        var tweener = new Tweener();
        tweener.Alpha(videoBlackOut, 0, 0.2f);
        yield return tweener.Run();
    }
}
