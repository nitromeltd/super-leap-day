using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;

public class CharacterSelectScreenBackstoryDropdown : MonoBehaviour
{
    public Image image;
    public Image imageFade;
    public TickerText text;

    public Sprite[] backstoryArtForYolk;
    public Sprite[] backstoryArtForGoop;
    public Sprite[] backstoryArtForSprout;
    public Sprite[] backstoryArtForPuffer;
    public Sprite[] backstoryArtForKing;

    private Sprite[] art;
    private string[] words;
    private float time = 0;
    private int page = 0;

    public void Init(Character character)
    {
        this.art = character switch
        {
            Character.Yolk   => this.backstoryArtForYolk,
            Character.Sprout => this.backstoryArtForSprout,
            Character.Goop   => this.backstoryArtForGoop,
            Character.Puffer => this.backstoryArtForPuffer,
            Character.King   => this.backstoryArtForKing,
            _                => null
        };
        this.words = character switch
        {
            Character.Yolk => new [] {
                "After defeating the King under Leap Day’s previous reign, many assumed that Yolk would be the new ruler of the Contest.",
                "For that to happen though you must take the fallen crown...",
                "The crown did indeed fall but was accidently caught by former rival Lick.",
                "Corrupted by greed, Lick soon adjusted to his new role as leader and was quickly accepted by all.",
                "Yolk was quickly forgotten, and many do not even remember that he was part of past events.",
                "Determined to conquer his fears, Yolk aims to learn tricks to do better this time around.",
                ""
            },
            Character.Puffer => new [] {
                "As a youngster Puffer watched his heroes with awe at the Leap Day event.",
                "He knows everything about them and is the number one Leap Day fan.",
                "Puffer was teased at school. Embarrassed, his face would swell to the bully’s amusement.",
                "If only they knew this ‘flaw’ would one day become his greatest asset!",
                "Now that Lick is King, Puffer jumps at the chance to become the new player two.",
                "To compete with his hero Yolk is more than he dreamed, but how far can he go?",
                ""
            },
            Character.Sprout => new [] {
                "As the Rainy Ruins were prepared for the Leap Day event and the traps were set,",
                "the rainwater reached areas of the temple locked away for years.",
                "A seed long lost and dried out is submerged in water and comes back to life.",
                "What started as a plant soon became more and was able to venture beyond.",
                "Emerging to a new life outside the Ruin walls,",
                "Sprout joins the contest in the hope of learning more about her origins.",
                ""
            },
            Character.Goop => new [] {
                "Formed as an experiment by the Beetle Army,",
                "produced using Cactus Oils they acquired at Conflict Canyon.",
                "His sticky properties enabled an escape from the facility where they kept him.",
                "He now hides in plain sight of the Leap Day event. Nobody can touch him while he is watched by millions.",
                "He aims to take the Leap Day Crown",
                "and with it the power to overthrow the Oil Empire that held him captive.",
                ""
            },
            Character.King => new [] {
                "Defeated during the last Leap Day event, the former King lost his crown",
                "and with it his status and his control of the competition.",
                "Struggled to cope with his new reality, his son Jr took low paid jobs to care for them both.",
                "Now working the Leap Day store, he is able to keep Dad close by.",
                "Through the generosity of fans, money was raised for the ex-King to enter the Leap Day competition.",
                "Donning a makeshift tin can cap in lieu of his former crown, he has vowed to win back his former glory.",
                ""
            },
            _ => new [] { "", "", "", "", "", "", "" }
        };
        this.page = 0;
        SetPage();
        this.time = -1f;// wait for drop down
        this.imageFade.sprite = this.image.sprite;
    }

    void SetPage()
    {
        this.text.SetText(this.words[page]);
        this.imageFade.color = Color.white;
        this.imageFade.sprite = this.image.sprite;
        this.image.sprite = this.art[page / 2];
        this.time = 0f;
        if(this.page == 6)
            this.time = 5.5f;
    }

    public void Update()
    {
        this.time += Time.deltaTime;
        if(this.imageFade.color.a > 0)
            this.imageFade.color -= new Color(0, 0, 0, 0.1f);
        if(this.time > 6f)
        {
            this.page = (this.page + 1) % 7;
            SetPage();
        }
    }

    public void NextPage()
    {
        this.page = (this.page + 1) % 7;
        SetPage();
    }
}
