using UnityEngine;
using UnityEngine.UI;

public class CharacterSelectWallpaper : Graphic
{
    [Header("Wallpaper Details")]
    public Sprite wallpaperSprite;
    public float wallpaperScale = 1;

	public override Texture mainTexture { get => wallpaperSprite.texture; }

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        if (this.wallpaperScale < 0.5f) return;

        const int RepeatWidth = 140;
        const int GradientHeight = 1610;
        const int RepeatHeight = 140;
        const int TextureHeight = GradientHeight + RepeatHeight;

        var color = Color.white;

        var xMin = (0 - rectTransform.pivot.x) * this.rectTransform.rect.width;
        var xMax = (1 - rectTransform.pivot.x) * this.rectTransform.rect.width;
        var yMin = (0 - rectTransform.pivot.y) * this.rectTransform.rect.height;
        var yMax = (1 - rectTransform.pivot.y) * this.rectTransform.rect.height;

        xMin /= wallpaperScale;
        xMax /= wallpaperScale;
        yMin /= wallpaperScale;
        yMax /= wallpaperScale;

        int repeatRows = Mathf.CeilToInt(((yMax - yMin) - GradientHeight) / RepeatHeight);
        if (repeatRows < 0) repeatRows = 0;

        for (float x = xMin; x < xMax; x += RepeatWidth)
        {
            float x1 = x;
            float x2 = Mathf.Min(x + RepeatWidth, xMax);
            float u1 = 0;
            float u2 = (x2 - x1) / RepeatWidth;

            for (int r = 0; r < repeatRows; r += 1)
            {
                float idealY = yMax - GradientHeight - (RepeatHeight * (repeatRows - r));
                float y1 = Mathf.Max(idealY, yMin);
                float y2 = idealY + RepeatHeight;
                float v1 = (y1 - idealY) / RepeatHeight;
                float v2 = (y2 - idealY) / RepeatHeight;
                v1 *= ((float)RepeatHeight / TextureHeight);
                v2 *= ((float)RepeatHeight / TextureHeight);
                AddRect(x1, x2, y1, y2, u1, u2, v1, v2);
            }

            {
                int firstVertex = vh.currentVertCount;
                float y1 = Mathf.Max(yMax - GradientHeight, yMin);
                float y2 = yMax;
                float v1 = (float)RepeatHeight / (float)TextureHeight;
                float v2 = 1;
                AddRect(x1, x2, y1, y2, u1, u2, v1, v2);
            }
        }

        void AddRect(
            float x1, float x2, float y1, float y2, float u1, float u2, float v1, float v2
        )
        {
            int firstVertex = vh.currentVertCount;

            x1 *= this.wallpaperScale;
            x2 *= this.wallpaperScale;
            y1 *= this.wallpaperScale;
            y2 *= this.wallpaperScale;

            vh.AddVert(new Vector3(x1, y1, 0), color, new Vector2(u1, v1));
            vh.AddVert(new Vector3(x2, y1, 0), color, new Vector2(u2, v1));
            vh.AddVert(new Vector3(x2, y2, 0), color, new Vector2(u2, v2));
            vh.AddVert(new Vector3(x1, y2, 0), color, new Vector2(u1, v2));

            vh.AddTriangle(firstVertex, firstVertex + 1, firstVertex + 2);
            vh.AddTriangle(firstVertex, firstVertex + 3, firstVertex + 2);
        }
    }
}
