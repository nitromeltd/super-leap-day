using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Also operates Puffer head (only visible when jumping)
public class KingHatForMenu : MonoBehaviour
{
    public Animated animated;
    public RectTransform rectTransform;
    public PlayerAnimationsForMenu playerAnimationsForMenu;
    public State state;

    [System.NonSerialized] public PlayerAnimationsForMenu.PlayerAnimations headAnimations;
    [System.NonSerialized] public Character character;

    private Animated.Animation currentPlayerAnimation;
    private RectTransform hatOff;

    public enum State
    {
        OnHead, Falling, Grounded
    }

    private Vector2 offset;
    private Vector2 droppedAt;
    private float fallingSpeed;

    private static Rect HitBoxNormalized = new Rect(99f / 288, 162f / 288, 70f / 288, 70f / 288);

    private void Awake()
    {
        this.headAnimations = this.playerAnimationsForMenu.kingHat;
    }

    private void OnEnable()
    {
        RefreshSelectedCharacter();
        this.droppedAt = this.rectTransform.anchoredPosition;
    }

    public void RefreshSelectedCharacter()
    {
        this.character = SaveData.GetSelectedCharacter();

        this.headAnimations = this.character switch
        {
            Character.King => this.playerAnimationsForMenu.kingHat,
            Character.Puffer => this.playerAnimationsForMenu.pufferHead,
            _ => null,
        };

        this.gameObject.SetActive(this.headAnimations != null);
        if(this.headAnimations == null)
            return;

        if(this.state == State.OnHead)
        {
            this.animated.PlayAndLoop(this.headAnimations.animationIdle);
        }
        else
        {
            if(this.character == Character.Puffer)
                this.animated.PlayOnce(this.headAnimations.animationIdle);
            else
                this.animated.PlayOnce(this.playerAnimationsForMenu.kingHatGrounded);
        }

    }

    public void TrackPlayer(PlayerForMenu player)
    {
        var hatHitBox = RectTransformToWorld(this.rectTransform);
        hatHitBox.position += HitBoxNormalized.position * hatHitBox.size;
        hatHitBox.size *= HitBoxNormalized.size;

        var playerRect = RectTransformToWorld(player.rectTransform);

        if(this.state == State.OnHead)
        {
            var playerAnim = player.animated.currentAnimation;
            var playerAnimations = player.playerAnimations;

            if(playerAnim != this.currentPlayerAnimation)
            {
                if(playerAnim == playerAnimations.animationIdle)
                    this.animated.PlayAndLoop(this.headAnimations.animationIdle);
                if(playerAnim == playerAnimations.animationRun)
                    this.animated.PlayAndLoop(this.headAnimations.animationRun);
                if(playerAnim == playerAnimations.animationJumpDown)
                    this.animated.PlayAndLoop(this.headAnimations.animationJumpDown);
                if(playerAnim == playerAnimations.animationJumpUp)
                    this.animated.PlayAndLoop(this.headAnimations.animationJumpUp);
                if(playerAnim == playerAnimations.animationSitDown)
                    this.animated.PlayOnce(this.headAnimations.animationSitDown);
                if(playerAnim == playerAnimations.animationStandUp)
                    this.animated.PlayOnce(this.headAnimations.animationStandUp, () =>
                    {
                        this.animated.PlayAndLoop(this.headAnimations.animationIdle);
                    });
                this.currentPlayerAnimation = playerAnim;
            }

            if(this.rectTransform.parent != player.rectTransform.parent)
            {
                this.rectTransform.SetParent(player.rectTransform.parent);
                this.rectTransform.SetSiblingIndex(player.rectTransform.GetSiblingIndex());
            }
                
            this.offset = Vector2.Lerp(this.offset, Vector2.zero, 0.2f);
            this.rectTransform.anchoredPosition = player.rectTransform.anchoredPosition + this.offset;
            this.rectTransform.localScale = player.rectTransform.localScale;

            // knock off King Hat when passing a "hat off" transform
            if
            (
                this.offset.sqrMagnitude < 0.1f &&
                this.rectTransform.parent.childCount > 2 &&
                this.character == Character.King
            )
            {
                foreach(Transform t in this.rectTransform.parent){
                    if(t.name == "hat off" && t != this.hatOff)
                    {
                        if(RectTransformToWorld((RectTransform)t).
                            Contains(playerRect.center + Vector2.down * playerRect.height * 0.499f))
                        {
                            this.animated.PlayOnce(this.playerAnimationsForMenu.kingHatGrounded);
                            this.droppedAt = this.rectTransform.anchoredPosition;
                            this.fallingSpeed = 0;
                            this.state = State.Falling;
                            this.hatOff = (RectTransform)t;
                        }
                    }
                }
            }
        }
        else if(this.state == State.Falling)
        {
            this.fallingSpeed += rectTransform.sizeDelta.y * 0.01f;
            var distToGround = HitBoxNormalized.y * rectTransform.sizeDelta.y * rectTransform.localScale.y;
            this.rectTransform.anchoredPosition += this.fallingSpeed * Vector2.down;
            if(this.droppedAt.y - this.rectTransform.anchoredPosition.y  > distToGround)
            {
                this.rectTransform.anchoredPosition =
                    new Vector2(this.rectTransform.anchoredPosition.x, this.droppedAt.y - distToGround);

                if(playerRect.Overlaps(hatHitBox) == false)
                    state = State.Grounded;
            }
        }
        else if(this.state == State.Grounded)
        {
            if(playerRect.Overlaps(hatHitBox) == true)
            {
                this.offset = this.rectTransform.anchoredPosition - player.rectTransform.anchoredPosition;
                this.state = State.OnHead;
            }
        }

        if(this.hatOff != null)
        {
            if(playerRect.Overlaps(RectTransformToWorld(this.hatOff)) == false)
                this.hatOff = null;
        }
    }
    
    public static Rect RectTransformToWorld(RectTransform rectTransform)
    {
        var corners = new Vector3[4];
        rectTransform.GetWorldCorners(corners);
        var tl = Vector2.Min(corners[0], corners[2]);
        var br = Vector2.Max(corners[0], corners[2]);
        return new Rect(tl.x, tl.y, br.x - tl.x, br.y - tl.y);
    }
}
