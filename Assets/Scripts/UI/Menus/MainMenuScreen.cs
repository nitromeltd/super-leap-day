using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class MainMenuScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    [Serializable]
    public class Variant
    {
        public int referenceWidth;
        public RectTransform container;
        public PlayerForMenu player;

        public Door doorShop;
        public Door doorCalendar;
        public Door doorCharacterSelect;
        public Door doorArcade;
        public Door doorSettings;
        public Door doorTitle;

        public Lift topLift;
        public Lift bottomLift;
        public RectTransform topPlayerContainer;
        public RectTransform bottomPlayerContainer;

        [NonSerialized] public bool isPlayerOnTopRow = false;

        public Door Door(DoorType type) => type switch
        {
            DoorType.Shop            => this.doorShop,
            DoorType.Calendar        => this.doorCalendar,
            DoorType.CharacterSelect => this.doorCharacterSelect,
            DoorType.Arcade          => this.doorArcade,
            DoorType.Settings        => this.doorSettings,
            DoorType.Title           => this.doorTitle,
            _                        => null
        };
    }

    [Serializable]
    public class Door
    {
        public DoorType type;
        public RectTransform rectTransform;
        public RectTransform playerContainer;
        public UnityEngine.UI.Button button;
        public bool isOnTopRow;
        public Image doorImage;
        public Sprite spriteDoorClosed;
        public Sprite spriteDoorOpen;
    };

    [Serializable]
    public class Lift
    {
        public RectTransform rectTransform;
        public RectTransform playerContainer;
        public Animator animator;
    }

    public enum DoorType
    {
        Shop, Calendar, CharacterSelect, Arcade, Settings, Title
    }

    public static DoorType? enterSceneThroughDoor;

    public Variant variantPortraitTall;
    public Variant variantPortraitShort;
    public Variant variantLandscape;
    public AudioClip music;
    public Coroutine walkingCoroutine;

    private DoorType? doorTypeTarget;

    public void Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        SetupVariant(this.variantLandscape);
        SetupVariant(this.variantPortraitShort);
        SetupVariant(this.variantPortraitTall);

        if (enterSceneThroughDoor != null)
        {
            var button = ActiveVariant().Door(enterSceneThroughDoor.Value).button;
            this.selectionCursor.Select(button, false);
            ActiveVariant().player.kingHat.state = KingHatForMenu.State.OnHead;
        }
        else
        {
            var button = ActiveVariant().doorCalendar.button;
            this.selectionCursor.Select(button, false);
        }

        enterSceneThroughDoor = null;
        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, options: new Audio.Options(1f, false, 0f));
        GameCenter.ShowAccessPoint();
    }

    public void Update()
    {
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortraitShort.container.gameObject.SetActive(
            activeVariant == this.variantPortraitShort
        );
        this.variantPortraitTall.container.gameObject.SetActive(
            activeVariant == this.variantPortraitTall
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);

        if(GameInput.pressedMenu)
            OnClickButton(DoorType.Title);
    }

    private void SetupVariant(Variant v)
    {
        // so that the inactive variants can start the player walking into the scene
        v.container.gameObject.SetActive(true);

        {
            var doors = new (string name, DoorType type)[] {
                ("Shop Door",             DoorType.Shop),
                ("Calendar Door",         DoorType.Calendar),
                ("Character Select Door", DoorType.CharacterSelect),
                ("Arcade Door",           DoorType.Arcade),
                ("Settings Door",         DoorType.Settings),
                ("Title Door",            DoorType.Title),
            };

            foreach (var door in doors)
            {
                var doorObject =
                    v.container.Find(            $"{door.name}") ??
                    v.container.Find($"Bottom Half/{door.name}") ??
                    v.container.Find(   $"Top Half/{door.name}");
                var button =
                    doorObject.Find("Button").GetComponent<UnityEngine.UI.Button>();
                var doorway =
                    doorObject.Find("Doorway").GetComponent<UnityEngine.UI.Button>();
                button .onClick.AddListener(() => { OnClickButton(door.type); });
                doorway.onClick.AddListener(() => { OnClickButton(door.type); });
            }

        }

        if (enterSceneThroughDoor != null)
        {
            var door = v.Door(enterSceneThroughDoor.Value);
            door.doorImage.sprite = door.spriteDoorOpen;
            Audio.instance.PlaySfx(sfxForMenu.sfxUIDoorOpen, options: new Audio.Options(1f, false, 0f));
            v.player.rectTransform.SetParent(door.playerContainer);
            v.player.shouldClimbWithStairs = false;

            v.isPlayerOnTopRow = door.isOnTopRow;

            var doorX = v.player.TargetXForObject(door.rectTransform);
            v.player.rectTransform.anchoredPosition = new Vector2(doorX + 400, 0);
            v.player.WalkToX(doorX - 100, () =>
            {
                door.doorImage.sprite = door.spriteDoorClosed;
                Audio.instance.PlaySfx(sfxForMenu.sfxUIDoorClose);
            });
        }
        else
        {
            var stairsX = v.player.TargetXForObject(v.player.stairs);
            v.player.rectTransform.anchoredPosition = new Vector2(stairsX - 400, 0);
            v.player.WalkToX(stairsX + 200);
            v.player.canPlayStairSfx = true;
        }
    }

    public Variant ActiveVariant()
    {
        if (GameCamera.IsLandscape() == true)
            return this.variantLandscape;
        else if ((float)Screen.height / (float)Screen.width > 1.9f)
            return this.variantPortraitTall;
        else
            return this.variantPortraitShort;
    }

    public void OnClickButton(DoorType door)
    {
        if(this.doorTypeTarget == door)
            return;
        
        this.doorTypeTarget = door;
        
        if (this.walkingCoroutine != null)
            StopCoroutine(this.walkingCoroutine);

        this.walkingCoroutine = StartCoroutine(WalkToDoor(door));

        this.selectionCursor.gameObject.SetActive(false);
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(1f, false, 0f));
        GameCenter.HideAccessPoint();

        // player can click several buttons, leaving one still selected
        UnityEngine.EventSystems.EventSystem.current?.SetSelectedGameObject(null);
    }

    private IEnumerator WalkToDoor(DoorType doorType)
    {
        var variant = ActiveVariant();
        var player  = variant.player;
        var door    = variant.Door(doorType);

        if (variant.isPlayerOnTopRow == true)
            variant.player.rectTransform.SetParent(variant.topPlayerContainer);
        else
            variant.player.rectTransform.SetParent(variant.bottomPlayerContainer);

        // take the lift if needed
        if (variant.isPlayerOnTopRow != door.isOnTopRow)
        {
            Lift fromLift, toLift;
            RectTransform toContainer;
            string arrowAnimationName;

            if (variant.isPlayerOnTopRow == true)
            {
                (fromLift, toLift) = (variant.topLift, variant.bottomLift);
                toContainer = variant.bottomPlayerContainer;
                arrowAnimationName = "Arrow Animate Down";
            }
            else
            {
                (fromLift, toLift) = (variant.bottomLift, variant.topLift);
                toContainer = variant.topPlayerContainer;
                arrowAnimationName = "Arrow Animate Up";
            }

            var liftX = player.TargetXForObject(fromLift.rectTransform);

            // walk to the lift
            yield return player.WalkToXCoroutine(liftX);

            // get in
            fromLift.animator.Play("Lift Open");
            if (!Audio.instance.IsSfxPlaying(sfxForMenu.sfxUIElevator))
            {
                Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator, options: new Audio.Options(1f, false, 0f));
            }
            yield return new WaitForSeconds(0.5f);
            player.transform.SetParent(fromLift.playerContainer);
            fromLift.animator.Play("Lift Close");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator2floorClose, options: new Audio.Options(1f, false, 0f));
            yield return new WaitForSeconds(0.3f);
            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevatorFloorchange, options: new Audio.Options(1f, false, 0f));
            fromLift.animator.Play(arrowAnimationName, 1);
            yield return new WaitForSeconds(0.5f);

            // switch to other row
            variant.isPlayerOnTopRow = door.isOnTopRow;
            player.transform.SetParent(toLift.playerContainer);
            player.rectTransform.anchoredPosition = new Vector2(
                player.rectTransform.anchoredPosition.x, 0
            );

            // get out
            toLift.animator.Play("Lift Open");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator2floorOpen, options: new Audio.Options( 1f, false, 0f));
            yield return new WaitForSeconds(0.5f);
            player.transform.SetParent(toContainer);
            toLift.animator.Play("Lift Close");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIElevator2floorClose, options: new Audio.Options(1f, false, 0f));
        }

        // is it the stairs?
        if(doorType == DoorType.Title)
        {
            var stairsX = player.TargetXForObject(player.stairs) - 400;
            player.canPlayStairSfx = true;
            player.shouldClimbWithStairs = true;
            yield return player.WalkToXCoroutine(stairsX);
            MainMenuScreen.enterSceneThroughDoor = null;

            #if UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX
            {
                Application.Quit();
            }
            #endif

            Transition.GoSimple("Title");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn);
            yield break;
        }

        // walk up to the door
        {
            var targetX = player.TargetXForObject(door.rectTransform) - 100;
            yield return player.WalkToXCoroutine(targetX);
        }

        {
            door.doorImage.sprite = door.spriteDoorOpen;
            player.transform.SetParent(door.playerContainer);
            Audio.instance.PlaySfx(sfxForMenu.sfxUIDoorOpen, options: new Audio.Options(1f, false, 0f));
            yield return new WaitForSeconds(0.2f);
        }

        // walk into the door
        {
            var targetWorld = door.rectTransform.TransformPoint(Vector2.zero);
            var playerTarget = player.transform.parent.InverseTransformPoint(targetWorld);
            yield return player.WalkToXCoroutine(playerTarget.x + 500);
        }

        switch (door.type)
        {
            case DoorType.Shop:
                Transition.GoSimple("Shop");
                break;
            case DoorType.Calendar:
                Transition.GoSimple("Calendar");
                break;
            case DoorType.CharacterSelect:
                Transition.GoSimple("Character Select");
                break;
            case DoorType.Arcade:
                Transition.GoSimple("Arcade");
                break;
            case DoorType.Settings:
                Transition.GoSimple("Settings");
                break;
            case DoorType.Title:
                Transition.GoSimple("Title");
                break;
        }

        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn);
    }
}
