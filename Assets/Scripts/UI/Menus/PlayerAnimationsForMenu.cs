using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Leap Day 2/Player Animations for Menu")]
public class PlayerAnimationsForMenu : ScriptableObject
{
    [Serializable]
    public class PlayerAnimations
    {
        public Animated.Animation animationIdle;
        public Animated.Animation animationRun;
        public Animated.Animation animationSitDown;
        public Animated.Animation animationStandUp;
        public Animated.Animation animationJumpUp;
        public Animated.Animation animationJumpDown;
    };
    public PlayerAnimations yolk;
    public PlayerAnimations puffer;
    public PlayerAnimations pufferHead;
    public PlayerAnimations goop;
    public PlayerAnimations sprout;
    public PlayerAnimations king;
    public PlayerAnimations kingHat;
    public Animated.Animation kingHatGrounded;

    public PlayerAnimations AnimationsForCharacter(Character character) =>
        character switch
        {
            Character.Puffer => this.puffer,
            Character.Goop   => this.goop,
            Character.Sprout => this.sprout,
            Character.King   => this.king,
            _                => this.yolk,
        };

}
