using UnityEngine;
using System;
using System.Collections;

public class PlayerForMenu : MonoBehaviour
{
    public RectTransform rectTransform;
    public Animated animated;
    public PlayerAnimationsForMenu playerAnimationsForMenu;
    public RectTransform stairs;
    public RectTransform containerWhenNearStairs;
    public KingHatForMenu kingHat;

    [NonSerialized] public PlayerAnimationsForMenu.PlayerAnimations playerAnimations;
    [NonSerialized] public Character character;

    [NonSerialized] public bool shouldClimbWithStairs = true;
    public Character Character { get => this.character; }

    public SfxForMenu sfxForMenu;

    private class Walk
    {
        public float targetX;
        public Action onComplete;
    };

    private Walk walk;
    private int delayBeforeStart = 40;
    private bool sitting = false;
    private bool animatingSit = false;
    private RectTransform parentBeforeSwitchingIntoStairsContainer;

    [NonSerialized] public bool canPlayStairSfx = false;

    public void Start()
    {
        RefreshSelectedCharacter();
        this.animated.PlayAndLoop(this.playerAnimations.animationIdle);
        
        CheckReparentingAgainstStairsContainer();
        this.canPlayStairSfx = false;
    }

    public void RefreshSelectedCharacter()
    {
        this.character = SaveData.GetSelectedCharacter();
        this.playerAnimations =
            this.playerAnimationsForMenu.AnimationsForCharacter(this.character);

        this.kingHat.RefreshSelectedCharacter();
        this.kingHat.gameObject.SetActive(this.kingHat.headAnimations != null);
    }

    public void Update()
    {
        if (this.delayBeforeStart > 0)
        {
            this.delayBeforeStart -= 1;
        }
        else if (this.animatingSit == true)
        {
            // wait before walking
        }
        else if (this.walk != null)
        {
            if (StepTowardsX(this.walk.targetX) == false)
            {
                this.walk.onComplete?.Invoke();
                this.walk = null;
                Audio.instance.StopSfxLoop(sfxForMenu.sfxPlayerFootstep);
            }
        }

        CheckReparentingAgainstStairsContainer();
        CheckForStairsSfx();

        if(this.kingHat.headAnimations != null)
            this.kingHat.TrackPlayer(this);
    }

    private void CheckReparentingAgainstStairsContainer()
    {
        if (this.stairs == null || this.containerWhenNearStairs == null)
            return;

        var stairsX = TargetXForObject(this.stairs);
        var thresholdX = stairsX + (110 * Mathf.Abs(this.transform.localScale.x));

        if (this.rectTransform.anchoredPosition.x < thresholdX &&
            this.shouldClimbWithStairs == true)
        {
            if (this.transform.parent != this.containerWhenNearStairs)
            {
                var targetWorld = this.rectTransform.parent.TransformPoint(
                    new Vector2(this.walk?.targetX ?? 0, 0)
                );

                this.parentBeforeSwitchingIntoStairsContainer =
                    (RectTransform)this.transform.parent;
                this.rectTransform.SetParent(this.containerWhenNearStairs);

                if (this.walk != null)
                {
                    var target =
                        this.rectTransform.parent.InverseTransformPoint(targetWorld);
                    this.walk.targetX = target.x;
                }
            }
        }
        else
        {
            if (this.transform.parent == this.containerWhenNearStairs &&
                this.parentBeforeSwitchingIntoStairsContainer != null)
            {
                var targetWorld = this.rectTransform.parent.TransformPoint(
                    new Vector2(this.walk?.targetX ?? 0, 0)
                );

                this.rectTransform.SetParent(
                    this.parentBeforeSwitchingIntoStairsContainer
                );

                if (this.walk != null)
                {
                    var target =
                        this.rectTransform.parent.InverseTransformPoint(targetWorld);
                    this.walk.targetX = target.x;
                }
            }
            this.parentBeforeSwitchingIntoStairsContainer = null;
        }
    }

    public void Sit()
    {
        if (this.sitting == true)
            return;

        this.sitting = true;
        this.animatingSit = true;
        this.animated.PlayOnce(this.playerAnimations.animationSitDown, () =>
        {
            this.animatingSit = false;
        });
    }

    public void Stand()
    {
        if (this.sitting == false)
            return;

        this.sitting = false;
        this.animatingSit = true;
        this.animated.PlayOnce(this.playerAnimations.animationStandUp, () =>
        {
            this.animated.PlayAndLoop(this.playerAnimations.animationIdle);
            this.animatingSit = false;
        });
    }

    public void Idle()
    {
        this.animated.PlayAndLoop(this.playerAnimations.animationIdle);
    }

    public void WalkToX(float toAnchoredPositionX, Action onComplete = null)
    {
        this.walk = new Walk
        {
            targetX = toAnchoredPositionX,
            onComplete = onComplete
        };
        if (!Audio.instance.IsSfxPlaying(sfxForMenu.sfxPlayerFootstep))
        {
            Audio.instance.PlaySfxLoop(sfxForMenu.sfxPlayerFootstep);
        }
    }

    public void DisplaceX(float distance)
    {
        if(this.walk != null) this.walk.targetX += distance;
        this.rectTransform.anchoredPosition += Vector2.right * distance;
    }

    public IEnumerator WalkToXCoroutine(float toAnchoredPositionX)
    {
        var walk = this.walk = new Walk
        {
            targetX = toAnchoredPositionX,
            onComplete = null
        };

        if (!Audio.instance.IsSfxPlaying(sfxForMenu.sfxPlayerFootstep))
        {
            Audio.instance.PlaySfxLoop(sfxForMenu.sfxPlayerFootstep);
        }

        while (this.walk == walk)
        {
            yield return null;
        }
    }

    public IEnumerator JumpToCoroutine(Vector2 anchoredPositionTarget, Action onComplete = null)
    {
        var position = this.rectTransform.anchoredPosition;
        var delta = anchoredPositionTarget - position;
        if(delta.y > 0)
            this.animated.PlayOnce(playerAnimations.animationJumpUp);
        else
            this.animated.PlayOnce(playerAnimations.animationJumpDown);
        float jumpDelay = 0.15f;
        float halfPI = Mathf.PI / 2;
        float rStep = halfPI / (60f * jumpDelay);
        for(float r = 0; r < halfPI; r += rStep)
        {
            this.rectTransform.anchoredPosition =
                position +
                new Vector2
                (
                    Mathf.Lerp(0, delta.x, r / halfPI),
                    (delta.y > 0 ? Mathf.Sin(r) : (1f - Mathf.Sin(halfPI - r))) * delta.y
                );

            yield return null;
        }
        onComplete?.Invoke();
    }

    public void CancelWalkAndStopHere()
    {
        Audio.instance.StopSfxLoop(sfxForMenu.sfxPlayerFootstep);
        this.walk = null;
    }

    public float TargetXForObject(RectTransform target)
    {
        var targetWorld = target.TransformPoint(Vector2.zero);
        var x = this.rectTransform.parent.InverseTransformPoint(targetWorld).x;

        var parent = (RectTransform)this.rectTransform.parent;
        var anchorX =
            (this.rectTransform.anchorMin.x + this.rectTransform.anchorMax.x) / 2;
        x += (parent.pivot.x - anchorX) * parent.rect.width;
        return x;
    }

    private bool StepTowardsX(float toAnchoredPositionX)
    {
        float scale = Mathf.Abs(this.rectTransform.localScale.x);
        float x = this.rectTransform.anchoredPosition.x;
        x = Util.Slide(x, toAnchoredPositionX, Time.deltaTime * 720 * scale);

        float y = 0;
        if (this.shouldClimbWithStairs == true && this.stairs != null)
        {
            var stairsX = TargetXForObject(this.stairs) - (60 * scale);
            y = Mathf.Max(stairsX - x, 0);
        }
        this.rectTransform.anchoredPosition = new Vector2(x, y);

        if (this.animatingSit == false)
        {
            if (x != toAnchoredPositionX)
            {
                this.animated.PlayAndLoop(this.playerAnimations.animationRun);
            }
            else
            {
                this.animated.PlayAndLoop(this.playerAnimations.animationIdle);
            }
        }

        if (toAnchoredPositionX < x)
            FaceLeft();
        else
            FaceRight();

        return !Mathf.Approximately(x, toAnchoredPositionX);
    }

    private void CheckForStairsSfx()
    {
        if (this.stairs == null) return;
        if (this.rectTransform.anchoredPosition.x < TargetXForObject(this.stairs) + (90 * Mathf.Abs(this.transform.localScale.x)) && this.rectTransform.anchoredPosition.x > TargetXForObject(this.stairs) - (200 * Mathf.Abs(this.transform.localScale.x)) && this.shouldClimbWithStairs == true)
        {
            if (this.canPlayStairSfx == true)
            {
                Audio.instance.PlaySfx(sfxForMenu.sfxUIStairs, options: new Audio.Options(1, false, 0));
                this.canPlayStairSfx = false;
            }
        }
    }
    public void FaceRight()
    {
        var scale = this.rectTransform.localScale;
        this.rectTransform.localScale = new Vector3(Mathf.Abs(scale.x), scale.y, 1);
    }

    public void FaceLeft()
    {
        var scale = this.rectTransform.localScale;
        this.rectTransform.localScale = new Vector3(-Mathf.Abs(scale.x), scale.y, 1);
    }

    public bool IsWalking()
    {
        return this.walk != null;
    }

}
