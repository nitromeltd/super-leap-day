﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;
using UnityEngine.UI;

public class PlushToyAd : MonoBehaviour
{
    [HideInInspector] public bool canContinue = false;

    private TMP_Text textDaysNumber;
    private LocalizeText textDays;
    private Toggle toggleDontShowAgain;

    // TEST
    //public static DateTime DateStart = DateTime.UtcNow;
    //public static DateTime DateFinish = new DateTime(2022, 1, 20, 10, 12, 00);

    public static DateTime DateStart = new DateTime(2022, 2, 11, 12, 00, 00);
    public static DateTime DateFinish = new DateTime(2022, 3, 4, 23, 59, 00);

    private void Awake()
    {
        this.textDaysNumber = this.transform.Find("Panel Container/panel/days number").GetComponent<TMP_Text>();
        this.textDays = this.transform.Find("Panel Container/panel/days").GetComponent<LocalizeText>();
        this.toggleDontShowAgain = this.transform.Find("Panel Container/panel/Dont Show Again Toggle").GetComponent<Toggle>();
        this.toggleDontShowAgain.isOn = SaveData.IsDontShowPlushToyAd();

        // GMT time. DateTime.TotalDays returns difference rounded down, we want to round up
        var dateNow = DateTime.UtcNow;
        var days = Mathf.CeilToInt((float)(DateFinish - dateNow).TotalMinutes / 60f / 24f);
        this.textDaysNumber.text = days.ToString();
        if(Localization.activeLanguage == Localization.LanguageId.Korean)
        {
            this.textDaysNumber.text += "일";
        }
        if(days == 1)
            this.textDays.SetText("DAY!");
    }

    public bool CanShow()
    {
        return
            DateTime.UtcNow >= DateStart &&
            DateTime.UtcNow <= DateFinish &&
            SaveData.IsDontShowPlushToyAd() == false;
    }

    public void OnClickContinue()
    {
        this.canContinue = true;
    }

    public void OnClickMoreInfo()
    {
        Application.OpenURL("https://shop.makeship.com/3G8exBV");
    }

    public void OnToggleDontShowAgain()
    {
        SaveData.SetDontShowPlushToyAd(this.toggleDontShowAgain.isOn);
    }


}
