using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class SettingsScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    public enum Panel
    {
        SfxVolume,
        MusicVolume,
        HapticFeedback,
        Language,
        Credits,
        SaveSlots,
        DeleteSaveSlot
    }

    [Serializable]
    public class Variant
    {
        public int referenceWidth;
        public RectTransform container;
        public PlayerForMenu player;
        public RectTransform panelSfxVolume;
        public RectTransform panelMusicVolume;
        public RectTransform panelHapticFeedback;
        public RectTransform panelLanguage;
        public RectTransform panelCredits;
        public RectTransform panelSaveSlots;
        public RectTransform panelDeleteSaveSlot;
        public Slider sfxVolumeSlider;
        public Slider musicVolumeSlider;
        public ScrollRect scrollRectLanguage;
        public UnityEngine.UI.Button sideButtonMusic;
        public UnityEngine.UI.Button sideButtonSfx;
        public UnityEngine.UI.Button sideButtonHaptic;
        public UnityEngine.UI.Button sideButtonLanguage;
        public UnityEngine.UI.Button sideButtonCredits;
        public UnityEngine.UI.Button sideButtonSaveSlots;
        public Animated hapticFeedbackButtonAnimated;
        public Animated characterChangePuffAnimated;
        [NonSerialized] public LanguageRow[] languageRows;
        [NonSerialized] public SettingsScreenSaveSlot[] saveSlots;
    };

    public class LanguageRow
    {
        public int index;
        public Localization.LanguageId languageId;
        public RectTransform rectTransform;
        public RectTransform selectionBorder;
        public RectTransform activeLanguage;
    };
    public Variant variantPortraitTall;
    public Variant variantPortraitShort;
    public Variant variantLandscape;
    public Image whiteFlashImage;

    public Sprite sfxOnNormalSprite;
    public Sprite sfxOnHoverSprite;
    public Sprite sfxOnPressedSprite;
    public Sprite sfxOffNormalSprite;
    public Sprite sfxOffHoverSprite;
    public Sprite sfxOffPressedSprite;
    public Sprite musicOnNormalSprite;
    public Sprite musicOnHoverSprite;
    public Sprite musicOnPressedSprite;
    public Sprite musicOffNormalSprite;
    public Sprite musicOffHoverSprite;
    public Sprite musicOffPressedSprite;
    public Sprite hapticOnNormalSprite;
    public Sprite hapticOnHoverSprite;
    public Sprite hapticOnPressedSprite;
    public Sprite hapticOffNormalSprite;
    public Sprite hapticOffHoverSprite;
    public Sprite hapticOffPressedSprite;

    public Animated.Animation animationToggleTurnOn;
    public Animated.Animation animationToggleTurnOff;
    public Animated.Animation animationChangeCharacterPuff;

    public float panelRaisedAmount = 1;
    public Panel? selectedPanel = null;
    public Panel? currentlyVisiblePanel = null;
    public Coroutine currentPanelSwitchCoroutine;

    private int saveSlotTarget;

    public AudioClip music;
    /* Dev Code:
     * Credits, Reset, Credits, Reset,
     * Language, Credits, Music, Credits,
     * Sound, Credits, Language, Credits
     */
    private const string DevCode = "010140203040";
    private const string DevClearCode = "111111111111";
    private string devCodeEntered;

    public IEnumerator Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        SetupVariant(this.variantPortraitTall);
        SetupVariant(this.variantPortraitShort);
        SetupVariant(this.variantLandscape);
        UpdatePanelPlacement();
        UpdateLanguageSelection();

        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, null, new Vector3(0, 0, 0));

        this.devCodeEntered = DevCode;

        // create slot name if we don't have one
        if(SaveData.DoesSlotExist(SaveData.currentSlotNumber) == false)
            SaveData.SetSlotName(SaveData.currentSlotNumber, "");

        this.selectionCursor.Select(ActiveVariant().container.Find("Exit Button")?.gameObject.GetComponent<UnityEngine.UI.Button>(), true);

        // short and landscape use a stretch container that is ignoring Canvas.ForceUpdateCanvases and LayoutRebuilder.ForceRebuildLayoutImmediate
        yield return null;
        PlayerWalkIn(this.variantPortraitTall);
        PlayerWalkIn(this.variantPortraitShort);
        PlayerWalkIn(this.variantLandscape);
    }


    private void SetupVariant(Variant v)
    {
        {
            var languages = new []
            {
                "English (American)",
                "English (English)",
                "French (France)",
                "French (Canadian)",
                "Italian",
                "German",
                "Spanish",
                "Chinese (Simplified)",
                "Chinese (Traditional)",
                "Japanese",
                "Korean",
                "Russian",
                "Turkish",
                "Arabic",
                "Portuguese (Brazilian)",
                "Dutch"
            };

            var languageIds = new[]
            {
                Localization.LanguageId.EnglishUS,
                Localization.LanguageId.EnglishUK,
                Localization.LanguageId.French,
                Localization.LanguageId.FrenchCanadian,
                Localization.LanguageId.Italian,
                Localization.LanguageId.German,
                Localization.LanguageId.Spanish,
                Localization.LanguageId.ChineseSimplified,
                Localization.LanguageId.ChineseTraditional,
                Localization.LanguageId.Japanese,
                Localization.LanguageId.Korean,
                Localization.LanguageId.Russian,
                Localization.LanguageId.Turkish,
                Localization.LanguageId.Arabic,
                Localization.LanguageId.Portuguese,
                Localization.LanguageId.Dutch
            };

            v.languageRows = new LanguageRow[languages.Length];

            var item = v.container.Find(
                "Center/Languages Panel/List/Viewport/Content/Item"
            );
            item.gameObject.name = languages[0];
            item.Find("Text").GetComponent<TMPro.TextMeshProUGUI>().text = languages[0];

            v.languageRows[0] = new LanguageRow
            {
                index = 0,
                rectTransform = item.GetComponent<RectTransform>(),
                selectionBorder = item.Find("Selection").GetComponent<RectTransform>(),
                activeLanguage = item.Find("Active Language").GetComponent<RectTransform>()
            };

            var list = new List<RectTransform>();
            var currentActiveLanguage = Localization.activeLanguage;
            for (int n = 1; n < languages.Length; n += 1)
            {
                var copy = Instantiate(item.gameObject, item.parent);
                copy.transform.SetAsLastSibling();
                copy.name = languages[n];

                Localization.Init(languageIds[n]);
                var tmp = copy.transform.Find("Text").GetComponent<TMPro.TMP_Text>();
                tmp.text = Localization.GetText(languages[n].ToUpper());
                Localization.SetTextFont(tmp);
                
                copy.GetComponent<Image>().enabled = (n % 2 == 0);

                v.languageRows[n] = new LanguageRow
                {
                    index = n,
                    languageId = languageIds[n],
                    rectTransform = copy.GetComponent<RectTransform>(),
                    selectionBorder =
                        copy.transform.Find("Selection").GetComponent<RectTransform>(),
                    activeLanguage =
                        copy.transform.Find("Active Language").GetComponent<RectTransform>()
                };
            }
            Localization.Init(currentActiveLanguage);

            foreach (var row in v.languageRows)
            {
                var button = row.rectTransform.GetComponent<UnityEngine.UI.Button>();
                button.onClick.AddListener(delegate
                {
                    UpdateLanguageSelection();
                    SaveData.SetSelectedLanguage(row.languageId);
                    Localization.Init();
                    Localization.UpdateAllText();

                    SwitchToPanel(null);
                    this.selectionCursor.Select(ActiveVariant().sideButtonLanguage, true);
                });
            }
        }

        v.saveSlots = v.panelSaveSlots.GetComponentsInChildren<SettingsScreenSaveSlot>();
        foreach(var saveSlot in v.saveSlots)
        {
            saveSlot.buttonDelete.onClick.AddListener(() => {
                OnClickSaveDataSlotDelete(saveSlot.slot);
            });
            saveSlot.buttonActivate.onClick.AddListener(() => {
                OnClickSaveDataSlotActivate(saveSlot.slot);
            });
            saveSlot.buttonRename.onClick.AddListener(() =>
            {
                OnClickRenameSaveSlot(saveSlot.slot);
            });
            saveSlot.inputName.onEndEdit.AddListener(OnEndEditRenameSaveSlot);
        }

        v.sideButtonSfx.onClick.AddListener(() => { 
            OnClickPanelButton(Panel.SfxVolume);
            OnClickDevCode(3);
        });
        v.sideButtonMusic.onClick.AddListener(() => { 
            OnClickPanelButton(Panel.MusicVolume);
            OnClickDevCode(2);
        });
        v.sideButtonHaptic?.onClick.AddListener(() => {
            OnClickPanelButton(Panel.HapticFeedback);
        });
        v.sideButtonLanguage.onClick.AddListener(() => { 
            OnClickPanelButton(Panel.Language);
            v.panelLanguage.gameObject.SetActive(true);
            Canvas.ForceUpdateCanvases();
            ScrollToSelection(v.scrollRectLanguage, v.languageRows.Single(r => r.languageId == Localization.activeLanguage).rectTransform);
            OnClickDevCode(4);
        });
        v.sideButtonCredits.onClick.AddListener(() => {
            OnClickPanelButton(Panel.Credits);
            OnClickDevCode(0);
        });
        v.sideButtonSaveSlots.onClick.AddListener(() => {
            OnClickPanelButton(Panel.SaveSlots);
            OnClickDevCode(1);
        });

        v.sfxVolumeSlider.value = SaveData.GetSfxVolume();
        v.musicVolumeSlider.value = SaveData.GetMusicVolume();
        v.hapticFeedbackButtonAnimated?.PlayOnce(
            SaveData.IsHapticFeedbackEnabled() ?
            this.animationToggleTurnOn :
            this.animationToggleTurnOff
        );
    }

    private void PlayerWalkIn(Variant v)
    {
        var stairsX = v.player.TargetXForObject(v.player.stairs);
        v.player.rectTransform.anchoredPosition = new Vector2(stairsX - 400, 0);
        if(v == this.variantLandscape)
            v.player.WalkToX(stairsX + 500);
        else
            v.player.WalkToX(stairsX + 300);
        var hatRectTransform = (RectTransform)v.player.kingHat.transform;
        hatRectTransform.anchoredPosition = new Vector2(stairsX + 100, hatRectTransform.anchoredPosition.y);
    }


    public void OnClickPanelButton(Panel panel)
    {
        SwitchToPanel(panel);
    }

    private void SwitchToPanel(Panel? panel)
    {
        if (this.selectedPanel != panel)
        {
            this.selectedPanel = panel;

            if (this.currentPanelSwitchCoroutine == null)
                this.currentPanelSwitchCoroutine = StartCoroutine(Switch());

            this.selectionCursor.RemoveConstraint();
        }

        IEnumerator Switch()
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(.5f, false, 0));

            if (this.currentlyVisiblePanel != null)
            {
                var tweener = new Tweener();
                var tween = tweener.TweenFloat(
                    this.panelRaisedAmount, 1, 0.2f, Easing.QuadEaseIn
                );
                Audio.instance.PlaySfx(sfxForMenu.sfxUISettingsChainUp);
                while (tweener.IsDone() == false)
                {
                    tweener.Advance(Time.deltaTime);
                    this.panelRaisedAmount = tween.Value();
                    UpdatePanelPlacement();
                    yield return null;
                }
            }

            this.currentlyVisiblePanel = this.selectedPanel;
            UpdatePanelPlacement();

            if(this.selectedPanel != null)
            {
                var tweener = new Tweener();
                var tween = tweener.TweenFloat(
                    this.panelRaisedAmount, 0, 0.3f, Easing.FlashStyle(0.2f)
                );
                Audio.instance.PlaySfx(sfxForMenu.sfxUISettingsChainDown);
                while (tweener.IsDone() == false)
                {
                    tweener.Advance(Time.deltaTime);
                    this.panelRaisedAmount = tween.Value();
                    UpdatePanelPlacement();
                    yield return null;
                }
            }

            this.currentPanelSwitchCoroutine = null;

            if (this.currentlyVisiblePanel != this.selectedPanel)
                StartCoroutine(Switch()); // again!

            if (this.currentlyVisiblePanel == this.selectedPanel)
            {
                if (this.selectedPanel == Panel.SfxVolume)
                    this.selectionCursor.Select(ActiveVariant().sfxVolumeSlider, true);
                if (this.selectedPanel == Panel.MusicVolume)
                    this.selectionCursor.Select(ActiveVariant().musicVolumeSlider, true);
                if (this.selectedPanel == Panel.Language)
                {
                    this.selectionCursor.Select(ActiveVariant().languageRows.First(row => row.languageId == Localization.activeLanguage).
                        rectTransform.GetComponent<UnityEngine.UI.Button>(), true);
                    this.selectionCursor.ConstrainMovement(this.selectionCursor.selection.transform.parent.gameObject);
                }
                if (this.selectedPanel == Panel.Credits)
                {
                    this.selectionCursor.Select(ActiveVariant().panelCredits.Find("Image/Button").GetComponent<UnityEngine.UI.Button>(), true);
                    this.selectionCursor.ConstrainMovement(this.selectionCursor.selection.transform.parent.gameObject);
                }
                if (this.selectedPanel == Panel.HapticFeedback)
                    this.selectionCursor.Select(ActiveVariant().panelHapticFeedback.Find("Image/Button").GetComponent<UnityEngine.UI.Button>());
                if (this.selectedPanel == Panel.SaveSlots)
                    this.selectionCursor.Select(GetSaveSlot(SaveData.currentSlotNumber).buttonActivate);
                if (this.selectedPanel == Panel.DeleteSaveSlot)
                    this.selectionCursor.Select(ActiveVariant().panelDeleteSaveSlot.Find("No Button").GetComponent<UnityEngine.UI.Button>());

            }
        }
    }

    private void UpdatePanelPlacement()
    {
        void UpdateVariant(Variant v)
        {
            float h = 2500;
            float y = h * this.panelRaisedAmount;

            var activePanel = this.currentlyVisiblePanel switch
            {
                Panel.MusicVolume    => v.panelMusicVolume,
                Panel.SfxVolume      => v.panelSfxVolume,
                Panel.HapticFeedback => v.panelHapticFeedback,
                Panel.Language       => v.panelLanguage,
                Panel.Credits        => v.panelCredits,
                Panel.SaveSlots      => v.panelSaveSlots,
                Panel.DeleteSaveSlot => v.panelDeleteSaveSlot,
                _                    => null
            };

            foreach (var p in new [] {
                v.panelMusicVolume, v.panelSfxVolume, v.panelHapticFeedback,
                v.panelLanguage, v.panelCredits, v.panelSaveSlots, v.panelDeleteSaveSlot
            })
            {
                p.gameObject.SetActive(p == activePanel);
                p.localPosition = new Vector2(0, p == activePanel ? y : h);
            }
        }

        UpdateVariant(this.variantLandscape);
        UpdateVariant(this.variantPortraitShort);
        UpdateVariant(this.variantPortraitTall);
    }

    public void UpdateButtonImages()
    {
        foreach (var v in new [] {
            this.variantLandscape, this.variantPortraitShort, this.variantPortraitTall
        })
        {
            var musicState = v.sideButtonMusic.spriteState;
            if (SaveData.GetMusicVolume() > 0)
            {
                v.sideButtonMusic.image.sprite = this.musicOnNormalSprite;
                musicState.highlightedSprite   = this.musicOnHoverSprite;
                musicState.pressedSprite       = this.musicOnPressedSprite;
                musicState.selectedSprite      = this.musicOnPressedSprite;
                v.sideButtonMusic.spriteState  = musicState;
            }
            else
            {
                v.sideButtonMusic.image.sprite = this.musicOffNormalSprite;
                musicState.highlightedSprite   = this.musicOffHoverSprite;
                musicState.pressedSprite       = this.musicOffPressedSprite;
                musicState.selectedSprite      = this.musicOffPressedSprite;
                v.sideButtonMusic.spriteState  = musicState;
            }

            var sfxState = v.sideButtonSfx.spriteState;
            if (SaveData.GetSfxVolume() > 0)
            {
                v.sideButtonSfx.image.sprite = this.sfxOnNormalSprite;
                sfxState.highlightedSprite     = this.sfxOnHoverSprite;
                sfxState.pressedSprite         = this.sfxOnPressedSprite;
                sfxState.selectedSprite        = this.sfxOnPressedSprite;
                v.sideButtonSfx.spriteState    = sfxState;
            }
            else
            {
                v.sideButtonSfx.image.sprite = this.sfxOffNormalSprite;
                sfxState.highlightedSprite   = this.sfxOffHoverSprite;
                sfxState.pressedSprite       = this.sfxOffPressedSprite;
                sfxState.selectedSprite      = this.sfxOffPressedSprite;
                v.sideButtonSfx.spriteState  = sfxState;
            }

            if (v.sideButtonHaptic != null)
            {
                var hapticState = v.sideButtonHaptic.spriteState;
                if (SaveData.IsHapticFeedbackEnabled() == true)
                {
                    v.sideButtonHaptic.image.sprite = this.hapticOnNormalSprite;
                    hapticState.highlightedSprite   = this.hapticOnHoverSprite;
                    hapticState.pressedSprite       = this.hapticOnPressedSprite;
                    hapticState.selectedSprite      = this.hapticOnPressedSprite;
                    v.sideButtonHaptic.spriteState  = hapticState;
                }
                else
                {
                    v.sideButtonHaptic.image.sprite = this.hapticOffNormalSprite;
                    hapticState.highlightedSprite   = this.hapticOffHoverSprite;
                    hapticState.pressedSprite       = this.hapticOffPressedSprite;
                    hapticState.selectedSprite      = this.hapticOffPressedSprite;
                    v.sideButtonHaptic.spriteState  = hapticState;
                }
            }
        }
    }

    public void UpdateLanguageSelection()
    {
        foreach (var row in ActiveVariant().languageRows)
        {
            row.activeLanguage.gameObject.SetActive(
                row.languageId == Localization.activeLanguage
            );
            row.selectionBorder.gameObject.SetActive(
                this.selectionCursor.IsActive &&
                this.selectionCursor.selection?.gameObject
                    == row.rectTransform.gameObject
            );
            if(
                this.selectionCursor.IsActive &&
                this.selectionCursor.selection?.gameObject
                    == row.rectTransform.gameObject
            )
            {
                ScrollToSelection(ActiveVariant().scrollRectLanguage, row.rectTransform);
                row.selectionBorder.gameObject.SetActive(true);
            }
            else
                row.selectionBorder.gameObject.SetActive(false);
        }
    }

    public void ScrollToSelection(ScrollRect scrollRect, RectTransform item)
    {
        var itemHeight = item.rect.height;
        var itemY = item.transform.localPosition.y + itemHeight;
        var itemDelta = scrollRect.content.rect.height - itemHeight;
        var targetNormalizedY = -itemY / itemDelta;
        scrollRect.verticalNormalizedPosition = 1f - targetNormalizedY;
    }

    public void RefreshCharacter()
    {
        var v = ActiveVariant();
        if(v.player.Character != SaveData.GetSelectedCharacter())
        {
            (v.characterChangePuffAnimated.transform as RectTransform).anchoredPosition =
                v.player.rectTransform.anchoredPosition + v.player.rectTransform.sizeDelta.y * Vector2.up * 0.5f;
            v.characterChangePuffAnimated.PlayOnce(this.animationChangeCharacterPuff);
            v.characterChangePuffAnimated.transform.SetAsLastSibling();
            v.player.RefreshSelectedCharacter();
            v.player.Idle();
        }
    }

    public Variant ActiveVariant()
    {
        if (GameCamera.IsLandscape() == true)
            return this.variantLandscape;
        else if ((float)Screen.height / (float)Screen.width > 1.9f)
            return this.variantPortraitTall;
        else
            return this.variantPortraitShort;
    }

    public void OnSfxVolumeValueChanged(float value)
    {
        SaveData.SetSfxVolume(value);
        this.variantLandscape    .sfxVolumeSlider.value = value;
        this.variantPortraitShort.sfxVolumeSlider.value = value;
        this.variantPortraitTall .sfxVolumeSlider.value = value;
        UpdateButtonImages();
    }

    public void OnMusicVolumeValueChanged(float value)
    {
        SaveData.SetMusicVolume(value);
        this.variantLandscape    .musicVolumeSlider.value = value;
        this.variantPortraitShort.musicVolumeSlider.value = value;
        this.variantPortraitTall .musicVolumeSlider.value = value;
        UpdateButtonImages();
    }

    public void OnMusicSliderConfirm()
    {
        SwitchToPanel(null);
        this.selectionCursor.Select(ActiveVariant().sideButtonMusic, true);
    }

    public void OnSfxSliderConfirm()
    {
        SwitchToPanel(null);
        this.selectionCursor.Select(ActiveVariant().sideButtonSfx, true);
    }

    public void OnClickHapticToggle()
    {
        var haptic = !SaveData.IsHapticFeedbackEnabled();
        var animation = haptic ? this.animationToggleTurnOn : this.animationToggleTurnOff;

        SaveData.SetHapticFeedbackEnabled(haptic);
        this.variantLandscape    .hapticFeedbackButtonAnimated.PlayOnce(animation);
        this.variantPortraitShort.hapticFeedbackButtonAnimated.PlayOnce(animation);
        this.variantPortraitTall .hapticFeedbackButtonAnimated.PlayOnce(animation);
        UpdateButtonImages();
    }

    public SettingsScreenSaveSlot GetSaveSlot(int slot)
    {
        foreach(var saveSlot in ActiveVariant().saveSlots)
            if(saveSlot.slot == slot) return saveSlot;
        return null;
    }

    public void OnClickSaveDataSlotActivate(int slot)
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm);
        
        SaveData.currentSlotNumber = slot;

        if(SaveData.DoesSlotExist(slot) == false)
        {
            OnClickRenameSaveSlot(slot);
            return;
        }
        
        if (SaveData.GetLastSaveTimestamp(slot) == null)
            SaveData.UpdateTimestampAndSave(slot);

        var v = ActiveVariant();
        foreach(var saveSlot in v.saveSlots)
            saveSlot.UpdateInfo();
        RefreshCharacter();
    }

    public void OnClickSaveDataSlotDelete(int slot)
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm);
        this.saveSlotTarget = slot;
        SwitchToPanel(Panel.DeleteSaveSlot);
    }

    public void OnClickSaveDataSlotDeleteYes()
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm);

        SaveData.DeleteSaveSlot(this.saveSlotTarget);
        GetSaveSlot(this.saveSlotTarget).UpdateInfo();
        GetSaveSlot(this.saveSlotTarget).inputName.SetTextWithoutNotify("");

        IEnumerator Go()
        {
            this.whiteFlashImage.gameObject.SetActive(true);

            var tweener = new Tweener();
            tweener.Alpha(this.whiteFlashImage, 1, 0.05f);
            while(!tweener.IsDone())
            {
                tweener.Advance(Time.deltaTime);
                yield return null;
            }

            var totalSlots = 0;
            for(int i = 1; i <= 3; i++)
                if(SaveData.DoesSlotExist(i) == true) totalSlots += 1;

            if(totalSlots > 0 && SaveData.currentSlotNumber == this.saveSlotTarget)
            {
                var n = this.saveSlotTarget;
                while(SaveData.DoesSlotExist(n) == false)
                {
                    n += 1;
                    if(n > 3) n = 1;
                }
                SaveData.currentSlotNumber = this.saveSlotTarget = n;
                RefreshCharacter();
            }
            this.selectedPanel = Panel.SaveSlots;
            this.currentlyVisiblePanel = Panel.SaveSlots;
            UpdatePanelPlacement();
            
            if(totalSlots == 0)
            {
                OnClickRenameSaveSlot(SaveData.currentSlotNumber);
                RefreshCharacter();
            }

            tweener = new Tweener();
            tweener.Alpha(this.whiteFlashImage, 0, 0.5f, Easing.QuadEaseIn, 0.05f);
            while(!tweener.IsDone())
            {
                tweener.Advance(Time.deltaTime);
                yield return null;
            }

            this.whiteFlashImage.gameObject.SetActive(false);

            if(totalSlots > 0)
                this.selectionCursor.Select(GetSaveSlot(this.saveSlotTarget).buttonActivate);
        }
        StartCoroutine(Go());
    }

    public void OnClickSaveDataSlotDeleteNo()
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack);
        SwitchToPanel(Panel.SaveSlots);
    }

    public void OnClickRenameSaveSlot(int slot)
    {
        this.saveSlotTarget = slot;
        var saveSlot = GetSaveSlot(slot);
        foreach(var ss in ActiveVariant().saveSlots)
            ss.buttonRename.gameObject.SetActive(false);
        this.selectionCursor.gameObject.SetActive(false);
        saveSlot.inputName.transform.parent.gameObject.SetActive(true);
        if(saveSlot.inputName.text == "")
            saveSlot.inputName.text = Localization.GetText("INPUT NAME");
        saveSlot.inputName.OnPointerClick(new UnityEngine.EventSystems.PointerEventData(null));
    }

    public void OnEndEditRenameSaveSlot(string text)
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm, options: new Audio.Options(.5f, false, 0));
        var saveSlot = GetSaveSlot(this.saveSlotTarget);
        SaveData.SetSlotName(this.saveSlotTarget, text);
        saveSlot.UpdateInfo();
        foreach(var ss in ActiveVariant().saveSlots)
        {
            ss.buttonRename.gameObject.SetActive(true);
            ss.UpdateInfo();
        }
        this.selectionCursor.gameObject.SetActive(true);
        RefreshCharacter();
        if(EventSystem.current.alreadySelecting == false)
            this.selectionCursor.Select(saveSlot.buttonRename);
    }

    public void OnClickExit()
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack);
        var player = ActiveVariant().player;
        var stairsX = player.TargetXForObject(player.stairs);
        player.canPlayStairSfx = true;

        player.WalkToX(stairsX - 1000, () =>
        {
            MainMenuScreen.enterSceneThroughDoor = MainMenuScreen.DoorType.Settings;
            Transition.GoSimple("Main Menu");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn);
        });
    }

    public void OnClickDevCode(int n)
    {
        devCodeEntered = devCodeEntered.Substring(1) + n;
        //Debug.Log(devCodeEntered);
        if(devCodeEntered == DevCode)
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxNitrome, options: new Audio.Options(1f, false, 0));
            this.whiteFlashImage.gameObject.SetActive(true);
            var tweener = new Tweener();
            tweener.Alpha(this.whiteFlashImage, 1, 0.25f);
            tweener.Alpha(this.whiteFlashImage, 0, 0.25f, Easing.Linear, 0.25f);
            tweener.Callback(0.5f, () => { this.whiteFlashImage.gameObject.SetActive(false); });
            StartCoroutine(tweener.Run());
            SaveData.SetDeveloperModeEnabled(true);
        }
        else if(devCodeEntered == DevClearCode && SaveData.IsDeveloperModeEnabled() == true)
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxKingRespawn, options: new Audio.Options(1f, false, 0));
            SaveData.SetDeveloperModeEnabled(false);
        }
    }

    public void Update()
    {
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortraitShort.container.gameObject.SetActive(
            activeVariant == this.variantPortraitShort
        );
        this.variantPortraitTall.container.gameObject.SetActive(
            activeVariant == this.variantPortraitTall
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);

        UpdateLanguageSelection();

        if(this.currentlyVisiblePanel == Panel.Credits && GameInput.pressedConfirm == true)
        {
            SwitchToPanel(null);
            this.selectionCursor.Select(ActiveVariant().sideButtonCredits);
        }

        if(GameInput.pressedMenu)
        {
            if(this.selectedPanel.HasValue == true)
                switch(this.selectedPanel)
                {
                    case Panel.MusicVolume: OnMusicSliderConfirm(); break;
                    case Panel.SfxVolume: OnSfxSliderConfirm(); break;
                    case Panel.DeleteSaveSlot: OnClickSaveDataSlotDeleteNo(); break;
                    case Panel.Language:
                        this.selectionCursor.Select(ActiveVariant().sideButtonLanguage, true);
                        goto default;
                    case Panel.HapticFeedback:
                        this.selectionCursor.Select(ActiveVariant().sideButtonHaptic, true);
                        goto default;
                    case Panel.Credits:
                        this.selectionCursor.Select(ActiveVariant().sideButtonCredits, true);
                        goto default;
                    case Panel.SaveSlots:
                        this.selectionCursor.Select(ActiveVariant().sideButtonSaveSlots, true);
                        goto default;
                    default: SwitchToPanel(null); break;
                }
            else if(ActiveVariant().player.IsWalking() == false)
                OnClickExit();
        }
    }
}
