using UnityEngine;
using LanguageId = Localization.LanguageId;

public class SettingsScreenCreditsBox : MonoBehaviour
{
    public LocalizeText text;
    private float textHeight;
    public RectTransform scrollRectTransform;

    private Vector2? mouseDownAt;

    public void Start()
    {
        var y = -200f;

        void AddHeader(string text, bool raw = false)
        {
            y -= 60;

            var t = Instantiate(this.text, this.text.transform.parent);
            t.gameObject.name = "# " + text;

            if (raw == true)
                t.SetTextRaw($"<color=#b77721>{text}</color>\n");
            else
                t.SetTextRaw($"<color=#b77721>{Localization.GetText(text)}</color>\n");

            var rt = t.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(0, y);
            y -= 100;
        }

        void AddText(
            string text,
            Localization.LanguageId? forceLanguage = null
        )
        {
            var t = Instantiate(this.text, this.text.transform.parent);
            t.gameObject.name = text;

            if (forceLanguage != null)
                t.SetTextRawForceLanguage(text, forceLanguage.Value);
            else
                t.SetTextRaw($"<size=125%>{text}</size>\n");

            var tmpText = t.GetComponent<TMPro.TMP_Text>();

            var rt = t.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(0, y);
            y -= Mathf.Max(tmpText.preferredHeight + 40, 110);
        }

        AddHeader("<sprite=0>");
        y -= 300;
        AddHeader("www.nitrome.com");
        y -= 50;
        AddHeader("A game by Nitrome");
        y -= 30;

        AddHeader("Game Director");
        AddText("Matthew Annal");

        AddHeader("Game Producer");
        AddText("Gwilym Hughes");

        AddHeader("Artwork and Animation");
        AddText("Matthew Annal");
        AddText("Markus Heinel");
        AddText("Fellipe Martins");
        AddText("Josiah Moore");

        AddHeader("Programming");
        AddText("Chris Burt-Brown");
        AddText("Gabriel Farfan");
        AddText("Aaron Steed");
        AddText("Marcin Zemblowski");

        AddHeader("Additional Programming");
        AddText("Anchit Sharma");

        AddHeader("Level Design");
        AddText("Alvaro Farfan");
        AddText("Owen Midgette");
        AddText("Marco Cifarelli");
        AddText("Kenny Glaze");

        AddHeader("Additional Levels");
        AddText("Matthew Annal");
        AddText("Markus Heinel");

        AddHeader("Music");
        AddText("Roberto Bazzoni");
        AddText("Dave Cowen");

        AddHeader("Sound Design");
        AddText("Fat Bard");
        AddText("Patrick Crecelius");
        AddText("Zach Fendelman");

        AddHeader("Translation");
        AddText("Annika Sophie Dürr");
        AddText("Catherine Dussault");
        AddText("Christoph Hönow");
        AddText("Erokay Andaç Yurdaer");
        AddText("Gaia Talamini");
        AddText("Inês Marques");
        AddText("Justin Luo");
        AddText("Kyulee Kim");
        AddText("Laura Cano");
        AddText("Linda Geschwandtner");
        AddText("Lingua Luminum");
        AddText("Lucas Pereira");
        AddText("Maite Azcutia");
        AddText("محمد البدري", forceLanguage: LanguageId.Arabic);
        AddText("朱凯 (nick9550)", forceLanguage: LanguageId.ChineseSimplified);
        AddText("Noé Rodríguez Daporta");
        AddText("Romain Portelli");
        AddText("Seyoon Oh");
        AddText("Stefan Hahn");
        AddText("Tjerk Teitsma");
        AddText("Tommy Nordkvist");
        AddText("Артем Нурмухаметов", forceLanguage: LanguageId.Russian);

        AddHeader("Special Thanks");
        AddText("The Nitrome Team");
        AddText("Our families and partners for understanding");
        AddText("Our Loyal Fanbase");

        AddHeader("Produced by Apple", raw: true);

        Destroy(this.text.gameObject);
        this.text = null;

        this.textHeight = -y;

        Invoke("OnEnable", 0.1f);
    }

    public void OnEnable()
    {
        this.scrollRectTransform.anchoredPosition = new Vector2(0, 0);
        Update();
    }

    public void Update()
    {
        float loop = this.textHeight + 1100;

        float y = this.scrollRectTransform.anchoredPosition.y;

        var speed = 120 * Time.deltaTime;
        if (GameInput.Player(1).holdingDown == true) speed *= 4;
        if (GameInput.Player(1).holdingUp == true) speed *= -2;

        var drag = Drag();
        if(drag != null)
            speed = drag.Value * 200f;

        y += speed;
        if(y < 0) y += loop;
        y %= loop;

        this.scrollRectTransform.anchoredPosition = new Vector2(0, y);
    }

    private float? Drag()
    {
        Vector2? mouseAt;
        if(Input.touchCount > 0)
            mouseAt = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
        else if(Input.GetMouseButton(0))
            mouseAt = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        else
        {
            this.mouseDownAt = null;
            return null;
        }
        var drag = 0f;
        if(mouseAt.HasValue == true)
        {
            if(this.mouseDownAt.HasValue == true)
                drag = mouseAt.Value.y - this.mouseDownAt.Value.y;
        }
        this.mouseDownAt = mouseAt;

        return drag;
    }
}
