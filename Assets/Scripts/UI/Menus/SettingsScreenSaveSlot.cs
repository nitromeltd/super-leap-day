using UnityEngine;
using TMPro;
using System;

public class SettingsScreenSaveSlot : MonoBehaviour
{
    public int slot;
    public LocalizeText textName;

    public TMP_Text textCoins;
    public TMP_Text textDaysStarted;
    public TMP_Text textGoldTrophies;
    public TMP_Text textDateTime;
    public TMP_InputField inputName;

    public Transform infoContainer;
    public Transform activeArrows;

    public UnityEngine.UI.Button buttonActivate;
    public UnityEngine.UI.Button buttonDelete;
    public UnityEngine.UI.Button buttonRename;

    public static bool isEditingSlotName;

    private void OnEnable()
    {
        UpdateInfo();
    }

    public void UpdateInfo()
    {
        if (SaveData.DoesSlotExist(slot) == true)
        {
            var name = SaveData.GetSlotName(slot);

            this.inputName.text = name;

            this.infoContainer.gameObject.SetActive(true);

            var characters = new[] {
                Character.Yolk,
                Character.Puffer,
                Character.Sprout,
                Character.Goop,
                Character.King
            };
            foreach(var c in characters)
                this.infoContainer.Find($"Portraits/{c}")?.gameObject.SetActive(false);
            this.infoContainer.Find($"Portraits/{SaveData.GetSelectedCharacter(slot)}")?.gameObject.SetActive(true);

            this.textName.SetTextRaw(name);
            this.textCoins.text = SaveData.GetCoins(slot).ToString();

            this.activeArrows.gameObject.SetActive(SaveData.currentSlotNumber == this.slot);

            var startedAndCompleted = SaveData.GetDaysStartedAndCompleted(slot);

            this.textDaysStarted.text = startedAndCompleted.started.ToString();
            this.textGoldTrophies.text = startedAndCompleted.completed.ToString();


            DateTime? saveDate = SaveData.GetLastSaveTimestamp(slot);
            if (saveDate != null)
            {
                this.textDateTime.text =
                    saveDate.Value.ToString("HH:mm") + "  " +
                    Localization.GetText(
                        "DAY_MONTH_YEAR",
                        Localization.GetTextForDayWithOrdinal(saveDate.Value),
                        Localization.GetTextForMonthName(saveDate.Value).ToUpper(),
                        saveDate.Value.Year.ToString()
                    );
            }
            else
            {
                this.textDateTime.text = "---";
            }
        }
        else
        {
            this.infoContainer.gameObject.SetActive(false);
            this.textName.SetText("NEW");
        }

        this.inputName.transform.parent.gameObject.SetActive(false);

    }

}
