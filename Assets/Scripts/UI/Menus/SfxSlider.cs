using UnityEngine;
using UnityEngine.EventSystems;

public class SfxSlider : MonoBehaviour
{
    public AudioClip[] soundsToPlay;
    private SelectionCursor selectionCursor;
    private bool dragging = false;
    private float time = 0;

    public void Start()
    {
        this.selectionCursor = FindObjectOfType<SelectionCursor>();

        var trigger = this.gameObject.AddComponent<EventTrigger>();

        var entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerDown;
        entry.callback.AddListener(delegate { this.dragging = true; });
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerUp;
        entry.callback.AddListener(delegate { this.dragging = false; });
        trigger.triggers.Add(entry);

        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.EndDrag;
        entry.callback.AddListener(delegate { this.dragging = false; });
        trigger.triggers.Add(entry);
    }

    public void Update()
    {
        if (
            this.dragging == true ||
            this.selectionCursor?.selection?.gameObject == this.gameObject
        )
        {
            this.time -= Time.deltaTime;
            if (this.time < 0)
            {
                this.time = 0.6f;
                Audio.instance.PlaySfx(Util.RandomChoice(this.soundsToPlay));
            }
        }
        else
        {
            this.time = 0;
        }
    }
}
