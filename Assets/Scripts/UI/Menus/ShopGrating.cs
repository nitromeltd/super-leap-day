using UnityEngine;
using UnityEngine.UI;

public class ShopGrating : Graphic
{
    [Header("Grating Details")]
    public Sprite gratingSprite;
    public float gratingScale = 1;
    public float shadowyAreaHeight = 200;

	public override Texture mainTexture { get => gratingSprite.texture; }

    private enum Stage { Shadowy, Transition, Normal, Top };

    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();

        if (this.gratingScale < 0.5f) return;

        const int RepeatWidth = 280;
        const int RepeatHeight = 280;
        const int TextureHeight = RepeatHeight * 4;

        var color = Color.white;

        var xMin = (0 - rectTransform.pivot.x) * this.rectTransform.rect.width;
        var xMax = (1 - rectTransform.pivot.x) * this.rectTransform.rect.width;
        var yMin = (0 - rectTransform.pivot.y) * this.rectTransform.rect.height;
        var yMax = (1 - rectTransform.pivot.y) * this.rectTransform.rect.height;

        xMin /= gratingScale;
        xMax /= gratingScale;
        yMin /= gratingScale;
        yMax /= gratingScale;

        var stage = Stage.Shadowy;
        int rows = Mathf.CeilToInt((float)(yMax - yMin) / RepeatHeight);
        float y = yMax - (RepeatHeight * rows);

        for (; y < yMax - 1; y += RepeatHeight)
        {
            if (stage == Stage.Shadowy && y > yMin + this.shadowyAreaHeight)
            {
                stage = Stage.Transition;
                y -= yMin + this.shadowyAreaHeight;
                y %= RepeatHeight / 2;
                y += yMin + this.shadowyAreaHeight;
            }
            else if (stage == Stage.Transition)
            {
                stage = Stage.Normal;
            }

            if (y + RepeatHeight >= yMax)
            {
                stage = Stage.Top;
                y = yMax - RepeatHeight;
            }

            float y1 = Mathf.Max(y, yMin);
            float y2 = Mathf.Min(y + RepeatHeight, yMax);
            float v1 = (y1 - y) / RepeatHeight;
            float v2 = (y2 - y) / RepeatHeight;

            v1 *= ((float)RepeatHeight / TextureHeight);
            v2 *= ((float)RepeatHeight / TextureHeight);

            switch (stage)
            {
                case Stage.Transition: v1 += 0.25f; v2 += 0.25f; break;
                case Stage.Normal:     v1 += 0.5f;  v2 += 0.5f;  break;
                case Stage.Top:        v1 += 0.75f; v2 += 0.75f; break;
            }

            for (float x = xMin; x < xMax; x += RepeatWidth)
            {
                float x1 = x;
                float x2 = Mathf.Min(x + RepeatWidth, xMax);
                float u1 = 0;
                float u2 = (x2 - x1) / RepeatWidth;

                AddRect(x1, x2, y1, y2, u1, u2, v1, v2);
            }

            if (stage == Stage.Top)
                break;
        }

        void AddRect(
            float x1, float x2, float y1, float y2, float u1, float u2, float v1, float v2
        )
        {
            int firstVertex = vh.currentVertCount;

            x1 *= this.gratingScale;
            x2 *= this.gratingScale;
            y1 *= this.gratingScale;
            y2 *= this.gratingScale;

            vh.AddVert(new Vector3(x1, y1, 0), color, new Vector2(u1, v1));
            vh.AddVert(new Vector3(x2, y1, 0), color, new Vector2(u2, v1));
            vh.AddVert(new Vector3(x2, y2, 0), color, new Vector2(u2, v2));
            vh.AddVert(new Vector3(x1, y2, 0), color, new Vector2(u1, v2));

            vh.AddTriangle(firstVertex, firstVertex + 1, firstVertex + 2);
            vh.AddTriangle(firstVertex, firstVertex + 3, firstVertex + 2);
        }
    }
}
