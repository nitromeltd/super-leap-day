using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using ShopItem = SaveData.ShopItem;

public class ShopScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    [Serializable]
    public class Variant
    {
        public int referenceWidth;
        public RectTransform container;
        public PlayerForMenu player;
        public RectTransform railContainer;
        public TextMeshProUGUI totalCoinsText;
        public RectTransform buttonLeft;
        public RectTransform buttonRight;
        public ShopScreenDialogueBox dialogueBox;
        public Animated charityBox;
        public RectTransform charityBoxOutOfOrder;
        public Animated jr;
        public Animated king;
        public Animated kingAwake;
        public RectTransform kingCloth;
        public RectTransform infoOnPurchaseContainer;
        public ShopScreenVideoDropdown videoDropdown;
        public Image dropdownGradient;
    }
    public Variant variantLandscape;
    public Variant variantPortrait;
    
    public RectTransform rail1;
    public RectTransform rail2;

    public Sprite spriteGratingTop;
    public Sprite spriteGratingNormal;
    public Sprite spriteGratingNormalToShadow;
    public Sprite spriteGratingShadow;
    public Sprite spriteInfoCharacterPurchase;
    public Sprite spriteInfoMinigamePurchase;

    private List<Image> gratingImages;
    private float offsetDropdownY;

    public enum Section
    {
        Characters, Minigames
    }

    public class Card
    {
        public RectTransform rectTransform;
        public ShopItem item;
        public bool isBlank;
        public Section section;
        public UnityEngine.UI.Button button;
        public RectTransform shine;
        public RectTransform soldOut;
        public Image fadeArt;
        public Image coinX;
        public Image denied;
        public RectTransform newBadge;
        public TextMeshProUGUI textPrice;
        public int price;
        public int page;
        public int row;
        public int column;
        public float angleDegrees;
        public float rotationSpeedDegrees;
    }
    public Card[] cardsForItems;
    public List<Card> blankCards;
    public Card[] allCards;

    public GameObject prefabBlank;
    public GameObject prefabGoop;
    public GameObject prefabSprout;
    public GameObject prefabPuffer;
    public GameObject[] prefabYolkUpgrades;
    public GameObject[] prefabGoopUpgrades;
    public GameObject[] prefabSproutUpgrades;
    public GameObject[] prefabPufferUpgrades;
    public GameObject[] prefabKingUpgrades;
    public GameObject prefabGolf;
    public GameObject prefabRacing;
    public GameObject prefabPinball;
    public GameObject prefabFishing;
    public GameObject prefabRolling;
    public GameObject prefabAbseiling;
    public GameObject prefabInfiniteJump;
    public GameObject prefabMidasTouch;
    public GameObject prefabSwordAndShield;

    public enum SnapDirection { Neutral, Up, Down }

    public AudioClip music;

    public Animated.Animation animationJrIdle;
    public Animated.Animation animationJrChat;
    public Animated.Animation animationJrReaction;
    public Animated.Animation animationCoinBoxWobble;
    public Animated.Animation animationKingIdle;
    public Animated.Animation animationKingDrop;
    public Animated.Animation animationKingCharge;
    public Animated.Animation animationKingReaction;

    private bool dragging;
    private float dragStartPosition;
    private Vector2 lastMousePosition;
    private (int, int)? lastScreenSize;
    private Coroutine currentScrollCoroutine;

    private bool isJrtalking;

    [NonSerialized] public float currentPage;
    [NonSerialized] public int totalPages;

    public static ShopItem? shopItemShowOnStart;

    public static ShopItem[] ItemsForSale()
    {
        ShopItem FirstItemNotPurchased(params ShopItem[] items)
        {
            for (int n = 0; n < items.Length; n += 1)
            {
                bool purchased = SaveData.IsItemPurchased(items[n]);
                if (SaveData.IsItemPurchased(items[n]) == false)
                    return items[n];
            }
            // if every available item is purchased, return the last
            // (we'll see it with a "sold out" label on it)
            return items[items.Length - 1];
        }

        var result = new List<ShopItem>();

        {
            var item = FirstItemNotPurchased(
                ShopItem.YolkUpgradeLevel2,
                ShopItem.YolkUpgradeLevel3,
                ShopItem.YolkUpgradeLevel4
            );
            result.Add(item);
        }


        {
            var item = FirstItemNotPurchased(
                ShopItem.Puffer,
                ShopItem.PufferUpgradeLevel2,
                ShopItem.PufferUpgradeLevel3,
                ShopItem.PufferUpgradeLevel4
            );
            result.Add(item);
        }

        {
            var item = FirstItemNotPurchased(
                ShopItem.Sprout,
                ShopItem.SproutUpgradeLevel2,
                ShopItem.SproutUpgradeLevel3,
                ShopItem.SproutUpgradeLevel4
            );
            result.Add(item);
        }

        if (SaveData.IsItemPurchased(ShopItem.YolkUpgradeLevel3) &&
            SaveData.IsItemPurchased(ShopItem.PufferUpgradeLevel3) &&
            SaveData.IsItemPurchased(ShopItem.SproutUpgradeLevel3))
        {
            var item = FirstItemNotPurchased(
                ShopItem.Goop,
                ShopItem.GoopUpgradeLevel2,
                ShopItem.GoopUpgradeLevel3,
                ShopItem.GoopUpgradeLevel4
            );
            result.Add(item);
        }

        if (SaveData.IsItemPurchased(ShopItem.King))
        {
            var item = FirstItemNotPurchased(
                ShopItem.KingUpgradeLevel2,
                ShopItem.KingUpgradeLevel3,
                ShopItem.KingUpgradeLevel4
            );
            result.Add(item);
        }

        {
            var item = FirstItemNotPurchased(
                ShopItem.PowerupInfiniteJump
            );
            result.Add(item);
        }

        {
            var item = FirstItemNotPurchased(
                ShopItem.PowerupSwordAndShield
            );
            result.Add(item);
        }

        {
            var item = FirstItemNotPurchased(
                ShopItem.PowerupMidasTouch
            );
            result.Add(item);
        }

        var golf         = minigame.Minigame.Type.Golf;
        var racing       = minigame.Minigame.Type.Racing;
        var pinball      = minigame.Minigame.Type.Pinball;
        var fishing      = minigame.Minigame.Type.Fishing;
        var rolling      = minigame.Minigame.Type.Rolling;
        var abseiling     = minigame.Minigame.Type.Abseiling;
        var beginner     = MinigameDifficulty.Beginner;
        var intermediate = MinigameDifficulty.Intermediate;
        var advanced     = MinigameDifficulty.Advanced;

        if (SaveData.HasCompletedMinigameDifficulty(golf, advanced))
            result.Add(ShopItem.MinigameGolfExpert);
        else if (SaveData.HasCompletedMinigameDifficulty(golf, intermediate))
            result.Add(ShopItem.MinigameGolfAdvanced);
        else if (SaveData.HasCompletedMinigameDifficulty(golf, beginner))
            result.Add(ShopItem.MinigameGolfIntermediate);
        else
            result.Add(ShopItem.MinigameGolfBeginner);

        if (SaveData.HasCompletedMinigameDifficulty(pinball, advanced))
            result.Add(ShopItem.MinigamePinballExpert);
        else if (SaveData.HasCompletedMinigameDifficulty(pinball, intermediate))
            result.Add(ShopItem.MinigamePinballAdvanced);
        else if (SaveData.HasCompletedMinigameDifficulty(pinball, beginner))
            result.Add(ShopItem.MinigamePinballIntermediate);
        else
            result.Add(ShopItem.MinigamePinballBeginner);

        if (SaveData.HasCompletedMinigameDifficulty(fishing, advanced))
            result.Add(ShopItem.MinigameFishingExpert);
        else if (SaveData.HasCompletedMinigameDifficulty(fishing, intermediate))
            result.Add(ShopItem.MinigameFishingAdvanced);
        else if (SaveData.HasCompletedMinigameDifficulty(fishing, beginner))
            result.Add(ShopItem.MinigameFishingIntermediate);
        else
            result.Add(ShopItem.MinigameFishingBeginner);

        if (SaveData.HasCompletedMinigameDifficulty(rolling, advanced))
            result.Add(ShopItem.MinigameRollingExpert);
        else if(SaveData.HasCompletedMinigameDifficulty(rolling, intermediate))
            result.Add(ShopItem.MinigameRollingAdvanced);
        else if(SaveData.HasCompletedMinigameDifficulty(rolling, beginner))
            result.Add(ShopItem.MinigameRollingIntermediate);
        else
            result.Add(ShopItem.MinigameRollingBeginner);

        if (SaveData.HasCompletedMinigameDifficulty(abseiling, advanced))
            result.Add(ShopItem.MinigameAbseilingExpert);
        else if(SaveData.HasCompletedMinigameDifficulty(abseiling, intermediate))
            result.Add(ShopItem.MinigameAbseilingAdvanced);
        else if(SaveData.HasCompletedMinigameDifficulty(abseiling, beginner))
            result.Add(ShopItem.MinigameAbseilingIntermediate);
        else
            result.Add(ShopItem.MinigameAbseilingBeginner);

        if
        (
            SaveData.HasCompletedMinigameDifficulty(golf, beginner) ||
            SaveData.HasCompletedMinigameDifficulty(pinball, beginner) ||
            SaveData.HasCompletedMinigameDifficulty(rolling, beginner) ||
            SaveData.HasCompletedMinigameDifficulty(fishing, beginner) ||
            SaveData.HasCompletedMinigameDifficulty(abseiling, beginner)
        )
        {
            if (SaveData.HasCompletedMinigameDifficulty(racing, advanced))
                result.Add(ShopItem.MinigameRacingExpert);
            else if (SaveData.HasCompletedMinigameDifficulty(racing, intermediate))
                result.Add(ShopItem.MinigameRacingAdvanced);
            else if (SaveData.HasCompletedMinigameDifficulty(racing, beginner))
                result.Add(ShopItem.MinigameRacingIntermediate);
            else
                result.Add(ShopItem.MinigameRacingBeginner);
        }

        return result.ToArray();
    }

    private Card MakeCard(ShopItem item)
    {
        Card CharacterCard(GameObject prefab, int price)
        {
            var instance = Instantiate(prefab);
            return new Card
            {
                rectTransform = instance.GetComponent<RectTransform>(),
                item = item,
                section = Section.Characters,
                price = price
            };
        }

        Card CharacterUpgradeCard(GameObject prefab, int upgradeLevelNumber, int price)
        {
            var instance = Instantiate(prefab);
            var card = new Card
            {
                rectTransform = instance.GetComponent<RectTransform>(),
                item = item,
                section = Section.Characters,
                price = price
            };
            var cardItself = card.rectTransform.Find("card itself");
            var levelNumberL =
                cardItself.Find("upgrade diamond l/number").GetComponent<TMP_Text>();
            var levelNumberR =
                cardItself.Find("upgrade diamond r/number").GetComponent<TMP_Text>();
            levelNumberL.text = upgradeLevelNumber.ToString();
            levelNumberR.text = upgradeLevelNumber.ToString();

            float amount = upgradeLevelNumber / 4f;
            var bar = cardItself.Find("slider/bar").GetComponent<RectTransform>();
            bar.anchorMax = new Vector2(amount, 1);

            return card;
        }

        Card MinigameCard(GameObject prefab, MinigameDifficulty difficulty, int price)
        {
            var instance = Instantiate(prefab);
            var card = new Card
            {
                rectTransform = instance.GetComponent<RectTransform>(),
                item = item,
                section = Section.Minigames,
                price = price
            };

            var cardItself = card.rectTransform.Find("card itself");
            cardItself.Find("beginner").gameObject
                .SetActive(difficulty == MinigameDifficulty.Beginner);
            cardItself.Find("intermediate").gameObject
                .SetActive(difficulty == MinigameDifficulty.Intermediate);
            cardItself.Find("advanced").gameObject
                .SetActive(difficulty == MinigameDifficulty.Advanced);
            cardItself.Find("expert").gameObject
                .SetActive(difficulty == MinigameDifficulty.Expert);

            return card;
        }

        Card PowerupCard(GameObject prefab, int price)
        {
            var instance = Instantiate(prefab);
            return new Card
            {
                rectTransform = instance.GetComponent<RectTransform>(),
                item = item,
                section = Section.Characters,
                price = price
            };
        }

        var card = item switch
        {
            ShopItem.Goop   => CharacterCard(this.prefabGoop, 100),
            ShopItem.Sprout => CharacterCard(this.prefabSprout, 25),
            ShopItem.Puffer => CharacterCard(this.prefabPuffer, 5),

            ShopItem.YolkUpgradeLevel2   => CharacterUpgradeCard(this.prefabYolkUpgrades[0], 2, 25),
            ShopItem.YolkUpgradeLevel3   => CharacterUpgradeCard(this.prefabYolkUpgrades[1], 3, 100),
            ShopItem.YolkUpgradeLevel4   => CharacterUpgradeCard(this.prefabYolkUpgrades[2], 4, 500),
            ShopItem.GoopUpgradeLevel2   => CharacterUpgradeCard(this.prefabGoopUpgrades[0], 2, 200),
            ShopItem.GoopUpgradeLevel3   => CharacterUpgradeCard(this.prefabGoopUpgrades[1], 3, 300),
            ShopItem.GoopUpgradeLevel4   => CharacterUpgradeCard(this.prefabGoopUpgrades[2], 4, 500),
            ShopItem.SproutUpgradeLevel2 => CharacterUpgradeCard(this.prefabSproutUpgrades[0], 2, 50),
            ShopItem.SproutUpgradeLevel3 => CharacterUpgradeCard(this.prefabSproutUpgrades[1], 3, 100),
            ShopItem.SproutUpgradeLevel4 => CharacterUpgradeCard(this.prefabSproutUpgrades[2], 4, 200),
            ShopItem.PufferUpgradeLevel2 => CharacterUpgradeCard(this.prefabPufferUpgrades[0], 2, 50),
            ShopItem.PufferUpgradeLevel3 => CharacterUpgradeCard(this.prefabPufferUpgrades[1], 3, 100),
            ShopItem.PufferUpgradeLevel4 => CharacterUpgradeCard(this.prefabPufferUpgrades[2], 4, 200),
            ShopItem.KingUpgradeLevel2   => CharacterUpgradeCard(this.prefabKingUpgrades[0], 2, 100),
            ShopItem.KingUpgradeLevel3   => CharacterUpgradeCard(this.prefabKingUpgrades[1], 3, 150),
            ShopItem.KingUpgradeLevel4   => CharacterUpgradeCard(this.prefabKingUpgrades[2], 4, 250),

            ShopItem.PowerupInfiniteJump    => PowerupCard(this.prefabInfiniteJump, 50),
            ShopItem.PowerupSwordAndShield  => PowerupCard(this.prefabSwordAndShield, 100),
            ShopItem.PowerupMidasTouch      => PowerupCard(this.prefabMidasTouch, 500),

            ShopItem.MinigameGolfBeginner           => MinigameCard(this.prefabGolf, MinigameDifficulty.Beginner, 10),
            ShopItem.MinigameGolfIntermediate       => MinigameCard(this.prefabGolf, MinigameDifficulty.Intermediate, 50),
            ShopItem.MinigameGolfAdvanced           => MinigameCard(this.prefabGolf, MinigameDifficulty.Advanced, 100),
            ShopItem.MinigameGolfExpert             => MinigameCard(this.prefabGolf, MinigameDifficulty.Expert, 250),
            ShopItem.MinigameRacingBeginner         => MinigameCard(this.prefabRacing, MinigameDifficulty.Beginner, 10),
            ShopItem.MinigameRacingIntermediate     => MinigameCard(this.prefabRacing, MinigameDifficulty.Intermediate, 50),
            ShopItem.MinigameRacingAdvanced         => MinigameCard(this.prefabRacing, MinigameDifficulty.Advanced, 100),
            ShopItem.MinigameRacingExpert           => MinigameCard(this.prefabRacing, MinigameDifficulty.Expert, 250),
            ShopItem.MinigamePinballBeginner        => MinigameCard(this.prefabPinball, MinigameDifficulty.Beginner, 50),
            ShopItem.MinigamePinballIntermediate    => MinigameCard(this.prefabPinball, MinigameDifficulty.Intermediate, 100),
            ShopItem.MinigamePinballAdvanced        => MinigameCard(this.prefabPinball, MinigameDifficulty.Advanced, 250),
            ShopItem.MinigamePinballExpert          => MinigameCard(this.prefabPinball, MinigameDifficulty.Expert, 500),
            ShopItem.MinigameFishingBeginner        => MinigameCard(this.prefabFishing, MinigameDifficulty.Beginner, 50),
            ShopItem.MinigameFishingIntermediate    => MinigameCard(this.prefabFishing, MinigameDifficulty.Intermediate, 100),
            ShopItem.MinigameFishingAdvanced        => MinigameCard(this.prefabFishing, MinigameDifficulty.Advanced, 250),
            ShopItem.MinigameFishingExpert          => MinigameCard(this.prefabFishing, MinigameDifficulty.Expert, 500),
            ShopItem.MinigameRollingBeginner        => MinigameCard(this.prefabRolling, MinigameDifficulty.Beginner, 50),
            ShopItem.MinigameRollingIntermediate    => MinigameCard(this.prefabRolling, MinigameDifficulty.Intermediate, 100),
            ShopItem.MinigameRollingAdvanced        => MinigameCard(this.prefabRolling, MinigameDifficulty.Advanced, 250),
            ShopItem.MinigameRollingExpert          => MinigameCard(this.prefabRolling, MinigameDifficulty.Expert, 500),
            ShopItem.MinigameAbseilingBeginner      => MinigameCard(this.prefabAbseiling, MinigameDifficulty.Beginner, 50),
            ShopItem.MinigameAbseilingIntermediate  => MinigameCard(this.prefabAbseiling, MinigameDifficulty.Intermediate, 100),
            ShopItem.MinigameAbseilingAdvanced      => MinigameCard(this.prefabAbseiling, MinigameDifficulty.Advanced, 250),
            ShopItem.MinigameAbseilingExpert        => MinigameCard(this.prefabAbseiling, MinigameDifficulty.Expert, 500),

            _ => null
        };


        if (card == null)
            return null;

        var cardItself = card.rectTransform.Find("card itself");
        card.button      = cardItself.GetComponent<UnityEngine.UI.Button>();
        card.soldOut     = cardItself.Find("sold out").GetComponent<RectTransform>();
        card.coinX       = cardItself.Find("price back/coin x").GetComponent<Image>();
        card.shine       = cardItself.Find("shine").GetComponent<RectTransform>();
        card.fadeArt     = cardItself.Find("fadeArt").GetComponent<Image>();
        card.denied      = cardItself.Find("denied").GetComponent<Image>();
        card.newBadge    = cardItself.Find("new badge/badge").GetComponent<RectTransform>();
        card.textPrice   =
            cardItself.Find("price back/price").GetComponent<TextMeshProUGUI>();
        card.textPrice.text = card.price.ToString();
        card.button.onClick.AddListener(() => { OnClickCard(card); });
        
        var isShowNewBadge = item switch
        {
            ShopItem.YolkUpgradeLevel2          => false,
            ShopItem.PowerupInfiniteJump        => false,
            ShopItem.PowerupSwordAndShield      => false,
            ShopItem.PowerupMidasTouch          => false,
            ShopItem.Sprout                     => false,
            ShopItem.Puffer                     => false,
            ShopItem.MinigameGolfBeginner       => false,
            ShopItem.MinigamePinballBeginner    => false,
            ShopItem.MinigameFishingBeginner    => false,
            ShopItem.MinigameRollingBeginner    => false,
            ShopItem.MinigameAbseilingBeginner  => false,
            _ => true
        };
        card.newBadge.gameObject.SetActive(isShowNewBadge && SaveData.IsItemNew(card.item));

        if (SaveData.IsItemPurchased(card.item) == true)
        {
            card.soldOut.gameObject.SetActive(true);
            card.coinX  .color = new Color(1, 1, 1, 0.0f);
            card.textPrice .color = new Color(1, 1, 1, 0.0f);
            card.fadeArt.color += new Color(0, 0, 0, 0.7f);
            card.newBadge.gameObject.SetActive(false);
        }

        return card;
    }

    public void Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        this.variantLandscape.jr.PlayAndLoop(this.animationJrIdle);
        this.variantPortrait.jr.PlayAndLoop(this.animationJrIdle);

        if (SaveData.IsCharacterPurchased(Character.King))
        {
            this.variantLandscape.king.gameObject.SetActive(false);
            this.variantPortrait.king.gameObject.SetActive(false);
            this.variantLandscape.kingAwake.gameObject.SetActive(false);
            this.variantPortrait.kingAwake.gameObject.SetActive(false);
            this.variantLandscape.charityBox.gameObject.SetActive(false);
            this.variantPortrait.charityBox.gameObject.SetActive(false);
            this.variantLandscape.charityBoxOutOfOrder.gameObject.SetActive(true);
            this.variantPortrait.charityBoxOutOfOrder.gameObject.SetActive(true);
        }
        else
        {
            this.variantLandscape.king.gameObject.SetActive(true);
            this.variantPortrait.king.gameObject.SetActive(true);
            this.variantLandscape.kingAwake.gameObject.SetActive(false);
            this.variantPortrait.kingAwake.gameObject.SetActive(false);
            this.variantLandscape.king.PlayAndLoop(this.animationKingIdle);
            this.variantPortrait.king.PlayAndLoop(this.animationKingIdle);
        }

        {
            var listCards = new List<Card>();
            foreach (var item in ItemsForSale())
            {
                var card = MakeCard(item);
                if (card != null)
                    listCards.Add(card);
            }
            this.cardsForItems = listCards.ToArray();
            this.allCards = this.cardsForItems.ToArray();

            {
                var instance = Instantiate(this.prefabBlank);
                var card = new Card
                {
                    rectTransform = instance.GetComponent<RectTransform>(),
                    isBlank = true
                };
                var cardItself = card.rectTransform.Find("card itself");
                card.denied = cardItself.Find("denied").GetComponent<Image>();
                card.button = cardItself.GetComponent<UnityEngine.UI.Button>();
                card.button.onClick.AddListener(() => { OnClickCard(card); });
                this.blankCards = new List<Card> { card };
            }
        }

        string valueText = SaveData.GetCoins().ToString();
        this.variantLandscape.totalCoinsText.text = valueText;
        this.variantPortrait.totalCoinsText.text = valueText;

        if (ActiveVariant() == this.variantLandscape)
        {
            this.variantLandscape.player.WalkToX(-800, onPlayerStop);
            this.variantPortrait.player.WalkToX(-500);
        }
        else
        {
            this.variantLandscape.player.WalkToX(-800);
            this.variantPortrait.player.WalkToX(-500, onPlayerStop);
        }

        void onPlayerStop()
        {
            if (ActiveVariant().dialogueBox.hasEverOpened == false)
            {
                ActiveVariant().dialogueBox.Say(
                    "Hey buddy. Welcome to the Super Leap Day Merch Shop!", null
                );
                jrTalk();
            }
        }
        this.isJrtalking = false;
        Layout();
        UpdateCardPrices();

        Canvas.ForceUpdateCanvases();
        this.offsetDropdownY = (float)Screen.height / Screen.width * ActiveVariant().referenceWidth;
        var offset = new Vector2(0, this.offsetDropdownY);

        ActiveVariant().infoOnPurchaseContainer.anchoredPosition += offset;
        ActiveVariant().videoDropdown.container.anchoredPosition += offset;
        ActiveVariant().dropdownGradient.color = new Color(1, 1, 1, 0);

        this.selectionCursor.Select(ActiveVariant().buttonRight.GetComponent<UnityEngine.UI.Button>(), false);
        
        if(shopItemShowOnStart.HasValue == true)
        {
            foreach(var card in allCards)
            {
                if(card.item == shopItemShowOnStart.Value)
                {
                    this.currentPage = card.page;
                    LayoutCards();
                    break;
                }
            }
            shopItemShowOnStart = null;
        }

        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn);
    }

    private Variant ActiveVariant() =>
        GameCamera.IsLandscape() ? this.variantLandscape : this.variantPortrait;

    public void Update()
    {
        GameInput.Advance();

        var activeVariant = ActiveVariant();

        if (Input.GetMouseButtonDown(0) == true &&
            this.totalPages > 1 &&
            IsMouseOverDragRegion() == true &&
            activeVariant.dialogueBox.IsOpen() == false)
        {
            if (this.currentScrollCoroutine != null)
            {
                StopCoroutine(this.currentScrollCoroutine);
                this.currentScrollCoroutine = null;
            }

            this.dragging = true;
            this.dragStartPosition = this.currentPage;
            this.lastMousePosition = Input.mousePosition;
        }
        else if (this.dragging == true && Input.GetMouseButton(0) == false)
        {
            var speed = (Input.mousePosition.x - this.lastMousePosition.x);
            SnapDirection snap = 0;
            if (speed > 10) snap = SnapDirection.Down;
            if (speed < -10) snap = SnapDirection.Up;

            this.dragging = false;
            StartScrollToValidPage(snap);

            foreach (var card in this.cardsForItems)
            {
                card.button.interactable = true;
            }
        }
        else if (this.dragging == true)
        {
            float dx =
                (Input.mousePosition.x - this.lastMousePosition.x) / Screen.width;
            this.currentPage -= dx;
            this.lastMousePosition = Input.mousePosition;

            var disableClick = Mathf.Abs(
                this.currentPage - this.dragStartPosition
            ) > 0.05f;

            foreach (var card in this.cardsForItems)
            {
                card.angleDegrees += SwingFactor(card) * dx * 0.02f;
                if (disableClick == true)
                    card.button.interactable = false;
            }
        }

        foreach (var card in this.allCards)
        {
            card.rotationSpeedDegrees -= card.angleDegrees * 0.02f;
            card.rotationSpeedDegrees *= 0.94f;
            card.angleDegrees += card.rotationSpeedDegrees;
            card.angleDegrees = Mathf.Clamp(card.angleDegrees, -15, 15);
            card.rectTransform.Find("card itself").localRotation =
                Quaternion.Euler(0, 0, card.angleDegrees);
        }

        var onRoundPageNumber =
            Mathf.Abs(this.currentPage - Mathf.RoundToInt(this.currentPage)) < 0.01f;
        bool showLeftButton =
            onRoundPageNumber == true && activeVariant.dialogueBox.IsOpen() == false && this.currentPage > 0.01f;
        bool showRightButton =
            onRoundPageNumber == true && activeVariant.dialogueBox.IsOpen() == false && this.currentPage < this.totalPages - 1.1f;
        
        // turning a gameObject on and off breaks a Button's selected state
        // selecting the button and sending it a null pointerevent fixes this problem
        if (activeVariant.buttonLeft.gameObject.activeSelf != showLeftButton)
        {
            activeVariant.buttonLeft.gameObject.SetActive(showLeftButton);
            if(this.selectionCursor.IsActive == true && this.selectionCursor.selection.gameObject == activeVariant.buttonLeft.gameObject)
            {
                var btn = activeVariant.buttonLeft.GetComponent<UnityEngine.UI.Button>();
                btn.Select();
                btn.OnSelect(null);
            }
        }
        if (activeVariant.buttonRight.gameObject.activeSelf != showRightButton)
        {
            activeVariant.buttonRight.gameObject.SetActive(showRightButton);
            if(this.selectionCursor.IsActive == true && this.selectionCursor.selection.gameObject == activeVariant.buttonRight.gameObject) { 
                var btn = activeVariant.buttonRight.GetComponent<UnityEngine.UI.Button>();
                btn.Select();
                btn.OnSelect(null);
            }
        }

        if (this.lastScreenSize != (Screen.width, Screen.height))
        {
            this.lastScreenSize = (Screen.width, Screen.height);
            Layout();
        }
        else
        {
            LayoutCards();
        }

        if (activeVariant.dialogueBox.IsOpen() == true && this.isJrtalking == false)
        {
            jrTalk();
        }
        else if (activeVariant.dialogueBox.IsOpen() == false)
        {
            this.isJrtalking = false;
            activeVariant.jr.PlayAndLoop(this.animationJrIdle);
        }

        Animated.AdvanceAll(Time.deltaTime);

        if(GameInput.pressedMenu)
        {
            if(activeVariant.dialogueBox.IsOpen() == true)
                activeVariant.dialogueBox.OnClickOutside();
            else if(ActiveVariant().player.IsWalking() == false)
                OnClickExit();
        }
    }

    private bool IsMouseOverDragRegion()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        if (results.Count == 0) return true;
        if (results[0].gameObject.GetComponent<UnityEngine.UI.Button>()) return false;
        return true;
    }

    public void OnClickLeft()
    {
        if (this.currentScrollCoroutine != null)
            return;

        int pageNumber = Mathf.RoundToInt(this.currentPage);
        if(pageNumber == 1)
            this.selectionCursor.Select(ActiveVariant().buttonRight.GetComponent<UnityEngine.UI.Button>(), false);
        if (pageNumber > 0)
            this.currentScrollCoroutine = StartCoroutine(ScrollToPage(pageNumber - 1));
        Audio.instance.PlaySfx(sfxForMenu.sfxUIShopSwipe);
    }

    public void OnClickRight()
    {
        if (this.currentScrollCoroutine != null)
            return;

        int pageNumber = Mathf.RoundToInt(this.currentPage);
        if(pageNumber + 1 == this.totalPages - 1)
            this.selectionCursor.Select(ActiveVariant().buttonLeft.GetComponent<UnityEngine.UI.Button>(), false);
        if (pageNumber < this.totalPages - 1)
            this.currentScrollCoroutine = StartCoroutine(ScrollToPage(pageNumber + 1));
        Audio.instance.PlaySfx(sfxForMenu.sfxUIShopSwipe);
    }

    public void OnClickCard(Card card)
    {
        void Denied()
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxUICalendarSwipeDeny);
            card.denied.color = Color.white;
            var tweener = new Tweener();
            tweener.Alpha(card.denied, 0, 0.5f);
            StartCoroutine(tweener.Run());
        }

        if(card.isBlank == true)
        {
            Denied();
            return;
        }

        if(SaveData.IsItemNew(card.item) == true)
        {
            SaveData.SetItemNew(card.item, false);
            card.newBadge.gameObject.SetActive(false);
        }

        this.selectionCursor.Select(card.button, false);

        if (SaveData.IsItemPurchased(card.item) == true)
        {
            ActiveVariant().dialogueBox.Say(
                "We’re sold out of that at the moment. It was very popular! We’re expecting more stock soon"
            );
            jrTalk();

            return;
        }
        if(card.price > SaveData.GetCoins())
        {
            Denied();
            return;
        }

        var v = ActiveVariant();

        v.dialogueBox.AskYesNo(
            "Ah {0}, Good Choice! That will be {1}",
            onYes,
            onNo,
            GetItemName(card.item), card.price.ToString()
        );

        OpenVideoDropdown(card);

        jrTalk();

        void onYes()
        {
            ShowDropdown(v.videoDropdown.container, false);

            Action endDialogue =
                FirstMinigamePurchasedDialogueAction(card.item) ??
                FirstCharacterPurchasedDialogueAction(card.item) ??
                delegate {
                    v.dialogueBox.Say("…Oh, by the way…No Refunds!");
                    jrTalk();
                };

            int coins = SaveData.GetCoins() - card.price;
            SaveData.SetItemPurchased(card.item, true);
            SaveData.SetCoins(coins);

            StartCoroutine(AnimateCoinCount(coins));
            StartCoroutine(AnimatePurchaseOnCard(card));

            v.dialogueBox.Say("Thanks buddy. Hope you enjoy it!", endDialogue);
            jrTalk();
            Audio.instance.PlaySfx(sfxForMenu.sfxUIShopPurchase);
        }

        void onNo()
        {
            ShowDropdown(v.videoDropdown.container, false);

            v.dialogueBox.Say("Ah shame! Let me know if you see anything else!");
            jrTalk();
        }
    }

    public void UpdateCardPrices()
    {
        int coins = SaveData.GetCoins();

        foreach(var card in this.cardsForItems)
        {
            if(card.soldOut.gameObject.activeInHierarchy == false)
                card.textPrice.color = card.price > coins ? Color.red : Color.white;
        }
    }

    // If 1st Minigame purchase, supply Action for dialogue and popup
    public Action FirstMinigamePurchasedDialogueAction(ShopItem item)
    {
        var items = new[]
        {
            ShopItem.MinigameGolfBeginner,
            ShopItem.MinigamePinballBeginner,
            ShopItem.MinigameFishingBeginner,
            ShopItem.MinigameRollingBeginner,
            ShopItem.MinigameRacingBeginner,
            ShopItem.MinigameAbseilingBeginner
        };
        var found = false;
        foreach(var i in items)
        {
            if(SaveData.IsItemPurchased(i))
                return null;
            if(item == i) found = true;
        }
        if(found) return delegate
        {
            var v = ActiveVariant();
            v.infoOnPurchaseContainer.Find("sign").GetComponent<Image>().sprite = this.spriteInfoMinigamePurchase;
            ShowInfoOnPurchaseDropdown(true);
            v.dialogueBox.Say("If you want to try out that bonus game look out for the games-lift in the levels!", delegate {
                v.dialogueBox.Say("To access it just hit the button on the top to open the doors and head inside", delegate { ShowInfoOnPurchaseDropdown(false); });
                jrTalk();
            });
            jrTalk();
        };
        return null;
    }

    // If 1st Character purchase, supply Action for dialogue and popup
    public Action FirstCharacterPurchasedDialogueAction(ShopItem item)
    {
        var items = new[]
        {
            ShopItem.Puffer,
            ShopItem.Goop,
            ShopItem.Sprout,
            ShopItem.King
        };
        var found = false;
        foreach(var i in items)
        {
            if(SaveData.IsItemPurchased(i))
                return null;
            if(item == i) found = true;
        }
        if(found) return delegate
            {
                var v = ActiveVariant();
                v.infoOnPurchaseContainer.Find("sign").GetComponent<Image>().sprite = this.spriteInfoCharacterPurchase;
                ShowInfoOnPurchaseDropdown(true);
                v.dialogueBox.Say("By the way if you want to change character head through the green door to the character select screen.", delegate
                {
                    v.dialogueBox.Say("Most character's powers are triggered by pressing and holding the screen / button.", delegate
                    {
                        v.dialogueBox.Say("You can get full details on what each power does by hitting the ? Button.", delegate
                        {
                            ShowInfoOnPurchaseDropdown(false);
                        });
                        jrTalk();
                    });
                    jrTalk();
                });
                jrTalk();
            };
                        
        return null;
    }

    public string GetItemName(ShopItem item)
    {
        return item switch
        {
            SaveData.ShopItem.YolkUpgradeLevel2             => characterLevel("yolk", 2),
            SaveData.ShopItem.YolkUpgradeLevel3             => characterLevel("yolk", 3),
            SaveData.ShopItem.YolkUpgradeLevel4             => characterLevel("yolk", 4),
            SaveData.ShopItem.Goop                          => "goop",
            SaveData.ShopItem.GoopUpgradeLevel2             => characterLevel("goop", 2),
            SaveData.ShopItem.GoopUpgradeLevel3             => characterLevel("goop", 3),
            SaveData.ShopItem.GoopUpgradeLevel4             => characterLevel("goop", 4),
            SaveData.ShopItem.Puffer                        => "puffer",
            SaveData.ShopItem.PufferUpgradeLevel2           => characterLevel("puffer", 2),
            SaveData.ShopItem.PufferUpgradeLevel3           => characterLevel("puffer", 3),
            SaveData.ShopItem.PufferUpgradeLevel4           => characterLevel("puffer", 4),
            SaveData.ShopItem.Sprout                        => "sprout",
            SaveData.ShopItem.SproutUpgradeLevel2           => characterLevel("sprout", 2),
            SaveData.ShopItem.SproutUpgradeLevel3           => characterLevel("sprout", 3),
            SaveData.ShopItem.SproutUpgradeLevel4           => characterLevel("sprout", 4),
            SaveData.ShopItem.King                          => "the x",
            SaveData.ShopItem.KingUpgradeLevel2             => characterLevel("the x", 2),
            SaveData.ShopItem.KingUpgradeLevel3             => characterLevel("the x", 3),
            SaveData.ShopItem.KingUpgradeLevel4             => characterLevel("the x", 4),
            SaveData.ShopItem.PowerupInfiniteJump           => "Wings",
            SaveData.ShopItem.PowerupSwordAndShield         => "Sword And Shield",
            SaveData.ShopItem.PowerupMidasTouch             => "Midas Touch",
            SaveData.ShopItem.MinigameGolfBeginner          => minigameDifficulty("golf", "beginner"),
            SaveData.ShopItem.MinigameGolfIntermediate      => minigameDifficulty("golf", "intermediate"),
            SaveData.ShopItem.MinigameGolfAdvanced          => minigameDifficulty("golf", "advanced"),
            SaveData.ShopItem.MinigameGolfExpert            => minigameDifficulty("golf", "expert"),
            SaveData.ShopItem.MinigameRacingBeginner        => minigameDifficulty("racing", "beginner"),
            SaveData.ShopItem.MinigameRacingIntermediate    => minigameDifficulty("racing", "intermediate"),
            SaveData.ShopItem.MinigameRacingAdvanced        => minigameDifficulty("racing", "advanced"),
            SaveData.ShopItem.MinigameRacingExpert          => minigameDifficulty("racing", "expert"),
            SaveData.ShopItem.MinigamePinballBeginner       => minigameDifficulty("pinball", "beginner"),
            SaveData.ShopItem.MinigamePinballIntermediate   => minigameDifficulty("pinball", "intermediate"),
            SaveData.ShopItem.MinigamePinballAdvanced       => minigameDifficulty("pinball", "advanced"),
            SaveData.ShopItem.MinigamePinballExpert         => minigameDifficulty("pinball", "expert"),
            SaveData.ShopItem.MinigameFishingBeginner       => minigameDifficulty("fishing", "beginner"),
            SaveData.ShopItem.MinigameFishingIntermediate   => minigameDifficulty("fishing", "intermediate"),
            SaveData.ShopItem.MinigameFishingAdvanced       => minigameDifficulty("fishing", "advanced"),
            SaveData.ShopItem.MinigameFishingExpert         => minigameDifficulty("fishing", "expert"),
            SaveData.ShopItem.MinigameRollingBeginner       => minigameDifficulty("rolling", "beginner"),
            SaveData.ShopItem.MinigameRollingIntermediate   => minigameDifficulty("rolling", "intermediate"),
            SaveData.ShopItem.MinigameRollingAdvanced       => minigameDifficulty("rolling", "advanced"),
            SaveData.ShopItem.MinigameRollingExpert         => minigameDifficulty("rolling", "expert"),
            SaveData.ShopItem.MinigameAbseilingBeginner     => minigameDifficulty("abseiling", "beginner"),
            SaveData.ShopItem.MinigameAbseilingIntermediate => minigameDifficulty("abseiling", "intermediate"),
            SaveData.ShopItem.MinigameAbseilingAdvanced     => minigameDifficulty("abseiling", "advanced"),
            SaveData.ShopItem.MinigameAbseilingExpert       => minigameDifficulty("abseiling", "expert"),
            _ => "???"
        };

        string characterLevel(string name, int level)
        {
            return Localization.GetText("{0} LEVEL {1}", new string[] { name, level.ToString() });
        }

        string minigameDifficulty(string name, string diff)
        {
            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                return $"{Localization.GetText(diff.ToUpper())} {Localization.GetText(name.ToUpper())}";
            else
                return $"{Localization.GetText(name.ToUpper())} {Localization.GetText(diff.ToUpper())}";
        }
    }

    public void OnClickCharityBox()
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm);

        var variant = ActiveVariant();
        var btn = variant.charityBox.GetComponent<UnityEngine.UI.Button>();
        btn.interactable = false;
        variant.charityBox.PlayOnce(
            this.animationCoinBoxWobble,
            () => { btn.interactable = true; }
        );

        var firstCharacterDialogue = FirstCharacterPurchasedDialogueAction(ShopItem.King);

        const int DonationRequiredForKing = 100;

        if (SaveData.IsCharacterPurchased(Character.King) == true)
            return;

        variant.dialogueBox.AskYesNo(
            "Hey buddy. How about donating a little something to get my old man back on his feet?",
            onYesDonate,
            null
        );

        void onYesDonate()
        {
            variant.dialogueBox.Say("Really? That’s Great!", onYesDonate2);
            jrTalk();
        }

        void onYesDonate2()
        {
            variant.dialogueBox.AskForNumber(
                "How much would you like to donate?",
                addAmount,
                () => addAmount(0)
            );
            jrTalk();
        }

        void addAmount(int amount)
        {
            if (amount == 0)
            {
                variant.dialogueBox.Say("Aww that sucks! I thought you wanted to help?");
                jrTalk();
                return;
            }

            int oldDonationAmount = SaveData.GetCoinsDonatedToCharity();
            int newDonationAmount = oldDonationAmount + amount;
            int newCoins = SaveData.GetCoins() - amount;

            SaveData.SetCoins(newCoins);
            SaveData.SetCoinsDonatedToCharity(newDonationAmount);
            StartCoroutine(AnimateCoinCount(newCoins));

            if (newDonationAmount >= DonationRequiredForKing)
            {
                SaveData.SetItemPurchased(ShopItem.King, true);
                kingWakeUpDialogue();
                StartCoroutine(kingWakeUp());
            }
            else
            {
                variant.dialogueBox.Say(
                    "Thanks buddy, this will really help Dad get back on his feet!"
                );
                jrTalk();
            }
        }

        IEnumerator kingWakeUp()
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxUIConfirm);
            this.variantLandscape.charityBox.gameObject.SetActive(false);
            this.variantPortrait.charityBox.gameObject.SetActive(false);
            this.variantLandscape.charityBoxOutOfOrder.gameObject.SetActive(true);
            this.variantPortrait.charityBoxOutOfOrder.gameObject.SetActive(true);
            
            variant.king.gameObject.SetActive(false);
            variant.kingAwake.gameObject.SetActive(true);
            {
                bool done = false;
                variant.kingAwake.PlayOnce(this.animationKingReaction, () => { done = true; });
                variant.kingAwake.OnFrame(21, () => 
                {
                    Audio.instance.PlaySfx(sfxForMenu.sfxKingRespawn);
                    Audio.instance.PlaySfx(sfxForMenu.sfxPlayerJump);
                    StartCoroutine(kingClothAnimate());
                });
                while (!done) yield return null;
            }

            for (int n = 0; n < 10; n += 1)
            {
                bool done = false;
                variant.kingAwake.PlayOnce(this.animationKingCharge, () => { done = true; });
                while (!done) yield return null;
            }

            variant.kingAwake.PlayOnce(this.animationKingDrop);

            yield return null;
            yield return null;

            var kingAnimations = variant.player.playerAnimationsForMenu.king;

            variant.kingAwake.PlayAndLoop(kingAnimations.animationIdle);
            variant.kingAwake.transform.localScale = new Vector2(-1, 1);
            variant.kingAwake.transform.SetAsLastSibling();
            yield return new WaitForSecondsRealtime(1.0f);

            variant.kingAwake.PlayAndLoop(kingAnimations.animationRun);
            variant.kingAwake.transform.localScale = new Vector2(1, 1);
            for (int n = 0; n < 150; n += 1)
            {
                variant.kingAwake.GetComponent<RectTransform>().anchoredPosition +=
                    new Vector2(10, 0);
                yield return null;
            }
        }

        IEnumerator kingClothAnimate()
        {
            variant.kingCloth.gameObject.SetActive(true);
            variant.kingCloth.SetAsLastSibling();

            Vector2 velocity = new Vector2(-20, 20);

            for (int n = 0; n < 100; n += 1)
            {
                velocity.y -= 1.5f;
                variant.kingCloth.anchoredPosition += velocity;
                yield return null;
            }

            variant.kingCloth.gameObject.SetActive(false);
        }


        void kingWakeUpDialogue()
        {
            variant.dialogueBox.Say(
                "Yippie! I can’t believe it! Dad is back on his feet!",
                delegate
                {
                    variant.dialogueBox.Say(
                        "He said he will use the cash to enter the event and win back his crown! Go Dad!",
                        firstCharacterDialogue
                    );
                    jrTalk();
                }
            );
            jrTalk();
        }
    }

    public void OnClickExit()
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack);
        var player = ActiveVariant().player;
        var stairsX = player.TargetXForObject(player.stairs);
        player.WalkToX(stairsX - 400, () =>
        {
            MainMenuScreen.enterSceneThroughDoor = MainMenuScreen.DoorType.Shop;
            Transition.GoSimple("Main Menu");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn);
        });
    }

    private IEnumerator AnimatePurchaseOnCard(Card card)
    {
        card.rectTransform.SetAsLastSibling();

        card.shine.localScale = Vector2.zero;
        card.shine.gameObject.SetActive(true);
        card.soldOut.localScale = Vector2.zero;
        card.soldOut.gameObject.SetActive(true);

        var tweener = new Tweener();
        tweener.ScaleLocal(card.shine, Vector3.one, 0.4f, Easing.Linear);
        tweener.RotateLocal(card.shine, -270, 3, Easing.Linear);
        tweener.ScaleLocal(card.shine, Vector3.zero, 0.4f, Easing.QuadEaseIn, 2.6f);
        tweener.ScaleLocal(card.soldOut, Vector3.one, 1, Easing.ElasticEaseOut, 3);
        tweener.Alpha(card.fadeArt, 0.7f, 0.5f, Easing.Linear, 3);
        tweener.Alpha(card.coinX , 0.0f, 0.5f, Easing.Linear, 3);
        tweener.Alpha(card.textPrice , 0.0f, 0.5f, Easing.Linear, 3);

        bool hasSoldoutPlayed = false;

        while (tweener.IsDone() == false)
        {
            tweener.Advance(1.0f / 60f);
            if (card.soldOut.localScale.x > 0.1f && !hasSoldoutPlayed)
            {
                hasSoldoutPlayed = true;
                Audio.instance.PlaySfx(sfxForMenu.sfxUIShopPurchaseSoldout);
            }
            yield return null;
        }
        
        UpdateCardPrices();
    }

    private IEnumerator AnimateCoinCount(int to)
    {
        int from = 0;
        int.TryParse(this.variantLandscape.totalCoinsText.text, out from);

        Audio.instance.PlaySfx(sfxForMenu.sfxUIShopPurchaseCoins);

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(from, to, 1.0f, Easing.QuartEaseInOut);
        while (tweener.IsDone() == false)
        {
            tweener.Advance(1.0f / 60f);
            string valueText = Mathf.RoundToInt(tween.Value()).ToString();
            this.variantLandscape.totalCoinsText.text = valueText;
            this.variantPortrait.totalCoinsText.text = valueText;
            yield return null;
        }
        Audio.instance.StopSfx(sfxForMenu.sfxUIShopPurchaseCoins);
    }

    private void OpenVideoDropdown(Card card)
    {
        ActiveVariant().videoDropdown.Init(card.item, GetItemName(card.item), card.price);
        ShowDropdown(ActiveVariant().videoDropdown.container, true);
    }

    private void ShowInfoOnPurchaseDropdown(bool value) =>
        ShowDropdown(ActiveVariant().infoOnPurchaseContainer, value);
    
    private void ShowDropdown(RectTransform container, bool value)
    {
        var tweener = new Tweener();
        tweener.MoveAnchoredPosition(
            container,
            container.anchoredPosition + this.offsetDropdownY * (value ? Vector2.down : Vector2.up)
            , 0.25f, Easing.QuadEaseOut);
        tweener.Alpha(ActiveVariant().dropdownGradient, value ? 1f : 0f, 0.25f, Easing.QuadEaseInOut);

        StartCoroutine(tweener.Run());
    }

    private void StartScrollToValidPage(SnapDirection snapDirection)
    {
        int target = snapDirection switch
        {
            SnapDirection.Up   => Mathf.CeilToInt(this.currentPage),
            SnapDirection.Down => Mathf.FloorToInt(this.currentPage),
            _                  => Mathf.RoundToInt(this.currentPage)
        };
        target = Mathf.Clamp(target, 0, this.totalPages - 1);
        this.currentScrollCoroutine = StartCoroutine(ScrollToPage(target));
    }

    private IEnumerator ScrollToPage(int targetPageNumber)
    {
        this.selectionCursor.gameObject.SetActive(false);

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(
            this.currentPage, targetPageNumber, 1.0f, Easing.QuadEaseInOut
        );

        while (tweener.IsDone() == false)
        {
            if (this.dragging == true)
            {
                this.currentScrollCoroutine = null;
                yield break;
            }

            tweener.Advance(Time.deltaTime);
            float dx = tween.Value() - this.currentPage;
            this.currentPage = tween.Value();

            foreach (var card in this.allCards)
            {
                card.angleDegrees += SwingFactor(card) * dx * 100.0f;
            }

            yield return null;
        }
        this.currentScrollCoroutine = null;

        this.selectionCursor.gameObject.SetActive(true);
    }

    private float SwingFactor(Card card)
    {
        int index = Array.IndexOf(this.allCards, card);
        float s = Mathf.Sin((index * 2.5f) + Time.realtimeSinceStartup);
        return 0.6f + (0.4f * s);
    }

    private void Layout()
    {
        Game.UpdateCameraLetterboxing();

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortrait.container.gameObject.SetActive(
            activeVariant == this.variantPortrait
        );
        var referenceWidth = activeVariant.referenceWidth;

        if (activeVariant == this.variantPortrait &&
            (float)Screen.height / (float)Screen.width < 1.5f)
        {
            this.variantPortrait.container.anchoredPosition = new Vector2(0, 260);
            this.variantPortrait.container.sizeDelta = new Vector2(0, 350);
        }
        else
        {
            this.variantPortrait.container.anchoredPosition = new Vector2(0, 78f);
            this.variantPortrait.container.sizeDelta = new Vector2(0, -156f);
        }
        this.canvasScaler.referenceResolution = new Vector2(referenceWidth, 600);

        this.rail1.SetParent(activeVariant.railContainer, false);
        this.rail2.SetParent(activeVariant.railContainer, false);
        this.rail1.SetAsFirstSibling();
        this.rail2.SetAsFirstSibling();

        if (GameCamera.IsLandscape() == false &&
            (float)Screen.height / (float)Screen.width > 2.0f)
        {
            this.rail1.anchoredPosition = new Vector2(0, 1224);
            this.rail2.anchoredPosition = new Vector2(0, -84);
            this.rail1.gameObject.SetActive(true);
            this.rail2.gameObject.SetActive(true);
            this.variantPortrait.buttonLeft.anchoredPosition = new Vector2(15, -301);
            this.variantPortrait.buttonRight.anchoredPosition = new Vector2(-15, -301);
        }
        else
        {
            this.rail1.anchoredPosition = new Vector2(0, 580);
            this.rail1.gameObject.SetActive(true);
            this.rail2.gameObject.SetActive(false);
            this.variantPortrait.buttonLeft.anchoredPosition = new Vector2(15, -135);
            this.variantPortrait.buttonRight.anchoredPosition = new Vector2(-15, -135);
        }

        void PlaceCard(Card card, int page, int row, int column)
        {
            (card.page, card.row, card.column) = (page, row, column);
        }

        bool twoRails = this.rail2.gameObject.activeSelf;
        int cardsPerPage =
            (GameCamera.IsLandscape() == true) ? 5 :
            (twoRails == true) ? 6 : 3;
        var allCards            = new List<Card>();
        var blankCardsRemaining = this.blankCards.ToList();

        int page = 0;
        foreach (var section in new [] { Section.Characters, Section.Minigames })
        {
            var cardsForSection =
                this.cardsForItems.Where(c => c.section == section).ToList();
            while (cardsForSection.Count % cardsPerPage != 0)
            {
                if (blankCardsRemaining.Count == 0)
                {
                    var card = new Card
                    {
                        rectTransform = Instantiate(
                            this.blankCards[0].rectTransform, this.rail1
                        ),
                        isBlank = true
                    };
                    var cardItself = card.rectTransform.Find("card itself");
                    card.denied = cardItself.Find("denied").GetComponent<Image>();
                    card.button = cardItself.GetComponent<UnityEngine.UI.Button>();
                    card.button.onClick.AddListener(() => { OnClickCard(card); });
                    this.blankCards.Add(card);
                    blankCardsRemaining.Add(card);
                }
                var blank = blankCardsRemaining[0];
                blank.rectTransform.gameObject.SetActive(true);
                cardsForSection.Add(blank);
                blankCardsRemaining.RemoveAt(0);
            }

            allCards.AddRange(cardsForSection);

            for (int n = 0; n < cardsForSection.Count; n += 1)
            {
                int row    = 0;
                int column = n % cardsPerPage;

                if (twoRails == true)
                {
                    row = column / 3;
                    column %= 3;
                }

                PlaceCard(cardsForSection[n], page, row, column);
                if (n % cardsPerPage == cardsPerPage - 1)
                    page += 1;
            }
        }

        foreach (var blank in blankCardsRemaining)
        {
            blank.rectTransform.gameObject.SetActive(false);
        }

        this.allCards = allCards.ToArray();

        this.totalPages = this.allCards.Select(c => c.page).Max() + 1;
        if (this.dragging == false &&
            this.currentScrollCoroutine == null &&
            this.currentPage > this.totalPages - 1)
        {
            this.currentPage = this.totalPages - 1;
        }

        LayoutCards();
    }

    private void LayoutCards()
    {
        float screenWidth = this.canvasScaler.referenceResolution.x;

        int totalColumns = this.allCards.Select(c => c.column).Max() + 1;
        float centerColumn = (totalColumns - 1) * 0.5f;

        foreach (var card in this.allCards)
        {
            var rail = (card.row == 1) ? this.rail2 : this.rail1;
            if (card.rectTransform.parent != rail)
                card.rectTransform.SetParent(rail, false);

            card.rectTransform.anchoredPosition = new Vector2(
                (card.column - centerColumn) * 720 +
                (card.page - this.currentPage) * screenWidth,
                0
            );

            if (this.selectionCursor.selection == card.button && SaveData.IsItemNew(card.item) == true)
            {
                SaveData.SetItemNew(card.item, false);
                card.newBadge.gameObject.SetActive(false);
            }
        }
    }

    private void jrTalk()
    {
        this.isJrtalking = true;

        var variant = ActiveVariant();

        Audio.instance.StopSfx(sfxForMenu.sfxShopKeeper[0]);
        Audio.instance.StopSfx(sfxForMenu.sfxShopKeeper[1]);
        Audio.instance.StopSfx(sfxForMenu.sfxShopKeeper[2]);

        Audio.instance.PlayRandomSfx(null, null, sfxForMenu.sfxShopKeeper);
        variant.jr.PlayOnce(this.animationJrChat, () =>
        {
            variant.jr.PlayAndLoop(this.animationJrIdle);
        });
    }
}
