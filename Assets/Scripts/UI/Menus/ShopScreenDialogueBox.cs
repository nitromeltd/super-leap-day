using UnityEngine;
using System;
using System.Text.RegularExpressions;
using TMPro;

public class ShopScreenDialogueBox : MonoBehaviour
{
    public RectTransform styleYesOrNo;
    public RectTransform styleOk;
    public RectTransform styleAmountSelect;
    public TickerText textYesOrNo;
    public TickerText textOk;
    public TickerText textAmountSelect;
    public TMP_Text amountText;
    public UnityEngine.UI.Button yesButtonForYesOrNoStyle;
    public UnityEngine.UI.Button yesButtonForOkStyle;
    public UnityEngine.UI.Button yesButtonForAmountSelect;
    public UnityEngine.UI.Button buttonClickOutside;

    public Action onClickYes;
    public Action onClickNo;
    public Action<int> onClickYesWithInt;

    private int amount;
    private SelectionCursor selectionCursor;
    private UnityEngine.UI.Selectable selectadPrevious;

    public ShopScreen shopScreen;

    public bool hasEverOpened = false;

    public void Start()
    {
        this.gameObject.SetActive(false);
        this.buttonClickOutside.gameObject.SetActive(false);
        this.selectionCursor = FindObjectOfType<SelectionCursor>();
    }

    public bool IsOpen() => this.gameObject.activeSelf;

    public void AskYesNo(string text, Action onYes, Action onNo, params string [] substitutions)
    {
        this.gameObject.SetActive(true);
        this.buttonClickOutside.gameObject.SetActive(true);
        this.styleYesOrNo     .gameObject.SetActive(true);
        this.styleOk          .gameObject.SetActive(false);
        this.styleAmountSelect.gameObject.SetActive(false);
        this.textYesOrNo      .SetText(text, substitutions);
        this.onClickYes            = onYes;
        this.onClickNo             = onNo;
        this.onClickYesWithInt     = null;
        hasEverOpened              = true;
        this.selectadPrevious      = this.selectionCursor.selection;
        this.selectionCursor.Select(this.yesButtonForYesOrNoStyle, false);
        this.selectionCursor.ConstrainMovement(this.gameObject);

        HighlightSelectedCard(true);
    }

    public void Say(string text, Action onClose = null)
    {
        this.gameObject.SetActive(true);
        this.buttonClickOutside.gameObject.SetActive(true);
        this.styleYesOrNo     .gameObject.SetActive(false);
        this.styleOk          .gameObject.SetActive(true);
        this.styleAmountSelect.gameObject.SetActive(false);
        this.textOk           .SetText(text);
        this.onClickYes            = onClose;
        this.onClickNo             = onClose;
        this.onClickYesWithInt     = null;
        hasEverOpened              = true;
        this.selectadPrevious      = this.selectionCursor.selection;
        
        // hidden yesButton is used by selectionCursor to advance
        this.selectionCursor.Select(this.yesButtonForOkStyle, false);
        this.selectionCursor.ConstrainMovement(this.gameObject);
        this.yesButtonForOkStyle.image.color = Color.clear;

        HighlightSelectedCard(true);
    }

    public void AskForNumber(string text, Action<int> onConfirm, Action onCancel)
    {
        this.gameObject.SetActive(true);
        this.buttonClickOutside.gameObject.SetActive(true);
        this.styleYesOrNo     .gameObject.SetActive(false);
        this.styleOk          .gameObject.SetActive(false);
        this.styleAmountSelect.gameObject.SetActive(true);
        this.textAmountSelect .SetText(text);
        this.onClickYes            = null;
        this.onClickNo             = null;
        this.onClickYesWithInt     = onConfirm;
        hasEverOpened              = true;
        this.selectadPrevious      = this.selectionCursor.selection;
        this.selectionCursor.Select(this.yesButtonForAmountSelect, false);
        this.selectionCursor.ConstrainMovement(this.gameObject);

        this.amount = 0;
        this.amountText.text = "0";

        HighlightSelectedCard(true);
    }

    public void OnClickYes()
    {
        HighlightSelectedCard(false);
        this.gameObject.SetActive(false);
        this.buttonClickOutside.gameObject.SetActive(false);
        this.selectionCursor.RemoveConstraint();
        this.selectionCursor.Select(this.selectadPrevious, false);

        var callback1 = this.onClickYes;
        var callback2 = this.onClickYesWithInt;
        this.onClickYes = null;
        this.onClickYesWithInt = null;

        callback1?.Invoke();
        callback2?.Invoke(this.amount);

        Audio.instance.PlaySfx(shopScreen.sfxForMenu.sfxUIConfirm);
    }

    public void OnClickNo()
    {
        HighlightSelectedCard(false);
        this.gameObject.SetActive(false);
        this.buttonClickOutside.gameObject.SetActive(false);
        this.selectionCursor.RemoveConstraint();
        this.selectionCursor.Select(this.selectadPrevious, false);

        var callback = this.onClickNo;
        this.onClickNo = null;

        callback?.Invoke();

        Audio.instance.PlaySfx(shopScreen.sfxForMenu.sfxUIBack);
    }

    public void OnClickAmountUp()
    {
        this.amount += 1;
        if (this.amount > SaveData.GetCoins())
            this.amount = SaveData.GetCoins();
        this.amountText.text = this.amount.ToString();
    }
    
    public void OnClickAmountDown()
    {
        this.amount -= 1;
        if (this.amount < 0)
            this.amount = 0;
        this.amountText.text = this.amount.ToString();
    }

    public void OnClickOutside()
    {
        if(this.styleYesOrNo.gameObject.activeInHierarchy == true)
            OnClickNo();
        else if(this.styleOk.gameObject.activeInHierarchy == true)
            OnClickYes();
    }

    private void HighlightSelectedCard(bool value)
    {
        this.selectadPrevious?.transform.Find("highlighted")?.gameObject.SetActive(value);
    }
}
