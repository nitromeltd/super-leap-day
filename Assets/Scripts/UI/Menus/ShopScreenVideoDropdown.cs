using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using TMPro;
using ShopItem = SaveData.ShopItem;

public class ShopScreenVideoDropdown : MonoBehaviour
{
    [Header("Characters")]
    public VideoClip[] videoClipsForYolk;
    public VideoClip[] videoClipsForSprout;
    public VideoClip[] videoClipsForGoop;
    public VideoClip[] videoClipsForPuffer;
    public VideoClip[] videoClipsForKing;

    [Header("Powers")]
    public VideoClip videoClipForMidas;
    public VideoClip videoClipForSwordAndShield;
    public VideoClip videoClipForInfiniteJump;

    [Header("Minigames")]
    public VideoClip videoClipForGolf;
    public VideoClip videoClipForPinball;
    public VideoClip videoClipForRacing;
    public VideoClip videoClipForFishing;
    public VideoClip videoClipForRolling;
    public VideoClip videoClipForAbseiling;

    public Color colorYolk;
    public Color colorSprout;
    public Color colorGoop;
    public Color colorPuffer;
    public Color colorKing;
    public Color colorMidas;
    public Color colorSwordAndShield;
    public Color colorInfiniteJump;
    public Color colorGolf;
    public Color colorPinball;
    public Color colorRacing;
    public Color colorFishing;
    public Color colorRolling;
    public Color colorAbseiling;

    //public Sprite spriteYolkPanel;
    //public Sprite spriteSproutPanel;
    //public Sprite spriteGoopPanel;
    //public Sprite spritePufferPanel;
    //public Sprite spriteKingPanel;
    //public Sprite spriteMidasPanel;
    //public Sprite spriteSwordAndShieldPanel;
    //public Sprite spriteInfiniteJumpPanel;
    //public Sprite spriteGolfPanel;
    //public Sprite spritePinballPanel;
    //public Sprite spriteRacingPanel;
    //public Sprite spriteFishingPanel;
    //public Sprite spriteRollingPanel;
    //public Sprite spriteAbseilingPanel;

    [Header("UI")]
    public RectTransform container;
    public Image panel;
    public VideoPlayer videoPlayer;
    public Image videoBlackOut;
    public TMP_Text textPrice;
    public LocalizeText textName;
    public LocalizeText textInfo;

    private void Awake()
    {
        // handle Arabic right-to-left
        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
        {
            this.container.Find("standard").gameObject.SetActive(false);
            var arabic = this.container.Find("arabic");
            arabic.gameObject.SetActive(true);
            this.textName = arabic.Find("Name").GetComponent<LocalizeText>();
            this.textPrice = arabic.Find("Price").GetComponent<TMP_Text>();
        }
    }

    public void Init(ShopItem item, string name, int price)
    {
        string info = "none";

        void SetupCharacter(Character character, int upgradeIndex)
        {
            this.videoPlayer.clip = (character switch
            {
                Character.Yolk =>   this.videoClipsForYolk,
                Character.Sprout => this.videoClipsForSprout,
                Character.Goop =>   this.videoClipsForGoop,
                Character.Puffer => this.videoClipsForPuffer,
                Character.King =>   this.videoClipsForKing,
                _ => null
            })[upgradeIndex - 1];
            this.panel.color = character switch
            {
                Character.Yolk =>   this.colorYolk,
                Character.Sprout => this.colorSprout,
                Character.Goop =>   this.colorGoop,
                Character.Puffer => this.colorPuffer,
                Character.King =>   this.colorKing,
                _ => Color.white
            };
            Debug.Log(panel.color);
            info = CharacterSelectScreenAbilityDropdown.AbilityText(character)[upgradeIndex - 1];
        }

        void SetupMinigame(minigame.Minigame.Type game)
        {
            this.videoPlayer.clip = game switch
            {
                minigame.Minigame.Type.Golf =>      this.videoClipForGolf,
                minigame.Minigame.Type.Pinball =>   this.videoClipForPinball,
                minigame.Minigame.Type.Racing =>    this.videoClipForRacing,
                minigame.Minigame.Type.Fishing =>   this.videoClipForFishing,
                minigame.Minigame.Type.Rolling =>   this.videoClipForRolling,
                minigame.Minigame.Type.Abseiling => this.videoClipForAbseiling,
                _ => null
            };
            this.panel.color = game switch
            {
                minigame.Minigame.Type.Golf =>      this.colorGolf,
                minigame.Minigame.Type.Pinball =>   this.colorPinball,
                minigame.Minigame.Type.Racing =>    this.colorRacing,
                minigame.Minigame.Type.Fishing =>   this.colorFishing,
                minigame.Minigame.Type.Rolling =>   this.colorRolling,
                minigame.Minigame.Type.Abseiling => this.colorAbseiling,
                _ => Color.white
            };
            info = game switch
            {
                minigame.Minigame.Type.Golf =>      "Take to the green for a round of golf!",
                minigame.Minigame.Type.Pinball =>   "Use the flippers to propel your ball through bumper laden tables.",
                minigame.Minigame.Type.Racing =>    "Speed around the tracks against the clock to the finish line in Racing!",
                minigame.Minigame.Type.Fishing =>   "Cast your line and avoid getting snagged to find the treasures of the deep.",
                minigame.Minigame.Type.Rolling =>   "Roll, bounce, swing, and fall through these mechanical mazes.",
                minigame.Minigame.Type.Abseiling => "Jump, swing, rappel, and descend to reach the bottom!",
                _ => ""
            };
        }

        switch(item)
        {
            case ShopItem.YolkUpgradeLevel2: SetupCharacter(Character.Yolk, 2); break;
            case ShopItem.YolkUpgradeLevel3: SetupCharacter(Character.Yolk, 3); break;
            case ShopItem.YolkUpgradeLevel4: SetupCharacter(Character.Yolk, 4); break;
            case ShopItem.Sprout: SetupCharacter(Character.Sprout, 1); break;
            case ShopItem.SproutUpgradeLevel2: SetupCharacter(Character.Sprout, 2); break;
            case ShopItem.SproutUpgradeLevel3: SetupCharacter(Character.Sprout, 3); break;
            case ShopItem.SproutUpgradeLevel4: SetupCharacter(Character.Sprout, 4); break;
            case ShopItem.Puffer: SetupCharacter(Character.Puffer, 1); break;
            case ShopItem.PufferUpgradeLevel2: SetupCharacter(Character.Puffer, 2); break;
            case ShopItem.PufferUpgradeLevel3: SetupCharacter(Character.Puffer, 3); break;
            case ShopItem.PufferUpgradeLevel4: SetupCharacter(Character.Puffer, 4); break;
            case ShopItem.Goop: SetupCharacter(Character.Goop, 1); break;
            case ShopItem.GoopUpgradeLevel2: SetupCharacter(Character.Goop, 2); break;
            case ShopItem.GoopUpgradeLevel3: SetupCharacter(Character.Goop, 3); break;
            case ShopItem.GoopUpgradeLevel4: SetupCharacter(Character.Goop, 4); break;
            case ShopItem.King: SetupCharacter(Character.King, 1); break;
            case ShopItem.KingUpgradeLevel2: SetupCharacter(Character.King, 2); break;
            case ShopItem.KingUpgradeLevel3: SetupCharacter(Character.King, 3); break;
            case ShopItem.KingUpgradeLevel4: SetupCharacter(Character.King, 4); break;
            case ShopItem.MinigameGolfBeginner:
            case ShopItem.MinigameGolfIntermediate:
            case ShopItem.MinigameGolfAdvanced:
            case ShopItem.MinigameGolfExpert: SetupMinigame(minigame.Minigame.Type.Golf); break;
            case ShopItem.MinigamePinballBeginner:
            case ShopItem.MinigamePinballIntermediate:
            case ShopItem.MinigamePinballAdvanced:
            case ShopItem.MinigamePinballExpert: SetupMinigame(minigame.Minigame.Type.Pinball); break;
            case ShopItem.MinigameRacingBeginner:
            case ShopItem.MinigameRacingIntermediate:
            case ShopItem.MinigameRacingAdvanced:
            case ShopItem.MinigameRacingExpert: SetupMinigame(minigame.Minigame.Type.Racing); break;
            case ShopItem.MinigameFishingBeginner:
            case ShopItem.MinigameFishingIntermediate:
            case ShopItem.MinigameFishingAdvanced:
            case ShopItem.MinigameFishingExpert: SetupMinigame(minigame.Minigame.Type.Fishing); break;
            case ShopItem.MinigameRollingBeginner:
            case ShopItem.MinigameRollingIntermediate:
            case ShopItem.MinigameRollingAdvanced:
            case ShopItem.MinigameRollingExpert: SetupMinigame(minigame.Minigame.Type.Rolling); break;
            case ShopItem.MinigameAbseilingBeginner:
            case ShopItem.MinigameAbseilingIntermediate:
            case ShopItem.MinigameAbseilingAdvanced:
            case ShopItem.MinigameAbseilingExpert: SetupMinigame(minigame.Minigame.Type.Abseiling); break;
            default:
                this.videoPlayer.clip = item switch
                {
                    ShopItem.PowerupMidasTouch =>       this.videoClipForMidas,
                    ShopItem.PowerupInfiniteJump =>     this.videoClipForInfiniteJump,
                    ShopItem.PowerupSwordAndShield =>   this.videoClipForSwordAndShield,
                    _ => null
                };
                this.panel.color = item switch
                {
                    ShopItem.PowerupMidasTouch =>       this.colorMidas,
                    ShopItem.PowerupInfiniteJump =>     this.colorInfiniteJump,
                    ShopItem.PowerupSwordAndShield =>   this.colorSwordAndShield,
                    _ => Color.white
                };
                info = item switch
                {
                    ShopItem.PowerupMidasTouch =>       "This power makes enemies and objects you touch turn to gold. A great way to boost finances!",
                    ShopItem.PowerupInfiniteJump =>     "Collect this power up to grow wings and gain the ability to jump infinitely.",
                    ShopItem.PowerupSwordAndShield =>   "This power up gives you a defensive extra hit shield and an enchanted sword to take out enemies.",
                    _ => ""
                };
                break;
        }

        this.textPrice.text = "" + price;
        this.textPrice.color = price > SaveData.GetCoins() ? Color.red : Color.white;
        this.textName.SetText(name);
        this.textInfo.SetText(info.ToUpper());

        this.videoBlackOut.color = Color.black;
        this.videoPlayer.Prepare();
        StartCoroutine(WaitForVideoPlayer());
    }

    private IEnumerator WaitForVideoPlayer()
    {
        while(this.videoPlayer.isPrepared == false)
            yield return null;

        this.videoPlayer.Play();
        var tweener = new Tweener();
        tweener.Alpha(videoBlackOut, 0, 0.2f);
        yield return tweener.Run();
    }
}
