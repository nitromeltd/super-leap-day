using UnityEngine;
using TMPro;
using System.Text.RegularExpressions;

// Tickers text, handles localization issues
public class TickerText : MonoBehaviour
{
    private TMP_Text[] targets;
    private string text = "";
    private int textShowingLength;

    private void Awake()
    {
        // get layers
        targets = GetComponentsInChildren<TMP_Text>();
        
        foreach(var t in targets)
        {
            // To get Arabic to word-wrap and ticker right-to-left we must:
            // reverse the string and enable RTL
            // (ArabicSupport.Fix is not redundant - characters will not be joined without it)
            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
            {
                t.isRightToLeftText = true;
                if(t.horizontalAlignment == HorizontalAlignmentOptions.Left)
                    t.horizontalAlignment = HorizontalAlignmentOptions.Right;
            }
        }
    }

    public void SetText(string text, params string [] substitutions)
    {
        text = Localization.GetText(text.ToUpper(), substitutions);

        if(Localization.activeLanguage == Localization.LanguageId.Arabic)
            text = Reverse(text);

        foreach(var t in targets)
            t.text = "";

        this.text = text;
        this.textShowingLength = 0;
    }

    void Update()
    {
        if(this.textShowingLength < this.text.Length)
        {
            if(this.text[this.textShowingLength] == '<')
                this.textShowingLength =
                    this.text.IndexOf('>', this.textShowingLength) + 1;
            else
                this.textShowingLength += 1;

            if(this.textShowingLength > this.text.Length)
                this.textShowingLength = this.text.Length;
        }

        string TextInWhite(string value)
        {
            value = Regex.Replace(value, "<color=#?[A-Za-z0-9]+>", "");
            value = Regex.Replace(value, "<\\/color>", "");
            return $"<color=#00000000>{value}</color>";
        }

        var newText =
            this.text.Substring(0, this.textShowingLength) +
            TextInWhite(this.text.Substring(this.textShowingLength));

        foreach(var t in targets)
            t.text = newText;
    }

    public static string Reverse(string s)
    {
        char[] charArray = s.ToCharArray();
        System.Array.Reverse(charArray);
        return new string(charArray);
    }
}
