using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.EventSystems;
using RecordForDate = SaveData.RecordForDate;

public class TrophyRoomScreen : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    [Serializable]
    public class Variant
    {
        public Transform container;
        public RectTransform wallpaper;
        public PlayerForMenu player;
        public RectTransform scrollContainer;
        public RectTransform ceilingExitContainer;
        public RectTransform infoPanel;
        public UnityEngine.UI.Button buttonUp;
        public UnityEngine.UI.Button buttonDown;
        public Dictionary<string, Award> awards;
        public int referenceWidth;
        [NonSerialized] public float scrollMax;
        [NonSerialized] public float scrollDist;
    }
    public Variant variantLandscape;
    public Variant variantPortrait;

    public Sprite[] spritesTutorialTrophy;
    public Sprite[] spritesDeathsTrophy;
    public Sprite[] spritesCheckpointTrophy;
    public Sprite[] spritesDaysTrophy;
    public Sprite[] spritesTimerTrophy;
    public Sprite[] spritesBestTrophy;

    public class Award
    {
        public string name;
        public int value;
        public int threshold;
        public int index;
        public bool isTrophy;
        public UnityEngine.UI.Button button;
        public RectTransform shelf;
    }
    private bool isLeaving = false;
    private float scrollUpAmount = 0;
    private float scrollUpTarget = 0;
    private Coroutine coroutineInfoPanel;
    private Coroutine coroutineScrollContainer;
    private string shelfSelected = "";
    private Variant[] variants;

    public enum SnapDirection { Neutral, Up, Down }
    private bool isDragging = false;
    private bool hasDragged = false;
    private Vector2 lastMousePosition;
    private Vector2 mouseDownPosition;
    private Coroutine currentScrollCoroutine;

    public Variant ActiveVariant() =>
        GameCamera.IsLandscape() ? this.variantLandscape : this.variantPortrait;

    public AudioClip music;

    public void Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        this.variants = new[] { this.variantPortrait, this.variantLandscape };
        foreach(var variant in this.variants)
            variant.container.gameObject.SetActive(true);

        {
            var player = this.variantLandscape.player;
            var stairsX = player.TargetXForObject(player.stairs);
            player.rectTransform.anchoredPosition = new Vector2(stairsX - 300, 0);
            player.WalkToX(stairsX + 400);
        }
        {
            var player = this.variantPortrait.player;
            var stairsX = player.TargetXForObject(player.stairs);
            player.rectTransform.anchoredPosition = new Vector2(stairsX - 300, 0);
            player.WalkToX(stairsX + 650);

        }

        void SetCeilingExit(Variant variant)
        {
            var cameraHeight = variant.referenceWidth / Camera.main.aspect;
            var ceilingExitPos = variant.ceilingExitContainer.anchoredPosition;
            ceilingExitPos.y = cameraHeight + variant.scrollDist * variant.scrollMax;
            variant.ceilingExitContainer.anchoredPosition = ceilingExitPos;

            variant.wallpaper.sizeDelta =
                new Vector2(variant.wallpaper.sizeDelta.x, ceilingExitPos.y - variant.wallpaper.anchoredPosition.y);
        }

        foreach(var variant in this.variants)
        {
            UpdateScrollDist(variant);
            SetCeilingExit(variant);
        }

        SetupTrophies();
        UpdateButtonsVisible();
        SelectShelf(null, false);

        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, null, new Vector3(0, 0, 0));

    }

    private void SetupTrophies()
    {
        /*
            Timer Cup
            -(Complete/Completed) a day in (5/6/7/10/15/20) minutes or less.
            Days Cup
            - (Complete/Completed) (1/7/30/90/180/365) days.
            Checkpoint Cup
            -(Skip/Skipped) (1/2/3/4/5) checkpoints without activating the flag.
            Deaths Cup
            -(Complete/Completed) a day with (0/1/5 or less/10 or less) deaths
            Best Cup
            -(Reach/Reached) the (Bronze/Silver/Gold/Fruit) Cup checkpoint!
            Graduation Cup
            -(Complete/Completed) the trial stage to graduate for the main event!
         */
        foreach(var variant in this.variants)
        {
            variant.awards = new Dictionary<string, Award>();
        
            var records = SaveData.GetAllRecordForDates();
            var deaths = int.MaxValue;
            var bestTrophy = SaveData.Trophy.None;
            var checkpointsSkipped = SaveData.GetMaxCheckpointsSkipped();
            var tutorialCompleted = SaveData.HasCompletedOpeningTutorial();
            var daysCompleted = 0;
            var bestTime = float.MaxValue;

            foreach(var record in records)
            {
                if(record.deaths < deaths) deaths = record.deaths;
                if(record.trophy > bestTrophy) bestTrophy = record.trophy;
                if(record.trophy >= SaveData.Trophy.Gold)
                {
                    daysCompleted +=  1;
                    if(record.timeInSeconds < bestTime) bestTime = record.timeInSeconds;
                }
            }
            bestTime = Mathf.Ceil(bestTime / 60);

            Award GetAward(int value, bool under, int[] thresholds, Sprite[] sprites, RectTransform shelf)
            {
                if(thresholds.Length != sprites.Length)
                    Debug.LogError($"Shelf {shelf.name} has {thresholds.Length} thresholds but {sprites.Length} sprites.");

                var index = under ? thresholds.Length - 1 : 0;
                var threshold = 0;

                if(under == true)
                {
                    for(int i = thresholds.Length - 1; i >= 0; i--)
                    {
                        if(value <= thresholds[i])
                        {
                            index = i;
                            threshold = thresholds[i];
                        }
                        else
                            break;
                    }
                }
                else
                {
                    for(int i = 0; i < thresholds.Length; i++)
                    {
                        if(value >= thresholds[i])
                        {
                            index = i;
                            threshold = thresholds[i];
                        }
                        else
                            break;
                    }
                }

                shelf.Find("Trophy").GetComponent<Image>().sprite = sprites[index];
                shelf.Find("Shelf Button").GetComponent<UnityEngine.UI.Button>()
                    .onClick.AddListener(() => { if(this.hasDragged == false) SelectShelf(shelf.name, true); });

                return new Award
                {
                    name = shelf.name,
                    index = index,
                    isTrophy = (under == true && index < thresholds.Length - 1) || (under == false && index > 0),
                    value = value,
                    threshold = threshold,
                    button = shelf.Find("Shelf Button").GetComponent<UnityEngine.UI.Button>(),
                    shelf = shelf
                };
            }

            var cabinet = variant.scrollContainer.Find("Cabinet");
            foreach(RectTransform shelf in cabinet)
            {
                variant.awards[shelf.name] = shelf.name switch
                {
                    "Tutorial"      => GetAward(tutorialCompleted ? 1 : 0, false, new[] { 0, 1 }, spritesTutorialTrophy, shelf),
                    "Best"          => GetAward((int)bestTrophy, false, new[] { 0, 1, 2, 3, 4 }, spritesBestTrophy, shelf),
                    "Deaths"        => GetAward(deaths, true, new[] { 0, 1, 5, 10, 11 }, spritesDeathsTrophy, shelf),
                    "Checkpoints"   => GetAward(checkpointsSkipped, false, new[] { 0, 1, 2, 3, 4, 5 }, spritesCheckpointTrophy, shelf),
                    "Days"          => GetAward(daysCompleted, false, new[] { 0, 1, 7, 30, 90, 180, 365 }, spritesDaysTrophy, shelf),
                    "Timer"         => GetAward((int)bestTime, true, new[] { 5, 6, 7, 10, 15, 20, 21 }, spritesTimerTrophy, shelf),
                    _ => null
                };
            }
        }
    }

    private void SelectShelf(string name, bool animate)
    {
        if(name == this.shelfSelected) return;

        //Debug.Log(name);

        var v = ActiveVariant();
        var container = v.scrollContainer.Find($"Cabinet/{name}") as RectTransform;
        var infoPanel = v.infoPanel;
        var targetY = 0f;
        if(name != null)
            targetY = container.anchoredPosition.y + 50;

        IEnumerator Animate()
        {
            if(this.shelfSelected != null)
            {
                var tweener = new Tweener();
                tweener.MoveAnchoredPosition(infoPanel, new Vector3(v.referenceWidth, infoPanel.anchoredPosition.y), 0.1f);
                yield return tweener.Run();
            }
            if(name == null)
            {
                this.coroutineInfoPanel = null;
                yield break;
            }
            else
            {
                UpdateInfoPanelText(container.name);
                infoPanel.anchoredPosition = new Vector2(v.referenceWidth, targetY);
                var tweener = new Tweener();
                tweener.MoveAnchoredPosition(infoPanel, new Vector3(0, targetY), 0.1f);
                yield return tweener.Run();
            }
            this.coroutineInfoPanel = null;
        }

        if(animate == true)
        {
            if(this.coroutineInfoPanel != null) StopCoroutine(this.coroutineInfoPanel);
            this.coroutineInfoPanel = StartCoroutine(Animate());
        }
        else
        {
            if(name == null)
                infoPanel.anchoredPosition = new Vector2(v.referenceWidth, 0);
            else
            {
                UpdateInfoPanelText(container.name);
                infoPanel.anchoredPosition = new Vector2(0, targetY);
            }
        }
        this.shelfSelected = name;
    }

    private void UpdateInfoPanelText(string name)
    {
        foreach(var variant in this.variants)
        {
            var title = variant.infoPanel.Find("Title").GetComponent<LocalizeText>();
            var text = variant.infoPanel.Find("Text").GetComponent<LocalizeText>();
            var award = variant.awards[name];

            switch(name)
            {
                case "Tutorial":
                    title.SetText("Graduation Cup");
                    if(award.isTrophy == true)
                        text.SetText("Completed the trial stage to graduate for the main event!");
                    else
                        text.SetText("Complete the trial stage to graduate for the main event.");
                    break;
                case "Best":
                    title.SetText("Best Cup");
                    if(award.isTrophy == true)
                        text.SetText("Reached the {0} checkpoint!", award.index switch
                        {
                            1 => "Bronze Cup",
                            2 => "Silver Cup",
                            3 => "Gold Cup",
                            4 => "Fruit Cup",
                            _ => ""
                        });
                    else
                        text.SetText("Reach a trophy checkpoint.");
                    break;
                case "Deaths":
                    title.SetText("Deaths Cup");
                    if(award.isTrophy == true)
                    {
                        if(award.threshold == 1)
                            text.SetText("Completed a day with 1 death!");
                        else
                            text.SetText("Completed a day with {0} deaths!", award.index switch
                            {
                                1 => "0",
                                2 => "1",
                                3 => "5 or less",
                                4 => "10 or less",
                                _ => ""
                            });

                    }
                    else
                        text.SetText("Complete a day with 10 or less deaths.");
                    break;
                case "Checkpoints":
                    title.SetText("Checkpoints Cup");
                    if(award.isTrophy == true)
                    {
                        if(award.threshold == 1)
                            text.SetText("Skipped 1 checkpoint without activating it!", award.threshold + "");
                        else
                            text.SetText("Skipped {0} Checkpoints without activating them!", award.threshold + "");
                    }
                    else
                        text.SetText("Skip a Checkpoint without activating it.");
                    break;
                case "Days":
                    title.SetText("Days Cup");
                    if(award.isTrophy == true)
                    {
                        if(award.threshold == 1)
                            text.SetText("Completed 1 day!", award.threshold + "");
                        else
                            text.SetText("Completed {0} days!", award.threshold + "");
                    }
                    else
                        text.SetText("Complete a day.");
                    break;
                case "Timer":
                    title.SetText("Timer Cup");
                    if(award.isTrophy == true)
                        text.SetText("Completed a day in {0} minutes or less!", award.threshold + "");
                    else
                        text.SetText("Complete a day in 20 minutes or less.");
                    break;
            }
        }
    }

    public void OnClickExit()
    {
        if (this.isLeaving == true)
            return;
        this.isLeaving = true;

        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack);

        var variant = ActiveVariant();
        var stairsX = variant.player.TargetXForObject(variant.player.stairs);
        variant.player.WalkToX(stairsX - 1000, () =>
        {
            ArcadeScreen.enterThroughTrophyDoor = true;
            Transition.GoSimple("Arcade");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn, null, new Vector3(0, 0, 0));
        });

        this.selectionCursor.gameObject.SetActive(false);
        variant.player.canPlayStairSfx = true;
    }

    public void OnClickUp() => StartScroll(this.scrollUpTarget + 1);
    public void OnClickDown() => StartScroll(this.scrollUpTarget - 1);

    private void UpdateButtonsVisible()
    {
        this.variantLandscape.buttonUp.gameObject.SetActive(scrollUpTarget < this.variantLandscape.scrollMax);
        this.variantPortrait.buttonUp.gameObject.SetActive(scrollUpTarget < this.variantPortrait.scrollMax);
        this.variantLandscape.buttonDown.gameObject.SetActive(scrollUpTarget > 0);
        this.variantPortrait.buttonDown.gameObject.SetActive(scrollUpTarget > 0);

        // shelf buttons interfere with arrow button selection
        foreach(var variant in this.variants)
            foreach(var kv in variant.awards)
            {
                if(kv.Value != null)
                {
                    var nav = new Navigation();
                    nav.mode = IsShelfInsideScreen(kv.Key, variant) == true ? Navigation.Mode.Automatic : Navigation.Mode.None;
                    kv.Value.button.navigation = nav;
                }
            }
    }

    private bool IsShelfInsideScreen(string name, Variant variant)
    {
        if(name != null && variant.awards.TryGetValue(name, out Award award) == true && award != null)
        {
            var cameraHeight = variant.referenceWidth / Camera.main.aspect;
            var shelf = variant.awards[name].shelf;
            var cabinet = shelf.parent as RectTransform;
            var minScreen = -(ActiveVariant().scrollContainer.anchoredPosition.y + cabinet.anchoredPosition.y);
            var maxScreen = minScreen + cameraHeight;
            var minShelf = shelf.anchoredPosition.y - ActiveVariant().infoPanel.sizeDelta.y * 0.7f;
            var maxShelf = shelf.anchoredPosition.y + shelf.sizeDelta.y;

            //if(variant == ActiveVariant())
            //{
            //    Debug.Log(name);
            //    Debug.Log($"{minShelf} {minScreen} {minShelf < minScreen}");
            //    Debug.Log($"{maxShelf} {maxScreen} {maxShelf > maxScreen}");
            //}

            return minShelf > minScreen && maxShelf < maxScreen;
        }
        return true;
    }

    private void StartScroll(float target)
    {
        if(this.coroutineScrollContainer != null)
            StopCoroutine(this.coroutineScrollContainer);
        this.coroutineScrollContainer = StartCoroutine(Scroll(target, true));
    }

    private void UpdateScrolling()
    {
        foreach(var variant in this.variants)
        {
            UpdateScrollDist(variant);
            variant.scrollContainer.anchoredPosition = new Vector2(
                0, variant.scrollDist * this.scrollUpAmount * -1
            );
        }
    }

    private IEnumerator Scroll(float target, bool fromButton)
    {
        Audio.instance.PlaySfx(sfxForMenu.sfxUICalendarMonthswipe);
        StartCoroutine(Audio.instance.ChangeSfxPitch(sfxForMenu.sfxUICalendarMonthswipe, setTo: 0.8f));

        target = Mathf.Clamp(target, 0, (int)ActiveVariant().scrollMax);

        if(target != this.scrollUpTarget) SelectShelf(null, true);

        var up = target > scrollUpTarget;

        this.scrollUpTarget = target;

        var tweener = new Tweener();
        var tween = tweener.TweenFloat(
            this.scrollUpAmount, this.scrollUpTarget, 1, Easing.QuadEaseInOut
        );
        while(!tweener.IsDone())
        {
            tweener.Advance(Time.deltaTime);
            this.scrollUpAmount = tween.Value();
            UpdateScrolling();
            yield return null;
        }

        UpdateButtonsVisible();

        this.selectionCursor.gameObject.SetActive(true);

        var v = ActiveVariant();

        if(fromButton == true)
        {
            if(up == true)
                this.selectionCursor.Select(this.scrollUpAmount < v.scrollMax ? v.buttonUp : v.buttonDown, false);
            else
                this.selectionCursor.Select(this.scrollUpAmount > 0 ? v.buttonDown : v.buttonUp, false);
        }

        this.coroutineScrollContainer = null;
    }

    private void UpdateScrollDist(Variant variant)
    {
        
        var heightCabinet = ActiveVariant().scrollContainer.Find("Cabinet/Top").
            GetComponent<RectTransform>().anchoredPosition.y + 500;
        var cameraHeight = variant.referenceWidth / Camera.main.aspect;
        variant.scrollDist = cameraHeight * 0.6f;
        variant.scrollMax = Mathf.Ceil((heightCabinet - cameraHeight) / variant.scrollDist);
    }

    private void StartScrollRespectingSnap(SnapDirection snapDirection)
    {
        int target = snapDirection switch
        {
            SnapDirection.Up => Mathf.CeilToInt(this.scrollUpAmount),
            SnapDirection.Down => Mathf.FloorToInt(this.scrollUpAmount),
            _ => Mathf.RoundToInt(this.scrollUpAmount)
        };

        this.currentScrollCoroutine = StartCoroutine(Scroll(target, false));
    }

    private void UpdateDragging()
    {
        if(Input.GetMouseButtonDown(0) == true &&
            IsMouseOverDragRegion() == true)
        {
            if(this.currentScrollCoroutine != null)
            {
                StopCoroutine(this.currentScrollCoroutine);
                this.currentScrollCoroutine = null;
            }

            this.hasDragged = false;
            this.isDragging = true;
            this.lastMousePosition = this.mouseDownPosition = Input.mousePosition;
        }
        else if(this.isDragging == true && Input.GetMouseButton(0) == false)
        {
            var speed = (Input.mousePosition.y - this.lastMousePosition.y);
            SnapDirection snap = 0;
            if(speed > 10) snap = SnapDirection.Down;
            if(speed < -10) snap = SnapDirection.Up;

            this.isDragging = false;
            this.hasDragged = false;
            StartScrollRespectingSnap(snap);
        }
        else if(this.isDragging == true)
        {
            var v = ActiveVariant();
            float dy =
                (Input.mousePosition.y - this.lastMousePosition.y) / v.scrollDist;
            this.scrollUpAmount -= dy;
            if(Mathf.Abs(Input.mousePosition.y - this.mouseDownPosition.y) > 20)
                this.hasDragged = true;
            UpdateScrolling();
            this.lastMousePosition = Input.mousePosition;
        }
    }

    private bool IsMouseOverDragRegion()
    {
        var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        foreach(var r in results)
        {
            if(r.gameObject.GetComponent<UnityEngine.UI.Button>())
            {
                // ignore shelf buttons
                if(r.gameObject.transform.parent?.parent?.name == "Cabinet")
                    return true;

                return false;
            }
        }
        return true;
    }

    public void Update()
    {
        Game.UpdateCameraLetterboxing();
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        if(this.isLeaving == false)
            UpdateDragging();

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortrait.container.gameObject.SetActive(
            activeVariant == this.variantPortrait
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);

        if(this.coroutineScrollContainer == null && this.selectionCursor.IsActive == true)
        {
            if(this.selectionCursor.selection?.name == "Shelf Button")
                SelectShelf(this.selectionCursor.selection.transform.parent.name, true);
            else
                SelectShelf(null, true);
        }
    }
}

