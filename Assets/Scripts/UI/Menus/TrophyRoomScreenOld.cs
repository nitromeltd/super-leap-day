using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class TrophyRoomScreenOld : MonoBehaviour
{
    public CanvasScaler canvasScaler;
    public SelectionCursor selectionCursor;
    public SfxForMenu sfxForMenu;

    [Serializable]
    public class Variant
    {
        public Transform container;
        public PlayerForMenu player;
        public RectTransform scrollContainer;
        public RectTransform ceilingExitContainer;
        public UnityEngine.UI.Button buttonUp;
        public UnityEngine.UI.Button buttonDown;
        public int referenceWidth;
    }
    public Variant variantLandscape;
    public Variant variantPortrait;
    private bool isLeaving = false;
    private float scrollUpAmount = 0;

    public Variant ActiveVariant() =>
        GameCamera.IsLandscape() ? this.variantLandscape : this.variantPortrait;

    public AudioClip music;

    public void Start()
    {
        Game.InitScene();
        Audio.instance.PlayMusic(this.music);

        {
            var player = this.variantLandscape.player;
            var stairsX = player.TargetXForObject(player.stairs);
            player.rectTransform.anchoredPosition = new Vector2(stairsX - 300, 0);
            player.WalkToX(stairsX + 400);
        }
        {
            var player = this.variantPortrait.player;
            var stairsX = player.TargetXForObject(player.stairs);
            player.rectTransform.anchoredPosition = new Vector2(stairsX - 300, 0);
            player.WalkToX(stairsX + 650);
        }

        UpdateTrophies();

        UpdateButtonsVisible();

        Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeOut, null, new Vector3(0, 0, 0));
    }

    private void UpdateTrophies()
    {
        foreach (var variant in new [] { this.variantLandscape, this.variantPortrait })
        {
            foreach (var game in new [] {
                (type: minigame.Minigame.Type.Golf, rowName: "Golf Day"),
                (type: minigame.Minigame.Type.Racing, rowName: "Racing Day"),
                (type: minigame.Minigame.Type.Pinball, rowName: "Pinball Day")
            })
            {
                var gameContainer =
                    variant.scrollContainer.Find($"Cabinet/{game.rowName}");

                foreach (var difficulty in new [] {
                    (type: MinigameDifficulty.Beginner,     name: "Beginner"),
                    (type: MinigameDifficulty.Intermediate, name: "Intermediate"),
                    (type: MinigameDifficulty.Advanced,     name: "Advanced"),
                    (type: MinigameDifficulty.Expert,       name: "Expert")
                })
                {
                    var trophyGameObject =
                        gameContainer.Find($"{difficulty.name} Trophy").gameObject;
                    var medalsContainer =
                        (RectTransform)gameContainer.Find($"{difficulty.name} Medals");

                    int collected = 0;
                    for (int n = 0; n < 4; n += 1)
                    {
                        if (SaveData.IsMinigameCompleted(game.type, difficulty.type, n))
                            collected += 1;
                    }

                    UpdateTrophyAndMedals(trophyGameObject, medalsContainer, collected);
                }
            }
        }

        void UpdateTrophyAndMedals(
            GameObject trophyContainer,
            RectTransform medalsContainer,
            int medalsCollected
        )
        {
            trophyContainer.gameObject.SetActive(medalsCollected == 5);
            medalsContainer.gameObject.SetActive(medalsCollected < 5);

            for (int n = 0; n < medalsContainer.childCount; n += 1)
            {
                var medal = medalsContainer.GetChild(n) as RectTransform;
                medal.gameObject.SetActive(n < medalsCollected);
                if (n < medalsCollected)
                {
                    float x = 146 * (n - ((medalsCollected - 1) / 2f));
                    medal.anchoredPosition = new Vector2(x, 0);
                }
            }
        }
    }

    public void OnClickUp() => Scroll(true);
    public void OnClickDown() => Scroll(false);

    private void Scroll(bool up)
    {
        IEnumerator Go()
        {
            Audio.instance.PlaySfx(sfxForMenu.sfxUICalendarMonthswipe);
            StartCoroutine(Audio.instance.ChangeSfxPitch(sfxForMenu.sfxUICalendarMonthswipe, setTo: 0.8f));

            this.selectionCursor.gameObject.SetActive(false);

            this.variantLandscape.buttonUp.gameObject.SetActive(false);
            this.variantLandscape.buttonDown.gameObject.SetActive(false);
            this.variantPortrait.buttonUp.gameObject.SetActive(false);
            this.variantPortrait.buttonDown.gameObject.SetActive(false);

            var tweener = new Tweener();
            var tween = tweener.TweenFloat(
                this.scrollUpAmount, up ? 1 : 0, 1, Easing.QuadEaseInOut
            );
            while (!tweener.IsDone())
            {
                tweener.Advance(Time.deltaTime);
                this.scrollUpAmount = tween.Value();
                UpdateScrolling();
                yield return null;
            }

            UpdateButtonsVisible();

            this.selectionCursor.gameObject.SetActive(true);
            this.selectionCursor.Select(
                up ? ActiveVariant().buttonDown : ActiveVariant().buttonUp, false
            );
        }

        StartCoroutine(Go());
    }

    private bool ShouldShowScrollButtons()
    {
        return (float)Screen.height / (float)Screen.width < 1.9f;
    }

    private void UpdateButtonsVisible()
    {
        var scroll = ShouldShowScrollButtons();
        var up = this.scrollUpAmount >= 0.5f;
        this.variantLandscape.buttonUp.gameObject.SetActive(scroll && !up);
        this.variantLandscape.buttonDown.gameObject.SetActive(scroll && up);
        this.variantPortrait.buttonUp.gameObject.SetActive(scroll && !up);
        this.variantPortrait.buttonDown.gameObject.SetActive(scroll && up);

        this.variantPortrait.ceilingExitContainer.gameObject.SetActive(scroll);
    }

    private void UpdateScrolling()
    {
        var cameraHeight = this.canvasScaler.referenceResolution.x / Camera.main.aspect;
        float top = 5300 - cameraHeight;
        this.variantLandscape.scrollContainer.anchoredPosition = new Vector2(
            0, top * this.scrollUpAmount * -1
        );
        this.variantPortrait.scrollContainer.anchoredPosition = new Vector2(
            0, top * this.scrollUpAmount * -1
        );
    }

    public void OnClickExit()
    {
        if (this.isLeaving == true)
            return;
        this.isLeaving = true;

        Audio.instance.PlaySfx(sfxForMenu.sfxUIBack);

        var variant = ActiveVariant();
        var stairsX = variant.player.TargetXForObject(variant.player.stairs);
        variant.player.WalkToX(stairsX - 1000, () =>
        {
            ArcadeScreen.enterThroughTrophyDoor = true;
            Transition.GoSimple("Arcade");
            Audio.instance.PlaySfx(sfxForMenu.sfxUIScreenwipeIn, null, new Vector3(0, 0, 0));
        });

        this.selectionCursor.gameObject.SetActive(false);
        variant.player.canPlayStairSfx = true;
    }

    public void Update()
    {
        Game.UpdateCameraLetterboxing();
        GameInput.Advance();
        Animated.AdvanceAll(Time.deltaTime);

        var activeVariant = ActiveVariant();

        this.variantLandscape.container.gameObject.SetActive(
            activeVariant == this.variantLandscape
        );
        this.variantPortrait.container.gameObject.SetActive(
            activeVariant == this.variantPortrait
        );
        this.canvasScaler.referenceResolution =
            new Vector2(activeVariant.referenceWidth, 600);
    }
}

