using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MultiplayerConnectionProblemsPanel : MonoBehaviour
{
    public LocalizeText playerNumberText;
    public TextMeshProUGUI tryingToReconnectText;
    public RectTransform tryingToReconnectSpinner;
    public Transform symbolYolk;
    public Transform symbolSprout;
    public Transform symbolPuffer;
    public Transform symbolGoop;
    public Transform symbolTheX;

    public void Init()
    {
        Hide();
    }

    private void AlignSpinnerWithText()
    {
        this.tryingToReconnectText.ForceMeshUpdate();

        var pos = this.tryingToReconnectSpinner.anchoredPosition;
        pos.x = this.tryingToReconnectText.textBounds.max.x + 5;
        this.tryingToReconnectSpinner.anchoredPosition = pos;
    }

    public void Show()
    {
        if (this.gameObject.activeSelf == false)
        {
            this.gameObject.SetActive(true);

            AlignSpinnerWithText();

            int thisPlayerNumber = GameCenterMultiplayer.PlayerNumber;
            int otherPlayerNumber = (thisPlayerNumber == 1) ? 2 : 1;
            this.playerNumberText.SetText("player {0}", otherPlayerNumber.ToString());

            Character character = Character.Yolk;
            if (Game.instance?.map1?.ghostPlayers?.Count > 0)
                character = Game.instance.map1.ghostPlayers[0].Character ?? character;

            this.symbolYolk  .gameObject.SetActive(character == Character.Yolk);
            this.symbolSprout.gameObject.SetActive(character == Character.Sprout);
            this.symbolPuffer.gameObject.SetActive(character == Character.Puffer);
            this.symbolGoop  .gameObject.SetActive(character == Character.Goop);
            this.symbolTheX  .gameObject.SetActive(character == Character.King);
        }
    }

    public void Hide()
    {
        if (this.gameObject.activeSelf == true)
            this.gameObject.SetActive(false);
    }

    public void OnPressExitGame()
    {
        Transition.GoSimple("Calendar");
    }

    public void OnPressRevertTo1P()
    {
        GameCenterMultiplayer.EndMultiplayerGame();
        Game.instance.map1.CreateGhostPlayers();
        Hide();
    }
}
