using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MultiplayerLastPlayerCutoff
{
    public const int ExtraTimeSeconds = 10;

    private bool[] localPlayersFinished = new [] { false, false, false, false };
    private bool cutoffActive = false;
    private int frameNumberCutoffBegan;
    private int? cutoffDurationReceivedFromGamekit;
    private bool hasSentTimeLeftMessage;
    private List<Action> onAllPlayersFinished = new();

    public int FrameNumberCutoffBegan => this.frameNumberCutoffBegan;

    public void AddDelegateWhenAllPlayersFinished(Action action)
    {
        if (action != null)
            this.onAllPlayersFinished.Add(action);
    }

    public void NotifyLocalPlayerHasFinished(int mapNumber)
    {
        this.localPlayersFinished[mapNumber - 1] = true;
        Recheck();
    }

    public void NotifyRemotePlayerHasFinished()
    {
        Recheck();

        if (GameCenterMultiplayer.HaveAllRemotePlayersReachedTheGoldTrophy() == true &&
            Game.instance.map1.finished == false &&
            this.hasSentTimeLeftMessage == false)
        {
            int timeLeftInFrames = (10 * 60) + (Game.instance.map1.timerCollected * 60);
            GameCenterMultiplayer.NotifyTimeLeftInFrames(timeLeftInFrames);
            this.hasSentTimeLeftMessage = true;
        }
    }

    public void NotifyRemotePlayerHasTimeLeftInFrames(int frames)
    {
        this.cutoffDurationReceivedFromGamekit = frames;
    }

    private void MarkCutoffStart()
    {
        if (this.cutoffActive == false)
        {
            this.cutoffActive = true;
            this.frameNumberCutoffBegan = Game.instance.map1.frameNumber;
        }
    }

    private void Recheck()
    {
        var timer = IngameUI.instance.CurrentLayout().multiplayerTimer;

        if (Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.GameKit)
        {
            var localPlayerFinished = this.localPlayersFinished[0];
            string playerIDStillPlaying = GameCenterMultiplayer.OnlyOneRemotePlayerStillPlaying();

            if (localPlayerFinished == false &&
                GameCenterMultiplayer.HaveAllRemotePlayersReachedTheGoldTrophy() == true)
            {
                MarkCutoffStart();
                timer.ShowOnLastPlayer(1);
            }
            else if (localPlayerFinished == true && playerIDStillPlaying != null)
            {
                MarkCutoffStart();

                var ghost = Game.instance.map1.ghostPlayers.Where(
                    g => g.trackingMultiplayerPlayerId == playerIDStillPlaying
                ).FirstOrDefault();

                if (ghost != null)
                    timer.ShowOnLastGhostPlayer(ghost);
            }
            else if (localPlayerFinished == true &&
                GameCenterMultiplayer.HaveAllRemotePlayersReachedTheGoldTrophy() == true)
            {
                timer.Hide();
                CallbackAllPlayersFinished();
            }
        }
        else if (Multiplayer.StyleOfMultiplayerGame() == Multiplayer.Style.SplitScreen)
        {
            int playersNotYetFinished = 0;
            int mapNumberOfPlayerNotFinished = 1;

            for (int n = 0; n < Game.instance.maps.Length; n += 1)
            {
                if (this.localPlayersFinished[n] == false)
                {
                    playersNotYetFinished += 1;
                    mapNumberOfPlayerNotFinished = n + 1;
                }
            }

            if (playersNotYetFinished == 1)
            {
                MarkCutoffStart();
                timer.ShowOnLastPlayer(mapNumberOfPlayerNotFinished);
            }
            else if (playersNotYetFinished == 0)
            {
                timer.Hide();
                CallbackAllPlayersFinished();
            }
        }
        else
        {
            // not a multiplayer game, but we still need to fire the callback
            if (this.localPlayersFinished[0] == true)
                CallbackAllPlayersFinished();
        }
    }

    public int TimeLeftInFrames()
    {
        int endTime;
        if (this.cutoffDurationReceivedFromGamekit.HasValue)
        {
            endTime =
                this.frameNumberCutoffBegan +
                this.cutoffDurationReceivedFromGamekit.Value;
        }
        else
        {
            endTime = this.frameNumberCutoffBegan + (ExtraTimeSeconds * 60);
            if (this.localPlayersFinished[0] == false)
                endTime += Game.instance.map1.timerCollected * 60;
            else if (Game.instance.map2 != null && this.localPlayersFinished[1] == false)
                endTime += Game.instance.map2.timerCollected * 60;
            else if (Game.instance.map3 != null && this.localPlayersFinished[2] == false)
                endTime += Game.instance.map3.timerCollected * 60;
            else if (Game.instance.map4 != null && this.localPlayersFinished[3] == false)
                endTime += Game.instance.map4.timerCollected * 60;
        }

        return endTime - Game.instance.map1.frameNumber;
    }

    public void Advance()
    {
        if (this.cutoffActive == true && TimeLeftInFrames() <= 0)
        {
            for (int n = 0; n < Game.instance.maps.Length; n += 1)
            {
                if (this.localPlayersFinished[n] == false)
                {
                    IngameUI.instance.CurrentLayout().multiplayerTimer.Hide();
                    KillPlayerAndMoveToTrophy(Game.instance.maps[n].player);
                    break;
                }
            }

            CallbackAllPlayersFinished();
        }
    }

    private void KillPlayerAndMoveToTrophy(Player player)
    {
        player.StartCoroutine(Sequence());

        IEnumerator Sequence()
        {
            player.Hit(player.position);

            yield return new WaitForSeconds(1f);
            
            player.gameObject.SetActive(false);

            var lastChunk = player.map.chunks[^1];
            var trophy = lastChunk.AnyItem<Trophy>();
            if (trophy == null) yield break;

            player.position = trophy.position + (Vector2.up * 5f);
            player.state = Player.State.Respawning;
            player.alive = true;
            player.map.StartCoroutine(trophy._FinishWithoutPlayer());
        }
    }

    private void CallbackAllPlayersFinished()
    {
        this.cutoffActive = false;

        foreach (var action in this.onAllPlayersFinished)
        {
            action.Invoke();
        }

        this.onAllPlayersFinished.Clear();
    }
}
