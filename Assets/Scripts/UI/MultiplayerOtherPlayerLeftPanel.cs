using UnityEngine;

public class MultiplayerOtherPlayerLeftPanel : MonoBehaviour
{
    public void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void Show()
    {
        if (this.gameObject.activeSelf == false)
            this.gameObject.SetActive(true);
    }
    public void Hide()
    {
        if (this.gameObject.activeSelf == true)
            this.gameObject.SetActive(false);
    }
}
