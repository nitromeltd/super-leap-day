using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;

public class PauseMenu : MonoBehaviour
{
    public ThemesAsset themesAsset;

    public TMPro.TMP_Text pauseText;
    public Image pauseSymbol;
    [NonSerialized] public Material circleMaterial;
    public RectTransform circleImageTransform;
    public RectTransform uiTransform;

    public RectTransform buttonExit;
    public RectTransform buttonCalendar;
    public RectTransform buttonResume;
    public RectTransform buttonReturnToCheckpoint;
    public RectTransform buttonWarpToMultiplayerRoom;
    public RectTransform buttonDevConsole;
    public RectTransform buttonSoundEffects;
    public RectTransform buttonMusic;
    public Slider sliderSfx;
    public Slider sliderMusic;
    public GameObject sfxButtonIconOn;
    public GameObject sfxButtonIconOff;
    public GameObject musicButtonIconOn;
    public GameObject musicButtonIconOff;
    public SelectionCursor selectionCursor;

    public AnimationCurve circleInAnimationCurve;
    public AnimationCurve buttonInAnimationCurve;

    [NonSerialized] public bool isOpen = false;
    [NonSerialized] public float circleOpenAmount = 1;
    [NonSerialized] public float circleClosedAmount = 0;
    [NonSerialized] public float timeToCloseVolumeSliders = 0;

    public static int Radius = Shader.PropertyToID("_Radius");
    public static int OutlineThickness = Shader.PropertyToID("_OutlineThickness");
    // public static int AntialiasThreshold = Shader.PropertyToID("_AntialiasThreshold");

    private bool isAnimating;
    private bool inMainGame;
    private minigame.Minigame game;

    public void Init()
    {
        var circleImage = this.circleImageTransform.GetComponent<Image>();
        this.circleMaterial = circleImage.material = new Material(circleImage.material);
        this.circleMaterial.SetFloat(Radius, 0.5f);

        if (GameObject.Find("Game").GetComponent<Game>() != null)
        {
            this.inMainGame = true;
        }
        else
        {
            this.inMainGame = false;
            this.game = GameObject.Find("Game").GetComponent<minigame.Minigame>();
        }

        this.gameObject.SetActive(false);
        this.isAnimating = false;
        this.buttonWarpToMultiplayerRoom.gameObject.SetActive(
            Multiplayer.IsMultiplayerGame() == false &&
            Game.IsOpeningTutorialSelected == false
        );
        this.buttonDevConsole.gameObject.SetActive(SaveData.IsDeveloperModeEnabled());
    }

    public void Open()
    {
        this.isOpen = true;
        SetupColours();
        this.gameObject.SetActive(true);
        this.selectionCursor.gameObject.SetActive(false);
        StartCoroutine(OpenAnimation());

        this.sliderMusic.value = SaveData.GetMusicVolume();
        this.sliderSfx.value = SaveData.GetSfxVolume();
        CloseVolumeSliders();
        UpdateAudioButtonIcons();

        if(this.inMainGame == true)
        {
            this.buttonReturnToCheckpoint.gameObject.SetActive(
                ShouldShowReturnToCheckpointButton()
            );

            Audio.instance.PlaySfx(Assets.instance.sfxUIWinIn, options: new Audio.Options(1f, false, 0f));
            GameCenter.ShowAccessPoint();
        }
        else
        {
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_in"], options: new Audio.Options(1f, false, 0f));
        }
    }

    private bool ShouldShowReturnToCheckpointButton()
    {
        if (Game.instance == null) return false;
        if (Game.instance.maps == null) return false;
        if (Game.instance.maps.Length != 1) return false;

        var player = Game.instance.map1.player;
        if (player.grabbingOntoItem is MultiplayerHeadset) return false;

        return true;
    }

    private void SetupColours()
    {
        if(Map.instance == null) return;
        var theme = this.themesAsset.ThemeInfo(Map.instance.theme);

        var buttonsColor1 = new [] {
            this.buttonExit.GetComponent<UnityEngine.UI.Button>(),
            this.buttonCalendar.GetComponent<UnityEngine.UI.Button>(),
        };
        var buttonsColor2 = new [] {
            this.buttonReturnToCheckpoint.GetComponent<UnityEngine.UI.Button>(),
            this.buttonWarpToMultiplayerRoom.GetComponent<UnityEngine.UI.Button>(),
            this.buttonDevConsole.GetComponent<UnityEngine.UI.Button>(),
            this.buttonSoundEffects.GetComponent<UnityEngine.UI.Button>(),
            this.buttonMusic.GetComponent<UnityEngine.UI.Button>(),
        };

        var colorShadow = new Color32(79, 79, 79, 0);
        var colorSelected = new Color32(247,228,27,255);

        this.circleImageTransform.GetComponent<Image>().color = ThemesAsset.CircleColor(theme.theme);
        this.pauseText.color = theme.textColour1;
        foreach (var btn in buttonsColor1)
        {
            var cb = btn.colors;
            cb.normalColor = cb.highlightedColor = theme.textColour1;
            cb.pressedColor = theme.textColour1 - colorShadow;
            cb.selectedColor = colorSelected;
            btn.colors = cb;
        }
        foreach (var btn in buttonsColor2)
        {
            var cb = btn.colors;
            cb.normalColor = cb.highlightedColor = theme.textColour2;
            cb.pressedColor = theme.textColour2 - colorShadow;
            cb.selectedColor = colorSelected;
            btn.colors = cb;
        }
    }

    private IEnumerator OpenAnimation()
    {
        this.isAnimating = true;
        var buttons = new []
        {
            this.buttonSoundEffects,
            this.buttonMusic,
            this.buttonCalendar,
            this.buttonResume,
            this.buttonExit,
            this.buttonDevConsole,
            this.buttonReturnToCheckpoint,
            this.buttonWarpToMultiplayerRoom
        };
        this.pauseText.transform.rotation = Quaternion.Euler(0, 0, 175);
        this.pauseSymbol.transform.localScale = new Vector3(0, 0, 1);
        foreach (var btn in buttons)
            btn.transform.localScale = new Vector3(0, 0, 1);

        var easeCircle = Easing.ByAnimationCurve(this.buttonInAnimationCurve);
        var easeButtons = Easing.ByAnimationCurve(this.buttonInAnimationCurve);

        var tweener = new Tweener();
        var tweenForCircle = tweener.TweenFloat(1, 0, 1, easeCircle);
        tweener.ScaleLocal(this.pauseSymbol.transform, Vector3.one, 1, easeCircle);
        tweener.RotateLocal(this.pauseText.transform, 0, 0.4f, Easing.QuadEaseOut, 0.5f);
        for (int n = 0; n < buttons.Length; n += 1)
        {
            var delay = 0.8f + (0.05f * n);
            tweener.ScaleLocal(buttons[n], Vector3.one, 0.5f, easeButtons, delay);
        }

        float timer = 0;
        bool hasPlayedOnce = false;
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();
            timer += Time.unscaledDeltaTime;
            if (timer > .78f && hasPlayedOnce == false)
            {
                hasPlayedOnce = true;
                if (this.inMainGame == true)
                {
                    Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesIn, options: new Audio.Options(1f, false, 0f));
                }
                else
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_pausebubbles_in"], options: new Audio.Options(1f, false, 0f));
                }
            }
            yield return null;
        }

        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.Select(this.buttonResume.GetComponent<Selectable>(), false);
        this.isAnimating = false;
    }

    private IEnumerator CloseAndResumeGameAnimation()
    {
        this.isAnimating = true;
        this.selectionCursor.gameObject.SetActive(false);
    
        var buttons = new []
        {
            this.buttonWarpToMultiplayerRoom,
            this.buttonReturnToCheckpoint,
            this.buttonDevConsole,
            this.buttonExit,
            this.buttonResume,
            this.buttonCalendar,
            this.buttonMusic,
            this.buttonSoundEffects,
        };

        Tween.Ease easeButtons = Easing.QuadEaseIn;
        var tweener = new Tweener();

        for (int n = 0; n < buttons.Length; n += 1)
        {
            tweener.ScaleLocal(buttons[n], Vector3.zero, 0.3f, easeButtons, 0.05f * n);
        }
        tweener.RotateLocal(this.pauseText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
        var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);
        tweener.ScaleLocal(
            this.pauseSymbol.transform, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.5f
        );

        if (this.inMainGame == true)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesOut, options: new Audio.Options(1f, false, 0f));
        }
        else
        {
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_pausebubbles_out"], options: new Audio.Options(1f, false, 0f));
        }

        float timer = 0;
        bool hasPlayedOnce = false;
        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();
            timer += Time.unscaledDeltaTime;
            if (timer > .3f && hasPlayedOnce == false)
            {
                hasPlayedOnce = true;
                if (this.inMainGame == true)
                {
                    Audio.instance.PlaySfx(Assets.instance.sfxUIWinOut, options: new Audio.Options(1f, false, 0f));
                }
                else
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_out"], options: new Audio.Options(1f, false, 0f));
                }
            }
            yield return null;
        }

        this.isOpen = false;
        this.isAnimating = false;
        this.gameObject.SetActive(false);
    }

    private IEnumerator ExitGameAnimation(string targetScene)
    {
        this.selectionCursor.gameObject.SetActive(false);

        var buttons = new []
        {
            this.buttonWarpToMultiplayerRoom,
            this.buttonReturnToCheckpoint,
            this.buttonDevConsole,
            this.buttonExit,
            this.buttonResume,
            this.buttonCalendar,
            this.buttonMusic,
            this.buttonSoundEffects,
        };

        Tween.Ease easeButtons = Easing.QuadEaseIn;
        var tweener = new Tweener();

        for (int n = 0; n < buttons.Length; n += 1)
        {
            tweener.ScaleLocal(buttons[n], Vector3.zero, 0.3f, easeButtons, 0.05f * n);
        }
        tweener.RotateLocal(this.pauseText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
        var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);
        tweener.ScaleLocal(
            this.pauseSymbol.transform, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.5f
        );

        Audio.instance.StopAllSfx(onlyLooped: true);
        if (this.inMainGame == true)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesOut, options: new Audio.Options(1f, false, 0f));
        }
        else
        {
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_pausebubbles_out"], options: new Audio.Options(1f, false, 0f));
        }

        float timer = 0;
        bool hasPlayedOnce = false;
        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleClosedAmount = tweenForCircle.Value();
            UpdateCircleSize();
            timer += Time.unscaledDeltaTime;
            if (timer > .3f && hasPlayedOnce == false)
            {
                hasPlayedOnce = true;
                if (this.inMainGame == true)
                {
                    Audio.instance.PlaySfx(Assets.instance.sfxUIWinOut, options: new Audio.Options(1f, false, 0f));
                }
                else
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_out"], options: new Audio.Options(1f, false, 0f));
                }
            }
            yield return null;
        }
        yield return null;
        yield return null;

        var canvas = this.transform.GetComponentInParent<Canvas>().gameObject;
        DontDestroyOnLoad(canvas);
        for (int n = 0; n < canvas.transform.childCount; n += 1)
        {
            var child = canvas.transform.GetChild(n).gameObject;
            if (child != this.gameObject)
                Destroy(child);
        }
        Destroy(canvas.GetComponent<IngameUI>());

        var minigameUI = canvas.GetComponent<minigame.MinigameUI>();
        if(minigameUI != null)
        {
            minigameUI.game.ExitGame();
            Destroy(minigameUI);
        }

        yield return Game.UnloadCurrentSceneThenLoadSceneAsync(targetScene);
        for (int n = 0; n < 5; n += 1)
        {
            yield return null;
        }

        this.circleOpenAmount = 1;

        tweener = new Tweener();
        tweenForCircle = tweener.TweenFloat(1, 0, 0.5f, Easing.QuadEaseIn);
        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleClosedAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        Destroy(canvas.gameObject);
    }

    private void UpdateCircleSize()
    {
        float width, height;

        if (GameCamera.IsLandscape() == true)
            (width, height) = (700, 400);
        else
            (width, height) = (256, 256 * Screen.height / Screen.width);

        float diagonal = Mathf.Sqrt(width * width + height * height);
        this.circleImageTransform.sizeDelta = new Vector2(diagonal, diagonal);

        float circleStandardSize;
        if (GameCamera.IsLandscape())
            circleStandardSize = 300;
        else
            circleStandardSize = 200;

        float circleRadiusForMaterial = Mathf.LerpUnclamped(
            circleStandardSize / diagonal, 1.0f, this.circleOpenAmount
        ) * (1.0f - this.circleClosedAmount);
        float outlineThickness = circleStandardSize * 0.03f / diagonal;

        this.circleMaterial.SetFloat(Radius, circleRadiusForMaterial);
        this.circleMaterial.SetFloat(OutlineThickness, outlineThickness);

        float scale = circleStandardSize / 220.0f;
        this.uiTransform.localScale = new Vector3(scale, scale, 1);
    }

    private void UpdateAudioButtonIcons()
    {
        bool sfxOn = this.sliderSfx.value > 0;
        bool musicOn = this.sliderMusic.value > 0;
        this.sfxButtonIconOn.gameObject.SetActive(sfxOn == true);
        this.sfxButtonIconOff.gameObject.SetActive(sfxOn == false);
        this.musicButtonIconOn.gameObject.SetActive(musicOn == true);
        this.musicButtonIconOff.gameObject.SetActive(musicOn == false);
    }

    private void CloseVolumeSliders()
    {
        if (this.selectionCursor.selection == this.sliderMusic)
            this.selectionCursor.Select(this.buttonMusic.GetComponent<Selectable>());
        if (this.selectionCursor.selection == this.sliderSfx)
            this.selectionCursor.Select(this.buttonSoundEffects.GetComponent<Selectable>());

        this.sliderMusic.gameObject.SetActive(false);
        this.sliderSfx.gameObject.SetActive(false);
    }

    public void OnPressResume()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(CloseAndResumeGameAnimation());
    }

    public void OnPressDevConsole()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();

        if(IngameUI.instance == null) return;

        this.isOpen = false;
        this.gameObject.SetActive(false);

        DebugPanel.debugPanelEnabled = !DebugPanel.debugPanelEnabled;
        IngameUI.instance.debugPanel.gameObject.SetActive(DebugPanel.debugPanelEnabled);
        IngameUI.instance.debugPanel.Expand();
    }

    public void OnPressReturnToCheckpoint()
    {
        Map.instance?.player.Die();
        OnPressResume();
    }

    public void OnPressWarpToMultiplayerRoom()
    {
        Game.instance.isLeaving = true;
        Time.timeScale = 1;

        var room = Game.instance.map1.AnyItem<MultiplayerRoom>();
        Map.detailsToPersist = new Map.DetailsToPersist
        {
            returnDate = Game.selectedDate,
            returnChunkIndex = 1,
            playerCheckpointPosition =
                room.position.Add(0, 1 + Player.PivotDistanceAboveFeet),
            forceNoEntranceLift = true
        };

        Transition.GoToGame();
        GameCenter.HideAccessPoint();
    }

    public void OnPressMusicButton()
    {
        CloseVolumeSliders();
        this.sliderMusic.gameObject.SetActive(true);
        this.selectionCursor.Select(this.sliderMusic, true);
        this.timeToCloseVolumeSliders = 5;
    }

    public void OnPressSoundEffectsButton()
    {
        CloseVolumeSliders();
        this.sliderSfx.gameObject.SetActive(true);
        this.selectionCursor.Select(this.sliderSfx, true);
        this.timeToCloseVolumeSliders = 5;
    }

    public void OnSfxVolumeValueChanged(float value)
    {
        SaveData.SetSfxVolume(value);
        UpdateAudioButtonIcons();
        this.timeToCloseVolumeSliders = 5;
    }

    public void OnMusicVolumeValueChanged(float value)
    {
        SaveData.SetMusicVolume(value);
        UpdateAudioButtonIcons();
        this.timeToCloseVolumeSliders = 5;
    }

    public void OnMusicSliderConfirm()
    {
        this.sliderMusic.gameObject.SetActive(false);
        this.selectionCursor.Select(
            this.buttonMusic.GetComponent<UnityEngine.UI.Button>(), true
        );
    }

    public void OnSfxSliderConfirm()
    {
        this.sliderSfx.gameObject.SetActive(false);
        this.selectionCursor.Select(
            this.buttonSoundEffects.GetComponent<UnityEngine.UI.Button>(), true
        );
    }

    public void OnPressShop()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Shop"));
    }

    public void OnPressCharacterSelect()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Character Select"));
    }

    public void OnPressTrophyRoom()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Trophy Room"));
    }

    public void OnPressSettings()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Settings"));
    }

    public void OnPressExit()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Main Menu"));
    }

    public void OnPressCalendar()
    {
        CloseVolumeSliders();
        GameCenter.HideAccessPoint();
        StartCoroutine(ExitGameAnimation("Calendar"));
    }

    private void Update()
    {
        if (GameInput.DidAnyonePressPause() &&
            this.isOpen == true &&
            this.isAnimating == false)
        {
            this.buttonResume.GetComponent<UnityEngine.UI.Button>()?.onClick.Invoke();
        }
    }

    private void LateUpdate()
    {
        #if UNITY_EDITOR
            UpdateCircleSize();
        #endif

        if (this.timeToCloseVolumeSliders > 0)
        {
            this.timeToCloseVolumeSliders -= Time.unscaledDeltaTime;
            if (this.timeToCloseVolumeSliders <= 0)
            {
                this.timeToCloseVolumeSliders = 0;
                CloseVolumeSliders();
            }
        }

        //this.pauseText.GetComponent<CircleText>()?.UpdateCurve();
    }
}
