﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectionCursor : MonoBehaviour
{
    public Vector2 borderSize;
    public Vector2 hoverSize;

    [NonSerialized] public Selectable selection;
    [NonSerialized] public UnityEngine.UI.Button selectionAsButton;
    [NonSerialized] public UnityEngine.UI.Toggle selectionAsToggle;
    [NonSerialized] public UnityEngine.UI.Dropdown selectionAsDropdown;
    [NonSerialized] public LD2Button selectionAsLD2Button;
    [NonSerialized] public LD2Slider selectionAsLD2Slider;

    private Image image;
    private bool active = false;
    private GameObject constrainMovementToItemsInsideThisGameobject;
    private Selectable holdingSubmitOnSelectable;
    private RectTransform rectTransform;
    private Coroutine coroutineForMovingCursor;
    private float hoverAnimationTime = 0;

    public bool IsActive { get => this.active; }

    public const float SelectionMoveTime = 0.25f;
    public const float SelectionMoveTimeInsideDropdown = 0.1f;

    public enum Style
    {
        Mobile, TvOS, Desktop
    }

    private void OnEnable()
    {
        this.rectTransform = transform as RectTransform;
        this.image = GetComponent<Image>();
        UpdateActive();
        UpdateImageVisible();
    }

    private void OnDisable()
    {
        if (this.holdingSubmitOnSelectable != null)
        {
            var data = new PointerEventData(EventSystem.current);
            data.button = PointerEventData.InputButton.Left;

            this.holdingSubmitOnSelectable.OnPointerUp(data);
            this.holdingSubmitOnSelectable = null;
        }
        EventSystem.current?.SetSelectedGameObject(null);
    }

    private void UpdateActive()
    {
        var wasActive = this.active;
        this.active = GameInput.usingSelectionCursor;

        if (this.active == true && wasActive == false)
        {
            UpdateSelectionInEventSystem();
            UpdateImageVisible();
        }

        if (this.active == false && wasActive == true)
        {
            if (this.coroutineForMovingCursor != null)
            {
                StopCoroutine(this.coroutineForMovingCursor);
                this.coroutineForMovingCursor = null;
            }

            UpdateSelectionInEventSystem();
            UpdateImageVisible();
        }
    }

    private (Vector2 anchoredPosition, Vector2 sizeDelta) PositionAndSizeForSelection(
        Selectable selection
    )
    {
        var localRect = (selection.transform as RectTransform).rect;
        var targetCenter = selection.transform.TransformPoint(localRect.center);
        Vector2 worldMin = selection.transform.TransformPoint(localRect.min);
        Vector2 worldMax = selection.transform.TransformPoint(localRect.max);
        Vector2 atTargetMin = this.rectTransform.parent.InverseTransformPoint(worldMin);
        Vector2 atTargetMax = this.rectTransform.parent.InverseTransformPoint(worldMax);
        if (atTargetMax.x < atTargetMin.x)
            (atTargetMin.x, atTargetMax.x) = (atTargetMax.x, atTargetMin.x);
        if (atTargetMax.y < atTargetMin.y)
            (atTargetMin.y, atTargetMax.y) = (atTargetMax.y, atTargetMin.y);
        var targetSize = atTargetMax - atTargetMin;
        return (targetCenter, targetSize);
    }

    public void Select(Selectable selection, bool moveTo = true)
    {
        this.selection = selection;
        this.selectionAsButton = selection.GetComponent<UnityEngine.UI.Button>();
        this.selectionAsToggle = selection.GetComponent<UnityEngine.UI.Toggle>();
        this.selectionAsDropdown = selection.GetComponent<UnityEngine.UI.Dropdown>();
        this.selectionAsLD2Button = selection.GetComponent<LD2Button>();
        this.selectionAsLD2Slider = selection.GetComponent<LD2Slider>();
        var parentDropdown = selection.GetComponentInParent<UnityEngine.UI.Dropdown>();

        if (this.coroutineForMovingCursor != null)
        {
            StopCoroutine(this.coroutineForMovingCursor);
            this.coroutineForMovingCursor = null;
        }

        var (position, size) = PositionAndSizeForSelection(selection);

        if (moveTo == true &&
            this.active == true &&
            this.gameObject.activeInHierarchy == true)
        {
            IEnumerator MoveCoroutine()
            {
                var duration = SelectionMoveTime;
                if (this.selectionAsToggle != null && parentDropdown != null)
                    duration = SelectionMoveTimeInsideDropdown;

                var tweener = new Tweener();
                tweener.Move(this.transform, position, duration, Easing.CubicEaseOut);
                var sizeTween = tweener.TweenVector(
                    this.rectTransform.sizeDelta,
                    size + this.borderSize,
                    duration,
                    Easing.CubicEaseOut
                );

                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    this.rectTransform.sizeDelta = sizeTween.Vector();
                    yield return null;
                }

                this.coroutineForMovingCursor = null;
            }
            this.coroutineForMovingCursor = StartCoroutine(MoveCoroutine());
        }
        else
        {
            this.transform.position = position;
            this.rectTransform.sizeDelta = size + this.borderSize;
        }

        this.hoverAnimationTime = 0;

        FocusElementInDropdown();
        UpdateImageVisible();
        UpdateSelectionInEventSystem();
    }

    private void UpdateImageVisible()
    {
        if (this.selection == null || this.active == false)
            this.image.enabled = false;
        else if (this.selectionAsLD2Slider != null)
            this.image.enabled = true;
        else if (this.selection.GetComponentInParent<LD2List>() != null)
            this.image.enabled = false;
        else if (
            this.selectionAsToggle != null &&
            this.selection.GetComponentInParent<Dropdown>() != null
        )
            this.image.enabled = false;
        else if (this.selection.transition == Selectable.Transition.SpriteSwap || this.selection.transition == Selectable.Transition.ColorTint)
            this.image.enabled = false;
        else if (this.selection.GetComponent<GameCenterAccessPointSubstitute>() != null)
            this.image.enabled = false;
        else
            this.image.enabled = true;
    }

    private void UpdateSelectionInEventSystem()
    {
        if (this.active == true && this.selection != null)
            EventSystem.current.SetSelectedGameObject(this.selection.gameObject);
        else
            EventSystem.current.SetSelectedGameObject(null);
    }

    private void FocusElementInDropdown()
    {
        if (this.selectionAsToggle == null) return;

        var dropdown = this.selection.transform.GetComponentInParent<Dropdown>();
        if (dropdown == null) return;

        var goDropdownList = dropdown.transform.Find("Dropdown List");
        var goContent      = dropdown.transform.Find("Dropdown List/Viewport/Content");
        if (goDropdownList == null || goContent == null) return;

        var rtList    = goDropdownList.GetComponent<RectTransform>();
        var rtContent = goContent     .GetComponent<RectTransform>();
        var rtEntry   = this.selection.GetComponent<RectTransform>();
        if (rtList == null || rtContent == null || rtEntry == null) return;

        {
            // scroll up if entry is off the top?
            var entryTop =
                rtEntry.anchoredPosition.y +
                (rtEntry.sizeDelta.y * (1 - rtEntry.pivot.y));
            entryTop += rtContent.anchoredPosition.y;
            entryTop -= rtContent.sizeDelta.y;
            if (entryTop > 0)
                StartCoroutine(ScrollBy(rtContent, -entryTop));
        }

        {
            // scroll down if entry is off the bottom?
            var entryBottom =
                rtEntry.anchoredPosition.y - (rtEntry.sizeDelta.y * rtEntry.pivot.y);
            entryBottom += rtContent.anchoredPosition.y;
            entryBottom -= rtContent.sizeDelta.y;
            if (entryBottom < -rtList.sizeDelta.y)
            {
                var yOffset = -rtList.sizeDelta.y - entryBottom;
                StartCoroutine(ScrollBy(rtContent, yOffset));
            }
        }

        IEnumerator ScrollBy(RectTransform rt, float deltaY)
        {
            for (int n = 0; n < 3; n += 1)
            {
                rt.anchoredPosition += new Vector2(0, deltaY / 3);
                yield return null;
            }
        }
    }

    public void SelectNearestToPosition(bool animateMovement)
    {
        var candidates = new List<Selectable>();

        foreach (var button in FindObjectsOfType<Selectable>())
        {
            if (IsCenterVisible(button.transform) == true && button.navigation.mode != Navigation.Mode.None)
                candidates.Add(button);
        }

        if (candidates.Count == 0)
            return;

        var best = candidates[0];
        var bestSquareDistance = Mathf.Infinity;

        foreach (var candidate in candidates)
        {
            Vector2 delta = candidate.transform.position - this.transform.position;
            var squareDistance = delta.sqrMagnitude;

            if (delta.sqrMagnitude < bestSquareDistance)
            {
                best = candidate;
                bestSquareDistance = delta.sqrMagnitude;
            }
        }

        Select(best, animateMovement);
    }

    public void SelectInDirection(Vector2 direction, bool animateMovement)
    {
        if (this.selection != null)
        {
            Selectable target = null;

            switch (this.selection.navigation.mode)
            {
                case Navigation.Mode.None:
                    break;

                case Navigation.Mode.Horizontal:
                    direction.y = 0;
                    if (direction.x != 0)
                        target = this.selection.FindSelectable(direction);
                    break;

                case Navigation.Mode.Vertical:
                    direction.x = 0;
                    if (direction.y != 0)
                        target = this.selection.FindSelectable(direction);
                    break;

                case Navigation.Mode.Automatic:
                    target = this.selection.FindSelectable(direction);
                    break;

                case Navigation.Mode.Explicit:
                default:
                    if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
                    {
                        if (direction.x > 0)
                            target = this.selection.FindSelectableOnRight();
                        else
                            target = this.selection.FindSelectableOnLeft();
                    }
                    else
                    {
                        if (direction.y > 0)
                            target = this.selection.FindSelectableOnUp();
                        else
                            target = this.selection.FindSelectableOnDown();
                    }
                    break;
            }

            if (target != null && IsCenterVisible(target.transform) == false)
                target = null;

            if (target != null &&
                this.constrainMovementToItemsInsideThisGameobject != null &&
                target.transform.IsChildOf(
                    this.constrainMovementToItemsInsideThisGameobject.transform
                ) == false
            )
            {
                target = null;
            }

            if (target != null)
            {
                Select(target);
                return;
            }

            if (this.selection.navigation.mode == Navigation.Mode.Explicit &&
                target == null)
            {
                return;
            }
        }

        SelectNearestToPosition(false);
    }

    public void ConstrainMovement(GameObject onlyItemsInsideThisGameobject)
    {
        this.constrainMovementToItemsInsideThisGameobject = onlyItemsInsideThisGameobject;
    }

    public void RemoveConstraint()
    {
        this.constrainMovementToItemsInsideThisGameobject = null;
    }

    void Update()
    {
        UpdateActive();

        if (Input.GetMouseButtonUp(0))
        {
            var current = EventSystem.current.currentSelectedGameObject;
            if (current != null)
            {
                if (current.TryGetComponent<Selectable>(out var newSelection))
                    Select(newSelection);
            }

            UpdateSelectionInEventSystem();
        }

        if (this.active == true && this.coroutineForMovingCursor == null)
        {
            if (this.selectionAsLD2Slider != null)
            {
                this.selectionAsLD2Slider.slider.value +=
                    GameInput.menuSlideInput.x * 0.02f;
            }
            else if (GameInput.menuMoveDirection != null)
            {
                SelectInDirection(GameInput.menuMoveDirection.Value, true);
            }

            PointerEventData LeftMouseButton()
            {
                var data = new PointerEventData(EventSystem.current);
                data.button = PointerEventData.InputButton.Left;
                return data;
            }

            if (GameInput.pressedConfirm == true && this.selectionAsLD2Slider != null)
            {
                this.selectionAsLD2Slider.onConfirm.Invoke();
            }
            else if (GameInput.pressedConfirm == true)
            {
                if (this.selection != null)
                    this.selection.OnPointerDown(LeftMouseButton());
                this.holdingSubmitOnSelectable = this.selection;

                Selectable SelectedItemAccordingToEventSystem() =>
                    EventSystem.current.currentSelectedGameObject
                        .GetComponent<Selectable>();

                if (this.selectionAsButton != null)
                {
                    this.selectionAsButton.OnPointerClick(LeftMouseButton());
                }
                else if (this.selectionAsToggle != null)
                {
                    this.selectionAsToggle.OnPointerClick(LeftMouseButton());
                    Select(SelectedItemAccordingToEventSystem());
                }
                else if (this.selectionAsDropdown != null)
                {
                    this.selectionAsDropdown.Show();
                    Select(SelectedItemAccordingToEventSystem());
                }
            }
            else if (
                this.holdingSubmitOnSelectable != null &&
                (
                    this.selection != this.holdingSubmitOnSelectable ||
                    GameInput.holdingConfirm == false
                ))
            {
                this.holdingSubmitOnSelectable.OnPointerUp(LeftMouseButton());
                this.holdingSubmitOnSelectable = null;
            }
        }

        if (this.active == true &&
            this.coroutineForMovingCursor == null &&
            this.selection != null)
        {
            var (position, size) = PositionAndSizeForSelection(this.selection);

            this.hoverAnimationTime += Time.unscaledDeltaTime;
            float s = 0.5f + (0.5f * -Mathf.Cos(this.hoverAnimationTime * 8));
            this.rectTransform.sizeDelta =
                size + this.borderSize + (this.hoverSize * s);
        }
    }

    private static bool IsCenterVisible(Transform transform)
    {
        if (Game.instance?.maps?.Length > 1)
        {
            return true;
        }
        else
        {
            var camera = IngameUI.instance?.UICamera ?? Camera.main;
            var viewPt = camera.WorldToViewportPoint(transform.position);
            if (viewPt.x < 0 || viewPt.x > 1) return false;
            if (viewPt.y < 0 || viewPt.y > 1) return false;
            return true;
        }
    }
}
