using UnityEngine;

[CreateAssetMenu(menuName = "Leap Day 2/Sfx for Menu")]
public class SfxForMenu : ScriptableObject
{
    public AudioClip sfxUIBack;
    public AudioClip sfxUICalendarDayswipe;
    public AudioClip sfxUICalendarMonthswipe;
    public AudioClip sfxUICalendarSwipeDeny;
    public AudioClip sfxUIConfirm;
    public AudioClip sfxUIElevator;
    public AudioClip sfxUIElevatorFloorchange;
    public AudioClip sfxUIScreenwipeIn;
    public AudioClip sfxUIScreenwipeOut;
    public AudioClip sfxUIDoorClose;
    public AudioClip sfxUIDoorOpen;
    public AudioClip sfxUIElevator2floorClose;
    public AudioClip sfxUIElevator2floorOpen;
    public AudioClip sfxUISettingsChainDown;
    public AudioClip sfxUISettingsChainUp;
    public AudioClip sfxUIShopPurchase;
    public AudioClip sfxUIShopPurchaseCoins;
    public AudioClip sfxUIShopPurchaseSoldout;
    public AudioClip sfxUIShopSwipe;
    public AudioClip sfxUIStairs;
    public AudioClip sfxPlayerFootstep;
    public AudioClip sfxPlayerJump;
    public AudioClip sfxPlayerGrab;
    public AudioClip sfxYolkVoice;
    public AudioClip sfxPuffer;
    public AudioClip sfxSprout;
    public AudioClip sfxKing;
    public AudioClip sfxKingRespawn;
    public AudioClip sfxGoop;
    public AudioClip[] sfxShopKeeper;
    public AudioClip sfxNitrome;
}
