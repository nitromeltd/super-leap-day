﻿using System;
using System.Linq;
using UnityEngine;

[CreateAssetMenu]
public class ThemesAsset : ScriptableObject
{
    public Theme[] themes;

    public Theme ThemeInfo(global::Theme theme)
    {
        return themes.Where(t => t.theme == theme).FirstOrDefault();
    }

    public Theme ThemeForDate(DateTime date)
    {
        var theme = LevelGeneration.ThemeForDate(date);
        return themes.Where(t => t.theme == theme).FirstOrDefault();
    }

    public static Color CircleColor(global::Theme theme)
    {
        return theme switch {
            global::Theme.RainyRuins => new Color32(83, 5, 119, 255),
            global::Theme.CapitalHighway => new Color32(88, 29, 76, 255),
            global::Theme.ConflictCanyon => new Color32(53, 57, 17, 255),
            global::Theme.HotNColdSprings => new Color32(57, 33, 83, 255),
            global::Theme.GravityGalaxy => new Color32(15, 35, 89, 255),
            global::Theme.OpeningTutorial => new Color32(52, 50, 114, 255),
            global::Theme.SunkenIsland => new Color32(9, 100, 185, 255),
            global::Theme.TreasureMines => new Color32(50, 0, 53, 255),
            global::Theme.WindySkies => new Color32(103, 225, 255, 255),
            global::Theme.TombstoneHill => new Color32(81, 0, 119, 255),
            global::Theme.MoltenFortress => new Color32(157, 50, 133, 255),
            global::Theme.VRTrainingRoom => new Color32(52, 50, 114, 255),
            _ => Color.white
        };
    }

    [System.Serializable]
    public class Theme
    {
        public string name;
        public global::Theme theme;
        /* Example RichText:
         * <size=79%><color=#1FBEFF>rainy</color></size>
         * <color=#8900C9>ruins</color>
         */
        [Multiline(2)]
        public string richText;
        public Color textColour1;
        public Color textColour2;
        public Sprite calendarBackgroundSprite;
        public Sprite calendarLogoSprite;
    }
}
