﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using TMPro;

public class Transition : MonoBehaviour
{
    public static Transition instance;// transition in progress
    private static TransitionThemed themedInstanceReady;

    public CanvasScaler canvasScaler;
    public RectTransform transformCircle;
    [NonSerialized] public Material circleMaterial;
    public float circleOpenAmount = 1;

    public static int Radius = Shader.PropertyToID("_Radius");

    public static void PrepareThemedTransition()
    {
        var prefab = Resources.Load<GameObject>(NameOfResourceForThemedTransition());
        var obj = Instantiate(prefab);
        obj.SetActive(false);
        themedInstanceReady = obj.GetComponent<TransitionThemed>();
    }

    private static string NameOfResourceForThemedTransition()
    {
        var theme = LevelGeneration.ThemeForDate(Game.selectedDate);
        var suffix = theme switch
        {
            Theme.RainyRuins      => "RainyRuins",
            Theme.CapitalHighway  => "CapitalHighway",
            Theme.ConflictCanyon  => "ConflictCanyon",
            Theme.HotNColdSprings => "HotNColdSprings",
            Theme.GravityGalaxy   => "GravityGalaxy",
            Theme.SunkenIsland    => "SunkenIsland",
            Theme.TreasureMines   => "TreasureMines",
            Theme.WindySkies      => "WindySkies",
            Theme.TombstoneHill   => "TombstoneHill",
            Theme.MoltenFortress  => "MoltenFortress",
            Theme.OpeningTutorial => "OpeningTutorial",
            _                     => "RainyRuins"
        };

        return
            GameCamera.IsLandscape() ?
            "TransitionThemedLandscape_" + suffix :
            "TransitionThemedPortrait_" + suffix;
    }

    public static void GoToGame()
    {
        if(instance != null)
            return;

        TransitionThemed transition;

        if (themedInstanceReady != null)
        {
            transition = themedInstanceReady;
            transition.gameObject.SetActive(true);
            themedInstanceReady = null;
        }
        else
        {
            GameObject prefab =
                Resources.Load<GameObject>(NameOfResourceForThemedTransition());
            var obj = Instantiate(prefab);
            transition = obj.GetComponent<TransitionThemed>();
        }

        transition.Init();
        instance = transition;
    }

    public static void GoToMinigame(minigame.Minigame.Type type, minigame.Minigame.Mode mode)
    {
        if(instance != null)
            return;

        minigame.Minigame.mode = mode;

        GameObject prefab = GameCamera.IsLandscape() ?
            Resources.Load<GameObject>("TransitionMinigameLandscape") :
            Resources.Load<GameObject>("TransitionMinigamePortrait");

        var obj = Instantiate(prefab, null);
        var transition = obj.GetComponent<TransitionMinigame>();
        transition.Init(type);
        instance = transition;
    }

    public static void GoSimple(string scene)
    {
        if(instance != null)
            return;

        GameObject prefab = Resources.Load<GameObject>("TransitionSimple");

        var obj = Instantiate(prefab, null);
        var transition = obj.GetComponent<TransitionSimple>();
        transition.Init(scene);
        instance = transition;
    }

    protected void CanvasScalerAndCircleInit()
    {
        float diagonal;
        if(GameCamera.IsLandscape() == false)
        {
            this.canvasScaler.scaleFactor = Screen.width / 750f;// default mobile width
            diagonal = Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height) * 1.01f / this.canvasScaler.scaleFactor;
        }
        else
        {
            this.canvasScaler.scaleFactor = Screen.width / 3840f;// default tv width
            diagonal = Mathf.Sqrt(Screen.width * Screen.width + Screen.height * Screen.height) * 1.01f / this.canvasScaler.scaleFactor;
        }

        var maxSize = new Vector2(diagonal, diagonal);
        this.transformCircle.sizeDelta = maxSize;
        var circleImage = this.transformCircle.GetComponent<Image>();
        this.circleMaterial = circleImage.material = new Material(circleImage.material);
        this.circleMaterial.SetFloat(Radius, 1f);
    }

    protected void UpdateCircleSize()
    {
        float width, height;

        if(GameCamera.IsLandscape() == true)
            (width, height) = (3840f, 3840f * Screen.height / Screen.width);
        else
            (width, height) = (750f, 750f * Screen.height / Screen.width);

        float diagonal = Mathf.Sqrt(width * width + height * height);
        this.transformCircle.sizeDelta = new Vector2(diagonal, diagonal);

        this.circleMaterial.SetFloat(Radius, this.circleOpenAmount);
    }

    protected static void DayInfoInit(DateTime date, LocalizeText textWeekday, LocalizeText textDate, LocalizeText textYear)
    {
        textWeekday.SetText(date.DayOfWeek.ToString().ToUpper());
        textDate.SetText(
            "DAY_MONTH",
            Localization.GetTextForDayWithOrdinal(date),
            Localization.GetTextForMonthName(date).ToUpper()
        );
        textYear.SetText("YEAR", date.Year.ToString());
    }

    protected static void AddSpokeToPivot(Transform item, List<Transform> pivots, Transform pivot)
    {
        var obj = new GameObject("pivot " + pivots.Count);
        obj.transform.SetParent(pivot);
        obj.transform.localPosition = Vector3.zero;
        item.SetParent(obj.transform, true);
        obj.transform.rotation = Quaternion.Euler(0, 0, 180);
        pivots.Add(obj.transform);
    }
}
