﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TransitionMinigame : Transition
{
    public ThemesAsset themesAsset;
    public RectMask2D pivotMask;

    public RectTransform difficultySelectBronze;
    public RectTransform difficultySelectSilver;
    public RectTransform difficultySelectGold;
    public RectTransform difficultySelectLeft;
    public RectTransform difficultySelectRight;
    public LocalizeText difficultyText;
    public Image[] stageSelectStars;
    public RectTransform stageSelectArrow;
    public Image thumnail;
    public Image trophy;
    public RectTransform transformPreview;
    public RectTransform transformSelector;
    public RectTransform transformStageSelector;

    public AudioClip sfxUIConfirm;
    public AudioClip sfxUISwipe;

    [Serializable]
    public struct TextColours
    {
        public Color textColour1;
        public Color textColour2;
        public Material textOutlineMaterial;
    };

    [Header("Text Colours")]
    public TextColours textColoursGolf;
    public TextColours textColoursRacing;
    public TextColours textColoursPinball;
    public TextColours textColoursFishing;
    public TextColours textColoursRolling;
    public TextColours textColoursAbseiling;
    [Header("Trophies")]
    public Sprite[] spritesGolfTrophy;
    public Sprite[] spritesPinballTrophy;
    public Sprite[] spritesRacingTrophy;
    public Sprite[] spritesFishingTrophy;
    public Sprite[] spritesRollingTrophy;
    public Sprite[] spritesAbseilingTrophy;
    public Sprite spriteStageComplete;
    public AnimationCurve selectorInAnimationCurve;

    private Tweener.Tween tweenForCircle;
    private Transform[] backgroundArc;
    private Transform[] logoArc;
    private List<Transform> spokes;

    private minigame.Minigame.Type minigameType;

    private MinigameDifficulty currentDifficulty;
    private int currentLevelIndex;
    private enum State { Animating, CyclingDifficulties, CyclingStages };
    private State state = State.Animating;
    private float time;
    private Tweener tweener;
    private MinigameDifficulty[] difficulties;
    private Sprite[][] thumbnails;
    private Tween.Ease easeSelectorIn;

    public void Init(minigame.Minigame.Type minigameType)
    {
        this.minigameType = minigameType;
        this.difficulties = DifficultiesPurchased();
        this.easeSelectorIn = Easing.ByAnimationCurve(this.selectorInAnimationCurve);
        this.thumbnails = minigame.Thumbnails.GetThumbnails(minigameType);
        DontDestroyOnLoad(gameObject);
        StartCoroutine(OpenAnimation());
    }

    private IEnumerator OpenAnimation()
    {
        var date = Game.selectedDate;
        this.tweener = new Tweener();

        CanvasScalerAndCircleInit();

        var pivotMask =
            this.transform.Find("pivotMask") ??
            this.transform.Find("16by9Mask/pivotMask");
        pivotMask.Find("golf day")?.gameObject.SetActive(false);
        pivotMask.Find("racing day")?.gameObject.SetActive(false);
        pivotMask.Find("pinball day")?.gameObject.SetActive(false);
        pivotMask.Find("fishing day")?.gameObject.SetActive(false);
        pivotMask.Find("rolling day")?.gameObject.SetActive(false);
        pivotMask.Find("abseiling day")?.gameObject.SetActive(false);

        Transform container = minigameType switch
        {
            minigame.Minigame.Type.Golf         => pivotMask.Find("golf day"),
            minigame.Minigame.Type.Pinball      => pivotMask.Find("pinball day"),
            minigame.Minigame.Type.Racing       => pivotMask.Find("racing day"),
            minigame.Minigame.Type.Fishing      => pivotMask.Find("fishing day"),
            minigame.Minigame.Type.Rolling      => pivotMask.Find("rolling day"),
            minigame.Minigame.Type.Abseiling    => pivotMask.Find("abseiling day"),
            _ => null
        };
        container ??= pivotMask;
        container.gameObject.SetActive(true);

        var pivot = pivotMask.Find("pivot");
        this.backgroundArc = (new[] {
            container.Find("layer6"),
            container.Find("layer5"),
            container.Find("layer4"),
            container.Find("layer3"),
            container.Find("layer2"),
            container.Find("layer1"),
            container.Find("layer0")
        }).Where(t => t != null).ToArray();
        this.logoArc = (new[] {
            container.Find("logo1"),
            container.Find("logo0"),
            pivotMask.Find("topText"),
            pivotMask.Find("bottomText")
        }).Where(t => t != null).ToArray();
        var textThemeBottom =
            pivotMask.Find("bottomText").GetComponent<TMPro.TextMeshProUGUI>();
        var textThemeTop =
            pivotMask.Find("topText").GetComponent<TMPro.TextMeshProUGUI>();


        // title
        {
            // No LocalizeText on the theme text because we're changing materials
            TextColours textColours = this.textColoursGolf;
            string[] lines;
            switch(minigameType)
            {
                case minigame.Minigame.Type.Golf:
                    lines = Localization.GetText("<size=79%><color=#F0A2FF>golf</color></size>\n<color=#BEEC9A>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursGolf;
                    break;
                case minigame.Minigame.Type.Racing:
                    lines = Localization.GetText("<size=79%><color=#8ACDFF>racing</color></size>\n<color=#FFA179>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursRacing;
                    break;
                case minigame.Minigame.Type.Fishing:
                    lines = Localization.GetText("<size=79%><color=#EEB32C>fishing</color></size>\n<color=#FF7DDC>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursFishing;
                    break;
                case minigame.Minigame.Type.Rolling:
                    lines = Localization.GetText("<size=79%><color=#FF7DDC>rolling</color></size>\n<color=#54E467>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursRolling;
                    break;
                case minigame.Minigame.Type.Abseiling:
                    lines = Localization.GetText("<size=79%><color=#68CBAE>abseiling</color></size>\n<color=#FF95C1>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursAbseiling;
                    break;
                default:// Pinball
                    lines = Localization.GetText("<size=79%><color=#BEEC9A>pinball</color></size>\n<color=#FF779E>day</color>".ToUpper()).Split('\n');
                    textColours = this.textColoursPinball;
                    break;
            }
            textThemeTop.text = lines[0];
            textThemeBottom.text = lines[1];
            textThemeTop.fontSharedMaterial = textColours.textOutlineMaterial;
            textThemeBottom.fontSharedMaterial = textColours.textOutlineMaterial;
            Localization.SetTextFont(textThemeTop);
            Localization.SetTextFont(textThemeBottom);
            textThemeTop.alpha = textThemeBottom.alpha = 0;
        }

        // tweening
        this.spokes = new List<Transform>();

        this.tweenForCircle = this.tweener.TweenFloat(1, 0f, 0.5F, Easing.QuadEaseIn);
        int j = 0;
        for(int i = 0; i < this.backgroundArc.Length; i++, j++)
        {
            AddSpokeToPivot(this.backgroundArc[i], this.spokes, pivot);
            var invI = (this.backgroundArc.Length - 1) - i;
            this.tweener.RotateLocal(this.spokes[j], 0, 0.6f - 0.033f * i, Easing.QuadEaseOut, 0.5f - invI * 0.033f);
        }
        for(int i = 0; i < logoArc.Length; i++, j++)
        {
            AddSpokeToPivot(this.logoArc[i], this.spokes, pivot);
            var invI = (this.logoArc.Length - 1) - i;
            this.tweener.RotateLocal(this.spokes[j], 0, (0.6f + invI * 0.04f) - 0.033f * invI, Easing.QuadEaseOut, 0.25f + invI * 0.04f);
        }

        // level selection setup
        this.transformStageSelector.gameObject.SetActive(false);
        this.transformPreview.localScale =
            this.transformSelector.localScale = new Vector3(0, 0, 1);


        yield return null;
        yield return null;
        textThemeTop.alpha = textThemeBottom.alpha = 1f;
        textThemeTop.GetComponent<CircleText>().ForceUpdate();
        textThemeTop.GetComponent<CircleText>().ForceUpdate();

        while(!this.tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = this.tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        yield return null;
        yield return null;

        var asyncOperation = SceneManager.LoadSceneAsync("Empty");
        while (asyncOperation.isDone == false)
        {
            yield return null;
        }

        Assets.instance = null;
        Map.instance = null;
        IngameUI.instance = null;
        Resources.UnloadUnusedAssets();
        GC.Collect();
        yield return null;

        this.tweener = new Tweener();
        this.tweener.ScaleLocal(this.transformSelector, Vector3.one, 0.4f, this.easeSelectorIn, 0.2f);

        var nextState = State.CyclingDifficulties;

        if(this.difficulties.Length == 1)
        {
            InitStageCycling();
            this.trophy.gameObject.SetActive(false);
            this.tweener.ScaleLocal(this.transformPreview, Vector3.one, 0.5f, this.easeSelectorIn);
            nextState = State.CyclingStages;
        }
        else
        {
            this.currentDifficulty = MinigameDifficulty.Beginner;
            for(int i = 0; i < 4; i++)
                if(SaveData.HasCompletedMinigameDifficulty(this.minigameType, (MinigameDifficulty)i) == false) {
                    this.currentDifficulty = (MinigameDifficulty)i;
                    break;
                }
            SetCurrentDifficulty(this.currentDifficulty);
        }


        while (!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        Audio.instance.PlaySfx(sfxUIConfirm);

        yield return new WaitForSeconds(0.5f);

        this.state = nextState;
    }

    private bool Pressed()
    {
        return GameInput.pressedConfirm == true ||
               Input.GetMouseButtonDown(0) == true;
    }

    public void Update()
    {
        if(state != State.Animating)
            GameInput.Advance();

        var delay = 0.75f;
        if(this.state == State.CyclingDifficulties)
        {
            this.time += Time.unscaledDeltaTime;
            if(this.time >= delay)
            {
                this.time -= delay;
                SetCurrentDifficulty(NextDifficulty(this.currentDifficulty));
                Audio.instance.PlaySfx(sfxUISwipe);
            }

            if(Pressed() == true)
            {
                StartCoroutine(minigame.Shake.TransformLocal(new minigame.Shake(Vector2.up, 0.125f, 0.5f, 20f), this.transformSelector));
                InitStageCycling();
                this.tweener = new Tweener();
                this.tweener.ScaleLocal(this.trophy.transform, Vector3.zero, 0.3f, Easing.QuadEaseIn);
                this.tweener.ScaleLocal(this.transformPreview, Vector3.one, 0.5f, this.easeSelectorIn);
                this.state = State.CyclingStages;
                Audio.instance.PlaySfx(sfxUIConfirm);
            }
        }
        else if(this.state == State.CyclingStages)
        {
            if(!this.tweener.IsDone())
                this.tweener.Advance(Time.unscaledDeltaTime);

            this.time += Time.unscaledDeltaTime;
            if(this.time >= delay)
            {
                this.time -= delay;
                this.currentLevelIndex += 1;
                this.currentLevelIndex %= 5;

                this.stageSelectArrow.SetParent(
                    this.stageSelectStars[this.currentLevelIndex].transform, false
                );
                this.thumnail.sprite = this.thumbnails[(int)currentDifficulty][currentLevelIndex];
                Audio.instance.PlaySfx(sfxUISwipe);
            }

            if (Pressed() == true)
            {
                minigame.Minigame.difficulty = this.currentDifficulty;
                minigame.Minigame.level = this.currentLevelIndex;
                StartCoroutine(minigame.Shake.TransformLocal(new minigame.Shake(Vector2.up, 0.125f, 0.5f, 20f), this.transformSelector));

                this.state = State.Animating;
                StartCoroutine(CloseAnimation());
                Audio.instance.PlaySfx(sfxUIConfirm);
            }
        }
    }

    private IEnumerator CloseAnimation()
    {
        // finish tween if player spam clicked through menu
        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        this.tweener = new Tweener();

        this.tweener.ScaleLocal(this.transformPreview, Vector3.zero, 0.3f, Easing.QuadEaseOut, 0.1f);
        this.tweener.ScaleLocal(this.transformSelector, Vector3.zero, 0.3f, Easing.QuadEaseOut, 0.1f);

        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        yield return null;

        string targetScene = this.minigameType switch
        {
            minigame.Minigame.Type.Golf         => "Golf",
            minigame.Minigame.Type.Racing       => "Racing",
            minigame.Minigame.Type.Pinball      => "Pinball",
            minigame.Minigame.Type.Fishing      => "Fishing",
            minigame.Minigame.Type.Rolling      => "Rolling",
            minigame.Minigame.Type.Abseiling    => "Abseiling",
            _ => throw new NotImplementedException()
        };
        var asyncOperation = SceneManager.LoadSceneAsync(targetScene);
        while (asyncOperation.isDone == false)
        {
            yield return null;
        }

        yield return null;
        yield return null;

        this.tweener = new Tweener();

        this.pivotMask.enabled = false;

        this.tweenForCircle = this.tweener.TweenFloat(0, 1f, 0.5F, Easing.QuadEaseIn, 0.3f);


        var j = 0;
        for(int i = 0; i < this.backgroundArc.Length; i++, j++)
        {
            var invI = (this.backgroundArc.Length - 1) - i;
            var duration = 0.6f - 0.033f * i;
            var delay = 0.5f - invI * 0.033f;
            this.tweener.RotateLocal(this.spokes[j], -180, duration, Easing.QuadEaseIn, delay);
            this.tweener.ScaleLocal(this.spokes[j], Vector3.one * 12f, duration, Easing.QuadEaseIn, delay);
        }
        for(int i = 0; i < logoArc.Length; i++, j++)
        {
            var invI = (this.logoArc.Length - 1) - i;
            var duration = (0.6f + invI * 0.04f) - 0.033f * invI;
            var delay = 0.25f + invI * 0.04f;
            this.tweener.RotateLocal(this.spokes[j], -180, duration, Easing.QuadEaseIn, delay);
            this.tweener.ScaleLocal(this.spokes[j], Vector3.one * 12f, duration, Easing.QuadEaseIn, delay);
        }

        while(!this.tweener.IsDone())
        {
            this.tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = this.tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        yield return null;
        yield return null;

        instance = null;
        Destroy(gameObject);
    }


    private void InitStageCycling()
    {
        this.currentLevelIndex = 0;
        var uncompletedLevel = false;
        for(int i = 0; i < 5; i++)
            if(SaveData.IsMinigameCompleted(minigameType, currentDifficulty, i) == true)
                stageSelectStars[i].sprite = this.spriteStageComplete;
            else if(uncompletedLevel == false)
            {
                this.currentLevelIndex = i;
                uncompletedLevel = true;
            }
        this.difficultySelectLeft.gameObject.SetActive(false);
        this.difficultySelectRight.gameObject.SetActive(false);
        this.transformStageSelector.gameObject.SetActive(true);
        this.thumnail.sprite = this.thumbnails[(int)currentDifficulty][currentLevelIndex];
        this.stageSelectArrow.SetParent(
            this.stageSelectStars[this.currentLevelIndex].transform, false
        );
        this.time = 0f;
    }

    private MinigameDifficulty NextDifficulty(MinigameDifficulty current)
    {
        int index = Array.IndexOf(this.difficulties, current);
        index += 1;
        index %= this.difficulties.Length;
        return this.difficulties[index];
    }

    private void SetCurrentDifficulty(MinigameDifficulty difficulty)
    {
        this.difficultySelectBronze.gameObject.SetActive(
            difficulty == MinigameDifficulty.Beginner
        );
        this.difficultySelectSilver.gameObject.SetActive(
            difficulty == MinigameDifficulty.Intermediate
        );
        this.difficultySelectGold.gameObject.SetActive(
            difficulty == MinigameDifficulty.Advanced ||
            difficulty == MinigameDifficulty.Expert
        );
        this.difficultyText.SetText(difficulty switch
        {
            MinigameDifficulty.Beginner => "BEGINNER",
            MinigameDifficulty.Intermediate => "INTERMEDIATE",
            MinigameDifficulty.Advanced => "ADVANCED",
            MinigameDifficulty.Expert => "EXPERT",
            _ => ""
        });
        this.trophy.sprite = TrophySprites()[(int)difficulty];
        this.currentDifficulty = difficulty;
    }

    private Sprite[] TrophySprites()
    {
        return this.minigameType switch
        {
            minigame.Minigame.Type.Golf         => spritesGolfTrophy,
            minigame.Minigame.Type.Pinball      => spritesPinballTrophy,
            minigame.Minigame.Type.Racing       => spritesRacingTrophy,
            minigame.Minigame.Type.Fishing      => spritesFishingTrophy,
            minigame.Minigame.Type.Rolling      => spritesRollingTrophy,
            minigame.Minigame.Type.Abseiling    => spritesAbseilingTrophy,
            _ => throw new NotImplementedException()
        };
    }

    private MinigameDifficulty[] DifficultiesPurchased()
    {
        var shopItemsAndDifficulties = this.minigameType switch
        {
            minigame.Minigame.Type.Golf =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigameGolfBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigameGolfIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigameGolfAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigameGolfExpert
                },
            minigame.Minigame.Type.Racing =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigameRacingBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigameRacingIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigameRacingAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigameRacingExpert
                },
            minigame.Minigame.Type.Pinball =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigamePinballBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigamePinballIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigamePinballAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigamePinballExpert
                },
            minigame.Minigame.Type.Fishing =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigameFishingBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigameFishingIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigameFishingAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigameFishingExpert
                },
            minigame.Minigame.Type.Rolling =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigameRollingBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigameRollingIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigameRollingAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigameRollingExpert
                },
            minigame.Minigame.Type.Abseiling =>
                new Dictionary<MinigameDifficulty, SaveData.ShopItem>
                {
                    [MinigameDifficulty.Beginner]       = SaveData.ShopItem.MinigameAbseilingBeginner,
                    [MinigameDifficulty.Intermediate]   = SaveData.ShopItem.MinigameAbseilingIntermediate,
                    [MinigameDifficulty.Advanced]       = SaveData.ShopItem.MinigameAbseilingAdvanced,
                    [MinigameDifficulty.Expert]         = SaveData.ShopItem.MinigameAbseilingExpert
                },
            _ => null
        };

        var permitted =
            shopItemsAndDifficulties
                .Where(kv => SaveData.IsItemPurchased(kv.Value))
                .Select(kv => kv.Key)
                .ToArray();

        if(permitted.Length == 0)
        {
            permitted = new MinigameDifficulty[] {
                MinigameDifficulty.Beginner,
                MinigameDifficulty.Intermediate,
                MinigameDifficulty.Advanced,
                MinigameDifficulty.Expert
            };
        }
        return permitted;
    }

}
