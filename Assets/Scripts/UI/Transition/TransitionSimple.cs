using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Linq;

public class TransitionSimple : Transition
{
    private static string[] SmallerScenes = new []
    {
        "Title", "Main Menu", "Calendar", "Shop",
        "Character Select", "Trophy Room", "Settings", "Arcade"
    };

    private string targetSceneName;

    public void Init(string targetSceneName)
    {
        this.targetSceneName = targetSceneName;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(OpenAnimation());
    }

    private IEnumerator OpenAnimation()
    {
        var tweener = new Tweener();

        CanvasScalerAndCircleInit();

        var tweenForCircle = tweener.TweenFloat(1, 0f, 0.5F, Easing.QuadEaseIn);

        AsyncOperation loadAsyncOperation = null;
        #if UNITY_SWITCH
        if (SmallerScenes.Contains(SceneManager.GetActiveScene().name) &&
            SmallerScenes.Contains(targetSceneName))
        {
            /*
                in cases where the departing and arriving scenes are both reasonably
                small, we can safely load one before unloading another without
                worrying about running out of memory.
                so, if this is safe, then starting the load earlier
                speeds things up a bit.
            */
            loadAsyncOperation = SceneManager.LoadSceneAsync(targetSceneName);
            loadAsyncOperation.allowSceneActivation = false;
        }
        #endif

        yield return null;
        yield return null;

        while(!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        yield return null;
        yield return null;

        var timeBefore = Time.realtimeSinceStartup;

        if (loadAsyncOperation != null)
        {
            loadAsyncOperation.allowSceneActivation = true;
            while (loadAsyncOperation.isDone == false)
            {
                yield return null;
            }
        }
        else
        {
            yield return Game.UnloadCurrentSceneThenLoadSceneAsync(targetSceneName);
        }

        for (int n = 0; n < 10; n += 1)
            yield return null;
        while (Time.realtimeSinceStartup < timeBefore + 0.6f)
            yield return null;

        tweener = new Tweener();
        tweenForCircle = tweener.TweenFloat(0, 1f, 0.5F, Easing.QuadEaseIn, 0.3f);

        while(!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        yield return null;
        yield return null;

        instance = null;
        Destroy(gameObject);
    }
}
