﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TransitionThemed : Transition
{
    public ThemesAsset themesAsset;
    public RectMask2D pivotMask;
    public Image imageCalendarLogo;
    public LocalizeText textWeekday;
    public LocalizeText textDate;
    public LocalizeText textYear;
    public Action actionWhenSwitchedToGameScene;

    public void Init()
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(OpenAnimation());
    }

    private IEnumerator OpenAnimation()
    {
        var themeDate = Game.selectedDate;
        var textDate = Game.selectedDate;

        if (Game.IsOpeningTutorialSelected == true)
            textDate = DateTime.Now.Date;

        Tweener tweener = new Tweener();

        CanvasScalerAndCircleInit();

        var pivotMask = this.transform.Find("pivotMask");

        Transform container = pivotMask.Find("layers");
        var circleImage = transformCircle.GetComponent<Image>();
        var themeForDate = LevelGeneration.ThemeForDate(themeDate);
        circleImage.color = ThemesAsset.CircleColor(themeForDate);

        container ??= pivotMask;
        container.gameObject.SetActive(true);
        var pivot = pivotMask.Find("pivot");
        var backgroundArc = (new[] {
            container.Find("layer6"),
            container.Find("layer5"),
            container.Find("layer4"),
            container.Find("layer3"),
            container.Find("layer2"),
            container.Find("layer1"),
            container.Find("layer0")
        }).Where(t => t != null).ToArray();
        var logoArc = (new[] {
            container.Find("logo1"),
            container.Find("logo0"),
            pivotMask.Find("topText"),
            pivotMask.Find("bottomText")
        }).Where(t => t != null).ToArray();
        var textThemeBottom =
            pivotMask.Find("bottomText").GetComponent<LocalizeText>();
        var textThemeTop =
            pivotMask.Find("topText").GetComponent<LocalizeText>();

        // day info
        DayInfoInit(textDate, this.textWeekday, this.textDate, this.textYear);
        var dayInfoGraphics = new Graphic[] { this.imageCalendarLogo, this.textWeekday.GetComponent<TMP_Text>(), this.textDate.GetComponent<TMP_Text>(), this.textYear.GetComponent<TMP_Text>() };

        // title
        var theme = this.themesAsset.ThemeForDate(themeDate);
        var lines = Localization.GetText(theme.richText.ToUpper()).Split('\n');
        textThemeTop.SetTextRaw(lines[0]);
        textThemeBottom.SetTextRaw(lines[1]);
        textThemeTop.SetAlpha(0);
        textThemeBottom.SetAlpha(0);

        // tweening
        var spokes = new List<Transform>();

        var tweenForCircle = tweener.TweenFloat(1, 0f, 0.5F, Easing.QuadEaseIn);
        int j = 0;
        for(int i = 0; i < backgroundArc.Length; i++, j++)
        {
            AddSpokeToPivot(backgroundArc[i], spokes, pivot);
            var invI = (backgroundArc.Length - 1) - i;
            tweener.RotateLocal(spokes[j], 0, 0.6f - 0.033f * i, Easing.QuadEaseOut, 0.5f - invI * 0.033f);
        }
        for(int i = 0; i < logoArc.Length; i++, j++)
        {
            AddSpokeToPivot(logoArc[i], spokes, pivot);
            var invI = (logoArc.Length - 1) - i;
            tweener.RotateLocal(spokes[j], 0, (0.6f + invI * 0.04f) - 0.033f * invI, Easing.QuadEaseOut, 0.25f + invI * 0.04f);
        }
        for(int i = 0; i < dayInfoGraphics.Length; i++)
        {
            dayInfoGraphics[i].color = new Color(1f, 1f, 1f, 0);
            tweener.Alpha(dayInfoGraphics[i], 1, 0.3f, Easing.QuadEaseOut, 0.5f + 0.0714f * i);
            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                if(dayInfoGraphics[i] is TMP_Text) dayInfoGraphics[i].GetComponent<LocalizeText>().SetAlpha(dayInfoGraphics[i].GetComponent<TMP_Text>().alpha);
        }

        yield return null;

        textThemeTop.SetAlpha(1);
        textThemeBottom.SetAlpha(1);
        textThemeTop.GetComponent<CircleText>().ForceUpdate();
        textThemeTop.GetComponent<CircleText>().ForceUpdate();

        yield return null;

        while(!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();

            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                foreach(var g in dayInfoGraphics)
                    if(g is TMP_Text) g.GetComponent<LocalizeText>().SetAlpha(g.GetComponent<TMP_Text>().alpha);

            yield return null;
        }

        yield return new WaitForSeconds(0.6f);

        yield return null;
        yield return null;

        yield return Game.UnloadCurrentSceneThenLoadSceneAsync("Game");

        while (Game.instance?.isFullyLoaded != true)
            yield return null;

        yield return null;
        yield return null;

        this.actionWhenSwitchedToGameScene?.Invoke();

        tweener = new Tweener();

        this.pivotMask.enabled = false;

        tweenForCircle = tweener.TweenFloat(0, 1f, 0.5F, Easing.QuadEaseIn, 0.3f);

        for(int i = 0; i < dayInfoGraphics.Length; i++)
            tweener.Alpha(dayInfoGraphics[i], 0f, 0.3f, Easing.QuadEaseIn, 0.0714f * i);

        j = 0;
        for(int i = 0; i < backgroundArc.Length; i++, j++)
        {
            var invI = (backgroundArc.Length - 1) - i;
            var duration = 0.6f - 0.033f * i;
            var delay = 0.5f - invI * 0.033f;
            tweener.RotateLocal(spokes[j], -180, duration, Easing.QuadEaseIn, delay);
            tweener.ScaleLocal(spokes[j], Vector3.one * 12f, duration, Easing.QuadEaseIn, delay);
        }
        for(int i = 0; i < logoArc.Length; i++, j++)
        {
            var invI = (logoArc.Length - 1) - i;
            var duration = (0.6f + invI * 0.04f) - 0.033f * invI;
            var delay = 0.25f + invI * 0.04f;
            tweener.RotateLocal(spokes[j], -180, duration, Easing.QuadEaseIn, delay);
            tweener.ScaleLocal(spokes[j], Vector3.one * 12f, duration, Easing.QuadEaseIn, delay);
        }

        while(!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();

            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                foreach(var g in dayInfoGraphics)
                    if(g is TMP_Text) g.GetComponent<LocalizeText>().SetAlpha(g.GetComponent<TMP_Text>().alpha);

            yield return null;
        }

        yield return null;
        yield return null;

        instance = null;
        Destroy(gameObject);
    }

}
