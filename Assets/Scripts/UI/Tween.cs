﻿using System.Collections.Generic;
using UnityEngine;

// Tween for Vector3 or float value
// linear, quadratic, or easing delegate
public class Tween
{

    public Vector3 start, finish;
    public float count, delay;
    public Ease ease;// optional delegate instead of quadratic / linear
    public float quadStrength;// amount of quadratic easing (-1.0 to 1.0, same as Flash)
    // (Current time in seconds, Starting value, Change in value, Duration of animation)
    public delegate double Ease(double t, double b, double c, double d);


    public Tween(float start, float finish, float delay, Ease ease, float pause = 0)
    {
        this.start = new Vector3(start, 0);
        this.finish = new Vector3(finish, 0);
        this.delay = delay;
        this.ease = ease;
        this.count = -pause;
    }

    public Tween(float start, float finish, float delay, float quadPerc = 0, float pause = 0)
    {
        this.start = new Vector3(start, 0);
        this.finish = new Vector3(finish, 0);
        this.delay = delay;
        this.quadStrength = quadPerc;
        this.count = -pause;
    }

    public Tween(Vector3 start, Vector3 finish, float delay, Ease ease, float pause = 0)
    {
        this.start = start;
        this.finish = finish;
        this.delay = delay;
        this.ease = ease;
        this.count = -pause;
    }

    public Tween(Vector3 start, Vector3 finish, float delay, float quadPerc = 0, float pause = 0)
    {
        this.start = start;
        this.finish = finish;
        this.delay = delay;
        this.quadStrength = quadPerc;
        this.count = -pause;
    }

    public Tween GetReversed(bool flipQuadPerc = true)
    {
        if(ease == null)
            return new Tween(finish, start, delay, flipQuadPerc ? -quadStrength : quadStrength);
        else
            return new Tween(finish, start, delay, ease);
    }

    public void Advance(float t)
    {
        this.count += t;
    }

    public void Advance(float t, out Vector3 position)
    {
        this.count += t;
        position = Position();
    }

    public void Advance(float t, out float value)
    {
        this.count += t;
        value = Value();
    }

    public void SetCountNormalized(float n)
    {
        this.count = n * this.delay;
    }

    public Vector3 Position()
    {
        if(this.count <= 0) return this.start;
        if(this.count >= delay) return this.finish;
        var v = this.start;
        if(ease != null)
        {
            if(this.start.x != this.finish.x)
                v.x = (float)ease(this.count, this.start.x, this.finish.x - this.start.x, this.delay);
            if(this.start.y != this.finish.y)
                v.y = (float)ease(this.count, this.start.y, this.finish.y - this.start.y, this.delay);
            if(this.start.z != this.finish.z)
                v.z = (float)ease(this.count, this.start.z, this.finish.z - this.start.z, this.delay);
        }
        else
        {
            float t = this.count / this.delay;
            if(this.quadStrength == 0)
            {
                if(this.start.x != this.finish.x)
                    v.x = (finish.x - start.x) * t + this.start.x;
                if(this.start.y != this.finish.y)
                    v.y = (finish.y - this.start.y) * t + this.start.y;
                if(this.start.x != this.finish.x)
                    v.z = (this.finish.z - this.start.z) * t + this.start.z;
            }
            else
            {
                var sq = t * t;
                var e = t + (sq - t) * -this.quadStrength;
                if(this.start.x != this.finish.x)
                    v.x = (this.finish.x - this.start.x) * e + this.start.x;
                if(start.y != finish.y)
                    v.y = (this.finish.y - this.start.y) * e + this.start.y;
                if(start.z != finish.z)
                    v.x = (this.finish.z - this.start.z) * e + this.start.z;
            }
        }
        return v;
    }

    public float Value()
    {
        if(this.count <= 0) return this.start.x;
        if(this.count >= this.delay) return this.finish.x;
        if(this.ease != null)
        {
            if(this.start.x != this.finish.x)
                return (float)this.ease(count, this.start.x, this.finish.x - this.start.x, this.delay);
        }
        else
        {
            float cn = this.count / this.delay;
            if(this.quadStrength == 0)
            {
                if(this.start.x != this.finish.x)
                    return (this.finish.x - this.start.x) * cn + this.start.x;
            }
            else
            {
                if(this.start.x != this.finish.x)
                {
                    var sq = cn * cn;
                    var e = cn + (sq - cn) * -this.quadStrength;
                    return (this.finish.x - this.start.x) * e + this.start.x;
                }
            }
        }
        return this.start.x;
    }

    public bool Done() { return this.count >= this.delay; }

}
