using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Tweener
{
    public enum Type
    {
        Position,
        LocalPosition,
        LocalRotationZ,
        LocalScale,
        AnchoredPosition,
        GraphicAlpha,
        Value
    }

    public class Tween
    {
        public Vector3? start;
        public Vector3 finish;
        public Vector3 value;
        public float duration;
        public global::Tween.Ease easing;
        public Transform target;
        public RectTransform targetRectTransform;
        public UnityEngine.UI.Graphic targetImage;
        public Type type;
        public float delayBeforeStart;

        public Vector3 Vector() => value;
        public float Value() => value.x;
    }
    public class TweenCallback
    {
        public float time;
        public Action action;
    }
    private List<Tween> tweens;
    private List<TweenCallback> callbacks;
    private float time;

    public Tweener()
    {
        this.tweens = new List<Tween>();
        this.callbacks = new List<TweenCallback>();
        this.time = 0;
    }

    public void Clear()
    {
        this.tweens = new List<Tween>();
        this.callbacks = new List<TweenCallback>();
        this.time = 0;
    }

    public Tween Move(
        Transform transform,
        Vector3 toPosition,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = transform,
            type = Type.Position,
            start = null,
            finish = toPosition,
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween MoveLocal(
        Transform transform,
        Vector3 toLocalPosition,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = transform,
            type = Type.LocalPosition,
            start = null,
            finish = toLocalPosition,
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween MoveAnchoredPosition(
        RectTransform transform,
        Vector3 toAnchoredPosition,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = transform,
            targetRectTransform = transform,
            type = Type.AnchoredPosition,
            start = null,
            finish = toAnchoredPosition,
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween RotateLocal(
        Transform transform,
        float toAngleDegrees,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = transform,
            type = Type.LocalRotationZ,
            start = null,
            finish = new Vector3(toAngleDegrees, 0),
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween ScaleLocal(
        Transform transform,
        Vector3 toScale,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = transform,
            type = Type.LocalScale,
            start = null,
            finish = toScale,
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween Alpha(
        UnityEngine.UI.Graphic image,
        float toAlpha,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            targetImage = image,
            type = Type.GraphicAlpha,
            start = null,
            finish = new Vector3(toAlpha, 0, 0),
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween TweenFloat(
        float fromValue,
        float toValue,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = null,
            type = Type.Value,
            start = new Vector3(fromValue, 0, 0),
            finish = new Vector3(toValue, 0, 0),
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public Tween TweenVector(
        Vector3 fromValue,
        Vector3 toValue,
        float duration,
        global::Tween.Ease easing = null,
        float delayBeforeStart = 0
    )
    {
        var tween = new Tween
        {
            target = null,
            type = Type.Value,
            start = fromValue,
            finish = toValue,
            duration = duration,
            easing = easing ?? Easing.Linear,
            delayBeforeStart = delayBeforeStart
        };
        this.tweens.Add(tween);
        return tween;
    }

    public void Callback(float time, Action action)
    {
        this.callbacks.Add(new TweenCallback
        {
            time = time,
            action = action
        });
    }

    public void Advance(float time)
    {
        this.time += time;
        foreach (var t in this.tweens)
        {
            if (this.time < t.delayBeforeStart) continue;

            t.start ??= t.type switch
            {
                Type.Position         => t.target.position,
                Type.LocalPosition    => t.target.localPosition,
                Type.LocalRotationZ   => new Vector3(t.target.localEulerAngles.z, 0, 0),
                Type.LocalScale       => t.target.localScale,
                Type.AnchoredPosition => t.targetRectTransform.anchoredPosition,
                Type.GraphicAlpha     => new Vector3(t.targetImage.color.a, 0, 0),
                _                     => Vector3.zero
            };

            float s = this.time - t.delayBeforeStart;
            if (s > t.duration) s = t.duration;

            t.value.x = (float)t.easing(
                s, t.start.Value.x, t.finish.x - t.start.Value.x, t.duration
            );
            t.value.y = (float)t.easing(
                s, t.start.Value.y, t.finish.y - t.start.Value.y, t.duration
            );
            t.value.z = (float)t.easing(
                s, t.start.Value.z, t.finish.z - t.start.Value.z, t.duration
            );

            switch (t.type)
            {
                case Type.Position:
                    if (t.target != null)
                        t.target.position = t.value;
                    break;
                case Type.LocalPosition:
                    if (t.target != null)
                        t.target.localPosition = t.value;
                    break;
                case Type.LocalRotationZ:
                    if (t.target != null)
                        t.target.localRotation = Quaternion.Euler(0, 0, t.value.x);
                    break;
                case Type.LocalScale:
                    if (t.target != null)
                        t.target.localScale = t.value;
                    break;
                case Type.AnchoredPosition:
                    if (t.targetRectTransform != null)
                        t.targetRectTransform.anchoredPosition = t.value;
                    break;
                case Type.GraphicAlpha:
                    if (t.targetImage != null)
                    {
                        t.targetImage.color = new Color(
                            t.targetImage.color.r,
                            t.targetImage.color.g,
                            t.targetImage.color.b,
                            t.value.x
                        );
                    }
                    break;
            }
        }
        foreach (var c in this.callbacks.ToArray())
        {
            if (this.time >= c.time)
            {
                c.action();
                this.callbacks.Remove(c);
            }
        }
    }

    public bool IsDone()
    {
        foreach (var tween in this.tweens)
        {
            if (this.time < tween.delayBeforeStart + tween.duration)
                return false;
        }
        return true;
    }

    public IEnumerator Run()
    {
        while (IsDone() == false)
        {
            Advance(1.0f / 60);
            yield return null;
        }
    }
}
