﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class UIFruit : MonoBehaviour
{
    public GameObject uiFruitPrefab;
    public Sprite[] fruitSprites;
    public AnimationCurve speedCurve;

    public void MoveFruit(
        Map map, int fruitNumber, Transform targetTransform, TextMeshPro fruitCount
    )
    {
        StartCoroutine(_MoveFruit(map, fruitNumber, targetTransform, fruitCount));
        StartCoroutine(FruitCountDown(fruitNumber, fruitCount));
    }

    private IEnumerator _MoveFruit(
        Map map, int fruitNumber, Transform targetTransform, TextMeshPro fruitCount
    )
    {
        RectTransform[] fruitUI = new RectTransform[fruitNumber];
        Vector3[] randomOffset = new Vector3[fruitNumber];

        int currentChildNumber = this.transform.childCount;

        for (int i = 0; i < fruitNumber; i++)
        {
            var fruitCounterImage = IngameUI.instance.FruitCounterImageRectTransform(map);
            var fruitCounterCenter = fruitCounterImage.transform.position;

            fruitUI[i] = Instantiate(this.uiFruitPrefab).GetComponent<RectTransform>();
            fruitUI[i].GetComponent<Image>().sprite = fruitSprites[i % fruitSprites.Length];
            fruitUI[i].transform.SetParent(GameObject.Find("Fruits").transform, false);
            fruitUI[i].transform.SetSiblingIndex(currentChildNumber);
            fruitUI[i].transform.position = fruitCounterCenter;
            fruitUI[i].sizeDelta =
                fruitCounterImage.sizeDelta * IngameUI.instance.FruitCounterScale(map);

            randomOffset[i] = Random.insideUnitCircle * .5f;
        }

        bool moveFruits = true;

        float timer = 0f;
        float delay = 0.10f;

        float minMoveSpeed = 20f * Time.deltaTime;
        float maxMoveSpeed = 50f * Time.deltaTime;
        float reachMaxSpeedTime = 0.50f;

        float? startDistance = null;
        while (moveFruits == true)
        {
            timer += Time.deltaTime;            
            moveFruits = false;
            
            for (int i = 0; i < fruitUI.Length; i++)
            {
                if(timer < delay * i) continue;

                float t = Mathf.InverseLerp(0f, reachMaxSpeedTime, timer);
                float currentMoveSpeed =
                    Mathf.Lerp(minMoveSpeed, maxMoveSpeed, this.speedCurve.Evaluate(t));

                Vector2 currentPosition = fruitUI[i].position;

                Vector2 targetWorldPosition = targetTransform.position + randomOffset[i];
                targetWorldPosition = IngameUI.instance.PositionForUICameraFromWorldPosition(
                    map, targetWorldPosition
                ); 
                Vector2 targetCanvasPosition = 
                    RectTransformUtility.WorldToScreenPoint(null, targetWorldPosition);

                Vector3 heading = targetCanvasPosition - currentPosition;
                float distance = heading.magnitude;
                Vector3 direction = heading / distance;

                if (startDistance == null)
                {
                    startDistance = distance;
                }

                if (distance > maxMoveSpeed)
                {
                    fruitUI[i].position += direction * currentMoveSpeed;
                    moveFruits = true;
                    if(startDistance / 2> distance)
                        fruitUI[i].localScale = Vector3.one * 2 * distance / startDistance.Value; 
                }
                else
                {
                    fruitUI[i].gameObject.SetActive(false);
                }

                if (distance > 3)
                {
                    GameObject.Find("Fruits").GetComponent<Canvas>().sortingLayerName = "UI";
                    GameObject.Find("Fruits").GetComponent<Canvas>().sortingOrder = -1;
                }
                else
                {
                    GameObject.Find("Fruits").GetComponent<Canvas>().sortingLayerName = "items (Front)";
                    GameObject.Find("Fruits").GetComponent<Canvas>().sortingOrder = -1;
                }
            }        
            yield return new WaitForEndOfFrame();
        }
    }
    public IEnumerator FruitCountDown(int fruitNumber, TextMeshPro fruitCount)
    {
        float timer = 0f;
        float fruitInt = fruitNumber;
        while (timer < 2.2f)
        {
            timer += Time.deltaTime;
            fruitInt = (2.2f - timer)/2.2f * 50;
            fruitCount.text = (Mathf.Ceil(fruitInt)).ToString();
            yield return null;
        }
        fruitCount.text = 0.ToString();
    }
}
