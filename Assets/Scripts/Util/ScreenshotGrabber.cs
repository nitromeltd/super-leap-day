using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System;

public static class ScreenshotGrabber 
{
    private static Camera _screenshotGrabberCamera;
    public static Camera ScreenshotGrabberCamera
    {
        get
        {
            if (_screenshotGrabberCamera == null)
            {
                _screenshotGrabberCamera = GameObject.Find("Screenshot Grabber Camera").GetComponent<Camera>();
            }
            return _screenshotGrabberCamera;
        }
    }

    public static bool grabbingScreenshot;

    public static void SetCamera()
    {
        GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = ScreenshotGrabberCamera;
    }

    public static void Capture4KScreenshots()
    {
        CaptureScreenshot(3360, 1890);
        DisablePlayerAndEnemies();
        CaptureScreenshot(3360, 1890, "_no_enemies");
        EnablePlayerAndEnemies();
    }

    private static void DisablePlayerAndEnemies()
    {
        foreach(var chunk in Map.instance.chunks)
        {
            if (chunk.gameObject.activeSelf)
            {
                foreach (var enemy in chunk.items.OfType<Enemy>().ToArray())
                {
                    enemy.gameObject.SetActive(false);
                }
            }
        }
        Map.instance.player.gameObject.SetActive(false);
    }

    private static void EnablePlayerAndEnemies()
    {
        foreach(var chunk in Map.instance.chunks)
        {
            if (chunk.gameObject.activeSelf)
            {
                foreach (var enemy in chunk.items.OfType<Enemy>().ToArray())
                {
                    enemy.gameObject.SetActive(true);
                }
            }
        }
        Map.instance.player.gameObject.SetActive(true);
    }

    private static void CaptureScreenshot(int width, int height, string postfix = "")
    {
        grabbingScreenshot = true;
        ScreenshotGrabberCamera.enabled = true;
        ScreenshotGrabberCamera.transform.position = Camera.main.transform.position;
		ScreenshotGrabberCamera.orthographicSize = Camera.main.orthographicSize;
		ScreenshotGrabberCamera.aspect = (float)width / (float)height;
		ScreenshotGrabberCamera.targetTexture = new RenderTexture(
            width,
            height,
            32,
            UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm
        );
        
        GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = ScreenshotGrabberCamera;

        var oldRenderTexture = RenderTexture.active;
		RenderTexture.active = ScreenshotGrabberCamera.targetTexture;

		ScreenshotGrabberCamera.Render();

		Texture2D screenShot = new Texture2D(
            RenderTexture.active.width,
            RenderTexture.active.height,
            TextureFormat.RGB24,
            false
        );
		screenShot.ReadPixels(
            new Rect(0, 0, RenderTexture.active.width, RenderTexture.active.height),
            0,
            0
        );
		screenShot.Apply ();

		RenderTexture.active = oldRenderTexture;
		ScreenshotGrabberCamera.enabled = false;
        
        //write to disk
        byte[] data = screenShot.EncodeToPNG();
        string filename =
            "screenshot " + System.DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + postfix + ".png";
        string fullPathname = filename;
        File.WriteAllBytes(fullPathname, data);
        GameObject.Find("Canvas").GetComponent<Canvas>().worldCamera = Camera.main;
        grabbingScreenshot = false;
    }
}
