using System;

public class Stopwatch
{
    private bool running;
    private float startedRunningAtTime;
    private float elapsedBeforeCurrentRun;

    public static Stopwatch Go() => new Stopwatch
    {
        running = true,
        startedRunningAtTime = UnityEngine.Time.realtimeSinceStartup,
        elapsedBeforeCurrentRun = 0
    };

    public void Start()
    {
        if (this.running == false)
        {
            this.running = true;
            this.startedRunningAtTime = UnityEngine.Time.realtimeSinceStartup;
        }
    }

    public void Stop()
    {
        if (this.running == true)
        {
            this.elapsedBeforeCurrentRun +=
                UnityEngine.Time.realtimeSinceStartup - this.startedRunningAtTime;
            this.startedRunningAtTime = 0;
            this.running = false;
        }
    }

    public void Reset()
    {
        this.running = false;
        this.startedRunningAtTime = 0;
        this.elapsedBeforeCurrentRun = 0;
    }

    public StopwatchRunner Auto()
    {
        return new StopwatchRunner(this);
    }

    public float Time()
    {
        var timeInCurrentRun =
            this.running ?
            (UnityEngine.Time.realtimeSinceStartup - this.startedRunningAtTime) :
            0;
        return this.elapsedBeforeCurrentRun + timeInCurrentRun;
    }

    public static Stopwatch operator +(Stopwatch s1, Stopwatch s2) => new Stopwatch
    {
        running = false,
        startedRunningAtTime = 0,
        elapsedBeforeCurrentRun = s1.Time() + s2.Time()
    };

    public override string ToString()
    {
        return
            "<b><color=white>" + (Time() * 1000).ToString("0.00") + "</color></b>ms";
    }

    public string MillisAsString()
    {
        return (Time() * 1000).ToString("0.00");
    }
}

public class StopwatchRunner : IDisposable
{
    private Stopwatch s;

    public StopwatchRunner(Stopwatch s)
    {
        this.s = s;
        this.s.Start();
    }

    public void Dispose()
    {
        this.s.Stop();
    }
}
