using System;
using System.Collections.Generic;

/* An array type which refuses to allocate memory on the heap
   unless we have at least four things.
   This is basically just for Raycast.CombinedResults.
   Even combined, you nearly never hit four different Tiles or Items. */

public struct TinyArray<T>
{
    private int length;
    private T value1;
    private T value2;
    private T value3;
    private T[] manyValues;

    public void SetValues(List<T> values)
    {
        this.length = values.Count;
        this.value1 = (values.Count >= 1 && values.Count <= 3) ? values[0] : default;
        this.value2 = (values.Count >= 2 && values.Count <= 3) ? values[1] : default;
        this.value3 = (values.Count == 3                     ) ? values[2] : default;
        this.manyValues = (values.Count > 3) ? values.ToArray() : null;
    }

    public readonly int Length => this.length;

    public readonly T this[int i]
    {
        get
        {
            if (i < 0 || i >= this.length)
            {
                throw new IndexOutOfRangeException(
                    $"Requested item {i} from a TinyArray<T> of length {0}."
                );
            }

            if (this.length <= 3)
            {
                if (i == 0) return this.value1;
                if (i == 1) return this.value2;
                else        return this.value3;
            }
            else
            {
                return this.manyValues[i];
            }
        }
    }

    /* This enumerator, being a struct, also doesn't go onto the heap. */

    public struct Enumerator
    {
        public TinyArray<T> tinyArray;
        public int index;

        public readonly T Current
        {
            get
            {
                if (tinyArray.length <= 3)
                {
                    if (this.index == 0) return tinyArray.value1;
                    if (this.index == 1) return tinyArray.value2;
                    if (this.index == 2) return tinyArray.value3;
                    return default;
                }
                else
                {
                    if (this.index < this.tinyArray.length)
                        return tinyArray.manyValues[this.index];
                    else
                        return default;
                }
            }
        }

        public bool MoveNext()
        {
            this.index += 1;
            return this.index < this.tinyArray.length;
        }
    }

    public readonly TinyArray<T>.Enumerator GetEnumerator() => new()
    {
        tinyArray = this,
        index = -1
    };

    public readonly T[] ToArray()
    {
        if (this.length == 0) return new T[0];
        if (this.length == 1) return new[] { this.value1 };
        if (this.length == 2) return new[] { this.value1, this.value2 };
        if (this.length == 3) return new[] { this.value1, this.value2, this.value3 };
        return this.manyValues;
    }

    /* A side effect is we can't use LINQ with these things.
       I'm just reimplementing the few function we actually need. */

    public readonly bool All(Func<T, bool> predicate)
    {
        foreach (var item in this)
        {
            if (predicate(item) == false) return false;
        }
        return true;
    }

    public readonly TinyArray<U> OfType<U>() where U : class
    {
        var count = 0;

        for (int n = 0; n < this.length; n += 1)
        {
            var item = this[n];
            if (item is U)
                count += 1;
        }

        if (count <= 3)
        {
            var result = new TinyArray<U> { length = count };

            for (int n = 0; n < this.length; n += 1)
            {
                var item = this[n];
                if (item is not U) continue;
                if (result.value1 == default)
                    result.value1 = item as U;
                else if (result.value2 == default)
                    result.value2 = item as U;
                else
                    result.value3 = item as U;
            }

            return result;
        }
        else
        {
            var result = new TinyArray<U>
            {
                length = count,
                manyValues = new U[count]
            };
            int resultN = 0;

            for (int n = 0; n < this.length; n += 1)
            {
                var item = this[n];
                if (item is not U) continue;

                result.manyValues[resultN] = item as U;
                resultN += 1;
            }

            return result;
        }
    }
}
