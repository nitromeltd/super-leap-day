
using UnityEngine;
using UnityEngine.U2D;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Reflection;

public static class Util
{
    public static float Tau = Mathf.PI * 2;

    public static Dictionary<string, Sprite> SpritesFromSpriteAtlas(SpriteAtlas atlas)
    {
        var sprites = new Sprite[atlas.spriteCount];
        atlas.GetSprites(sprites);

        var result = new Dictionary<string, Sprite>();

        foreach (var s in sprites)
            result[s.name.Replace("(Clone)", "")] = s;

        return result;
    }

    public static void Shuffle<T>(this System.Random rng, T[] array)
    {
        int n = array.Length;
        while (n > 1) 
        {
            int k = rng.Next(n --);
            T temp = array[n];
            array[n] = array[k];
            array[k] = temp;
        }
    }
    public static void Shuffle<T>(this System.Random rng, List<T> list)
    {
        int n = list.Count;
        while (n > 1) 
        {
            int k = rng.Next(n --);
            T temp = list[n];
            list[n] = list[k];
            list[k] = temp;
        }
    }

    public static T RandomChoice<T>(params T[] array)
    {
        Debug.Assert(array.Length > 0);
        return array[UnityEngine.Random.Range(0, array.Length)];
    }

    public static T RandomChoice<T>(List<T> list)
    {
        Debug.Assert(list.Count > 0);
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    public static float Slide(float from, float to, float by)
    {
        if (from < to)
        {
            from += by;
            if (from >= to) return to;
            return from;
        }
        else
        {
            from -= by;
            if (from <= to) return to;
            return from;
        }
    }

    public static float Sign(float value)
    {
        if (value < 0) return -1;
        if (value > 0) return 1;
        return 0;
    }

    public static int Modulo(int num, int div)
    {
        num %= div;
        return (num >= 0) ? num : num + div;
    }

    public static float Modulo(float num, float div)
    {
        num %= div;
        return (num >= 0) ? num : num + div;
    }

    public static Vector2 Add(this Vector2 vector, float x, float y)
    {
        vector.x += x;
        vector.y += y;
        return vector;
    }

    public static Rect Offset(this Rect rect, Vector2 offset) => new Rect(
        rect.xMin + offset.x,
        rect.yMin + offset.y,
        rect.width,
        rect.height
    );

    public static Rect Inflate(this Rect rect, float size) => new Rect(
        rect.xMin - size,
        rect.yMin - size,
        rect.width + (size * 2),
        rect.height + (size * 2)
    );

    public static Rect Inflate(this Rect rect, Vector2 size) => new Rect(
        rect.xMin - size.x,
        rect.yMin - size.y,
        rect.width + (size.x * 2),
        rect.height + (size.y * 2)
    );
    
    public static float MapRange(
        float value,
        float originalMin,
        float originalMax,
        float newMin,
        float newMax
    )
    {
        float s = (value - originalMin) / (originalMax - originalMin);
        if (float.IsNaN(s)) s = 0f;
        return Mathf.Lerp(newMin, newMax, s);
    }

    public static T MinBy<T, U>(
        this IEnumerable<T> items,
        Func<T, U> valueProvider,
        T resultForEmptyList = default(T)
    ) where U : IComparable
    {
        var enumerator = items.GetEnumerator();
        if (enumerator.MoveNext() == false)
            return resultForEmptyList;

        T bestItem  = enumerator.Current;
        U bestValue = valueProvider(bestItem);

        while (enumerator.MoveNext() == true)
        {
            T item  = enumerator.Current;
            U value = valueProvider(item);

            if (value.CompareTo(bestValue) < 0)
            {
                bestValue = value;
                bestItem  = item;
            }
        }

        return bestItem;
    }

    public static T MaxBy<T, U>(
        this IEnumerable<T> items,
        Func<T, U> valueProvider,
        T resultForEmptyList = default(T)
    ) where U : IComparable
    {
        var enumerator = items.GetEnumerator();
        if (enumerator.MoveNext() == false)
            return resultForEmptyList;

        T bestItem  = enumerator.Current;
        U bestValue = valueProvider(bestItem);

        while (enumerator.MoveNext() == true)
        {
            T item  = enumerator.Current;
            U value = valueProvider(item);

            if (value.CompareTo(bestValue) > 0)
            {
                bestValue = value;
                bestItem  = item;
            }
        }

        return bestItem;
    }

    public static NitromeEditor.Path MakeClosedPathByPingPonging(NitromeEditor.Path path)
    {
        if (path.closed == true)
            return path;

        var result = path.DeepCopy();
        var nodes = new List<NitromeEditor.Path.Node>(result.nodes);
        var edges = new List<NitromeEditor.Path.Edge>(result.edges);
        result.closed = true;

        for (int n = path.nodes.Length - 2; n > 0; n -= 1)
        {
            var oldNode = path.nodes[n];
            var newNode = new NitromeEditor.Path.Node(result, oldNode.x, oldNode.y);
            newNode.arcLength = oldNode.arcLength;
            newNode.delaySeconds = oldNode.delaySeconds;
            newNode.properties =
                new Dictionary<string, PropertyValue>(oldNode.properties);
            nodes.Add(newNode);
        }

        for (int n = path.edges.Length - 1; n >= 0; n -= 1)
        {
            var oldEdge                  = path.edges[n];
            var newEdge                  = new NitromeEditor.Path.Edge();
            newEdge.fromIndex            = edges.Count;
            newEdge.toIndex              = (edges.Count + 1) % nodes.Count;
            newEdge.from                 = nodes[newEdge.fromIndex];
            newEdge.to                   = nodes[newEdge.toIndex];
            newEdge.easing               = -oldEdge.easing;
            newEdge.speedPixelsPerSecond = oldEdge.speedPixelsPerSecond;
            edges.Add(newEdge);
        }

        result.nodes = nodes.ToArray();
        result.edges = edges.ToArray();
        result.Recalculate();
        return result;
    }

    public static float WrapAngleMinus180To180(float angleDegrees)
    {
        angleDegrees %= 360;
        while (angleDegrees <= -180) angleDegrees += 360;
        while (angleDegrees >  +180) angleDegrees -= 360;
        return angleDegrees;
    }

    public static float WrapAngle0To360(float angleDegrees)
    {
        angleDegrees %= 360;
        if (angleDegrees < 0) angleDegrees += 360;
        return angleDegrees;
    }

    public static float ClampAngleDegrees(float angle, float min, float max)
    {
        angle -= min;
        angle = Util.WrapAngle0To360(angle);

        var range = Util.WrapAngle0To360(max - min);
        if (angle > range)
        {
            var crossover = Mathf.Lerp(range, 360, 0.5f);
            if (angle < crossover)
                angle = range;
            else
                angle = 0;
        }

        angle += min;
        return angle;
    }

    public static Vector2? LineIntersection(
        Vector2 a1,
        Vector2 a2,
        Vector2 b1,
        Vector2 b2
    )
    {
        var d = (a2.x - a1.x) * (b2.y - b1.y) - (a2.y - a1.y) * (b2.x - b1.x);
        if (d == 0.0f) return null;

        var u = ((b1.x - a1.x) * (b2.y - b1.y) - (b1.y - a1.y) * (b2.x - b1.x)) / d;
        return new Vector2(
            a1.x + u * (a2.x - a1.x),
            a1.y + u * (a2.y - a1.y)
        );
    }

    public static Vector2? LineSegmentIntersection(
        Vector2 a1,
        Vector2 a2,
        Vector2 b1,
        Vector2 b2
    )
    {
        var d = (a2.x - a1.x) * (b2.y - b1.y) - (a2.y - a1.y) * (b2.x - b1.x);
        if (d == 0.0f) return null;

        var u = ((b1.x - a1.x) * (b2.y - b1.y) - (b1.y - a1.y) * (b2.x - b1.x)) / d;
        var v = ((b1.x - a1.x) * (a2.y - a1.y) - (b1.y - a1.y) * (a2.x - a1.x)) / d;
        if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f) return null;

        return new Vector2(
            a1.x + u * (a2.x - a1.x),
            a1.y + u * (a2.y - a1.y)
        );
    }

    public static (Vector2, Vector2)? LineCircleIntersection(
        Vector2 pt1, Vector2 pt2, Vector2 circleCenter, float circleRadius
    ) 
    {
        var lineVector = pt2 - pt1;

        float a = lineVector.x * lineVector.x + lineVector.y * lineVector.y;
        float b = 2 * (
            lineVector.x * (pt1.x - circleCenter.x) +
            lineVector.y * (pt1.y - circleCenter.y)
        );
        float c = circleCenter.x * circleCenter.x + circleCenter.y * circleCenter.y;
        c += pt1.x * pt1.x + pt1.y * pt1.y;
        c -= 2 * (circleCenter.x * pt1.x + circleCenter.y * pt1.y);
        c -= circleRadius * circleRadius;
        float bb4ac = b * b - 4 * a * c; 

        if (Mathf.Abs(a) < float.Epsilon || bb4ac < 0) 
            return null;

        float mu1 = (-b + Mathf.Sqrt(bb4ac)) / (2 * a);
        float mu2 = (-b - Mathf.Sqrt(bb4ac)) / (2 * a);
        return (pt1 + (mu1 * lineVector), pt1 + (mu2 * lineVector));
    }

    public static string DescriptionOfFakeNull(UnityEngine.Object obj)
    {
        if (!(obj is UnityEngine.Object))
            return "???";

        var field = typeof(UnityEngine.Object).GetField(
            "m_UnityRuntimeErrorString", BindingFlags.NonPublic | BindingFlags.Instance
        );
        var runtimeErrorString = (string)field.GetValue(obj);
        var regex = new Regex("The variable ([A-Za-z0-9_]+) of ([A-Za-z0-9_]+)");
        var match = regex.Match(runtimeErrorString);
        if (match.Success == true)
            return match.Groups[2] + "." + match.Groups[1];
        else
            return "???";
    }

    public static string FullPath(this Transform transform)
    {
        if (transform != null && transform.parent != null)
            return FullPath(transform.parent) + "/" + transform.gameObject.name;
        else if (transform != null)
            return transform.gameObject.name;
        else
            return "[null]";
    }

    public static string FullPath(this GameObject gameObject)
    {
        if (gameObject != null)
            return FullPath(gameObject.transform);
        else
            return "[null]";
    }

    public static void SetLayerRecursively(
        Transform root, int layer, bool includeInactive
    )
    {
        root.gameObject.layer = layer;

        foreach (var child in root.GetComponentsInChildren<Transform>(includeInactive))
        {
            child.gameObject.layer = layer;
        }
    }

    public static float AdjustmentFactorForCompressedArcs(
        NitromeEditor.Path path, float distance
    )
    {
        var pt = path.PointAtDistanceAlongPath(distance);

        var edgeRealLength = pt.edge.distance;
        if (pt.edge.from.arc != null)
        {
            edgeRealLength += (
                pt.edge.from.arc.distanceAround - pt.edge.from.arc.distanceWithoutArc
            ) / 2;
        }
        if (pt.edge.to.arc != null)
        {
            edgeRealLength += (
                pt.edge.to.arc.distanceAround - pt.edge.to.arc.distanceWithoutArc
            ) / 2;
        }
        if (edgeRealLength < 0.01f)
            edgeRealLength = 0.01f;
        if (edgeRealLength > pt.edge.distance)
            edgeRealLength = pt.edge.distance;

        return pt.edge.distance / edgeRealLength;
    }

    public static string Hex(byte[] bytes)
    {
        var sb = new System.Text.StringBuilder();
        for (int row = 0; row < bytes.Length; row += 16)
        {
            for (int n = row; n < row + 16 && n < bytes.Length; n += 1)
            {
                sb.Append(bytes[n].ToString("x2"));
                if (n % 16 < 15)
                    sb.Append(' ');
            }

            sb.Append("    ");

            for (int n = row; n < row + 16 && n < bytes.Length; n += 1)
            {
                if (bytes[n] < 32)
                    sb.Append('.');
                else
                    sb.Append((char)bytes[n]);

                if (n % 16 < 15)
                    sb.Append(' ');
            }

            sb.Append('\n');
        }
        return sb.ToString();
    }

    public static bool CoroutineMoveNext(IEnumerator coroutine)
    {
        if (coroutine.Current is IEnumerator inner)
        {
            if (CoroutineMoveNext(inner))
                return true;
        }
        return coroutine.MoveNext();
    }

    public static bool CoroutineRunForTime(IEnumerator coroutine, float timeSeconds)
    {
        var startTime = Time.realtimeSinceStartup;

        while (Time.realtimeSinceStartup < startTime + timeSeconds)
        {
            if (CoroutineMoveNext(coroutine) == false)
                return false;
        }

        return true;
    }

    public static void CoroutineFinish(IEnumerator coroutine)
    {
        while (CoroutineMoveNext(coroutine) == true) { /* tight loop */ }
    }
}
