using System.Linq;
using UnityEngine;

// in landscape mode, we can toggle a black transparent border to show
// which areas of the screen would be visible in portrait mode,
// to assist level editors in not accidentally making blind jumps/falls

public class VisibleAreaDebugGuide : MonoBehaviour
{
    private Map map;

    private float zoomOutAmount;
    private float zoomOutBiasLeftRight;

    public void Init(Map map)
    {
        this.map = map;

        if (Game.instance.maps.Length > 1) return;
    }

    public void Advance()
    {
        if (GameCamera.IsLandscape() == false)
        {
            this.gameObject.SetActive(false);
            return;
        }
        if (this.gameObject.activeSelf == false)
            this.gameObject.SetActive(true);

        var chunk = this.map.NearestChunkTo(this.map.player.position);
        if (chunk.isTransitionalTunnel == true)
        {
            var prevChunk = this.map.chunks[chunk.index - 1];
            var nextChunk = this.map.chunks[chunk.index + 1];

            var (leftChunk, rightChunk) =
                chunk.isFlippedHorizontally == true ?
                (nextChunk, prevChunk) :
                (prevChunk, nextChunk);

            chunk = (this.map.player.velocity.x > 0) ? rightChunk : leftChunk;
        }

        var pos = this.map.player.position;
        pos.x += 4 * this.zoomOutBiasLeftRight * this.zoomOutAmount;

        if (chunk.isHorizontal == true)
        {
            var (left, right) = this.map.gameCamera.ChunksInHorizontalSection(chunk);
            var halfwidth = Mathf.Lerp(8, 12, this.zoomOutAmount);
            var xMin = Mathf.Min(left.xMin, right.xMin) + halfwidth;
            var xMax = Mathf.Max(left.xMax + 1, right.xMax + 1) - halfwidth;
            pos.x = GameCamera.ClampOrCenter(pos.x, xMin, xMax);
        }
        else
        {
            pos.x = (chunk.xMin + chunk.xMax + 1) / 2;
        }

        if (GameCamera.ShouldZoomOutForChunk(chunk) == true)
        {
            this.zoomOutAmount = Util.Slide(this.zoomOutAmount, 1, 0.1f);
            this.zoomOutBiasLeftRight = Util.Slide(
                this.zoomOutBiasLeftRight,
                Mathf.Clamp(this.map.player.velocity.x * 10, -1f, 1f),
                0.05f
            );
        }
        else
        {
            this.zoomOutAmount = Util.Slide(this.zoomOutAmount, 0, 0.1f);
        }
        float scale = Mathf.Lerp(1.0f, 1.5f, this.zoomOutAmount);

        this.transform.position = new Vector3(pos.x, pos.y, 0);
        this.transform.localScale = new Vector3(scale, scale, 1);
    }
}
