using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Waterline
{
    private Map map;
    public float currentLevel;
    public float targetLevel;

    [HideInInspector] public Transform waterTransform;
    [HideInInspector] public GameObject swimmingSplashEffect;
    private float currentWaterRiseSpeed;
    private LoopPath loopPath;
    public Heightmap heightmap;
    private MeshRenderer waterMeshRenderer;
    private MeshFilter waterMeshFilter;
    private Mesh waterMesh;
    private Chunk lastFramePlayerChunk;

    public const float DefaultRiseSpeed = 0.15f;
    public const float LevelThreshold = 0.05f;

    public int transparentCount = 0;

    public bool isElectricityOn = false;
    
    public bool IsMoving => this.targetLevel != this.currentLevel;
    public float VelocityThreshold => Mathf.Abs(Velocity().y) + LevelThreshold;  
    private bool IsLoop => this.loopPath != null && this.loopPath?.loopPoints != null;     

    public Vector2 Velocity()
    {
        if(IsMoving == true)
        {
            return this.currentWaterRiseSpeed *
                (this.currentLevel > this.targetLevel ? Vector2.down : Vector2.up); 
        }

        return Vector2.zero;
    }

    public class LoopPath
    {
        public LoopPoint[] loopPoints;
        public int currentIndex;
        public float delayTimer;
        public bool controlByExternalItem;
        public bool closedLoop;
        private bool movingForward;

        public LoopPoint CurrentTargetPoint => this.loopPoints[currentIndex];
        public bool HasReachTarget(float currentLevel) =>
            CurrentTargetPoint.targetLevel == currentLevel;

        public LoopPath(LoopPoint[] loopPoints, bool controlByExternalItem, bool closedLoop)
        {
            this.loopPoints = loopPoints;
            this.currentIndex = 0;
            this.delayTimer = 0;
            this.controlByExternalItem = controlByExternalItem;
            this.closedLoop = closedLoop;
            this.movingForward = true;
        }

        public void GoToNextPoint()
        {
            this.currentIndex = NextIndex();
            this.delayTimer = 0;
        }

        private int NextIndex()
        {
            if(this.closedLoop == false)
            {
                if(this.movingForward == true)
                {
                    if(this.currentIndex + 1 >= this.loopPoints.Length)
                    {
                        this.movingForward = false;
                        return this.currentIndex - 1;
                    }
                    else
                    {
                        return this.currentIndex + 1;
                    }
                } 
                else
                {
                    if(this.currentIndex - 1 < 0)
                    {
                        this.movingForward = true;
                        return this.currentIndex + 1;
                    }
                    else
                    {
                        return this.currentIndex - 1;
                    }
                }
            }

            return (this.currentIndex + 1 >= this.loopPoints.Length) ?
                0 : this.currentIndex + 1;
        }

        public bool WaitForDelay()
        {
            return this.delayTimer < CurrentTargetPoint.delayTime;
        }
    }

    public struct LoopPoint
    {
        public float targetLevel;
        public float riseSpeed;
        public float delayTime;

        public LoopPoint(float targetLevel, float riseSpeed, float delayTime)
        {
            this.targetLevel = targetLevel;
            this.riseSpeed = riseSpeed;
            this.delayTime = delayTime;
        }
    }

    public Waterline(Map map)
    {
        this.map = map;

        Chunk playerCheckpointChunk =
            this.map.NearestChunkTo(map.player.checkpointPosition);
        this.currentLevel = this.targetLevel = playerCheckpointChunk.yMin;
        
        this.currentWaterRiseSpeed = DefaultRiseSpeed;
        this.loopPath = null;

        this.waterTransform = GameObject.Instantiate(Assets.instance.sunkenIsland.water).transform;
        this.waterTransform.parent = map.transform;
        this.swimmingSplashEffect = GameObject.Instantiate(Assets.instance.sunkenIsland.swimmingSplashEffect);
        this.swimmingSplashEffect.GetComponent<PlayerSwimmingSplashEffect>().Init(map);

        this.waterMeshRenderer = this.waterTransform.GetComponent<MeshRenderer>();
        this.waterMeshFilter = this.waterTransform.GetComponent<MeshFilter>();
        this.waterMesh = new Mesh();

        this.heightmap = new Heightmap(this);
        this.heightmap.SetBoundaries(-1, 17);
        this.heightmap.Advance();
        this.heightmap.DrawTo(this.waterMeshFilter, this.waterMesh);
    }

    public void ChangeLevel(float newTargetLevel, float riseSpeed)
    {
        if(this.loopPath == null && this.targetLevel == newTargetLevel) return;

        this.targetLevel = newTargetLevel;
        this.currentWaterRiseSpeed = riseSpeed;
        this.loopPath = null;

        CheckIfCurrentLevelIsTooFar();
    }

    public void LoopLevel(
        List<WaterMarker> loopMarkers,
        bool controlByExternalItem = false,
        bool closedLoop = true,
        bool forceFirstNode = false)
    {
        LoopPoint[] loopPoints = new LoopPoint[loopMarkers.Count];
        
        for (int i = 0; i < loopMarkers.Count; i++)
        {
            loopPoints[i] = loopMarkers[i].GenerateLoopPoint();
        }

        if(this.loopPath != null &&
            IsSameLoop(this.loopPath.loopPoints, loopPoints) &&
            forceFirstNode == false)
        {
            MoveToNextLoopPoint();
            return;
        }

        this.loopPath = new LoopPath(loopPoints,
            controlByExternalItem: controlByExternalItem, closedLoop: closedLoop);
        this.targetLevel = this.loopPath.CurrentTargetPoint.targetLevel;
        this.currentWaterRiseSpeed = this.loopPath.CurrentTargetPoint.riseSpeed;
        
        CheckIfCurrentLevelIsTooFar();
    }

    private void CheckIfCurrentLevelIsTooFar()
    {
        float yMinCamera = this.map.gameCamera.VisibleRectangle().yMin;
        float cameraLevel = yMinCamera - 5;

        // If current level is lower than the camera bottom (with an extra margin),
        // we made the current level jump to that height immediately
        if(this.targetLevel > this.currentLevel && cameraLevel > this.currentLevel)
        {
            this.currentLevel = cameraLevel;
        }
    }

    private bool IsSameLoop(LoopPoint[] lp1, LoopPoint[] lp2)
    {
        if(lp1.Length != lp2.Length) return false;

        for (int i = 0; i < lp1.Length; i++)
        {
            if(lp1[i].targetLevel != lp2[i].targetLevel)
            {
                return false;
            }
        }

        return true;
    }

    private void MoveToNextLoopPoint()
    {
        if(this.loopPath == null) return;

        this.loopPath.GoToNextPoint();
        this.targetLevel = this.loopPath.CurrentTargetPoint.targetLevel;
        this.currentWaterRiseSpeed =
            this.loopPath.CurrentTargetPoint.riseSpeed;
    }

    public void Advance()
    {
        AdvancePlayer();
        SetWaterColor();

        if(IsLoop == true && this.loopPath.controlByExternalItem == false)
        {
            if(this.loopPath.HasReachTarget(this.currentLevel))
            {
                if(this.loopPath.WaitForDelay())
                {
                    this.loopPath.delayTimer += 1f / 60f;
                }
                else
                {
                    MoveToNextLoopPoint();
                }
            }
        }

        this.currentLevel = Util.Slide(
            this.currentLevel, this.targetLevel, this.currentWaterRiseSpeed
        );
        this.waterTransform.position =
            new Vector2(this.heightmap.xMin, this.currentLevel);
        
        var activeChunks = this.map.chunks.Where(
            c =>
                c.gameObject.activeSelf == true
        ).ToArray();

        var cameraRectangle = this.map.gameCamera.VisibleRectangle();

        this.heightmap.SetBoundaries(cameraRectangle.xMin - 1, cameraRectangle.xMax + 1);
        this.heightmap.Advance();
        this.heightmap.DrawTo(this.waterMeshFilter, this.waterMesh);

        if(IsMoving == true && !Audio.instance.IsSfxPlaying(Assets.instance.sfxWaterRise) && this.targetLevel > this.currentLevel)
        {
            Audio.instance.PlaySfx(Assets.instance.sfxWaterRise, position: new Vector2(waterTransform.position.x, currentLevel), options: new Audio.Options(1, false, 0));
            Audio.instance.ChangeSfxPitch(Assets.instance.sfxWaterRise, currentLevel / targetLevel);
        }
        else if (IsMoving == true)
        {
            Audio.instance.ChangeSfxPitch(Assets.instance.sfxWaterRise, currentLevel / targetLevel);
            Audio.instance.ChangeSfxPosition(Assets.instance.sfxWaterRise, new Vector2(waterTransform.position.x, currentLevel));
        }
        else if(IsMoving == false && Audio.instance.IsSfxPlaying(Assets.instance.sfxWaterRise))
        {
            Audio.instance.StopSfx(Assets.instance.sfxWaterRise);
        }
    }

    private void AdvancePlayer()
    {
        Player player = this.map.player;

        if(player.alive == false) 
        {
            this.lastFramePlayerChunk = null;
            return;
        }
        
        if(player.state == Player.State.Respawning) return;
        if(player.state == Player.State.InsideLift) return;
        if(player.state == Player.State.Finished) return;

        Chunk currentPlayerChunk = this.map.NearestChunkTo(player.position);
        bool enteringChunk = currentPlayerChunk != lastFramePlayerChunk;

        int currentChunkIndex = currentPlayerChunk != null ? currentPlayerChunk.index : -1;
        int lastFrameChunkIndex = lastFramePlayerChunk != null ? lastFramePlayerChunk.index : -1;
        bool movingForward = currentChunkIndex > lastFrameChunkIndex;

        // In certain situations we want to control the water level ourselves to avoid issues
        if(enteringChunk == true && movingForward == true)
        {
            bool autoTriggerWaterMarker = false;

            foreach(var wm in currentPlayerChunk.items.OfType<WaterMarker>())
            {
                if(wm.AutoTrigger == true)
                {
                    wm.RaiseWater(forceFirstNode: true);
                    autoTriggerWaterMarker = true;
                }
            }

            if(autoTriggerWaterMarker == false)
            {
                // Everytime we enter a new checkpoint or a chunk with a trophy,
                // the water will rise to the bottom of the chunk
                bool riseLevelForCheckpointChunk =
                    currentLevel < currentPlayerChunk.yMin &&
                    (currentPlayerChunk.AnyItem<Checkpoint>() || currentPlayerChunk.AnyItem<Trophy>());

                // If we enter a chunk for the first time and the water level is already
                // covering it, we will lower it down to the bottom of the current chunk
                // Practical example of what this is solving: we have two horizontal chunks,
                // one after the other, and the first one increases the water level to the top,
                // while the second one is assuming the water is not covering it when the player enters
                bool lowerTheLevelForCurrentChunk = currentLevel > currentPlayerChunk.yMin;
                
                if(riseLevelForCheckpointChunk == true || lowerTheLevelForCurrentChunk == true)
                {
                    ChangeLevel(currentPlayerChunk.yMin, 0.20f);
                }
            }
        }

        this.lastFramePlayerChunk = currentPlayerChunk;
    }

    public void ResetToCheckpoint()
    {
        Chunk playerCheckpointChunk =
            this.map.NearestChunkTo(this.map.player.checkpointPosition);
        this.currentLevel = this.targetLevel = playerCheckpointChunk.yMin;
        this.loopPath = null;
    }

    public bool IsDeadly()
    {
        Chunk playerChunk =
            this.map.NearestChunkTo(this.map.player.position);

        foreach(var item in playerChunk.items)
        {
            if(item is Jellyfish &&
                (item as Jellyfish).IsElectricityAffectingWater == true)
            {
                return true;
            }
        }

        return false;
    }

    public void SetWaterColor()
    {
        Chunk playerChunk =
            this.map.NearestChunkTo(this.map.player.position);

        bool waterColorChanged = false;

        foreach (var item in playerChunk.items)
        {
            if(item is Jellyfish)
            {
                if ((item as Jellyfish).IsElectricityAffectingWater == true)
                {
                    if ((item as Jellyfish).CurrentFrame == 0)
                    {
                        this.waterMeshRenderer.material.SetFloat("_HueValue", 0.115f);
                        this.waterMeshRenderer.material.SetFloat("_SaturationValue", 0.1f);
                        this.waterMeshRenderer.material.SetFloat("_LightnessValues", -0.47f);
                    }
                    else if ((item as Jellyfish).CurrentFrame == 1)
                    {
                        this.waterMeshRenderer.material.SetFloat("_HueValue", 0.194f);
                        this.waterMeshRenderer.material.SetFloat("_SaturationValue", -0.045f);
                        this.waterMeshRenderer.material.SetFloat("_LightnessValue", -0.17f);
                    }
                    else
                    {
                        this.waterMeshRenderer.material.SetFloat("_HueValue", 0.557f);
                        this.waterMeshRenderer.material.SetFloat("_SaturationValue", -0.43f);
                        this.waterMeshRenderer.material.SetFloat("_LightnessValue", .3f);
                    }
                    this.waterMeshRenderer.sortingLayerName = "Default";
                    this.waterMeshRenderer.sortingOrder = 1;
                    this.isElectricityOn = true;
                    waterColorChanged = true;
                    return;
                }
            }            
        }

        if(this.isElectricityOn == true && waterColorChanged == false)
        {
            this.isElectricityOn = false;
            this.waterMeshRenderer.material = Assets.instance.spriteWater;
            this.waterMeshRenderer.material.SetFloat("_HueValue", 0);
            this.waterMeshRenderer.material.SetFloat("_SaturationValue", 0);
            this.waterMeshRenderer.material.SetFloat("_LightnessValue", 0);
        }
    }

    public class Heightmap
    {
        public Map map;
        public Waterline waterline;
        public const float Spacing = 0.25f;
        public float xMin;
        public float xMax;
        public Point[] points;

        private List<Vector3> meshVertices = new();
        private List<Color>   meshColors   = new();
        private List<Vector2> meshUVs      = new();
        private List<int>     meshIndices  = new();
        private float[] newValues; // temporary, but a member to avoid reallocating

        public struct Point
        {
            public float displacement;
            public float displacementLastFrame;
        }

        public Heightmap(Waterline waterline)
        {
            this.map = waterline.map;
            this.waterline = waterline;
        }

        public void SetBoundaries(float xMin, float xMax)
        {
            float oldXMin = this.xMin;
            float oldXMax = this.xMax;
            xMin = Mathf.FloorToInt(xMin / Spacing) * Spacing;
            xMax = Mathf.CeilToInt(xMax / Spacing) * Spacing;

            if (xMin == oldXMin && xMax == oldXMax)
                return;

            var oldPoints = this.points;
            int count = 1 + Mathf.CeilToInt((xMax - xMin) / Spacing);
            if (this.points == null || this.points.Length < count)
                this.points = new Point[count];
            this.xMin = xMin;
            this.xMax = xMax;

            if (oldPoints != null)
            {
                for (int n = 0; n < this.points.Length; n += 1)
                {
                    float x = xMin + (n * Spacing);
                    int indexFromOld = Mathf.RoundToInt((x - oldXMin) / Spacing);
                    indexFromOld = Mathf.Clamp(indexFromOld, 0, oldPoints.Length - 1);
                    this.points[n] = oldPoints[indexFromOld];
                }
            }
            else
            {
                for (int n = 0; n < this.points.Length; n += 1)
                {
                    this.points[n] = default;
                }
            }
        }

        public float DisplacementAtX(float x)
        {
            float indexFloat = (x - this.xMin) / Spacing;
            int indexOnLeft = Mathf.FloorToInt(indexFloat);
            if (indexOnLeft < 0)
                return this.points[0].displacement;
            if (indexOnLeft >= this.points.Length - 1)
                return this.points[this.points.Length - 1].displacement;

            float toNextIndex = indexFloat - indexOnLeft;
            return Mathf.Lerp(
                this.points[indexOnLeft].displacement,
                this.points[indexOnLeft + 1].displacement,
                indexFloat - indexOnLeft
            );
        }

        public void Advance()
        {
            if (this.newValues == null || this.newValues.Length < this.points.Length)
                this.newValues = new float[this.points.Length];
            int frame = this.map.frameNumber;

            for (int n = 0; n < this.points.Length; n += 1)
            {
                int nMinus1 = Mathf.Clamp(n - 1, 0, this.points.Length - 1);
                int nPlus1  = Mathf.Clamp(n + 1, 0, this.points.Length - 1);

                float surroundingValues = (
                    this.points[nMinus1].displacement +
                    this.points[nPlus1].displacement
                ) * .5f;
                float speed =
                    this.points[n].displacement -
                    this.points[n].displacementLastFrame;

                this.newValues[n] = surroundingValues;
                this.newValues[n] += speed * .97f;
                this.newValues[n] -= (this.points[n].displacement * .05f);

                if (this.waterline.currentLevel != this.waterline.targetLevel)
                {
                    this.newValues[n] += 0.004f * Mathf.Sin((frame * 0.3f) + (n * 0.06f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.2f) + (n * 0.10f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.12f) + (n * 0.15f));
                    this.newValues[n] += 0.008f * Mathf.Sin((frame * 0.06f) + (n * 0.09f));
                }
                else
                {
                    this.newValues[n] += 0.006f * Mathf.Sin((frame * 0.12f) + (n * 0.15f));
                    this.newValues[n] += 0.006f * Mathf.Sin((frame * 0.06f) + (n * 0.09f));
                }
            }

            void PushWater(Rect rectangle, float velocityY)
            {
                if (rectangle.yMin > this.waterline.currentLevel) return;
                if (rectangle.yMax < this.waterline.currentLevel) return;

                var first = Mathf.RoundToInt((rectangle.xMin - 2 - this.xMin) / Spacing);
                if (first >= this.points.Length) return;

                var last = Mathf.RoundToInt((rectangle.xMax + 2 - this.xMin) / Spacing);
                if (last < 0) return;

                var centerX = (rectangle.xMin + rectangle.xMax) * 0.5f;
                var centerIndex = Mathf.RoundToInt((centerX - this.xMin) / Spacing);
                int halfwidth = centerIndex - first;
                first = Mathf.Clamp(first, 0, this.points.Length - 1);
                last  = Mathf.Clamp(last, 0, this.points.Length - 1);
                for (int n = first; n <= last; n += 1)
                {
                    float centralness =
                        1.0f - ((float)Mathf.Abs(n - centerIndex) / halfwidth);
                    this.newValues[n] += velocityY * 0.14f * centralness;
                }
            }

            PushWater(this.map.player.Rectangle(), this.map.player.velocity.y);

            var chunk = this.map.NearestChunkTo(new Vector2(
                this.map.player.position.x,
                this.waterline.currentLevel
            ));
            foreach (var enemy in chunk.subsetOfItemsOfTypeEnemy)
            {
                var w = enemy as WalkingEnemy;
                if (w != null)
                    PushWater(w.hitboxes[0].InWorldSpace(), w.velocity.y);
            }
 
            for (int n = 0; n < this.points.Length; n += 1)
            {
                this.points[n].displacementLastFrame = this.points[n].displacement;
                this.points[n].displacement = this.newValues[n];
            }
        }

        public void DrawTo(MeshFilter meshFilter, Mesh mesh)
        {
            this.meshVertices = new List<Vector3>();
            this.meshColors   = new List<Color>();
            this.meshUVs      = new List<Vector2>();
            this.meshIndices  = new List<int>();

            var baseY = (this.map.gameCamera?.VisibleRectangle().yMin ?? 0) - 5;
            var baseYRelative = baseY - meshFilter.transform.position.y;

            for (int n = 0; n < this.points.Length - 1; n += 1)
            {
                float x1 = ((n + 0) * Spacing) + this.xMin;
                float x2 = ((n + 1) * Spacing) + this.xMin;

                int firstVertex = this.meshVertices.Count;
                var a = new Vector2(n * Spacing, this.points[n].displacement);
                var b = new Vector2((n + 1) * Spacing, this.points[n + 1].displacement);
                this.meshVertices.Add(a);
                this.meshVertices.Add(b);
                {
                    float movementOffset = this.map.frameNumber * 0.1f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.4f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.4f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                {
                    float movementOffset = this.map.frameNumber * -0.1f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.72f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.72f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                {
                    float movementOffset = this.map.frameNumber * 0.05f;
                    a.y += -0.2f + (Mathf.Sin(x1 * 0.56f + movementOffset) * 0.16f);
                    b.y += -0.2f + (Mathf.Sin(x2 * 0.56f + movementOffset) * 0.16f);
                    this.meshVertices.Add(a);
                    this.meshVertices.Add(b);
                }
                this.meshVertices.Add(new Vector2(a.x, baseYRelative));
                this.meshVertices.Add(new Vector2(b.x, baseYRelative));
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshColors.Add(Color.white);
                this.meshUVs.Add(new Vector2(0, 1));
                this.meshUVs.Add(new Vector2(1, 1));
                this.meshUVs.Add(new Vector2(0, 1 - 0.0625f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.0625f));
                this.meshUVs.Add(new Vector2(0, 1 - 0.125f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.125f));
                this.meshUVs.Add(new Vector2(0, 1 - 0.1875f));
                this.meshUVs.Add(new Vector2(1, 1 - 0.1875f));
                this.meshUVs.Add(new Vector2(0, 0));
                this.meshUVs.Add(new Vector2(1, 0));

                for (int offset = 0; offset < 8; offset += 2)
                {
                    this.meshIndices.Add(firstVertex + offset + 0);
                    this.meshIndices.Add(firstVertex + offset + 2);
                    this.meshIndices.Add(firstVertex + offset + 1);
                    this.meshIndices.Add(firstVertex + offset + 1);
                    this.meshIndices.Add(firstVertex + offset + 2);
                    this.meshIndices.Add(firstVertex + offset + 3);
                }
            }

            mesh.Clear();
            mesh.SetVertices(this.meshVertices);
            mesh.SetColors(this.meshColors);
            mesh.SetUVs(0, this.meshUVs);
            mesh.SetIndices(this.meshIndices, MeshTopology.Triangles, 0);
            meshFilter.mesh = mesh;
        }
    }
}
