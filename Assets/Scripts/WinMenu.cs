using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class WinMenu : MonoBehaviour
{
    public ThemesAsset themesAsset;

    public TMP_Text headerText;
    public LocalizeText headerTextLocalize;
    [NonSerialized] public Material circleMaterial;
    public RectTransform circleImageTransform;
    public RectTransform uiTransform;
    public RectTransform uiBehindTransform;
    public RectTransform uiStarsTransform;

    public RectTransform starsContainer;
    public RectTransform goldCup;
    public RectTransform fruitCup;
    public RectTransform multiplayerWinnerCup;
    public RectTransform multiplayerLoserCup;
    public RectTransform backShine1;
    public RectTransform backShine2;
    public RectTransform totalCoinsBox;
    public CircleText totalCoinsBoxWordTotal;
    public RectTransform totalCoinsBoxCoin;
    public TMP_Text totalCoinsBoxCount;
    public RectTransform counterTime;
    public TMP_Text[] counterTimeText;
    public RectTransform counterDeaths;
    public TMP_Text[] counterDeathsText;
    public RectTransform remainingFruitBox;
    public LocalizeText remainingFruitText;
    public RectTransform remainingFruitFilledRect;
    public RectTransform buttonCalendar;
    public RectTransform buttonCharacterSelect;
    public RectTransform buttonShop;
    public RectTransform buttonShare;
    public RectTransform shopExclamationMark;
    public SelectionCursor selectionCursor;
    public RectTransform smackParent;
    public RectTransform timerIcon;
    public TMP_Text timerText;
    public RectTransform coinsParent;
    public RectTransform fruitGlasses;
    public RectTransform[] goldGlasses;
    public RectTransform purpleGlasses;
    public RectTransform hatCrown;
    public Animated juice;
    public TMP_Text[] juiceFruitsText;
    public TMP_Text[] juiceCoinsText;
    public Animated[] stackedCoins;
    public RectTransform sparkleEffectPrefab;
    public RectTransform smackEffectPrefab;
    public Animated spinningCoin;

    [Serializable]
    public class MultiplayerTrophyForPlayer
    {
        public string name;
        public Transform container;
        public Image trophyImage;
        public Image playerIcon;
        public RectTransform balloonRectTransform;
        public LocalizeText winnerLoserText;
    }

    [Serializable]
    public class MultiplayerTrophyLayout
    {
        public Transform container;
        public float containerScale;
        public MultiplayerTrophyForPlayer[] players;
    }

    public MultiplayerTrophyLayout splitscreen2PlayerLayout;
    public MultiplayerTrophyLayout splitscreen3PlayerLayout;
    public MultiplayerTrophyLayout splitscreen4PlayerLayout;
    public Sprite multiplayerWinnerTrophySprite;
    public Sprite multiplayerLoserTrophySprite;
    public Sprite[] playerIconSprites;

    public RectTransform leaderboardContainer;
    public LeaderboardRow[] leaderboardRows;
    public Image backWide;
    [Serializable]
    public struct LeaderboardRow
    {
        public Animator animator;
    }

    public RectTransform starPrefab;
    public Sprite[] starSprites;
    public Animated.Animation animationCoinSpin;
    public AnimationCurve circleInAnimationCurve;
    public AnimationCurve buttonInAnimationCurve;

    public GameObject coinPrefab;

    public bool isOpen = false;
    public float circleOpenAmount = 1;
    public float circleClosedAmount = 0;

    public static int Radius = Shader.PropertyToID("_Radius");
    public static int OutlineThickness = Shader.PropertyToID("_OutlineThickness");

    public RectTransform smallCoinPrefab;
    public Animated.Animation smackEffect;
    public Animated.Animation sparkleEffect;
    public Animated.Animation animationCoinSpin2;
    public Animated.Animation animationTimerHit;
    public Animated.Animation animationDeathHit;
    public Animated.Animation animationJuice;
    public Animated.Animation animationCoinFlicker;
    private Coroutine openCoroutine;

    private int bonusCoinsToAwardFromFruit;
    
    public void Init()
    {
        var circleImage = this.circleImageTransform.GetComponent<Image>();
        this.circleMaterial = circleImage.material = new Material(circleImage.material);
        this.circleMaterial.SetFloat(Radius, 0.5f);

        this.gameObject.SetActive(false);
        this.selectionCursor.gameObject.SetActive(false);
    }

    public void Open()
    {
        this.isOpen = true;
        SetupColours();
        this.gameObject.SetActive(true);

        // save any coins collected at the end of the level (e.g. out of the trophy)
        Game.instance.map1.SaveCoins();

        // calculate any bonus coins immediately, and save them, too.
        if (Multiplayer.IsMultiplayerGame() == false)
        {
            // this.bonusCoinsToAwardFromFruit = Game.instance.map1.fruitCollected / 30;
            this.bonusCoinsToAwardFromFruit =
                Game.instance.map1.fruitCollected * 10 /
                Game.instance.map1.TotalFruitAtStart();

            this.bonusCoinsToAwardFromFruit = Mathf.Clamp(
                this.bonusCoinsToAwardFromFruit, 0, 10
            );

            SaveData.SetCoins(SaveData.GetCoins() + this.bonusCoinsToAwardFromFruit);
        }
        else
        {
            this.bonusCoinsToAwardFromFruit = 0;
        }

        // at this point, SaveData's coin count is finalized.
        // all animation that happens is purely aesthetic,
        // and SaveData shouldn't be touched

        var record = SaveData.GetRecordForDate(Game.selectedDate);

        foreach (var text in this.counterTimeText)
        {
            text.text = SaveData.TimeToString(record.timeInSeconds);
        }
        foreach (var text in this.counterDeathsText)
        {
            text.text = 0.ToString();
        }

        int coinStartValue =
            SaveData.GetCoins()
            - Game.instance.map1.coinsCollected
            - this.bonusCoinsToAwardFromFruit;
        this.totalCoinsBoxCount.text = Mathf.Max(coinStartValue, 0).ToString();

        openCoroutine = StartCoroutine(OpenAnimation());
    }

    public void OpenMultiplayerSplitscreen()
    {
        this.isOpen = true;
        SetupColours();
        this.gameObject.SetActive(true);
    }

    private void SetupColours()
    {
        if(Map.instance == null) return;
        var theme = this.themesAsset.ThemeInfo(Map.instance.theme);

        this.circleImageTransform.GetComponent<Image>().color  = ThemesAsset.CircleColor(theme.theme);
        this.headerText.color                                  = theme.textColour1;

        var buttonsColor1 = new[] {
            this.buttonCharacterSelect.GetComponent<UnityEngine.UI.Button>(),
            this.buttonShare.GetComponent<UnityEngine.UI.Button>(),
            this.buttonShop.GetComponent<UnityEngine.UI.Button>(),
            this.buttonCalendar.GetComponent<UnityEngine.UI.Button>(),
        };

        var colorShadow = new Color32(79, 79, 79, 0);
        var colorSelected = new Color32(247, 228, 27, 255);

        foreach(var btn in buttonsColor1)
        {
            var cb = btn.colors;
            cb.normalColor = cb.highlightedColor = theme.textColour1;
            cb.pressedColor = theme.textColour1 - colorShadow;
            cb.selectedColor = colorSelected;
            btn.colors = cb;
        }
    }

    private IEnumerator OpenAnimation()
    {
        Map.instance.PlayWinMusic();

        Vector3 scaleZero = new Vector3(0, 0, 1);

        bool showFruitCup = Map.instance.ShouldAwardFruitCup();

        this.fruitCup.gameObject.SetActive(false);
        this.goldCup.gameObject.SetActive(false);
        this.multiplayerWinnerCup.gameObject.SetActive(false);
        this.multiplayerLoserCup.gameObject.SetActive(false);

        bool isMultiplayerGame = Multiplayer.IsMultiplayerGame();
        bool isMultiplayerSplitScreenGame = Multiplayer.IsMultiplayerSplitScreenGame();
        bool playerOneWins = Multiplayer.IsPlayerWinning(Game.instance.map1.player);
        bool localPlayerWins = Multiplayer.IsPlayerWinning(Map.instance.player);

        if(isMultiplayerSplitScreenGame == true)
        {
            var multiplayer = Game.instance.maps.Length switch
            {
                4 => this.splitscreen4PlayerLayout,
                3 => this.splitscreen3PlayerLayout,
                _ => this.splitscreen2PlayerLayout
            };
            multiplayer.container.gameObject.SetActive(true);

            for (int p = 0; p < Game.instance.maps.Length; p += 1)
            {
                var playerUI = multiplayer.players[p];
                var map = Game.instance.maps[p];

                var isWinner = Multiplayer.IsPlayerWinning(map.player);
                playerUI.trophyImage.sprite =
                    isWinner ?
                    this.multiplayerWinnerTrophySprite :
                    this.multiplayerLoserTrophySprite;
                
                var spriteIndex = Mathf.Clamp(
                    (int)map.player.Character(), 0, this.playerIconSprites.Length - 1
                );
                playerUI.playerIcon.sprite = this.playerIconSprites[spriteIndex];

                playerUI.balloonRectTransform.anchoredPosition = new Vector3(
                    playerUI.balloonRectTransform.anchoredPosition.x,
                    (isWinner == true) ? 102f : 86f
                );

                playerUI.winnerLoserText.SetText(isWinner ? "Winner" : "Loser");
            }
        }

        if(isMultiplayerGame == true)
        {
            if(isMultiplayerSplitScreenGame == false)
            {
                this.headerTextLocalize.SetText(localPlayerWins ? "Winner" : "Loser");
                this.multiplayerWinnerCup.gameObject.SetActive(localPlayerWins == true);
                this.multiplayerLoserCup.gameObject.SetActive(localPlayerWins == false);
            }
        }
        else
        {
            if(showFruitCup == true)
            {
                this.fruitCup.gameObject.SetActive(true);
            }
            else
            {
                this.goldCup.gameObject.SetActive(true);
            }
        }

        this.headerText.transform.rotation = Quaternion.Euler(0, 0, 175);
        this.backShine1           .localScale = scaleZero;
        this.backShine2           .localScale = scaleZero;
        this.goldCup              .localScale = scaleZero;
        this.fruitCup             .localScale = scaleZero;
        this.multiplayerWinnerCup .localScale = scaleZero;
        this.multiplayerLoserCup  .localScale = scaleZero;
        this.buttonShop           .localScale = scaleZero;
        this.buttonCharacterSelect.localScale = scaleZero;
        this.buttonCalendar       .localScale = scaleZero;
        this.buttonShare          .localScale = scaleZero;
        this.totalCoinsBox        .localScale = scaleZero;
        this.counterTime          .localScale = scaleZero;
        this.counterDeaths        .localScale = scaleZero;
        this.remainingFruitBox    .localScale = scaleZero;
        this.splitscreen2PlayerLayout.players[0].container.localScale = scaleZero;
        this.splitscreen2PlayerLayout.players[1].container.localScale = scaleZero;
        this.splitscreen3PlayerLayout.players[0].container.localScale = scaleZero;
        this.splitscreen3PlayerLayout.players[1].container.localScale = scaleZero;
        this.splitscreen3PlayerLayout.players[2].container.localScale = scaleZero;
        this.splitscreen4PlayerLayout.players[0].container.localScale = scaleZero;
        this.splitscreen4PlayerLayout.players[1].container.localScale = scaleZero;
        this.splitscreen4PlayerLayout.players[2].container.localScale = scaleZero;
        this.splitscreen4PlayerLayout.players[3].container.localScale = scaleZero;
        this.leaderboardContainer.gameObject.SetActive(false);
        this.backWide.gameObject.SetActive(false);

        var easeCircle = Easing.ByAnimationCurve(this.buttonInAnimationCurve);
        var easeButtons = Easing.ByAnimationCurve(this.buttonInAnimationCurve);

        var tweener = new Tweener();
        var tweenForCircle = tweener.TweenFloat(1, 0, 1, easeCircle);
        tweener.ScaleLocal(this.backShine1, Vector3.one, 0.5f, easeCircle);
        tweener.ScaleLocal(this.backShine2, Vector3.one, 0.5f, easeCircle);
        tweener.ScaleLocal(this.goldCup, Vector3.one, 0.5f, Easing.QuadEaseOut);
        tweener.ScaleLocal(this.fruitCup, Vector3.one, 0.5f, Easing.QuadEaseOut);
        tweener.ScaleLocal(this.multiplayerWinnerCup, Vector3.one, 0.5f, Easing.QuadEaseOut);
        tweener.ScaleLocal(this.multiplayerLoserCup, Vector3.one, 0.5f, Easing.QuadEaseOut);
        tweener.RotateLocal(this.headerText.transform, 0, 0.4f, Easing.QuadEaseOut, 0.5f);

        if (Game.instance.maps.Length <= 2)
        {
            Vector2 leaderboardPosition = this.leaderboardContainer.transform.localPosition;
            {
                this.leaderboardContainer.transform.localPosition =
                    leaderboardPosition.Add(0, -100);
                this.leaderboardContainer.gameObject.SetActive(true);
            }

            /*for (int n = 0; n < this.leaderboardRows.Length; n += 1)
            {
                this.leaderboardRows[n].animator.enabled = true;
                this.leaderboardRows[n].animator.speed = 0;
            }*/

            tweener.MoveLocal(this.leaderboardContainer, leaderboardPosition, 0.5f, Easing.QuadEaseOut, 2);
        }
        else
        {
            var rt = this.backWide.GetComponent<RectTransform>();
            rt.anchoredPosition = new Vector2(0, -120);
            tweener.MoveAnchoredPosition(rt, new Vector2(0, -32.5f), 0.5f, Easing.QuadEaseOut);
            this.backWide.gameObject.SetActive(true);
        }

        Audio.instance.PlaySfx(Assets.instance.sfxUIWinIn, options: new Audio.Options(1f, false, 0f));

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleOpenAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }
        yield return new WaitForSecondsRealtime(0.7f);

        tweener.Clear();

        if(isMultiplayerSplitScreenGame == false)
        {
            tweener.ScaleLocal(this.totalCoinsBox, Vector3.one, 0.8f, Easing.BounceEaseOut, 0.5f);

            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }
            tweener.Clear();

            //yield return CollectFruitAnimation();
        
            tweener.ScaleLocal(this.counterTime, Vector3.one, 0.8f, Easing.BounceEaseOut);
            tweener.ScaleLocal(this.counterDeaths, Vector3.one, 0.8f, Easing.BounceEaseOut, 0.25f);
        }

        var buttons = new []
        {
            this.buttonShare,
            this.buttonCharacterSelect,
            this.buttonCalendar,
            this.buttonShop
        };
        for (int n = 0; n < buttons.Length; n += 1)
        {
            var delay = 0.5f + (0.08f * n);
            tweener.ScaleLocal(buttons[n], Vector3.one, 0.5f, easeButtons, delay);
        }

        Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesIn, options: new Audio.Options(1f, false, 0f));

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        tweener.Clear();

        if (isMultiplayerSplitScreenGame == true)
        {
            var multiplayer = Game.instance.maps.Length switch
            {
                4 => this.splitscreen4PlayerLayout,
                3 => this.splitscreen3PlayerLayout,
                _ => this.splitscreen2PlayerLayout
            };
            foreach (var player in multiplayer.players)
            {
                tweener.ScaleLocal(
                    player.container, Vector3.one * multiplayer.containerScale,
                    0.2f, Easing.QuadEaseIn
                );
            }
        }

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        if(isMultiplayerSplitScreenGame == false)
        {
            float time = Map.instance.timerCollected;
            float totalTime = SaveData.GetRecordForDate(Game.selectedDate).timeInSeconds;
            float timer = 0.05f;

            if(time > 0)
            {
                CreateSmackParticle();
                Audio.instance.PlaySfx(Assets.instance.sfxPrizeWinShort);
                Audio.instance.PlaySfx(Assets.instance.sfxPinballPortalIn);

                this.timerIcon.gameObject.SetActive(true);
                this.timerText.text = "-" + time.ToString();
                this.timerIcon.localScale = scaleZero;
                tweener.ScaleLocal(this.timerIcon, Vector3.one, 0.1f, Easing.QuadEaseIn);
                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }
                tweener.Clear();
                yield return new WaitForSecondsRealtime(0.7f);

                tweener.MoveLocal(this.timerIcon, new Vector3(32.5f, 22.7f, 0), 0.3f, Easing.SineEaseIn);
                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }
                tweener.Clear();

                tweener.MoveLocal(this.timerIcon, this.counterTime.localPosition + new Vector3(-1,10,0) , 0.35f, Easing.QuartEaseIn);
                tweener.ScaleLocal(this.timerIcon, Vector3.one/2, 0.35f, Easing.QuadEaseIn);

                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }

                tweener.Clear();
                this.timerIcon.gameObject.SetActive(false);
                Audio.instance.PlaySfx(Assets.instance.sfxPowerupCollect);
                this.counterTime.GetComponent<Animated>().PlayAndHoldLastFrame(this.animationTimerHit);
                if(time == 0)
                {
                    time = -10;
                }
                while (time >= 0)
                {
                    timer -= Time.deltaTime;
                    if (timer <= 0)
                    {
                        timer = 0.05f;
                        time -= 10;
                        totalTime -= 10;
                        foreach (var text in this.counterTimeText)
                        {
                            text.text = SaveData.TimeToString(totalTime);
                        }

                        var sparkle = Instantiate(this.sparkleEffectPrefab, this.counterTime);
                        sparkle.transform.localPosition = new Vector3(4.8f + UnityEngine.Random.Range(-4,4), -23.62f);
                        sparkle.transform.localScale = Vector3.one*0.7f;
                        var sparkleAnimated = sparkle.GetComponent<Animated>();
                        sparkleAnimated.PlayOnce(sparkleEffect, () =>
                        {
                            Destroy(sparkle.gameObject);
                        });

                        tweener.MoveLocal(sparkle, new Vector3(sparkle.transform.localPosition.x, sparkle.transform.localPosition.y - UnityEngine.Random.Range(10, 20)), 1f, Easing.QuintEaseOut);
                    }
                    if (!tweener.IsDone())
                    {
                        tweener.Advance(Time.unscaledDeltaTime);
                    }
                    yield return null;
                }
                tweener.Clear();

                yield return new WaitForSecondsRealtime(0.5f);
            }

            CreateSmackParticle();
            Audio.instance.PlaySfx(Assets.instance.sfxPrizeWinShort);
            Audio.instance.PlaySfx(Assets.instance.sfxPinballPortalIn, new() { pitchIncrement = 0.1f });
            this.spinningCoin.gameObject.SetActive(true);
            this.spinningCoin.PlayAndLoop(animationCoinSpin2);
            yield return new WaitForSecondsRealtime(0.5f);

            Audio.instance.PlaySfx(Assets.instance.sfxUIShopPurchaseCoins);

            int totalCoinCount =
                SaveData.GetCoins()
                - Map.instance.coinsCollected
                - this.bonusCoinsToAwardFromFruit;

            for (int n = 0; n < Map.instance.coinsCollected; n += 1)
            {
                RectTransform coin = Instantiate(this.smallCoinPrefab, this.coinsParent).GetComponent<RectTransform>();
                coin.GetComponent<Animated>().PlayAndLoop(this.animationCoinSpin2);
                coin.localScale = Vector3.one * 0.95f;
                tweener.MoveLocal(coin, this.totalCoinsBoxCoin.localPosition - new Vector3(0, 9, 0), 0.15f, Easing.QuadEaseIn, n * 0.15f);
                tweener.ScaleLocal(coin, Vector3.one * 0.33f, 0.15f, Easing.QuadEaseOut, n * 0.15f);
                tweener.Callback((n * 0.15f) + 0.15f, delegate
                {

                    Destroy(coin.gameObject);

                    totalCoinCount += 1;
                    this.totalCoinsBoxCount.text = Mathf.Max(totalCoinCount, 0).ToString();

                    var sparkle = Instantiate(this.sparkleEffectPrefab, this.totalCoinsBox);
                    sparkle.transform.localPosition = new Vector3(48.9f + UnityEngine.Random.Range(-4, 4), 40.7f);
                    sparkle.transform.localScale = Vector3.one * 0.7f;
                    var sparkleAnimated = sparkle.GetComponent<Animated>();
                    sparkleAnimated.PlayOnce(sparkleEffect, () =>
                    {
                        Destroy(sparkle.gameObject);
                    });
                    tweener.MoveLocal(sparkle, new Vector3(sparkle.transform.localPosition.x, sparkle.transform.localPosition.y - UnityEngine.Random.Range(10, 20)), 1f, Easing.QuintEaseOut);

                    var smack = Instantiate(this.smackEffectPrefab, this.totalCoinsBoxCoin);
                    smack.transform.localScale = Vector3.one * 0.0085f;
                    var smackAnimated = smack.GetComponent<Animated>();
                    smackAnimated.PlayOnce(smackEffect, () =>
                    {
                        Destroy(smack.gameObject);
                    });
                });
                if (n == Map.instance.coinsCollected - 1)
                {
                    tweener.Callback((n * 0.15f) + 0.15f, delegate
                    {
                        Audio.instance.StopSfx(Assets.instance.sfxUIShopPurchaseCoins);
                    });
                }
            }

            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }
            tweener.Clear();

            Audio.instance.StopSfx(Assets.instance.sfxUIShopPurchaseCoins);

            yield return new WaitForSecondsRealtime(0.1f);

            this.spinningCoin.gameObject.SetActive(false);
            CreateSmackParticle();
            Audio.instance.PlaySfx(Assets.instance.sfxPrizeWinShort);
            Audio.instance.PlaySfx(Assets.instance.sfxPinballPortalIn, new() { pitchIncrement = 0.2f });

            yield return new WaitForSecondsRealtime(0.3f);

            this.juice.gameObject.SetActive(true);

            this.juice.transform.localScale = Vector3.one * 0.7f;
            tweener.ScaleLocal(this.juice.transform, Vector3.one, 0.15f, Easing.QuadEaseOut, 0.1f);
            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }
            tweener.Clear();

            foreach (var text in this.juiceFruitsText)
            {
                text.text = this.bonusCoinsToAwardFromFruit.ToString();
            }
            foreach (var text in this.juiceCoinsText)
            {
                text.text = 0.ToString();
            }
            yield return new WaitForSecondsRealtime(0.5f);

            Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesOut);

            this.juice.PlayOnce(this.animationJuice, () =>
            {
                this.juice.GetComponent<Image>().enabled = false;
            });

            timer = 0;
            totalTime = 0.6f;
            int coinsActivated = 0;
            while (timer < totalTime)
            {
                timer += Time.deltaTime;

                int tempCoin = (int)Mathf.Floor(
                    (timer / totalTime) * (this.bonusCoinsToAwardFromFruit)
                );
                if (tempCoin > coinsActivated)
                {
                    this.stackedCoins[coinsActivated].gameObject.SetActive(true);
                    this.stackedCoins[coinsActivated].PlayAndHoldLastFrame(animationCoinFlicker);
                    coinsActivated++;
                }
                foreach (var text in this.juiceFruitsText)
                {
                    text.text = Mathf.FloorToInt(
                        Mathf.Lerp(this.bonusCoinsToAwardFromFruit, 0, timer / totalTime)
                    ).ToString();
                }
                foreach (var text in this.juiceCoinsText)
                {
                    text.text = Mathf.FloorToInt(
                        Mathf.Lerp(0, this.bonusCoinsToAwardFromFruit, timer / totalTime)
                    ).ToString();
                }
                yield return null;
            }
            yield return new WaitForSecondsRealtime(0.5f);

            if (coinsActivated > 0)
                Audio.instance.PlaySfx(Assets.instance.sfxUIShopPurchaseCoins);

            var coinsNew = new List<RectTransform>();

            for (int n = 0; n < coinsActivated; n += 1)
            {
                RectTransform test = Instantiate(this.smallCoinPrefab, this.stackedCoins[1].transform.parent).GetComponent<RectTransform>();
                test.GetComponent<Animated>().PlayAndLoop(this.animationCoinSpin2);
                test.localScale = Vector3.one * 0.33f;
                test.localPosition = this.stackedCoins[coinsActivated - n - 1].transform.localPosition + new Vector3(0, 10, 0);
                test.GetComponent<Image>().enabled = false;
                tweener.MoveLocal(test, totalCoinsBoxCoin.localPosition + new Vector3(4, 4, 0), 0.15f, Easing.QuadEaseIn, n * 0.15f);
                tweener.Callback((n * 0.15f) + 0.15f, delegate
                {
                    Destroy(test.gameObject);
                    totalCoinCount += 1;
                    this.totalCoinsBoxCount.text = Mathf.Max(totalCoinCount, 0).ToString();

                    var sparkle = Instantiate(this.sparkleEffectPrefab, this.totalCoinsBox);
                    sparkle.transform.localPosition = new Vector3(48.9f + UnityEngine.Random.Range(-4, 4), 40.7f);
                    sparkle.transform.localScale = Vector3.one * 0.7f;
                    var sparkleAnimated = sparkle.GetComponent<Animated>();
                    sparkleAnimated.PlayOnce(sparkleEffect, () =>
                    {
                        Destroy(sparkle.gameObject);
                    });

                    tweener.MoveLocal(sparkle, new Vector3(sparkle.transform.localPosition.x, sparkle.transform.localPosition.y - UnityEngine.Random.Range(10, 20)), 0.5f, Easing.SineEaseIn);
                });
                if (n == coinsActivated - 1)
                {
                    tweener.Callback((n * 0.15f) + 0.15f, delegate
                    {
                        Audio.instance.StopSfx(Assets.instance.sfxUIShopPurchaseCoins);
                    });
                }
                coinsNew.Add(test);
            }

            timer = 0;
            totalTime = 0.15f + (0.15f * (coinsActivated + 1));
            int p = 0;
            bool stop = false;
            while (!tweener.IsDone())
            {
                timer += Time.deltaTime;
                
                if (timer > p * 0.15f && stop == false)
                {
                    coinsNew[p].GetComponent<Image>().enabled = true;
                    p++;
                    this.stackedCoins[coinsActivated-1].gameObject.SetActive(false);
                    coinsActivated--;
                    foreach (var text in this.juiceCoinsText)
                    {
                        text.text = (coinsActivated).ToString();
                    }
                }

                if (coinsActivated == 0)
                {
                    timer += 10;
                    stop = true;
                }
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }

            tweener.Clear();

            yield return new WaitForSecondsRealtime(0.5f);
            this.juice.gameObject.SetActive(false);

            RectTransform[] skullAccessories = null;
            int deaths = Map.instance.deaths;

            if(deaths <= 0)
            {
                skullAccessories = new RectTransform[] { this.hatCrown };
            }
            else if(deaths <= 1)
            {
                skullAccessories = new RectTransform[] { this.purpleGlasses };
            }
            else if(deaths <= 5)
            {
                skullAccessories = this.goldGlasses;
            }
            else if(deaths <= 10)
            {
                skullAccessories = new RectTransform[] { this.fruitGlasses };
            }

            if(skullAccessories != null && skullAccessories.Length > 0)
            {
                CreateSmackParticle();
                Audio.instance.PlaySfx(Assets.instance.sfxPrizeWinShort);
                Audio.instance.PlaySfx(Assets.instance.sfxPinballPortalIn, new() { pitchIncrement = 0.3f });
                yield return new WaitForSecondsRealtime(0.1f);

                foreach(var sa in skullAccessories)
                {
                    sa.gameObject.SetActive(true);
                    sa.localRotation = Quaternion.Euler(0,0,45);
                    tweener.RotateLocal(sa, 0, 0.5f, Easing.QuadEaseInOut);
                }

                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }
                tweener.Clear();

                yield return new WaitForSecondsRealtime(0.1f);

                foreach(var sa in skullAccessories)
                {
                    tweener.MoveLocal(sa, new Vector3(-34.4f, 6.1f, 0), 0.2f, Easing.SineEaseIn);
                }

                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }
                tweener.Clear();

                foreach(var sa in skullAccessories)
                {
                    tweener.MoveLocal(sa, new Vector3(73.13f, 8.9f, 0), 0.3f, Easing.QuartEaseIn);
                    tweener.ScaleLocal(sa, Vector3.one/2, 0.3f, Easing.QuadEaseIn);
                }

                while (!tweener.IsDone())
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    yield return null;
                }
                tweener.Clear();
                Audio.instance.PlaySfx(Assets.instance.sfxPowerupCollect);
                this.counterDeaths.GetComponent<Animated>().PlayAndHoldLastFrame(animationDeathHit);
            }

            timer = 0;
            totalTime = 0.7f;
            while (timer < totalTime && deaths > 0)
            {
                timer += Time.deltaTime;
                int current = Mathf.FloorToInt(Mathf.Lerp(0, deaths, timer / totalTime));
                foreach (var text in this.counterDeathsText)
                {
                    text.text = current.ToString();
                }
                yield return null;
            }

            yield return new WaitForSecondsRealtime(0.7f);

            if(skullAccessories != null && skullAccessories.Length > 0)
            {
                var smack = Instantiate(this.smackEffectPrefab, skullAccessories[0]);
                smack.transform.localPosition = new Vector3(29.4f, 7.5f);
                smack.transform.localScale = Vector3.one * 0.22f;
                var smackAnimated = smack.GetComponent<Animated>();
                smackAnimated.PlayOnce(smackEffect, () =>
                {
                    Destroy(smack.gameObject);
                });
            }
        }

        yield return new WaitForSecondsRealtime(0.2f);

        this.selectionCursor.gameObject.SetActive(true);
        this.selectionCursor.Select(
            this.buttonCalendar.GetComponent<Selectable>(), false
        );

        /*for (int n = 0; n < this.leaderboardRows.Length; n += 1)
        {
            this.leaderboardRows[n].animator.speed = 1;
            yield return new WaitForSecondsRealtime(0.3f);
        }*/

        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1.05f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();

        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();
        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1.05f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();

        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();
        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1.05f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();

        tweener.ScaleLocal(this.buttonCalendar, Vector3.one * 1f, 0.6f, Easing.SineEaseIn);
        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
        tweener.Clear();

        if (SaveData.WasRatingPopUpDisplayed() == false)
        {
            var startedAndCompleted = SaveData.GetDaysStartedAndCompleted(SaveData.currentSlotNumber);
            if (startedAndCompleted.completed >= 2)
            {
                //show the popup on completion of day 2 and onwards, but only once
                RequestReview.ShowRequestReview();
            }
        }
    }

    public IEnumerator CollectFruitAnimation()
    {
        int collected = Map.instance.fruitCollected;
        int possible = Map.instance.TotalFruitAtStart();

        this.remainingFruitText.SetText("REMAINING_FRUIT", collected.ToString());
        this.remainingFruitFilledRect.anchorMax =
            new Vector2((float)collected / (float)possible, 1);

        var tweener = new Tweener();
        tweener.ScaleLocal(this.remainingFruitBox, Vector3.one, 0.8f, Easing.BounceEaseOut, 0.5f);
        tweener.ScaleLocal(this.totalCoinsBox, Vector3.one, 0.8f, Easing.BounceEaseOut, 0.5f);

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(0.5f);

        var coins = new List<RectTransform>();

        for (int n = collected; n >= 1; n -= 1)
        {
            this.remainingFruitText.SetText("REMAINING_FRUIT", n.ToString());
            this.remainingFruitFilledRect.anchorMax =
                new Vector2((float)n / (float)possible, 1);

            if (n % 50 == 0)
            {
                var coin =
                    Instantiate(this.coinPrefab, this.uiTransform)
                    .GetComponent<RectTransform>();
                coin.anchoredPosition = new Vector2(
                    UnityEngine.Random.Range(35, 55),
                    UnityEngine.Random.Range(-35, -25)
                );
                coin.GetComponent<Image>().color = new Color(1, 1, 1, 0);
                coin.GetComponent<Animated>().PlayAndLoop(this.animationCoinSpin);
                coins.Add(coin);

                IEnumerator CoinAppear()
                {
                    var target = coin.transform.localPosition;
                    target.y += UnityEngine.Random.Range(10, 30);

                    var tweener = new Tweener();
                    tweener.MoveLocal(coin, target, 0.5f, Easing.QuadEaseOut);
                    tweener.Alpha(coin.GetComponent<Image>(), 1, 0.3f, null);
                    
                    while (!tweener.IsDone())
                    {
                        tweener.Advance(Time.unscaledDeltaTime);
                        yield return null;
                    }
                }
                StartCoroutine(CoinAppear());
            }

            if (n % 2 == 0)
                yield return null;
        }
        this.remainingFruitText.SetText("REMAINING_FRUIT", "0");

        yield return new WaitForSeconds(0.2f);

        int totalCoinCount = SaveData.GetCoins();
        // SaveData.SetCoins(totalCoinCount + coins.Count);

        tweener.Clear();
        for (int n = 0; n < coins.Count; n += 1)
        {
            var coin = coins[n];
            var target = this.totalCoinsBoxCoin.transform.position;
            tweener.Move(coin, target, 0.5f, Easing.QuadEaseIn, n * 0.1f);
            tweener.Callback((n * 0.1f) + 0.5f, delegate
            {
                Destroy(coin.gameObject);
                totalCoinCount += 1;
                this.totalCoinsBoxCount.text = Mathf.Max(totalCoinCount, 0).ToString();
            });
        }

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }

        yield return new WaitForSeconds(0.5f);

        tweener.Clear();
        tweener.ScaleLocal(
            this.remainingFruitBox, new Vector3(0, 0, 1), 0.8f, Easing.QuadEaseIn, 0.5f
        );

        while (!tweener.IsDone())
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
    }

    public void OnPressShop()
    {
        StartCoroutine(ExitGameAnimation("Shop"));
    }

    public void OnPressCharacterSelect()
    {
        StartCoroutine(ExitGameAnimation("Character Select"));
    }

    public void OnPressCalendar()
    {
        StartCoroutine(ExitGameAnimation("Calendar"));
    }

    private IEnumerator ExitGameAnimation(string targetScene)
    {
        StopCoroutine(openCoroutine);

        this.selectionCursor.gameObject.SetActive(false);

        var buttons = new []
        {
            this.buttonShare,
            this.buttonCalendar,
            this.buttonCharacterSelect,
            this.buttonShop,
        };

        Tween.Ease easeButtons = Easing.QuadEaseIn;
        var tweener = new Tweener();

        for (int n = 0; n < buttons.Length; n += 1)
        {
            tweener.ScaleLocal(buttons[n], Vector3.zero, 0.3f, easeButtons, 0.05f * n);
        }
        tweener.RotateLocal(this.headerText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.goldCup, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.fruitCup, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.multiplayerWinnerCup, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.multiplayerLoserCup, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.counterTime, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.counterDeaths, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.totalCoinsBox, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.remainingFruitBox, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.3f);
        tweener.ScaleLocal(this.timerIcon, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.coinsParent, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.fruitGlasses, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.goldGlasses[0], Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.goldGlasses[1], Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.purpleGlasses, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.hatCrown, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);
        tweener.ScaleLocal(this.juice.GetComponent<RectTransform>(), Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.23f);

        Vector2 leaderboardPosition = this.leaderboardContainer.transform.localPosition;
        tweener.MoveLocal(this.leaderboardContainer, leaderboardPosition.Add(0, -100), 0.5f, Easing.QuadEaseIn, 0.3f);

        var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);

        Audio.instance.PlaySfx(Assets.instance.sfxUIPauseBubblesOut, options: new Audio.Options(1f, false, 0f));
        Audio.instance.PlaySfx(Assets.instance.sfxUIScreenwipeIn, options: new Audio.Options(1f, false, 0f));

        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleClosedAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }
        yield return null;
        yield return null;

        this.uiBehindTransform.gameObject.SetActive(false);
        this.uiStarsTransform.gameObject.SetActive(false);

        var canvas = this.transform.GetComponentInParent<Canvas>().gameObject;
        DontDestroyOnLoad(canvas);
        for (int n = 0; n < canvas.transform.childCount; n += 1)
        {
            var child = canvas.transform.GetChild(n).gameObject;
            if (child != this.gameObject)
                Destroy(child);
        }
        Destroy(canvas.GetComponent<IngameUI>());

        var minigameUI = canvas.GetComponent<minigame.MinigameUI>();
        if (minigameUI != null)
            Destroy(minigameUI);

        SceneManager.LoadScene(targetScene);
        yield return null;

        this.circleOpenAmount = 1;

        tweener = new Tweener();
        tweenForCircle = tweener.TweenFloat(1, 0, 0.5f, Easing.QuadEaseIn);
        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            this.circleClosedAmount = tweenForCircle.Value();
            UpdateCircleSize();
            yield return null;
        }

        Destroy(canvas.gameObject);
    }

    private void Update()
    {
        if (UnityEngine.Random.Range(0, 2) == 1)
        {
            var star = Instantiate(this.starPrefab, this.starsContainer);
            star.transform.localPosition = new Vector3(0, 0);
            var starImage = star.GetComponent<Image>();
            starImage.sprite = Util.RandomChoice(this.starSprites);
            starImage.SetNativeSize();

            StartCoroutine(StarAnimation());
            IEnumerator StarAnimation()
            {
                float angle = UnityEngine.Random.Range(0, Mathf.PI * 2);
                float speed = UnityEngine.Random.Range(1f, 2f);
                float spinSpeed = UnityEngine.Random.Range(-10, 10);
                for (int n = 0; n < 180; n += 1)
                {
                    star.transform.localPosition = new Vector2(
                        Mathf.Cos(angle) * n * speed,
                        Mathf.Sin(angle) * n * speed
                    );
                    star.transform.localRotation = Quaternion.Euler(0, 0, n * spinSpeed);
                    yield return null;
                }
                Destroy(star.gameObject);
            }
        }

        this.backShine1.Rotate(new Vector3(0, 0, -1));
        this.backShine2.Rotate(new Vector3(0, 0, -1.2f));
    }

    private void UpdateCircleSize()
    {
        float width, height;

        if (GameCamera.IsLandscape() == true)
            (width, height) = (700, 400);
        else
            (width, height) = (256, 256 * Screen.height / Screen.width);

        float diagonal = Mathf.Sqrt(width * width + height * height);
        this.circleImageTransform.sizeDelta = new Vector2(diagonal, diagonal);

        float circleStandardSize;
        if (GameCamera.IsLandscape())
            circleStandardSize = 300;
        else
            circleStandardSize = 200;

        float circleRadiusForMaterial = Mathf.LerpUnclamped(
            circleStandardSize / diagonal, 1.0f, this.circleOpenAmount
        ) * (1.0f - this.circleClosedAmount);
        float outlineThickness = circleStandardSize * 0.03f / diagonal;

        this.circleMaterial.SetFloat(Radius, circleRadiusForMaterial);
        this.circleMaterial.SetFloat(OutlineThickness, outlineThickness);

        float scale = circleStandardSize / 220.0f;
        this.uiTransform.localScale = new Vector3(scale, scale, 1);
        this.uiBehindTransform.localScale = new Vector3(scale, scale, 1);

        scale = circleRadiusForMaterial * diagonal / 220f;
        this.uiStarsTransform.localScale = new Vector3(scale, scale, 1);
    }

    public void CreateSmackParticle()
    {
        var smack = Instantiate(this.smackEffectPrefab, this.smackParent);
        smack.transform.localPosition = new Vector3(0, 0);
        var smackAnimated = smack.GetComponent<Animated>();
        smackAnimated.PlayOnce(smackEffect, () =>
        {
            Destroy(smack.gameObject);
        });

        StartCoroutine(CreateSparkles());
    }

    private IEnumerator CreateSparkles()
    {
        var tweener = new Tweener();

        for (int i = 0; i < 4; i++)
        {
            var sparkle = Instantiate(this.sparkleEffectPrefab, this.smackParent);
            int angle = UnityEngine.Random.Range(0, 360);

            sparkle.transform.localPosition = new Vector3(60 * Mathf.Cos(Mathf.Deg2Rad * angle), 60 * Mathf.Sin(Mathf.Deg2Rad * angle));
            var sparkleAnimated = sparkle.GetComponent<Animated>();
            sparkleAnimated.PlayOnce(sparkleEffect, () =>
            {
                Destroy(sparkle.gameObject);
            });

            tweener.MoveLocal(sparkle, new Vector3(70 * Mathf.Cos(Mathf.Deg2Rad * angle), 70 * Mathf.Sin(Mathf.Deg2Rad * angle)), 0.5f, Easing.Linear);
        }

        while (tweener.IsDone() == false)
        {
            tweener.Advance(Time.unscaledDeltaTime);
            yield return null;
        }
    }
    private void LateUpdate()
    {
        #if UNITY_EDITOR
            UpdateCircleSize();
        #endif

        //this.headerText.GetComponent<CircleText>()?.UpdateCurve();
        //this.totalCoinsBoxWordTotal.UpdateCurve();
        //this.totalCoinsBoxCountLarge.GetComponent<CircleText>()?.UpdateCurve();
        //this.totalCoinsBoxCountSmall.GetComponent<CircleText>()?.UpdateCurve();
    }
}
