
public struct XY
{
    public static readonly XY Zero = new XY(0, 0);

    public XY(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int x;
    public int y;

    public override string ToString() => $"xy({this.x}, {this.y})";
}
