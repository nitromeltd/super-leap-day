using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    [CreateAssetMenu(menuName = "Minigame/AssetLibrary")]
    public class AssetLibrary : ScriptableObject
    {

        [Header("Levels")]
        public TextAsset levelTest;
        [Header("Sprites")]
        public List<Sprite> sprites;
        public Dictionary<string, Sprite> spriteLookup;
        public List<SpriteArray> spriteArrays;
        public Dictionary<string, SpriteArray> spriteArrayLookup;
        [Header("Tilesets")]
        public List<TileTheme> tileThemes;
        public Dictionary<string, TileTheme> tileThemeLookup;
        [Header("Prefabs")]
        public List<GameObject> prefabs;
        public Dictionary<string, GameObject> prefabLookup;
        public List<PrefabArray> prefabArrays;
        public Dictionary<string, PrefabArray> prefabArrayLookup;
        [Header("Animations")]
        public List<AnimatedAnimation> animatedAnimations;
        public Dictionary<string, Animated.Animation> animationLookup;
        [Header("Audio")]
        public List<AudioClip> audioClips;
        public Dictionary<string, AudioClip> audioClipLookup;
        [Header("Autotiles")]
        public List<AutotileArray> autotileArrays;
        public Dictionary<string, AutotileArray> autotileArrayLookup;
        [Header("Curves")]
        public List<AnimationCurveName> animationCurves;
        public Dictionary<string, AnimationCurve> animationCurveLookup;
        [Header("Materials")]
        public List<Material> materials;
        public Dictionary<string, Material> materialLookup;
        [Header("Physics Materials")]
        public List<PhysicsMaterial2D> physicsMaterial2Ds;
        public Dictionary<string, PhysicsMaterial2D> physicsMaterial2DLookup;
        [Header("Fetch base asset")]
        public List<BaseAsset> baseNames;
        public Dictionary<string, string> baseNameLookup;

        [System.Serializable]
        public struct BaseAsset
        {
            public string name, target;
        }
        [System.Serializable]
        public class AnimatedAnimation
        {
            public string name;
            public Animated.Animation animation;
        }
        [System.Serializable]
        public class SpriteArray
        {
            public string name;
            public Sprite[] list;
        }
        [System.Serializable]
        public class PrefabArray
        {
            public string name;
            public GameObject[] list;
        }
        [System.Serializable]
        public class AutotileArray
        {
            public string name;
            public MinigameMap.Autotile[] list;
        }
        [System.Serializable]
        public class AnimationCurveName
        {
            public string name;
            public AnimationCurve animationCurve;
        }

        public void Init()
        {
            this.spriteLookup = new Dictionary<string, Sprite>();
            this.spriteArrayLookup = new Dictionary<string, SpriteArray>();
            this.tileThemeLookup = new Dictionary<string, TileTheme>();
            this.prefabLookup = new Dictionary<string, GameObject>();
            this.prefabArrayLookup = new Dictionary<string, PrefabArray>();
            this.materialLookup = new Dictionary<string, Material>();
            this.animationLookup = new Dictionary<string, Animated.Animation>();
            this.autotileArrayLookup = new Dictionary<string, AutotileArray>();
            this.animationCurveLookup = new Dictionary<string, AnimationCurve>();
            this.physicsMaterial2DLookup = new Dictionary<string, PhysicsMaterial2D>();
            this.audioClipLookup = new Dictionary<string, AudioClip>();
            this.baseNameLookup = new Dictionary<string, string>();
        }

        public void BuildLookup(AssetLibrary assetLibrary, string prefix = "")
        {
            foreach(var item in assetLibrary.sprites)
            {
                //Debug.Log(item.name);
                if(item != null) this.spriteLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.spriteArrays)
            {
                //Debug.Log(item.name);
                if(item != null) this.spriteArrayLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.tileThemes)
            {
                //Debug.Log(item.name);
                if(item != null) this.tileThemeLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.prefabs)
            {
                //Debug.Log(item.name);
                if(item != null) this.prefabLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.prefabArrays)
            {
                //Debug.Log(item.name);
                if(item != null) this.prefabArrayLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.materials)
            {
                //Debug.Log(item.name);
                if(item != null) this.materialLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.animatedAnimations)
            {
                //Debug.Log(item.name);
                if(item != null) this.animationLookup[prefix + item.name] = item.animation;
            }
            foreach(var item in assetLibrary.audioClips)
            {
                if(item != null) this.audioClipLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.autotileArrays)
            {
                //Debug.Log(item.name);
                if(item != null) this.autotileArrayLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.animationCurves)
            {
                if(item != null) this.animationCurveLookup[prefix + item.name] = item.animationCurve;
            }
            foreach(var item in assetLibrary.physicsMaterial2Ds)
            {
                //Debug.Log(item.name);
                if(item != null) this.physicsMaterial2DLookup[prefix + item.name] = item;
            }
            foreach(var item in assetLibrary.baseNames)
            {
                //Debug.Log(item.name);
                this.baseNameLookup[prefix + item.name] = prefix + item.target;
            }
        }


    }
}
