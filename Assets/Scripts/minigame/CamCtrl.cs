using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    public class CamCtrl
    {
        public LookAtTween lookAtTween;
        public Tween zoomTween;
        public float zoomTarget;
        public Rect rect;
        public Camera cam;
        public Minigame game;
        public Rect rectDefault;
        public float scale;
        public Vector2 shakeOffset;
        public float floorOffsetY;
        public Vector2 clampTolerance;
        public ClampAction clampAction;
        public delegate Vector3 ClampAction(Vector3 position);

        private Queue<LookAtTween> lookAtTweenQueue;
        private List<Shake> shakes;

        /* Standalone PC test sizes:
         * 896x504 (16:9)
         * 375x812 (iPhoneX/3)
         */

        public CamCtrl(Camera cam, Minigame game)
        {
            this.cam = cam;
            this.game = game;
            float halfScaledWidth = Minigame.ScreenWidth / 2;
            float aspectRatio = (float)(Screen.height) / (float)(Screen.width);
            float halfScaledHeight = halfScaledWidth * aspectRatio; // this is the minimum orthographicSize
            float screenHeight = this.cam.orthographicSize = halfScaledHeight;
            this.rectDefault = this.rect = new Rect(Vector2.zero, new Vector2(Minigame.ScreenWidth, screenHeight * 2));
            //Debug.Log("camera size " + this.rect.size);
            this.lookAtTweenQueue = new Queue<LookAtTween>();
            this.clampAction = Clamp;// Clamp() or ClampFloor
            this.clampTolerance = Vector2.one * Minigame.Scale;
            this.shakeOffset = Vector2.zero;
            this.shakes = new List<Shake>();
        }

        public void Clear()
        {
            this.lookAtTweenQueue.Clear();
            this.shakes.Clear();
        }

        public void SetZoom(float scale)
        {
            this.rect.size = this.rectDefault.size * scale;
            this.cam.orthographicSize = this.rect.size.y / 2;
            this.zoomTarget = scale;
        }

        public void SetZoomTween(float targetScale, float delay, float quad = 0, float pause = 0)
        {
            this.zoomTarget = targetScale;
            this.zoomTween = new Tween(this.rect.size, this.rectDefault.size * targetScale, delay, quad, pause);
        }

        public void SetPosition(Vector3 target, Vector3 offset)
        {
            this.cam.transform.position = this.clampAction(target + offset);
            if(this.shakeOffset != Vector2.zero)
            {
                // have to reapply clamp as the shake can get swallowed off-screen
                this.cam.transform.position = this.clampAction(this.cam.transform.position + (Vector3)this.shakeOffset);
            }
        }

        public void UpdateZoomTween(float t)
        {
            if(this.zoomTween != null)
            {
                this.zoomTween.Advance(t);
                rect.size = zoomTween.Position();
                cam.orthographicSize = rect.size.y / 2;
                if(this.zoomTween.Done() == true)
                    this.zoomTween = null;

            }
        }

        public float ZoomScale()
        {
            return rect.size.y / rectDefault.size.y;
        }

        // move target inside levelRect
        public Vector3 Clamp(Vector3 target)
        {
            if(this.game.levelRect.width - this.clampTolerance.x <= this.rect.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + this.rect.width * 0.5f, this.game.levelRect.xMax - this.rect.width * 0.5f);
            if(this.game.levelRect.height - this.clampTolerance.y <= this.rect.height)
                target.y = this.game.levelRect.center.y;
            else
                target.y = Mathf.Clamp(target.y, this.game.levelRect.yMin + this.rect.height * 0.5f, this.game.levelRect.yMax - this.rect.height * 0.5f);
            target.z = -10;
            return target;
        }

        // move target inside levelRect with rectDefault * scale
        public Vector3 Clamp(Vector3 target, float scale)
        {
            var r = this.rectDefault;
            r.size *= scale;
            
            if(this.game.levelRect.width - this.clampTolerance.x <= r.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + r.width * 0.5f, this.game.levelRect.xMax - r.width * 0.5f);
            
            if(this.game.levelRect.height - this.clampTolerance.y <= r.height)
                target.y = this.game.levelRect.center.y;
            else
                target.y = Mathf.Clamp(target.y, this.game.levelRect.yMin + r.height * 0.5f, this.game.levelRect.yMax - r.height * 0.5f);
            
            target.z = -10;
            return target;
        }

        // move target.x inside levelRect, target.y above floor
        public Vector3 ClampFloor(Vector3 target)
        {
            if(this.game.levelRect.width - this.clampTolerance.x <= this.rect.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + this.rect.width * 0.5f, this.game.levelRect.xMax - this.rect.width * 0.5f);

            float minY = this.game.levelRect.yMin + floorOffsetY + this.rect.height * 0.5f;
            if(target.y < minY)
                target.y = minY;

            target.z = -10;
            return target;
        }

        // move target,x inside levelRect with rectDefault * scale, target.y above floor
        public Vector3 ClampFloor(Vector3 target, float scale)
        {
            var r = this.rectDefault;
            r.size *= scale;
            
            if(this.game.levelRect.width - this.clampTolerance.x <= r.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + r.width * 0.5f, this.game.levelRect.xMax - r.width * 0.5f);

            float minY = this.game.levelRect.yMin + floorOffsetY + r.height * 0.5f;
            if(target.y < minY)
                target.y = minY;

            target.z = -10;
            return target;
        }

        // move target inside levelRect width
        public Vector3 ClampX(Vector3 target)
        {
            if(this.game.levelRect.width - this.clampTolerance.x <= this.rect.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + this.rect.width * 0.5f, this.game.levelRect.xMax - this.rect.width * 0.5f);
            target.z = -10;
            return target;
        }

        // move target inside levelRect width with rectDefault * scale
        public Vector3 ClampX(Vector3 target, float scale)
        {
            var r = this.rectDefault;
            r.size *= scale;

            if(this.game.levelRect.width - this.clampTolerance.x <= r.width)
                target.x = this.game.levelRect.center.x;
            else
                target.x = Mathf.Clamp(target.x, this.game.levelRect.xMin + r.width * 0.5f, this.game.levelRect.xMax - r.width * 0.5f);

            target.z = -10;
            return target;
        }

        public Shake AddShake(Vector2 normal, float wavelength, float duration, float magnitude)
        {
            var s = new Shake(normal, wavelength, duration, magnitude);
            this.shakes.Add(s);
            return s; 
        }

        public void AdvanceShakes(float t)
        {
            shakeOffset = Vector2.zero;
            for(int i = shakes.Count - 1; i > -1; i--)
            {
                var s = shakes[i];
                s.Advance(t);
                if(s.time == s.duration)
                {
                    shakes.RemoveAt(i);
                }
                else
                {
                    shakeOffset += s.Position();
                }
            }
        }

        public bool OnScreen(Vector2 p, Vector2 border)
        {
            var r = this.rect;
            r.size += border * 2;
            r.center = this.cam.transform.position;
            return r.Contains(p);
        }

        public void PauseAndPanTo(Vector2 dest, float startDelay, float panDelayPerDist, float endDelay, Action finishAction)
        {
            var begin = this.cam.transform.localPosition;
            var end = clampAction(dest);
            var dist = (end - begin).magnitude;
            // pause at start
            AddLookAtTween(new LookAtTween(this, begin, begin, null, 0f, startDelay, false));
            // pan to start
            AddLookAtTween(new LookAtTween(this, begin, end, null, Mathf.Max(panDelayPerDist * dist, 0.25f), endDelay, false, finishAction));
        }

        public void UpdateLookAt(float t)
        {
            this.lookAtTween.Advance(t);
            if(this.lookAtTween.Done() == true)
            {
                if(this.lookAtTweenQueue.Count > 0)
                {
                    this.lookAtTween = this.lookAtTweenQueue.Dequeue();
                }
                else
                {
                    this.lookAtTween = null;
                }
            }
        }

        public void AddLookAtTween(LookAtTween lookAtTween)
        {
            if(this.lookAtTween == null)
            {
                this.lookAtTween = lookAtTween;
            }
            else
            {
                this.lookAtTweenQueue.Enqueue(lookAtTween);
            }
        }

        public void TweenZoom(Vector2 size, float delay, float quadPerc = 0, float pause = 0)
        {
            this.zoomTween = new Tween(this.rect.size, size, delay, quadPerc, pause);
        }

        public IEnumerator TweenTo(Vector3 target, Vector3 offset, float duration, Tween.Ease ease, Action onUpdate = null)
        {
            var tweener = new Tweener();
            tweener.Move(this.cam.transform, this.clampAction(target + offset), duration, ease);
            while(tweener.IsDone() == false)
            {
                if(Minigame.paused == false)
                {
                    tweener.Advance(1.0f / 60);
                    onUpdate?.Invoke();
                }
                yield return null;
            }
        }

        // Manages camera panning events with an update-till-complete function in the middle
        public class LookAtTween
        {
            public State state;
            public Func<bool> isLookCompleteFunc;// Action returns bool
            public Action tweenDoneAction;
            public Action lookStartAction;
            public Action lookFinishAction;
            public Tween tweenIn, tweenOut;
            public CamCtrl camCtrl;
            public float waitCount;

            public LookAtTween(CamCtrl camCtrl, Vector3 start, Vector3 finish, Func<bool> isLookCompleteFunc, float tweenDelay, float waitDelay = 0, bool inOut = true, Action lookDoneAction = null)
            {
                this.camCtrl = camCtrl;
                this.isLookCompleteFunc = isLookCompleteFunc;
                this.tweenDoneAction = lookDoneAction;
                this.waitCount = waitDelay;
                this.tweenIn = new Tween(start, finish, tweenDelay, Easing.QuadEaseInOut);
                // we might want to chain tweens to look at a sequence of things,
                // or just move the camera to a new location
                if(inOut)
                {
                    this.tweenOut = new Tween(finish, start, tweenDelay, Easing.QuadEaseInOut);
                }
                this.state = State.IN;
            }

            public LookAtTween(CamCtrl camCtrl, Tween tweenIn, Tween tweenOut = null, Func<bool> isLookCompleteFunc = null, float waitDelay = 0, Action lookDoneAction = null)
            {
                this.camCtrl = camCtrl;
                this.isLookCompleteFunc = isLookCompleteFunc;
                this.tweenDoneAction = lookDoneAction;
                this.waitCount = waitDelay;
                this.tweenIn = tweenIn;
                this.tweenOut = tweenOut;
                this.state = State.IN;
            }

            public enum State
            {
                NONE, IN, LOOK, OUT, WAIT, DONE
            }

            public void Advance(float t)
            {
                switch(this.state)
                {
                    case State.IN:
                        this.tweenIn.Advance(t);
                        this.camCtrl.cam.transform.localPosition = this.camCtrl.clampAction(this.tweenIn.Position());
                        if(this.tweenIn.Done() == true)
                        {
                            if(this.isLookCompleteFunc != null)
                            {
                                this.state = State.LOOK;
                                this.lookStartAction?.Invoke();
                            }
                            else if(this.tweenOut != null)
                            {
                                this.state = State.OUT;
                            }
                            else if(this.waitCount > 0)
                            {
                                this.state = State.WAIT;
                            }
                            else
                            {
                                goto case State.DONE;
                            }
                        }
                        break;
                    case State.LOOK:
                        if(this.isLookCompleteFunc() == true)
                        {
                            this.lookFinishAction?.Invoke();

                            if(this.tweenOut != null)
                            {
                                this.state = State.OUT;
                            }
                            else if(this.waitCount > 0)
                            {
                                this.state = State.WAIT;
                            }
                            else
                            {
                                goto case State.DONE;
                            }
                        }
                        break;
                    case State.OUT:
                        this.tweenOut.Advance(t);
                        this.camCtrl.cam.transform.localPosition = this.camCtrl.clampAction(this.tweenOut.Position());
                        if(this.tweenOut.Done() == true)
                        {
                            if(this.waitCount > 0)
                            {
                                this.state = State.WAIT;
                            }
                            else
                            {
                                goto case State.DONE;
                            }
                        }
                        break;
                    case State.WAIT:
                        this.waitCount -= t;
                        if(this.waitCount <= 0)
                        {
                            goto case State.DONE;
                        }
                        break;
                    case State.DONE:
                        if(this.state != State.DONE)
                        {
                            this.tweenDoneAction?.Invoke();
                            this.state = State.DONE;
                        }
                        break;
                }
            }

            public bool Done(){
                return this.state == State.DONE;
            }
        }
        
    }

}
