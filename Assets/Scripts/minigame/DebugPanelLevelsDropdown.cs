using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace minigame
{
    public class DebugPanelLevelsDropdown : MonoBehaviour
    {

        public TextAsset[] testLevels;
        public Minigame.Type typeMinigame;

        public Dropdown dropdown;

        private TextAsset[] levels;

        public static int index = 0;

        // Start() won't get called until DebugPanel is opened so
        // buffer is cleared when scene change is detected
        private static TextAsset _level;
        private static string sceneName;
        public static TextAsset level
        {
            set
            {
                _level = value;
                sceneName = SceneManager.GetActiveScene().name;
            }
            get
            {
                if(SceneManager.GetActiveScene().name != sceneName)
                    _level = null;
                return _level;
            }
        }


        private void Start()
        {
            var options = new List<Dropdown.OptionData>();
            var list = new List<TextAsset>(testLevels);

            var difficulties = (MinigameDifficulty[])System.Enum.GetValues(typeof(MinigameDifficulty));
            for(int i = 0; i < difficulties.Length; i ++)
                for(int j = 0; j < 5; j++)
                {
                    var item = Levels.GetLevelFile(typeMinigame, (MinigameDifficulty)i, j);
                    if(item != null)
                        list.Add(item);
                }

            foreach(var f in list)
                options.Add(new Dropdown.OptionData(f.name));

            this.levels = list.ToArray();
            this.dropdown.options = options;
            this.dropdown.value = Mathf.Min(index, options.Count - 1);
        }

        public void OnClickLoad()
        {
            index = this.dropdown.value;
            level = this.levels[index];
            Minigame.testingLevel = null;
            var objs = UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects();
            foreach(var obj in objs)
            {
                var minigame = obj.GetComponent<Minigame>();
                if(minigame != null)
                {
                    minigame.ExitGame();
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
                    break;
                }
            }
        }
    }

}
