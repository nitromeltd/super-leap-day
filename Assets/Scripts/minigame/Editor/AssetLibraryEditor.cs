using System.Collections;
using System.Collections.Generic;
using minigame;
using UnityEngine;
using UnityEditor;

// Batch operations on AssetLibrary
// Keeping this skeleton around in case we need to perform some further tasks
[CustomEditor(typeof(AssetLibrary), true)]
public class AssetLibraryEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
       
        AssetLibrary script = (AssetLibrary)target;

        /*
        if(GUILayout.Button("Do it!"))
        {
        } */

    }
}
