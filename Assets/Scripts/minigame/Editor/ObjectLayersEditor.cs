
using minigame;
using UnityEditor;
using UnityEngine;


[CustomEditor(typeof(ObjectLayers), true)]
public class ObjectLayersEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ObjectLayers script = (ObjectLayers)target;
        GUILayout.Space(10);
        // To create a selection box like SpriteRenderer has for sorting layers,
        // You have to create it from scratch.
        EditorGUILayout.LabelField("Sprite Sorting Layers", EditorStyles.boldLabel);
        string[] sortingLayerNames = new string[SortingLayer.layers.Length + 1];
        for(int i = 0; i < SortingLayer.layers.Length + 1; i++)
        {
            if(i == SortingLayer.layers.Length)
            {
                // we want to add an extra item that redirects to the TagManager window
                sortingLayerNames[i] = "Add Sorting Layer...";
            }
            else
            {
                sortingLayerNames[i] = SortingLayer.layers[i].name;
            }
        }
        if(sortingLayerNames.Length == 0)
        {
            EditorGUILayout.HelpBox("There are no Sorting Layers.", MessageType.Error);
        }
        else if(sortingLayerNames != null)
        {
            script.backgroundSortingLayer = SortingInspector("0: Background", script.backgroundSortingLayer, sortingLayerNames);
            script.tilesSortingLayer = SortingInspector("1: Tiles", script.tilesSortingLayer, sortingLayerNames);
            script.objectsSortingLayer = SortingInspector("2: Objects", script.objectsSortingLayer, sortingLayerNames);
            script.playerSortingLayer = SortingInspector("3: Player", script.playerSortingLayer, sortingLayerNames);
            script.foregroundSortingLayer = SortingInspector("4: Foreground", script.foregroundSortingLayer, sortingLayerNames);
            script.fxSortingLayer = SortingInspector("5: FX", script.fxSortingLayer, sortingLayerNames);
            script.uiSortingLayer = SortingInspector("6: UI", script.uiSortingLayer, sortingLayerNames);

        }
    }

    public string SortingInspector(string label, string oldName, string[] sortingLayerNames)
    {
        // Use the name to look up our array index into the names list
        int oldLayerIndex = -1;
        for(int a = 0; a < sortingLayerNames.Length; a++)
        {
            if(sortingLayerNames[a].Equals(oldName))
            {
                oldLayerIndex = a;
            }
        }
        // Show the popup for the names
        int newLayerIndex = EditorGUILayout.Popup(label, oldLayerIndex, sortingLayerNames);
        // Open the TagManager
        if(newLayerIndex == sortingLayerNames.Length - 1)
        {
            var asset = AssetDatabase.LoadMainAssetAtPath("ProjectSettings/TagManager.asset");
            Selection.activeObject = asset;
            // If the index changes, look up the ID for the new index to store as the new ID
        }
        else if(newLayerIndex != oldLayerIndex)
        {
            return sortingLayerNames[newLayerIndex];
        }
        return oldName;
    }
}