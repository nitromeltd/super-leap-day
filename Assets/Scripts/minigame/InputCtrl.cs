using UnityEngine;

namespace minigame
{
    public class InputCtrl : MonoBehaviour
    {

        public Camera cam;
        public Minigame game;

        public Touch? touch;
        public Vector2 position;
        public Vector2 positionDown;
        public Vector2 positionUp;
        public bool holding;
        public bool pressed;
        public bool released;

        public void Advance()
        {
            GameInput.Advance();

            if
            (
                Minigame.paused ||
                GameInput.holdingMenu ||
                IngameUI.IsPositionOverUI(Input.mousePosition) == true
            )
            {
                this.holding = this.pressed = this.released = false;
                return;
            }

            if(GameInput.Player(1).pressedJump == true) this.pressed = true;
            if(GameInput.Player(1).releasedJump == true) this.released = true;
            this.holding = GameInput.Player(1).holdingJump;


            if(Input.touchCount > 0)
                this.touch = Input.GetTouch(0);
            else
                this.touch = null;

            if(touch.HasValue)
                this.position = cam.ScreenToWorldPoint(new Vector3(touch.Value.position.x, touch.Value.position.y));
            else
                this.position = cam.ScreenToWorldPoint(Input.mousePosition);

            if(Input.GetMouseButtonDown(0) || (touch.HasValue && touch.Value.phase == TouchPhase.Began))
                this.positionDown = position;

            if(Input.GetMouseButtonUp(0) || (touch.HasValue && touch.Value.phase == TouchPhase.Ended))
                this.positionUp = position;
        }

        // Call at end of FixedUpdate to clear input
        public void FixedAdvance()
        {
            this.pressed = false;
            this.released = false;
        }
    }

}
