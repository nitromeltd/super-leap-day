﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    public class Launcher : MonoBehaviour
    {
        public Transform transformPlunger;
        public Transform transformPivot;
        public Transform transformCounterRotate;
        public Rigidbody2D rigidBody2DBall;
        public Shake shakeLaunch;
        public float power;
        public bool throb;// perform scaling animation
        public SpriteRenderer spriteRendererNozzleBottom;// uses different sorting layer
        public Animated animNozzleTop;
        public Animated animNozzleBottom;
        public Animated.Animation animationShootTop;
        public Animated.Animation animationShootBottom;
        [System.NonSerialized] public int rank;// preference when used as checkpoint
        [System.NonSerialized] public Vector2 posBall;// tracking ball passing checkpoint
        [System.NonSerialized] public float distToFinish;// distance to finish area (used in Rolling Day)
        [System.NonSerialized] public bool wait;// wait for game
        [System.NonSerialized] public float throbStart;// time throb started

        public Vector2 flexPositive = new Vector2(1f, -1f);
        public Vector2 flexNegative = new Vector2(-1f, 1f);

        public const float OffsetPlungerMax = 1f;


        // n >= 0 && n <= 1
        public void SetPower(float n)
        {
            this.power = n;
            this.transformPlunger.localPosition = Vector3.left * OffsetPlungerMax * n;
        }

        public void Load(Rigidbody2D rigidBody2DBall)
        {
            this.rigidBody2DBall = rigidBody2DBall;
            this.throb = true;
            this.throbStart = Minigame.fixedDeltaTimeElapsed;
        }

        public void SetPower(float value, float min, float max)
        {
            this.power = (value - min) / (max - min);
            this.transformPlunger.localPosition = Vector3.left * OffsetPlungerMax * this.power;
        }

        public void SetRotation(NitromeEditor.TileLayer.Tile tile)
        {
            SetRotation(tile.rotation + (tile.flip ? 135 : 45));
        }

        public void SetRotation(float angle)
        {
            this.transform.localRotation = Quaternion.Euler(0, 0, angle);
            this.transformCounterRotate.localRotation = Quaternion.Euler(0, 0, -angle);
            this.transformPivot.localRotation = Quaternion.Euler(0, 0, angle);
        }

        private void FixedUpdate()
        {
            Advance();
        }

        public virtual void Advance()
        {
            if(this.shakeLaunch != null)
            {
                this.shakeLaunch.Advance(Time.fixedDeltaTime / Time.timeScale);

                this.transform.localScale = Vector3.one + (Vector3)this.shakeLaunch.Flex(flexNegative);
                if(this.shakeLaunch.time >= this.shakeLaunch.duration)
                {
                    this.transform.localScale = Vector3.one;
                    this.shakeLaunch = null;
                }
            }
            else if(this.rigidBody2DBall != null)
            {
                if(this.wait == true)
                    this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector2.one, 0.1f);
                else if(this.throb == true)
                    this.transform.localScale =
                        Vector3.one + (Vector3)Shake.WaveFlex(flexPositive, flexNegative, Minigame.fixedDeltaTimeElapsed - this.throbStart, 1f, 0.5f);
                else
                    this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector2.one + flexNegative * this.power * 0.125f, 0.1f);
            }
        }

        public void Launch(float power, float min, float max)
        {
            SetPower(0);
            LaunchThrust(power);
            this.power = (power - min) / (max - min);
            this.shakeLaunch = new Shake(flexPositive, 0.1f + 0.2f / this.power, 1.2f, 0.1f + 0.65f * this.power);
            this.transform.localScale = Vector3.one;
            animNozzleTop.PlayAndHoldLastFrame(animationShootTop);
            animNozzleBottom.PlayAndHoldLastFrame(animationShootBottom);
            StartCoroutine(ScaleLaunched(this.rigidBody2DBall.transform, 0.25f, 1f, 0.25f));
            this.rigidBody2DBall = null;
            this.throb = false;
           // Debug.Break();
        }

        public virtual void LaunchThrust(float power)
        {
            Vector2 launchVector = this.transformPivot.localRotation * Vector2.right;
            this.rigidBody2DBall.velocity += launchVector * power;
        }

        IEnumerator ScaleLaunched(Transform launched, float start, float finish, float duration)
        {
            float t = 0;
            float len = finish - start;
            while(t < finish)
            {
                t += Time.deltaTime * Time.timeScale / duration;
                launched.localScale = Vector3.one * (start + t * len);
                yield return null;
            }
            launched.localScale = Vector3.one * finish;
        }
    }
}
