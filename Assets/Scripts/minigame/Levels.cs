using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using Type = minigame.Minigame.Type;

namespace minigame
{
    [CreateAssetMenu(menuName = "Minigame/Levels")]
    public class Levels : ScriptableObject
    {
        public static TextAsset[][][] levels;
        public static Difficulty[][] difficulties;

        [Header("Golf")]
        public Difficulty[] difficultiesGolf;
        [Header("Pinball")]
        public Difficulty[] difficultiesPinball;
        [Header("Racing")]
        public Difficulty[] difficultiesRacing;
        [Header("Fishing")]
        public Difficulty[] difficultiesFishing;
        [Header("Rolling")]
        public Difficulty[] difficultiesRolling;
        [Header("Abseiling")]
        public Difficulty[] difficultiesAbseiling;


        [System.Serializable]
        public struct Difficulty
        {
            public TextAsset[] levels;
        }

        public static void Init()
        {
            if(levels != null) return;// already initialised

            Levels data = Resources.Load<Levels>("Minigame Levels");

            difficulties = new Difficulty[][]
            {
                data.difficultiesGolf,
                data.difficultiesPinball,
                data.difficultiesRacing,
                data.difficultiesFishing,
                data.difficultiesRolling,
                data.difficultiesAbseiling
            };
            levels = new TextAsset[difficulties.Length][][];

            for(int i = 0; i < levels.Length; i++)
            {
                var str = PlayerPrefs.GetString(((Type)i).ToString() + "completedLevels", "");
                var diffStrs = str.Split('|');
                var diff = Levels.difficulties[i];
                var difficulties = new TextAsset[diff.Length][];
                for(int j = 0; j < difficulties.Length; j++)
                {
                    var levelStrs = j < diffStrs.Length ? diffStrs[j].Split(',') : new string[0];
                    var levels = diff[j].levels;
                    var completedLevels = new int[levels.Length];
                    for(int k = 0; k < completedLevels.Length; k++)
                    {
                        if(k < levelStrs.Length)
                        {
                            int.TryParse(levelStrs[k], out int value);
                            completedLevels[k] = value;
                        }
                        else
                            completedLevels[k] = 0;
                    }
                    difficulties[j] = levels;
                }
                levels[i] = difficulties;
            }
        }

        public static TextAsset GetLevelFile(Type type, MinigameDifficulty difficulty, int level)
        {
            if(levels == null) Init();

            if((int)type < levels.Length)
                if((int)difficulty < levels[(int)type].Length)
                    if(level < levels[(int)type][(int)difficulty].Length)
                        return levels[(int)type][(int)difficulty][level];
            return null;
        }

        public static void GetNext(Type type, ref MinigameDifficulty difficulty, ref int level)
        {
            int t = (int)type;
            if(t < levels.Length)
                if((int)difficulty < levels[t].Length)
                    if(level < levels[t][(int)difficulty].Length)
                    {
                        if(level + 1 >= levels[t][(int)difficulty].Length)
                        {
                            if((int)difficulty + 1 >= levels[t].Length)
                                difficulty = 0;
                            else
                                difficulty++;
                            level = 0;
                        }
                        else
                            level++;
                    }
        }

        public static void GetPrev(Type type, ref MinigameDifficulty difficulty, ref int level)
        {
            int t = (int)type;
            if(t < levels.Length)
                if((int)difficulty < levels[t].Length)
                    if(level < levels[t][(int)difficulty].Length)
                    {
                        if(level - 1 < 0)
                        {
                            if(difficulty - 1 < 0)
                                difficulty = (MinigameDifficulty)(levels[t].Length - 1);
                            else
                                difficulty--;
                            level = levels[t][(int)difficulty].Length - 1;
                        }
                        else
                            level--;
                    }
        }

        public static void GetNextLevelNotCompleted(Type type, ref MinigameDifficulty difficulty, ref int level)
        {
            if(levels == null) Init();

            int t = (int)type;
            var difficulties = SaveData.MinigameDifficultiesPurchased(type);

            // find the next uncompleted level to play
            for(int i = 0; i < difficulties.Length; i++)
            {
                var dif = (MinigameDifficulty)((i + (int)difficulty) % difficulties.Length);
                if(SaveData.HasCompletedMinigameDifficulty(type, dif))
                    continue;

                for(int j = 0; j < levels[t][(int)dif].Length; j++)
                {
                    var lev = (i + (int)dif) % levels[t][(int)dif].Length;
                    if(dif == difficulty && lev == level)
                        continue;

                    if(SaveData.IsMinigameCompleted(type, dif, lev) == false)
                    {
                        (difficulty, level) = (dif, lev);
                        return;
                    }
                }
            }
            // all completed, select the next level in sequence
            if(level + 1 >= levels[t][(int)difficulty].Length)
            {
                if((int)difficulty + 1 >= difficulties.Length)
                    difficulty = 0;
                else
                    difficulty++;
                level = 0;
            }
            else
                level++;
        }

        public static void GetTrophies(Type type, out int? trophy, out int medals)
        {
            if(levels == null) Init();

            trophy = null;
            medals = 0;
            for(int i = 0; i < difficulties[(int)type].Length; i++)
            {
                var dif = (MinigameDifficulty)(i);

                if(SaveData.HasCompletedMinigameDifficulty(type, dif) == true)
                {
                    trophy = i;
                    medals = 0;
                }
                else
                {
                    // you only have medals inbetween trophies
                    for(int j = 0; j < levels[(int)type][(int)dif].Length - 1; j++)
                    {
                        if(SaveData.IsMinigameCompleted(type, dif, j) == true)
                            medals++;
                        else
                            break;
                    }
                    break;
                }
            }
        }


    }

}
