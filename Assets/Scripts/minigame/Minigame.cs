using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace minigame
{

    /* Base class for mini games. It is assumed that most mini games will have these vars in common.
     */
    public partial class Minigame : MonoBehaviour
    {
        public enum Type { Golf, Pinball, Racing, Fishing, Rolling, Abseiling }
        public enum Mode { BonusLift, Arcade }

        [Header("Minigame")]
        public static int level;
        public static MinigameDifficulty difficulty;
        public static Mode mode;

        public MinigameMap map;
        public GameObject playerObj;
        public Camera cam;
        public CamCtrl camCtrl;
        public Canvas canvas;
        public RetryMenu retryMenu;
        public WinMenu winMenu;
        public CanvasScaler canvasScaler;
        public InputCtrl input;
        public AssetLibrary assets;
        public AssetLibrary assetsGlobal;
        public ObjectLayers objectLayers;// defines layers we're using for minigames
        public Levels levels;
        public PlayerAssetsForMinigame playerAssets;
        public Character character;
        public SpriteRendererPool spriteRendererPool;
        public Rect levelRect;
        public RectInt levelRectInt;
        public RectTransform safeArea;
        public MinigameInputTypePrompt minigameInputTypePrompt;
        public List<Motor> motors;
        public Dictionary<Vector2Int, int> spikesLookup;
        public Dictionary<Vector2Int, List<Rect>> spikesOffsetLookup;
        public PickupMap pickupMap;
        public List<Coroutine> coroutines;

        public AudioClip music;
        public AudioClip musicTransition;

        [NonSerialized] public TextAsset fileLevel;
        private float timeScalePrePause;

        public const string GlobalPrefix = "GLOBAL:";
        public const string FilledTile = GlobalPrefix + "filled";
        public const string PathsLayer = "Paths";
        public const string ConnectionsLayer = "Connections";

        public static float CamChangeInterpolation = 0.1f;
        // Update in FixedUpdate()
        public static float fixedDeltaTimeElapsed;
        public static int frameCount;
        public static bool paused;

        public static Color[] ConfettiColors = new Color[]
        {
            new Color32(255, 138, 0, 255)// orange
            ,new Color32(122, 212, 42, 255)// green
            ,new Color32(254, 247, 52, 255)// yellow
            ,new Color32(204, 30, 161, 255)// magenta
            ,new Color32(251, 4, 4, 255)// red
            ,new Color32(136, 242, 244, 255)// cyan
            ,new Color32(119, 1, 244, 255)// violet
        };
        public static Dictionary<System.Type, Type> TypeToType = new Dictionary<System.Type, Type>
        {
            [typeof(golf.GolfGame)] = Type.Golf
            , [typeof(racing.RacingGame)] = Type.Racing
            , [typeof(pinball.PinballGame)] = Type.Pinball
            , [typeof(fishing.FishingGame)] = Type.Fishing
            , [typeof(rolling.RollingGame)] = Type.Rolling
            , [typeof(abseiling.AbseilingGame)] = Type.Abseiling
        };

        public static string RetrySubtitle = "whoops";
        public static NitromeEditor.Level testingLevel;

        public static float Scale = 1;
        public static float EditorScale = 16;
        public static float PixelsPerUnit = 70;
        public static Vector2 ScaleOne = new Vector2(Scale, Scale);
        public static Vector2 ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
        public const float ScreenWidthPortrait = 16f;
        public const float ScreenWidthLandscape = 34.66f;
        public static float AspectRatio = 1334f / 750f;// iphone5
        public const float WidthPortraitDefault = 750f;
        public static float ScreenWidth = 16;
        public static float ScreenHeight = 16;
        public static Audio.Options NoRandomPitch = new Audio.Options(1f, false);
        public static Vector2 SpeedDebrisMin = new Vector2(-0.05f, 0.3f);
        public static Vector2 SpeedDebrisMax = new Vector2(0.05f, 0.4f);

        // set up dictionaries and lists - called by all games
        public void Init()
        {
            Game.InitScene();

            if(mode == Mode.Arcade)
            {
                Map.detailsToPersist = new Map.DetailsToPersist
                {
                    returnDate = DateTime.Now.Date,
                    fruitCollected = 0,
                    fruitCurrent = 0,
                    coins = SaveData.GetCoins(),
                    silverCoins = 0
                };
            }
            else if (Map.detailsToPersist == null)
            {
                Map.detailsToPersist = new Map.DetailsToPersist
                {
                    returnDate = DateTime.Now.Date,
                    fruitCollected = 20,
                    fruitCurrent = 20,
                    coins = 20,
                    silverCoins = 5
                };
            }

            character = SaveData.GetSelectedCharacter();
            //character = Character.King;

            //Debug.Log("Init " + GetType().ToString());
            // init ui scale
            AspectRatio = (float)Screen.width / Screen.height;
            if(AspectRatio < 1f)
            {
                this.canvasScaler.scaleFactor = Screen.width / WidthPortraitDefault;
                Minigame.ScreenWidth = Minigame.ScreenWidthPortrait;
            }
            else
            {
                this.canvasScaler.scaleFactor = (Screen.width / WidthPortraitDefault) / (ScreenWidthLandscape / ScreenWidthPortrait);
                Minigame.ScreenWidth = Minigame.ScreenWidthLandscape;
            }
            Minigame.ScreenHeight = Minigame.ScreenWidth / AspectRatio;

            this.safeArea.anchorMin = new Vector2(
                Screen.safeArea.min.x / Screen.width,
                Screen.safeArea.min.y / Screen.height
            );
            this.safeArea.anchorMax = new Vector2(
                Screen.safeArea.max.x / Screen.width,
                Screen.safeArea.max.y / Screen.height
            );

            // debugging safeArea assets
            //this.safeArea.anchorMin = new Vector2(0.1f, 0.1f);
            //this.safeArea.anchorMax = new Vector2(0.9f, 0.9f);

            this.assets.Init();
            this.assets.BuildLookup(this.assets);
            this.assets.BuildLookup(this.assetsGlobal, GlobalPrefix);
            // some polygons are better as adjustable templates
            this.assets.prefabArrayLookup.TryGetValue(GlobalPrefix + "polygon_templates", out AssetLibrary.PrefabArray fabArray);
            if(fabArray != null)
                TileGrid.AddPathFromShapeTemplates(fabArray.list);
            TileTexture.SpritesDefaultMaterial = this.assets.materialLookup[Minigame.GlobalPrefix + "Sprites-Default"];
            this.camCtrl = new CamCtrl(cam, this);
            //this.debris = new List<Debris>();
            this.motors = new List<Motor>();
            this.coroutines = new List<Coroutine>();
            this.spikesLookup = new Dictionary<Vector2Int, int>();
            this.spikesOffsetLookup = new Dictionary<Vector2Int, List<Rect>>();
            this.pickupMap = new PickupMap(this);

            Minigame.paused = false;

            Audio.instance.PlayMusic(this.music);
        }


        // override to define new game init
        public virtual void Restart(bool hard)
        {
            if(hard == true)
            {
                testingLevel = null;
            }
            foreach(var c in this.coroutines)
                StopCoroutine(c);
            this.coroutines.Clear();
            this.motors.Clear();
            this.spikesLookup.Clear();
            this.spikesOffsetLookup.Clear();
            this.pickupMap.Clear();
            this.spriteRendererPool.Clear();
            this.camCtrl.Clear();
            fixedDeltaTimeElapsed = 0f;
            frameCount = 0;
        }

        // override to release memory (eg: Texture2Ds)
        public virtual void ExitGame()
        {
            // transfer funds in Arcade
            if(mode == Mode.Arcade)
            {
                if(Map.detailsToPersist != null)
                {
                    SaveData.SetCoins(Map.detailsToPersist.coins);
                    Map.detailsToPersist = null;
                }
            }
            // clear aggregate references
            this.assets.Init();
            // shut down coroutines
            if(this.coroutines != null)
                foreach(var c in this.coroutines)
                    StopCoroutine(c);
            // shut down scripts
            this.gameObject.SetActive(false);
            Time.timeScale = 1f;
        }

        // ------------------------------------------------------------

        // InputCtrl.cs reads these to quick-restart the game 

        // override to define fail
        public virtual bool IsFailed() { return false; }

        // override to define completion state
        public virtual bool IsComplete() { return false; }


        // override to save preferences
        public virtual void Save()
        {
            PlayerPrefs.Save();
        }

        // Called when level is complete
        public virtual void CompleteLevel()
        {
            var isDifficultyAlreadyCompleted = SaveData.HasCompletedMinigameDifficulty(GetMinigameType(), difficulty);
            this.winMenu.isCompletedMinigameDifficultyEvent = false;

            if(Minigame.testingLevel == null)
                SaveData.SetMinigameCompleted(GetMinigameType(), difficulty, level, true);

            if(SaveData.HasCompletedMinigameDifficulty(GetMinigameType(), difficulty))
            {
                Achievements.CompleteMinigameAchievement(GetMinigameType(), (MinigameDifficulty)difficulty);

                if(isDifficultyAlreadyCompleted == false && mode == Mode.Arcade)
                    this.winMenu.isCompletedMinigameDifficultyEvent = true;
            }
            
            this.winMenu.Open();
        }

        // Called when level is failed
        public virtual void FailLevel()
        {
            Debug.Log("FAILED");
            this.retryMenu.Open();
        }

        public TextAsset GetLevelFile(MinigameDifficulty difficulty, int level)
        {
            // load from level groups or load from level list
            return 
                DebugPanelLevelsDropdown.level ??
                Levels.GetLevelFile(GetMinigameType(), difficulty, level) ??
                this.assets.levelTest;
        }


        public virtual void SetPaused(bool value)
        {
            if(value != Minigame.paused)
            {
                if(value)
                {
                    timeScalePrePause = Time.timeScale;
                    Time.timeScale = 0f;
                }
                else
                {
                    Time.timeScale = timeScalePrePause;
                }
                Minigame.paused = value;
            }
        }


        // -------------------------------------------------------------

        public Particle ParticleCreateAndPlayOnce(string name, Vector3 position, Transform parent = null)
        {
            return Particle.CreateAndPlayOnce(this.assets.animationLookup[name], position, parent ?? this.transform);
        }

        public void CreateParticleRect(string name, Rect rect, int total, int frameOffsetMax = 0, Transform parent = null)
        {
            for(int i = 0; i < total; i++)
            {
                var p = ParticleCreateAndPlayOnce(name, RandomUtil.InsideRect(rect), parent ?? this.transform);
                p.animated.frameNumber = (int)UnityEngine.Random.value * frameOffsetMax;
            }
        }

        public Animated CreateAnimated(Animated.Animation animation, Vector3 position, Transform parent, string sortingLayer)
        {
            var obj = Instantiate(this.assets.prefabLookup["GLOBAL:Animated"], position, Quaternion.identity, parent);
            var animated = obj.GetComponent<Animated>();
            animated.PlayAndLoop(animation);
            var spriteRenderer = obj.GetComponent<SpriteRenderer>();
            spriteRenderer.sortingLayerName = sortingLayer;
            return animated;
        }

        public void AddDustCloud(Vector2 center, float radius, int total, string sortingLayerName = null, int sortingOrder = -1, string animName = "GLOBAL:dust")
        {
            for(int i = 0; i < total; i++)
            {
                var particle = ParticleCreateAndPlayOnce(animName, center + (Vector2)UnityEngine.Random.insideUnitSphere * radius, this.transform);
                if(sortingLayerName != null)
                    particle.spriteRenderer.sortingLayerName = sortingLayerName;
                if(sortingOrder >= 0)
                    particle.spriteRenderer.sortingOrder = sortingOrder;
            }
        }

        public Particle AddDebris(string name, Vector2 position)
        {
            return AddDebris(name, position, SpeedDebrisMin, SpeedDebrisMax);
        }

        public void AddDebrisRect(string name, Rect rect, int total)
        {
            for(int i = 0; i < total; i++)
                AddDebris(name, RandomUtil.InsideRect(rect), SpeedDebrisMin, SpeedDebrisMax);
        }

        public Particle AddDebris(string name, Vector2 position, Vector2 speedMin, Vector2 speedMax)
        {
            Sprite sprite;
            if(this.assets.spriteArrayLookup.ContainsKey(name))
                sprite = RandomUtil.Pick(this.assets.spriteArrayLookup[name].list);
            else
                sprite = this.assets.spriteLookup[name];
            Particle particle = Particle.CreateWithSprite(
                            sprite,
                            120,
                            position,
                            this.transform
                        );
            particle.velocity = RandomUtil.Range(speedMin, speedMax);
            particle.acceleration = new Vector2(0, -0.025f);// gravity
            particle.spin = RandomUtil.Pick(5f, -5f);
            particle.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
            particle.spriteRenderer.sortingLayerName = this.objectLayers.fxSortingLayer;

            return particle;
        }

        public virtual void BreakTile(Collision2D collision)
        {
            var name = collision.otherCollider.name;
            var collider = collision.otherCollider;
            var pos = (Vector2)collision.otherCollider.transform.localPosition + collider.offset;
            this.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, Scale * 0.2f);
            var spec = BreakTileSpec.Specs[name];
            this.AddDebris(spec.pieceLeft, pos + spec.posLeft);
            this.AddDebris(spec.pieceRight, pos + spec.posRight);
            var particle = ParticleCreateAndPlayOnce(spec.fxAnim, pos, this.transform);
            particle.transform.localScale = spec.fxScale;
            Audio.instance.PlaySfx(assets.audioClipLookup["GLOBAL:"+spec.soundEffect]);

            Destroy(collision.otherCollider.gameObject);
        }

        public void UpdateMotors()
        {
            float t = Time.fixedDeltaTime;
            for(int i = this.motors.Count - 1; i > -1; i--)
            {
                var m = this.motors[i];
                if(m.transform != null)
                    m.Advance(t);
                else
                    this.motors.RemoveAt(i);
            }
        }

        public void RemoveMotor(Transform t)
        {
            for (int i = 0; i < this.motors.Count; i++)
                if(this.motors[i].transform == t)
                {
                    this.motors.RemoveAt(i);
                    break;
                }
        }

        public Motor GetMotor(Transform t)
        {
            for(int i = 0; i < this.motors.Count; i++)
                if(this.motors[i].transform == t)
                    return this.motors[i];
            return null;
        }

        public bool IsMotorLookAtTweenFinished()
        {
            int total = 0;
            Vector3 center = Vector3.zero;
            for(int i = 0; i < this.motors.Count; i++)
            {
                var m = this.motors[i];
                if(m.isLookAtTween)
                {
                    m.Advance(Time.fixedDeltaTime);
                    if(m.Done() == true)
                    {
                        m.isLookAtTween = false;
                    }
                    center += m.transform.localPosition;
                }
                else
                {
                    total++;
                }
            }
            if(total == this.motors.Count)
            {
                // get return tween to match current camera position and ball
                this.camCtrl.lookAtTween.tweenOut.start = this.cam.transform.localPosition;
                this.camCtrl.lookAtTween.tweenOut.finish = this.camCtrl.clampAction(GetCameraTargetDefault());
                return true;
            }
            else
            {
                center /= this.motors.Count - total;
                this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.localPosition, center, CamChangeInterpolation / 2), Vector3.zero);
                return false;
            }
        }

        // Override for MotorLookAtTween to find return target
        public virtual Vector3 GetCameraTargetDefault()
        {
            return Vector3.zero;
        }

        public Type GetMinigameType()
        {
            TypeToType.TryGetValue(GetType(), out Type type);
            return type;
        }

        // scaling methods
        public static Vector2 ToVector2(int x, int y) { return new Vector2((x + 0.5f) * Scale, (y + 0.5f) * Scale); }
        public static Vector2 ToVector2(Vector2Int p) { return new Vector2((p.x + 0.5f) * Scale, (p.y + 0.5f) * Scale); }
        public static Vector2Int ToVector2Int(Vector2 p) { return new Vector2Int((int)(p.x / Scale), (int)(p.y / Scale)); }
        // nodes are recorded at editor-pixel-scale
        public static Vector2Int ToVector2Int(NitromeEditor.Path.Node node) { return new Vector2Int((int)(node.x / EditorScale), (int)(node.y / EditorScale)); }
        public static Vector2 ToVector2(NitromeEditor.Path.Node node) { return new Vector2((node.x / EditorScale) * Scale, (node.y / EditorScale) * Scale); }

        public static Vector2 NormalFromZAngle(float z) { return new Vector2(Mathf.Cos(z * Mathf.Deg2Rad), Mathf.Sin(z * Mathf.Deg2Rad)); }

        public static Vector2 RectangleEdgeIntersection(Rect rect, Vector2 rayStart, Vector2 rayDirection)
        {
            // rayStart must be inside rectangle
            // rayDirection must be normalized

            float xDistance =
                    (rayDirection.x >= 0) ?
                    (rect.xMax - rayStart.x) / rayDirection.x :
                    (rect.xMin - rayStart.x) / rayDirection.x;

            float yDistance =
                    (rayDirection.y >= 0) ?
                    (rect.yMax - rayStart.y) / rayDirection.y :
                    (rect.yMin - rayStart.y) / rayDirection.y;

            float distance = Mathf.Min(xDistance, yDistance);
            return rayStart + (rayDirection * distance);
        }

        public static void SideNormals(Vector3 normal, out Vector3 left, out Vector3 right)
        {
            left = new Vector3(normal.y, -normal.x);
            right = new Vector3(-normal.y, normal.x);
        }

        public static Vector2 PointsCenter(Vector2[] points)
        {
            var v = Vector2.zero;
            if(points.Length == 0)
                return v;
            for(int i = 0; i < points.Length; i++)
                v += points[i];
            return v / points.Length;
        }

        public static List<Transform> FindTransforms(Transform transform, string name, List<Transform> list = null)
        {
            if(list == null)
            {
                list = new List<Transform>();
            }
            foreach(Transform childTransform in transform)
            {
                if(childTransform.childCount > 0)
                {
                    FindTransforms(childTransform, name, list);
                }
                if(childTransform.name == name)
                {
                    list.Add(childTransform);
                }
            }
            return list;
        }

        public static T GetMostCommon<T>(List<T> list)
        {
            Dictionary<T, int> totals = new Dictionary<T, int>();
            foreach(T item in list)
            {
                totals[item]++;
            }
            int best = 0;
            T bestItem = default;
            foreach(var kv in totals)
            {
                if(kv.Value > best)
                {
                    best = kv.Value;
                    bestItem = kv.Key;
                }
            }
            return bestItem;
        }

        public Vector2 WorldPositionToCanvasAnchoredPosition(Vector3 worldPosition)
        {
            //first you need the RectTransform component of your canvas
            RectTransform rect = canvas.GetComponent<RectTransform>();
            //then you calculate the position of the UI element
            //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.
            Vector2 ViewportPosition = cam.WorldToViewportPoint(worldPosition);
            return new Vector2(
            ((ViewportPosition.x * rect.sizeDelta.x) - (rect.sizeDelta.x * 0.5f)),
            ((ViewportPosition.y * rect.sizeDelta.y) - (rect.sizeDelta.y * 0.5f)));
        }

        public void DelayedCall(float time, Action action)
        {
            System.Collections.IEnumerator Impl()
            {
                yield return new WaitForSeconds(time);
                action();
            }
            StartCoroutine(Impl());
        }

        public void SparkleFx(Vector2 position)
        {
            var points = RandomUtil.Radial(5, 1f);
            for(int i = 0; i < points.Length; i++)
            {
                var p = position + points[i];
                DelayedCall(UnityEngine.Random.Range(0, 0.13f), () => { ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "trophy_sparkle", p); });
            }
            ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "trophy_sparkle", position);
        }

        public static bool IsLevelTesting()
        {
            return DebugPanel.selectedMode == DebugPanel.Mode.MinigameGolf
                    || DebugPanel.selectedMode == DebugPanel.Mode.MinigamePinball
                    || DebugPanel.selectedMode == DebugPanel.Mode.MinigameRacing
                    || DebugPanel.selectedMode == DebugPanel.Mode.MinigameFishing
                    || Minigame.testingLevel != null;
        }

        public static void DebugRect(Rect rect, Color color)
        {
            var topRight = rect.min + new Vector2(rect.width, 0);
            var bottomLeft = rect.min + new Vector2(0, rect.height);
            Debug.DrawLine(rect.min, topRight, color);
            Debug.DrawLine(topRight, rect.max, color);
            Debug.DrawLine(rect.max, bottomLeft, color);
            Debug.DrawLine(bottomLeft, rect.min, color);
        }
        public static void DebugPolygon(Vector2[] points, Color color)
        {
            for(int i = 0; i < points.Length; i++)
                Debug.DrawLine(points[i], points[(i + 1) % points.Length], color);
        }

        public static void DebugX(Vector2 p, Color color, float size = 0.25f)
        {
            Debug.DrawLine(p + new Vector2(-size, -size), p + new Vector2(size, size), color);
            Debug.DrawLine(p + new Vector2(size, -size), p + new Vector2(-size, size), color);
        }

        public static void ForeachInRectInt(RectInt rect, Action<Vector2Int> action)
        {
            for(int y = rect.y; y < rect.yMax; y++)
                for(int x = rect.x; x < rect.xMax; x++)
                    action(new Vector2Int(x, y));
        }

        public static void ForeachInFlood<T>
        (
            Vector2Int start,
            Dictionary<Vector2Int, T> dict,
            Predicate<KeyValuePair<Vector2Int, T>> canFloodTo,
            Action<Vector2Int> action,
            int stepsFlood = int.MaxValue,
            int stepsCell = int.MaxValue,
            bool isShuffleDirs = false
        ) => ForeachInFlood(new Vector2Int[] { start }, dict, canFloodTo, action, stepsFlood, stepsCell, isShuffleDirs);

        public static void ForeachInFlood<T>
        (
            ICollection<Vector2Int> start,
            Dictionary<Vector2Int, T> dict,
            Predicate<KeyValuePair<Vector2Int, T>> canFloodTo,
            Action<Vector2Int> action,
            int stepsFlood = int.MaxValue,
            int stepsCell = int.MaxValue,
            bool isShuffleDirs = false
        )
        {
            var dirs = new[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };
            var queue = new Queue<Vector2Int>();
            var visited = new Dictionary<Vector2Int, bool>();
            foreach(var p in start)
            {
                visited[p] = true;
                queue.Enqueue(p);
                action(p);
            }

            while(queue.Count > 0 && stepsFlood-- > 0)
            {
                int total = queue.Count;
                while(total-- > 0)
                {
                    var p = queue.Dequeue();
                    if(isShuffleDirs)
                        RandomUtil.Shuffle(dirs);
                    foreach(var d in dirs)
                    {
                        var key = p + d;
                        if(visited.ContainsKey(key) == false && dict.TryGetValue(key, out T value) == true)
                        {
                            if(canFloodTo(new KeyValuePair<Vector2Int, T>(key, value)) == true)
                            {
                                action(key);
                                queue.Enqueue(key);
                                visited[key] = true;
                                if(--stepsCell <= 0) return;
                            }
                        }
                    }
                }
            }
        }

        public class BreakTileSpec
        {
            public string pieceLeft;
            public string pieceRight;
            public string soundEffect;
            public string fxAnim;
            public int score;
            public Vector3 fxScale;
            public Vector2 posLeft;
            public Vector2 posRight;

            public static Dictionary<string, BreakTileSpec> Specs = new Dictionary<string, BreakTileSpec>
            {
                ["break block small"] = new BreakTileSpec("break block small left", "break block small right", "sfx_golf_break_block_small", Vector2.left * 0.25f, Vector2.right * 0.25f, Vector3.one * 0.5f)
                , ["break_tile_peg"] = new BreakTileSpec("peg_break1", "peg_break2", "sfx_golf_break_block_small", Vector2.left * 0.25f, Vector2.right * 0.25f, Vector3.one * 0.5f, 10, "peg_break")
                , ["break_block_small_red"] = new BreakTileSpec("break_block_small_red_shard_1", "break_block_small_red_shard_2", "sfx_golf_break_block_small", Vector2.left * 0.25f, Vector2.right * 0.25f, Vector3.one * 0.5f)
                , ["break_block_small_pink"] = new BreakTileSpec("break_block_small_pink_shard_1", "break_block_small_pink_shard_2", "sfx_golf_break_block_small", Vector2.left * 0.25f, Vector2.right * 0.25f, Vector3.one * 0.5f)
                , ["break_block_small_orange"] = new BreakTileSpec("break_block_small_orange_shard_1", "break_block_small_orange_shard_2", "sfx_golf_break_block_small", Vector2.left * 0.25f, Vector2.right * 0.25f, Vector3.one * 0.5f)
                , ["break block big"] = new BreakTileSpec("break block big left", "break block big right", "sfx_golf_break_block_big", Vector2.left * 0.5f, Vector2.right * 0.5f, Vector3.one, 20)
                , ["break_block_big_purple"] = new BreakTileSpec("break_block_big_purple_shard_1", "break_block_big_purple_shard_2", "sfx_golf_break_block_big", Vector2.left * 0.5f, Vector2.right * 0.5f, Vector3.one, 20)
                , ["break_block_big_red"] = new BreakTileSpec("break_block_big_red_shard_1", "break_block_big_red_shard_2", "sfx_golf_break_block_big", Vector2.left * 0.5f, Vector2.right * 0.5f, Vector3.one, 20)
                , ["break block landscape"] = new BreakTileSpec("break block landscape_piece_01", "break block landscape_piece_02", "sfx_golf_break_block_med", Vector2.left * 0.5f, Vector2.right * 0.5f, new Vector3(1f, 0.5f, 1f), 15)
                , ["break_block_landscape_purple"] = new BreakTileSpec("break_block_landscape_purple_shard_1", "break_block_landscape_purple_shard_2", "sfx_golf_break_block_med", Vector2.left * 0.5f, Vector2.right * 0.5f, new Vector3(1f, 0.5f, 1f), 15)
                , ["break_block_landscape_yellow"] = new BreakTileSpec("break_block_landscape_yellow_shard_1", "break_block_landscape_yellow_shard_2", "sfx_golf_break_block_med", Vector2.left * 0.5f, Vector2.right * 0.5f, new Vector3(1f, 0.5f, 1f), 15)
                , ["break block portrait"] = new BreakTileSpec("break block portrait_piece_01", "break block portrait_piece_02", "sfx_golf_break_block_med", Vector2.left * 0.25f, Vector2.right * 0.25f, new Vector3(0.5f, 1f, 1f), 15)
                , ["break_block_portrait_blue"] = new BreakTileSpec("break_block_portrait_blue_shard_1", "break_block_portrait_blue_shard_2", "sfx_golf_break_block_med", Vector2.left * 0.25f, Vector2.right * 0.25f, new Vector3(0.5f, 1f, 1f), 15)
                , ["break_block_portrait_yellow"] = new BreakTileSpec("break_block_portrait_yellow_shard_1", "break_block_portrait_yellow_shard_2", "sfx_golf_break_block_med", Vector2.left * 0.25f, Vector2.right * 0.25f, new Vector3(0.5f, 1f, 1f), 15)
                , ["break_block_portrait_purple"] = new BreakTileSpec("break_block_portrait_purple_shard_1", "break_block_portrait_purple_shard_2", "sfx_golf_break_block_med", Vector2.left * 0.25f, Vector2.right * 0.25f, new Vector3(0.5f, 1f, 1f), 15)
            };

            public BreakTileSpec(string pieceLeft, string pieceRight, string soundEffect, Vector2 posLeft, Vector2 posRight, Vector2 fxScale, int score = 10, string fxAnim = "GLOBAL:break_block_fx")
            {
                this.pieceLeft = pieceLeft;
                this.pieceRight = pieceRight;
                this.soundEffect = soundEffect;
                this.posLeft = posRight;
                this.fxScale = fxScale;
                this.score = score;
                this.fxAnim = fxAnim;
            }
        }

    }

}




