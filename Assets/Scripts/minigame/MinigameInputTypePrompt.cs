using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using minigame;

public class MinigameInputTypePrompt : InputTypePrompt
{
    public Animated.Animation animationAction;
    protected Animated animatedAction;
    public Minigame.Type minigameType;

    public static Dictionary<Minigame.Type, bool> visited = new Dictionary<Minigame.Type, bool>();

    protected override void Init()
    {
        base.Init();

        var animateds = this.prompt.GetComponentsInChildren<Animated>();
        this.animatedAction = animateds[0];
        this.animatedShadow = animateds[1];
        this.animated = animateds[2];

        //Show();//debug
    }

    public void FirstShow()
    {
        this.secondsIdle = 0f;
        if(visited.ContainsKey(minigameType) == false)
        {
            Show();
            visited[minigameType] = true;
        }
    }

    public void ShowIfIdle(float time)
    {
        if(this.isHidden == true && this.secondsIdle > time)
            Show();
    }

    protected override void UpdateAnimations()
    {
        base.UpdateAnimations();
        this.animatedAction.PlayAndLoop(this.animationAction);
    }
}
