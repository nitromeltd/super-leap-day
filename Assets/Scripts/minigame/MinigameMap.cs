using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    public class MinigameMap : MonoBehaviour
    {

        public static bool IsCreatePreviewSnapshot = false;// set to true only when making level thumbnails

        public class Record
        {
            public string name;
            public string nameTile;
            public NitromeEditor.TileLayer.Tile tile;
            public GameObject gameObject;
            public string layerName;
            public int x;
            public int y;
            public Vector2Int p;
            public Vector2 pos;
            public string sortingLayer;
            public Vector2 offset;
            public bool isSprite;
            public bool isPrefab;
            public bool isPickup;
            public T GetComponent<T>() => this.gameObject.GetComponent<T>();
        }
        public class Connection
        {
            public Vector2Int a, b;
            public Vector2 posA, posB;
            public GameObject objA, objB;
            public string nameA, nameB;
            public float speedPixelsPerSecond;
            public bool broken;// attempt to create connection failed
            public Connection(MinigameMap map, NitromeEditor.Path path)
            {
                this.posA = new Vector2(path.nodes[0].x / EditorScale, path.nodes[0].y / EditorScale);
                this.posB = new Vector2(path.nodes[1].x / EditorScale, path.nodes[1].y / EditorScale);
                this.a = ToVector2Int(path.nodes[0]);
                this.b = ToVector2Int(path.nodes[1]);
                map.vector2IntToGameObject.TryGetValue(a, out this.objA);
                map.vector2IntToGameObject.TryGetValue(b, out this.objB);
                map.vector2IntToString.TryGetValue(a, out this.nameA);
                map.vector2IntToString.TryGetValue(b, out this.nameB);
                this.broken = this.objA == null || this.objB == null;
                this.speedPixelsPerSecond = path.edges[0].speedPixelsPerSecond;
            }
            public String GetName() => this.nameA ?? this.nameB;
            public GameObject GetObj() => this.objA ?? this.objB;
            public T GetComponent<T>() => (this.objA ?? this.objB).GetComponent<T>();
        }

        public Minigame game;
        public TileTheme tileTheme;
        public PhysicsMaterial2D physicsMaterialNormal;
        public PhysicsMaterial2D physicsMaterialSlope;
        public PhysicsMaterial2D physicsMaterialDirt;

        public List<Record> records;
        public List<Connection> connections;
        public List<Connection> connectionsBroken;
        public List<Motor> motors;
        public TileGrid tileGrid;
        public NitromeEditor.Level levelOriginal;

        [NonSerialized] public int width, height;
        [NonSerialized] public Rect rect;
        [NonSerialized] public RectInt rectInt;
        [NonSerialized] public Transform transformObjects;
        [NonSerialized] public Transform transformTiles;
        [NonSerialized] public Transform transformBackground;
        [NonSerialized] public Rect? rectPreviewSnapshot = null;// assigned when preview snapshot can be taken

        public Dictionary<string, bool> pickupPrefabs = new Dictionary<string, bool>();
        public Dictionary<string, bool> dontRotateTiles = new Dictionary<string, bool>();
        public Dictionary<string, bool> noAutoTiles = new Dictionary<string, bool>();
        public Dictionary<string, bool> filledTiles = new Dictionary<string, bool>();
        public Dictionary<string, RectInt> filledRectInts = new Dictionary<string, RectInt>();
        // level-gen pairing lookup
        public Dictionary<Vector2Int, string> vector2IntToString;
        public Dictionary<GameObject, Vector2Int> gameObjectToVector2Int;
        public Dictionary<Vector2Int, GameObject> vector2IntToGameObject;
        public Dictionary<Vector2Int, Motor> motorLookup;

        public static float Scale = 1;
        public static float EditorScale = 16;
        public static float PixelsPerUnit = 70;
        public static Vector2 ScaleOne = new Vector2(Scale, Scale);
        public static Vector2 ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
        public static Vector2 SizePreviewSnapshot = new Vector2(16f, 16f);

        public static string[] FruitRandom = new string[]
        {
            "apple"
            ,"banana"
            ,"cherry"
            ,"kiwi"
            ,"lemon"
            ,"melon"
            ,"orange"
            ,"pear"
            ,"pineapple"
            ,"strawberry"
        };

        public static Dictionary<string, bool> PreviewNames = new Dictionary<string, bool>
        {
            ["preview12x12"] = true,
            ["preview16x16"] = true,
            ["preview24x24"] = true
        };

        public void AddPickupPrefabs(params string[] list)
        {
            foreach(var s in list)
                pickupPrefabs[s] = true;
        }

        public void AddNoAutoTiles(string prefix, params string[] list)
        {
            foreach(var s in list)
                noAutoTiles[prefix + s] = true;
        }

        public void AddDontRotateTiles(params string[] list)
        {
            foreach(var s in list)
                dontRotateTiles[s] = true;
        }

        public void AddFilledTiles(params string[] list)
        {
            foreach(var s in list)
                filledTiles[s] = true;
        }

        public void AddFilledRectInts(params (string name, RectInt rect)[] list)
        {
            foreach(var s in list)
                filledRectInts[s.name] = s.rect;
        }

        public string StringAt(Vector2Int p) => this.vector2IntToString[p];
        public GameObject GameObjectAt(Vector2Int p) => this.vector2IntToGameObject[p];
        public Vector2Int Vector2IntFrom(GameObject o) => this.gameObjectToVector2Int[o];
        
        public void Create(TextAsset file)
        {
            Create(ParseLevel(file));
        }

        public void Create(NitromeEditor.Level level)
        {
            foreach(Transform childTransform in transform)
                Destroy(childTransform.gameObject);

            this.transformObjects = new GameObject("objects").transform;
            this.transformObjects.SetParent(this.transform);
            this.transformTiles = new GameObject("tiles").transform;
            this.transformTiles.SetParent(this.transform);
            this.transformBackground = new GameObject("background").transform;
            this.transformBackground.SetParent(this.transform);

            this.records = new List<Record>();
            this.connections = new List<Connection>();
            this.connectionsBroken = new List<Connection>();
            this.motors = new List<Motor>();
            this.vector2IntToGameObject = new Dictionary<Vector2Int, GameObject>();
            this.vector2IntToString = new Dictionary<Vector2Int, string>();
            this.gameObjectToVector2Int = new Dictionary<GameObject, Vector2Int>();

            var pickupLookup = new Dictionary<Vector2Int, Pickup>();

            var tileLayerTiles = level.TileLayerByName("Tiles");

            for(int i = 0; i < level.tileLayers.Length; i++)
            {
                var tileLayer = level.tileLayers[i];
                var tileSortingLayer = tileLayer.layerName switch
                {
                    "Background" => this.game.objectLayers.backgroundSortingLayer,
                    "Tiles" => this.game.objectLayers.tilesSortingLayer,
                    "Objects" => this.game.objectLayers.objectsSortingLayer,
                    "Foreground" => this.game.objectLayers.foregroundSortingLayer,
                    _ => "Default"
                };

                for(int j = 0; j < tileLayer.tiles.Length; j++)
                {
                    var t = tileLayer.tiles[j];
                    var name = t.tile;

                    if(name != null)
                    {
                        var x = j % width;
                        var y = j / width;
                        var p = new Vector2Int(x, y);
                        var pos = (Vector2)p;
                        var sortingLayer = tileSortingLayer;
                        var offset = new Vector2(t.offsetX, t.offsetY) / Minigame.EditorScale;
                        var isSprite = false;
                        var isPrefab = false;
                        var isPickup = false;

                        if(t.tile == Minigame.GlobalPrefix + "random_fruit") t.tile = Minigame.GlobalPrefix + RandomUtil.Pick(FruitRandom);

                        GameObject obj = null;
                        // does this asset use a common resource?
                        if(this.game.assets.baseNameLookup.ContainsKey(name))
                            name = this.game.assets.baseNameLookup[name];

                        if(this.game.assets.prefabLookup.TryGetValue(name, out GameObject prefab) == true)
                        {
                            if(this.pickupPrefabs.ContainsKey(name) == true)
                            {
                                if(this.game.pickupMap.hide.ContainsKey(p) == false)
                                {
                                    obj = Instantiate(prefab, this.transformObjects);
                                    this.game.pickupMap.CreatePickup(t, obj, p, pickupLookup);
                                }
                                isPickup = true;
                            }
                            else
                            {
                                obj = Instantiate(prefab, this.transformObjects);
                                if(this.vector2IntToString.ContainsKey(p) == false || tileLayer.layerName == "Objects")
                                    this.vector2IntToString[p] = name;
                                isPrefab = true;
                            }
                        }
                        else if(this.game.assets.spriteLookup.TryGetValue(name, out Sprite sprite) == true)
                        {
                            var sr = this.game.spriteRendererPool.Get(pos, sprite, sortingLayer, name, this.transform);
                            obj = sr.gameObject;
                            isSprite = true;
                        }
                        else if(this.game.pickupMap.names.ContainsKey(t.tile))
                        {
                            this.game.pickupMap.CreatePickup(t, p, pos, pickupLookup, ref obj, offset);
                            isPickup = true;
                        }
                        else
                        {
                            this.vector2IntToString[p] = name;
                        }

                        if(obj != null)
                        {
                            obj.name = t.tile;
                            if(this.dontRotateTiles.ContainsKey(t.tile) == false)
                                obj.transform.rotation *= Quaternion.Euler(0, t.flip ? 180 : 0, t.rotation * (t.flip ? -1 : 1));

                            //Debug.Log($"tile {tile.tile} r:{tile.rotation} flip:{tile.flip} result: {obj.transform.rotation.eulerAngles}");

                            obj.transform.localPosition = pos + offset;

                            if(tileLayer.layerName == "Objects")
                            {
                                this.gameObjectToVector2Int[obj] = p;
                                this.vector2IntToGameObject[p] = obj;
                            }
                            if(sortingLayer != "")
                                ObjectLayers.SetSortingLayer(obj, sortingLayer);
                        }

                        var record = new Record
                        {
                            name = name,
                            nameTile = t.tile,
                            gameObject = obj,
                            tile = t,
                            x = x,
                            y = y,
                            p = p,
                            pos = pos,
                            sortingLayer = sortingLayer,
                            offset = offset,
                            isSprite = isSprite,
                            isPrefab = isPrefab,
                            isPickup = isPickup,
                            layerName = tileLayer.layerName
                        };
                        this.records.Add(record);

                        // some tiles are filled in to influence the autotiler
                        if(filledTiles.ContainsKey(name))
                            tileLayerTiles.tiles[j].tile = Minigame.FilledTile;

                        if(filledRectInts.ContainsKey(name))
                        {
                            var r = filledRectInts[name];
                            r.position = p;
                            r = RectIntFromTile(r, p, t);
                            SetTiles(r, Minigame.FilledTile, tileLayerTiles, width);
                        }

                        // don't confuse the autotile system
                        if(noAutoTiles.ContainsKey(name) && tileLayer == tileLayerTiles)
                            tileLayerTiles.tiles[j].tile = "";

                        // set thumnail (executes only in editor)
                        if(PreviewNames.ContainsKey(name))
                            SetPreviewSnapshot(name, pos + offset);
                    }
                }
            }

            this.tileGrid = GetAutoTiles(level, this.tileTheme, this.physicsMaterialNormal, this.physicsMaterialSlope, this.physicsMaterialDirt);

            this.motorLookup = new Dictionary<Vector2Int, Motor>();

            for(int i = 0; i < level.pathLayers.Length; i++)
            {
                var layer = level.pathLayers[i];
                for(int j = 0; j < layer.paths.Length; j++)
                {
                    var path = layer.paths[j];
                    if(layer.layerName == Minigame.ConnectionsLayer && path.nodes.Length == 2)
                    {
                        var conn = new Connection(this, path);
                        if(conn.broken == false)
                            this.connections.Add(conn);
                        else
                            this.connectionsBroken.Add(conn);
                    }
                    else if(path.nodes.Length > 1)
                        AddMotorFromPath(path, motorLookup, pickupLookup);
                }
            }

        }

        // Import JSON
        public NitromeEditor.Level ParseLevel(TextAsset file)
        {

            // if we have test level waiting load it instead
            NitromeEditor.Level level;
            if(LevelTestingServer.receivedLevel != null)
            {
                Minigame.testingLevel = LevelTestingServer.receivedLevel;
                level = CloneLevel(Minigame.testingLevel);
                LevelTestingServer.receivedLevel = null;
            }
            else if(Minigame.testingLevel != null)
                level = CloneLevel(Minigame.testingLevel);
            else
            {
                if(file.text.StartsWith("{\""))
                    level = TinyJson.JSONParser.FromJson<NitromeEditor.Level>(file.text);
                else
                    level = NitromeEditor.Level.UnserializeFromBytes(file.bytes);
            }
            this.width = level.Columns();
            this.height = level.Rows();
            this.rect = new Rect(0, 0, width, height);
            this.rectInt = new RectInt(0, 0, width, height);

            // we're going to alter level, we might want a backup
            this.levelOriginal = CloneLevel(level);

            return level;
        }


        // Level gets butchered to perform auto-tile hacks - we need a copy
        public NitromeEditor.Level CloneLevel(NitromeEditor.Level source)
        {
            var level = new NitromeEditor.Level
            {
                maxX = source.maxX,
                maxY = source.maxY,
                minX = source.minX,
                minY = source.minY,
                tileLayers = (NitromeEditor.TileLayer[])source.tileLayers.Clone(),
                pathLayers = (NitromeEditor.PathLayer[])source.pathLayers.Clone(),
                shapeLayers = (NitromeEditor.ShapeLayer[])source.shapeLayers.Clone(),
                properties = new Dictionary<string, PropertyValue>(source.properties)
            };
            for(int i = 0; i < level.tileLayers.Length; i++)
            {
                var tl = level.tileLayers[i];
                level.tileLayers[i] = new NitromeEditor.TileLayer
                {
                    layerName = tl.layerName,
                    tiles = (NitromeEditor.TileLayer.Tile[])tl.tiles.Clone()
                };
            }
            return level;
        }

        // Execute autotiling
        public TileGrid GetAutoTiles(
            NitromeEditor.Level level,
            TileTheme tileTheme,
            PhysicsMaterial2D physicsMaterialNormal,
            PhysicsMaterial2D physicsMaterialSlope,
            PhysicsMaterial2D physicsMaterialDirt,
            bool isPolySlopes = false,
            bool isNoMergers = false
        )
        {
            var tileGrid = new TileGrid();
            tileGrid.isPolySlopes = isPolySlopes;
            tileGrid.isNoMergers = isNoMergers;
            var w = level.Columns();
            var h = level.Rows();
            // do background layer
            tileGrid.Init(w, h, level.TileLayerByName("Background"), transformBackground, this.rectInt);
            tileGrid.sortingOrder = ++this.game.spriteRendererPool.sortingOrderCount;
            tileGrid.Autotile(tileTheme, this.game.objectLayers.backgroundSortingLayer);
            // do tiles layer
            tileGrid.Init(w, h, level.TileLayerByName("Tiles"), this.transformTiles, this.rectInt);
            tileGrid.sortingOrder = ++this.game.spriteRendererPool.sortingOrderCount;
            tileGrid.Autotile(tileTheme, this.game.objectLayers.tilesSortingLayer, ObjectLayers.ToLayer(this.game.objectLayers.tileLayerMask.value));
            tileGrid.physicsMaterialNormal = physicsMaterialNormal;
            tileGrid.physicsMaterialSlope = physicsMaterialSlope;
            tileGrid.physicsMaterialDirt = physicsMaterialDirt;
            tileGrid.objectLayers = this.game.objectLayers;
            tileGrid.GetColliders();
            return tileGrid;
        }

        // Called whilst parsing NitromeEditor.Path
        public void AddMotorFromPath(NitromeEditor.Path path, Dictionary<Vector2Int, Motor> motorLookup, Dictionary<Vector2Int, Pickup> pickupLookup)
        {
            var start = ToVector2Int(path.nodes[0]);
            // we might be assigning a path to a pickup that was collected on a previous run
            if(this.vector2IntToGameObject.ContainsKey(start) == false)
                return;

            var transform = this.vector2IntToGameObject[start].transform;
            var motor = AddMotorFromPath(path, transform);
            motorLookup[start] = motor;
            pickupLookup.TryGetValue(start, out Pickup pickup);
            if(pickup != null)
            {
                pickup.RemoveFromHashMap(this.game.pickupMap.hashMap);
                this.game.pickupMap.moving.Add(pickup);
                pickup.moving = true;
            }
            else
                motor.dots = AddPathDots(path, transform);
        }

        public Motor AddMotorFromPath(NitromeEditor.Path path, Transform objTransform)
        {
            var n = path.nodes[0];
            var first = ToVector2Int(n);
            var firstV = ToVector2(first);
            var offset = (Vector2)objTransform.localPosition - firstV;
            var motor = new Motor(objTransform);
            Rigidbody2D rigidbody2D = objTransform.gameObject.GetComponent<Rigidbody2D>();
            if(rigidbody2D == null)
            {
                rigidbody2D = objTransform.gameObject.AddComponent<Rigidbody2D>();
                rigidbody2D.bodyType = RigidbodyType2D.Kinematic;
            }
            motor.AddNode(firstV + offset, 0, null);
            var prev = first;
            if(n.delaySeconds > 0)
            {
                motor.AddNode(ToVector2(first), n.delaySeconds, 0);
            }

            for(int k = 1; k < path.nodes.Length; k++)
            {
                n = path.nodes[k];
                var p = ToVector2Int(n);
                var e = path.edges[k - 1];
                motor.AddNode(ToVector2(p) + offset, (p - prev).magnitude / (e.speedPixelsPerSecond / EditorScale), e.easing);
                if(n.delaySeconds > 0)
                {
                    motor.AddNode(ToVector2(p) + offset, n.delaySeconds);
                }
                prev = p;
            }
            if(path.closed == true)
            {
                n = path.nodes[0];
                var p = ToVector2Int(n);
                var e = path.edges[^1];
                motor.AddNode(ToVector2(p) + offset, (p - prev).magnitude / (e.speedPixelsPerSecond / EditorScale), e.easing);
            }

            this.motors.Add(motor);
            return motor;
        }
        public SpriteRenderer[] AddPathDots(NitromeEditor.Path path, Transform objTransform)
        {
            var dots = new List<SpriteRenderer>();
            var n = path.nodes[0];
            var first = ToVector2Int(n);
            var firstV = ToVector2(first);
            var offset = (Vector2)objTransform.localPosition - firstV;
            var total = path.nodes.Length;
            if(path.closed == true) total++;
            for(int k = 1; k < total; k++)
            {
                var n0 = path.nodes[(k % path.nodes.Length)];
                var n1 = path.nodes[k - 1];
                var p0 = ToVector2(n0);
                var p1 = ToVector2(n1);
                var v = p1 - p0;
                var length = v.magnitude;
                v.Normalize();
                for(float len = 0; len < length; len += Scale)
                {
                    dots.Add(this.game.spriteRendererPool.Get(p0 + v * len + offset, this.game.assets.spriteLookup["dot"], "Background", "pathDot", transform));
                }
            }
            if(path.closed == false || path.nodes[0].Position != path.nodes[^1].Position)
            {
                var p0 = ToVector2(path.nodes[0]);
                dots.Add(this.game.spriteRendererPool.Get(p0 + offset, this.game.assets.spriteLookup["dot"], "Background", "pathDot", transform));
            }
            return dots.ToArray();
        }

        // Extend paths for motors that don't have overlapping start-finish nodes
        public static void BacktrackMotors(ICollection<Motor> motors)
        {
            foreach(var m in motors)
                if(m.start != m.tweens[^1].finish) m.AddBacktrackTweens();
        }
        // scaling methods
        public static Vector2 ToVector2(int x, int y) { return new Vector2((x + 0.5f) * Scale, (y + 0.5f) * Scale); }
        public static Vector2 ToVector2(Vector2Int p) { return new Vector2((p.x + 0.5f) * Scale, (p.y + 0.5f) * Scale); }
        public static Vector2Int ToVector2Int(Vector2 p) { return new Vector2Int((int)(p.x / Scale), (int)(p.y / Scale)); }
        // nodes are recorded at editor-pixel-scale
        public static Vector2Int ToVector2Int(NitromeEditor.Path.Node node) { return new Vector2Int((int)(node.x / EditorScale), (int)(node.y / EditorScale)); }
        public static Vector2 ToVector2(NitromeEditor.Path.Node node) { return new Vector2((node.x / EditorScale) * Scale, (node.y / EditorScale) * Scale); }

        public static Vector2 NormalFromZAngle(float z) { return new Vector2(Mathf.Cos(z * Mathf.Deg2Rad), Mathf.Sin(z * Mathf.Deg2Rad)); }

        // Change the names of a block of tiles in a NitromeEditor.TileLayer
        public static void SetTiles(RectInt rectInt, string name, NitromeEditor.TileLayer tileLayer, int levelWidth)
        {
            for(int r = rectInt.y; r < rectInt.y + rectInt.height; r++)
                for(int c = rectInt.x; c < rectInt.x + rectInt.width; c++)
                    tileLayer.tiles[c + r * levelWidth].tile = name;
        }
        public static void SetTile(int x, int y, string name, NitromeEditor.TileLayer tileLayer, int levelWidth)
        {
            tileLayer.tiles[x + y * levelWidth].tile = name;
        }
        public static NitromeEditor.TileLayer.Tile GetTile(int x, int y, NitromeEditor.TileLayer tileLayer, int levelWidth)
        {
            if(x >= 0 && y >= 0 && x < levelWidth && y < tileLayer.tiles.Length / levelWidth)
                return tileLayer.tiles[x + y * levelWidth];
            else
                return new NitromeEditor.TileLayer.Tile { tile = Minigame.GlobalPrefix + "square" };
        }
        public static NitromeEditor.TileLayer.Tile FindNameOrTile(int x, int y, string name, NitromeEditor.Level level)
        {
            if(x >= level.minX && y >= level.minY && x <= level.maxX && y <= level.maxY)
            {
                var t = level.TileLayerByName("Objects").tiles[x + y * level.Columns()];
                if(t.tile == name)
                    return t;
                else
                    return level.TileLayerByName("Tiles").tiles[x + y * level.Columns()];
            }
            else
                return new NitromeEditor.TileLayer.Tile { tile = Minigame.GlobalPrefix + "square" };
        }
        public static bool OutOfBounds(int x, int y, NitromeEditor.Level level)
        {
            return (x >= level.minX && y >= level.minY && x <= level.maxX && y <= level.maxY) == false;
        }

        // transform a RectInt around Vector2Int p using data from NitromeEditor.TileLayer.Tile tile
        public static RectInt RectIntFromTile(RectInt rectInt, Vector2Int p, NitromeEditor.TileLayer.Tile tile)
        {
            if(tile.flip == true)
                rectInt.x = p.x + (p.x - (rectInt.x + rectInt.width - 1));
            if(tile.rotation != 0)
            {
                var inset = p - rectInt.position;
                var outset = (rectInt.max - Vector2Int.one) - p;
                switch(tile.rotation)
                {
                    case 90:
                        (rectInt.width, rectInt.height) = (rectInt.height, rectInt.width);
                        rectInt.x = p.x - outset.y;
                        rectInt.y = p.y - inset.x;
                        break;
                    case 180:
                        rectInt.x = p.x - outset.x;
                        rectInt.y = p.y - outset.y;
                        break;
                    case 270:
                        (rectInt.width, rectInt.height) = (rectInt.height, rectInt.width);
                        rectInt.x = p.x - inset.y;
                        rectInt.y = p.y - outset.x;
                        break;
                }
            }
            return rectInt;
        }

        // Press Ctrl/Cmd+R to refresh the Project window after playing a level in the editor
        public void PreviewSnapshot(Rect rect, Action onPreRender = null)
        {
#if UNITY_EDITOR

            if(IsCreatePreviewSnapshot == true)
            {
                var gameObjectsToHide = new GameObject[] { this.game.canvas.gameObject, this.game.winMenu.transform.parent.gameObject };
                foreach(var obj in gameObjectsToHide)
                    obj.SetActive(false);
                var texture = Snapshot(rect, this.game.cam, (int)PixelsPerUnit, onPreRender);
                var filename = Minigame.testingLevel != null ? "testing_level" : this.game.fileLevel.name;
                var path = $"Resources/Minigame Levels/{this.game.GetMinigameType()}/images/{filename}.png";
                System.IO.File.WriteAllBytes(Application.dataPath + "/" + path, texture.EncodeToPNG());
                Debug.Log($"Level preview saved to: {path}");
                Destroy(texture);
                foreach(var obj in gameObjectsToHide)
                    obj.SetActive(true);
            }
#endif
        }

        // sets the preview rect if the name matches
        public void SetPreviewSnapshot(string name, Vector2 position)
        {
            switch(name)
            {
                case "preview12x12": this.rectPreviewSnapshot = new Rect(position - new Vector2(6, 6), new Vector2(12, 12)); break;
                case "preview16x16": this.rectPreviewSnapshot = new Rect(position - SizePreviewSnapshot * 0.5f, SizePreviewSnapshot); break;
                case "preview24x24": this.rectPreviewSnapshot = new Rect(position - new Vector2(12, 12), new Vector2(24, 24)); break;
            }
        }

        // Create a Texture2D snapshot
        //
        // -- WARNING: you must destroy the Texture2D manually when you are finished with it.
        //
        public static Texture2D Snapshot(Rect rect, Camera camera, int pixelsPerUnit, Action onPreRender = null)
        {
            int widthPixels = Mathf.FloorToInt(rect.width * pixelsPerUnit);
            int heightPixels = Mathf.FloorToInt(rect.height * pixelsPerUnit);
            // buffer camera
            (Vector3 position, float orthographicSize, float aspect, RenderTexture targetTexture) camBuffer =
                (camera.transform.position, camera.orthographicSize, camera.aspect, camera.targetTexture);
            // create render texture
            RenderTexture rt = RenderTexture.GetTemporary(widthPixels, heightPixels, 32, RenderTextureFormat.ARGB32);
            rt.useMipMap = false;
            rt.anisoLevel = 0;
            rt.Create();
            camera.aspect = rect.width / rect.height;
            camera.orthographicSize = rect.height / 2;
            camera.transform.position = (Vector3)rect.center + new Vector3(0, 0, camera.transform.position.z);
            camera.targetTexture = rt;
            // unity reads pixels from the active render texture, we need to buffer it
            var currentRT = RenderTexture.active;
            RenderTexture.active = rt;
            if(onPreRender != null) onPreRender();// actions to take before rendering
            camera.Render();
            // set the pixels (unity reads / writes from the bottom of the image upwards)
            Texture2D image = new Texture2D(widthPixels, heightPixels);
            image.ReadPixels(new Rect(0, 0, widthPixels, heightPixels), 0, 0);
            image.Apply();
            image.hideFlags = HideFlags.HideAndDontSave;
            // tidy up
            RenderTexture.active = currentRT;
            camera.targetTexture = null;
            RenderTexture.ReleaseTemporary(rt);
            // restore camera
            camera.transform.position = camBuffer.position;
            camera.orthographicSize = camBuffer.orthographicSize;
            camera.aspect = camBuffer.aspect;
            camera.targetTexture = camBuffer.targetTexture;
            return image;
        }

        // =======================================================================================
        // Autotiling animations
        // =======================================================================================

        [System.Serializable]
        public struct Autotile
        {
            public Animated.Animation anim;
            public bool useCorners;
            public Neighbours neighbours;
        }

        [System.Serializable]
        public struct Neighbours
        {
            public bool up;
            public bool down;
            public bool left;
            public bool right;

            [Header("Corners")]
            public bool upLeft;
            public bool upRight;
            public bool downLeft;
            public bool downRight;

            public Neighbours(bool up, bool down, bool left, bool right,
                bool upLeft, bool upRight, bool downLeft, bool downRight)
            {
                this.up = up;
                this.down = down;
                this.left = left;
                this.right = right;
                this.upLeft = upLeft;
                this.upRight = upRight;
                this.downLeft = downLeft;
                this.downRight = downRight;
            }

            public bool IsEqual(Neighbours n, bool useCorners = false)
            {
                bool nonCornersEqual =
                    this.up == n.up &&
                    this.down == n.down &&
                    this.left == n.left &&
                    this.right == n.right;

                bool cornersEqual =
                    this.upLeft == n.upLeft &&
                    this.upRight == n.upRight &&
                    this.downLeft == n.downLeft &&
                    this.downRight == n.downRight;

                return useCorners ? cornersEqual && nonCornersEqual : nonCornersEqual;
            }
        }


        public static void AddAutotiling(Animated anim, NitromeEditor.TileLayer.Tile t, int x, int y, AssetLibrary.AutotileArray autotileArray, NitromeEditor.Level level)
        {
            //Debug.Log("===");
            //Debug.Log($"Autotile {t.tile} at {x},{y}");

            var name = t.tile;
            var tUp = FindNameOrTile(x, y + 1, name, level);
            var tDown = FindNameOrTile(x, y - 1, name, level);
            var tLeft = FindNameOrTile(x - 1, y, name, level);
            var tRight = FindNameOrTile(x + 1, y, name, level);

            var tUpLeft = FindNameOrTile(x - 1, y + 1, name, level);
            var tUpRight = FindNameOrTile(x + 1, y + 1, name, level);
            var tDownLeft = FindNameOrTile(x - 1, y - 1, name, level);
            var tDownRight = FindNameOrTile(x + 1, y - 1, name, level);

            //Debug.Log($"up: {tUp.tile}, down: {tDown.tile}, left: {tLeft.tile}, right: {tRight.tile}");
            //Debug.Log($"up-left: {tUpLeft.tile}, up-right: {tUpRight.tile}, down-left: {tDownLeft.tile}, down-right: {tDownRight.tile}");

            if(tUp.tile != null && tUp.tile != name
                || tDown.tile != null && tDown.tile != name
                || tLeft.tile != null && tLeft.tile != name
                || tRight.tile != null && tRight.tile != name
            )
            {
                // there are regular tiles next to it
                bool tileUp = tUp.tile != null;
                bool tileDown = tDown.tile != null;
                bool tileLeft = tLeft.tile != null;
                bool tileRight = tRight.tile != null;

                bool tileUpLeft = tUpLeft.tile != null;
                bool tileUpRight = tUpRight.tile != null;
                bool tileDownLeft = tDownLeft.tile != null;
                bool tileDownRight = tDownRight.tile != null;

                var myNeighbours = new Neighbours(
                    tileUp, tileDown, tileLeft, tileRight, tileUpLeft, tileUpRight, tileDownLeft, tileDownRight
                );

                int i = 0;
                foreach(Autotile at in autotileArray.list)
                {
                    if(myNeighbours.IsEqual(at.neighbours, at.useCorners))
                    {
                        anim.PlayAndLoop(at.anim);
                        //Debug.Log($"a - animation {i}");
                    }
                    i++;
                }
                return;
            }

            {
                bool tileUp = tUp.tile == name || OutOfBounds(x, y + 1, level) == true;
                bool tileDown = tDown.tile == name || OutOfBounds(x, y - 1, level) == true;
                bool tileLeft = tLeft.tile == name || OutOfBounds(x - 1, y, level) == true;
                bool tileRight = tRight.tile == name || OutOfBounds(x + 1, y, level) == true;

                bool tileUpLeft = tUpLeft.tile == name || OutOfBounds(x - 1, y + 1, level) == true;
                bool tileUpRight = tUpRight.tile == name || OutOfBounds(x + 1, y + 1, level) == true;
                bool tileDownLeft = tDownLeft.tile == name || OutOfBounds(x - 1, y - 1, level) == true;
                bool tileDownRight = tDownRight.tile == name || OutOfBounds(x + 1, y - 1, level) == true;

                var myNeighbours = new Neighbours(
                    tileUp, tileDown, tileLeft, tileRight, tileUpLeft, tileUpRight, tileDownLeft, tileDownRight
                );
                int i = 0;
                foreach(Autotile at in autotileArray.list)
                {
                    if(myNeighbours.IsEqual(at.neighbours, at.useCorners))
                    {
                        anim.PlayAndLoop(at.anim);
                        //Debug.Log($"b - animation {i}");
                    }
                    i++;
                }
            }
        }
    }


}
