
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System;
using TMPro;

namespace minigame
{
    public class MinigameUI : MonoBehaviour
    {
        public Minigame game;

        public RectTransform safeArea;
        public Image flashImage;
        public Text textDebugPanelLevelName;

        [Serializable]
        public class Variant
        {
            public GameObject parent;
            public GameObject container;
            public TextMeshProUGUI fruitCounterText;
            public RectTransform fruitCounterImageRT;
            public TextMeshProUGUI coinCounterText;
            public RectTransform[] silverCoins;
            public RectTransform pauseBox;
            public Image appleTVMenuIcon;
            public Image appleTVPlayIcon;
            public Text fpsCounter;
            public RectTransform powerupBox;
        }

        public Variant portrait;
        public Variant landscape;
        public PauseMenu pauseMenu;
        public DebugPanel debugPanel;

        private CanvasScaler canvasScaler;
        [HideInInspector] public UIFruit uiFruit;

        private float lastRealtime;
        private int framesCounted;


        private void Awake()
        {
            this.canvasScaler = this.GetComponent<CanvasScaler>();
            this.uiFruit = this.GetComponent<UIFruit>();
        }

        IEnumerator Start()
        {
            

            //Debug.Log("box:"+this.landscape.powerupBox.gameObject.activeInHierarchy);
            #if UNITY_TVOS
                this.landscape.appleTVMenuIcon.gameObject.SetActive(true);
                this.landscape.appleTVPlayIcon.gameObject.SetActive(true);
            #else
                this.landscape.appleTVMenuIcon.gameObject.SetActive(false);
                this.landscape.appleTVPlayIcon.gameObject.SetActive(false);
            #endif

            if(GameCamera.IsLandscape())
            {
                this.canvasScaler.referenceResolution = new Vector2(700, 400);
                this.portrait.parent.SetActive(false);
                this.landscape.parent.SetActive(true);
            }
            else
            {
                this.canvasScaler.referenceResolution = new Vector2(256, 400);
                this.portrait.parent.SetActive(true);
                this.landscape.parent.SetActive(false);
            }

            this.pauseMenu.Init();
            this.debugPanel.Init();

#if !UNITY_TVOS || UNITY_EDITOR
            this.safeArea.anchorMin = new Vector2(
                Screen.safeArea.min.x / Screen.width,
                Screen.safeArea.min.y / Screen.height
            );
            this.safeArea.anchorMax = new Vector2(
                Screen.safeArea.max.x / Screen.width,
                Screen.safeArea.max.y / Screen.height
            );

#if UNITY_IOS && !UNITY_EDITOR
            if (Screen.height == 2436 ||     // iPhone X, iPhone Xs, iPhone 11 Pro
                Screen.height == 1792 ||    // iPhone 11, iPhone Xr
                Screen.height == 2688 ||    // iPhone 11 Pro Max, iPhone Xs Max
                Screen.height == 2340 ||    // iPhone 12 mini
                Screen.height == 2532 ||    // iPhone 12, iPhone 12 Pro
                Screen.height == 2778)      // iPhone 12 Pro Max
            {
                this.safeArea.anchorMin += new Vector2(0.03f, -0.025f);
                this.safeArea.anchorMax += new Vector2(-0.03f, 0.045f);
            }
#endif
#endif
            // something keeps re-activating the powerup box -
            // can't find source, destroying instead
            if(this.game.GetMinigameType() != Minigame.Type.Golf)
            {
                Destroy(this.portrait.powerupBox.gameObject);
                Destroy(this.landscape.powerupBox.gameObject);
            }
            if (DebugPanel.uiStyle == DebugPanel.UIStyle.Clean ||
                Application.platform == RuntimePlatform.Switch)
            {
                this.landscape.pauseBox.gameObject.SetActive(false);
                if (this.landscape.powerupBox != null)
                {
                    var rt = this.landscape.powerupBox.GetComponent<RectTransform>();
                    rt.anchoredPosition = rt.anchoredPosition.Add(0, 32);
                }
            }

            yield return null;

            if (this.debugPanel.gameObject.activeSelf == true &&
                this.debugPanel.isExpanded == true &&
                DebugPanel.debugPanelEnabled == true)
            {
                this.game.SetPaused(true);
            }
        }

        private void Update()
        {
#if UNITY_EDITOR
            {
                if(GameCamera.IsLandscape())
                {
                    this.canvasScaler.referenceResolution = new Vector2(700, 400);
                    this.portrait.parent.SetActive(false);
                    this.landscape.parent.SetActive(true);
                }
                else
                {
                    this.canvasScaler.referenceResolution = new Vector2(256, 400);
                    this.portrait.parent.SetActive(true);
                    this.landscape.parent.SetActive(false);
                }
            }
#endif

            if (GameInput.pressedMenu == true && IsGamePaused() == false)
                OnClickPause();

            if(DebugPanel.fpsEnabled == true)
            {
                this.framesCounted += 1;
                if(this.framesCounted >= 8)
                {
                    this.framesCounted = 0;

                    float elapsed = (Time.realtimeSinceStartup - this.lastRealtime) / 8;
                    this.lastRealtime = Time.realtimeSinceStartup;

                    var ms = (elapsed * 1000).ToString("0.0");
                    var fps = Mathf.Round(1 / elapsed).ToString("0");
                    this.portrait.fpsCounter.text = this.landscape.fpsCounter.text =
                        $"<size=12>{fps}</size>FPS\n" +
                        $"<size=10>{ms}</size>MS\n";
                }
            }
            else if(this.portrait.fpsCounter.text != "" ||
                this.landscape.fpsCounter.text != "")
            {
                this.portrait.fpsCounter.text = "";
                this.landscape.fpsCounter.text = "";
            }
            UpdateVariant(this.landscape);
            UpdateVariant(this.portrait);
        }

        private void UpdateVariant(Variant ui)
        {
            if(Map.detailsToPersist != null){
                var value = Map.detailsToPersist.fruitCurrent.ToString();
                if(ui.fruitCounterText.text != value)
                    ui.fruitCounterText.text = value;

                value = Map.detailsToPersist.coins.ToString();
                if(ui.coinCounterText.text != value)
                    ui.coinCounterText.text = value;

                int amountSilverCoins = Map.detailsToPersist.silverCoins;
                for(int i = 0; i < ui.silverCoins.Length; i++)
                {
                    bool silverCoinActive = (i < amountSilverCoins);

                    ui.silverCoins[i].gameObject.SetActive(silverCoinActive);

                    float targetScale = silverCoinActive ? 1f : 0f;
                    float currentScale = Util.Slide(
                        ui.silverCoins[i].localScale.x, targetScale, 0.20f
                    );
                    ui.silverCoins[i].localScale = Vector3.one * currentScale;
                }
            }
        }

        public void OnClickPause()
        {
            this.game.SetPaused(true);
            this.pauseMenu.Open();
        }

        public bool IsGamePaused()
        {
            return this.pauseMenu.isOpen;
        }
        // Called by the DebugPanel toggle
        public void OnClickDebugPanelOpenClose()
        {
            this.game.SetPaused(this.debugPanel.isExpanded);
        }
        // Called by Resume Button in PauseMenu
        public void OnPressResume()
        {
            this.game.SetPaused(false);
        }
        // Replaces PauseMenu.OnPressDevConsole in minigames.
        public void OnPressDevConsole()
        {
            this.pauseMenu.isOpen = false;
            this.pauseMenu.gameObject.SetActive(false);
            this.textDebugPanelLevelName.text = this.game.fileLevel.name;
            DebugPanel.debugPanelEnabled = !DebugPanel.debugPanelEnabled;
            this.debugPanel.gameObject.SetActive(DebugPanel.debugPanelEnabled);
            StartCoroutine(DebugPanelExpandHack());
        }
        // Debug panel closes itself because Start() gets called after OnPressDevConsole when the scene is reloaded
        System.Collections.IEnumerator DebugPanelExpandHack()
        {
            yield return new WaitForEndOfFrame();
            this.debugPanel.Expand();
        }


        // Replaces Chunk up / down in DebugPanel
        public void NextLevel()
        {
            this.game.ExitGame();
            DebugPanel.debugPanelEnabled = false;
            Levels.GetNext(this.game.GetMinigameType(), ref Minigame.difficulty, ref Minigame.level);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        public void PrevLevel()
        {
            this.game.ExitGame();
            DebugPanel.debugPanelEnabled = false;
            Levels.GetPrev(this.game.GetMinigameType(), ref Minigame.difficulty, ref Minigame.level);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        public static bool IsPositionOverUI(Vector2 position)
        {
            var eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = position;
            var results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public void FlashFadeIn(float flashTime, AnimationCurve flashCurve)
        {
            StartCoroutine(_FlashFade(flashTime, 1f, flashCurve));
        }

        public void FlashFadeOut(float flashTime, AnimationCurve flashCurve)
        {
            StartCoroutine(_FlashFade(flashTime, 0f, flashCurve));
        }

        private System.Collections.IEnumerator _FlashFade(float targetTime, float targetAlpha, AnimationCurve fadeCurve)
        {
            float initialTime = 0f;
            Color initialColor = flashImage.color;
            Color targetColor = new Color(flashImage.color.r, flashImage.color.g, flashImage.color.b, targetAlpha);

            while(initialTime < targetTime)
            {
                initialTime += Time.deltaTime;
                flashImage.color = Color.Lerp(initialColor, targetColor, fadeCurve.Evaluate(initialTime / targetTime));
                yield return null;
            }
        }
    }
}

