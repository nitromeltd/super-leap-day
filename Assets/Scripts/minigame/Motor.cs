using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    public class Motor
    {
        public List<Tween> tweens;
        public Transform transform;
        public Rigidbody2D body;
        public Vector3 start;
        public Vector3 prev;
        public SpriteRenderer[] dots;
        public int index;
        public bool looped;
        public bool paused;
        public bool isLookAtTween;
        public float speed = 1f;

        public Motor(Transform transform, List<Tween> tweens = null, bool looped = true)
        {
            this.transform = transform;
            this.tweens = tweens;
            this.looped = looped;
        }

        public void AddTween(Tween tween)
        {
            this.tweens.Add(tween);
        }

        public void AddNode(Vector3 p, float delay, Tween.Ease ease)
        {
            if(this.tweens == null)
            {
                // first node
                this.start = p;
                this.tweens = new List<Tween>();
            }
            else
            {
                if(this.tweens.Count == 0)
                    this.tweens.Add(new Tween(this.start, p, delay, ease));
                else
                    this.tweens.Add(new Tween(this.tweens[this.tweens.Count - 1].finish, p, delay, ease));
            }
        }

        public void AddNode(Vector3 p, float delay, float quadPerc = 0)
        {
            if(this.tweens == null)
            {
                // first node
                this.start = p;
                this.tweens = new List<Tween>();
            }
            else
            {
                if(this.tweens.Count == 0)
                    this.tweens.Add(new Tween(this.start, p, delay, quadPerc));
                else
                    this.tweens.Add(new Tween(this.tweens[this.tweens.Count - 1].finish, p, delay, quadPerc));
            }
        }

        // Add tweens to backtrack to starting position
        public void AddBacktrackTweens()
        {
            var list = new List<Tween>(this.tweens);
            for(int i = this.tweens.Count - 1; i > -1; i--)
                list.Add(this.tweens[i].GetReversed());
            this.tweens = list;
        }

        public void Advance(float t)
        {
            if(this.paused) return;

            var tween = this.tweens[this.index];
            tween.count += t * this.speed;
            this.prev = this.transform.position;
            if(this.body != null)
                this.body.MovePosition(tween.Position());
            else
                this.transform.position = tween.Position();
            if(tween.count < 0 || tween.count >= tween.delay)
            {
                this.index += (int)Mathf.Sign(this.speed);

                var overflow = this.speed > 0 ? tween.count - tween.delay : tween.count;

                if(this.looped == true)
                {
                    if(this.index >= this.tweens.Count) this.index = 0;
                    if(this.index < 0) this.index = this.tweens.Count - 1;
                }

                if(this.index >= 0 && this.index < this.tweens.Count)
                {
                    tween = this.tweens[this.index];
                    tween.count = overflow;
                    if(this.speed < 0) tween.count += tween.delay;

                    if(this.body != null)
                        this.body.MovePosition(tween.Position());
                    else
                        this.transform.position = tween.Position();
                }
                else
                {
                    this.paused = true;
                }
            }

        }

        public void SetSpeed(float speed)
        {
            if(this.speed != speed)
            {
                if(speed == 0)
                {
                    this.paused = true;
                    return;
                }
                this.speed = speed;
                this.paused = false;
                if(speed > 0)
                {
                    if(this.index < 0)
                    {
                        this.index = 0;
                        this.tweens[this.index].count = 0;
                    }
                }
                else
                {
                    if(this.index >= this.tweens.Count)
                    {
                        this.index = this.tweens.Count - 1;
                        this.tweens[this.index].count = this.tweens[this.index].delay;
                    }
                }
            }
        }

        public bool Done()
        { 
            return !looped && (this.index >= this.tweens.Count || this.index < 0);
        }
    }
}
