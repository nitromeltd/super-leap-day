using System.Collections.Generic;
using UnityEngine;

public class MoveWithRigidBody : MonoBehaviour
{

    public List<Rigidbody2D> contactBodies;
    public Vector2 contactNormal = Vector2.down;
    public Vector2 prevPos;

    void Start()
    {
        this.prevPos = this.transform.localPosition;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        var rb = collision.collider.attachedRigidbody;
        if(rb != null)
        {
            var contact = collision.GetContact(0);
            if(contact.normal == this.contactNormal)
            {
                if(!this.contactBodies.Contains(rb))
                {
                    this.contactBodies.Add(rb);
                }
            }
        }
    }

    void FixedUpdate()
    {
        var moved = this.prevPos - (Vector2)transform.localPosition;
        if(this.contactBodies.Count > 0)
        {
            foreach(var b in this.contactBodies)
            {
                b.transform.localPosition -= (Vector3)moved;
            }
        }
        this.prevPos = this.transform.localPosition;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        var rb = collision.collider.attachedRigidbody;
        if(rb != null)
        {
            if(this.contactBodies.Contains(rb))
            {
                this.contactBodies.Remove(rb);
            }
        }
    }

}
