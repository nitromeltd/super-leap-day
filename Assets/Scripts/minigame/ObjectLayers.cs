using UnityEngine;


/* This asset deals with the problem of importing the minigames and assigning layers to all of their assets
 * 
 * All minigames refer to this asset as if it were a global constant for the sorting / collision layers they need
 * Set the properties to whatever sorting / collision layers you have avaiable to spare
 */
namespace minigame
{
    [CreateAssetMenu(menuName = "Minigame/ObjectLayers")]
    public class ObjectLayers : ScriptableObject
    {

        [Header("Collision Layers")]
        public LayerMask triggerLayerMask;
        public LayerMask tileLayerMask;
        public LayerMask objectLayerMask;
        public LayerMask playerLayerMask;

        [HideInInspector]
        public string backgroundSortingLayer = "Default";
        [HideInInspector]
        public string tilesSortingLayer = "Default";
        [HideInInspector]
        public string objectsSortingLayer = "Default";
        [HideInInspector]
        public string playerSortingLayer = "Default";
        [HideInInspector]
        public string foregroundSortingLayer = "Default";
        [HideInInspector]
        public string fxSortingLayer = "Default";
        [HideInInspector]
        public string uiSortingLayer = "Default";

        public static void SetSortingLayer(GameObject obj, string layer, int order = 0)
        {
            var sr = obj.GetComponent<SpriteRenderer>();
            if(sr != null)
            {
                sr.sortingLayerName = layer;
                sr.sortingOrder += order;
            }
            var mr = obj.GetComponent<MeshRenderer>();
            if(mr != null)
            {
                mr.sortingLayerName = layer;
                mr.sortingOrder += order;
            }
            foreach(Transform t in obj.transform)
                SetSortingLayer(t.gameObject, layer, order);
        }

        public static void SetLayerMask(GameObject obj, LayerMask layerMask, bool recurse = true)
        {
            obj.layer = ToLayer(layerMask.value);
            if(recurse)
            {
                foreach(Transform t in obj.transform)
                    SetLayerMask(t.gameObject, layerMask, recurse);
            }
        }

        public static int ToLayer(int bitmask)
        {
            int result = bitmask > 0 ? 0 : 31;
            while(bitmask > 1)
            {
                bitmask >>= 1;
                result++;
            }
            return result;
        }
    }

}