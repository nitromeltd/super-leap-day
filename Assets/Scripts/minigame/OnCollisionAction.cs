using System;
using UnityEngine;
using UnityEngine.Events;

namespace minigame
{
    // Triggers UnityEvent / Action in response to a hard collision (not a trigger)
    public class OnCollisionAction : MonoBehaviour
    {

        public Action<Collision2D> action;
        public UnityEvent unityEvent;

        void OnCollisionEnter2D(Collision2D collision)
        {
            this.action?.Invoke(collision);
            this.unityEvent.Invoke();
        }

    }

}
