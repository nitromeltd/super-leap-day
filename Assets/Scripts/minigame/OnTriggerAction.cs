using System;
using UnityEngine;
using UnityEngine.Events;

namespace minigame
{
    // Triggers UnityEvent / Action in response to an area overlap (trigger)
    public class OnTriggerAction : MonoBehaviour
    {
        public Action<Collider2D> action;
        public UnityEvent unityEvent;

        public LayerMask filter;

        void OnTriggerEnter2D(Collider2D collision)
        {
            if((this.filter.value & (1 << collision.gameObject.layer)) != 0)
            {
                this.action?.Invoke(collision);
                this.unityEvent.Invoke();
            }
        }
    }
}
