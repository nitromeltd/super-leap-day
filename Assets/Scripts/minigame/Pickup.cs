using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    /* Object that detects collision without Unity Physics
     * 
     * The plan is to let Unity Physics handle permanent GameObjects.
     * Let Pickup handle one-shots so gfx can be recycled.
     * 
     * Call Minigame.CircleCollidePickup to query pickupLookup hashmap.
     */
    public class Pickup
    {
        public string name;
        public SpriteRenderer spriteRenderer;
        public Animated anim;
        public Transform transform;
        public System.Action<Pickup> action;
        public bool moving;
        public Vector2 size;
        public Vector2 offset;
        public Vector2Int pos;
        public Shape shape;
        public float value;

        public enum Shape
        {
            Rect, Circle
        }

        public Pickup(string name, Animated anim, Vector2Int pos, Vector2 size, Shape shape, System.Action<Pickup> action)
        {
            this.name = name;
            this.transform = anim.transform;
            this.spriteRenderer = anim.GetComponent<SpriteRenderer>();
            this.anim = anim;
            this.pos = pos;
            this.size = size;
            this.shape = shape;
            this.action = action;
        }

        public Pickup(string name, SpriteRenderer spriteRenderer, Vector2Int pos, Vector2 size, Shape shape, System.Action<Pickup> action)
        {
            this.name = name;
            this.spriteRenderer = spriteRenderer;
            this.transform = spriteRenderer.transform;
            this.pos = pos;
            this.size = size;
            this.shape = shape;
            this.action = action;
        }

        public bool CollideCircle(Vector2 position, float radius)
        {
            Vector2 p = (Vector2)transform.position + offset;
            Minigame.DebugRect(new Rect(p - size * 0.5f, size), Color.red);
            switch(shape)
            {
                case Shape.Circle:
                    var r = this.size.x * 0.5f;
                    return (position - p).sqrMagnitude <= (radius + r) * (radius + r);
                case Shape.Rect:
                    var testX = position.x;
                    var testY = position.y;
                    if(testX < p.x - this.size.x * 0.5f) testX = p.x - this.size.x * 0.5f;
                    if(testX >= p.x + this.size.x * 0.5f) testX = p.x + this.size.x * 0.5f;
                    if(testY < p.y - this.size.y * 0.5f) testY = p.y - this.size.y * 0.5f;
                    if(testY >= p.y + this.size.y * 0.5f) testY = p.y + this.size.y * 0.5f;
                    return (p.x - testX) * (p.x - testX) + (p.y - testY) * (p.y - testY) < radius * radius;
            }
            return false;
        }

        public bool CollideRect(Rect rect)
        {
            Vector2 p = (Vector2)transform.position + offset;
            Minigame.DebugRect(new Rect(p - size * 0.5f, size), Color.red);
            switch(shape)
            {
                case Shape.Rect:
                    return
                    (
                        rect.x < p.x + this.size.x &&
                        rect.y < p.y + this.size.y &&
                        p.x - this.size.x < rect.x + rect.width &&
                        p.y - this.size.y < rect.y + rect.height
                    );
                case Shape.Circle:
                    var r = this.size.x * 0.5f;
                    var testX = p.x;
                    var testY = p.y;
                    if(testX < rect.x) testX = rect.x;
                    if(testX >= rect.x + rect.width) testX = rect.x + rect.width;
                    if(testY < rect.y) testY = rect.y;
                    if(testY >= rect.y + rect.height) testY = rect.y + rect.height;
                    return (p.x - testX) * (p.x - testX) + (p.y - testY) * (p.y - testY) < r * r;
            }
            return false;
        }

        public void Iterate(System.Action<Vector2Int> perCell)
        {
            // Scale is set to 1f in all the minigames so we're going to skip scaling the gfx
            Vector2 p = (Vector2)transform.position + offset;
            int xMin = Mathf.FloorToInt(p.x - size.x * 0.5f);
            int yMin = Mathf.FloorToInt(p.y - size.y * 0.5f);
            int xMax = Mathf.CeilToInt(p.x + size.x * 0.5f);// exclusive
            int yMax = Mathf.CeilToInt(p.y + size.y * 0.5f);// exclusive

            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                {
                    perCell(new Vector2Int(c, r));
                    Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                }
        }

        public void AddToHashMap(Dictionary<Vector2Int, List<Pickup>> hashMap)
        {
            void AddCell(Vector2Int p)
            {
                hashMap.TryGetValue(p, out List<Pickup> list);
                if(list == null) list = new List<Pickup>();
                list.Add(this);
                hashMap[p] = list;
            }
            Iterate(AddCell);
        }

        public void RemoveFromHashMap(Dictionary<Vector2Int, List<Pickup>> hashMap)
        {
            void RemoveCell(Vector2Int p)
            {
                hashMap.TryGetValue(p, out List<Pickup> list);
                if(list != null)
                {
                    list.Remove(this);
                    if(list.Count == 0) hashMap.Remove(p);
                }
            }
            Iterate(RemoveCell);
        }

        public virtual void Destruct(Minigame game, PickupMap pickupMap)
        {
            if(this.moving == false) RemoveFromHashMap(pickupMap.hashMap);
            else game.RemoveMotor(this.transform);

            if(this.anim != null)
            {
                GameObject.Destroy(this.anim.gameObject);
            }
            else if(this.spriteRenderer != null)
            {
                game.spriteRendererPool.Put(this.spriteRenderer);
            }
        }

    }

}
