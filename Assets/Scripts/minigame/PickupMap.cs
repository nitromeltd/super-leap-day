using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    // Grid of buckets containing Pickups and utilities
    public class PickupMap
    {
        public Minigame game;


        // Pickup definitions
        public Dictionary<string, bool> names;
        public Dictionary<string, Vector2> sizes;
        public Dictionary<Vector2Int, List<Pickup>> hashMap;// grid of buckets
        public Dictionary<Vector2Int, bool> hide;// Pickups we don't want to recollect on Retry
        public List<Pickup> moving;// Pickups on rails
        public System.Action<Pickup> onPickupCollected;

        public static string[] NamesGlobal = new string[]
        {
            "coin"
            ,"silver_coin"
            ,"apple"
            ,"banana"
            ,"cherry"
            ,"kiwi"
            ,"lemon"
            ,"melon"
            ,"orange"
            ,"pear"
            ,"pineapple"
            ,"strawberry"
        };
        public static string[] FruitRandom = new string[]
        {
            "apple"
            ,"banana"
            ,"cherry"
            ,"kiwi"
            ,"lemon"
            ,"melon"
            ,"orange"
            ,"pear"
            ,"pineapple"
            ,"strawberry"
        };
        public static Dictionary<string, bool> IsFruit = new Dictionary<string, bool>
        {
            ["apple"] = true
            , ["banana"] = true
            , ["cherry"] = true
            , ["kiwi"] = true
            , ["lemon"] = true
            , ["melon"] = true
            , ["orange"] = true
            , ["pear"] = true
            , ["pineapple"] = true
            , ["strawberry"] = true
        };

        public PickupMap(Minigame game)
        {
            this.game = game;
            this.hide = new Dictionary<Vector2Int, bool>();
            this.hashMap = new Dictionary<Vector2Int, List<Pickup>>();
            this.moving = new List<Pickup>();
            this.names = new Dictionary<string, bool>();
            this.sizes = new Dictionary<string, Vector2>();

            foreach(var name in NamesGlobal)
            {
                var globalName = "GLOBAL:" + name;
                this.names[globalName] = true;
                if(name == "coin")
                    this.sizes[globalName] = new Vector2(1.8f, 1.8f);
                else
                    this.sizes[globalName] = Vector2.one;
            }
        }

        public void Clear()
        {
            this.hashMap.Clear();
            this.moving.Clear();
        }


        public void CollectPickup(Pickup pickup)
        {
            onPickupCollected?.Invoke(pickup);

            pickup.Destruct(this.game, this);
            switch(pickup.name)
            {
                case "GLOBAL:coin":
                    Map.detailsToPersist.coins += 1;
                    this.hide[pickup.pos] = true;
                    break;
                case "GLOBAL:silver_coin":
                    Map.detailsToPersist.silverCoins += 1;
                    if(Map.detailsToPersist.silverCoins >= 10)
                    {
                        Map.detailsToPersist.silverCoins = 0;
                        Map.detailsToPersist.coins += 1;
                    }
                    this.hide[pickup.pos] = true;
                    break;
                default:
                    if(IsFruit.ContainsKey(pickup.name.Substring(Minigame.GlobalPrefix.Length)))
                    {
                        Map.detailsToPersist.fruitCurrent += 1;
                        this.hide[pickup.pos] = true;
                    }
                    break;
            }
            var collectName = pickup.name + "_collect";
            var particle = this.game.ParticleCreateAndPlayOnce(collectName, pickup.transform.position);
            particle.gameObject.layer = ObjectLayers.ToLayer(this.game.objectLayers.objectLayerMask);

            //Debug.Log("collect " + pickup.name + " " + collectName);
        }

        public void CreatePickup(NitromeEditor.TileLayer.Tile tile, Vector2Int p, Vector2 pos, Dictionary<Vector2Int, Pickup> pickupLookup, ref GameObject obj, Vector2 offset)
        {
            if(this.hide.ContainsKey(p)) return;

            var pickup = this.CreatePickup(tile, p, pos + offset);
            pickupLookup[p] = pickup;
            obj = pickup.transform.gameObject;
            obj.layer = ObjectLayers.ToLayer(this.game.objectLayers.objectLayerMask);
        }

        public Pickup CreatePickup(NitromeEditor.TileLayer.Tile tile, Vector2Int p, Vector2 pos)
        {
            var pickup = new Pickup
            (
                tile.tile,
                this.game.CreateAnimated(this.game.assets.animationLookup[tile.tile], pos, this.game.transform, this.game.objectLayers.objectsSortingLayer),
                p,
                this.sizes[tile.tile],
                Pickup.Shape.Circle,
                CollectPickup
            );
            pickup.spriteRenderer.sortingOrder = this.game.spriteRendererPool.sortingOrderCount++;
            pickup.AddToHashMap(this.hashMap);
            return pickup;
        }

        public Pickup CreatePickup(NitromeEditor.TileLayer.Tile tile, GameObject obj, Vector2Int p, Dictionary<Vector2Int, Pickup> pickupLookup)
        {
            var pickup = new Pickup
            (
                tile.tile,
                obj.GetComponent<Animated>(),
                p,
                this.sizes[tile.tile],
                Pickup.Shape.Circle,
                CollectPickup
            );
            pickup.spriteRenderer.sortingOrder = this.game.spriteRendererPool.sortingOrderCount++;
            pickup.AddToHashMap(this.hashMap);
            pickupLookup[p] = pickup;
            return pickup;
        }


        // Look for a circle collision with the hashMap
        public void CircleCollidePickup(Vector2 position, float radius)
        {
            // WARNING: Unity physics executes a frame late - use debug rects to verify
            // Assume Scale is 1f - saves us a bit of math
            //Minigame.DebugRect(new Rect(position + new Vector2(-radius, -radius), Vector2.one * radius * 2), Color.white);
            int xMin = Mathf.FloorToInt(position.x - radius);
            int yMin = Mathf.FloorToInt(position.y - radius);
            int xMax = Mathf.CeilToInt(position.x + radius);// exclusive
            int yMax = Mathf.CeilToInt(position.y + radius);// exclusive
            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                {
                    var p = new Vector2Int(c, r);
                    this.hashMap.TryGetValue(p, out List<Pickup> list);
                    //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                    //Debug.Log(c+" "+r);
                    if(list != null)
                    {
                        for(int i = list.Count - 1; i > -1; i--)
                        {
                            var pickup = list[i];
                            if(pickup.CollideCircle(position, radius))
                            {
                                // Assuming Pickup is removed in action - preventing multi-action
                                pickup.action?.Invoke(pickup);
                                //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.green);
                                SfxPickupCoinOrFruit(pickup.name);
                            }
                        }
                    }
                }
            for(int i = this.moving.Count - 1; i > -1; i--)
            {
                var pickup = this.moving[i];
                if(pickup.CollideCircle(position, radius))
                {
                    pickup.action?.Invoke(pickup);
                    SfxPickupCoinOrFruit(pickup.name);
                    this.moving.RemoveAt(i);
                }
            }
        }

        // Look for a rect collision with the pickupHashMap
        public void RectCollidePickup(Vector2 position, Vector2 size)
        {
            // WARNING: Unity physics executes a frame late - use debug rects to verify
            // Assume Scale is 1f - saves us a bit of math
            var rect = new Rect(position - size * 0.5f, size);
            //Minigame.DebugRect(rect, Color.black);
            int xMin = Mathf.FloorToInt(position.x - size.x * 0.5f);
            int yMin = Mathf.FloorToInt(position.y - size.y * 0.5f);
            int xMax = Mathf.CeilToInt(position.x + size.x * 0.5f);// exclusive
            int yMax = Mathf.CeilToInt(position.y + size.y * 0.5f);// exclusive
            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                {
                    var p = new Vector2Int(c, r);
                    this.hashMap.TryGetValue(p, out List<Pickup> list);
                    //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                    if(list != null)
                    {
                        for(int i = list.Count - 1; i > -1; i--)
                        {
                            var pickup = list[i];
                            //Minigame.DebugRect(rect, Color.black);
                            if(pickup.CollideRect(rect))
                            {
                                // Assuming Pickup is removed in action - preventing multi-action
                                pickup.action?.Invoke(pickup);
                                //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.green);
                                SfxPickupCoinOrFruit(pickup.name);
                            }
                        }
                    }
                }
            for(int i = this.moving.Count - 1; i > -1; i--)
            {
                var pickup = this.moving[i];
                if(pickup.CollideRect(rect))
                {
                    pickup.action?.Invoke(pickup);
                    SfxPickupCoinOrFruit(pickup.name);
                    this.moving.RemoveAt(i);
                }
            }
        }

        public void SfxPickupCoinOrFruit(string name)
        {
            var sfx = "GLOBAL:sfx_collect_fruit";
            if(name.Contains("coin")) sfx = "GLOBAL:sfx_collect_coin";
            Audio.instance.PlaySfx(this.game.assets.audioClipLookup[sfx]);
        }

        public static List<Vector2Int> GetCircleCollideCells<T>(Dictionary<Vector2Int, T> dict, Vector2 position, float radius)
        {
            int xMin = Mathf.FloorToInt(position.x - radius);
            int yMin = Mathf.FloorToInt(position.y - radius);
            int xMax = Mathf.CeilToInt(position.x + radius);// exclusive
            int yMax = Mathf.CeilToInt(position.y + radius);// exclusive
            var result = new List<Vector2Int>();
            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                {
                    //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                    var p = new Vector2Int(c, r);
                    if(dict.ContainsKey(p))
                    {
                        //Minigame.DebugRect(new Rect(position - radius * (Vector2.right + Vector2.up), radius * 2 * (Vector2.right + Vector2.up)), Color.red);
                        var testX = position.x;
                        var testY = position.y;
                        var pos = new Vector2(p.x + 0.5f, p.y + 0.5f);
                        if(testX < p.x) testX = p.x;
                        if(testX >= p.x + 1) testX = p.x + 1;
                        if(testY < p.y) testY = p.y;
                        if(testY >= p.y + 1) testY = p.y + 1;
                        if((position.x - testX) * (position.x - testX) + (position.y - testY) * (position.y - testY) < radius * radius == true)
                        {
                            result.Add(p);
                            //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.red);

                        }
                        //Debug.DrawLine(new Vector2(testX, testY), position, Color.blue);
                    }
                }
            return result;
        }

        public static List<Vector2Int> GetRectCollideCells<T>(Dictionary<Vector2Int, T> dict, Vector2 position, Vector2 size)
        {
            var rect = new Rect(position - size * 0.5f, size);
            //Minigame.DebugRect(rect, Color.white);
            int xMin = Mathf.FloorToInt(position.x - size.x * 0.5f);
            int yMin = Mathf.FloorToInt(position.y - size.y * 0.5f);
            int xMax = Mathf.CeilToInt(position.x + size.x * 0.5f);// exclusive
            int yMax = Mathf.CeilToInt(position.y + size.y * 0.5f);// exclusive

            var result = new List<Vector2Int>();
            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                {
                    //Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                    var p = new Vector2Int(c, r);
                    if(dict.ContainsKey(p))
                    {
                        //Minigame.DebugRect(new Rect(position - size * 0.5f, size), Color.red);
                        result.Add(p);
                    }
                }
            return result;
        }

        public static void ForCellsInRect(Rect rect, System.Action<Vector2Int> action)
        {
            int xMin = Mathf.FloorToInt(rect.x);
            int yMin = Mathf.FloorToInt(rect.y);
            int xMax = Mathf.CeilToInt(rect.xMax);// exclusive
            int yMax = Mathf.CeilToInt(rect.yMax);// exclusive
            for(int r = yMin; r < yMax; r++)
                for(int c = xMin; c < xMax; c++)
                    action(new Vector2Int(c, r));
        }

    }

}
