using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAnimated : MonoBehaviour
{
    public Animated.Animation anim;
    public Animated animated;
    public bool eventTrigger;

    void Start()
    {
        if(eventTrigger == false)
            this.animated.PlayAndLoop(anim);
    }

    public void PlayOnce()
    {
        this.animated.PlayOnce(anim);
    }
}
