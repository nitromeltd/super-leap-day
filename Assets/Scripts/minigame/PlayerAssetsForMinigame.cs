using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{

    [CreateAssetMenu(menuName = "Minigame/PlayerAssetsForMinigame")]
    public class PlayerAssetsForMinigame : ScriptableObject
    {
        // golf squat/jump animations are in golf/PlayerStand.cs

        [Serializable]
        public class PlayerAssets
        {
            public Sprite spriteBall;
            public Sprite spriteDriving;
            public Sprite spriteDrivingInFront;
            public Sprite spriteDrivingArm;
            public Animated.Animation[] animationsDeath;
            public AudioClip sfxDeath;
        };
        public PlayerAssets yolk;
        public PlayerAssets puffer;
        public PlayerAssets goop;
        public PlayerAssets sprout;
        public PlayerAssets king;

        public PlayerAssets AssetsForCharacter(Character character) =>
            character switch
            {
                Character.Puffer => this.puffer,
                Character.Goop => this.goop,
                Character.Sprout => this.sprout,
                Character.King => this.king,
                _ => this.yolk,
            };

        public static void Death(Vector2 position, Minigame game)
        {
            var playerAssets = game.playerAssets.AssetsForCharacter(game.character);
            var deathParticle = Particle.CreateAndPlayOnce(playerAssets.animationsDeath[0], position, game.transform);
            //ObjectLayers.SetLayerMask(deathParticle.gameObject, game.objectLayers.playerLayerMask);
            Audio.instance.PlaySfx(playerAssets.sfxDeath);

            switch(game.character)
            {
                case Character.Sprout:
                    game.DelayedCall(0.20f, delegate
                    {
                        Vector2 headPosition = position.Add(0, 1f);
                        Particle headSpin = Particle.CreatePlayAndLoop(
                            playerAssets.animationsDeath[2],
                            60 * 5,
                            headPosition,
                            game.transform
                        );

                        headSpin.velocity = Vector2.up * 0.10f;
                        headSpin.acceleration = Vector2.down * 0.005f;
                        headSpin.spriteRenderer.sortingLayerName = "Items (Front)";
                        headSpin.spriteRenderer.sortingOrder = 1;

                        Vector2 GetRandomPetalVelocity()
                        {
                            return new Vector2(
                                UnityEngine.Random.Range(0.01f, 0.03f),
                                UnityEngine.Random.Range(0.005f, 0.03f)
                            );
                        }

                        int numberPetals = 4;
                        Vector2 savedPetalVelocity = Vector2.zero;

                        for(int i = 0; i < numberPetals; i++)
                        {
                            float direction = i % 2 == 0 ? 1f : -1f;
                            Vector2 velocity = Vector2.zero;

                            if(i % 2 == 0)
                                velocity = savedPetalVelocity = GetRandomPetalVelocity();
                            else
                                velocity = savedPetalVelocity;

                            Particle petalParticle = Particle.CreateWithSprite(
                                game.assets.spriteLookup[Minigame.GlobalPrefix + "death_petal"],
                                60,
                                headPosition,
                                game.transform
                            );

                            petalParticle.velocity = new Vector2(velocity.x * direction, -velocity.y);
                            petalParticle.FadeOut(game.assets.animationCurveLookup[Minigame.GlobalPrefix + "petal_fade_out"]);
                            petalParticle.transform.localScale = new Vector3(-direction, 1f, 1f);
                            petalParticle.spriteRenderer.sortingLayerName = "Items (Front)";
                            petalParticle.spriteRenderer.sortingOrder = 2;
                        }
                    });
                    Audio.instance.PlaySfx(playerAssets.sfxDeath);
                    break;

                case Character.Goop:
                    game.DelayedCall(0.40f, delegate
                    {
                        float x = position.x;
                        float y = position.y;

                        float circleRadius = 1f;
                        int particlesAmount = 16;
                        Vector2 originPosition = new Vector2(x, y);

                        for(var n = 0; n < particlesAmount; n += 1)
                        {
                            float currentAngle = (n * Mathf.PI * 2) / particlesAmount;
                            Vector2 offset =
                                new Vector2(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle)) * circleRadius;
                            Vector2 spawnPos = originPosition + offset;

                            var p = Particle.CreateWithSprite(
                                RandomUtil.Pick(game.assets.spriteArrayLookup[Minigame.GlobalPrefix + "goop_particles_big"].list),
                                60 * 2,
                                spawnPos,
                                game.transform
                            );
                            p.velocity.x =
                                (((spawnPos.x > originPosition.x) ? 1 : -1) *
                                UnityEngine.Random.Range(0.03f, 0.20f));
                            p.velocity.y = UnityEngine.Random.Range(0.17f, 0.30f);

                            float verticalAccelaration = -0.027f;
                            p.acceleration = new Vector2(0f, verticalAccelaration);
                        }
                    });
                    break;
                case Character.Puffer:
                    {
                        var headPos = position + Vector2.up * 0.4f;
                        var headParticle = Particle.CreateAndPlayOnce(playerAssets.animationsDeath[2], headPos, game.transform);
                        headParticle.spriteRenderer.sortingOrder += 1;
                        Particle popParticle = Particle.CreateAndPlayOnce(
                            playerAssets.animationsDeath[1],
                            position.Add(-0.15f, 0.60f),
                            game.transform
                        );
                        popParticle.spriteRenderer.sortingOrder += 2;
                        game.DelayedCall(0.4f, delegate {
                            game.coroutines.Add(game.StartCoroutine(SpawnDeflatedHead(headPos, game, headParticle)));
                        });

                    }
                    break;

                default:
                    break;
            }
        }

        public static void GoopParticles(Vector2 position, Vector2 normal, float power, Minigame game)
        {
            float x = position.x;
            float y = position.y;

            float circleRadius = 1f;
            int particlesAmount = Mathf.CeilToInt(10 * power);
            Vector2 originPosition = new Vector2(x, y);
            Quaternion rotation = Quaternion.LookRotation(Vector3.forward, normal);

            for(var n = 0; n < particlesAmount; n += 1)
            {
                float currentAngle = (n * Mathf.PI * 2) / particlesAmount;
                Vector2 offset =
                    new Vector2(Mathf.Sin(currentAngle), Mathf.Cos(currentAngle)) * circleRadius;
                Vector2 spawnPos = originPosition + (Vector2)(rotation * offset);

                var p = Particle.CreateWithSprite(
                    RandomUtil.Pick(game.assets.spriteArrayLookup[Minigame.GlobalPrefix + "goop_particles_small"].list),
                    60 * 2,
                    spawnPos,
                    game.transform
                );
                p.velocity.x =
                    (((spawnPos.x > originPosition.x) ? 1 : -1) *
                    UnityEngine.Random.Range(0.03f, 0.20f));
                p.velocity.y = UnityEngine.Random.Range(0.17f, 0.30f);
                p.velocity = rotation * p.velocity;
                p.velocity *= power;
                float verticalAccelaration = -0.027f;
                p.acceleration = new Vector2(0f, verticalAccelaration);
            }
        }

        private static IEnumerator SpawnDeflatedHead(Vector2 position, Minigame game, Particle headParticle)
        {
            headParticle.StopAndReturnToPool();

            var playerAssets = game.playerAssets.AssetsForCharacter(Character.Puffer);
            var deflatedHeadInitialPosition = position.Add(0.30f, 1.10f);
            var deflatedHeadLastPosition = deflatedHeadInitialPosition;

            var deflatedHeadParticle = Particle.CreatePlayAndLoop(
                playerAssets.animationsDeath[3],
                180,
                deflatedHeadInitialPosition,
                game.transform
            );

            deflatedHeadParticle.transform.localScale =
                new Vector3(1f, 1f, 1f);

            deflatedHeadParticle.velocity = Vector2.up * 0.05f;

            // reset particle variables
            var deflatedHeadTimer = 0f;
            var deflatedHeadVelocity = Vector3.zero;
            var frameN = 0;

            while(frameN < 180)
            {
                float period = 7;
                float amplitude = 2f + (4f * Mathf.InverseLerp(60f, 0f, deflatedHeadTimer));

                float theta = deflatedHeadTimer / period;
                float distance = amplitude * Mathf.Sin(theta);
                deflatedHeadTimer += 1;

                float hPosition = deflatedHeadInitialPosition.x + distance;
                float vAcceleration = 0.01f; //0.005f;

                deflatedHeadVelocity += new Vector3(0f, vAcceleration);
                deflatedHeadParticle.transform.position += deflatedHeadVelocity;
                deflatedHeadParticle.transform.position = new Vector2(
                    hPosition, deflatedHeadParticle.transform.position.y
                );

                Vector2 deflatedHeadDirection =
                    ((Vector2)deflatedHeadParticle.transform.position - deflatedHeadLastPosition).normalized;
                deflatedHeadLastPosition = deflatedHeadParticle.transform.position;

                deflatedHeadParticle.transform.localRotation =
                    Quaternion.LookRotation(Vector3.forward, deflatedHeadDirection);

                if(frameN % 2 == 0)
                {
                    float x = deflatedHeadParticle.transform.position.x;
                    float y = deflatedHeadParticle.transform.position.y;

                    int lifetime = UnityEngine.Random.Range(25, 35);

                    Particle p = Particle.CreatePlayAndHoldLastFrame(
                        playerAssets.animationsDeath[4],
                        lifetime,
                        new Vector2(
                            x + UnityEngine.Random.Range(-0.2f, 0.2f),
                            y + UnityEngine.Random.Range(-0.2f, 0.2f)
                        ),
                        game.transform
                    );
                    float s = UnityEngine.Random.Range(0.75f, 1f);
                    p.transform.localScale = new Vector3(s, s, 1);

                    p.spriteRenderer.sortingLayerName = "Enemies";
                    p.spriteRenderer.sortingOrder = -2;
                }
                frameN++;
                yield return null;
            }
        }
    }
}
