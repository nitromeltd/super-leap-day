using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RandomUtil
{
    public static Vector2 InsideRect(Rect rect)
    {
        // Random.value is inclusive 0.0 -> inclusive 1.0
        // we need to sanitise 1.0 outputs
        var x = rect.x + Random.Range(0, rect.width);
        if(x == rect.xMax) x -= Mathf.Epsilon;
        var y = rect.y + Random.Range(0, rect.height);
        if(y == rect.yMax) y -= Mathf.Epsilon;
        return new Vector2(x, y);
    }

    public static Vector2 Normal()
    {
        return Quaternion.Euler(0, 0, Random.value * 360f) * Vector2.up;
    }

    public static Vector2[] Radial(int total, float radius = 1f)
    {
        var list = new Vector2[total];
        var a = Random.value * 360f;
        var step = 360f / total;
        for(int i = 0; i < total; i++)
            list[i] = Quaternion.Euler(0, 0, a + i * step) * Vector2.up * radius;
        return list;
    }

    public static Vector2 Range(Vector2 min, Vector2 max)
    {
        return new Vector2(Random.Range(min.x, max.x), Random.Range(min.y, max.y));
    }

    //==================
    // Generic selection
    //==================

    /* Pick one from a list */
    public static T Pick<T>(List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }

    /* Pick one from a list */
    public static T Pick<T>(params T[] list)
    {
        return list[Random.Range(0, list.Length)];
    }

    /* Pick one from a list when Predicate returns true 
	 * 
	 * eg, pick only a random 3: rng.PickOnly(myInts, (n) => {return n == 3});
	 */
    public static T PickOnly<T>(List<T> list, System.Predicate<T> condition)
    {
        var n = Random.Range(0, list.Count);
        var total = list.Count;
        for(int i = 0; i < total; i++)
        {
            T item = list[(i + n) % total];
            if(condition(item))
            {
                return item;
            }
        }
        return default(T);
    }

    /* Pick one from an array when Predicate returns true 
	 * 
	 * eg, pick only a random 3: rng.PickOnly(myInts, (n) => {return n == 3});
	 */
    public static T PickOnly<T>(T[] list, System.Predicate<T> condition)
    {
        var n = Random.Range(0, list.Length);
        var total = list.Length;
        for(int i = 0; i < total; i++)
        {
            T item = list[(i + n) % total];
            if(condition(item))
            {
                return item;
            }
        }
        return default(T);
    }

    /*Pick one from a list and remove it from the list */
    public static T Pluck<T>(List<T> list)
    {
        var n = Random.Range(0, list.Count);
        T item = list[n];
        list.RemoveAt(n);
        return item;
    }

    /* Pick one from a list and remove it from the list when Predicate returns true 
	 * 
	 * eg, pluck only a random 3: rng.PluckOnly(myInts, (n) => {return n == 3});
	 */
    public static T PluckOnly<T>(List<T> list, System.Predicate<T> condition)
    {
        var n = Random.Range(0, list.Count);
        var total = list.Count;
        for(int i = 0; i < total; i++)
        {
            var index = (i + n) % total;
            T item = list[index];
            if(condition(item))
            {
                list.RemoveAt(index);
                return item;
            }
        }
        return default(T);
    }

    /* Pluck until empty then refill */
    public static T Draw<T>(List<T> list, ICollection<T> refill)
    {
        if(list.Count == 0) list.AddRange(refill);
        return Pluck(list);
    }

    /* PluckOnly until empty then refill */
    public static T DrawOnly<T>(List<T> list, ICollection<T> refill, System.Predicate<T> condition)
    {
        if(list.Count == 0) list.AddRange(refill);
        return PluckOnly(list, condition);
    }

    public static List<T> Shuffle<T>(List<T> list)
    {
        T x;
        for(int j, i = list.Count; i > 0; j = Random.Range(0, list.Count), x = list[--i], list[i] = list[j], list[j] = x) ;
        return list;
    }

    public static T[] Shuffle<T>(T[] list)
    {
        T x;
        for(int j, i = list.Length; i > 0; j = Random.Range(0, list.Length), x = list[--i], list[i] = list[j], list[j] = x) ;
        return list;
    }

    // weaker shuffle - roughly the same order
    public static List<T> Jumble<T>(List<T> list, int switches = 0)
    {
        if(switches == 0) switches = list.Count;
        T x;
        while(switches-- > 0)
        {
            var i = Random.Range(0, list.Count - 1);
            x = list[i];
            list[i] = list[i + 1];
            list[i + 1] = x;
        }
        return list;
    }
}
