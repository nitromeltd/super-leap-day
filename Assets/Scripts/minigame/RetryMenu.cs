using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;

namespace minigame
{
    public class RetryMenu : MonoBehaviour
    {
        public Minigame game;
        public LocalizeText titleText;
        public LocalizeText subtitleText;
        public TMPro.TMP_Text totalText;
        public TMPro.TMP_Text totalFruitText;
        public TMPro.TMP_Text continueCostText;
        public RectTransform transformCenterIn;
        [NonSerialized] public Material circleMaterial;
        public RectTransform circleImageTransform;
        public RectTransform uiTransform;
        public RectTransform retryButton;
        public RectTransform exitButton;
        public SelectionCursor selectionCursor;
        public CircleText[] circleTexts;

        public AnimationCurve circleInAnimationCurve;
        public AnimationCurve buttonInAnimationCurve;

        public bool isOpen = false;
        public float circleOpenAmount = 1;
        public float circleClosedAmount = 0;
        private bool chargedCost = false;

        private Color totalColorDefault;
        private Vector3[] positionButtons;

        public static int Radius = Shader.PropertyToID("_Radius");
        public static int OutlineThickness = Shader.PropertyToID("_OutlineThickness");

        public static int FruitCost = 10;
        public static int CoinCost = 10;

        private bool isBonusMusicPlaying = true;

        private void Start()
        {
            var circleImage = this.circleImageTransform.GetComponent<Image>();
            this.circleMaterial = circleImage.material = new Material(circleImage.material);
            this.circleMaterial.SetFloat(Radius, 0.5f);
            totalColorDefault = totalText.color;
            var buttons = new[]
            {
                exitButton, retryButton
            };
            positionButtons = new Vector3[buttons.Length];
            for(int n = 0; n < positionButtons.Length; n++)
            {
                positionButtons[n] = buttons[n].localPosition;
            }
            this.gameObject.SetActive(false);

            this.selectionCursor.gameObject.SetActive(false);

            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                CircleText.DisplaceByTextHeight(new UnityEngine.Object[] { titleText, subtitleText, totalText });

        }

        public void Open()
        {
            var buttons = new[]
            {
                exitButton, retryButton
            };

            int costSymbolSprite = 0;
            int costIconSprite = 0;
            int cost = FruitCost;
            int total = Map.detailsToPersist.fruitCurrent;

            if(Minigame.mode == Minigame.Mode.Arcade)
            {
                costSymbolSprite += 2;
                costIconSprite += 1;
                cost = CoinCost;
                total = Map.detailsToPersist.coins;
            }

            if(cost <= total)
            {
                this.titleText.SetText("retry?");
                this.subtitleText.SetText(Minigame.RetrySubtitle);
                totalText.color = totalColorDefault;
                retryButton.gameObject.SetActive(true);
                for(int n = 0; n < buttons.Length; n++)
                {
                    buttons[n].localPosition = positionButtons[n];
                }
            }
            else
            {
                this.titleText.SetText("fail!");
                this.subtitleText.SetText("not enough fruit!");
                totalText.color = totalFruitText.color = new Color32(251, 4, 4, 255);// apple red
                costSymbolSprite++;
                retryButton.gameObject.SetActive(false);
                exitButton.localPosition = (buttons[0].localPosition + buttons[1].localPosition) / 2;
            }

            totalFruitText.text = $"<sprite={costSymbolSprite}>{total}";
            continueCostText.text = $"<sprite={costIconSprite}><size=125%><rotate=45>+</rotate></size>" + cost;

            this.isOpen = true;
            this.gameObject.SetActive(true);
            StartCoroutine(OpenAnimation());
        }

        private IEnumerator OpenAnimation()
        {
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_lose_in"], options: new Audio.Options(1f, false, 0f));
            StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: 0.1f, audioClip: game.music));
            this.isBonusMusicPlaying = false;
            StartCoroutine(PlayMusicIn(6f));
            var buttons = new[]
            {
                exitButton, retryButton
            };
            this.titleText.transform.rotation = Quaternion.Euler(0, 0, 175);
            this.subtitleText.transform.rotation = Quaternion.Euler(0, 0, 175);

            this.transformCenterIn.localScale = new Vector3(0, 0, 1);
            foreach(var btn in buttons)
                btn.localScale = new Vector3(0, 0, 1);

            var easeCircle = Easing.ByAnimationCurve(this.buttonInAnimationCurve);
            var easeButtons = Easing.ByAnimationCurve(this.buttonInAnimationCurve);

            var tweener = new Tweener();
            var tweenForCircle = tweener.TweenFloat(1, 0, 1, easeCircle);
            tweener.ScaleLocal(this.transformCenterIn, Vector3.one, 1, easeCircle);
            tweener.RotateLocal(this.titleText.transform, 0, 0.4f, Easing.QuadEaseOut, 0.5f);
            tweener.RotateLocal(this.subtitleText.transform, 0, 0.5f, Easing.QuadEaseOut, 0.5f);
            for(int n = 0; n < buttons.Length; n += 1)
            {
                var delay = 0.8f + (0.05f * n);
                tweener.ScaleLocal(buttons[n], Vector3.one, 0.5f, easeButtons, delay);
            }
            float timer = 0;
            bool hasPlayedOnce = false;
            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleOpenAmount = tweenForCircle.Value();
                UpdateCircleSize();
                timer += Time.unscaledDeltaTime;
                if (timer > .35f && hasPlayedOnce == false)
                {
                    hasPlayedOnce = true;
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_out"], options: new Audio.Options(1f, false, 0f));
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:FailJingle"], options: new Audio.Options(1f, false, 0f));
                }
                yield return null;
            }

            this.selectionCursor.gameObject.SetActive(true);
            this.selectionCursor.Select(
                this.retryButton.gameObject.activeSelf == true ?
                    this.retryButton.GetComponent<Selectable>() :
                    this.exitButton.GetComponent<Selectable>(),
                false
            );
        }

        private IEnumerator ExitGameAnimation()
        {
            Audio.instance.StopSfx(game.assets.audioClipLookup["GLOBAL:sfx_lose_in"]);
            Audio.instance.StopSfx(game.assets.audioClipLookup["GLOBAL:FailJingle"]);

            if (this.isBonusMusicPlaying == false)
            {
                StartCoroutine(Audio.instance.FadeMusic(setTo: 1, duration: 2f, audioClip: game.music));
                this.isBonusMusicPlaying = true;
            }

            this.selectionCursor.gameObject.SetActive(false);

            var buttons = new[]
            {
                exitButton, retryButton
            };
            Tween.Ease easeButtons = Easing.QuadEaseIn;
            var tweener = new Tweener();

            for(int n = 0; n < buttons.Length; n += 1)
            {
                tweener.ScaleLocal(buttons[n], Vector3.zero, 0.3f, easeButtons, 0.05f * n);
            }
            tweener.RotateLocal(this.titleText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
            tweener.RotateLocal(this.subtitleText.transform, 179, 0.5f, Easing.QuadEaseIn, 0.3f);

            var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);
            tweener.ScaleLocal(
                this.transformCenterIn, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.5f
            );

            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_screenwipe_out"], options: new Audio.Options(1f, false, 0f));

            float timer = 0;
            bool hasPlayedOnce = false;
            while (tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleClosedAmount = tweenForCircle.Value();
                UpdateCircleSize();
                timer += Time.unscaledDeltaTime;
                if (timer > .4f && hasPlayedOnce == false)
                {
                    hasPlayedOnce = true;
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_out"], options: new Audio.Options(1f, false, 0f));
                }
                yield return null;
            }
            yield return null;

            //MinigameUI.DetatchFromMinigameUI(this.gameObject);
            Audio.instance.PlayMusic(game.musicTransition);
            game.ExitGame();
            Minigame.testingLevel = null;
            this.game = null;
            
            yield return null;

            // use theme transition to return to game
            var targetCircleCol = 
                Minigame.mode == Minigame.Mode.Arcade ? Color.black :
                ThemesAsset.CircleColor(LevelGeneration.ThemeForDate(Game.selectedDate));
            timer = 0f;
            while(timer < 0.5f)
            {
                timer = Mathf.Min(timer + Time.unscaledDeltaTime, 0.5f);
                this.circleImageTransform.GetComponent<Image>().color = Color.Lerp(Color.white, targetCircleCol, timer * 2f);
                yield return null;
            }

            if(Minigame.mode == Minigame.Mode.BonusLift)
                Transition.GoToGame();
            if(Minigame.mode == Minigame.Mode.Arcade)
                Transition.GoSimple("Arcade");
        }

        private IEnumerator RestartGameAnimation()
        {
            Audio.instance.StopSfx(game.assets.audioClipLookup["GLOBAL:sfx_lose_in"]);
            Audio.instance.StopSfx(game.assets.audioClipLookup["GLOBAL:FailJingle"]);

            if (this.isBonusMusicPlaying == false)
            {
                StartCoroutine(Audio.instance.FadeMusic(setTo: 1, duration: 2f, audioClip: game.music));
                this.isBonusMusicPlaying = true;
            }

            this.selectionCursor.gameObject.SetActive(false);

            var buttons = new[]
            {
                exitButton, retryButton
            };
            Tween.Ease easeButtons = Easing.QuadEaseIn;
            var tweener = new Tweener();

            for(int n = 0; n < buttons.Length; n += 1)
            {
                tweener.ScaleLocal(buttons[n], Vector3.zero, 0.3f, easeButtons, 0.05f * n);
            }
            tweener.RotateLocal(this.titleText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
            tweener.RotateLocal(this.subtitleText.transform, 179, 0.5f, Easing.QuadEaseIn, 0.3f);

            var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);
            tweener.ScaleLocal(
                this.transformCenterIn, Vector3.zero, 0.5f, Easing.QuadEaseIn, 0.5f
            );

            float timer = 0;
            bool hasPlayedOnce = false;
            while (tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleClosedAmount = tweenForCircle.Value();
                UpdateCircleSize();
                timer += 1 / 60.0f;
                if (timer > .4f && hasPlayedOnce == false)
                {
                    hasPlayedOnce = true;
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_screenwipe_out"], options: new Audio.Options(1f, false, 0f));
                }
                yield return null;
            }
            yield return null;
            yield return null;

            this.game.Restart(false);

            yield return null;
            yield return null;

            this.circleOpenAmount = 1;

            tweener = new Tweener();
            tweenForCircle = tweener.TweenFloat(1, 0, 0.5f, Easing.QuadEaseIn);
            while(tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleClosedAmount = tweenForCircle.Value();
                UpdateCircleSize();
                yield return null;
            }

            this.chargedCost = false;

            this.gameObject.SetActive(false);

        }

        private void UpdateCircleSize()
        {
            float width, height;

            if(GameCamera.IsLandscape() == true)
                (width, height) = (700, 400);
            else
                (width, height) = (256, 256 * Screen.height / Screen.width);

            float diagonal = Mathf.Sqrt(width * width + height * height);
            this.circleImageTransform.sizeDelta = new Vector2(diagonal, diagonal);

            float circleStandardSize;
            if(GameCamera.IsLandscape())
                circleStandardSize = 270;
            else
                circleStandardSize = 200;

            float circleRadiusForMaterial = Mathf.LerpUnclamped(
                circleStandardSize / diagonal, 1.0f, this.circleOpenAmount
            ) * (1.0f - this.circleClosedAmount);
            float outlineThickness = circleStandardSize * 0.03f / diagonal;

            this.circleMaterial.SetFloat(Radius, circleRadiusForMaterial);
            this.circleMaterial.SetFloat(OutlineThickness, outlineThickness);

            float scale = circleStandardSize / 220.0f;
            this.uiTransform.localScale = new Vector3(scale, scale, 1);
        }

        public void OnPressContinue()
        {
            // don't charge fruit when testing
            if(Minigame.IsLevelTesting()) this.chargedCost = true;

            if (this.chargedCost == false && Map.detailsToPersist != null)
            {
                void ChargeValue(ref int value, int cost)
                {
                    value -= cost;
                    StartCoroutine(CountDownResource(value + cost, value));
                }
                this.chargedCost = true;

                if(Minigame.mode == Minigame.Mode.BonusLift)
                    ChargeValue(ref Map.detailsToPersist.fruitCurrent, FruitCost);
                else
                    ChargeValue(ref Map.detailsToPersist.coins, CoinCost);
            }

            StartCoroutine(RestartGameAnimation());
        }

        private IEnumerator CountDownResource(int from, int to)
        {
            var tweener = new Tweener();
            var tween = tweener.TweenFloat(from, to, 0.5f, Easing.QuadEaseInOut);
            var symbol = 0;
            if(Minigame.mode == Minigame.Mode.Arcade) symbol += 2;
            while (tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                int newValue = Mathf.RoundToInt(tween.Value());
                totalFruitText.text = $"<sprite={symbol}>{newValue}";
                yield return null;
            }
            totalFruitText.text = $"<sprite={symbol}>{to}";
        }

        public void OnPressExit()
        {
            StartCoroutine(ExitGameAnimation());
        }
        private IEnumerator PlayMusicIn(float duration)
        {
            yield return new WaitForSeconds(duration - .5f);
            if (this.isBonusMusicPlaying == false)
            {
                StartCoroutine(Audio.instance.FadeMusic(setTo: 1, duration: 2f, audioClip: game.music));
                this.isBonusMusicPlaying = true;
            }

        }

        private void LateUpdate()
        {
#if UNITY_EDITOR
            UpdateCircleSize();
#endif
        }
    }
}
