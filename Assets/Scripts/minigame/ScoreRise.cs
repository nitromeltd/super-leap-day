using UnityEngine;
using System.Collections.Generic;

namespace minigame
{
    public class ScoreRise : MonoBehaviour
    {

        public SpriteRenderer[] digits;
        public SpriteRendererPool recycler;
        public Tween tween;

        public static string Layer = "Foreground";
        public static float Spacing = -0.05f;
        public static float RiseDist = 2f;
        public static float RiseDelay = 1f;

        public void Init(int value, SpriteRendererPool recycler, AssetLibrary assets, string prefixSprite = "", string digitNamePrefix = "")
        {
            this.recycler = recycler;
            this.digits = GetDigits(value, Vector2.zero, transform, recycler, assets, prefixSprite, digitNamePrefix);
            this.tween = new Tween(transform.localPosition, transform.localPosition + Vector3.up * RiseDist, RiseDelay, Easing.QuadEaseIn);
        }

        public void SetColor(Color color)
        {
            for(int i = 0; i < this.digits.Length; i++)
            {
                this.digits[i].color = color;
            }
        }

        public static SpriteRenderer[] GetDigits(int value, Vector2 offset, Transform parent, SpriteRendererPool recycler, AssetLibrary assets, string prefixSprite = "", string digitNamePrefix = "")
        {
            string str = Mathf.Abs(value).ToString();// use prefixSprite for "-" or "+"
            int len = str.Length;
            var digits = new List<SpriteRenderer>(len);
            var width = 0f;
            for(int i = 0; i < len; i++)
            {
                var sr = recycler.Get(Vector3.zero, assets.spriteLookup[digitNamePrefix + str[i]], Layer, ""+str[i], parent);
                digits.Add(sr);
                width += sr.bounds.size.x;
            }
            if(prefixSprite.Length > 0)
            {
                var sr = recycler.Get(Vector3.zero, assets.spriteLookup[prefixSprite], Layer, "" + prefixSprite, parent);
                digits.Insert(0, sr);
                width += sr.bounds.size.x;
                len++;
            }
            width += (len - 1) * Spacing;
            var x = -width / 2;
            for(int i = 0; i < len; i++)
            {
                var w = digits[i].bounds.size.x;
                x += w * 0.5f;
                digits[i].transform.localPosition = new Vector3(x, 0) + (Vector3)offset;
                x += w * 0.5f + Spacing;
            }
            return digits.ToArray();
        }

        void Update()
        {
            if(this.tween.Done() == false)
            {
                this.tween.Advance(Time.deltaTime);
                this.transform.localPosition = this.tween.Position();
            }
            else
            {
                for(int i = 0; i < this.digits.Length; i++)
                {
                    this.recycler.Put(this.digits[i]);
                }
                Destroy(this.gameObject);
            }
        }

    }

}
