using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* A generic wobble manager with linear decay */
namespace minigame
{
    public class Shake
    {
        public Vector2 normal;
        public float magnitude;
        public float wavelength;
        public float duration;
        public float time;

        public Shake(Vector2 normal, float wavelength, float duration, float magnitude)
        {
            this.normal = normal;
            this.magnitude = magnitude;
            this.wavelength = wavelength;
            this.duration = duration;
            this.time = 0;
        }

        public Shake(float wavelength, float duration, float magnitude = 1f)
        {
            this.magnitude = magnitude;
            this.wavelength = wavelength;
            this.duration = duration;
            this.time = 0;
        }

        public void Advance(float t)
        {
            this.time += t;
            if(this.time > this.duration)
            {
                this.time = this.duration;
            }
        }

        public float Value()
        {
            // (wobble) * (linear decay) * size
            return Mathf.Sin((this.time / this.wavelength) * 2 * Mathf.PI) * (1.0f - this.time / this.duration) * this.magnitude;
        }

        public Vector2 Position()
        {
            return this.normal * Mathf.Sin((this.time / this.wavelength) * 2 * Mathf.PI) * (1.0f - this.time / this.duration) * this.magnitude;
        }

        public Quaternion Rotation(Quaternion min, Quaternion max)
        {
            var n = Mathf.Sin((this.time / this.wavelength) * 2 * Mathf.PI) * (1.0f - this.time / this.duration) * 0.5f;
            return Quaternion.Lerp(min, max, 0.5f + n);
        }

        public Vector2 Flex(Vector2 negative)
        {
            var n = Mathf.Sin((this.time / this.wavelength) * 2 * Mathf.PI) * (1.0f - this.time / this.duration) * this.magnitude;
            if(n > 0)
                return this.normal * n;
            else
                return negative * n;
        }

        // wave function without linear decay
        public static float Wave(float time, float wavelength, float magnitude = 1f)
        {
            return Mathf.Sin((time / wavelength) * 2 * Mathf.PI) * magnitude;
        }

        public static Vector2 WavePosition(Vector2 normal, float time, float wavelength, float magnitude = 1f)
        {
            return normal * Mathf.Sin((time / wavelength) * 2 * Mathf.PI) * magnitude;
        }

        public static Vector2 WaveFlex(Vector2 normal, Vector2 negative, float time, float wavelength, float magnitude = 1f)
        {
            var n = Mathf.Sin((time / wavelength) * 2 * Mathf.PI) * magnitude;
            if(n > 0)
                return normal * n;
            else
                return negative * n;
        }

        public static Quaternion WaveRotation(float time, float wavelength, Quaternion min, Quaternion max)
        {
            var n = Mathf.Sin((time / wavelength) * 2 * Mathf.PI) * 0.5f;
            return Quaternion.Lerp(min, max, 0.5f + n);
        }

        public static IEnumerator TransformLocal(Shake shake, Transform transform)
        {
            var position = transform.localPosition;
            while(shake.time < shake.duration)
            {
                shake.Advance(0.016667f);
                transform.localPosition = position + (Vector3)shake.Position();
                yield return null;
            }
            transform.localPosition = position;
        }
    }
}
