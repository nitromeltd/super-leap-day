using System.Collections.Generic;
using UnityEngine;

namespace minigame
{

    public class SpriteRendererPool : MonoBehaviour
    {

        public Stack<SpriteRenderer> pool;
        public int sortingOrderCount;
        public int poolSize = 100;
        public int destroyRate = 1;
        public int instCount = 0;
        public static bool LOG = false;

        void Awake()
        {
            this.pool = new Stack<SpriteRenderer>();
            this.sortingOrderCount = 0;
        }

        public SpriteRenderer Get(Vector3 v, Sprite sprite, string sortingLayerName = "Default", string name = null, Transform parent = null, bool incrementSortOrder = true)
        {

            if(name == null) name = "no-name";
            name += ":" + this.instCount++;
            GameObject obj = null;
            SpriteRenderer renderer = null;
            if(this.pool.Count > 0)
            {
                renderer = this.pool.Pop();
                obj = renderer.gameObject;
                obj.layer = 0;

                if(LOG) Debug.Log("Remove:" + renderer.name);
            }
            else
            {
                obj = new GameObject(name);
                renderer = obj.AddComponent<SpriteRenderer>();

                if(LOG) Debug.Log("Create:" + renderer.name);
            }

            obj.name = name;
            renderer.enabled = true;
            renderer.color = Color.white;
            renderer.flipX = false;
            renderer.flipY = false;
            renderer.sortingLayerName = sortingLayerName;
            renderer.drawMode = SpriteDrawMode.Simple;
            if(incrementSortOrder)
            {
                renderer.sortingOrder = ++this.sortingOrderCount;
            }
            else
            {
                renderer.sortingOrder = 0;
            }
            //renderer.material = inst.defaultMaterial;
            renderer.sprite = sprite;


            Transform t = renderer.transform;
            if(parent != null) t.SetParent(parent);
            t.localRotation = Quaternion.identity;
            t.localScale = new Vector3(1, 1, 1);
            t.localPosition = v;

            return renderer;
        }

        public void DestroyChildren(Transform root)
        {
            int count = root.childCount;
            if(LOG) Debug.Log("Destroyed (children):" + root.childCount + " " + root);
            for(int i = count - 1; i > -1; i--)
            {
                //Debug.Log(root.GetChild(i).gameObject);
                Destroy(root.GetChild(i).gameObject);
            }
        }

        public void Put(SpriteRenderer item)
        {
            if(LOG) Debug.Log("Stored:" + item.name);
            item.transform.parent = null;
            this.pool.Push(item);
            item.enabled = false;
            item.transform.SetParent(transform);
        }

        // Destroy items slowly
        void FixedUpdate()
        {
            if(this.pool.Count > this.poolSize)
            {
                for(int i = 0; i < destroyRate; i++)
                {
                    var item = pool.Pop();
                    if(LOG) Debug.Log("Destroyed:" + item.name);
                    if(item.transform.childCount > 0) item.transform.DetachChildren();
                    Destroy(item.transform.gameObject);
                    if(this.pool.Count <= this.poolSize) break;
                }
            }
        }

        public void Clear()
        {
            foreach(Transform childTransform in transform)
            {
                Destroy(childTransform.gameObject);
            }
            this.pool.Clear();
            this.sortingOrderCount = 0;
        }

    }
}
