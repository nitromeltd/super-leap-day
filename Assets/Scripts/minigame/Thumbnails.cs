using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

namespace minigame
{
    [CreateAssetMenu(menuName = "Minigame/Thumbnails")]
    public class Thumbnails : ScriptableObject
    {

        [System.Serializable]
        public struct Difficulty
        {
            public Sprite[] sprites;
        }
        public Difficulty[] difficulties;

        public static Sprite[][] GetThumbnails(Minigame.Type type)
        {
            var thumbnails = type switch
            {
                Minigame.Type.Golf      => Resources.Load<Thumbnails>("Minigame Thumbnails/Golf Thumbnails"),
                Minigame.Type.Pinball   => Resources.Load<Thumbnails>("Minigame Thumbnails/Pinball Thumbnails"),
                Minigame.Type.Racing    => Resources.Load<Thumbnails>("Minigame Thumbnails/Racing Thumbnails"),
                Minigame.Type.Fishing   => Resources.Load<Thumbnails>("Minigame Thumbnails/Fishing Thumbnails"),
                Minigame.Type.Rolling   => Resources.Load<Thumbnails>("Minigame Thumbnails/Rolling Thumbnails"),
                Minigame.Type.Abseiling => Resources.Load<Thumbnails>("Minigame Thumbnails/Abseiling Thumbnails"),
                _ => null
            };
            return thumbnails?.difficulties.Select(d => d.sprites).ToArray();
        }

    }

}
