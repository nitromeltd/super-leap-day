
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace minigame
{
    // Butchered version of Tile from main game
    // Used for autotiling and creating physics objects
    public partial class Tile
    {
        public static int Size = 1;
        
        public enum Shape
        {
            Square,
            Large2x1,
            Large1x2,
            TopLine,
            Slope45BL,
            Slope45BR,
            Slope45TL,
            Slope45TR,
            Slope22BL,
            Slope22BR,
            Slope22TL,
            Slope22TR,
            CurveBL3,
            CurveBR3,
            CurveTL3,
            CurveTR3,
            CurveBL3Plus1,
            CurveBR3Plus1,
            CurveTL3Plus1,
            CurveTR3Plus1,
            CurveBL4Plus1,
            CurveBR4Plus1,
            CurveTL4Plus1,
            CurveTR4Plus1,
            CurveBL8Plus1,
            CurveBR8Plus1,
            CurveTL8Plus1,
            CurveTR8Plus1,
            CurveOutBL3,
            CurveOutBR3,
            CurveOutTL3,
            CurveOutTR3
        }

        public class Spec
        {
            public static Spec Empty = Spec.WithShape(Shape.Square, 1, 1, false);
            public static Spec Square = Spec.WithShape(Shape.Square);
            public static Spec SquareNoWallSlide = Spec.WithShape(Shape.Square, wallSlideEnabled: false);
            public static Spec Large2x1 = Spec.WithShape(Shape.Large2x1, spanX: 2);
            public static Spec Large1x2 = Spec.WithShape(Shape.Large1x2, spanY: 2);
            public static Spec Large2x2 = Spec.WithShape(Shape.Large1x2, spanX: 2, spanY: 2);
            public static Spec TopLine = Spec.WithShape(Shape.TopLine, topOnly: true);
            public static Spec Slope45BL = Spec.WithShape(Shape.Slope45BL);
            public static Spec Slope45BR = Spec.WithShape(Shape.Slope45BR);
            public static Spec Slope45TL = Spec.WithShape(Shape.Slope45TL);
            public static Spec Slope45TR = Spec.WithShape(Shape.Slope45TR);
            public static Spec Slope22BL = Spec.WithShape(Shape.Slope22BL, spanX: 2);
            public static Spec Slope22BR = Spec.WithShape(Shape.Slope22BR, spanX: 2);
            public static Spec Slope22TL = Spec.WithShape(Shape.Slope22TL, spanX: 2);
            public static Spec Slope22TR = Spec.WithShape(Shape.Slope22TR, spanX: 2);
            public static Spec CurveBR3 = Spec.WithCurve(Shape.CurveBR3, 3, 3, new Curve(0, 3, 3, false, 0, 90));
            public static Spec CurveTR3 = Spec.WithCurve(Shape.CurveTR3, 3, 3, new Curve(0, 0, 3, false, 90, 180));
            public static Spec CurveTL3 = Spec.WithCurve(Shape.CurveTL3, 3, 3, new Curve(3, 0, 3, false, 180, 270));
            public static Spec CurveBL3 = Spec.WithCurve(Shape.CurveBL3, 3, 3, new Curve(3, 3, 3, false, 270, 0));
            public static Spec CurveBR3Plus1 = Spec.WithCurve(Shape.CurveBR3Plus1, 4, 4, new Curve(0, 4, 3, false, 0, 90));
            public static Spec CurveTR3Plus1 = Spec.WithCurve(Shape.CurveTR3Plus1, 4, 4, new Curve(0, 0, 3, false, 90, 180));
            public static Spec CurveTL3Plus1 = Spec.WithCurve(Shape.CurveTL3Plus1, 4, 4, new Curve(4, 0, 3, false, 180, 270));
            public static Spec CurveBL3Plus1 = Spec.WithCurve(Shape.CurveBL3Plus1, 4, 4, new Curve(4, 4, 3, false, 270, 0));
            public static Spec CurveBR4Plus1 = Spec.WithCurve(Shape.CurveBR4Plus1, 5, 5, new Curve(0, 5, 4, false, 0, 90));
            public static Spec CurveTR4Plus1 = Spec.WithCurve(Shape.CurveTR4Plus1, 5, 5, new Curve(0, 0, 4, false, 90, 180));
            public static Spec CurveTL4Plus1 = Spec.WithCurve(Shape.CurveTL4Plus1, 5, 5, new Curve(5, 0, 4, false, 180, 270));
            public static Spec CurveBL4Plus1 = Spec.WithCurve(Shape.CurveBL4Plus1, 5, 5, new Curve(5, 5, 4, false, 270, 0));
            public static Spec CurveBR8Plus1 = Spec.WithCurve(Shape.CurveBR8Plus1, 9, 9, new Curve(0, 9, 8, false, 0, 90));
            public static Spec CurveTR8Plus1 = Spec.WithCurve(Shape.CurveTR8Plus1, 9, 9, new Curve(0, 0, 8, false, 90, 180));
            public static Spec CurveTL8Plus1 = Spec.WithCurve(Shape.CurveTL8Plus1, 9, 9, new Curve(9, 0, 8, false, 180, 270));
            public static Spec CurveBL8Plus1 = Spec.WithCurve(Shape.CurveBL8Plus1, 9, 9, new Curve(9, 9, 8, false, 270, 0));
            public static Spec CurveOutBR3 = Spec.WithCurve(Shape.CurveOutBR3, 3, 3, new Curve(0, 3, 3, true, 0, 90));
            public static Spec CurveOutTR3 = Spec.WithCurve(Shape.CurveOutTR3, 3, 3, new Curve(0, 0, 3, true, 90, 180));
            public static Spec CurveOutTL3 = Spec.WithCurve(Shape.CurveOutTL3, 3, 3, new Curve(3, 0, 3, true, 180, 270));
            public static Spec CurveOutBL3 = Spec.WithCurve(Shape.CurveOutBL3, 3, 3, new Curve(3, 3, 3, true, 270, 0));

            public static Spec FromShape(Shape shape)
            {
                switch(shape)
                {
                    case Shape.Square: return Square;
                    case Shape.Large2x1: return Large2x1;
                    case Shape.Large1x2: return Large1x2;
                    case Shape.TopLine: return TopLine;
                    case Shape.Slope45BL: return Slope45BL;
                    case Shape.Slope45BR: return Slope45BR;
                    case Shape.Slope45TL: return Slope45TL;
                    case Shape.Slope45TR: return Slope45TR;
                    case Shape.Slope22BL: return Slope22BL;
                    case Shape.Slope22BR: return Slope22BR;
                    case Shape.Slope22TL: return Slope22TL;
                    case Shape.Slope22TR: return Slope22TR;
                    case Shape.CurveBL3: return CurveBL3;
                    case Shape.CurveBR3: return CurveBR3;
                    case Shape.CurveTL3: return CurveTL3;
                    case Shape.CurveTR3: return CurveTR3;
                    case Shape.CurveBL3Plus1: return CurveBL3Plus1;
                    case Shape.CurveBR3Plus1: return CurveBR3Plus1;
                    case Shape.CurveTL3Plus1: return CurveTL3Plus1;
                    case Shape.CurveTR3Plus1: return CurveTR3Plus1;
                    case Shape.CurveBL4Plus1: return CurveBL4Plus1;
                    case Shape.CurveBR4Plus1: return CurveBR4Plus1;
                    case Shape.CurveTL4Plus1: return CurveTL4Plus1;
                    case Shape.CurveTR4Plus1: return CurveTR4Plus1;
                    case Shape.CurveBL8Plus1: return CurveBL8Plus1;
                    case Shape.CurveBR8Plus1: return CurveBR8Plus1;
                    case Shape.CurveTL8Plus1: return CurveTL8Plus1;
                    case Shape.CurveTR8Plus1: return CurveTR8Plus1;
                    case Shape.CurveOutBL3: return CurveOutBL3;
                    case Shape.CurveOutBR3: return CurveOutBR3;
                    case Shape.CurveOutTL3: return CurveOutTL3;
                    case Shape.CurveOutTR3: return CurveOutTR3;
                }
                return Empty;
            }

            public Shape shape;
            public int spanX;
            public int spanY;
            public Curve curve;
            public bool solid;
            public bool topOnly;
            public bool wallSlideEnabled;
            public Spec flipped;

            private static Spec WithShape(
                Shape shape,
                int spanX = 1,
                int spanY = 1,
                bool solid = true,
                bool topOnly = false,
                bool wallSlideEnabled = true
            ) => new Spec
            {
                shape = shape,
                spanX = spanX,
                spanY = spanY,
                solid = solid,
                topOnly = topOnly,
                wallSlideEnabled = wallSlideEnabled,
                curve = null
            };

            private static Spec WithCurve(
                Shape shape, int spanX, int spanY, Curve curve
            ) => new Spec
            {
                shape = shape,
                spanX = spanX,
                spanY = spanY,
                solid = true,
                topOnly = false,
                curve = curve,
                wallSlideEnabled = true
            };
        }

        public static Dictionary<string, Spec> Specs = new Dictionary<string, Spec>
        {
            ["square"] = Spec.Square,

            ["1x2_a"] = Spec.Large1x2,
            ["1x2_b"] = Spec.Large1x2,
            ["2x1_b"] = Spec.Large2x1,

            ["pillar_thick_1"] = Spec.Large2x1,
            ["pillar_thick_2"] = Spec.Large2x1,
            ["pillar_thick_3"] = Spec.Large2x1,
            ["pillar_thick_4"] = Spec.Large2x1,
            ["pillar_thick_bottom"] = Spec.Large2x1,
            ["pillar_thick_top"] = Spec.Large2x1,
            ["back_pillar2wide"] = Spec.Large2x1,

            ["45br"] = Spec.Slope45BR,
            ["45br_a"] = Spec.Slope45BR,
            ["45br_b"] = Spec.Slope45BR,
            ["45bl"] = Spec.Slope45BL,
            ["45bl_a"] = Spec.Slope45BL,
            ["45bl_b"] = Spec.Slope45BL,
            ["45tl"] = Spec.Slope45TL,
            ["45tr"] = Spec.Slope45TR,

            ["22br"] = Spec.Slope22BR,
            ["22bl"] = Spec.Slope22BL,
            ["22tr"] = Spec.Slope22TR,
            ["22tl"] = Spec.Slope22TL,

            ["cloudleft"] = Spec.TopLine,
            ["cloudright"] = Spec.TopLine,
            ["cloud1"] = Spec.TopLine,
            ["cloud2"] = Spec.TopLine,
            ["cloud3"] = Spec.TopLine,
            ["cloud4"] = Spec.TopLine,
            ["cloud5"] = Spec.TopLine,
            ["cloud6"] = Spec.TopLine,
            ["cloud7"] = Spec.TopLine,
            ["topline"] = Spec.TopLine,

            ["nosolid1"] = Spec.TopLine,
            ["nosolid2"] = Spec.TopLine,
            ["nosolid3"] = Spec.TopLine,

            ["noslide"] = Spec.SquareNoWallSlide,

            ["curvebr3"] = Spec.CurveBR3,
            ["curvetr3"] = Spec.CurveTR3,
            ["curvetl3"] = Spec.CurveTL3,
            ["curvebl3"] = Spec.CurveBL3,
            ["curvebr3plus1"] = Spec.CurveBR3Plus1,
            ["curvetr3plus1"] = Spec.CurveTR3Plus1,
            ["curvetl3plus1"] = Spec.CurveTL3Plus1,
            ["curvebl3plus1"] = Spec.CurveBL3Plus1,
            ["curvebr4plus1"] = Spec.CurveBR4Plus1,
            ["curvetr4plus1"] = Spec.CurveTR4Plus1,
            ["curvetl4plus1"] = Spec.CurveTL4Plus1,
            ["curvebl4plus1"] = Spec.CurveBL4Plus1,
            ["curvebr8plus1"] = Spec.CurveBR8Plus1,
            ["curvetr8plus1"] = Spec.CurveTR8Plus1,
            ["curvetl8plus1"] = Spec.CurveTL8Plus1,
            ["curvebl8plus1"] = Spec.CurveBL8Plus1,

            ["curveoutbr3"] = Spec.CurveOutBR3,
            ["curveouttr3"] = Spec.CurveOutTR3,
            ["curveouttl3"] = Spec.CurveOutTL3,
            ["curveoutbl3"] = Spec.CurveOutBL3,

            ["metal1x1_a"] = Spec.Square,
            ["metal1x1_b"] = Spec.Square,
            ["metal1x1_c"] = Spec.Square,
            ["metal1x2_a"] = Spec.Large1x2,
            ["metal1x2_b"] = Spec.Large1x2,
            ["metal2x1_a"] = Spec.Large2x1,
            ["metal2x1_b"] = Spec.Large2x1,
            ["metal2x2_a"] = Spec.Large2x2
        };

        private static HashSet<string> NonSolidTiles = new HashSet<string> {
        "flower_0", "flower2_0", "sunflower_0", "sunflower2_0",
        "bush 1", "bush 2", "bush 3", "tree 1", "tree 2",
        "tiles_174-", "tiles_175-", "tiles_183-", "tiles_185-", "tiles_188-",
        "tiles_189-", "tiles_197-", "tiles_198-", "tiles_199-", "tiles_200-",
        "tiles_211-", "tiles_212-", "tiles_214-", "tiles_215-", "tiles_216-",
        "tiles_219-", "tiles_220-", "tiles_221-", "tiles_223-", "tiles_228-",
        "tiles_231-", "tiles_236-", "tiles_237-", "tiles_239-", "tiles_240-",
        "tiles_241-", "tiles_242-", "tiles_243-", "tiles_244-", "tiles_245-"
    };


        public TileGrid tileGrid;
        public int mapTx;
        public int mapTy;
        public int gridTx;
        public int gridTy;
        public Spec spec;
        public string tileName;
        public Sprite sprite;
        public Sprite backSprite;
        public bool shouldAutotile;
        public Tileset addInteriorFromTileset;
        public bool autotileChoseSkew;
        public bool autotileChoseThin; // column, row, or single piece
        public SpriteRenderer spriteRenderer;
        public SpriteRenderer backSpriteRenderer;
        public List<MeshRenderer> backMeshRenderers;

        public class Curve
        {
            public Vector2 center;
            public int radius;
            public bool solidInside;
            public int minAngle;
            public int maxAngle;

            public Curve(
                int centerX,
                int centerY,
                int radius,
                bool solidInside,
                int minAngle,
                int maxAngle
            )
            {
                this.center = new Vector2(centerX, centerY);
                this.radius = radius;
                this.solidInside = solidInside;
                this.minAngle = minAngle;
                this.maxAngle = maxAngle;
            }
        }

        public void Init(
            int mapTx,
            int mapTy,
            int gridTx,
            int gridTy,
            NitromeEditor.TileLayer.Tile tile
        )
        {
            this.mapTx = mapTx;
            this.mapTy = mapTy;
            this.gridTx = gridTx;
            this.gridTy = gridTy;
            this.tileName = tile.tile;
            //this.ice = false;

            var (prefix, suffix) = ("", tile.tile);

            var colon = tile.tile.IndexOf(':');
            if(colon != -1)
            {
                prefix = tile.tile.Substring(0, colon);
                suffix = tile.tile.Substring(colon + 1);
            }

            //if(prefix == Minigame.GlobalPrefix)
            //    this.shouldAutotile = true;

            //if(prefix == "ICE")
            //    this.ice = true;

            if(Specs.TryGetValue(suffix, out this.spec) == false)
            {
                if(NonSolidTiles.Contains(suffix) == true)
                    this.spec = Spec.Empty;
                else
                    this.spec = Spec.Square;
            }

            Shape Flip(Shape shape)
            {
                switch(shape)
                {
                    case Shape.Slope22BL: return Shape.Slope22BR;
                    case Shape.Slope22BR: return Shape.Slope22BL;
                    case Shape.Slope22TL: return Shape.Slope22TR;
                    case Shape.Slope22TR: return Shape.Slope22TL;
                    case Shape.Slope45BL: return Shape.Slope45BR;
                    case Shape.Slope45BR: return Shape.Slope45BL;
                    case Shape.Slope45TL: return Shape.Slope45TR;
                    case Shape.Slope45TR: return Shape.Slope45TL;
                    case Shape.CurveBL3: return Shape.CurveBR3;
                    case Shape.CurveBR3: return Shape.CurveBL3;
                    case Shape.CurveTL3: return Shape.CurveTR3;
                    case Shape.CurveTR3: return Shape.CurveTL3;
                    case Shape.CurveBL3Plus1: return Shape.CurveBR3Plus1;
                    case Shape.CurveBR3Plus1: return Shape.CurveBL3Plus1;
                    case Shape.CurveTL3Plus1: return Shape.CurveTR3Plus1;
                    case Shape.CurveTR3Plus1: return Shape.CurveTL3Plus1;
                    case Shape.CurveBL4Plus1: return Shape.CurveBR4Plus1;
                    case Shape.CurveBR4Plus1: return Shape.CurveBL4Plus1;
                    case Shape.CurveTL4Plus1: return Shape.CurveTR4Plus1;
                    case Shape.CurveTR4Plus1: return Shape.CurveTL4Plus1;
                    case Shape.CurveBL8Plus1: return Shape.CurveBR8Plus1;
                    case Shape.CurveBR8Plus1: return Shape.CurveBL8Plus1;
                    case Shape.CurveTL8Plus1: return Shape.CurveTR8Plus1;
                    case Shape.CurveTR8Plus1: return Shape.CurveTL8Plus1;
                    case Shape.CurveOutBL3: return Shape.CurveOutBR3;
                    case Shape.CurveOutBR3: return Shape.CurveOutBL3;
                    case Shape.CurveOutTL3: return Shape.CurveOutTR3;
                    case Shape.CurveOutTR3: return Shape.CurveOutTL3;
                    default: return shape;
                }
            }

            Shape Rotate180(Shape shape)
            {
                switch(shape)
                {
                    case Shape.Slope22BL: return Shape.Slope22TR;
                    case Shape.Slope22BR: return Shape.Slope22TL;
                    case Shape.Slope22TL: return Shape.Slope22BR;
                    case Shape.Slope22TR: return Shape.Slope22BL;
                    case Shape.Slope45BL: return Shape.Slope45TR;
                    case Shape.Slope45BR: return Shape.Slope45TL;
                    case Shape.Slope45TL: return Shape.Slope45BR;
                    case Shape.Slope45TR: return Shape.Slope45BL;
                    case Shape.CurveBL3: return Shape.CurveTR3;
                    case Shape.CurveBR3: return Shape.CurveTL3;
                    case Shape.CurveTL3: return Shape.CurveBR3;
                    case Shape.CurveTR3: return Shape.CurveBL3;
                    case Shape.CurveBL3Plus1: return Shape.CurveTR3Plus1;
                    case Shape.CurveBR3Plus1: return Shape.CurveTL3Plus1;
                    case Shape.CurveTL3Plus1: return Shape.CurveBR3Plus1;
                    case Shape.CurveTR3Plus1: return Shape.CurveBL3Plus1;
                    case Shape.CurveBL4Plus1: return Shape.CurveTR4Plus1;
                    case Shape.CurveBR4Plus1: return Shape.CurveTL4Plus1;
                    case Shape.CurveTL4Plus1: return Shape.CurveBR4Plus1;
                    case Shape.CurveTR4Plus1: return Shape.CurveBL4Plus1;
                    case Shape.CurveBL8Plus1: return Shape.CurveTR8Plus1;
                    case Shape.CurveBR8Plus1: return Shape.CurveTL8Plus1;
                    case Shape.CurveTL8Plus1: return Shape.CurveBR8Plus1;
                    case Shape.CurveTR8Plus1: return Shape.CurveBL8Plus1;
                    case Shape.CurveOutBL3: return Shape.CurveOutTR3;
                    case Shape.CurveOutBR3: return Shape.CurveOutTL3;
                    case Shape.CurveOutTL3: return Shape.CurveOutBR3;
                    case Shape.CurveOutTR3: return Shape.CurveOutBL3;
                    default: return shape;
                }
            }

            System.Nullable<Shape> Rotate90(Shape shape)
            {
                switch(shape)
                {
                    case Shape.Slope22BL: return null;
                    case Shape.Slope22BR: return null;
                    case Shape.Slope22TL: return null;
                    case Shape.Slope22TR: return null;
                    case Shape.Slope45BL: return Shape.Slope45BR;
                    case Shape.Slope45BR: return Shape.Slope45TR;
                    case Shape.Slope45TL: return Shape.Slope45BL;
                    case Shape.Slope45TR: return Shape.Slope45TL;
                    case Shape.CurveBL3: return Shape.CurveBR3;
                    case Shape.CurveBR3: return Shape.CurveTR3;
                    case Shape.CurveTL3: return Shape.CurveBL3;
                    case Shape.CurveTR3: return Shape.CurveTL3;
                    case Shape.CurveBL3Plus1: return Shape.CurveBR3Plus1;
                    case Shape.CurveBR3Plus1: return Shape.CurveTR3Plus1;
                    case Shape.CurveTL3Plus1: return Shape.CurveBL3Plus1;
                    case Shape.CurveTR3Plus1: return Shape.CurveTL3Plus1;
                    case Shape.CurveBL4Plus1: return Shape.CurveBR4Plus1;
                    case Shape.CurveBR4Plus1: return Shape.CurveTR4Plus1;
                    case Shape.CurveTL4Plus1: return Shape.CurveBL4Plus1;
                    case Shape.CurveTR4Plus1: return Shape.CurveTL4Plus1;
                    case Shape.CurveBL8Plus1: return Shape.CurveBR8Plus1;
                    case Shape.CurveBR8Plus1: return Shape.CurveTR8Plus1;
                    case Shape.CurveTL8Plus1: return Shape.CurveBL8Plus1;
                    case Shape.CurveTR8Plus1: return Shape.CurveTL8Plus1;
                    case Shape.CurveOutBL3: return Shape.CurveOutBR3;
                    case Shape.CurveOutBR3: return Shape.CurveOutTR3;
                    case Shape.CurveOutTL3: return Shape.CurveOutBL3;
                    case Shape.CurveOutTR3: return Shape.CurveOutTL3;
                    default: return shape;
                }
            }

            if(tile.flip == true || tile.rotation != 0)
            {
                Shape? shape = null;

                if(tile.rotation == 0 && tile.flip == true)
                {
                    shape = Flip(this.spec.shape);
                    this.mapTx -= (this.spec.spanX - 1);
                    this.gridTx -= (this.spec.spanX - 1);
                }
                else if(tile.rotation == 90 && tile.flip == false)
                {
                    shape = Rotate90(this.spec.shape);
                    this.mapTx -= this.spec.spanY - 1;
                    this.gridTx -= this.spec.spanY - 1;
                }
                else if(tile.rotation == 90 && tile.flip == true)
                {
                    shape = Rotate90(Flip(this.spec.shape));
                    this.mapTx -= this.spec.spanY - 1;
                    this.mapTy -= this.spec.spanX - 1;
                    this.gridTx -= this.spec.spanY - 1;
                    this.gridTy -= this.spec.spanX - 1;
                }
                else if(tile.rotation == 180 && tile.flip == false)
                {
                    shape = Rotate180(this.spec.shape);
                    this.mapTx -= this.spec.spanX - 1;
                    this.mapTy -= this.spec.spanY - 1;
                    this.gridTx -= this.spec.spanX - 1;
                    this.gridTy -= this.spec.spanY - 1;
                }
                else if(tile.rotation == 180 && tile.flip == true)
                {
                    shape = Flip(Rotate180(this.spec.shape));
                    this.mapTy -= this.spec.spanY - 1;
                    this.gridTy -= this.spec.spanY - 1;
                }
                else if(tile.rotation == 270 && tile.flip == false)
                {
                    shape = Rotate90(Rotate180(this.spec.shape));
                    this.mapTy -= this.spec.spanY - 1;
                    this.gridTy -= this.spec.spanY - 1;
                }
                else if(tile.rotation == 270 && tile.flip == true)
                {
                    shape = Rotate90(Rotate180(Flip(this.spec.shape)));
                }

                if(shape.HasValue == true)
                    this.spec = Spec.FromShape(shape.Value);
                else
                    Debug.LogError(
                        $"Tile {mapTx}, {mapTy}, {this.spec.shape}, was flipped/rotated in impossible way."
                    );
            }
        }

        public bool IncludeTileInTileGridSpace(int txOffset, int tyOffset)
        {
            if(this.spec.curve == null)
                return true;

            int x1 = txOffset * Tile.Size;
            int x2 = (txOffset + 1) * Tile.Size;
            int y1 = tyOffset * Tile.Size;
            int y2 = (tyOffset + 1) * Tile.Size;

            return
                IsSolidAtPoint(x1, y1) ||
                IsSolidAtPoint(x2, y1) ||
                IsSolidAtPoint(x1, y2) ||
                IsSolidAtPoint(x2, y2);
        }

        public bool IsEntirelySolidInCell(int relativeX, int relativeY)
        {
            switch(this.spec.shape)
            {
                case Shape.Square: return true;
                case Shape.Large1x2: return true;
                case Shape.Large2x1: return true;
                case Shape.CurveBL3Plus1: return relativeX == 0 || relativeY == 0;
                case Shape.CurveBR3Plus1: return relativeX == 3 || relativeY == 0;
                case Shape.CurveTL3Plus1: return relativeX == 0 || relativeY == 3;
                case Shape.CurveTR3Plus1: return relativeX == 3 || relativeY == 3;
                case Shape.CurveBL4Plus1: return relativeX == 0 || relativeY == 0;
                case Shape.CurveBR4Plus1: return relativeX == 4 || relativeY == 0;
                case Shape.CurveTL4Plus1: return relativeX == 0 || relativeY == 4;
                case Shape.CurveTR4Plus1: return relativeX == 4 || relativeY == 4;
                case Shape.CurveBL8Plus1: return relativeX == 0 || relativeY == 0;
                case Shape.CurveBR8Plus1: return relativeX == 8 || relativeY == 0;
                case Shape.CurveTL8Plus1: return relativeX == 0 || relativeY == 8;
                case Shape.CurveTR8Plus1: return relativeX == 8 || relativeY == 8;
                default: return false;
            }
        }

        public bool IsSolidAtPoint(float relativeX, float relativeY)
        {
            if(this.spec.solid == false) return false;

            if(this.spec.curve != null)
            {
                var dx = relativeX - this.spec.curve.center.x;
                var dy = relativeY - this.spec.curve.center.y;
                var sqDist = (dx * dx) + (dy * dy);
                var radius = this.spec.curve.radius;
                if(this.spec.curve.solidInside == true)
                    return sqDist < radius * radius;
                else
                    return sqDist > radius * radius;
            }

            switch(this.spec.shape)
            {
                case Shape.Square: return true;
                case Shape.Large1x2: return true;
                case Shape.Large2x1: return true;
                case Shape.TopLine: return relativeY >= 0.875f;
                case Shape.Slope45BR: return relativeY <= relativeX;
                case Shape.Slope45BL: return relativeY <= Size - relativeX;
                case Shape.Slope45TR: return relativeY >= Size - relativeX;
                case Shape.Slope45TL: return relativeY >= relativeX;
                case Shape.Slope22BR: return relativeY <= (relativeX * 0.5f);
                case Shape.Slope22BL: return relativeY <= Size - (relativeX * 0.5f);
                case Shape.Slope22TR: return relativeY >= Size - (relativeX * 0.5f);
                case Shape.Slope22TL: return relativeY >= (relativeX * 0.5f);
            }
            return false;
        }

    }
}
