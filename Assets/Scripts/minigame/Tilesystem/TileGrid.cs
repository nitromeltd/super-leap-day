using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace minigame
{
    // Butchered version of TileGrid from main game
    // Used for autotiling and creating physics objects
    public partial class TileGrid
    {
        // required Minigame properties (replaces Chunk)
        public int sortingOrder;
        public Transform containerTransform;
        public RectInt levelRectInt;

        public int xMin;
        public int xMax;
        public int yMin;
        public int yMax;
        public int columns;
        public int rows;
        public Tile[] tiles;
        public SpriteRenderer[] tilePatterns;

        public void Init(
            int tileLayerColumns,
            int tileLayerRows,
            NitromeEditor.TileLayer tileLayer,
            Transform containerTransform,
            RectInt levelRectInt
        )
        {
            this.containerTransform = containerTransform;
            this.levelRectInt = levelRectInt;
            
            this.xMin = 0;
            this.xMin = 0;
            this.xMax = this.xMin + tileLayerColumns - 1;
            this.yMax = this.yMin + tileLayerRows - 1;
            this.columns = tileLayerColumns;
            this.rows = tileLayerRows;
            this.tiles = new Tile[this.columns * this.rows];
            this.tilePatterns = new SpriteRenderer[this.columns * this.rows];

            if(tileLayer == null)
                return;

            for(int y = 0; y < rows; y += 1)
            {
                for(int x = 0; x < columns; x += 1)
                {
                    int n = x + (y * this.columns);
                    var tData = tileLayer.tiles[n];
                    if(tData.tile == null) continue;
                    if(tData.tile == "") continue;

                    int mapTx = x + this.xMin;
                    int mapTy = y + this.yMin;

                    var tile = new Tile();
                    tile.Init(mapTx, mapTy, x, y, tData);
                    tile.tileGrid = this;

                    // this is just if the tile changes its own tx (can happen if it flips)
                    int gridTx = tile.mapTx - this.xMin;
                    int gridTy = tile.mapTy - this.yMin;

                    for(int dx = 0; dx < tile.spec.spanX; dx += 1)
                    {
                        for(int dy = 0; dy < tile.spec.spanY; dy += 1)
                        {
                            if(tile.IncludeTileInTileGridSpace(dx, dy) == false)
                                continue;
                            this.tiles[(gridTx + dx) + ((gridTy + dy) * columns)] = tile;
                        }
                    }
                }
            }
        }
        public void Autotile(
            TileTheme tileTheme,
            string sortingLayerName,
            int layerMaskNumber = 0
        )
        {
            // first thin tiles need to be found and treated separately from
            // everything else, so that no other tiles try to merge into them
            foreach(var tile in this.tiles)
            {
                if(tile == null) continue;
                if(tile.sprite != null) continue;

                if(tile.tileName == Minigame.GlobalPrefix + "square")
                    SelectThinTile(tileTheme.normalTiles, tile);
                else if(tile.tileName == Minigame.GlobalPrefix + "dirt")
                    SelectThinTile(tileTheme.dirtTiles, tile);
                else if(tile.tileName == Minigame.GlobalPrefix + "back")
                    SelectThinTile(tileTheme.backTiles, tile);
            }

            // then slope tiles; we need to know when skew tiles are selected
            // because the choice of sprites for those tiles affect other tiles around them
            foreach(var tile in this.tiles)
            {
                if(tile == null) continue;
                if(tile.sprite != null) continue;
                SelectSlopeTile(tileTheme.normalTiles, tile);
            }

            // anything unclaimed gets selected by more standard rules.
            foreach(var tile in this.tiles)
            {
                if(tile == null) continue;
                if(tile.sprite != null) continue;

                int x = tile.gridTx;
                int y = tile.gridTy;

                if(tile.spec.shape == Tile.Shape.TopLine)
                {
                    var left = GetTileForAutotile(tile.gridTx - 1, tile.gridTy);
                    var right = GetTileForAutotile(tile.gridTx + 1, tile.gridTy);
                    bool leftmost = left.tile?.spec?.shape != Tile.Shape.TopLine;
                    bool rightmost = right.tile?.spec?.shape != Tile.Shape.TopLine;

                    if(leftmost == true && rightmost == false)
                        tile.sprite = tileTheme.cloudLeft;
                    else if(rightmost == true && leftmost == false)
                        tile.sprite = tileTheme.cloudRight;
                    else
                        tile.sprite = Util.RandomChoice(tileTheme.cloudMiddle);
                }
                else if(tile.tileName == Minigame.GlobalPrefix + "square")
                    SelectTile(tileTheme.normalTiles, tile);
                else if(tile.spec.curve != null)
                    SelectCurveTile(tileTheme.normalTiles, tile);
                else if(tile.tileName == Minigame.GlobalPrefix + "dirt")
                    SelectTile(tileTheme.dirtTiles, tile);
                else if(tile.tileName == Minigame.GlobalPrefix + "back")
                    SelectTile(tileTheme.backTiles, tile);
            }

            var listOfTiles = this.tiles.Where(t => t != null).Distinct().ToArray();

            foreach(var tile in listOfTiles)
            {
                GameObject gameObject;

                gameObject = new GameObject();
                gameObject.name = $"tile ({tile.mapTx}, {tile.mapTy})";
                gameObject.transform.position = new Vector3(
                    tile.mapTx * Tile.Size,
                    tile.mapTy * Tile.Size
                );
                
                gameObject.transform.parent = this.containerTransform;

                var spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
                spriteRenderer.sprite = tile.sprite;
                spriteRenderer.sortingLayerName = sortingLayerName;
                if(tile.autotileChoseSkew == true)
                    spriteRenderer.sortingOrder = this.sortingOrder + 1;
                else
                    spriteRenderer.sortingOrder = this.sortingOrder;
                tile.spriteRenderer = spriteRenderer;

                if (tile.addInteriorFromTileset != null)
                {
                    TileTexture.Init(
                        gameObject.transform.parent,
                        tile,
                        tile.addInteriorFromTileset,
                        sortingLayerName,
                        sortingOrder: this.sortingOrder - 1,
                        layerMaskNumber
                    );
                }
            }
        }
        private (Tile tile, bool outOfBounds) GetTileForAutotile(int gridTx, int gridTy)
        {
            // the autotile functions need a bit more. we are kind of pretending that
            // there is an infinite grid of filled tiles outside the defined chunks.
            if(gridTx >= 0 && gridTx < this.columns &&
                gridTy >= 0 && gridTy < this.rows)
            {
                return (this.tiles[gridTx + (gridTy * this.columns)], false);
            }

            int mapTx = gridTx + this.xMin;
            int mapTy = gridTy + this.yMin;

            if(this.levelRectInt.Contains(new Vector2Int(mapTx, mapTy)) == false)
                return (null, true);

            return (GetTile(mapTx - this.xMin, mapTy - this.yMin), false);
        }

        private void SelectThinTile(Tileset tileset, Tile tile)
        {
            bool IsFilled((Tile tile, bool outOfBounds) locationContents)
            {
                if(locationContents.outOfBounds)
                    return true;
                else
                    return IsTileRelevant(locationContents.tile);
            }

            bool IsTileRelevant(Tile t)
            {
                if(t == null) return false;

                if(tileset.type == Tileset.TilesetType.Dirt)
                    return (t.tileName == Minigame.GlobalPrefix + "dirt");
                else if(tileset.type == Tileset.TilesetType.Outside)
                    return (t.tileName == Minigame.GlobalPrefix + "outside");

                if(t.spec.shape == Tile.Shape.TopLine) return false;
                if(t.tileName == Minigame.GlobalPrefix + "dirt") return false;
                if(t.tileName == Minigame.GlobalPrefix + "outside") return false;
                if(t.tileName == Minigame.FilledTile) return true;
                return true;
                // return t.spec.shape == Tile.Shape.Square;
            }

            bool IsSingleColumn(Tile t)
            {
                if(t == null || t.spec.shape != Tile.Shape.Square)
                    return false;
                else
                    return
                        IsFilled(GetTileForAutotile(t.gridTx - 1, t.gridTy)) == false &&
                        IsFilled(GetTileForAutotile(t.gridTx + 1, t.gridTy)) == false;
            }

            bool IsSingleRow(Tile t)
            {
                if(t == null || t.spec.shape != Tile.Shape.Square)
                    return false;
                else
                    return
                        IsFilled(GetTileForAutotile(t.gridTx, t.gridTy + 1)) == false &&
                        IsFilled(GetTileForAutotile(t.gridTx, t.gridTy - 1)) == false;
            }

            if(tile.spec.shape != Tile.Shape.Square)
                return;
            if(IsTileRelevant(tile) == false)
                return;

            {
                var l = GetTileForAutotile(tile.gridTx - 1, tile.gridTy);
                var r = GetTileForAutotile(tile.gridTx + 1, tile.gridTy);
                var b = GetTileForAutotile(tile.gridTx, tile.gridTy - 1);
                var t = GetTileForAutotile(tile.gridTx, tile.gridTy + 1);
                var tl = GetTileForAutotile(tile.gridTx - 1, tile.gridTy + 1);
                var tr = GetTileForAutotile(tile.gridTx + 1, tile.gridTy + 1);
                var bl = GetTileForAutotile(tile.gridTx - 1, tile.gridTy - 1);
                var br = GetTileForAutotile(tile.gridTx + 1, tile.gridTy - 1);

                if(IsSingleColumn(tile) == true)
                {
                    var isTop = IsFilled(t) == false || IsSingleColumn(t.tile) == false;
                    var isBottom = IsFilled(b) == false || IsSingleColumn(b.tile) == false;

                    var isShort = false;
                    if(isTop == true)
                    {
                        var bb = GetTileForAutotile(tile.gridTx, tile.gridTy - 2);
                        if(IsFilled(bb) == false || IsSingleColumn(bb.tile) == false)
                            isShort = true;
                    }
                    else if(isBottom == true)
                    {
                        var tt = GetTileForAutotile(tile.gridTx, tile.gridTy + 2);
                        if(IsFilled(tt) == false || IsSingleColumn(tt.tile) == false)
                            isShort = true;
                    }

                    if(isShort == true)
                        tile.sprite = Util.RandomChoice(tileset.singleSquare);
                    else if(isTop == true && isBottom == true)
                        tile.sprite = Util.RandomChoice(tileset.singleSquare);
                    else if(isTop == true && isBottom == false)
                        tile.sprite = tileset.pillar1WideTop;
                    else if(isTop == false && isBottom == true)
                        tile.sprite = tileset.pillar1WideBottom;
                    else
                        tile.sprite = Util.RandomChoice(tileset.pillar1WideMiddle);

                    tile.autotileChoseThin = true;
                }
                else if(IsSingleRow(tile) == true)
                {
                    var isLeft = IsFilled(l) == false || IsSingleRow(l.tile) == false;
                    var isRight = IsFilled(r) == false || IsSingleRow(r.tile) == false;

                    var isShort = false;
                    if(isLeft == true)
                    {
                        var rr = GetTileForAutotile(tile.gridTx + 2, tile.gridTy);
                        if(IsFilled(rr) == false || IsSingleRow(rr.tile) == false)
                            isShort = true;
                    }
                    else if(isRight == true)
                    {
                        var ll = GetTileForAutotile(tile.gridTx - 2, tile.gridTy);
                        if(IsFilled(ll) == false || IsSingleRow(ll.tile) == false)
                            isShort = true;
                    }

                    if(isShort == true)
                        tile.sprite = Util.RandomChoice(tileset.singleSquare);
                    else if(isLeft == true && isRight == true)
                        tile.sprite = Util.RandomChoice(tileset.singleSquare);
                    else if(isLeft == true && isRight == false)
                        tile.sprite = tileset.singleRowLeft;
                    else if(isLeft == false && isRight == true)
                        tile.sprite = tileset.singleRowRight;
                    else
                        tile.sprite = Util.RandomChoice(tileset.singleRowMiddle);

                    tile.autotileChoseThin = true;
                }
                else if(
                    (IsFilled(tl) == false || IsFilled(t) == false || IsFilled(l) == false) &&
                    (IsFilled(tr) == false || IsFilled(t) == false || IsFilled(r) == false) &&
                    (IsFilled(bl) == false || IsFilled(b) == false || IsFilled(l) == false) &&
                    (IsFilled(br) == false || IsFilled(b) == false || IsFilled(r) == false)
                )
                {
                    tile.sprite = Util.RandomChoice(tileset.singleSquare);
                    tile.autotileChoseThin = true;
                }
            }
        }

        private void SelectSlopeTile(Tileset tileset, Tile tile)
        {
            bool IsFilled(int dx, int dy)
            {
                var (t, outOfBounds) =
                    GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy);

                if(outOfBounds == true) return true;
                
                if(t == null) return false;

                if(tileset.type == Tileset.TilesetType.Dirt)
                    return (t.tileName == Minigame.GlobalPrefix + "dirt");
                else if(tileset.type == Tileset.TilesetType.Outside)
                    return (t.tileName == Minigame.GlobalPrefix + "outside");

                if(t.autotileChoseThin == true) return false;
                if(t.spec.shape == Tile.Shape.TopLine) return false;
                if(t.tileName == Minigame.GlobalPrefix + "dirt") return false;
                if(t.tileName == Minigame.GlobalPrefix + "outside") return false;
                if(t.tileName == Minigame.FilledTile) return true;
                return true;
            }

            switch(tile.spec.shape)
            {
                case Tile.Shape.Slope22BR:
                    {
                        bool solidOnLeft = IsFilled(-1, -1);
                        bool solidOnRight = IsFilled(2, 0);
                        bool solidBelow = IsFilled(0, -1) && IsFilled(1, -1);
                        Sprite fallbackSprite = tileset.slope22BottomRight;

                        if(solidOnLeft == true && solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoined22BottomRight;
                        else if(solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedLeft22BottomRight;
                        else if(solidOnLeft == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedRight22BottomRight;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope22BL:
                    {
                        bool solidOnLeft = IsFilled(-1, 0);
                        bool solidOnRight = IsFilled(2, -1);
                        bool solidBelow = IsFilled(0, -1) && IsFilled(1, -1);
                        Sprite fallbackSprite = tileset.slope22BottomLeft;

                        if(solidOnLeft == true && solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoined22BottomLeft;
                        else if(solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedLeft22BottomLeft;
                        else if(solidOnLeft == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedRight22BottomLeft;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope22TL:
                    {
                        bool solidOnLeft = IsFilled(-1, 0);
                        bool solidOnRight = IsFilled(2, 1);
                        bool solidAbove = IsFilled(0, 1) && IsFilled(1, 1);
                        Sprite fallbackSprite = tileset.slope22TopLeft;

                        if(solidOnLeft == true && solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoined22TopLeft;
                        else if(solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedLeft22TopLeft;
                        else if(solidOnLeft == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedRight22TopLeft;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope22TR:
                    {
                        bool solidOnLeft = IsFilled(-1, 1);
                        bool solidOnRight = IsFilled(2, 0);
                        bool solidAbove = IsFilled(0, 1) && IsFilled(1, 1);
                        Sprite fallbackSprite = tileset.slope22TopRight;

                        if(solidOnLeft == true && solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoined22TopRight;
                        else if(solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedLeft22TopRight;
                        else if(solidOnLeft == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedRight22TopRight;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope45BR:
                    {
                        bool solidOnLeft = IsFilled(-1, -1);
                        bool solidOnRight = IsFilled(1, 0);
                        bool solidBelow = IsFilled(0, -1);
                        Sprite fallbackSprite = tileset.slope45BottomRight;

                        if(solidOnLeft == true && solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoined45BottomRight;
                        else if(solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedLeft45BottomRight;
                        else if(solidOnLeft == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedRight45BottomRight;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope45BL:
                    {
                        bool solidOnLeft = IsFilled(-1, 0);
                        bool solidOnRight = IsFilled(1, -1);
                        bool solidBelow = IsFilled(0, -1);
                        Sprite fallbackSprite = tileset.slope45BottomLeft;

                        if(solidOnLeft == true && solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoined45BottomLeft;
                        else if(solidOnRight == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedLeft45BottomLeft;
                        else if(solidOnLeft == true && solidBelow == true)
                            tile.sprite = tileset.slopeJoinedRight45BottomLeft;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope45TL:
                    {
                        bool solidOnLeft = IsFilled(-1, 0);
                        bool solidOnRight = IsFilled(1, 1);
                        bool solidAbove = IsFilled(0, 1);
                        Sprite fallbackSprite = tileset.slope45TopLeft;

                        if(solidOnLeft == true && solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoined45TopLeft;
                        else if(solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedLeft45TopLeft;
                        else if(solidOnLeft == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedRight45TopLeft;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }

                case Tile.Shape.Slope45TR:
                    {
                        bool solidOnLeft = IsFilled(-1, 1);
                        bool solidOnRight = IsFilled(1, 0);
                        bool solidAbove = IsFilled(0, 1);
                        Sprite fallbackSprite = tileset.slope45TopRight;

                        if(solidOnLeft == true && solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoined45TopRight;
                        else if(solidOnRight == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedLeft45TopRight;
                        else if(solidOnLeft == true && solidAbove == true)
                            tile.sprite = tileset.slopeJoinedRight45TopRight;
                        else
                            tile.sprite = fallbackSprite;

                        tile.autotileChoseSkew = (tile.sprite != fallbackSprite);
                        break;
                    }
            }

            if (tile.autotileChoseSkew == true &&
                tileset.interiorUnderneathJoinedSlopes == true)
            {
                tile.addInteriorFromTileset = tileset;
            }
        }

        private void SelectCurveTile(Tileset tileset, Tile tile)
        {
            switch(tile.spec.shape)
            {
                case Tile.Shape.CurveBL3Plus1:
                    if (tileset.curve3Plus1BottomLeft == null &&
                        tileset.curve3Plus1BottomLeftJoined != null)
                    {
                        tile.sprite = tileset.curve3Plus1BottomLeftJoined;
                        tile.addInteriorFromTileset = tileset;
                    }
                    else
                    {
                        tile.sprite = tileset.curve3Plus1BottomLeft;
                    }
                    break;
                case Tile.Shape.CurveBR3Plus1:
                    if (tileset.curve3Plus1BottomRight == null &&
                        tileset.curve3Plus1BottomRightJoined != null)
                    {
                        tile.sprite = tileset.curve3Plus1BottomRight;
                        tile.addInteriorFromTileset = tileset;
                    }
                    else
                    {
                        tile.sprite = tileset.curve3Plus1BottomRight;
                    }
                    break;
                case Tile.Shape.CurveTL3Plus1:
                    if (tileset.curve3Plus1TopLeft == null &&
                        tileset.curve3Plus1TopLeftJoined != null)
                    {
                        tile.sprite = tileset.curve3Plus1TopLeft;
                        tile.addInteriorFromTileset = tileset;
                    }
                    else
                    {
                        tile.sprite = tileset.curve3Plus1TopLeft;
                    }
                    break;
                case Tile.Shape.CurveTR3Plus1:
                    if (tileset.curve3Plus1TopRight == null &&
                        tileset.curve3Plus1TopRightJoined != null)
                    {
                        tile.sprite = tileset.curve3Plus1TopRight;
                        tile.addInteriorFromTileset = tileset;
                    }
                    else
                    {
                        tile.sprite = tileset.curve3Plus1TopRight;
                    }
                    break;
                case Tile.Shape.CurveOutBL3:
                    tile.sprite = tileset.curveOut3Plus1BottomLeft;
                    break;
                case Tile.Shape.CurveOutBR3:
                    tile.sprite = tileset.curveOut3Plus1BottomRight;
                    break;
                case Tile.Shape.CurveOutTL3:
                    tile.sprite = tileset.curveOut3Plus1TopLeft;
                    break;
                case Tile.Shape.CurveOutTR3:
                    tile.sprite = tileset.curveOut3Plus1TopRight;
                    break;
            }

            if (tileset.interiorUnderneathAllCurves == true)
            {
                tile.addInteriorFromTileset = tileset;
            }
        }

        private void SelectTile(Tileset tileset, Tile tile)
        {
            Tile TileRelative(int dx, int dy) =>
                GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy).tile;

            bool IsFilled(int dx, int dy)
            {
                if(tileset.type == Tileset.TilesetType.Back)
                {
                    var st = this.GetTileForAutotile(
                        tile.gridTx + dx, tile.gridTy + dy
                    );

                    if(st.outOfBounds == true) return true;
                    if(st.tile != null)
                    {
                        if(st.tile.IsEntirelySolidInCell(
                            tile.gridTx + dx - st.tile.mapTx,
                            tile.gridTy + dy - st.tile.mapTy
                        ) == true)
                            return true;
                    }
                }

                var (_t, outOfBounds) =
                    GetTileForAutotile(tile.gridTx + dx, tile.gridTy + dy);
                
                if(outOfBounds == true) return true;
                
                if(_t == null) return false;

                if(tileset.type == Tileset.TilesetType.Dirt)
                    return (_t.tileName == Minigame.GlobalPrefix + "dirt");

                if(_t.autotileChoseSkew == true) return true;
                if(_t.autotileChoseThin == true) return false;

                if(_t.spec.shape == Tile.Shape.TopLine) return false;
                if(_t.tileName == Minigame.GlobalPrefix + "dirt") return false;
                if(_t.tileName == Minigame.GlobalPrefix + "outside") return false;
                if(_t.tileName == Minigame.FilledTile) return true;

                if(tileset.flowIntoSlopes == false)
                {
                    if(_t.tileName?.StartsWith(Minigame.GlobalPrefix + "22") == true) return false;
                    if(_t.tileName?.StartsWith(Minigame.GlobalPrefix + "45") == true) return false;
                }
                return (_t.tileName?.StartsWith(Minigame.GlobalPrefix) == true);
            }

            Sprite Select(Sprite[] sprites) =>
                sprites[UnityEngine.Random.Range(0, sprites.Length - 1)];

            var l = IsFilled(-1, 0);
            var r = IsFilled(1, 0);
            var b = IsFilled(0, -1);
            var t = IsFilled(0, 1);
            var tl = IsFilled(-1, 1);
            var tr = IsFilled(1, 1);
            var bl = IsFilled(-1, -1);
            var br = IsFilled(1, -1);

            if(TileRelative(0, 1)?.autotileChoseSkew == true)
                l = r = tl = tr = t = true;
            if(TileRelative(0, -1)?.autotileChoseSkew == true)
                l = r = bl = br = b = true;

            var allSides = l && r && t && b;
            var allCorners = tl && tr && bl && br;

            if(allSides == true && allCorners == true)
            {
                tile.addInteriorFromTileset = tileset;
            }

            else if(l == false && r == true && b == true && t == true) tile.sprite = Select(tileset.left);
            else if(l == true && r == false && b == true && t == true) tile.sprite = Select(tileset.right);
            else if(l == true && r == true && b == false && t == true) tile.sprite = Select(tileset.bottom);
            else if(l == true && r == true && b == true && t == false) tile.sprite = Select(tileset.top);

            else if(l == true && bl == true && b == true && r == false && t == false) tile.sprite = tileset.topRight;
            else if(r == true && br == true && b == true && l == false && t == false) tile.sprite = tileset.topLeft;
            else if(l == true && tl == true && t == true && r == false && b == false) tile.sprite = tileset.bottomRight;
            else if(r == true && tr == true && t == true && l == false && b == false) tile.sprite = tileset.bottomLeft;

            else if(allSides == true && tl == false && tr == true && bl == true && br == true) tile.sprite = tileset.insideTopLeft;
            else if(allSides == true && tl == true && tr == false && bl == true && br == true) tile.sprite = tileset.insideTopRight;
            else if(allSides == true && tl == true && tr == true && bl == false && br == true) tile.sprite = tileset.insideBottomLeft;
            else if(allSides == true && tl == true && tr == true && bl == true && br == false) tile.sprite = tileset.insideBottomRight;

            else if(tileset.singleSquare.Length > 0)
                tile.sprite = Util.RandomChoice(tileset.singleSquare);

            if((allSides == false || allCorners == false) &&
                tileset.interiorUnderneathEdges == true)
            {
                tile.addInteriorFromTileset = tileset;
            }
        }
        public Tile GetTile(int gridTx, int gridTy)
        {
            // note: the tile you get back might have a different tx, ty
            if(gridTx < 0 || gridTx >= columns) return null;
            if(gridTy < 0 || gridTy >= rows) return null;
            return this.tiles[gridTx + (gridTy * columns)];
        }
    }

}
