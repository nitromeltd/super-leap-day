using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace minigame
{

    // All methods for building Unity physics around TileGrid Tiles

    public partial class TileGrid
    {
        public PhysicsMaterial2D physicsMaterialNormal;
        public PhysicsMaterial2D physicsMaterialDirt;
        public PhysicsMaterial2D physicsMaterialSlope;
        public ObjectLayers objectLayers;
        public bool isPolySlopes;
        public bool isNoMergers;

        public static Dictionary<Tile.Shape, Vector2[]> PathFromShape = new Dictionary<Tile.Shape, Vector2[]>
        {
            [Tile.Shape.Slope22BL] = new Vector2[] { new Vector2(0, 1), new Vector2(2, 0) }
            , [Tile.Shape.Slope22BR] = new Vector2[] { new Vector2(0, 0), new Vector2(2, 1) }

            , [Tile.Shape.Slope22TL] = new Vector2[] { new Vector2(0, 0), new Vector2(2, 1) }
            , [Tile.Shape.Slope22TR] = new Vector2[] { new Vector2(0, 1), new Vector2(2, 0) }

            , [Tile.Shape.Slope45BL] = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0) }
            , [Tile.Shape.Slope45BR] = new Vector2[] { new Vector2(0, 0), new Vector2(1, 1) }

            , [Tile.Shape.Slope45TL] = new Vector2[] { new Vector2(0, 0), new Vector2(1, 1) }
            , [Tile.Shape.Slope45TR] = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0) }

            , [Tile.Shape.TopLine] = new Vector2[] { new Vector2(0, 1), new Vector2(1, 1) }

            // Keys for complex polygons are added via AddPathFromShapeTemplates() at runtime
        };

        // Alternate solution for slopes in polygon-only systems
        public static Dictionary<Tile.Shape, Vector2[]> PolyFromShape = new Dictionary<Tile.Shape, Vector2[]>
        {
            [Tile.Shape.Slope22BL] = new Vector2[] { new Vector2(0, 1), new Vector2(2, 0), new Vector2(0, 0) }
            , [Tile.Shape.Slope22BR] = new Vector2[] { new Vector2(0, 0), new Vector2(2, 1), new Vector2(2, 0) }

            , [Tile.Shape.Slope22TL] = new Vector2[] { new Vector2(0, 0), new Vector2(2, 1), new Vector2(0, 1) }
            , [Tile.Shape.Slope22TR] = new Vector2[] { new Vector2(0, 1), new Vector2(2, 0), new Vector2(2, 1) }

            , [Tile.Shape.Slope45BL] = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0), new Vector2(0, 0) }
            , [Tile.Shape.Slope45BR] = new Vector2[] { new Vector2(0, 0), new Vector2(1, 1), new Vector2(1, 0) }

            , [Tile.Shape.Slope45TL] = new Vector2[] { new Vector2(0, 0), new Vector2(1, 1), new Vector2(1, 0) }
            , [Tile.Shape.Slope45TR] = new Vector2[] { new Vector2(0, 1), new Vector2(1, 0), new Vector2(1, 1) }
        };

        public static Dictionary<string, bool> RectCloudWhiteList = new Dictionary<string, bool>
        {
            [Minigame.GlobalPrefix + "square"] = true
            , [Minigame.GlobalPrefix + "dirt"] = true
            , [Minigame.GlobalPrefix + "topline"] = true
        };

        public void GetColliders()
        {
            var names = new Dictionary<Vector2Int, string>();
            var visited = new Dictionary<Vector2Int, Tile>();
            var colliderLookup = new Dictionary<Vector2Int, Collider2D>();

            for(int i = 0; i < tiles.Length; i++)
            {
                var t = this.tiles[i];
                if(t != null)
                {
                    var p = new Vector2Int(t.mapTx, t.mapTy);
                    visited.TryGetValue(p, out Tile tileAtP);
                    if(tileAtP == null || t != tileAtP)
                    {
                        var collider = GetCollider2D(t);
                        if(collider != null)
                        {
                            names[p] = t.tileName;
                            colliderLookup[p] = collider;
                            if(t.spec.spanX == 1 && t.spec.spanY == 1)
                                visited[p] = t;
                            else
                                for(int r = 0; r < t.spec.spanY; r++)
                                    for(int c = 0; c < t.spec.spanX; c++)
                                        visited[p + new Vector2Int(c, r)] = t;
                        }
                    }

                    // debug
                    //t.spriteRenderer.color = Color.black;
                }
            }

            if(isNoMergers == false)
            {
                var mergers = MergeData.GetMergers(names, new RectInt(xMin, yMin, columns, rows), RectCloudWhiteList);
                //Debug.Log(mergers.Count);
                MergeData.MergeRectsAndClouds(mergers, colliderLookup);
                MergeSlopes(names, colliderLookup);
            }
        }

        public class MergeData
        {
            public RectInt rect;
            public List<Vector2Int> list;
            public MergeData(RectInt rect, List<Vector2Int> list)
            {
                this.rect = rect;
                this.list = list;
            }

            // Performs mergers with BoxColliders and PlatformEffectors
            public static void MergeRectsAndClouds(List<MergeData> mergers, Dictionary<Vector2Int, Collider2D> colliderLookup)
            {
                foreach(var m in mergers)
                {
                    for(int i = 1; i < m.list.Count; i++)
                    {
                        var coll = colliderLookup[m.list[i]];
                        if(coll is EdgeCollider2D)
                        {
                            var eff = coll.GetComponent<PlatformEffector2D>();
                            if(eff != null)
                            {
                                GameObject.Destroy(eff);
                            }
                        }
                        GameObject.Destroy(coll);
                    }
                    var c = colliderLookup[m.list[0]];
                    if(c is BoxCollider2D)
                    {
                        var b = c as BoxCollider2D;
                        b.size = (Vector2)m.rect.size * Minigame.Scale;
                        c.offset = b.size * 0.5f;
                    }
                    else if(c is EdgeCollider2D)
                    {
                        var e = c as EdgeCollider2D;
                        var points = e.points;
                        var p = e.points[1];
                        p.x = m.rect.width * Minigame.Scale;
                        points[1] = p;
                        e.points = points;
                    }
                }
            }
            public static List<MergeData> GetMergers(Dictionary<Vector2Int, string> names, RectInt bounds, Dictionary<string, bool> whitelist)
            {
                var list = new List<MergeData>();
                var keys = names.Keys.ToList();
                foreach(var k in keys)
                {
                    if(names.ContainsKey(k) && whitelist.ContainsKey(names[k]))// collection has been modified
                    {
                        var m = GetMergeData(k, names, bounds);
                        if(m != null)
                        {
                            foreach(var p in m.list)
                            {
                                names.Remove(p);
                            }
                            list.Add(m);
                        }
                    }
                }
                return list;
            }

            public static MergeData GetMergeData(Vector2Int seed, Dictionary<Vector2Int, string> names, RectInt bounds)
            {
                var id = names[seed];
                var size = Vector2Int.one;
                var p = seed;
                var list = new List<Vector2Int> { seed };
                // get initial y-bar
                p.y++;
                while(names.ContainsKey(p) && names[p] == id)
                {
                    list.Add(p);
                    size.y++;
                    p.y++;
                }
                // try to make bar wider
                for(p.x = seed.x + 1; p.x < bounds.xMax; p.x++)
                {
                    for(p.y = seed.y; p.y < seed.y + size.y; p.y++)
                    {
                        if(!names.ContainsKey(p) || names[p] != id)
                        {
                            goto breakWider;
                        }
                    }
                    // add new bar length to list
                    for(p.y = seed.y; p.y < seed.y + size.y; p.y++)
                    {
                        list.Add(p);
                    }
                    size.x++;
                }
                breakWider:;
                if(list.Count > 1)
                {
                    return new MergeData(new RectInt(seed, size), list);
                }
                return null;
            }
        }

        /* Merges sloping edge colliders
         * - returns a dictionary of the ends of slopes (for gfx)
         * - removes destroyed colliders from colliderLookup
         */
        public Dictionary<Vector2Int, GameObject> MergeSlopes(Dictionary<Vector2Int, string> names, Dictionary<Vector2Int, Collider2D> colliderLookup)
        {
            var endObjs = new Dictionary<Vector2Int, GameObject>();
            for(int y = 0; y < this.yMax; y++)
            {
                for(int x = 0; x < this.xMax; x++)
                {
                    var p = new Vector2Int(x, y);
                    if(names.ContainsKey(p) == true && colliderLookup.ContainsKey(p) == true)
                    {
                        EdgeCollider2D e = colliderLookup[p] as EdgeCollider2D;
                        if(e != null)
                        {
                            var shape = names[p];
                            var pts = e.points;
                            GameObject endObj = null;
                            // get world space gradient
                            Vector2 v = e.transform.TransformPoint(pts[1]) - e.transform.TransformPoint(pts[0]);
                            // get edge collider gradient
                            Vector2 vReal = pts[1] - pts[0];
                            //Debug.Log("slope is " + v);
                            // convert to integer
                            var vInt = new Vector2Int((int)(v.x / Minigame.Scale), (int)(v.y / Minigame.Scale));
                            // we're scanning the map upwards, so a downwards vector won't work
                            var flipped = false;
                            if(vInt.y < 0)
                            {
                                vInt = -vInt;
                                flipped = true;
                            }
                            //Debug.Log("to integer " + vInt);
                            p += vInt;
                            while
                            (
                                names.ContainsKey(p) == true &&
                                names[p] == shape &&
                                colliderLookup.ContainsKey(p) == true
                            )
                            {
                                var ek = (EdgeCollider2D)colliderLookup[p];
                                Vector2 vk = e.transform.TransformPoint(ek.points[1]) - e.transform.TransformPoint(ek.points[0]);
                                if(vk == v)
                                {
                                    if(flipped)
                                        pts[0] -= vReal;
                                    else
                                        pts[1] += vReal;
                                    endObj = ek.gameObject;
                                    var pe = ek.GetComponent<PlatformEffector2D>();
                                    if(pe != null)
                                        GameObject.Destroy(pe);
                                    GameObject.Destroy(ek);
                                    colliderLookup.Remove(p);
                                }
                                else
                                    break;
                                p += vInt;
                            }
                            endObjs[p - vInt] = endObj;
                            e.points = pts;
                        }
                    }
                }
            }
            return endObjs;
        }

        public Collider2D GetCollider2D(Tile tile)
        {
            // this tile usually means we have a special prefab in place
            if(tile.tileName == Minigame.GlobalPrefix + "filled") return null;

            var obj = tile.spriteRenderer.gameObject;
            Collider2D collider = null;
            obj.layer = ObjectLayers.ToLayer(this.objectLayers.tileLayerMask.value);

            switch(tile.spec.shape)
            {
                case Tile.Shape.Square:
                    var box = obj.AddComponent<BoxCollider2D>();
                    collider = box;
                    box.size = new Vector2(Tile.Size * tile.spec.spanX, Tile.Size * tile.spec.spanY);
                    box.offset = box.size * 0.5f;
                    if(tile.tileName == Minigame.GlobalPrefix + "dirt")
                        collider.sharedMaterial = this.physicsMaterialDirt ?? this.physicsMaterialNormal;
                    else
                        collider.sharedMaterial = this.physicsMaterialNormal;
                    break;
                case Tile.Shape.TopLine:
                    {
                        var edgeCollider = obj.AddComponent<EdgeCollider2D>();
                        collider = edgeCollider;
                        edgeCollider.points = PathFromShape[tile.spec.shape];
                        edgeCollider.usedByEffector = true;
                        var platformEffector = obj.AddComponent<PlatformEffector2D>();
                        platformEffector.useOneWay = true;
                        platformEffector.useOneWayGrouping = true;
                        platformEffector.surfaceArc = 180;
                        platformEffector.colliderMask = this.objectLayers.objectLayerMask.value | this.objectLayers.playerLayerMask.value;
                        goto default;
                    }
                case Tile.Shape.Slope22BL: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope22BR: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope22TL: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope22TR: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope45BL: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope45BR: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope45TL: goto case Tile.Shape.Slope45TR;
                case Tile.Shape.Slope45TR:
                    {
                        if(this.isPolySlopes == true)
                        {
                            var polygonCollider = obj.AddComponent<PolygonCollider2D>();
                            collider = polygonCollider;
                            polygonCollider.points = PolyFromShape[tile.spec.shape];
                            collider.sharedMaterial = this.physicsMaterialSlope ?? this.physicsMaterialNormal;
                        }
                        else
                        {
                            var edgeCollider = obj.AddComponent<EdgeCollider2D>();
                            collider = edgeCollider;
                            edgeCollider.points = PathFromShape[tile.spec.shape];
                            collider.sharedMaterial = this.physicsMaterialSlope ?? this.physicsMaterialNormal;
                        }
                        break;
                    }
                case Tile.Shape.CurveOutBL3: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveOutBR3: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveOutTL3: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveOutTR3: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveBL3Plus1: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveBR3Plus1: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveTL3Plus1: goto case Tile.Shape.CurveTR3Plus1;
                case Tile.Shape.CurveTR3Plus1:
                    {
                        var polygonCollider = obj.AddComponent<PolygonCollider2D>();
                        collider = polygonCollider;
                        polygonCollider.points = PathFromShape[tile.spec.shape];
                        goto default;
                    }
                default:
                    if(collider != null)
                    {
                        collider.sharedMaterial = this.physicsMaterialNormal;
                    }
                    break;
            }

            return collider;
        }

        public static void AddPathFromShapeTemplates(GameObject[] templates)
        {
            var tempObj = new GameObject("tempShapeTemplates");
            foreach(var fab in templates)
            {
                var obj = GameObject.Instantiate(fab, tempObj.transform);
                var polygon = obj.GetComponent<PolygonCollider2D>();
                //Debug.Log("build polygon: " + fab.name);
                PathFromShape[Tile.Specs[fab.name].shape] = polygon.points;
            }
            GameObject.Destroy(tempObj);
        }
    }
}
