
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace minigame
{

    public class TileTexture
    {
        public static Material SpritesDefaultMaterial;

        private struct ShapeKey
        {
            public Tile.Shape shape;
            public int tx;
            public int ty;
        }
        private struct CurveKey
        {
            public Tile.Spec spec;
            public int tx;
            public int ty;
        }

        private static Dictionary<ShapeKey, Vector2[]> backShapesForShapes;
        private static Dictionary<CurveKey, Vector2[]> backShapesForCurves;

        private static void FillDictionary()
        {
            backShapesForShapes = new Dictionary<ShapeKey, Vector2[]>();
            backShapesForCurves = new Dictionary<CurveKey, Vector2[]>();

            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope45BR }] = new [] {
                new Vector2(0, 0),
                new Vector2(1, 1),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope45BL }] = new [] {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope45TR }] = new [] {
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope45TL }] = new [] {
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(0, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22BR, tx = 0, ty = 0 }] = new [] {
                new Vector2(0, 0),
                new Vector2(1, 0.5f),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22BR, tx = 1, ty = 0 }] = new [] {
                new Vector2(0, 0),
                new Vector2(0, 0.5f),
                new Vector2(1, 1),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22BL, tx = 0, ty = 0 }] = new [] {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0.5f),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22BL, tx = 1, ty = 0 }] = new [] {
                new Vector2(0, 0),
                new Vector2(0, 0.5f),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22TR, tx = 0, ty = 0 }] = new [] {
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 0.5f)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22TR, tx = 1, ty = 0 }] = new [] {
                new Vector2(0, 0.5f),
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 0)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22TL, tx = 0, ty = 0 }] = new [] {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 1),
                new Vector2(1, 0.5f)
            };
            backShapesForShapes[new ShapeKey { shape = Tile.Shape.Slope22TL, tx = 1, ty = 0 }] = new [] {
                new Vector2(0, 0.5f),
                new Vector2(0, 1),
                new Vector2(1, 1)
            };

            FillShapeForTRCurve(Tile.Spec.CurveTR3Plus1);
            FillShapeForTRCurve(Tile.Spec.CurveTR4Plus1);
            FillShapeForTRCurve(Tile.Spec.CurveTR8Plus1);
            FillShapeForTRCurve(Tile.Spec.CurveOutTR3);

            FillShapeForCurveByReflection(Tile.Spec.CurveTL3Plus1, Tile.Spec.CurveTR3Plus1, true, false);
            FillShapeForCurveByReflection(Tile.Spec.CurveBL3Plus1, Tile.Spec.CurveTR3Plus1, true, true);
            FillShapeForCurveByReflection(Tile.Spec.CurveBR3Plus1, Tile.Spec.CurveTR3Plus1, false, true);

            FillShapeForCurveByReflection(Tile.Spec.CurveTL4Plus1, Tile.Spec.CurveTR4Plus1, true, false);
            FillShapeForCurveByReflection(Tile.Spec.CurveBL4Plus1, Tile.Spec.CurveTR4Plus1, true, true);
            FillShapeForCurveByReflection(Tile.Spec.CurveBR4Plus1, Tile.Spec.CurveTR4Plus1, false, true);

            FillShapeForCurveByReflection(Tile.Spec.CurveTL8Plus1, Tile.Spec.CurveTR8Plus1, true, false);
            FillShapeForCurveByReflection(Tile.Spec.CurveBL8Plus1, Tile.Spec.CurveTR8Plus1, true, true);
            FillShapeForCurveByReflection(Tile.Spec.CurveBR8Plus1, Tile.Spec.CurveTR8Plus1, false, true);

            FillShapeForCurveByReflection(Tile.Spec.CurveOutTL3, Tile.Spec.CurveOutTR3, true, false);
            FillShapeForCurveByReflection(Tile.Spec.CurveOutBL3, Tile.Spec.CurveOutTR3, true, true);
            FillShapeForCurveByReflection(Tile.Spec.CurveOutBR3, Tile.Spec.CurveOutTR3, false, true);
        }

        private static void FillShapeForTRCurve(Tile.Spec spec)
        {
            var intersections = new List<Vector2>();
            float bias = spec.curve.solidInside ? -0.125f : 0.125f;
            float radiusTiles = (spec.curve.radius + bias) / Tile.Size;

            for(var x = 0; x <= radiusTiles; x += 1)
            {
                var theta = Mathf.Acos((float)x / (float)radiusTiles);
                var sin = Mathf.Abs(Mathf.Sin(theta));
                var i = new Vector2(x, radiusTiles * sin);
                intersections.Add(i);
            }
            for(var y = 0; y <= radiusTiles; y += 1)
            {
                var theta = Mathf.Asin((float)y / (float)radiusTiles);
                var cos = Mathf.Abs(Mathf.Cos(theta));
                var i = new Vector2(cos * radiusTiles, y);
                intersections.Add(i);
            }

            intersections = intersections.OrderBy(i => i.x).ToList();

            for(int y = 0; y < spec.spanY; y += 1)
            {
                for(int x = 0; x < spec.spanX; x += 1)
                {
                    var key = new CurveKey { spec = spec, tx = x, ty = y };

                    var iList = intersections.Select(
                        i => new Vector2(i.x - x, i.y - y)
                    ).Where(i =>
                        i.x >= 0 && i.x <= 1 &&
                        i.y >= 0 && i.y <= 1
                    ).ToList();
                    if(iList.Count == 0)
                    {
                        var pt = new Vector2(x + 0.5f, y + 0.5f) * Tile.Size;
                        var outside = (spec.curve.center - pt).sqrMagnitude >=
                            spec.curve.radius * spec.curve.radius;
                        if(outside != spec.curve.solidInside)
                        {
                            backShapesForCurves[key] = new[]
                            {
                            new Vector2(0, 0),
                            new Vector2(1, 0),
                            new Vector2(1, 1),
                            new Vector2(0, 1)
                        };
                        }
                        continue;
                    }

                    if(spec.curve.solidInside == true)
                    {
                        var tl = iList.First().y >= 0.99f;
                        var br = iList.Last().x >= 0.99f;
                        if(tl) iList.Insert(0, new Vector2(0, 1));
                        iList.Insert(0, new Vector2(0, 0));
                        if(br) iList.Add(new Vector2(1, 0));
                    }
                    else
                    {
                        var br = iList.Last().y <= 0.01f;
                        var tl = iList.First().x <= 0.01f;
                        if(tl) iList.Insert(0, new Vector2(0, 1));
                        iList.Insert(0, new Vector2(1, 1));
                        if(br) iList.Add(new Vector2(1, 0));
                    }

                    backShapesForCurves[key] = iList.ToArray();
                }
            }
        }

        private static void FillShapeForCurveByReflection(
            Tile.Spec targetSpec,
            Tile.Spec sourceSpec,
            bool flipHorizontal,
            bool flipVertical
        )
        {
            for(int y = 0; y < targetSpec.spanY; y += 1)
            {
                for(int x = 0; x < targetSpec.spanX; x += 1)
                {
                    var targetKey = new CurveKey { spec = targetSpec, tx = x, ty = y };
                    var sourceKey = new CurveKey { spec = sourceSpec, tx = x, ty = y };
                    if(flipHorizontal) sourceKey.tx = sourceSpec.spanX - 1 - sourceKey.tx;
                    if(flipVertical) sourceKey.ty = sourceSpec.spanY - 1 - sourceKey.ty;

                    Vector2[] shape;
                    if(backShapesForCurves.TryGetValue(sourceKey, out shape) == false)
                        continue;

                    shape = shape.ToArray();

                    if(flipHorizontal)
                    {
                        for(var n = 0; n < shape.Length; n += 1)
                            shape[n].x = 1.0f - shape[n].x;
                    }
                    if(flipVertical)
                    {
                        for(var n = 0; n < shape.Length; n += 1)
                            shape[n].y = 1.0f - shape[n].y;
                    }

                    backShapesForCurves[targetKey] = shape;
                }
            }
        }

        private static int[] TriangleFan(int indexCount)
        {
            if(indexCount == 3) return new[] { 0, 1, 2 };
            if(indexCount == 4) return new[] { 0, 1, 2, 0, 2, 3 };
            if(indexCount == 5) return new[] { 0, 1, 2, 0, 2, 3, 0, 3, 4 };
            if(indexCount == 6) return new[] { 0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5 };
            return null;
        }

        public static GameObject Init(
            Transform parent,
            Tile tile,
            Tileset tileset,
            string sortingLayerName,
            int sortingOrder,
            int layerMaskNumber
        )
        {
            Sprite InteriorAt(int mapTx, int mapTy)
            {
                if (tileset.interiorStyle == Tileset.InteriorStyle.Grid)
                {
                    int cx = mapTx % tileset.interiorGridColumns;
                    int cy = mapTy % tileset.interiorGridRows;
                    if (cx < 0) cx += tileset.interiorGridColumns;
                    if (cy < 0) cy += tileset.interiorGridRows;
                    cy = (tileset.interiorGridRows - 1) - cy;
                    return tileset.interior[
                        cx + (cy * tileset.interiorGridColumns)
                    ];
                }
                else if (tileset.interiorStyle == Tileset.InteriorStyle.Random)
                {
                    return Util.RandomChoice(tileset.interior);
                }
                else
                {
                    return null;
                }
            }

            if (tile.spec.shape == Tile.Shape.Square)
            {
                var spriteObject   = new GameObject();
                var spriteRenderer = spriteObject.AddComponent<SpriteRenderer>();

                spriteRenderer.sprite = InteriorAt(tile.mapTx, tile.mapTy);
                spriteObject.name = $"pattern ({tile.mapTx}, {tile.mapTy})";
                spriteObject.transform.position = new Vector2(
                    tile.mapTx * Tile.Size, tile.mapTy * Tile.Size
                );
                spriteObject.transform.parent = parent;
                spriteRenderer.sortingLayerName = sortingLayerName;
                spriteRenderer.sortingOrder = sortingOrder;
                spriteObject.gameObject.layer = layerMaskNumber;

                return spriteObject;
            }

            if (backShapesForShapes == null)
                FillDictionary();

            var vertices = new List<Vector3>();
            var uvs      = new List<Vector2>();
            var colors   = new List<Color>();
            var indices  = new List<int>();

            for (int dy = 0; dy < tile.spec.spanY; dy += 1)
            {
                for (int dx = 0; dx < tile.spec.spanX; dx += 1)
                {
                    Sprite sprite = InteriorAt(tile.mapTx + dx, tile.mapTy + dy);

                    var uvXs = sprite.uv.Select(v => v.x);
                    var uvYs = sprite.uv.Select(v => v.y);
                    Vector2 min = new Vector2(uvXs.Min(), uvYs.Min());
                    Vector2 max = new Vector2(uvXs.Max(), uvYs.Max());
                    Vector2 mid = (min + max) * 0.5f;

                    Vector2[] shape;
                    if (tile.spec.curve != null)
                    {
                        var key = new CurveKey { spec = tile.spec, tx = dx, ty = dy };
                        if (backShapesForCurves.TryGetValue(key, out shape) == false)
                            continue;
                    }
                    else
                    {
                        var key = new ShapeKey { shape = tile.spec.shape, tx = dx, ty = dy };
                        if (backShapesForShapes.TryGetValue(key, out shape) == false)
                            continue;
                    }
                    if (shape == null) continue;

                    int firstIndex = vertices.Count;
                    foreach (var shapeVertex in shape)
                    {
                        vertices.Add(new Vector3(
                            dx + shapeVertex.x, dy + shapeVertex.y, 0
                        ));
                        uvs.Add(new Vector2(
                            Mathf.Lerp(min.x, max.x, shapeVertex.x),
                            Mathf.Lerp(min.y, max.y, shapeVertex.y)
                        ));
                        colors.Add(Color.white);
                    }
                    foreach (var shapeIndex in TriangleFan(shape.Length))
                    {
                        indices.Add(firstIndex + shapeIndex);
                    }
                }
            }

            var mesh = new Mesh();
            mesh.SetVertices(vertices.ToList());
            mesh.SetUVs(0, uvs.ToList());
            mesh.SetColors(colors.ToList());
            mesh.SetIndices(indices, MeshTopology.Triangles, 0);

            var gameObject   = new GameObject();
            var meshFilter   = gameObject.AddComponent<MeshFilter>();
            var meshRenderer = gameObject.AddComponent<MeshRenderer>();

            gameObject.name = $"pattern mesh ({tile.mapTx}, {tile.mapTy})";
            gameObject.transform.parent = parent;
            gameObject.transform.position =
                new Vector2(tile.mapTx * Tile.Size, tile.mapTy * Tile.Size);
            meshFilter.mesh = mesh;
            meshRenderer.material = new Material(SpritesDefaultMaterial);
            meshRenderer.material.mainTexture = InteriorAt(0, 0).texture;
            meshRenderer.sortingLayerName = sortingLayerName;
            meshRenderer.sortingOrder = sortingOrder;
            gameObject.layer = layerMaskNumber;

            return gameObject;
        }
    }
}
