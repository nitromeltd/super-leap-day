
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    [CreateAssetMenu(menuName = "Minigame/TileTheme")]
    public class TileTheme : ScriptableObject
    {
        [Header("Tile sets")]
        public Tileset normalTiles;
        public Tileset dirtTiles;
        //public Tileset outsideTiles;
        public Tileset backTiles;
        //public Tileset checkerboardLightTiles;
        //public Tileset checkerboardDarkTiles;

        [Header("Cloud platform tiles")]
        public Sprite cloudLeft;
        public Sprite cloudRight;
        public Sprite[] cloudMiddle;


        //[Header("Outside texture")]
        //public Sprite outsideTexture;
        //public int outsideTextureSize;

        //[Serializable]
        //public class FoliageDefinition
        //{
        //    public string name;
        //    public Animated.Animation idleAnimation;
        //    public Animated.Animation brushLeftAnimation;
        //    public Animated.Animation brushRightAnimation;
        //}

        //public enum DecorationStyle
        //{
        //    Floor,
        //    LeftWall,
        //    RightWall,
        //    Ceiling,
        //    OnSolidTiles,
        //    OnBackTiles
        //}

        //[Serializable]
        //public class DecorationDefinition
        //{
        //    public string name;
        //    public Sprite sprite;
        //    public DecorationStyle style;
        //    public bool flipSpriteHorizontally;
        //    public int reachLeft;
        //    public int reachRight;
        //    public int reachAbove;
        //    public int reachBelow;
        //}

        //[System.Serializable]
        //public class PillarDefinition
        //{
        //    public Sprite top;
        //    public Sprite[] middle;
        //}

        //[Header("Surface decoration")]
        //public FoliageDefinition[] foliageTallAnimated;
        //public FoliageDefinition[] foliageShortAnimated;
        //public DecorationDefinition[] decorationSprites;
        //public PillarDefinition pillar;

        public void OnEnable()
        {
            void Check(Tileset tileset, string name, Tileset.TilesetType type)
            {
                if (tileset.type != type)
                {
                    Debug.LogError(
                        $"{this.name}.{name}: tileset has wrong type " +
                        $"({tileset.type} instead of {type})."
                    );
                }
            }
            Check(this.normalTiles, "normalTiles", Tileset.TilesetType.Normal);
            Check(this.dirtTiles, "dirtTiles", Tileset.TilesetType.Dirt);
            //Check(this.outsideTiles, "outsideTiles", Tileset.TilesetType.Outside);
            Check(this.backTiles, "backTiles", Tileset.TilesetType.Back);
            //Check(this.checkerboardLightTiles, "checkerboardLightTiles", Tileset.TilesetType.CheckerboardLight);
            //Check(this.checkerboardDarkTiles, "checkerboardDarkTiles", Tileset.TilesetType.CheckerboardDark);
        }
    }

}
