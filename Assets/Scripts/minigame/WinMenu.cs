using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using ShopItem = SaveData.ShopItem;

namespace minigame
{
    public class WinMenu : MonoBehaviour
    {
        public Minigame game;
        public TMPro.TMP_Text titleText;
        public RectTransform transformShaker;
        public RectTransform transformBorder;
        public RectTransform transformSpinner;
        public RectTransform transformSpinnerShine;
        public RectTransform transformMiddle;
        public RectTransform transformPointer;
        public RectTransform transformPointerPin;
        public RectTransform transformStarRing;
        public RectTransform transformTrophyShine;
        public RectTransform transformTrophyStarHolder;
        public RectTransform transformPrizeHolder;
        public RectTransform circleImageTransform;
        public RectTransform[] transformPrizeBubbles;
        public RectTransform transformUiBehindCircle;
        public RectTransform transformUiInFrontOfCircle;
        public RectTransform transformAwardHolder;
        public RectTransform transformArcade;
        public RectTransform buttonExit;
        public RectTransform buttonContinue;
        public RectTransform buttonContinueSmall;
        public RectTransform buttonShop;
        public RectTransform buttonShopBig;
        public TMPro.TMP_Text textReward;
        public SelectionCursor selectionCursor;
        public Animated[] animPrizeBubbles;
        public Image[] imagePrizeItems;
        public Image imageStopPrizeFlash;
        public Image imageStopSpinnerFlash;
        public Image imageTrophy;
        public Image imageMedal;
        public Image imageStopSpinnerShakeBorder;
        [NonSerialized] public Material circleMaterial;
        public GameObject prefabTrophyStar;
        public Animated.Animation animBubblePop;
        public Sprite[] spritePrizeIcons;// In order of appearance in enum Prize
        public InputTypePrompt inputPrompt;

        public AnimationCurve circleInAnimationCurve;
        public AnimationCurve circleOutAnimationCurve;
        public AnimationCurve bubbleInAnimationCurve;
        public AnimationCurve spinnerAnimationCurve;
        public AnimationCurve pointerAnimationCurve;
        public AnimationCurve stopFlashAnimationCurve;

        public bool isOpen = false;
        public bool isCompletedMinigameDifficultyEvent = false;
        public float circleOpenAmount = 1;
        public float circleClosedAmount = 0;

        private Tween.Ease easeCircleIn;
        private Tween.Ease easeCircleOut;
        private Tween.Ease easeButtons;
        private Tween.Ease easeSpin;
        private Tween.Ease easePointer;
        private Tween.Ease easeStopFlash;
        private bool spinning;
        private float spinSpeed;
        private bool lockButtons;
        private Minigame.Type typeMinigame;
        private const float SpinSpeedMax = 6f;
        private const float SpinSpeedAcc = 0.0625f;
        

        public List<RectTransform> transformSpins;

        private List<TrophyStar> trophyStars;

        public static Vector3 HideScale = new Vector3(0, 0, 1);

        public static int Radius = Shader.PropertyToID("_Radius");
        public static int OutlineThickness = Shader.PropertyToID("_OutlineThickness");

        private bool hasTicked = false;
        public enum Prize
        {
            Coin,
            Invincibility,
            PetYellow,
            PetTornado,
            Shield,
            InfiniteJump,
            SwordAndShield,
            MidasTouch
        }

        public static Prize[] PrizeDeck = new Prize[]
        {
            Prize.Coin
            ,Prize.Coin
            ,Prize.Coin
            ,Prize.Coin
            ,Prize.Invincibility
            ,Prize.PetYellow
            ,Prize.PetTornado
            ,Prize.Shield
        };

        public Dictionary<Prize, PowerupPickup.Type> PrizeToPowerup = new Dictionary<Prize, PowerupPickup.Type>
        {
            [Prize.Coin] = PowerupPickup.Type.None
            , [Prize.Invincibility] = PowerupPickup.Type.Invincibility
            , [Prize.PetYellow] = PowerupPickup.Type.PetYellow
            , [Prize.PetTornado] = PowerupPickup.Type.PetTornado
            , [Prize.Shield] = PowerupPickup.Type.Shield
            , [Prize.InfiniteJump] = PowerupPickup.Type.InfiniteJump
            , [Prize.SwordAndShield] = PowerupPickup.Type.SwordAndShield
            , [Prize.MidasTouch] = PowerupPickup.Type.MidasTouch
        };

        private void Start()
        {
            var circleImage = this.circleImageTransform.GetComponent<Image>();
            this.circleMaterial = circleImage.material = new Material(circleImage.material);
            this.circleMaterial.SetFloat(Radius, 0.5f);
            this.easeCircleIn = Easing.ByAnimationCurve(this.bubbleInAnimationCurve);
            this.easeCircleOut = Easing.ByAnimationCurve(this.circleOutAnimationCurve);
            this.easeButtons = Easing.ByAnimationCurve(this.bubbleInAnimationCurve);
            this.easeSpin = Easing.ByAnimationCurve(this.spinnerAnimationCurve);
            this.easePointer = Easing.ByAnimationCurve(this.pointerAnimationCurve);
            this.easeStopFlash = Easing.ByAnimationCurve(this.stopFlashAnimationCurve);
            this.gameObject.SetActive(false);

            if(Localization.activeLanguage == Localization.LanguageId.Arabic)
                CircleText.DisplaceByTextHeight(titleText);
        }

        public void Open()
        {

            //Minigame.mode = Minigame.Mode.Arcade;
            //this.isCompletedMinigameDifficultyEvent = true;

            this.isOpen = true;
            this.spinning = false;
            this.lockButtons = false;
            this.typeMinigame = this.game.GetMinigameType();
            this.gameObject.SetActive(true);
            this.trophyStars = new List<TrophyStar>();

            if(SaveData.IsInfiniteJumpPurchased() == true)
                PrizeDeck[3] = Prize.InfiniteJump;

            if (SaveData.IsSwordAndShieldPurchased() == true)
                PrizeDeck[2] = Prize.SwordAndShield;

            if (SaveData.IsMidasTouchPurchased() == true)
                PrizeDeck[1] = Prize.MidasTouch;

            StartCoroutine(OpenAnimation());
        }


        private IEnumerator OpenAnimation()
        {

            Tweener tweener;

            // setup
            this.imageTrophy.sprite = this.game.assets.spriteArrayLookup["trophies"].list[(int)Minigame.difficulty];
            this.imageMedal.sprite = this.game.assets.spriteArrayLookup["GLOBAL:medals"].list[(int)Minigame.difficulty];
            if(SaveData.HasCompletedMinigameDifficulty(this.typeMinigame, Minigame.difficulty))
                this.imageMedal.gameObject.SetActive(false);
            else
                this.imageTrophy.gameObject.SetActive(false);

            this.transformArcade.gameObject.SetActive(false);


            RandomUtil.Shuffle(PrizeDeck);
            for(int i = 0; i < 8; i++)
                this.imagePrizeItems[i].sprite = this.spritePrizeIcons[(int)PrizeDeck[i]];

            this.titleText.transform.rotation = Quaternion.Euler(0, 0, 175);

            foreach(var t in transformPrizeBubbles)
            {
                t.transform.localScale = HideScale;
                t.transform.localRotation = Quaternion.identity;
            }

            this.transformBorder.localScale = Vector3.one * 1.25f;
            this.transformBorder.gameObject.SetActive(false);
            this.transformTrophyShine.localScale = HideScale;
            this.transformAwardHolder.localScale = HideScale;
            this.transformSpinner.localScale = HideScale;
            this.transformSpinner.localRotation = Quaternion.identity;
            this.transformMiddle.localScale = HideScale;
            this.transformPointer.localScale = HideScale;
            this.transformPointerPin.localScale = HideScale;
            this.transformMiddle.localScale = HideScale;
            this.transformStarRing.localScale = HideScale;
            this.buttonExit.localScale = HideScale;
            this.buttonContinue.localScale = HideScale;
            this.buttonShop.localScale = HideScale;
            this.textReward.rectTransform.localScale = HideScale;
            this.imageStopPrizeFlash.color = new Color(1, 1, 1, 0);
            this.imageStopSpinnerFlash.color = new Color(1, 1, 1, 0);
            this.transformAwardHolder.gameObject.SetActive(false);
            this.circleImageTransform.gameObject.SetActive(false);
            this.imageStopSpinnerShakeBorder.enabled = false;

            StartCoroutine(Audio.instance.FadeMusic(setTo: 0, duration: .5f, audioClip: game.music));

            // pause to see win
            yield return new WaitForSeconds(0.5f);
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_in"], options: new Audio.Options(1, false, 0));
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:Fanfare_Fruit"], options: new Audio.Options(1, false, 0));

            // circle / title
            this.circleImageTransform.gameObject.SetActive(true);
            tweener = new Tweener();
            var tweenForCircle = tweener.TweenFloat(1, 0.1f, 1, easeCircleIn);
            tweener.RotateLocal(this.titleText.transform, 0, 0.4f, Easing.QuadEaseOut, 0.5f);
            while(!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleOpenAmount = tweenForCircle.Value();
                UpdateCircleSize();
                yield return null;
            }

            yield return null;
            yield return null;

            // jump to circle out when testing
            if(Minigame.IsLevelTesting())
                StartCoroutine(CircleOutToGame());
            else
                StartCoroutine(TrophyAnimation());
        }

        private IEnumerator TrophyAnimation()
        {
            // trophy
            var tweener = new Tweener();
            tweener.ScaleLocal(this.transformTrophyShine, Vector3.one, 0.5f, Easing.QuadEaseIn);
            tweener.ScaleLocal(this.transformAwardHolder, Vector3.one, 0.5f, Easing.QuadEaseIn);
            this.transformAwardHolder.gameObject.SetActive(true);

            var sfxString = Minigame.difficulty switch
            {
                MinigameDifficulty.Beginner => "GLOBAL:sfx_win_trophy_bronze",
                MinigameDifficulty.Intermediate => "GLOBAL:sfx_win_trophy_silver",
                MinigameDifficulty.Advanced => "GLOBAL:sfx_win_trophy_gold",
                _ => "GLOBAL:sfx_win_fruit",
            };
            Audio.instance.PlaySfx(game.assets.audioClipLookup[sfxString]);

            while(!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }

            // pause to see trophy
            yield return new WaitForSeconds(3f);

            if(Minigame.mode == Minigame.Mode.BonusLift)
                StartCoroutine(SpinnerAnimation());
            else if(Minigame.mode == Minigame.Mode.Arcade)
                StartCoroutine(ArcadeAnimation());
        }

        private IEnumerator ArcadeAnimation()
        {
            this.transformArcade.gameObject.SetActive(true);
            var tweener = new Tweener();
            tweener.ScaleLocal(this.transformTrophyShine, HideScale, 0.5f, Easing.QuadEaseOut);
            tweener.ScaleLocal(this.transformAwardHolder, HideScale, 0.5f, Easing.QuadEaseOut);
            tweener.ScaleLocal(this.transformTrophyStarHolder, HideScale, 0.5f, Easing.QuadEaseOut);

            var altButtons = this.isCompletedMinigameDifficultyEvent == true &&
                    SaveData.HasCompletedAllMinigameDifficulties(this.typeMinigame) == false;

            Transform[] buttons = null;
            if(altButtons == true)
                buttons = new[] { buttonExit, buttonShopBig, buttonContinueSmall };
            else 
                buttons = new[] { buttonExit, buttonContinue, buttonShop };

            buttonContinue.gameObject.SetActive(altButtons == false);
            buttonContinueSmall.gameObject.SetActive(altButtons == true);
            buttonShop.gameObject.SetActive(altButtons == false);
            buttonShopBig.gameObject.SetActive(altButtons == true);

            for(int n = 0; n < buttons.Length; n += 1)
            {
                var delay = 0.8f + (0.05f * n);
                tweener.ScaleLocal(buttons[n], Vector3.one, 0.5f, easeButtons, delay);
            }
            if(this.isCompletedMinigameDifficultyEvent == true)
            {
                tweener.ScaleLocal(this.textReward.rectTransform, Vector3.one, 0.5f, easeButtons, 0.5f);
                Map.detailsToPersist.coins += RetryMenu.CoinCost;
            }

            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_pausebubbles_in"], options: new Audio.Options(1f, false, 0f));

            while(!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }

            this.selectionCursor.gameObject.SetActive(true);
            this.selectionCursor.Select(
                    altButtons == true
                    ?
                    this.buttonShopBig.GetComponent<Selectable>() :
                    this.buttonContinue.GetComponent<Selectable>(),
                false
            );
        }

        private IEnumerator SpinnerAnimation()
        {
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_in"], options: new Audio.Options(1, false, 0));
            
            var tweener = new Tweener();
            this.transformBorder.gameObject.SetActive(true);
            tweener.ScaleLocal(this.transformBorder, Vector3.one, 1, easeCircleIn);
            tweener.ScaleLocal(this.transformSpinner, Vector3.one, 1, easeCircleIn);
            tweener.ScaleLocal(this.transformMiddle, Vector3.one, 1, easeCircleIn);
            tweener.ScaleLocal(this.transformPointer, Vector3.one, 1, easeCircleIn);
            tweener.ScaleLocal(this.transformPointerPin, Vector3.one, 1, easeCircleIn);

            for(int n = 0; n < transformPrizeBubbles.Length; n += 1)
            {
                var delay = 0.8f + (0.05f * n);
                tweener.ScaleLocal(transformPrizeBubbles[n].transform, Vector3.one, 0.5f, easeButtons, delay);
            }

            float currentTime = 0;
            bool hasBubblesPlayed = false;

            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);

                currentTime += Time.unscaledDeltaTime;
                if(currentTime > 0.9f && !hasBubblesPlayed) // 0.9 seconds is a rough guess
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_bubble_in"]);
                    hasBubblesPlayed = true;
                }

                yield return null;
            }

            yield return null;
            yield return null;

            this.transformAwardHolder.gameObject.SetActive(false);
            this.transformTrophyShine.gameObject.SetActive(false);
            this.imageStopSpinnerShakeBorder.enabled = true;
            this.spinning = true;
            this.spinSpeed = 0f;
        }

        private void AdvanceSpin()
        {
            if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]))
            {
                Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]);
                StartCoroutine(
                    Audio.instance.ChangeSfxPitch((
                        game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]),
                        startFrom: 0.5f,
                        setTo: 1,
                        duration: 1.5f
                ));
                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_SpeedUpPortion_Trimmed"], options: new Audio.Options(1, false, 0));
            }
            if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_SpeedUpPortion_Trimmed"]) &&
                 !Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_WheelLoopPortion"]))
            {
                Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_WheelLoopPortion"]);
            }
            var angle = this.transformSpinner.localEulerAngles.z;
            if(this.spinSpeed < SpinSpeedMax)
            {
                this.spinSpeed += SpinSpeedAcc;
                if(this.spinSpeed >= SpinSpeedMax)
                {
                    this.inputPrompt.Show();
                    this.spinSpeed = SpinSpeedMax;
                }
            }
            angle -= this.spinSpeed;
            this.transformSpinner.localRotation = Quaternion.Euler(0, 0, angle);
            // counter rotate
            this.transformSpinnerShine.localRotation = Quaternion.Euler(0, 0, -angle);
            foreach(var t in this.transformPrizeBubbles)
                t.localRotation = Quaternion.Euler(0, 0, -angle);
            // pointer wiggle
            var delta = (angle / 45) % 1f;
            this.easePointer = Easing.ByAnimationCurve(this.pointerAnimationCurve);
            this.transformPointer.localRotation = Quaternion.Euler(0, 0, (float)easePointer(delta, 0, -30f, 1f));
            if (transformPointer.localRotation.z < -0.01 && !this.hasTicked)
            {
                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_tick"], options: new Audio.Options(0.6f, false, 0));
                this.hasTicked = true;
            }
            else if (transformPointer.localRotation.z > -0.01)
            {
                this.hasTicked = false;
            }
            // prize is at 180 degrees, reading from +30 degrees to account for pointer wiggle
            float prizeAngle = (angle + 210f) % 360f;
            int prizeIndex = Mathf.FloorToInt(prizeAngle * (8f / 360f));
            float prizeActual = (angle + 180f) % 360;
            float degreesTillStop = 720 - (prizeIndex * 45f - prizeActual);

            // debugging prize selection
            //foreach(var i in imagePrizeItems)
            //    i.color = Color.white;
            //this.imagePrizeItems[prizeIndex].color = Color.red;
            if((this.spinSpeed == SpinSpeedMax) && (
                GameInput.pressedConfirm == true ||
                Input.GetMouseButtonDown(0) == true
            ))
            {
                this.spinning = false;
                SetPrize(prizeIndex);
                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_WheelLoopPortion"]);
                Audio.instance.StopSfx(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_SpeedUpPortion_Trimmed"]);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_stop"], options: new Audio.Options(1, false, 0));
                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:Wheel Of Prizes_Stop_MusicCue"], options: new Audio.Options(1, false, 0));
                StartCoroutine(StopSpinnerAnimation(prizeIndex, -degreesTillStop));
            }
        }


        private IEnumerator StopSpinnerAnimation(int prizeIndex, float degreesTillStop)
        {
            // spin to stop
            var tweener = new Tweener();
            var delaySpin = (Mathf.Abs(degreesTillStop) / (SpinSpeedMax * 0.5f)) * Time.unscaledDeltaTime;
            tweener.RotateLocal(this.transformSpinner, this.transformSpinner.localEulerAngles.z + degreesTillStop, delaySpin, easeSpin);
            // counter rotate
            tweener.RotateLocal(this.transformSpinnerShine, this.transformSpinnerShine.localEulerAngles.z - degreesTillStop, delaySpin, easeSpin);
            foreach(var t in this.transformPrizeBubbles)
                tweener.RotateLocal(t, t.localEulerAngles.z - degreesTillStop, delaySpin, easeSpin);
            this.imageStopSpinnerFlash.enabled = true;
            var tweenForStopFlash = tweener.TweenFloat(0, 1f, 0.125f, easeStopFlash);
            var screenShake = new Shake(Vector2.up, 0.25f, 1f, 10);
            var positionShakeDefault = this.transformShaker.anchoredPosition;

            StartCoroutine(
                Audio.instance.ChangeSfxPitch((
                    game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]),
                    startFrom: 1,
                    setTo: 0.7f,
                    duration: 2
            ));

            StartCoroutine(
                Audio.instance.ChangeSfxVolume((
                    game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]),
                    startFrom: 1,
                    setTo: .5f,
                    duration: 3
            ));

            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                screenShake.Advance(Time.unscaledDeltaTime);
                this.transformShaker.anchoredPosition = positionShakeDefault + screenShake.Position() ;
                this.imageStopSpinnerFlash.color = new Color(1, 1, 1, tweenForStopFlash.Value());
                // pointer wiggle
                var delta = ((this.transformSpinner.localEulerAngles.z) / 45) % 1f;
                this.transformPointer.localRotation = Quaternion.Euler(0, 0, (float)easePointer(delta, 0, -30f, 1f));

                if (transformPointer.localRotation.z > -0.01 && !this.hasTicked)
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_tick"], options: new Audio.Options(0.6f, false, 0));
                    this.hasTicked = true;
                }
                else if (transformPointer.localRotation.z < -0.01)
                {
                    this.hasTicked = false;
                }

                yield return null;
            }

            Audio.instance.StopSfxLoop(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_spin"]);

            yield return null;
            yield return null;

            this.imageStopSpinnerShakeBorder.enabled = false;
            tweener = new Tweener();
            this.imageStopPrizeFlash.enabled = true;
            tweenForStopFlash = tweener.TweenFloat(0, 1f, 0.1f, easeStopFlash);

            while(!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.imageStopPrizeFlash.color = new Color(1, 1, 1, tweenForStopFlash.Value());
                yield return null;
            }
            this.imageStopPrizeFlash.enabled = false;
            yield return null;
            yield return null;

            // animate bubbles disappearing

            // TODO: I'd like Tweener to manage this nonsense
            {
                var i = (prizeIndex + 1) % 8;

                void playBubble()
                {
                    this.animPrizeBubbles[i].PlayOnce(this.animBubblePop);
                    this.imagePrizeItems[i].enabled = false;
                    i = (i + 1) % 8;
                }
                playBubble();
                var newAnimDelay = 0.03f;
                var delay = 0.8f;

                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_bubble_out"]);

                while (delay > 0)
                {
                    newAnimDelay -= Time.unscaledDeltaTime;
                    if(newAnimDelay <= 0 && i != prizeIndex)
                    {
                        newAnimDelay = 0.1f;
                        playBubble();
                    }
                    delay -= Time.unscaledDeltaTime;

                    // Minigame is advancing Animated in its Update()
                    //Animated.AdvanceAll();
                    yield return null;
                }
            }
            yield return null;
            yield return null;

            // spinning stars on selection

            tweener = new Tweener();
            tweener.ScaleLocal(this.transformStarRing, Vector3.one, 1, easeCircleIn);

            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_win"], options: new Audio.Options(1, false, 0));
            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:WinningGameClosure_Jingle"], options: new Audio.Options(1, false, 0));

            while (!tweener.IsDone())
            {
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }

            yield return new WaitForSeconds(0.5f);


            // spinner parts out

            tweener = new Tweener();
            var time = 1.25f;

            // attach prize to outgoing anim
            this.transformPrizeBubbles[prizeIndex].SetParent(transformPrizeHolder, true);

            tweener.ScaleLocal(this.transformBorder, Vector3.one * 1.25f, time, easeCircleOut);
            tweener.ScaleLocal(this.transformSpinner, HideScale, time, easeCircleOut);
            tweener.ScaleLocal(this.transformMiddle, HideScale, time, easeCircleOut);
            tweener.ScaleLocal(this.transformPointer, HideScale, time, easeCircleOut);
            tweener.ScaleLocal(this.transformPointerPin, HideScale, time, easeCircleOut);
            tweener.ScaleLocal(this.transformPrizeBubbles[prizeIndex], HideScale, time, easeCircleOut);
            var ringScaleOut = tweener.TweenFloat(1f, 3f, 0.5f);

            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_prize_roulette_out"], options: new Audio.Options(1, false, 0));
            while (!tweener.IsDone())
            {
                var s = ringScaleOut.Value();
                if(s < 2f)
                    this.transformStarRing.transform.localScale = new Vector3(s, s, 1);
                else
                    this.transformStarRing.gameObject.SetActive(false);
                tweener.Advance(Time.unscaledDeltaTime);
                yield return null;
            }
            this.transformBorder.gameObject.SetActive(false);

            StartCoroutine(CircleOutToGame());
        }

        private IEnumerator CircleOut()
        {
            var tweener = new Tweener();
            tweener.RotateLocal(this.titleText.transform, 179, 0.4f, Easing.QuadEaseIn, 0.3f);
            var tweenForCircle = tweener.TweenFloat(0, 1, 0.5f, Easing.QuadEaseIn, 0.5f);

            if(Minigame.mode == Minigame.Mode.Arcade)
            {
                Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_ui_pausebubbles_out"], options: new Audio.Options(1f, false, 0f));

                var altButtons = this.isCompletedMinigameDifficultyEvent == true &&
                   SaveData.HasCompletedAllMinigameDifficulties(this.typeMinigame) == false;
                Transform[] buttons = null;
                if(altButtons == true)
                    buttons = new[] { buttonExit, buttonShopBig, buttonContinueSmall };
                else
                    buttons = new[] { buttonExit, buttonContinue, buttonShop };
                for(int n = 0; n < buttons.Length; n += 1)
                {
                    var delay = 0.1f + (0.05f * n);
                    tweener.ScaleLocal(buttons[n], HideScale, 0.5f, Easing.QuadEaseIn, delay);
                }
                if(this.isCompletedMinigameDifficultyEvent == true)
                    tweener.ScaleLocal(this.textReward.rectTransform, HideScale, 0.5f, Easing.QuadEaseIn);
            }

            Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_win_out"], options: new Audio.Options(1, false, 0));
            while(tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleClosedAmount = tweenForCircle.Value();
                UpdateCircleSize();
                yield return null;
            }
            yield return null;
            yield return null;
        }

        private IEnumerator CircleOutToGame()
        {
            yield return CircleOut();

            Audio.instance.PlayMusic(game.musicTransition);

            this.game.ExitGame();
            this.game = null;

            if(Minigame.IsLevelTesting())
            {
                var canvas = GetComponentInParent<Canvas>().gameObject;
                DontDestroyOnLoad(canvas);
                for(int n = 0; n < canvas.transform.childCount; n += 1)
                {
                    var child = canvas.transform.GetChild(n).gameObject;
                    if(child != this.gameObject)
                        Destroy(child);
                }
                Destroy(canvas.GetComponent<MinigameUI>());
                Resources.UnloadUnusedAssets();
                System.GC.Collect();

                SceneManager.LoadScene(SceneManager.GetActiveScene().name);

                this.circleOpenAmount = 1;

                var tweener = new Tweener();
                var tweenForCircle = tweener.TweenFloat(1, 0, 0.5f, Easing.QuadEaseIn);
                while(tweener.IsDone() == false)
                {
                    tweener.Advance(Time.unscaledDeltaTime);
                    this.circleClosedAmount = tweenForCircle.Value();
                    UpdateCircleSize();
                    yield return null;
                }
                Destroy(canvas);
            }
            else
            {
                Minigame.testingLevel = null;
                // use theme transition to return to game
                var targetCircleCol = ThemesAsset.CircleColor(LevelGeneration.ThemeForDate(Game.selectedDate));
                var timer = 0f;
                while(timer < 0.5f)
                {
                    timer = Mathf.Min(timer + Time.unscaledDeltaTime, 0.5f);
                    this.circleImageTransform.GetComponent<Image>().color = Color.Lerp(Color.white, targetCircleCol, timer * 2f);
                    yield return null;
                }
                Transition.GoToGame();
            }
        }

        private IEnumerator CircleOutToScene(string sceneName)
        {
            yield return CircleOut();

            this.game.ExitGame();
            this.game = null;

            Minigame.testingLevel = null;
            // use theme transition to return to game
            var targetCircleCol = Color.black;
            var timer = 0f;
            while(timer < 0.5f)
            {
                timer = Mathf.Min(timer + Time.unscaledDeltaTime, 0.5f);
                this.circleImageTransform.GetComponent<Image>().color = Color.Lerp(Color.white, targetCircleCol, timer * 2f);
                yield return null;
            }
            Transition.GoSimple(sceneName);
        }

        private IEnumerator CircleOutToNextLevel()
        {
            StartCoroutine(Audio.instance.FadeMusic(setTo: 1, duration: 2f, audioClip: game.music));

            yield return CircleOut();

            this.game.ExitGame();
            this.game = null;

            // select next unlocked level
            Levels.GetNextLevelNotCompleted(this.typeMinigame, ref Minigame.difficulty, ref Minigame.level);

            var canvas = GetComponentInParent<Canvas>().gameObject;
            DontDestroyOnLoad(canvas);
            for(int n = 0; n < canvas.transform.childCount; n += 1)
            {
                var child = canvas.transform.GetChild(n).gameObject;
                if(child != this.gameObject)
                    Destroy(child);
            }
            Destroy(canvas.GetComponent<MinigameUI>());
            Resources.UnloadUnusedAssets();
            System.GC.Collect();

            SceneManager.LoadScene(SceneManager.GetActiveScene().name);

            this.circleOpenAmount = 1;

            var tweener = new Tweener();
            var tweenForCircle = tweener.TweenFloat(1, 0, 0.5f, Easing.QuadEaseIn);
            while(tweener.IsDone() == false)
            {
                tweener.Advance(Time.unscaledDeltaTime);
                this.circleClosedAmount = tweenForCircle.Value();
                UpdateCircleSize();
                yield return null;
            }
            Destroy(canvas);
        }

        public void OnPressExit()
        {
            if(this.lockButtons == true)
                return;
            this.lockButtons = true;
            StartCoroutine(CircleOutToScene("Arcade"));
        }

        public void OnPressContinue()
        {
            if(this.lockButtons == true)
                return;
            this.lockButtons = true;
            StartCoroutine(CircleOutToNextLevel());
        }

        public void OnPressShop()
        {
            if(this.lockButtons == true)
                return;
            this.lockButtons = true;
            if(
                this.isCompletedMinigameDifficultyEvent == true &&
                SaveData.HasCompletedAllMinigameDifficulties(this.typeMinigame) == false
            ){
                Minigame.difficulty++;
                ShopScreen.shopItemShowOnStart = SaveData.ShopItemForMinigameDifficulty(this.typeMinigame, Minigame.difficulty);
            }
            StartCoroutine(CircleOutToScene("Shop"));
        }

        private void Update()
        {
            // things that constantly rotate
            foreach(var t in this.transformSpins)
            {
                float a = t.localEulerAngles.z;
                t.localRotation = Quaternion.Euler(0, 0, a + 3f);
            }

            // animate trophy star field
            if(this.transformAwardHolder.gameObject.activeInHierarchy == true)
            {
                if(this.trophyStars.Count < 20)
                {
                    var obj = Instantiate(this.prefabTrophyStar, Vector3.zero, Quaternion.identity, this.transformTrophyStarHolder);
                    var star = new TrophyStar{ transform = obj.transform };
                    star.Init();
                    this.trophyStars.Add(star);
                }
                foreach(var star in this.trophyStars)
                    star.Advance();

            }
            else if(this.trophyStars.Count > 0)
            {
                foreach(var star in this.trophyStars)
                    Destroy(star.transform.gameObject);
                this.trophyStars.Clear();
            }

            if(this.spinning == true)
                AdvanceSpin();
        }

        private void UpdateCircleSize()
        {
            float width, height;

            if(GameCamera.IsLandscape() == true)
                (width, height) = (700, 400);
            else
                (width, height) = (256, 256 * Screen.height / Screen.width);

            float diagonal = Mathf.Sqrt(width * width + height * height);
            this.circleImageTransform.sizeDelta = new Vector2(diagonal, diagonal);
            this.imageStopSpinnerFlash.rectTransform.sizeDelta = new Vector2(diagonal, diagonal);
            this.imageStopSpinnerShakeBorder.rectTransform.sizeDelta = new Vector2(diagonal, diagonal * 1.5f);

            float circleStandardSize;
            if(GameCamera.IsLandscape())
                circleStandardSize = 270;
            else
                circleStandardSize = 200;

            float circleRadiusForMaterial = Mathf.LerpUnclamped(
                circleStandardSize / diagonal, 1.0f, this.circleOpenAmount
            ) * (1.0f - this.circleClosedAmount);
            float outlineThickness = circleStandardSize * 0.03f / diagonal;

            this.circleMaterial.SetFloat(Radius, circleRadiusForMaterial);
            this.circleMaterial.SetFloat(OutlineThickness, outlineThickness);

            float scale = circleStandardSize / 220.0f;

            scale += 0.1f;// managed to break the whole animation when getting to work on TV
            this.transformUiBehindCircle.localScale = new Vector3(scale, scale, 1);
            this.transformUiInFrontOfCircle.localScale = new Vector3(scale, scale, 1);
            this.transformArcade.localScale = new Vector3(scale, scale, 1);
        }

        private void SetPrize(int index)
        {
            var prize = PrizeDeck[index];
            PrizeToPowerup.TryGetValue(prize, out PowerupPickup.Type powerup);

            if(powerup == PowerupPickup.Type.None)
            {
                switch(prize)
                {
                    case Prize.Coin:
                        Map.detailsToPersist.prizeCoins += 1;
                        break;
                }
            }
            else
                Map.detailsToPersist.minigamePrizePowerup = powerup;
        }

        private void LateUpdate()
        {
#if UNITY_EDITOR
            UpdateCircleSize();
#endif
            //this.titleText.GetComponent<CircleText>()?.UpdateCurve();
        }

        private class TrophyStar
        {
            public Transform transform;
            public Vector2 speed;
            public Vector3 scale;
            public float rotation;

            public const float DistMax = 180f;

            public void Init()
            {
                this.transform.localPosition = Vector3.zero;
                this.rotation = UnityEngine.Random.Range(1, 4);
                this.speed = RandomUtil.Normal() * UnityEngine.Random.Range(2, 4);
                this.scale = this.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.5f, 1.5f);
            }

            public void Advance()
            {
                var a = this.transform.localRotation.eulerAngles.z;
                this.transform.localRotation = Quaternion.Euler(0, 0, a + this.rotation);
                this.transform.localPosition += (Vector3)this.speed;
                var dist = this.transform.localPosition.magnitude;
                this.transform.localScale = this.scale + (dist / DistMax) * Vector3.one;
                if(dist > DistMax) Init();
            }
        }
    }
}
