using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using System;

namespace minigame
{
    namespace abseiling
    {
        public partial class AbseilingGame : Minigame
        {
            [Header("Abseiling Game")]
            public GameObject livesLeftPortrait;
            public GameObject livesLeftLandscape;
            public TMP_Text finishText;
            public TMP_Text textState;
            public Image[] imageLives;
            public TriggerMap triggerMap;
            public int livesLeft;
            public int score;
            public bool completed;
            [Header("Background")]
            public Transform transformBackground;
            public SpriteRenderer[] spriteRenderersBackgroundFog;
            public Transform transformScaffold;

            [NonSerialized] public Vector3 camOffset;
            [NonSerialized] public Coroutine coroutineForCamera;
            [NonSerialized] public Player player;

            // Player checks this value when input is down
            public bool isOverDebugGoUpButton;
            public void SetOverDebugGoUpButton(bool value) => isOverDebugGoUpButton = value;
            public void ChangeCharacterButton() {
                this.character++;
                if((int)this.character >= ((Character[])Enum.GetValues(typeof(Character))).Length) {
                    this.character = 0;
                }
                this.player.DebugUpdateCharacter();
            }

            public const float PanDelay = 0.02f;// multplied by distance

            public Dictionary<Vector2Int, Vector2> windLookup = new Dictionary<Vector2Int, Vector2>();

            // used by triggerMap to distinguish from other objects
            public class StopHook { };
            public class Finish { };

            void Start()
            {
                Scale = 1f;
                ScaleOne = new Vector2(Scale, Scale);
                ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
                ScreenWidth = 16;
                if(GameCamera.IsLandscape())
                {
                    livesLeftPortrait.SetActive(false);
                    imageLives = livesLeftLandscape.transform.Find("container")
                        .gameObject.GetComponentsInChildren<Image>().ToArray();
                }
                else
                    livesLeftLandscape.SetActive(false);

                Init();// MiniGame setup

                ObjectLayers.SetSortingLayer(this.transformBackground.gameObject, this.objectLayers.backgroundSortingLayer);

                Restart(true);

                this.minigameInputTypePrompt.FirstShow();

#if UNITY_EDITOR

                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.levelRect.width * 0.5f, this.playerObj.transform.localPosition.y + 8f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);

                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, () => {
                    UpdateBackground();
                });
                UpdateBackground();
#endif
            }
            void OnApplicationQuit() => Save();

            public override bool IsFailed() =>
                this.livesLeft == 0 && this.player != null && this.player.state == Player.State.Death;

            public override bool IsComplete() => completed;

            public override void CompleteLevel()
            {
                this.completed = true;
                base.CompleteLevel();
            }

            public void InitUI()
            {
            }

            public override void Restart(bool hard)
            {
                base.Restart(hard);

                if(this.coroutineForCamera != null)
                {
                    StopCoroutine(this.coroutineForCamera);
                    this.coroutineForCamera = null;
                }
                this.livesLeft = 3;
                UpdateLivesLeft();
                this.windLookup.Clear();
                this.score = 0;
                this.camCtrl.clampAction = this.camCtrl.Clamp;
                this.triggerMap = new TriggerMap();

                this.fileLevel = GetLevelFile(difficulty, level);
                //this.fileLevel = DebugPanelLevelsDropdown.level ?? this.assets.levelTest;

                CreateLevel(this.fileLevel);

                InitUI();

                this.camOffset = Vector3.zero;
                this.camCtrl.SetPosition(this.playerObj.transform.position, Vector3.zero);
            }

            void Update()
            {
                this.input.Advance();

#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R) && Input.GetKey(KeyCode.LeftShift))
                    Restart(true);
                else if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
#endif

                if(Minigame.paused == false && this.camCtrl.lookAtTween == null)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }

                UpdateBackground();
                this.triggerMap.DebugDraw();
            }

            void FixedUpdate()
            {

                if(IsFailed() == true)
                {
                    return;
                }
                if(IsComplete() == true)
                {
                    return;
                }


                float timestep = Time.fixedDeltaTime / Time.timeScale;

                if(this.coroutineForCamera != null)
                    return;

                if(this.camCtrl.lookAtTween == null)
                {

                    this.player.Advance();

                    switch(this.player.state)
                    {
                        case Player.State.Death: break;
                        case Player.State.Run:
                        case Player.State.KickStick:
                        case Player.State.Drop:
                        case Player.State.Stop:
                        case Player.State.Win:
                            this.camCtrl.SetPosition(this.cam.transform.position + (GetCameraTargetDefault() - this.cam.transform.position) * 0.05f, this.camOffset);
                            break;
                        case Player.State.RunIn:
                        case Player.State.StopIn:
                            this.camCtrl.SetPosition(this.cam.transform.position + (this.player.GetPositionStart(this.player.currentCheckpoint) - this.cam.transform.position) * 0.05f, this.camOffset);
                            break;
                    }

                    UpdateMotors();
                }
                else
                {
                    this.camCtrl.UpdateLookAt(Time.fixedDeltaTime);
                }
                camCtrl.AdvanceShakes(timestep);

                this.input.FixedAdvance();

                frameCount++;
            }

            public override Vector3 GetCameraTargetDefault()
            {
                return this.player.transform.position + Vector3.down * 3f;
            }

            public void SpendLife()
            {
                this.livesLeft--;
                UpdateLivesLeft();
            }

            public void UpdateLivesLeft()
            {
                for(int i = 0; i < this.imageLives.Length; i++)
                    this.imageLives[i].enabled = i < this.livesLeft;
                if(this.livesLeft == 0) this.imageLives[0].transform.localScale = Vector3.one;
            }

            public void UpdateBackground()
            {
                var cornerOffset = new Vector3(this.map.width, this.camCtrl.rect.height) * 0.5f;
                var camCorner = this.cam.transform.position - cornerOffset;
                camCorner.z = 0;

                this.transformBackground.position = camCorner;
                this.transformScaffold.localPosition = -camCorner * 0.8f;


                for(int i = 0; i < this.spriteRenderersBackgroundFog.Length; i++)
                {
                    var sr = this.spriteRenderersBackgroundFog[i];
                    var p = sr.transform.localPosition;
                    
                    if(Minigame.paused == false && this.camCtrl.lookAtTween == null)
                        p.x -= 0.0125f + i * 0.005f;
                    if(p.x < -sr.size.x * 0.5f) p.x += sr.size.x * 0.5f;
                    
                    var h = sr.sprite.rect.size.y / 70;
                    p.y = -((camCorner.y * ((0.1f * i) + 0.05f)) % h);
                    sr.transform.localPosition = p;
                }
            }

        }
    }

}
