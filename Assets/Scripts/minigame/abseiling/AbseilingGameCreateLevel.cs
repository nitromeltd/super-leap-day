using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public partial class AbseilingGame : Minigame
        {

            public void CreateLevel(TextAsset file)
            {
                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                );
                this.map.AddNoAutoTiles(""
                    , "polysquare"
                    , "poly45br"
                    , "poly22br"
                    , "poly67br"
                );
                this.map.AddDontRotateTiles(
                    "SpikesRetractable"
                    , "SpikesRetractable_in"
                    , "SpikesRetractable_out"
                    , "rope_hook"
                    , "rope_hook_winch"
                );

                var level = this.map.ParseLevel(file);

                // generate all objects
                this.map.Create(level);

                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;

                MinigameMap.Record playerRecord = null;
                List<RopeHook> checkpointHooks = new List<RopeHook>();
                List<Checkpoint> checkpoints = new List<Checkpoint>();

                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        // spikes map
                        this.spikesLookup[record.p] = record.name switch
                        {
                            "GLOBAL:spike_top_1" => 1,
                            "GLOBAL:spike_top_2" => 1,
                            "GLOBAL:spike_right_1" => 2,
                            "GLOBAL:spike_right_2" => 2,
                            "GLOBAL:spike_bottom_1" => 3,
                            "GLOBAL:spike_bottom_2" => 3,
                            "GLOBAL:spike_left_1" => 4,
                            "GLOBAL:spike_left_2" => 4,
                            "GLOBAL:spike_vert_1" => 5,
                            "GLOBAL:spike_vert_2" => 5,
                            "GLOBAL:spike_horz_1" => 6,
                            "GLOBAL:spike_horz_2" => 6,
                            _ => 0
                        };
                        record.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
                    }

                    if(record.isPrefab == true)
                    {

                        switch(record.name)
                        {
                            case "Player":
                                {
                                    var checkpoint = Instantiate(
                                        this.assets.prefabLookup["Checkpoint"],
                                        record.pos + record.offset + Vector2.down * 0.5f,
                                        Quaternion.identity,
                                        this.map.transformObjects
                                    ).GetComponent<Checkpoint>();
                                    checkpoints.Add(checkpoint);
                                    checkpoint.Init(record.tile.flip ? -1 : 1, this);

                                    // demote previous player item to checkpoint
                                    if(playerRecord != null)
                                        Destroy(this.playerObj);
                                    
                                    playerRecord = record;
                                    this.playerObj = record.gameObject;
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);

                                    this.triggerMap.Add(new StopHook(), record.gameObject);
                                }
                                break;
                            case "Chest":
                                record.GetComponent<Chest>().game = this;
                                ObjectLayers.SetSortingLayer(record.gameObject.transform.Find("Chest Bonsai").gameObject, this.objectLayers.backgroundSortingLayer);
                                break;
                            case "Spring":
                                if(record.tile.flip || record.tile.rotation == 180)
                                    record.GetComponent<Spring>().dir = Vector2.left;
                                break;
                            case "SpikeFloat":
                                record.GetComponent<SpikedItem>().game = this;
                                break;
                            case "SpikeHoopBig":
                            case "SpikeHoop":
                                {
                                    var component = record.GetComponent<SpikeHoop>();
                                    component.start = component.transform.position;
                                    component.spriteRendererFront.sortingLayerName = this.objectLayers.foregroundSortingLayer;
                                    component.game = this;
                                    // bait for player
                                    this.pickupMap.CreatePickup(new NitromeEditor.TileLayer.Tile { tile = "GLOBAL:" + RandomUtil.Pick(MinigameMap.FruitRandom) }, record.p, record.pos + record.offset + new Vector2(0, 1f));
                                }
                                break;
                            case "Button":
                                {
                                    var component = record.GetComponent<TriggerButton>();
                                    component.game = this;
                                    this.triggerMap.Add(component, component.trigger, component.transform);
                                }
                                break;
                            case "SpikesRetractable":
                                record.GetComponent<SpikesRetractable>().Init(record.p, record.tile, this);
                                break;
                            case "SpikeFlap":
                                record.GetComponent<SpikeFlap>().Init(record.p, record.tile, this);
                                this.triggerMap.Add(record.GetComponent<SpikeFlap>(), (Vector2)record.gameObject.transform.position + (record.tile.flip ? Vector2.right : Vector2.left), Vector2.one);

                                break;
                            case "start_hook":
                                checkpointHooks.Add(record.GetComponent<RopeHook>());
                                record.GetComponent<RopeHook>().Init(this, record.tile);
                                break;
                            case "rope_hook_winch":
                            case "rope_hook":
                                this.triggerMap.Add<RopeHook>(record.gameObject);
                                record.GetComponent<RopeHook>().Init(this, record.tile);
                                break;
                            case "RopeWalker":
                                {
                                    var component = record.GetComponent<RopeWalker>();
                                    this.triggerMap.Add<RopeWalker>(record.gameObject);
                                    component.Init(this);
                                }
                                break;
                            case "SpikesForPlayer":
                                record.GetComponent<SpikesForPlayer>().Init(this);
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        switch(record.tile.tile)
                        {
                            case "wind":
                                {
                                    var dir = record.tile.rotation switch
                                    {
                                        90 => Vector2.up,
                                        180 => Vector2.left,
                                        270 => Vector2.down,
                                        _ => Vector2.right
                                    };
                                    if(record.tile.flip) dir *= -1;
                                    this.windLookup[record.p] = dir;
                                    // preview capture sees editor sprite before it's destroyed
                                    record.GetComponent<SpriteRenderer>().enabled = false;
                                    Destroy(record.gameObject);// remove level building sprite
                                }
                                break;
                            case "stop_hook":
                                this.triggerMap.Add(new StopHook(), record.gameObject);
                                ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                record.GetComponent<SpriteRenderer>().sortingOrder = -1;
                                break;
                            case "finish":
                                this.triggerMap.Add(new Finish(), record.pos + record.offset + Vector2.up, Vector2.one);
                                // preview capture sees editor sprite before it's destroyed
                                record.GetComponent<SpriteRenderer>().enabled = false;
                                Destroy(record.gameObject);
                                break;
                        }
                        if(record.GetComponent<SpriteRenderer>().sortingLayerName == this.objectLayers.foregroundSortingLayer)
                            record.GetComponent<SpriteRenderer>().sortingLayerName = this.objectLayers.backgroundSortingLayer;
                    }
                }

                // add finish tiles
                for(int y = 0; y < this.map.height; y++)
                {
                    for(int x = 0; x < this.map.height; x++)
                    {
                        var v = new Vector2(x, y) + Vector2.one * 0.5f;
                        if(this.triggerMap.Get<Finish>(v) != null)
                        {
                            var layer = this.objectLayers.playerSortingLayer;
                            var sr = this.spriteRendererPool.Get(v + Vector2.down, this.assets.spriteLookup["end_tile_left"], layer, "finish", this.transform);
                            sr.sortingOrder = -1;
                            while(this.triggerMap.Get<Finish>(v + Vector2.right * 2) != null)
                            {
                                v.x += 1f;
                                sr = this.spriteRendererPool.Get(v + Vector2.down, this.assets.spriteLookup["end_tile_middle"], layer, "finish", this.transform);
                                sr.sortingOrder = -1;
                            }
                            v.x += 1f;
                            sr = this.spriteRendererPool.Get(v + Vector2.down, this.assets.spriteLookup["end_tile_right"], layer, "finish", this.transform);
                            sr.sortingOrder = -1;
                            break;
                        }
                    }
                }

                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "Button":
                            {
                                var tb = conn.objB.GetComponent<TriggerButton>();
                                tb.game = this;
                                tb.targets.Add(conn.a);
                                triggerButtons.Add(tb);

                                if(conn.nameA == "rope_hook_winch")
                                {
                                    conn.objA.GetComponent<RopeHook>().isWinch = false;
                                    conn.objA.GetComponent<RopeHook>().button = tb;
                                }
                            }
                            break;
                        case "Portal":
                            var portalA = conn.objA.GetComponent<Portal>();
                            var portalB = conn.objB.GetComponent<Portal>();
                            portalA.Init(this, portalB);
                            portalB.game = this;
                            ObjectLayers.SetSortingLayer(conn.objA, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount++);
                            ObjectLayers.SetSortingLayer(conn.objB, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount++);
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                    }
                }
                // remove paths from SpikesForPlayer
                foreach(var m in this.motors)
                {
                    if(m.transform.name == "SpikesForPlayer")
                        foreach(var d in m.dots)
                        {
                            d.enabled = false;
                            Destroy(d.gameObject);
                        }
                }

                // Encase level
                {
                    var wallOutside = Instantiate(this.assets.prefabLookup["WallOutside"], new Vector3(-1, 0), Quaternion.identity, this.map.transformTiles);
                    (var size, var offset) = (new Vector2(1, this.map.height), new Vector2(0.5f, this.map.height * 0.5f));
                    wallOutside.GetComponent<BoxCollider2D>().size = size;
                    wallOutside.GetComponent<BoxCollider2D>().offset = offset;
                    wallOutside = Instantiate(this.assets.prefabLookup["WallOutside"], new Vector3(this.map.width, 0), Quaternion.identity, this.map.transformTiles);
                    wallOutside.GetComponent<BoxCollider2D>().size = size;
                    wallOutside.GetComponent<BoxCollider2D>().offset = offset;
                }

                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);

                // DEBUGGING
                //Minigame.testingLevel ??= this.map.levelOriginal;

                // add a start hook if none present
                if(checkpointHooks.Count < checkpoints.Count)
                    checkpointHooks.Add(Instantiate(
                        this.assets.prefabLookup["start_hook"],
                        new Vector3(this.map.rect.x + this.map.rect.width * 0.5f, this.map.rect.yMax + 0.5f),
                        Quaternion.identity,
                        this.map.transformObjects
                    ).GetComponent<RopeHook>());

                for(int i = 0; i < checkpoints.Count; i++)
                    checkpoints[i].hook = checkpointHooks[i];
                checkpoints[^1].active = true;

                // handholds pattern
                ScatterPatternOnTiles(this.map.rectInt, this.assets.spriteArrayLookup["handholds"].list);

                // scaffold
                {
                    foreach(Transform t in this.transformScaffold)
                        Destroy(t.gameObject);
                    var p = Vector2.zero;

                    while(p.y < this.map.height * 0.8f)
                    {
                        p.y += UnityEngine.Random.Range(10f, 18f);
                        var sr = this.spriteRendererPool.Get(p, RandomUtil.Pick(this.assets.spriteArrayLookup["scaffold"].list), this.objectLayers.backgroundSortingLayer, "scaffold", this.transformScaffold);
                        sr.sortingOrder = -1;
                    }
                }

                // create side fill in landscape mode
                if(GameCamera.IsLandscape() == true)
                {
                    SpriteRenderer sr;
                    sr = this.spriteRendererPool.Get(new Vector3(0.25f, 0), this.assets.spriteLookup["Apple TV Side 1"], this.objectLayers.foregroundSortingLayer, "fill left", this.map.transformBackground);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, this.map.height);
                    sr = this.spriteRendererPool.Get(new Vector3(this.map.width - 0.25f, 0), this.assets.spriteLookup["Apple TV Side 2"], this.objectLayers.foregroundSortingLayer, "fill right", this.map.transformBackground);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, this.map.height);
                }

                // wind animations
                for(int y = 0; y < this.map.height; y++)
                {
                    for(int x = 0; x < this.map.width; x++)
                    {
                        if(this.windLookup.TryGetValue(new Vector2Int(x, y), out Vector2 dir) == true)
                        {
                            // offset X position for wind that doesn't span level
                            var objX = 0f;
                            if(Physics2D.OverlapPoint(new Vector2(x - 0.5f, y + 0.5f)) == null)
                                objX = x;
                            else
                            {
                                var testX = x + 1;
                                while(testX < this.map.width)
                                {
                                    if(
                                        this.windLookup.ContainsKey(new Vector2Int(testX, y)) == false &&
                                        Physics2D.OverlapPoint(new Vector2(testX + 0.5f, y + 0.5f)) == null
                                    )
                                    {
                                        objX = -this.map.width + testX;
                                        break;
                                    }
                                    testX++;
                                }
                            }

                            var obj = Instantiate(this.assets.prefabLookup["Wind"], new Vector3(objX, y), Quaternion.identity, this.map.transformObjects);
                            if(dir.x < 0)
                            {
                                obj.GetComponent<SpriteRenderer>().flipX = true;
                                obj.transform.position += new Vector3(this.map.width, 0);
                            }
                            var playAnim = obj.GetComponent<PlayAnimated>();
                            playAnim.animated.PlayAndLoop(playAnim.anim);
                            playAnim.animated.frameNumber = Mathf.FloorToInt(UnityEngine.Random.value * playAnim.anim.sprites.Length);
                            ObjectLayers.SetSortingLayer(obj, this.objectLayers.backgroundSortingLayer);
                            break;
                        }

                    }

                }

                // init player
                this.player = playerRecord.GetComponent<Player>();
                this.player.Init(this, checkpoints);

            }

            public const float SpikeLength = 0.9f;
            public static Vector2 SpikeIndexToDir(int i) => i switch
            {
                1 => Vector2.up * SpikeLength,
                2 => Vector2.right * SpikeLength,
                3 => Vector2.down * SpikeLength,
                4 => Vector2.left * SpikeLength,
                5 => Vector2.up,
                6 => Vector2.right,
                _ => Vector2.zero
            };

            public static int SpikeRotationToDir(int r) => r switch
            {
                0 => 1,
                90 => 4,
                180 => 3,
                270 => 2,
                _ => 1
            };

            public void ScatterPatternOnTiles(RectInt rect, Sprite[] sprites)
            {
                var reserved = new Dictionary<Vector2Int, bool>();
                var tileNeighbours = new Dictionary<Vector2Int, int>();
                var countdown = UnityEngine.Random.Range(1, 4);
                var sortOffset = rect.width * rect.height;

                var dontDecorate = new Dictionary<string, bool>
                {
                    ["button_trigger_platform"] = true,
                    ["button_trigger_platform_thin"] = true
                };

                Minigame.ForeachInRectInt(rect, (p) =>
                {
                    reserved[p] = false;

                    var v = p + Vector2.one * 0.5f;
                    var collider = Physics2D.OverlapPoint(v);
                    if
                    (
                        collider != null &&
                        collider.isTrigger == false &&
                        dontDecorate.ContainsKey(collider.name) == false
                    )
                        tileNeighbours[p] = 0;
                });
                // count neighbours of tiles so we can keep pattern away from edges
                var dirs = new[] { Vector2Int.up, Vector2Int.right, Vector2Int.down, Vector2Int.left };
                Minigame.ForeachInRectInt(rect, (p) => {
                    if(tileNeighbours.ContainsKey(p))
                        foreach(var d in dirs)
                            if(tileNeighbours.ContainsKey(p + d) || rect.Contains(p + d) == false)
                                tileNeighbours[p]++;
                });

                Minigame.ForeachInRectInt(rect, (p) =>
                {
                    if(countdown-- > 0) return;

                    if(reserved[p] == false && tileNeighbours.TryGetValue(p, out int neighbours) == true && neighbours == 4)
                    {
                        Minigame.ForeachInFlood
                        (
                            p,
                            reserved,
                            (kv) => kv.Value == false,
                            (fp) => reserved[fp] = true,
                            stepsCell: UnityEngine.Random.Range(11, 27),
                            isShuffleDirs: true
                        );

                        var sr = this.spriteRendererPool.Get(
                            p + Vector2.one * 0.5f,
                            RandomUtil.Pick(sprites),
                            this.objectLayers.tilesSortingLayer,
                            "handhold",
                            this.map.transformTiles
                        );
                        sr.sortingOrder += sortOffset;
                        sr.flipX = UnityEngine.Random.value > 0.5f;
                    }
                });
            }
        }
    }
}
