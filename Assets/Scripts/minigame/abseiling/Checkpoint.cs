using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace abseiling
    {
        public class Checkpoint : MonoBehaviour
        {

            [NonSerialized] AbseilingGame game;

            public Animated.Animation animationShine;
            public Animated.Animation animationBulb;
            public Animated.Animation animationBulbSmack;
            public Animated.Animation animationFlagIdle;
            public Animated.Animation animationFlagUnfold;
            public Animated.Animation animationSparkle;
            public SpriteRenderer spriteRendererStopHook;
            public SpriteRenderer spriteRendererDoorBack;
            public SpriteRenderer spriteRendererDoorFront;
            public int dir;

            private Animated single;
            private Animated flag;
            private Animated bulb;
            private Transform pole;
            private Transform poleBase;
            private GameObject pieces;
            private Vector2 bulbVelocity;

            private MaterialPropertyBlock propertyBlock;

            [NonSerialized] public bool active;
            [NonSerialized] public RopeHook hook;

            public readonly Vector2 NormalBulbPosition = new Vector2(0, 4.695f) * 0.75f;
            public readonly Vector2 NormalFlagPosition = new Vector2(0.16f, 2f) * 0.75f;
            public const float BulbAttachOffset = -0.69f * 0.75f;
            public const float NormalPoleLength = 3.251f * 0.75f;


            public void Init(int dir, AbseilingGame game)
            {
                this.dir = dir;
                this.game = game;
                this.single = this.transform.Find("single").GetComponent<Animated>();
                this.pieces = this.transform.Find("pieces").gameObject;
                this.flag = this.transform.Find("pieces/flag").GetComponent<Animated>();
                this.pole = this.transform.Find("pieces/pole");
                this.poleBase = this.transform.Find("pieces/pole base");
                this.bulb = this.transform.Find("pieces/bulb").GetComponent<Animated>();
                
                // place checkpoint in front of hook if it isn't on a cliff
                var p = this.pieces.transform.localPosition;
                var side = dir;
                var test = this.transform.position + p * side + Vector3.down * 0.5f + Vector3.right * side * 0.1f;
                if(test.x >= 0 && test.x <= game.map.width && Physics2D.OverlapPoint(test) == null) side *= -1;
                this.pieces.transform.localPosition = new Vector3(p.x * side, p.y);
                p = this.single.transform.localPosition;
                this.single.transform.localPosition = new Vector3(p.x * side, p.y);
                // nudge checkpoint in when hidden by doorway
                p = this.single.transform.position;
                if(p.x < 0.1f || p.x > game.map.width - 0.1f)
                {
                    this.single.transform.position += Vector3.right * -side * 0.5f;
                    this.pieces.transform.position += Vector3.right * -side * 0.5f;
                }

                ObjectLayers.SetSortingLayer(this.gameObject, game.objectLayers.playerSortingLayer);

                p = this.transform.position;
                this.spriteRendererDoorBack.transform.position =
                    this.spriteRendererDoorFront.transform.position = dir > 0 ? new Vector3(0, p.y) : new Vector3(game.map.width, p.y);
                this.spriteRendererDoorBack.flipX =
                    this.spriteRendererDoorFront.flipX = dir < 0;
                this.spriteRendererDoorBack.sortingLayerName = game.objectLayers.backgroundSortingLayer;
                this.spriteRendererDoorFront.sortingLayerName = game.objectLayers.foregroundSortingLayer;

                this.propertyBlock = new MaterialPropertyBlock();
            }

            private IEnumerator TintFlashCoroutine()
            {
                Tint(Color.black, 1);
                do
                {
                    yield return new WaitForSeconds(1 / 15f);
                }
                while(Time.timeScale == 0);

                Tint(new Color32(179, 94, 121, 255), 1);
                do
                {
                    yield return new WaitForSeconds(1 / 15f);
                }
                while(Time.timeScale == 0);

                Tint(Color.white, 1);
                do
                {
                    yield return new WaitForSeconds(1 / 15f);
                }
                while(Time.timeScale == 0);

                Tint(Color.white, 0);
            }

            private void Tint(Color color, float intensity)
            {
                void TintSprite(SpriteRenderer sr)
                {
                    sr.GetPropertyBlock(this.propertyBlock);
                    this.propertyBlock.SetColor("_Color", color);
                    this.propertyBlock.SetFloat("_Intensity", intensity);
                    sr.SetPropertyBlock(this.propertyBlock);
                }

                TintSprite(this.single.GetComponent<SpriteRenderer>());
                TintSprite(this.bulb.GetComponent<SpriteRenderer>());
                TintSprite(this.poleBase.GetComponent<SpriteRenderer>());
                TintSprite(this.pole.GetComponent<SpriteRenderer>());
                TintSprite(this.flag.GetComponent<SpriteRenderer>());
            }

            private void Update()
            {
                if(this.active == false && Minigame.paused == false && Time.frameCount % 80 == 0)
                    this.single.PlayOnce(this.animationShine);

                if(this.active == true)
                {
                    Vector2 targetVelocity =
                        (NormalBulbPosition - (Vector2)this.bulb.transform.localPosition) * 0.2f;

                    this.bulbVelocity *= 0.95f;
                    this.bulbVelocity += targetVelocity * 0.2f;

                    this.bulb.transform.position += (Vector3)this.bulbVelocity;

                    Vector2 top = this.bulb.transform.localPosition;
                    Vector2 bottom = this.pole.localPosition;
                    Vector2 vector = top - bottom;
                    float scale = vector.magnitude / NormalPoleLength;
                    float angleDegrees = (Mathf.Atan2(vector.y, vector.x) * 180 / Mathf.PI) - 90;

                    this.pole.transform.localRotation = Quaternion.Euler(0, 0, angleDegrees);
                    this.pole.transform.localScale = new Vector3(1, scale, 1);
                    this.flag.transform.localRotation = this.pole.transform.localRotation;
                    this.flag.transform.position = this.pole.transform.TransformPoint(
                        new Vector2(
                            NormalFlagPosition.x,
                            (NormalFlagPosition.y + (
                                vector.magnitude + BulbAttachOffset - NormalPoleLength
                            )) / scale
                        )
                    );
                    this.bulb.transform.localRotation = this.pole.transform.localRotation;
                }
            }

            public void Activate(bool skipFlagAnimation)
            {
                this.active = true;

                this.single.gameObject.SetActive(false);
                this.pieces.SetActive(true);

                this.hook.spriteRenderer.sortingOrder = 6;

                if(skipFlagAnimation == true)
                {
                    this.flag.GetComponent<SpriteRenderer>().enabled = true;
                    this.flag.PlayAndLoop(this.animationFlagIdle);
                }
                else
                {
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_player_checkpoint"]);

                    StartCoroutine(TintFlashCoroutine());
                    {
                        var p = Particle.CreateAndPlayOnce(
                            this.animationBulbSmack, this.bulb.transform.position, this.bulb.transform
                        );
                    }
                    this.bulbVelocity = Vector2.up * 0.35f;
                    Invoke(() =>
                    {
                        this.flag.GetComponent<SpriteRenderer>().enabled = true;
                        this.flag.PlayOnce(this.animationFlagUnfold, delegate
                        {
                            this.flag.PlayAndLoop(this.animationFlagIdle);
                        });
                    }, 1.5f);
                    for(var n = 0; n < 10; n += 1)
                    {
                        var delay = UnityEngine.Random.Range(0.1f, 1.5f);
                        Invoke(() =>
                        {
                            Particle.CreateAndPlayOnce(
                                this.animationSparkle,
                                this.bulb.transform.position + new Vector3(
                                    UnityEngine.Random.Range(-1.2f, 1.2f),
                                    UnityEngine.Random.Range(-1.2f, 1.2f)
                                ),
                                this.transform.parent
                            );
                        }, delay);
                    }
                }

            }

            public void Invoke(Action action, float time)
            {
                System.Collections.IEnumerator Impl()
                {
                    yield return new WaitForSeconds(time);
                    action();
                }
                StartCoroutine(Impl());
            }
        }

    }
}
