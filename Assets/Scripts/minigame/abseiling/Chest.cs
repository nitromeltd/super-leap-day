using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace abseiling
    {
        public class Chest : MonoBehaviour
        {
            [HideInInspector] public AbseilingGame game;
            public Animated.Animation anim;
            public Animated animated;

            private void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.game.IsComplete() || this.game.IsFailed()) return;

                this.animated.PlayOnce(anim);
                collision.GetComponent<Player>()?.SetState(Player.State.Win);
            }


        }

    }
}
