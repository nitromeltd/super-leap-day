using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace abseiling
    {
        public class Line
        {
            //public LineRenderer lineRenderer;
            public PathRenderer pathRenderer;
            public Sprite spriteLine;
            public List<Vector2> points = new List<Vector2>();
            public List<float> bends = new List<float>();
            public Vector2? drawEnd;
            public Vector2? drawStart;
            public Dictionary<int, bool> bendsLocked = new Dictionary<int, bool>();
            public Tween.Ease easeDroop;

            private float length;

            public void Reset(Vector2 start, Vector2 end, string sortingLayer)
            {
                //this.lineRenderer.material.mainTextureOffset = Vector2.zero;
                this.points.Clear();
                this.bends.Clear();
                this.bendsLocked.Clear();
                this.points.Add(start);
                this.points.Add(end);
                //this.lineRenderer.material.SetFloat("_ScrollOffset", 0);
                UpdateLineRenderer(0, sortingLayer);
            }

            public void AddBend(float dir, Vector2 cut)
            {
                this.bends.Add(dir);
                var end = this.points[^1];
                this.points[^1] = cut;
                this.points.Add(end);
            }


            // Prevent unbending
            public void LockBend(int index)
            {
                bendsLocked[index] = true;
            }

            public void UpdateLineRenderer(float droopMax, string sortingLayer, bool isSkipEnd = false)
            {
                var list = new List<Vector2>(this.points);
                if(this.drawStart.HasValue == true)
                    list[0] = this.drawStart.Value;
                if(this.drawEnd.HasValue == true)
                    list[^1] = this.drawEnd.Value;

                if(isSkipEnd == true)
                {
                    var n = list.Count - 1;
                    while(n > 0)
                    {
                        list.RemoveAt(n);
                        if(this.bendsLocked.ContainsKey(n - 2))
                            break;
                        n--;
                    }
                }
                else if(droopMax > 0f)
                {
                    var start = list[^2];
                    var end = list[^1];
                    list.RemoveAt(list.Count - 1);
                    list.AddRange(CreateLineDroop(start, end, 0.3f, droopMax));
                }

                if(list.Count == 1)
                    list.Add(list[0] += Vector2.down * 0.01f);

                this.pathRenderer.Draw(
                    this.spriteLine,
                    list.ToArray(),
                    false,
                    sortingLayer,
                    1f
                );
            }

            public List<Vector2> CreateLineDroop(Vector2 start, Vector2 end, float segmentLength, float maxDroop)
            {
                var dir = end - start;
                var len = dir.magnitude;
                if(len == 0)
                    return new List<Vector2> { start, end };
                
                var steps = Mathf.Ceil(len / segmentLength);
                var stepSize = len / steps;
                dir /= len;
                var list = new List<Vector2>();
                for(int i = 0; i < steps + 1; i++)
                {
                    var p = start + dir * i * stepSize;
                    p.y -= (float)this.easeDroop(len - i * stepSize, 0f, 1f, len) * maxDroop;
                    list.Add(p);
                }
                return list;
            }

            public Vector2 EndVector() =>
                this.points[^1] - this.points[^2];

            public (Vector2 a, Vector2 b) EndSegment() =>
                (this.points[^2], this.points[^1]);

            public Vector2 GetVector(int index) =>
                this.points[index + 1] - this.points[index];

            public float EndAngle()
            {
                var v = EndVector();
                return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
            }

            public Vector3 End
            {
                get => this.points[^1];
                set => this.points[^1] = value;
            }

            public Vector3 BeforeEnd
            {
                get => this.points[^2];
                set => this.points[^2] = value;
            }

            public float Length(int totalPoints = int.MaxValue)
            {
                if(this.points.Count <= 1) return 0;
                if(totalPoints > this.points.Count) totalPoints = this.points.Count;

                float len = 0f;
                for(int i = 1; i < totalPoints - 1; i++)
                    len += (this.points[i] - this.points[i - 1]).magnitude;
                return len;
            }

            public float LengthFrom(int i = 0)
            {
                float len = 0f;
                for(; i < this.points.Count - 1; i++)
                    len += (this.points[i] - this.points[i + 1]).magnitude;
                return len;
            }

            public Vector2 GetPositionAtLength(float length, out Vector2 delta)
            {
                delta = Vector2.down;

                for(int i = 1; i < this.points.Count; i++)
                {
                    (var a, var b) = (this.points[i - 1], this.points[i]);
                    delta = b - a;
                    if(delta.sqrMagnitude < length * length)
                        length -= delta.magnitude;
                    else
                        return a + delta.normalized * length;
                }
                return this.points[^1];
            }

            public float GetSegmentLengthRemaining(float dist, out int index, out float segmentLength)
            {
                segmentLength = 0;
                for(int i = 1; i < this.points.Count; i++)
                {
                    (var a, var b) = (this.points[i - 1], this.points[i]);
                    var delta = b - a;
                    segmentLength = delta.magnitude;
                    if(segmentLength < dist)
                    {
                        dist -= segmentLength;
                    }
                    else
                    {
                        index = i - 1;
                        return delta.magnitude - dist;
                    }
                }
                index = this.points.Count;
                return 0;
            }

            public List<Vector2> GetDeathPoints(float segmentLength, (int index, Vector2 ip)? deathCut, out List<Vector2> cutPoints)
            {
                cutPoints = new List<Vector2>();
                var index = 0;
                if(this.bendsLocked.Count > 0)
                {
                    for(int i = this.points.Count - 1; i > 0; i--)
                        if(this.bendsLocked.ContainsKey(i - 1))
                        {
                            index = i;
                            break;
                        }
                }
                var list = new List<Vector2>();

                void AddPoints(Vector2 a, Vector2 b, List<Vector2> target)
                {
                    var v = b - a;
                    var len = v.magnitude;
                    v /= len;
                    var total = Mathf.Ceil(len / segmentLength);
                    for(int j = 0; j < total; j++)
                        target.Add(a + v * segmentLength * j);
                }

                for(int i = index; i < this.points.Count - 1; i++)
                {
                    (var a, var b) = (this.points[i], this.points[i + 1]);
                    if(deathCut.HasValue && i == deathCut.Value.index)
                    {
                        AddPoints(a, deathCut.Value.ip, list);
                        AddPoints(deathCut.Value.ip, b, cutPoints);
                        continue;
                    }
                    if(cutPoints.Count > 0)
                        AddPoints(a, b, cutPoints);
                    else
                        AddPoints(a, b, list);
                }

                return list;
            }


            public void DoAtEachCell(Action<Vector2Int, (Vector2 a, Vector2 b), int> action)
            {
                for(int i = 1; i < this.points.Count; i++)
                {
                    var a = this.points[i - 1];
                    var b = this.points[i];
                    var cells = GetCells(a, b);
                    for(int j = 0; j < cells.Count; j++)
                        action(cells[j], (a, b), i - 1);
                }
            }

            public void DoAtEachSegment(Action<Vector2, Vector2> action)
            {
                for(int i = 1; i < points.Count; i++)
                    action(points[i - 1], points[i]);
            }

            public void DoAtEachSegment(Action<(Vector2 a, Vector2 b), int> action)
            {
                for(int i = 1; i < points.Count; i++)
                    action((points[i - 1], points[i]), i - 1);
            }

            #region UTILITIES

            // Returns true if both line segments intersect
            public static bool GetLineIntersection(Vector2 a1, Vector2 b1, Vector2 a2, Vector2 b2, out Vector2 intersection)
            {
                float dx1 = b1.x - a1.x;
                float dy1 = b1.y - a1.y;
                float dx2 = b2.x - a2.x;
                float dy2 = b2.y - a2.y;

                float denominator = (dy1 * dx2 - dx1 * dy2);
                float t1 = ((a1.x - a2.x) * dy2 + (a2.y - a1.y) * dx2) / denominator;
                intersection = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);

                if(float.IsInfinity(t1))
                    return false;

                float t2 = ((a2.x - a1.x) * dy1 + (a1.y - a2.y) * dx1) / -denominator;

                if(t1 < 0 || t1 > 1 || t2 < 0 || t2 > 1)
                    return false;

                return true;
            }

            public static List<Vector2Int> GetCells(Vector2 a, Vector2 b, bool skipFirstCell = false)
            {
                var result = new List<Vector2Int>();
                var delta = b - a;
                var length = delta.magnitude;
                var normal = delta / length;
                var invNormal = new Vector2(1f / normal.x, 1f / normal.y);
                var cell = new Vector2Int(Mathf.FloorToInt(a.x), Mathf.FloorToInt(a.y));
                Vector2Int dir = new Vector2Int(normal.x < 0 ? -1 : 1, normal.y < 0 ? -1 : 1);
                Vector2 positionWithinCell = a - cell;
                Vector2 targetFromWithinCell = new Vector2((normal.x < 0) ? 0 : 1f, (normal.y < 0) ? 0 : 1f);
                //Minigame.DebugX(cell + positionWithinCell, Color.white, 0.1f);
                while(length > 0)
                {
                    //Minigame.DebugRect(new Rect(cell.x, cell.y, 1, 1), Color.red);
                    if(skipFirstCell == false)
                        result.Add(cell);
                    else
                        skipFirstCell = false;

                    float nextX = Mathf.Abs((positionWithinCell.x - targetFromWithinCell.x) * invNormal.x);
                    float nextY = Mathf.Abs((positionWithinCell.y - targetFromWithinCell.y) * invNormal.y);

                    if(nextX < nextY || nextY == 0)
                    {
                        cell.x += dir.x;
                        length -= nextX;
                        positionWithinCell += normal * nextX;
                        positionWithinCell.x -= dir.x;
                    }
                    else
                    {
                        cell.y += dir.y;
                        length -= nextY;
                        positionWithinCell += normal * nextY;
                        positionWithinCell.y -= dir.y;
                    }
                    //Minigame.DebugX(cell + positionWithinCell, Color.white, 0.1f);
                }
                return result;
            }

            public static (Vector2 a, Vector2 b) LineFromSideOfACell(Vector2Int cell, Vector2 dir)
            {
                var a = new Vector2
                (
                    cell.x + (dir.y != 0 ? 0.5f : (dir.x < 0 ? 1f : 0f)),
                    cell.y + (dir.x != 0 ? 0.5f : (dir.y < 0 ? 1f : 0f))
                );
                return (a, a + dir);
            }

            // In polygon.points where rayHit is on a segment, return vertex in direction of rayVector
            public static Vector2 GetBendVertex(Vector2[] points, Vector2 rayFrom, Vector2 rayTo, Vector2 rayHit)
            {
                // Unity doesn't tell us about the segment the raycast hit so we have to find it
                var best = float.MaxValue;
                var length = points.Length;
                (Vector2 a, Vector2 b) bestSegment = (Vector2.zero, Vector2.zero);
                var pointsCenter = Vector2.zero;

                for(int i = 0; i < length; i++)
                {
                    var a = points[i];
                    var b = points[(i + 1) % length];
                    var dist = SquareDistanceEdgeToPoint(a, b, rayHit);
                    if(dist < best)
                    {
                        best = dist;
                        bestSegment = (a, b);
                    }
                    pointsCenter += a;
                }

                // target vertex is usually be in direction of rayVector
                Vector2 target = bestSegment.b;
                Vector2 alternate = bestSegment.a;
                var rayVector = rayTo - rayFrom;
                var segmentVector = bestSegment.b - bestSegment.a;
                if(Vector2.Dot(segmentVector, rayVector) < 0)
                {
                    target = bestSegment.a;
                    alternate = bestSegment.b;
                }

                // near-perpendicular ray edge case
                // check we are the correct side of the center
                var rayVectorLeftHand = Vector2.Perpendicular(rayVector);
                //Debug.Log(Vector2.Dot(rayVectorLeftHand.normalized, segmentVector.normalized));
                if(Mathf.Abs(Vector2.Dot(rayVectorLeftHand.normalized, segmentVector.normalized)) > 0.5f)
                {
                    pointsCenter /= points.Length;
                    //Minigame.DebugX(pointsCenter, Color.green);
                    var rayToCenterLeftHand = Vector2.Perpendicular(pointsCenter - rayFrom);
                    var originalSide = Mathf.Sign(Vector2.Dot(rayToCenterLeftHand, rayVector));
                    var targetSide = Mathf.Sign(Vector2.Dot(rayToCenterLeftHand, target - rayFrom));
                    //Debug.Log($"original  {originalSide} {targetSide}");
                    if(targetSide != originalSide)
                    {
                        //Debug.Log("perpendicular - switch target");
                        return alternate;
                    }
                }
                return target;
            }

            public static Vector2 GetNearestPointToLine(Vector2 lineFrom, Vector2 lineTo, params Vector2[] points)
            {
                var best = float.MaxValue;
                var length = points.Length;
                var bestPoint = Vector2.zero;

                for(int i = 0; i < length; i++)
                {
                    var p = points[i];
                    var dist = SquareDistanceEdgeToPoint(lineFrom, lineTo, p);
                    if(dist < best)
                    {
                        best = dist;
                        bestPoint = p;
                    }
                }
                return bestPoint;
            }

            public static float SquareDistanceEdgeToPoint(Vector2 from, Vector2 to, Vector2 point)
            {
                float edgeDeltaX = to.x - from.x;
                float edgeDeltaY = to.y - from.y;
                float toTargetX = point.x - from.x;
                float toTargetY = point.y - from.y;

                var along =
                    (toTargetX * edgeDeltaX + toTargetY * edgeDeltaY) /
                    (edgeDeltaX * edgeDeltaX + edgeDeltaY * edgeDeltaY);
                if(along < 0) along = 0;
                if(along > 1) along = 1;

                var nearestX = from.x + (edgeDeltaX * along);
                var nearestY = from.y + (edgeDeltaY * along);
                var toNearestX = point.x - nearestX;
                var toNearestY = point.y - nearestY;
                var sqDist = toNearestX * toNearestX + toNearestY * toNearestY;
                return sqDist;
            }

            #endregion

            #region NOTES

            // Find the point of intersection between lines 1 & 2
            // copied from: http://csharphelper.com/blog/2014/08/determine-where-two-lines-intersect-in-c/
            // This method is here for future reference
            void LineIntersectionMath(
                Vector2 a1, Vector2 b1, Vector2 a2, Vector2 b2,
                out bool lines_intersect, out bool segments_intersect,
                out Vector2 intersection,
                out Vector2 close_p1, out Vector2 close_p2)
            {
                // Get the segments' parameters.
                float dx1 = b1.x - a1.x;
                float dy1 = b1.y - a1.y;
                float dx2 = b2.x - a2.x;
                float dy2 = b2.y - a2.y;

                // Solve for t1 and t2
                float denominator = (dy1 * dx2 - dx1 * dy2);

                float t1 =
                    ((a1.x - a2.x) * dy2 + (a2.y - a1.y) * dx2)
                        / denominator;
                if(float.IsInfinity(t1))
                {
                    // The lines are parallel (or close enough to it).
                    lines_intersect = false;
                    segments_intersect = false;
                    intersection = new Vector2(float.NaN, float.NaN);
                    close_p1 = new Vector2(float.NaN, float.NaN);
                    close_p2 = new Vector2(float.NaN, float.NaN);
                    return;
                }
                lines_intersect = true;

                float t2 =
                    ((a2.x - a1.x) * dy1 + (a1.y - a2.y) * dx1)
                        / -denominator;

                // Find the point of intersection.
                intersection = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);

                // The segments intersect if t1 and t2 are between 0 and 1.
                segments_intersect =
                    ((t1 >= 0) && (t1 <= 1) &&
                     (t2 >= 0) && (t2 <= 1));

                // Find the closest points on the segments.
                if(t1 < 0)
                    t1 = 0;
                else if(t1 > 1)
                    t1 = 1;

                if(t2 < 0)
                    t2 = 0;
                else if(t2 > 1)
                    t2 = 1;

                close_p1 = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);
                close_p2 = new Vector2(a2.x + dx2 * t2, a2.y + dy2 * t2);
            }

            #endregion


        }
    }
}
