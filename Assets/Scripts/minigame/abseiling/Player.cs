using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using StopHook = minigame.abseiling.AbseilingGame.StopHook;

namespace minigame
{
    namespace abseiling
    {
        public class Player : MonoBehaviour
        {
            [NonSerialized] public AbseilingGame game;
            [NonSerialized] public Vector3 endPrev;
            [NonSerialized] public float dirHorizontal;
            [NonSerialized] public Checkpoint currentCheckpoint;
            [NonSerialized] public List<Checkpoint> checkpoints;

            public State state;
            public SpriteRenderer spriteRenderer;
            public SpriteRenderer spriteRendererHead;
            public SpriteRenderer spriteRendererArms;
            public Animated animated;
            public Animated animatedHead;
            public PathRenderer ropeRenderer;
            public PathRenderer ropeFakeRenderer;
            public PathRenderer ropeFakeCutRenderer;
            public CircleCollider2D circle;
            public Transform pinRun;
            public Transform pinHang;
            public Rigidbody2D body;
            public DistanceJoint2D joint;
            public ContactFilter2D filterTile;
            public ContactFilter2D filterTrigger;
            public List<Line> lines;
            public Line lineCurrent;
            public Sprite spriteDirOn;
            public Sprite spriteDiroff;
            public Sprite spriteRope;
            public AnimationCurve animationCurveScale;
            public AnimationCurve animationCurveLineDroop;
            public List<RopeHook> ropeHooks;
            public Transform ropeEnd;
            public Transform ropeEndCutTop;
            public Transform ropeEndCutBottom;
            public Color colorRope = Color.white;
            public (int index, Vector2 ip)? deathCut;
            public int? winchIndex;

            [Serializable]
            public class PlayerAnimations
            {
                public Animated.Animation animationIdle;
                public Animated.Animation animationRun;
                public Animated.Animation animationSwing;
                public Animated.Animation animationSwingKick;
                public Animated.Animation animationJumpUp;
                public Animated.Animation animationJumpToDown;
                public Animated.Animation animationJumpDown;
                public Animated.Animation animationGrabStand;
                public Sprite spriteGrabStandArm;
            };
            public PlayerAnimations yolk;
            public PlayerAnimations puffer;
            public PlayerAnimations pufferHead;
            public PlayerAnimations goop;
            public PlayerAnimations sprout;
            public PlayerAnimations king;
            public PlayerAnimations kingHat;
            public PlayerAnimations AnimationsForCharacter(Character character) =>
                character switch
                {
                    Character.Puffer => this.puffer,
                    Character.Goop => this.goop,
                    Character.Sprout => this.sprout,
                    Character.King => this.king,
                    _ => this.yolk,
                };

            private List<Motor> motorsPaused;
            [NonSerialized] public float timeKick;
            [NonSerialized] public float kickLock;
            [NonSerialized] public float timeKickDust;
            private float timeKickStick;
            private ContactPoint2D kickContact;
            private Tween tweenScale;
            private RopeFake ropeFake;
            private RopeFake ropeFakeCut;
            private Tween tweenRopeSlack;
            private Tween tweenRopeTighten;
            private AbseilingGame.StopHook stopHook;
            private bool isWindSfx;
            private Coroutine coroutineWindSfx;
            private int timeSinceDustParticle;

            public const float KickDelay = 0.1f;
            public const float KickDustDelay = 0.25f;
            public const float KickStickDelay = 0.3f;
            public const float DropSpeed = 0.2126129f;
            public const float WalkSpeed = 0.1376364f;
            public const float SwingStr = 2.24f;
            public const float KickPowerMin = 5.5f;
            public const float KickPowerBase = 14.5f;
            public const float KickPowerDecPerTile = 0.129f;
            public const float WinchLengthMin = 2f;
            public const float WinchSpeed = -DropSpeed;

            const float CutPadding = 0.1f;

            static Vector2[] BendWalk = new[]
            {
                Vector2.up,
                (Vector2.up + Vector2.right).normalized,
                Vector2.right,
                (Vector2.right + Vector2.down).normalized,
                Vector2.down,
                (Vector2.down + Vector2.left).normalized,
                Vector2.left,
                (Vector2.left + Vector2.up).normalized
            };

            public enum State
            {
                RunIn, Stop, StopIn, Drop, Run, Death, KickStick, Win
            }

            public Vector3 GetPositionStart(Checkpoint checkpoint) => checkpoint.transform.position + Vector3.up * this.circle.radius;


            public void Init(AbseilingGame game, List<Checkpoint> checkpoints)
            {
                this.game = game;
                this.checkpoints = checkpoints;

                this.lineCurrent = new Line
                {
                    pathRenderer = ropeRenderer,
                    easeDroop = Easing.ByAnimationCurve(this.animationCurveLineDroop),
                    spriteLine = this.spriteRope
                };
                this.lines = new List<Line> { lineCurrent };
                this.motorsPaused = new List<Motor>();
                this.tweenScale = new Tween(Vector3.zero, Vector3.one, 1f, Easing.ByAnimationCurve(this.animationCurveScale));
                this.currentCheckpoint = this.checkpoints[^1];
                this.currentCheckpoint.Activate(true);
                this.currentCheckpoint.hook.ropeKnot.gameObject.SetActive(true);
                this.ropeEnd.SetParent(this.transform.parent);
                this.ropeHooks = new List<RopeHook> { this.checkpoints[^1].hook };
                this.tweenRopeTighten = new Tween(1.2f, 0, 0.3f, Easing.BounceEaseOut);
                this.tweenRopeSlack = new Tween(0, 1.2f, 0.5f, Easing.QuadEaseOut);

                SetState(State.RunIn);

            }

            public void SetState(State newState)
            {
                var character = this.game.character;
                var anims = AnimationsForCharacter(character);
                this.spriteRenderer.enabled = true;
                this.spriteRendererHead.enabled = character == Character.King || character == Character.Puffer;
                this.spriteRendererHead.sortingOrder = this.spriteRenderer.sortingOrder + (character == Character.King ? -1 : 1);
                this.spriteRendererArms.gameObject.SetActive(newState == State.Stop || newState == State.StopIn);
                this.spriteRendererArms.sprite = anims.spriteGrabStandArm;

                // stop wind sfx when not swinging
                if
                (
                    this.isWindSfx == true &&
                    newState != State.Drop &&
                    newState != State.KickStick
                )
                {
                    if(this.coroutineWindSfx != null)
                        StopCoroutine(this.coroutineWindSfx);
                    StartCoroutine(ChangeWindSfx(false));
                }

                switch(newState)
                {
                    case State.RunIn:
                        // start offscreen and run in
                        this.dirHorizontal = this.currentCheckpoint.dir;
                        var startPosition = GetPositionStart(this.currentCheckpoint);
                        startPosition.x = this.dirHorizontal > 0 ? this.game.map.rect.x - 2f : this.game.map.rect.xMax + 2f;
                        this.body.simulated = false;
                        this.lineCurrent.Reset(this.ropeHooks[0].position, startPosition, this.game.objectLayers.playerSortingLayer);
                        this.transform.rotation = Quaternion.identity;
                        this.transform.position = this.lineCurrent.End;
                        this.joint.connectedAnchor = this.lineCurrent.BeforeEnd;
                        this.timeKick = Time.timeSinceLevelLoad - KickDelay;
                        this.timeKickDust = Time.timeSinceLevelLoad - KickDustDelay;
                        this.kickLock = 0f;
                        this.deathCut = null;
                        foreach(var m in this.motorsPaused)
                            m.paused = false;
                        this.motorsPaused.Clear();
                        UpdateFacing();
                        this.animated.PlayAndLoop(anims.animationIdle);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationIdle);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationIdle);
                        this.ropeFakeCut = null;
                        this.ropeEndCutTop.gameObject.SetActive(false);
                        this.ropeEndCutBottom.gameObject.SetActive(false);
                        this.ropeFakeCutRenderer.gameObject.SetActive(false);
                        this.ropeFake = new RopeFake(this.pinRun.position, 0.2f, 20, this.ropeEnd);
                        // warm up rope
                        this.ropeFake.direction = Vector2.left * this.dirHorizontal;
                        this.ropeFake.start = this.lineCurrent.End;
                        for(int i = 0; i < 60; i++) this.ropeFake.Advance();
                        this.tweenRopeSlack.count = this.tweenRopeSlack.delay;
                        this.animated.transform.rotation =
                            this.animatedHead.transform.rotation = Quaternion.identity;

                        this.animated.PlayAndLoop(anims.animationRun);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationRun);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationRun);
                        UpdateFacing();
                        this.tweenRopeTighten.count = 0;
                        this.animated.transform.rotation =
                        this.animatedHead.transform.rotation = Quaternion.identity;
                        break;

                    case State.Run:
                        this.timeKick = Time.timeSinceLevelLoad - KickDelay;
                        this.timeKickDust = Time.timeSinceLevelLoad - KickDustDelay;
                        this.kickLock = 0f;
                        this.joint.enabled = false;
                        this.body.simulated = true;
                        this.animated.PlayAndLoop(anims.animationRun);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationRun);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationRun);
                        UpdateFacing();
                        this.tweenRopeTighten.count = 0;
                        this.animated.transform.rotation =
                        this.animatedHead.transform.rotation = Quaternion.identity;
                        if(this.state == State.Drop)
                            CreateLandDust(this.transform.position + this.circle.radius * Vector3.down);
                        break;
                    case State.Stop:
                        this.body.velocity = Vector2.zero;
                        this.body.simulated = false;
                        this.animated.PlayAndLoop(anims.animationGrabStand);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationGrabStand);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationGrabStand);

                        break;
                    case State.StopIn:
                        this.body.velocity = Vector2.zero;
                        this.body.simulated = false;
                        this.animated.PlayAndLoop(anims.animationGrabStand);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationGrabStand);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationGrabStand);

                        break;
                    case State.KickStick:
                        this.timeKickStick = Time.timeSinceLevelLoad;
                        this.joint.enabled = false;
                        this.body.simulated = false;
                        break;
                    case State.Drop:
                        this.tweenRopeSlack.count = 0;
                        this.joint.connectedAnchor = this.lineCurrent.BeforeEnd;
                        this.joint.distance = this.lineCurrent.EndVector().magnitude;
                        this.joint.enabled = true;
                        this.body.simulated = true;
                        //this.containerDirs.gameObject.SetActive(true);
                        UpdateFacing();
                        this.animated.PlayAndLoop(anims.animationSwing);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationSwing);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationSwing);
                        break;
                    case State.Death:
                        if(this.state == State.Death || this.game.IsComplete() || this.game.IsFailed()) return;
                        this.game.coroutineForCamera = StartCoroutine(DeathSequence());
                        break;
                    case State.Win:
                        if(this.game.IsComplete() || this.game.IsFailed()) return;
                        this.joint.enabled = false;
                        this.body.simulated = false;
                        this.animated.PlayAndLoop(anims.animationIdle);
                        if(this.game.character == Character.King)
                            this.animatedHead.PlayAndLoop(kingHat.animationIdle);
                        else if(this.game.character == Character.Puffer)
                            this.animatedHead.PlayAndLoop(pufferHead.animationIdle);
                        this.game.CompleteLevel();
                        break;
                }

                this.state = newState;
            }

            IEnumerator DeathSequence()
            {
                this.spriteRenderer.enabled = false;
                this.spriteRendererHead.enabled = false;
                this.body.simulated = false;
                this.joint.enabled = false;
                if(this.winchIndex.HasValue == true)
                {
                    this.winchIndex = null;
                    this.ropeHooks[^1].Winch(false);
                }

                var deathPos = this.transform.position + Vector3.up * 0.2f;
                PlayerAssetsForMinigame.Death(deathPos, this.game);
                if(this.game.character == Character.King)
                {
                    Particle hatParticle = Particle.CreateWithSprite(
                                    game.assets.spriteLookup["king_hat_debris"],
                                    120,
                                    deathPos,
                                    game.transform
                                );
                    hatParticle.velocity = Vector2.up * 0.5f;
                    hatParticle.acceleration = new Vector2(0, -0.025f);
                    hatParticle.spin = 5f;
                    hatParticle.spriteRenderer.flipX = this.spriteRenderer.flipX;
                }

                this.lineCurrent.End = this.body.position;
                UpdateLineRenderers();

                var points = this.lineCurrent.GetDeathPoints(this.ropeFake.segmentLength, this.deathCut, out List<Vector2> cutPoints);
                if(cutPoints.Count > 0)
                    cutPoints.AddRange(this.ropeFake.points);
                else
                    points.AddRange(this.ropeFake.points);

                this.ropeFake = new RopeFake(points, this.ropeFake.segmentLength - 0.01f, null, cutPoints.Count > 0 ? this.ropeEndCutTop : this.ropeEnd);
                if(cutPoints.Count > 0)
                {
                    this.ropeFakeCutRenderer.gameObject.SetActive(true);
                    this.ropeEndCutTop.gameObject.SetActive(true);
                    this.ropeEndCutBottom.gameObject.SetActive(true);
                    this.ropeFakeCut = new RopeFake(cutPoints, this.ropeFake.segmentLength - 0.01f, this.ropeEndCutBottom, this.ropeEnd);

                    var highlight = this.game.ParticleCreateAndPlayOnce("death_highlight", cutPoints[0]);
                    highlight.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                    highlight.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 3;
                    highlight.transform.localScale = Vector3.one * 1.5f;

                    this.ropeFakeCut.start += Vector2.left * this.dirHorizontal;
                }

                game.SpendLife();

                yield return new WaitForSeconds(2f);

                var color = Color.white;
                while(color.a > 0f)
                {
                    color.a -= 0.125f;
                    SetRopeColor(color);
                    this.ropeFakeRenderer.GetComponent<MeshRenderer>().material.SetColor("_Color", color);
                    yield return null;
                }

                if(this.game.livesLeft <= 0)
                {
                    this.game.FailLevel();
                    yield break;
                }
                while(this.lines.Count > 1)
                    RemoveLastLine();
                for(int i = 0; i < this.ropeHooks.Count; i++)
                    this.ropeHooks[i].Restart();

                // select first checkpoint activated
                for(int i = 0; i < this.checkpoints.Count; i++)
                    if(this.checkpoints[i].active == true)
                    {
                        this.currentCheckpoint = this.checkpoints[i];
                        break;
                    }

                yield return this.game.camCtrl.TweenTo(GetPositionStart(this.currentCheckpoint), Vector3.zero, 0.5f, Easing.QuadEaseOut);

                this.currentCheckpoint.hook.ropeKnot.gameObject.SetActive(true);
                this.ropeHooks = new List<RopeHook> { this.currentCheckpoint.hook };
                SetRopeColor(new Color(1, 1, 1, 0f));

                SetState(State.RunIn);

                this.game.coroutineForCamera = null;
            }

            public void Advance()
            {
                switch(this.state)
                {
                    case State.RunIn:
                        this.lineCurrent.End += WalkSpeed * dirHorizontal * Vector3.right;
                        this.joint.distance = this.lineCurrent.EndVector().magnitude;
                        this.transform.position = this.lineCurrent.End;
                        if(this.colorRope.a < 1f)
                        {
                            this.colorRope.a += 0.125f;
                            SetRopeColor(this.colorRope);
                        }
                        StopHookCheck();
                        AdvanceRunDust();

                        this.game.textState.text = "run in";
                        break;
                    case State.StopIn:
                        if(this.game.input.pressed == true)
                        {
                            //this.game.SpendLife();
                            SetState(State.Run);
                        }
                        this.game.textState.text = "stop in";
                        break;
                    case State.Stop:
                        if(this.game.input.pressed == true)
                        {
                            //this.game.SpendLife();
                            SetState(State.Run);
                        }
                        this.game.textState.text = "stop";
                        break;
                    case State.Run:
                        if(this.winchIndex.HasValue == true)
                        {
                            if(this.lineCurrent.LengthFrom(this.winchIndex.Value) > WinchLengthMin)
                            {
                                this.joint.distance += WinchSpeed;
                                SetState(State.Drop);
                            }
                            else
                            {
                                this.winchIndex = null;
                                this.ropeHooks[^1].Winch(false);
                            }
                        }
                        // DEBUG
                        if(Input.GetMouseButton(1) || (Input.GetMouseButton(0) && game.isOverDebugGoUpButton))
                        {
                            SetState(State.Drop);
                            joint.distance -= DropSpeed;
                        }
                        MoveLineEnd(this.transform.position, 0.75f);
                        this.body.MovePosition(Run());
                        CollisionChecks();
                        StopHookCheck();
                        AdvanceRunDust();
                        {
                            // check for finish
                            var test = this.lineCurrent.End + Vector3.down * (circle.radius - 0.1f);
                            var finish = this.game.triggerMap.GetItem<AbseilingGame.Finish>(test);
                            if(finish != null)
                                SetState(State.Win);
                        }

                        this.game.textState.text = "run";
                        break;
                    case State.Drop:
                        //Debug.Log($"VEL {this.body.velocity}");

                        if(this.winchIndex.HasValue == true)
                        {
                            if(this.lineCurrent.LengthFrom(this.winchIndex.Value) > WinchLengthMin)
                            {
                                this.joint.distance += WinchSpeed;
                            }
                            else
                            {
                                this.winchIndex = null;
                                this.ropeHooks[^1].Winch(false);
                            }
                        }
                        else if(this.game.input.holding == true)
                        {
                            var jointVector = (this.transform.position - this.lineCurrent.BeforeEnd).normalized;
                            var cast = CircleCastFiltered(this.transform.position, this.circle.radius, jointVector, filterTile);
                            if(cast == null || cast.Value.normal.y == 1 || cast.Value.distance > circle.radius + DropSpeed)
                            {
                                this.joint.distance += DropSpeed;
                            }
                            else if(cast.HasValue)
                            {
                                this.joint.distance += DropSpeed;
                                this.timeKick = Time.timeSinceLevelLoad;
                            }
                            if(cast != null)
                            {
                                //Debug.Log($"Drop Collision {cast.Value.normal} {cast.Value.distance}");
                            }
                        }


#if UNITY_EDITOR || DEVELOPMENT_BUILD
                        // right mouse button to ascend rope when debugging
                        if(Input.GetMouseButton(1) || (Input.GetMouseButton(0) && game.isOverDebugGoUpButton))
                            this.joint.distance -= DropSpeed;
#endif

                        MoveLineEnd(this.transform.position, 0.6f);
                        //Debug.Log(this.joint.distance);
                        CollisionChecks();

                        if(this.state == State.Death)
                            break;

                        this.game.textState.text = "drop";

                        var axis = Input.GetAxisRaw("Horizontal");
                        if(axis < -0.1f)
                            this.body.AddForce(Vector2.left * 10);
                        else if(axis > 0.1f)
                            this.body.AddForce(Vector2.right * 20);

                        SwingCheck();

                        if(Time.timeSinceLevelLoad - this.timeKickDust < KickDustDelay && (Time.frameCount & 1) == 1)
                        {
                            var endNormal = this.lineCurrent.EndVector().normalized;

                            var p = (Vector2)this.transform.position + endNormal * 0.25f + Vector2.Perpendicular(endNormal) * this.dirHorizontal;
                            this.game.AddDustCloud(p, 0.25f, 1, this.game.objectLayers.fxSortingLayer);
                        }

                        break;
                    case State.KickStick:
                        if(Time.timeSinceLevelLoad - this.timeKickStick >= KickStickDelay)
                        {
                            SetState(State.Drop);
                            KickOut();
                        }
                        CollisionChecks();
                        break;
                    case State.Death:
                        this.game.textState.text = "dead";
                        break;
                }

                this.endPrev = this.body.position;

                Debug.DrawLine(this.transform.position, this.joint.connectedAnchor, Color.white);

            }

            private void LateUpdate()
            {
                // Unity updates the body position and attached gfx *after* Advance()
                // This forces us to update the line here so it actually connects
                switch(this.state)
                {
                    case State.Win:
                    case State.Drop:
                    case State.Stop:
                    case State.StopIn:
                    case State.Run:
                    case State.RunIn:
                    case State.KickStick:
                    case State.Death:
                        UpdateLineRenderers();
                        break;
                }
            }

            // Manage thrust when swinging freely
            public void SwingCheck()
            {
                if(Time.timeSinceLevelLoad - this.timeKick < KickDelay)
                    return;

                var swingX = this.transform.position.x - this.endPrev.x;

                if(kickLock != 0)
                {
                    if(swingX != 0 && Mathf.Sign(swingX) == Mathf.Sign(this.dirHorizontal))
                    {
                        //Debug.Log($"KICK LOCK OFF {swingX} {dirHorizontal} {Minigame.frameCount}");
                        this.kickLock = 0;
                    }
                    else
                        return;
                }

                SwingSide(out Vector2 perp, out float swingSide);

                if(swingX > 0 && dirHorizontal < 0)
                {
                    //Debug.Log($"> {swingX} {dirHorizontal} {swingSide} {Minigame.frameCount}");
                    dirHorizontal = 1;
                    UpdateFacing();
                }
                else if(swingX < 0 && dirHorizontal > 0)
                {
                    //Debug.Log($"< {swingX} {dirHorizontal} {swingSide} {Minigame.frameCount}");
                    dirHorizontal = -1;
                    UpdateFacing();
                }

                if(Mathf.Sign(dirHorizontal) == Mathf.Sign(swingSide))
                {
                    this.body.AddForce(perp * swingSide * SwingStr);

                }
            }

            public void SwingSide(out Vector2 perpendicular, out float swingSide)
            {
                perpendicular = Vector2.Perpendicular(this.lineCurrent.EndVector()).normalized;
                var dot = Vector2.Dot(perpendicular, Vector2.down);
                swingSide = (1f - Mathf.Abs(dot)) * Mathf.Sign(dot);
            }

            public void CollisionChecks()
            {
                // check for cell collision
                foreach(var line in lines)
                {
                    if(this.deathCut != null)
                        break;

                    line.DoAtEachCell((cell, segment, index) =>
                    {
                        if(this.deathCut != null)
                            return;

                        var c = Color.blue;
                        // look for spikes
                        if(this.game.spikesLookup.ContainsKey(cell) == true)
                        {
                            var delta = segment.b - segment.a;
                            var spikeLine = Line.LineFromSideOfACell(cell, AbseilingGame.SpikeIndexToDir(this.game.spikesLookup[cell]));

                            if(Line.GetLineIntersection(spikeLine.a, spikeLine.b, segment.a, segment.b, out Vector2 ip) == true)
                            {
                                c = Color.red;
                                //Minigame.DebugX(ip, Color.white);
                                this.deathCut = (index, ip);
                                return;
                            }
                        }
                        // look for buttons
                        if(this.game.triggerMap.TryGet(cell, out TriggerButton triggerButton) == true)
                        {
                            if(triggerButton.enabled == true)
                            {
                                var raycast = RaycastFiltered(segment.a, (segment.b - segment.a).normalized, this.filterTrigger);
                                if(raycast.HasValue && raycast.Value.collider == triggerButton.trigger)
                                    triggerButton.Hit();
                            }
                        }
                        // look for spike flaps
                        if(this.game.triggerMap.TryGet(cell, out SpikeFlap spikeFlap) == true)
                        {
                            if(spikeFlap.locked == false)
                            {
                                var raycast = RaycastFiltered(segment.a, (segment.b - segment.a).normalized, this.filterTrigger);
                                if(raycast.HasValue && raycast.Value.collider == spikeFlap.trigger)
                                    spikeFlap.Lock();
                            }
                        }
                        // look for corner hooks
                        if
                        (
                            index == this.lineCurrent.points.Count - 2 &&
                            this.game.triggerMap.TryGet(cell, out RopeHook hook) == true &&
                            segment.b.y < cell.y - 0.5f &&
                            segment.a.y > cell.y + 1
                        ) {
                            if(this.winchIndex.HasValue == true) {
                                this.winchIndex = null;
                                this.ropeHooks[^1].Winch(false);
                            }
                            this.lineCurrent.AddBend(1, hook.transform.position);
                            this.lineCurrent.LockBend(this.lineCurrent.bends.Count - 1);
                            this.joint.connectedAnchor = this.lineCurrent.BeforeEnd;
                            this.joint.distance = this.lineCurrent.EndVector().magnitude;
                            this.ropeHooks.Add(hook);
                            hook.Attach(index+1);
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_abseiling_hook_attach"]);
                        }
                        // look for rope walkers
                        if
                        (
                            this.game.triggerMap.TryGet(cell, out RopeWalker ropeWalker) == true &&
                            ropeWalker.IsReady() == true &&
                            Line.SquareDistanceEdgeToPoint(segment.a, segment.b, (Vector2)cell + Vector2.one * 0.5f) < 0.5f * 0.5f
                        ) {
                            ropeWalker.Attach(this.lineCurrent, index, (((Vector2)cell + Vector2.one * 0.5f) - segment.a).magnitude);
                        }
                        //Minigame.DebugRect(new Rect(cell.x, cell.y, 1, 1), c);

                    });
                }
                if(this.deathCut != null)
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                    SetState(State.Death);
                    return;
                }
                var hitRect = new Rect(0, 0, 1, 1.5f);
                hitRect.center = (Vector2)this.transform.position + Vector2.up * 0.4f;
                // check for spikes (be generous)
                var spikeHits = PickupMap.GetRectCollideCells(this.game.spikesLookup, hitRect.center, hitRect.size * 0.6f);
                if(spikeHits.Count > 0)
                {
                    SetState(State.Death);
                    return;
                }
                // check for coins
                this.game.pickupMap.RectCollidePickup(hitRect.center, hitRect.size);

                // read wind map
                var windCells = PickupMap.GetCircleCollideCells(this.game.windLookup, this.transform.position, this.circle.radius);
                var boost = Vector2.zero;
                foreach(var c in windCells)
                    boost += this.game.windLookup[c];
                boost.Normalize();

                if(boost != Vector2.zero)
                {
                    this.body.AddForce(boost * 5f);

                    if(this.isWindSfx == false && this.coroutineWindSfx != null)
                        StopCoroutine(this.coroutineWindSfx);
                    StartCoroutine(ChangeWindSfx(true));
                }
                else
                {
                    if(this.isWindSfx == true && this.coroutineWindSfx != null)
                        StopCoroutine(this.coroutineWindSfx);
                    StartCoroutine(ChangeWindSfx(false));
                }

                // update checkpoints
                foreach(var c in this.checkpoints)
                {
                    if(c.active == false && this.transform.position.y - (this.circle.radius + 0.25f) < c.transform.position.y + 0.5f)
                        c.Activate(false);
                }

                //Minigame.DebugRect(hitRect, Color.black);
                //var cn = hitRect.center;
                //hitRect.size *= 0.6f;
                //hitRect.center = cn;
                //Minigame.DebugRect(hitRect, Color.black);

            }

            IEnumerator ChangeWindSfx(bool value)
            {
                this.isWindSfx = value;
                var sfxWind = game.assets.audioClipLookup["sfx_abseiling_wind"];
                if(value == true)
                {
                    if(!Audio.instance.IsSfxPlaying(sfxWind))
                        Audio.instance.PlaySfxLoop(sfxWind);
                    yield return Audio.instance.ChangeSfxVolume(sfxWind, startFrom: 0, setTo: 1f, duration: 1f);
                }
                else
                {
                    yield return Audio.instance.ChangeSfxVolume(sfxWind, startFrom: 1f, setTo: 0, duration: 0.25f);
                }
            }

            public void StopHookCheck()
            {
                var test = this.lineCurrent.End + Vector3.down * (circle.radius - 0.1f);

                var stopHookItem = this.game.triggerMap.GetItem<StopHook>(test);

                if(stopHookItem == null)
                {
                    this.stopHook = null;
                }
                else if(stopHookItem.value != this.stopHook)
                {
                    if(
                        (this.dirHorizontal > 0 && test.x > stopHookItem.position.x) ||
                        (this.dirHorizontal < 0 && test.x < stopHookItem.position.x)
                    )
                    {
                        this.transform.position = new Vector3(stopHookItem.position.x, this.transform.position.y);
                        this.stopHook = stopHookItem.value as StopHook;
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_player_grab"]);
                        if(this.state == State.RunIn)
                            SetState(State.StopIn);
                        else if(this.state == State.Run)
                            SetState(State.Stop);
                    }
                }
            }

            // Interpolate line end to target whilst testing for bends
            public void MoveLineEnd(Vector3 target, float stepSize)
            {
                //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.red);

                int breaker = 0;
                var bendFound = false;
                var changed = false;

                while((target - this.lineCurrent.End).sqrMagnitude > 0.001f || bendFound)
                {

                    if(breaker++ > 100)
                    {
                        Debug.LogWarning("Line bend resolve took over 100 steps");
                        break;
                    }

                    bendFound = false;

                    var endPrev = this.lineCurrent.End;
                    this.lineCurrent.End = Vector3.MoveTowards(this.lineCurrent.End, target, stepSize);

                    //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.blue);

                    if(this.lineCurrent.points.Count > 2)
                        if(UnbendCheck() == true)
                            changed = true;

                    var endVector = this.lineCurrent.EndVector();
                    var endVectorLeft = Vector2.Perpendicular(endVector).normalized;
                    {
                        var raycast = RaycastFiltered(this.lineCurrent.BeforeEnd, endVector.normalized, this.filterTile);
                        if(raycast.HasValue && raycast.Value.distance < endVector.magnitude)
                        {
                            //Minigame.DebugX(raycast.Value.point, Color.red, 0.1f);

                            BendLine(raycast.Value, endVectorLeft);
                            bendFound = changed = true;
                            // the bend creates a new line - we must backtrack to test it
                            this.lineCurrent.End = endPrev;

                            //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.red);
                            //Debug.Break();
                        }
                    }
                }

                if(changed)
                {
                    this.joint.connectedAnchor = this.lineCurrent.BeforeEnd;
                    this.joint.distance = this.lineCurrent.EndVector().magnitude;
                    //Debug.Log("UN/BEND");
                }
            }

            public Vector2 Run()
            {
                var raycastDown = RaycastFiltered(this.body.position, Vector2.down, this.filterTile);
                var middleOnFloor = raycastDown.HasValue == true && raycastDown.Value.distance < this.circle.radius + 0.1f;
                if(middleOnFloor == false)
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_player_jump_"+UnityEngine.Random.Range(1, 4)]);
                    
                    SetState(State.Drop);
                    SwingSide(out Vector2 perp, out float swingSide);
                    // can't use body.AddForce because the joint-shrink cancels it
                    if(Mathf.Sign(swingSide) != Mathf.Sign(this.dirHorizontal))
                    {
                        //Debug.Log("JUMP");
                        // jump up and flip around
                        this.joint.distance += 1.5f;
                        this.body.velocity = (perp * this.dirHorizontal * 15f);
                        this.kickLock = this.dirHorizontal;
                        this.timeKick = Time.timeSinceLevelLoad;
                        this.dirHorizontal *= -1f;
                        UpdateFacing();
                    }
                    else
                    {
                        //Debug.Log("LIFT");
                        // shorten rope so we leave the ground
                        this.joint.distance -= 1f;
                        this.kickLock = this.dirHorizontal;
                        this.timeKick = Time.timeSinceLevelLoad;
                        this.body.velocity = (perp * this.dirHorizontal * 10f);
                    }
                }

                // bump raycast (OnCollisionEnter2D can fail to catch wall bump)
                var raycastForwards = RaycastFiltered(this.body.position, Vector2.right * this.dirHorizontal, this.filterTile);
                if
                (
                    (raycastForwards.HasValue == true && raycastForwards.Value.distance < this.circle.radius + 0.02f)
                )
                {
                    this.dirHorizontal *= -1;
                    UpdateFacing();
                }

                return this.lineCurrent.End + WalkSpeed * dirHorizontal * Vector3.right;
            }


            public void BendLine(RaycastHit2D raycast, Vector2 endVectorLeft)
            {
                var cut = raycast.point;
                Vector2[] points;

                Vector2[] boxPoints(BoxCollider2D box)
                {
                    Vector2 extents = box.bounds.extents;
                    Vector2 offset = box.offset;
                    return new[]
                    {
                        offset + new Vector2(extents.x, extents.y),
                        offset + new Vector2(extents.x, -extents.y),
                        offset + new Vector2(-extents.x, -extents.y),
                        offset + new Vector2(-extents.x, extents.y)
                    };
                }

                if(raycast.collider is BoxCollider2D)
                    points = boxPoints((BoxCollider2D)raycast.collider);
                else if(raycast.collider is PolygonCollider2D)
                    points = ((PolygonCollider2D)raycast.collider).points;
                else if(raycast.collider is EdgeCollider2D)
                    points = ((EdgeCollider2D)raycast.collider).points;
                else if(raycast.collider is CircleCollider2D)
                {
                    // the ray passes which side of the center?
                    var circle = (CircleCollider2D)raycast.collider;
                    var centerToCast = cut - ((Vector2)raycast.collider.transform.position + circle.offset);
                    points = new[] { endVectorLeft * Mathf.Sign(Vector2.Dot(centerToCast, endVectorLeft)) * circle.radius };
                }
                else
                    return;

                // move points to transform
                for(int i = 0; i < points.Length; i++)
                    points[i] = raycast.collider.transform.TransformPoint((Vector3)points[i]);

                //Minigame.DebugPolygon(points, Color.red);

                // get the vertex we want to wrap around
                Vector2 vertex = Vector2.zero;
                if(points.Length == 1)
                    vertex = points[0];
                else if(points.Length == 2)
                    vertex = Line.GetNearestPointToLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, points);
                else
                    vertex = Line.GetBendVertex(points, this.lineCurrent.BeforeEnd, this.lineCurrent.End, raycast.point);

                //Minigame.DebugX(vertex, Color.black);

                // walk 8 steps around edge to create a push-out vector
                Vector2 bendVectorSum = Vector2.zero;
                for(int i = 0; i < BendWalk.Length; i++)
                {
                    if(Physics2D.OverlapPoint(vertex + BendWalk[i] * CutPadding * 2, this.filterTile.layerMask) == null)
                        bendVectorSum += BendWalk[i];
                }
                bendVectorSum.Normalize();

                var bendDir = Mathf.Sign(Vector2.Dot(bendVectorSum, endVectorLeft));
                this.lineCurrent.AddBend(bendDir, vertex + bendVectorSum * CutPadding);

                PauseMotor(raycast.collider.transform);
            }

            public bool UnbendCheck()
            {
                var bends = this.lineCurrent.bends;
                if(this.lineCurrent.bendsLocked.ContainsKey(bends.Count - 1))
                    return false;

                var points = this.lineCurrent.points;
                var prevVector = points[^2] - points[^3];
                var dot = Vector2.Dot(Vector2.Perpendicular(prevVector), this.lineCurrent.EndVector());
                if(bends[^1] > 0 == dot > 0)
                {
                    var end = this.lineCurrent.End;
                    bends.RemoveAt(bends.Count - 1);
                    points.RemoveAt(points.Count - 1);
                    this.lineCurrent.End = end;
                    return true;
                }
                return false;
            }

            public void SplitLine(Vector3 newStart)
            {
                var pathRendererCopy = Instantiate(this.lineCurrent.pathRenderer.gameObject, this.lineCurrent.pathRenderer.transform.parent);

                var nextLine = new Line
                {
                    pathRenderer = pathRendererCopy.GetComponent<PathRenderer>(),
                    easeDroop = this.lineCurrent.easeDroop,
                    spriteLine = this.spriteRope
                };
                this.lineCurrent = nextLine;
                this.lines.Add(lineCurrent);
                this.transform.position = newStart;
                this.lineCurrent.points.Add(newStart);
                this.lineCurrent.points.Add(newStart);
            }

            public void EnterPortal(Portal portal)
            {
                (var entry, var exit) = (portal.transform.position, portal.exit.transform.position);
                var endVector = this.lineCurrent.EndVector();
                this.lineCurrent.drawEnd = entry;
                SplitLine(exit);
                this.ropeHooks.Add(portal.exit);
                this.lineCurrent.End += (Vector3)endVector.normalized * 1.5f;
                this.joint.connectedAnchor = this.lineCurrent.BeforeEnd;
                this.joint.distance = 1.5f;
                this.tweenScale.count = 0;
            }

            public void RemoveLastLine()
            {
                Destroy(this.lineCurrent.pathRenderer.gameObject);
                this.lines.RemoveAt(this.lines.Count - 1);
                this.lineCurrent = this.lines[^1];
            }

            public List<Vector2> GetSegmentIntersectionPoints(Vector2 a1, Vector2 b1)
            {
                List<Vector2> result = new List<Vector2>();
                foreach(var line in lines)
                {
                    line.DoAtEachSegment((a2, b2) =>
                    {
                        if(Line.GetLineIntersection(a1, b1, a2, b2, out Vector2 ip) == true)
                        {
                            Minigame.DebugX(ip, Color.white);
                            result.Add(ip);
                        }
                    });
                }
                return result;
            }

            public List<(int, Vector2)> GetLineCuts(Vector2 a1, Vector2 b1)
            {
                List<(int, Vector2)> result = new List<(int, Vector2)>();
                foreach(var line in lines)
                {
                    line.DoAtEachSegment((segment, index) =>
                    {
                        if(Line.GetLineIntersection(a1, b1, segment.a, segment.b, out Vector2 ip) == true)
                        {
                            Minigame.DebugX(ip, Color.white);
                            result.Add((index, ip));
                        }
                    });
                }
                return result;
            }

            public void DoAtEachSegment(Action<Vector2, Vector2> action)
            {
                foreach(var line in lines)
                    line.DoAtEachSegment(action);
            }

            public void UpdateLineRenderers()
            {
                
                for(int i = this.lines.Count - 1; i > 0; i--)
                {
                    lines[i - 1].UpdateLineRenderer(0, this.game.objectLayers.playerSortingLayer);
                }
                // offset lineRenderer to connect to pin on gfx
                this.lineCurrent.drawEnd = this.state switch
                {
                    State.Stop => this.pinRun.position,
                    State.StopIn => this.pinRun.position,
                    State.Run => this.pinRun.position,
                    State.RunIn => this.pinRun.position,
                    State.Win => this.pinRun.position,
                    _ => this.pinHang.position
                };
                var ropeDroop = 0f;
                switch(this.state)
                {
                    case State.Run:
                    case State.RunIn:
                    case State.Stop:
                    case State.StopIn:
                        this.tweenRopeSlack.count += Time.deltaTime;
                        ropeDroop = this.tweenRopeSlack.Value();
                        break;
                    default:
                        this.tweenRopeTighten.count += Time.deltaTime;
                        ropeDroop = this.tweenRopeTighten.Value();
                        break;
                }
                
                this.lineCurrent.UpdateLineRenderer(ropeDroop, this.game.objectLayers.playerSortingLayer, this.state == State.Death);

                if(this.state != State.Death)
                {
                    var endVector = this.lineCurrent.EndVector();
                    this.ropeHooks[^1].SetKnotRotation(this.lineCurrent.GetVector(this.ropeHooks[^1].index));
                    
                    var drawEndNormal = (this.lineCurrent.drawEnd.Value - (Vector2)this.lineCurrent.BeforeEnd).normalized;
                    this.ropeFake.direction = this.state switch
                    {
                        State.KickStick => drawEndNormal,
                        State.Drop => drawEndNormal,
                        _ => Vector2.left * this.dirHorizontal
                    };
                    this.ropeFake.start = this.lineCurrent.drawEnd.Value;
                    this.ropeFake.Advance();

                    if(this.state == State.Drop || this.state == State.KickStick)
                    {
                        this.spriteRenderer.transform.rotation = 
                        this.spriteRendererHead.transform.rotation =
                            Quaternion.Euler(0, 0, 90 + Mathf.Atan2(endVector.y, endVector.x) * Mathf.Rad2Deg);
                    }
                }
                else
                {
                    this.ropeHooks[^1].SetKnotRotation(this.ropeFake.points[1] - this.ropeFake.points[0]);
                    this.ropeFake.Advance();
                }

                this.ropeFakeRenderer.Draw(
                    this.spriteRope,
                    this.ropeFake.points.ToArray(),
                    false,
                    this.game.objectLayers.playerSortingLayer,
                    1f
                );

                if(this.ropeFakeCut != null)
                {
                    this.ropeFakeCut.Advance();
                    this.ropeFakeCutRenderer.Draw(
                        this.spriteRope,
                        this.ropeFakeCut.points.ToArray(),
                        false,
                        this.game.objectLayers.playerSortingLayer,
                        1f
                    );
                }

            }

            public void SetRopeColor(Color color)
            {
                this.colorRope = color;
                this.ropeHooks[^1].SetColor(this.colorRope);
                this.ropeRenderer.GetComponent<MeshRenderer>().material.SetColor("_Color", this.colorRope);
                this.ropeFakeRenderer.GetComponent<MeshRenderer>().material.SetColor("_Color", this.colorRope);
                this.ropeEnd.GetComponent<SpriteRenderer>().color =
                    this.ropeEndCutTop.GetComponent<SpriteRenderer>().color = this.colorRope;
            }

            public void UpdateFacing()
            {
                this.spriteRenderer.flipX =
                    this.spriteRendererArms.flipX =
                    this.spriteRendererHead.flipX = this.dirHorizontal < 0;
            }

            private void CreateLandDust(Vector2 position)
            {
                for(var n = 0; n < 10; n += 1)
                {
                    float horizontalSpeed =
                        ((n % 2 == 0) ? 1 : -1) * UnityEngine.Random.Range(0.4f, 2.3f) / 16;
                    float verticalSpeed = UnityEngine.Random.Range(-0.2f, 0.2f) / 16;

                    var p = Particle.CreateAndPlayOnce(
                        this.game.assets.animationLookup["dust_land"],
                        position,
                        this.game.transform
                    );
                    p.velocity = new Vector2(horizontalSpeed, verticalSpeed);
                    p.acceleration = p.velocity * -0.05f / 16;
                    p.animated.playbackSpeed = UnityEngine.Random.Range(0.8f, 1.2f);
                    p.spriteRenderer.sortingLayerName = this.game.objectLayers.fxSortingLayer;
                }
            }

            private void AdvanceRunDust()
            {
                // don't create dust over side fill
                if(this.transform.position.x < 0 || this.transform.position.x > this.game.map.width)
                    return;

                if(this.timeSinceDustParticle <= 0)
                {
                    this.timeSinceDustParticle = UnityEngine.Random.Range(1, 4);
                    Particle p = Particle.CreateAndPlayOnce(
                        this.game.assets.animationLookup["GLOBAL:dust"],
                        this.transform.position + this.circle.radius * Vector3.down +
                        new Vector3(
                            UnityEngine.Random.Range(-0.2f, 0.2f),
                            UnityEngine.Random.Range(-0.2f, 0.2f)
                        ),
                        this.game.transform
                    );
                    float s = UnityEngine.Random.Range(0.5f, 1.0f);
                    p.transform.localScale = new Vector3(s, s, 1);

                    p.velocity = new Vector2(
                        UnityEngine.Random.Range(-0.1f, 0.1f),
                        UnityEngine.Random.Range(0, 0.2f)
                    ) / 16;
                    p.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                    p.spriteRenderer.sortingOrder = this.game.spriteRendererPool.sortingOrderCount++;
                }
                else
                {
                    this.timeSinceDustParticle -= 1;
                }
            }

            public static RaycastHit2D? RaycastFiltered(Vector2 origin, Vector2 direction, ContactFilter2D filter)
            {
                var results = new List<RaycastHit2D>();
                int total = Physics2D.Raycast(origin, direction, filter, results);
                if(total > 0)
                    return results[0];
                return null;
            }

            public static RaycastHit2D? CircleCastFiltered(Vector2 origin, float radius, Vector2 direction, ContactFilter2D filter)
            {
                var results = new List<RaycastHit2D>();
                int total = Physics2D.CircleCast(origin, radius, direction, filter, results);
                if(total > 0)
                    return results[0];
                return null;
            }

            public static float RaycastFilteredDist(Vector2 origin, Vector2 direction, ContactFilter2D filter)
            {
                var cast = RaycastFiltered(origin, direction, filter);
                if(cast.HasValue) return cast.Value.distance;
                return float.MaxValue;
            }


            public void PauseMotor(Transform t)
            {
                var m = this.game.GetMotor(t);
                if(m != null && m.paused == false)
                {
                    m.paused = true;
                    this.motorsPaused.Add(m);
                }
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                PauseMotor(collision.transform);
                var contact = collision.GetContact(0);
                // debug contact point
                //game.spriteRendererPool.Get(contact.point, game.assets.spriteLookup["dot"], "Default", "dot", game.transform);
                switch(this.state)
                {
                    case State.Drop:
                        //Debug.Log($"Collide {contact.normal} {this.dirHorizontal}");
                        // is floor?
                        if(contact.normal == Vector2.up)
                        {
                            SetState(State.Run);
                            break;
                        }
                        // is outside level?
                        if(contact.collider.name.Contains("WallEdge"))
                            break;

                        SwingSide(out Vector2 perp, out float swingSide);

                        if(Mathf.Sign(contact.normal.x) != Mathf.Sign(this.dirHorizontal))
                        {
                            this.kickContact = contact;
                            if(Mathf.Sign(swingSide) == Mathf.Sign(this.dirHorizontal) || Mathf.Abs(swingSide) > 0.85f)
                                SetState(State.KickStick);
                            else
                                KickOut();
                        }
                        else
                        {
                            //Debug.Log($"CAUGHT {dirHorizontal} {swingSide}");
                            if(Mathf.Sign(this.dirHorizontal) != Mathf.Sign(swingSide))
                            {
                                this.dirHorizontal *= -1f;
                                this.kickContact = contact;
                                SetState(State.KickStick);
                                UpdateFacing();
                            }
                        }
                        break;
                    case State.Run:
                        if(contact.normal == new Vector2(-this.dirHorizontal, 0))
                        {
                            this.dirHorizontal *= -1;
                            UpdateFacing();
                        }
                        break;
                }
            }

            private void KickOut()
            {
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_player_jump_wall_" + UnityEngine.Random.Range(1, 3)]);

                var endVector = this.lineCurrent.EndVector();
                var power = KickPowerBase;
                power -= endVector.magnitude * KickPowerDecPerTile;
                power = Mathf.Max(power, KickPowerMin);

                this.body.velocity = this.kickContact.normal * power;
                this.timeKick = Time.timeSinceLevelLoad;
                this.timeKickDust = Time.timeSinceLevelLoad;
                this.kickLock = this.dirHorizontal;

                var anims = AnimationsForCharacter(this.game.character);
                this.animated.PlayOnce(anims.animationSwingKick);
                if(this.game.character == Character.King)
                    this.animatedHead.PlayOnce(kingHat.animationSwingKick);
                else if(this.game.character == Character.Puffer)
                    this.animatedHead.PlayOnce(pufferHead.animationSwingKick);

                this.game.AddDustCloud(this.kickContact.point, 0.75f, 3, this.game.objectLayers.fxSortingLayer, -1, "GLOBAL:dust_big");
                this.game.AddDustCloud(this.kickContact.point, 0.5f, 5, this.game.objectLayers.fxSortingLayer);

                //Debug.Log($"KICK {power}");
            }


            public void DebugUpdateCharacter() {
                var anims = AnimationsForCharacter(this.game.character);

                this.animated.PlayAndLoop(anims.animationIdle);
                if(this.game.character == Character.King)
                    this.animatedHead.PlayAndLoop(kingHat.animationIdle);
                else if(this.game.character == Character.Puffer)
                    this.animatedHead.PlayAndLoop(pufferHead.animationIdle);
            }

        }

    }
}
