﻿using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class Portal : RopeHook
        {
            public Portal exit;
            public bool locked = false;// no infinite loops
            public SpriteRenderer[] spriteRenderers;

            public void Init(AbseilingGame game, Portal portalExit)
            {
                this.game = game;
                this.exit = portalExit;
                portalExit.exit = this;
                Destroy(portalExit.GetComponent<CircleCollider2D>());
                portalExit.transform.localScale *= 0.8f;
            }

            void Update()
            {
                if(this.game == null || Minigame.paused == true)
                    return;
                
                if(this.game.player.state == Player.State.RunIn)
                    this.locked = false;
                
                for(int i = 0; i < this.spriteRenderers.Length; i++)
                {
                    var sr = this.spriteRenderers[i];
                    var angles = sr.transform.localEulerAngles;
                    sr.transform.localEulerAngles = angles + new Vector3(0, 0, (i + 1) * (exit == null ? 1 : -1));
                    sr.transform.localScale = Vector3.one + (Vector3)Shake.WaveFlex(new Vector2(1, 1), new Vector2(-1, -1), Minigame.fixedDeltaTimeElapsed + i * 0.4f, 3f, 0.2f + i * 0.05f);
                }
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.locked) return;

                var player = collision.GetComponent<Player>();
                if(player == null) return;

                player.EnterPortal(this);
                this.locked = true;
                player.body.simulated = false;
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_abseiling_portal"]);
                this.game.camCtrl.PauseAndPanTo(this.exit.transform.localPosition, 0.5f, 0.02f, 0.15f, () => { player.body.simulated = true; });
            }

        }

    }
}
