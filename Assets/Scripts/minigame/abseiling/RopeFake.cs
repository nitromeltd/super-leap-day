using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class RopeFake
        {
            public List<Vector2> points;
            public Vector2 direction;
            public Transform ropeStart;
            public Transform ropeEnd;
            public float stiffness = 0.5f;
            public float segmentLength;

            public Vector2 start
            {
                get => points[0];
                set => points[0] = value;
            }
            public Vector2 end => points[^1];

            private const float Gravity = 0.15f;

            public RopeFake(Vector2 start, float segmentLength, int length, Transform ropeEnd)
            {
                this.segmentLength = segmentLength;
                this.ropeEnd = ropeEnd;
                this.points = new List<Vector2>(length);
                for(int i = 0; i < length; i++)
                    this.points.Add(start + Vector2.left * i * segmentLength);
            }
            public RopeFake(List<Vector2> points, float segmentLength, Transform ropeStart, Transform ropeEnd)
            {
                this.points = points;
                this.segmentLength = segmentLength;
                this.ropeStart = ropeStart;
                this.ropeEnd = ropeEnd;
            }

            public void Advance()
            {
                var dir = this.direction;
                var g = Gravity;

                if(this.ropeStart != null)
                {
                    g *= 2f;
                    points[0] += Vector2.down * g;
                }

                for(int i = 1; i < points.Count; i++)
                {
                    (var a, var b) = (points[i - 1], points[i]);
                    // drop
                    b += Vector2.down * g;
                    // constrain length and use previous normal to constrain angle

                    var normal = (b - a).normalized;
                    dir = Vector2.Lerp(normal, dir, stiffness);
                    b = a + dir * segmentLength;
                    points[i] = b;

                    //var delta = b - a;
                    //var len = delta.magnitude;
                    //delta /= len;
                    //delta += dir;
                    //delta.Normalize();
                    //b = a + delta * segmentLength;
                    //points[i] = b;
                    //dir = delta;
                }
                this.ropeEnd.position = end;
                this.ropeEnd.localRotation = Quaternion.Euler(0, 0, 180 + Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg);

                if(this.ropeStart)
                {
                    this.ropeStart.position = start;
                    this.ropeStart.localRotation = Quaternion.Euler(0, 0, 180 + Mathf.Atan2(this.direction.y, this.direction.x) * Mathf.Rad2Deg);
                }

                //for(int i = 1; i < points.Count; i++)
                //{
                //    Debug.DrawLine(points[i], points[i - 1], Color.red);
                //}
            }
        }
    }
}
