using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class RopeHook : MonoBehaviour
        {
            [System.NonSerialized] public AbseilingGame game;
            [System.NonSerialized] public TriggerButton button;
            [System.NonSerialized] public int index;

            public SpriteRenderer spriteRenderer;
            public Animated animatedWinch;
            public bool isWinch;
            public Transform ropeKnot;
            public int r;
            public bool flip;
            public Vector2 position => this.transform.position;

            public void Init(AbseilingGame game, NitromeEditor.TileLayer.Tile tile)
            {
                this.game = game;
                ObjectLayers.SetSortingLayer(this.gameObject, this.game.objectLayers.playerSortingLayer);
                this.spriteRenderer.sortingLayerName = this.game.objectLayers.tilesSortingLayer;

                if(tile.tile == "start_hook")
                {
                    // flip start hook handle?
                    if(Physics2D.OverlapPoint(transform.position + Vector3.up + Vector3.left) == null)
                        this.spriteRenderer.flipX = true;

                }
                else if(tile.tile == "rope_hook_winch")
                {
                    spriteRenderer.transform.rotation = Quaternion.Euler(0, 0, tile.rotation);
                }
                else
                {
                    if(tile.rotation == 90)
                        spriteRenderer.flipX = true;
                    else if(tile.rotation == 180)
                    {
                        spriteRenderer.sprite = game.assets.spriteLookup["corner hook down"];
                        spriteRenderer.flipX = true;
                    }
                    else if(tile.rotation == 270)
                        spriteRenderer.sprite = game.assets.spriteLookup["corner hook down"];
                }
                if(tile.flip == true)
                    spriteRenderer.flipX = !spriteRenderer.flipX;
            }

            public void Attach(int index)
            {
                this.index = index;
                this.ropeKnot.gameObject.SetActive(true);
                this.game.ParticleCreateAndPlayOnce("smack", this.transform.position);
                if(this.isWinch == true && this.game.player.joint.distance > Player.WinchLengthMin)
                {
                    Winch(true);
                }
            }

            public void Winch(bool value)
            {
                if(value == true)
                {
                    this.animatedWinch.PlayAndLoop(this.game.assets.animationLookup["winch"]);
                    this.game.player.winchIndex = this.index;
                    Audio.instance.PlaySfxLoop(this.game.assets.audioClipLookup["sfx_abseiling_winch"]);
                }
                else
                {
                    this.animatedWinch.Stop();
                    this.button?.UnHit();
                    Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["sfx_abseiling_winch"]);
                }
            }

            public void SetColor(Color color)
            {
                var srs = this.ropeKnot.GetComponentsInChildren<SpriteRenderer>();
                foreach(var sr in srs)
                    sr.color = color;
            }

            public void Restart()
            {
                SetColor(Color.white);
                this.ropeKnot.gameObject.SetActive(false);
            }

            public void SetKnotRotation(Vector2 vector)
            {
                this.ropeKnot.transform.rotation = Quaternion.Euler(0, 0, 90 + Mathf.Atan2(vector.y, vector.x) * Mathf.Rad2Deg);
            }
        }

    }
}
