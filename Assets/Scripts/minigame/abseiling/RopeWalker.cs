using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame {
    namespace abseiling {
        public class RopeWalker : MonoBehaviour
        {
            [System.NonSerialized] public AbseilingGame game;

            public SpriteRenderer spriteRenderer;
            public Animated animated;
            public Animated.Animation animIdle;
            public Animated.Animation animCrawl;
            public Animated.Animation animDeath;

            public Sprite[] spriteStarts;

            public Vector3 initPos;
            public Line line;
            public float length;

            public const float SpeedCrawl = 0.05f;
            public const float DistToKillPlayer = 1f;
            public bool IsReady() => this.line == null && this.spriteRenderer.enabled == true;

            public void Init(AbseilingGame game)
            {
                this.game = game;
                this.initPos = this.transform.position;
                this.animated.PlayAndLoop(this.animIdle);
                this.length = 0;

                var sprite = this.spriteStarts[1];
                if(this.initPos.x > game.map.width / 2)
                    sprite = this.spriteStarts[0];

                this.game.spriteRendererPool.Get(this.initPos, sprite, this.game.objectLayers.backgroundSortingLayer, "rope_walker_start", this.game.map.transformBackground);
            }   

            // Update is called once per frame
            void FixedUpdate()
            {
                if(this.line != null)
                {
                    if(this.game.player.state == Player.State.RunIn || this.game.player.state == Player.State.Death)
                        Death();
                    else
                    {
                        this.length += SpeedCrawl;
                        var len = this.line.GetSegmentLengthRemaining(this.length, out int index, out float segmentLength);

                        if(index >= this.line.points.Count - 2 && len <= DistToKillPlayer)
                        {
                            if(this.line == this.game.player.lineCurrent)
                                HitPlayer();
                            else
                                Death();
                        }
                        else if(this.line.bendsLocked.ContainsKey(index - 1) && segmentLength - len < DistToKillPlayer)
                            Death();
                        else if(this.game.player.state == Player.State.Win)
                            Death();
                    }
                }

                if(this.spriteRenderer.enabled == false)
                {
                    if(this.game.player.state == Player.State.RunIn || this.game.player.state == Player.State.Death)
                        Restart();
                }
                
                if(this.spriteRenderer.color.a < 1)
                    this.spriteRenderer.color += new Color(0, 0, 0, 0.01f);
            }

            private void LateUpdate()
            {
                if(this.line != null)
                {
                    this.transform.position = this.line.GetPositionAtLength(this.length, out Vector2 delta);
                    this.transform.rotation = Quaternion.Euler(0, 0, 90 + Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg);
                }
            }

            public void Restart()
            {
                this.line = null;
                this.spriteRenderer.enabled = true;
                this.transform.position = this.initPos;
                this.animated.PlayAndLoop(this.animIdle);
                this.spriteRenderer.sortingLayerName = this.game.objectLayers.objectsSortingLayer;
                this.spriteRenderer.color = new Color(1, 1, 1, -0.5f);
                this.transform.localRotation = Quaternion.identity;
            }

            public void Death()
            {
                this.line = null;
                this.game.AddDebris("crawler_fall", this.transform.position);
                this.spriteRenderer.enabled = false;
                Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["sfx_abseiling_rope_crawler_crawl"]);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_abseiling_rope_crawler_death"]);
            }

            public void HitPlayer() {
                this.game.player.SetState(Player.State.Death);
                this.game.AddDebris("crawler_hit", this.transform.position);
                Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["sfx_abseiling_rope_crawler_crawl"]);
                Restart();
            }

            public void Attach(Line line, int index, float distAlongSegment) {

                if(this.spriteRenderer.enabled == false)
                    return;

                this.line = line;
                this.length = line.Length(index + 2) + distAlongSegment;
                this.animated.PlayAndLoop(animCrawl);
                this.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                this.spriteRenderer.sortingOrder = this.game.player.spriteRenderer.sortingOrder - 1;
                Audio.instance.PlaySfxLoop(this.game.assets.audioClipLookup["sfx_abseiling_rope_crawler_crawl"]);
            }

        }

    }

}
