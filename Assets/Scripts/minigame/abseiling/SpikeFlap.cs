using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class SpikeFlap : MonoBehaviour
        {

            [System.NonSerialized] public AbseilingGame game;
            
            public bool extended;
            public bool isOffset;
            public SpikedItem spikedItem;
            public SpriteRenderer spriteRenderer;
            public SpriteRenderer spriteRendererLight;
            public BoxCollider2D trigger;
            public bool locked;

            public Animated animated;
            public Animated animatedLight;

            public Animated.Animation animIdle;
            public Animated.Animation animExtend;
            public Animated.Animation animRetract;
            public Animated.Animation animLightToOn;
            public Animated.Animation animLightToOff;

            public Sprite spriteLocked;
            public Sprite spriteLightOn;
            public Sprite spriteLightOff;

            private float time;
            private float delayIn;
            private float delayTotal;

            private Vector2Int spikeCell;
            private int indexSpikeDir;

            private const float DelayWarning = 1f;

            public void Init(Vector2Int pos, NitromeEditor.TileLayer.Tile tile, AbseilingGame game)
            {
                this.game = game;
                this.spikedItem.game = game;
                this.isOffset = tile.offsetX != 8 || tile.offsetX != 8;

                this.delayIn = tile.properties["in"].f;
                var delayOut = tile.properties["out"].f;
                this.time = tile.properties["offset"].f;
                this.delayTotal = this.delayIn + delayOut;
                this.time %= this.delayTotal;
                this.extended = this.time > this.delayIn;

                if(this.isOffset == true)
                {
                    this.spikedItem.transform.localRotation = Quaternion.Euler(0, 0, tile.rotation);
                    this.spikedItem.gameObject.SetActive(this.extended);
                }
                else
                    this.spikedItem.gameObject.SetActive(false);

                this.indexSpikeDir = tile.flip ? 2 : 4;
                var dir = AbseilingGame.SpikeIndexToDir(this.indexSpikeDir).normalized;
                this.spikeCell = pos + new Vector2Int((int)dir.x, (int)dir.y);

                SetExtended(this.extended, false);
            }

            public void Lock()
            {
                if(this.extended == false)
                {
                    this.locked = true;
                    this.animated.enabled = false;
                    this.animatedLight.enabled = false;
                    this.spriteRenderer.sprite = this.spriteLocked;
                    this.spriteRendererLight.sprite = this.spriteLightOff;
                    this.trigger.enabled = false;

                    var p = transform.position + new Vector3((this.indexSpikeDir == 4 ? -0.5f : 0.5f), -0.6f);

                    for(float y = 0f; y < 1.1f; y += UnityEngine.Random.Range(0.2f, 0.3f))
                    {
                        var particle = this.game.ParticleCreateAndPlayOnce("GLOBAL:dust", p + y * Vector3.up, this.game.transform);
                        particle.spriteRenderer.sortingLayerName = game.objectLayers.fxSortingLayer;
                    }
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_abseiling_spike_flap_lock"]);

                }
            }

            public void SetExtended(bool value, bool animate)
            {
                if(this.isOffset == true)
                {
                    this.spikedItem.gameObject.SetActive(value);
                }
                else
                {
                    if(value == true)
                        this.game.spikesLookup[this.spikeCell] = this.indexSpikeDir;
                    else
                        this.game.spikesLookup.Remove(this.spikeCell);
                }
                if(animate == true)
                {
                    if(
                        (this.game.IsComplete() || this.game.IsFailed()) == false &&
                        this.game.camCtrl.OnScreen(this.transform.position, Vector2.one) == true
                    )
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_retractable_spike"]);

                    if(value == false)
                    {
                        this.animated.PlayOnce(animRetract);
                        this.animatedLight.PlayOnce(animLightToOff);
                    }
                }
                else
                {
                    if(value == true)
                    {
                        this.spriteRenderer.sprite = animExtend.sprites[^1];
                        this.spriteRendererLight.sprite = this.spriteLightOn;
                    }
                    else
                    {
                        this.animated.PlayOnce(animIdle);
                        this.spriteRendererLight.sprite = this.spriteLightOff;

                    }
                }
                this.extended = value;
            }

            private void FixedUpdate()
            {
                if(this.locked == true)
                    return;

                this.time = (this.time + Time.fixedDeltaTime) % this.delayTotal;
                if(this.time > this.delayIn)
                {
                    if(this.extended == false)
                        ToggleExtended();
                }
                else
                {
                    if(this.extended == true)
                        ToggleExtended();

                    if(
                        this.time > this.delayIn - DelayWarning &&
                        this.animatedLight.currentAnimation != this.animLightToOn
                    )
                    {
                        this.animatedLight.PlayAndHoldLastFrame(this.animLightToOn);
                    }
                    else if
                    (
                        this.time > this.delayIn - 0.5f &&
                        this.animated.currentAnimation != this.animExtend
                    )
                    {
                        this.animated.PlayAndHoldLastFrame(animExtend);
                    }
                }
            }

            public void ToggleExtended()
            {
                SetExtended(!this.extended, true);
            }
        }
    }
}
