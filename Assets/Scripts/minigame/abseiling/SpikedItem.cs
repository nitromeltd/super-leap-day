using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class SpikedItem : MonoBehaviour
        {
            [System.Serializable]
            public class AB
            {
                public Transform a, b;
            }

            public AbseilingGame game;
            public List<AB> spikes;

            public void FixedUpdate()
            {
                Advance();
            }

            public virtual void Advance()
            {
                if(this.game.player.state == Player.State.Drop || this.game.player.state == Player.State.Drop)
                {
                    foreach(var ab in spikes)
                    {
                        var a = ab.a.position;
                        var b = ab.b.position;

                        var cuts = this.game.player.GetLineCuts(a, b);

                        if(cuts.Count > 0)
                        {
                            this.game.player.deathCut = cuts[0];
                            this.game.player.SetState(Player.State.Death);
                            break;
                        }

                    }
                }

            }

        }
    }
}
