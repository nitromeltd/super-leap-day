using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class SpikesForPlayer : MonoBehaviour
        {
            [System.NonSerialized] public AbseilingGame game;

            public SpriteRenderer spriteRenderer;
            public Animated animated;
            public Animated.Animation animFly;
            public Animated.Animation animTurn;
            public float wavelengthBob = 2f;
            public float magnitudeBob = 0.2f;
            public Animated.Animation animExplosion;

            private Transform transformBob;
            private Vector3 startBob;
            private float offsetBob;
            private Vector3 prevPos;

            public void Init(AbseilingGame game)
            {
                this.game = game;
                this.animated.PlayAndLoop(this.animFly);
                this.transformBob = animated.transform;
                this.startBob = this.transformBob.localPosition;
                this.offsetBob = Random.value * this.wavelengthBob;
                this.prevPos = transform.position;
            }

            private void FixedUpdate()
            {
                if(this.transformBob != null)
                {
                    this.transformBob.localPosition = this.startBob + Shake.Wave(
                        Time.timeSinceLevelLoad + this.offsetBob,
                        this.wavelengthBob,
                        this.magnitudeBob
                    ) * Vector3.up;
                }
            }

            private void Update()
            {
                if
                (
                    (this.transform.position.x > this.prevPos.x && this.spriteRenderer.flipX == false)
                    ||
                    (this.transform.position.x < this.prevPos.x && this.spriteRenderer.flipX == true)
                )
                {
                    this.spriteRenderer.flipX = !this.spriteRenderer.flipX;
                    this.animated.PlayOnce(this.animTurn, () => {
                        this.animated.PlayAndLoop(this.animFly);
                    });
                }

                this.prevPos = this.transform.position;


                if(this.spriteRenderer.enabled == false)
                {
                    if(this.game.player.state == Player.State.RunIn || this.game.player.state == Player.State.Death)
                        Restart();
                }

                if(this.spriteRenderer.color.a < 1)
                    this.spriteRenderer.color += new Color(0, 0, 0, 0.01f);
            }

            public void Restart()
            {
                this.spriteRenderer.enabled = true;
                this.spriteRenderer.color = new Color(1, 1, 1, -0.5f);
            }

            private void OnTriggerEnter2D(Collider2D collision)
            {
                var player = collision.GetComponent<Player>();
                if(player != null)
                {
                    player.SetState(Player.State.Death);
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_explosion"]);
                    this.spriteRenderer.enabled = false;
                    var p = this.game.ParticleCreateAndPlayOnce("GLOBAL:enemy_death_2", this.transform.position, this.game.transform);
                    p.transform.localScale *= 1.5f;
                }
            }

        }
    }
}
