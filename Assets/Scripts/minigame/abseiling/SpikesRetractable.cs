using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class SpikesRetractable : MonoBehaviour
        {

            public AbseilingGame game;
            public bool extended;
            public bool isOffset;
            public SpikedItem spikedItem;

            public SpriteRenderer spriteRenderer;
            public Animated animated;
            public Sprite[] spritesHoriz;
            public Sprite[] spritesVert;

            [HideInInspector] public Animated.Animation animExtend;
            [HideInInspector] public Animated.Animation animRetract;

            private Vector2Int spikeCell;
            private int indexSpikeDir;

            public void SetExtended(bool value, bool animate)
            {
                if(this.isOffset == true)
                {
                    this.spikedItem.gameObject.SetActive(value);
                }
                else
                {
                    if(value == true)
                        this.game.spikesLookup[this.spikeCell] = this.indexSpikeDir;
                    else
                        this.game.spikesLookup.Remove(this.spikeCell);
                }
                if(animate == true)
                {
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_retractable_spike"]);
                    if(value == true)
                        this.animated.PlayOnce(animExtend);
                    else
                        this.animated.PlayOnce(animRetract);
                }
                else
                {
                    if(value == true)
                        this.spriteRenderer.sprite = animExtend.sprites[^1];
                    else
                        this.spriteRenderer.sprite = animExtend.sprites[0];
                }
                this.extended = value;
            }

            public void ToggleExtended()
            {
                SetExtended(!this.extended, true);
            }

            public void Init(Vector2Int pos, NitromeEditor.TileLayer.Tile tile, AbseilingGame game)
            {
                this.game = game;
                this.spikedItem.game = game;
                this.isOffset = tile.offsetX != 8 || tile.offsetX != 8;
                this.extended = tile.tile == "SpikesRetractable_out" ? true : false;
                if(this.isOffset == true)
                {
                    this.spikedItem.transform.localRotation = Quaternion.Euler(0, 0, tile.rotation);
                    this.spikedItem.gameObject.SetActive(this.extended);
                }
                else
                    this.spikedItem.gameObject.SetActive(false);
                this.indexSpikeDir = AbseilingGame.SpikeRotationToDir(tile.rotation);
                var dir = AbseilingGame.SpikeIndexToDir(this.indexSpikeDir).normalized;
                this.spikeCell = pos + new Vector2Int((int)dir.x, (int)dir.y);
                var spritesVertList = new List<Sprite>(spritesVert);
                var spritesHorizList = new List<Sprite>(spritesHoriz);
                spritesVertList.Reverse();
                spritesHorizList.Reverse();
                switch(this.indexSpikeDir)
                {
                    case 1:
                    case 3:
                        animExtend.sprites = spritesVert;
                        animRetract.sprites = spritesVertList.ToArray();
                        if(this.indexSpikeDir == 3)
                        {
                            this.spriteRenderer.flipY = true;
                        }
                        break;
                    case 2:
                    case 4:
                        animExtend.sprites = spritesHoriz;
                        animRetract.sprites = spritesHorizList.ToArray();
                        if(this.indexSpikeDir == 2)
                        {
                            this.spriteRenderer.flipX = true;
                        }
                        break;
                }
                SetExtended(this.extended, false);
            }
        }
    }
}
