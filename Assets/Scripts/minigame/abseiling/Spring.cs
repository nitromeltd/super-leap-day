using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class Spring : MonoBehaviour
        {

            public BoxCollider2D box;
            public Animated animated;
            public Animated.Animation anim;
            public Vector2 dir = Vector2.right;

            private float timeHit;

            private void OnTriggerEnter2D(Collider2D collision)
            {
                if(Time.timeSinceLevelLoad - this.timeHit > 0.2f && collision.name.Contains("Player"))
                {
                    var player = collision.GetComponent<Player>();
                    if(player != null)
                    {
                        this.timeHit = Time.timeSinceLevelLoad;
                        this.animated.PlayOnce(anim);
                        player.body.velocity = dir * Player.KickPowerBase * 1.5f;
                        player.dirHorizontal = -this.dir.x;
                        player.UpdateFacing();
                        player.timeKick = Time.timeSinceLevelLoad;
                        player.kickLock = player.dirHorizontal;
                        Audio.instance.PlaySfx(player.game.assets.audioClipLookup["sfx_golf_spring_weak"]);
                    }
                }
            }


        }

    }
}
