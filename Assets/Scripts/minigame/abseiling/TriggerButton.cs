using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        public class TriggerButton : MonoBehaviour
        {
            public AbseilingGame game;
            public List<Motor> motors = new List<Motor>();
            public BoxCollider2D trigger;
            public SpriteRenderer spriteRenderer;
            public List<Vector2Int> targets = new List<Vector2Int>();

            public Sprite spriteUp;
            public Sprite spriteDown;

            public void AddMotor(Motor m)
            {
                this.motors.Add(m);
                m.paused = true;
            }


            void OnTriggerEnter2D(Collider2D collision)
            {
                Hit();
            }

            public void UnHit()
            {
                this.spriteRenderer.sprite = this.spriteUp;
                this.trigger.enabled = true;
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_switch_disengage"]);
            }

            public void Hit()
            {
                if(this.game != null)
                {
                    if(this.game.player.state == Player.State.Death) return;
                    if(this.game.IsComplete()) return;// don't trigger events during end game

                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_switch_engage"]);
                    if(this.motors.Count > 0)
                    {
                        this.spriteRenderer.sprite = spriteDown;

                        var center = Vector3.zero;
                        foreach(var m in this.motors)
                        {
                            m.paused = false;
                            m.isLookAtTween = true;
                            center += m.transform.localPosition;
                        }
                        center /= this.motors.Count;
                        this.game.player.body.simulated = false;
                        var lookAtTween = new CamCtrl.LookAtTween(
                            this.game.camCtrl, this.game.cam.transform.localPosition, center, this.game.IsMotorLookAtTweenFinished, 0.5f, 0.5f, true, () =>
                            {
                                this.game.player.body.simulated = this.game.player.state switch
                                {
                                    Player.State.Drop => true,
                                    Player.State.Run => true,
                                    _ => false
                                };
                            }
                        );
                        lookAtTween.lookStartAction = () => { Audio.instance.PlaySfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]); };
                        lookAtTween.lookFinishAction = () => {
                            Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]);
                            motors.Clear();
                        };
                        this.game.camCtrl.AddLookAtTween(lookAtTween);
                    }

                    foreach(var pos in this.targets)
                    {
                        if(this.game.map.vector2IntToGameObject.TryGetValue(pos, out GameObject obj) == true)
                        {
                            if(obj.name.Contains("Spikes") == true)
                                obj.GetComponent<SpikesRetractable>().ToggleExtended();
                            else if(obj.name.Contains("rope_hook_winch") == true)
                                obj.GetComponent<RopeHook>().Winch(true);
                        }
                    }

                    this.spriteRenderer.sprite = this.spriteDown;
                    this.trigger.enabled = false;
                }

            }

        }
    }
}
