using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace abseiling
    {
        // Testing a generic version of the PickupMap

        public class TriggerMap
        {
            public Dictionary<Vector2Int, List<Item>> map = new Dictionary<Vector2Int, List<Item>>();

            public class Item
            {
                public object value;
                public Vector2 position;
                public Vector2 size;

                public void Iterate(System.Action<Vector2Int> perCell)
                {
                    int xMin = Mathf.FloorToInt(position.x - size.x * 0.5f);
                    int yMin = Mathf.FloorToInt(position.y - size.y * 0.5f);
                    int xMax = Mathf.CeilToInt(position.x + size.x * 0.5f);// exclusive
                    int yMax = Mathf.CeilToInt(position.y + size.y * 0.5f);// exclusive

                    for(int r = yMin; r < yMax; r++)
                        for(int c = xMin; c < xMax; c++)
                        {
                            perCell(new Vector2Int(c, r));
                            Minigame.DebugRect(new Rect(c, r, 1, 1), Color.yellow);
                        }
                }
            }

            public void DebugDraw()
            {
                foreach(var kv in map)
                    foreach(var item in kv.Value)
                    {
                        Debug.DrawLine(item.position, (Vector2)kv.Key, Color.yellow);
                        Minigame.DebugRect(new Rect(item.position - item.size * 0.5f, item.size), Color.cyan);
                    }

            }


            public Item Add(object value, Vector2 position, Vector2 size)
            {
                var item = new Item
                {
                    value = value,
                    position = position,
                    size = size
                };
                void AddCell(Vector2Int p)
                {
                    this.map.TryGetValue(p, out List<Item> list);
                    if(list == null) list = new List<Item>();
                    list.Add(item);
                    this.map[p] = list;
                }
                item.Iterate(AddCell);
                return item;
            }

            public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
            {
                return Quaternion.Euler(angles) * (point - pivot) + pivot;
            }

            public Item Add(object value, BoxCollider2D boxCollider2D, Transform parent = null)
            {
                parent ??= boxCollider2D.transform;
                var position = boxCollider2D.transform.position + (Vector3)boxCollider2D.offset;
                position = parent.rotation * (position - parent.position) + parent.position;
                var angle = boxCollider2D.transform.rotation.eulerAngles.z;
                var size = boxCollider2D.size;
                if(angle == 90 || angle == 270 || angle == -90) (size.x, size.y) = (size.y, size.x);
                return Add(value, position, size);
            }

            public Item Add(object value, GameObject obj) =>
                Add(value, obj.transform.position, Vector2.one);

            public Item Add<T>(GameObject obj) =>
                Add(obj.GetComponent<T>(), obj.transform.position, Vector2.one);

            public void Remove(Item item)
            {
                void RemoveCell(Vector2Int p)
                {
                    this.map.TryGetValue(p, out List<Item> list);
                    if(list != null)
                    {
                        list.Remove(item);
                        if(list.Count == 0) map.Remove(p);
                    }
                }
                item.Iterate(RemoveCell);
            }

            public bool TryGet<T>(Vector2Int p, out T value)
            {
                if(this.map.TryGetValue(p, out List<Item> list) == true)
                    foreach(var item in list)
                        if(item.value is T)
                        {
                            value = (T)item.value;
                            return true;
                        }
                value = default(T);
                return false;
            }

            public T Get<T>(Vector2Int p)
            {
                if(this.map.TryGetValue(p, out List<Item> list) == true)
                    foreach(var item in list)
                        if(item.value is T)
                            return (T)item.value;
                
                return default(T);
            }

            public Item GetItem<T>(Vector2Int p)
            {
                if(this.map.TryGetValue(p, out List<Item> list) == true)
                    foreach(var item in list)
                        if(item.value is T)
                            return item;

                return null;
            }

            public T Get<T>(Vector2 position) =>
                Get<T>(new Vector2Int((int)position.x, (int)position.y));

            public bool TryGet<T>(Vector2 position, out T value)
            {
                var b = TryGet<T>(new Vector2Int((int)position.x, (int)position.y), out T v);
                value = v;
                return b;
            }

            public Item GetItem<T>(Vector2 position) =>
                GetItem<T>(new Vector2Int((int)position.x, (int)position.y));
        }
    }
}
