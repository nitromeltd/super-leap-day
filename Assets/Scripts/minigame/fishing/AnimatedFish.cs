using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class AnimatedFish : MonoBehaviour
        {
            public Animated animated;
            public SpriteRenderer spriteRenderer;
            public Animated.Animation animSwim;
            public Animated.Animation animTurn;
            public Transform transformBob;
            public float wavelengthBob = 2f;
            public float magnitudeBob = 0.2f;
            public Shake shakeHit;

            public float faceX = 1f;
            private Vector3 startBob;
            private float offsetBob;


            void Start()
            {
                this.animated.PlayAndLoop(animSwim);
                if(this.transformBob != null)
                {
                    this.startBob = this.transformBob.localPosition;
                    this.offsetBob = Random.value * this.wavelengthBob;
                }
            }

            public void Update()
            {
                if(this.transformBob != null)
                {
                    this.transformBob.localPosition = this.startBob + Shake.Wave(
                        Time.timeSinceLevelLoad + this.offsetBob,
                        this.wavelengthBob,
                        this.magnitudeBob
                    ) * Vector3.up;
                    if(this.shakeHit != null)
                    {
                        if(this.shakeHit.time < this.shakeHit.duration)
                        {
                            this.shakeHit.Advance(Time.deltaTime);
                            this.transformBob.localPosition += (Vector3)this.shakeHit.Position();
                        }
                        else
                        {
                            this.shakeHit = null;
                        }
                    }
                }
            }

            public void Face(float x)
            {

                if(this.faceX != x)
                {
                    // strange bug where immediate call to turn animation doesn't play on level load
                    // possibly execution order issue - this patches it
                    if(this.animated.currentAnimation != animTurn)
                        this.animated.PlayOnce(animTurn, () =>
                        {
                            this.spriteRenderer.flipX = !this.spriteRenderer.flipX;
                            this.animated.PlayAndLoop(animSwim);
                            this.faceX = x;
                        });
                }
            }
        }

    }
}
