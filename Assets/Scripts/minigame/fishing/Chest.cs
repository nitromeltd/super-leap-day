using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace fishing
    {
        public class Chest : MonoBehaviour
        {
            [HideInInspector] public FishingGame game;
            public Transform transformChest;
            public Transform transformHookTarget;
            public SpriteRenderer spriteRenderer;
            public SpriteRenderer spriteRendererFront;
            public SpriteRenderer spriteRendererBack;
            public Sprite spriteTilted;
            public Sprite spriteTiltedFront;
            public Sprite spriteTiltedBack;

            private float timeOpened = -1f;

            public Vector3 PullPosition() => this.transformHookTarget.position;

            private void FixedUpdate()
            {
                float timeOpen = Time.timeSinceLevelLoad - this.timeOpened;
                if(timeOpen > 0.1f && timeOpen < 0.6f)
                {
                    if(this.spriteRenderer.sprite != this.spriteTilted)
                    {
                        this.spriteRenderer.sprite = this.spriteTilted;
                        this.spriteRendererBack.sprite = this.spriteTiltedBack;
                        this.spriteRendererFront.sprite = this.spriteTiltedFront;
                    }
                    SpawnBubble();
                }
            }

            void SpawnBubble()
            {
                var bubble = this.game.CreateBubble
                    (
                        this.transformChest.position + (Vector3)RandomUtil.Range(new Vector2(-0.8f, 1f), new Vector2(0.8f, 1f)),
                        RandomUtil.Range(new Vector2(-0.01f, 0.025f), new Vector2(0.01f, 0.1f)),
                        FishingGame.BubbleSpeedDefault * 0.3f,
                        "bubble_hook"
                    );
                bubble.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
            }

            public void Open()
            {
                Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_chest_open"]);
                this.game.ParticleCreateAndPlayOnce("smack", this.transformHookTarget.position);
                this.spriteRenderer.enabled = false;
                this.timeOpened = Time.timeSinceLevelLoad;
            }

            private void OnTriggerEnter2D(Collider2D collision)
            {
                this.game.playerHook.chest = this;
                this.game.playerHook.SetState(PlayerHook.State.Win);
            }


        }

    }
}
