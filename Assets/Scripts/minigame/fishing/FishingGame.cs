﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace minigame
{
    namespace fishing
    {
        public partial class FishingGame : Minigame
        {
            [Header("Fishing Game")]
            public GameObject hooksLeftPortrait;
            public GameObject hooksLeftLandscape;
            public TMP_Text finishText;
            public Image[] imageHooksPortrait;
            public Image[] imageHooksLandscape;
            private Image[] imageHooks;

            public SpriteRenderer backgroundTiled;
            public Transform containerBackgroundPieces;
            public Transform maskBackground;
            public WaterlineSingle waterline;

            public List<Particle> bubbles;
            public List<Particle> buoys;

            [NonSerialized] public Vector3 camOffset;
            [NonSerialized] public PlayerHook playerHook;
            [NonSerialized] public Vector3? positionChest;
            [NonSerialized] public Coroutine coroutineForCamera;
            [NonSerialized] public float waterY;

            public int hooksLeft;
            public int score;
            public bool completed;

            public Dictionary<Vector2Int, Flow> flowLookup;
            public Dictionary<Vector2Int, int> dirLookup;
            public Dictionary<Vector2Int, TriggerButton> triggerButtonLookup;
            public Dictionary<Vector2Int, PlayAnimated> changeDirDownLookup;
            public Dictionary<Vector2Int, PlayAnimated> changeDirUpLookup;

            public const float WaterGap = 5;
            public const float BubbleSpeedDefault = 0.075f;

            public class Flow
            {
                public Vector2 speed;
                public bool isOn;
            }

            public const float BackgroundPiecesRatio = 0.7f;

            public static string[] PickupFish = new string[]
            {
            "apple_fish", "banana_fish", "orange_fish", "pear_fish", "melon_fish"
            };

            public static Dictionary<string, bool> IsFish;


            void Start()
            {
                Scale = 1f;
                ScaleOne = new Vector2(Scale, Scale);
                ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
                ScreenWidth = 16;
                if(GameCamera.IsLandscape() == true)
                {
                    this.imageHooks = this.imageHooksLandscape;
                    hooksLeftPortrait.SetActive(false);
                }
                else
                {
                    this.imageHooks = this.imageHooksPortrait;
                    this.hooksLeftLandscape.SetActive(false);
                }

                Init();// MiniGame setup

                this.pickupMap.onPickupCollected += (pickup) =>
                {
                    // convert fish into fruit on collection
                    switch(pickup.name)
                    {
                        case "apple_fish":
                            pickup.name = "GLOBAL:apple";
                            break;
                        case "banana_fish":
                            pickup.name = "GLOBAL:banana";
                            break;
                        case "orange_fish":
                            pickup.name = "GLOBAL:orange";
                            break;
                        case "pear_fish":
                            pickup.name = "GLOBAL:pear";
                            break;
                        case "melon_fish":
                            pickup.name = "GLOBAL:melon";
                            break;
                    }
                };
                IsFish = new Dictionary<string, bool>();
                foreach(var name in PickupFish)
                {
                    this.pickupMap.names[name] = true;
                    this.pickupMap.sizes[name] = Vector2.one;
                    IsFish[name] = true;
                }
                IsFish["ButtonFish"] = true;

                Restart(true);

                this.minigameInputTypePrompt.FirstShow();

#if UNITY_EDITOR
                
                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.levelRect.width * 0.5f, this.playerObj.transform.localPosition.y + 8f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);

                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, () => {
                    // warm up bubbles
                    for(int i = 0; i < 15; i++)
                    {
                        UpdateBackground();
                        Animated.AdvanceAll(1f / 60f);
                        Particle.AdvanceAll();
                        frameCount++;
                    }
                });
                UpdateBackground();
#endif
            }
            void OnApplicationQuit() => Save();

            public override bool IsFailed() =>
                this.hooksLeft == 0 && this.playerHook != null && this.playerHook.state == PlayerHook.State.Death;
            
            public override bool IsComplete() => completed;

            public override void CompleteLevel()
            {
                this.completed = true;
                base.CompleteLevel();
            }

            public void InitUI()
            {
            }

            public override void Restart(bool hard)
            {

                base.Restart(hard);

                if(this.coroutineForCamera != null)
                {
                    StopCoroutine(this.coroutineForCamera);
                    this.coroutineForCamera = null;
                }
                this.hooksLeft = 3;
                UpdateHooksLeft();
                this.score = 0;
                this.camCtrl.clampAction = this.camCtrl.Clamp;
                this.flowLookup = new Dictionary<Vector2Int, Flow>();
                this.dirLookup = new Dictionary<Vector2Int, int>();
                this.triggerButtonLookup = new Dictionary<Vector2Int, TriggerButton>();
                this.changeDirDownLookup = new Dictionary<Vector2Int, PlayAnimated>();
                this.changeDirUpLookup = new Dictionary<Vector2Int, PlayAnimated>();
                this.positionChest = null;
                this.bubbles.Clear();
                this.buoys.Clear();

                this.fileLevel = GetLevelFile(difficulty, level);
                //this.fileLevel = DebugPanelLevelsDropdown.level ?? this.assets.levelTest;

                CreateLevel(this.fileLevel);

                InitUI();

                this.camOffset = Vector3.zero;

                this.playerHook.SetState(PlayerHook.State.Wait);
                
                if(this.positionChest.HasValue == true)
                {
                    IEnumerator LookAtChest()
                    {
                        this.camCtrl.SetPosition(this.positionChest.Value, Vector3.zero);
                        
                        yield return DoWhileCoroutine(1f, UpdateEnumerator);

                        var dist = Mathf.Abs((this.positionChest.Value - this.playerObj.transform.position).y);
                        
                        yield return this.camCtrl.TweenTo(this.playerHook.HookStart(), Vector3.zero, dist * 0.03f, Easing.QuadEaseOut, UpdateEnumerator);
                        
                        this.coroutineForCamera = null;
                    }
                    this.coroutineForCamera = StartCoroutine(LookAtChest());
                }
                else
                    this.camCtrl.SetPosition(this.playerHook.HookStart(), Vector3.zero);

            }

            // call during coroutines
            public void UpdateEnumerator()
            {
                if(this.playerHook.state == PlayerHook.State.Wait)
                    this.playerHook.UpdateWaitHook();
                
                UpdateBackground();

                frameCount++;
            }

            public IEnumerator DoWhileCoroutine(float delay, Action action)
            {
                var count = 0f;
                while(count < delay)
                {
                    count += Time.unscaledDeltaTime;
                    action.Invoke();
                    yield return null;
                }
            }

            void Update()
            {
                this.input.Advance();


#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R) && Input.GetKey(KeyCode.LeftShift))
                    Restart(true);
                else if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
#endif

                if(Minigame.paused == false)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }
                Minigame.fixedDeltaTimeElapsed += Time.fixedDeltaTime;
            }

            void FixedUpdate()
            {

                if(IsFailed() == true)
                {
                    return;
                }
                if(IsComplete() == true)
                {
                    if(this.positionChest.HasValue)
                        this.camCtrl.SetPosition(
                            this.cam.transform.position + (this.positionChest.Value - this.cam.transform.position) * 0.05f, this.camOffset
                        );
                    return;
                }


                float timestep = Time.fixedDeltaTime / Time.timeScale;

                if(this.coroutineForCamera != null)
                    return;

                if(this.camCtrl.lookAtTween == null)
                {

                    this.playerHook.Advance();

                    switch(this.playerHook.state)
                    {
                        case PlayerHook.State.Death: break;
                        case PlayerHook.State.Win: break;
                        case PlayerHook.State.Grabbed:
                        case PlayerHook.State.GoDown:
                            this.camCtrl.SetPosition(this.cam.transform.position + (GetCameraTargetDefault() - this.cam.transform.position) * 0.05f, this.camOffset);
                            break;
                        default:
                            this.camCtrl.SetPosition(this.cam.transform.position + (this.playerHook.HookStart() - this.cam.transform.position) * 0.05f, this.camOffset);
                            break;
                    }

                    UpdateMotors();
                    // set facing of fish
                    foreach(var m in this.motors)
                    {
                        if(IsFish.ContainsKey(m.transform.name))
                        {
                            var vx = m.transform.position.x - m.prev.x;
                            if(vx != 0)
                            {
                                var fish = m.transform.GetComponent<AnimatedFish>();
                                if(fish != null)
                                    fish.Face(Mathf.Sign(vx));
                                else
                                    m.transform.localScale = new Vector3(Mathf.Sign(vx), 1, 1);
                            }
                        }
                    }
                }
                else
                {
                    this.camCtrl.UpdateLookAt(Time.fixedDeltaTime);
                }
                camCtrl.AdvanceShakes(timestep);
                UpdateBackground();

                this.input.FixedAdvance();

                frameCount++;
            }

            public override Vector3 GetCameraTargetDefault()
            {
                return this.playerHook.transform.position + Vector3.up * this.playerHook.dirVertical * 5f;
            }

            public void SpendHook()
            {
                this.hooksLeft--;
                UpdateHooksLeft();
            }

            public void UpdateHooksLeft()
            {
                for(int i = 0; i < this.imageHooks.Length; i++)
                    this.imageHooks[i].enabled = i < this.hooksLeft;
                if(this.hooksLeft == 0) this.imageHooks[0].transform.localScale = Vector3.one;
            }

            public void UpdateBackground()
            {
                UpdateParticles();
                this.waterline?.Advance();

                var camY = this.cam.transform.position.y - this.camCtrl.rect.height * 0.5f;
                this.backgroundTiled.transform.position = new Vector3(0, camY * 0.9f);
                this.containerBackgroundPieces.position = new Vector3(0, camY * BackgroundPiecesRatio);
            }

            public Particle CreateBubble(Vector3 pos, Vector2 velocity, float variance, string name)
            {
                var particle = ParticleCreateAndPlayOnce(name, pos);
                particle.transform.SetParent(this.transform);
                particle.Throw(velocity, variance, variance, Vector2.zero);
                particle.spriteRenderer.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                particle.spriteRenderer.sortingOrder = this.bubbles.Count + 1000;
                particle.transform.localScale = Vector3.one * UnityEngine.Random.Range(0.8f, 1.4f);
                this.bubbles.Add(particle);
                return particle;
            }

            public void UpdateParticles()
            {
                var border = new Vector2(2f, 2f);
                for(int i = this.bubbles.Count - 1; i > -1; i--)
                {
                    var b = this.bubbles[i];
                    if(b == null || b.gameObject == null || b.gameObject.activeInHierarchy == false)
                    {
                        this.bubbles.RemoveAt(i);
                    }
                    else
                    {
                        if(this.flowLookup.TryGetValue(ToVector2Int(b.transform.position), out Flow flow) == true)
                        {
                            if(flow.isOn == true)
                            {
                                b.velocity += flow.speed * BubbleSpeedDefault;
                                b.velocity *= 0.5f;
                            }
                        }
                    }
                }
                // spawn bubbles
                var period = 13;
                if(frameCount % period == 0)
                {
                    foreach(var kv in this.flowLookup)
                    {
                        // alternate spawns in a checkerboard pattern
                        if(((kv.Key.x + kv.Key.y) % 2 == 0) != (frameCount % (period * 2) == 0))
                            continue;

                        var speed = kv.Value.speed;
                        var variance = BubbleSpeedDefault * 0.3f;
                        var bubbleType = RandomUtil.Pick("bubble_fast", "bubble");
                        if(kv.Value.isOn == false)
                        {
                            if(frameCount % (period * 5) != 0)
                                continue;
                            speed *= 0.33f;
                            variance *= 0.33f;
                            bubbleType = "bubble_slow";
                        }

                        var p = (ToVector2(kv.Key) + UnityEngine.Random.insideUnitCircle * 0.5f) - speed;
                        if(this.camCtrl.OnScreen(p, new Vector2(2f, 2f)))
                            CreateBubble(p, speed * BubbleSpeedDefault, variance, bubbleType);
                    }
                }
                // update discarded buoys
                for(int i = this.buoys.Count - 1; i > -1; i--)
                {
                    var b = this.buoys[i];
                    if(b.transform.position.y > this.waterY - 1f)
                    {
                        b.velocity = Vector2.zero;
                        b.spriteRenderer.color -= new Color(0, 0, 0, 0.1f);
                        if(b.spriteRenderer.color.a <= 0f)
                        {
                            this.buoys.RemoveAt(i);
                            Destroy(b.gameObject);
                        }
                    }
                }
            }

        }

    }
}

