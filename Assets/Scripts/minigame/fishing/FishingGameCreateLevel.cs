﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public partial class FishingGame : Minigame
        {

            public void CreateLevel(TextAsset file)
            {
                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                );
                this.map.AddNoAutoTiles(""
                    , "polysquare"
                    , "poly45br"
                    , "poly22br"
                    , "poly67br"
                    , "Circle2"
                    , "Circle3"
                    , "Circle4"
                );
                this.map.AddDontRotateTiles(
                    "SpikesRetractable"
                    ,"SpikesRetractable_in"
                    ,"SpikesRetractable_out"
                );
                this.map.AddPickupPrefabs(
                    "apple_fish"
                    , "banana_fish"
                    , "orange_fish"
                    , "pear_fish"
                    , "melon_fish"
                );

                var level = this.map.ParseLevel(file);
                var objects = level.TileLayerByName("Objects");

                // replace random_fish
                for(int i = 0; i < objects.tiles.Length; i++)
                    if(objects.tiles[i].tile == "random_fish") objects.tiles[i].tile = RandomUtil.Pick(PickupFish);

                // give background layer some room when we build it later
                this.spriteRendererPool.sortingOrderCount = 1000;

                // generate all objects
                this.map.Create(level);

                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;

                this.waterY = this.levelRect.height + WaterGap;
                var waterDirDownY = this.levelRectInt.size.y + (int)WaterGap - 2;

                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        // spikes map
                        this.spikesLookup[record.p] = record.name switch
                        {
                            "GLOBAL:spike_top_1" => 1,
                            "GLOBAL:spike_top_2" => 1,
                            "GLOBAL:spike_right_1" => 2,
                            "GLOBAL:spike_right_2" => 2,
                            "GLOBAL:spike_bottom_1" => 3,
                            "GLOBAL:spike_bottom_2" => 3,
                            "GLOBAL:spike_left_1" => 4,
                            "GLOBAL:spike_left_2" => 4,
                            "GLOBAL:spike_vert_1" => 5,
                            "GLOBAL:spike_vert_2" => 5,
                            "GLOBAL:spike_horz_1" => 6,
                            "GLOBAL:spike_horz_2" => 6,
                            _ => 0
                        };
                        record.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
                    }

                    if(record.isPrefab == true)
                    {

                        switch(record.name)
                        {
                            case "Player":
                                {
                                    
                                    this.playerObj = record.gameObject;
                                    var p = this.playerObj.transform.position;
                                    this.playerObj.transform.position = new Vector3(p.x, this.waterY + PlayerHook.BoatAnchorStartY);
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                    // force hook to go down below boat
                                    for(int column = 0; column < this.map.width; column++)
                                        dirLookup[new Vector2Int(column, waterDirDownY)] = -1;
                                }
                                break;
                            case "Chest":
                                record.GetComponent<Chest>().game = this;
                                positionChest = record.pos;
                                ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                break;
                            case "Grabber":
                                record.GetComponent<Grabber>().Init(this, record);
                                break;
                            case "ButtonFish":
                            case "Button":
                                {
                                    var component = record.GetComponent<TriggerButton>();
                                    component.game = this;
                                    if(record.name == "Button")
                                    {
                                        // slightly longer rect, assuming there may be an offset
                                        Minigame.ForeachInRectInt(MinigameMap.RectIntFromTile(new RectInt(record.x, record.y, 3, 1), record.p, record.tile), (vint) =>
                                        {
                                            this.triggerButtonLookup[record.p] = component;
                                        });
                                    }
                                }
                                break;
                            case "MineHook":
                                record.GetComponent<MineHook>().game = this;
                                break;
                            case "MineProxy":
                                record.GetComponent<MineProxy>().Init(this);
                                break;
                            case "SpikeOffset2":
                            case "SpikeOffset3":
                            case "SpikeOffset4":
                            case "SpikeFloat":
                                record.GetComponent<SpikedItem>().game = this;
                                break;
                            case "SpikesRetractable":
                                record.GetComponent<SpikesRetractable>().Init(record.p, record.tile, this);
                                break;
                            case "SpikeHoop":
                                {
                                    var component = record.GetComponent<SpikeHoop>();
                                    component.start = component.transform.position;
                                    component.spriteRendererFront.sortingLayerName = this.objectLayers.foregroundSortingLayer;
                                    component.game = this;
                                    // bait for player
                                    this.pickupMap.CreatePickup(new NitromeEditor.TileLayer.Tile { tile = "GLOBAL:" + RandomUtil.Pick(MinigameMap.FruitRandom) }, record.p, record.pos + record.offset + new Vector2(0, 1f));
                                }
                                break;
                            case "seaweed_1":
                            case "seaweed_2":
                            case "seaweed_3":
                            case "seaweed_4":
                            case "seaweed_5":
                            case "seaweed_6":
                                record.gameObject.transform.Find("spine").GetComponent<MeshRenderer>().sortingOrder += 1000;
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        switch(record.tile.tile)
                        {
                            case "Speed":
                            case "SpeedOff":
                                {
                                    var dir = record.tile.rotation switch
                                    {
                                        90 => Vector2.left,
                                        180 => Vector2.down,
                                        270 => Vector2.right,
                                        _ => Vector2.up
                                    };
                                    var flow = new Flow
                                    {
                                        isOn = record.tile.tile == "Speed",
                                        speed = dir
                                    };
                                    this.flowLookup[record.p] = flow;

                                    // MinigameMap made connections using GameObject
                                    // We no longer need that data
                                    record.GetComponent<SpriteRenderer>().enabled = false;
                                    Destroy(record.gameObject);
                                    this.map.vector2IntToGameObject.Remove(record.p);
                                }
                                break;
                        }
                    }
                    else
                    {
                        switch(record.tile.tile)
                        {
                            case "ChangeDirUp":
                                this.dirLookup[record.p] = 1;
                                break;
                            case "ChangeDirDown":
                                this.dirLookup[record.p] = -1;
                                break;
                        }
                    }
                }

                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "ButtonFish":
                        case "Button":
                            {
                                var tb = conn.objB.GetComponent<TriggerButton>();
                                tb.game = this;
                                tb.targets.Add(conn.a);
                                triggerButtons.Add(tb);
                            }
                            break;
                        case "Portal":
                            var portalA = conn.objA.GetComponent<Portal>();
                            var portalB = conn.objB.GetComponent<Portal>();
                            portalA.Init(this, portalB);
                            portalB.game = this;
                            ObjectLayers.SetSortingLayer(conn.objA, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount++);
                            ObjectLayers.SetSortingLayer(conn.objB, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount++);
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                    }
                }

                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);

                // DEBUGGING
                //Minigame.testingLevel ??= this.map.levelOriginal;

                // extract hook from player boat
                var hook = this.playerObj.transform.Find("hook");
                hook.transform.SetParent(transform, true);
                this.playerHook = hook.GetComponent<PlayerHook>();
                this.playerHook.Init(this, this.playerObj);

                // create change direction graphics
                var buoyBlockerLines = new Dictionary<int, bool>();
                for(int y = 0; y < this.levelRectInt.height; y++)
                {
                    if(y == waterDirDownY) continue;
                    for(int x = 0; x < this.levelRectInt.width; x++)
                    {
                        var p = new Vector2Int(x, y);
                        if(this.dirLookup.TryGetValue(p, out int dir) == true)
                        {
                            if(dir < 0)
                            {
                                var obj = Instantiate(this.assets.prefabLookup["buoy_blocker"], this.map.transformObjects);
                                obj.transform.position = new Vector3(x, y);
                                buoyBlockerLines[y] = true;
                                this.changeDirDownLookup[p] = obj.GetComponent<PlayAnimated>();
                                ObjectLayers.SetSortingLayer(obj, this.objectLayers.objectsSortingLayer);
                            }
                            else
                            {
                                var w = 1;
                                while(this.dirLookup.TryGetValue(new Vector2Int(x + w, y), out int dirDown) == true && dirDown == 1)
                                    w++;
                                for(int i = 0; i < w; i++)
                                {
                                    var obj = Instantiate(this.assets.prefabLookup["buoy_ground"], this.map.transformObjects);
                                    var xPos = x + i + 0.5f;
                                    if(i == 0) xPos += 0.32f;
                                    if(i == w - 1) xPos -= 0.32f;
                                    obj.transform.position = new Vector3(xPos, y);
                                    this.changeDirUpLookup[p + Vector2Int.right * i] = obj.GetComponent<PlayAnimated>();
                                    ObjectLayers.SetSortingLayer(obj, this.objectLayers.objectsSortingLayer);
                                }
                                x += w - 1;
                            }
                        }
                    }
                }
                // rope connection buoy blockers
                foreach(var y in buoyBlockerLines.Keys)
                {
                    var sr = this.spriteRendererPool.Get(new Vector3(0, y), this.assets.spriteLookup["buoy_blocker_line"], this.objectLayers.backgroundSortingLayer, "buoy_blocker_line", this.map.transformObjects);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(16, 1);
                }

                BuildBackground();

                // allowing the exit-chest to be visible in the bottom half of the win-porthole
                float cameraGap = ScreenHeight * 0.5f - 4f;

                // create side fill
                {
                    float heightFloor = (this.levelRect.height - 4f) + cameraGap;
                    string sortingLayer = this.objectLayers.tilesSortingLayer;
                    Sprite spriteEdge = this.assets.spriteLookup["level edge"];
                    int sortingOrder = 2000;

                    SpriteRenderer sr;
                    // edge
                    sr = this.spriteRendererPool.Get(new Vector3(1, -cameraGap), spriteEdge, sortingLayer, "level edge", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor);
                    sr.sortingOrder = sortingOrder;
                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width - 1, -cameraGap), spriteEdge, sortingLayer, "level edge", this.transform);
                    sr.flipX = true;
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor);
                    sr.sortingOrder = sortingOrder;

                    // caps
                    Sprite sprite;
                    sprite = this.assets.spriteLookup["left sea bed"];
                    sr = this.spriteRendererPool.Get(new Vector3(2, heightFloor - cameraGap), sprite, sortingLayer, "cap_left", this.transform);
                    sr.sortingOrder = sortingOrder;

                    sprite = this.assets.spriteLookup["right sea bed"];
                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width - 2f, heightFloor - cameraGap), sprite, sortingLayer, "cap_right", this.transform);
                    sr.sortingOrder = sortingOrder;

                    // fill below level
                    sr = this.spriteRendererPool.Get(new Vector3(1, -cameraGap), this.assets.spriteLookup["tile_fill"], sortingLayer, "tile_fill", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(14, cameraGap + 1);
                    sr.sortingOrder = sortingOrder;
                    sr = this.spriteRendererPool.Get(new Vector3(1, 0), this.assets.spriteLookup["tile_top_fill"], sortingLayer, "tile_top_fill", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(14, 1);
                    sr.sortingOrder = sortingOrder+1;

                    // strip unused edge tiles
                    foreach(Transform t in this.map.transformTiles)
                    {
                        if(t.name.Contains("(0, ") == true || t.name.Contains("(15, ") == true)
                        {
                            var collider = t.gameObject.GetComponent<BoxCollider2D>();
                            var spriteRenderer = t.gameObject.GetComponent<SpriteRenderer>();
                            if(collider == null) Destroy(t.gameObject);
                            else if(spriteRenderer != null) Destroy(spriteRenderer);
                        }
                    }
                }

                // add horizon
                {
                    var obj = Instantiate(this.assets.prefabLookup["Horizon"], this.transform);
                    obj.transform.position = new Vector3(8f, this.waterY);
                    ObjectLayers.SetSortingLayer(obj, this.objectLayers.backgroundSortingLayer);
                    obj.GetComponent<Horizon>().Init();
                }

                // add waterline
                {
                    float xMin = -1f;
                    float xMax = 17f;
                    if(GameCamera.IsLandscape() == true)
                        (xMin, xMax) = (7f - (Minigame.ScreenWidth * 0.5f), 9 + Minigame.ScreenWidth * 0.5f);
                    this.waterline = new WaterlineSingle(this.assets.prefabLookup["Water"], this.transform, this.waterY, xMin, xMax);
                }

                // modify level bounds for camera
                this.levelRect.y -= cameraGap;
                this.levelRect.height += 20f + cameraGap;
            }

            public void BuildBackground()
            {
                this.backgroundTiled.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                this.spriteRendererPool.sortingOrderCount = 0;

                foreach(Transform childTransform in this.containerBackgroundPieces)
                    if(childTransform != this.backgroundTiled.transform)
                        Destroy(childTransform.gameObject);

                this.backgroundTiled.size = new Vector2(this.backgroundTiled.size.x, this.camCtrl.rect.height + this.levelRect.height * 0.1f);

                List<(Sprite sprite, bool isSpan, SpriteRenderer spriteRenderer)> FillDeck()
                {
                    var pieces = new List<(Sprite sprite, bool isSpan, SpriteRenderer sr)>();
                    foreach(var s in this.assets.spriteArrayLookup["background_span"].list)
                        pieces.Add((s, true, null));
                    foreach(var s in this.assets.spriteArrayLookup["background_side"].list)
                        pieces.Add((s, false, null));
                    return pieces;
                }

                var pieces = FillDeck();
                var piece = RandomUtil.Pluck(pieces);
                (Sprite sprite, bool isSpan, SpriteRenderer spriteRenderer)? piecePrev = null;
                var y = -3f;

                var yMax = this.camCtrl.rect.height * 0.5f + this.waterY * (1f - BackgroundPiecesRatio);

                while(y < yMax)
                {
                    var flipped = Random.value < 0.5f;

                    if(piecePrev.HasValue == true) {
                        var height = piecePrev.Value.spriteRenderer.size.y;
                        if(piecePrev.Value.isSpan == false && piece.isSpan == false)
                        {
                            flipped = !piecePrev.Value.spriteRenderer.flipX;
                            y -= Random.Range(height / 4, height / 2);
                        }
                        else
                        {
                            y += Random.Range(0.5f, 1.5f);
                        }
                    }
                    var x = flipped == true ? 16f : 0f;
                    var sr = this.spriteRendererPool.Get(new Vector3(x, y), piece.sprite, this.objectLayers.backgroundSortingLayer, piece.sprite.name, this.containerBackgroundPieces);
                    sr.maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    sr.flipX = flipped;
                    piece.spriteRenderer = sr;

                    y += sr.size.y;

                    if(y >= yMax)
                    {
                        y -= sr.size.y;
                        Destroy(sr.gameObject);
                        break;
                    }

                    piecePrev = piece;
                    if(pieces.Count == 0) pieces = FillDeck();
                    piece = RandomUtil.Pluck(pieces);
                }

                this.maskBackground.localScale = new Vector3(16, this.waterY, 1);
            }

            public const float SpikeLength = 0.9f;
            public static Vector2 SpikeIndexToDir(int i) => i switch
            {
                1 => Vector2.up * SpikeLength,
                2 => Vector2.right * SpikeLength,
                3 => Vector2.down * SpikeLength,
                4 => Vector2.left * SpikeLength,
                5 => Vector2.up,
                6 => Vector2.right,
                _ => Vector2.zero
            };

            public static int SpikeRotationToDir(int r) => r switch
            {
                0 => 1,
                90 => 4,
                180 => 3,
                270 => 2,
                _ => 1
            };
        }
    }
}

