using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class Grabber : MonoBehaviour
        {
            public FishingGame game;
            public Collider2D trigger;
            public SpriteRenderer spriteRenderer;
            public SpriteRenderer spriteRendererFront;
            public Sprite spriteIdle;
            public Sprite spriteOpen;
            public Sprite spriteClosed;
            public Animated animated;
            public Animated.Animation animIdle;
            public Animated.Animation animOpen;
            public Animated.Animation animOpenToIdle;
            public Animated.Animation animIdleToOpen;
            public Animated.Animation animClosed;
            public Motor motor;
            public State state;

            public enum State
            {
                Idle, Grab, Close, Release
            }

            private float timeOfRelease = -1f;

            public void Init(FishingGame game, MinigameMap.Record record)
            {
                this.game = game;
                this.spriteRendererFront.sortingLayerName = game.objectLayers.playerSortingLayer;
                this.spriteRendererFront.enabled = false;
                this.animated.PlayAndLoop(this.animIdle);
                if(game.map.motorLookup.TryGetValue(record.p, out Motor motor) == true){
                    this.motor = motor;
                    motor.paused = true;
                    motor.speed = 0;
                    motor.looped = false;
                    this.game.map.motorLookup.Remove(record.p);
                }
            }

            private void FixedUpdate()
            {
                Advance();
            }

            void Advance()
            {
                if(this.motor == null) return;

                float angle = this.transform.localEulerAngles.z;

                switch(this.state)
                {
                    case State.Release:
                        if(Time.timeSinceLevelLoad - this.timeOfRelease > 1f)
                        {
                            this.animated.PlayOnce(this.animOpenToIdle, () => {
                                this.animated.PlayAndLoop(this.animIdle);
                            });
                            this.spriteRenderer.sprite = this.spriteIdle;
                            this.state = State.Idle;
                        }
                        angle = Mathf.LerpAngle(angle, 0, 0.1f);
                        this.transform.localRotation = Quaternion.Euler(0, 0, angle);
                        break;
                    case State.Close:
                    case State.Grab:
                        angle = Mathf.LerpAngle(angle, this.game.playerHook.transform.localEulerAngles.z, 0.1f);
                        if(this.motor.paused == true && this.game.playerHook.state == PlayerHook.State.Grabbed)
                            Release();
                        this.transform.localRotation = Quaternion.Euler(0, 0, angle);
                        break;

                }

            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                Grab();
            }

            public void Grab()
            {
                if(this.game != null)
                {
                    if(this.game.playerHook.state != PlayerHook.State.GoDown) return;
                    if(this.game.IsComplete()) return;// don't trigger events during end game
                    if(this.motor == null || this.state != State.Idle) return;

                    this.animated.PlayOnce(this.animIdleToOpen, () => {
                        this.animated.PlayAndLoop(this.animOpen);
                    });
                    this.state = State.Grab;
                    this.spriteRenderer.sprite = this.spriteOpen;
                    this.trigger.enabled = false;
                    this.motor.SetSpeed(1);
                    this.game.playerHook.grabber = this;
                    this.game.playerHook.SetState(PlayerHook.State.Grabbed);
                }

            }

            public void Close()
            {
                this.state = State.Close;
                this.animated.PlayOnce(this.animClosed);
                this.spriteRendererFront.enabled = true;
                Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_squid"]);
                this.game.ParticleCreateAndPlayOnce("smack", this.transform.position);
            }

            public void Release()
            {
                this.state = State.Release;
                this.spriteRenderer.sprite = this.spriteOpen;
                this.spriteRendererFront.enabled = false;
                this.trigger.enabled = true;
                this.timeOfRelease = Time.timeSinceLevelLoad;
                if(this.motor.speed > 0)
                {
                    this.motor.SetSpeed(-1);
                }
                this.game.playerHook.grabber = null;
                this.game.playerHook.SetState(PlayerHook.State.GoDown);
            }

        }
    }
}
