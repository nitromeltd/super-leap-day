using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class Horizon : MonoBehaviour
        {

            public float widthSprite;
            public SpriteRenderer[] spriteRenderersWater;
            public SpriteRenderer spriteRendererGradient;
            public SpriteRenderer spriteRendererBack;
            public SpriteRenderer spriteRendererSun;
            public SpriteRenderer spriteRendererBackgroundAlpha;
            public SpriteRenderer spriteRendererSkyFill;

            public void Init()
            {
                this.widthSprite = spriteRendererBack.size.x;
                foreach(var sr in spriteRenderersWater)
                    sr.size = new Vector2(Minigame.ScreenWidth + widthSprite, sr.size.y);
                spriteRendererGradient.size = new Vector2(Minigame.ScreenWidth, spriteRendererGradient.size.y);
                spriteRendererBack.size = new Vector2(Minigame.ScreenWidth, spriteRendererBack.size.y);

                spriteRenderersWater[0].transform.localPosition =
                    spriteRenderersWater[1].transform.localPosition = 
                    spriteRendererGradient.transform.localPosition = Vector3.zero;
                spriteRendererBack.transform.localPosition = new Vector3(-Minigame.ScreenWidth * 0.5f, 0);
                spriteRendererSun.transform.localPosition = new Vector3(spriteRendererSun.transform.localPosition.x, Minigame.ScreenHeight * 0.4f);

                spriteRendererSkyFill.transform.localPosition = new Vector3(-Minigame.ScreenWidth * 0.5f, spriteRendererSkyFill.transform.localPosition.y);
                spriteRendererSkyFill.size = new Vector2(Minigame.ScreenWidth, Minigame.ScreenHeight * 0.5f);

                var alphaSprite = this.spriteRendererBackgroundAlpha.sprite;
                if(alphaSprite.rect.size.x / alphaSprite.pixelsPerUnit < Minigame.ScreenWidth)
                    spriteRendererBackgroundAlpha.transform.localScale = new Vector3(Minigame.ScreenWidth / (alphaSprite.rect.size.x / alphaSprite.pixelsPerUnit), 1f, 1f);
            }

            void Update()
            {
                for(int i = 0; i < spriteRenderersWater.Length; i++)
                {
                    var sr = spriteRenderersWater[i];
                    sr.transform.localPosition += Vector3.left * (0.005f + i * 0.005f);
                    if(sr.transform.localPosition.x < -this.widthSprite)
                    {
                        sr.transform.localPosition = new Vector3(0, 0);
                    }
                }
            }
        }

    }
}
