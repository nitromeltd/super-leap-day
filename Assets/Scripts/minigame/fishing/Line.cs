using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace fishing
    {
        public class Line
        {
            public LineRenderer lineRenderer;
            public List<Vector3> points = new List<Vector3>();
            public List<float> bends = new List<float>();
            public Vector3? drawEnd;
            public Vector3? drawStart;

            public void Reset(Vector3 start, Vector3 end)
            {
                this.lineRenderer.material.mainTextureOffset = Vector2.zero;
                this.points.Clear();
                this.bends.Clear();
                this.points.Add(start);
                this.points.Add(end);
                this.lineRenderer.material.SetFloat("_ScrollOffset", 0);
                UpdateLineRenderer();
            }

            public void AddBend(float dir, Vector3 cut)
            {
                this.bends.Add(dir);
                var end = this.points[^1];
                this.points[^1] = cut;
                this.points.Add(end);
            }

            public void UpdateLineRenderer(float lengthNext = 0)
            {
                if(this.lineRenderer.positionCount != this.points.Count)
                {
                    this.lineRenderer.positionCount = this.points.Count;
                }
                var reverse = new List<Vector3>(this.points);
                reverse.Reverse();
                if(this.drawStart.HasValue == true)
                    reverse[^1] = this.drawStart.Value;
                if(this.drawEnd.HasValue == true)
                    reverse[0] = this.drawEnd.Value;
                this.lineRenderer.SetPositions(reverse.ToArray());
                
                // offset when multiple Lines in use
                if(lengthNext > 0)
                    this.lineRenderer.material.SetFloat("_ScrollOffset", lengthNext);
            }

            public Vector2 EndVector() =>
                this.points[^1] - this.points[^2];

            public (Vector2 a, Vector2 b) EndSegment() =>
                (this.points[^2], this.points[^1]);

            public float EndAngle()
            {
                var v = EndVector();
                return Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
            }

            public Vector3 End
            {
                get => this.points[^1];
                set => this.points[^1] = value;
            }

            public Vector3 BeforeEnd => this.points[^2];

            public float Length()
            {
                if(this.points.Count <= 1) return 0;
                float len = 0f;
                for(int i = this.points.Count - 1; i > 0; i--)
                    len += (this.points[i] - this.points[i - 1]).magnitude;
                return len;
            }

            public void DoAtEachCell(Action<Vector2Int, (Vector2 a, Vector2 b)> action)
            {
                for(int i = 1; i < this.points.Count; i++)
                {
                    var a = this.points[i - 1];
                    var b = this.points[i];
                    var cells = GetCells(a, b);
                    for(int j = 0; j < cells.Count; j++)
                        action(cells[j], (a, b));
                }
            }

            public void DoAtEachSegment(Action<Vector2, Vector2> action)
            {
                for(int i = 1; i < points.Count; i++)
                    action(points[i - 1], points[i]);
            }

            #region UTILITIES

            // Returns true if both line segments intersect
            public static bool GetLineIntersection(Vector2 a1, Vector2 b1, Vector2 a2, Vector2 b2, out Vector2 intersection)
            {
                float dx1 = b1.x - a1.x;
                float dy1 = b1.y - a1.y;
                float dx2 = b2.x - a2.x;
                float dy2 = b2.y - a2.y;

                float denominator = (dy1 * dx2 - dx1 * dy2);
                float t1 = ((a1.x - a2.x) * dy2 + (a2.y - a1.y) * dx2) / denominator;
                intersection = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);

                if(float.IsInfinity(t1))
                    return false;

                float t2 = ((a2.x - a1.x) * dy1 + (a1.y - a2.y) * dx1) / -denominator;

                if(t1 < 0 || t1 > 1 || t2 < 0 || t2 > 1)
                    return false;
                
                return true;
            }

            public static List<Vector2Int> GetCells(Vector2 a, Vector2 b, bool skipFirstCell = false)
            {
                var result = new List<Vector2Int>();
                var delta = b - a;
                var length = delta.magnitude;
                var normal = delta / length;
                var invNormal = new Vector2(1f / normal.x, 1f / normal.y);
                var cell = new Vector2Int(Mathf.FloorToInt(a.x), Mathf.FloorToInt(a.y));
                Vector2Int dir = new Vector2Int(normal.x < 0 ? -1 : 1, normal.y < 0 ? -1 : 1);
                Vector2 positionWithinCell = a - cell;
                Vector2 targetFromWithinCell = new Vector2((normal.x < 0) ? 0 : 1f, (normal.y < 0) ? 0 : 1f);
                //Minigame.DebugX(cell + positionWithinCell, Color.white, 0.1f);
                while(length > 0)
                {
                    //Minigame.DebugRect(new Rect(cell.x, cell.y, 1, 1), Color.red);
                    if(skipFirstCell == false)
                        result.Add(cell);
                    else
                        skipFirstCell = false;

                    float nextX = Mathf.Abs((positionWithinCell.x - targetFromWithinCell.x) * invNormal.x);
                    float nextY = Mathf.Abs((positionWithinCell.y - targetFromWithinCell.y) * invNormal.y);

                    if(nextX < nextY || nextY == 0)
                    {
                        cell.x += dir.x;
                        length -= nextX;
                        positionWithinCell += normal * nextX;
                        positionWithinCell.x -= dir.x;
                    }
                    else
                    {
                        cell.y += dir.y;
                        length -= nextY;
                        positionWithinCell += normal * nextY;
                        positionWithinCell.y -= dir.y;
                    }
                    //Minigame.DebugX(cell + positionWithinCell, Color.white, 0.1f);
                }
                return result;
            }

            public static (Vector2 a, Vector2 b) LineFromSideOfACell(Vector2Int cell, Vector2 dir)
            {
                var a = new Vector2
                (
                    cell.x + (dir.y != 0 ? 0.5f : (dir.x < 0 ? 1f : 0f)),
                    cell.y + (dir.x != 0 ? 0.5f : (dir.y < 0 ? 1f : 0f))
                );
                return (a, a + dir);
            }

            // In polygon.points where rayHit is on a segment, return vertex in direction of rayVector
            public static Vector2 GetBendVertex(Vector2[] points, Vector2 rayFrom, Vector2 rayTo, Vector2 rayHit)
            {
                // Unity doesn't tell us about the segment the raycast hit so we have to find it
                var best = float.MaxValue;
                var length = points.Length;
                (Vector2 a, Vector2 b) bestSegment = (Vector2.zero, Vector2.zero);
                var pointsCenter = Vector2.zero;
                
                for(int i = 0; i < length; i++)
                {
                    var a = points[i];
                    var b = points[(i + 1) % length];
                    var dist = SquareDistanceEdgeToPoint(a, b, rayHit);
                    if(dist < best)
                    {
                        best = dist;
                        bestSegment = (a, b);
                    }
                    pointsCenter += a;
                }

                // target vertex is usually be in direction of rayVector
                Vector2 target = bestSegment.b;
                Vector2 alternate = bestSegment.a;
                var rayVector = rayTo - rayFrom;
                var segmentVector = bestSegment.b - bestSegment.a;
                if(Vector2.Dot(segmentVector, rayVector) < 0)
                {
                    target = bestSegment.a;
                    alternate = bestSegment.b;
                }

                // near-perpendicular ray edge case
                // check we are the correct side of the center
                var rayVectorLeftHand = Vector2.Perpendicular(rayVector);
                //Debug.Log(Vector2.Dot(rayVectorLeftHand.normalized, segmentVector.normalized));
                if(Mathf.Abs(Vector2.Dot(rayVectorLeftHand.normalized, segmentVector.normalized)) > 0.5f)
                {
                    pointsCenter /= points.Length;
                    //Minigame.DebugX(pointsCenter, Color.green);
                    var rayToCenterLeftHand = Vector2.Perpendicular(pointsCenter - rayFrom);
                    var originalSide = Mathf.Sign(Vector2.Dot(rayToCenterLeftHand, rayVector));
                    var targetSide = Mathf.Sign(Vector2.Dot(rayToCenterLeftHand, target - rayFrom));
                    //Debug.Log($"original  {originalSide} {targetSide}");
                    if(targetSide != originalSide)
                    {
                        //Debug.Log("perpendicular - switch target");
                        return alternate;
                    }
                }
                return target;
            }

            public static Vector2 GetNearestPointToLine(Vector2 lineFrom, Vector2 lineTo, params Vector2[] points)
            {
                var best = float.MaxValue;
                var length = points.Length;
                var bestPoint = Vector2.zero;

                for(int i = 0; i < length; i++)
                {
                    var p = points[i];
                    var dist = SquareDistanceEdgeToPoint(lineFrom, lineTo, p);
                    if(dist < best)
                    {
                        best = dist;
                        bestPoint = p;
                    }
                }
                return bestPoint;
            }

            public static float SquareDistanceEdgeToPoint(Vector2 from, Vector2 to, Vector2 point)
            {
                float edgeDeltaX = to.x - from.x;
                float edgeDeltaY = to.y - from.y;
                float toTargetX = point.x - from.x;
                float toTargetY = point.y - from.y;

                var along =
                    (toTargetX * edgeDeltaX + toTargetY * edgeDeltaY) /
                    (edgeDeltaX * edgeDeltaX + edgeDeltaY * edgeDeltaY);
                if(along < 0) along = 0;
                if(along > 1) along = 1;

                var nearestX = from.x + (edgeDeltaX * along);
                var nearestY = from.y + (edgeDeltaY * along);
                var toNearestX = point.x - nearestX;
                var toNearestY = point.y - nearestY;
                var sqDist = toNearestX * toNearestX + toNearestY * toNearestY;
                return sqDist;
            }

            public static float PointToLineDistance(Vector2 point, Vector2 a, Vector2 b)
            {
                var A = point.x - a.x;
                var B = point.y - a.y;
                var C = b.x - a.x;
                var D = b.y - b.y;
                var dot = A * C + B * D;
                var lenSq = C * C + D * D;
                var param = -1f;
                if(lenSq != 0) //in case of 0 length line
                    param = dot / lenSq;
                float xx, yy;
                if(param < 0)
                {
                    xx = a.x;
                    yy = a.y;
                }
                else if(param > 1)
                {
                    xx = b.x;
                    yy = b.y;
                }
                else
                {
                    xx = a.x + param * C;
                    yy = a.y + param * D;
                }
                var dx = point.x - xx;
                var dy = point.y - yy;
                return Mathf.Sqrt(dx * dx + dy * dy);
            }

            #endregion

            #region NOTES

            // Find the point of intersection between lines 1 & 2
            // copied from: http://csharphelper.com/blog/2014/08/determine-where-two-lines-intersect-in-c/
            // This method is here for future reference
            void LineIntersectionMath(
                Vector2 a1, Vector2 b1, Vector2 a2, Vector2 b2,
                out bool lines_intersect, out bool segments_intersect,
                out Vector2 intersection,
                out Vector2 close_p1, out Vector2 close_p2)
            {
                // Get the segments' parameters.
                float dx1 = b1.x - a1.x;
                float dy1 = b1.y - a1.y;
                float dx2 = b2.x - a2.x;
                float dy2 = b2.y - a2.y;

                // Solve for t1 and t2
                float denominator = (dy1 * dx2 - dx1 * dy2);

                float t1 =
                    ((a1.x - a2.x) * dy2 + (a2.y - a1.y) * dx2)
                        / denominator;
                if(float.IsInfinity(t1))
                {
                    // The lines are parallel (or close enough to it).
                    lines_intersect = false;
                    segments_intersect = false;
                    intersection = new Vector2(float.NaN, float.NaN);
                    close_p1 = new Vector2(float.NaN, float.NaN);
                    close_p2 = new Vector2(float.NaN, float.NaN);
                    return;
                }
                lines_intersect = true;

                float t2 =
                    ((a2.x - a1.x) * dy1 + (a1.y - a2.y) * dx1)
                        / -denominator;

                // Find the point of intersection.
                intersection = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);

                // The segments intersect if t1 and t2 are between 0 and 1.
                segments_intersect =
                    ((t1 >= 0) && (t1 <= 1) &&
                     (t2 >= 0) && (t2 <= 1));

                // Find the closest points on the segments.
                if(t1 < 0)
                    t1 = 0;
                else if(t1 > 1)
                    t1 = 1;

                if(t2 < 0)
                    t2 = 0;
                else if(t2 > 1)
                    t2 = 1;

                close_p1 = new Vector2(a1.x + dx1 * t1, a1.y + dy1 * t1);
                close_p2 = new Vector2(a2.x + dx2 * t2, a2.y + dy2 * t2);
            }

            #endregion


        }
    }
}
