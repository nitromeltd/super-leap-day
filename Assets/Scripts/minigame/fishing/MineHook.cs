using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace fishing
    {
        public class MineHook : MonoBehaviour
        {
            public FishingGame game;
            public SpriteRenderer spriteRenderer;
            public CircleCollider2D circleTrigger;
            public CircleCollider2D circleTile;
            public ContactFilter2D filterPlayer;
            public ContactFilter2D filterTiles;

            private float timeHit;

            private void FixedUpdate()
            {
                if
                (
                    Minigame.fixedDeltaTimeElapsed - this.timeHit > 1f &&
                    this.spriteRenderer.color.a < 1f
                )
                {
                    this.spriteRenderer.color += new Color(0, 0, 0, 0.1f);
                }
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                this.game.playerHook.positionDeath = this.game.playerHook.transform.position;
                this.game.playerHook.isHookDeath = true;
                this.game.playerHook.SetState(PlayerHook.State.Death);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_mine"]);
                var p = this.game.ParticleCreateAndPlayOnce("GLOBAL:enemy_death_2", this.transform.position, this.game.transform);
                p.transform.localScale *= 1.5f;
                this.spriteRenderer.color = new Color(1, 1, 1, 0);
                this.timeHit = Minigame.fixedDeltaTimeElapsed;
            }

        }

    }
}
