using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace fishing
    {
        public class MineProxy : MonoBehaviour
        {
            [NonSerialized] FishingGame game;
            public Rigidbody2D body;
            public float radius;
            public SpriteRenderer spriteRenderer;
            public Transform transformBlip;
            public Transform transformBob;
            public Animated animated;

            public Animated.Animation animActive;
            public Animated.Animation animInactive;
            public Animated.Animation animPreExplode;
            public Animated.Animation animExplosion;

            private Vector3 start;
            private float speed = 0f;
            private bool active;
            private bool exploding;
            private Vector3 prev;
            private float offsetBob;
            private float timeBeep;

            public const float BeepDelay = 0.25f;
            public const float WavelengthBob = 1.8f;
            public const float MagnitudeBob = 0.15f;

            public void Init(FishingGame game)
            {
                this.game = game;
                this.start = this.prev = this.transform.position;
                this.transformBlip.SetParent(game.map.transformObjects);
                this.animated.PlayAndLoop(this.animInactive);
                this.offsetBob = UnityEngine.Random.value * WavelengthBob;
            }

            public void FixedUpdate()
            {
                var hook = this.game.playerHook;
                if
                (
                    hook.state == PlayerHook.State.GoDown &&
                    (hook.transform.position - start).sqrMagnitude < (this.radius + hook.circle.radius) * (this.radius + hook.circle.radius)
                )
                {
                    if(this.active == false)
                    {
                        this.active = true;
                        this.animated.PlayAndLoop(animActive);
                        this.timeBeep = BeepDelay;
                    }
                    this.speed += (0.04f - this.speed) * 0.5f;
                    this.body.MovePosition(Vector2.MoveTowards(this.body.position, hook.transform.position, this.speed));
                    this.timeBeep -= Time.fixedDeltaTime;
                    if(this.timeBeep <= 0)
                    {
                        this.timeBeep = BeepDelay;
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_proxymine_beep"]);
                    }
                }
                else if(this.exploding == false)
                {
                    
                        if(this.active == true)
                        {
                            this.active = false;
                            this.animated.PlayAndLoop(this.animInactive);
                        }

                        var dest = Vector2.MoveTowards(this.body.position, this.start, this.speed);

                        if(((Vector3)this.body.position - start).sqrMagnitude > 0.01f)
                            this.speed += (0.025f - this.speed) * 0.5f;
                        else
                        {
                            this.speed *= 0.5f;
                            if(this.spriteRenderer.color.a < 1f)
                                this.spriteRenderer.color += new Color(0, 0, 0, 0.1f);
                        }
                        this.body.MovePosition(dest);

                }
                var travel = (this.transform.position - this.prev).x;
                if(travel != 0)
                    this.spriteRenderer.flipX = travel > 0;
                this.prev = this.transform.position;

                this.transformBob.localPosition = Shake.Wave(
                        Time.timeSinceLevelLoad + this.offsetBob,
                        WavelengthBob,
                        MagnitudeBob
                    ) * Vector3.up;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                var hook = collision.GetComponent<PlayerHook>();
                if(hook != null)
                {
                    hook.positionDeath = hook.transform.position;
                    this.game.playerHook.isHookDeath = true;
                    hook.SetState(PlayerHook.State.Death);
                    this.exploding = true;
                    this.animated.PlayOnce(this.animPreExplode, () => {
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_mine"]);
                        Particle.CreateAndPlayOnce(this.animExplosion, this.body.position, this.game.transform);
                        this.spriteRenderer.color = new Color(1, 1, 1, 0);
                        this.animated.PlayAndLoop(this.animInactive);
                        this.exploding = false;
                    });
                }
            }
        }
    }
}