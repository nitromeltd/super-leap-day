﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class PlayerHook : MonoBehaviour
        {
            //const bool debug = false;

            [NonSerialized] public FishingGame game;
            [NonSerialized] public Vector3 endPrev;
            [NonSerialized] public float dirHorizontal;
            [NonSerialized] public float dirVertical;

            public State state;
            public SpriteRenderer spriteRenderer;
            public Animated animated;
            public CircleCollider2D circle;
            public Transform pin;
            public Transform propellerLeft;
            public Transform propellerRight;
            public Rigidbody2D body;
            public ContactFilter2D filterTile;
            public ContactFilter2D filterTrigger;
            public SpriteRenderer spriteRendererBoatPlayer;
            public SpriteRenderer spriteRendererBoatPlayerArm;
            public SpriteRenderer spriteRendererBuoy;
            public SpriteRenderer spriteRendererBuoyFrontRope;
            public SpriteRenderer spriteRendererBuoySeparateRope;
            public SpriteRenderer spriteRendererWheel;
            public SpriteRenderer spriteRendererPropeller;
            public Animated animatedPropeller;
            public Transform containerBoat;
            public List<Line> lines;
            public Line lineCurrent;
            public float timeBump;
            public AnimationCurve animationCurveBuoy;
            public Sprite[] wheelFrames;
            public Vector3 cranePosition;
            public Sprite spriteHook;
            public Sprite spritePropeller;
            public PlayerInBoat playerInBoat;
            public Animated.Animation animPropeller;
            public Animated.Animation animHookBreak;
            public Animated.Animation animHookBreakParticle1;
            public Animated.Animation animHookBreakParticle2;
            [NonSerialized] public Grabber grabber;
            [NonSerialized] public Chest chest;
            [NonSerialized] public Vector3 positionDeath;
            [NonSerialized] public bool isHookDeath;

            private WallScrape scrapeVert;
            private WallScrape scrapeSlope;
            private WallScrape scrapePoint;
            private Vector3 speedCurrent;
            private List<Motor> motorsPaused;
            private Tween tweenBuoy;
            private Tween tweenScale;
            private Tween.Ease easeBuoy;
            private Coroutine coroutineRopeWhiteOut;
            private float timeOfSplash;
            private bool isHookStuck;

            const float SpeedVertical = 0.05f;
            const float DropGap = 1f;
            const float UpSpeed = 0.2f;
            const float SpeedHorizontal = 0.045f;
            const float CutPadding = 0.1f;
            const float StartOffset = 2f;
            const float DurationBump = 1f;
            const float SplashBubbleDuration = 0.5f;

            public const float BoatAnchorStartY = 3.5f;
            public Vector3 HookStart() => this.cranePosition + Vector3.down * 2;

            static Vector2[] BendWalk = new[]
            {
                Vector2.up,
                (Vector2.up + Vector2.right).normalized,
                Vector2.right,
                (Vector2.right + Vector2.down).normalized,
                Vector2.down,
                (Vector2.down + Vector2.left).normalized,
                Vector2.left,
                (Vector2.left + Vector2.up).normalized
            };


            public enum State
            {
                Wait, GoDown, Death, Grabbed, Win
            }

            public void Init(FishingGame game, GameObject obj)
            {
                this.game = game;
                this.lineCurrent = new Line { lineRenderer = GetComponentInChildren<LineRenderer>() };
                this.lines = new List<Line> { lineCurrent };
                this.lineCurrent.lineRenderer.sortingLayerName = game.objectLayers.playerSortingLayer;
                this.lineCurrent.lineRenderer.sortingOrder = 9;
                this.motorsPaused = new List<Motor>();
                this.easeBuoy = Easing.ByAnimationCurve(this.animationCurveBuoy);
                this.cranePosition = obj.transform.position;
                this.tweenScale = new Tween(Vector3.zero, Vector3.one, 1f, this.easeBuoy);
                this.scrapeVert = new WallScrape(Instantiate(this.game.assets.prefabLookup["WallScrape"], this.game.transform), this.game.objectLayers);
                this.scrapeSlope = new WallScrape(Instantiate(this.game.assets.prefabLookup["WallScrape"], this.game.transform), this.game.objectLayers);
                this.scrapePoint = new WallScrape(Instantiate(this.game.assets.prefabLookup["WallScrapeRound"], this.game.transform), this.game.objectLayers);
                
                this.playerInBoat.SetCharacter(this.game.character);

                SetState(State.Wait);
            }

            public void SetState(State newState)
            {
                //Debug.Log(newState);
                this.spriteRenderer.color = Color.white;

                switch(newState)
                {
                    case State.Wait:
                        TintLine(Color.white, 0);
                        HookTint(0, false);
                        this.body.simulated = false;
                        this.lineCurrent.Reset(this.cranePosition, HookStart());
                        this.dirHorizontal = 1;
                        this.dirVertical = -1;
                        this.transform.localRotation = Quaternion.identity;
                        this.transform.localScale = Vector3.one;
                        this.tweenScale.count = this.tweenScale.delay;
                        this.spriteRenderer.sprite = this.spriteHook;
                        this.spriteRendererPropeller.sprite = this.spritePropeller;
                        this.spriteRendererPropeller.flipX = true;
                        this.spriteRendererPropeller.enabled = true;
                        this.animatedPropeller.Stop();
                        this.transform.position = this.lineCurrent.End;
                        this.timeBump = 0;
                        this.isHookDeath = false;
                        this.timeOfSplash = -SplashBubbleDuration;
                        this.speedCurrent = Vector2.zero;
                        this.spriteRendererBuoy.enabled =
                            this.spriteRendererBuoyFrontRope.enabled =
                            this.spriteRendererBuoySeparateRope.enabled = false;
                        this.tweenBuoy = null;
                        if(this.coroutineRopeWhiteOut != null)
                        {
                            StopCoroutine(this.coroutineRopeWhiteOut);
                            this.coroutineRopeWhiteOut = null;
                        }
                        foreach(var m in this.motorsPaused)
                            m.paused = false;
                        this.motorsPaused.Clear();
                        this.playerInBoat.PlayIdle();
                        UpdateLineRenderers();
                        break;
                    case State.GoDown:
                        this.body.simulated = true;
                        this.animatedPropeller.PlayAndLoop(this.animPropeller);
                        break;
                    case State.Grabbed:
                        this.body.simulated = false;
                        break;
                    case State.Death:
                        if(this.state == State.Death || this.state == State.Win || this.game.IsComplete() || this.game.IsFailed()) return;
                        this.game.coroutineForCamera = StartCoroutine(DeathSequence());
                        break;
                    case State.Win:
                        if(this.state == State.Death || this.state == State.Win || this.game.IsComplete() || this.game.IsFailed()) return;
                        this.game.coroutineForCamera = StartCoroutine(WinSequence());
                        break;
                }
                this.state = newState;
            }

            public void Update()
            {
                // boat bob
                containerBoat.localPosition = Shake.Wave(Time.timeSinceLevelLoad, 2f, 0.2f) * Vector3.up;
                scrapeVert.Advance();
                scrapeSlope.Advance();
                scrapePoint.Advance();
            }

            public void UpdateWaitHook()
            {
                this.transform.position = this.lineCurrent.End + this.containerBoat.localPosition;
                UpdateLineRenderers();
            }

            public void Advance()
            {
                // when exiting a portal
                if(this.tweenScale.Done() == false)
                {
                    this.tweenScale.Advance(1 / 60f);
                    this.transform.localScale = this.tweenScale.Position();
                }
                if(this.isHookStuck == true)
                    HookTint(0.25f + Shake.Wave(Time.timeSinceLevelLoad, 0.3f, 0.25f));
                else
                    HookTint(0f);


                switch(this.state)
                {
                    case State.Wait:
                        UpdateWaitHook();
                        if(this.game.input.pressed == true)
                        {
                            this.game.SpendHook();
                            this.playerInBoat.PlayJumpUp();
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_anchor_down"]);
                            SetState(State.GoDown);
                        }
                        break;
                    case State.Grabbed:
                    case State.GoDown:
                        if(this.game.input.pressed == true)
                        {
                            this.dirHorizontal *= -1;
                            this.spriteRendererPropeller.flipX = this.dirHorizontal == 1;
                            if(this.state == State.Grabbed)
                            {
                                this.grabber.Release();
                            }
                        }

                        MoveLineEnd(this.transform.position, 0.75f);
                        
                        CollisionChecks();
                        if(this.state == State.Death)
                            break;

                        if(this.state == State.GoDown)
                            MoveBody();
                        else if(this.state == State.Grabbed)
                        {
                            if(this.grabber.state == Grabber.State.Grab)
                            {
                                this.transform.position += (this.grabber.transform.position - this.transform.position) * 0.25f;
                                if((this.transform.position - this.grabber.transform.position).sqrMagnitude < 0.25f)
                                {
                                    this.grabber.Close();
                                    this.transform.position = this.grabber.transform.position;
                                }
                            }
                            else
                                this.transform.position = this.grabber.transform.position;
                        }
                        if(this.state == State.Death)
                            break;


                        //// debug bending
                        //if(game.input.pressed)
                        //{
                        //    var end = game.input.position;
                        //    this.body.MovePosition(end);
                        //}
                        UpdateLineRenderers();

                        // bubbles
                        if(this.body.position.y < this.game.waterY && (Minigame.frameCount % 4) == 0)
                        {
                            var speed = Vector2.left * this.dirHorizontal * FishingGame.BubbleSpeedDefault;
                            speed.y = speedCurrent.y * 0.5f;
                            var left = this.dirHorizontal > 0;
                            if(this.dirVertical > 0) left = !left;
                            this.game.CreateBubble(left ? this.propellerLeft.position : this.propellerRight.position, speed, FishingGame.BubbleSpeedDefault * 0.3f, "bubble_hook");
                        }
                        // splash bubbles
                        if(Time.timeSinceLevelLoad - this.timeOfSplash < SplashBubbleDuration)
                        {
                            SplashBubble();
                        }
                        // dir-up buoy
                        if(this.tweenBuoy != null && this.tweenBuoy.Done() == false)
                        {
                            this.tweenBuoy.Advance(Time.fixedDeltaTime);
                            this.spriteRendererBuoyFrontRope.transform.localScale = 
                                this.spriteRendererBuoy.transform.localScale = this.tweenBuoy.Position();
                        }
                        // splash impact with water
                        if(this.transform.position.y < this.game.waterY - 0.25f && this.endPrev.y >= this.game.waterY - 0.25f)
                        {
                            for(int i = 0; i < 12; i++)
                                SplashBubble();
                            this.game.waterline.heightmap.Splash(GetRect(), -2f);
                            this.timeOfSplash = Time.timeSinceLevelLoad;
                            this.game.ParticleCreateAndPlayOnce("splash", new Vector3(this.transform.position.x, this.game.waterY));
                        }
                        
                        break;
                    case State.Death:

                        break;
                }

                this.endPrev = this.body.position;
            }

            private void LerpHookRotation(float angle)
            {
                this.transform.localRotation = Quaternion.Euler(0, 0, angle);
                this.spriteRendererBuoy.transform.localRotation =
                    this.spriteRendererBuoySeparateRope.transform.localRotation = Quaternion.Euler(0, 0, -angle);
            }

            private void SplashBubble()
            {
                var pos = (Vector2)this.transform.position + UnityEngine.Random.insideUnitCircle * this.circle.radius;
                if(pos.y > this.game.waterY) pos.y = this.game.waterY;
                var bubble = this.game.CreateBubble(pos, UnityEngine.Random.insideUnitCircle * 0.05f, FishingGame.BubbleSpeedDefault * 0.3f, "bubble_hook");
                if(bubble.velocity.y > 0) bubble.velocity.y *= -1f;
                bubble.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
            }

            // Interpolate line end to target whilst testing for bends
            public void MoveLineEnd(Vector3 target, float stepSize)
            {
                //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.red);

                int breaker = 0;
                var bendFound = false;

                while((target - this.lineCurrent.End).sqrMagnitude > 0.001f || bendFound)
                {

                    if(breaker++ > 100)
                    {
                        Debug.LogWarning("Line bend resolve took over 100 steps");
                        break;
                    }

                    bendFound = false;

                    var endPrev = this.lineCurrent.End;
                    this.lineCurrent.End = Vector3.MoveTowards(this.lineCurrent.End, target, stepSize);

                    //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.blue);

                    if(this.lineCurrent.points.Count > 2)
                        UnbendCheck();

                    var endVector = this.lineCurrent.EndVector();
                    var endVectorLeft = Vector2.Perpendicular(endVector).normalized;
                    {
                        var raycast = RaycastFiltered(this.lineCurrent.BeforeEnd, endVector.normalized, this.filterTile);
                        if(raycast.HasValue && raycast.Value.distance < endVector.magnitude)
                        {
                            //Minigame.DebugX(raycast.Value.point, Color.red, 0.1f);

                            BendLine(raycast.Value, endVectorLeft);
                            bendFound = true;
                            // the bend creates a new line - we must backtrack to test it
                            this.lineCurrent.End = endPrev;

                            //Debug.DrawLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, Color.red);
                            //Debug.Break();
                        }
                    }
                }
            }

            public void ChangeDirVertical(float dir, List<Vector2Int> cells)
            {
                if(dir == this.dirVertical)
                {
                    if(dir < 0)
                    {
                        foreach(var p in cells)
                            if(this.game.changeDirDownLookup.TryGetValue(p, out PlayAnimated pa) == true)
                                pa.PlayOnce();
                    }
                    return;
                }
                if(dir > 0)
                {
                    this.tweenBuoy = new Tween(Vector3.zero, Vector3.one, 0.5f, this.easeBuoy);
                    this.spriteRendererBuoy.transform.localScale =
                        this.spriteRendererBuoyFrontRope.transform.localScale = Vector3.zero;
                    this.spriteRendererBuoy.enabled =
                        this.spriteRendererBuoyFrontRope.enabled = true;
                    var p = RandomUtil.Pick(cells);
                    if(this.game.changeDirUpLookup.TryGetValue(p, out PlayAnimated pa) == true)
                        pa.PlayOnce();
                    // sfx?
                }
                else
                {
                    this.coroutineRopeWhiteOut = StartCoroutine(ChangeDirDown(cells));
                    
                }
                this.dirVertical = dir;
            }
            IEnumerator ChangeDirDown(List<Vector2Int> cells)
            {
                this.spriteRendererBuoy.enabled = false;
                var particle = Particle.CreateWithSprite(
                    this.game.assets.spriteLookup["Buoy Isolated"], 0,
                    this.spriteRendererBuoy.transform.position,
                    this.game.transform
                );
                particle.lifetime = null;
                particle.velocity = new Vector2(0, 0.05f);
                particle.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                particle.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;
                this.game.buoys.Add(particle);
                foreach(var p in cells)
                {
                    for(int x = -1; x < 2; x++)
                        if(this.game.changeDirDownLookup.TryGetValue(p + Vector2Int.right * x, out PlayAnimated pa) == true)
                            pa.PlayOnce();
                }
                this.spriteRendererBuoySeparateRope.enabled = true;
                var tweenShrink = new Tween(Vector3.one, Vector3.zero, 0.5f, 1f);
                while(tweenShrink.Done() == false)
                {
                    tweenShrink.Advance(Time.fixedDeltaTime);
                    this.spriteRendererBuoySeparateRope.transform.localScale =
                        this.spriteRendererBuoyFrontRope.transform.localScale = tweenShrink.Position();
                    yield return null;
                }
                this.spriteRendererBuoySeparateRope.enabled =
                   this.spriteRendererBuoyFrontRope.enabled = false;
                this.coroutineRopeWhiteOut = null;
            }

            public void CollisionChecks()
            {
                // check for cell collision
                var cuts = new List<Vector2>();
                foreach(var line in lines)
                {
                    line.DoAtEachCell((cell, segment) =>
                    {
                        var c = Color.blue;

                        // look for spikes
                        if(this.game.spikesLookup.ContainsKey(cell) == true)
                        {
                            var delta = segment.b - segment.a;
                            var spikeLine = Line.LineFromSideOfACell(cell, FishingGame.SpikeIndexToDir(this.game.spikesLookup[cell]));

                            if(Line.GetLineIntersection(spikeLine.a, spikeLine.b, segment.a, segment.b, out Vector2 ip) == true)
                            {
                                c = Color.red;
                                //Minigame.DebugX(ip, Color.white);
                                cuts.Add(ip);
                            }
                        }
                        // look for buttons
                        if(this.game.triggerButtonLookup.TryGetValue(cell, out TriggerButton triggerButton) == true)
                        {
                            if(triggerButton.enabled == true)
                            {
                                var raycast = RaycastFiltered(segment.a, (segment.b - segment.a).normalized, this.filterTrigger);
                                if(raycast.HasValue && raycast.Value.collider == triggerButton.trigger)
                                    triggerButton.Hit();
                            }
                        }
                        //Minigame.DebugRect(new Rect(cell.x, cell.y, 1, 1), c);
                    });
                }
                if(cuts.Count > 0)
                {
                    this.positionDeath = cuts[0];
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                    SetState(State.Death);
                    return;
                }
                // check for spikes against hook
                var tilePos = Minigame.ToVector2Int(this.body.position);
                if(this.game.spikesLookup.ContainsKey(tilePos))
                {
                    this.positionDeath = this.transform.position;
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                    this.game.playerHook.isHookDeath = true;
                    SetState(State.Death);
                    return;
                }
                // check for coins
                this.game.pickupMap.CircleCollidePickup(this.transform.position, this.circle.radius);
            }

            public void MoveBody()
            {
                // bump check
                var end = (Vector3)this.body.position;
                var travel = end - endPrev;
                var travelMagnitude = travel.magnitude;
                if(travelMagnitude < SpeedVertical * 0.2f)
                {
                    this.timeBump += Time.fixedDeltaTime;
                    if(this.timeBump > Time.fixedDeltaTime * 3)
                        this.isHookStuck = true;
                    if(this.timeBump > DurationBump)
                    {
                        this.positionDeath = this.transform.position;
                        this.game.playerHook.isHookDeath = true;
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                        SetState(State.Death);
                        return;
                    }
                }
                else
                {
                    this.timeBump = 0;
                    this.isHookStuck = false;
                }

                // execute movement for next frame
                // read flow map
                var flowCells = PickupMap.GetCircleCollideCells(this.game.flowLookup, this.transform.position, this.circle.radius);
                var boost = Vector2.zero;
                foreach(var c in flowCells)
                    if(this.game.flowLookup[c].isOn == true)
                        boost += this.game.flowLookup[c].speed;
                boost.Normalize();

                boost *= SpeedVertical;
                boost.y *= 2f; // strong vertical flow

                // read dir map - take orders from most cells of one type
                var dirCells = PickupMap.GetCircleCollideCells(this.game.dirLookup, this.transform.position, this.circle.radius * 0.75f);
                var downs = new List<Vector2Int>();
                var ups = new List<Vector2Int>();
                foreach(var c in dirCells)
                {
                    var dir = this.game.dirLookup[c];
                    if(dir > 0) ups.Add(c);
                    if(dir < 0) downs.Add(c);
                }
                if((ups.Count != 0 || downs.Count != 0) && ups.Count != downs.Count)
                    ChangeDirVertical(ups.Count > downs.Count ? 1 : -1, ups.Count > downs.Count ? ups : downs);

                var speedTarget = (Vector3.up * SpeedVertical * this.dirVertical) + (Vector3.right * this.dirHorizontal * SpeedHorizontal) + (Vector3)boost;

                // cancel x movement next to walls
                if(this.dirHorizontal > 0)
                {
                    var raycast = RaycastFiltered(this.body.position, Vector2.right, this.filterTile);
                    if(raycast.HasValue == true && raycast.Value.distance < this.circle.radius + 0.02f)
                    {
                        if(raycast.Value.normal.x == -this.dirHorizontal)
                            this.scrapeVert.Track(raycast.Value.point, this.dirHorizontal);
                        speedTarget.x = 0;
                    }
                }
                if(this.dirHorizontal < 0)
                {
                    var raycast = RaycastFiltered(this.body.position, Vector2.left, this.filterTile);
                    if(raycast.HasValue == true && raycast.Value.distance < this.circle.radius + 0.02f)
                    {
                        if(raycast.Value.normal.x == -this.dirHorizontal)
                            this.scrapeVert.Track(raycast.Value.point, this.dirHorizontal);
                        speedTarget.x = 0;
                    }
                }

                // cancel x above water
                if(this.body.position.y > this.game.waterY)
                    speedTarget.x = 0;

                // boost if we're stuck on a slope
                if(Mathf.Abs(travel.y) < SpeedVertical * 0.5f)
                {
                    if(boost.sqrMagnitude > 0) speedTarget += (Vector3)boost;
                    else speedTarget.y += SpeedVertical * 2 * dirVertical;
                }
                this.speedCurrent += (speedTarget - speedCurrent) * 0.1f;

                // drop fast into water
                if(this.transform.position.y > this.game.waterY - DropGap)
                    this.speedCurrent += Vector3.down * SpeedVertical * 0.25f;

                end += this.speedCurrent;

                // rotate hook to match line
                var target = 0f;
                if(this.body.position.y < this.game.waterY)
                {
                    if(this.dirHorizontal > 0)
                        target = 35f;
                    else if(this.dirHorizontal < 0)
                        target = -35f;
                }

                LerpHookRotation(Mathf.LerpAngle(this.transform.rotation.eulerAngles.z, target, 0.1f));

                // body.position won't be updated till next frame
                this.body.MovePosition(end);
            }

            public void BendLine(RaycastHit2D raycast, Vector2 endVectorLeft)
            {
                var cut = raycast.point;
                Vector2[] points;

                Vector2[] boxPoints(BoxCollider2D box)
                {
                    Vector2 extents = box.bounds.extents;
                    Vector2 offset = box.offset;
                    return new[]
                    {
                        offset + new Vector2(extents.x, extents.y),
                        offset + new Vector2(extents.x, -extents.y),
                        offset + new Vector2(-extents.x, -extents.y),
                        offset + new Vector2(-extents.x, extents.y)
                    };
                }

                if(raycast.collider is BoxCollider2D)
                    points = boxPoints((BoxCollider2D)raycast.collider);
                else if(raycast.collider is PolygonCollider2D)
                    points = ((PolygonCollider2D)raycast.collider).points;
                else if(raycast.collider is EdgeCollider2D)
                    points = ((EdgeCollider2D)raycast.collider).points;
                else if(raycast.collider is CircleCollider2D)
                {
                    // the ray passes which side of the center?
                    var circle = (CircleCollider2D)raycast.collider;
                    var centerToCast = cut - ((Vector2)raycast.collider.transform.position + circle.offset);
                    points = new[] { endVectorLeft * Mathf.Sign(Vector2.Dot(centerToCast, endVectorLeft)) * circle.radius };
                }
                else
                    return;

                // move points to transform
                for(int i = 0; i < points.Length; i++)
                    points[i] = raycast.collider.transform.TransformPoint((Vector3)points[i]);
                
                //Minigame.DebugPolygon(points, Color.red);

                // get the vertex we want to wrap around
                Vector2 vertex = Vector2.zero;
                if(points.Length == 1)
                    vertex = points[0];
                else if(points.Length == 2)
                    vertex = Line.GetNearestPointToLine(this.lineCurrent.BeforeEnd, this.lineCurrent.End, points);
                else
                    vertex = Line.GetBendVertex(points, this.lineCurrent.BeforeEnd, this.lineCurrent.End, raycast.point);

                //Minigame.DebugX(vertex, Color.black);

                // walk 8 steps around edge to create a push-out vector
                Vector2 bendVectorSum = Vector2.zero;
                for(int i = 0; i < BendWalk.Length; i++)
                {
                    if(Physics2D.OverlapPoint(vertex + BendWalk[i] * CutPadding * 2, this.filterTile.layerMask) == null)
                        bendVectorSum += BendWalk[i];
                }
                bendVectorSum.Normalize();

                var bendDir = Mathf.Sign(Vector2.Dot(bendVectorSum, endVectorLeft));
                this.lineCurrent.AddBend(bendDir, vertex + bendVectorSum * CutPadding);

                PauseMotor(raycast.collider.transform);
            }

            public void UnbendCheck()
            {
                var bends = this.lineCurrent.bends;
                var points = this.lineCurrent.points;
                var prevVector = points[^2] - points[^3];
                var dot = Vector2.Dot(Vector2.Perpendicular(prevVector), this.lineCurrent.EndVector());
                if(bends[^1] > 0 == dot > 0)
                {
                    var end = this.lineCurrent.End;
                    bends.RemoveAt(bends.Count - 1);
                    points.RemoveAt(points.Count - 1);
                    this.lineCurrent.End = end;
                }
            }

            public void SplitLine(Vector3 newStart)
            {
                var lineRendererCopy = Instantiate(lineCurrent.lineRenderer.gameObject, this.lineCurrent.lineRenderer.transform.parent);
                var nextLine = new Line { lineRenderer = lineCurrent.lineRenderer };
                lineCurrent.lineRenderer = lineRendererCopy.GetComponent<LineRenderer>();
                lineCurrent = nextLine;
                lines.Add(lineCurrent);
                this.transform.position = newStart;
                this.lineCurrent.points.Add(newStart);
                this.lineCurrent.points.Add(newStart);
            }

            public void EnterPortal(Vector3 start, Vector3 end)
            {
                // offset lineRenderer to connect to center
                this.lineCurrent.drawEnd = start;
                SplitLine(end);
                for(int i = 0; i < 12; i++)
                    SplashBubble();
                this.timeOfSplash = Time.timeSinceLevelLoad;
                this.tweenScale.count = 0;
            }

            public void RemoveLastLine()
            {
                Destroy(this.lineCurrent.lineRenderer.gameObject);
                this.lines.RemoveAt(this.lines.Count - 1);
                this.lineCurrent = this.lines[^1];
            }

            public List<Vector2> GetSegmentIntersectionPoints(Vector2 a1, Vector2 b1)
            {
                List<Vector2> result = new List<Vector2>();
                foreach(var line in lines)
                {
                    line.DoAtEachSegment((a2, b2) =>
                    {
                        if(Line.GetLineIntersection(a1, b1, a2, b2, out Vector2 ip) == true)
                        {
                            Minigame.DebugX(ip, Color.white);
                            result.Add(ip);
                        }
                    });
                }
                return result;
            }

            public void DoAtEachSegment(Action<Vector2, Vector2> action)
            {
                foreach(var line in lines)
                    line.DoAtEachSegment(action);
            }

            public void UpdateLineRenderers()
            {
                var totalLengthNext = 0f;
                var totalLength = lines[0].Length();
                lines[0].drawStart = cranePosition + this.containerBoat.localPosition;
                for(int i = this.lines.Count - 1; i > 0; i--)
                {
                    totalLengthNext += lines[i].Length();
                    lines[i - 1].UpdateLineRenderer(totalLengthNext);
                }
                totalLength += totalLengthNext;
                // offset lineRenderer to connect to pin on gfx
                this.lineCurrent.drawEnd = this.pin.position;
                this.lineCurrent.UpdateLineRenderer();
                this.spriteRendererWheel.sprite = this.wheelFrames[^(Mathf.FloorToInt(totalLength * 10) % this.wheelFrames.Length + 1)];
            }

            public void TintLine(Color color, float intensity)
            {
                foreach(var line in this.lines)
                {
                    line.lineRenderer.material.SetColor("_Color", color);
                    line.lineRenderer.material.SetFloat("_Intensity", intensity);
                }
            }
            public void HookTint(float intensity, bool animate = true)
            {
                float v;
                if(animate == true)
                    v = Mathf.MoveTowards(this.spriteRenderer.material.GetFloat("_Intensity"), intensity, 0.2f);
                else
                    v = intensity;
                this.spriteRenderer.material.SetFloat("_Intensity", v);
                this.spriteRendererPropeller.material.SetFloat("_Intensity", v);
            }

            public Rect GetRect() => new Rect((Vector2)this.transform.position - this.circle.radius * Vector2.one, this.circle.radius * Vector2.one * 2f);

            public static RaycastHit2D? RaycastFiltered(Vector2 origin, Vector2 direction, ContactFilter2D filter)
            {
                var results = new List<RaycastHit2D>();
                int total = Physics2D.Raycast(origin, direction, filter, results);
                if(total > 0)
                    return results[0];
                return null;
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                PauseMotor(collision.transform);
            }


            public void PauseMotor(Transform t)
            {
                var m = this.game.GetMotor(t);
                if(m != null && m.paused == false)
                {
                    m.paused = true;
                    this.motorsPaused.Add(m);
                }

            }

            private IEnumerator DeathSequence()
            {
                this.body.simulated = false;
                this.isHookStuck = false;
                this.lineCurrent.End = this.body.position;
                HookTint(0, false);
                UpdateLineRenderers();

                var pos = this.positionDeath;
                if(this.game.hooksLeft <= 0)
                    pos += Vector3.up * 3f;

                if(this.isHookDeath == true)
                {
                    this.spriteRendererPropeller.enabled = false;
                    this.animated.PlayOnce(this.animHookBreak);
                    var p1 = Particle.CreateAndPlayOnce(this.animHookBreakParticle1, this.transform.position, this.game.transform);
                    var p2 = Particle.CreateAndPlayOnce(this.animHookBreakParticle2, this.transform.position, this.game.transform);
                    p1.spriteRenderer.sortingLayerName =
                        p2.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                    p1.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 1;
                    p2.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder + 1;
                }

                yield return this.game.camCtrl.TweenTo(pos, Vector3.zero, 0.25f, Easing.QuadEaseOut, this.game.UpdateEnumerator);

                var delay = 0.1f;
                var highlight = this.game.ParticleCreateAndPlayOnce("death_highlight", this.positionDeath);
                highlight.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
                highlight.spriteRenderer.sortingOrder = this.spriteRenderer.sortingOrder - 3;
                highlight.transform.localScale = Vector3.one * 1.5f;
                for(int i = 0; i < 3; i++)
                {
                    TintLine(Color.white, 0.7f);
                    yield return game.DoWhileCoroutine(delay * 2, game.UpdateEnumerator);
                    TintLine(Color.white, 0f);
                    yield return game.DoWhileCoroutine(delay, game.UpdateEnumerator);

                }
                if(this.game.hooksLeft <= 0)
                {
                    this.game.FailLevel();
                    yield break;
                }

                var lineThickness = this.lines[0].lineRenderer.startWidth;
                var copy = Instantiate(this, this.transform.parent);
                var srs = new List<SpriteRenderer>();
                var lrs = new List<LineRenderer>();
                srs.Add(copy.GetComponent<SpriteRenderer>());
                foreach(Transform t in copy.transform)
                {
                    var sr = t.GetComponent<SpriteRenderer>();
                    if(sr != null) srs.Add(sr);
                    var lr = t.GetComponent<LineRenderer>();
                    if(lr != null) lrs.Add(lr);
                }
                Destroy(copy.GetComponent<PlayerHook>());

                while(this.lines.Count > 1)
                    RemoveLastLine();

                this.lineCurrent.lineRenderer.gameObject.SetActive(false);
                this.spriteRenderer.enabled = false;

                var tween = new Tween(1f, 0, 0.5f);

                void CamUpdate()
                {
                    this.game.UpdateEnumerator();
                    tween.Advance(1f / 60);
                    foreach(var sr in srs)
                        sr.color = new Color(1, 1, 1, tween.Value());
                    foreach(var lr in lrs)
                        lr.startWidth = lr.endWidth = tween.Value() * lineThickness;
                }

                yield return this.game.camCtrl.TweenTo(HookStart(), Vector3.zero, 0.5f, Easing.QuadEaseOut, CamUpdate);

                this.lineCurrent.lineRenderer.gameObject.SetActive(true);
                this.spriteRenderer.enabled = true;
                Destroy(copy);

                SetState(State.Wait);

                this.game.coroutineForCamera = null;
            }

            private IEnumerator WinSequence()
            {
                this.body.simulated = false;
                this.lineCurrent.End = this.body.position;
                UpdateLineRenderers();

                void UpdateCamera()
                {
                    this.lineCurrent.End = this.body.position;
                    UpdateLineRenderers();
                    this.game.camCtrl.SetPosition(
                        this.game.cam.transform.position +
                        ((this.transform.position + Vector3.up * 1f) - this.game.cam.transform.position) * 0.05f, Vector3.zero
                    );
                    LerpHookRotation(0);
                }

                yield return null;

                var tweener = new Tweener();
                tweener.Move(this.transform, this.chest.PullPosition(), 0.5f, Easing.QuadEaseOut);
                while(tweener.IsDone() == false)
                {
                    tweener.Advance(1 / 60f);
                    game.UpdateEnumerator();
                    UpdateCamera();
                    yield return null;
                }
                this.chest.Open();

                tweener = new Tweener();
                tweener.Move(this.chest.transformChest, this.chest.transformChest.position + Vector3.up * 1f, 1f, Easing.QuadEaseOut);
                tweener.Move(this.transform, this.transform.position + Vector3.up * 1f, 1f, Easing.QuadEaseOut);
                while(tweener.IsDone() == false)
                {
                    tweener.Advance(1 / 60f);
                    game.UpdateEnumerator();
                    UpdateCamera();
                    yield return null;
                }

                this.game.CompleteLevel();
                this.game.coroutineForCamera = null;
            }
            private void OnCollisionStay2D(Collision2D collision)
            {
                var normal = collision.contacts[0].normal;
                // scrape?
                if(normal.y != 0)
                {
                    var point = collision.contacts[0].point;
                    Minigame.SideNormals(normal, out Vector3 left, out Vector3 right);
                    // get surface type
                    var rayA = RaycastFiltered(this.body.position + (Vector2)left * 0.2f, -normal, this.filterTile);
                    var rayB = RaycastFiltered(this.body.position + (Vector2)right * 0.2f, -normal, this.filterTile);

                    //Debug.Log($"{rayA.Value}");

                    if(rayA.HasValue && rayB.HasValue)
                    {
                        if(rayA.Value.normal == normal && rayB.Value.normal == normal)
                        {
                            this.scrapeSlope.Track(point, normal);
                        }
                        else
                        {
                            this.scrapePoint.Track(point, normal);
                        }
                    }

                }
            }

            public class WallScrape
            {
                public Transform transform;
                private SpriteRenderer spriteRenderer;
                private bool isTracking;

                public WallScrape(GameObject obj, ObjectLayers objectLayers)
                {
                    this.spriteRenderer = obj.GetComponentInChildren<SpriteRenderer>();
                    this.transform = obj.transform;
                    ObjectLayers.SetSortingLayer(obj, objectLayers.backgroundSortingLayer);
                    //ObjectLayers.SetSortingLayer(obj, objectLayers.foregroundSortingLayer);// debug
                    this.spriteRenderer.color = new Color(1, 1, 1, 0);
                    this.transform.localScale = Vector3.zero;
                }

                public void Advance()
                {
                    if(this.isTracking == true)
                    {
                        if(this.spriteRenderer.color.a < 1f)
                            this.spriteRenderer.color += new Color(0, 0, 0, 0.05f);
                        if(this.transform.localScale.x < 1f)
                            this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector3.one, 0.05f);
                    }
                    else
                    {
                        if(this.spriteRenderer.color.a > 0f)
                            this.spriteRenderer.color -= new Color(0, 0, 0, 0.05f);
                        if(this.transform.localScale.x > 0f)
                            this.transform.localScale = Vector3.MoveTowards(this.transform.localScale, Vector3.zero, 0.05f);
                    }
                    this.isTracking = false;
                }

                public void Track(Vector3 pos, float x)
                {
                    var travel = pos - this.transform.position;
                    if(travel.sqrMagnitude > 0.0001f)
                    {
                        this.isTracking = true;
                        if(travel.y > 0)
                        {
                            this.spriteRenderer.flipX = x > 0;
                            this.transform.localEulerAngles = new Vector3(0, 0, 180);
                        }
                        else
                        {
                            this.spriteRenderer.flipX = x < 0;
                            this.transform.localEulerAngles = new Vector3(0, 0, 0);
                        }
                    }
                    this.transform.position = pos;
                }

                public void Track(Vector3 pos, Vector2 normal)
                {
                    var rotation = Mathf.Atan2(-normal.y, -normal.x) * Mathf.Rad2Deg;

                    var travel = pos - this.transform.position;
                    if(travel.sqrMagnitude > 0.0001f)
                    {
                        this.isTracking = true;
                        if(travel.y < 0)
                        {
                            this.spriteRenderer.flipX = normal.x > 0;
                            this.transform.localEulerAngles = new Vector3(0, 0, normal.x > 0 ? rotation + 180 : rotation);
                        }
                        else
                        {
                            this.spriteRenderer.flipX = normal.x < 0;
                            this.transform.localEulerAngles = new Vector3(0, 0, normal.x > 0 ? rotation : rotation + 180);
                        }
                    }

                    this.transform.position = pos;
                }
            }

        }
    }
}
