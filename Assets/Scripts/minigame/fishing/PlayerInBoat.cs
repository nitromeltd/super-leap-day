using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        // NB: Non-Yolk character anims are 76.5625 pixels per unit, Yolk is 70ppu instead of 64ppu
        public class PlayerInBoat : MonoBehaviour
        {
            [System.Serializable]
            public class PlayerAnimations
            {
                public Animated.Animation animationIdle;
                public Animated.Animation animationJumpUp;
                public Animated.Animation animationJumpToDown;
                public Animated.Animation animationJumpDown;
                public Sprite spriteSitting;
                public Sprite spriteSittingArm;
            };
            public Character character;
            public PlayerAnimations yolk;
            public PlayerAnimations puffer;
            public PlayerAnimations pufferHead;
            public PlayerAnimations goop;
            public PlayerAnimations sprout;
            public PlayerAnimations king;
            public PlayerAnimations kingHat;
            public Animated.Animation animationKingIdleHat;

            // jumping
            public Animated animated;
            public Animated animatedHead;
            public SpriteRenderer spriteRendererJumping;
            public SpriteRenderer spriteRendererJumpingHead;
            public SpriteRenderer spriteRendererSitting;
            public SpriteRenderer spriteRendererSittingArm;

            public GameObject jumping;
            public GameObject sitting;

            private bool isJumping;
            private Vector3 startJump;
            private float stopCount;
            private const float JumpDelay = 0.5f;
            private const float JumpHeight = 1.875f;

            public PlayerAnimations AnimationsForCharacter(Character character) =>
        character switch
            {
                Character.Puffer => this.puffer,
                Character.Goop => this.goop,
                Character.Sprout => this.sprout,
                Character.King => this.king,
                _ => this.yolk,
            };

            public void SetCharacter(Character character)
            {
                this.character = character;
                this.spriteRendererSitting.sprite = AnimationsForCharacter(this.character).spriteSitting;
                this.spriteRendererSittingArm.sprite = AnimationsForCharacter(this.character).spriteSittingArm;
                var h = this.spriteRendererJumping.sprite.rect.height / this.spriteRendererJumping.sprite.pixelsPerUnit;
                this.spriteRendererJumping.transform.localPosition = new Vector3(0, h / 2);
                if(this.character == Character.King)
                {
                    this.jumping.transform.localPosition = new Vector3(-3.08f, this.jumping.transform.localPosition.y);
                    this.spriteRendererJumpingHead.sortingOrder = this.spriteRendererJumping.sortingOrder - 1;
                }
                startJump = this.jumping.transform.localPosition;
                PlayIdle();
            }
            public void PlayIdle()
            {
                this.isJumping = false;
                this.sitting.SetActive(false);
                this.jumping.SetActive(true);
                this.animated.PlayAndLoop(AnimationsForCharacter(this.character).animationIdle);
                this.spriteRendererJumpingHead.enabled = false;
                if(this.character == Character.King)
                {
                    this.spriteRendererJumpingHead.enabled = true;
                    this.animatedHead.PlayAndLoop(this.kingHat.animationIdle);
                }
            }
            public void PlaySit()
            {
                this.isJumping = false;
                this.sitting.SetActive(true);
                this.jumping.SetActive(false);
            }

            public void PlayJumpUp()
            {
                this.stopCount = 0;
                this.isJumping = true;
                this.animated.PlayAndHoldLastFrame(AnimationsForCharacter(this.character).animationJumpUp);
                this.spriteRendererJumpingHead.enabled = true;

                if(this.character == Character.King)
                    this.animatedHead.PlayAndHoldLastFrame(this.kingHat.animationJumpUp);
                else if(this.character == Character.Puffer)
                    this.animatedHead.PlayAndHoldLastFrame(this.pufferHead.animationJumpUp);
                else
                    this.spriteRendererJumpingHead.enabled = false;
            }

            public void PlayJumpToDown()
            {
                this.animated.PlayOnce(AnimationsForCharacter(this.character).animationJumpToDown, () =>
                {
                    this.animated.PlayOnce(AnimationsForCharacter(this.character).animationJumpDown);
                });

                if(this.character == Character.King)
                    this.animatedHead.PlayOnce(this.kingHat.animationJumpToDown, () =>
                    {
                        this.animatedHead.PlayOnce(this.kingHat.animationJumpDown);
                    });
                else if(this.character == Character.Puffer)
                    this.animatedHead.PlayOnce(this.pufferHead.animationJumpToDown, () =>
                    {
                        this.animatedHead.PlayOnce(this.pufferHead.animationJumpDown);
                    });
            }

            void Update()
            {
                Advance();    
            }

            void Advance()
            {
                if(this.isJumping == true)
                {
                    this.stopCount = Mathf.Min(JumpDelay, this.stopCount + Time.deltaTime);
                    var jumpArc = Mathf.Sin(Mathf.PI * (this.stopCount / JumpDelay));
                    this.jumping.transform.localPosition = this.startJump + Vector3.up * jumpArc * JumpHeight;
                    if(this.stopCount == JumpDelay)
                    {
                        this.jumping.transform.localPosition = this.startJump;
                        PlaySit();
                    }
                    else if(this.animated.currentAnimation == this.AnimationsForCharacter(this.character).animationJumpUp)
                    {
                        if(this.stopCount / JumpDelay >= 0.5f)
                            PlayJumpToDown();
                    }
                }
            }
        }

    }
}
