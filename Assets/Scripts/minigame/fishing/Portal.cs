﻿using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class Portal : MonoBehaviour
        {
            [System.NonSerialized] public FishingGame game;

            public Portal exit;
            public bool locked = false;
            public SpriteRenderer[] spriteRenderers;

            public void Init(FishingGame game, Portal portal)
            {
                this.game = game;
                this.exit = portal;
                portal.exit = this;
                Destroy(portal.GetComponent<CircleCollider2D>());
                portal.transform.localScale *= 0.8f;
            }

            void Update()
            {
                if(this.game == null)
                    return;
                if(this.game.playerHook.state == PlayerHook.State.Wait)
                {
                    this.locked = false;
                }
                for(int i = 0; i < this.spriteRenderers.Length; i++)
                {
                    var sr = this.spriteRenderers[i];
                    var angles = sr.transform.localEulerAngles;
                    sr.transform.localEulerAngles = angles + new Vector3(0, 0, (i + 1) * (exit == null ? 1 : -1));
                    sr.transform.localScale = Vector3.one + (Vector3)Shake.WaveFlex(new Vector2(1, 1), new Vector2(-1, -1), Minigame.fixedDeltaTimeElapsed + i * 0.4f, 3f, 0.2f + i * 0.05f);
                }
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                this.locked = true;
                this.game.playerHook.EnterPortal(this.transform.position, this.exit.transform.position);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_portal"]);
            }

        }

    }
}
