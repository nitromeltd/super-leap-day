using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class SpikeHoop : SpikedItem
        {

            public bool captured;
            public Vector3 start;
            public SpriteRenderer spriteRendererFront;

            private List<Vector3> hoopHits = new List<Vector3>();

            public override void Advance()
            {
                switch(this.game.playerHook.state)
                {
                    case PlayerHook.State.Death:
                    case PlayerHook.State.GoDown:
                        HoopCollision();
                        base.Advance();
                        if(this.captured == false)
                            this.transform.position = Vector3.MoveTowards(this.transform.position, start, 0.075f);
                        break;
                    default:
                        this.captured = false;
                        this.transform.position = Vector3.Lerp(this.transform.position, start, 0.5f);
                        break;
                }
            }

            void HoopCollision()
            {
                this.hoopHits.Clear();
                var a1 = spikes[0].a.position;
                var b1 = spikes[1].a.position;
                var intersects = false;
                this.game.playerHook.DoAtEachSegment((a2, b2) =>
                {
                    if(Line.GetLineIntersection(a1, b1, a2, b2, out Vector2 ip) == true)
                    {
                        this.captured = true;
                        intersects = true;
                    }
                    // maintain position
                    if(this.captured == true && a2.y >= a1.y && b2.y <= a1.y)
                    {
                        this.hoopHits.Add(ip);
                    }
                });
                if(intersects == false)
                {
                    this.captured = false;
                }
                if(this.captured == true)
                {
                    var pos = this.transform.position;
                    var best = float.MaxValue;
                    var target = pos;
                    foreach(var p in this.hoopHits)
                    {
                        Minigame.DebugX(p, Color.blue);
                        var dist = (p - pos).sqrMagnitude;
                        if(dist < best)
                        {
                            best = dist;
                            target = p;
                        }
                    }
                    // intersection drifts downward each frame
                    target.y = start.y;

                    this.transform.position = Vector3.Lerp(pos, target, 0.5f);
                }

            }

        }
    }
}
