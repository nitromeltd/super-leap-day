using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class SpikedItem : MonoBehaviour
        {
            [System.Serializable]
            public class AB
            {
                public Transform a, b;
            }

            public FishingGame game;
            public List<AB> spikes;

            public void FixedUpdate()
            {
                Advance();
            }

            public void Test()
            {
                foreach(var ab in spikes)
                {
                    var a = ab.a.position;
                    var b = ab.b.position;

                    Debug.Log(Line.PointToLineDistance(Input.mousePosition, a, b));

                    //if(Line.PointToLineDistance(a, b, hook.body.position) < hook.circle.radius)
                    //{
                    //    hook.SetState(PlayerHook.State.Death);
                    //    break;
                    //}

                }
            }

            public virtual void Advance()
            {
                if(this.game.playerHook.state == PlayerHook.State.GoDown)
                {
                    var hook = this.game.playerHook;
                    foreach(var ab in spikes)
                    {
                        var a = ab.a.position;
                        var b = ab.b.position;

                        var cuts = hook.GetSegmentIntersectionPoints(a, b);

                        if(cuts.Count > 0)
                        {
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                            hook.positionDeath = cuts[0];
                            hook.SetState(PlayerHook.State.Death);
                            break;
                        }

                        // TODO: I'm skeptical this method is working

                        //Debug.Log(Line.PointToLineDistance(hook.body.position, a, b));

                        if(Line.PointToLineDistance(hook.body.position, a, b) < hook.circle.radius)
                        {
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_cut"]);
                            hook.positionDeath = hook.transform.position;
                            this.game.playerHook.isHookDeath = true;
                            hook.SetState(PlayerHook.State.Death);
                            break;
                        }

                    }
                }

            }

        }
    }
}
