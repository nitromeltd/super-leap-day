using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace fishing
    {
        public class TriggerButton : MonoBehaviour
        {
            public FishingGame game;
            public List<Motor> motors = new List<Motor>();
            public Collider2D trigger;
            public SpriteRenderer spriteRenderer;
            public List<Vector2Int> targets = new List<Vector2Int>();

            public Sprite spriteDown;

            public void AddMotor(Motor m)
            {
                this.motors.Add(m);
                m.paused = true;
            }


            void OnTriggerEnter2D(Collider2D collision)
            {
                Hit();
            }

            public void Hit()
            {
                if(this.game != null)
                {
                    if(this.game.playerHook.state != PlayerHook.State.GoDown) return;
                    if(this.game.IsComplete()) return;// don't trigger events during end game

                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_switch_engage"]);

                    if(this.name.Contains("ButtonFish"))
                    {
                        this.game.ParticleCreateAndPlayOnce("smack", this.transform.position + Vector3.up);
                        var fish = GetComponent<AnimatedFish>();
                        fish.shakeHit = new Shake(Vector2.up, 0.15f, 0.75f, 0.15f);
                    }

                    if(this.motors.Count > 0)
                    {
                        this.spriteRenderer.sprite = spriteDown;
                        //Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_switch_engage"]);
                        var center = Vector3.zero;
                        foreach(var m in this.motors)
                        {
                            m.paused = false;
                            m.isLookAtTween = true;
                            center += m.transform.localPosition;
                        }
                        center /= this.motors.Count;
                        this.game.playerHook.body.simulated = false;
                        var lookAtTween = new CamCtrl.LookAtTween(
                            this.game.camCtrl, this.game.cam.transform.localPosition, center, this.game.IsMotorLookAtTweenFinished, 0.5f, 0.5f, true, () =>
                            {
                                this.game.playerHook.body.simulated = true;
                            }
                        );
                        lookAtTween.lookStartAction = () => { Audio.instance.PlaySfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]); };
                        lookAtTween.lookFinishAction = () => { Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]); };
                        this.game.camCtrl.AddLookAtTween(lookAtTween);
                        this.trigger.enabled = false;
                    }

                    foreach(var pos in this.targets)
                    {
                        if(this.game.map.vector2IntToGameObject.TryGetValue(pos, out GameObject obj) == true)
                        {
                            if(obj.name.Contains("Spikes") == true)
                            {
                                obj.GetComponent<SpikesRetractable>().ToggleExtended();
                            }
                        }
                        if(this.game.flowLookup.ContainsKey(pos) == true)
                        {
                            Minigame.ForeachInFlood(pos, this.game.flowLookup, (kv) => { return true; }, (p) =>
                            {
                                var flow = this.game.flowLookup[p];
                                if(flow.isOn == false)
                                {
                                    flow.isOn = true;
                                    Audio.instance.PlaySfx(game.assets.audioClipLookup["fishing_waterflow_on"]);
                                    for(int i = 0; i < 3; i++)
                                    {
                                        var position = (Minigame.ToVector2(p) + Random.insideUnitCircle * 0.5f) - flow.speed;
                                        this.game.camCtrl.OnScreen(p, Vector2.zero);
                                        this.game.CreateBubble(position, flow.speed * FishingGame.BubbleSpeedDefault, FishingGame.BubbleSpeedDefault * 0.3f, RandomUtil.Pick("bubble_fast", "bubble"));
                                    }
                                }
                                else
                                    flow.isOn = false;
                            });
                        }
                    }

                    this.spriteRenderer.sprite = this.spriteDown;
                    this.trigger.enabled = false;
                }

            }

        }
    }
}
