using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace minigame
{
    namespace fishing
    {

        public class WaterlineSingle
        {
            public Heightmap heightmap;
            protected Transform waterTransform;
            protected MeshRenderer waterMeshRenderer;
            protected MeshFilter waterMeshFilter;
            protected Mesh waterMesh;


            public WaterlineSingle(GameObject prefabWater, Transform parent, float y, float xMin, float xMax)
            {
                this.waterTransform = GameObject.Instantiate(prefabWater, parent).transform;

                this.waterTransform.position = new Vector3(xMin, y);

                this.waterMeshRenderer = this.waterTransform.GetComponent<MeshRenderer>();
                this.waterMeshFilter = this.waterTransform.GetComponent<MeshFilter>();
                this.waterMesh = new Mesh();

                this.heightmap = new Heightmap(this);
                this.heightmap.SetBoundaries(xMin, xMax);
                this.heightmap.Advance();
                this.heightmap.DrawTo(this.waterMeshFilter, this.waterMesh);
            }

            public void Advance()
            {
                this.heightmap.Advance();
                this.heightmap.DrawTo(this.waterMeshFilter, this.waterMesh);
            }

            public class Heightmap
            {
                public WaterlineSingle waterline;
                public List<(Rect rect, float velocityY)> pushRects;
                public const float Spacing = 0.25f;
                public float xMin;
                public float xMax;
                public Point[] points;

                public struct Point
                {
                    public float displacement;
                    public float displacementLastFrame;
                }

                public Heightmap(WaterlineSingle waterline)
                {
                    this.waterline = waterline;
                    this.pushRects = new List<(Rect rect, float velocityY)>();
                }

                public void SetBoundaries(float xMin, float xMax)
                {
                    float oldXMin = this.xMin;
                    float oldXMax = this.xMax;
                    xMin = Mathf.FloorToInt(xMin / Spacing) * Spacing;
                    xMax = Mathf.CeilToInt(xMax / Spacing) * Spacing;

                    if(xMin == oldXMin && xMax == oldXMax)
                        return;

                    var oldPoints = this.points;
                    int count = 1 + Mathf.CeilToInt((xMax - xMin) / Spacing);
                    this.points = new Point[count];
                    this.xMin = xMin;
                    this.xMax = xMax;

                    if(oldPoints != null)
                    {
                        for(int n = 0; n < this.points.Length; n += 1)
                        {
                            float x = xMin + (n * Spacing);
                            int indexFromOld = Mathf.RoundToInt((x - oldXMin) / Spacing);
                            indexFromOld = Mathf.Clamp(indexFromOld, 0, oldPoints.Length - 1);
                            this.points[n] = oldPoints[indexFromOld];
                        }
                    }
                }

                public float DisplacementAtX(float x)
                {
                    float indexFloat = (x - this.xMin) / Spacing;
                    int indexOnLeft = Mathf.FloorToInt(indexFloat);
                    if(indexOnLeft < 0)
                        return this.points[0].displacement;
                    if(indexOnLeft >= this.points.Length - 1)
                        return this.points[this.points.Length - 1].displacement;

                    return Mathf.Lerp(
                        this.points[indexOnLeft].displacement,
                        this.points[indexOnLeft + 1].displacement,
                        indexFloat - indexOnLeft
                    );
                }

                public void Advance()
                {
                    var newValues = new float[this.points.Length];
                    int frame = Time.frameCount;

                    for(int n = 0; n < this.points.Length; n += 1)
                    {
                        int nMinus1 = Mathf.Clamp(n - 1, 0, this.points.Length - 1);
                        int nPlus1 = Mathf.Clamp(n + 1, 0, this.points.Length - 1);

                        float surroundingValues = (
                            this.points[nMinus1].displacement +
                            this.points[nPlus1].displacement
                        ) * .5f;
                        float speed =
                            this.points[n].displacement -
                            this.points[n].displacementLastFrame;

                        newValues[n] = surroundingValues;
                        newValues[n] += speed * .97f;
                        newValues[n] -= (this.points[n].displacement * .05f);

                        newValues[n] += 0.006f * Mathf.Sin((frame * 0.12f) + (n * 0.15f));
                        newValues[n] += 0.006f * Mathf.Sin((frame * 0.06f) + (n * 0.09f));
                    }

                    void PushWater(Rect rectangle, float velocityY)
                    {
                        var first = Mathf.RoundToInt((rectangle.xMin - 2 - this.xMin) / Spacing);
                        if(first >= this.points.Length) return;

                        var last = Mathf.RoundToInt((rectangle.xMax + 2 - this.xMin) / Spacing);
                        if(last < 0) return;

                        var centerX = (rectangle.xMin + rectangle.xMax) * 0.5f;
                        var centerIndex = Mathf.RoundToInt((centerX - this.xMin) / Spacing);
                        int halfwidth = centerIndex - first;
                        first = Mathf.Clamp(first, 0, this.points.Length - 1);
                        last = Mathf.Clamp(last, 0, this.points.Length - 1);
                        for(int n = first; n <= last; n += 1)
                        {
                            float centralness =
                                1.0f - ((float)Mathf.Abs(n - centerIndex) / halfwidth);
                            newValues[n] += velocityY * 0.14f * centralness;
                        }
                    }

                    foreach(var pushRect in this.pushRects)
                        PushWater(pushRect.rect, pushRect.velocityY);
                    this.pushRects.Clear();

                    for(int n = 0; n < this.points.Length; n += 1)
                    {
                        this.points[n].displacementLastFrame = this.points[n].displacement;
                        this.points[n].displacement = newValues[n];
                    }
                }

                public void Splash(Rect rect, float power)
                {
                    this.pushRects.Add((rect, power));
                }

                public void DrawTo(MeshFilter meshFilter, Mesh mesh)
                {
                    var vertices = new List<Vector3>();
                    var colors = new List<Color>();
                    var uvs = new List<Vector2>();
                    var indices = new List<int>();

                    var frame = Time.frameCount;
                    

                    for(int n = 0; n < this.points.Length - 1; n += 1)
                    {
                        float x1 = ((n + 0) * Spacing) + this.xMin;
                        float x2 = ((n + 1) * Spacing) + this.xMin;

                        int firstVertex = vertices.Count;
                        var a = new Vector2(n * Spacing, this.points[n].displacement);
                        var b = new Vector2((n + 1) * Spacing, this.points[n + 1].displacement);
                        vertices.Add(a);
                        vertices.Add(b);
                        {
                            float movementOffset = frame * 0.05f;
                            a.y += -0.2f + (Mathf.Sin(x1 * 0.4f + movementOffset) * 0.11f);
                            b.y += -0.2f + (Mathf.Sin(x2 * 0.4f + movementOffset) * 0.11f);
                            vertices.Add(a);
                            vertices.Add(b);
                        }
                        {
                            float movementOffset = frame * -0.05f;
                            a.y += -0.2f + (Mathf.Sin(x1 * 0.72f + movementOffset) * 0.14f);
                            b.y += -0.2f + (Mathf.Sin(x2 * 0.72f + movementOffset) * 0.14f);
                            vertices.Add(a);
                            vertices.Add(b);
                        }
                        {
                            float movementOffset = frame * 0.025f;
                            a.y += -0.2f + (Mathf.Sin(x1 * 0.56f + movementOffset) * 0.16f);
                            b.y += -0.2f + (Mathf.Sin(x2 * 0.56f + movementOffset) * 0.16f);
                            vertices.Add(a);
                            vertices.Add(b);
                        }
                        vertices.Add(new Vector2(a.x, 0));
                        vertices.Add(new Vector2(b.x, 0));
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        colors.Add(Color.white);
                        uvs.Add(new Vector2(0, 1));
                        uvs.Add(new Vector2(1, 1));
                        uvs.Add(new Vector2(0, 1 - 0.0625f));
                        uvs.Add(new Vector2(1, 1 - 0.0625f));
                        uvs.Add(new Vector2(0, 1 - 0.125f));
                        uvs.Add(new Vector2(1, 1 - 0.125f));
                        uvs.Add(new Vector2(0, 1 - 0.1875f));
                        uvs.Add(new Vector2(1, 1 - 0.1875f));
                        uvs.Add(new Vector2(0, 0));
                        uvs.Add(new Vector2(1, 0));

                        for(int offset = 0; offset < 8; offset += 2)
                        {
                            indices.Add(firstVertex + offset + 0);
                            indices.Add(firstVertex + offset + 2);
                            indices.Add(firstVertex + offset + 1);
                            indices.Add(firstVertex + offset + 1);
                            indices.Add(firstVertex + offset + 2);
                            indices.Add(firstVertex + offset + 3);
                        }
                    }

                    mesh.Clear();
                    mesh.SetVertices(vertices);
                    mesh.SetColors(colors);
                    mesh.SetUVs(0, uvs);
                    mesh.SetIndices(indices.ToArray(), MeshTopology.Triangles, 0);
                    meshFilter.mesh = mesh;
                }
            }
        }

    }
}
