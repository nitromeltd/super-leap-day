using System;
using UnityEngine;

namespace minigame
{
    namespace golf
    {
        public class Ball : MonoBehaviour
        {

            //[NonSerialized] 
            public State state;
            public Rigidbody2D body;
            public CircleCollider2D circle;
            public GolfGame game;
            public SpriteRenderer spriteRendererRoll;
            public Transform transformRoll;
            public Golfer golfer;
            public PlayerStand playerStand;
            [NonSerialized] public SpinnyCannon launcher;
            [NonSerialized] public PhysicsMaterial2D floorMaterial;

            private float angle, power, direction;
            private Vector3 start;
            private float airCount;
            private float stopCount;
            private bool onSand;
            private float launchDustCount;

            private Vector3 golferOffset;
            private Tween golferTween;
            private float fakeSpin;
            private float fakeSpinDamping;

            private PhysicsMaterial2D surfacePhysicsMaterial;
            private PhysicsMaterial2D conveyorPhysicsMaterial;
            private PhysicsMaterial2D sandPhysicsMaterial;

            private const float AngleMin = -20f;
            private const float AngleMax = 90f;
            private const float AngleInc = 2f;
            private const float PowerMin = 0.1f;
            private const float PowerMax = 1f;
            private const float PowerInc = 0.02f;
            private const float StopVelocity = 1f;
            private const float StopRoll = 124f;
            private const float PowerScale = 25000f;
            private const float ConveyorSpeed = 6.375f;
            private const float GolferCatchUpDelay = 1.5f;
            private const float DustDelay = 0.1f;
            private const float StopDelay = 0.5f;
            private const float StopJumpHeight = 1.875f;
            private const float SandDamping = 0.75f;
            private const float FakeSpinDampingMax = 0.99f;
            private const float FakeSpinDampingMin = 0.85f;
            private const float FakeSpinSpeed = 20f;
            private const float DeadDelay = 0.5f;

            private bool hasAnglePlayed = false;
            private bool hasPowerPlayed = false;
            private bool isConveyorPlaying = false;

            // set in GolfGame from PowerBar ui asset
            public static float PowerBarMax;



            // Show -> Angle -> Power -> Putt -> Roll -> Stop -> WaitForGolfer -> Angle
            public enum State
            {
                None, Show, Angle, Power, Putt, Roll, Stop, WaitForGolfer, Hole, Dead, Launcher, Fail
            }

            public void Init(GolfGame game)
            {
                this.game = game;
                airCount = 0;
                PutBallOnFloor();

                this.spriteRendererRoll.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteBall;

                this.start = this.transform.localPosition;

                // quick reference
                this.surfacePhysicsMaterial = game.assets.physicsMaterial2DLookup["golf_surface"];
                this.conveyorPhysicsMaterial = game.assets.physicsMaterial2DLookup["golf_conveyor"];
                this.sandPhysicsMaterial = game.assets.physicsMaterial2DLookup["golf_sand"];

                GameObject obj;

                // setup golfer
                this.golferOffset = new Vector3(-Minigame.Scale * 1.25f, -this.circle.radius);
                obj = Instantiate(this.game.assets.prefabLookup["golfer"], this.transform.localPosition + golferOffset, Quaternion.identity, this.game.map.transformObjects);
                this.golfer = obj.GetComponent<Golfer>();
                ObjectLayers.SetSortingLayer(obj, game.objectLayers.playerSortingLayer);
                this.golferTween = new Tween(obj.transform.localPosition, obj.transform.localPosition, 0);

                // setup standing player
                obj = Instantiate(this.game.assets.prefabLookup["player_stand"], this.transform.localPosition + Vector3.down * this.circle.radius, Quaternion.identity, this.game.map.transformObjects);
                this.playerStand = obj.GetComponent<PlayerStand>();
                this.playerStand.SetCharacter(this.game.character);
                ObjectLayers.SetSortingLayer(obj, game.objectLayers.playerSortingLayer);

                ResetArrow();
                SetState(State.Show);
            }

            public void Advance()
            {
                float timestep = Time.fixedDeltaTime / Time.timeScale;

                this.golfer.Advance(timestep);

                switch(this.state)
                {
                    case State.Angle:
                        if(SetAngle(this.angle + AngleInc * this.direction))
                        {
                            this.direction *= -1;
                            this.hasAnglePlayed = false;
                        }
                        if (!this.hasAnglePlayed)
                        {
                            this.hasAnglePlayed = true;
                            if (this.direction == 1)
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_aim_up"], Minigame.NoRandomPitch);
                            else
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_aim_down"], Minigame.NoRandomPitch);
                        }
                        if (this.game.input.pressed == true)
                        {
                            this.hasAnglePlayed = false;
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_aim_confirm"], Minigame.NoRandomPitch);
                            if (this.direction == 1)
                                Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_golf_aim_up"]);
                            else
                                Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_golf_aim_down"]);
                            SetState(State.Power);
                        }
                        else
                        {
                            this.game.minigameInputTypePrompt.ShowIfIdle(8f);
                        }
                        this.playerStand.SquatFx(this, this.game);
                        break;
                    case State.Power:
                        if(SetPower(this.power + PowerInc * this.direction))
                        {
                            this.direction *= -1;
                            this.hasPowerPlayed = false;
                        }
                        if (!this.hasPowerPlayed)
                        {
                            this.hasPowerPlayed = true;
                            if (this.direction == 1)
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_power_up"], Minigame.NoRandomPitch);
                            else
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_power_down"], Minigame.NoRandomPitch);
                        }
                        if (this.game.input.pressed == true)
                        {
                            this.hasPowerPlayed = false;
                            if (this.direction == 1)
                                Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_golf_power_up"]);
                            else
                                Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_golf_power_down"]);
                            SetState(State.Putt);
                        }
                        else
                        {
                            this.game.minigameInputTypePrompt.ShowIfIdle(8f);
                        }
                        this.playerStand.SquatFx(this, this.game);
                        break;
                    case State.Putt:
                        if(this.golfer.animSwing.currentAnimation == this.game.assets.animationLookup["golfer swing out"])
                        {
                            this.game.shots--;
                            this.game.UpdateShots();
                            this.body.simulated = true;
                            var normal = Minigame.NormalFromZAngle(this.angle);
                            var v = normal * this.power * PowerScale;
                            this.fakeSpin = FakeSpinSpeed;
                            this.fakeSpinDamping = FakeSpinDampingMin + this.power * (FakeSpinDampingMax - FakeSpinDampingMin);
                            this.body.AddForce(v, ForceMode2D.Force);
                            if(this.game.character == Character.King)
                                DropKingHat();
                            SetState(State.Roll);
                            this.game.ParticleCreateAndPlayOnce("smack", this.transform.localPosition);
                            this.game.camCtrl.AddShake(normal, 0.25f, 1f, 1.5f * this.power);
                            if(this.onSand)
                                AddSandDebris(Mathf.CeilToInt(80 * this.power), normal * this.power * 10f);
                            if(this.game.character == Character.Goop)
                                PlayerAssetsForMinigame.GoopParticles(this.transform.position, normal, this.power, this.game);
                            if (power <= 0.33)
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_swing_weak"], Minigame.NoRandomPitch);
                            else if (power > 0.33 && power <= 0.66)
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_swing_med"], Minigame.NoRandomPitch);
                            else
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_swing_strong"], Minigame.NoRandomPitch);

                            if (this.floorMaterial == this.sandPhysicsMaterial)
                            {
                                Audio.instance.PlayRandomSfx(
                                    options: null,
                                    position: this.transform.position,
                                    game.assets.audioClipLookup["sfx_golf_sand_1"],
                                    game.assets.audioClipLookup["sfx_golf_sand_2"],
                                    game.assets.audioClipLookup["sfx_golf_sand_3"]
                                );
                            }

                        }
                        break;
                    case State.Roll:
                        // edge of levelRect collision
                        if
                        (
                            this.body.position.x - this.circle.radius < this.game.levelRect.xMin ||
                            this.body.position.x + this.circle.radius > this.game.levelRect.xMax
                        )
                        {
                            this.body.position =
                            (
                                new Vector2
                                (
                                    Mathf.Clamp(this.body.position.x, this.game.levelRect.xMin + this.circle.radius, this.game.levelRect.xMax - this.circle.radius),
                                    this.body.position.y
                                )
                            );
                            var power = -Mathf.Sign(this.body.position.x - this.game.levelRect.center.x) * 0.3f;
                            this.body.velocity = new Vector2(Mathf.Abs(this.body.velocity.x) * power, this.body.velocity.y);
                        }

                        // check for spikes
                        var tilePos = Minigame.ToVector2Int(transform.localPosition);
                        if(this.game.spikesLookup.ContainsKey(tilePos))
                        {
                            Death();
                            break;
                        }

                        // check for coins
                        this.game.pickupMap.CircleCollidePickup(this.transform.position, this.circle.radius);

                        // are we on the floor? (because you can have 0 velocity after going straight up then down midway)
                        Vector2 p = transform.position;
                        float dist = this.circle.radius + 0.1f;
                        Debug.DrawLine(p, p + Vector2.down * dist, Color.red);
                        var result = Physics2D.Raycast(p, Vector2.down, dist, this.game.objectLayers.tileLayerMask);

                        this.floorMaterial = null;
                        if(result.collider != null)
                        {
                            this.floorMaterial = result.collider.sharedMaterial;
                            if(this.airCount <= DustDelay) this.airCount = 0;
                        }
                        this.onSand = false;

                        // conveyor belts
                        if(this.floorMaterial == this.conveyorPhysicsMaterial)
                        {
                            ConveyorPush(result.collider);
                            if (!this.isConveyorPlaying)
                            {
                                Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                                this.isConveyorPlaying = true;
                            }
                        }
                        else if (this.floorMaterial != this.conveyorPhysicsMaterial && this.isConveyorPlaying)
                        {
                            Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                            this.isConveyorPlaying = false;
                        }

                        // sand
                        else if(this.floorMaterial == this.sandPhysicsMaterial)
                        {
                            this.onSand = true;
                            if(this.body.velocity.sqrMagnitude >= 10f)
                                AddSandDebris(8, this.body.velocity);
                            this.body.velocity *= SandDamping;
                            if (this.floorMaterial == this.sandPhysicsMaterial &&
                                !Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_golf_sand_1"]) &&
                                !Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_golf_sand_2"]) &&
                                !Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_golf_sand_3"]))
                            {
                                Audio.instance.PlayRandomSfx(
                                    options: null,
                                    position: this.transform.position,
                                    game.assets.audioClipLookup["sfx_golf_sand_1"],
                                    game.assets.audioClipLookup["sfx_golf_sand_2"],
                                    game.assets.audioClipLookup["sfx_golf_sand_3"]
                                );
                            }    
                        }

                        // fake spin
                        if (this.fakeSpin > 0.01f)
                        {
                            this.transformRoll.localRotation *= Quaternion.Euler(0, 0, -this.fakeSpin);
                            this.fakeSpin *= this.fakeSpinDamping;
                        }
                        // stop check (also, are we in the hole?)
                        if(
                            this.body.velocity.magnitude < StopVelocity &&
                            result.collider != null &&
                            Mathf.Abs(this.body.angularVelocity) <= StopRoll &&
                            (
                                this.floorMaterial == this.surfacePhysicsMaterial ||
                                this.floorMaterial == this.sandPhysicsMaterial
                            )
                        )
                        {
                            this.body.angularVelocity = 0;
                            this.body.velocity = Vector2.zero;
                            this.body.simulated = false;
                            if(result.collider.name == "holeBottom")
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_flag_ping"], Minigame.NoRandomPitch);
                                SetState(State.Hole);
                                break;
                            }
                            else if(this.game.shots <= 0)
                            {
                                SetState(State.Fail);
                                break;
                            }
                            ResetArrow();
                            SetState(State.Stop);
                        }

                        if(this.launchDustCount > 0f)
                        {
                            launchDustCount -= Time.fixedDeltaTime;
                            if(this.launchDustCount < 0.9f)
                                this.game.AddDustCloud(this.transform.localPosition, 0.5f, 3, this.game.objectLayers.playerSortingLayer, this.spriteRendererRoll.sortingOrder - 1);
                        }

                        break;
                    case State.Stop:
                        {
                            this.stopCount = Mathf.Min(StopDelay, this.stopCount + timestep);
                            var jumpArc = Mathf.Sin(Mathf.PI * (this.stopCount / StopDelay));
                            var ground = this.transform.localPosition + Vector3.down * this.circle.radius;
                            this.playerStand.transform.localPosition = ground + Vector3.up * jumpArc * StopJumpHeight;
                            if(this.stopCount == StopDelay)
                            {
                                this.playerStand.transform.localPosition = this.transform.localPosition + Vector3.down * this.circle.radius;
                                this.playerStand.PlaySquat(false);
                                DustPuff(ground, Vector3.up, 1.5f, 12);
                                SetState(State.WaitForGolfer);
                            }
                            else if(this.playerStand.animated.currentAnimation == this.playerStand.AnimationsForCharacter(this.game.character).animationJumpUp)
                            {
                                if(this.stopCount / StopDelay >= 0.5f)
                                    this.playerStand.PlayJumpToDown();
                            }
                        }
                        break;
                    case State.WaitForGolfer:
                        if(this.golferTween.Done() == true && this.game.IsFailed() == false)
                            SetState(State.Angle);
                        break;
                    case State.Dead:
                        this.stopCount += timestep;
                        if(this.stopCount >= DeadDelay)
                            SetState(State.Fail);
                        break;
                    case State.Show:
                        if(this.game.camCtrl.lookAtTween == null)
                        {
                            SetState(State.Angle);
                            this.game.minigameInputTypePrompt.FirstShow();
                        }
                        break;
                    case State.Launcher:

                        if(this.game.input.pressed == true)
                        {
                            SetState(Ball.State.Roll);
                            this.game.ball.body.simulated = true;
                            this.launchDustCount = 1f;
                            this.launcher.Launch(SpinnyCannon.Power, 0, SpinnyCannon.Power);
                            this.launcher = null;
                        }
                        break;
                }

                if(this.golferTween.Done() == false)
                {
                    this.golferTween.Advance(timestep);
                    this.golfer.transform.localPosition = this.golferTween.Position();
                }
                // keep track of air time (reset in OnCollisionEnter2D below)
                this.airCount += timestep;
            }

            public void SetState(State s)
            {
                Time.timeScale = 1f;
                switch(s)
                {
                    case State.Angle:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault * 0.5f;
                        this.game.rectTransformAimArrow.gameObject.SetActive(true);
                        this.game.imageAimArrowCircle.sprite = this.game.assets.spriteLookup["aim_circle"];
                        this.direction = 1f;
                        this.angle = AngleMin;
                        this.golfer.aimHolder.SetActive(true);
                        this.golfer.animSwing.gameObject.SetActive(false);
                        this.game.minigameInputTypePrompt.secondsIdle = 0f;
                        break;
                    case State.Power:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault * 0.25f;
                        this.game.rectTransformPowerBar.gameObject.SetActive(true);
                        this.game.imageAimArrowCircle.sprite = this.game.assets.spriteLookup["aim_circle_extremes"];
                        this.direction = 1f;
                        this.power = PowerMin;
                        this.golfer.aimHolder.SetActive(false);
                        this.golfer.animSwing.gameObject.SetActive(true);
                        this.golfer.animPower.gameObject.SetActive(true);
                        this.golfer.animPower.PlayOnce(this.game.assets.animationLookup["golfer power particles in"], () => { this.golfer.animPower.PlayAndLoop(this.game.assets.animationLookup["golfer power particles"]); });
                        this.golfer.animSwing.PlayOnce(this.game.assets.animationLookup["golfer draw back"],() =>
                        {
                            this.golfer.animSwing.PlayAndLoop(this.game.assets.animationLookup["golfer power"]);
                        });
                        break;
                    case State.Putt:
                        this.golfer.bobMagnitudeTarget = 0;
                        this.playerStand.transform.localScale = Vector3.one;
                        this.game.rectTransformAimArrow.gameObject.SetActive(false);
                        this.game.rectTransformPowerBar.gameObject.SetActive(false);
                        this.golfer.animSwing.PlayOnce(this.game.assets.animationLookup["swish"]);
                        this.golfer.animPower.gameObject.SetActive(false);
                        this.golfer.animSwing.PlayOnce(this.game.assets.animationLookup["golfer swing in"], ()=>
                        {
                            this.golfer.animSwing.PlayOnce(this.game.assets.animationLookup["golfer swing out"], () =>
                            {
                                this.golfer.animSwing.PlayOnce(this.game.assets.animationLookup["golfer to idle"], () =>
                                {
                                    this.golfer.animSwing.PlayAndLoop(this.game.assets.animationLookup["golfer idle"]);
                                });
                            });
                        });
                        break;
                    case State.Roll:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault;
                        this.playerStand.gameObject.SetActive(false);
                        this.spriteRendererRoll.enabled = true;
                        Time.timeScale = 1.5f;
                        this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleDefault, 1f);

                        break;
                    case State.Stop:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault;
                        this.playerStand.gameObject.SetActive(true);
                        this.spriteRendererRoll.enabled = false;
                        this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleShow, GolfGame.StopRollZoomDelay, 1f);
                        this.playerStand.PlayJumpUp();
                        stopCount = 0;
                        this.golferTween = new Tween(this.golfer.transform.localPosition, transform.localPosition + golferOffset, GolferCatchUpDelay, 1f);
                        PutBallOnFloor();
                        break;
                    case State.WaitForGolfer:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault;
                        break;
                    case State.Show:
                        this.golfer.bobMagnitudeTarget = Golfer.BobMagnitudeDefault;
                        this.golfer.aimHolder.SetActive(true);
                        this.golfer.animSwing.gameObject.SetActive(false);
                        this.golfer.animPower.gameObject.SetActive(false);
                        this.playerStand.gameObject.SetActive(true);
                        this.spriteRendererRoll.enabled = false;
                        this.game.rectTransformAimArrow.gameObject.SetActive(false);
                        if(this.game.hole != null)
                        {
                            this.game.camCtrl.SetPosition(this.game.hole.transform.localPosition, Vector3.zero);
                            this.game.SetZoomButtonEnabled(false);
                            ShowLookAtTween
                            (
                                this.game.cam.transform.localPosition,
                                this.game.camCtrl.ClampFloor(
                                    this.transform.localPosition + new Vector3
                                    (
                                        this.game.camCtrl.rect.width * GolfGame.ZoomScaleShow * 0.5f - GolfGame.PuttInsetX,
                                        GolfGame.PuttOffsetY
                                    ),
                                    GolfGame.ZoomScaleShow),
                                () =>
                                {
                                    this.game.UpdatePuttingCamOffset(true);
                                    this.game.SetZoomButtonEnabled(true);
                                }
                            );
                        }
                        else
                        {
                            this.game.camCtrl.SetPosition(this.transform.position, this.game.camOffset);
                            this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleShow, 0.5f, 1f);
                        }
                        break;
                    case State.Dead:
                        this.spriteRendererRoll.enabled = false;
                        PlayerAssetsForMinigame.Death(this.transform.position, this.game);
                        //this.game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "death", this.transform.localPosition);
                        
                        Freeze();
                        this.stopCount = 0f;
                        break;
                    case State.Launcher:
                        this.spriteRendererRoll.enabled = false;
                        this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleShow, 0.5f, 1f);
                        Freeze();
                        break;
                    case State.Hole:
                        this.game.CompleteLevel();
                        this.game.hole.animBulb.PlayAndHoldLastFrame(this.game.assets.animationLookup["bulb"]);
                        // disable floor clamp
                        this.game.camCtrl.floorOffsetY = -Mathf.Floor(this.game.camCtrl.rectDefault.height);
                        if(this.game.isZoomedOutByButton == true)
                        {
                            this.game.OnPressZoomButton();
                            this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleDefault, 0.5f, 1f);
                        }
                        this.game.SetZoomButtonEnabled(false);
                        break;
                    case State.Fail:
                        Freeze();
                        // disable floor clamp
                        this.game.camCtrl.floorOffsetY = -Mathf.Floor(this.game.camCtrl.rectDefault.height);
                        this.game.FailLevel();
                        break;
                }
                this.state = s;
            }

            public void ShowLookAtTween(Vector3 begin, Vector3 end, Action lookAtDoneAction)
            {
                var dist = (end - begin).magnitude;
                var delay = Mathf.Max(GolfGame.HoleToBallDelay * dist, GolfGame.HoleToBallMinDelay);
                this.game.camCtrl.AddLookAtTween(
                    new CamCtrl.LookAtTween
                    (
                        this.game.camCtrl,
                        new Tween(begin, end, delay, 1f, GolfGame.BeforeHoleToBallDelay),
                        null,
                        null,
                        0,
                        lookAtDoneAction
                    )
                );
                this.game.camCtrl.SetZoomTween(GolfGame.ZoomScaleShow, delay, 1f, GolfGame.BeforeHoleToBallDelay);
            }

            public void Freeze()
            {
                this.body.angularVelocity = 0;
                this.body.velocity = Vector2.zero;
                this.body.simulated = false;
            }

            public void RestoreBallSimulation()
            {
                this.body.simulated = true;
            }
            public void ConveyorPush(Collider2D collider)
            {
                var vel = this.body.velocity;
                var len = vel.magnitude;
                var dir = collider.name.Contains("left") ? -1 : 1;
                if(len < ConveyorSpeed)
                    this.body.velocity += new Vector2(dir * (ConveyorSpeed - len), 0);
                else if(dir * vel.x < 0)
                    this.body.velocity += new Vector2(dir * ConveyorSpeed * 0.01f, 0);
            }

            public void ResetBall()
            {
                this.body.simulated = true;
                this.transform.localPosition = this.start;
                ResetArrow();
                SetState(State.Angle);
            }

            public void Death()
            {
                SetState(State.Dead);
            }

            public void ResetArrow()
            {
                this.playerStand.transform.localPosition = this.transform.localPosition + Vector3.down * this.circle.radius;
                SetAngle(AngleMin);
                var s = this.game.rectTransformPowerBarMask.sizeDelta;
                s.y = PowerBarMax;
                this.game.rectTransformPowerBarMask.sizeDelta = s;
            }

            public bool SetAngle(float a)
            {
                var clamp = Mathf.Clamp(a, AngleMin, AngleMax);
                this.game.rectTransformAimArrowTop.localRotation = this.game.rectTransformAimArrowBottom.localRotation = Quaternion.Euler(0, 0, a);
                this.golfer.transformHead.localRotation = Quaternion.Euler(0, 0, a * Golfer.HeadAngleReduction);
                this.angle = clamp;
                return clamp != a;
            }

            public bool SetPower(float p)
            {
                var clamp = Mathf.Clamp(p, PowerMin, PowerMax);
                var s = this.game.rectTransformPowerBarMask.sizeDelta;
                s.y = PowerBarMax * clamp;
                this.game.rectTransformPowerBarMask.sizeDelta = s;
                this.power = clamp;
                return clamp != p;
            }

            public void DustPuff(Vector3 p, Vector3 normal, float width, int total)
            {
                Vector3 left, right;
                Minigame.SideNormals(normal, out left, out right);
                for(int j = 0; j < total; j++)
                    this.game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "dust", p + left * width * 0.5f + right * UnityEngine.Random.Range(0, width));
            }

            public void AddSandDebris(int total, Vector2 vel)
            {
                var particleVel = new Vector2(-vel.x, Mathf.Abs(vel.y));
                var floor = this.transform.localPosition + Vector3.down * this.circle.radius;
                for(int i = 0; i < total; i++)
                {
                    var p = floor + Vector3.left * 0.5f + Vector3.right * UnityEngine.Random.value * 1.0f;
                    var d = this.game.AddDebris("sand", p, (Minigame.SpeedDebrisMin + particleVel) * 0.01f, (Minigame.SpeedDebrisMax + particleVel) * 0.02f);
                    d.acceleration *= 0.125f + UnityEngine.Random.value * 0.05f;
                    d.ScaleOut(AnimationCurve.Linear(0, 0.5f, 1f, 0f));
                }
            }

            public void PutBallOnFloor()
            {
                // cast down to hit floor
                var result = Physics2D.Raycast(this.transform.localPosition, Vector2.down, this.game.levelRect.height, this.game.objectLayers.tileLayerMask);
                if(result.collider != null)
                {
                    var p = result.point + Vector2.up * circle.radius;
                    body.position = p;
                    transform.localPosition = p;
                    floorMaterial = result.collider.sharedMaterial;
                    if(this.floorMaterial == this.sandPhysicsMaterial)
                        this.onSand = true;
                }
            }

            private void DropKingHat()
            {
                Particle hatParticle = Particle.CreateWithSprite(
                                game.assets.spriteLookup[Minigame.GlobalPrefix + "king_hat_flat"],
                                60,
                                this.playerStand.transform.position + PlayerStand.PositionKingHatDrop,
                                game.transform
                            );
                hatParticle.velocity = new Vector2(-0.1f, 0.35f);
                hatParticle.acceleration = new Vector2(0, -0.025f);
                hatParticle.spin = 4f;
                hatParticle.spriteRenderer.sortingOrder = ++this.game.spriteRendererPool.sortingOrderCount;
            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                if(this.airCount >= DustDelay)
                {
                    for (int i = 0; i < collision.contactCount; i++)
                    {
                        var power = Mathf.Min(body.velocity.magnitude / 16f, 1f);
                        DustPuff(collision.contacts[i].point, collision.contacts[i].normal, 0.625f, UnityEngine.Random.Range(5, 8));
                        if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_golf_floor_bump"]))
                        {
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_floor_bump"], options: new Audio.Options(0.5f, true, 0.2f));
                            if(this.game.character == Character.Goop)
                                PlayerAssetsForMinigame.GoopParticles(this.transform.position, collision.contacts[i].normal, power, this.game);
                        }
                    }
                }

                this.airCount = 0;

                // need to push on contact because we often resolve too far apart to detect the conveyor
                if(collision.collider.sharedMaterial == this.conveyorPhysicsMaterial)
                    ConveyorPush(collision.collider);
            }
        }

    }
}
