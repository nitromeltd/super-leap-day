using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/* Golf Minigame
 * 
 * Most of the interaction logic takes place in Ball.cs
 * 
*/

namespace minigame
{
    namespace golf
    {
        public partial class GolfGame : Minigame
        {
            [Header("Golf Game")]
            public RectTransform rectTransformShotsHolder;
            [NonSerialized] public Image[] imageShots;
            public RectTransform rectTransformPowerBar;
            public RectTransform rectTransformPowerBarMask;
            public RectTransform rectTransformAimArrow;
            public RectTransform rectTransformAimArrowTop;
            public RectTransform rectTransformAimArrowBottom;
            public UnityEngine.UI.Button buttonCancelZoom;
            public Image imageAimArrowCircle;
            public Transform transformBackground;
            public MinigameUI minigameUI;
            public Hole hole;
            public Wobbler pole;
            public Ball ball;
            public int shots;
            [NonSerialized] public float zoomTargetPrev;// before using zoom button
            [NonSerialized] public bool isZoomedOutByButton;
            public Image imageZoomButtonIconPortrait;
            public Image imageZoomButtonIconLandscape;
            [NonSerialized] public Vector3 camOffset;
            private bool zoomButtonEnabled;
            private Tween tweenCamDuringZoomButton;

            // I'm am losing whole days debugging this zoom nonsense:
            //
            //public Vector3 _camOffset;
            //public Vector3 camOffset
            //{
            //    get { return _camOffset; }
            //    set { _camOffset = value; Debug.Log(value); }
            //}

            //public Tween camOffsetTween;
            public Dictionary<Vector2Int, int> floorLookup = new Dictionary<Vector2Int, int>();
            public List<Transform> backgroundLayers = new List<Transform>();
            public List<Transform> backgroundScalers = new List<Transform>();

            public const float HoleToBallDelay = 0.032f;// multplied by distance between hole and ball
            public const float HoleToBallMinDelay = 1f;
            public const float BeforeHoleToBallDelay = 1f;
            public const float PuttInsetX = 6.5f;
            public const float PuttOffsetY = 3f;
            public const float ZoomScaleShowPortraitPlus = 2f;
            public const float ZoomScaleShowPortrait = 1.5f;
            public const float ZoomScaleShowLandscape = 1.25f;
            public static float ZoomScaleShow = 1.5f;
            public static float ZoomScaleDefault = 1f;
            public const float StopRollZoomDelay = 1f;
            //public const float ZoomButtonScaleMax = 2f;
            public const int BackgroundHillLayersTotal = 5;

            void Start()
            {
                Init();// Minigame setup
                Minigame.RetrySubtitle = "no shots left!";
                CamChangeInterpolation = 0.05f;
                
                if(Minigame.AspectRatio < 1f)
                {
                    // iPad
                    if(Minigame.AspectRatio > 0.69f)
                    {
                        ZoomScaleShow = ZoomScaleShowPortraitPlus;
                        this.rectTransformAimArrow.localScale = new Vector3(0.8f, 0.8f, 1f);
                        this.rectTransformPowerBar.localScale = new Vector3(0.8f, 0.8f, 1f);
                    }
                    else
                        ZoomScaleShow = ZoomScaleShowPortrait;
                    this.minigameUI.portrait.powerupBox.gameObject.SetActive(true);
                }
                else
                {
                    ZoomScaleShow = ZoomScaleShowLandscape;
                    this.minigameUI.landscape.powerupBox.gameObject.SetActive(true);
                }
                this.camCtrl.clampAction = this.camCtrl.ClampFloor;
                this.camCtrl.floorOffsetY = -Mathf.Floor(this.camCtrl.rectDefault.height * 0.2f);
                Ball.PowerBarMax = rectTransformPowerBarMask.rect.height;
                this.rectTransformPowerBar.gameObject.SetActive(false);
                this.buttonCancelZoom.gameObject.SetActive(false);
            
                Restart(true);

#if UNITY_EDITOR
                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.playerObj.transform.position.x, this.playerObj.transform.position.y + 4f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);
                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, () => { UpdateBackground(); });
                UpdateBackground();
#endif
            }

            void OnApplicationQuit()
            {
                Save();
            }

            public override void Save()
            {
                base.Save();
            }

            public override bool IsFailed()
            {
                return this.shots <= 0 && this.ball.state == Ball.State.WaitForGolfer;
            }

            public override bool IsComplete()
            {
                return ball.state == Ball.State.Hole;
            }

            public override void Restart(bool hard)
            {
                base.Restart(hard);

                this.floorLookup.Clear();
                this.backgroundLayers.Clear();
                this.backgroundScalers.Clear();
                this.camCtrl.floorOffsetY = -Mathf.Floor(this.camCtrl.rectDefault.height * 0.2f);
                this.camCtrl.SetZoom(ZoomScaleDefault);
                SetZoomButtonEnabled(true);
                this.imageZoomButtonIconLandscape.sprite = this.imageZoomButtonIconPortrait.sprite =
                    this.assets.spriteLookup["Golf Zoom Out Icon"];
                
                
                this.fileLevel = GetLevelFile(difficulty, level);
                //this.fileLevel = this.assets.levelTest;



                CreateLevel(this.fileLevel);
                this.input.holding = false;
                if(this.hole != null)
                {
                    this.camCtrl.SetPosition(this.playerObj.transform.position + this.camOffset, Vector3.zero);
                    UpdateBackground();
                }
                foreach(Transform childTransform in this.rectTransformShotsHolder)
                    Destroy(childTransform.gameObject);
                // setup golf shots ui
                this.rectTransformShotsHolder.localScale = Vector3.one;
                this.imageShots = new Image[this.shots];
                var totalWidth = 0f;
                for(int i = 0; i < this.shots; i++)
                {
                    var obj = Instantiate(this.assets.prefabLookup["GolfShotUI"], this.rectTransformShotsHolder);
                    this.imageShots[i] = obj.GetComponent<Image>();
                    var rt = this.imageShots[i].rectTransform;
                    rt.localPosition = new Vector3(-(rt.sizeDelta.x + 2) * (this.shots - 1) * 0.5f + rt.sizeDelta.x * i, 0);
                    totalWidth += rt.sizeDelta.x + 2;
                }
                if(totalWidth > this.rectTransformShotsHolder.sizeDelta.x)
                    this.rectTransformShotsHolder.localScale = Vector3.one * this.rectTransformShotsHolder.sizeDelta.x / totalWidth;

            }

            public override void ExitGame()
            {
                base.ExitGame();
            }

            void Update()
            {
                this.input.Advance();

                if(GameInput.Player(1).pressedPowerup)
                    OnPressZoomButton();


                //if(Input.GetKeyDown(KeyCode.T))
                //{
                //    ball.SetState(Ball.State.Fail);
                //    Debug.Break();
                //}

#if UNITY_EDITOR || DEVELOPMENT_BUILD

                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
#endif


                if(Minigame.paused == false)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }

            }

            void FixedUpdate()
            {

                float timestep = Time.fixedDeltaTime / Time.timeScale;

                this.camCtrl.UpdateZoomTween(timestep);

                if(this.camCtrl.lookAtTween == null)
                {
                    this.ball.Advance();
                    SetCameraByState();
                    UpdateMotors();
                }
                else
                {
                    this.camCtrl.UpdateLookAt(timestep);
                }
                camCtrl.AdvanceShakes(timestep);
                UpdateBackground();
                UpdateUIPosition();

                if(this.shots == 1)
                    this.imageShots[0].transform.localScale = Vector3.one * (1f + Shake.Wave(fixedDeltaTimeElapsed, 0.5f, 0.25f));

                Minigame.fixedDeltaTimeElapsed += timestep;
                Minigame.frameCount++;

                this.input.FixedAdvance();
            }

            public override void SetPaused(bool value)
            {
                if(this.isZoomedOutByButton == true)
                    return;
                base.SetPaused(value);
            }

            public void SetCameraByState()
            {
                switch(ball.state)
                {
                    case Ball.State.Show: break;
                    case Ball.State.Dead: break;
                    case Ball.State.Stop: goto case Ball.State.Angle;
                    case Ball.State.WaitForGolfer: goto case Ball.State.Angle;
                    case Ball.State.Power: goto case Ball.State.Angle;
                    case Ball.State.Angle: UpdatePuttingCamOffset(); break;
                    case Ball.State.Launcher:
                        var look = NormalFromZAngle(ball.launcher.angle) * Mathf.Abs(this.camCtrl.rect.width * 0.5f - PuttInsetX);
                        look.y = 0;
                        this.camOffset = Vector3.Lerp(this.camOffset, look, CamChangeInterpolation);
                        break;
                    case Ball.State.Hole:
                        // look just above the ball, so the continue cost doesn't cover it
                        this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.playerObj.transform.position + Vector3.up * 1f, 0.1f), Vector3.zero);
                        // skip normal camera
                        return;
                    case Ball.State.Roll:
                        this.camOffset = Vector3.Lerp(this.camOffset, Vector3.zero, 0.05f);

                        // TODO: comment out this hack for steering the ball
                        //if(Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) ball.body.velocity += Vector2Int.left * 10;
                        //if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) ball.body.velocity += Vector2Int.right * 10;
                        break;
                    case Ball.State.Fail:
                        // look just above the ball, so the continue cost doesn't cover it
                        this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.playerObj.transform.position + Vector3.up * 3f, 0.1f), Vector3.zero);
                        // skip normal camera
                        return;
                }
                this.camCtrl.SetPosition(this.playerObj.transform.position, this.camOffset);
            }

            public void UpdatePuttingCamOffset(bool skip = false)
            {
                var offsetX = this.camCtrl.rect.width * 0.5f - PuttInsetX;
                var target = new Vector3(offsetX, PuttOffsetY);
                var interpolation = 0.5f;
                if(this.ball.state == Ball.State.Stop || this.ball.state == Ball.State.WaitForGolfer)
                    interpolation = 0.1f;
                if(skip == true)
                    this.camOffset = target;
                else
                    this.camOffset = Vector3.Lerp(this.camOffset, target, interpolation);
            }

            public void UpdateUIPosition()
            {
                this.rectTransformAimArrow.anchoredPosition =
                    this.WorldPositionToCanvasAnchoredPosition(this.ball.transform.position);
                this.rectTransformPowerBar.anchoredPosition =
                    this.WorldPositionToCanvasAnchoredPosition(this.ball.transform.position + new Vector3(-4.5f, 5.5f));
            }

            public override Vector3 GetCameraTargetDefault()
            {
                return this.ball.transform.localPosition + this.camOffset;
            }

            public void UpdateShots()
            {
                for(int i = 0; i < this.imageShots.Length; i++)
                    if(i < this.shots)
                        this.imageShots[i].sprite = this.assets.spriteLookup["Golf Club UI 1"];
                    else
                        this.imageShots[i].sprite = this.assets.spriteLookup["Golf Club UI 2"];
                if(this.shots == 0)
                    this.imageShots[0].transform.localScale = Vector3.one;
            }

            public void UpdateBackground()
            {

                // look at the left edge of the camera rect
                var focusPoint = (this.cam.transform.localPosition) + Vector3.left * Minigame.ScreenWidth * camCtrl.ZoomScale() * 0.5f;

                //var focusPoint = (this.cam.transform.localPosition);// + Vector3.left * Minigame.ScreenWidth * camCtrl.ZoomScale() * 0.5f;
                focusPoint.z = focusPoint.y = 0;
                var camPosHoriz = new Vector3(focusPoint.x, 0);
                var layerTotal = this.backgroundLayers.Count - BackgroundHillLayersTotal;
                var step = 0.9f / (layerTotal + 1);
                var hillStep = step / BackgroundHillLayersTotal;
                var zoom = this.camCtrl.ZoomScale();
                for(int i = 0; i < this.backgroundLayers.Count; i++)
                {
                    var p = camPosHoriz;
                    float offset;
                    if(i < layerTotal)
                        offset = (i + 1) * step;
                    else
                        offset = step * layerTotal + (i-layerTotal) * hillStep;
                    offset = 1.0f - offset;
                    if(i > layerTotal)
                        offset *= 2f;
                    offset *= 0.9f;
                    p *= offset;
                    this.backgroundLayers[i].localPosition = p - focusPoint;

                    this.backgroundScalers[i].localPosition = focusPoint;

                    var zoomOffset = (zoom - 1f) * offset + 1f;
                    this.backgroundScalers[i].localScale = Vector3.one * zoomOffset;
                }
                var camBottomY = this.cam.transform.localPosition.y - this.camCtrl.rect.height * 0.5f;
                this.transformBackground.localPosition = new Vector3(this.transformBackground.localPosition.x, camBottomY / 2);
            }

            public void OnPressZoomButton()
            {
                if
                (
                    this.zoomButtonEnabled == false ||
                    this.minigameUI.pauseMenu.isOpen == true ||
                    IsComplete() || IsFailed()
                ) return;

                if(this.isZoomedOutByButton == false)
                {
                    SetPaused(true);
                    this.imageZoomButtonIconLandscape.sprite =
                        this.imageZoomButtonIconPortrait.sprite =
                            this.assets.spriteLookup["Golf Zoom In Icon"];
                    this.zoomTargetPrev = this.camCtrl.zoomTarget;
                    var scale = this.levelRect.width / Minigame.ScreenWidth;
                    var camPos = this.camCtrl.cam.transform.position;
                    this.camCtrl.SetZoomTween(scale, 1f);
                    this.tweenCamDuringZoomButton = new Tween(camPos, this.camCtrl.ClampFloor(camPos, scale), 1f);
                    this.rectTransformAimArrow.gameObject.SetActive(false);
                    this.rectTransformPowerBar.gameObject.SetActive(false);
                    Audio.instance.PlaySfx(this.assets.audioClipLookup["sfx_golf_zoom_out"]);
                }
                else
                {
                    this.buttonCancelZoom.gameObject.SetActive(false);
                    this.imageZoomButtonIconLandscape.sprite =
                        this.imageZoomButtonIconPortrait.sprite =
                            this.assets.spriteLookup["Golf Zoom Out Icon"];
                    this.camCtrl.SetZoomTween(this.zoomTargetPrev, 1f);
                    this.tweenCamDuringZoomButton = this.tweenCamDuringZoomButton.GetReversed();
                    Audio.instance.PlaySfx(this.assets.audioClipLookup["sfx_golf_zoom_in"]);
                }
                this.isZoomedOutByButton = !this.isZoomedOutByButton;
                SetZoomButtonEnabled(false);
                Audio.instance.PlaySfx(this.assets.audioClipLookup["sfx_ui_confirm"]);
                StartCoroutine(ZoomByButton());
            }

            System.Collections.IEnumerator ZoomByButton()
            {
                while(this.camCtrl.zoomTween != null)
                {
                    this.camCtrl.UpdateZoomTween(Time.fixedDeltaTime);
                    this.tweenCamDuringZoomButton.Advance(Time.fixedDeltaTime);
                    this.cam.transform.position = this.tweenCamDuringZoomButton.Position();
                    UpdateBackground();
                    yield return null;
                }
                if(this.isZoomedOutByButton == false)
                {
                    switch(this.ball.state)
                    {
                        case Ball.State.Angle: this.rectTransformAimArrow.gameObject.SetActive(true); break;
                        case Ball.State.Power: this.rectTransformPowerBar.gameObject.SetActive(true); break;
                    }
                    if(this.minigameUI.pauseMenu.isOpen == false)
                        SetPaused(false);
                }
                else
                {
                    this.buttonCancelZoom.gameObject.SetActive(true);
                }
                SetZoomButtonEnabled(true);
            }

            public void SetZoomButtonEnabled(bool value)
            {
                this.zoomButtonEnabled = value;
                if(value == true)
                    this.imageZoomButtonIconLandscape.color =
                        this.imageZoomButtonIconPortrait.color =
                            Color.white;
                else
                    this.imageZoomButtonIconLandscape.color =
                        this.imageZoomButtonIconPortrait.color =
                            new Color(1f, 1f, 1f, 0.5f);
            }


        }
    }
}
