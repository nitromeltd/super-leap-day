using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace golf
    {
        public partial class GolfGame : Minigame
        {
            public void CreateLevel(TextAsset file)
            {
                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                );
                this.map.AddNoAutoTiles(""
                    , "polysquare"
                    , "poly45br"
                    , "poly22br"
                    , "poly67br"
                    , "break_tile"
                );
                this.map.AddDontRotateTiles("spinny_cannon");
                this.map.AddFilledTiles("steep_hump_tile", "shallow_hump_tile");
                this.map.AddFilledRectInts(
                    ("golf_hole", new RectInt(0, 0, 3, 1))
                );

                // generate all objects
                this.map.Create(file);

                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;

                this.shots = map.levelOriginal.properties["shots"].i;
                Vector3? titleOverride = null;
                var golfHolePosition = Vector3.zero;


                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        this.spikesLookup[record.p] = 1;
                        record.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
                    }

                    if(record.isPrefab == true)
                    {
                        switch(record.name)
                        {
                            case "player":
                                {
                                    this.playerObj = record.gameObject;
                                    ball = record.GetComponent<Ball>();
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                }
                                break;
                            case "break_tile":
                                {
                                    var collisionAction = record.GetComponent<OnCollisionAction>();
                                    collisionAction.action = BreakTile;
                                    var bc = collisionAction.GetComponent<Collider2D>() as BoxCollider2D;
                                    var sr = record.GetComponent<SpriteRenderer>();
                                    sr.sprite = this.assets.spriteLookup[record.nameTile];
                                    var s = bc.size;
                                    switch(record.tile.tile)
                                    {
                                        case "break_block_big_red":
                                        case "break block big":
                                            s *= 2;
                                            break;
                                        case "break_block_portrait_purple":
                                        case "break block portrait":
                                            s.y += 1;
                                            break;
                                        case "break_block_landscape_purple":
                                        case "break block landscape":
                                            s.x += 1;
                                            break;
                                    }
                                    bc.offset = s / 2;
                                    bc.size = s;
                                }
                                break;
                            case "shot_pickup":
                                record.GetComponent<ShotPickup>().game = this;
                                break;
                            case "golf_hole":
                                golfHolePosition = MinigameMap.ToVector2(record.p) + Vector2.right * Minigame.Scale;
                                this.hole = record.GetComponent<Hole>();
                                this.pole = hole.GetComponentInChildren<Wobbler>();
                                pole.Init(this);
                                break;
                            case "spinny_cannon":
                                {
                                    var cannon = record.GetComponent<SpinnyCannon>();
                                    cannon.Init(record.tile);
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    cannon.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                }
                                break;
                            case "conveyor_left": goto case "conveyor_right";
                            case "conveyor_right":
                                {
                                    var a = record.GetComponent<Animated>();
                                    MinigameMap.AddAutotiling(a, record.tile, record.x, record.y, this.assets.autotileArrayLookup[Minigame.GlobalPrefix + record.name], this.map.levelOriginal);
                                }
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        //switch(record.nameTile)
                        //{
                        //}
                        // put foreground on tiles layer so golfer can sort over them
                        if(record.layerName == "Foreground")
                        {
                            var sr = record.gameObject.GetComponent<SpriteRenderer>();
                            sr.sortingLayerName = this.objectLayers.tilesSortingLayer;
                            sr.sortingOrder += this.map.width * this.map.height;
                        }
                    }
                    else
                    {
                        switch(record.nameTile)
                        {
                            case "title_override":
                                titleOverride = MinigameMap.ToVector2(record.p);
                                break;
                            case "GLOBAL:square":
                            case "GLOBAL:dirt":
                                this.floorLookup[record.p] = 1;
                                break;
                        }
                    }
                }


                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "button":
                            var tb = conn.objB.GetComponent<TriggerButton>();
                            tb.Init(this);
                            tb.targets.Add(conn.a);
                            triggerButtons.Add(tb);
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                    }
                }
                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);


                BuildBackground(golfHolePosition, titleOverride, this.map.width);



                //GetAutoTiles
                //(
                //    level,
                //    this.assets.tileThemeLookup["TileThemeGolf"],
                //    this.assets.physicsMaterial2DLookup["golf_surface"],
                //    this.assets.physicsMaterial2DLookup["golf_slope"],
                //    this.assets.physicsMaterial2DLookup["golf_sand"]
                //);


                this.playerObj.transform.localPosition += new Vector3(0, ball.circle.radius);
                ball.Init(this);
            }

            public void BuildBackground(Vector3 holePosition, Vector3? titleOverride, int levelWidth)
            {
                GameObject obj;
                Transform container;
                Sprite[] sprites;
                GameObject[] prefabs;
                float spacing;
                Rect rect;
                List<Vector2> positions;
                SpriteRenderer spriteRenderer;

                foreach(Transform childTransform in transformBackground)
                    Destroy(childTransform.gameObject);

                this.transformBackground.transform.localPosition = this.levelRect.min;

                this.spriteRendererPool.sortingOrderCount = 0;

                // brick wall
                spriteRenderer = this.spriteRendererPool.Get(Vector3.zero, this.assets.spriteLookup["brick_wall"], this.objectLayers.backgroundSortingLayer, "bricks", transformBackground);
                spriteRenderer.drawMode = SpriteDrawMode.Tiled;
                spriteRenderer.size = new Vector2(this.levelRect.width, spriteRenderer.size.y);
                backgroundScalers.Add(NestGameObject(spriteRenderer.transform));
                backgroundLayers.Add(spriteRenderer.transform);
                var backgroundRect = new Rect(Vector2.zero, spriteRenderer.size);
                // repeating top bricks - child of brick wall
                container = spriteRenderer.transform;
                spriteRenderer = spriteRendererPool.Get(new Vector3(0, backgroundRect.height), this.assets.spriteLookup["brick_wall_top"], objectLayers.backgroundSortingLayer, "bricks_top", container);
                spriteRenderer.drawMode = SpriteDrawMode.Tiled;
                spriteRenderer.size = new Vector2(this.levelRect.width, spriteRenderer.size.y * 4);
                spriteRenderer.sortingOrder = 0;
                // recording height for texture stretching below
                var heightBackgroundTotal = backgroundRect.height + spriteRenderer.size.y;

                // spacingDefault == width of a hill
                var sprite = this.assets.spriteArrayLookup["hills"].list[0];
                var spacingDefault = sprite.rect.width / sprite.pixelsPerUnit;

                // build clouds
                obj = AddGameObject(transformBackground, "clouds");
                backgroundScalers.Add(NestGameObject(obj.transform));
                backgroundLayers.Add(obj.transform);
                container = obj.transform;

                spacing = spacingDefault * 0.25f;
                rect = backgroundRect;
                rect.x -= spacing;
                rect.y += backgroundRect.height * 0.3f;
                rect.height *= 0.25f;
                rect.width += spacing;

                prefabs = this.assets.prefabArrayLookup["clouds"].list;
                positions = Vector2sAcrossRect(rect, spacing, spacing * 2);
                foreach(var p in positions)
                {
                    obj = Instantiate(RandomUtil.Pick(prefabs), p, Quaternion.identity, container);
                    ObjectLayers.SetSortingLayer(obj, this.objectLayers.backgroundSortingLayer);
                    StretchTiled(obj, "tile", heightBackgroundTotal - p.y);
                }

                // build lighting rafters
                obj = AddGameObject(transformBackground, "rafters");
                backgroundScalers.Add(NestGameObject(obj.transform));
                backgroundLayers.Add(obj.transform);
                container = obj.transform;
                spacing = spacingDefault * 0.3f;

                rect = backgroundRect;
                rect.x -= spacing;
                rect.y += backgroundRect.height * 0.7f;
                rect.height *= 0.25f;
                rect.width += spacing;

                prefabs = this.assets.prefabArrayLookup["rafters"].list;
                positions = Vector2sAcrossRect(rect, spacing, spacing * 2);
                foreach(var p in positions)
                {
                    obj = Instantiate(RandomUtil.Pick(prefabs), p, Quaternion.identity, container);
                    ObjectLayers.SetSortingLayer(obj, this.objectLayers.backgroundSortingLayer);
                    StretchTiled(obj, "tile", heightBackgroundTotal - p.y);
                }

                // build hills
                sprites = this.assets.spriteArrayLookup["hills"].list;
                spacing = spacingDefault * 0.5f;
                var depths = BackgroundHillLayersTotal;

                var layers = new List<Transform>();
                for(int i = 0; i < BackgroundHillLayersTotal; i++)
                {
                    obj = AddGameObject(transformBackground, "hills" + i);
                    backgroundScalers.Add(NestGameObject(obj.transform));
                    backgroundLayers.Add(obj.transform);
                    layers.Add(obj.transform);
                }

                {
                    var p = new Vector3(-spacing, 0);
                    while(p.x < backgroundRect.xMax)
                    {
                        var depth = UnityEngine.Random.Range(1, depths + 1);
                        var scale = Vector3.one * (0.5f + ((0.5f / depths) * depth));
                        p.x += UnityEngine.Random.Range(spacing * 0.7f, spacing * 1.25f) * scale.x;
                        var sr = spriteRendererPool.Get(p, RandomUtil.Pick(sprites), this.objectLayers.backgroundSortingLayer, "hill" + depth, layers[depth - 1]);
                        sr.sortingOrder = depth;
                        sr.transform.localScale = scale;
                    }
                }
                spriteRendererPool.sortingOrderCount += depths;

                // build foliage
                // foliage is added to the game - it's pinned to the floor
                obj = AddGameObject(transform, "foliage");
                container = obj.transform;

                var spritesDeck = new List<Sprite>(this.assets.spriteArrayLookup["foliage"].list);
                var spritesDeckRefill = new List<Sprite>(spritesDeck);
                var raycastPoint = Vector2.up * backgroundRect.height;
                spacing = spacingDefault;
                sprite = null;
                {
                    var p = new Vector3(-spacing * 0.5f, 0);
                    while(p.x < backgroundRect.xMax - spacing * 0.5f)
                    {
                        p.x += UnityEngine.Random.Range(spacing * 0.5f, spacing);
                        var total = 1 + (UnityEngine.Random.Range(0, 4)) % 3;// more singles
                        for(int i = 0; i < total; i++)
                        {
                            p.x += UnityEngine.Random.Range(spacing * 0.1f, spacing * 0.1f);
                            p.y = 0;
                            // move up to floor
                            sprite = RandomUtil.DrawOnly(spritesDeck, spritesDeckRefill, (s) => { return s != sprite; });
                            var width = sprite.rect.width / sprite.pixelsPerUnit;
                            var left = LowestFloorTileWithSpaceAbove(p + Vector3.up + Vector3.left * width * 0.5f, levelWidth).y * Scale;
                            var center = LowestFloorTileWithSpaceAbove(p + Vector3.up, levelWidth).y * Scale;
                            var right = LowestFloorTileWithSpaceAbove(p + Vector3.up + Vector3.right * width * 0.5f, levelWidth).y * Scale;
                            p.y += Mathf.Min(left, center, right) - Scale * 0.5f;
                            spriteRendererPool.Get(p, sprite, this.objectLayers.backgroundSortingLayer, sprite.name, container);
                        }
                    }
                }
                // title
                // the title is also pinned above the floor
                {
                    obj = AddGameObject(transform, "title");
                    container = obj.transform;
                    var pos = holePosition;
                    sprite = this.assets.spriteLookup["golf_day_logo_clubs"];
                    var width = sprite.rect.width / sprite.pixelsPerUnit;
                    var height = sprite.rect.height / sprite.pixelsPerUnit;
                    if(titleOverride.HasValue == true)
                        pos = titleOverride.Value;
                    else
                    {
                        pos.y = 0;
                        var left = LowestFloorTileWithSpaceAbove(pos + Vector3.up + Vector3.left * width * 0.5f, levelWidth).y * Scale;
                        var center = LowestFloorTileWithSpaceAbove(pos + Vector3.up, levelWidth).y * Scale;
                        var right = LowestFloorTileWithSpaceAbove(pos + Vector3.up + Vector3.right * width * 0.5f, levelWidth).y * Scale;
                        pos.y += Mathf.Min(left, center, right) + (sprite.rect.height / sprite.pixelsPerUnit) * 0.5f + Scale;
                        pos.y += Minigame.Scale * 2;// move title above flag pole
                    }
                    spriteRenderer = spriteRendererPool.Get(pos, sprite, this.objectLayers.backgroundSortingLayer, "golf_day_logo_clubs", container);
                    // attach text to golf clubs
                    obj = Instantiate(this.assets.prefabLookup["golf_background_title"], Vector3.zero, Quaternion.identity, spriteRenderer.transform);
                    obj.transform.localPosition = Vector3.zero;// Unity sets world position, not local
                    ObjectLayers.SetSortingLayer(obj, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount);
                    this.spriteRendererPool.sortingOrderCount += 2;
                    // attach poles to golf clubs
                    var sr = spriteRendererPool.Get(new Vector3(-(width - 3f) * 0.5f, -(height - 2f) * 0.5f), this.assets.spriteLookup["golf day sign pole left"], this.objectLayers.backgroundSortingLayer, "golf day sign pole left", spriteRenderer.transform);
                    sr.sortingOrder = spriteRenderer.sortingOrder - 1;
                    sr.drawMode = SpriteDrawMode.Sliced;
                    sr.size = new Vector2(sr.size.x, pos.y + sr.transform.localPosition.y);
                    sr = spriteRendererPool.Get(new Vector3((width - 3f) * 0.5f, -(height - 2f) * 0.5f), this.assets.spriteLookup["golf day sign pole right"], this.objectLayers.backgroundSortingLayer, "golf day sign pole right", spriteRenderer.transform);
                    sr.sortingOrder = spriteRenderer.sortingOrder - 1;
                    sr.drawMode = SpriteDrawMode.Sliced;
                    sr.size = new Vector2(sr.size.x, pos.y + sr.transform.localPosition.y);
                }

                // floor fill
                float fillHeight = Mathf.Floor(this.camCtrl.rectDefault.height * 0.5f);
                spriteRenderer = spriteRendererPool.Get(new Vector3(0, -fillHeight), this.assets.spriteLookup["floor_fill"], objectLayers.backgroundSortingLayer, "floor_fill", transform);
                spriteRenderer.drawMode = SpriteDrawMode.Tiled;
                spriteRenderer.size = new Vector2(this.levelRect.width, fillHeight);

            }

            public Vector2Int LowestFloorTileWithSpaceAbove(Vector2 p, int levelWidth)
            {
                var pos = ToVector2Int(p);
                pos.x = Mathf.Clamp(pos.x, 0, levelWidth - 1);
                while(floorLookup.ContainsKey(pos))
                    pos.y++;
                return pos;
            }

            public static void StretchTiled(GameObject obj, string tileName, float height)
            {
                var tiles = FindTransforms(obj.transform, tileName);
                foreach(var t in tiles)
                {
                    var sr = t.GetComponent<SpriteRenderer>();
                    if(sr != null)
                        sr.size = new Vector2(sr.size.x, height);
                }
            }

            public static Transform NestGameObject(Transform target)
            {
                var container = AddGameObject(target.parent, target.name, "nested").transform;
                target.SetParent(container, true);
                return container;
            }

            public static GameObject AddGameObject(Transform parent, string name, string suffix = "layer")
            {
                var obj = new GameObject(name + " " + suffix);
                obj.transform.SetParent(parent);
                obj.transform.localPosition = Vector3.zero;
                return obj;
            }

            public static List<Vector2> Vector2sAcrossRect(Rect rect, float stepMin, float stepMax)
            {
                var list = new List<Vector2>();
                for(float x = rect.xMin + UnityEngine.Random.Range(0, stepMin); x < rect.xMax; x += UnityEngine.Random.Range(stepMin, stepMax))
                    list.Add(new Vector2(x, UnityEngine.Random.Range(rect.y, rect.yMax)));
                return list;
            }
        }
    }
}

