using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace golf
    {
        public class Golfer : MonoBehaviour
        {
            public GameObject aimHolder;
            public Animated animPower;
            public Animated animSwish;
            public Animated animSwing;
            public Transform transformHead;
            public Transform transformBob;

            public const float BobMagnitudeDefault = 0.3f;
            public const float BobMagnitudeSpeed = 0.5f;
            public float bobMagnitude = 0.0f;
            public float bobMagnitudeTarget = 0.0f;
            public const float HeadAngleReduction = 0.35f;



            public void Advance(float timestep)
            {
                transformBob.localPosition = Shake.WavePosition(Vector2.up, Minigame.fixedDeltaTimeElapsed, 1f, bobMagnitude);
                if(bobMagnitude != bobMagnitudeTarget)
                    bobMagnitude = Mathf.MoveTowards(bobMagnitude, bobMagnitudeTarget, timestep * BobMagnitudeDefault * BobMagnitudeSpeed);
            }
        }

    }
}
