using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace golf
    {

        // NB: Non-Yolk character anims are 76.5625 pixels per unit, Yolk is 70ppu instead of 64ppu

        public class PlayerStand : MonoBehaviour
        {
            [System.Serializable]
            public class PlayerAnimations
            {
                public Animated.Animation animationSquat;
                public Animated.Animation animationJumpUp;
                public Animated.Animation animationJumpToDown;
                public Animated.Animation animationJumpDown;
            };
            public PlayerAnimations yolk;
            public PlayerAnimations puffer;
            public PlayerAnimations pufferHead;
            public PlayerAnimations goop;
            public PlayerAnimations sprout;
            public PlayerAnimations king;
            public Animated.Animation animationKingSquatHat;

            public Character character;
            public Animated animated;
            public Animated animatedHead;
            public SpriteRenderer spriteRenderer;
            public SpriteRenderer spriteRendererHead;

            private int sweatCount;

            // UnityRandom isn't random enough
            private static int[] SweatDirs = new int[] { 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1 };

            public static Vector3 PositionKingHatDrop = new Vector3(0.1659f, 2.43f, 0);
            public PlayerAnimations AnimationsForCharacter(Character character) =>
        character switch
        {
            Character.Puffer => this.puffer,
            Character.Goop => this.goop,
            Character.Sprout => this.sprout,
            Character.King => this.king,
            _ => this.yolk,
        };

            public void SetCharacter(Character character)
            {
                this.character = character;
                PlaySquat(true);
                var h = this.spriteRenderer.sprite.rect.height / this.spriteRenderer.sprite.pixelsPerUnit;
                this.spriteRenderer.transform.localPosition = new Vector3(0, h / 2);
            }


            public void PlaySquat(bool firstShot)
            {
                if(this.character == Character.King && firstShot == true)
                    this.animated.PlayAndHoldLastFrame(this.animationKingSquatHat);
                else
                    this.animated.PlayAndHoldLastFrame(AnimationsForCharacter(this.character).animationSquat);
                this.spriteRendererHead.enabled = false;
            }

            public void PlayJumpUp()
            {
                this.animated.PlayAndHoldLastFrame(AnimationsForCharacter(this.character).animationJumpUp);
                if(this.character == Character.Puffer)
                {
                    this.spriteRendererHead.enabled = true;
                    this.animatedHead.PlayAndHoldLastFrame(this.pufferHead.animationJumpUp);
                }
                else
                    this.spriteRendererHead.enabled = false;
            }

            public void PlayJumpToDown()
            {
                this.animated.PlayOnce(AnimationsForCharacter(this.character).animationJumpToDown, () =>
                {
                    this.animated.PlayOnce(AnimationsForCharacter(this.character).animationJumpDown);
                });

                if(this.character == Character.Puffer)
                {
                    this.spriteRendererHead.enabled = true;
                    this.animatedHead.PlayOnce(this.pufferHead.animationJumpToDown, () =>
                    {
                        this.animatedHead.PlayOnce(this.pufferHead.animationJumpDown);
                    });
                }
                else
                    this.spriteRendererHead.enabled = false;
            }


            public void SquatFx(Ball ball, GolfGame game)
            {
                var wave = 0.98f + Shake.Wave(Minigame.fixedDeltaTimeElapsed, (ball.state == Ball.State.Angle ? 0.15f : 0.1f), 0.02f);
                this.transform.localScale = new Vector3(1f, wave, 1f);
                // sweat particles
                if(Minigame.frameCount % (ball.state == Ball.State.Angle ? 10 : 8) == 0)
                {
                    var head = ball.transform.localPosition + Vector3.up * (ball.circle.radius + 0.3f);
                    Particle d = game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "sweat", head + Vector3.right * UnityEngine.Random.value * 0.4f);
                    if(SweatDirs[sweatCount++ % SweatDirs.Length] == 0)
                        d.velocity = RandomUtil.Range(new Vector2(-0.05f, 0.06f), new Vector2(-0.075f, 0.10f));
                    else
                        d.velocity = RandomUtil.Range(new Vector2(0.05f, 0.06f), new Vector2(0.075f, 0.10f));

                    d.acceleration = new Vector2(0, -0.0054f);
                    d.spriteRenderer.sortingLayerName = game.objectLayers.playerSortingLayer;
                    d.spriteRenderer.sortingOrder = 6;// behind squat anim
                }
            }


        }

    }
}
