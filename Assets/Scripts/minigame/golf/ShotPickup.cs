using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace golf
    {
        public class ShotPickup : MonoBehaviour
        {
            public GolfGame game;
            public Transform transformBob;

            private void FixedUpdate()
            {
                this.transformBob.localPosition = Shake.WavePosition(Vector2.up, Minigame.fixedDeltaTimeElapsed, 1f, 0.25f);
            }

            private void OnTriggerEnter2D(Collider2D collision)
            {
                if(collision.name == "player" && this.game.shots < this.game.imageShots.Length)
                {
                    if(this.game.shots == 1)
                        this.game.imageShots[0].transform.localScale = Vector3.one;
                    this.game.shots++;
                    this.game.UpdateShots();
                    this.game.SparkleFx(this.transform.position);
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_golf_extra_shot_pickup"]);
                    Destroy(this.gameObject);
                }
            }

        }

    }
}
