using UnityEngine;

namespace minigame
{
    namespace golf
    {
        public class SpinnyCannon : Launcher
        {

            public float angle;
            public float dir;
            private float resetCount;

            public const float Power = 15625f;
            private const float AngleInc = 2f;
            private const float ResetDelay = 0.25f;


            public void Init(NitromeEditor.TileLayer.Tile tile)
            {
                this.angle = (tile.flip ? 180 : 0) + tile.rotation;
                this.dir = tile.flip ? -1 : 1;
                this.resetCount = ResetDelay;
            }

            public override void Advance()
            {
                base.Advance();
                if(this.resetCount < ResetDelay)
                    this.resetCount += Time.fixedDeltaTime / Time.timeScale;
                this.angle += AngleInc * this.dir;
                SetRotation(this.angle);
                if(this.rigidBody2DBall)
                    this.rigidBody2DBall.transform.localPosition = transform.localPosition;
            }

            public override void LaunchThrust(float power)
            {
                Vector2 launchVector = this.transformPivot.localRotation * Vector2.right;
                // using force instead of velocity for a more punchy shot
                this.rigidBody2DBall.AddForce(launchVector * Power);
                resetCount = 0;
                Audio.instance.PlaySfx(this.rigidBody2DBall.gameObject.GetComponent<Ball>().game.assets.audioClipLookup["sfx_env_pipes_cannon"]);
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(collision.name == "player" && resetCount >= ResetDelay)
                {
                    collision.transform.localPosition = transform.localPosition;
                    resetCount = 0;
                    var ball = collision.GetComponent<Ball>();
                    if(ball != null)
                    {
                        SetPower(1f);
                        Load(ball.body);
                        ball.launcher = this;
                        ball.SetState(Ball.State.Launcher);
                        Audio.instance.PlaySfx(ball.game.assets.audioClipLookup["sfx_env_pipe_in"]);
                    }
                }
            }


        }
    }
}
