using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace golf
    {
        public class Wobbler : MonoBehaviour
        {
            public float wavelength;
            public float duration;
            public float angleMin, angleMax;

            private Shake shake;
            private Quaternion min, max;

            public GolfGame game;

            public void Init(GolfGame game)
            {
                this.game = game;
            }
            public void StartShake()
            {
                if (shake == null)
                {
                    this.shake = new Shake(this.wavelength, this.duration);
                    this.min = Quaternion.Euler(0, 0, angleMin);
                    this.max = Quaternion.Euler(0, 0, angleMax);
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_golf_flag_wobble"]);
                }
            }

            void FixedUpdate()
            {
                if (shake != null)
                {
                    this.shake.Advance(Time.fixedDeltaTime);
                    this.transform.localRotation = this.shake.Rotation(this.min, this.max);
                    if (this.shake.time == this.shake.duration)
                        this.shake = null;
                }
            }

        }
    }
    
}
