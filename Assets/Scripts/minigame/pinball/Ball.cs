using System;
using UnityEngine;

namespace minigame
{
    namespace pinball
    {
        public class Ball : MonoBehaviour
        {

            [NonSerialized] public State state;
            public PinballGame game;
            public Rigidbody2D body;
            public CircleCollider2D circle;
            public SpriteRenderer spriteRenderer;
            public Launcher launcher;
            public bool spend;
            private bool inputDown;
            private float launchPower;
            private float launchDustCount;

            public const float LaunchMin = 10f;
            public const float LaunchMax = 30f;
            public const float LaunchStep = 0.2f;
            private const float ConveyorSpeed = 6.375f;
            public static Vector2 LaunchVector = Vector2.up + Vector2.right;

            private bool isflipperdown = true;

            public enum State
            {
                None, Launcher, Windup, Play, Lost, Finish
            }

            public void Init(PinballGame game)
            {
                this.game = game;
                this.inputDown = false;
                this.transform.position = this.launcher.transform.position;
                this.spriteRenderer.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteBall;
                SetState(State.Launcher);
                this.game.minigameInputTypePrompt.FirstShow();
            }

            void FixedUpdate()
            {
                switch(this.state)
                {
                    case State.Launcher:
                        if(InputDownNew())
                        {
                            SetState(State.Windup);
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_charge"]);
                        }
                        else
                        {
                            this.transform.localPosition = this.launcher.transform.localPosition;
                            if(this.game.balls.Count == 1)
                                this.game.minigameInputTypePrompt.ShowIfIdle(8f);
                        }
                        break;
                    case State.Windup:
                        this.transform.localPosition = this.launcher.transform.localPosition;
                        if(this.game.input.holding)
                        {
                            if(this.launchPower < LaunchMax)
                            {
                                this.launchPower += LaunchStep;
                                this.launcher.SetPower(this.launchPower, LaunchMin, LaunchMax);
                            }
                        }
                        else
                        {
                            SetState(State.Play);
                            this.launcher.Launch(this.launchPower, LaunchMin, LaunchMax);
                            this.launchDustCount = this.launcher.power * 0.5f + 0.25f;

                            if(this.launcher == this.game.launcher && this.spend == true)
                            {
                                if(this.game.ballsLeft == 3 && this.game.character == Character.King)
                                    ShootKingHat();
                                this.game.SpendBall();

                            }

                            if (launchPower < 16.6)
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_weak"]);
                            }
                            else if(launchPower>= 16.6 && launchPower < 23.2)
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_med"]);
                            }
                            else
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_strong"]);
                            }
                            Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_pinball_launch_charge"]);
                        }
                        break;
                    case State.Play:
                        // check for spikes
                        var tilePos = Minigame.ToVector2Int(transform.localPosition);
                        if(this.game.spikesLookup.ContainsKey(tilePos))
                        {
                            Death();
                            break;
                        }
                        // are we on the floor?
                        Vector2 p = transform.position;
                        float dist = this.circle.radius + 0.1f;
                        Debug.DrawLine(p, p + Vector2.down * dist, Color.red);
                        var result = Physics2D.Raycast(p, Vector2.down, dist, this.game.objectLayers.tileLayerMask);
                        if(result.collider != null)
                        {
                            // conveyor belts
                            if(result.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["pinball_conveyor"])
                            {
                                ConveyorPush(result.collider);
                                if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]))
                                {
                                    Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"], startFrom: 0, setTo: 1f, duration: .1f));
                                }
                            }
                            else
                            {
                                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                            }
                        }
                        else if(result.collider == null)
                        {
                            Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                        }

                        // check for coins
                        this.game.pickupMap.CircleCollidePickup(this.transform.position, this.circle.radius);
                        // check for checkpoints
                        foreach(var chk in this.game.launcherCheckpoints)
                        {
                            // pass?
                            var y = chk.transform.localPosition.y;
                            if(
                                chk != this.game.launcher &&
                                Mathf.Sign(chk.posBall.y - y) != Mathf.Sign(this.transform.localPosition.y - y) &&
                                (
                                    (chk.rank == this.game.launcher.rank && y > this.game.launcher.transform.localPosition.y) ||
                                    (chk.rank > this.game.launcher.rank)
                                )
                            )
                                this.game.launcher = chk;
                            chk.posBall = this.transform.localPosition;
                        }
                        // check for win
                        if(this.game.finishLookup.ContainsKey(tilePos))
                        {
                            SetState(State.Finish);
                            Audio.instance.PlaySfx(this.game.assets.audioClipLookup["GLOBAL:sfx_pinball_goal"]);
                            break;
                        }
                        // launch fx
                        if(this.launchDustCount > 0f)
                        {
                            launchDustCount -= Time.fixedDeltaTime;
                            if(this.launchDustCount < 0.9f)
                                this.game.AddDustCloud(this.transform.localPosition, 0.5f, 3, this.game.objectLayers.playerSortingLayer, this.spriteRenderer.sortingOrder - 1);
                        }
                        break;
                    case State.Finish:
                        break;
                }

                if (InputDownNew() && isflipperdown && game.winMenu.isOpen == false)
                {
                    Audio.instance.PlayRandomSfx(null, null, 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_1"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_3"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_3"]);
                    isflipperdown = false;
                }
                else if(!InputDownNew() && !isflipperdown && game.winMenu.isOpen == false)
                {
                    Audio.instance.PlayRandomSfx(null, null, 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_1"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_3"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_3"]
                    );
                    isflipperdown = true;
                }

                this.inputDown = this.game.input.holding;
            }

            public void SetState(State s)
            {
                switch(s)
                {
                    case State.Launcher:
                        this.body.velocity = Vector2.zero;
                        this.body.simulated = false;
                        this.spriteRenderer.enabled = false;
                        this.inputDown = false;
                        this.launcher.wait = false;
                        this.launcher.throbStart = Minigame.fixedDeltaTimeElapsed;
                        this.game.minigameInputTypePrompt.secondsIdle = 0f;
                        break;
                    case State.Play:
                        this.body.simulated = true;
                        this.spriteRenderer.enabled = true;
                        break;
                    case State.Lost:
                        this.launcher = this.game.launcher;
                        this.launcher.Load(body);
                        this.launcher.wait = true;
                        this.body.simulated = false;
                        this.spriteRenderer.enabled = false;
                        break;
                    case State.Windup:
                        this.launchPower = LaunchMin;
                        this.launcher.throb = false;
                        break;
                    case State.Finish:
                        if(this.game.IsComplete() == false)
                        {
                            this.game.CompleteLevel();
                        }
                        this.body.gravityScale = 0;
                        break;
                }

                this.state = s;
            }

            public void Death()
            {
                PlayerAssetsForMinigame.Death(this.transform.position + (Vector3)this.circle.offset, this.game);
                
                if(this.game.BallCount() > 1)
                {
                    this.game.balls.Remove(this);
                    this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, Minigame.Scale * 0.2f);
                    Destroy(gameObject);
                }
                else
                {
                    SetState(State.Lost);
                    if(this.game.IsFailed() == false)
                        this.game.camCtrl.PauseAndPanTo(this.launcher.transform.localPosition, 0.5f, 0.05f, 0.5f, ()=>
                        {
                            this.transform.localPosition = this.launcher.transform.localPosition;
                            this.spend = true;
                            SetState(State.Launcher);
                        });
                    else
                    {
                        this.game.positionLastBall = this.transform.localPosition;
                        this.game.camCtrl.clampAction = this.game.camCtrl.ClampX;
                        this.game.FailLevel();
                    }
                }
            }

            public void ConveyorPush(Collider2D collider)
            {
                var vel = this.body.velocity;
                var len = vel.magnitude;
                var dir = collider.name.Contains("left") ? -1 : 1;
                if(len < ConveyorSpeed)
                    this.body.velocity += new Vector2(dir * (ConveyorSpeed - len), 0);
                else if(dir * vel.x < 0)
                    this.body.velocity += new Vector2(dir * ConveyorSpeed * 0.1f, 0);
            }

            public bool InputDownNew()
            {
                var isDown = this.game.input.holding == true && this.inputDown == false;
                if(isDown)
                {
                    this.inputDown = true;
                }
                return isDown;
            }

            private void ShootKingHat()
            {
                Vector2 launchVector = this.launcher.transformPivot.localRotation * Vector2.right;
                Particle hatParticle = Particle.CreateWithSprite(
                                game.assets.spriteLookup[Minigame.GlobalPrefix + "king_hat_flat"],
                                60,
                                this.launcher.transform.position,
                                game.transform
                            );
                hatParticle.velocity = launchVector * Mathf.Max(this.launcher.power, 0.5f);
                hatParticle.acceleration = new Vector2(0, -0.025f);
                hatParticle.spin = 5f;
                hatParticle.ScaleOut(AnimationCurve.Linear(0, 0.2f, 0.25f, 1f));
                hatParticle.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                if(collision.gameObject.name.StartsWith("tile") || collision.gameObject.name.StartsWith("cloud"))
                {
                    if (!Audio.instance.IsSfxPlaying(this.game.assets.audioClipLookup["sfx_golf_floor_bump"]))
                    {
                        Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_golf_floor_bump"]);
                    }
                }
            }
        }
    }
}
