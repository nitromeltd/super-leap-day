using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace pinball
    {
        public class Bumper : MonoBehaviour
        {

            public PinballGame game;
            private PlayAnimated playAnimated;
            public bool isRound = false;

            private void Awake()
            {
                this.playAnimated = GetComponent<PlayAnimated>();
            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                var rb = collision.collider.attachedRigidbody;
                if(rb != null && rb.name.Contains("player"))
                {
                    this.playAnimated.PlayOnce();

                    var bumperTransform = collision.otherCollider.transform;
                    var polygonCollider = collision.otherCollider.GetComponent<PolygonCollider2D>();
                    this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, 0.2f);
                    Vector2 v = Vector2.zero;
                    if(polygonCollider != null)
                    {
                        v = -collision.contacts[0].normal;
                        var pos = bumperTransform.localRotation * Minigame.PointsCenter(polygonCollider.points) + bumperTransform.position;
                        this.game.AddScore(pos, 10);
                    }
                    else
                    {
                        v = (Vector2)(rb.transform.position - bumperTransform.position).normalized;
                        this.game.AddScore(bumperTransform.localPosition, 10);
                    }
                    rb.velocity += v * PinballGame.BumperPower;

                    if (isRound)
                    {
                        Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_pinball_bumper_round"]);
                    }
                    else
                    {
                        Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_pinball_bumper_triangle"]);
                    }
                }
            }
        }

    }
}

