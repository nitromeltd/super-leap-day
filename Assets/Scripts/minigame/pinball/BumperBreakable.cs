using UnityEngine;


namespace minigame
{
    namespace pinball
    {
        public class BumperBreakable : MonoBehaviour
        {

            public int health;
            public SpriteRenderer spriteRenderer;
            public Animated anim;
            private PinballGame game;
            
            public void Init(PinballGame game, int health)
            {
                this.game = game;
                this.health = health;
            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                var rb = collision.collider.attachedRigidbody;
                if(rb != null && rb.name.Contains("player"))
                {
                    this.health--;
                    if(this.health <= 0)
                    {
                        this.game.AddScore(transform.localPosition, 30);
                        this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, Minigame.Scale * 0.2f);
                        this.game.RemoveMotor(this.transform);
                        for(int i=1; i <= 4; i++)
                            this.game.AddDebris("bumper_death_piece_"+i, transform.localPosition);
                        this.game.AddDebris("bumper_death_face", transform.localPosition);
                        Bump(collision, 1f);
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_bumper_breakable_1"], options: new Audio.Options(1, false, 0));
                        Destroy(gameObject);
                    }
                    else
                    {
                        this.game.AddScore(transform.localPosition, 10);
                        this.anim.PlayAndHoldLastFrame(this.game.assets.animationLookup["bumper hit " + (this.health + 1)]);
                        Bump(collision, 0.75f);
                        switch (health)
                        {
                            case 4:
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_bumper_breakable_5"], options: new Audio.Options(1, false, 0));
                                break;
                            case 3:
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_bumper_breakable_4"], options: new Audio.Options(1, false, 0));
                                break;
                            case 2:
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_bumper_breakable_3"], options: new Audio.Options(1, false, 0));
                                break;
                            case 1:
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_bumper_breakable_2"], options: new Audio.Options(1, false, 0));
                                break;
                        }
                    }
                }
            }

            void Bump(Collision2D collision, float power)
            {
                this.game.camCtrl.AddShake(Vector2.up, 0.4f / power, 0.7f * power, Minigame.Scale * 0.2f * power);
                var v = (Vector2)(collision.rigidbody.transform.position - collision.otherCollider.transform.position).normalized;
                collision.rigidbody.velocity += v * PinballGame.BumperPower * power;

            }

        }
    }
}
