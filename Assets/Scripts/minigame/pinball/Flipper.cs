using UnityEngine;
using System.Collections.Generic;

namespace minigame
{
    namespace pinball
    {
        public class Flipper : MonoBehaviour
        {
            public Transform transformBar;
            public Transform transformBarCenter;
            private PinballGame game;
            private bool flipped = false;

            public float angleMin, angleMax;

            private Dictionary<Rigidbody2D, bool> bodyContacts;

            private const float AngleSpeed = 25f;

            public void Init(PinballGame game, bool flipped)
            {
                this.game = game;
                this.flipped = flipped;
                this.angleMin = this.transformBar.eulerAngles.z;
                this.angleMax = this.angleMin + (flipped ? -90 : 90);
                if(this.angleMax > 360) this.angleMax -= 360;
                this.bodyContacts = new Dictionary<Rigidbody2D, bool>();
            }

            void FixedUpdate()
            {
                float angle = this.transformBar.localRotation.eulerAngles.z;

                if(this.game.input.holding)
                {
                    var newAngle = Mathf.MoveTowardsAngle(angle, this.angleMax, AngleSpeed);
                    if(newAngle < 0) newAngle += 360;

                    if(Mathf.Approximately(angle, newAngle) == false)
                    {
                        if(this.bodyContacts.Count > 0)
                        {
                            foreach(var kv in this.bodyContacts)
                            {
                                var body = kv.Key;
                                var deltaCenter = (Vector2)this.transformBarCenter.position - body.position;
                                var vectorPush = -deltaCenter.normalized;
                                var power = Mathf.Max(0, 50f - deltaCenter.magnitude * 20f);
                                body.velocity += vectorPush * power;
                            }
                            this.bodyContacts.Clear();
                        }

                        this.transformBar.localRotation = Quaternion.Euler(0, 0, newAngle);
                    }
                }
                else
                {
                    var newAngle = Mathf.MoveTowardsAngle(angle, this.angleMin, AngleSpeed);
                    if(Mathf.Approximately(angle, newAngle) == false)
                        this.transformBar.localRotation = Quaternion.Euler(0, 0, newAngle);

                }
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                this.bodyContacts[collision.rigidbody] = true;
            }

            private void OnCollisionExit2D(Collision2D collision)
            {
                this.bodyContacts.Remove(collision.rigidbody);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_player_jump_1"]);
                Audio.instance.PlayRandomSfx(null, null,
                    game.assets.audioClipLookup["sfx_player_jump_1"],
                    game.assets.audioClipLookup["sfx_player_jump_2"],
                    game.assets.audioClipLookup["sfx_player_jump_3"]
                );
            }
        }
    }
}
