using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace minigame
{
    namespace pinball
    {
        public partial class PinballGame : Minigame
        {
            [Header("Pinball Game")]
            public GameObject ballsLeftPortrait;
            public GameObject ballsLeftLandscape;
            public Image[] imageBalls;
            public TMP_Text scoreText;// currently disabled, may reinstate later
            public SpriteRenderer backgroundWall;
            public Camera cameraShadow;
            public MeshRenderer meshShadow;
            public RenderTexture renderTextureShadow;
            [NonSerialized] public Vector3 camOffset;
            [NonSerialized] public List<Ball> balls = new List<Ball>();
            [NonSerialized] public List<Flipper> flippers = new List<Flipper>();
            [NonSerialized] public Launcher launcher;
            [NonSerialized] public List<Launcher> launcherCheckpoints = new List<Launcher>();
            public Dictionary<Vector2Int, int> finishLookup = new Dictionary<Vector2Int, int>();
            public int ballsLeft;
            public int score;
            public int queuedBalls;
            public const float DeltaBackgroundHeight = -0.5f;
            public const float DeltaBackgroundLandscapeHeight = -2f;
            [NonSerialized] public Vector3 positionLastBall;
            [NonSerialized] public Transform transformBackgroundLandscape;

            private List<ScoreGate> scoreGates = new List<ScoreGate>();

            public const float Hole2BallDelay = 0.002f;// multplied by distance between hole and ball
            public const float BeforeBall2HoleDelay = 1f;
            public const float PanDelay = 0.02f;// multplied by distance
            public const float BumperPower = 20f;
            public static int shadowSortingOrder = 0;

            void Start()
            {
                Scale = 1f;
                ScoreRise.Layer = this.objectLayers.fxSortingLayer;
                ScoreRise.Spacing = -0.05f * Scale;
                ScoreRise.RiseDist = 2f * Scale;
                ScoreRise.RiseDelay = 1f * Scale;
                ScaleOne = new Vector2(Scale, Scale);
                ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
                ScreenWidth = 16;
                if(GameCamera.IsLandscape())
                {
                    ballsLeftPortrait.SetActive(false);
                    imageBalls = ballsLeftLandscape.GetComponentsInChildren<Image>().Skip(1).ToArray();
                }
                else
                    ballsLeftLandscape.SetActive(false);

                Init();// Minigame setup

                this.pickupMap.onPickupCollected += (pickup) =>
                {
                    switch(pickup.name)
                    {
                        case "GLOBAL:coin":
                            AddScore((Vector2)pickup.transform.position + Vector2.up * Scale, 10);
                            break;
                    }
                };
                
                // shadow mesh / camera / renderTexture
                this.meshShadow.transform.localScale = new Vector3(camCtrl.rect.width, camCtrl.rect.height, 1f);
                this.cameraShadow.orthographicSize = this.cam.orthographicSize;
                this.renderTextureShadow = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32);
                this.renderTextureShadow.Create();
                this.cameraShadow.targetTexture = this.renderTextureShadow;
                this.meshShadow.material.SetTexture("_MainTex", this.renderTextureShadow);

                Minigame.RetrySubtitle = "no balls left!";
                CamChangeInterpolation = 0.1f;

                Restart(true);

#if UNITY_EDITOR
                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.levelRect.width * 0.5f, this.playerObj.transform.localPosition.y + 8f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);

                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, ()=> { UpdateBackground(); this.cameraShadow.Render(); });
                UpdateBackground();
#endif
            }

            void OnApplicationQuit()
            {
                Save();
            }

            public override void Save()
            {
                base.Save();
            }

            public override bool IsFailed()
            {
                return 
                    this.ballsLeft <= 0 && 
                    this.balls.Count + this.queuedBalls == 1 && 
                    this.balls[0].state == Ball.State.Lost;
            }
            public override bool IsComplete()
            {
                foreach(var b in this.balls)
                {
                    if(b.state == Ball.State.Finish) return true;
                }
                return false;
            }

            public override void Restart(bool hard)
            {
                base.Restart(hard);

                if(this.renderTextureShadow != null)
                    this.renderTextureShadow.Release();
                this.ballsLeft = 3;
                UpdateBallsLeft();
                this.balls.Clear();
                this.flippers.Clear();
                this.finishLookup.Clear();
                this.scoreGates.Clear();
                this.launcherCheckpoints.Clear();
                this.queuedBalls = 0;
                this.score = 0;
                if(this.transformBackgroundLandscape != null)
                    Destroy(this.transformBackgroundLandscape.gameObject);
                //this.scoreText.text = this.score.ToString("D6");
                this.camCtrl.clampAction = this.camCtrl.Clamp;

                this.fileLevel = GetLevelFile(difficulty, level);
                //this.fileLevel = DebugPanelLevelsDropdown.level ?? this.assets.levelTest;

                CreateLevel(this.fileLevel);
                this.input.holding = false;
                this.camOffset = Vector3.zero;
                this.camCtrl.SetPosition(GetCameraTargetDefault(), Vector3.zero);
            }

            public override void ExitGame()
            {
                base.ExitGame();
                this.renderTextureShadow.Release();
                this.renderTextureShadow = null;
            }

            void Update()
            {
                this.input.Advance();


                // debug multiball
                //if(Input.GetKeyDown(KeyCode.B))
                //AddMultiBall();


#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
#endif

                if(Minigame.paused == false)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }
            }

            void FixedUpdate()
            {

                if(IsFailed() == true)
                {
                    // look just above the ball, so the continue cost doesn't cover it
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.positionLastBall + Vector3.up * 3f, 0.1f), Vector3.zero);
                    return;
                }
                if(IsComplete() == true)
                {
                    // look just above the ball
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, GetCameraTargetDefault() + Vector3.up * 1f, 0.1f), Vector3.zero);
                    return;
                }

                if(this.camCtrl.lookAtTween == null)
                {
                    if(this.queuedBalls > 0)
                    {
                        if(IsLauncherFree())
                        {
                            var obj = Instantiate(this.assets.prefabLookup["player"], this.launcher.transform.localPosition, Quaternion.identity, transform);
                            obj.name = "player";
                            ObjectLayers.SetSortingLayer(obj, this.objectLayers.playerSortingLayer);
                            var ball = obj.GetComponent<Ball>();
                            ball.launcher = this.launcher;
                            ball.Init(this);
                            this.balls.Add(ball);
                            this.launcher.Load(ball.body);
                            this.queuedBalls--;
                        }
                    }
                    UpdateMotors();
                    this.camCtrl.AdvanceShakes(Time.fixedDeltaTime);
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, GetCameraTargetDefault(), 0.05f), this.camOffset);
                }
                else
                    this.camCtrl.UpdateLookAt(Time.fixedDeltaTime);

                if(this.ballsLeft == 1 && this.launcher.rigidBody2DBall != null)
                    this.imageBalls[0].transform.localScale = Vector3.one * (1f + Shake.Wave(fixedDeltaTimeElapsed, 0.5f, 0.25f));

                UpdateBackground();
                Minigame.fixedDeltaTimeElapsed += Time.fixedDeltaTime;
            }

            public void UpdateBackground()
            {
                // look at the bottom edge of the camera rect
                var focusPoint = this.cam.transform.localPosition + Vector3.down * camCtrl.rect.height * 0.5f;
                focusPoint.z = focusPoint.x = 0;
                // offset background wall
                //backgroundWall.transform.position = Vector3.up * DeltaBackgroundHeight - focusPoint * (DeltaBackgroundHeight / (levelRect.height - camCtrl.rect.height));
                backgroundWall.transform.position = Vector3.up * DeltaBackgroundHeight - focusPoint * (DeltaBackgroundHeight / (levelRect.height - camCtrl.rect.height));
                if(transformBackgroundLandscape != null)
                    transformBackgroundLandscape.transform.position = Vector3.up * DeltaBackgroundLandscapeHeight - focusPoint * (DeltaBackgroundLandscapeHeight / (levelRect.height - camCtrl.rect.height));

            }

            // debug scrolling for testing background
            //float tempy = 0f;
            public override Vector3 GetCameraTargetDefault()
            {
                //var speed = 0.2f;
                //if(Input.GetKey(KeyCode.UpArrow)) tempy += speed;
                //if(Input.GetKey(KeyCode.DownArrow)) tempy -= speed;
                //var pos = HighestBall().transform.position + Vector3.up * tempy;

                var pos = HighestBall().transform.position;
                pos.x = this.levelRect.width * 0.5f;
                return pos;
            }

            //public Ball LowestBall()
            //{
            //    var bestY = float.MaxValue;
            //    Ball ball = null;
            //    foreach(var b in this.balls)
            //    {
            //        var y = b.transform.position.y;
            //        if(y < bestY)
            //        {
            //            bestY = y;
            //            ball = b;
            //        }
            //    }
            //    return ball;
            //}
            public Ball HighestBall()
            {
                var bestY = float.MinValue;
                Ball ball = null;
                foreach(var b in this.balls)
                {
                    var y = b.transform.position.y;
                    if(y > bestY)
                    {
                        bestY = y;
                        ball = b;
                    }
                }
                return ball;
            }

            public int BallCount()
            {
                return this.balls.Count + this.queuedBalls;
            }

            public void SpendBall()
            {
                this.ballsLeft--;
                UpdateBallsLeft();
            }

            public void UpdateBallsLeft()
            {
                for(int i = 0; i < this.imageBalls.Length; i++)
                    this.imageBalls[i].enabled = i < this.ballsLeft;
                if(this.ballsLeft == 0) this.imageBalls[0].transform.localScale = Vector3.one;
            }

            public override void BreakTile(Collision2D collision)
            {
                this.AddScore(
                    (Vector2)collision.otherCollider.transform.localPosition + collision.otherCollider.offset,
                    BreakTileSpec.Specs[collision.otherCollider.name].score
                );
                var rb = collision.collider.GetComponent<Rigidbody2D>();
                if(rb != null)
                {
                    rb.velocity += 5f * -collision.contacts[0].normal;
                }
                base.BreakTile(collision);
            }

            public void EventPause()
            {
                for(int i = 0; i < this.balls.Count; i++)
                    this.balls[i].body.simulated = false;
            }

            public void EventUnpause()
            {
                for(int i = 0; i < this.balls.Count; i++)
                    this.balls[i].body.simulated = true;
            }

            public void AddMultiBall()
            {
                this.queuedBalls++;
            }

            public void AddScore(Vector2 pos, int value)
            {
                this.score += value;
                //this.scoreText.text = this.score.ToString("D6");
                if(this.scoreGates.Count > 0)
                {
                    var obj = Instantiate(this.assets.prefabLookup["score_rise"], pos, Quaternion.identity, transform);
                    var scoreRise = obj.GetComponent<ScoreRise>();
                    scoreRise.Init(value, this.spriteRendererPool, this.assets, "", "GLOBAL:num_");
                    for(int i = this.scoreGates.Count - 1; i > -1; i--)
                    {
                        var sg = this.scoreGates[i];
                        if(this.score >= sg.value)
                        {
                            sg.Clear();
                            this.scoreGates.RemoveAt(i);
                        }
                        else
                            sg.UpdateScore(this.score);
                    }
                }
            }

            public bool IsLauncherFree()
            {
                int i = 0;
                for(; i < this.balls.Count; i++)
                {
                    var b = this.balls[i];
                    if(b.state != Ball.State.Play && b.launcher == this.launcher)
                    {
                        return false;
                    }
                }
                return true;
            }


        }
    }
}

