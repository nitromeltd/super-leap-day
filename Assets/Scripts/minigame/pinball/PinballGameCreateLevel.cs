using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace minigame
{
    namespace pinball
    {

        public partial class PinballGame : Minigame
        {

            public void CreateLevel(TextAsset file)
            {

                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                );
                this.map.AddNoAutoTiles(""
                    , "polysquare"
                    , "poly45br"
                    , "poly22br"
                    , "poly67br"
                    , "break_tile"
                    , "score_gate"
                    , "cloud_slope_down"
                    , "cloud_slope_up"
                    , "cloud_down"
                );
                this.map.AddDontRotateTiles(
                    "spinny_cannon"
                    , "conveyor"
                    , "player"
                    , "flipper_left"
                    , "flipper_right"
                    , "launcher_checkpoint"
                );
                //this.map.AddFilledTiles();
                this.map.AddFilledRectInts(
                    ("launcher_cloud", new RectInt(0, 0, 4, 4))
                );

                var scoreGateTiles = new Dictionary<Vector2Int, GameObject>();
                var slopeColliders = new Dictionary<Vector2Int, Collider2D>();
                var slopeNames = new Dictionary<Vector2Int, string>();

                var addToTiles = new List<Transform>();

                var level = this.map.ParseLevel(file);

                var tiles = level.TileLayerByName("Tiles");
                var objects = level.TileLayerByName("Objects");
                
                // find every finish without a finish below and place a cloud tile on tiles layer below
                for(int i = 0; i < objects.tiles.Length; i++)
                {
                    var t = objects.tiles[i];
                    if(t.tile == "finish")
                    {
                        var tBelow = objects.tiles[i - this.map.width];
                        if(tBelow.tile != "finish")
                        {
                            var tt = tiles.tiles[i - this.map.width];
                            if(tt.tile == null)
                            {
                                tt.tile = GlobalPrefix + "topline";
                                tiles.tiles[i - this.map.width] = tt;
                            }
                        }
                    }
                }

                // generate all objects
                this.map.Create(level);

                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;


                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        this.spikesLookup[record.p] = 1;
                        record.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
                    }

                    if(record.isPrefab == true)
                    {

                        switch(record.name)
                        {
                            case "player":
                                {
                                    this.playerObj = record.gameObject;
                                    var ball = record.GetComponent<Ball>();
                                    record.gameObject.name = record.name;
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                    // create launcher
                                    var objLauncher = Instantiate(this.assets.prefabLookup["launcher"], record.pos, Quaternion.identity, this.map.transformObjects);
                                    ball.launcher = this.launcher = objLauncher.GetComponent<Launcher>();
                                    ball.spend = true;
                                    this.launcher.Load(ball.body);
                                    this.balls.Add(ball);
                                    ObjectLayers.SetSortingLayer(objLauncher, this.objectLayers.foregroundSortingLayer);
                                    launcher.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                    this.launcher.SetRotation(record.tile);
                                }
                                break;
                            case "launcher_checkpoint":
                                {
                                    var launcher = record.GetComponent<Launcher>();
                                    this.launcherCheckpoints.Add(launcher);
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    launcher.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                    launcher.rank = record.tile.properties["rank"].i;
                                    launcher.SetRotation(record.tile);
                                }
                                break;
                            case "break_tile":
                                {
                                    var collisionAction = record.GetComponent<OnCollisionAction>();
                                    collisionAction.action = BreakTile;
                                    var bc = collisionAction.GetComponent<Collider2D>() as BoxCollider2D;
                                    var sr = record.GetComponent<SpriteRenderer>();
                                    sr.sprite = this.assets.spriteLookup[record.nameTile];
                                    var s = bc.size;
                                    switch(record.nameTile)
                                    {
                                        case "break_block_big_purple": goto case "break block big";
                                        case "break_block_big_red": goto case "break block big";
                                        case "break block big":
                                            s *= 2;
                                            break;
                                        case "break_block_portrait_blue": goto case "break block portrait";
                                        case "break_block_portrait_yellow": goto case "break block portrait";
                                        case "break block portrait":
                                            s.y += 1;
                                            break;
                                        case "break_block_landscape_purple": goto case "break block landscape";
                                        case "break_block_landscape_yellow": goto case "break block landscape";
                                        case "break block landscape":
                                            s.x += 1;
                                            break;
                                    }
                                    bc.offset = s / 2;
                                    bc.size = s;
                                }
                                break;
                            case "bumper_4":
                            case "bumper_3":
                            case "bumper_2":
                            case "bumper_1":
                            case "bumper_4x2_up":
                            case "bumper_2x4_up":
                            case "bumper_3x3_up":
                            case "bumper_4x4_up":
                            case "bumper_4x2_down":
                            case "bumper_2x4_down":
                            case "bumper_3x3_down":
                            case "bumper_4x4_down":
                            case "bumper":
                                record.GetComponent<Bumper>().game = this;
                                break;
                            case "bumper_breakable":
                                record.GetComponent<BumperBreakable>().Init(this, record.tile.properties["health"].i);
                                break;
                            case "launcher_cloud":
                                
                                addToTiles.Add(record.gameObject.transform);
                                break;
                            case "flipper_right":
                            case "flipper_left":
                                {
                                    var flipper = record.gameObject.GetComponentInChildren<Flipper>();
                                    flipper.Init(this, record.nameTile == "flipper_right");
                                    this.flippers.Add(flipper);
                                }
                                break;
                            case "score_gate":
                                {
                                    var component = record.GetComponent<ScoreGate>();
                                    this.scoreGates.Add(component);
                                    component.Init(this, record.tile.properties["score"].i, record.p, level.TileLayerByName("Tiles"), this.map.width);
                                }
                                break;
                            case "cloud_slope_down":
                            case "cloud_slope_up":
                            case "cloud_down":
                                slopeColliders[record.p] = record.GetComponent<EdgeCollider2D>();
                                slopeNames[record.p] = record.name;
                                break;
                            case "spinny_cannon":
                                {
                                    var cannon = record.GetComponent<SpinnyCannon>();
                                    cannon.Init(record.tile);
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    cannon.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                }
                                break;
                            case "conveyor_left":
                            case "conveyor_right":
                                {
                                    var a = record.GetComponent<Animated>();
                                    MinigameMap.AddAutotiling(a, record.tile, record.x, record.y, this.assets.autotileArrayLookup[Minigame.GlobalPrefix + record.name], level);
                                }
                                break;
                            case "splash_illustration_3":
                            case "splash_illustration_2":
                            case "splash_illustration_1":
                                ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount);
                                this.spriteRendererPool.sortingOrderCount += 2;
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        switch(record.tile.tile)
                        {
                            case "finish":
                                this.finishLookup[record.p] = 1;
                                ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.backgroundSortingLayer);
                                break;
                        }
                    }
                }

                foreach(var chk in this.launcherCheckpoints)
                    chk.posBall = this.balls[0].transform.localPosition;

                // cloud slopes
                var slopeEnds = this.map.tileGrid.MergeSlopes(slopeNames, slopeColliders);
                foreach(var kv in slopeColliders)
                {
                    var sr = kv.Value.GetComponent<SpriteRenderer>();
                    sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_left"];
                }
                foreach(var kv in slopeEnds)
                {
                    var sr = kv.Value.GetComponent<SpriteRenderer>();
                    sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_right"];
                }

                foreach(var transform in addToTiles) transform.SetParent(this.map.transformTiles, true);



                // create some room for the splash illustration starburst
                this.spriteRendererPool.sortingOrderCount = 0;
                BuildBackground();

                // create side fill in landscape mode
                if(GameCamera.IsLandscape() == true)
                {
                    float heightFloor = this.camCtrl.rectDefault.height + this.map.height;
                    float halfCamHeight = this.camCtrl.rectDefault.height * 0.5f;
                    SpriteRenderer sr;
                    Vector2 size = new Vector2(2, heightFloor);
                    // tile fill
                    sr = this.spriteRendererPool.Get(new Vector3(-2, -halfCamHeight), this.assets.spriteLookup["floor_fill"], this.objectLayers.backgroundSortingLayer, "fill_left", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                    sr = this.spriteRendererPool.Get(new Vector3(this.map.width, -halfCamHeight), this.assets.spriteLookup["floor_fill"], this.objectLayers.backgroundSortingLayer, "fill_right", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;

                    // fancy sides
                    var obj = new GameObject("Background Landscape");
                    this.transformBackgroundLandscape = obj.transform;


                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width * 0.5f - ScreenWidth * 0.5f, -halfCamHeight), this.assets.spriteLookup["Apple TV Pinball BG left"], this.objectLayers.backgroundSortingLayer, "Apple TV Pinball BG left", this.transformBackgroundLandscape);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor + DeltaBackgroundLandscapeHeight);
                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width * 0.5f + ScreenWidth * 0.5f, -halfCamHeight), this.assets.spriteLookup["Apple TV Pinball BG right"], this.objectLayers.backgroundSortingLayer, "Apple TV Pinball BG right", this.transformBackgroundLandscape);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor + DeltaBackgroundLandscapeHeight);

                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width + 1.8f, -halfCamHeight), this.assets.spriteLookup["Apple TV Pinball Shadow"], this.objectLayers.backgroundSortingLayer, "Apple TV Pinball Shadow", this.transformBackgroundLandscape);
                    sr.transform.localScale = new Vector3(1, (heightFloor + DeltaBackgroundLandscapeHeight) / sr.size.y, 1);

                    sr = this.spriteRendererPool.Get(new Vector3(-2, -halfCamHeight), this.assets.spriteLookup["Apple TV Pinball BG edges left"], this.objectLayers.backgroundSortingLayer, "Apple TV Pinball BG edges left", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor);
                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width + 2, -halfCamHeight), this.assets.spriteLookup["Apple TV Pinball BG edges right"], this.objectLayers.backgroundSortingLayer, "Apple TV Pinball BG edges right", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor);

                }
                // create top / bottom fill for Retry screen
                {
                    SpriteRenderer sr;
                    Vector2 size = new Vector2(this.map.width, this.camCtrl.rectDefault.height * 0.5f);
                    sr = this.spriteRendererPool.Get(new Vector3(0, this.map.height), this.assets.spriteLookup["floor_fill"], this.objectLayers.tilesSortingLayer, "fill_top", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                    sr = this.spriteRendererPool.Get(new Vector3(0, -size.y), this.assets.spriteLookup["floor_fill"], this.objectLayers.tilesSortingLayer, "fill_bottom", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                }

                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "player":
                            {
                                var tb = conn.objA.GetComponent<TriggerButton>();
                                tb.Init(this);
                                tb.multiball = true;
                            }
                            break;
                        case "button":
                            {
                                var tb = conn.objB.GetComponent<TriggerButton>();
                                tb.Init(this);
                                tb.targets.Add(conn.a);
                                triggerButtons.Add(tb);
                            }
                            break;
                        case "portal_out_3":
                        case "portal_out_2":
                        case "portal_out_1":
                            var portal = conn.objA.GetComponentInChildren<Portal>();
                            var destAnim = conn.objB.GetComponent<PlayAnimated>();
                            ObjectLayers.SetSortingLayer(conn.objA, this.objectLayers.backgroundSortingLayer, PinballGame.shadowSortingOrder - 10);
                            ObjectLayers.SetSortingLayer(conn.objB, this.objectLayers.backgroundSortingLayer, PinballGame.shadowSortingOrder - 10);
                            portal.Init(this, destAnim);
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                        else if(this.map.vector2IntToGameObject.TryGetValue(target, out GameObject obj) == true)
                        {
                            if(obj.name == "launcher")
                                tb.multiball = true;
                        }
                    }
                }

                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);

                this.balls[0].Init(this);
            }

            public void BuildBackground()
            {
                this.backgroundWall.sortingLayerName = this.objectLayers.backgroundSortingLayer;

                foreach(Transform childTransform in this.backgroundWall.transform)
                    Destroy(childTransform.gameObject);

                this.backgroundWall.transform.position = Vector3.zero;

                // shorten background to match level when parallaxed
                var backgroundHeight = this.map.height + DeltaBackgroundHeight;
                var size = this.backgroundWall.size;
                this.backgroundWall.size = new Vector2(size.x, backgroundHeight);

                // place shapes
                var shapes = this.assets.spriteArrayLookup["background_shapes"].list;
                var shapesDeck = new List<Sprite>(shapes);
                // set spacing to biggest shape
                var spacing = 0f;
                for(int i = 0; i < shapes.Length; i++)
                {
                    var s = shapes[i];
                    if(s.rect.height / s.pixelsPerUnit > spacing) spacing = s.rect.height / s.pixelsPerUnit;
                }
                var pos = new Vector2(0, -spacing * 0.5f);
                while(pos.y < backgroundHeight - spacing * 0.5f)
                {
                    var s = RandomUtil.Draw(shapesDeck, shapes);
                    var w = s.rect.width / s.pixelsPerUnit;
                    w = w * 0.125f + UnityEngine.Random.value * w * 0.125f;
                    if(pos.x < this.map.width * 0.5f)
                        pos.x = this.map.width - w;
                    else
                        pos.x = w;
                    pos.y += UnityEngine.Random.value * spacing * 0.5f + spacing * 0.5f;
                    this.spriteRendererPool.Get(pos, s, this.objectLayers.backgroundSortingLayer, s.name, this.backgroundWall.transform);
                }

                // find spike groups
                var spikeMap = new Dictionary<Vector2Int, string>();
                foreach(var kv in this.spikesLookup)
                    spikeMap[kv.Key] = "s";

                List<TileGrid.MergeData> spikeGroups = TileGrid.MergeData.GetMergers(spikeMap, levelRectInt, new Dictionary<string, bool> { ["s"] = true });
                for(int i = 0; i < spikeGroups.Count; i++)
                {
                    var sg = spikeGroups[i];
                    var fab = Instantiate(this.assets.prefabLookup["death_zone"], this.backgroundWall.transform);
                    fab.transform.localPosition = new Vector3(sg.rect.x + sg.rect.width * 0.5f, sg.rect.y + DeltaBackgroundHeight);
                    ObjectLayers.SetSortingLayer(fab, this.objectLayers.backgroundSortingLayer, this.spriteRendererPool.sortingOrderCount++);
                    var transforms = fab.GetComponentsInChildren<Transform>();
                    if(sg.rect.width <= 4f)
                    {
                        // too thin, remove backing and mask
                        transforms[1].SetParent(this.backgroundWall.transform, true);
                        Destroy(transforms[0].gameObject);
                    }
                    else // set mask width
                        transforms[2].localScale = new Vector3(sg.rect.width, transforms[2].localScale.y, 1);
                }

                // set shadow above background
                this.meshShadow.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                PinballGame.shadowSortingOrder = this.spriteRendererPool.sortingOrderCount + 100;
                this.meshShadow.sortingOrder = PinballGame.shadowSortingOrder;
            }
        }
    }
}
