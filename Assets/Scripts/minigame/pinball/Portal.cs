using UnityEngine;
using System.Collections;

namespace minigame
{
    namespace pinball
    {
        public class Portal : MonoBehaviour
        {
            public PinballGame game;
            public PlayAnimated destAnim;
            private Rigidbody2D bodyBall;
            public Sprite spriteExitStar;
            public Sprite[] spriteEnterParticles;
            public Transform[] transformSpins;
            public Transform[] transformWobbles;

            public const int ParticleLifetime = 30;

            private bool entering;

            public void Init(PinballGame game, PlayAnimated destAnim)
            {
                this.game = game;
                this.destAnim = destAnim;
                StartCoroutine(SpawnEnterParticle());
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.game != null)
                {
                    if(collision.name.Contains("player") == true && game.IsComplete() == false)
                    {
                        this.bodyBall = collision.attachedRigidbody;
                        this.bodyBall.simulated = false;
                        this.entering = true;
                        StartCoroutine(ScaleTransform(this.bodyBall.transform, 1f, 0f, 0.25f));
                        if(this.game.balls.Count == 1)
                        {
                            this.game.camCtrl.PauseAndPanTo(this.destAnim.transform.localPosition, 0.5f, PinballGame.PanDelay, 0.5f, Exit);
                        }
                        else
                            Exit();
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_portal_in"]);
                    }
                }
            }

            void FixedUpdate()
            {
                if(this.bodyBall != null && this.entering == true)
                    this.bodyBall.transform.localPosition = Vector3.Lerp(this.bodyBall.transform.localPosition, this.transform.parent.localPosition, 0.2f);
                for(int i = 0; i < this.transformSpins.Length; i++)
                    this.transformSpins[i].localRotation = Quaternion.Euler(0, 0, this.transformSpins[i].eulerAngles.z + 2 + i * 0.25f);
                foreach(var t in this.transformWobbles)
                    t.localScale = Vector3.one + Vector3.one * Shake.Wave(Minigame.fixedDeltaTimeElapsed, 1f, 0.025f);
            }

            void Exit()
            {
                StartCoroutine(SpitOutBall());
            }

            IEnumerator SpawnEnterParticle()
            {
                yield return new WaitForSeconds(Random.Range(0.1f, 0.5f));
                var p = Particle.CreateWithSprite(RandomUtil.Pick(spriteEnterParticles), 30, this.transform.parent.localPosition, this.transform.parent);
                p.velocity = RandomUtil.Normal() * 0.03f;
                p.ScaleOut(this.game.assets.animationCurveLookup["portal_particle_scale"]);
                p.FadeOut(this.game.assets.animationCurveLookup["portal_particle_scale"]);
                p.spriteRenderer.sortingLayerName = this.game.objectLayers.backgroundSortingLayer;
                p.spriteRenderer.sortingOrder = (PinballGame.shadowSortingOrder - 10) + 4;// sort under the top layer
                StartCoroutine(SpawnEnterParticle());
                p.Advance();
            }

            IEnumerator ScaleTransform(Transform target, float start, float finish, float delay)
            {
                var step = Mathf.Abs((finish - start) / (delay * Application.targetFrameRate));
                var scale = start;
                while(Mathf.Approximately(scale, finish) == false)
                {
                    target.localScale = Vector3.one * scale;
                    scale = Mathf.MoveTowards(scale, finish, step);
                    yield return null;
                }
                target.localScale = Vector3.one * finish;
            }

            IEnumerator SpitOutBall()
            {
                this.entering = false;
                this.destAnim.PlayOnce();
                this.bodyBall.transform.localPosition = this.destAnim.transform.localPosition;
                yield return new WaitForSeconds(0.6f);
                for(int i = 0; i < 5; i++)
                {
                    var p = Particle.CreateWithSprite(spriteExitStar, 25 + Random.Range(0, 15), this.destAnim.transform.localPosition, this.transform.parent);
                    p.ScaleOut();
                    p.FadeOut(this.game.assets.animationCurveLookup["portal_star_fade"]);
                    p.spin = Random.Range(10, 20);
                    p.velocity = RandomUtil.Normal() * Random.Range(0.055f, 0.07f);
                    p.spriteRenderer.sortingLayerName = this.game.objectLayers.backgroundSortingLayer;
                    p.spriteRenderer.sortingOrder = (PinballGame.shadowSortingOrder - 10) + 6;// sort over portal
                    p.Advance();
                }
                this.bodyBall.simulated = true;
                StartCoroutine(ScaleTransform(this.bodyBall.transform, 0f, 1f, 0.05f));
                this.bodyBall = null;
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_portal_out"]);
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_env_portal"]);
            }


        }
    }
}
