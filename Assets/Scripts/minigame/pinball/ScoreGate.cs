using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace pinball
    {
        public class ScoreGate : MonoBehaviour
        {

            public int value;
            public BoxCollider2D boxCollider2D;
            public Animated animLeft;
            public Animated animRight;
            public SpriteRenderer spriteRendererLeft;
            public SpriteRenderer spriteRendererRight;
            public TMPro.TMP_Text text;

            private PinballGame game;
            private Vector2Int pos;
            private int width;

            public void Init(PinballGame game, int value, Vector2Int pos, NitromeEditor.TileLayer tileLayer, int levelWidth)
            {
                this.game = game;
                this.value = value;
                this.pos = pos;
                this.width = 4;
                this.text.text = value.ToString();

                // expand gate
                for(int x = this.pos.x + this.width; x < levelWidth; x++)
                {
                    var t = tileLayer.tiles[x + this.pos.y * levelWidth];
                    if(t.tile != null)
                    {
                        var w = x - (this.pos.x + 4);
                        if(w > 0)
                        {
                            this.spriteRendererRight.size = new Vector2(w, 1f);
                            this.boxCollider2D.size += Vector2.right * w;
                            this.boxCollider2D.offset += Vector2.right * w * 0.5f;
                            this.width += w;
                        }
                        break;
                    }
                }
                for(int x = this.pos.x - 1; x > -1; x--)
                {
                    var t = tileLayer.tiles[x + this.pos.y * levelWidth];
                    if(t.tile != null)
                    {
                        var w = this.pos.x - x;
                        if(w > 0)
                        {
                            this.spriteRendererLeft.size = new Vector2(w, 1f);
                            this.spriteRendererLeft.transform.localPosition = Vector3.left * w;
                            this.boxCollider2D.size += Vector2.right * w;
                            this.boxCollider2D.offset += Vector2.left * w * 0.5f;
                            this.width += w;
                        }
                        break;
                    }
                }


            }

            public void UpdateScore(int score)
            {
                var newValue = this.value - score;
                this.text.text = newValue.ToString();
            }

            public void Clear()
            {
                // move sides into fx anims
                this.animLeft.transform.SetParent(this.game.transform);
                this.animRight.transform.SetParent(this.game.transform);
                this.animLeft.PlayOnce(this.game.assets.animationLookup["gate fade"]);
                this.animRight.PlayOnce(this.game.assets.animationLookup["gate fade"]);

                var particle = this.game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "break_block_fx", transform.localPosition + (Vector3)Minigame.ScaleHalf + Vector3.right * 1.5f);
                particle.transform.localScale = new Vector3(2f, 0.5f, 1f);

                this.game.camCtrl.AddShake(Vector2.up, 0.25f, 1f, Minigame.Scale * 0.4f);
                Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_pinball_scoregate"]);
                Destroy(gameObject);
            }


        }
    }
}
