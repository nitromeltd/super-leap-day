using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace racing
    {
        public class BackgroundTV : MonoBehaviour
        {
            public SpriteRenderer spriteRendererChannel;
            public Animated animatedChangeChannel;
            public Animated.Animation animationChangeChannelIn;
            public Animated.Animation animationChangeChannelOut;

            public Sprite[] spriteChannels;

            private int indexChannel;
            private float time = 4f;
            private bool changingChannel;

            void Update()
            {
                if(this.changingChannel == false)
                {
                    this.time -= Time.deltaTime;
                    if(this.time <= 0)
                    {
                        this.changingChannel = true;
                        this.indexChannel = (this.indexChannel + 1) % this.spriteChannels.Length;
                        this.animatedChangeChannel.PlayOnce(this.animationChangeChannelIn, delegate
                        {
                            this.spriteRendererChannel.sprite = this.spriteChannels[this.indexChannel];
                            this.animatedChangeChannel.PlayOnce(this.animationChangeChannelOut);
                            this.time = 6f;
                            this.changingChannel = false;
                        });
                    }

                }
            }
        }

    }
}
