using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace racing
    {
        public class Boost : MonoBehaviour
        {
            private float resetCount;
            public const float ResetDelay = 0.25f;

            void FixedUpdate()
            {
                if(this.resetCount < ResetDelay)
                    this.resetCount += Time.fixedDeltaTime;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(collision.name == "player" && this.resetCount >= ResetDelay)
                {
                    var playerCar = collision.GetComponent<PlayerCar>();
                    if(playerCar != null)
                    {
                        playerCar.Boost();
                        Audio.instance.PlaySfx(playerCar.game.assets.audioClipLookup["sfx_env_gripgrounds_boost"]);
                    }
                }
            }
        }
    }
}
