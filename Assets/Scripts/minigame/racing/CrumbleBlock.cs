using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace racing
    {
        public class CrumbleBlock : MonoBehaviour
        {
            public Minigame game;
            public SpriteRenderer spriteRenderer;
            public Sprite spriteSolid;
            public Sprite spriteCrumble;
            public BoxCollider2D boxCollider2D;
            public LayerMask filter;

            private float count = 0f;
            private Rect rect;
            private int totalFx;

            public const float DelayBeforeCrumble = 0.5f;
            public const float ResetDelay = 2f;
            public static Color ColorFx = new Color32(248, 108, 126, 255);

            public void Init(Minigame game, Vector2 position, Vector2 size)
            {
                this.game = game;
                this.boxCollider2D.size = size;
                this.boxCollider2D.offset = size / 2;
                this.rect = new Rect(position, size);
                this.totalFx = (int)(this.rect.width * this.rect.height);
            }

            void FixedUpdate()
            {
                if(this.count > 0)
                {
                    this.count -= Time.fixedDeltaTime;
                    if(this.count <= ResetDelay && this.boxCollider2D.enabled)
                    {
                        spriteRenderer.sprite = null;
                        this.boxCollider2D.enabled = false;
                        this.game.AddDebrisRect("crumble_particles", this.rect, this.totalFx * 6);
                        this.game.CreateParticleRect("crumble_dust", this.rect, this.totalFx * 10, 5);
                    }
                }
                else if(this.boxCollider2D.enabled == false)
                {
                    Collider2D test = Physics2D.OverlapArea(this.transform.position, this.transform.position + (Vector3)this.boxCollider2D.size, this.filter.value);
                    if(test == null)
                    {
                        var particle = this.game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "break_block_fx", (Vector2)this.transform.position + this.boxCollider2D.offset);
                        particle.transform.localScale = new Vector3(this.boxCollider2D.size.x, this.boxCollider2D.size.y, 1f);
                        particle.spriteRenderer.color = ColorFx;
                        this.spriteRenderer.sprite = this.spriteSolid;
                        this.boxCollider2D.enabled = true;
                        this.game.CreateParticleRect("crumble_dust", this.rect, this.totalFx * 10, 5);
                    }
                }
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                if(this.count <= 0)
                {
                    this.spriteRenderer.sprite = this.spriteCrumble;
                    this.count = ResetDelay + DelayBeforeCrumble;
                    Audio.instance.PlayRandomSfx(null, this.transform.position, 
                        this.game.assets.audioClipLookup["sfx_crumble_1"], 
                        this.game.assets.audioClipLookup["sfx_crumble_2"], 
                        this.game.assets.audioClipLookup["sfx_crumble_3"]
                    );
                    this.game.CreateParticleRect("crumble_dust", this.rect, this.totalFx * 3, 5);
                }
            }

        }

    }
}
