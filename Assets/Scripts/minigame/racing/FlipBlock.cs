using UnityEngine;


namespace minigame
{
    namespace racing
    {
        public class FlipBlock : MonoBehaviour
        {

            private float resetCount;
            public Animated anim;
            public Animated.Animation animAnim;

            public const float LiftPower = 5f;
            public const float TurnPower = 2.5f;
            public const float ResetDelay = 0.25f;


            void FixedUpdate()
            {
                if(this.resetCount < ResetDelay)
                    this.resetCount += Time.fixedDeltaTime;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.resetCount < ResetDelay) return;

                var car = collision.GetComponent<PlayerCar>();
                if(car != null)
                {
                    var up = this.transform.localRotation * Vector2.up;

                    if(Vector2.Dot(car.Normalized(), this.transform.localRotation * Vector2.left) < -0.9f)
                    {
                        var v = car.body.velocity;
                        v.y = 0;
                        car.body.velocity = v;
                        car.body.AddForce(up * LiftPower, ForceMode2D.Impulse);
                        car.body.AddForceAtPosition(up * TurnPower, car.circleFrontWheel.transform.position, ForceMode2D.Impulse);
                        this.anim.PlayOnce(animAnim);
                        Audio.instance.PlaySfx(car.game.assets.audioClipLookup["sfx_flip_block"]);
                    }

                }
            }
        }

    }
}
