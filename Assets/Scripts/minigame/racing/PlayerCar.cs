using System;
using UnityEngine;


namespace minigame
{
    namespace racing
    {
        public class PlayerCar : MonoBehaviour
        {
            public State state;
            public RacingGame game;
            public Rigidbody2D body;
            public CircleCollider2D circleBackWheel;
            public CircleCollider2D circleFrontWheel;
            public Transform transformFrontWheel;
            public Transform transformBackWheel;
            public Transform transformChassis;
            public Transform transformFlagPole;
            public Transform transformFlipHolder;
            public SpriteRenderer spriteRendererCharacter;
            public SpriteRenderer spriteRendererArm;
            public SpriteRenderer spriteRendererInFront;

            private Vector3 start;
            [NonSerialized] public bool flipped;
            [NonSerialized] public bool flippedDriver;
            [NonSerialized] public float speed = 0f;
            [NonSerialized] public float length = 0f;
            private float defaultGravity;
            private Vector2Int tilePos;

            private bool dirtFront, dirtBack;
            private Vector2 downVector, moveVector;
            private RaycastHit2D? contactFront, contactBack;
            private float speedWheel;
            private Spring2D springChassis;
            private Spring2D springFlag;
            private float rotationFlagDefault;
            private float framesDustOffset;
            private int airCount;
            private float boostCount;

            public const float BoostForce = 500f;

            private const float Acc = 1f;
            private const float DecWheel = 1f;
            private const float MaxSpeed = 35f;
            private const float DirtDamping = 2f;
            private const float WheelFloorRotationConst = 2f;
            private const float FlipTolerance = 0.2f;
            private const float ConveyorSpeed = 6.375f;
            private const float BoostDelay = 0.5f;

            private bool isPaused = false;
            private bool hasStopped = true;
            private bool isAccelerationOn = false;
            private bool isIdleOn = false;
            public enum State
            {
                None, Drive, Dead, TimeOut, Finish
            }

            private float[] velocities = new float[180];
            public float velocityAvg;

            public void Init(RacingGame game)
            {
                this.game = game;
                this.start = transform.localPosition;
                this.defaultGravity = this.body.gravityScale;
                this.length = this.circleFrontWheel.transform.position.x - this.circleBackWheel.transform.position.x;
                this.springChassis = new Spring2D(Vector2.zero);
                this.springFlag = new Spring2D(Vector2.zero);
                this.rotationFlagDefault = this.transformFlagPole.eulerAngles.z;
                this.framesDustOffset = this.game.assets.animationLookup[Minigame.GlobalPrefix + "dust"].sprites.Length / 2;
                this.boostCount = 0f;
                this.spriteRendererCharacter.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteDriving;
                this.spriteRendererArm.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteDrivingArm;
                this.spriteRendererInFront.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteDrivingInFront;
                SetState(State.Drive);
                for(int i = 0; i < velocities.Length; i++)
                    velocities[i] = 0f;
            }

            public void SetState(State s)
            {
                this.airCount = 0;
                switch(s)
                {
                    case State.Drive:
                        break;
                    case State.TimeOut:
                        break;
                    case State.Dead:
                        this.body.angularVelocity = 0;
                        this.body.velocity = Vector2.zero;
                        this.body.simulated = false;
                        break;
                    case State.Finish:
                        Audio.instance.PlaySfx(game.assets.audioClipLookup["GLOBAL:sfx_pinball_goal"], Minigame.NoRandomPitch);
                        this.game.CompleteLevel();
                        this.body.gravityScale = this.defaultGravity;
                        break;
                }
                this.state = s;
            }

            void FixedUpdate()
            {
                if(this.game.startCount > 0)
                    return;

                UpdateState();
                UpdateGfx();
            }

            public void UpdateState()
            {
                switch(this.state)
                {
                    case State.Finish: goto case State.Drive;
                    case State.TimeOut: goto case State.Drive;
                    case State.Drive:

                        // handle user input
                        if(this.state == State.Drive && this.game.input.holding)
                        {
                            if(this.speed < MaxSpeed)
                            {
                                this.speed += Acc;
                                this.speedWheel = this.speed;
                            }
                        }
                        else
                        {
                            if(this.speed > 0 && this.boostCount <= 0) this.speed = 0;
                            this.speedWheel = Mathf.Max(this.speedWheel - DecWheel, 0f);
                        }
                        if(this.game.input.holding == true || this.boostCount > 0)
                        {
                            var invSpeedNormalized = 1.0f - Mathf.Min(this.speed, MaxSpeed) / MaxSpeed;
                            if(this.contactFront.HasValue == true && this.dirtFront == false)
                                AddDust(this.contactFront.Value.point, (int)(2 + this.framesDustOffset * invSpeedNormalized));
                            if(this.contactBack.HasValue == true && this.dirtBack == false)
                                AddDust(this.contactBack.Value.point, (int)(2 + this.framesDustOffset * invSpeedNormalized));
                        }

                        if (this.game.input.holding && this.state != State.Finish)
                        {
                            if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_engine_accelerate_loop"]) && !isPaused)
                            {
                                Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_engine_accelerate_loop"]);
                                StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_engine_accelerate_loop"],startFrom: 0, setTo: 0.5f, duration:.5f));
                                if (this.hasStopped && !Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_engine_ramp_up"]))
                                {
                                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_engine_ramp_up"], Minigame.NoRandomPitch);
                                    this.hasStopped = false;
                                }
                                if (Vector2.Dot(this.body.velocity.normalized, moveVector) == -1)
                                {
                                    Audio.instance.PlayRandomSfx( null, null,
                                        game.assets.audioClipLookup["sfx_wheel_spin_1"],
                                        game.assets.audioClipLookup["sfx_wheel_spin_2"],
                                        game.assets.audioClipLookup["sfx_wheel_spin_3"]
                                    );
                                }
                                if (Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_engine_idle_loop"]) && this.isIdleOn)
                                {
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_engine_idle_loop"], startFrom: 0.5f, setTo: 0f, duration: .5f));
                                    this.isIdleOn = false;
                                }
                                this.isAccelerationOn = true;
                            }
                        }
                        else if (this.state != State.Finish && !this.game.input.holding)
                        {
                            if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_engine_idle_loop"]) && this.speed == 0 && !isPaused)
                            {
                                Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_engine_idle_loop"]);
                                StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_engine_idle_loop"], startFrom:0, setTo: 0.5f, duration: .5f));
                            }
                            if (Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_engine_accelerate_loop"]) && this.isAccelerationOn)
                            {
                                StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_engine_accelerate_loop"], startFrom: 0.5f, setTo: 0f, duration: 1f));
                                this.isAccelerationOn = false;
                            }
                            if (this.body.velocity == Vector2.zero)
                            {
                                this.hasStopped = true;
                            }
                            this.isIdleOn = true;
                        }
                        if (this.speed > 0 || this.body.velocity.sqrMagnitude > 0.5f)
                        {
                            if(this.dirtFront == true)
                            {
                                AddMudDebris(this.contactFront.Value.point, 1 + this.speed > 0 ? 1 : 0);
                                if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_mud_loop"]))
                                {
                                    Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_mud_loop"]);
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_mud_loop"], startFrom:0 , setTo: 1f, duration: 1));
                                }
                            }

                            if (this.dirtBack == true)
                            {
                                AddMudDebris(this.contactBack.Value.point, 1 + this.speed > 0 ? 1 : 0);
                                if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_mud_loop"]))
                                {
                                    Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_mud_loop"]);
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_mud_loop"], startFrom: 0, setTo: 1f, duration: 1));
                                }
                            }
                        }
                        if ((!this.dirtBack && !this.dirtFront) || this.speed == 0)
                        {
                            Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_mud_loop"]);
                            StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_mud_loop"], startFrom: 1, setTo: 0f, duration: 1));

                        }

                        if (this.boostCount > 0)
                            this.boostCount -= Time.fixedDeltaTime;
                        else if(this.speed > MaxSpeed)
                            this.speed -= 0.5f;


                        // check for spikes
                        this.tilePos = Minigame.ToVector2Int(this.circleFrontWheel.transform.position);
                        if(this.game.spikesLookup.ContainsKey(this.tilePos))
                        {
                            Death();
                            break;
                        }
                        this.tilePos = Minigame.ToVector2Int(this.circleBackWheel.transform.position);
                        if(this.game.spikesLookup.ContainsKey(this.tilePos))
                        {
                            Death();
                            break;
                        }

                        // define directions
                        this.moveVector = Minigame.NormalFromZAngle(transform.localRotation.eulerAngles.z);
                        this.downVector = new Vector2(this.moveVector.y, -this.moveVector.x);

                        var wheelDelta = this.circleFrontWheel.transform.position.x - this.circleBackWheel.transform.position.x;
                        if(this.flipped == true)
                        {
                            if(wheelDelta > FlipTolerance) this.flipped = false;
                        }
                        else
                        {
                            if(wheelDelta < -FlipTolerance) this.flipped = true;
                        }
                        if(this.flipped) this.downVector *= -1;

                        // must test mid-point to avoid getting "beached" with wheels not touching floor
                        var midPos = Vector2.Lerp(this.circleBackWheel.transform.position, this.circleFrontWheel.transform.position, 0.5f);
                        var wheelSize = this.circleBackWheel.radius + 0.08f;

                        IsOnFloor(this.circleBackWheel.transform.position, wheelSize, this.downVector, ref this.contactFront);
                        IsOnFloor(this.circleFrontWheel.transform.position, wheelSize, this.downVector, ref this.contactBack);
                        this.dirtFront = this.contactFront.HasValue ? this.contactFront.Value.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["racing_dirt"] : false;
                        this.dirtBack = this.contactBack.HasValue ? this.contactBack.Value.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["racing_dirt"] : false;
                        var conveyorFront = this.contactFront.HasValue ? this.contactFront.Value.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["racing_conveyor"] : false;
                        var conveyorBack = this.contactBack.HasValue ? this.contactBack.Value.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["racing_conveyor"] : false;
                        var downBack = IsOnFloor(this.circleBackWheel.transform.position, wheelSize, Vector2.down);
                        var downFront = IsOnFloor(this.circleFrontWheel.transform.position, wheelSize, Vector2.down);
                        if (
                            this.contactBack.HasValue == true ||
                            this.contactFront.HasValue == true ||
                            IsOnFloor(midPos, wheelSize, downVector) ||
                            downBack ||
                            downFront
                        )
                        {
                            var modifiedSpeed = this.speed;

                            // TODO: tie conveyor belts to input
                            if (this.game.input.holding == true)
                            {
                                if (conveyorBack == true)
                                    modifiedSpeed += ConveyorSpeedMod(this.contactBack.Value, this.speed * 0.35f);
                                if (conveyorFront == true)
                                    modifiedSpeed += ConveyorSpeedMod(this.contactFront.Value, this.speed * 0.35f);
                            }

                            // apply speed
                            this.body.AddForce(this.moveVector * modifiedSpeed, ForceMode2D.Force);
                            // add resistance on dirt floor
                            if (this.dirtFront == true)
                            {
                                this.body.AddForce(-DirtDamping * this.body.velocity);
                            }

                            if (this.dirtBack == true)
                            {
                                this.body.AddForce(-DirtDamping * this.body.velocity);
                            }

                            // add conveyor belt push
                            if (this.game.input.holding == false)
                            {
                                if (conveyorFront == true)
                                    ConveyorPush(this.contactFront.Value);
                                if (conveyorBack == true)
                                    ConveyorPush(this.contactBack.Value);
                            }

                            if (conveyorFront || conveyorBack)
                            {
                                if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]))
                                {
                                    Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"], startFrom: 0, setTo: 1f, duration: 1));
                                }
                            }
                            else if(!conveyorFront && !conveyorBack)
                            {
                                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                            }

                            // stick to surface
                            if (
                                (this.contactBack.HasValue == true || this.contactFront.HasValue == true) &&
                                (this.speed > 0 || conveyorBack == true || conveyorFront == true)
                            )
                            {
                                this.body.AddForce(this.downVector * 10, ForceMode2D.Force);
                                this.body.gravityScale = this.defaultGravity * Mathf.Abs(Direction());
                            }
                            else
                            {
                                this.body.gravityScale = this.defaultGravity;
                                // with one wheel on the floor and the car dead vertical we get stuck, so...
                                if(downBack != downFront && this.speed > 0)
                                    this.body.AddTorque(10f * (this.flipped ? 1 : -1));

                            }

                            if(this.flippedDriver != this.flipped) this.flippedDriver = this.flipped;
                        }
                        else
                        {
                            this.body.gravityScale = this.defaultGravity;
                            if
                            (
                                IsOnFloor(this.circleBackWheel.transform.position, wheelSize, -this.downVector) == false &&
                                IsOnFloor(this.circleFrontWheel.transform.position, wheelSize, -this.downVector) == false
                            )
                                this.airCount++;
                        }

                        if(this.state == State.Drive)
                        {
                            // check for pickups
                            this.game.pickupMap.CircleCollidePickup(this.circleBackWheel.transform.position, this.circleBackWheel.radius);
                            this.game.pickupMap.CircleCollidePickup(this.circleFrontWheel.transform.position, this.circleFrontWheel.radius);

                            // check for finish area collision
                            if (this.game.finishLookup.ContainsKey(this.tilePos))
                            {
                                SetState(State.Finish);
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_finish"], Minigame.NoRandomPitch);
                                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_engine_accelerate_loop"]);
                                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_engine_idle_loop"]);
                            }
                        }

                        break;
                }

            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                // chassis wobble when landing
                if(this.airCount > 2)
                {
                    var twang = Vector3.ClampMagnitude(this.transform.localRotation * collision.relativeVelocity * 0.01f, 0.25f);
                    springChassis.pos += (Vector2)twang;
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_floor_bump"]);
                    Audio.instance.PlayRandomSfx( null, null, 
                        game.assets.audioClipLookup["sfx_car_suspension_1"], 
                        game.assets.audioClipLookup["sfx_car_suspension_2"], 
                        game.assets.audioClipLookup["sfx_car_suspension_3"]
                    );
                }
                // impact dust (very sketchy - acceleration dust covers up most of the false positives)
                if (collision.relativeVelocity.sqrMagnitude > 256f)
                {
                    foreach(var c in collision.contacts)
                    {
                        Minigame.SideNormals(c.normal, out Vector3 left, out Vector3 right);
                        for(int i = 0; i < 8; i++)
                            AddDust((Vector3)c.point + left * 0.3f + right * UnityEngine.Random.Range(0, 0.6f), UnityEngine.Random.Range(2, 4));
                    }
                }
                this.airCount = 0;
            }

            public float ConveyorSpeedMod(RaycastHit2D raycastHit2D, float value)
            {
                var dir = raycastHit2D.collider.name.Contains("left") ? -1 : 1;
                var surfaceVector = -Vector2.Perpendicular(raycastHit2D.normal);
                return Vector2.Dot(surfaceVector * dir, this.moveVector) * value;
            }

            // push a Rigidbody2D up to a minimum speed and no more
            public void ConveyorPush(RaycastHit2D raycastHit2D)
            {
                var vel = this.body.velocity;
                var len = vel.magnitude;
                var dir = raycastHit2D.collider.name.Contains("left") ? -1 : 1;
                var surfaceVector = -Vector2.Perpendicular(raycastHit2D.normal);
                if(len < ConveyorSpeed)
                {
                    body.velocity += surfaceVector * dir * (ConveyorSpeed - len);
                    //Debug.Log($"push {dir * (ConveyorSpeed - len)}");
                }
                else if(dir * vel.x < 0)
                {
                    body.velocity += surfaceVector * dir * ConveyorSpeed * 0.1f;
                    //Debug.Log($"resist {dir * ConveyorSpeed}");
                }
            }

            public void Boost()
            {
                this.body.AddForce(BoostForce * Normalized(), ForceMode2D.Force);
                this.boostCount = BoostDelay;
                this.speed = MaxSpeed * 1.5f;
            }

            private void UpdateGfx()
            {
                this.velocityAvg = 0;
                var velMagnitude = this.body.velocity.magnitude;
                if(RacingGame.ZoomScaleMin != RacingGame.ZoomScaleMax)
                {
                    for(int i = 0; i < velocities.Length - 1; i++)
                    {
                        velocities[i] = velocities[i + 1];
                        this.velocityAvg += velocities[i];
                    }
                    velocities[velocities.Length - 1] = velMagnitude;
                    this.velocityAvg += velMagnitude;
                    this.velocityAvg /= velocities.Length;
                }
                // spin wheels
                if(this.contactBack.HasValue == true)
                    SpinWheelFloor(this.transformBackWheel);
                else
                    SpinWheelAir(this.transformBackWheel);
                if(this.contactFront.HasValue == true)
                    SpinWheelFloor(this.transformFrontWheel);
                else
                    SpinWheelAir(this.transformFrontWheel);
                // wobble chassis
                springChassis.Update();
                transformChassis.localPosition = Vector2.up * springChassis.pos;
                // flip driver animation
                var flipScale = this.transformFlipHolder.localScale;
                var flipTarget = this.flippedDriver == true ? -1f : 1f;
                if(flipScale.y != flipTarget)
                {
                    flipScale.y = Mathf.MoveTowards(flipScale.y, flipTarget, 0.2f);
                    this.transformFlipHolder.localScale = flipScale;
                }
                // wobble flag
                if(Minigame.fixedDeltaTimeElapsed > 0.2f)// wait for car to touch floor first
                {
                    this.springFlag.pos.x -= velMagnitude * 2;
                    this.springFlag.Update();
                    this.transformFlagPole.localRotation = Quaternion.Euler(0, 0, this.springFlag.pos.x + this.rotationFlagDefault);
                }
            }

            private void SpinWheelFloor(Transform transformWheel)
            {
                // (wheel position - contact position).normalized = surface normal
                // surface normal rotated 90° = surface tangent
                // Vector2.Dot(surface tangent, wheel velocity) = wheel speed along surface
                var wheelSpeed = Vector2.Dot(-Vector2.Perpendicular(this.downVector), this.body.velocity);
                var rot = Quaternion.Euler(0, 0, WheelFloorRotationConst * wheelSpeed);
                transformWheel.rotation *= rot;
            }

            private void SpinWheelAir(Transform transformWheel)
            {
                var rot = Quaternion.Euler(0, 0, this.speedWheel);
                transformWheel.rotation *= rot;
            }

            private void AddDust(Vector2 p, int frameOffset = 0)
            {
                p += (Vector2)UnityEngine.Random.insideUnitSphere * 0.1f;
                var particle = this.game.ParticleCreateAndPlayOnce("GLOBAL:dust", p);
                particle.animated.frameNumber += frameOffset;
            }

            public void AddMudDebris(Vector2 pos, int total)
            {
                var n = Normalized();
                var v = (-n + (this.flipped ? new Vector2(n.y, -n.x) : new Vector2(-n.y, n.x))) * 5f - this.body.velocity * 0.2f;
                for(int i = 0; i < total; i++)
                {
                    var p = pos -n * 0.25f + n * UnityEngine.Random.value * 0.5f;
                    var d = this.game.AddDebris("mud", p, (Minigame.SpeedDebrisMin + v) * 0.01f, (Minigame.SpeedDebrisMax + v) * 0.02f);
                    d.ScaleOut(AnimationCurve.Linear(0, 1f, 1f, 0f));
                    d.spin = 0f;
                    d.acceleration *= 0.25f;
                }
            }


            // Car direction as a normalized float
            public float Direction()
            {
                return (this.circleFrontWheel.transform.position.x - this.circleBackWheel.transform.position.x) / this.length;
            }

            public Vector2 Normalized()
            {
                return (this.circleFrontWheel.transform.position - this.circleBackWheel.transform.position).normalized;
            }

            protected bool IsOnFloor(Vector2 pos, float dist, Vector2 down, ref RaycastHit2D? contact)
            {
                Debug.DrawLine(pos, pos + down * dist, Color.blue);
                var result = Physics2D.Raycast(pos, down, dist, this.game.objectLayers.tileLayerMask);
                if(result.collider != null)
                    contact = result;
                else
                    contact = null;
                return result.collider != null;
            }

            protected bool IsOnFloor(Vector2 pos, float dist, Vector2 down)
            {
                Debug.DrawLine(pos, pos + down * dist, Color.red);
                var result = Physics2D.Raycast(pos, down, dist, this.game.objectLayers.tileLayerMask);
                return result.collider != null;
            }

            public void EventPause()
            {
                this.body.simulated = false;
            }

            public void EventUnpause()
            {
                this.body.simulated = true;
            }
            public void ResetCar()
            {
                this.body.simulated = true;
                transform.localPosition = this.start;
                transform.localRotation = Quaternion.identity;
                SetState(State.Drive);
                this.hasStopped = true;
            }

            public void Death()
            {
                SetState(State.Dead);
                this.game.camOffset = this.game.DrivingCamOffset(false);
                this.game.camCtrl.PauseAndPanTo(this.start + this.game.DrivingCamOffset(false), RacingGame.BeforePanDelay, RacingGame.PanDelay, 0.5f, RestartLevel);
            }

            void RestartLevel()
            {
                this.game.Restart(true);
            }

            // Used for chassis wobble when landing
            // https://processing.org/examples/spring.html
            public class Spring2D
            {
                // Spring simulation variables
                public Vector2 pos;           // Position
                public Vector2 vel;           // Velocity
                // Spring simulation constants
                public float damping = 0.92f; // Damping
                public float Mass = 0.8f;     // Mass
                public float K = 0.2f;        // Spring constant
                public Vector2 rest;          // Rest position

                private const float Tolerance = 0.001f;

                public Spring2D(Vector2 pos)
                {
                    this.pos = this.rest = pos;
                    this.vel = Vector2.zero;
                }
                public void Update()
                {
                    var force = new Vector2(-K * (this.pos.x - this.rest.x), -K * (this.pos.y - this.rest.y));
                    var acc = new Vector2(force.x / Mass, force.y / Mass);
                    this.vel = new Vector2(damping * (this.vel.x + acc.x), damping * (this.vel.y + acc.y));
                    this.pos += this.vel;
                    if(Mathf.Abs(this.vel.x) < Tolerance)
                        this.vel.x = 0.0f;
                    if(Mathf.Abs(this.vel.y) < Tolerance)
                        this.vel.y = 0.0f;
                }
            }
        }

    }
}
