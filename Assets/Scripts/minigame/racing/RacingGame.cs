using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace minigame
{
    namespace racing
    {
        public partial class RacingGame : Minigame
        {
            [Header("Racing Game")]
            public TMP_Text textCountdown;
            public Image imageCountdownBack;
            public Image imageCountdownSpin;
            public Image imageCountdownSwitch;
            public RectTransform transformCountdownScaler;
            public Transform transformBackground;
            public Transform transformTitleHolder;
            public BackgroundTV backgroundTV;
            public GameObject gameObjectStartCount;
            public Image[] imageStartLights;
            public SpriteRenderer backgroundWallSpriteRenderer;
            public SpriteRenderer[] backgroundStretchSpriteRenderers;
            public SpriteRenderer[] backgroundCrowdSpriteRenderers;
            public SpriteRenderer[] backgroundHillSpriteRenderers;
            [NonSerialized] public PlayerCar playerCar;
            [NonSerialized] public Vector3 camOffset;
            [NonSerialized] public float zoomScale;
            public float countdown;
            public float startCount;
            public Dictionary<Vector2Int, int> finishLookup = new Dictionary<Vector2Int, int>();

            private float countdownStart;
            private float drivingCamOffset = 0.35f;
            private Vector3[] backgroundCrowdPos;
            
            private const float CamOffsetInterpolation = 0.1f;
            public const float PanDelay = 0.02f;// multplied by distance
            public const float BeforePanDelay = 1f;
            
            public const float StartDelay = 2f;
            //public const float StartDelay = 0f;// debug

            public const float BackgroundCrowdWaveWavelength = 1f;
            public const float BackgroundCrowdWaveMagnitude = 0.25f;
            public const float ZoomScaleMaxPortrait = 1.5f;
            public const float ZoomScaleMaxLandscape = 1.0f;
            public const float ZoomScaleMin = 1.0f;
            public const float ZoomSpeedMax = 30f;
            public const float ZoomStep = 0.01f;
            public const float BackgroundWallSpeed = 0.2f;
            public const float HeightChimneyLevel = 53f;// iPhoneX * ZoomScaleMaxPortrait is 52ish tiles tall
            public const float BackgroundWidthDefault = 149.3143f;
            public const float TitleY = 36.62599f;
            public const float UIPadding = 4f;

            public static float BackgroundScale;
            public static float ZoomScaleMax = 1.5f;

            private int currentStartCount = 2;
            private float countdownLastFrame;

            public static Dictionary<int, string> StartLightIntToString = new Dictionary<int, string>
            {
                [0] = "green light "
                , [1] = "amber light "
                , [2] = "red light "
            };

            void Start()
            {
                ScoreRise.Layer = this.objectLayers.fxSortingLayer;
                ScoreRise.Spacing = -0.05f * Scale;
                ScoreRise.RiseDist = 2f * Scale;
                ScoreRise.RiseDelay = 1f * Scale;
                Init();// Minigame setup
                Minigame.RetrySubtitle = "out of time!";

                if(Minigame.AspectRatio < 1f)
                    ZoomScaleMax = ZoomScaleMaxPortrait;
                else
                {
                    var s = this.transformCountdownScaler.sizeDelta;
                    s.y += 30;
                    this.transformCountdownScaler.sizeDelta = s;
                    this.transformCountdownScaler.anchoredPosition += Vector2.down * 15;
                    ZoomScaleMax = ZoomScaleMaxLandscape;
                }
                // avoid camera popping when moving to centered view
                camCtrl.clampTolerance = Vector2.zero;

                // setup defaults for background
                backgroundCrowdPos = new Vector3[backgroundCrowdSpriteRenderers.Length];
                for(int i = 0; i < backgroundCrowdPos.Length; i++)
                    backgroundCrowdPos[i] = backgroundCrowdSpriteRenderers[i].transform.localPosition;
                // scale background to fit camera height
                var backgroundChild = this.transformBackground.GetChild(0);
                var backgroundHeight = Mathf.Abs(backgroundChild.transform.localPosition.y) * 2;
                BackgroundScale = (this.camCtrl.rectDefault.height * ZoomScaleMax) / backgroundHeight;
                this.transformBackground.transform.localScale = new Vector3(BackgroundScale, BackgroundScale, 1f);
                // make sure hills are wide enough to parallax
                for(int i = 0; i < this.backgroundHillSpriteRenderers.Length; i++)
                {
                    var sr = this.backgroundHillSpriteRenderers[i];
                    var spriteWidth = (sr.sprite.rect.width / sr.sprite.pixelsPerUnit);
                    sr.size = new Vector2((Mathf.Ceil((this.camCtrl.rectDefault.width * ZoomScaleMax) / (spriteWidth * BackgroundScale)) + 1) * spriteWidth, sr.size.y);
                }
                ObjectLayers.SetSortingLayer(this.backgroundTV.gameObject, this.objectLayers.backgroundSortingLayer);


                Restart(true);


                this.minigameInputTypePrompt.FirstShow();

#if UNITY_EDITOR
                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.playerObj.transform.position.x, this.playerObj.transform.position.y + 4f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);
                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, () =>
                {
                    var backgroundScale = (this.camCtrl.rectDefault.height * ZoomScaleMaxPortrait) / backgroundHeight;
                    this.transformBackground.transform.localScale = new Vector3(backgroundScale, backgroundScale, 1f);
                    UpdateBackground();
                });
                this.transformBackground.transform.localScale = new Vector3(BackgroundScale, BackgroundScale, 1f);
                UpdateBackground();
#endif
            }
            void OnApplicationQuit()
            {
                Save();
            }

            public override void Save()
            {
                base.Save();
            }

            public override bool IsFailed()
            {
                return this.countdown <= 0;
            }
            public override bool IsComplete()
            {
                return this.playerCar.state == PlayerCar.State.Finish;
            }

            public override void Restart(bool hard)
            {
                base.Restart(hard);
                this.spriteRendererPool.sortingOrderCount = 20;// drop sprites in front of background

                this.finishLookup.Clear();
                this.camCtrl.clampAction = this.camCtrl.Clamp;
                this.fileLevel = GetLevelFile(difficulty, level);
                CreateLevel(this.fileLevel);
                this.startCount = Minigame.testingLevel == null ? StartDelay : 0;
                this.gameObjectStartCount.gameObject.SetActive(this.startCount > 0);
                UpdateCountdownUI();
                this.input.holding = false;
                this.camOffset = DrivingCamOffset(false);
                this.zoomScale = ZoomScaleMin;
                this.camCtrl.SetZoom(ZoomScaleMin);

                this.currentStartCount = 2;
                Audio.instance.PlaySfxLoop(this.assets.audioClipLookup["GLOBAL:sfx_amb_crowd"]);
                StartCoroutine(Audio.instance.ChangeSfxVolume(this.assets.audioClipLookup["GLOBAL:sfx_amb_crowd"], setTo: 0.5f));
            }

            public override void ExitGame()
            {
                base.ExitGame();
            }

            void Update()
            {
                this.input.Advance();

                if(Minigame.paused == false)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
#endif

                this.countdownLastFrame = this.countdown;
                this.minigameInputTypePrompt.ShowIfIdle(12f);
            }

            void FixedUpdate()
            {

                if(this.camCtrl.lookAtTween == null)
                {
                    switch(this.playerCar.state)
                    {

                        case PlayerCar.State.Finish:
                            // look just above the car, so the continue cost doesn't cover it
                            this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.playerObj.transform.position + Vector3.up * 1f, 0.1f), Vector3.zero);
                            this.zoomScale = Mathf.MoveTowards(this.zoomScale, ZoomScaleMin, ZoomStep);
                            this.camCtrl.SetZoom(this.zoomScale);
                            UpdateBackground();
                            // skip normal camera
                            return;

                        case PlayerCar.State.TimeOut:
                            // look just above the car, so the continue cost doesn't cover it
                            this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.playerObj.transform.position + Vector3.up * 3f, 0.1f), Vector3.zero);
                            this.zoomScale = Mathf.MoveTowards(this.zoomScale, ZoomScaleMin, ZoomStep);
                            this.camCtrl.SetZoom(this.zoomScale);
                            UpdateBackground();
                            // skip normal camera
                            return;
                        case PlayerCar.State.Dead:
                            this.camOffset = Vector3.Lerp(this.camOffset, Vector3.zero, CamOffsetInterpolation);
                            break;
                        default:
                            // advance start count and break before game logic
                            AdvanceStartCount();
                            if(this.startCount > 0) break;

                            // advance countdown
                            if(this.playerCar.state == PlayerCar.State.Drive)
                            {
                                countdown -= Time.fixedDeltaTime;
                                if(countdown <= 0)
                                {
                                    this.playerCar.SetState(PlayerCar.State.TimeOut);
                                    // remove vertical clamp on camera
                                    this.camCtrl.clampAction = this.camCtrl.ClampX;
                                    this.FailLevel();
                                }
                            }


                            var target = DrivingCamOffset(true);
                            var dist = (target - this.camOffset).magnitude;
                            if(dist > 1f)
                                this.camOffset = Vector3.MoveTowards(this.camOffset, target, 0.1f);
                            else
                                this.camOffset = Vector3.Lerp(this.camOffset, target, CamOffsetInterpolation);
                            break;
                    }
                    UpdateMotors();

                    this.camCtrl.AdvanceShakes(Time.fixedDeltaTime);
                    this.camCtrl.SetPosition(this.playerObj.transform.position, this.camOffset * this.zoomScale);
                }
                else
                {
                    this.camCtrl.UpdateLookAt(Time.fixedDeltaTime);
                }
                UpdateZoom();
                UpdateBackground();
                UpdateCountdownUI();
                Minigame.fixedDeltaTimeElapsed += Time.fixedDeltaTime;
            }

            public Vector3 DrivingCamOffset(bool carDirection)
            {
                return Vector3.right * (this.drivingCamOffset * this.camCtrl.rectDefault.width) * (carDirection ? this.playerCar.Direction() : 1f);
            }

            public void UpdateZoom()
            {
                var speedNormal = Mathf.Min(this.playerCar.velocityAvg, ZoomSpeedMax) / ZoomSpeedMax;
                this.zoomScale = Mathf.MoveTowards(this.zoomScale, ZoomScaleMin + speedNormal * (ZoomScaleMax - ZoomScaleMin), ZoomStep);
                if(this.startCount > 0)
                    this.zoomScale = ZoomScaleMin;
                this.camCtrl.SetZoom(this.zoomScale);
            }

            public void UpdateBackground()
            {

                // holder for background - centralise
                var camPos = this.cam.transform.position;
                camPos.z = 0;

                this.transformBackground.position = camPos + Vector3.left * (Minigame.ScreenWidth * this.zoomScale) * 0.5f;
                var focusPoint = this.cam.transform.localPosition + Vector3.left * (Minigame.ScreenWidth * this.zoomScale) * 0.5f;
                focusPoint.y = focusPoint.z = 0;
                var camPosHoriz = new Vector3(focusPoint.x, 0);

                var hillStep = 0.4f / this.backgroundHillSpriteRenderers.Length;
                Vector3 p;
                float spriteWidth;
                for(int i = 0; i < this.backgroundHillSpriteRenderers.Length; i++)
                {
                    p = camPosHoriz;
                    float offset = i * hillStep + 0.4f;
                    offset *= 0.9f;
                    p *= -offset;
                    var sr = this.backgroundHillSpriteRenderers[i];
                    spriteWidth = (sr.sprite.rect.width / sr.sprite.pixelsPerUnit);
                    while(p.x < -spriteWidth)
                        p.x += spriteWidth;
                    p.y = sr.transform.localPosition.y;
                    sr.transform.localPosition = p;

                }
                p = -camPosHoriz * BackgroundWallSpeed;// 0.2f
                spriteWidth = (this.backgroundWallSpriteRenderer.sprite.rect.width / this.backgroundWallSpriteRenderer.sprite.pixelsPerUnit);
                while(p.x < -spriteWidth)
                    p.x += spriteWidth;
                p.y = this.backgroundWallSpriteRenderer.transform.localPosition.y;
                this.backgroundWallSpriteRenderer.transform.localPosition = p;

                // update crowd wave
                for(int i = 0; i < this.backgroundCrowdSpriteRenderers.Length; i++)
                {
                    var t = Minigame.fixedDeltaTimeElapsed + (i % 4) * (BackgroundCrowdWaveWavelength * 0.125f);
                    p = (Vector3)Shake.WavePosition(Vector2.up, t, BackgroundCrowdWaveWavelength, BackgroundCrowdWaveMagnitude);
                    this.backgroundCrowdSpriteRenderers[i].transform.localPosition = this.backgroundCrowdPos[i] + p;
                }
            }

            public void AddScoreRise(Vector2 pos, int value, string prefixSprite = "")
            {
                var obj = Instantiate(this.assets.prefabLookup["score_rise"], pos, Quaternion.identity, transform);
                var scoreRise = obj.GetComponent<ScoreRise>();
                scoreRise.Init(value, this.spriteRendererPool, this.assets, prefixSprite, "GLOBAL:num_");
                if(value < 0)
                    scoreRise.SetColor(Color.red);
            }

            public static Color[] ColorTimers = new Color[]
            {
                Color.white
                ,new Color32(255, 180, 89, 255)
                ,new Color32(255, 59, 44, 255)
                ,new Color32(85, 85, 85, 255)
            };

            public void UpdateCountdownUI()
            {

                this.textCountdown.text = Mathf.CeilToInt(Mathf.Max(this.countdown, 0)).ToString("D2");

                // countdown is created by spinning a bottom semicircle,
                // whilst on top we flip a top semicircle to either side

                Color colorCurrent, colorNext;

                if(this.countdown <= 10)
                {
                    if(Mathf.Floor(this.countdown) % 2 == 0)
                    {
                        colorCurrent = ColorTimers[2];
                        colorNext = ColorTimers[3];
                    }
                    else
                    {
                        colorCurrent = ColorTimers[3];
                        colorNext = ColorTimers[2];
                    }

                    if (Mathf.Floor(this.countdownLastFrame) != Mathf.Floor(this.countdown))
                    {
                        Audio.instance.PlaySfx(assets.audioClipLookup["sfx_time_10_secs"], options: new Audio.Options(1f, false, 0));
                    }
                }
                else
                {
                    if(Mathf.Floor(this.countdownStart - this.countdown) % 2 == 0)
                    {
                        colorCurrent = ColorTimers[0];
                        colorNext = ColorTimers[1];
                    }
                    else
                    {
                        colorCurrent = ColorTimers[1];
                        colorNext = ColorTimers[0];
                    }
                }
                imageCountdownBack.color = colorCurrent;
                textCountdown.color = colorCurrent;
                var second = Mathf.Abs(this.countdown) % 1.0f;
                if(second > 0.5f || second == 0f)
                {
                    imageCountdownSwitch.transform.localRotation = Quaternion.identity;
                    imageCountdownSwitch.color = colorCurrent;
                }
                else
                {
                    imageCountdownSwitch.transform.localRotation = Quaternion.Euler(0, 0, 180);
                    imageCountdownSwitch.color = colorNext;
                }
                imageCountdownSpin.color = colorNext;
                imageCountdownSpin.transform.localRotation = Quaternion.Euler(0, 0, 360 * second);

            }

            public void AdvanceStartCount()
            {
                if(this.startCount > -1)
                {
                    this.startCount -= Time.fixedDeltaTime;
                    if(this.startCount <= -1)
                    {
                        this.gameObjectStartCount.SetActive(false);
                        return;
                    }
                    for(int i = 0; i < imageStartLights.Length; i++)
                    {
                        imageStartLights[i].sprite = this.assets.spriteLookup[StartLightIntToString[i] + (i == Mathf.CeilToInt(this.startCount) ? "on" : "off")];
                        if (Mathf.CeilToInt(this.startCount) == this.currentStartCount)
                        {
                            if(this.currentStartCount >= 1)
                            {
                                Audio.instance.PlaySfx(this.assets.audioClipLookup["sfx_ready_set_go_1"], options: new Audio.Options(1f, false, 0f));
                            }
                            else
                            {
                                Audio.instance.PlaySfx(this.assets.audioClipLookup["sfx_ready_set_go_2"], options: new Audio.Options(1f, false, 0f));
                                StartCoroutine(Audio.instance.ChangeSfxVolume(this.assets.audioClipLookup["GLOBAL:sfx_amb_crowd"], startFrom: 0.5f, setTo: 0f, duration: 1.5f));

                            }
                            this.currentStartCount--;
                        }
                    }
                    
                }
            }

        }
    }
}
