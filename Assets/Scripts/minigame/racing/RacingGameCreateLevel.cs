using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace racing
    {
        public partial class RacingGame : Minigame
        {

            public void CreateLevel(TextAsset file)
            {

                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                );
                this.map.AddNoAutoTiles(""
                    , "poly22br"
                    , "poly67br"
                    , "crumble_block"
                );
                this.map.AddDontRotateTiles("");
                this.map.AddFilledTiles(
                    "polysquare"
                    , "poly45br"
                );
                this.map.AddFilledRectInts(
                    ("poly22br", new RectInt(0, 0, 2, 1))
                    , ("half_pipe_small", new RectInt(0, 0, 3, 6))
                    , ("half_pipe_smaller", new RectInt(0, 0, 2, 5))
                    , ("poly67br", new RectInt(0, 0, 1, 2))
                );


                // generate all objects
                this.map.Create(file);

                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;

                var uiPadding = RacingGame.UIPadding * RacingGame.ZoomScaleMax;

                this.countdown = this.countdownStart = map.levelOriginal.properties["countdown"].i;


                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        this.spikesLookup[record.p] = 1;
                    }

                    if(record.isPrefab == true)
                    {

                        switch(record.name)
                        {
                            case "player":
                                {
                                    record.gameObject.transform.localPosition -= new Vector3(0, Scale * 0.5f);
                                    this.playerObj = record.gameObject;
                                    this.playerCar = record.GetComponent<PlayerCar>();
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.playerSortingLayer);
                                }
                                break;
                            case "boost":
                                {
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    ObjectLayers.SetSortingLayer(record.gameObject.transform.Find("back").gameObject, this.objectLayers.backgroundSortingLayer);
                                }
                                break;
                            case "spring":
                                record.GetComponent<Spring>().Init(this, record.tile.properties != null ? record.tile.properties["flipCar"].b : false);
                                break;
                            case "time_penalty":
                            case "time_pickup":
                                {
                                    var component = record.GetComponent<TimePickup>();
                                    component.game = this;
                                    component.value = record.tile.properties[record.tile.tile].i * (record.name == "time_pickup" ? 1 : -1);
                                }
                                break;
                            case "conveyor_left":
                            case "conveyor_right":
                                {
                                    var a = record.GetComponent<Animated>();
                                    MinigameMap.AddAutotiling(a, record.tile, record.x, record.y, this.assets.autotileArrayLookup[Minigame.GlobalPrefix + record.name], this.map.levelOriginal);
                                    a.playbackSpeed = 3f;
                                }
                                break;
                            case "crumble_block":
                                {
                                    var crumbleBlock = record.GetComponent<CrumbleBlock>();
                                    var sr = record.GetComponent<SpriteRenderer>();
                                    sr.sprite = this.assets.spriteLookup[record.nameTile];
                                    var s = crumbleBlock.boxCollider2D.size;
                                    switch(record.nameTile)
                                    {
                                        case "1x3 crumble 1":
                                            s.y += 2;
                                            crumbleBlock.spriteSolid = this.assets.spriteLookup[record.nameTile];
                                            crumbleBlock.spriteCrumble = this.assets.spriteLookup["1x3 crumble 2"];
                                            break;
                                        case "3x1 crumble 1":
                                            s.x += 2;
                                            crumbleBlock.spriteSolid = this.assets.spriteLookup[record.nameTile];
                                            crumbleBlock.spriteCrumble = this.assets.spriteLookup["3x1 crumble 2"];
                                            break;
                                    }
                                    crumbleBlock.Init(this, record.pos + record.offset, s);
                                }
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        switch(record.tile.tile)
                        {
                            case "finish":
                                ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.backgroundSortingLayer);
                                this.finishLookup[record.p] = 1;
                                break;
                        }

                        if(record.nameTile.Contains("sign_") == true)
                            record.GetComponent<SpriteRenderer>().sortingOrder += this.map.width * this.map.height;
                    }
                }

                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "button":
                            var tb = conn.objB.GetComponent<TriggerButton>();
                            tb.Init(this);
                            tb.targets.Add(conn.a);
                            triggerButtons.Add(tb);
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                    }
                }
                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);


                //GetAutoTiles
                //(
                //    level,
                //    this.assets.tileThemeLookup["TileThemeRacing"],
                //    this.assets.physicsMaterial2DLookup["racing_surface"],
                //    null,
                //    this.assets.physicsMaterial2DLookup["racing_dirt"]
                //);


                // FLOOR FILL
                // for open top levels and padding when in portrait
                //===========================================================================================
                // chimney detection
                var chimneys = new List<(int left, int right)>();
                {
                    var chimneyLookup = new Dictionary<Vector2Int, bool>();
                    var tileLayer = this.map.levelOriginal.TileLayerByName("Tiles");
                    // standard curve tiles leave gaps in the level data
                    // so we create a lookup table instead of reading tileLayer
                    void FillLookup(RectInt rect)
                    {
                        for(int y = rect.y; y < rect.y + rect.height; y++)
                            for(int x = rect.x; x < rect.x + rect.width; x++)
                                chimneyLookup[new Vector2Int(x, y)] = true;
                    }
                    for(int i = 0; i < tileLayer.tiles.Length; i++)
                    {
                        var t = tileLayer.tiles[i];
                        if(t.tile != null)
                        {

                            var x = i % this.map.width;
                            var y = i / this.map.width;
                            var p = new Vector2Int(x, y);
                            switch(t.tile)
                            {
                                case Minigame.GlobalPrefix + "22br":
                                    FillLookup(MinigameMap.RectIntFromTile(new RectInt(x, y, 2, 1), p, t));
                                    break;
                                case Minigame.GlobalPrefix + "curvebr3plus1":
                                    FillLookup(MinigameMap.RectIntFromTile(new RectInt(x, y, 4, 4), p, t));
                                    break;
                                case Minigame.GlobalPrefix + "curveoutbr3":
                                    FillLookup(MinigameMap.RectIntFromTile(new RectInt(x, y, 3, 3), p, t));
                                    break;
                                default:
                                    chimneyLookup[p] = true;
                                    break;
                            }
                        }
                    }
                    (int left, int right)? chimney = null;
                    for(int x = 0; x < this.map.width; x++)
                    {
                        if(chimneyLookup.ContainsKey(new Vector2Int(x, this.map.height - 1)))
                        {
                            // close a chimney?
                            if(chimney != null)
                            {
                                chimney = (chimney.Value.left, x - 1);
                                chimneys.Add(chimney.Value);
                                chimney = null;
                            }
                        }
                        // open top detected
                        else if(chimney == null)
                            chimney = (x, x);
                    }
                }
                // create floor fill above and below level (need to account for Retry UI)
                Vector2 floorFillSize = new Vector2(this.map.width, uiPadding + (this.camCtrl.rectDefault.height * ZoomScaleMax) * 0.5f);
                // is it a chimney level?
                if(chimneys.Count > 0)
                {
                    // Needs a fixed minimum height to work on all devices
                    floorFillSize.y = Mathf.Max((HeightChimneyLevel - this.map.height) * 0.5f, floorFillSize.y);
                    // create ceiling box to prevent escape
                    var ceilingObj = new GameObject("ceiling");
                    var ceilingBox = ceilingObj.AddComponent<BoxCollider2D>();
                    ceilingBox.size = new Vector2(this.map.width, 1f);
                    ceilingBox.offset = ceilingBox.size * 0.5f;
                    ceilingObj.transform.SetParent(this.transform);
                    ceilingObj.transform.localPosition = Vector2.up * Mathf.Max(this.map.height, this.map.height + floorFillSize.y);
                }
                // calculate fills
                if(chimneys.Count > 0)
                {
                    var c = chimneys[0];
                    // top left corner
                    if(c.left > 1)
                        CreateFill(new Vector2(0, this.map.height), new Vector2(c.left - 1, floorFillSize.y), "floor_fill");
                    for(int i = 0; i < chimneys.Count; i++)
                    {
                        c = chimneys[i];
                        CreateFillBoxCollider(new Vector2(c.left - 1, this.map.height), new Vector2(1, floorFillSize.y), "side_fill_r");
                        if(i > 0 && c.left > chimneys[i - 1].right + 3)
                        {
                            var cPrev = chimneys[i - 1];
                            CreateFill(new Vector2(cPrev.right + 2, this.map.height), new Vector2((c.left - cPrev.right) - 3, floorFillSize.y), "floor_fill");
                        }
                        CreateFillBoxCollider(new Vector2(c.right + 1, this.map.height), new Vector2(1, floorFillSize.y), "side_fill_l");
                    }
                    // top right corner
                    if(c.right < this.map.width - 2)
                        CreateFill(new Vector2(c.right + 2, this.map.height), new Vector2((this.map.width - c.right) - 2, floorFillSize.y), "floor_fill");
                }
                // top fill - no chimneys
                else CreateFill(new Vector3(0, this.map.height), floorFillSize, "floor_fill");

                this.levelRect.height += uiPadding;

                // create thumbspace on portrait and ground on landscape
                if(Minigame.AspectRatio < 1f)
                {
                    // give 1/8th of the screen
                    var thumbSpace = this.camCtrl.rectDefault.height * RacingGame.ZoomScaleMax * 0.125f;
                    levelRect.y -= thumbSpace;
                    levelRect.height += thumbSpace;
                }
                else
                {
                    levelRect.y -= 1f;
                    levelRect.height += 1f;
                }
                floorFillSize.y = Mathf.Ceil(floorFillSize.y / Scale) * Scale;
                CreateFill(new Vector3(0, -floorFillSize.y), floorFillSize, "floor_fill_dirt");
                CreateFill(new Vector3(0, -1f), new Vector2(floorFillSize.x, 1f), "floor_fill_dirt_top");
                //===========================================================================================

                // expand backgroundWall and crowd to match level when parallaxed
                // (camera width) + (parallax offset when camera at farthest right)
                var camWidth = this.camCtrl.rectDefault.width * RacingGame.ZoomScaleMax;
                var backgroundWidth = (camWidth / RacingGame.BackgroundScale) + ((this.map.width - camWidth) * RacingGame.BackgroundWallSpeed);
                foreach(var sr in this.backgroundStretchSpriteRenderers)
                    sr.size = new Vector2(backgroundWidth, sr.size.y);

                // add titles to background wrap (after removing any titles that were there previously)
                foreach(Transform t in transformTitleHolder)
                    Destroy(t.gameObject);
                {
                    var p = new Vector2(0, TitleY);
                    while(p.x <= backgroundWidth)
                    {
                        var obj = Instantiate(this.assets.prefabLookup["racing_day_title"], this.backgroundStretchSpriteRenderers[0].transform);
                        obj.transform.localPosition = p;
                        p.x += BackgroundWidthDefault;
                    }
                }

                // make sure background is on background layer
                ObjectLayers.SetSortingLayer(this.transformBackground.gameObject, this.objectLayers.backgroundSortingLayer);


                this.playerObj.transform.localPosition += new Vector3(0, Scale);
                this.playerCar.Init(this);
            }

            public SpriteRenderer CreateFill(Vector2 position, Vector2 size, string spriteName)
            {
                var sr = this.spriteRendererPool.Get(position, this.assets.spriteLookup[spriteName], this.objectLayers.tilesSortingLayer, "fill_top", this.transform);
                sr.drawMode = SpriteDrawMode.Tiled;
                sr.size = size;
                return sr;
            }

            public void CreateFillBoxCollider(Vector2 position, Vector2 size, string spriteName)
            {
                var sr = CreateFill(position, size, spriteName);
                var b = sr.gameObject.AddComponent<BoxCollider2D>();
                b.size = size;
                b.offset = size * 0.5f;
                b.sharedMaterial = this.assets.physicsMaterial2DLookup["racing_surface"];
            }


        }

    }
}
