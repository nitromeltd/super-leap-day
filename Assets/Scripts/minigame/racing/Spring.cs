using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace racing
    {
        public class Spring : MonoBehaviour
        {

            public SpriteRenderer spriteRenderer;
            public Animated anim;
            public Animated.Animation animAnim;
            public Minigame game;
            public float power = 20f;
            public bool flipCar;// flip the car when triggered

            private Rigidbody2D bodyBuffer;
            public void Init(Minigame game, bool flipCar)
            {
                this.game = game;
                this.flipCar = flipCar;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                var body = collision.GetComponent<Rigidbody2D>();
                if(body != null && body.GetComponent<PlayerCar>() != null)
                {
                    if(this.anim.currentAnimation != null)
                    {
                        this.bodyBuffer = body;
                        return;
                    }
                    else
                        Launch(body);
                }
            }

            // wait for spring animation to finish
            private void OnTriggerStay2D(Collider2D collision)
            {
                if(this.bodyBuffer != null && this.anim.currentAnimation == null)
                {
                    Launch(this.bodyBuffer);
                    this.bodyBuffer = null;
                }
            }

            void Launch(Rigidbody2D body)
            {
                var dir = this.transform.localRotation * Vector2.up;
                this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.5f, Minigame.Scale);
                var v = body.velocity;
                // cancel velocity opposing the direction of the spring
                if(v.x * dir.x < 0) v.x = 0f;
                if(v.y * dir.y < 0) v.y = 0f;
                body.velocity = v;
                body.AddForce(dir * power, ForceMode2D.Impulse);
                this.anim.PlayOnce(animAnim);
                // flip car
                var car = body.GetComponent<PlayerCar>();
                if(car != null)
                {
                    if(this.flipCar == true)
                        car.flipped = !car.flipped;
                    Audio.instance.PlaySfx(car.game.assets.audioClipLookup["sfx_env_spring_tan"]);
                }
            }
        }
    }
}
