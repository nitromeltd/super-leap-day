﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace racing
    {
        public class TimePickup : MonoBehaviour
        {
            public RacingGame game;
            public float value;
            public Transform transformHand;
            public Transform transformBob;
            private float angleHand = 0f;
            private bool isCollected;
            public const float DegreesPerSecondHand = -180f;

            private void FixedUpdate()
            {
                if(this.transformHand != null)
                {
                    this.angleHand += DegreesPerSecondHand * (Time.fixedDeltaTime / Time.timeScale);
                    this.transformHand.localRotation = Quaternion.Euler(0, 0, this.angleHand);
                }
                this.transformBob.localPosition = Shake.WavePosition(Vector2.up, Minigame.fixedDeltaTimeElapsed, 1f, 0.25f);
            }

            private void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.isCollected == true) return;// race car is made of multiple colliders
                this.isCollected = true;
                this.game.countdown += this.value;
                var prefix = "";
                if(this.value < 0)
                {
                    this.game.ParticleCreateAndPlayOnce(Minigame.GlobalPrefix + "enemy_death_2", this.transform.position);
                    prefix = Minigame.GlobalPrefix + "minus";
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_time_penalty"]);
                }
                else
                {
                    this.game.SparkleFx(this.transform.position);
                    Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_time_bonus"]);
                }
                this.game.AddScoreRise(this.transform.position, (int)this.value, prefix);

                Destroy(this.gameObject);
            }

        }

    }

}
