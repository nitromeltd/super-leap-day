using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace racing
    {
        public class TriggerButton : MonoBehaviour
        {
            public RacingGame game;
            public List<Motor> motors = new List<Motor>();
            public BoxCollider2D trigger;
            public SpriteRenderer spriteRenderer;
            public List<Vector2Int> targets = new List<Vector2Int>();

            public void Init(RacingGame game)
            {
                this.game = game;
            }

            public void AddMotor(Motor m)
            {
                this.motors.Add(m);
                m.paused = true;
            }


            void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.game != null)
                {
                    this.spriteRenderer.sprite = this.game.assets.spriteLookup["button_down"];
                    foreach(var m in this.motors)
                        m.paused = false;
                    this.trigger.enabled = false;
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_env_button_engage"]);
                }
            }

        }
    }
}
