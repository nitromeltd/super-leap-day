using System;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {
        public class Ball : MonoBehaviour
        {

            [NonSerialized] public State state;
            public RollingGame game;
            public Rigidbody2D body;
            public CircleCollider2D circle;
            public SpriteRenderer spriteRenderer;
            public Launcher launcher;
            public bool spend;
            public DistanceJoint2D jointSwing;
            public Vector2 prev;
            public float gravityDefault;
            private bool inputDown;
            private float launchPower;
            private float launchDustCount;
            private float intensityTint;
            private SwingAnchor swingAnchor;
            private float timeDeadStop;

            public const float GravityShrinkStartY = -15f;
            public const float GravityShrinkSpan = 30f;
            public const float LaunchMin = 10f;
            public const float LaunchMax = 30f;
            public const float LaunchStep = 0.2f;
            private const float ConveyorSpeed = 6.375f;
            //private const float ForceSpinMax = 1500f;
            //private const float ForceSpinMin = 500f;
            private const float LengthSpinMin = 1f;
            private const float DelayDeadStop = 0.5f;
            public static Vector2 LaunchVector = Vector2.up + Vector2.right;

            private bool isflipperdown = true;


            public enum State
            {
                None, Launcher, Windup, Play, Lost, Finish
            }

            public void Init(RollingGame game)
            {
                this.game = game;
                this.inputDown = false;
                this.transform.position = this.launcher.transform.position;
                this.spriteRenderer.sprite = this.game.playerAssets.AssetsForCharacter(this.game.character).spriteBall;
                SetState(State.Launcher);
                this.game.minigameInputTypePrompt.FirstShow();
                this.gravityDefault = this.body.gravityScale;
            }

            public void Advance()
            {
                switch(this.state)
                {
                    case State.Launcher:
                        if(InputDownNew())
                        {
                            SetState(State.Windup);
                            Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_charge"]);
                        }
                        else
                        {
                            this.transform.localPosition = this.launcher.transform.localPosition;
                            if(this.game.balls.Count == 1)
                                this.game.minigameInputTypePrompt.ShowIfIdle(8f);
                        }
                        break;
                    case State.Windup:
                        this.transform.localPosition = this.launcher.transform.localPosition;
                        if(this.game.input.holding)
                        {
                            if(this.launchPower < LaunchMax)
                            {
                                this.launchPower += LaunchStep;
                                this.launcher.SetPower(this.launchPower, LaunchMin, LaunchMax);
                            }
                        }
                        else
                        {
                            SetState(State.Play);
                            this.launcher.Launch(this.launchPower, LaunchMin, LaunchMax);
                            this.launchDustCount = this.launcher.power * 0.5f + 0.25f;

                            if(this.launcher == this.game.launcher && this.spend == true && this.game.isRelaunch == false)
                            {
                                if(this.game.ballsLeft == 3 && this.game.character == Character.King)
                                    ShootKingHat();
                                this.game.SpendBall();
                            }
                            this.game.isRelaunch = false;

                            if (launchPower < 16.6)
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_weak"]);
                            }
                            else if(launchPower>= 16.6 && launchPower < 23.2)
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_med"]);
                            }
                            else
                            {
                                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_pinball_launch_strong"]);
                            }
                            Audio.instance.StopSfx(game.assets.audioClipLookup["sfx_pinball_launch_charge"]);
                        }
                        break;
                    case State.Play:
                        // check for spikes
                        var tilePos = Minigame.ToVector2Int(transform.localPosition);
                        if(this.game.spikesLookup.ContainsKey(tilePos))
                        {
                            Death();
                            break;
                        }
                        if(this.game.spikesOffsetLookup.TryGetValue(tilePos, out List<Rect> list) == true)
                        {
                            var dead = false;
                            foreach(var r in list)
                            {
                                //Minigame.DebugRect(r, Color.red);
                                if(r.Contains(transform.localPosition))
                                {
                                    dead = true;
                                    break;
                                }
                            }
                            if(dead) Death();
                            break;
                        }
                        // are we on the floor?
                        Vector2 p = transform.position;
                        float dist = this.circle.radius + 0.1f;
                        Debug.DrawLine(p, p + Vector2.down * dist, Color.red);
                        var result = Physics2D.Raycast(p, Vector2.down, dist, this.game.objectLayers.tileLayerMask);
                        if(result.collider != null)
                        {
                            // conveyor belts
                            if(result.collider.sharedMaterial == this.game.assets.physicsMaterial2DLookup["pinball_conveyor"])
                            {
                                ConveyorPush(result.collider);
                                if (!Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]))
                                {
                                    Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                                    StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"], startFrom: 0, setTo: 1f, duration: .1f));
                                }
                            }
                            else
                            {
                                Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                            }
                        }
                        else if(result.collider == null)
                        {
                            Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_env_conveyor_belt_neutral"]);
                        }
                        // check for portal walls (shove if sitting in one)
                        if(this.game.portalLookup.TryGetValue(new Vector2Int((int)p.x, (int)p.y), out Vector2 v) == true)
                        {
                            if(this.body.velocity.sqrMagnitude < 1f)
                                this.body.AddForce(v * 100f);
                        }
                        // check for coins
                        this.game.pickupMap.CircleCollidePickup(this.transform.position, this.circle.radius);
                        // check for checkpoints
                        foreach(var chk in this.game.launcherCheckpoints)
                        {
                            // pass?
                            var y = chk.transform.localPosition.y;
                            if(
                                chk != this.game.launcher &&
                                Mathf.Sign(chk.posBall.y - y) != Mathf.Sign(this.transform.localPosition.y - y) &&
                                (
                                    (chk.rank == this.game.launcher.rank && chk.distToFinish < this.game.launcher.distToFinish) ||
                                    (chk.rank > this.game.launcher.rank)
                                )
                            )
                                this.game.launcher = chk;
                            chk.posBall = this.transform.localPosition;
                        }
                        // check for win
                        if(this.game.finishLookup.ContainsKey(tilePos))
                        {
                            SetState(State.Finish);
                            Audio.instance.PlaySfx(this.game.assets.audioClipLookup["GLOBAL:sfx_pinball_goal"]);
                            break;
                        }
                        // launch fx
                        if(this.launchDustCount > 0f)
                        {
                            launchDustCount -= Time.fixedDeltaTime;
                            if(this.launchDustCount < 0.9f)
                                this.game.AddDustCloud(this.transform.localPosition, 0.5f, 3, this.game.objectLayers.playerSortingLayer, this.spriteRenderer.sortingOrder - 1);
                        }


                        // swing anchor
                        if(this.game.input.pressed)
                        {
                            var bestDist = float.MaxValue;
                            foreach(var swingAnchor in this.game.swingAnchors)
                            {
                                var len = (swingAnchor.transform.position - this.transform.position).magnitude;
                                if(len <= swingAnchor.radius + this.circle.radius)
                                {
                                    if(this.swingAnchor == null || len < bestDist)
                                    {
                                        this.swingAnchor = swingAnchor;
                                        bestDist = len;
                                    }
                                }
                            }
                            if(this.swingAnchor != null)
                            {
                                this.swingAnchor.SetGrab(this);
                                this.jointSwing.enabled = true;
                                this.jointSwing.connectedAnchor = this.swingAnchor.transform.position;
                                this.jointSwing.distance = this.swingAnchor.lengthJoint;

                                if(this.swingAnchor.motorised == true)
                                {
                                    var delta = swingAnchor.transform.position - this.transform.position;

                                    // going same way?
                                    var moved = (Vector2)this.transform.position - this.prev;
                                    var vector = Vector2.Perpendicular(delta.normalized);
                                    var dot = Vector2.Dot(vector, moved);
                                    this.swingAnchor.dir = Mathf.Sign(dot);
                                }
                            }
                        }
                        else if(this.game.input.holding)
                        {
                            if(this.swingAnchor != null)
                            {
                                // spin anchor
                                if(this.swingAnchor.motorised == true)
                                {
                                    var delta = this.swingAnchor.transform.position - this.transform.position;
                                    var vector = Vector2.Perpendicular(delta.normalized);
                                    this.body.velocity = vector * this.swingAnchor.dir * 17f;
                                }
                            }
                        }
                        else
                        {
                            DisableSwing();
                        }

                        // shrink gravity when travelling down fast
                        if(this.body.velocity.y < GravityShrinkStartY)
                        {
                            var y = Mathf.Abs(this.body.velocity.y - GravityShrinkStartY);
                            y = (GravityShrinkSpan - Mathf.Min(y, GravityShrinkSpan)) / GravityShrinkSpan;
                            this.body.gravityScale = this.gravityDefault * y;
                            //Debug.Log($"vel:{this.body.velocity} grav:{this.body.gravityScale} ratio:{y}");
                        }
                        else
                        {
                            this.body.gravityScale = this.gravityDefault;
                        }

                        // crush detection
                        var colliders = Physics2D.OverlapCircleAll(this.transform.position, this.circle.radius * 0.4f);
                        if(colliders.Length > 1)
                        {
                            var obs = new List<Collider2D>();
                            foreach(var c in colliders)
                                if(c.isTrigger == false && c != this.circle && c.usedByEffector == false)
                                    obs.Add(c);

                            if(obs.Count > 0)
                            {
                                //Debug.Log("OVERLAPS:");
                                //foreach(var c in obs)
                                //    Debug.Log(c.name);
                                //Debug.Break();
                                Death();
                                break;
                            }
                        }
                        // out-of-bounds
                        if(this.game.levelRect.Contains(this.transform.position) == false)
                        {
                            //Debug.Log("OUT OF BOUNDS");
                            //Debug.Break();
                            Death();
                            break;
                        }
                        // dead stop nudge
                        if(this.body.velocity.sqrMagnitude < 0.00001f)
                        {
                            this.timeDeadStop += Time.fixedDeltaTime;
                            if(this.timeDeadStop > DelayDeadStop)
                                this.body.AddForce((UnityEngine.Random.value > 0.5f ? Vector2.right : Vector2.left) * 10f);
                        }
                        else
                            this.timeDeadStop = 0f;

                        break;
                    case State.Finish:
                        break;
                }

                if (InputDownNew() && isflipperdown && game.winMenu.isOpen == false)
                {
                    Audio.instance.PlayRandomSfx(null, null, 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_1"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_3"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_up_3"]);
                    isflipperdown = false;
                }
                else if(!InputDownNew() && !isflipperdown && game.winMenu.isOpen == false)
                {
                    Audio.instance.PlayRandomSfx(null, null, 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_1"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_3"], 
                        game.assets.audioClipLookup["sfx_pinball_flipper_down_3"]
                    );
                    isflipperdown = true;
                }

                this.inputDown = this.game.input.holding;
                this.prev = this.transform.position;

                if(this.intensityTint > 0f)
                    SetTint(this.intensityTint - 0.05f);
            }

            public void SetState(State s)
            {
                switch(s)
                {
                    case State.Launcher:
                        DisableSwing();
                        this.body.velocity = Vector2.zero;
                        this.body.simulated = false;
                        this.spriteRenderer.enabled = false;
                        this.inputDown = false;
                        this.launcher.wait = false;
                        this.launcher.throbStart = Minigame.fixedDeltaTimeElapsed;
                        this.game.minigameInputTypePrompt.secondsIdle = 0f;
                        this.spriteRenderer.transform.localScale = Vector3.one;
                        this.prev = this.transform.position;
                        SetTint(0);
                        break;
                    case State.Play:
                        this.body.simulated = true;
                        this.spriteRenderer.enabled = true;
                        break;
                    case State.Lost:
                        DisableSwing();
                        this.launcher = this.game.launcher;
                        this.launcher.Load(body);
                        this.launcher.wait = true;
                        this.body.simulated = false;
                        this.spriteRenderer.enabled = false;
                        break;
                    case State.Windup:
                        this.launchPower = LaunchMin;
                        this.launcher.throb = false;
                        break;
                    case State.Finish:
                        if(this.game.IsComplete() == false)
                        {
                            DisableSwing();
                            this.game.CompleteLevel();
                        }
                        this.body.gravityScale = 0;
                        break;
                }

                this.state = s;
            }

            public void Death()
            {
                PlayerAssetsForMinigame.Death(this.transform.position + (Vector3)this.circle.offset, this.game);
                
                if(this.game.BallCount() > 1)
                {
                    this.game.balls.Remove(this);
                    this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, Minigame.Scale * 0.2f);
                    Destroy(gameObject);
                }
                else
                {
                    SetState(State.Lost);
                    foreach(var inputListener in this.game.inputListeners)
                        inputListener.ClearData();
                    Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["sfx_rolling_magnet_tiles_loop"]);
                    if(this.game.IsFailed() == false)
                        this.game.camCtrl.PauseAndPanTo(this.launcher.transform.localPosition, 0.5f, 0.05f, 0.5f, ()=>
                        {
                            this.transform.localPosition = this.launcher.transform.localPosition;
                            this.spend = true;
                            SetState(State.Launcher);
                        });
                    else
                    {
                        this.game.positionLastBall = this.transform.localPosition;
                        this.game.camCtrl.clampAction = this.game.camCtrl.ClampX;
                        this.game.FailLevel();
                    }
                }
            }

            public void Teleport(Vector2 position)
            {
                SetTint(1f);
                this.body.position = position;
                DisableSwing();
            }

            public void DisableSwing()
            {
                this.swingAnchor?.SetGrab(null);
                this.jointSwing.enabled = false;
                this.swingAnchor = null;
            }

            public void ConveyorPush(Collider2D collider)
            {
                var vel = this.body.velocity;
                var len = vel.magnitude;
                var dir = collider.name.Contains("left") ? -1 : 1;
                if(len < ConveyorSpeed)
                    this.body.velocity += new Vector2(dir * (ConveyorSpeed - len), 0);
                else if(dir * vel.x < 0)
                    this.body.velocity += new Vector2(dir * ConveyorSpeed * 0.1f, 0);
            }

            public void RestoreBallSimulation()
            {
                this.body.simulated = true;
            }

            public bool InputDownNew()
            {
                var isDown = this.game.input.holding == true && this.inputDown == false;
                if(isDown)
                {
                    this.inputDown = true;
                }
                return isDown;
            }

            private void ShootKingHat()
            {

                Vector2 launchVector = this.launcher.transformPivot.localRotation * Vector2.right;
                Particle hatParticle = Particle.CreateWithSprite(
                                game.assets.spriteLookup[Minigame.GlobalPrefix + "king_hat_flat"],
                                60,
                                this.launcher.transform.position,
                                game.transform
                            );
                hatParticle.velocity = launchVector * Mathf.Max(this.launcher.power, 0.5f);
                hatParticle.acceleration = new Vector2(0, -0.025f);
                hatParticle.spin = 5f;
                hatParticle.ScaleOut(AnimationCurve.Linear(0, 0.2f, 0.25f, 1f));
                hatParticle.spriteRenderer.sortingLayerName = this.game.objectLayers.playerSortingLayer;
            }

            private void SetTint(float value)
            {
                if(value < 0) value = 0;
                this.intensityTint = value;
                this.spriteRenderer.material.SetFloat("_Intensity", value);
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
                if(collision.gameObject.name.StartsWith("tile") || collision.gameObject.name.StartsWith("cloud"))
                {
                    if (!Audio.instance.IsSfxPlaying(this.game.assets.audioClipLookup["sfx_golf_floor_bump"]))
                    {
                        Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_golf_floor_bump"]);
                    }
                }
            }
        }
    }
}
