using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace rolling
    {
        public class Bumper : MonoBehaviour, IInputListener
        {

            public RollingGame game;
            private PlayAnimated playAnimated;
            public bool isRound = false;

            public List<Collision2D> collisions = new List<Collision2D>();
            public List<(Collision2D collision, float time)> collisionBuffer = new List<(Collision2D collision, float time)>();
            private float timeInput = -1f;
            const float BufferDelay = 0.1f;
            
            // adjacent bumpers can double-bump, so this needs to be recorded globally
            public static Dictionary<Collider2D, float> bumped = new Dictionary<Collider2D, float>();

            private void Awake()
            {
                this.playAnimated = GetComponent<PlayAnimated>();
            }


            public void InputPressed()
            {
                this.timeInput = Time.timeSinceLevelLoad;

                foreach(var i in collisions)
                    Bump(i);

                if(this.collisionBuffer.Count > 0)
                {
                    foreach(var cb in collisionBuffer)
                    {
                        if(Time.timeSinceLevelLoad - cb.time < BufferDelay)
                            Bump(cb.collision);
                    }
                    this.collisionBuffer.Clear();
                }
            }

            public void InputHolding() { }
            public void InputReleased() { }

            private void Bump(Collision2D collision)
            {
                if
                (
                    bumped.TryGetValue(collision.collider, out float time) == false ||
                    Time.timeSinceLevelLoad - time > BufferDelay
                )
                {
                    var rb = collision.collider.attachedRigidbody;
                    if(rb != null && rb.name.Contains("player"))
                    {
                        this.playAnimated.PlayOnce();
                        if(isRound)
                            Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_pinball_bumper_round"]);
                        else
                            Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_pinball_bumper_triangle"]);

                        var bumperTransform = transform;
                        var polygonCollider = this.GetComponent<PolygonCollider2D>();
                        this.game.camCtrl.AddShake(Vector2.up, 0.25f, 0.4f, 0.2f);
                        Vector2 v;
                        if(polygonCollider != null)
                            v = -collision.GetContact(0).normal;
                        else
                            v = (Vector2)(rb.transform.position - bumperTransform.position).normalized;
                        rb.velocity += v * RollingGame.BumperPower;
                        bumped[collision.collider] = Time.timeSinceLevelLoad;
                        //Debug.Log($"BUMP {v}");
                    }
                }
                //else
                //{
                //    if(this.bumped.TryGetValue(collision.collider, out float time2))
                //    {
                //        Debug.Log(Time.timeSinceLevelLoad - time2);

                //    }
                //}
            }

            public void ClearData()
            {
                this.collisionBuffer.Clear();
                this.collisions.Clear();
            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                //this.GetComponent<SpriteRenderer>().color = Color.red;
                if(Time.timeSinceLevelLoad - this.timeInput < BufferDelay)
                {
                    //Debug.Log("on collision enter");
                    Bump(collision);
                }
                else
                {
                    this.collisions.Add(collision);
                }
            }

            void OnCollisionExit2D(Collision2D collision)
            {
                for(int i = 0; i < this.collisions.Count; i++)
                    if(this.collisions[i].collider == collision.collider)
                    {
                        this.collisionBuffer.Add((this.collisions[i], Time.timeSinceLevelLoad));
                        this.collisions.RemoveAt(i);
                        break;
                    }
                //this.GetComponent<SpriteRenderer>().color = Color.white;
            }


        }

    }
}

