using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDebugScroll : MonoBehaviour
{

    public Camera cam;
    public float scrollStep = 5f;

    public void ScrollDown()
    {
        cam.transform.position += Vector3.down * scrollStep;
    }

    public void ScrollUp()
    {
        cam.transform.position += Vector3.up * scrollStep;
    }
}

