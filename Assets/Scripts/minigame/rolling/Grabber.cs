using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {
        public class Grabber : MonoBehaviour, IInputListener
        {

            public RollingGame game;
            public Collider2D trigger;
            public Ball ball;
            public Transform transformAnchor;
            public SpriteRenderer spriteRendererClaw;
            public LineRenderer lineRenderer;
            public Motor motor;
            public Vector3 dirGrab;
            public float length;
            public float lengthCurrent;
            public State state;

            public Sprite spriteIdle;
            public Sprite spriteExtend;
            public Sprite spriteGrab;

            private float gravityBallDefault;

            float speedReachOut = 0.2f;
            float speedReachIn = 0.3f;

            const float LengthOffset = -1f;

            public enum State
            {
                Idle, Reach, Grabbed
            }

            public void Init(MinigameMap.Connection conn, RollingGame game)
            {
                this.game = game;
                if(game.map.motorLookup.TryGetValue(conn.a, out Motor motor) == true)
                {
                    this.motor = motor;
                    motor.paused = true;
                    motor.speed = 0;
                    motor.looped = false;
                    this.game.map.motorLookup.Remove(conn.a);
                }
                this.speedReachOut = conn.speedPixelsPerSecond / 60f / MinigameMap.EditorScale;
                this.speedReachIn = this.speedReachOut * 2f;
                var delta = conn.posB - conn.posA;
                this.dirGrab = delta.normalized;
                this.length = delta.magnitude;
                this.lengthCurrent = 0;
                this.lineRenderer.sortingLayerName = game.objectLayers.objectsSortingLayer;
                this.lineRenderer.sortingOrder = -1;
                this.lineRenderer.material.mainTextureOffset = Vector2.zero;
                this.lineRenderer.material.SetFloat("_ScrollOffset", 0);
                this.spriteRendererClaw.transform.localRotation = 
                    this.transformAnchor.localRotation = 
                        Quaternion.Euler(0, 0, Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg + 90f);
                this.spriteRendererClaw.transform.position = LengthOffset * this.dirGrab;
                this.transformAnchor.localPosition = LengthOffset * this.dirGrab;
            }

            private void FixedUpdate()
            {
                Advance();
            }

            void Advance()
            {
                switch(this.state)
                {
                    case State.Idle:
                        if(this.lengthCurrent > 0f)
                            this.lengthCurrent = Mathf.MoveTowards(this.lengthCurrent, 0f, speedReachIn);
                        break;
                    case State.Reach:

                        break;
                    case State.Grabbed:
                        break;
                }
                this.spriteRendererClaw.transform.localPosition = this.dirGrab * (this.lengthCurrent + LengthOffset);
                UpdateLineRenderer();
                if(this.state == State.Grabbed)
                    this.ball.body.position = this.spriteRendererClaw.transform.position - this.dirGrab * LengthOffset;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(this.game.input.holding == true)
                    Grab(collision.GetComponent<Ball>());
            }

            public void ClearData()
            {
                this.ball = null;
                this.state = State.Idle;
                this.spriteRendererClaw.sprite = this.spriteIdle;
                this.trigger.enabled = true;
                if(this.motor != null && this.motor.speed > 0)
                {
                    this.motor.SetSpeed(-1);
                }
            }

            public void InputPressed()
            {
                this.state = State.Reach;
                this.spriteRendererClaw.sprite = this.spriteExtend;

                if(this.game.camCtrl.OnScreen(this.spriteRendererClaw.transform.position, Vector2.zero))
                {
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_rolling_grabber_extend"]);
                }
            }

            public void InputHolding() {
                if(state == State.Reach)
                    this.lengthCurrent = Mathf.MoveTowards(this.lengthCurrent, this.length, speedReachOut);
                else
                    this.lengthCurrent = Mathf.MoveTowards(this.lengthCurrent, 0f, speedReachIn);
            }

            public void InputReleased() {
                if(this.ball != null)
                {
                    this.ball.gravityDefault = this.gravityBallDefault;
                    this.ball.body.velocity = this.dirGrab * 10f;
                }
                this.ball = null;
                this.state = State.Idle;
                this.spriteRendererClaw.sprite = this.spriteIdle;
                this.trigger.enabled = true;
                if(this.motor != null && this.motor.speed > 0)
                {
                    this.motor.SetSpeed(-1);
                }
            }

            public void Grab(Ball ball)
            {
                if(this.game != null)
                {
                    if(this.game.IsComplete()) return;// don't trigger events during end game
                    //if(this.motor == null || this.state != State.Reach) return;
                    if(this.state != State.Reach) return;

                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_rolling_grabber_catch"]);

                    this.ball = ball;
                    this.ball.body.velocity = Vector2.zero;
                    this.gravityBallDefault = ball.gravityDefault;
                    ball.gravityDefault = 0f;
                    this.state = State.Grabbed;
                    this.trigger.enabled = false;
                    this.spriteRendererClaw.sprite = this.spriteGrab;
                    this.motor?.SetSpeed(1);
                }

            }

            public void UpdateLineRenderer()
            {
                this.lineRenderer.positionCount = 2;
                this.lineRenderer.SetPositions(new[] {
                    this.transformAnchor.position,
                    this.spriteRendererClaw.transform.position
                });
            }
        }
    }
}
