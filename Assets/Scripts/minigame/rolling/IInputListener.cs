using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputListener
{
    public void InputPressed();
    public void InputReleased();
    public void InputHolding();
    public void ClearData();
}
