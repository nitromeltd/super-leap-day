using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {
        public class MagnetInput : MonoBehaviour, IInputListener
        {
            public RollingGame game;
            public float speed = 1f;
            public Vector2 size;
            public Shape shape;

            public enum Shape
            {
                Circle, Rect
            }

            public void InputPressed() {
            }
            public void InputReleased() { }
            public void InputHolding()
            {
                switch(this.shape)
                {
                    case Shape.Circle:
                        foreach(var b in this.game.balls)
                        {
                            var range = this.size.x + b.circle.radius;
                            var delta = b.transform.position - transform.position;
                            if(delta.sqrMagnitude <= range * range)
                            {
                                b.body.AddForce(delta.normalized * speed);
                            }
                        }
                        break;
                }
            }
            public void ClearData() { }


        }
    }
}
