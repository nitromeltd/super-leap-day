using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    public class MotorInput : Motor, IInputListener
    {
        public MotorInput(Transform transform, List<Tween> tweens = null) : base(transform, tweens, false)
        {
            this.paused = true;
            this.speed = 0;
        }

        public void InputPressed()
        {
            SetSpeed(1);
        }

        public void InputHolding()
        {

        }

        public void InputReleased()
        {
            SetSpeed(-2);
        }

        public void ClearData()
        {

        }
    }

}
