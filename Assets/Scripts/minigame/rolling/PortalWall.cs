using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace rolling
    {
        public class PortalWall : MonoBehaviour
        {
            public RollingGame game;
            public EdgeCollider2D edgeCollider;
            public PortalWall target;
            public Vector2 dir;
            public Animated.Animation lightAnimation;
            public Animated.Animation lightAnimation2;

            [HideInInspector] public Vector2Int p;
            [HideInInspector] public int i;
            [HideInInspector] public Animated animated;
            [HideInInspector] public List<PortalWall> list;

            [Header("Teleporter")]
            public SpriteRenderer teleporterSR;
            public Sprite leftTeleporter;
            public Sprite middleTeleporter;
            public Sprite rightTeleporter;

            [Header("Glow")]
            public SpriteRenderer glowSR;
            public Sprite leftGlow;
            public Sprite middleGlow;
            public Sprite rightGlow;

            [Header("Particles")]
            public Sprite[] particles;
            public AnimationCurve particleFadeOutCurve;

            [Header("White")]
            public Animated animatedWhiteEffect;
            public Animated.Animation emptyAnimation;
            public Animated.Animation whiteAnimation;

            private int particleRatioOffset;

            private const int ParticleRatio = 25;

            private float timeExit;
            private const float ExitDelay = 0.1f;

            public const float ThresholdSpitOut = 1f;

            [System.NonSerialized] public Vector2 min, max;

            public void Init(NitromeEditor.TileLayer.Tile tile, Vector2Int p, RollingGame game)
            {
                this.p = p;
                this.game = game;
                dir = Quaternion.Euler(0, tile.flip ? 180 : 0, tile.rotation * (tile.flip ? -1 : 1)) * Vector2.up;
                dir = new Vector2(Mathf.Round(dir.x), Mathf.Round(dir.y));
            }

            public void Merge(List<PortalWall> list)
            {
                this.list = list;
                (var minPoint, var maxPoint) = (new Vector2(float.MaxValue, float.MaxValue), new Vector2(float.MinValue, float.MinValue));
                var i = 0;
                foreach(var pw in list)
                {
                    var a = (Vector2)pw.transform.position;
                    var b = (Vector2)pw.transform.position + (Vector2)(pw.transform.localRotation * Vector2.right);
                    minPoint.x = Mathf.Min(minPoint.x, a.x, b.x);
                    maxPoint.x = Mathf.Max(minPoint.x, a.x, b.x);
                    minPoint.y = Mathf.Min(minPoint.y, a.y, b.y);
                    maxPoint.y = Mathf.Max(minPoint.y, a.y, b.y);

                    pw.animated = pw.GetComponent<Animated>();
                    if(pw != this)
                    {
                        pw.transform.SetParent(this.transform, true);
                        Destroy(pw.edgeCollider);
                    }
                    pw.animated.PlayAndLoop(i % 2 == 0 ? this.lightAnimation : this.lightAnimation2);
                    Sprite teleporterSprite, glowSprite;
                    var flipped = dir.x < 0 || dir.y > 0;
                    if(i == 0)
                    {
                        teleporterSprite = flipped ? leftTeleporter : rightTeleporter;
                        glowSprite = flipped ? leftGlow : rightGlow;
                    }
                    else if(i == list.Count - 1)
                    {
                        teleporterSprite = flipped ? rightTeleporter : leftTeleporter;
                        glowSprite = flipped ? rightGlow : leftGlow;
                    }
                    else
                    {
                        teleporterSprite = middleTeleporter;
                        glowSprite = middleGlow;
                    }
                    pw.teleporterSR.sprite = teleporterSprite;
                    pw.glowSR.sprite = glowSprite;
                    pw.particleRatioOffset = UnityEngine.Random.Range(0, ParticleRatio);
                    pw.animatedWhiteEffect.PlayOnce(pw.emptyAnimation);
                    i++;
                }
                (this.min, this.max) = (minPoint, maxPoint);
                
                minPoint -= (Vector2)this.transform.position;
                maxPoint -= (Vector2)this.transform.position;

                Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) => Quaternion.Euler(angles) * (point - pivot) + pivot;
                
                minPoint = RotatePointAroundPivot(minPoint, Vector2.zero, new Vector3(0, 0, -this.transform.localRotation.eulerAngles.z));
                maxPoint = RotatePointAroundPivot(maxPoint, Vector2.zero, new Vector3(0, 0, -this.transform.localRotation.eulerAngles.z));

                this.edgeCollider.SetPoints(new List<Vector2> { minPoint, maxPoint });
            }

            private void FixedUpdate()
            {
                if((Time.frameCount + this.particleRatioOffset) % ParticleRatio == 0)
                {
                    EmitParticles();
                }
            }

            private void EmitParticles()
            {
                float size = 0.40f;
                Vector2 randomPosition = new Vector2(
                    UnityEngine.Random.Range(-size, size),
                    UnityEngine.Random.Range(-size, size)
                );

                Particle particle = Particle.CreateWithSprite(
                    this.particles[UnityEngine.Random.Range(0, this.particles.Length)],
                    18,
                    Minigame.ToVector2(this.p) + randomPosition,
                    this.transform.parent
                );

                float speed = 0.08f;
                particle.velocity = this.dir * speed;

                particle.FadeOut(this.particleFadeOutCurve);

                particle.spriteRenderer.sortingLayerName = "Warps";
                particle.spriteRenderer.sortingOrder = -5;
            }

            public void LightUp()
            {
                foreach(var portalBit in list)
                {
                    portalBit.animatedWhiteEffect.PlayOnce(portalBit.whiteAnimation, () =>
                    {
                        portalBit.animatedWhiteEffect.PlayOnce(portalBit.emptyAnimation);
                    });
                }
            }

            void OnCollisionEnter2D(Collision2D collision)
            {
                var rb = collision.collider.attachedRigidbody;
                
                if(this.target == null)
                    Debug.LogWarning($"No target PortalWall for {name}");
                
                if(
                    this.target != null &&
                    Time.timeSinceLevelLoad - this.timeExit > ExitDelay &&
                    rb != null && rb.name.Contains("player")
                )
                {
                    this.target.timeExit = Time.timeSinceLevelLoad;
                    Debug.DrawLine(rb.position, this.min, Color.black);
                    var enterOffset = rb.position - this.min;

                    var dest = rb.position;

                    if(dir == Vector2.up || dir == Vector2.down)
                    {
                        var entryLength = this.max.x - this.min.x;
                        var exitLength = this.target.max.x - this.target.min.x;
                        var entryNormal = enterOffset.x / entryLength;
                        dest = this.target.min + new Vector2(entryNormal * exitLength, -enterOffset.y);
                    }
                    else if(dir == Vector2.right || dir == Vector2.left)
                    {
                        var entryLength = this.max.y - this.min.y;
                        var exitLength = this.target.max.y - this.target.min.y;
                        var entryNormal = enterOffset.y / entryLength;
                        dest = this.target.min + new Vector2(-enterOffset.x, entryNormal * exitLength) + target.dir * 0.05f;
                    }

                    rb.GetComponent<Ball>()?.Teleport(dest);

                    if(rb.velocity.magnitude <= ThresholdSpitOut)
                    {
                        //Debug.Log("TOO SLOW");
                        rb.velocity += dir;
                    }

                    LightUp();
                    target.LightUp();
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_env_portal"]);

                    //Debug.Break();
                }
            }
        }
    }
}
