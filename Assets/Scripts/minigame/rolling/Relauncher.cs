using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace minigame
{
    namespace rolling
    {
        public class Relauncher : Launcher
        {

            [NonSerialized] public float resetCount = ResetDelay;
            private const float ResetDelay = 0.25f;

            public override void Advance()
            {
                base.Advance();
                if(this.resetCount < ResetDelay)
                    this.resetCount += Time.fixedDeltaTime;
            }

            public override void LaunchThrust(float power)
            {
                base.LaunchThrust(power);
                resetCount = 0;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(collision.name == "player" && this.resetCount >= ResetDelay && this.rigidBody2DBall == null)
                {
                    collision.transform.localPosition = this.transform.localPosition;
                    this.resetCount = 0;
                    var ball = collision.GetComponent<Ball>();
                    if(ball != null)
                    {
                        Audio.instance.PlaySfx(ball.game.assets.audioClipLookup["sfx_env_pipe_in"]);
                        Load(ball.body);
                        if(this == ball.game.launcher)// don't spend a ball when relaunching
                            ball.game.isRelaunch = true;
                        ball.launcher = this;
                        ball.SetState(Ball.State.Launcher);
                    }
                }
            }

        }
    }
}
