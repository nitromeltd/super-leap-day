using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace minigame
{
    namespace rolling
    {
        public partial class RollingGame : Minigame
        {
            [Header("Rolling Game")]
            public Camera cameraBakeBackground;
            public TileTheme tileThemeRotate;
            public GameObject ballsLeftPortrait;
            public GameObject ballsLeftLandscape;
            public Image[] imageBalls;
            public TMP_Text scoreText;// currently disabled, may reinstate later
            public SpriteRenderer backgroundTiled;
            public SpriteRenderer backgroundBaked;
            public Camera cameraShadow;
            public MeshRenderer meshShadow;
            public RenderTexture renderTextureShadow;
            [NonSerialized] public Vector3 camOffset;
            [NonSerialized] public List<Ball> balls = new List<Ball>();
            [NonSerialized] public List<SwingAnchor> swingAnchors = new List<SwingAnchor>();
            [NonSerialized] public Launcher launcher;
            [NonSerialized] public List<Launcher> launcherCheckpoints = new List<Launcher>();
            [NonSerialized] public bool isRelaunch;
            [NonSerialized] public Transform transformRotateTiles;
            public Dictionary<Vector2Int, int> finishLookup = new Dictionary<Vector2Int, int>();
            public Dictionary<Vector2Int, MagnetPath> magnetLookup = new Dictionary<Vector2Int, MagnetPath>();
            public Dictionary<Vector2Int, Vector2> portalLookup = new Dictionary<Vector2Int, Vector2>();
            public int ballsLeft;
            public int queuedBalls;
            public Vector2 positionFinish;
            public List<IInputListener> inputListeners = new List<IInputListener>();
            public const float DeltaBackgroundHeight = -0.5f;
            public const float DeltaBackgroundLandscapeHeight = -2f;
            [NonSerialized] public Vector3 positionLastBall;
            [NonSerialized] public Transform transformBackgroundLandscape;
            [NonSerialized] public Texture2D texture2DBackground;

            public const float Hole2BallDelay = 0.002f;// multplied by distance between hole and ball
            public const float BeforeBall2HoleDelay = 1f;
            public const float PanDelay = 0.02f;// multplied by distance
            public const float BumperPower = 30f;
            public const float BackgroundShinePerTile = 80.31373f / 4.69f;
            public static string SortingLayerBake = "Pickups Above UI";
            public static int shadowSortingOrder = 0;

            public class MagnetPath
            {
                public Vector2 dir;
                public SpriteRenderer spriteRenderer;
                public bool isOn = true;
                public Color colTarget = new Color(1, 1, 1, 0);
            }

            void Start()
            {
                SortingLayerBake = this.objectLayers.backgroundSortingLayer;

                Scale = 1f;
                ScoreRise.Layer = this.objectLayers.fxSortingLayer;
                ScoreRise.Spacing = -0.05f * Scale;
                ScoreRise.RiseDist = 2f * Scale;
                ScoreRise.RiseDelay = 1f * Scale;
                ScaleOne = new Vector2(Scale, Scale);
                ScaleHalf = new Vector2(Scale * 0.5f, Scale * 0.5f);
                ScreenWidth = 16;
                if(GameCamera.IsLandscape())
                {
                    ballsLeftPortrait.SetActive(false);
                    imageBalls = ballsLeftLandscape.GetComponentsInChildren<Image>().Skip(1).ToArray();
                }
                else
                    ballsLeftLandscape.SetActive(false);

                Init();// Minigame setup

                // shadow mesh / camera / renderTexture
                this.meshShadow.transform.localScale = new Vector3(camCtrl.rect.width, camCtrl.rect.height, 1f);
                this.cameraShadow.orthographicSize = this.cam.orthographicSize;
                this.renderTextureShadow = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGB32);
                this.renderTextureShadow.Create();
                this.cameraShadow.targetTexture = this.renderTextureShadow;
                this.meshShadow.material.SetTexture("_MainTex", this.renderTextureShadow);

                Minigame.RetrySubtitle = "no balls left!";
                CamChangeInterpolation = 0.1f;

                Restart(true);

#if UNITY_EDITOR
                // Create level thumbnail
                if(this.map.rectPreviewSnapshot.HasValue == false)
                    this.map.rectPreviewSnapshot = new Rect(new Vector2(this.levelRect.width * 0.5f, this.playerObj.transform.localPosition.y + 8f) - MinigameMap.SizePreviewSnapshot * 0.5f, MinigameMap.SizePreviewSnapshot);

                this.map.PreviewSnapshot(this.map.rectPreviewSnapshot.Value, () => { UpdateBackground(); this.cameraShadow.Render(); });
                UpdateBackground();
#endif

            }

            void OnApplicationQuit()
            {
                Save();
            }

            public override void Save()
            {
                base.Save();
            }

            public override bool IsFailed()
            {
                return
                    this.ballsLeft <= 0 &&
                    this.balls.Count + this.queuedBalls == 1 &&
                    this.balls[0].state == Ball.State.Lost;
            }
            public override bool IsComplete()
            {
                foreach(var b in this.balls)
                {
                    if(b.state == Ball.State.Finish) return true;
                }
                return false;
            }

            public override void Restart(bool hard)
            {
                base.Restart(hard);

                if(this.renderTextureShadow != null)
                    this.renderTextureShadow.Release();
                this.ballsLeft = 3;
                UpdateBallsLeft();
                this.balls.Clear();
                this.finishLookup.Clear();
                this.magnetLookup.Clear();
                this.portalLookup.Clear();
                this.swingAnchors.Clear();
                this.launcherCheckpoints.Clear();
                this.inputListeners.Clear();
                this.queuedBalls = 0;
                this.isRelaunch = false;
                Bumper.bumped.Clear();
                foreach(var inputListener in this.inputListeners)
                    inputListener.ClearData();
                if(this.transformBackgroundLandscape != null)
                    Destroy(this.transformBackgroundLandscape.gameObject);
                this.camCtrl.clampAction = this.camCtrl.Clamp;

                this.fileLevel = GetLevelFile(difficulty, level);
                //this.fileLevel = DebugPanelLevelsDropdown.level ?? this.assets.levelTest;

                CreateLevel(this.fileLevel);
                this.input.holding = false;
                this.camOffset = Vector3.zero;
                this.camCtrl.SetPosition(GetCameraTargetDefault(), Vector3.zero);
            }

            public override void ExitGame()
            {
                base.ExitGame();
                if(this.backgroundBaked != null)
                    this.backgroundBaked.sprite = null;
                if(this.texture2DBackground != null)
                {
                    Destroy(this.texture2DBackground);
                    this.texture2DBackground = null;
                }
                if(this.renderTextureShadow != null)
                    this.renderTextureShadow.Release();
                this.renderTextureShadow = null;
            }

            private void OnDestroy()
            {
                ExitGame();
            }

            void Update()
            {
                this.input.Advance();

#if UNITY_EDITOR || DEVELOPMENT_BUILD
                if(Input.GetKeyDown(KeyCode.W))
                    winMenu.Open();
                if(Input.GetKeyDown(KeyCode.R) && Input.GetKey(KeyCode.LeftShift))
                    Restart(true);
                else if(Input.GetKeyDown(KeyCode.R))
                    retryMenu.Open();
                if(Input.GetKeyDown(KeyCode.B))
                    AddMultiBall();
#endif

                if(Minigame.paused == false)
                {
                    Animated.AdvanceAll(Time.deltaTime);
                    Particle.AdvanceAll();
                }
            }

            void FixedUpdate()
            {

                if(IsFailed() == true)
                {
                    // look just above the ball, so the continue cost doesn't cover it
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, this.positionLastBall + Vector3.up * 3f, 0.1f), Vector3.zero);
                    return;
                }
                if(IsComplete() == true)
                {
                    // look just above the ball
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, GetCameraTargetDefault() + Vector3.up * 1f, 0.1f), Vector3.zero);
                    return;
                }

                if(this.camCtrl.lookAtTween == null)
                {
                    if(this.queuedBalls > 0)
                    {
                        if(IsLauncherFree())
                        {
                            var obj = Instantiate(this.assets.prefabLookup["player"], this.launcher.transform.localPosition, Quaternion.identity, transform);
                            obj.name = "player";
                            ObjectLayers.SetSortingLayer(obj, this.objectLayers.playerSortingLayer);
                            var ball = obj.GetComponent<Ball>();
                            ball.launcher = this.launcher;
                            ball.Init(this);
                            this.balls.Add(ball);
                            this.launcher.Load(ball.body);
                            this.queuedBalls--;
                        }
                    }

                    foreach(var ball in this.balls)
                        ball.Advance();

                    foreach(var kv in this.magnetLookup)
                    {
                        var col = kv.Value.spriteRenderer.color;
                        kv.Value.spriteRenderer.color = Color.Lerp(col, kv.Value.colTarget, 0.1f);
                        kv.Value.colTarget = new Color(1, 1, 1, 0);
                    }

                    if(this.input.pressed == true)
                        foreach(var inputListener in this.inputListeners)
                            inputListener.InputPressed();


                    var magnetCellsActive = new List<Vector2Int>();
                    var sfxMagnet = false;
                    if(this.input.holding == true)
                    {
                        foreach(var b in this.balls)
                        {
                            if(b.state == Ball.State.Lost)
                                continue;
                            var magnetCells = PickupMap.GetCircleCollideCells(this.magnetLookup, b.transform.position, b.circle.radius);
                            var boost = Vector2.zero;
                            foreach(var c in magnetCells)
                                if(this.magnetLookup[c].isOn == true)
                                    boost += this.magnetLookup[c].dir;
                            magnetCellsActive.AddRange(magnetCells);
                            if(boost != Vector2.zero)
                            {
                                sfxMagnet = true;
                                boost.Normalize();
                                b.body.AddForce(boost * 750);
                            }
                        }
                        foreach(var inputListener in this.inputListeners)
                            inputListener.InputHolding();
                    }
                    ForeachInFlood(magnetCellsActive, this.magnetLookup, (p) => true, (p) =>
                    {
                        this.magnetLookup[p].colTarget = Color.white;
                    }, 1);
                    if(sfxMagnet == true)
                    {
                        if(Audio.instance.IsSfxPlaying(this.assets.audioClipLookup["sfx_rolling_magnet_tiles_loop"]) == false)
                        {
                            Audio.instance.PlaySfxLoop(this.assets.audioClipLookup["sfx_rolling_magnet_tiles_loop"]);
                            StartCoroutine(Audio.instance.ChangeSfxVolume(this.assets.audioClipLookup["sfx_rolling_magnet_tiles_loop"], startFrom: 0, setTo: 1f, duration: .1f));
                        }
                    }
                    else
                        Audio.instance.StopSfxLoop(this.assets.audioClipLookup["sfx_rolling_magnet_tiles_loop"]);

                    if(this.input.released == true)
                        foreach(var inputListener in this.inputListeners)
                            inputListener.InputReleased();

                    UpdateMotors();
                    this.camCtrl.AdvanceShakes(Time.fixedDeltaTime);
                    this.camCtrl.SetPosition(Vector3.Lerp(this.cam.transform.position, GetCameraTargetDefault(), 0.05f), this.camOffset);
                }
                else
                    this.camCtrl.UpdateLookAt(Time.fixedDeltaTime);

                // last ball warning
                if
                (
                    this.ballsLeft == 1 &&
                    this.launcher.rigidBody2DBall != null &&
                    this.isRelaunch == false &&
                    this.launcher.rigidBody2DBall.GetComponent<Ball>().spend == true
                )
                this.imageBalls[0].transform.localScale = Vector3.one * (1f + Shake.Wave(fixedDeltaTimeElapsed, 0.5f, 0.25f));

                UpdateBackground();
                Minigame.fixedDeltaTimeElapsed += Time.fixedDeltaTime;
                this.input.FixedAdvance();

                frameCount++;
            }

            private void LateUpdate()
            {
                foreach(var swingAnchor in this.swingAnchors)
                    swingAnchor.UpdateRotation();
            }

            public void UpdateBackground()
            {
                var focusPoint = this.cam.transform.localPosition + Vector3.down * camCtrl.rect.height * 0.5f;
                focusPoint.z = focusPoint.x = 0;
                this.backgroundBaked.material.SetTextureOffset("_BlendTex", new Vector2(0, -focusPoint.y / 120));
            }

            // debug scrolling for testing background
            //float tempy = 0f;
            public override Vector3 GetCameraTargetDefault()
            {
                //var speed = 0.2f;
                //if(Input.GetKey(KeyCode.UpArrow)) tempy += speed;
                //if(Input.GetKey(KeyCode.DownArrow)) tempy -= speed;
                //var pos = HighestBall().transform.position + Vector3.up * tempy;
                var pos = HighestBall().transform.position;
                pos.x = this.levelRect.width * 0.5f;
                return pos;
            }

            public Ball HighestBall()
            {
                var bestY = float.MinValue;
                Ball ball = null;
                foreach(var b in this.balls)
                {
                    var y = b.transform.position.y;
                    if(y > bestY)
                    {
                        bestY = y;
                        ball = b;
                    }
                }
                return ball;
            }

            public Vector3 ClosestBallDelta(Vector3 pos)
            {
                var best = float.MaxValue;
                var delta = new Vector2();
                foreach(var b in this.balls)
                {
                    var d = b.transform.position - pos;
                    var dist = d.sqrMagnitude;
                    if(dist < best)
                    {
                        delta = d;
                        best = dist;
                    }
                }
                return delta;
            }

            public int BallCount()
            {
                return this.balls.Count + this.queuedBalls;
            }

            public void SpendBall()
            {
                this.ballsLeft--;
                UpdateBallsLeft();
            }

            public void UpdateBallsLeft()
            {
                for(int i = 0; i < this.imageBalls.Length; i++)
                    this.imageBalls[i].enabled = i < this.ballsLeft;
                if(this.ballsLeft == 0) this.imageBalls[0].transform.localScale = Vector3.one;
            }

            public override void BreakTile(Collision2D collision)
            {
                var rb = collision.collider.GetComponent<Rigidbody2D>();
                if(rb != null)
                {
                    rb.velocity += 5f * -collision.contacts[0].normal;
                }
                base.BreakTile(collision);
            }

            public void EventPause()
            {
                for(int i = 0; i < this.balls.Count; i++)
                    this.balls[i].body.simulated = false;
            }

            public void EventUnpause()
            {
                for(int i = 0; i < this.balls.Count; i++)
                    this.balls[i].body.simulated = true;
            }

            public void AddMultiBall()
            {
                this.queuedBalls++;
            }

            public bool IsLauncherFree()
            {
                int i = 0;
                for(; i < this.balls.Count; i++)
                {
                    var b = this.balls[i];
                    if(b.state != Ball.State.Play && b.launcher == this.launcher)
                    {
                        return false;
                    }
                }
                return true;
            }

        }
    }
}

