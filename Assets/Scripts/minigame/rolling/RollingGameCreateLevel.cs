using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace minigame
{
    namespace rolling
    {

        public partial class RollingGame : Minigame
        {

            public void CreateLevel(TextAsset file)
            {

                this.map.AddNoAutoTiles("GLOBAL:"
                    , "spike_top_1"
                    , "spike_top_2"
                    , "spike_right_1"
                    , "spike_right_2"
                    , "spike_bottom_1"
                    , "spike_bottom_2"
                    , "spike_left_1"
                    , "spike_left_2"
                    , "spike_vert_1"
                    , "spike_vert_2"
                    , "spike_horz_1"
                    , "spike_horz_2"
                    , "spike_junct_1"
                );
                this.map.AddNoAutoTiles(""
                    , "polysquare"
                    , "poly45br"
                    , "poly22br"
                    , "poly67br"
                    , "break_tile"
                    , "score_gate"
                    , "cloud_slope_down"
                    , "cloud_slope_up"
                    , "cloud_down"
                    , "magnet_path"
                    , "magnet_path_diagonal"
                    , "magnet_path_diagonal"
                    , "rotate_square"
                    , "rotate_45br"
                    , "rotate_poly45br"
                    , "rotate_polysquare"
                    , "peg"
                    , "peg_small"
                    , "break_tile_peg"
                );
                this.map.AddDontRotateTiles(
                    "spinny_cannon"
                    , "conveyor"
                    , "player"
                    , "flipper_left"
                    , "flipper_right"
                    , "launcher_start"
                    , "launcher_start45"
                    , "launcher_checkpoint"
                    , "launcher_checkpoint45"
                    , "launcher"
                    , "magnet_path"
                    , "magnet_path_diagonal"
                    , "grabber"
                );
                this.map.AddFilledTiles(
                    "portal_wall"
                );
                this.map.AddFilledRectInts(
                    ("launcher_cloud", new RectInt(0, 0, 4, 4))
                );

                var scoreGateTiles = new Dictionary<Vector2Int, GameObject>();
                var slopeColliders = new Dictionary<Vector2Int, Collider2D>();
                var rotTransforms = new Dictionary<Vector2Int, List<Transform>>();
                var slopeNames = new Dictionary<Vector2Int, string>();
                var portalWalls = new Dictionary<Vector2Int, PortalWall>();

                var addToTiles = new List<Transform>();

                var level = this.map.ParseLevel(file);

                CreateRotateTilesLevel(level, rotTransforms);

                var tiles = level.TileLayerByName("Tiles");
                var objects = level.TileLayerByName("Objects");

                // wall-off finish areas with cloud tiles
                void wallInCheck(int index, string wall, int rotation = 0, int offsetX = 0, int offsetY = 0)
                {
                    var look = objects.tiles[index];
                    if(look.tile != "finish")
                    {
                        var tileLook = tiles.tiles[index];
                        if(tileLook.tile == null)
                        {
                            tileLook.tile = wall;
                            tileLook.rotation = rotation;
                            tileLook.offsetX = offsetX;
                            tileLook.offsetY = offsetY;
                            tiles.tiles[index] = tileLook;
                        }
                    }
                }
                this.positionFinish = Vector2.zero;
                var finishTiles = 0;
                for(int i = 0; i < objects.tiles.Length; i++)
                {
                    var t = objects.tiles[i];
                    if(t.tile == "finish")
                    {
                        wallInCheck(i - this.map.width, GlobalPrefix + "topline");// down
                        wallInCheck(i + this.map.width, "cloud_down");// up
                        wallInCheck(i - 1, "cloud_down", 90, 16);// left
                        wallInCheck(i + 1, "cloud_down", 270, 0, 16);// right
                        this.positionFinish += new Vector2(i % this.map.width, i / this.map.width);
                        finishTiles++;
                    }
                }
                if(finishTiles == 0)
                    positionFinish = new Vector2(this.map.width / 2, this.map.height);
                else
                    positionFinish /= finishTiles;


                // generate all objects
                this.map.Create(level);

                this.transformRotateTiles.SetParent(this.transform, true);


                this.motors = this.map.motors;
                this.levelRect = this.map.rect;
                this.levelRectInt = this.map.rectInt;


                // initialise
                foreach(var record in this.map.records)
                {
                    if(record.name.Contains("spike_") == true)
                    {
                        if(record.offset == Vector2.zero)
                            this.spikesLookup[record.p] = 1;
                        else
                        {
                            var r = new Rect(record.p + record.offset, Vector2.one);
                            PickupMap.ForCellsInRect(r, (p) =>
                            {
                                this.spikesOffsetLookup.TryGetValue(p, out List<Rect> list);
                                list ??= new List<Rect>();
                                list.Add(r);
                                this.spikesOffsetLookup[p] = list;
                            });
                        }
                        record.gameObject.layer = ObjectLayers.ToLayer(this.objectLayers.objectLayerMask);
                        record.gameObject.transform.SetParent(this.map.transformObjects);
                    }

                    if(record.isPrefab == true)
                    {
                        switch(record.name)
                        {
                            case "launcher":
                                {
                                    var launcher = record.GetComponent<Launcher>();
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    launcher.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                    if(record.tile.tile.Contains("start"))
                                    {
                                        this.playerObj = Instantiate(this.assets.prefabLookup["player"], record.pos, Quaternion.identity, this.map.transformObjects);
                                        this.playerObj.name = "player";
                                        var ball = playerObj.GetComponent<Ball>();
                                        record.gameObject.name = record.name;
                                        ObjectLayers.SetSortingLayer(playerObj, this.objectLayers.playerSortingLayer);
                                        ball.launcher = this.launcher = launcher;
                                        ball.spend = true;
                                        this.launcher.Load(ball.body);
                                        this.balls.Add(ball);
                                    }
                                    if(record.tile.tile.Contains("checkpoint"))
                                    {
                                        launcher.rank = record.tile.properties["rank"].i;
                                        this.launcherCheckpoints.Add(launcher);
                                    }
                                    if(record.tile.tile.Contains("45"))
                                        launcher.SetRotation(record.tile);
                                    else
                                        launcher.SetRotation(record.tile.rotation);
                                }
                                break;
                            case "break_tile":
                                {
                                    var collisionAction = record.GetComponent<OnCollisionAction>();
                                    collisionAction.action = BreakTile;
                                    var bc = collisionAction.GetComponent<Collider2D>() as BoxCollider2D;
                                    var sr = record.GetComponent<SpriteRenderer>();
                                    sr.sprite = this.assets.spriteLookup[record.nameTile];
                                    var s = bc.size;
                                    switch(record.nameTile)
                                    {
                                        case "break_block_big_purple": goto case "break block big";
                                        case "break_block_big_red": goto case "break block big";
                                        case "break block big":
                                            s *= 2;
                                            break;
                                        case "break_block_portrait_blue": goto case "break block portrait";
                                        case "break_block_portrait_yellow": goto case "break block portrait";
                                        case "break block portrait":
                                            s.y += 1;
                                            break;
                                        case "break_block_landscape_purple": goto case "break block landscape";
                                        case "break_block_landscape_yellow": goto case "break block landscape";
                                        case "break block landscape":
                                            s.x += 1;
                                            break;
                                    }
                                    bc.offset = s / 2;
                                    bc.size = s;
                                }
                                break;
                            case "break_tile_peg":
                                {
                                    var collisionAction = record.GetComponent<OnCollisionAction>();
                                    collisionAction.action = BreakTile;
                                }
                                break;
                            case "bumper_4":
                            case "bumper_3":
                            case "bumper_2":
                            case "bumper_1":
                            case "bumper_4x2_up":
                            case "bumper_2x4_up":
                            case "bumper_3x3_up":
                            case "bumper_4x4_up":
                            case "bumper_4x2_down":
                            case "bumper_2x4_down":
                            case "bumper_3x3_down":
                            case "bumper_4x4_down":
                            case "bumper":
                                record.GetComponent<Bumper>().game = this;
                                this.inputListeners.Add(record.GetComponent<Bumper>());
                                break;
                            case "launcher_cloud":
                                
                                addToTiles.Add(record.gameObject.transform);
                                break;
                            case "cloud_slope_down":
                            case "cloud_slope_up":
                            case "cloud_down":
                                slopeColliders[record.p] = record.GetComponent<EdgeCollider2D>();
                                slopeNames[record.p] = record.name;
                                break;
                            case "spinny_cannon":
                                {
                                    var cannon = record.GetComponent<SpinnyCannon>();
                                    cannon.Init(record.tile);
                                    ObjectLayers.SetSortingLayer(record.gameObject, this.objectLayers.foregroundSortingLayer);
                                    launcher.spriteRendererNozzleBottom.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                }
                                break;
                            case "conveyor_left":
                            case "conveyor_right":
                                {
                                    var a = record.GetComponent<Animated>();
                                    MinigameMap.AddAutotiling(a, record.tile, record.x, record.y, this.assets.autotileArrayLookup[record.name], level);
                                }
                                break;
                            case "rotate_left_pin":
                            case "rotate_right_pin":
                                record.GetComponent<RotateGlom>().Init(record.p, rotTransforms, this);
                                this.inputListeners.Add(record.GetComponent<RotateGlom>());
                                break;
                            case "spin_anchor":
                            case "swing_anchor":
                                {
                                    var dir = 0f;
                                    if(record.name == "spin_anchor")
                                        dir = 1f;

                                    var swingAnchor = record.GetComponent<SwingAnchor>();
                                    swingAnchor.Init(this, dir);
                                    this.swingAnchors.Add(swingAnchor);
                                }
                                break;
                            case "portal_wall":
                                record.GetComponent<PortalWall>().Init(record.tile, record.p, this);
                                portalWalls[record.p] = record.GetComponent<PortalWall>();
                                break;
                        }
                    }
                    else if(record.isSprite == true)
                    {
                        switch(record.tile.tile)
                        {
                            case "finish":
                                this.finishLookup[record.p] = 1;
                                ObjectLayers.SetSortingLayer(record.gameObject, SortingLayerBake);
                                record.GetComponent<Transform>().SetParent(this.map.transformBackground, true);
                                break;
                            case "magnet_path":
                            case "magnet_path_diagonal":
                                {
                                    var sr = record.GetComponent<SpriteRenderer>();
                                    sr.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                                    var srTop = this.spriteRendererPool.Get(Vector3.zero, sr.sprite, this.objectLayers.backgroundSortingLayer, "magnet", sr.transform);
                                    srTop.color = new Color(1, 1, 1, 0);
                                    sr.transform.SetParent(this.map.transformObjects);

                                    var srBack1 = this.spriteRendererPool.Get(record.pos + record.offset, this.assets.spriteLookup["Magnet-Tile-back1"], SortingLayerBake, "magnetBack1", this.map.transformBackground);
                                    srBack1.sortingOrder = -200;
                                    var srBack2 = this.spriteRendererPool.Get(record.pos + record.offset, this.assets.spriteLookup["Magnet-Tile-back2"], SortingLayerBake, "magnetBack2", this.map.transformBackground);
                                    srBack2.sortingOrder = -300;

                                    Vector2 dir;
                                    if(record.tile.tile == "magnet_path")
                                    {
                                        dir = record.tile.rotation switch
                                        {
                                            90 => Vector2.left,
                                            180 => Vector2.down,
                                            270 => Vector2.right,
                                            _ => Vector2.up
                                        };
                                        switch(record.tile.rotation)
                                        {
                                            case 0:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Up 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Up 2"];
                                                break;
                                            case 90:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Left 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Left 2"];
                                                break;
                                            case 180:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Down 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Down 2"];
                                                break;
                                            case 270:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Left 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Left 2"];
                                                sr.flipX = srTop.flipX = true;
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        dir = record.tile.rotation switch
                                        {
                                            90 => new Vector2(-0.7071068f, 0.7071068f),
                                            180 => new Vector2(-0.7071068f, -0.7071068f),
                                            270 => new Vector2(0.7071068f, -0.7071068f),
                                            _ => new Vector2(0.7071068f, 0.7071068f)
                                        };
                                        switch(record.tile.rotation)
                                        {
                                            case 0:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Diagonal 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Diagonal 2"];
                                                sr.flipX = srTop.flipX = true;
                                                break;
                                            case 90:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Diagonal 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Diagonal 2"];
                                                break;
                                            case 180:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Diagonal Down 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Diagonal Down 2"];
                                                break;
                                            case 270:
                                                sr.sprite = this.assets.spriteLookup["Magnet Tile Diagonal Down 1"];
                                                srTop.sprite = this.assets.spriteLookup["Magnet Tile Diagonal Down 2"];
                                                sr.flipX = srTop.flipX = true;
                                                break;
                                        }

                                    }
                                    var path = new MagnetPath
                                    {
                                        dir = dir,
                                        spriteRenderer = srTop
                                    };
                                    this.magnetLookup[record.p] = path;

                                    // stop conveyor belt autotiler from thinking this is a wall
                                    MinigameMap.SetTile(record.p.x, record.p.y, null, level.TileLayerByName("Tiles"), this.levelRectInt.width);
                                }
                                break;
                            case "background_tile1":
                            case "background_tile2":
                            case "background_tile3":
                            case "background_tile4":
                            case "background_tile5":
                            case "background_tile6":
                            case "background_tile7":
                            case "background_tile8":
                                record.gameObject.transform.SetParent(this.map.transformBackground);
                                record.GetComponent<SpriteRenderer>().sortingOrder = -498;
                                record.GetComponent<SpriteRenderer>().sortingLayerName = SortingLayerBake;
                                break;
                        }
                    }
                }

                foreach(var chk in this.launcherCheckpoints)
                    (chk.posBall, chk.distToFinish) =
                        (this.balls[0].transform.localPosition, Mathf.Abs(chk.transform.position.y - this.positionFinish.y));
                this.launcher.distToFinish = Mathf.Abs(this.launcher.transform.position.y - this.positionFinish.y);

                // cloud slopes
                var slopeEnds = this.map.tileGrid.MergeSlopes(slopeNames, slopeColliders);
                foreach(var kv in slopeColliders)
                {
                    var sr = kv.Value.GetComponent<SpriteRenderer>();
                    // edge case for 270 rotation
                    if(sr.transform.localRotation.eulerAngles.z == 270)
                        sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_right"];
                    else
                        sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_left"];
                }
                foreach(var kv in slopeEnds)
                {
                    var sr = kv.Value.GetComponent<SpriteRenderer>();
                    // edge case for 270 rotation
                    if(sr.transform.localRotation.eulerAngles.z == 270)
                        sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_left"];
                    else
                        sr.sprite = this.assets.spriteLookup[slopeNames[kv.Key] + "_right"];
                }

                foreach(var transform in addToTiles) transform.SetParent(this.map.transformTiles, true);

                // merge portal walls
                var keys = portalWalls.Keys.ToList();
                var portalConns = new List<MinigameMap.Connection>();
                foreach(var conn in map.connections)
                {
                    if(conn.nameA == "portal_wall")
                        portalConns.Add(conn);
                }
                foreach(var k in keys)
                {
                    var list = new List<PortalWall>();
                    if(portalWalls.TryGetValue(k, out PortalWall seed) == true)
                    {
                        this.portalLookup[k] = seed.dir;
                        ForeachInFlood(k, portalWalls, (kv) => kv.Value.dir == seed.dir, (k) => {
                            var pw = portalWalls[k];
                            this.portalLookup[k] = pw.dir;
                            list.Add(pw);
                            foreach(var conn in portalConns)
                            {

                                if(conn.objA == pw.gameObject) conn.objA = seed.gameObject;
                                if(conn.objB == pw.gameObject) conn.objB = seed.gameObject;
                            }
                            portalWalls.Remove(k);
                        });
                        if(seed.dir.x == 0)
                            list = list.OrderBy(o => o.p.y).ToList();
                        else
                            list = list.OrderBy(o => o.p.x).ToList();
                        seed.Merge(list);
                    }
                    
                }

                this.spriteRendererPool.sortingOrderCount = 0;

                // create side fill in landscape mode
                if(GameCamera.IsLandscape() == true)
                {
                    float heightFloor = this.camCtrl.rectDefault.height + this.map.height;
                    float halfCamHeight = this.camCtrl.rectDefault.height * 0.5f;
                    SpriteRenderer sr;
                    Vector2 size = new Vector2(2, heightFloor);
                    // tile fill
                    sr = this.spriteRendererPool.Get(new Vector3(-2, -halfCamHeight), this.assets.spriteLookup["floor_fill"], this.objectLayers.backgroundSortingLayer, "fill_left", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                    sr = this.spriteRendererPool.Get(new Vector3(this.map.width, -halfCamHeight), this.assets.spriteLookup["floor_fill"], this.objectLayers.backgroundSortingLayer, "fill_right", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;

                    // fancy sides
                    var obj = new GameObject("Background Landscape");
                    this.transformBackgroundLandscape = obj.transform;

                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width * 0.5f - ScreenWidth * 0.5f, -halfCamHeight), this.assets.spriteLookup["rolling day left"], this.objectLayers.backgroundSortingLayer, "rolling day left", this.transformBackgroundLandscape);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor + DeltaBackgroundLandscapeHeight);
                    sr = this.spriteRendererPool.Get(new Vector3(levelRect.width * 0.5f + ScreenWidth * 0.5f, -halfCamHeight), this.assets.spriteLookup["rolling day right"], this.objectLayers.backgroundSortingLayer, "rolling day right", this.transformBackgroundLandscape);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = new Vector2(sr.size.x, heightFloor + DeltaBackgroundLandscapeHeight);

                }
                // create top / bottom fill for Retry screen
                {
                    SpriteRenderer sr;
                    Vector2 size = new Vector2(this.map.width, this.camCtrl.rectDefault.height * 0.5f);
                    sr = this.spriteRendererPool.Get(new Vector3(0, this.map.height), this.assets.spriteLookup["floor_fill"], this.objectLayers.tilesSortingLayer, "fill_top", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                    sr = this.spriteRendererPool.Get(new Vector3(0, -size.y), this.assets.spriteLookup["floor_fill"], this.objectLayers.tilesSortingLayer, "fill_bottom", this.transform);
                    sr.drawMode = SpriteDrawMode.Tiled;
                    sr.size = size;
                }

                var triggerButtons = new List<TriggerButton>();

                foreach(var conn in this.map.connections)
                {
                    switch(conn.nameB)
                    {
                        case "player":
                            {
                                var tb = conn.objA.GetComponent<TriggerButton>();
                                tb.Init(this);
                                tb.multiball = true;
                            }
                            break;
                        case "button":
                            {
                                var tb = conn.objB.GetComponent<TriggerButton>();
                                tb.Init(this);
                                tb.targets.Add(conn.a);
                                triggerButtons.Add(tb);
                            }
                            break;
                        case "portal_out_3":
                        case "portal_out_2":
                        case "portal_out_1":
                            var portal = conn.objA.GetComponentInChildren<Portal>();
                            var destAnim = conn.objB.GetComponent<PlayAnimated>();
                            ObjectLayers.SetSortingLayer(conn.objA, this.objectLayers.backgroundSortingLayer, RollingGame.shadowSortingOrder - 10);
                            ObjectLayers.SetSortingLayer(conn.objB, this.objectLayers.backgroundSortingLayer, RollingGame.shadowSortingOrder - 10);
                            portal.Init(this, destAnim);
                            break;
                        case "portal_wall":
                            var portalA = conn.objA.GetComponent<PortalWall>();
                            var portalB = conn.objB.GetComponent<PortalWall>();
                            (portalA.target, portalB.target) = (portalB, portalA);
                            break;
                    }
                }
                foreach(var conn in this.map.connectionsBroken)
                {
                    switch(conn.GetName())
                    {
                        case "swing_anchor":
                        case "spin_anchor":
                            var obj = conn.GetObj();
                            foreach(var swingAnchor in this.swingAnchors)
                            {
                                if(swingAnchor.transform == obj.transform)
                                {
                                    swingAnchor.SetRadius((conn.posB - conn.posA).magnitude);
                                    break;
                                }
                            }
                            break;
                        case "grabber":
                            var grabber = conn.GetComponent<Grabber>();
                            grabber.Init(conn, this);
                            this.inputListeners.Add(grabber);
                            foreach(var m in this.motors)
                                if(m.transform == grabber.transform)
                                {
                                    foreach(var sr in m.dots)
                                    {
                                        sr.sortingLayerName = this.objectLayers.objectsSortingLayer;
                                        sr.sortingOrder = -1;
                                        sr.transform.position -= grabber.dirGrab * 1.05f;
                                    }
                                    break;
                                }
                            break;
                    }
                }
                foreach(var tb in triggerButtons)
                {
                    foreach(var target in tb.targets)
                    {
                        if(this.map.motorLookup.TryGetValue(target, out Motor m) == true)
                        {
                            tb.motors.Add(m);
                            m.paused = true;
                            m.looped = false;
                            this.map.motorLookup.Remove(target);
                        }
                        else if(this.map.vector2IntToGameObject.TryGetValue(target, out GameObject obj) == true)
                        {
                            if(obj.name == "launcher")
                                tb.multiball = true;
                        }
                    }
                }
                // find and replace MotorInput targets
                var isMotorInput = new Dictionary<string, bool>
                {
                    ["input_trigger_platform"] = true
                };
                var inputMotors = new List<Motor>();
                var remainingMotors = this.map.motorLookup.Keys.ToList();
                foreach(var key in remainingMotors)
                {
                    var m = this.map.motorLookup[key];
                    if(isMotorInput.ContainsKey(m.transform.name))
                    {
                        this.map.motorLookup.Remove(key);
                        var mi = new MotorInput(m.transform, m.tweens);
                        this.motors.Remove(m);
                        this.motors.Add(mi);
                        this.inputListeners.Add(mi);
                        m.body = m.transform.GetComponent<Rigidbody2D>();
                    }
                }

                BuildBackground();

                // backtrack motors without buttons that don't finish where they start
                MinigameMap.BacktrackMotors(this.map.motorLookup.Values);


                this.balls[0].Init(this);
            }

            public void BuildBackground()
            {
                //this.backgroundTiled.gameObject.SetActive(true);
                var offsetX = -64f;
                ObjectLayers.SetSortingLayer(this.backgroundTiled.gameObject, SortingLayerBake);
                this.backgroundTiled.transform.position = new Vector3(1f + offsetX, 0);

                // shorten background to match level when parallaxed
                var backgroundHeight = this.map.height + DeltaBackgroundHeight;
                var size = this.backgroundTiled.size;
                this.backgroundTiled.size = new Vector2(size.x, backgroundHeight);

                // set shadow above background
                this.meshShadow.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                RollingGame.shadowSortingOrder = this.spriteRendererPool.sortingOrderCount + 100;
                this.meshShadow.sortingOrder = RollingGame.shadowSortingOrder;

                // bake background
                this.backgroundBaked.sprite = null;
                this.backgroundBaked.sortingLayerName = this.objectLayers.backgroundSortingLayer;
                if(this.texture2DBackground != null)
                {
                    Destroy(this.texture2DBackground);
                    this.texture2DBackground = null;
                }

                var r = this.map.rect;
                r.position = new Vector2(offsetX, 0);
                this.cameraBakeBackground.gameObject.SetActive(true);

                this.map.transformBackground.position += new Vector3(offsetX, 0, -1f);
                this.texture2DBackground = MinigameMap.Snapshot(r, this.cameraBakeBackground, (int)MinigameMap.PixelsPerUnit);
                this.texture2DBackground.name = "Baked Background Texture";

                r.size *= MinigameMap.PixelsPerUnit;
                r.position = Vector2.zero;
                this.backgroundBaked.sprite =
                    Sprite.Create(this.texture2DBackground, r, new Vector2(), (int)MinigameMap.PixelsPerUnit);

                //this.map.transformBackground.gameObject.SetActive(false);
                //this.backgroundTiled.gameObject.SetActive(false);
                this.cameraBakeBackground.gameObject.SetActive(false);

                this.backgroundBaked.material.SetTextureScale("_BlendTex", new Vector2(1, this.map.height / BackgroundShinePerTile));

            }

            // Problem: Autotiler only takes a whole level and theme as input
            // Solution: Create a fake level and theme for it to process
            public void CreateRotateTilesLevel(NitromeEditor.Level source, Dictionary<Vector2Int, List<Transform>> rotTransforms)
            {
                var rotateTileMatch = new Dictionary<string, bool>
                {
                    ["rotate_square"] = true
                    , ["rotate_45br"] = true
                    , ["rotate_polysquare"] = true
                    , ["rotate_poly45br"] = true
                };
                var level = this.map.CloneLevel(source);
                this.map.transformTiles = this.transformRotateTiles = new GameObject("rotated").transform;
                
                for(int i = 0; i < level.tileLayers.Length; i++)
                {
                    var layer = level.tileLayers[i];
                    for(int j = 0; j < layer.tiles.Length; j++)
                    {
                        var t = layer.tiles[j];
                        if(t.tile == null || rotateTileMatch.ContainsKey(t.tile) == false)
                        {
                            t.tile = null;
                            layer.tiles[j] = t;
                        }
                        else
                        {
                            if(t.tile == "rotate_polysquare" || t.tile == "rotate_poly45br")
                            {
                                var obj = Instantiate(this.assets.prefabLookup[t.tile], this.transformRotateTiles);
                                obj.transform.position = new Vector2(j % this.map.width, j / this.map.width);
                            }
                            t.tile = t.tile switch
                            {
                                "rotate_square" => "GLOBAL:square",
                                "rotate_45br" => "45br",
                                _ => null
                            };
                            layer.tiles[j] = t;
                            t.tile = null;
                            source.tileLayers[i].tiles[j] = t;
                        }
                    }
                }

                this.map.tileGrid = this.map.GetAutoTiles(level, this.tileThemeRotate, this.map.physicsMaterialNormal, this.map.physicsMaterialSlope, this.map.physicsMaterialDirt, true, true);

                foreach(Transform t in this.transformRotateTiles)
                {
                    var p = new Vector2Int((int)t.position.x, (int)t.position.y);
                    if(rotTransforms.TryGetValue(p, out List<Transform> list) == true)
                        list.Add(t);
                    else
                        rotTransforms[p] = new List<Transform> { t };
                }
            }
        }
    }
}
