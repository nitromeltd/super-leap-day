using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {
        public class RotateGlom : MonoBehaviour, IInputListener
        {
            public RollingGame game;
            public Rigidbody2D body;
            public CompositeCollider2D composite;
            public SpriteRenderer spriteRendererPin;
            public float angleTarget;

            private float angle;

            private const float AngleSpeed = 12f;

            public void Init(Vector2Int pos, Dictionary<Vector2Int, List<Transform>> rotBlockMap, RollingGame game)
            {
                this.game = game;
                Minigame.ForeachInFlood(pos, rotBlockMap, (kv) => true, (p) =>
                {
                    var list = rotBlockMap[p];
                    foreach(var t in list)
                    {
                        t.SetParent(this.transform, true);
                        var collider = t.GetComponent<Collider2D>();
                        if(collider != null)
                            collider.usedByComposite = true;
                    }
                    rotBlockMap.Remove(p);
                });
                this.angle = 0f;
                this.composite.GenerateGeometry();
            }

            void FixedUpdate()
            {
                if(this.game.input.holding == false)
                {
                    var newAngle = Mathf.MoveTowardsAngle(this.angle, 0, AngleSpeed);
                    if(Mathf.Approximately(angle, newAngle) == false)
                    {
                        this.angle = newAngle;
                        this.body.MoveRotation(this.angle);
                    }
                }
                this.spriteRendererPin.transform.localRotation = Quaternion.Euler(0, 0, -this.angle);
            }

            public void InputPressed()
            {
                //this.body.SetRotation(angleTarget);
                //this.spriteRendererPin.transform.localRotation = Quaternion.Euler(0, 0, -this.angle);
            }
            public void InputReleased()
            {
                //this.body.SetRotation(0);
                //this.spriteRendererPin.transform.localRotation = Quaternion.Euler(0, 0, 0);
            }
            public void InputHolding()
            {
                var newAngle = Mathf.MoveTowardsAngle(this.angle, this.angleTarget, AngleSpeed);
                if(newAngle < 0) newAngle += 360;

                if(Mathf.Approximately(this.angle, newAngle) == false)
                {
                    this.angle = newAngle;
                    this.body.MoveRotation(this.angle);
                }
            }
            public void ClearData() {
            }

            private void OnCollisionEnter2D(Collision2D collision)
            {
            }

            private void OnCollisionExit2D(Collision2D collision)
            {
                Audio.instance.PlaySfx(game.assets.audioClipLookup["sfx_player_jump_1"]);
                Audio.instance.PlayRandomSfx(null, null,
                    game.assets.audioClipLookup["sfx_player_jump_1"],
                    game.assets.audioClipLookup["sfx_player_jump_2"],
                    game.assets.audioClipLookup["sfx_player_jump_3"]
                );
            }
        }

    }
}
