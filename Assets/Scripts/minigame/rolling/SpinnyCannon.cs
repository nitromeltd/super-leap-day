using System;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {
        public class SpinnyCannon : Launcher
        {

            public float angle;
            public float dir;
            [NonSerialized] public float resetCount;

            private const float AngleInc = 2f;
            private const float ResetDelay = 0.25f;

            public void Init(NitromeEditor.TileLayer.Tile tile)
            {
                this.angle = (tile.flip ? 180 : 0) + tile.rotation;
                this.dir = tile.flip ? -1 : 1;
                this.resetCount = ResetDelay;
            }

            public override void Advance()
            {
                base.Advance();
                if(this.resetCount < ResetDelay)
                    this.resetCount += Time.fixedDeltaTime;
                this.angle += AngleInc * this.dir;
                SetRotation(this.angle);
            }

            public override void LaunchThrust(float power)
            {
                base.LaunchThrust(power);
                resetCount = 0;
            }

            void OnTriggerEnter2D(Collider2D collision)
            {
                if(collision.name == "player" && this.resetCount >= ResetDelay && this.rigidBody2DBall == null)
                {
                    collision.transform.localPosition = this.transform.localPosition;
                    this.resetCount = 0;
                    var ball = collision.GetComponent<Ball>();
                    if (ball != null)
                    {
                        Audio.instance.PlaySfx(ball.game.assets.audioClipLookup["sfx_env_pipe_in"]);
                        Load(ball.body);
                        ball.launcher = this;
                        ball.SetState(Ball.State.Launcher);
                    }
                }
            }

        }
    }
}
