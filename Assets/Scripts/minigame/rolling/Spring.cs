using System.Collections.Generic;
using UnityEngine;


namespace minigame
{
    namespace rolling
    {
        public class Spring : MonoBehaviour
        {
            public SpriteRenderer spriteRenderer;
            public Animated anim;
            public Animated.Animation animAnim;
            public Vector2 direction = Vector2.up;
            public float power = 312f;

            private Rigidbody2D bodyBuffer;

            void OnTriggerEnter2D(Collider2D collision)
            {
                var body = collision.GetComponent<Rigidbody2D>();
                if(body != null && body.GetComponent<Ball>() != null)
                {
                    if(this.anim.currentAnimation != null) {
                        this.bodyBuffer = body;
                        return;
                    }
                    else
                        Launch(body);
                }
            }

            // Sometimes the ball can end up stopping on a spring because it was busy animating
            private void OnTriggerStay2D(Collider2D collision)
            {
                if(this.bodyBuffer != null && this.anim.currentAnimation == null)
                {
                    Launch(this.bodyBuffer);
                    this.bodyBuffer = null;
                }
            }

            void Launch(Rigidbody2D body)
            {
                var ball = body.GetComponent<Ball>();
                if(ball != null)
                {
                    var dir = this.transform.localRotation * this.direction;
                    var v = body.velocity;
                    // cancel velocity opposing the direction of the spring
                    if(v.x * dir.x < 0) v.x = 0f;
                    if(v.y * dir.y < 0) v.y = 0f;
                    body.velocity = v;
                    body.AddForce(dir * power, ForceMode2D.Impulse);
                    this.anim.PlayOnce(animAnim);
                
                    if(this.power >= 612)
                        Audio.instance.PlaySfx(ball.game.assets.audioClipLookup["sfx_golf_spring_strong"]);
                    else
                        Audio.instance.PlaySfx(ball.game.assets.audioClipLookup["sfx_golf_spring_weak"]);
                }
            }

        }
    }
}
