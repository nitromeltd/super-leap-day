using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace minigame
{
    namespace rolling
    {
        public class SwingAnchor : MonoBehaviour
        {

            public RollingGame game;
            public SpriteRenderer spriteRendererClaw;
            public SpriteRenderer spriteRendererBolt;
            public SpriteRenderer [] spriteRendererRadius;
            public LineRenderer lineRenderer;
            public Transform transformTurn;
            public float radius;
            public float dir;
            public float lengthJoint;

            public Sprite spriteBoltIdle;
            public Sprite spriteBoltGrab;
            public Sprite spriteClawIdle;
            public Sprite spriteClawGrab;

            [Header("Motorised parts")]
            public bool motorised;
            public Transform transformBaseCog;
            public Transform transformBaseCogPin;
            public Transform transformChainCog;
            public Transform transformChainCogPin;
            public Animated animatedChain;
            public Animated.Animation animChain;
            private float angleCog;

            Ball ball;
            const float OffsetLineStart = 0.65f;
            const float OffsetClawStart = 1.56f;
            const float AngleCogSpeed = 8f;
            const float OffsetGrabMin = 2f;

            public const float RadiusInnerDefault = 5.74285f;
            public const float RadiusOuterDefault = 6f;
            public const float WidthBorder = 0.2f;

            public void Init(RollingGame game, float dir)
            {
                this.game = game;
                this.dir = dir;

                this.spriteRendererRadius = new[]{
                    game.spriteRendererPool.Get(this.transform.position, game.assets.spriteLookup["Spinner Radius 2"], RollingGame.SortingLayerBake, "radius2", game.map.transformBackground),
                    game.spriteRendererPool.Get(this.transform.position, game.assets.spriteLookup["Spinner Radius 1"], RollingGame.SortingLayerBake, "radius1", game.map.transformBackground),
                    game.spriteRendererPool.Get(this.transform.position, game.assets.spriteLookup["Spinner Radius 0"], RollingGame.SortingLayerBake, "radius0", game.map.transformBackground)
                };
                for(int i = 0; i < spriteRendererRadius.Length; i++)
                    this.spriteRendererRadius[i].sortingOrder = -100 + i;

                SetRadius(RadiusOuterDefault);

                this.motorised = dir != 0f;
                this.radius = RadiusOuterDefault;

                this.lineRenderer.sortingLayerName = game.objectLayers.objectsSortingLayer;
                this.lineRenderer.sortingOrder = 4;
                this.lineRenderer.material.mainTextureOffset = Vector2.zero;
                this.lineRenderer.material.SetFloat("_ScrollOffset", 0);
                this.spriteRendererClaw.transform.localPosition = Vector3.right * OffsetClawStart;
                this.lineRenderer.enabled = false;


            }

            public void SetRadius(float value)
            {
                this.radius = value;
                this.spriteRendererRadius[0].transform.localScale = Vector3.one * (1f / RadiusOuterDefault) * value;
                this.spriteRendererRadius[1].transform.localScale = Vector3.one * (1f / RadiusInnerDefault) * (value - WidthBorder);
            }

            public void SetGrab(Ball ball)
            {
                if(ball == null)
                {
                    if(this.motorised == true)
                    {
                        var delta = this.transform.position - this.ball.transform.position;
                        var vector = Vector2.Perpendicular(delta.normalized);
                        this.ball.body.velocity = vector * this.dir * 24f;

                        this.animatedChain.Stop();
                        Audio.instance.StopSfxLoop(game.assets.audioClipLookup["sfx_rolling_swinganchor_motor_loop"]);
                    }

                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_rolling_swinganchor_release"]);
                    this.ball = null;
                    this.lineRenderer.enabled = false;
                    this.spriteRendererClaw.transform.localPosition = Vector3.right * OffsetClawStart;
                    this.spriteRendererBolt.sprite = this.spriteBoltIdle;
                    this.spriteRendererClaw.sprite = this.spriteClawIdle;
                }
                else
                {
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_rolling_swinganchor_grab"]);
                    this.ball = ball;
                    this.spriteRendererBolt.sprite = this.spriteBoltGrab;
                    this.spriteRendererClaw.sprite = this.spriteClawGrab;
                    var delta = this.ball.transform.position - this.transform.position;
                    if(delta.magnitude < OffsetGrabMin)
                    {
                        delta = delta.normalized * OffsetGrabMin;
                        this.ball.body.MovePosition(this.transform.position + delta);
                    }
                    this.lengthJoint = delta.magnitude;
                    this.spriteRendererClaw.transform.localPosition = Vector3.right * Mathf.Max(delta.magnitude, OffsetClawStart);
                    this.lineRenderer.enabled = true;
                    UpdateLineRenderer(delta);

                    if(this.motorised == true)
                    {
                        this.animatedChain.PlayAndLoop(this.animChain);
                        if(Audio.instance.IsSfxPlaying(game.assets.audioClipLookup["sfx_rolling_swinganchor_motor_loop"]) == false)
                        {
                            Audio.instance.PlaySfxLoop(game.assets.audioClipLookup["sfx_rolling_swinganchor_motor_loop"]);
                            StartCoroutine(Audio.instance.ChangeSfxVolume(game.assets.audioClipLookup["sfx_rolling_swinganchor_motor_loop"], startFrom: 0, setTo: 1f, duration: .1f));
                        }

                    }
                }
            }

            public void UpdateRotation()
            {
                Vector3 delta;
                if(this.ball != null)
                {
                    delta = this.ball.transform.position - this.transform.position;
                    UpdateLineRenderer(delta);
                }
                else
                    delta = this.game.ClosestBallDelta(this.transform.position);

                var angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;
                var rotation = Quaternion.Euler(0, 0, angle);

                this.transformTurn.localRotation = rotation;
                
                if(this.motorised == true)
                {
                    var rotationInv = Quaternion.Euler(0, 0, -angle);
                    this.transformChainCogPin.localRotation =
                        rotationInv;

                    if(this.ball != null)
                    {
                        this.angleCog -= this.dir * AngleCogSpeed;
                        var rotationCog = Quaternion.Euler(0, 0, this.angleCog);
                        this.transformBaseCog.localRotation =
                        this.transformChainCog.localRotation =
                            rotationCog;
                    }
                }
            }

            public void UpdateLineRenderer(Vector3 delta)
            {
                this.lineRenderer.positionCount = 2;
                this.lineRenderer.SetPositions(new[] {
                    this.transform.position + delta.normalized * OffsetLineStart,
                    this.ball.transform.position
                });
            }
        }
    }
}