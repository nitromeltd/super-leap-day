using System.Collections.Generic;
using UnityEngine;

namespace minigame
{
    namespace rolling
    {

        public class TriggerButton : MonoBehaviour
        {

            public RollingGame game;
            public List<Motor> motors = new List<Motor>();
            public BoxCollider2D trigger;
            public SpriteRenderer spriteRenderer;
            public List<Vector2Int> targets = new List<Vector2Int>();
            public bool multiball;

            public void Init(RollingGame game)
            {
                this.game = game;
            }

            public void AddMotor(Motor m)
            {
                this.motors.Add(m);
                m.paused = true;
            }


            void OnTriggerEnter2D(Collider2D collision)
            {

                if(this.game.IsComplete()) return;// don't trigger events during end game

                if(this.game != null)
                {
                    var center = Vector3.zero;
                    this.spriteRenderer.sprite = this.game.assets.spriteLookup["button_down"];
                    if(this.multiball)
                    {
                        this.game.AddMultiBall();
                    }
                    else
                    {
                        foreach(var m in this.motors)
                        {
                            m.paused = false;
                            m.isLookAtTween = true;
                            center += m.transform.localPosition;
                        }
                        center /= this.motors.Count;
                        this.game.EventPause();
                        var lookAtTween = new CamCtrl.LookAtTween(
                            this.game.camCtrl, this.game.cam.transform.localPosition, center, this.game.IsMotorLookAtTweenFinished, 0.5f, 0.5f, true, () =>
                            {
                                this.game.EventUnpause();
                            }
                        );
                        lookAtTween.lookStartAction = () => { Audio.instance.PlaySfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]); };
                        lookAtTween.lookFinishAction = () => { Audio.instance.StopSfxLoop(this.game.assets.audioClipLookup["GLOBAL:sfx_golf_moving_block"]); };
                        this.game.camCtrl.AddLookAtTween(lookAtTween);
                    }
                    this.trigger.enabled = false;
                    Audio.instance.PlaySfx(this.game.assets.audioClipLookup["sfx_env_switch_engage"]);
                }
            }

        }
    }
}
